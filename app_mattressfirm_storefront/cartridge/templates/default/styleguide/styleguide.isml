<!--- TEMPLATENAME: styleguide.isml --->
<iscontent type="text/html" charset="UTF-8" compact="true"/>

<isdecorate template="styleguide/components/pt_styleguide">
	<isinclude template="util/modules"/>

	<h1>Mattress Firm Style Guide</h1>

	<h2 class="section-header">Fluid Grids</h2>
	<p>As needed, custom page styling should extend the fluid grid system developed for this site.  This Style Guide also utilizes this system.</p>
	<p><a href="${URLUtils.url('StyleGuide-Grids')}">Please see fluid grid examples here</a>.</p>

	<h2 class="section-header">Colors</h2>

	<div class="color-grid responsive-grid">
		<div class="grid-row">
			<div class="grid-col color">
				<div class="color-block"></div>
				<div class="color-name">$crimson (${"#"}e5173e)</div>
				<div class="color-use">
					• Sale Price<br>
					• Error Messaging<br>
					• Red Icons<br>
					• Link Hover States<br>
					• Primary Button Color
                </div>
			</div>

			<div class="grid-col color">
				<div class="color-block"></div>
				<div class="color-name">$night-rider (${"#"}333333)</div>
				<div class="color-use">
					• Headings<br>
					• Product Names<br>
					• Category Navigation Bar<br>
					• Header/Footer Links<br>
					• Gray Icons
                </div>
			</div>

			<div class="grid-col color">
				<div class="color-block"></div>
				<div class="color-name">$dark-gray (${"#"}AAAAAA)</div>
				<div class="color-use">
					• Breadcrumbs<br>
					• Other Ancillary Text<br>
					• Lines & Outlines
                </div>
			</div>

			<div class="grid-col color">
				<div class="color-block"></div>
				<div class="color-name">$concrete (${"#"}f2f2f2)</div>
				<div class="color-use">• Light Background Color for Contained Contentr</div>
			</div>

			<div class="grid-col color">
				<div class="color-block"></div>
				<div class="color-name">$mirage (${"#"}394848)</div>
				<div class="color-use">• Dark Background Color for Contained Content</div>
			</div>

			<div class="grid-col color">
				<div class="color-block"></div>
				<div class="color-name">$summer-sky (${"#"}40BDDA)</div>
				<div class="color-use">
                    • Secondary Button Color
                </div>
			</div>

			<div class="grid-col color">
				<div class="color-block"></div>
				<div class="color-name">$fruit-salad (${"#"}4C994C)</div>
				<div class="color-use">• Success Message <br>
					• Gift Card Balance</div>
			</div>

			<div class="grid-col color">
				<div class="color-block"></div>
				<div class="color-name">$white (${"#"}FFFFFF)</div>
				<div class="color-use">
                    • White Text on Dark Backgrounds
                </div>
			</div>

			<div class="grid-col color">
				<div class="color-block"></div>
				<div class="color-name">$egyptian-blue (${"#"}13428A)</div>
				<div class="color-use">
					• Accent Color Dark Blue
				</div>
			</div>

			<div class="grid-col color">
				<div class="color-block"></div>
				<div class="color-name">$navy-blue (${"#"}006FBA)</div>
				<div class="color-use">
					• Accent Color Light Blue
				</div>
			</div>

			<div class="grid-col color">
				<div class="color-block"></div>
				<div class="color-name">$sweet-corn (${"#"}f9e375)</div>
				<div class="color-use">
					• Accent Color Yellow
				</div>
			</div>
		</div>
	</div>

	<h1 class="section-header">Typography</h1>

	<h3>Fonts</h3>

	<div class="fonts style-section">
		<h3>Ubuntu Bold<small> (%font-bold)</small></h3>
		<h3>Ubuntu Regular<small> (%font-regular)</small></h3>
	</div>

	<h3>Headings</h3>

	<div class="headings style-section">
		<div class="right">Page Title, Price on PDP</div>
		<div class="block-page-title">Ubuntu Bold / 24 px</div>
		<small>Color: #333333, Uppercase, Line-height: 26px</small>
		<hr>
		<div class="right">Product Name on PDP, Section Title</div>
		<div class="block-product-name">Ubuntu Regular / 24 px</div>
		<small>Color: #333333, Title Case, Line-height: 26px</small>
		<hr>
		<div class="right">Secondary Heading, Brand Title on PDP, Modal Window Title</div>
		<div class="secondary-heading">Ubuntu Bold / 18 px</div>
		<small>Color: #333333, Upper Case, Line-height: 20px</small>
		<hr>
		<div class="right">Attribute Title on PDP and Browse Pages, Brand Title on Browse</div>
		<div class="attribute-title">Ubuntu Bold / 14 px</div>
		<small>Color: #333333, Upper Case, Line-height: 16px</small>
		<hr>
		<div class="right">Paragraph Headings</div>
		<div class="paragraph-head">Ubuntu Bold / 14 px</div>
		<small>Color: #333333, Sentence Case, Line-height: 21px</small>
		<hr>
		<div class="right">Product Name on Browse, Category and Attribute Name on Refinements, Paragraph Text</div>
		<div class="product-name-browse">Ubuntu Regular / 14 px</div>
		<small>Color: #333333, Title Case, Line-height: 21px</small>
	</div>

	<h3>Paragraph Styles</h3>

	<div class="paragraphs style-section">
		<div class="paragraph-grid responsive-grid">
			<div class="grid-row">
				<div class="grid-col">
					<p>
						Paragraph Body Text - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur rhoncus nisi nisl, sed posuere arcu facilisis quis.
						Etiam quis erat tincidunt, posuere mauris ut, bibendum massa. Nullam rhoncus, velit vitae posuere dapibus, lectus lorem
						suscipit ligula, id convallis ex justo quis urna. <a href="javascript:void(0);">This is a link within a paragraph</a>. Morbi ullamcorper non arcu vel ornare. Integer elementum, erat et
						bibendum tristique, neque dui sollicitudin risus, et facilisis felis augue vitae arcu. Maecenas ut mauris semper, gravida
						ante a, viverra diam. Sed sed lectus at velit aliquet aliquet non rhoncus dui. Curabitur tristique lectus eros, in pulvinar
						odio rhoncus nec. Pellentesque ornare lobortis velit, in sagittis metus ultricies id. Integer vitae pretium felis. Maecenas
						dignissim gravida lacinia.
					</p>

					<p class="style-spec">Font Family: Ubuntu Regular</p>
					<p class="style-spec">Font Size: 14px</p>
					<p class="style-spec">Line Height: 21px</p>
					<p class="style-spec">Text-Color: 333333</p>
				</div>

				<div class="grid-col">
					<p class="">
						Smaller Paragraph Text (.alt-p) - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur rhoncus nisi nisl, sed posuere arcu facilisis quis.
						Etiam quis erat tincidunt, posuere mauris ut, bibendum massa. Nullam rhoncus, velit vitae posuere dapibus, lectus lorem
						suscipit ligula, id convallis ex justo quis urna. <a href="javascript:void(0);">This is a link within a smaller paragraph</a>. Morbi ullamcorper non arcu vel ornare. Integer elementum, erat et
						bibendum tristique, neque dui sollicitudin risus, et facilisis felis augue vitae arcu.
					</p>
				</div>

				<div class="grid-col">
					<p class="">
						Small Informational Text (.info) - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur rhoncus nisi nisl, sed posuere arcu facilisis quis.
						Etiam quis erat tincidunt, posuere mauris ut, bibendum massa. Nullam rhoncus, velit vitae posuere dapibus, lectus lorem
						suscipit ligula, id convallis ex justo quis urna. <a href="javascript:void(0);">This is a link within an info text paragraph</a>. Morbi ullamcorper non arcu vel ornare. Integer elementum, erat et
						bibendum tristique, neque dui sollicitudin risus, et facilisis felis augue vitae arcu.
					</p>
				</div>
			</div>
		</div>
	</div>

	<h3>List Styles</h3>

	<div class="lists style-section">
		<div class="list-grid responsive-grid">
			<div class="grid-row">
				<div class="grid-col">
					<h4>Unordered List (ul)</h4>
					<ul>
						<li>Item 1 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
						<li>Item 2 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
						<li>Item 3 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
					</ul>
				</div>

				<div class="grid-col">
					<h4>Ordered List (ol)</h4>
					<ol>
						<li>Item 1 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
						<li>Item 2 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
						<li>Item 3 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
					</ol>
				</div>
			</div>
		</div>

		<p class="style-spec">Font Family: Ubuntu Regular</p>
		<p class="style-spec">Font Size: 14px</p>
		<p class="style-spec">Line Height: 21px</p>
		<p class="style-spec">Text-Color: ${'#'}333333</p>
	</div>

	<h3>Links</h3>

	<div class="links style-section">
		<a href="javascript:void(0);" class="link-paragraph">Paragraph Links</a>
		<small>(Ubuntu Regular 14px)</small>
		<hr>
		<a href="javascript:void(0);" class="link-footer">Footer Links</a>
		<small>(Ubuntu Regular 14px)</small>
		<hr>
		<a href="javascript:void(0);" class="link-header-utility">Header Utility Links</a>
		<small>(Ubuntu Regular 12px)</small>
		<hr>
		<a href="javascript:void(0);" class="link-main-navigation">Header Category Navigation Links</a>
		<small>(Ubuntu Bold 14px)</small>
		<hr>
		<a href="javascript:void(0);" class="link-header-edit">Standalone Call to Action (Edit, Remove) Links</a>
		<small>(Ubuntu Regular 14px)</small>
		<hr>
		<a href="javascript:void(0);" class="link-refinement-attr">Browse Page Refinement Attribute & Category Links</a>
		<small>(Ubuntu Regular 14px)</small>
		<hr>
	</div>

	<h1>Form Elements</h1>

	<div class="forms responsive-grid">
		<form>
			<fieldset>
				<div class="grid-row">

					<div class="grid-col">
						<h3>Input Fields (options and variations)</h3>

						<isinputfield formfield="${pdict.CurrentForms.styleguide.exampleinput}" type="input" fieldclass="custom-field-class" placeholder="Place Holder Text"/>

						<isinputfield formfield="${pdict.CurrentForms.styleguide.examplerequired}" type="input" placeholder="Focus/unfocus field to see error state" /><!-- Required status must be defined in form xml (madatory="true") -->

						<isinputfield formfield="${pdict.CurrentForms.styleguide.examplecaption}" type="input" /><!-- this caption is defined in form xml -->

						<isinputfield formfield="${pdict.CurrentForms.styleguide.examplepassword}" type="password" caption="Optional caption text in markup"/>

						<isinputfield formfield="${pdict.CurrentForms.styleguide.exampleemail}" type="email" placeholder="See touch keyboard in mobile" />

						<isinputfield formfield="${pdict.CurrentForms.styleguide.examplephone}" type="phone" helpcid="help-telephone" helplabel="(Tooltip?)" rowClass="has-tooltip" placeholder="See touch keyboard in mobile" />

						<isinputfield formfield="${pdict.CurrentForms.styleguide.exampletextarea}" type="textarea"/>

					</div>

					<div class="grid-col">

						<h3>Selects Boxes, Radios, and Checkboxes</h3>

						<isinputfield formfield="${pdict.CurrentForms.styleguide.exampleselectbox}" type="select" />

						<isinputfield formfield="${pdict.CurrentForms.styleguide.examplecheckbox}" type="checkbox" />

						<isinputfield formfield="${pdict.CurrentForms.styleguide.examplecheckbox2}" type="checkbox" caption="optional caption for checkbox" />

						<isinputfield formfield="${pdict.CurrentForms.styleguide.exampleradios}" type="radio" />    <!-- option 2 is checked by default in the form xml default-value -->

						<isinputfield formfield="${pdict.CurrentForms.styleguide.exampleradios2}" type="radio" rowclass="options-inline"/>

						<!-- Manual markup is sometimes required for custom checkbox and radio styling -->
						<h3>Using custom markup</h3>

						<div class="form-row label-inline checkbox form-checkbox">
							<input class="input-checkbox uniform" type="checkbox" checked="checked" id="checked-box-1" />
							<label class="checkbox" for="checked-box-1">Checkbox Label</label>
							<input class="input-checkbox uniform" type="checkbox" id="unchecked-box-2" />
							<label class="checkbox" for="unchecked-box-2">Checkbox Label</label>
						</div>

						<div class="form-row label-inline radio-custom  form-radio">
							<input class="input-radio uniform" type="radio" id="radio-button-1" name="radio-test" />
							<label class="radio" for="radio-button-1">Radio Label</label>
							<input class="input-checkbox uniform" type="radio" id="radio-button-2" name="radio-test" />
							<label class="radio" for="radio-button-2">Radio Label</label>
						</div>
					</div>

					<iscomment> Commenting our this stylistically redundant example of custom markup form elements...
						<div class="grid-col">
							<h3>Using custom markup</h3>

							<div class="form-row">
								<label>Input Label</label>
								<input class="input-text" type="text" placeholder="Place Holder Text" />
							</div>

							<div class="form-row required">
								<label>Required Field<span class="required-indicator">*</span></label>
								<input class="input-text required" type="text" placeholder="Focus/unfocus field to see error state" />
							</div>

							<div class="form-row">
								<label>Field with Caption</label>
								<input class="input-text" type="text" />
								<div class="form-caption">Optional caption text in markup</div>
							</div>

							<div class="form-row required">
								<label>
									Password (required)<span class="required-indicator">*</span>
								</label>
								<input class="input-text required" type="password" />
								<div class="form-caption">Optional caption text in markup</div>
							</div>

							<div class="form-row">
								<label>Email</label>
								<input class="input-text" type="email" placeholder="See touch keyboard in mobile" />
							</div>

							<div class="form-row has-tooltip">
								<label>Phone</label>
								<div class="field-wrapper">
									<input class="input-text" type="tel" placeholder="See touch keyboard in mobile" />
								</div>
								<div class="form-field-tooltip">
									<a href="${URLUtils.url('Page-Show', 'cid', 'help-phone')}" class="tooltip">(Tooltip?)
										<div class="tooltip-content" data-layout="small">
											<iscontentasset aid="help-telephone" />
										</div>
									</a>
								</div>
							</div>

							<div class="form-row">
								<label>Select Label</label>
								<select class="input-select">
									<option value="">Select Option 1</option>
									<option value="">Select Option 2</option>
								</select>
							</div>

							<div class="form-row ">
								<label for="example_textarea"><span>Textarea</span></label>
								<textarea class="input-textarea" id="example_textarea" name="example_textarea"></textarea>
							</div>

							<h3><small>Custom markup is generally preferred for checkboxes and radio buttons</small></h3>
							<div class="form-row label-inline checkbox">
								<input class="input-checkbox" type="checkbox" checked="checked" id="checked-box-1" />
								<label class="checkbox" for="checked-box-1">Checkbox Label</label>
								<input class="input-checkbox" type="checkbox" id="unchecked-box-2" />
								<label class="checkbox" for="unchecked-box-2">Checkbox Label</label>
							</div>

							<div class="form-row label-inline radio-custom">
								<input class="input-radio" type="radio" id="radio-button-1" name="radio-test" />
								<label class="radio" for="radio-button-1">Radio Label</label>
								<input class="input-checkbox" type="radio" id="radio-button-2" name="radio-test" />
								<label class="radio" for="radio-button-2">Radio Label</label>
							</div>

						</div>
					</iscomment>

				</div>
			</fieldset>
		</form>
	</div>

	<h2 class="section-header">Buttons</h2>

	<div class="button-grid responsive-grid">
		<div class="grid-row">
			<div class="button-style grid-col">
				<button class="">Sign Up</button><br/>
                <div class="button-description">
                    <p class="style-spec">Button Height: 40 px</p>
					<p class="style-spec">Side Padding: 20 px</p>
					<p class="style-spec">Border Radius: 10px</p>
					<p class="style-spec">Font: Ubuntu Bold / 14px</p>
					<p class="style-spec">Text Color: ${'#'}FFFFFF</p>
					<p class="style-spec">Background Color: ${'#'}ed1d24</p>
                </div>
				<button class="secondary">Apply</button><br/>
				<div class="button-description">
					<p class="style-spec">Button Height: 40 px</p>
					<p class="style-spec">Side Padding: 20 px</p>
					<p class="style-spec">Border Radius: 10px</p>
					<p class="style-spec">Font: Ubuntu Bold / 14px</p>
					<p class="style-spec">Text Color: ${'#'}FFFFFF</p>
					<p class="style-spec">Background Color: ${'#'}40bdda</p>
				</div>
			</div>

			<div class="button-style grid-col">
				<button disabled="disabled">Disabled Button</button><br/>
				<button disabled="disabled" class="secondary">Disabled Button</button>
			</div>

			<div class="button-style grid-col">
				<h3>Text Button</h3>
				<p>The following is a button rendered like a normal paragraph link. This <button class="button-text">text button</button> can be implemented with the following markup:</p>
				<p>&lt;button class="button-text"&gt;&lt;/button&gt;.</p>
			</div>
		</div>
	</div>

	<h2 class="section-header">UI Elements</h2>

	<h3>Dialogs</h3>

	<div class="dialogs style-section">

		<button id="dialog-example">Dialog</button>

		<div id="dialog-message" title="Modal Window Heading">
			<div class="paragraph-head">Paragraph Heading</div>
			<p>Paragraph. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
		</div>

	</div>

	<h3>Tooltips</h3>

	<div class="tooltips style-section">
		<a href="javascript:void(0);" class="tooltip">Tooltip Link
			<div class="tooltip-content" data-layout="small">
				<p><strong>Headine</strong></p>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur rhoncus nisi nisl, sed posuere arcu facilisis quis. Etiam quis erat tincidunt, posuere mauris ut, bibendum massa. Nullam rhoncus, velit vitae posuere dapibus, lectus lorem suscipit ligula, id convallis ex justo quis urna.</p>
			</div>
		</a>

	</div>

	<h2 class="section-header">Table Style</h2>

	<table class="item-list">
		<thead>
		<tr>
			<th>Table Header 1</th>
			<th>Table Header 2</th>
			<th>Table Header 3</th>
			<th>Table Header 4</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td>
				<div class="paragraph-head">Content Heading</div>
				Lorem ipsum dolor sit amet,
				consectetur adipisicing elit, sed
				do eiusmod tempor incididunt
				ut labore et dolore magna aliqua.
			</td>
			<td><div class="paragraph-head">Content Heading</div>
				Lorem ipsum dolor sit amet,
				consectetur adipisicing elit, sed
				do eiusmod tempor incididunt
				ut labore et dolore magna aliqua.</td>
			<td><div class="paragraph-head">Content Heading</div>
				Lorem ipsum dolor sit amet,
				consectetur adipisicing elit, sed
				do eiusmod tempor incididunt
				ut labore et dolore magna aliqua.</td>
			<td><div class="paragraph-head">Content Heading</div>
				Lorem ipsum dolor sit amet,
				consectetur adipisicing elit, sed
				do eiusmod tempor incididunt
				ut labore et dolore magna aliqua.</td>
		</tr>
		<tr>
			<td><div class="paragraph-head">Content Heading</div>
				Lorem ipsum dolor sit amet,
				consectetur adipisicing elit, sed
				do eiusmod tempor incididunt
				ut labore et dolore magna aliqua.</td>
			<td><div class="paragraph-head">Content Heading</div>
				Lorem ipsum dolor sit amet,
				consectetur adipisicing elit, sed
				do eiusmod tempor incididunt
				ut labore et dolore magna aliqua.</td>
			<td><div class="paragraph-head">Content Heading</div>
				Lorem ipsum dolor sit amet,
				consectetur adipisicing elit, sed
				do eiusmod tempor incididunt
				ut labore et dolore magna aliqua.</td>
			<td></td>
		</tr>
		</tbody>
	</table>

	<h2 class="section-header">Iconography</h2>

	<div class="icons">
		<h3>Logos</h3>

		<div class="logos style-section">

			<div class="logo-col">
				<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" class="lazyload" data-src="${URLUtils.staticURL('images/logo.png')}" alt="Sleepy's Logo" />
				<p class="style-spec">Dimensions: 262px x 68px</p>
			</div>

		</div>

		<h3>SVG Sprite Icons</h3>

		<div class="svg-icons">

			<div class="icon-col">
				<issvghelper icon="checkmark" />
				<div class="style-spec">checkmark (for input checkboxes)</div>
			</div>

			<div class="icon-col">
				<issvghelper icon="radio-dot" />
				<div class="style-spec">radio-dot (for radio buttons)</div>
			</div>

			<div class="icon-col">
				<issvghelper icon="down-triangle" />
				<div class="style-spec">down-triangle (for select boxes)</div>
			</div>

			<div class="icon-col">
				<issvghelper icon="live-chat" extraclasses="header-icons" />
				<div class="style-spec">live-chat (header)</div>
			</div>

			<div class="icon-col">
				<issvghelper icon="help" extraclasses="header-icons" />
				<div class="style-spec">help (header)</div>
			</div>

			<div class="icon-col">
				<issvghelper icon="my-account" extraclasses="header-icons" />
				<div class="style-spec">my-account (header)</div>
			</div>

			<div class="icon-col">
				<issvghelper icon="shopping-cart" extraclasses="header-icons" />
				<div class="style-spec">shopping-cart (header)</div>
			</div>

			<div class="icon-col">
				<issvghelper icon="search" />
				<div class="style-spec">search (header)</div>
			</div>

			<div class="icon-col">
				<issvghelper icon="circle-play" />
				<div class="style-spec">circle-play (PDP)</div>
			</div>

			<div class="icon-col">
				<issvghelper icon="circle-arrow" />
				<div class="style-spec">circle-arrow (PDP)</div>
			</div>

			<div class="icon-col">
				<issvghelper icon="circle-plus" />
				<div class="style-spec">circle-plus (PDP)</div>
			</div>

			<div class="icon-col">
				<issvghelper icon="circle-wifi" />
				<div class="style-spec">circle-wifi (PDP)</div>
			</div>

			<div class="icon-col">
				<issvghelper icon="credit-card" />
				<div class="style-spec">credit-card (my account)</div>
			</div>

			<div class="icon-col">
				<issvghelper icon="orders" />
				<div class="style-spec">orders (my account)</div>
			</div>

			<div class="icon-col">
				<issvghelper icon="address-book" />
				<div class="style-spec">address-book (my account)</div>
			</div>

			<div class="icon-col">
				<issvghelper icon="facebook" extraclasses="social-icons"/>
				<div class="style-spec">facebook (footer)</div>
			</div>

			<div class="icon-col">
				<issvghelper icon="twitter" extraclasses="social-icons"/>
				<div class="style-spec">twitter (footer)</div>
			</div>

			<div class="icon-col">
				<issvghelper icon="youtube" extraclasses="social-icons"/>
				<div class="style-spec">youtube (footer)</div>
			</div>

			<div class="icon-col">
				<issvghelper icon="linkedin" extraclasses="social-icons"/>
				<div class="style-spec">linkedin (footer)</div>
			</div>

			<div class="icon-col">
				<issvghelper icon="instagram" extraclasses="social-icons"/>
				<div class="style-spec">instagram (footer)</div>
			</div>
		</div>

	</div>
</isdecorate>