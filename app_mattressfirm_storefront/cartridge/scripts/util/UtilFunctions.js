var Site = require('dw/system/Site');
var PromotionMgr = require('dw/campaign/PromotionMgr');
var StringUtils = require('dw/util/StringUtils');
var Transaction = require('dw/system/Transaction');
/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var DeliveryCalendarUtils = require('~/cartridge/scripts/cart/DeliveryCalendarUtils').DeliveryCalendarUtils;
var ProductMgr = require('dw/catalog/ProductMgr');

var UtilFunctions = {
		
		isChicagoProduct : function (manufacturerSKU) {
			var Product = app.getModel('Product');
			var product = Product.get('mfi'+manufacturerSKU);
			var masterSku = '';
			if (!empty(product.object) && product.object.variationModel) {
				var masterProduct = product.object.variationModel.master;
				masterSku = masterProduct ? masterProduct.manufacturerSKU : '';
			}
			var enableChicagoDCDelivery = "enableChicagoDCDelivery" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("enableChicagoDCDelivery") : false;
		    var chicagoInventLocationId = '';
		    var parcelableProductIDs = '';
		    if (enableChicagoDCDelivery) {
		    	parcelableProductIDs = "parcelableProductIDs" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("parcelableProductIDs") : '';
		    	if( (!empty(manufacturerSKU) && parcelableProductIDs.indexOf(manufacturerSKU) > -1) || (!empty(masterSku) && parcelableProductIDs.indexOf(masterSku) > -1)) {
			    	return true;
			    }
		    }
		    return false;
		},
		isParcelableAndCore : function (manufacturerSKU) {
			var Product = app.getModel('Product');
			var product = Product.get('mfi'+manufacturerSKU);
			var masterProduct = product.object.isVariant() ? product.object.masterProduct : product.object;
			var enableChicagoDCDelivery = "enableChicagoDCDelivery" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("enableChicagoDCDelivery") : false;
			if (enableChicagoDCDelivery && !empty(masterProduct)) {
				if(masterProduct.custom.shippingInformation.toString().toLowerCase() == 'core' && masterProduct.custom.isParcelable){
					return true;
				}
			}
			return false;
		},
		
		isDropShip : function (manufacturerSKU) {
			var Product = app.getModel('Product');
			var product = Product.get('mfi'+manufacturerSKU);
			var masterProduct = product.object.isVariant() ? product.object.masterProduct : product.object;
			if (!empty(masterProduct)) {
				if(masterProduct.custom.shippingInformation.toString().toLowerCase().indexOf("drop") > -1){
					return true;
				}
			}
			return false;
		},

		isChicagoCampaign : function (lineItem) {
				// Check if it is chicago gift campaign
				var isChicagoProduct = !empty(lineItem.product) ? this.isParcelableAndCore(lineItem.product.manufacturerSKU) : false;
				var promotions = PromotionMgr.activeCustomerPromotions.getProductPromotions(lineItem.product);
				var isChicagoCampaign = false;
				//var enableChicagoDCDelivery = "enableChicagoDCDelivery" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("enableChicagoDCDelivery") : false;
				var chicagoCampaignID = "chicagoCampaignID" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("chicagoCampaignID") : '';
				var chicagoCampaign = !empty(chicagoCampaignID) ? PromotionMgr.getCampaign(chicagoCampaignID) : null;
				if (isChicagoProduct && !empty(chicagoCampaign)) {
    				for (var i=0; i < promotions.length; i++) {
    	        	  	if ( promotions[i].getCampaign().ID == chicagoCampaign.ID ) {
    	        	  	isChicagoCampaign = true;
    	        	  	break;	                                          	  	
    	        	  	}
        	  	}
        	  }
        	  return isChicagoCampaign;
		},
		chicagoInventLocationId : function () {
			var enableChicagoDCDelivery = "enableChicagoDCDelivery" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("enableChicagoDCDelivery") : false;
    	    var chicagoInventLocationId = '';
    	    if (enableChicagoDCDelivery) {
    	    	chicagoInventLocationId = Site.getCurrent().getCustomPreferenceValue('chicagoInventLocationId');
    	    }
    	    return chicagoInventLocationId;
		},
		appendParamToURL: function (url, name, value) {
			// quit if the param already exists
			if (url.indexOf(name + '=') !== -1) {
				return url;
			}
			var separator = url.indexOf('?') !== -1 ? '&' : '?';
			var refinedURL = url + separator + name + '=' + encodeURIComponent(value);
			return refinedURL;
		},
		appendParamsToUrl: function (url, params) {
			var _url = url;
			for(var key in params) {
				_url = this.appendParamToURL(_url, key, params[key]);
			};
			return _url;
		},
		isCyberMondayPromotionProduct: function (productId) {
			
			var result = false;
			
			if (!empty(productId)){
				var productModel = app.getModel('Product');
				var product = productModel.get(productId);
				var masterID = '';
				if (!empty(product.object) && product.object.variationModel) {
					var masterProduct = product.object.variationModel.master;
					masterID = masterProduct ? masterProduct.ID : '';
				}
				
				var cyberMondayPromotionProductIDs = "cmEmailSignupProductIds" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("cmEmailSignupProductIds") : '';
		    	
				if (!empty(cyberMondayPromotionProductIDs)){
					if(!empty(masterID) && cyberMondayPromotionProductIDs.indexOf(masterID) > -1) {
				    	result = true;
				    }
				}
			}
		    return result;
		},
		sortAndJoinArrayElements : function (Arr) {
			var resultsKey : String = "";
			if (!empty(Arr) && Arr.length > 0) {
				resultsKey = Arr.sort().join('_');
			}
			return resultsKey;
		},
		existsInCustomPreference: function (preferenceId, elementId) {
			
			var result = false;			
			if(!empty(preferenceId) && !empty(elementId)) {				
				var listIds = preferenceId in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue(preferenceId) : '';
		    	
				if (!empty(listIds)){					
					if(listIds.indexOf(elementId) > -1) {
				    	result = true;
				    }
				}
			}
			return result;		
		},
		getCarrierDetails: function (carrierCode, trackingNo) {
			
			var trackURL = "";
			var carrierName = "";
			
			if(!empty(carrierCode)){
				
				var baseURL = "";
				// Fedex
				if(this.existsInCustomPreference("dispatchTrackFedexCarrierCodes", carrierCode )){
					carrierName = "dispatchTrackFedexCarrierName" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("dispatchTrackFedexCarrierName") : '';
					baseURL 	= "dispatchTrackFedexCarrierURL" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("dispatchTrackFedexCarrierURL") : '';
					trackURL = baseURL + trackingNo;
				// Global Tranz	
				}else if(this.existsInCustomPreference("dispatchTrackGlobalTranzCarrierCodes", carrierCode )){
					carrierName = "dispatchTrackGlobalTranzCarrierName" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("dispatchTrackGlobalTranzCarrierName") : '';
					baseURL 	= "dispatchTrackGlobalTranzCarrierURL" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("dispatchTrackGlobalTranzCarrierURL") : '';
					trackURL = baseURL;
				// R&L Carriers	
				}else if(this.existsInCustomPreference("dispatchTrackRLCarriersCarrierCodes", carrierCode )){
					carrierName = "dispatchTrackRLCarriersCarrierName" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("dispatchTrackRLCarriersCarrierName") : '';
					baseURL 	= "dispatchTrackRLCarriersCarrierURL" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("dispatchTrackRLCarriersCarrierURL") : '';
					trackURL = baseURL;
				// CH Robinson
				}else if(this.existsInCustomPreference("dispatchTrackCHRobinsonCarrierCodes", carrierCode )){
					carrierName = "dispatchTrackCHRobinsonCarrierName" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("dispatchTrackCHRobinsonCarrierName") : '';
					baseURL 	= "dispatchTrackCHRobinsonCarrierURL" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("dispatchTrackCHRobinsonCarrierURL") : '';
					trackURL = baseURL + trackingNo;
				// JB Hunt
				}else if(this.existsInCustomPreference("dispatchTrackJBHuntCarrierCodes", carrierCode )){
					carrierName = "dispatchTrackJBHuntCarrierName" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("dispatchTrackJBHuntCarrierName") : '';
					baseURL 	= "dispatchTrackJBHuntCarrierURL" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("dispatchTrackJBHuntCarrierURL") : '';
					trackURL = baseURL;
				// Progistics	
				}else if(this.existsInCustomPreference("dispatchTrackProgisticsCarrierCodes", carrierCode )){
					carrierName = "dispatchTrackProgisticsCarrierName" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("dispatchTrackProgisticsCarrierName") : '';
					baseURL 	= "dispatchTrackProgisticsCarrierURL" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("dispatchTrackProgisticsCarrierURL") : '';
					trackURL = baseURL;
				// UPS
				}else if(this.existsInCustomPreference("dispatchTrackUPSCarrierCodes", carrierCode )){
					carrierName = "dispatchTrackUPSCarrierName" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("dispatchTrackUPSCarrierName") : '';
					baseURL 	= "dispatchTrackUPSCarrierURL" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("dispatchTrackUPSCarrierURL") : '';
					trackURL = baseURL + trackingNo;
					
				}else{
					trackURL = "";
					carrierName = carrierCode;
					
				}
			}
			return {url: trackURL, name: carrierName};
		},
		getWillCallDateStr: function () {
			
			var  dateStr = "";
			var leadDays = "willCallLeadDays" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("willCallLeadDays") : 60;
			var currentDate = new Date();
			currentDate.setDate(currentDate.getDate() + leadDays);
			dateStr = currentDate.toISOString();
				
		    return dateStr;
		},
		getAddressAtpInfoAttr_FromShipment: function(cart) {
			var inMarketShipment = cart.object.getShipment('In-Market');
		    
		    
		    var zone = 'null';
		    var deliveryTime = 'null';
		    var deliveryDate = 'null';
		    if(!empty(inMarketShipment)) {
			    var pliIterator = inMarketShipment.getProductLineItems().iterator();
			    while (pliIterator.hasNext()) {
			        var pli = pliIterator.next();
			        zone = pli.custom.mfiDSZoneLineId ? pli.custom.mfiDSZoneLineId : 'null';
			        break;
			    }
			    
			    
			    if(!empty(inMarketShipment) && !empty(inMarketShipment.custom.deliveryDate)) {
			    	deliveryDate = inMarketShipment.custom.deliveryDate ? inMarketShipment.custom.deliveryDate : 'null';
			    }
			    
			   
			    if(!empty(inMarketShipment) && !empty(inMarketShipment.custom.deliveryTime)) {
			    	deliveryTime = inMarketShipment.custom.deliveryTime ? inMarketShipment.custom.deliveryTime : 'null';
			    }
		    }
		    var addressAtpInfoAttr  = deliveryDate + '|' + deliveryTime +'|' + zone +'|'+ session.custom.customerZip;
		    return addressAtpInfoAttr;
		},
		setMobileDefaultDate: function(Basket, deliveryDatesArr) {
			var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;
			var isRedCarpet = mattressPipeletHelper.getOrderIsRedCarpetStatus(Basket);
			if (isRedCarpet) {
					if ( !empty(deliveryDatesArr) && deliveryDatesArr.length > 0 ) {
					var sessionDeliveryDates = session.custom.deliveryDates;
					var firstAvailableDateKey = DeliveryCalendarUtils.getFirstActiveSlotKey(sessionDeliveryDates);
					var firstAvailableDate = !empty(firstAvailableDateKey) ? sessionDeliveryDates[firstAvailableDateKey] : null;
					var firstAvailableSlot = null;
					if (!empty(firstAvailableDate) && !empty(firstAvailableDate.timeslots)) {
						for each(var slot in firstAvailableDate.timeslots) {
							if (slot.available.toUpperCase() == "YES") {
								firstAvailableSlot = slot;
								break;
							}
						}
					}
					if (!empty(firstAvailableSlot) && firstAvailableSlot != null) {
						var startTime = firstAvailableSlot.startTime;
						var	endTime = firstAvailableSlot.endTime
						var deliveryDate = firstAvailableSlot.SlotDate;
						var dateString = !empty(deliveryDate) ? new Date(deliveryDate).toISOString() : 'null';
						var timeString = !empty(startTime) && !empty(endTime) ? StringUtils.formatDate(startTime, "h:mma").toString() +'-' + StringUtils.formatDate(endTime, "h:mma").toString() : 'null';
						var zoneId = !empty(firstAvailableSlot.mfiDSZoneLineId)  ? firstAvailableSlot.mfiDSZoneLineId : 'null';
						var zonZipCode = !empty(firstAvailableSlot.mfiDSZipCode) ? firstAvailableSlot.mfiDSZipCode : 'null';
						var addressAtpInfoAttr  =  dateString + '|' + timeString +'|' + zoneId +'|'+ zonZipCode;
						if(empty(Basket.custom.defaultDeliveryDateTimeZoneZip)) {
							//2049-12-31T06:11:55.699Z|8:00am-8:00pm|DZL002446249|77025
							Transaction.wrap(function () {
								Basket.custom.defaultDeliveryDateTimeZoneZip = addressAtpInfoAttr;
							});
						}
						 
						var deliveryScheduleChange = request.httpParameterMap.deliveryScheduleChanged.submitted;//request.httpParameterMap.deliveryScheduleChanged.stringValue;//session.forms.singleshipping.shippingAddress.deliveryScheduleChanged.value;
						session.custom.deliveryScheduleChangeFromMobShipping = deliveryScheduleChange;
						if(!deliveryScheduleChange){
							//2049-12-31T06:11:55.699Z|8:00am-8:00pm|DZL002446249|77025
						    Transaction.wrap(function () {
						    	Basket.custom.selectedDeliveryDateTimeZoneZip = addressAtpInfoAttr;
						    });
						}
					    
					}
					
				} else {
				    var cart = app.getModel('Cart').get();
					var addressAtpInfoAttr = this.getAddressAtpInfoAttr_FromShipment(cart);
					if(empty(Basket.custom.defaultDeliveryDateTimeZoneZip)) {
						//2049-12-31T06:11:55.699Z|8:00am-8:00pm|DZL002446249|77025
						Transaction.wrap(function () {
							Basket.custom.defaultDeliveryDateTimeZoneZip = addressAtpInfoAttr;
						});
					}
					var deliveryScheduleChange = request.httpParameterMap.deliveryScheduleChanged.submitted;//request.httpParameterMap.deliveryScheduleChanged.stringValue;//session.forms.singleshipping.shippingAddress.deliveryScheduleChanged.value;
					session.custom.deliveryScheduleChangeFromMobShipping = deliveryScheduleChange;
					if(!deliveryScheduleChange){
						//2049-12-31T06:11:55.699Z|8:00am-8:00pm|DZL002446249|77025
					    Transaction.wrap(function () {
					    	Basket.custom.selectedDeliveryDateTimeZoneZip = addressAtpInfoAttr;
					    });
					}
				}
					
		  }//redcarpet check
			//Function end here//
		},
		getStateInformation: function(cart) {
		 var stateCode = "";
         if (!empty(cart)) {
           defaultShipment = cart.getDefaultShipment();
           if (!empty(defaultShipment)) {
            shippingAddress = defaultShipment.shippingAddress;
            if (!empty(shippingAddress)) {
                stateCode = shippingAddress.stateCode;
                if (!empty(stateCode)) {
                    stateCode = stateCode.toUpperCase();
                }
            }
           }
         }
         return stateCode;
       },
       buildProductsGeoAvailabilityMap : function (basket) {
    		var geoAvailability = true;
    		var isChicagoProduct;
    		var availability = true;
    		var productsGeoAvilabilityMAP = new dw.util.HashMap();
    		var ProductUtils = require('app_storefront_core/cartridge/scripts/product/ProductUtils.js');
    		for each(var pli in basket.productLineItems) {
    	    	if (!empty(pli) && !empty(pli.product)) {
    				geoAvailability = ProductUtils.checkGeoAvailability(pli.product);
    				isChicagoProduct = this.isParcelableAndCore(pli.product.manufacturerSKU);
    				availability = geoAvailability || isChicagoProduct;
    				productsGeoAvilabilityMAP.put(pli.productID, availability);
    	    	}
    	    }
    		return productsGeoAvilabilityMAP;
    	},
    	checkAllProductsGeoAvailable : function(basket,productsGeoAvailabilityMap) {
    		var allProductsGeoAvailable= true;
			for each(var pli in basket.productLineItems) {
		     	if(productsGeoAvailabilityMap[pli.productID] == false) {
		     		allProductsGeoAvailable = false;
		     		break;
		     	}
		    }
			return allProductsGeoAvailable;
    	},
    	escapeXml: function (xmlStr) {
       	    
       	    if(!empty(xmlStr)){
       		    return xmlStr.replace(/[<>&'"]/g, function (c) {
       		    	// Escape the invalid xml characters, sequence is important
       		        switch (c) {
       		            case '<': return '&lt;';
       		            case '>': return '&gt;';
       		            case '&': return '&amp;';
       		            case '\'': return '&apos;';
       		            case '"': return '&quot;';
       		        }
       		    });
       		}
       	},
       	jcCategoryTest: function (keyword, product) {
    		var masterProduct = product.variationModel.getMaster();
    		var jcTest: Bolean = false;
    		if(!empty(masterProduct)) {
    			var jcArray: Array = product.variationModel.getMaster().allCategoryAssignments;
    			var jcStr: String;
    			for each(var jcCategory in jcArray) {
    				jcStr = jcCategory.getCategory().displayName;
    				if (jcStr == keyword) {
    					jcTest = true;
    					break;
    				}
    			}
    		}
    		return jcTest;
    	},
    	getDefaultDeliveryTier: function (basket) {
    		var shippingMethodID = '';
    		if(dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
	    		var inMarketShipment = basket.getShipment('In-Market');
	    		if (!empty(inMarketShipment) && inMarketShipment.productLineItems.length > 0) {
	    			shippingMethodID = inMarketShipment.getShippingMethodID();	
	    			//Double Check which delivery tier should be PreSelected on delivery info page landing
    				var productLineItems = inMarketShipment.productLineItems;
					var mattressProductsQty = 0;
    				for each(var productLineItem in productLineItems) {
    					var product = ProductMgr.getProduct(productLineItem.productID);
    					var isMattress = UtilFunctions.jcCategoryTest('Mattresses', product);
    					if (isMattress) {
    						mattressProductsQty = mattressProductsQty  + productLineItem.quantityValue;
    					}
    					if(mattressProductsQty >= 3) {
    						shippingMethodID = 'platinum';
    						break;
    					}
    					var isAdjustableBase = UtilFunctions.jcCategoryTest('Adjustable Beds', product);
    					if(isAdjustableBase) {
    						shippingMethodID = 'platinum';
    						break;
    					}
                    }
	    			if(shippingMethodID == 'flatrate' || shippingMethodID == 'silver') {
	    				shippingMethodID = 'gold';
    				}
	    		}
    		}
    		return shippingMethodID;
    	},
    	deliveryTierSKUsArray : function () {
    		var allShippingMethodsCollection : Collection = null;
    	    var allShippingMethodsMap = null;
    	    
    	    // Get all the shipping methods and place them in a hashmap to make it easier to retrieve a specific one.
    	    allShippingMethodsCollection = dw.order.ShippingMgr.allShippingMethods;
    	    allShippingMethodsMap = new dw.util.HashMap();
    	    for each (var SM : ShippingMethod in allShippingMethodsCollection) {
    	        allShippingMethodsMap.put(SM.ID, SM);
    	    }
    	    
    	    var deliveryTiersSKUs = new Array(3);
    	    //initialize array with empty strings
    	    deliveryTiersSKUs[0] = '';
    	    deliveryTiersSKUs[1] = '';
    	    deliveryTiersSKUs[2] = '';
    	    var shipInfo = "";
    	    for (var entry in allShippingMethodsMap) {
    	    	if(entry == 'silver') {
    	    		shipInfo = allShippingMethodsMap.get(entry);
    	    		deliveryTiersSKUs [0] = shipInfo.custom.deliveryTierSkuId;;
    	    	}
    	    	if(entry == 'gold') {
    	    		shipInfo = allShippingMethodsMap.get(entry);
    	    		deliveryTiersSKUs [1] = shipInfo.custom.deliveryTierSkuId;;
    	    	}
    	    	if(entry == 'platinum') {
    	    		shipInfo = allShippingMethodsMap.get(entry);
    	    		deliveryTiersSKUs [2] = shipInfo.custom.deliveryTierSkuId;;
    	    	}
    	    }
    	    return deliveryTiersSKUs;
    	},
    	validShipmentsCount: function(basket) {
    		var validShipmentsCount = 0;
    		var inMarketShipment = basket.getShipment('In-Market');
    		if (!empty(inMarketShipment) && inMarketShipment.productLineItems.length > 0) {
    			validShipmentsCount++;
    		}
    		var dropShipShipment = basket.getShipment('Drop-Ship');
    		if (!empty(dropShipShipment) && dropShipShipment.productLineItems.length > 0) {
    			if(empty(dropShipShipment.productLineItems[0].custom.isDeliveryTier)) {
    				validShipmentsCount++;
    			}
    		}
    		var parcelShipment = basket.getShipment('Parcel');
    		if(!empty(parcelShipment) && parcelShipment.productLineItems.length > 0) {
    			validShipmentsCount++;
    		}
    		var natiowideShipment = basket.getShipment('Nationwide');
    		if(!empty(natiowideShipment) && natiowideShipment.productLineItems.length > 0) {
    			validShipmentsCount++;
    		}
    		return validShipmentsCount;
    	}

};

exports.UtilFunctions = UtilFunctions;
