/**
 * This Util class implements functions to log and send error message
 */

var UUIDUtils = require('dw/util/UUIDUtils');
var StringUtils = require('dw/util/StringUtils');

var LogUtilFunctions = {
		
		logErrorMessage : function (salesOrderID, category , ErrorLogDetails){
			var transaction = require('dw/system/Transaction');
			var CustomObjectMgr = require('dw/object/CustomObjectMgr');
			transaction.begin();
				var coMakeaPaymentErrorLog = CustomObjectMgr.createCustomObject('MakeaPaymentErrorLog', UUIDUtils.createUUID());
				coMakeaPaymentErrorLog.custom.salesOrderID = salesOrderID;
				coMakeaPaymentErrorLog.custom.category = category;
				coMakeaPaymentErrorLog.custom.ErrorLogDetails = ErrorLogDetails;
			transaction.commit();
		},
		sendEmail : function (emailTemplate, pdictParams, emailRecipients, emailSender, emaiSubject) {
			var content: MimeEncodedText = emailTemplate.render(pdictParams);
			var mail: Mail = new dw.net.Mail();			
			if (!empty(emailSender) && !empty(emailRecipients) && emailRecipients.length > 0) {
				mail.setFrom(emailSender);
				for (var i=0; i<emailRecipients.length; i++) {
	    			mail.addTo(emailRecipients[i]);
	        	}
				mail.setSubject(emaiSubject);
				mail.setContent(content); 
				mail.send();
			}
		},
		logAndEmailError: function (salesOrderID, category , ErrorLogDetails){
			this.logErrorMessage(salesOrderID ,category.ID, ErrorLogDetails)
			
			var emailTemplate: Template = new dw.util.Template("mail/wmap_failure_log");
			var pdictParams : Map = new dw.util.HashMap();
			pdictParams.put("orderNumber", salesOrderID);
			pdictParams.put("category", category.description);
			pdictParams.put("ErrorLogDetails", ErrorLogDetails);
			
			if ('wmapLogEmailRecipients' in dw.system.Site.current.preferences.custom && 'customerServiceEmail' in dw.system.Site.current.preferences.custom){
				var emailSender = dw.system.Site.getCurrent().getCustomPreferenceValue('customerServiceEmail');
				var emailRecipients = dw.system.Site.getCurrent().getCustomPreferenceValue('wmapLogEmailRecipients');
				if ('wmapFailureStoreEmailSubject' in dw.system.Site.current.preferences.custom){
					var emaiSubject = StringUtils.format(dw.system.Site.getCurrent().getCustomPreferenceValue('wmapFailureStoreEmailSubject'),salesOrderID);
				} else {
					var emaiSubject = 'WMAP - Order Payment Failed - ' + salesOrderID;
				}
				
				this.sendEmail(emailTemplate, pdictParams, emailRecipients, emailSender, emaiSubject);
				
			}
			
		}
};

exports.LogUtilFunctions = LogUtilFunctions;
