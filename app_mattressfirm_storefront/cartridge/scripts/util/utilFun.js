var Site = require('dw/system/Site');
/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');

var UtilFunctions = {
		
    isChicagoProduct : function (manufacturerSKU) {
        var Product = app.getModel('Product');
        var product = Product.get('mfi'+manufacturerSKU);
        var masterSku = '';
        if (!empty(product.object) && product.object.variationModel) {
            var masterProduct = product.object.variationModel.master;
            masterSku = masterProduct ? masterProduct.manufacturerSKU : '';
        }
        var enableChicagoDCDelivery = "enableChicagoDCDelivery" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("enableChicagoDCDelivery") : false;
        var chicagoInventLocationId = '';
        var parcelableProductIDs = '';
        if (enableChicagoDCDelivery) {
            parcelableProductIDs = "parcelableProductIDs" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("parcelableProductIDs") : '';
            if( (!empty(manufacturerSKU) && parcelableProductIDs.indexOf(manufacturerSKU) > -1) || (!empty(masterSku) && parcelableProductIDs.indexOf(masterSku) > -1)) {
                return true;
            }
        }
        return false;
    },
    chicagoInventLocationId : function () {
        var enableChicagoDCDelivery = "enableChicagoDCDelivery" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("enableChicagoDCDelivery") : false;
        var chicagoInventLocationId = '';
        if (enableChicagoDCDelivery) {
            chicagoInventLocationId = Site.current.getCustomPreferenceValue('chicagoInventLocationId');
        }
        return chicagoInventLocationId;
    },
	validShipmentsCount: function(basket) {
    		var validShipmentsCount = 0;
    		var inMarketShipment = basket.getShipment('In-Market');
    		if (!empty(inMarketShipment) && inMarketShipment.productLineItems.length > 0) {
    			validShipmentsCount++;
    		}
    		var dropShipShipment = basket.getShipment('Drop-Ship');
    		if (!empty(dropShipShipment) && !empty(dropShipShipment.productLineItems) && dropShipShipment.productLineItems.length > 0) {
    			if(empty(dropShipShipment.productLineItems[0].custom.isDeliveryTier)) {
    				validShipmentsCount++;
    			}
    		}
    		var parcelShipment = basket.getShipment('Parcel');
    		if(!empty(parcelShipment) && parcelShipment.productLineItems.length > 0) {
    			validShipmentsCount++;
    		}
    		var natiowideShipment = basket.getShipment('Nationwide');
    		if(!empty(natiowideShipment) && natiowideShipment.productLineItems.length > 0) {
    			validShipmentsCount++;
    		}
    		return validShipmentsCount;
    	}

};

exports.UtilFunctions = UtilFunctions;
