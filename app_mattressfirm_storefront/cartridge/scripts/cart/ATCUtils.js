'use strict';

var ProductMgr = require('dw/catalog/ProductMgr');
var ProductUtils = require('app_storefront_core/cartridge/scripts/product/ProductUtils.js');


var ATCUtils = {
/**
 * Get the ATC configuration from Site preference
 */
getATCConfiguration : function() {
	if('atcConfigs' in dw.system.Site.current.preferences.custom){
		var atcConfigs = dw.system.Site.current.preferences.custom.atcConfigs;
		var atcConfigsValue = JSON.parse(atcConfigs);
		return atcConfigsValue;			
	}
	return null;
},
/**
 * Method for Checking if Product has box spring as an option product
 * and also checks if option product has been added at ATC Panel
 * @returns {result} 
 */
hasOptionProduct : function(lineItem) {
	var result = {
		hasOptionProduct: false,
		hasOptionProductAdded: false
	}
	if(lineItem.optionProductLineItems.size() > 0){
		for each(var opl in lineItem.optionProductLineItems){
				if(opl.optionID == 'boxSpring' && opl.productID != 'none'){
					result.hasOptionProduct = true;
					result.hasOptionProductAdded = true;
					break;
				} else if(opl.optionID == 'boxSpring' && opl.productID == 'none') {
					result.hasOptionProduct = true;
					result.hasOptionProductAdded = false;
				}
		}
	}
	return result;
},

/**
 * Method for checking the ATC Panel state
 * It iterates through all the added items in cart and update the ATC status
 * @returns {ATCPanelState} 
 */
setATCPanelState : function(productLineItems) {
	
	var atcPanelState = {
			isMattressProductAdded: false,
			isFoundationProductAdded: false,
			isAcessoryProductAdded: false
	}
	
	var atcConfigs = ATCUtils.getATCConfiguration();

		
	for each(var lineItem in productLineItems) {
		
		if(atcPanelState.isMattressProductAdded && atcPanelState.isFoundationProductAdded && atcPanelState.isAcessoryProductAdded){
			break;
		}
		
		var isMattressProductAdded = ProductUtils.checkProductFamily(lineItem.product, atcConfigs.ATCCategories['MATTRESS']);
		
		if(isMattressProductAdded){
			atcPanelState.isMattressProductAdded = true;
			
			if(!atcPanelState.isFoundationProductAdded){
				var optionProductStatus = ATCUtils.hasOptionProduct(lineItem);
				if(optionProductStatus.hasOptionProduct && optionProductStatus.hasOptionProductAdded){
					atcPanelState.isFoundationProductAdded = true;
				}
			}
		}
		
		if(!atcPanelState.isFoundationProductAdded){	
			atcPanelState.isFoundationProductAdded = ProductUtils.checkProductFamily(lineItem.product, atcConfigs.ATCCategories['FOUNDATION']);
		}
		
		if(!atcPanelState.isAcessoryProductAdded){
			atcPanelState.isAcessoryProductAdded = ProductUtils.checkProductFamily(lineItem.product, atcConfigs.ATCCategories['ACCESSORY']);
		}
		
	}
	return atcPanelState;
},

/**
 * Method for calculating cart count and its message
 * @returns {cart count and message} 
 */
getCartValueAndMessage : function(cart) {

	//Calculate total items in cart
	var cartQty : Number = 0;
 	var pliIt : dw.util.Iterator = cart.object.productLineItems.iterator();
	
	while (pliIt.hasNext()) {
		var pli : dw.order.ProductLineItem = pliIt.next();
		cartQty += pli.quantity;
	}
	// add the gift certificates to cart quantity
	cartQty += cart.object.giftCertificateLineItems.size();	
	var minicartmessage : String = "shopping cart link with " + cartQty.toString() + " item";
	
	return {
		cartQty: cartQty,
		minicartmessage: minicartmessage
	}
}
}

exports.ATCUtils = ATCUtils;
