'use strict';

var ProductMgr = require('dw/catalog/ProductMgr');
var ProductUtils = require('app_storefront_core/cartridge/scripts/product/ProductUtils.js');


var DeliveryCalendarUtils = {
/**
 * @function
 * @description Calculate the days in month
 * @Params Expect Month and Year
 * @return Number: number of days in month 
 */
daysInMonth : function(iMonth, iYear) {
    return 32 - new Date(iYear, iMonth, 32).getDate();
},
/**
 * @function
 * @description give month full name.
 * @Params Expect month index from date object.
 * @return string: containg full month name 
 */
getMonthFullName : function(month) {
	var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
	return months[month];
},
/**
 * @function
 * @description give last date from today to given days.
 * @Params Expect number of Days ahead from todays date.
 * @return Date: Last date by calculate from today to given dates. 
 */
getWeeksLastDay : function(numberOfDaysAhead) {
	var date = new Date();
	date.setDate(date.getDate() + numberOfDaysAhead);
    var daysInMonth = this.daysInMonth(date.getMonth(), date.getFullYear());
    date.setDate(daysInMonth);
    return date;
},
isMonthLastDate : function(year, month, date) {
	var isLastDate = false;
	if (!empty(year) && !empty(month) && !empty(date)) {
		var currentDate = new Date(year, month, date);
		var monthLastDate = new Date(year, month, this.daysInMonth(month, year));
	    if (date === monthLastDate.getDate() && year === monthLastDate.getFullYear() && month === monthLastDate.getMonth()) {
	    	isLastDate = true;
	    }
	}
    return isLastDate;
},
/**
 * @function
 * @description Evaluate the classes for the delivery dates.
 * @Params Expect calander day @Number(date, year, month), deliverdates @Object conataing delivery data.
 * datekey current date key @string, isAnySlotActive @boolean flag to detect active slot.
 * selectedSlotDateKey @string to open calender with selected date in mobile.
 * @return Date: Last date by calculate from today to given dates.
 */
getDateSlotClasses : function(date, year, month, deliverydates, dateKey, isAnySlotActive, selectedSlotDateKey) {
	var dateClass = '';
	// we will not show current date unavailable even it is avaialable.
	var isCurrentDate = this.isCurrentDate(date, year , month);
    if (isCurrentDate) {
         dateClass = 'current-date ';
    }
    if (!empty(deliverydates[dateKey]) && deliverydates[dateKey]['Available'] === 'YES' && !isCurrentDate) {
    	if (!empty(selectedSlotDateKey) && !empty(deliverydates[selectedSlotDateKey]['timeslots']) && deliverydates[selectedSlotDateKey]['timeslots'].length > 0) {
    		var selectedDate = new Date(deliverydates[selectedSlotDateKey]['timeslots'][0]['SlotDate']);
    		var isSelectedDate = (date === selectedDate.getDate() && year === selectedDate.getFullYear() && month === selectedDate.getMonth());
    		dateClass += isSelectedDate ? 'active-slot slot-available' : 'slot-available ';
    	} else {
        	dateClass += !isAnySlotActive ? 'active-slot slot-available' : 'slot-available ';
    	}
    } else if (!empty(deliverydates[dateKey]) && (deliverydates[dateKey]['Available'] === 'NO' || isCurrentDate)) {
  		dateClass += 'no-slot-available ';
  	} else {
  		dateClass += 'no-slot-available ';
  	}
    // to add last date in month class.
    if (this.isMonthLastDate(year, month, date)) {
    	dateClass += 'month-last-date';
    } else if(date === 1) {
    	dateClass += 'month-first-date';
    }
  	return dateClass;
},
/**
 * @function
 * @description Get first active slot for the mobile.
 * @Params deliverydates @Object containg delivery dates data.
 * @return @string active slot key
 */
getFirstActiveSlotKey : function(deliverydates) {
	var slotKey = "";
    for each(var deliveryDate in deliverydates) {
    	if (!empty(deliveryDate) && deliveryDate['Available'] === 'YES') {
    		var calenderDate = !empty(deliveryDate['timeslots']) && deliveryDate['timeslots'].length > 0 && !empty(deliveryDate['timeslots'][0]['SlotDate']) ? new Date(deliveryDate['timeslots'][0]['SlotDate']) : '';
    		if (!empty(calenderDate) && !this.isCurrentDate(calenderDate.getDate(), calenderDate.getFullYear(), calenderDate.getMonth())) {
        		slotKey = calenderDate.toDateString().replace(' ', '', 'g');;
        		break;
    		}
    	}
    }
	return slotKey;
},
/**
 * @function
 * @description Get Available Slots for the mobile with key
 * @Params deliverydates @Object containg delivery dates data.
 * datekey @string datekey for slot.
 * @return @ArrayList  containing available slots.
 */
getAvailableSlotsByKey : function(deliverydates,dateKey) {
	var availableSlots = new dw.util.ArrayList();
	selectedATPDate = deliverydates[dateKey];
	if(!empty(selectedATPDate)) {
		for (var i = 0; i < selectedATPDate.timeslots.length; i++ ) {
			if(selectedATPDate.timeslots[i].available.toUpperCase() == "YES") {
				availableSlots.add(selectedATPDate.timeslots[i]);
			}	
		}
	}
	return availableSlots;
},
/**
 * @function
 * @description To check calander date is today's date or not.
 * @Params Expect calander day @Number(date, year, month)
 * @return @Boolean today date or not
 */
isCurrentDate : function(date, year, month) {
	var isCurrentDate = false;
	var today = new Date();
    if (date === today.getDate() && year === today.getFullYear() && month === today.getMonth()) {
    	isCurrentDate = true;
    }
    return isCurrentDate;
}
}

exports.DeliveryCalendarUtils = DeliveryCalendarUtils;
