	var util = require("./util");
	function getDefaultAvailabilityMessageAjax(pid) {
		var chicagoHiddenMsg= $('.chicago-delivery-hidden-msg');
		var url = util.appendParamsToUrl(Urls.getDefaultAvailabilityMessage, {productId: pid});
		chicagoHiddenMsg.load(url, function () {
			if($('.availability-web.deliverytracking-page').length > 0) {
				$('.availability-web.deliverytracking-page').html(chicagoHiddenMsg.html());
			}
			if($(".replaceMe_shippingDate_parcel").length > 0 && $(".replaceMe_shippingDate_parcel").hasClass("have-delivery-msg") == false) {
				var label = $(".replaceMe_shippingDate_parcel").html();
				var dateMsg = chicagoHiddenMsg.find('.availability-msg p').html();
				var chicagoMsg = label + dateMsg;
				$(".replaceMe_shippingDate_parcel").html(chicagoMsg);
				$(".replaceMe_shippingDate_parcel").addClass("have-delivery-msg");
			}
		});
	}

	function getMaxAvailabilityMessage(items, deliveryType, replacementTypes) {
		var ids = [];
	
		for(var i=0; i<items.length; i++) {
			if(items[i].productID !== "") {
				ids.push(items[i].productID);
			}
		}
	
		//shippingType corresponds to values of product's shipping category
		var ajax = "productIds=" + ids + "&shippingType=" + deliveryType;
		$.getJSON(Urls.getMaxAvailabilityMessage, ajax, function(data, status) {
			if (status == "success" && !data.error) {
				if(replacementTypes === "focusedItems") {
					if($('#jcCalendar').attr('data-hide-calendar') === "true" ) {
						$('#jcCalendar').hide();
					}
					$('#maxAvailabilityMessage').html(data.availabilityMessage);
	
					for(var i=0; i<ids.length; i++) {
						$('.thisDelivery #lineItem' + ids[i] + 'ShippingDate').html("Shipping Date: " + data.availabilityMessage);
					}
				}
					
				if(replacementTypes === "otherItems") {
	
					for(var i=0; i<ids.length; i++) {
						$('.otherDelivery #lineItem' + ids[i] + 'ShippingDate').html("Shipping Date: " + data.availabilityMessage);
					}
				}
			} else if (status == "success" && data.error) {
				$('#maxAvailabilityMessage').html(data.error);
			} else {
				$('#maxAvailabilityMessage').html("Availability message not found.");
			}
		});
	}

	function showDeliveryDateAndCalendarStatus(curItem) {
		if(typeof curItem.deliveryMode != "undefined" && curItem.deliveryMode == "Parcel" && (curItem.trackingNumber == "" || typeof curItem.trackingNumber == "undefined")) {
			return false;
		} else {
			return true;		
		}
	}

	//Call these 2 chicago functions together as used in code
	function isChicago(order) {
		if(order.ax_line_items.length > 0) {
			for(var i = 0; i < order.ax_line_items.length; i++) {
			 if(order.ax_line_items[i].deliveryMode == 'Parcel') {
				  return true;
			  }
			}
		}
		return false;
	}

	function chicagoOrderHasTrackingNumber(order) {
		if(order.ax_line_items.length > 0) {
	    	for(var i = 0; i < order.ax_line_items.length; i++) {
	  		  if(order.ax_line_items[i].deliveryMode == 'Parcel' && (order.ax_line_items[i].trackingNumber == "" || typeof order.ax_line_items[i].trackingNumber == "undefined")) {
	  			  return false;
	  		  }
	  		}
		}
		return true;
	}

var deliverytracking = {
    init: function() {
    	$(document).on('click', '.jcBack a.simplereload , jcRefresh', function(e){
    		location.reload();
    	});

    	$(document).on('click', '.jcBack a.goback', function(e){
    		refreshPage(1);
    	});

    	$(document).on('click', '.jcTrackThisDelivery', function(e) {
    		if(typeof $(this).attr('data-date') != "undefined") {
    			showOderDate($(this).attr('data-date'));
    		} else {
    			showOderDate();	
    		}
    	});
    	
    	$(document).on('click', '#trackingCopyDiv', function(e) {
			 copyTrackingText($(this));
    	});
	
    	$("#trackingSearchOption").change(function() {
    	    if ($("#trackingSearchOption").val() == "Phone-Number") {
    	        $("#trackingNumber").attr("placeholder", "Enter Phone Number")
    	    } else {
    	        $("#trackingNumber").attr("placeholder", "Enter Order Number")
    	    }
    	});

    	$(document).on('click', '#dtchat', function(e) {
    		dtChatClick();
    	});
    	
    	$(document).on('click', '.chooseOrder', function(e) {
    		var orderArrayNumber = $(this).attr('data-orderArrayNumber');
    		chooseOrder(orderArrayNumber);
    	});
    	
    	function dtChatClick() {
    	    document.getElementById("helpButtonSpan").click();
    	}
    	
    	function viewAnswer(num) {
    	    $("#jcAnswer" + num).toggle();
    	}

    	$('input#lastName, input#zipCode, input#phoneNumber').on('keyup', function() {
    	    if (this.value.length > 0) {
    	        // do search for this.value here
    	        var curInput = $(this).attr('id');
    	        $('input#lastName, input#zipCode, input#phoneNumber').removeClass("jcBoldBorder");
    	        $('input#' + curInput).addClass("jcBoldBorder");
    	        if (curInput == "lastName") {
    	            $('input#zipCode, input#phoneNumber').val("");
    	        } else if (curInput == "zipCode") {
    	            $('input#lastName, input#phoneNumber').val("");
    	        } else if (curInput == "phoneNumber") {
    	            $('input#lastName, input#zipCode').val("");
    	        }
    	    }
    	});

    	function initICS(scheduledDate, scheduledTime, location) {
    	    //alert(scheduledDate);

    	    var scheduledTimeSplit = scheduledTime.split("-");
    	    var startTime = scheduledTimeSplit[0].trim();
    	    var endTimeSplit = scheduledTimeSplit[1].trim().split(" ");
    	    var endTime = endTimeSplit[0] + " " + endTimeSplit[1];
    	    var timeZone = endTimeSplit[2];
    	    //alert(startTime + "-" + endTime + "-" + timeZone);

    	    var icsDate = new Date(scheduledDate);
    	    var icsStartDate = (icsDate.getMonth() + 1) + '/' + icsDate.getDate() + '/' + icsDate.getFullYear() + ' ' + startTime;
    	    console.log(icsStartDate);
    	    var icsEndDate = (icsDate.getMonth() + 1) + '/' + icsDate.getDate() + '/' + icsDate.getFullYear() + ' ' + endTime;

    	    cal_single = ics();
    	    cal_single.addEvent('Mattress Firm Delivery Date', 'Scheduled Time: ' + scheduledDate + ' ' + scheduledTime + '. You will receive a call the day before your scheduled delivery with a 2 hour delivery window & our drivers will call you prior to arrival.', location, icsStartDate, icsEndDate);

    	    var calendarIcon = Urls.libraryPath + '/images/Landing/dispatch-tracker-page/delivery-tracking-add-to-calendar-icon.png';
    	    $("#jcCalendarWrapper").append('<a class="jcAddToCalendar" href="javascript:;" onclick="javascript:cal_single.download(' + "'Mattress Firm Delivery'" + ')"><img src="' + calendarIcon + '" alt="calendar icon" > Add to calendar</a>');
    	    return;
    	}


    	function initCalendar(scheduledDate, scheduledTime, location) {
    	    //alert(scheduledDate);
    	    var events = [{
    	        'Date': new Date(scheduledDate),
    	        'Title': 'Delivery.',
    	        'Link': '#jcOrderContent'
    	    }, ];
    	    var element = caleandar(document.getElementById('jcCalendar'), events);
    	    if (scheduledTime) {
    	        initICS(scheduledDate, scheduledTime, location);
    	    }
    	    return;
    	}

    	function normalizePhone(phone) {
    	    //normalize string and remove all unnecessary characters
    	    phone = phone.replace(/[^\d]/g, "");

    	    //check if number length equals to 10
    	    if (phone.length == 10) {
    	        //reformat and return phone number
    	        return phone.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
    	    }

    	    return null;
    	}

    	function copyTrackingText($this) {
    	    var copyText = $this.next();
    	    var textArea = document.createElement("textarea");
    	    textArea.value = copyText.text();
    	    document.body.appendChild(textArea);
    	    textArea.select();
    	    document.execCommand("Copy");
    	    textArea.remove();
    	    $this.text('COPIED')
    	}
    	//var phone = '(123)4567890';
    	//phone = normalize(phone); //(123) 456-7890


    	// Init google map with Long/Lat
    	function initMap(orderLat, orderLong) {
    	    var map = new google.maps.Map(document.getElementById('map'), {
    	        zoom: 16,
    	        center: {
    	            lat: orderLat,
    	            lng: orderLong
    	        }
    	    });


    	    var image = Urls.libraryPath + "/images/Landing/dispatch-tracker-page/delivery-tracking-map-marker.png";
    	    var beachMarker = new google.maps.Marker({
    	        position: {
    	            lat: orderLat,
    	            lng: orderLong
    	        },
    	        map: map,
    	        icon: image
    	    });
    	    return;
    	}


    	// Init google map with address
    	function initMapAddress(address, address2) {
    	    var geocoder;
    	    var map;
    	    geocoder = new google.maps.Geocoder();
    	    geocoder.geocode({
    	        'address': address
    	    }, function(results, status) {
    	        if (status == 'OK') {

    	            var map = new google.maps.Map(document.getElementById('map'), {
    	                zoom: 16,
    	                center: results[0].geometry.location
    	            });

    	            var image = Urls.libraryPath + "/images/Landing/dispatch-tracker-page/delivery-tracking-map-marker.png";
    	            var beachMarker = new google.maps.Marker({
    	                position: results[0].geometry.location,
    	                map: map,
    	                icon: image
    	            });

    	        } else {
    	            $("#map").hide();
    	            console.log('Geocode was not successful for the following reason: ' + status + ' - For address: ' + address);
    	            //if simpler address exists try that
    	            if (address2) {
    	                initMapAddress2(address2);
    	            }
    	        }
    	    });
    	    return;
    	}

    	// Init google map with simple address (address + zip)
    	function initMapAddress2(address) {
    	    var geocoder;
    	    var map;
    	    geocoder = new google.maps.Geocoder();
    	    geocoder.geocode({
    	        'address': address
    	    }, function(results, status) {
    	        if (status == 'OK') {

    	            var map = new google.maps.Map(document.getElementById('map'), {
    	                zoom: 16,
    	                center: results[0].geometry.location
    	            });

    	            var image = Urls.libraryPath + "/images/Landing/dispatch-tracker-page/delivery-tracking-map-marker.png";
    	            var beachMarker = new google.maps.Marker({
    	                position: results[0].geometry.location,
    	                map: map,
    	                icon: image
    	            });
    	            $("#map").show();

    	        } else {
    	            $("#map").hide();
    	            console.log('Second Geocode was not successful for the following reason: ' + status + ' - For address: ' + address);
    	        }
    	    });
    	    return;
    	}


    	function refreshPage() {
    	    location.reload();
    	}

    	var orderObj = {};
    	var orderIdArray = [];
    	var orderCount = 0;
		var curOrder = 0;
		var showPrepareForDelivery = false;

		function checkForInHomeSetup(isParcelable, shippingInfo) {
			if(isParcelable == "false" && shippingInfo.toLowerCase() == "core") {
				showPrepareForDelivery = true;
			}
		}

    	function addProductInfo(productIdArray, printMode) {
    	    for (var i = 0; i < productIdArray.length; i++) {
    	        //on/demandware.store/Sites-Mattress-Firm-Site/default/DispatchTracking-GetProductInfo?pid=mfiV000095216
    	        if (printMode == 1) {
    	            var productID = productIdArray[i];
    	        } else {
    	            var productID = productIdArray[i].productID;
    	        }
    	        var ajax = "pid=mfi" + productID;
    	        $.getJSON("/on/demandware.store/Sites-Mattress-Firm-Site/default/DispatchTracking-GetProductInfo?", ajax, function(data, status) {
    	            //alert(data.imageID);
    	            if (status == "success" && data != "" && data.error != "No product found") {
    	                if (printMode == 1) {
    	                    var image_url = 'https://i1.adis.ws/i/hmk/' + data.imageID + '.jpg?h=100&w=120&sm=CM';
    	                    $.get(image_url)
    	                        .done(function() {
    	                            $("#" + data.ID + ".jcLineItemImage").html('<img src="' + image_url + '" alt="photo of product" title="' + data.name + '" />');
    	                        }).fail(function() {
    	                            $("#" + data.ID + ".jcLineItemImage").html('<div class="jcNoImage" style="">' + data.name + '</div>');
    	                        })

								if(data.isParcelable != null && data.shippingInfo != null) {
									checkForInHomeSetup(data.isParcelable, data.shippingInfo);
								}
    	                    return;
    	                } else {

    	                    var image_url = 'https://i1.adis.ws/i/hmk/' + data.imageID + '.jpg?h=145&w=160&sm=CM';
    	                    $.get(image_url)
    	                        .done(function() {
    	                            $("#" + data.ID + " .jcLineItemImage").html('<img src="' + image_url + '" alt="photo of product" />');
    	                        }).fail(function() {
    	                            $("#" + data.ID + " .jcLineItemImage").html('<div class="jcNoImage" style="">No<br>Image<br>Available</div>');
    	                        })
    	                    if (data.name && data.name != null && data.name != "null") {
    	                        $("#" + data.ID + " .jcLineItemName").html(data.name);
    	                    }
    	                    if (data.size && data.size != null && data.size != "null") {
    	                        $("#" + data.ID + " .jcLineItemSize").html('Size: ' + data.size);
    	                    }
							if(data.isParcelable != null && data.shippingInfo != null) {
								checkForInHomeSetup(data.isParcelable, data.shippingInfo);
							}
    	                }
    	            } else {

    	            }
    	        }).then(function() {
					if(showPrepareForDelivery) {
						$('#jcBox5').show();
					} else {
						$('#jcBox5').hide();
					}
				});
    	    }
    	}

    	function chooseOrder(orderArrayNumber) {
    	    curOrder = orderArrayNumber;
    	    orderCount = 1;
    	    printOrders();
    	}

    	function refreshPage(num) {
    	    if (orderIdArray.length > 1 && num != 1) {
    	        orderCount = orderIdArray.length;
    	        printOrders();
    	    } else {
    	        history.pushState(null, null, '?refresh=true');
    	        window.location.reload(true);
    	    }
    	}

    	function printOrders() {

    	    $("#jcBox1 .jcBoxInner #jcOrderContent").html("");
    	    $("#jcBox1 .jcBoxInner #jcOrderItems").html("");
    	    var deliveryDate = $("#deliveryDate").val();
    	    //console.log("jcTest = " + deliveryDate);

    	    // function to print line items - printItems(order.ax_line_items,order.earliest_scheduled_shipping_date_time,true,order.earliest_scheduled_carrier,order.earliest_scheduled_tracking_number);
    	    function printItems(itemArray, earliestShippingDateTime, sameOrder, earliestScheduledCarrier, earliestScheduledTrackingNumber) {
    	        if (itemArray.length > 0) {
    	            var collect = "";
    	            for (var j = 0; j < itemArray.length; j++) {
    	                var goodToAdd = false;
    	                var sameOrderTest = false;
    	                if (typeof sameOrder !== 'undefined') {
    	                    //if "sameOrder" isn't null check if line item has the same date and drop ship info (if not null) as the function params
    	                    if (earliestScheduledCarrier) {
                                if (earliestShippingDateTime == itemArray[j].shippingDateConfirmedTime && earliestScheduledCarrier == itemArray[j].carrierName && typeof itemArray[j].trackingNumber != "undefined") {
    	                            sameOrderTest = true
    	                        }
    	                    } else {
    	                        if (earliestShippingDateTime == itemArray[j].shippingDateConfirmedTime) {
    	                            sameOrderTest = true
    	                        } else if(earliestShippingDateTime === "") {
									sameOrderTest = true;
								}
    	                    }
    	                    if ((sameOrder == true && sameOrderTest == true) || (sameOrder == false && sameOrderTest == false)) {
    	                        goodToAdd = true;
    	                    }
    	                } else {
    	                    goodToAdd = true;
    	                }

    	                if (goodToAdd == true) {

							if(sameOrder) {
								collect += '<div class="jcLineItem thisDelivery" id="mfi' + itemArray[j].productID + '">';
							} else {
								collect += '<div class="jcLineItem otherDelivery" id="mfi' + itemArray[j].productID + '">';
							}
							
							collect += '<div class="jcLineItemImage"><div class="jcNoImage" style="">No<br>Image<br>Available</div></div>';
							collect += '<p><span class="jcLineItemName">' + itemArray[j].description + '</span><span class="jcLineItemSize"></span><span class="jcLineItemQuantity">Quantity: ' + itemArray[j].quantity + '</span>';
    	                    
    	                    //Writing date at product line items in order
    	                 	if(itemArray[j].shippingDateConfirmed) {
                        		if(itemArray[j].deliveryMode == "Parcel") {
                        			if(typeof itemArray[j].trackingNumber != "undefined") {
                        				collect += '<span class="jcLineItemConfirmedDate">Shipping Date: ' + itemArray[j].shippingDateConfirmed + '</span>';
                        			} else {
                        				collect += '<span id="lineItem'+itemArray[j].productID+'ShippingDate" class="jcPdpMessage"></span>';
                        			}
								} else if(itemArray[j].deliveryMode == "Direct Del" && (!itemArray[j].carrierName || !itemArray[j].trackingNumber)) {
									collect += '<span id="lineItem'+itemArray[j].productID+'ShippingDate" class="jcPdpMessage"></span>';
									if(CAProdList.includes(itemArray[j].productID) && orderObj[orderIdArray[0]].state.toUpperCase() == "CA") {
		                				collect += '<span class="ca-delivery-delay red">As a result of nationwide Covid-19 impacts, delivery times may be delayed.</span>';
		                			}
								} else {
                        			collect += '<span class="jcLineItemConfirmedDate">Shipping Date: ' + itemArray[j].shippingDateConfirmed + '</span>';
                        		}
                        	} else {
								if(itemArray[j].deliveryMode == "Direct Del" && (!itemArray[j].carrierName || !itemArray[j].trackingNumber)) {
									collect += '<span id="lineItem'+itemArray[j].productID+'ShippingDate" class="jcPdpMessage"></span>';
									if(CAProdList.includes(itemArray[j].productID) && orderObj[orderIdArray[0]].state.toUpperCase() == "CA") {
		                				collect += '<span class="ca-delivery-delay red">As a result of nationwide Covid-19 impacts, delivery times may be delayed.</span>';
		                			}
								}
							}
    	                    
    	                    if (itemArray[j].carrierName) {
    	                        collect += '<span>' + itemArray[j].carrierName + '';
    	                        if (itemArray[j].trackingNumber) {
    	                            if (itemArray[j].trackingURL && itemArray[j].trackingURL != "") {
    	                                collect += ' - Tracking Number: <a href="' + itemArray[j].trackingURL + '" target="_blank">' + itemArray[j].trackingNumber + '</a></span>';
    	                            } else {
    	                                collect += ' - Tracking Number: ' + itemArray[j].trackingNumber + '</span>';
    	                            }
    	                        } else {
    	                            collect += ' - Tracking Number: coming soon</span>';
    	                        }
    	                    }
    	                    if (itemArray[j].type == "AX") {
								if (itemArray[j].shippingDateConfirmedTime) {
									if((itemArray[j].deliveryMode == "Direct Del" || itemArray[j].deliveryMode == "Parcel") && itemArray[j].trackingNumber) {
    	                            	collect += '<a class="jcTrackThisDelivery" href="#jcWrapper" data-date=' + itemArray[j].shippingDateConfirmedTime + '>Track this delivery ›</a>';
									} else if(itemArray[j].deliveryMode == "Red Carpet") {
										collect += '<a class="jcTrackThisDelivery" href="#jcWrapper" data-date=' + itemArray[j].shippingDateConfirmedTime + '>Track this delivery ›</a>';
									}
								}
    	                    } else {
    	                        collect += '<a class="jcTrackThisDelivery" href="#jcWrapper">Track this delivery ›</a>';
							}
    	                    collect += '</p>';
    	                    collect += '</div>';
    	                }
            		}
    	            return collect;
    	        } else {
    	            return "";
    	        }
    	    }

    	    if (orderCount == 1) {
    	        var order = orderObj[orderIdArray[curOrder]];
    	        var phoneNumberMessage = "For items being delivered via our In Home Delivery Service, you will receive a call to the above number between 6-8pm the evening before your scheduled delivery with a smaller, 2-3 hour delivery window.  Once this assigned window is communicated via phone it cannot be adjusted so be sure someone 18 or older is there to accept delivery.";
    	        $("#jcBox1 .jcBoxInner #jcOrderContent").html("<p>Order Number: " + order.order_number + "<br>Status: " + order.status + "<br>Scheduled Date: " + order.scheduled_date + "<br>Scheduled Time: " + order.scheduled_time_window + "</p>");

    	        if (order.status == "Scheduled" && deliveryDate == '') {

    	            collect = '<p class="jcBack"><a class="goback" href="javascript:;">< Back to search</a> <a class="simplereload" style="float:right;" href="javascript:;">Refresh ↻</a></p>';
    	            collect += '<h2>PREPARING ORDER</h2>';
    	            collect += '<p class="jcOrderID">Order Number: ' + order.order_number + '</p>';
    	            var statusImageFile = Urls.libraryPath + "/images/Landing/dispatch-tracker-page/delivery-tracking-preparing-order.png";
    	            var statusImageFileMobile = Urls.libraryPath + "/images/Landing/dispatch-tracker-page/delivery-tracking-preparing-order-mobile.png";
    	            collect += '<img class="jcStatusImage jcDesktop" src="' + statusImageFile + '" alt="preparing order icon" /><img class="jcStatusImage jcMobile" src="' + statusImageFileMobile + '" alt="preparing order icon" />';
    	            collect += '<div class="jcOrderPhone"><h3>Your phone number: <br><span class="jcRed jcPhone">' + order.customer_phone + '</span></h3><br><p>' + phoneNumberMessage + '</p></div>';
    	            if (order.scheduled_date != "TBA") {
    	                collect += '<div class="jcFloat" id="jcCalendarWrapper"><h3>Delivery date:</h3><h3 class="jcRed">' + order.scheduled_date + '<br>Between ' + order.scheduled_time_window + '</h3><div id="jcCalendar" style=""></div></div>';
    	            } else {
    	                collect += '<div class="jcFloat"><h3>Delivery date:</h3><h3 class="jcRed">TBA</h3></div>';
    	            }
    	            if (order.address1 != "N/A") {
    	                collect += '<div class="jcFloat"><h3>Delivery address:</h3><h3 class="jcRed">' + order.address1 + '<br>' + order.city + ', ' + order.state + ' ' + order.zip + '</h3><div id="map"></div></div>';
    	            } else {
    	                collect += '<div class="jcFloat"><h3>Delivery address:</h3><h3 class="jcRed">N/A<br><br></h3><div id="map"></div></div>';
    	            }
    	            var collect2 = '';
    	            if (order.line_items.length > 0) {
	    	            collect2 = '<h2 class="jcLine">ITEMS IN THIS DELIVERY</h2>';
	
	    	            for (var j = 0; j < order.line_items.length; j++) {
	    	                collect2 += printItems(order.line_items);
	    	            }
    	            }

    	            if (order.ax_line_items.length > 0) {
    	                var collect3 = '<h2 class="jcLine">OTHER ITEMS IN THIS ORDER</h2><p>These items are in the same order but are being delivered separately.</p>';
    	                collect3 += printItems(order.ax_line_items);
    	                $("#jcBox5b .jcBoxInner").html(collect3);
    	                addProductInfo(order.ax_line_items);
    	                $("#jcBox5b").show();
    	            } else {
    	                $("#jcBox5b").hide();
    	            }

    	            $("#jcBox1 .jcBoxInner #jcOrderContent").html(collect);
    	            $("#jcBox1 .jcBoxInner #jcOrderItems").html(collect2);

    	            if (order.scheduled_date != "TBA") {
    	                if (order.scheduled_time_window != "TBA") {
    	                    initCalendar(order.scheduled_date, order.scheduled_time_window, order.address1 + ', ' + order.city + ' ' + order.state + ' ' + order.zip);
    	                } else {
    	                    initCalendar(order.scheduled_date, "", order.address1 + ', ' + order.city + ' ' + order.state + ' ' + order.zip);
    	                }
    	            }
    	            if (order.latitude != "N/A" && order.longitude != "N/A") {
    	                initMap(order.latitude, order.longitude);
    	            } else {
    	                $("#map").hide();
    	            }
    	            addProductInfo(order.line_items);
    	            $("#jcBox2, #jcBox3, #jcBox4").hide();
					$("#jcBox6, #jcBox1 #jcOrderError1").show();

    	        } else if (order.status == "On the way" && deliveryDate == '') {

    	            collect = '<p class="jcBack"><a class="goback" href="javascript:;">< Back to search</a> <a class"simplereload" style="float:right;" href="javascript:;">Refresh ↻</a></p>';
    	            collect += '<h2>IN TRANSIT</h2>';
    	            collect += '<p class="jcOrderID">Order Number: ' + order.order_number + '</p>';
    	            var statusImageFile = Urls.libraryPath + "/images/Landing/dispatch-tracker-page/delivery-tracking-in-transit.png";
    	            var statusImageFileMobile = Urls.libraryPath + "/images/Landing/dispatch-tracker-page/delivery-tracking-in-transit-mobile.png";
    	            collect += '<img class="jcStatusImage jcDesktop" src="' + statusImageFile + '" alt="order in transit icon" /><img class="jcStatusImage jcMobile" src="' + statusImageFileMobile + '" alt="order in transit icon" />';
    	            collect += '<div class="jcOrderPhone"><h3>Your phone number: <br><span class="jcRed jcPhone">' + order.customer_phone + '</span></h3><br><p>' + phoneNumberMessage + '</p></div>';
    	            if (order.scheduled_date != "TBA") {
    	                collect += '<div class="jcFloat" id="jcCalendarWrapper"><h3>Delivery date:</h3><h3 class="jcRed">TODAY!<br>Between ' + order.scheduled_time_window + '</h3></div>';
    	            } else {
    	                collect += '<div class="jcFloat"><h3>Delivery date:</h3><h3 class="jcRed">TBA</h3></div>';
    	            }
    	            if (order.address1 != "N/A") {
    	                collect += '<div class="jcFloat"><h3>Delivery address:</h3><h3 class="jcRed">' + order.address1 + '<br>' + order.city + ', ' + order.state + ' ' + order.zip + '</h3></div>';
    	            } else {
    	                collect += '<div class="jcFloat"><h3>Delivery address:</h3><h3 class="jcRed">N/A<br><br></h3><div id="map"></div></div>';
    	            }
    	            collect += '<div style="clear:both;"></div>';
    	            var truckImageFile = Urls.libraryPath + "/images/Landing/dispatch-tracker-page/delivery-tracking-truck-map-overlay.png";
    	            var truckImageFileMobile = Urls.libraryPath + "/images/Landing/dispatch-tracker-page/delivery-tracking-truck-map-overlay-mobile.png";
    	            collect += '<div id="mapWrapper"><div id="map"></div></div>';
    	            collect += '<div class="jcFloat">';
    	            if (order.expected_time != "") {
    	                collect += '<h3>Expected arrival:</h3><h3 class="jcRed">' + order.expected_time + '<br>Stop number ' + order.stop + '</h3>';
    	            } else {
    	                collect += '<h3>Expected arrival:</h3><h3 class="jcRed">Your delivery is being scheduled. <br>Please, check back shortly. <br>Stop number ' + order.stop + '</h3>';
    	            }
    	            collect += '</div>';
    	            collect += '<div class="jcFloat"><h3>Your service team:</h3><h3 class="jcRed">' + order.driver_name + '<br>';
    	            if (order.current_stop != "") {
    	                collect += 'Currently at stop no. ' + order.current_stop + ' <a href="javascript:;" class="jcRefresh" title="refresh">↻</a></h3>';
    	            } else {
    	                collect += 'Current stop N/A <a href="javascript:;" class="jcRefresh" title="refresh">↻</a></h3>';
    	            }
    	            collect += '</div>';
    	            
    	            var collect2 = '';
    	            if (order.line_items.length > 0) {
    	            	collect2 = '<h2 class="jcLine">ITEMS IN THIS DELIVERY</h2>';
    	            	collect2 += printItems(order.line_items);
    	            }
    	            
    	            if (order.ax_line_items.length > 0) {
    	                var collect3 = '<h2 class="jcLine">OTHER ITEMS IN THIS ORDER</h2><p>These items are in the same order but are being delivered separately.</p>';
    	                collect3 += printItems(order.ax_line_items);
    	                $("#jcBox5b .jcBoxInner").html(collect3);
    	                addProductInfo(order.ax_line_items);
    	                $("#jcBox5b").show();
    	            } else {
    	                $("#jcBox5b").hide();
    	            }

    	            $("#jcBox1 .jcBoxInner #jcOrderContent").html(collect);
    	            $("#jcBox1 .jcBoxInner #jcOrderItems").html(collect2);

    	            if (order.latitude != "N/A" && order.longitude != "N/A") {
    	                initMap(order.latitude, order.longitude);
    	            } else {
    	                $("#map").hide();
    	            }
    	            addProductInfo(order.line_items);
    	            $("#jcBox2, #jcBox3, #jcBox4").hide();
					$("#jcBox6, #jcBox1 #jcOrderError1").show();

    	        } else if (order.status == "Finished" && deliveryDate == '') {
    	            collect = '<p class="jcBack"><a class="goback" href="javascript:;">< Back to search</a></p>';
    	            collect += '<h2>DELIVERED</h2>';
    	            collect += '<p class="jcOrderID">Order Number: ' + order.order_number + '</p>';
    	            var statusImageFile = Urls.libraryPath + "/images/Landing/dispatch-tracker-page/delivery-tracking-delivered.png";
    	            var statusImageFileMobile = Urls.libraryPath + "/images/Landing/dispatch-tracker-page/delivery-tracking-delivered-mobile.png";
    	            collect += '<img class="jcStatusImage jcDesktop" src="' + statusImageFile + '" alt="order delivered icon" /><img class="jcStatusImage jcMobile" src="' + statusImageFileMobile + '" alt="order delivered icon" />';
    	            if (order.actual_delivery_time != "TBA") {
    	                collect += '<div class="jcFloat" id="jcCalendarWrapper"><h3>Delivered on:</h3><h3 class="jcRed">' + order.actual_delivery_time + '<br><br></h3><div id="jcCalendar" style=""></div></div>';
    	            } else {
    	                collect += '<div class="jcFloat"><h3>Delivery date:</h3><h3 class="jcRed">TBA<br><br></h3></div>';
    	            }
    	            if (order.address1 != "N/A") {
    	                collect += '<div class="jcFloat"><h3>Delivery address:</h3><h3 class="jcRed">' + order.address1 + '<br>' + order.city + ', ' + order.state + ' ' + order.zip + '</h3><div id="map"></div></div>';
    	            } else {
    	                collect += '<div class="jcFloat"><h3>Delivery address:</h3><h3 class="jcRed">N/A<br><br></h3><div id="map"></div></div>';
    	            }
    	            collect += '<div style="clear:both;"></div>';
    	            collect += '<div class="jcFloat"></div>';
    	            collect += '<div class="jcFloat"><h3>Your service team:</h3><h3 class="jcRed">' + order.driver_name + '</h3></div>';
    	            collect += '<div style="clear:both;"></div>';
    	            
    	            var collect2 = '';
    	            if (order.line_items.length > 0) {
	    	            collect2 = '<h2 class="jcLine">ITEMS IN THIS DELIVERY</h2>';
	    	            collect2 += printItems(order.line_items);
    	            }
    	            if (order.ax_line_items.length > 0) {
    	                var collect3 = '<h2 class="jcLine">OTHER ITEMS IN THIS ORDER</h2><p>These items are in the same order but are being delivered separately.</p>';
    	                collect3 += printItems(order.ax_line_items);
    	                $("#jcBox5b .jcBoxInner").html(collect3);
    	                addProductInfo(order.ax_line_items);
    	                $("#jcBox5b").show();
    	            } else {
    	                $("#jcBox5b").hide();
    	            }

    	            $("#jcBox1 .jcBoxInner #jcOrderContent").html(collect);
    	            $("#jcBox1 .jcBoxInner #jcOrderItems").html(collect2);

    	            if (order.actual_delivery_time != "TBA") {
    	                initCalendar(order.scheduled_date);
    	            }

    	            if (order.latitude != "N/A" && order.longitude != "N/A") {
    	                initMap(order.latitude, order.longitude);
    	            } else {
    	                $("#map").hide();
    	            }
    	            addProductInfo(order.line_items);

    	            $("#jcBox2, #jcBox3, #jcBox5, #jcBox1 .jcOrderError").hide();
    	            $("#jcBox6, #jcBox4").show();

    	        } else {
    	            var curDate = new Date();
    	            var curItem = null;
					var curItemDropShip = false;
					var curItemParcel = false;
    	            if (deliveryDate) {
    	                function getByDateValue(arr, value) {
    	                    for (var i = 0, iLen = arr.length; i < iLen; i++) {
    	                        if (arr[i].shippingDateConfirmedTime == value) return arr[i];
    	                    }
    	                }
    	                curItem = getByDateValue(order.ax_line_items, deliveryDate);
    	                if (curItem) {
    	                    if (curItem.carrierName) {
    	                        curItemDropShip = true;
    	                    }
    	                }
    	            } else {
						for(var i = 0; i<order.ax_line_items.length; i++) {
							if(order.ax_line_items[i].deliveryMode === "Parcel") {
								curItemParcel = true;
							} else if(order.ax_line_items[i].deliveryMode === "Direct Del") {
								curItemDropShip = true;
							}
						}
					}
    	            collect = '<p class="jcBack"><a class="goback" href="javascript:;">< Back to search</a> <a class="simplereload" style="float:right;" href="javascript:;">Refresh ↻</a></p>';
    	            collect += '<h2>ORDER</h2><p style="text-align:center;margin:0;padding:0;"></p>';
    	            collect += '<p class="jcOrderID">Order Number: ' + order.order_number + '</p>';
    	            if (order.status != "???" && order.status != "") {
    	                //collect += '<p class="jcOrderID">Order Status: ' + order.status + '</p>';
    	            }
    	            var statusImageFile = Urls.libraryPath + "/images/Landing/dispatch-tracker-page/delivery-tracking-truck.png";
    	            if (order.drop_ship == true || curItemDropShip == true) {
    	                statusImageFile = Urls.libraryPath + "/images/Landing/dispatch-tracker-page/delivery-tracking-truck-ds.png";
    	                var dropShipInfo = ""

    	                if (order.trackingURL && order.trackingURL != "") {
    	                    dropShipInfo = "<h2 style='margin-top:20px;'>" + order.earliest_scheduled_carrier + "</h2><h3>Tracking Number: <strong><a href='" + order.trackingURL + "' target='_blank'>" + order.earliest_scheduled_tracking_number + "</a></strong> <div id='trackingCopyDiv'>Copy</div>";
    	                    dropShipInfo += "<div class='orderTrackingDiv' style='display:none;'>" + order.earliest_scheduled_tracking_number + "</div></h3>"
						} else if (order.earliest_scheduled_carrier && order.earliest_scheduled_carrier != "") { 
    	                    dropShipInfo = "<h2 style='margin-top:20px;'>" + order.earliest_scheduled_carrier + "</h2><h3>Tracking Number: <strong><a href='" + order.trackingURL + "' target='_blank'>" + order.earliest_scheduled_tracking_number + "</a></strong> <div id='trackingCopyDiv'>Copy</div>";
							dropShipInfo += "<div class='orderTrackingDiv' style='display:none;'>" + order.earliest_scheduled_tracking_number + "</div></h3>"
							// dropShipInfo += "<h3><strong>The tracking status for your order is not available, please contact the carrier directly for tracking updates or call the phone number on your receipt for more help.</strong></h3>";
						}else {
    	                    dropShipInfo = "<h2 style='margin-top:20px;'>" + order.earliest_scheduled_carrier + "</h2><h3>Tracking Number: <strong>" + order.earliest_scheduled_tracking_number;
    	                    dropShipInfo += "<div class='orderTrackingDiv' style='display:none;'>" + order.earliest_scheduled_tracking_number + "</div></h3>"
    	                    // dropShipInfo += "<h3><strong>The tracking status for your order is not available, please contact the carrier directly for tracking updates or call the phone number on your receipt for more help.</strong></h3>";
    	                }
    	                if (curItem) {
    	                    if (curItem.trackingURL && curItem.trackingURL != "") {
    	                        dropShipInfo = "<h2 style='margin-top:20px;'>" + curItem.carrierName + "</h2><h3>Tracking Number: <strong><a href='" + curItem.trackingURL + "' target='_blank'>" + curItem.trackingNumber + "</a></strong><div id='trackingCopyDiv'>Copy</div>"
    	                        dropShipInfo += "<div class='orderTrackingDiv' style='display:none;'>" + curItem.trackingNumber + "</div></h3>"
    	                    } else {
    	                        dropShipInfo = "<h2 style='margin-top:20px;'>" + curItem.carrierName + "</h2><h3>Tracking Number: <strong>" + curItem.trackingNumber + "</strong><div id='trackingCopyDiv'>Copy</div>"
    	                        dropShipInfo += "<div class='orderTrackingDiv' style='display:none;'>" + curItem.trackingNumber + "</div></h3>"
    	                        // dropShipInfo += "<h3><strong>The tracking status for your order is not available, please contact the carrier directly for tracking updates or call the phone number on your receipt for more help.</strong></h3>";
    	                    }
    	                }
    	                $('#jcDropShipInfo').html(dropShipInfo);
    	                $('#jcDropShipInfo, #jcBox5c').show();
						$('#jcOrderError1').hide();
    	            } else {
    	                $('#jcDropShipInfo, #jcBox5c').hide();
						$('#jcOrderError1').show();
					}
    	            collect += '<img class="jcStatusImage" src="' + statusImageFile + '" alt="delivery truck icon" />';

    	            var deliveryH3 = "Delivery date:";
    	            if ((curItemDropShip == false && curItemParcel == false) || order.hasInHomeDelivery) {
    	                collect += '<div class="jcOrderPhone"><h3>Your phone number: <br><span class="jcRed jcPhone">' + order.customer_phone + '</span></h3><br><p>' + phoneNumberMessage + '</p></div>';
    	            } else {
    	                collect += "<div style='clear:both;padding: 20px;'></div>";
    	                deliveryH3 = "Shipping date:";
    	            }
    	            var chicagoOrderStatus = isChicago(order);
    	            if(!chicagoOrderStatus && order.drop_ship == false && order.earliest_scheduled_shipping_date == "") {
    	            	collect += '<p class="covid19-message">A representative will contact you in 48-72 hours via email to schedule your delivery.</p>';
    	            }
    	            if (curItem) {
    	            	var showDeliveryDate = true;
    	            	if (curItem) {
    	            		showDeliveryDate = showDeliveryDateAndCalendarStatus(curItem);
    	            	}
    	            	if (showDeliveryDate) {
        	            	collect += '<div class="jcFloat" id="jcCalendarWrapper"><h3>' + deliveryH3 + '</h3><h3 class="jcRed">' + curItem.shippingDateConfirmed + '<br><br></h3><div id="jcCalendar" style=""></div></div>';
        	            } else {
							collect += '<div class="jcFloat" id="jcCalendarWrapper"><h3>' + deliveryH3 + '</h3><div class="availability-web deliverytracking-page"><h3 id="maxAvailabilityMessage" class="jcRed"></h3></div><div id="jcCalendar" data-hide-calendar="true" style=""></div></div>';
            			}
    	            } else if (order.scheduled_date != "TBA") {
                		var chicagoTrackingNumberStatus = false;
                		if(chicagoOrderStatus) {
                			chicagoTrackingNumberStatus = chicagoOrderHasTrackingNumber(order);
                		}
                		if(chicagoOrderStatus) {
                			if(chicagoTrackingNumberStatus) {
                				collect += '<div class="jcFloat" id="jcCalendarWrapper"><h3>' + deliveryH3 + '</h3><h3 class="jcRed">' + order.scheduled_date + '<br>Between ' + order.scheduled_time_window + '</h3><div id="jcCalendar" style=""></div></div>';
                			} else {
								collect += '<div class="jcFloat" id="jcCalendarWrapper"><h3>' + deliveryH3 + '</h3><div class="availability-web deliverytracking-page"><h3 id="maxAvailabilityMessage" class="jcRed"></h3></div><div id="jcCalendar" data-hide-calendar="true" style=""></div></div>';
                			}
                		} else {
                				collect += '<div class="jcFloat" id="jcCalendarWrapper"><h3>' + deliveryH3 + '</h3><h3 class="jcRed">' + order.scheduled_date + '<br>Between ' + order.scheduled_time_window + '</h3><div id="jcCalendar" style=""></div></div>';
                		}
                	} else if (order.earliest_scheduled_shipping_date != "") {
                    	if(order.earliest_scheduled_tracking_number != "") {
                    		var chicagoOrderStatus = isChicago(order);
                    		var chicagoTrackingNumberStatus = false;
                    		if(chicagoOrderStatus) {
                    			chicagoTrackingNumberStatus = chicagoOrderHasTrackingNumber(order);
                    		}
                    		if(chicagoOrderStatus) {
                    			if(chicagoTrackingNumberStatus) {
                    				collect += '<div class="jcFloat" id="jcCalendarWrapper"><h3>' + deliveryH3 + '</h3><h3 class="jcRed">' + order.earliest_scheduled_shipping_date + '<br><br></h3><div id="jcCalendar" style=""></div></div>';
                    			} else {
									collect += '<div class="jcFloat" id="jcCalendarWrapper"><h3>' + deliveryH3 + '</h3><div class="availability-web deliverytracking-page"><h3 id="maxAvailabilityMessage" class="jcRed"></h3></div><div id="jcCalendar" data-hide-calendar="true" style=""></div></div>';
								}
                    		} else if (curItemDropShip) { 
								collect += '<div class="jcFloat" id="jcCalendarWrapper"><h3>' + deliveryH3 + '</h3><div class="availability-web deliverytracking-page"><h3 id="maxAvailabilityMessage" class="jcRed"></h3></div><div id="jcCalendar" data-hide-calendar="true" style=""></div></div>';
							} else {
                    			collect += '<div class="jcFloat" id="jcCalendarWrapper"><h3>' + deliveryH3 + '</h3><h3 class="jcRed">' + order.earliest_scheduled_shipping_date + '<br><br></h3><div id="jcCalendar" style=""></div></div>';
                    		}
                    	}
                    } else {
						if(order.drop_ship) { 
							collect += '<div class="jcFloat" id="jcCalendarWrapper"><h3>' + deliveryH3 + '</h3><div class="availability-web deliverytracking-page"><h3 id="maxAvailabilityMessage" class="jcRed"></h3></div><div id="jcCalendar" data-hide-calendar="true" style=""></div></div>';
						} else {
							collect += '<div class="jcFloat"><h3>' + deliveryH3 + '</h3><h3 class="jcRed">To Be Determined.</h3></div>';
						}

    	            }
    	            if (order.address1 != "N/A") {
    	                collect += '<div class="jcFloat"><h3>Delivery address:</h3><h3 class="jcRed">' + order.address1 + '<br>' + order.city + ', ' + order.state + ' ' + order.zip + '</h3><div id="map"></div></div>';
    	            } else {
    	                collect += '<div class="jcFloat"><h3>Delivery address:</h3><h3 class="jcRed">N/A<br><br></h3><div id="map"></div></div>';
    	            }

    	            //if delivery is today (no DT data is received)
                    if (order.earliest_scheduled_shipping_date != "" && !curItem) {
    	                var essd = new Date(order.earliest_scheduled_shipping_date)
    	                var essd_ymd = essd.getFullYear() + ("0" + essd.getMonth()).slice(-2) + ("0" + essd.getDate()).slice(-2);
    	                var curDate_ymd = curDate.getFullYear() + ("0" + curDate.getMonth()).slice(-2) + ("0" + curDate.getDate()).slice(-2);
    	                console.log(essd_ymd + " - " + curDate_ymd);
    	                console.log(essd_ymd > curDate_ymd);
    	                if (essd_ymd == curDate_ymd) {
    	                    collect += '<div class="jcFloat"><h3>Expected arrival:</h3><h3 class="jcRed">Your delivery is being scheduled. <br>Please, check back shortly.</h3></div><div class="jcFloat"> </div>';
    	                }
    	            }

					var collect2 = '<h2 class="jcLine">ITEMS IN THIS DELIVERY</h2>';

    	            var otherItems = "";
    	            var curOrderDateTime = order.earliest_scheduled_shipping_date_time;
    	            if (curItem) {
    	                curOrderDateTime = curItem.shippingDateConfirmedTime;
    	            }
    	            if (order.drop_ship == true || curItemDropShip == true) {
    	                collect2 += printItems(order.ax_line_items, curOrderDateTime, true, order.earliest_scheduled_carrier, order.earliest_scheduled_tracking_number);
    	                otherItems = printItems(order.ax_line_items, curOrderDateTime, false, order.earliest_scheduled_carrier, order.earliest_scheduled_tracking_number);
    	                if (curItem) {
    	                    otherItems += printItems(order.line_items, curOrderDateTime, false, order.earliest_scheduled_carrier, order.earliest_scheduled_tracking_number);
    	                }
    	            } else {
    	                if (curOrderDateTime !== "") {
    	                    collect2 += printItems(order.ax_line_items, curOrderDateTime, true);
    	                    otherItems = printItems(order.ax_line_items, curOrderDateTime, false);
    	                    if (curItem) {
    	                        otherItems += printItems(order.line_items, curOrderDateTime, false);
    	                    }
    	                } else {
    	                    collect2 += printItems(order.ax_line_items);
    	                    if (curItem) {
    	                        otherItems = printItems(order.line_items);
    	                    }
    	                }
    	            }

    	            if (otherItems != "") {
    	                var collect3 = '<h2 class="jcLine">OTHER ITEMS IN THIS ORDER</h2><p>These items are in the same order but are being delivered separately.</p>';
    	                collect3 += otherItems;
    	                $("#jcBox5b .jcBoxInner").html(collect3);
    	                addProductInfo(order.ax_line_items);
    	                $("#jcBox5b").show();
    	            } else {
    	                $("#jcBox5b").hide();
    	            }


    	            $("#jcBox1 .jcBoxInner #jcOrderContent").html(collect);
    	            $("#jcBox1 .jcBoxInner #jcOrderItems").html(collect2);
    	            
					var showCalendarStatus;
					if (curItem) {
    	            	showCalendarStatus = showDeliveryDateAndCalendarStatus(curItem);
    	            } else if(typeof chicagoOrderStatus != "undefined" && chicagoOrderStatus == true && typeof chicagoTrackingNumberStatus != "undefined" && chicagoTrackingNumberStatus == false) {
    	            	showCalendarStatus = false;
    	            } else {
    	            	showCalendarStatus = true;
    	            }
					
					if(showCalendarStatus) {
	    	            if (curItem) {
	    	                initCalendar(curItem.shippingDateConfirmed, "", order.address1 + ', ' + order.city + ' ' + order.state + ' ' + order.zip);
	    	            } else if (order.scheduled_date != "TBA") {
	    	                if (order.scheduled_time_window != "TBA") {
	    	                    initCalendar(order.scheduled_date, order.scheduled_time_window, order.address1 + ', ' + order.city + ' ' + order.state + ' ' + order.zip);
	    	                } else {
	    	                    initCalendar(order.scheduled_date, "", order.address1 + ', ' + order.city + ' ' + order.state + ' ' + order.zip);
	    	                }
	    	            } else if (order.earliest_scheduled_shipping_date != "") {
	    	                initCalendar(order.earliest_scheduled_shipping_date, "", order.address1 + ', ' + order.city + ' ' + order.state + ' ' + order.zip);
	    	            }
    	            }
    	            if (order.address1 != "N/A") {
    	                var mapAddress = order.address1 + ', ' + order.city + ', ' + order.state + ' ' + order.zip;
    	                var mapAddressSimple = order.address1 + ', ' + order.zip;
    	                initMapAddress(mapAddress, mapAddressSimple);
    	            } else {
    	                $("#map").hide();
    	            }
    	            addProductInfo(order.line_items);
    	            addProductInfo(order.ax_line_items);

    	            $("#jcBox2, #jcBox3, #jcBox4").hide();
    	            $("#jcBox6").show();
    	        }

    	        $("#jcBox1").fadeIn();
    	        //check replaceMeID_deliveryTracking
	            var chicagoProdID = "";
				if (($('#maxAvailabilityMessage') != null && $('#maxAvailabilityMessage').length) 
					|| ($('.jcPdpMessage') != null && $('.jcPdpMessage').length)) {

					var focusedItems = $('.thisDelivery')
					var thisConfirmedDelivery = $('.thisDelivery .jcLineItemConfirmedDate')
					var focusedId = "";
					var focusedDelivery = "";

					if(focusedItems.length > 0) {
						for(var i=0; i<focusedItems.length; i++) {
							focusedId = focusedItems[i].id.split('mfi')[1];
							if(focusedId != "") 
								i=focusedItems.length;
						}
					}

					for(var i=0; i<order.ax_line_items.length; i++) {
						if(order.ax_line_items[i].productID == focusedId) {
							focusedDelivery = order.ax_line_items[i].deliveryMode;
						}
					}

					var otherItems = $('.otherDelivery');
					if(otherItems.length > 0) {

					}

					if(focusedDelivery == "Red Carpet" || focusedDelivery == "Parcel") {
						$('#jcDropShipInfo, #jcBox5c').hide();
						$('#jcOrderError1').show();
					} else {
						$('#jcDropShipInfo, #jcBox5c').show();
						$('#jcOrderError1').hide();
					}

					if(thisConfirmedDelivery.length !== 0) {
						var confirmedShippingDate = thisConfirmedDelivery[0].innerText.split('Shipping Date: ')[1];
						$('#maxAvailabilityMessage').html(confirmedShippingDate);
						
						if($('.jcPdpMessage') != null && $('.jcPdpMessage').length) {

							var otherDropShipExists = false;
							var otherParcelExists = false;

							if(otherItems.length > 0) {
								for(var i=0; i<otherItems.length; i++) {
									var otherId = otherItems[i].id.split('mfi')[1];
									if(otherId != "") {
										var otherDeliveryMode = "";
										for(var i=0; i<order.ax_line_items.length; i++) {
											if(order.ax_line_items[i].productID == otherId) {
												otherDeliveryMode = order.ax_line_items[i].deliveryMode;
											}
										}

										if(otherDeliveryMode == "Parcel")
											otherParcelExists = true;
										else if(otherDeliveryMode == "Direct Del")
											otherDropShipExists = true;
									}
								}
							}

							if (curItemDropShip || otherDropShipExists) {
								getMaxAvailabilityMessage(order.ax_line_items, "2", "otherItems");
							}
							
							if (curItemParcel || otherParcelExists) {
								getMaxAvailabilityMessage(order.ax_line_items, "3", "otherItems");
							}
						}
					} else if (curItemDropShip && curItemParcel) {						
						if(focusedDelivery == "Direct Del") {
							getMaxAvailabilityMessage(order.ax_line_items, "2", "focusedItems");
							getMaxAvailabilityMessage(order.ax_line_items, "3", "otherItems");
						} else if(focusedDelivery == "Parcel") {
							getMaxAvailabilityMessage(order.ax_line_items, "3", "focusedItems");
							getMaxAvailabilityMessage(order.ax_line_items, "2", "otherItems");
						}
					} else if (curItemDropShip) {
						getMaxAvailabilityMessage(order.ax_line_items, "2", "focusedItems");
					} else if (curItemParcel) {
						getMaxAvailabilityMessage(order.ax_line_items, "3", "focusedItems");
					}
				}

    	    } else {
    	        var lineItemArray = [];
    	        collect = '<p class="jcBack"><a class="goback" href="javascript:;">< Back</a></p>';
    	        collect += '<h2 class="jcSearchH2">Delivery Tracking Search Results</h2><p class="jcSearchP" style="text-align:left;">We found multiple deliveries linked to your phone number. Which one do you want to track?</p>';
    	        collect += '<table class="jcOrderTable"><tr><th>Date Ordered</th><th>Status</th><th>Order Number</th><th>Items</th><th></th>';
    	        for (var i = 0; i < orderIdArray.length; i++) {
    	            console.log(orderIdArray.length);
    	            order = orderObj[orderIdArray[i]];
    	            if (order.status == "Scheduled") {
    	                var status = "Preparing Order";
    	            } else if (order.status == "On the way") {
    	                var status = "In Transit";
    	            } else if (order.status == "Finished") {
    	                var status = "Delivered";
    	            } else {
    	                var status = order.status;
    	            }
    	            collect += '<tr>';
    	            collect += '<td><strong class="jcMobileInline">Date Ordered:</strong> ' + order.created_date + '</td>';
    	            collect += '<td><strong class="jcMobileInline">Status:</strong> ' + order.status + ' <a href="javascript:;" class="jcMobile chooseOrder" style="float:right;" data-orderArrayNumber=' + i + '><strong>Track ></strong></a></td>';
    	            collect += '<td><strong class="jcMobileInline">Order Number:</strong> ' + order.order_number + '</td>';
    	            collect += '<td>';
    	            for (var j = 0; j < order.line_items.length; j++) {
    	                if (j < 2) {
    	                    collect += '<div class="jcLineItemImage" id="mfi' + order.line_items[j].productID + '"></div>';
    	                    lineItemArray.push(order.line_items[j].productID);
    	                }
    	            }
    	            if (order.line_items.length > 2) {
    	                collect += '<div class="jcLineItemImage"><div class="jcLineItemImageMore">+' + (order.line_items.length - 2) + ' more</div></div>';
    	            }
    	            collect += '</td>';
    	            collect += '<td><a class="jcDesktop chooseOrder" href="javascript:;"  data-orderArrayNumber=' + i + '><strong>Track ></strong></a></td>';
    	        }
    	        collect += '</table>';
    	        addProductInfo(lineItemArray, 1);

    	        $("#jcBox1 .jcBoxInner #jcOrderContent").html(collect);
    	        $("#jcBox2, #jcBox3, #jcBox4, #jcBox6").hide();
				$("#jcBox1").fadeIn();
    	    }

    	}

    	function getOrders(orderNumber, securityOption, securityValue) {

    	    orderObj = {};
			orderIdArray = [];
			showPrepareForDelivery = false;
    	    var lineItemExclusions = ["CALIFORNIA RECYCLING FEE", "CONNECTICUT RECYCLING FEE"];

    	    var ajax = "orderNumber=" + orderNumber + "&securityOption=" + securityOption + "&securityValue=" + securityValue + "&" + CSRFTokenName + "=" + CSRFToken;
    	    $.ajaxSetup({
    	        cache: false
    	    });
    	    $.getJSON("/on/demandware.store/Sites-Mattress-Firm-Site/default/DispatchTracking-SubmitOrderNumber?", ajax, function(data, status) {

    	        if (status == "success" && data != "" && data.error == "An error occurred with status code 500") {
    	            $("#jcWarn1").html("Could not connect. Please, try again.");
    	        } else if (status == "success" && data != "" && data.error == "An error occurred with status code 400") {
    	            $("#jcWarn1").html("Last Name, Delivery Zipcode or Phone Number did not match our records.");
    	        } else if (status == "success" && data != "" && data.error == "CSRF expired") {
    	            $("#trackingSearch").hide();
    	            $("#jcFormSubmit").hide();
    	            $("#jcWarn1").html("Session expired. Please <a href='javascript:;'>refresh</a> page.");
    	            $("#jcBox2, #jcBox3, #jcBox4, #jcBox5").show();
    	            $("#jcBox6, #jcBox1").hide();
    	        } else if (status == "success" && data != "") {
    	            orderCount = data.length;

    	            for (var i = 0; i < data.length; i++) {
    	                $.each(data, function(key, value) {

    	                    //add order information to order object
    	                    orderObj[value.order_number] = {
    	                        order_number: "N/A",
    	                        status: "N/A",
    	                        customer_phone: "N/A",
    	                        created_date: "N/A",
    	                        scheduled_date: "TBA",
    	                        scheduled_time_window: "TBA",
    	                        expected_time: "",
    	                        actual_delivery_time: "TBA",
    	                        current_stop: "N/A",
    	                        last_stop_served: "N/A",
    	                        stop: "N/A",
    	                        latitude: "N/A",
    	                        longitude: "N/A",
    	                        address1: "N/A",
    	                        city: "",
    	                        state: "",
    	                        zip: "",
    	                        driver_name: "TBA",
    	                        driver_description: "TBA",
    	                        line_items: [],
    	                        ax_line_items: [],
    	                        earliest_scheduled_shipping_date: "",
    	                        earliest_scheduled_shipping_date_time: "",
    	                        earliest_scheduled_carrier: "",
    	                        earliest_scheduled_tracking_number: "Coming Soon",
								drop_ship: false,
								hasInHomeDelivery: false,
    	                        customer_name: "",
    	                    }

    	                    if (value.order_number) {
    	                        orderObj[value.order_number].order_number = value.order_number;
    	                    }
    	                    if (value.status) {
    	                        orderObj[value.order_number].status = value.status;
    	                    }
    	                    if (value.customer_phone) {
    	                        orderObj[value.order_number].customer_phone = normalizePhone(value.customer_phone);
    	                    }
    	                    if (value.created_date) {
    	                        orderObj[value.order_number].created_date = value.created_date;
    	                    }
    	                    if (value.scheduled_date) {
    	                        orderObj[value.order_number].scheduled_date = value.scheduled_date;
    	                    }
    	                    if (value.scheduled_time_window) {
    	                        orderObj[value.order_number].scheduled_time_window = value.scheduled_time_window;
    	                    }
    	                    if (value.expected_time) {
    	                        orderObj[value.order_number].expected_time = value.expected_time;
    	                    }
    	                    if (value.actual_delivery_time) {
    	                        orderObj[value.order_number].actual_delivery_time = value.actual_delivery_time;
    	                    }
    	                    if (value.current_stop) {
    	                        orderObj[value.order_number].current_stop = value.current_stop;
    	                    }
    	                    if (value.last_stop_served) {
    	                        orderObj[value.order_number].last_stop_served = value.last_stop_served;
    	                    }
    	                    if (value.stop) {
    	                        orderObj[value.order_number].stop = value.stop;
    	                    }
    	                    if (value.latitude) {
    	                        orderObj[value.order_number].latitude = value.latitude;
    	                    }
    	                    if (value.longitude) {
    	                        orderObj[value.order_number].longitude = value.longitude;
							}
    	                    if (value.delivery_address.address1) {
    	                        orderObj[value.order_number].address1 = value.delivery_address.address1;
    	                    }
    	                    if (value.delivery_address.city) {
    	                        orderObj[value.order_number].city = value.delivery_address.city;
    	                    }
    	                    if (value.delivery_address.state) {
    	                        orderObj[value.order_number].state = value.delivery_address.state;
    	                    }
    	                    if (value.delivery_address.zip) {
    	                        orderObj[value.order_number].zip = value.delivery_address.zip;
    	                    }
    	                    if (value.customer_name) {
    	                        orderObj[value.order_number].customer_name = value.customer_name;
    	                    }


    	                    if (value.driver_details) {
    	                        if (value.driver_details[0]) {
    	                            orderObj[value.order_number].driver_name = value.driver_details[0].name;
    	                            orderObj[value.order_number].driver_description = value.driver_details[0].description;
    	                        }
    	                    }

    	                    var itemCount = 0;
    	                    var axItemCount = 0;
    	                    var earliestShippingDate = "";
    	                    var earliestShippingDateTime = 0;
    	                    for (var j = 0; j < value.line_items.length; j++) {
    	                        var itemObj = {};
    	                        var itemDescription = "";
    	                        if (value.line_items[j].description) {
    	                            itemDescription = value.line_items[j].description;
    	                        }

    	                        if (lineItemExclusions.indexOf(value.line_items[j].description) < 0) {
    	                            var serialNumber = "";
    	                            var productID = "";
    	                            if (value.line_items[j].serial_number) {
    	                                serialNumber = value.line_items[j].serial_number.split("-");
    	                                productID = serialNumber[1];
    	                            }

    	                            itemObj = {
    	                                quantity: "",
    	                                serial_number: "",
    	                                description: itemDescription,
    	                                productID: productID,
    	                            }

    	                            if (value.line_items[j].quantity) {
    	                                itemObj.quantity = value.line_items[j].quantity;
    	                            }
    	                            if (value.line_items[j].serial_number) {
    	                                itemObj.serial_number = value.line_items[j].serial_number;
    	                            }
    	                            if (value.line_items[j].shippingDateConfirmed) {
    	                                if (value.line_items[j].shippingDateConfirmed != "Dec 31, 2049") {
    	                                    var shippingDateConfirmedTime = new Date(value.line_items[j].shippingDateConfirmed).getTime();
    	                                    if (earliestShippingDateTime < 1) {
    	                                        earliestShippingDateTime = shippingDateConfirmedTime;
    	                                        earliestShippingDate = value.line_items[j].shippingDateConfirmed;
    	                                        if (value.line_items[j].carrierName) {
    	                                            orderObj[value.order_number].drop_ship = true;
    	                                            orderObj[value.order_number].earliest_scheduled_carrier = value.line_items[j].carrierName;
    	                                            if (value.line_items[j].trackingNumber) {
    	                                                orderObj[value.order_number].earliest_scheduled_tracking_number = value.line_items[j].trackingNumber;
    	                                                orderObj[value.order_number].trackingURL = value.line_items[j].trackingURL;
    	                                            }
    	                                        }
    	                                    } else if (shippingDateConfirmedTime < earliestShippingDateTime) {
    	                                        earliestShippingDateTime = shippingDateConfirmedTime;
    	                                        earliestShippingDate = value.line_items[j].shippingDateConfirmed;
    	                                        if (value.line_items[j].carrierName) {
    	                                            orderObj[value.order_number].drop_ship = true;
    	                                            orderObj[value.order_number].earliest_scheduled_carrier = value.line_items[j].carrierName;
    	                                            if (value.line_items[j].trackingNumber) {
    	                                                orderObj[value.order_number].earliest_scheduled_tracking_number = value.line_items[j].trackingNumber;
    	                                                orderObj[value.order_number].trackingURL = value.line_items[j].trackingURL;
    	                                            }
    	                                        } else {
    	                                            orderObj[value.order_number].drop_ship = false;
    	                                        }
    	                                    }
    	                                    if ($("#deliveryDate").val()) {
    	                                        orderObj[value.order_number].drop_ship = false;
    	                                    }
    	                                    itemObj.shippingDateConfirmed = value.line_items[j].shippingDateConfirmed;
    	                                    itemObj.shippingDateConfirmedTime = shippingDateConfirmedTime;
    	                                } else {
    	                                    itemObj.shippingDateConfirmed = "Not Scheduled";
    	                                }
    	                            }

    	                            if (value.line_items[j].carrierName) {
    	                                itemObj.carrierName = value.line_items[j].carrierName;
    	                            }

    	                            if (value.line_items[j].trackingNumber) {
    	                                itemObj.trackingNumber = value.line_items[j].trackingNumber;
    	                                itemObj.trackingURL = value.line_items[j].trackingURL;
    	                            }
    	                            
    	                            if(value.line_items[j].deliveryMode) {
										itemObj.deliveryMode = value.line_items[j].deliveryMode;
										if(value.line_items[j].deliveryMode == "Direct Del") {
											orderObj[value.order_number].drop_ship = true;
										} else if(value.line_items[j].deliveryMode == "Red Carpet") {
											orderObj[value.order_number].hasInHomeDelivery = true;
										}
    	                            }

    	                            if (value.line_items[j].type) {
    	                                itemObj.type = value.line_items[j].type;

    	                                if (value.line_items[j].type == "AX") {
    	                                    orderObj[value.order_number].ax_line_items[axItemCount] = itemObj;
    	                                    axItemCount++;
    	                                } else {
    	                                    orderObj[value.order_number].line_items[itemCount] = itemObj;
    	                                    itemCount++;
    	                                }
    	                            }
    	                        }

    	                    }


    	                    if (earliestShippingDate != "") {
    	                        orderObj[value.order_number].earliest_scheduled_shipping_date = earliestShippingDate;
    	                        orderObj[value.order_number].earliest_scheduled_shipping_date_time = earliestShippingDateTime;
    	                    }

    	                    //remove duplicate items in DT array from AX array
    	                    for (var k = 0; k < orderObj[value.order_number].line_items.length; k++) {
    	                        var serialNumber = orderObj[value.order_number].line_items[k].serial_number;
    	                        for (var l = 0; l < orderObj[value.order_number].ax_line_items.length; l++) {
    	                            if (orderObj[value.order_number].ax_line_items[l].serial_number === serialNumber) {
    	                                orderObj[value.order_number].ax_line_items.splice(l, 1);;
    	                            }
    	                        }

    	                    }

    	                    orderIdArray.push(value.order_number);


    	                });

    	                //security check
    	                var securityCheck = 0;
    	                var formLastName = $("form #lastName").val().trim();
    	                var formZipCode = $("form #zipCode").val().replace(/ /g, '');
    	                var formPhoneNumber = $("form #phoneNumber").val().replace(/ /g, '');
    	                var order = orderObj[orderIdArray[curOrder]];
    	                var warnMessage = "";

    	                if (formLastName != "") {
    	                    var customerLastName = order.customer_name.toLowerCase().split(' ');
    	                    var customerLastNameCheck = 0;

    	                    if (customerLastName.indexOf(formLastName.toLowerCase()) >= 0) {
    	                        customerLastNameCheck = 1;
    	                    } else if (formLastName.length > 5 && order.customer_name.toLowerCase().indexOf(formLastName.toLowerCase()) >= 0) {
    	                        customerLastNameCheck = 1;
    	                    }

    	                    if (customerLastNameCheck == 1) {
    	                        securityCheck = 1;
    	                        $("form #lastName").removeClass("warning");
    	                    } else {
    	                        warnMessage += "Last Name does not match our records. <br>";
    	                        $("form #lastName").addClass("warning");
    	                    }
    	                }

    	                if (formZipCode != "") {
    	                    if (order.zip == formZipCode) {
    	                        securityCheck = 1;
    	                        $("form #zipCode").removeClass("warning");
    	                    } else {
    	                        warnMessage += "Delivery Zip Code does not match our records. <br>";
    	                        $("form #zipCode").addClass("warning");
    	                    }
    	                }

    	                if (formPhoneNumber != "") {
    	                    var customerPhone = order.customer_phone.replace(/\D/g, '');
    	                    var formPhone = formPhoneNumber.replace(/\D/g, '');
    	                    console.log(customerPhone);
    	                    if (customerPhone == formPhone) {
    	                        securityCheck = 1;
    	                        $("form #phoneNumber").removeClass("warning");
    	                    } else {
    	                        warnMessage += "Phone Number does not match our records. <br>";
    	                        $("form #phoneNumber").addClass("warning");
    	                    }
    	                }

    	                if (formLastName == "" && formZipCode == "" && formPhoneNumber == "") {
    	                    warnMessage += "Enter Last Name, Delivery Zip Code or Phone Number. <br>";
    	                    $("form #lastName, form #zipCode, form #phoneNumber").addClass("warning");
    	                } else {
    	                    $("form #lastName, form #zipCode, form #phoneNumber").removeClass("warning");
    	                }

    	                if (securityCheck == 1) {
    	                    //print orders to page
    	                    $("#jcWarn1").html("");
    	                    printOrders();
    	                } else {
    	                    $("#jcWarn1").html(warnMessage);
    	                }
    	            }


    	            $("#jcOrderNotFound").hide();

    	            //add orderNumber and search option parameters to url
    	            addURLParams(orderNumber);

    	        } else {
    	            if ($("#trackingSearchOption").val() == "Phone-Number") {
    	                $("#jcWarn1").html("Order could not be found. <br>Try using your order number.");
    	            } else {
    	                $("#jcWarn1").html("Order could not be found.");
    	                $("#jcOrderNotFound").fadeIn();
    	            }
    	        }
    	    });

    	}

    	function jcSubmitForm() {
    	    var trackingSearchOption = $("#trackingSearchOption").val();
    	    var orderNumber = $("#trackingNumber").val().replace(/ /g, '').toUpperCase().replace("AX-", "");

    	    if (trackingSearchOption == "Phone-Number") {
    	        var jcIntRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    	        if ((orderNumber.length < 10) || (!jcIntRegex.test(orderNumber))) {
    	            $("#trackingNumber").addClass("warning");
    	            $("#jcWarn1").html("Please enter valid 10 digit phone number. (###-###-####)");
    	        } else {
    	            if (orderNumber == "111-222-3333") {
    	                orderObj = orderObjTest;
    	                orderIdArray = orderIdArrayTest;
    	                var storedOrderCount = getUrlParameter("orderCount");
    	                if (!storedOrderCount) {
    	                    orderCount = 3;
    	                }
    	                printOrders();
    	                //add orderNumber and search option parameters to url
    	                addURLParams(orderNumber);
    	            } else {
    	                getOrders(orderNumber);
    	                $("#trackingNumber").removeClass("warning");
    	                $("#jcWarn1").html("Loading...");
    	            }
    	        }
    	    } else {
    	        var regexD = /dw/i;
    	        var regexS = /s/i;
    	        var regexWeb = /web/i;
    	        var regexTPP = /tpp/i;
    	        if (orderNumber.length > 9 && (regexD.test(orderNumber) || regexS.test(orderNumber) || regexWeb.test(orderNumber) || regexTPP.test(orderNumber))) {
    	            if (orderNumber == "dw1111111111") {
    	                orderObj = orderObjTest;
    	                orderIdArray = orderIdArrayTest;
    	                var storedOrderCount = getUrlParameter("orderCount");
    	                if (!storedOrderCount) {
    	                    orderCount = 3;
    	                }
    	                printOrders();
    	                //add orderNumber and search option parameters to url
    	                addURLParams(orderNumber);
    	            } else {

    	                var securityCheck = 0;
    	                var securityOption = "";
    	                var securityValue = "";
    	                var formLastName = $("form #lastName").val().trim();
    	                var formZipCode = $("form #zipCode").val().replace(/ /g, '');
    	                var formPhoneNumber = $("form #phoneNumber").val().replace(/ /g, '');
    	                var warnMessage = "";

    	                if (formLastName != "") {
    	                    if (formLastName.length > 1) {
    	                        securityCheck = 1;
    	                        securityOption = "name";
    	                        securityValue = formLastName;
    	                        $("form #lastName").removeClass("warning");
    	                    } else {
    	                        warnMessage += "Last Name must be atleast 2 characters. <br>";
    	                        $("form #lastName").addClass("warning");
    	                    }
    	                }

    	                if (formZipCode != "") {
    	                    if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(formZipCode) == true) {
    	                        securityCheck = 1;
    	                        securityOption = "zip";
    	                        securityValue = formZipCode;
    	                        $("form #zipCode").removeClass("warning");
    	                    } else {
    	                        warnMessage += "Enter a valid 5 digit zip code. <br>";
    	                        $("form #zipCode").addClass("warning");
    	                    }
    	                }

    	                if (formPhoneNumber != "") {
    	                    var formPhone = formPhoneNumber.replace(/\D/g, '');
    	                    if (formPhone.length > 9) {
    	                        securityCheck = 1;
    	                        securityOption = "phone";
    	                        securityValue = formPhone;
    	                        $("form #phoneNumber").removeClass("warning");
    	                    } else {
    	                        warnMessage += "Enter a valid 10 digit phone number. <br>";
    	                        $("form #phoneNumber").addClass("warning");
    	                    }
    	                }
    	                if (formLastName == "" && formZipCode == "" && formPhoneNumber == "") {
    	                    warnMessage += "Enter Last Name, Delivery Zip Code or Phone Number. <br>";
    	                    $("form #lastName, form #zipCode, form #phoneNumber").addClass("warning");
    	                } else {
    	                    $("form #lastName, form #zipCode, form #phoneNumber").removeClass("warning");
    	                }

    	                if (securityCheck == 1) {
    	                    //print orders to page
    	                    $("#trackingNumber").removeClass("warning");
    	                    $("#jcWarn1").html("Loading...");
    	                    getOrders(orderNumber, securityOption, securityValue);
    	                } else {
    	                    $("#jcWarn1").html(warnMessage);
    	                }

    	            }
    	        } else {
    	            $("#trackingNumber").addClass("warning");
    	            $("#jcWarn1").html("Please enter valid order number. <br>(Phone numbers are not allowed at this time.)");
    	        }
    	    }
    	}

    	$("#jcFormSubmit").click(function() {
    	    jcSubmitForm();
    	});


    	$('#trackingSearch input').keypress(function(e) {
    	    if (e.which == 13) {
    	        jcSubmitForm();
    	    }
    	});

    	function showOderDate(date) {
    	    if (date) {
    	        $("#deliveryDate").val(date);
    	        refresh();
    	    } else {
    	        $("#deliveryDate").val("");
    	        refresh();
    	    }
    	}

    	function refresh() {
    	    $("#jcBox1 .jcBoxInner #jcOrderContent").html('<p style="text-align;center">Loading...</p>');
    	    $("#jcBox1 .jcBoxInner #jcOrderItems").html('<p style="text-align;center">Loading...</p>');
    	    jcSubmitForm();
    	    setTimeout(function() {
    	        chooseOrder(curOrder);
    	    }, 2000)
    	    if($("#jcDropShipInfo"). is(":visible")) {
    	    	$('html, body').animate({
	    	  		scrollTop: $("#jcDropShipInfo").offset().top
	    	        }, 1000);
    	    } else {
	    	  	$('html, body').animate({
	    	  		scrollTop: $("#jcOrderContent").offset().top
	    	        }, 1000);
	    	}
    	}

    	function addURLParams(orderNumber) {

    	    //add form field parameters to url
    	    var trackingSearchOption = $("#trackingSearchOption").val();
    	    var deliveryDate = $("#deliveryDate").val();
    	    var formLastName = $("form #lastName").val().trim();
    	    var formZipCode = $("form #zipCode").val().replace(/ /g, '');
    	    var formPhoneNumber = $("form #phoneNumber").val().replace(/ /g, '').replace(/\D/g, '');
    	    if (deliveryDate) {
    	        history.pushState(null, null, '?orderNumber=' + orderNumber + "&trackingSearchOption=" + trackingSearchOption + "&formLastName=" + formLastName + "&formZipCode=" + formZipCode + "&formPhoneNumber=" + formPhoneNumber + "&deliveryDate=" + deliveryDate);
    	    } else {
    	        history.pushState(null, null, '?orderNumber=' + orderNumber + "&trackingSearchOption=" + trackingSearchOption + "&formLastName=" + formLastName + "&formZipCode=" + formZipCode + "&formPhoneNumber=" + formPhoneNumber);
    	    }

    	}


    	var getUrlParameter = function getUrlParameter(sParam) {
    	    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    	        sURLVariables = sPageURL.split('&'),
    	        sParameterName,
    	        i;

    	    for (var i = 0; i < sURLVariables.length; i++) {
    	        sParameterName = sURLVariables[i].split('=');

    	        if (sParameterName[0] === sParam) {
    	            return sParameterName[1] === undefined ? true : sParameterName[1];
    	        }
    	    }
    	};

    	//on load function
    	$(function() {
    	    var orderNumber = getUrlParameter("orderNumber");
    	    if (orderNumber) {
    	        //alert(storedOrderNumber);
    	        $("#trackingNumber").val(orderNumber);
    	        var trackingSearchOption = getUrlParameter("trackingSearchOption");
    	        $("#trackingSearchOption").val(trackingSearchOption);
    	        var deliveryDate = getUrlParameter("deliveryDate");
    	        $("#deliveryDate").val(deliveryDate);
    	        var formLastName = getUrlParameter("formLastName");
    	        $("#lastName").val(formLastName);
    	        var formZipCode = getUrlParameter("formZipCode");
    	        $("#zipCode").val(formZipCode);
    	        var formPhoneNumber = getUrlParameter("formPhoneNumber");
    	        $("#phoneNumber").val(formPhoneNumber);
    	        jcSubmitForm();
    	    }
    	});

    	var orderIdArrayTest = ["Test1", "Test2", "Test3"];
    	var orderObjTest = {
    	    Test1: {
    	        actual_delivery_time: "TBA",
    	        address1: "2293 Swedish Drive Apt 9",
    	        city: "Clearwater",
    	        created_date: "Jan 14, 2018",
    	        current_stop: "8",
    	        customer_phone: "(111) 222-3333",
    	        driver_description: "",
    	        driver_name: "YUNIER HERNANDEZ",
    	        expected_time: "TBA",
    	        last_stop_served: "6",
    	        latitude: 28.0017,
    	        line_items: [{
    	                quantity: 1,
    	                serial_number: "107848-V000037421",
    	                description: "GREENWOOD FM MATT - S011531058",
    	                productID: "V000037421"
    	            },
    	            {
    	                quantity: 1,
    	                serial_number: "112297-V000061518",
    	                description: "2015 BRWC BOX - S011531058",
    	                productID: "V000061518"
    	            },
    	        ],
    	        longitude: -82.74297,
    	        order_number: "Test1",
    	        scheduled_date: "Jan 15, 2018",
    	        scheduled_time_window: "07:15 PM - 09:15 PM EST",
    	        state: "FL",
    	        status: "Scheduled",
    	        stop: "7",
    	        zip: "33763",
    	    },
    	    Test2: {
    	        actual_delivery_time: "TBA",
    	        address1: "2293 Swedish Drive Apt 9",
    	        city: "Clearwater",
    	        created_date: "Jan 14, 2018",
    	        current_stop: "8",
    	        customer_phone: "(111) 222-3333",
    	        driver_description: "",
    	        driver_name: "YUNIER HERNANDEZ",
    	        expected_time: "08:15 PM - 08:45 PM EST*",
    	        last_stop_served: "6",
    	        latitude: 28.0017,
    	        line_items: [{
    	                quantity: 1,
    	                serial_number: "107848-V000037421",
    	                description: "GREENWOOD FM MATT - S011531058",
    	                productID: "V000037421"
    	            },
    	            {
    	                quantity: 1,
    	                serial_number: "107848-V000037421",
    	                description: "GREENWOOD FM MATT - S011531058",
    	                productID: "V000037421"
    	            },
    	            {
    	                quantity: 2,
    	                serial_number: "112297-V000061518",
    	                description: "2015 BRWC BOX - S011531058",
    	                productID: "V000061518"
    	            },
    	        ],
    	        longitude: -82.74297,
    	        order_number: "Test2",
    	        scheduled_date: "Jan 15, 2018",
    	        scheduled_time_window: "07:15 PM - 09:15 PM EST",
    	        state: "FL",
    	        status: "On the way",
    	        stop: "7",
    	        zip: "33763",
    	    },
    	    Test3: {
    	        actual_delivery_time: "TBA",
    	        address1: "2293 Swedish Drive Apt 9",
    	        city: "Clearwater",
    	        created_date: "Jan 14, 2018",
    	        actual_delivery_time: "Jan 15, 2018",
    	        current_stop: "8",
    	        customer_phone: "(111) 222-3333",
    	        driver_description: "",
    	        driver_name: "YUNIER HERNANDEZ",
    	        expected_time: "TBA",
    	        last_stop_served: "6",
    	        latitude: 28.0017,
    	        line_items: [{
    	                quantity: 1,
    	                serial_number: "107848-V000037421",
    	                description: "GREENWOOD FM MATT - S011531058",
    	                productID: "V000037421"
    	            },
    	            {
    	                quantity: 1,
    	                serial_number: "107848-V000037421",
    	                description: "GREENWOOD FM MATT - S011531058",
    	                productID: "V000037421"
    	            },
    	            {
    	                quantity: 1,
    	                serial_number: "107848-V000037421",
    	                description: "GREENWOOD FM MATT - S011531058",
    	                productID: "V000037421"
    	            },
    	            {
    	                quantity: 3,
    	                serial_number: "112297-V000061518",
    	                description: "2015 BRWC BOX - S011531058",
    	                productID: "V000061518"
    	            },
    	        ],
    	        longitude: -82.74297,
    	        order_number: "Test3",
    	        scheduled_date: "Jan 15, 2018",
    	        scheduled_time_window: "07:15 PM - 09:15 PM EST",
    	        state: "FL",
    	        status: "Finished",
    	        stop: "7",
    	        zip: "33763",
    	    },
    	};
    }
};

module.exports = deliverytracking;