'use strict';

exports.init = function () {
	/*email signup Ajax*/
	$('#gen-email-alert-signup').on('submit', function (e) {
		e.preventDefault();
		var $this = $(this),
			emailSignupInput = $('#sub-email-address'),
			validationErrorBlock = $('#sub-email-address-error');
		var signupInputValue = $(emailSignupInput).val();		
		var validatetest = validateSubEmail(signupInputValue);
		if(!emailSignupInput.val()) {
			if(!$("#sub-email-address").hasClass('error')){
				$("#sub-email-address").addClass('error');
			}
			return;
		}
		if (validatetest && signupInputValue.length > 1 ) {		
			$('#deal-button').attr("disabled","true");
			var _zipCode = $('#footer-social-zipCode').val();
			var _siteId = $('#footer-social-siteId').val();
			var _leadSource = $('#lead-source').val();
			var _optOutFlag = $('#footer-social-optOutFlag').val();
			var _signupVersion = $('#signup-version').val();
			var _successAsset = $('#success-asset').val();
			var _emailErrorAsset = $('#email-error-asset').val();

			if ($.cookie) {
				var _gclid = $.cookie('_ga');
				console.log(_gclid);
			}
			var params = {emailAddress: signupInputValue, zipCode: _zipCode, leadSource: _leadSource, siteId: _siteId, optOutFlag: _optOutFlag, gclid: _gclid, signupVersion: _signupVersion, successAsset: _successAsset, emailErrorAsset: _emailErrorAsset};

			$.ajax({
				url: Urls.sfEmailSubscription,
				data: params,
				type: 'post',
				success: function (data) {
					if($('#lead-source').val().toLowerCase() == 'bf2019') {
						if ($('.bf-version-three').length > 0) {
							$('.bf-version-three-content').hide();
							$('.bf-version-three-thankyou').show();
						} else {
							$('.bf-deals-signup-wrap').hide();
							$('.bf-thanks-signup-wrap').show();
						}
					} else {
						$this.hide().parent().append($(data));
						$('#deal-button').removeAttr("disabled");
					}
				},
				error: function (errorThrown) {
					console.log(errorThrown);
				}
			});
		}
	});
	
	/*if($("#sub-email-address").hasClass('error')){
		$("#sub-email-address").removeClass('error');
	}*/
};

$('#gen-email-alert-signup').validate({
	focusInvalid: false,
	errorPlacement: function(error, element) {
		element.attr("placeholder", Resources.VALIDATE_EMAIL_REQUIRED);
	}
});

//Submit Sign up form on press enter 
$('#sub-email-address').keyup(function (e) {
	  if (e.which == '13') {
	    $('#gen-email-alert-signup').submit();
	    return false;    
	  } 
});

function validateSubEmail(email) {
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	if (!emailReg.test(email)) {			
		if(!$("#sub-email-address").hasClass('error')){
			$("#sub-email-address").addClass('error');
		}	
		$("#sub-email-address").val("");
		$("#sub-email-address").attr("placeholder", Resources.VALIDATE_EMAIL);
		$("#sub-email-address-error").css('display','block');
		return false;
	} else {
		if($("#sub-email-address").hasClass('error')){
			$("#sub-email-address").removeClass('error');
		}
		return true;
	}
}