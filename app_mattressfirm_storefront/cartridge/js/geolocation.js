'use strict';
var util = require('./util');
var userCurrentGeoLocation = require("./userCurrentGeoLocation");
exports.init = function() {
	if(SitePreferences.isGeoLocationSearchEnabled) {
		//get brand content asset delivery
		if($('.geo-content-selector').length>0) {
			$.ajax({
				url: Urls.getDeliveryLocationShopByBrand,
				type: 'get',									
				success: function (data) {           
				   if(data){			   
					   $('.geo-content-selector').html(data);
				   }           
				},
				error: function (error) {
					var msg = error;			
				}		
		     });
		}
		//get home content asset delivery
		if($('.geo-home-selector').length>0) {
			$.ajax({
				url: Urls.getDeliveryLocationHome,
				type: 'get',									
				success: function (data) {           
				   if(data){			   
					   $('.geo-home-selector').html(data);
				   }           
				},
				error: function (error) {
					var msg = error;			
				}		
		     });
		}
		if($('#geo-mark-selector').length>0) {
			$.ajax({
				url: Urls.GetDeliveryLocationMarketLanding,
				type: 'get',									
				success: function (data) {           
				   if(data){			   
					   $('#geo-mark-selector').html(data);
				   }           
				},
				error: function (error) {
					var msg = error;			
				}		
		     });
		}
	}else{
		$('.geo-content-selector').css('display','none');
	}
   
	//brand content asset zip change
	$(document).on('click','.btn-change-zip-asset', function (e) {
		e.preventDefault();
		$(".delivery-shipping-asset").hide();
		$(".previous-location-asset").css('display','flex');
	});
	// zipcode selection on focus
	$(document).on('focus','#home-customer-zip', function (e) {	
		$(this).select();
		this.setSelectionRange(0,5);
	});
	$(document).on('focus','#store-customer-zip', function (e) {	
		$(this).select();
		this.setSelectionRange(0,5);
	});
	$("#store-customer-zip").on('keydown', function (e) {
	   e.stopPropagation();
	   if (e.which === 13) {
			e.preventDefault();
			$(".btn-update-store").click();
		}
	});
	$(document).on('focus','#store-customer-zip-mobile', function (e) {	
		$(this).select();
		this.setSelectionRange(0,5);
		$(window).scrollTop(0);
	});
	$(document).on('focus','#header-customer-zip-delivery', function (e) {
		$(this).select();
		this.setSelectionRange(0,5);
	});
	$(document).on('focus','#asset-customer-zip', function (e) {
		$(this).select();
		this.setSelectionRange(0,5);
	});
	$(document).on('focus','#plp-customer-zip', function (e) {
		$(this).select();
		this.setSelectionRange(0,5);
	});
	$(document).on('focus','#header-customer-zip-delivery-mobile', function (e) {
		$(this).select();
		this.setSelectionRange(0,5);
	});
	$(document).on('focus','#plp-customer-zip-mobile', function (e) {
		$(this).select();
		this.setSelectionRange(0,5);
	});
	//close buttons on tooltips
	$(document).on('click','.geo-close-btn', function (e) {
		e.preventDefault();
		$('.store-details.header-tooltip ul').hide();
	});
	$(document).on('touchstart','.geo-close-btn', function (e) {
		e.preventDefault();
		$('.store-details.header-tooltip ul').hide();
	});
	
	$(document).on('click','.geo-shipping-close-btn', function () {
		$('.shipping-details.header-tooltip ul').hide();
	});
	$(document).on('touchstart','.geo-shipping-close-btn', function (e) {
		e.preventDefault();
		$('.shipping-details.header-tooltip ul').hide();
	});
	$(document).on('click','.store-details .btn.primary', function () {
		$('.store-details.header-tooltip ul').show();
	});
	$(document).on('click','.shipping-details .btn.primary', function () {
		$('.header-tooltip ul').show();
	});
	
	$(document).on('touchstart','.mobile-find-store .store-details .btn', function (e) {
		e.preventDefault();
		if(utag) {
			utag.link({
				"eventCategory" : ["Mobile Find a Store"],
				"eventLabel" : ["Mobile Find a Store Clicked"],
				"eventAction" : ["click"]
			});
		}
	});
	
	//image hover
	$( ".deliver-ship" ).hover(function() {
	  $( ".deliver-ship__icon-active" ).css("display" , "none");
	  $( ".deliver-ship__icon" ).css("display" , "block");
	}, function(){
		$( ".deliver-ship__icon-active" ).css("display" , "block");
		$( ".deliver-ship__icon" ).css("display" , "none");
	});
	
	$( ".find-store" ).hover(function() {
		  $( ".find-store__icon" ).css("display" , "none");
		  $( ".find-store__icon-active" ).css("display" , "block");
	}, function(){
		$( ".find-store__icon" ).css("display" , "block");
		$( ".find-store__icon-active" ).css("display" , "none");
		});
	//home content asset zip change
	$(document).on('click','.btn-change-zip-home', function (e) {
		e.preventDefault();
		$(".delivery-shipping-home").hide();
		$(".previous-location-home").show();
	});
	
	//zip change pdp
	$(document).on('click','.btn-change-zip-pdp', function (e) {
		e.preventDefault();
		$(".delivery-shipping-pdp").hide();
		$(".previous-location-pdp").show();
		$(".in-stock-msg").hide();
		$("#pdp-customer-zip").select();
		$(".details-link").hide();
		$(".availability-web").hide();
	});
	//try in store pdp
	$(document).on('click','.storename-wrap', function (e) {
		e.preventDefault();
		var pid = $(this).closest('#try-in-store').length > 0 ? $(this).closest('#try-in-store').data('pid') : null;
		if(pid != null) {
			if($('#store-info-'+pid).length >0 ){
				$('#store-info-'+pid).toggle();
			}
			if($('#change-tryinstore-'+pid).length >0 ){
				$('#change-tryinstore-'+pid).toggle();
			}
			if($('#regular-'+pid).length >0 ){
				$('#regular-'+pid).toggle();
			}
			if($('#fa-angle-up-'+pid).length >0 ){
				$('#fa-angle-up-'+pid).toggleClass('flip');
			}
		} 
		else {
			$(".store-info").toggle();
			$(".change-tryinstore").toggle();
			$("a.regular").toggle();
			$("i.fa-angle-up").toggleClass('flip');
		}
	});
	
	//brand content asset zip update
	$(document).on('click','.btn-update-asset', function (e) {
		e.preventDefault();
		var zipCodeVal = $('#asset-customer-zip').val();
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) { 
			var url = Urls.setCustomerZipCode;	
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location-asset .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location-asset .btn-update-asset");
	        }
		}	
	});
	
	//home content asset zip update
	$(document).on('click','.btn-update-home', function (e) {
		e.preventDefault();
		var zipCodeVal = $('#home-customer-zip').val();	
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) { 
			var url = Urls.setCustomerZipCode;	
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location-home .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location-home .btn-update-home");
	        }
		}	
	});	
	
	//pdp zip update
	$(document).on('click','.btn-update-pdp', function (e) {
		e.preventDefault();
		var zipCodeVal = $('#pdp-customer-zip').val();	
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) { 
			var url = Urls.setCustomerZipCode;	
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location-pdp .error-message").length < 1 && zipCodeVal.length != 0) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location-pdp .btn-update-pdp");
	            $("#pdp-customer-zip").removeAttr('placeholder');
	        }
	    	else{
	    		$("#pdp-customer-zip").on('keydown', function (e) {
	    			if ($(".previous-location-pdp .error-message").length > 0 && zipCodeVal.length == 0) {
			            $(".error-message").remove();
			        }
				});
	    	}
		}	
	});	
	
	//plp desktop zip update	
	$(document).on('click','.btn-update-plp', function (e) {	
		e.preventDefault();
		var zipCodeVal = $('#plp-customer-zip').val();
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) { 
			var url = Urls.setCustomerZipCode;	
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location .btn-update-plp");
	        }
		}		
	});
	
	
	//plp desktop zip change
	$(document).on('click','.btn-change-zip-plp', function (e) {	
		e.preventDefault();
		$(".delivery-shipping").hide();
		$(".geo-main-wrapper.desktop-plp").hide();
		$(".previous-location").slideDown(300);
	});
	
	//plp mobile zip update
	$(document).on('click','.btn-update-plp-mobile', function (e) {	
		e.preventDefault();
		var zipCodeVal = $('#plp-customer-zip-mobile').val();
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) {
			var url = Urls.setCustomerZipCode;	
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location .btn-update-plp-mobile");
	        }
		}
	});
	
	//plp mobile zip change
	$(document).on('click','.btn-change-zip-plp-mobile', function (e) {	
		e.preventDefault();
		$(".delivery-shipping-mobile").hide();
		$(".geo-main-wrapper.desktop-plp").hide();
		$(".previous-location-mobile").show(100);
	});
	
	//header store zip change
	$(document).on('click','.btn-change-zip-header-store', function (e) {
		e.preventDefault();
		$(".store-delivery-shipping").hide();
		$(".store-previous-location").show(100);
	});
	
	//header store desktop zip update
	$(document).on('click','.btn-update-store', function (e) {	
		e.preventDefault();
		var zipCodeVal = $('#store-customer-zip').val();
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) {
			var url = Urls.setCustomerZipCode;	
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location .btn-update-store");
	        }
		}
	});
	//MAT-1770 - Marketing page - Slide up and down Zip code change portion
	$(document).on('click','.change-location', function (e) {	
		//$('.location-address-text').slideUp(100);
		//$('.location-form-wrap').slideDown(500);
		$('.location-address-text').fadeOut(0);
		$('.location-form-wrap').fadeIn("slow");
	});
		
	$(document).on('click','.location-button', function (e) {	
		e.preventDefault();
		var zipCodeVal = $('#mark-customer-zip').val();
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) {
			var url = Urls.setCustomerZipCode;	
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
			//$('.location-form-wrap').slideUp(500);	
			//$('.location-address-text').slideDown(100);
			$('.location-form-wrap').fadeOut(0);
			$('.location-address-text').fadeIn("slow");
		}else{
			//display error for zip
	    	if ($(".location-form-wrap .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".location-form-wrap .location-button");
	        }
		}
	});
	
	//header store mobile zip change
	$(document).on('click','.btn-change-zip-header-store-mobile', function (e) {
		e.preventDefault();
		$(".store-mobile-delivery-shipping").hide();
		$(".store-mobile-previous-location").show(100);
	});
	
	//header store mobile zip update
	$(document).on('click','.btn-update-store-mobile', function (e) {	
		e.preventDefault();
		var zipCodeVal = $('#store-customer-zip-mobile').val();	
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) {
			var url = Urls.setCustomerZipCode;	
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".store-mobile-previous-location .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".store-mobile-previous-location .btn-update-store-mobile");
	        }
		}
	});	
	
	//header delivery desktop zip update
	$(document).on('click','.btn-update-header', function (e) {	
		e.preventDefault();
		var zipCodeVal = $('#header-customer-zip-delivery').val();	
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) { 
			var url = Urls.setCustomerZipCode;	
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location .btn-update-header");
	        }
		}
		
	});
	
	//header delivery mobile zip update
	$(document).on('click','.btn-update-header-mobile', function (e) {	
		e.preventDefault();
		var zipCodeVal = $('#header-customer-zip-delivery-mobile').val();	
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) { 
			var url = Urls.setCustomerZipCode;	
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location .btn-update-header-mobile");
	        }
		}
		
	});
	
	//user current location 
	$(document).on('click touchstart','.current-location-selector', function (e) {	
        e.preventDefault();
        var zipCodeVal = userCurrentGeoLocation.init();        
    });	
	
	//cookies locations 
	$(document).on('click','.last-visited-zip-selector', function (e) {	
		e.preventDefault();
		var zipCodeVal = $(this).attr('data-zipcode');		
		var url = Urls.setCustomerZipCode;	
		var data = {zipCode: zipCodeVal};
		updateCustomerZipCode(url,data);
		
	});    
};

function updateCustomerZipCode (url,data) {
	$.ajax({
		url: url,
		type: 'get',
		data: data,							
		success: function (data) {           
		   if(data){			   
			if (window.location.search.search('paypaloom') > 0 || window.location.search.search('restrictpaypal') > 0) {
				window.location.href = util.appendParamsToUrl(Urls.redirectToCart);
			   } else {
				   window.location.reload()
			   }
		   }           
		},
		error: function (error) {
			var msg = error;
		}
	});
}

