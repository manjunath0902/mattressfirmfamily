'use strict';
/**
@class app.setSFEmailDialogHandler
*/

var dialog = require('./dialog'),
util = require('./util');

var setSFEmailDialogHandler = function (timeout,cookieExpOveride) {

	var dheight = parseInt(SFEmailVars.sfEmailModalHeight);
	var dwidth = parseInt(SFEmailVars.sfEmailModalWidth);
	var durl = SFEmailVars.sfEmailModalUrl;
	var wait = timeout ? timeout : parseInt(SFEmailVars.sfEmailModalWait * 1000);
	var cookieExpiration = cookieExpOveride ? cookieExpOveride : parseInt(SFEmailVars.sfEmailModalCookieExpiration);
	var siteId = SFEmailVars.sfEmailModalSiteId;
	var cookieName = siteId + '_email_sub';

	var customerAuthenticated = SFEmailVars.sfEmailModalCustomerAuthenticated == true;
	var customerSubscribed = SFEmailVars.sfEmailModalCustomerSubscribed == true;
	var callFromEmail = SFEmailVars.sfEmailModalCallFromEmail == true;
			
	if (customerSubscribed || callFromEmail) {
		util.setCookie(cookieName, '1', 365);
	} else if (util.getCookie(cookieName) == "" && !customerAuthenticated && (!customerAuthenticated || !customerSubscribed)) {
		setTimeout(function () {
			dialog.open({
				url: durl,
				options: {
					height: dheight,
					width: dwidth,
					title: null,
					open: function () {
						var dialog = this;
						$(dialog).dialog( "option", "classes.ui-dialog", "email-modal-dialog" );
						$('.ui-widget-overlay').on('click', function () {
							$(dialog).dialog('close');
						});

						$('#sf-email-subscription-form').on('submit', function (e) {
							e.preventDefault();
							var _email = $('#email').val();
							var _leadSource = $('#leadSource').val();
							var _leadCreative = $('#leadCreative').val();
							var _leadPage = $('#leadPage').val();
							var _zipCode = $('#zipCode').val();
							var _zipCodeModal = $('#zip').val();
							var _fname = $('#name').val();
							var _siteId = $('#siteId').val();
							var _optOutFlag = $('#optOutFlag').val();
							var _gclid = util.getCookie("_ga");

							if (/^[a-zA-Z0-9]+(?:(\.|_)[A-Za-z0-9!#$%&'*+/=?^`{|}~-]+)*@(?!([a-zA-Z0-9]*\.[a-zA-Z0-9]*\.[a-zA-Z0-9]*\.))(?:[A-Za-z0-9](?:[a-zA-Z0-9-]*[A-Za-z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/.test(_email)) {
								var params = {firstName: _fname, emailAddress: _email, zipCode: _zipCode, zipCodeModal: _zipCodeModal, leadSource: _leadSource, leadCreative: _leadCreative, leadPage: _leadPage, siteId: _siteId, optOutFlag: _optOutFlag, gclid: _gclid};
								$("body").attr("style", "cursor:wait;");
								$.ajax({
									url: Urls.sfEmailSubscription,
									data: params,
									type: 'post',
									success: function (data) {
										$('#sf-email-subscription-content').html(data);
										$('#sf-start-shopping-button').show();
										$("body").attr("style", "cursor:default;");
									},
									error: function (request, status, error) {
										$("body").attr("style", "cursor:default;");
									}
								});
							} else {
								$('#sf-email-container').find('.error').remove().end().append(
									$('<div/>', {"class":"error"}).html('Please enter a valid email address')
								);
							}
						});
						$('#sf-start-shopping-button').on('click', function (e) {
							e.preventDefault();
							$(dialog).dialog('close');
						});
					}
				}
			})
		}, wait);
		util.setCookie(cookieName, '1', cookieExpiration);
	}

};

exports.init = function (timeout) {
	if (SFEmailVars.sfEmailModalEnabled && (($('#sf-email-footer-include-div').length > 0) || SFEmailVars.sfEmailModalPageEnabled || SFEmailVars.sfEmailModalCallFromSourceCode)) {
		setSFEmailDialogHandler(timeout);
	}
	$('.email-modal-trigger').on('click', function () {
		setSFEmailDialogHandler(1);
	});
	window.emailModalInit = function (wait,cookieExpOveride) {
		setSFEmailDialogHandler(wait,cookieExpOveride);
	}
};