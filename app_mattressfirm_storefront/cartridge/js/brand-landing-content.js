'use strict';

// load the purple brand page accessories section images and copy
function writePurplePorductsFeatureFromAsset() {
	var purpleFeatureArray = new Array();
	$('.purple-feature-from-asset').each(function(i, obj){
		purpleFeatureArray.push($(obj).html());
	});
	
	$('.purple-product-feature').each(function(i, obj) {
	    $(obj).html(purpleFeatureArray[i]);
	});
}

//load the serta brand page product carousel section promo call out
function writePromoCallOutFromAsset() {
	var promoCallOutArray = new Array();
	$('.promo-call-out-from-asset').each(function(y, objCallOut){
		promoCallOutArray.push($(objCallOut).html());
	});
	
	$('.promo-call-out').each(function(y, objCallOut) {
	    $(objCallOut).html(promoCallOutArray[y]);
	});
}

//load the tempur pedic brand page product carousel section cool icons
function writeTempurPedicIconFromAsset() {
	var tempurPedicIconArray = new Array();
	$('.tempur-pedic-icon-from-asset').each(function(index, objTempurPedicIcon){
		tempurPedicIconArray.push($(objTempurPedicIcon).html());
	});
	
	$('.tempur-pedic-icon').each(function(index, objTempurPedicIcon) {
	    $(objTempurPedicIcon).html(tempurPedicIconArray[index]);
	});
}

//load the foster brand page product carousel section promo call out
function writeFosterIconFromAsset() {
	var fosterIconArray = new Array();
	$('.foster-icon-from-asset').each(function(index, objFosterIcon){
		fosterIconArray.push($(objFosterIcon).html());
	});
	
	$('.foster-icon').each(function(index, objFosterIcon) {
	    $(objFosterIcon).html(fosterIconArray[index]);
	});
}

exports.init = function() {	
	// load the purple brand page accessories section images and copy
	if ($('.purple-feature-from-asset').length > 0) {
		writePurplePorductsFeatureFromAsset();
	}
	
	//load the serta brand page product carousel section promo call out
	if ($('.promo-call-out-from-asset').length > 0) {
		writePromoCallOutFromAsset();
	}
	
	//load the tempur pedic brand page product carousel section cool icons
	if ($('.tempur-pedic-icon-from-asset').length > 0) {
		writeTempurPedicIconFromAsset();
	}
	
	//load the foster brand page product carousel section promo call out
	if ($('.foster-icon-from-asset').length > 0) {
		writeFosterIconFromAsset();
	}
};