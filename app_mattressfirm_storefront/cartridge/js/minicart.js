'use strict';

var util = require('./util'),
bonusProductsView = require('./bonus-products-view'),
TPromise = require('promise');

var timer = {
	id: null,
	clear: function () {
		if (this.id) {
			window.clearTimeout(this.id);
			delete this.id;
		}
	},
	start: function (duration, callback) {
		this.id = setTimeout(callback, duration);
	}
};

var addItemToCart = function (form) {
    var $form = $(form),
        $qty = $form.find('input[name="Quantity"]');
    if ($qty.length === 0 || isNaN($qty.val()) || parseInt($qty.val(), 10) === 0) {
        $qty.val('1');
    }
    var optionName ="";
	var optionId ="";
	var optionPrice="";
	if($form.find( ':input[name*="_boxSpring"]' ).length > 0)
	{
		 optionName = $form.find( ':input[name*="_boxSpring"] option:selected' ).data('option-product-name');
		if(optionName != "none"){
			 optionId = $form.find( ':input[name*="_boxSpring"] option:selected' ).val();
			 optionPrice = $form.find( ':input[name*="_boxSpring"] option:selected' ).data('option-price');
		}
		else{
			 optionName = "";
			}
		if(optionName != "none" && optionId !="" && optionPrice !=""){
			if(typeof utag != undefined && utag != null ){ 
				utag.link({ 
				    "enhanced_action": "add",
				    "option_product_name": optionName.split(),
				    "option_product_value_id": optionId.split(),
				    "option_product_price": optionPrice.split(),
				    "as_product_quantity": $qty.val().length > 0 ? $qty.val().split() : [""]
				});
			}
		}
	}
    var url = util.ajaxUrl(Urls.addProduct) + '&AddtoCart_Redesign=v1';
    return TPromise.resolve($.ajax({
        type: 'POST',
        url: url,
        data: $form.serialize()
    }));
};

var minicart = {
	init: function () {
		this.$el = $('#mini-cart');
		this.$elATC = $("#mini-cart-ATC");
		this.$content = this.$el.find('.mini-cart-content');
		this.$minicartlink = this.$el.find('.mini-cart-link');
		// events
		this.$el.find('.mini-cart-total').on('mouseenter touchstart focusin', function () {
			if (this.$content.not(':visible')) {
				//this.slide();
				var screenwidth = $(window).width();
				this.slide();
				//if (screenwidth > 767) {
				//	this.slide();
				//} else {
				//	this.$content.hide();
				//}
			}
		}.bind(this));
		this.$el.find('.mini-cart-link').on('click ', function (e) {
			if ($(this).hasClass('mini-cart-empty')) {
				e.preventDefault();
				this.close();
				console.log('log');
			}

			if (util.isMobile() && (!$(this).hasClass('mobile-active'))) {
				e.preventDefault();
				$(this).addClass('mobile-active');
			}
		});
		this.$content.on('mouseenter touchstart focusin', function () {
			timer.clear();
		}).on('mouseleave touchend focusout', function () {
			//timer.clear();
			timer.start(30, this.close.bind(this));
		}.bind(this));
		$('.mini-cart-products').mCustomScrollbar();
		
	},
	/**
	 * @function
	 * @description Shows the given content in the mini cart
	 * @param {String} A HTML string with the content which will be shown
	 */
	show: function (html) {
		this.$el.html(html);
		this.init();
		this.$minicartlink.addClass("addline");
		this.$minicartlink.removeClass("addnoline");
	},
	/**
	 * @function
	 * @description Render ATC view on Add Box Spring
	 */
	addBoxSpring: function (e) {
	    e.preventDefault();
	    var $form = $(this).closest('form');
	    addItemToCart($form).then(function (response) {
	    	$("#mini-cart-ATC").html(response);
	    	minicart.init();
			minicart.initATCBindings();
			var optionValue = $('.pdpForm-ATC #optionValue').val();
			if(optionValue.length > 0 && optionValue != 'none') {
				$('#itemAddedtoCart').show();
				setTimeout(function() {  
					$('#itemAddedtoCart').slideUp('slow');
			    }, 2500); // run for 2.5 seconds only
				$("#add-to-cart-ATC").text("ADDED TO CART");
				$('#add-to-cart-ATC').prop('title', 'Added to cart');
			}
	    });
	},
	/**
	 * @function
	 * @description Closes the ATC Panel
	 */
	closeATCPanel: function (e) {
		e.preventDefault();
		$('html').removeClass('mini-cart-active');
	    $('body').removeClass('mini-cart-active');
	    $('html').css("overflow", "visible");
	},
	/**
	 * @function
	 * @description Bind Remove Foundation event  
	 */
	removeFoundation: function(e) {
		e.preventDefault();
		$('.pdpForm-ATC #optionValue').val('none');
		$('#pdpATC .product-options select').val('none');
		$('#add-to-cart-ATC').click();
	},
	
	initAccordions: function(){
		if($("#pdpATC").attr("data-mattressadded").length > 0){
			var isMattressProductAdded =  $("#pdpATC").attr("data-mattressadded");
			if(isMattressProductAdded == "false"){
				var openMattressAccordion = 0;
			} else {
				var openMattressAccordion = false;
			}
		}
		$( "#mattressaccordion" ).accordion({
	        collapsible: true,
	        active: openMattressAccordion,
	        autoHeight:false
	        
	    });
		
		if($("#pdpATC").attr("data-foundationadded").length > 0){
			var isFoundationProductAdded =  $("#pdpATC").attr("data-foundationadded");
			var checkBoxSpringAdded = $("#pdpATC").attr("data-boxspingavailableandadded").length > 0 ? $("#pdpATC").attr("data-boxspingavailableandadded") : "false";
			var isLastAddedMattress = $("#pdpATC").attr("data-isLastAddedMattress").length > 0 ? $("#pdpATC").attr("data-isLastAddedMattress") : "false";
			if(isFoundationProductAdded == "false" || (isLastAddedMattress == "true" && checkBoxSpringAdded == "false")){
				var openFoundationAccordion = 0;
			}
			else {
				var openFoundationAccordion = false;
			}
		}
		$("#foundationaccordion").accordion({
	        collapsible: true,
	        active: openFoundationAccordion,
	        autoHeight:false
	        
	    });
		
		if($("#pdpATC").attr("data-accessoryadded").length > 0){
			var isAcessoryProductAdded = $("#pdpATC").attr("data-accessoryadded");
			if(isAcessoryProductAdded == "false"){
				var openAccessoryAccordion = 0;
			} else {
				var openAccessoryAccordion = false;
			}
		}
		$("#accessoryaccordion").accordion({
	        collapsible: true,
	        active: openAccessoryAccordion,
	        autoHeight:false
	        
	    });
		
		if(isLastAddedMattress == "true" && checkBoxSpringAdded == "false"){
			var openBoxSpringAccordion = 0;
		} else {
			var openBoxSpringAccordion = false;
		}
			
        $( "#accordion2" ).accordion({
	        collapsible: true,
	        active: openBoxSpringAccordion,
	        autoHeight:false 
	    });
        
        if(isMattressProductAdded == "false" && isFoundationProductAdded == "false" && isAcessoryProductAdded == "true") {
        	$(".build-your-bed").addClass("add-accessory-case");
        }
	},
	
	/**
	 * @function
	 * @description Bind the ATC panel contents and events
	 */
	initATCBindings: function(e) {
		$("body").addClass("mini-cart-active");
		$("html").addClass("mini-cart-active");		
		$("html").addClass("scroll-overflow");
		$("body").addClass("scroll-overflow");
		
		util.uniform();
		var height1 = $(".justaddedproduct").outerHeight();
		var height2 = $(".free-bonus").outerHeight();
		var totalHeight = height1 + height2 + 10;
		
		$('.build-your-bed').css("height","calc(100vh - " + totalHeight + "px)");
		$('.add-to-cart-intercept-wrapper').hide(); //temporary added this code have to remove after proper fix
		
		minicart.initAccordions();
		
		if($("#pdpATC").attr("data-optionvalue").length > 0){
			var addedOptionValue =  $("#pdpATC").attr("data-optionvalue");
		}
		 
        $('#pdpATC').on('change', '.product-options select', function () {
            var newOptionValue = $('#pdpATC').find('.product-options .product-option').first().val();
            if(newOptionValue.length > 0) {
                $('.pdpForm-ATC #optionValue').val(newOptionValue);
                if(newOptionValue != addedOptionValue) {
                	$("#add-to-cart-ATC").text("ADD TO CART");
                } else if(newOptionValue != 'none' && newOptionValue == addedOptionValue) {
                	$("#add-to-cart-ATC").text("ADDED TO CART");
                	$('#add-to-cart-ATC').prop('title', 'Added to cart');
                }
            }
        });
        $('#pdpATC').on('click', '#add-to-cart-ATC', this.addBoxSpring);
        $('#pdpATC').on('click', '.continue-shopping', this.closeATCPanel);
        $('#pdpATC').on('click', '#removefoundation', this.removeFoundation);
        $('#pdpATC').on('click', '.continue-shopping',function(event){
        	$('html').removeClass('scroll-overflow');
            $('body').removeClass('scroll-overflow');
        });
        
        //Maintain state of Option drop down 
        var optionvalue = $(".pdpForm-ATC #optionValue").val();
        $('#pdpATC .product-options select').val(optionvalue);   
        
        this.$elATC.find('a.remove').on('click', function (e) {
			if($(this)[0].id !=null && $(this)[0].id === 'removefoundation') {
				var selectedFoundation =$('#pdpATC .product-options select option:selected')
				if(selectedFoundation.length > 0 && typeof utag != undefined && utag != null ){ 
					utag.link({ 
					    "enhanced_action": "remove",
					    "option_product_name": selectedFoundation.data('option-product-name') != null ? selectedFoundation.data('option-product-name').split() : [""],
					    "option_product_value_id": selectedFoundation.val() !=null ? selectedFoundation.val().split() : [""],
					    "option_product_price": selectedFoundation.data('optionPrice') != null ? selectedFoundation.data('optionPrice').split() : [""],
					    "product_quantity": ["1"]
					});
				}
			}
			else {
				var product =$('#pdpATC').data('lineitemjson')				
				if(product != null){					
					var productImageURL = product.productexternalid != 0 ? 'https://' + SitePreferences.amplienceHost +'/i/hmk/'+ product.productexternalid+'?h=1220&w=1350' : '';
					var productURL = util.appendParamsToUrl(Urls.getHttpProductUrl, {"pid": product.variantid});
					var product_price = (parseFloat(product.price) * parseInt(product.quantity)).toFixed(2).toString().split();
					if(typeof utag != undefined && utag != null ){ 
						utag.link({ 
							"enhanced_actions"		:	"remove", 
							"product_id"			:	[product.productid],
						    "product_name"			:	[product.productname],
						    "product_brand"			:	[product.brand],
						    "product_quantity"		:	[product.quantity.toString()],
						    "product_price"			:	product_price,
						    "product_unit_price"	:	product.price.split(),
						    "product_category"		:	[product.productcategory],
						    "product_sku"			:	[product.productsku],
						    "product_child_sku"		:	[product.productchildsku], 
						    "product_img_url"		:	[productImageURL],
						    "product_url"			:	[productURL],
						    "product_size"			:	[product.productsize]
						});
					}						
				}				
			}
		});
	},
	/**
	 * @function
	 * @description display the side panel and Bind the contents
	 */
	showATC: function (html) {
		this.$elATC.html(html);
		this.init();
		this.initATCBindings();
		$('#itemAddedtoCart').show();
		setTimeout(function() {  
			$('#itemAddedtoCart').slideUp('slow');
	    }, 2500); // <-- time in milliseconds
		//ATC Option product selection with PDP option product selection 
		var pdpOptionValue = $('#pdpMain .product-options select').children().filter(':selected').first().val();
		$('#pdpATC .product-options select').val(pdpOptionValue);
	},
	
	/**
	 * @function
	 * @description Slides down and show the contents of the mini cart
	 */
	slide: function () {
		//timer.clear();
		// show the item
		var screenwidth = $(window).width();
		if (screenwidth > 767) {
			this.$content.slideDown('slow');
		}
		if (this.$minicartlink.hasClass('mini-cart-empty')) {
			this.$minicartlink.addClass("addnoline");
			this.$minicartlink.removeClass("addline");
		} else {
			this.$minicartlink.addClass("addline");
			this.$minicartlink.removeClass("addnoline");
		}
		// after a time out automatically close it
		timer.start(6000, this.close.bind(this));
	},
	/**
	 * @function
	 * @description Closes the mini cart with given delay
	 * @param {Number} delay The delay in milliseconds
	 */
	close: function (delay) {		
		timer.clear();
		this.$content.slideUp(delay);
		this.$minicartlink.addClass("addnoline");
		this.$minicartlink.removeClass("addline");
		$('.mini-cart-link').removeClass('mobile-active');
	},
	/**
	 * @function
	 * @description Refresh the Mini Cart 	 
	 */
	refreshMiniCart: function() {
		$.get(Urls.refreshMinicart)
		.done(function(text){
			var $response = $(text);
			//update content in minicart
			minicart.show($response);		
			minicart.init();					
		});
	}
};

module.exports = minicart;
