(function ($) {
	var headerBanner = {
		promoContainer: $('.desktop-header-promo'),
		promos: $('.desktop-header-promo .html-slot-container > *'),

		init: function () {
			if ($(window).width() >= 1032 && headerBanner.promos.length > 1) {
				headerBanner.fade();
			}
		},

		fade: function () {
			var active = 'is-active',
				current = headerBanner.promos.eq(0);

			current.addClass(active);

			function fadingPromos() {
				var current = $('.desktop-header-promo .html-slot-container > *.is-active'),
					next = current.next(),
					timing = 500;

				if (next.length === 0) {
					next = headerBanner.promos.eq(0);
				}

				current.removeClass(active).fadeOut(timing);
				setTimeout(function () {
					next.addClass(active).fadeIn(timing);
				}, timing);
			}

			var fadePromos = setInterval(fadingPromos, 4500);

			headerBanner.promoContainer.mouseenter(function () {
				clearInterval(fadePromos);
      });
      
      headerBanner.promoContainer.mouseleave(function () {
        fadePromos = setInterval(fadingPromos, 4500);
      });
		}
	};

	$(document).ready(function () {
		headerBanner.init();
	});
})(jQuery);
