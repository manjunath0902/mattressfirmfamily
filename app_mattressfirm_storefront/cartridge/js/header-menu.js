(function ($) {
	var headerMenu = {
    all: $('.header-navigation > li, .header-submenu'),
    el: $('.header-navigation > li'),
    submenu: $('.header-submenu'),
    open: 'is-open',

		init: function () {
      if ($(window).width() >= 1032) {
        headerMenu.setupListeners();      
      }
		},

		setupListeners: function() {
      headerMenu.el.mouseenter(function() {
        $(this).addClass(headerMenu.open);
        $(this).find(headerMenu.submenu).show();
      });

      headerMenu.el.mouseleave(function() {
        $(this).removeClass(headerMenu.open);
        $(this).find(headerMenu.submenu).hide();
      });
		},
	};

	$(document).ready(function () {
		headerMenu.init();
	});
})(jQuery);