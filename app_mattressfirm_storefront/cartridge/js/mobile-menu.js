(function ($) {
  var mobileMenu = {
    elem: $('html, body, #main, .header-bottom'),
    btn: $('#header-mobile-toggle'),
    submenu: $('.header-submenu'),
    submenuItem: $('.submenu-title'),
    searchBtn: $('.header-search-button'),
    searchBar: $('.header-search'),
    open: 'is-open',

  init: function () {
      mobileMenu.setupListeners();
    },

    setupListeners: function() {
      if ($(window).width() <= 1031) {
        mobileMenu.btn.click(function() {
          $("header.header-main").toggleClass("menu-opener");
          $(this).toggleClass(mobileMenu.open);
          mobileMenu.elem.toggleClass(mobileMenu.open);
          if($(this).hasClass(mobileMenu.open)){
         	 $(this).attr('aria-expanded','true');
          }else{
         	 $(this).attr('aria-expanded','false');
          }
        });

        mobileMenu.menuToggle().click(function() {
          $(this).toggleClass(mobileMenu.open);
          $(this).find(mobileMenu.submenu).slideToggle();
        });

        mobileMenu.submenuItem.click(function(e) {
          e.stopPropagation();
          $(this).toggleClass(mobileMenu.open);
          $(this).next('ul').slideToggle();
        });
      }

      mobileMenu.searchBtn.click(function() {
        $(this).find('img').toggle();
        mobileMenu.searchBar.toggleClass(mobileMenu.open);
      });
    },

    menuToggle: function () {
      mobileMenu.submenu.parent().addClass('mobile-menu-expand');
      var expand = $('.mobile-menu-expand');
      return expand;
    }
  };

  $(document).ready(function () {
  mobileMenu.init();
  });
})(jQuery);
