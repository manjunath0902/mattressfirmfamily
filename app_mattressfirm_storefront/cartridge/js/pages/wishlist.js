'use strict';

var addProductToCart = require('./product/addToCart'),
	page = require('../page'),
	util = require('../util'),
	ajax = require('../ajax');
//add to cart button click on wishlist
 function wishlist() {
	$('.toright').click (function (e) {
			e.preventDefault();
			$(this).hide();
			var itemId = $(this).closest("tr").find('[name="itemid"]').val();
			var plId = $(this).closest("tr").find('[name="plid"]').val();
			var quantity = $(this).closest("tr").find("#Quantity").val();
			var addCartId = $(this).attr('id');
			var viewCartId = addCartId.substring(0, addCartId.indexOf("a"));
			$('#' + viewCartId + 'vc').show();
			$('#' + viewCartId + 'di').hide();
			$('#' + viewCartId + 'div').hide();
			$('#' + viewCartId + 'qty').show();
			var getQty = document.getElementsByClassName("." + viewCartId + "val");
			var qty_value = $("." + viewCartId + "vall").val();
			document.getElementById(viewCartId + 'qty').innerHTML = qty_value;
			var data = {productListID:plId,
					Quantity:quantity,
					itemid:itemId,
					plid:plId}
			$.ajax({
				url: Urls.addProduct,
				data: data,//$('#dwfrm_wishlistpage').serialize(),
				type: 'POST'
			})
			// success
			.done(function (response) {
				$("#mini-cart").load(location.href + " #mini-cart>*","");
		})
		.error(function (error) {
		});
	});
	$('.close_dialogue').click (function (e) {
		e.preventDefault();
		var cross = $(this).attr('id');
		var crossid = cross.substring(0, cross.indexOf("c"));
		$('#' + crossid + 'vc').hide();
		$('#' + crossid + 'ac').show();
		$('#' + crossid + 'di').show();
		$('#' + crossid + 'div').show();
		$('#' + crossid + 'qty').hide();
		var itemId = $(this).closest("tr").find('[name="itemid"]').val();
		var productID = $(this).closest("tr").find('[name="productId"]').val();
		var plId = $(this).closest("tr").find('[name="plid"]').val();
		var quantity = $(this).closest("tr").find("#Quantity").val();
		var data = {productListID:plId,
				Quantity:quantity,
				itemid:itemId,
				plid:plId,
				productId:productID}
		$.ajax({
			url: Urls.removeFromCart,
			data: data,//$('#dwfrm_wishlistpage').serialize(),
			type: 'POST'
		})
		// success
		.done(function (response) {
			$('#' + crossid + 'div').show();
			$('#' + crossid + 'qty').hide();
			$("#mini-cart").load(location.href + " #mini-cart>*","");
	})
	.error(function (error) {
	});
	});
 }
 function wishlist_dropdownchange() {
	$('.quantity-dropdown').on('change', function (e) {
			var qty = $(this).attr('class');
			var qty_id = qty.substring(0, qty.indexOf("v"));
			var qty_num = qty_id.substring(qty_id.length - 1, qty_id.length);
			var selected_qty = $(this).closest("div").find(":selected").html();
			$('.desired_qty').val(selected_qty);
			$('#' + qty_num + 'up').trigger('click');
	});
 }
exports.init = function () {
	wishlist_dropdownchange();
	wishlist();
	addProductToCart();
	$('#editAddress').on('change', function () {
		page.redirect(util.appendParamToURL(Urls.wishlistAddress, 'AddressID', $(this).val()));
	});

	//add js logic to remove the , from the qty feild to pass regex expression on client side
	$('.option-quantity-desired input').on('focusout', function () {
		$(this).val($(this).val().replace(',', ''));
	});
};
