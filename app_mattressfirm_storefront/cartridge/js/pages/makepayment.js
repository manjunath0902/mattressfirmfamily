'use strict';

exports.init = function () {
    $("#dwfrm_billingform_address1,#dwfrm_billingform_address2,#dwfrm_billingform_city,#dwfrm_billingform_fullname").each(function(){
    	$(this).keypress(function(event) {
    		var key = event.key;
    		var pattren = /^[\w\. \[\]!&()+,\-?=%@_{}#_{},#$ÀÁÂÄàáâäÈÉÊèéêëÌÍÎìíîïÒÓÔÖòóôöÙÚÛÜùúûüÇçÑñ¿¡]+$/;
    		return pattren.test(key);
        });
    });    
    var otherAmountControl=$('#dwfrm_billingform_PartialPayment_otherAmount');
	otherAmountControl.prop("readonly", true);	
	otherAmountControl.addClass('half-opacity');
	otherAmountControl.focusout(function() {
		if($("input[name=PaymentOptions]:checked").val()=='OtherAmountOption' && otherAmountControl.val()!='')
			{    				
			 	var OtherAmount = addCommas(otherAmountControl.val());
			 	otherAmountControl.val(OtherAmount);
				$('.order-amount').html('$'+OtherAmount);				
			}
	});
	
	$("input[name=PaymentOptions]").click(function() {    		
		updatePaymentOptionView();		
	});
	
	$(".balance").click(function() {				
		$('input:radio[name=PaymentOptions]')[0].checked=true;
		updatePaymentOptionView();
		validateBillingForm();
	});
	
	$(".other-amount").click(function() {				
		$('input:radio[name=PaymentOptions]')[1].checked=true;		
		updatePaymentOptionView();
	});
	
	function updatePaymentOptionView() {
		if($("input[name=PaymentOptions]:checked").val()=='OutstandingBalOption'){    				
			otherAmountControl.prop("readonly", true);			
			otherAmountControl.val('0.00');
			otherAmountControl.switchClass('full-opacity','half-opacity');
			otherAmountControl.removeClass('border-red');
			$('#amountRangMsg').text('');			
			$(".balance").addClass('border-red');			
			$(".other-amount").removeClass('border-red');
			$(".order-amount").html('$'+$("input[name=AmountDue]").val());
			$("#OutstandingBalanceOption").addClass("checked");
			$("#OutstandingBalOption").addClass("checked");
			$("#OtherAmountOption").removeClass("checked");
			$("#OtherAmountOpt").removeClass("checked");
		}
		else if($("input[name=PaymentOptions]:checked").val()=='OtherAmountOption'){
			otherAmountControl.prop("readonly", false);
			otherAmountControl.select();
			if (otherAmountControl.val() === '0.00'){
				otherAmountControl.val("");
			}
			otherAmountControl.switchClass('half-opacity','full-opacity');
			$('.order-amount').html('$'+otherAmountControl.val());	
			$(".other-amount").addClass('border-red');			
			$(".balance").removeClass('border-red');
			$("#OtherAmountOption").addClass("checked");
			$("#OtherAmountOpt").addClass("checked");
			$("#OutstandingBalOption").removeClass("checked");
			$("#OutstandingBalanceOption").removeClass("checked");
		}
		$('#amountReview').switchClass("hide-review-amount","show-review-amount");
	}
	
	function addCommas(num) {
	    var parts = num.toString().split(".");
	    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    return parts.join(".");
	}
	//only allow numeric in other amount field
	otherAmountControl.on("keypress keyup blur",function (event) {        
	     $(this).val($(this).val().replace(/[^0-9\.\,]/g,''));
	            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
	                event.preventDefault();
	            }
	        });
};
