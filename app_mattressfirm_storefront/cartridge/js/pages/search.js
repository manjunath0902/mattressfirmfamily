'use strict';

var compareWidget = require('../compare-widget'),
	productTile = require('../product-tile'),
	progress = require('../progress'),
	util = require('../util'),
	truncate = require('../curtail'),
	ResponsiveSlots = require('../responsiveslots/responsiveSlots');
var refinementsCollection = [];
var refinementDivPosition;
var _ = require('lodash');

// Only for non-mattress plp pages. Check to see if page is less than 1200px
// if so, add class and show styles for ab-plp-tiles-v3. Else, if page is 1200px
// or greater, remove the ab-plp-tiles-v3 class.
function getMobileStyleForNonMattressPlp() {
	$('.search-result-items').each(function (index, element) {
		var $self = $(this);
		if (!$self.hasClass('ab-plp-tiles-v3') && $(window).width() < 1200) {
			$self.addClass('ab-plp-tiles-v3');
		}
		else if (!$self.hasClass('mattress-category') && $self.hasClass('ab-plp-tiles-v3') && $(window).width() >= 1200) {
			$self.removeClass('ab-plp-tiles-v3');
		}
	});
}

function updateProductTile(){
	getMobileStyleForNonMattressPlp();
	// On debounced resize of window check if ab-plp-tiles-v3 class is needed
	var onWindowResize = function() {
		getMobileStyleForNonMattressPlp();
	};
	$(window).on('resize', _.debounce(onWindowResize, 200));

	// Hide btn-features in features-list
	$('.features-list').each(function (index, element) {
		if ($(this).find('.features-wrap').find('.feature').length < 1) {
			$(element).closest('.comfort-and-features').find('.btn-features').css('display', 'none');
		}
	});

	// Custom checkbox
	$('.compare-check').on('change', function () {
		if ($(this).is(":checked")) {
			$(this).closest('.product-compare').find('label').addClass('select-compare');
		} else {
			$(this).closest('.product-compare').find('label').removeClass('select-compare');
		}
	});



	// Changing stars position
	if (screen.width < 767) {
		$('.product-tile').each(function () {
			var $productTile = $(this);
			var $clprating = $productTile.find('.brand-and-rating .clp-product-reviews');
			if ($clprating.length) {
				$productTile.find('.product-name').after($clprating);
			}

			var $deliveryInfo = $productTile.find('.delivery-info');
			if ($deliveryInfo.length) {
				$productTile.find('.product-image').append($deliveryInfo);
			}
		});
	}
}

function infiniteScroll() {
	// getting the hidden div, which is the placeholder for the next page
	var loadingPlaceHolder = $('.infinite-scroll-placeholder[data-loading-state="unloaded"]');
	// get url hidden in DOM
	var gridUrl = loadingPlaceHolder.attr('data-grid-url');

	if (loadingPlaceHolder.length === 1 && util.elementInViewport(loadingPlaceHolder.get(0), 250)) {
		// Update Multiple Pricing in URL if we have Prices Selected
		if (SitePreferences.CURRENT_SITE === "Mattress-Firm" && $('.plp-filters-mvc').length) {
			var pmax = '', pmin = '';
			var selectedPriceRefinements = $('a.price-refinement').closest('ul').find('li.selected');
			if (gridUrl.indexOf('pmax')>-1 && gridUrl.indexOf('pmin')>-1) {
				gridUrl = util.removeParamFromURL(gridUrl, 'pmax');
				gridUrl = util.removeParamFromURL(gridUrl, 'pmin');
			}
			$.each(selectedPriceRefinements, function(index, value){
			    var priceValues = $(this).find('a').data();
			    pmin += (index == selectedPriceRefinements.length -1) ? Number(priceValues.valuefrom) : Number(priceValues.valuefrom) + '|';
			    pmax += (index == selectedPriceRefinements.length -1) ? Number(priceValues.valueto) : Number(priceValues.valueto) + '|';
			});
			if (pmin != '' && pmax != '') {
				pmin = pmin.split('|').sort(function(a, b){return a - b}).join('|');	
				pmax = pmax.split('|').sort(function(a, b){return b - a}).join('|');	
				gridUrl = util.appendParamsToUrl(gridUrl, {'pmax':pmax, 'pmin':pmin});
			}
		}
		// switch state to 'loading'
		// - switches state, so the above selector is only matching once
		// - shows loading indicator
		loadingPlaceHolder.attr('data-loading-state', 'loading');
		loadingPlaceHolder.addClass('infinite-scroll-loading');


		// named wrapper function, which can either be called, if cache is hit, or ajax repsonse is received
		var fillEndlessScrollChunk = function (html) {
			loadingPlaceHolder.removeClass('infinite-scroll-loading');
			loadingPlaceHolder.attr('data-loading-state', 'loaded');
			$('div.search-result-content').append(html);
			updateProductTile();
			getMobileStyleForNonMattressPlp();
		};

		// old condition for caching was `'sessionStorage' in window && sessionStorage["scroll-cache_" + gridUrl]`
		// it was removed to temporarily address RAP-2649
		if (false) {
			// if we hit the cache
			fillEndlessScrollChunk(sessionStorage['scroll-cache_' + gridUrl]);
		} else {
			// else do query via ajax
			$.ajax({
				type: 'GET',
				dataType: 'html',
				url: gridUrl,
				success: function (response) {
					// put response into cache
					try {
						sessionStorage['scroll-cache_' + gridUrl] = response;
					} catch (e) {
						// nothing to catch in case of out of memory of session storage
						// it will fall back to load via ajax
					}
					// update UI
					fillEndlessScrollChunk(response);
					//productTile.init();
					productTile.initCompare();
					productTile.initComfortLevelTagging();
					productTile.initTileEvents();
				}
			});
		}
	}
}
/**
 * @private
 * @function
 * @description replaces breadcrumbs, lefthand nav and product listing with ajax and puts a loading indicator over the product listing
 */
function updateProductListing(url, isStopEventTriggered, isMobileMVC) {
	isMobileMVC = ($('.plp-filters-mvc').length && isMobileMVC != "undefined" && isMobileMVC === true);
	if ((!url || url === window.location.href) && !isMobileMVC) {
		return;
	}

	progress.show($('.search-result-content'));
	($('.plp-filters-mvc').length && util.isMobileSize()) ? $('.search-result-content .loader').css({'z-index':'99999'}) : null;
	var listView = '';
	if ($('.search-result-content').hasClass('wide-tiles')) {
		var listView = true;
	}

	var filterOpen = false;
	if ($('#secondary.refinements').find('#nav_label').prop('checked')) {
		filterOpen = true;
	}
	
	var currentURL = window.location.href;
	
	//store all classes
	refinementsCollection = [];
	$(".refinement-big-container .refinement").each(function (index) {		   
		   var refinementObj = {};
		   refinementObj.divId = $(this).attr('id');
		   refinementObj.existingClass = $(this).find('h3').attr('class');
		   refinementObj.displayBlock = $(this).find('ul').css('display');
		   refinementsCollection.push(refinementObj);
		});
	refinementDivPosition = $(".refinements").scrollTop();
	// In Case of Mobile MVC PLP Update only Refinement Panel.
	if ($('.plp-filters-mvc').length && util.isMobileSize()) {
		$.ajax({ 
		    url: util.appendParamToURL(url, 'format', 'ajax'), 
		    type: "GET", 
		    dataType: "html" 
		}).done(function(data) {
			if ($(data).find('.mobile-refinements-filter__inner').length > 0 && $('.no-results').length == 0 && !isMobileMVC) {
			    $('#secondary .mobile-refinements-filter__inner').html($(data).find('.mobile-refinements-filter__inner').html());
			} else { //InCase We need to update whole main wrapper for mobile.
				// in Case of results with full upgrade update the refinedURLState
				if (typeof $(data) != undefined && $(data).find('.mobile-refinements-filter__inner').length > 0) {
					sessionStorage['refinedURLState'] = url;
				}
				$('#main').html($(data));
				$("body").removeClass('mobile-refinements-filter-show');
				// Apply formatting post ajax call.
				updateProductTile();
			}
		  //get site name
			var siteNameVal = $("input[name='siteName']").val();
			if (siteNameVal == 'Mattress-Firm') {
				if (filterOpen) {
					var _closeButton = $('.refinement-big-container').find('div.close-reminement');
					if (_closeButton.hasClass('sticky')) {
						_closeButton.fadeTo(300, 0, function() {
							$(this).removeClass('sticky');
						});
					} else {
						_closeButton.addClass('sticky');
					}
				}
				$('.no-results__button').on("click", function(e) {
					e.preventDefault();
					if( sessionStorage['refinedURLState'] ){
						updateProductListing(sessionStorage['refinedURLState']);
					} else if( currentURL ) {
						updateProductListing(currentURL);
						//window.location = currentURL;
					}
					$(".no-results").css("opacity" , "0.5");
					$(".no-results-filters").css("opacity" , "0.5");
				});
			}
			initContainerHeight();
			initRefinementButtons(isStopEventTriggered);
			initSubcategoryContent();
			compareWidget.init();
			productTile.init();
			productTile.initComfortLevelTagging();
			truncate.init();
			progress.hide();
			// util.uniform();
			initPlaceHolder();
			history.pushState(undefined, '', url);
			updatePaginationPosition();
			updateSortPosition();
			updateRefinedByPosition();
			drawerMobileOpener();
			mobileCloseRefinement();
			positionGridList();
			if (listView) {
				$('.search-result-content').addClass('wide-tiles');
				$('.toggle-grid').addClass('wide');
				$(".product-key-features-plp").show();
			}
			if (util.isMobileSize() && siteNameVal != 'Mattress-Firm') {
				$('html,body').animate({
					scrollTop: $('.refinement-big-container').offset().top},
				'slow');
			}
			var queryString = window.location.search;
			var n = queryString.indexOf("q=");
			if ((siteNameVal == 'Mattress-Firm' || siteNameVal == '1800Mattress-RV') && queryString.indexOf("q=") < 0) {
				//get category name
				var defaultTitle = $("input[name='catName']").val();
				//get selected refinement from hidden field on click checkbox
				var ajaxTitle = $("input[name='selectedRefinements']").val();
				if (ajaxTitle && ajaxTitle.length > 0) {
					document.title = ajaxTitle;
				}else if (defaultTitle && defaultTitle.length > 0) {
					//if uncheck all refinment on click
					document.title = defaultTitle;
				}
				var singleFacet = $("input[name='hfCanonicalIdSingleFacet']").val();
				var multiFacet = $("input[name='hfCanonicalIdMultipleFacet']").val();
				var prevPage = $("input[name='prevPageInfo']").val();
				var nextPage = $("input[name='nextPageInfo']").val();
				var priceRefinementCheck = $("input[name='PriceRefinementCheck']").val();
				var isRobot = $("input[name='isRobotAppliesForSingleCanonical']").val();
				if (singleFacet && singleFacet.length > 0 && singleFacet != "null") {
					$('link[name=singleCanonical]').remove();
					$('link[name=multiCanonical]').remove();
					$('meta[name=ROBOTS]').remove();
					$('head').append('<link name="singleCanonical" rel="canonical" href=' + singleFacet + '>');
					if (priceRefinementCheck && priceRefinementCheck.length > 0) {
						$('head').append('<meta name="ROBOTS" content="NOINDEX, FOLLOW">');
					} else if(isRobot == "true") {
						$('head').append('<meta name="ROBOTS" content="NOINDEX, FOLLOW">');
					}
				}
				if (multiFacet && multiFacet.length > 0 && multiFacet != "null") {
					$('link[name=singleCanonical]').remove();
					$('link[name=multiCanonical]').remove();
					$('meta[name=ROBOTS]').remove();
					$('head').append('<meta name="ROBOTS" content="NOINDEX, FOLLOW">');
					$('head').append('<link name="multiCanonical" rel="canonical" href=' + multiFacet + '>');
				}
				$('link[name=prevPageInfo]').remove();
				$('link[name=nextPageInfo]').remove();
				if (prevPage && prevPage.length > 0) {
					$('head').append('<link name="prevPageInfo" rel="prev" href=' + prevPage + '>');
				}
				if (nextPage && nextPage.length > 0) {
					$('head').append('<link name="nextPageInfo" rel="next" href=' + nextPage + '>');
				}

				/*ZipCode Update Event for Clearance Page*/
				var zipCode = User.customerZip;
				var currentLocationZipCode = User.currentLocationZipCode;
				if(zipCode) {
					$('#customer-zip').val(zipCode);
				}
				else if(currentLocationZipCode) {
					$('#customer-zip').val(currentLocationZipCode);
				}

				document.title = document.title.replace("Shop Shop","Shop");
			}
					
		});
	} else {
	    $('#main').load(util.appendParamToURL(url, 'format', 'ajax'), function () {
		  //get site name
			var siteNameVal = $("input[name='siteName']").val();
			if (siteNameVal == 'Mattress-Firm') {
				if (filterOpen) {
					$('#secondary.refinements').find('#nav_label').click();
					var _closeButton = $('.refinement-big-container').find('div.close-reminement');
					if (_closeButton.hasClass('sticky')) {
						_closeButton.fadeTo(300, 0, function() {
							$(this).removeClass('sticky');
						});
					} else {
						_closeButton.addClass('sticky');
					}
				}
				$('.no-results__button').on("click", function(e) {
					e.preventDefault();
					if( currentURL ){
						updateProductListing(currentURL);
						//window.location = currentURL;
					}
					$(".no-results").css("opacity" , "0.5");
					$(".no-results-filters").css("opacity" , "0.5");
				});
			}
			initContainerHeight();
			initRefinementButtons(isStopEventTriggered);
			initSubcategoryContent();
			compareWidget.init();
			productTile.init();
			productTile.initComfortLevelTagging();
			truncate.init();
			progress.hide();
			// util.uniform();
			initPlaceHolder();
			history.pushState(undefined, '', url);
			updatePaginationPosition();
			updateSortPosition();
			updateRefinedByPosition();
			drawerMobileOpener();
			mobileCloseRefinement();
			positionGridList();
			if (listView) {
				$('.search-result-content').addClass('wide-tiles');
				$('.toggle-grid').addClass('wide');
				$(".product-key-features-plp").show();
			}
			if (util.isMobileSize() && siteNameVal != 'Mattress-Firm') {
				$('html,body').animate({
					scrollTop: $('.refinement-big-container').offset().top},
				'slow');
			}
			var queryString = window.location.search;
			var n = queryString.indexOf("q=");
			if ((siteNameVal == 'Mattress-Firm' || siteNameVal == '1800Mattress-RV') && queryString.indexOf("q=") < 0) {
				//get category name
				var defaultTitle = $("input[name='catName']").val();
				//get selected refinement from hidden field on click checkbox
				var ajaxTitle = $("input[name='selectedRefinements']").val();
				if (ajaxTitle && ajaxTitle.length > 0) {
					document.title = ajaxTitle;
				}else if (defaultTitle && defaultTitle.length > 0) {
					//if uncheck all refinment on click
					document.title = defaultTitle;
				}
				var singleFacet = $("input[name='hfCanonicalIdSingleFacet']").val();
				var multiFacet = $("input[name='hfCanonicalIdMultipleFacet']").val();
				var prevPage = $("input[name='prevPageInfo']").val();
				var nextPage = $("input[name='nextPageInfo']").val();
				var priceRefinementCheck = $("input[name='PriceRefinementCheck']").val();
				var isRobot = $("input[name='isRobotAppliesForSingleCanonical']").val();
				if (singleFacet && singleFacet.length > 0 && singleFacet != "null") {
					$('link[name=singleCanonical]').remove();
					$('link[name=multiCanonical]').remove();
					$('meta[name=ROBOTS]').remove();
					$('head').append('<link name="singleCanonical" rel="canonical" href=' + singleFacet + '>');
					if (priceRefinementCheck && priceRefinementCheck.length > 0) {
						$('head').append('<meta name="ROBOTS" content="NOINDEX, FOLLOW">');
					} else if(isRobot == "true") {
						$('head').append('<meta name="ROBOTS" content="NOINDEX, FOLLOW">');
					}
				}
				if (multiFacet && multiFacet.length > 0 && multiFacet != "null") {
					$('link[name=singleCanonical]').remove();
					$('link[name=multiCanonical]').remove();
					$('meta[name=ROBOTS]').remove();
					$('head').append('<meta name="ROBOTS" content="NOINDEX, FOLLOW">');
					$('head').append('<link name="multiCanonical" rel="canonical" href=' + multiFacet + '>');
				}
				$('link[name=prevPageInfo]').remove();
				$('link[name=nextPageInfo]').remove();
				if (prevPage && prevPage.length > 0) {
					$('head').append('<link name="prevPageInfo" rel="prev" href=' + prevPage + '>');
				}
				if (nextPage && nextPage.length > 0) {
					$('head').append('<link name="nextPageInfo" rel="next" href=' + nextPage + '>');
				}
	
				/*ZipCode Update Event for Clearance Page*/
				var zipCode = User.customerZip;
				var currentLocationZipCode = User.currentLocationZipCode;
				if(zipCode) {
					$('#customer-zip').val(zipCode);
				}
				else if(currentLocationZipCode) {
					$('#customer-zip').val(currentLocationZipCode);
				}
	
				document.title = document.title.replace("Shop Shop","Shop");
			}
			// maintaining state for mobile refinedUrl MVC Filters
			if ($('.plp-filters-mvc').length && $('.no-results').length == 0) {
				sessionStorage['refinedURLState'] = url;
			}
			
			updateProductTile();			
			
		});
}
  // end test
}

/**
 * @private
 * @function
 * @description initialize refinement: Price Slider
 */
function initPriceSliderRefinement() {
	
	var priceLabel = $('#pricing-labels');
	
	if (priceLabel.length > 0 ){
		var amountsStr = priceLabel.data('amounts').split(',');
		var amounts = [];
		for (var i=0; i < amountsStr.length; i++) {
			amounts.push(Number(amountsStr[i]));
		}
		var maxAvailableAmount = Number(priceLabel.data('max-available-amount'));
		var indexMin = 0;
		var indexMax = amounts.length -1;
	
		var initialIndexes = getInitialPositionOfPricingSlider(amounts);
		if (initialIndexes.indexMin != -1) {
			indexMin = initialIndexes.indexMin;
		}
		if (initialIndexes.indexMax != -1) {
			indexMax = initialIndexes.indexMax;
		}
		// If indexMin & indexMax are both 0 then update indexMax to have a window of 1 step on slider
		if (indexMax == 0 && amounts.length > 1) {
			indexMax++;
		}
		var initSteps = [indexMin, indexMax];
	
		$("#plp-filter-slider-range").slider({
			range: true,
			animate: true,
			orientation: "horizontal",
			min: 0,
			max: amounts.length - 1,
			values: initSteps,
			step: 1,
			create: function(event, ui) {
				var minIndex = $("#plp-filter-slider-range").slider("values", 0)+1;
				var maxIndex = $("#plp-filter-slider-range").slider("values", 1)+1
				showSelectedPrices(minIndex, maxIndex);
			},
			slide: function(event, ui) {
					if(ui.values[0] == ui.values[1]) // min/max equal values not allowed
						return false;
					// If min value selected was the least value from available amounts then take it as 0 (i.e. <100)
					$("#min-selected-price").val(ui.values[0] > 0 ? amounts[ui.values[0]] : 0);
					// If max value selected was the maximum value from available amounts then take it as maxAvailableAmount (i.e. 500+/4000+)
					$("#max-selected-price").val(ui.values[1] < amounts.length-1 ? amounts[ui.values[1]] : maxAvailableAmount);
					showSelectedPrices(ui.values[0]+1, ui.values[1]+1);
			},
			stop: function( event, ui ) {
				var pmax = $("#max-selected-price").val();
				var pmin = $("#min-selected-price").val();
				// Get Search Results on the basis of Price Range selected and render products on PLP
				updateProductListing(updateURLforPricingPLPFilter(pmax, pmin));
				if (util.isMobileSize() || $(window).width() < 768) {
					$('html,body').animate({
				        scrollTop: $(".no-result").offset().top - 100
				    }, 'slow');
				}
				$('html,body').animate({
			        scrollTop: $("#primary").offset().top - 100
			    }, 'slow');
			}
		});
		var amountMinSelected = $("#plp-filter-slider-range").slider("values", 0)>0?amounts[$("#plp-filter-slider-range").slider("values", 0)]:0;
		var amountMaxSelected = $("#plp-filter-slider-range").slider("values", 1)<amounts.length-1?amounts[$("#plp-filter-slider-range").slider("values", 1)]:maxAvailableAmount;
		$("#min-selected-price").val(amountMinSelected);
		$("#max-selected-price").val(amountMaxSelected);
	
		$("#plp-filter-slider-range").draggable();
	}
}

/**
 * @private
 * @function
 * @description find out the current position of Pricing Slider positions
 * @returns {Object} the minIndex and maxIndex of the slider positions
 */
function getInitialPositionOfPricingSlider(amounts) {
	/*
	 * Setting the Min & Max values of the Slider.
	 * If pmax & pmin parameters exist in the Product Search Result then set those values, else take 0 & last index as default values
	 * */
	var indexMin = -1,
		indexMax = -1;
	if ($('#selected-prices').data('pmin') == null || $('#selected-prices').data('pmax') == null) {
		return {
			indexMin : indexMin,
			indexMax : indexMax
		}
	}
	var pMinFromPSR = Number($('#selected-prices').data('pmin'));
	var pMaxFromPSR = Number($('#selected-prices').data('pmax'));

	/*
	 * The below logic will find out the indices for Pricing Slider as such to have the pMinFromPSR & pMaxFromPSR inclusive
	 * */
	// finding out indexMin
	for (var i=0; i<amounts.length-1; i++) {
		if (pMinFromPSR >= amounts[i]) {
			indexMin = i;
		}
	}
	// Select closest value for indexMin from amounts array
	if (indexMin != -1 && (pMinFromPSR - amounts[indexMin]) > (amounts[indexMin+1] - pMinFromPSR)) {
		indexMin++;
	}
	// finding out indexMax
	for (var i=amounts.length-1; i>=0; i--) {
		if (pMaxFromPSR <= amounts[i]) {
			indexMax = i;
		}
	}
	// Select closest value for indexMax from amounts array
	if (indexMax != -1 && (amounts[indexMax] - pMaxFromPSR) > (pMaxFromPSR - amounts[indexMax-1])) {
		indexMax--;
	}
	// non-equal values for indexMin & indexMax
	if (indexMin == indexMax) {
		if (indexMax < amounts.length-1)
			indexMax++;
		else if (indexMin > 0)
			indexMin--;
	}
/*	indexMin = amounts.indexOf(pMinFromPSR)>-1 ? amounts.indexOf(pMinFromPSR) : 0;
	// If pmax was not found in URL or it's value was greater than max value in amounts then last index from amounts array, else have the respective index selected
	indexMax = (amounts.indexOf(pMaxFromPSR) == -1 || pMaxFromPSR > amounts[amounts.length-1]) ? amounts.length-1 : amounts.indexOf(pMaxFromPSR);
*/
	return {
		indexMin : indexMin,
		indexMax : indexMax
	}
}

/**
 * @private
 * @function
 * @description display only selected min/max prices on the Pricing Slider
 * @param {String} min the index of min price label
 * @param {String} max the index of max price label
 */
function showSelectedPrices(min, max) {
	// Hide all price labels
	$('#selected-prices-labels .price-steps *').addClass('hide-me');
	// Display the selected min/max price labels
	$('#amount-selected-label-'+min).removeClass('hide-me');
	$('#amount-selected-label-'+max).removeClass('hide-me');
}

/**
 * @private
 * @function
 * @description initialize refinement: Thickness Slider
 */
function initThicknessSliderRefinement(isStopEventTriggered) {
	var heightIndex = $(".thickness-height-val").length-1;
	var minVal = 0;
	var maxVal = heightIndex;

	var initialIndexes = getInitialPositionOfMattressHeightSlider(isStopEventTriggered);
	if (initialIndexes.minVal != -1) {
		minVal = initialIndexes.minVal;
	}
	if (initialIndexes.maxVal != -1) {
		maxVal = initialIndexes.maxVal;
	}
	var initSteps = [minVal, maxVal];

	$('#mattress-thickness-slider').slider({
        orientation: "vertical",
        animate: true,
        range: true,
        min: 0,
        max: heightIndex,
        step: 1,
        values: initSteps, // Initial state
        stop: function( event, ui ) {
        	sessionStorage['updatedURLForMattressHeight'] = null;
        	var updatedURLForMattressHeight = updateURLforMattressHeightPLPFilter(ui.values[0], ui.values[1]);
        	sessionStorage['updatedURLForMattressHeight'] = updatedURLForMattressHeight;
            updateProductListing(updatedURLForMattressHeight, true);
            $('html,body').animate({
		        scrollTop: $("#primary").offset().top - 100
		    }, 'slow');
        }
    });
	$('#mattress-thickness-slider').draggable();
}

/**
 * @private
 * @function
 * @description find out the current position of Mattress Height Slider positions
 * @returns {Object} the minIndex and maxIndex of the slider positions
 */
function getInitialPositionOfMattressHeightSlider(isStopEventTriggered) {
	var minVal = -1,
		maxVal = -1;

	var prefn = '';
	var currentURL = '';
	if (isStopEventTriggered) {
		currentURL = (sessionStorage['updatedURLForMattressHeight'] != null && sessionStorage['updatedURLForMattressHeight'] != '') ? sessionStorage['updatedURLForMattressHeight'] : window.location.href;
	} else {
		currentURL = window.location.href;
	}
	if(currentURL != null && currentURL != '') {
		prefn = util.getParamNameFromURL(currentURL, 'mattressHeight');
	}

	var paramsVal = "";
	var prefv = 'prefv';
	if (prefn != '') {
		prefv += prefn[prefn.length-1];
		paramsVal = util.getParamValueFromURL(currentURL, prefv);
	}

	if(paramsVal != null && paramsVal != "") {
		var mattressHeightParam = paramsVal.substring(paramsVal.indexOf('=')+1,paramsVal.length);
		var decodedURL = decodeURIComponent(mattressHeightParam);
		var mattressHeightParams = decodedURL.split('|');

		$(".thickness-height-val").each(function (index) {
			if(($("#thickness_"+index).text().toString().replace('"','').replace(/\n/g,'')) == (mattressHeightParams[0].toString().replace('"','').replace(/\n/g,''))){
				minVal = index;
			}
			if(($("#thickness_"+index).text().toString().replace('"','').replace(/\n/g,'')) == (mattressHeightParams[mattressHeightParams.length-1].toString().replace('"','').replace(/\n/g,''))){
				maxVal = index;
			}
		});
	}
	return {
		minVal : minVal,
		maxVal : maxVal
		};
}
function updateURLforMattressHeightPLPFilter(from, to) {
	var selectedHeights = '';
	for (var i=from; i<=to; i++) {
		selectedHeights += $('#thickness_'+i).text().replace(/\n/g,'') + '|';
	}
	selectedHeights = selectedHeights.substring(0, selectedHeights.length-1);
	var currentURL = window.location.href;
	var prefn = util.getParamNameFromURL(currentURL, 'mattressHeight');
	var prefv = 'prefv';
	if (prefn != '') {
		prefv += prefn[prefn.length-1];
		currentURL = util.removeParamFromURL(currentURL, prefv);
	} else {
		var j=1;
		prefn = 'prefn';
		while (util.getParamValueFromURL(currentURL, prefn+j) != '') {
			j++;
		}
		prefn += j;
		prefv += j;
		currentURL = util.appendParamToURL(currentURL, prefn, 'mattressHeight');
	}
	currentURL = util.appendParamToURL(currentURL, prefv, selectedHeights);
	return currentURL;
}

/**
 * @private
 * @function
 * @description initialize refinement buttons for mobile
 */
function initRefinementButtons(isStopEventTriggered) {
	var siteNameVal = $("input[name='siteName']").val();
	if (siteNameVal == 'Mattress-Firm') {
		var _closeButton = $('.refinement-big-container').find('div.close-reminement');
		$('.refinement-big-container label.mobile-only').on('click', function() {
			// PLP Filters MVC AB Test
			if ($('.plp-filters-mvc').length) {
				if (!$('#mobile-refinements-filter').hasClass('show-mobile-refinements-filter')) {
					$('#mobile-refinements-filter').addClass('show-mobile-refinements-filter');
					// $("body").addClass('mobile-refinements-filter-show');
					$(window).scrollTop(0);
				}
			}
			if (_closeButton.hasClass('sticky')) {
				_closeButton.fadeTo(300, 0, function() {
					$(this).removeClass('sticky');
				});
			} else {
				_closeButton.addClass('sticky');
			}
		});

		$(window).on('scroll', function() {
			if ($('.refinement-big-container nav').height() > 0) {
				var containerHeight = parseInt($('.refinement-big-container nav')[0].scrollHeight);
				var position = parseInt(containerHeight);
				if (parseInt($(document).scrollTop()) >= position) {
					_closeButton.addClass('relative');
				} else {
					_closeButton.removeClass('relative');
				}
			}
		});

		// If AB testing params were found in the URL
		if ($('.plp-filter').length) {
			initPriceSliderRefinement();
			initThicknessSliderRefinement(isStopEventTriggered);
			$('#clearAllFilters').on("click", function(e) {
				e.preventDefault();
				updateProductListing($(this).attr('href'));
			});			
			
		}
	}
	// Refinements Truncate functionality
	$(".hideRefinement").hide();
	$(".refinements-see-less").hide();
	$(".refinements-see-more").on("click", function() {
		/*
		 * MAT-1733 | Astound Sprint 3 - [PLP] Mattress Filters
		 * If you select a value within a collapsed facet the facet should stay open to the “see more” for that filter.
		 */
		var filterId = $(this).parents('ul').parent().attr('id');	// ID of Filter/Refinement to which this "see more" belongs
		if (sessionStorage.getItem('keepFacetsOpenForRefinements') == null) {
			sessionStorage.setItem('keepFacetsOpenForRefinements', filterId);
		} else {
			if (sessionStorage.getItem('keepFacetsOpenForRefinements').indexOf(filterId) == -1) { // if this filter already does not exists in the session variable
				var allFilterIds = sessionStorage.getItem('keepFacetsOpenForRefinements').split(',');
				allFilterIds.push(filterId);
				sessionStorage.setItem('keepFacetsOpenForRefinements', allFilterIds);
			}
		}
		
		$(this).parents('ul').find(".hideRefinement").show();
		$(this).parents('ul').find(".refinements-see-less").show();
		$(this).hide();
	});
	$(".refinements-see-less").on("click", function() {
		/*
		 * MAT-1733 | Astound Sprint 3 - [PLP] Mattress Filters
		 * If you select a value within a collapsed facet the facet should stay open to the “see more” for that filter.
		 */
		var filterId = $(this).parents('ul').parent().attr('id');	// ID of Filter/Refinement to which this "see more" belongs
		if (sessionStorage.getItem('keepFacetsOpenForRefinements') != null) {
			if (sessionStorage.getItem('keepFacetsOpenForRefinements').indexOf(filterId) != -1) { // if this exists then remove it
				var allFilterIds = sessionStorage.getItem('keepFacetsOpenForRefinements').split(',');
				var filterIndex = allFilterIds.indexOf(filterId);
				if (filterIndex > -1) {
					allFilterIds.splice(filterIndex, 1); // Remove this element
					sessionStorage.setItem('keepFacetsOpenForRefinements', allFilterIds);
				}
			}
		}

		$(this).parents('ul').find(".hideRefinement").hide();
		$(this).parents('ul').find(".refinements-see-more").show();
		$(this).hide();

	});
	if($('.plp-filter').length > 0){
		$(".close-filter-mobile").click(function(){
	        $("#nav_label").prop("checked", false);
	        //$('#secondary.refinements').find('#nav_label').click();
			var _closeButton = $('.refinement-big-container').find('div.close-reminement');
			if (_closeButton.hasClass('sticky')) {
				_closeButton.fadeTo(300, 0, function() {
					$(this).removeClass('sticky');
				});
			} else {
				_closeButton.addClass('sticky');
			}
	    });
	}
	// Keep the Facets open those were expanded by the Customer
	openFacets();
}

$(document).ready(function(e){
	openFacets();
});

function openFacets() {
	if (sessionStorage.getItem('keepFacetsOpenForRefinements') != null) {
		var openFacets = sessionStorage.getItem('keepFacetsOpenForRefinements').split(',');
		for (var i=0; i<openFacets.length; i++) {
			if(openFacets[i]){
				$('#'+openFacets[i]+' h3').removeClass('expanded-title');
				$('#'+openFacets[i]+' ul').css("display", "block");
	            if ($('#'+openFacets[i]).find('.refinements-see-more').length) {
	            	$('#'+openFacets[i]).find('.refinements-see-more').click();
	            }	
			}
								
		}
	}
	
	
	//set refinement classes
	for(var x=0; x < refinementsCollection.length; x++) {
		var jObj = refinementsCollection[x];
		var elemId = jObj.divId;
		var elemClass = jObj.existingClass;
		var diplayProp = jObj.displayBlock;
		if(elemId){
			$('#'+elemId+' h3').removeClass();
			$('#'+elemId+' h3').addClass(elemClass);
			$('#'+elemId+' ul').css("display", diplayProp);			
		}		 
	}
	
	var windowOffset = $(document).scrollTop();
	if ($('#secondary.refinements').length > 0 && parseInt($(window).width()) > 767) {
		var contentHeight = parseInt($('.content-slot.slot-grid-header').height() + 200);
		if (parseInt(windowOffset) > contentHeight) {
			$('#secondary.refinements').addClass('sticky');
		} else {
			$('#secondary.refinements').removeClass('sticky');
		}		
	} 
	$(".refinements").scrollTop(refinementDivPosition);
}

/**
 * @private
 * @function
 * @description returns new URL with updated parameters values of pmax & pmin
 * @param {String} pmax the value of max price
 * @param {String} pmin the value of min price
 */
function updateURLforPricingPLPFilter(pmax, pmin) {
	var currentURL = $('#price-url').val();
	if (currentURL.indexOf('pmax')>-1 && currentURL.indexOf('pmin')>-1) {
		currentURL = util.removeParamFromURL(currentURL, 'pmax');
		currentURL = util.removeParamFromURL(currentURL, 'pmin');
	}
	return util.appendParamsToUrl(currentURL, {'pmax':pmax, 'pmin':pmin});
}

/**
 * @private
 * @function
 * @description Set min-height of sub-category container based on height of filters
 */
function initContainerHeight() {
	if ($('#secondary.refinements').length > 0 && parseInt($(window).width()) > 767) {
		var filterNavHeight = $('#secondary.refinements').height();
		$('#primary').css('min-height', filterNavHeight + 'px');
	}
}

/**
 * @private
 * @function
 * @description initialize sub-category header content
 */
function initSubcategoryContent() {
	// sub-category tab content
	var _tabContainer = $('.subcategory-content-tabs');
	_tabContainer.fadeIn();
	var _tabNav = _tabContainer.find('ul.tab-nav li');

	_tabNav.find('a').bind('click', function(e) {
		e.preventDefault();
		_tabNav.removeClass('tab-active');
		var target = $(this);
		var tabClass = target.data('tabclass');
		_tabContainer.find('.cat-content-block-text').each(function() {
			if ($(this).hasClass(tabClass)) {
				$(this).show();
				if ($(this).hasClass('related')) {
					$(this).css('display', 'inline-flex');
				}
				target.parent().addClass('tab-active');
			} else {
				$(this).hide();
			}
		});
	});
}

/**
 * @private
 * @function
 * @description updates pagination position for mobile
 */
function updatePaginationPosition() {
	if (util.isMobileSize() && $('.refinements .pagination').length < 1) {
		$('.search-result-options.top .pagination').prependTo('.refinements');
	}
	ResponsiveSlots.smartResize(function () {
		if (util.isMobileSize() && $('.refinements .pagination').length < 1) {
			$('.search-result-options.top .pagination').prependTo('.refinements');
		} else {
			if ($('.search-result-options.top .pagination').length < 1) {
				$('.refinements .pagination').prependTo('.search-result-options.top');
			}
		}
	});
}
/**
 * @private
 * @function
 * @description updates sort by position for mobile
 */
var siteNameVal = $("input[name='siteName']").val();
if (siteNameVal == '1800Mattress-RV') {
	if (util.isMobileSize() || $(window).width() < 1025) {
		if ($('.search-result-options.top .sort-by').length > 0) {
			$('.search-result-options.top .sort-by').hide();
			$('.search-result-options.top .sort-by').insertBefore('#primary').css("display", "block");
		}
		if ($('.refinement-big-container').length > 0) {
			$('.refinement-big-container').hide();
			$('.refinement-big-container').prependTo('#primary').css("display", "block");
		}
	}
}
function updateSortPosition() {
	if (!$('.plp-filters-mvc').length) {
		if (util.isMobileSize() && siteNameVal != '1800Mattress-RV') {
			if ($('.refinement-big-container .sort-by').length < 1) {
				$('.search-result-options.top .sort-by').prependTo('.refinement-big-container');
			}
		}
		ResponsiveSlots.smartResize(function () {
			if (util.isMobileSize() && siteNameVal != '1800Mattress-RV') {
				if ($('.refinement-big-container .sort-by').length < 1) {
					$('.search-result-options.top .sort-by').prependTo('.refinement-big-container');
				}
			} else {
				if ($('.search-result-options.top .sort-by').length < 1) {
					$('.refinement-big-container .sort-by').insertAfter('.search-result-options.top .items-per-page-inner');
				}
			}
		});
	}
	$('.refinement-big-container nav .refinement').last().addClass('refinement-last');
}
/**
 * @private
 * @function
 * @description updates refined by position for mobile
 */
function updateRefinedByPosition() {
	if (!$('.plp-filters-mvc').length) {
		if (util.isMobileSize()) {
			if ($('.refinements .breadcrumb-refinement-mobile').length < 1) {
				$('.breadcrumb-refinement-mobile').insertAfter('.refinement-big-container');
			}
		}
		if ($(window).width() < 1025 && siteNameVal == '1800Mattress-RV') {
			if ($('.refinement-big-container')) {
				$('.refinement-big-container').hide();
				$('.refinement-big-container').insertAfter('.search-result-options.top').css("display", "block");
			}
			if ($(window).width() >767 && $(window).width() < 1025) {
				$('.search-result-options.top').css("bottom", "28px");
			}
		}
	}
}
function initPlaceHolder() {
	$('[placeholder]').focus(function () {
		var input = $(this);
		if (input.attr('id') == 'email-alert-address') {
			return false;
		}
		if (input.val() == input.attr('placeholder')) {
			input.val('');
			input.removeClass('placeholder');
			}
		}).blur(function () {
			var input = $(this);
			if (input.attr('id') == 'email-alert-address') {
				return false;
			}
			if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.addClass('placeholder');
				input.val(input.attr('placeholder'));
				}
			}).blur().parents('form').submit(function () {
				$(this).find('[placeholder]').each(function () {
					var input = $(this);
					if (input.val() == input.attr('placeholder')) {
						input.val('');
					}
				})
			});
}

function drawerMobileOpener() {
	if ($('.plp-filters-mvc').length > 0) { 
		$('#nav_label').on('click', function() {
			$('body').addClass('scrollHidden');
			$('html').addClass('scrollHidden');
		});
	}
}

function mobileCloseRefinement() {
	if ($('.plp-filters-mvc').length === 0) { 
		$('.close-reminement').on('click', function (e) {			
			$("body").removeClass('mobile-refinements-filter-show');
			$('#nav_label').click();
			$('body').removeClass('scrollHidden');
			$('html').removeClass('scrollHidden');
			var container = $('.refinement-big-container').offset().top;
			$('html,body').animate({
				scrollTop: container},
			'slow');
			var closeButton = $('.refinement-big-container').find('div.close-reminement.sticky');
			if (closeButton.length > 0) {
				closeButton.removeClass('sticky');
			}
		});
	} else {
		$('#apply-filter-btn').on('click', function() {
				$("body").removeClass('mobile-refinements-filter-show');
				$('#nav_label').click();
				var container = $('.refinement-big-container').offset().top;
				$('html,body').animate({
					scrollTop: container
				}, 'slow');

				$('body').removeClass('scrollHidden');
				$('html').removeClass('scrollHidden');

				var closeButton = $('.refinement-big-container').find('div.close-reminement.sticky');
				if (closeButton.length > 0) {
					closeButton.removeClass('sticky');
				}
				// In Case User Apply Filter then Update Grid If User have Different Selection
				var currentURLPLPMVC = window.location.href;
				if (sessionStorage['refinedURLState'] != undefined && sessionStorage['refinedURLState'] != currentURLPLPMVC) {
					sessionStorage['refinedURLState'] = window.location.href;
                    updateProductListing(window.location.href, undefined, true);
				}
			//$('#mobile-refinements-filter').removeClass('show-mobile-refinements-filter');
		});
	}
}
function positionGridList() {
	if (!util.isMobileSize()) {
		if ($('.items-per-page').length == 0) {
			$('.toggle-grid').addClass('one-page-items');
		}
		if ($('.items-per-page select option:selected').attr('data-count')) {
			var widthSelect = $('.items-per-page select').width();
			$('.toggle-grid').css('right', widthSelect + 'px');
		}
		$('.toggle-grid').css('display', 'block');
	}
}
function ellipsizeTextBox(el) {
	//var el = document.getElementById(id);
	var wordArray = el.innerHTML.split(' ');
	while (el.scrollHeight > el.offsetHeight) {
		wordArray.pop();
		el.innerHTML = wordArray.join(' ') + '<span class="product-text-hover">...</span>';
		}
	}


function ellipsizeTextBoxFor1800(el) {
	var wordArray = el.innerHTML.split(' ');
	var trimmedName = "";
	while (el.scrollHeight > el.offsetHeight) {
		trimmedName = trimmedName + wordArray.pop();
		el.innerHTML = wordArray.join(' ') + '<span class="product-text-hover">...</span> ' + ' <span class="trimmed-name"> ' + trimmedName + ' </span>';
		}
	}

/**
 * @private
 * @function
 * @description updates clearance zipcode on clearance PLP
 */
function updateClearanceZipCode() {
	var _zip = $.trim($('#customer-zip').val());
	// 1. Verify a valid US 5-digit ZIP first, and that the same ZIP was not entered again
	if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(_zip)) {
		var data = {zip: _zip};
		var url = Urls.pdpZoneCheck;
		$.ajax({
			url: url,
			type: 'post',
			data: data,
			beforeSend: function () {
				//progress.show('#product-content');
			},
			dataType: 'html',
			success: function () {
				window.location = window.location.pathname;
			},
			error: function (request, status, error) {
				/* service is down or had an error so enable the add to cart button */
				//$('#add-to-cart').removeAttr('disabled');
			},
			complete: function () {
				//progress.hide('#product-content');
			}
		});
	}
}

/**
 * @private
 * @function
 * @description trigger Tealium Tag Events related to PLP Filters
 */
function tagPLPFilter(Label, Action) {
	if (typeof (utag) != "undefined") {
		utag.link({ eventCategory: 'PLP Filter', eventLabel: Label, eventAction: Action });
	}
}

/**
 * @private
 * @function
 * @description trigger Tealium Tag Events related to PLP Filters Impression
 */
function tagPLPFilterImpression() {
	$(window).load(function(){
		var eventCategory = 'PLP Test',
			Action = 'PLP Test - Impression',
			Label = ($('.plp-filters-mvc').length > 0) ? 'PLP Test - Segment A' : 'Control';		
		if (typeof (utag) != "undefined") {
			utag.link({eventCategory : eventCategory, eventLabel : Label, eventAction : Action, "nonInteraction" : "true"})
		}
	});
}

/**
 * @private
 * @function
 * @description Initializes events for the following elements:<br/>
 * <p>refinement blocks</p>
 * <p>updating grid: refinements, pagination, breadcrumb</p>
 * <p>item click</p>
 * <p>sorting changes</p>
 */
function initializeEvents() {
	var $main = $('#main');
	// compare checked
	$main.on('click', 'input[type="checkbox"].compare-check', function () {
		var cb = $(this);
		var tile = cb.closest('.product-tile');

		var func = this.checked ? compareWidget.addProduct : compareWidget.removeProduct;
		var itemImg = tile.find('.product-image a img').first();
		func({
			itemid: tile.data('itemid'),
			uuid: tile[0].id,
			img: itemImg,
			cb: cb
		});

	});

	updateProductTile();

	// handle toggle refinement blocks
	$main.on('click', '.refinement h3', function () {
		$(this).toggleClass('expanded-title')
		.siblings('ul').toggle()
		.siblings('.refinement-top-level').toggle();

		// plp-filters-mvc
		if ($('.plp-filters-mvc').length) {
			if (!$(this).hasClass('expanded-title')) {
				$(this).closest('.refinement').removeClass('collapsed-refinement').addClass('expanded-refinement');
			} else {
				$(this).closest('.refinement').removeClass('expanded-refinement').addClass('collapsed-refinement');
			}
		}
	});

	/*  */
	// handle toggle refinement blocks on enter key press
	$main.on('keypress', '.refinement h3', function (ev) {
		ev.preventDefault();
		if (ev.keyCode == 13 || ev.which == 13) {
			$(this).toggleClass('expanded-title')
			.siblings('ul').toggle()
			.siblings('.refinement-top-level').toggle();
			}
		});

	var siteNameVal = $("input[name='siteName']").val();
	if (siteNameVal == 'Mattress-Firm' && $('.plp-filters-mvc').length) {
		$main.on('click', '.refinements a', function () {
			if (util.isMobileSize()) {
				$('html,body').animate({
			        scrollTop: $("#main").offset().top
			    }, 'slow');
			} else {
				$('html,body').animate({
			        scrollTop: $("#primary").offset().top - 100
			    }, 'slow');
			}
		});
	}
	if (siteNameVal == '1800Mattress-RV') {
		$main.on('click', '.refinements a', function () {
			$('html,body').animate({
		        scrollTop: $("#primary").offset().top
		    }, 'slow');
		});
	}

	$(document).on('click', '.breadcrumb-refinement-clear-all', function () {
		$('body').removeClass('scrollHidden');
		$('html').removeClass('scrollHidden');
	});

	// handle events for updating grid
	$main.on('click', '.refinements a, .pagination a, .breadcrumb-refinement-value a, a.selected-filter,.selected-filters-mvc a.selected-filters__clear-all', function (e) {
		
		// SHOP-3030 Taging PLP Filters, it will handle next-day delivery and all other filters
		var actionFilter = ($(this).parents('ul.nextday_delivery_filter').length > 0) ? $(this).parents('ul.nextday_delivery_filter') : $(this).parents('.refinement');
		if (actionFilter.length > 0) {
			var filterLabel = $(this).find('.refinement-text');
			var eventLabel  = (typeof(filterLabel) != 'undefined' && filterLabel.length > 0) ? filterLabel.text().trim() : '',
			eventAction     = (typeof(actionFilter.data('tealium-action')) != 'undefined') ? actionFilter.data('tealium-action').trim() : '';
			(eventLabel != '' && eventAction != '') ? tagPLPFilter(eventLabel, eventAction) : null;
		}
		
		// dont't run for closing the refinement window
		if ($(e.target).hasClass('breadcrumb-refinement-clear-all')) {
			$('body').removeClass('scrollHidden');
			$('html').removeClass('scrollHidden');
			return;
		}
		// don't intercept for category and folder refinements, as well as unselectable
		if ($(this).parents('.category-refinement').length > 0 || $(this).parents('.folder-refinement').length > 0 || $(this).parent().hasClass('unselectable')) {
			return;
		}
		e.preventDefault();

		/*
		* This fixes the issue of the main site loading into the #main div when no filters are selected (MAT-518)
		*/
		var selectedBreadcrumbs = $('span.breadcrumb-refinement-value').length;
		var selectedRefinements = $('div.refinement ul li.selected').length;
		// handle breadcrumbs
		if ($(e.target).hasClass('breadcrumb-relax')) {
			if (selectedBreadcrumbs === 1) {
				var url = $('span.breadcrumb-refinement-value a').attr('href');
                updateProductListing(url);
				return;
			}
		}
		//Update Pricing in action URL in Case of PLP MVC Refinement AB Test User
		if ($('.plp-filters-mvc').length && !$(this).hasClass('selected-filters__clear-all')) {
			var pmax = '', pmin = '';
			if ($(this).hasClass('breadcrumb-relax-filters') && $(this).hasClass('price-relax')) {
				if (this.href.indexOf('pmax')>-1 && this.href.indexOf('pmin')>-1) {
					this.href = util.removeParamFromURL(this.href, 'pmax');
					this.href = util.removeParamFromURL(this.href, 'pmin');
				}
				var selectedPriceRanges = $(this).siblings('a.price-relax');
				if (selectedPriceRanges.length > 0) {
					$.each(selectedPriceRanges, function(index, value){
					    var priceValues = $(this).data();
					    pmin += (index == selectedPriceRanges.length -1) ? Number(priceValues.valuefrom) : Number(priceValues.valuefrom) + '|';
					    pmax += (index == selectedPriceRanges.length -1) ? Number(priceValues.valueto) : Number(priceValues.valueto) + '|';
					});
				}
			}
			if ($(this).hasClass('price-refinement')) { // In Case of Price Refinement Selection
				var currentRefinementMin = Number($(this).data('valuefrom')),
				currentRefinementMax = Number($(this).data('valueto'));
				if (this.href.indexOf('pmax')>-1 && this.href.indexOf('pmin')>-1) {
					this.href = util.removeParamFromURL(this.href, 'pmax');
					this.href = util.removeParamFromURL(this.href, 'pmin');
				}
				var $currentPrice = $(this).closest('li');
				var selectedPriceRanges = $currentPrice.siblings('li.selected');
				if ($currentPrice.hasClass('selected')) { //In Case Unselect Price Refinement, Exclude Selected Price Refinement from URL
					$.each(selectedPriceRanges, function(index, value){
					    var priceValues = $(this).find('a').data();
					    pmin += (index == selectedPriceRanges.length -1) ? Number(priceValues.valuefrom) : Number(priceValues.valuefrom) + '|';
					    pmax += (index == selectedPriceRanges.length -1) ? Number(priceValues.valueto) : Number(priceValues.valueto) + '|';
					});
				} else { // In Case new Price Range Selection
					$.each(selectedPriceRanges, function(index, value){
					    var priceValues = $(this).find('a').data();
					    pmin += (index == selectedPriceRanges.length -1) ? Number(priceValues.valuefrom) : Number(priceValues.valuefrom) + '|';
					    pmax += (index == selectedPriceRanges.length -1) ? Number(priceValues.valueto) : Number(priceValues.valueto) + '|';
					});
					pmin += (pmin.length > 0) ? '|' + currentRefinementMin : currentRefinementMin;
					pmax += (pmax.length > 0) ? '|' + currentRefinementMax : currentRefinementMax;
				}
			} else if(!$(this).hasClass('price-relax') && $('a.price-refinement').length > 0 && $('a.price-refinement').closest('ul').find('li.selected').length > 0) { // in Case any other price selection to make sure selected prices not miss in search Query URL
				var selectedPriceRefinements = $('a.price-refinement').closest('ul').find('li.selected');
				if (this.href.indexOf('pmax')>-1 && this.href.indexOf('pmin')>-1) {
					this.href = util.removeParamFromURL(this.href, 'pmax');
					this.href = util.removeParamFromURL(this.href, 'pmin');
				}
				$.each(selectedPriceRefinements, function(index, value){
				    var priceValues = $(this).find('a').data();
				    pmin += (index == selectedPriceRefinements.length -1) ? Number(priceValues.valuefrom) : Number(priceValues.valuefrom) + '|';
				    pmax += (index == selectedPriceRefinements.length -1) ? Number(priceValues.valueto) : Number(priceValues.valueto) + '|';
				});
			}
			if (pmin != '' && pmax != '') {
				pmin = pmin.split('|').sort(function(a, b){return a - b}).join('|');	
				pmax = pmax.split('|').sort(function(a, b){return b - a}).join('|');	
				this.href = util.appendParamsToUrl(this.href, {'pmax':pmax, 'pmin':pmin});
			}
		}
		// handle refinements
		var target = $(e.target).closest('li');
		if (target.hasClass('selected') && selectedRefinements === 1) {
			updateProductListing(this.href);//window.location = this.href;
			return;
		}
		/* End MAT-518 fix */
		
		// Handle Mobile Relax Refinements PLP Filters MVC
		if ($('.plp-filters-mvc').length > 0 && ($(this).hasClass('breadcrumb-relax-filters') || $(this).hasClass('selected-filters__clear-all')) && util.isMobileSize()) {
			updateProductListing(this.href, undefined, true);
		} else {
			updateProductListing(this.href);
		}
		if ($(this).closest('.search-result-options.bottom').length > 0) {
			window.location.hash = '#primary';
		}
		//window.location.href = this.href;
	});
	$main.on('click', '.product-swatches-all', function (e) {
		if (util.isMobileSize()) {
			var parentHeight = $(this).parents('.product-tile').height();
			if (!$(this).next().is(':visible')) {
				$(this).parents('.product-tile').removeAttr('style').css('min-height', parentHeight + 'px');
			}
			$(this).next().toggle('show');
		}
	});
	// handle events item click. append params.
	$main.on('click', '.product-tile a:not("#quickviewbutton")', function () {
		var a = $(this);
		// get current page refinement values
		var wl = window.location;

		var qsParams = (wl.search.length > 1) ? util.getQueryStringParams(wl.search.substr(1)) : {};
		var hashParams = (wl.hash.length > 1) ? util.getQueryStringParams(wl.hash.substr(1)) : {};

		// merge hash params with querystring params
		var params = $.extend(hashParams, qsParams);
		if (!params.start) {
			params.start = 0;
		}
		// get the index of the selected item and save as start parameter
		var tile = a.closest('.product-tile');
		var idx = tile.data('idx') ? + tile.data('idx') : 0;

		// convert params.start to integer and add index
		params.start = (+params.start) + (idx + 1);
		// set the hash and allow normal action to continue
		a[0].hash = $.param(params);
	});

	// handle sorting change
	$main.on('change', '.sort-by select', function (e) {
		e.preventDefault();
		var sortTargetUrl = $(this).find('option:selected').val();
		if (SitePreferences.CURRENT_SITE === "Mattress-Firm" && $('.plp-filters-mvc').length) {
			var pmax = '', pmin = '';
			var selectedPriceRefinements = $('a.price-refinement').closest('ul').find('li.selected');
			if (sortTargetUrl.indexOf('pmax')>-1 && sortTargetUrl.indexOf('pmin')>-1) {
				sortTargetUrl = util.removeParamFromURL(sortTargetUrl, 'pmax');
				sortTargetUrl = util.removeParamFromURL(sortTargetUrl, 'pmin');
			}
			$.each(selectedPriceRefinements, function(index, value){
			    var priceValues = $(this).find('a').data();
			    pmin += (index == selectedPriceRefinements.length -1) ? Number(priceValues.valuefrom) : Number(priceValues.valuefrom) + '|';
			    pmax += (index == selectedPriceRefinements.length -1) ? Number(priceValues.valueto) : Number(priceValues.valueto) + '|';
			});
			if (pmin != '' && pmax != '') {
				pmin = pmin.split('|').sort(function(a, b){return a - b}).join('|');	
				pmax = pmax.split('|').sort(function(a, b){return b - a}).join('|');	
				sortTargetUrl = util.appendParamsToUrl(sortTargetUrl, {'pmax':pmax, 'pmin':pmin});
			}
		}	
		updateProductListing(sortTargetUrl);
	})
	.on('change', '.items-per-page select', function () {
		var refineUrl = $(this).find('option:selected').val();
		$('.toggle-grid').css('display', 'none');
		if (refineUrl === 'INFINITE_SCROLL') {
			$('html').addClass('infinite-scroll').removeClass('disable-infinite-scroll');
		} else {
			$('html').addClass('disable-infinite-scroll').removeClass('infinite-scroll');
			updateProductListing(refineUrl);
		}
	});
	var windowW = $(window).width();
	if (windowW >= 768 && SitePreferences.CURRENT_SITE != "1800Mattress-RV") {
		$('.product-name .name-link .name-link-text').each(function () {
			ellipsizeTextBox(this);
		});
	}else if (windowW >= 768 && SitePreferences.CURRENT_SITE == "1800Mattress-RV") {
		$('.product-name .name-link .name-link-text').each(function () {
			ellipsizeTextBoxFor1800(this);
		});
	}

	function onShowSelectedFilters() {
		if ($('.selected-filters').length) {
			$('.results-hits-total').css({'position': 'absolute','bottom': '7px'})
		}
	}

	// PLP Filters MVC AB Test
	function onClickHideMobileFilters() {
		if ($('.plp-filters-mvc').length) {
			$main.on('click', '.close-filter', function (e) {
				e.preventDefault();
				
				$(".mobile-only").trigger('click');
				$('body').removeClass('scrollHidden');
				$('html').removeClass('scrollHidden');
				$("body").removeClass('mobile-refinements-filter-show');
				// in Case if User Click Close Filter, Retail User Previous Selection if User Change any thing
				var currentURLPLPMVC = window.location.href;
				if (sessionStorage['refinedURLState'] != undefined && sessionStorage['refinedURLState'] != currentURLPLPMVC) {
                    updateProductListing(sessionStorage['refinedURLState'], undefined);
				}
			});
		}
	}

	$('.refinement-big-container nav .refinement').last().addClass('refinement-last');
	updatePaginationPosition();
	updateSortPosition();
	updateRefinedByPosition();
	initPlaceHolder();
	drawerMobileOpener();
	mobileCloseRefinement();
	positionGridList();
	onShowSelectedFilters();
	onClickHideMobileFilters();
	$('.primary-logo').focus();

	/*Submit Event for Clearance Page ZipCode Change*/
	$(document).on("submit","#customer-zip-form",function (e) {
		e.preventDefault();
		updateClearanceZipCode();
	});

	/*ZipCode Update Event for Clearance Page*/
	var zipCode = User.customerZip;
	var currentLocationZipCode = User.currentLocationZipCode;
	if(zipCode) {
		$('#customer-zip').val(zipCode);
	}
	else if(currentLocationZipCode) {
		$('#customer-zip').val(currentLocationZipCode);
	}
}

exports.init = function () {
	compareWidget.init();
	if (SitePreferences.LISTING_INFINITE_SCROLL) {
		$(window).on('scroll', infiniteScroll);
	}
	productTile.init();
	productTile.initComfortLevelTagging();
	truncate.init();
	initializeEvents();
	initContainerHeight();
	initRefinementButtons();
	initSubcategoryContent();
	var prevPageLoad = $("input[name='prevPageInfo']").val();
	var nextPageLoad = $("input[name='nextPageInfo']").val();
	$('link[name=prevPageInfo]').remove();
	$('link[name=nextPageInfo]').remove();
	if (prevPageLoad && prevPageLoad.length > 0) {
		$('head').append('<link name="prevPageInfo" rel="prev" href=' + prevPageLoad + '>');
	}
	if (nextPageLoad && nextPageLoad.length > 0) {
		$('head').append('<link name="nextPageInfo" rel="next" href=' + nextPageLoad + '>');
	}
	if ($('.plp-filters-mvc').length) {
		sessionStorage['refinedURLState'] = window.location.href;
	}
	// SHOP-3030 Tag PLP Filters Impression on load
	(User.isParticipantPLPMVCABTest) ? tagPLPFilterImpression() : null;
};
jQuery(function () {
	initTabNav();
});


// key handling
function initTabNav() {
	jQuery ('.menu-category').tabNav({
		items: 'li'
	});
}
/*
 * Accessible TAB navigation
 */
;(function ($) {
	var isWindowsPhone = /Windows Phone/.test(navigator.userAgent);
	var isTouchDevice = ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;

	$.fn.tabNav = function (opt) {
		var options = $.extend({
			hoverClass: 'hover',
			items: 'li',
			opener: '>a',
			delay: 10
		},opt);

		if (isWindowsPhone || isTouchDevice) {
			return this;
		}

		return this.each(function () {
			var nav = $(this), items = nav.find(options.items);

			items.each(function (index, navItem) {
				var item = $(this), navActive, touchNavActive;
				var anchor = $(this).children('a.has-sub-menu , a.no-sub-menu-items');
				var anchorSiblingDiv = anchor.next();
				var link = item.find(options.opener), timer;
				link.bind('focus', function () {
					navActive = nav.hasClass('js-nav-active');
					touchNavActive = window.TouchNav && TouchNav.isActiveOn(navItem);
					if (!navActive || touchNavActive) {
						initSimpleNav();
					}
					item.trigger(navActive && touchNavActive ? 'itemhover' : 'mouseenter');
				}).bind('blur', function () {
					item.trigger(navActive && touchNavActive ? 'itemleave' : 'mouseleave');
				});

				var initSimpleNav = function () {
					if (!initSimpleNav.done) {
						initSimpleNav.done = true;
						item.hover(function () {
							clearTimeout(timer);
							timer = setTimeout(function () {
							item.addClass('open');
							item.addClass(options.hoverClass);
							anchor.addClass('open');
							anchorSiblingDiv.css("display","block");
							}, options.delay);
						}, function () {
							clearTimeout(timer);
							timer = setTimeout(function () {
								item.removeClass('open');
								item.removeClass(options.hoverClass);
								anchor.removeClass('open');
								anchorSiblingDiv.css("display","none");
							}, options.delay);
						});
					}
				};
			});
		});
	};
}(jQuery));
