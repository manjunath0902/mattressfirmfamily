(function (window, document) {
   var __close_message__ = "__PROGRESSIVE_UI_CLOSE__";
   window.ProgHangtag = window.ProgHangtag || {};
   var ProgHangtag = window.ProgHangtag;
   ProgHangtag.CloseApplicationUI = function () {
       window.postMessage(__close_message__, "*");
   };
   ProgHangtag.OpenPaymentEstimator = function (storeId, stateCode, itemCost, requestType, placement) {
       document.body.style.overflow = "hidden";
       var mobileViewThreshold = 1024;
       
       var url = "https://progressivelp.com/eComUI/#/apply/est-settings?storeId=" + storeId + "&itemCost=" + itemCost + "&requestType=" + requestType;
       if (stateCode != null)
           url = url + "&stateCode=" + stateCode;
       var backdrop = document.createElement("div");
       var iframe = document.createElement("iframe");
       backdrop.id = "progressive-backdrop";
       backdrop.zIndex = "1677727";
       iframe.zIndex = "1677728";
       iframe.src = url;
       backdrop.style.opacity = 0;
       iframe.style.opacity = 0;
       backdrop.style.transition = "all 300ms";
       iframe.style.transition = "all 300ms";
       iframe.style.backgroundColor = "#FFFFFF";
       iframe.style.zIndex = "1677728";
       backdrop.style.zIndex = "1677727";
       
       function isMobile() {
           return window.innerWidth < mobileViewThreshold;
       }
       function setMobileStyles() {
           backdrop.style.position = 'fixed';
           backdrop.style.top = 0;
           backdrop.style.left = 0;
           backdrop.style.bottom = 0;
           backdrop.style.right = 0;
           backdrop.style.background = "#000000";
           iframe.style.position = "fixed";
           iframe.style.display = "block";
           iframe.style.left = 0;
           iframe.style.top = 0;
           iframe.style.right = 0;
           iframe.style.bottom = 0;
           iframe.style.width = "100%";
           iframe.style.height = "100%";
           iframe.style.marginLeft = "0px";
           iframe.style.marginTop = "0px";
           iframe.style.borderRadius = "0px";
           iframe.style.border = "none";
       }
       
       function setDesktopStyles() {
           backdrop.style.position = "fixed";
           backdrop.style.top = 0;
           backdrop.style.left = 0;
           backdrop.style.bottom = 0;
           backdrop.style.right = 0;
           
           iframe.style.width = "500px";
           iframe.style.maxHeight = "686px";
           iframe.style.height = "calc(100vh - 80px - 5%)";
           iframe.style.position = "fixed";
           if (placement === "right") {
               iframe.style.right = "8px";
               iframe.style.bottom = 0;
               iframe.style.boxShadow = "-1px -1px 0 0 rgb(223, 227, 232)";
           } else { // style for center placement
               backdrop.style.background = "#000000";
               iframe.style.top = "80px";
               iframe.style.left = "50%";
               iframe.style.marginLeft = "-250px";
               iframe.style.marginTop = "3%";
           }
           iframe.style.border = "1px solid rgb(223, 227, 232)";
       }
       function animateIn() {
           body.appendChild(backdrop);
           body.appendChild(iframe);
           setTimeout(function () {
               backdrop.style.opacity = 0.5;
               setTimeout(function () {
                   iframe.style.opacity = 1;
               }, 200);
           }, 1);
       }
       function animateOut() {
           iframe.style.opacity = 0;
           backdrop.style.opacity = 0;
           setTimeout(function() {
                   document.body.removeChild(iframe);
                   document.body.removeChild(backdrop);
                   document.body.style.overflow = null;
               },
               300);
       }
       function updateStyles() {
           window.innerWidth >= mobileViewThreshold ? setDesktopStyles() : setMobileStyles();
       }
       window.addEventListener('resize', updateStyles);
       var body = document.body;
       updateStyles();
       animateIn();
       function handleFrameMessage(msg) {
           if (msg.data === __close_message__) {
               animateOut();
               window.removeEventListener('message', handleFrameMessage);
           }
       }
       window.addEventListener('message', handleFrameMessage);
   };
})(window, document)
