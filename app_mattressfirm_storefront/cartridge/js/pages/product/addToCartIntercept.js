'use strict';

var dialog = require('./../../dialog'),
	page = require('./../../page'),
	util = require('./../../util'),
	minicart = require('../../minicart');

var hideSwatches = function () {
	$('.recommendation-item:not([data-producttype="master"]) .swatches li').not('.selected').not('.variation-group-value').hide();
	// prevent unselecting the selected variant
	$('.recommendation-item .swatches .selected').on('click', function () {
		return false;
	});
};
$.fn.equalizeHeights = function () {
    var maxHeight = this.map(function (i, e) {
        return $(e).height();
    }).get();
    return this.height(Math.max.apply(this, maxHeight));
}; 

function calculateRecHeight() {
    var cnt = 0, step = 4, rowNum = 1;
    if ($(window).width() < 768) {
        step = 2;
    }
    $('.product-recommendations .product-variations').each(function () {
        if (cnt == (rowNum * step)) {
            rowNum++;
        }
        if (cnt < (rowNum * step)) {
            $(this).addClass('recrow' + rowNum)
        }
        cnt++;
    });

    for (var index = 1; index <= rowNum; index++) {
        $('.recrow' + index).equalizeHeights();
        $('.recrow' + index).css('margin-bottom','20px');
    }
}
/**
 * @function
 * @description Intialize Recommendation Callbacks, plugins to beautify drop downs and calculate content height.
 */
function initRecommendationGrid() {
	initializeRecommendationGrid();
    util.uniform();
    calculateRecHeight();
}
/**
 * @function
 * @description Wait for the Einstein Recommendation contnet then intialize it and remove the loader.
 */
function waitForRecommendations() {
	if (window.jQuery && document.querySelector(".recomm-cart-cross-sell .product-recommendations") !== null) {
		initRecommendationGrid();
		$(".addtocart-recom-web .loader-indicator-addtocart").length > 0 ? $(".addtocart-recom-web .loader-indicator-addtocart").remove() : null;
	} else {
		window.setTimeout(waitForRecommendations, 100);
	}
}

function addToCartRecommendations() {
	if($(".addtocart-recom-web").length > 0){
		$(".addtocart-recom-web").append('<div class="loader-indicator-addtocart"></div>');
		var pid = $('#pid').val();
		var url = util.appendParamsToUrl(Urls.addToCartRecommendations, {productId: pid,qty: 1, format: 'ajax'});
		
		$.ajax({
			url: url,
			success: function (response) {
				
				//$(".addtocart-recom-web").remove();
				$(".addtocart-recom-web").html(response);
				//SHOP-2277 In case we dont have explicit Recommendations, then show Einstien Recommendations.
				var isEinsteinRecom = ( typeof $(".recomm-cart-cross-sell") != "undefined" && $(".recomm-cart-cross-sell").length > 0 && $(".recomm-cart-cross-sell").attr("data-einstein-recomm") === "true" && $('.recomm-cart-cross-sell').children().length > 0 );
				if (isEinsteinRecom) {
					//if Einstien available then another ajax call happened thats why turn on loader and stop when got response in waitForRecommendations()
					$(".addtocart-recom-web").append('<div class="loader-indicator-addtocart"></div>');
					waitForRecommendations();
				} else {
					initRecommendationGrid();
				}
			}
		});
	}
}

function initializeRecommendationGrid () {
	var $recommendationList = $('.add-to-cart-intercept-wrapper .recommendations');
	$recommendationList.on('change', '.swatches', function (e) {
		e.preventDefault();
		var url = $(this).val(),
			$this = $(this);
		url = util.appendParamsToUrl(url, {
			'source': 'recommendation',
			'format': 'ajax'
		});
		$.ajax({
			url: url,
			success: function (response) {
				$this.closest('.recommendation-item').empty().html(response);
				util.uniform();
				calculateRecHeight();
			}
		});
	})
	.on('click', '.add-recommendations-item', function (e) {
		e.preventDefault();
		var pid = $(this).closest('form').find('input[name="pid"]').val();
		var url = util.appendParamsToUrl(util.ajaxUrl(Urls.addProduct), {pid: pid, source: 'recommendation'});
		var $this = $(this);
		var recItem = $this.closest('li');		
		
		// make the server call
		$.ajax({
			type: 'POST',
			cache: false,
			url: url,
			data: $this.closest('form').serialize()
		})
		.done(function (response) {
			minicart.show(response);
			//$this.next('.add-to-cart-resp').text('Added to Cart');
			$this.replaceWith("<div class='added'>ADDED</div>" );
			if($('.cart-total').length == 1) {
				$('.cart-total').text($($('.mini-cart-subtotals').children('.value')[0]).text());
			}
		})
		.fail(function (xhr, textStatus) {
			if (textStatus === 'parsererror') {
				//window.alert(Resources.BAD_RESPONSE);
				console.error(Resources.BAD_RESPONSE);
			} else {
				//window.alert(Resources.SERVER_CONNECTION_ERROR);
				console.error(Resources.SERVER_CONNECTION_ERROR);

			}
		});
		
		//MAT-1421 - Create GA tag for cross sell product
		var prodNamewithBrand = recItem.data('brandname')+ ' '+recItem.data('productname');
		var ProdIndex = Number(recItem.index())+1;
		var productURL = recItem.find('div.product-image a').attr('href'); 
		var productImageURL= recItem.find('div.product-image img').attr('src'); 
		var size= recItem.find('select.swatches.size option:selected').text().trim();
		var quantity = recItem.find('.quantity-dropdown option:selected').val();
		var unitPrice = recItem.find('p.priceWrapper span.price-sales').text().trim().replace(/[^0-9.-]+/g,"");
		var TotalPrice = parseFloat(unitPrice) * Number(quantity);
		var productName = recItem.data('productname');
		var productBrand = recItem.data('brandname');
		var productCategory = recItem.find('.see-more').data('category');
		var productSku= recItem.data('productsku');
		var productChildSku= recItem.data('productchildsku');
		var productId = recItem.data('masterproductid');

		if(utag) {
			utag.link({ 
				"enhanced_actions"		:	"add", 
				"eventCategory"			:	["Cart Cross Sell Products"], 
				"eventLabel"			:	["POS" + ProdIndex + " - " + prodNamewithBrand], 
				"eventAction" 			:	["click"],
				"product_id"			:	[productId],
	            "product_name"			:	[productName],
	            "product_brand"			:	[productBrand],
	            "product_quantity"		:	[quantity],
	           	"product_price"			:	[TotalPrice],
	            "product_unit_price"	:	[unitPrice],
	            "product_category"		:	[productCategory],
	            "product_sku"			:	[productSku],
	            "product_child_sku"		:	[productChildSku], 
	            "product_img_url"		:	[productImageURL],
	            "product_url"			:	[productURL],
	            "product_size"			:	[size]
			});
		}
			
	});
}

var addToCartIntercept = {
	show: function () {
			$('.price-standard').next().css("color", "#d63426");
			$('.Adjusted-Price').prev().css( "margin-left", "5px" );
			var block = $('#mini-cart .add-to-cart-intercept-wrapper');
			addToCartRecommendations();
			
			if (block.size() > 0) {
				dialog.open({
					html: block,
					options: {
						width: 840,
						title: $('.intercept-title').val(),
						dialogClass: 'add-to-cart-intercept-dialog',
						close: function () {
							$(this).find('.add-to-cart-intercept-wrapper').remove();
						},
						open: function () {
							var dialog = $(this);
							$('.ui-dialog-buttonpane').remove();
							util.uniform();
							$(this).find('a.continue').on('click', function (e) {
								e.preventDefault();
								location.reload(true);
								dialog.dialog('close');
							});
						}
					}
				});
			} 
	}
};

module.exports = addToCartIntercept;