'use strict';

var account = require('./account'),
    bonusProductsView = require('../bonus-products-view'),
    quickview = require('../quickview'),
    cartStoreInventory = require('../storeinventory/cart'),
    util = require('../util'),
    page = require('../page'),
    dialog = require('../dialog');

/**
 * @private
 * @function
 * @description Binds events to the cart page (edit item's details, bonus item's actions, coupon code entry)
 */
function initializeEvents() {
	
    $('#cart-table').on('click', '.item-edit-details a', function (e) {
        e.preventDefault();
        quickview.show({
            url: e.target.href,
            source: 'cart'
        });
    })
    .on('click',  '.item-details .bonusproducts a', function (e) {
        e.preventDefault();
        bonusProductsView.show(this.href);
    });
    $(document).on('click', '.amazonpay-button-inner-image', function(e) {
    	 e.preventDefault();
    	 ga('send', 'event', { eventCategory: 'UX-Pay', eventAction: 'click', eventLabel: 'AmazonPay', eventValue: 1});
    	 return true;
    });
    $(document).ready(function() {
	    if($(".amazon-btn.amazon-btn-right").length > 0) {
	    	$(".amazon-btn.amazon-btn-right img").attr("role" , "button");
	    }
    });
    $('#cart-items-form, #cart-items-form-os').on('click', '.bonus-item-actions a', function (e) {
        e.preventDefault();
        bonusProductsView.show({
        	url: this.href
        });
    });
    
    // override enter key for coupon code entry
    $('form input[name$="_couponCode"]').on('keydown', function (e) {
        if (e.which === 13 && $(this).val().length === 0) { return false; }
    });
    
    if (!util.isMobileSize()) {
        $('.cart-footer-slot').appendTo('.cart-footer');
    }
    /*$('.quantity-dropdown').on('change', function () {
        $('#update-cart').trigger('click');
    });*/
    //MAT-557 binding event when coupon code response renders on cart
    // if this code uncommented then comment50 to 52 line number
    $('.primary-content').on('change','.quantity-dropdown', function () {
        $('#update-cart').trigger('click');
    });
    $('.primary-content-old').on('click', '.bonus-item-actions a', function (e) {
    	e.preventDefault();
        var $bonusProduct = $('#bonus-product-dialog');
        //bonusProductsView.show(this.href);
        var dialogWidth = $(this).parent().prev().val() || 650;
        // 'dialogueWidth' is passed as a paramater in the href of anchor tag (Choice of Bonus Product button) from cart.isml
        var dwidth = util.getParamValueFromURL($(this).attr('href'), 'dialogueWidth');
		if (dwidth != '') {
			dialogWidth = Number(dwidth);
		}
		// create the dialog
		dialog.open({
			target: $bonusProduct,
			url: this.href,
			options: {
				width: dialogWidth,
				title: Resources.BONUS_PRODUCTS
			},
			callback: function () {
				initializeGrid();
				hideSwatches();
				util.uniform();
				$(".ui-dialog-title").show();
				$(".ui-dialog").addClass('bonus-product-popup');
				amplience.initManyProductImages("", $('.col-1'));
				$(".ui-dialog").find('a.continue').on('click', function (e) {
					e.preventDefault();
					$('.ui-dialog-titlebar-close').trigger('click');				
				});
				$(".ui-dialog").find('.ui-icon-closethick').on('click', function (e) {
					e.preventDefault();					
					//page.refresh();
				});
			}
		});
    });
    //for bonus product button
    if($('.cart-btns-holder .bonus-item-actions').length == 0) {
		$('tr.cart-row-btns, td.cart-row-btns-ab').css('display' , 'none');
	}
}


var addPersonaliCartRescue = function (netotiate_offer_id) {
	var options = {
		url: Urls.addCartRescuePriceAdjustments,
		data: {netotiateOfferId: netotiate_offer_id},
		type: 'POST'
	};
    $.ajax(options).done(function (data) {
        if (typeof(data) !== 'string') {
            if (data.success) {
                //dialog.close();
                //page.refresh();
            } else {
                window.alert(data.message);
                return false;
            }
        } else {
            console.log(data);
            //dialog.close();
            page.refresh();
        }
    });
}
var cart = {
	init : function () {
    initializeEvents();
    var $addCoupon = $('#add-coupon');
	var $couponCode = $('input[name$="_couponCode"]');
	var $checkoutForm = $('.checkout-billing');	
    if (SitePreferences.STORE_PICKUP) {
        cartStoreInventory.init();
    }
    account.initCartLogin();
    $("body").on('click', '.change-store', function (e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href'),
            title: $(e.target).attr('title'),
            options: {
                height: 600
            }
        });
    });
    $("body").on('click', '.select-store', function (e) {
        var selectedStore = $(e.target).val();
        setPreferredStore(selectedStore);
        $("div[class*='store']").removeClass('selected');
        $('.store_' + selectedStore).addClass('selected');
        $(".select-store:contains('Preferred Store')").closest('button').first().text('Select Store');
        $(e.target).text('Preferred Store');
    });

    $('body').on('submit', '#dwfrm_pickupinstore', function (e) {
        e.preventDefault();
        // serialize the form and get the post url
        var buttonName = $('#dwfrm_pickupinstore').find('.pickupinstore').attr('name');
        var options = {
            url: Urls.pickupInStore,
            data: $('#dwfrm_pickupinstore').serialize() + '&' + buttonName + '=x',
            type: 'POST'
        };
        $.ajax(options).done(function (data) {
            if (typeof(data) !== 'string') {
                if (data.success) {
                    //dialog.close();
                    //page.refresh();
                } else {
                    window.alert(data.message);
                    return false;
                }
            } else {
                $('#dialog-container').html(data);
                //dialog.close();
                //page.refresh();
            }
        });

    });   
    
    $(document).on("click","#showCouponDisclaimerMsgMobile", function(e){
    	e.preventDefault();
    	/*$(this).next().addClass('active');*/
    	 var $html = $(this).next().find('.couponDisclaimerMsg').children('.promo-modal').html();
    	 dialog.open({
				html: $html,
				options: {
					autoOpen: false,
					dialogClass: 'promo-modal-class'
					
				}
			});
    });
    
    /*$(document).on("click","#hideCouponDisclaimerMsg-mob", function(e){
    	e.preventDefault();
    	$(this).closest('.mobileCouponDisclaimerMsg-popup').removeClass('active');
    });*/
    
    $(document).on("click","#showCouponDisclaimerMsg", function(e){
    	e.preventDefault();
        $('.couponDisclaimerMsg').slideDown("slow");
    });
     
    $(document).on("click","#hideCouponDisclaimerMsg", function(e){
    	e.preventDefault();
    	$('.couponDisclaimerMsg').slideUp("slow");
    });
    
    //MAT-557 coupon code default behavior to ajax
  $(".primary-content").on('click',"#add-coupon",  function (e) {
	  e.preventDefault();	
	  if ($('.new-cart-mob').length > 0) {
		utag.link({ eventCategory: 'Cart CTA', eventLabel: 'Enter Promo Code', eventAction: 'Cart' });
	  } else if ($('.isMobileDevice').length > 0 && $('.isMobileDevice').val() == 'true') {
		utag.link({ eventCategory: 'Cart CTA - old', eventLabel: 'Enter Promo Code', eventAction: 'Cart' });
	  }
		addHiddenInput( this.name );
    	submitCartAction(e);
	});
  
  $(".primary-content").on('click',"#deleteCoupon, #deleteCoupon-mob",  function (e) {
		e.preventDefault();	
		addHiddenInput( this.name );
		submitCartAction(e);
	});
  
  //to prevent multiple submissions of the form when removing a product from the cart
  var removeItemEvent = false;
  $('button[name$="deleteProduct"]').on('click', function (e) {
      if (removeItemEvent) {
          e.preventDefault();            
      } else {
          removeItemEvent = true;
      }
      addHiddenInput( this.name );
      submitCartAction(e);
  });

	// trigger events on enter
   $(".primary-content").on('keydown','input[name$="_couponCode"]', function (e) {
	   e.stopPropagation();
	   if (e.which === 13) {
			e.preventDefault();
			$("#add-coupon").click();
		}
	});
   
   // open First Selection Program Modal
	$('.select-fsp-items').unbind('click'); 
	$('.select-fsp-items').on('click', function (e) {
		e.preventDefault();
		cart.openFSPDialog({
		    	url: this.href
		});
	});
	
	// Remove price adjustment from cart
	$(document).on('click',".remove-fsp-discount",function(e){
		e.preventDefault();
		var productID = $(this).attr('data-pid');
		if(productID.length > 0) {
			var url = util.appendParamsToUrl(Urls.removeFSPPriceAdjustments, {productID: productID});
	 		
			// make the server call
	 		$.ajax({
	 			type: 'POST',
	 			dataType: 'json',
	 			cache: false,
	 			contentType: 'application/json',
	 			url: url
	 		})
	 		.done(function () {
	 			page.refresh();
	 		})
	 		.fail(function (xhr, textStatus) {
	 			// failed
	 			if (textStatus === 'parsererror') {
	 				window.alert(Resources.BAD_RESPONSE);
	 			} else {
	 				window.alert(Resources.SERVER_CONNECTION_ERROR);
	 			}
	 		});
		}
	});
	
/* 
 // This code has been written in app.js because it was needed on multiple pages
 //Add Promo Code on Order Summary
   $('.primary-content').on('click', '#add-promo-code-label-id', function (e) {
   	e.preventDefault();
   	$('#add-promo-code-label-id').addClass('visually-hidden');
   	$('#add-promo-code-input-id').removeClass('visually-hidden');
   });
*/   
   
// #coupon-success-message is the id of overlay which comes after bonus product addition in basket via coupon code.
   $('#coupon-success-message').on('click tap', function (e) {
       e.preventDefault();
       $('#coupon-success-message').hide();
   });
	//#promo-success-message is the id of overlay which comes after discount applied on basket via coupon code.
   $('#promo-success-message').on('click tap', function (e) {
	   	e.preventDefault();
	   	$('#promo-success-message').hide();
   });

   var submitCartAction  = function (e){
		e.preventDefault();
		var $form =  $('#cart-items-form, #cart-items-form-os');
		var ajaxdest = $form.attr('action');
		var url = util.appendParamsToUrl(ajaxdest, {format: 'ajax'});
		var formData = $(":input").serialize(); //$form.serialize();
		$.post( url, formData )
		.done(function( text ){
			var $response = $( text );
			var divcontent = $('.item-quantity.item-quantity-details');
			var cartAction = $response.filter('div.cart-content-wrapper') != null && $response.filter('div.cart-content-wrapper').data('action') !=null ? $response.filter('div.cart-content-wrapper').data('action') : '';
			var couponError = $($response).find("#couponError").length>0 ? $($response).find("#couponError").val() : '';
			
			if(couponError === "true"){				
				$("#add-promo-code-input-id").replaceWith($($response).find("#add-promo-code-input-id"));
				$(".promo-code-label-wrapper .label").css("color" , "#d63426");
			} else {
			
			/*if($('#couponApplied').length > 0 && $('#couponApplied').val() == "true" && $('#couponError').val() == "false") {
				if($('#couponType').val() == "BONUS") {
					$(function(){
						page.refresh();
						// #coupon-success-message is the id of overlay which comes after bonus product addition in basket via coupon code.
						$('#coupon-success-message').show();
					    setTimeout(function() {
						    $('#coupon-success-message').fadeOut('fast');
						    page.refresh();
						}, 2000);
					});
				} else {
					$(function(){
						page.refresh();
						//#promo-success-message is the id of overlay which comes after discount applied on basket via coupon code.
						$('#promo-success-message').show();
						setTimeout(function() {
						    $('#promo-success-message').fadeOut('fast');
						    page.refresh();
						}, 2000);
					});
				}
			}*/
			$(".promo-code-label-wrapper .label").css("color" , "#2d2926");
			$($response).find('.item-quantity.item-quantity-details').each(function(e) {
				$(this).html($(divcontent[e]).html());
			});			
			$($response).find("#checkout-button-ID").removeClass('disabled-btn');
			$($response).find("#checkout-button-ID-ab").removeClass('disabled-btn');
						
            var bonusProducts = $response.filter('div.cart-content-wrapper') != null && $response.filter('div.cart-content-wrapper').data('bonusproducts') !=null ? $response.filter('div.cart-content-wrapper').data('bonusproducts') : null;
			
			if(bonusProducts != null && cartAction === 'addcoupon') {
					addBonusProducttUtag(bonusProducts);
			}
			else if(bonusProducts != null && (cartAction === 'deleteCoupon' || cartAction === 'deleteCoupon-mob')) {				
				removeProducttUtagByJson(bonusProducts);								   
			}
			else if(bonusProducts != null && cartAction === 'deleteProduct') {				
				removeProducttUtagByJson(bonusProducts);				
			}
			
			if(couponError === "false" || cartAction === 'addcoupon' || (cartAction === 'deleteCoupon' || cartAction === 'deleteCoupon-mob') ||  cartAction === 'deleteProduct') {			
				page.refresh();
			} else {
				$(".primary-content").html($response);
				$(".promo-code-label-wrapper .label").css("color" , "#d63426");
			}
			
		}
			
		});	
	};
	
	/***
	 * Send Bonus Products Details to Tealium
	 * @returns
	 */
	function addBonusProducttUtag(bonusProducts) {
		if(utag) {
			var index = 0;
			if(bonusProducts != null){
				while (bonusProducts[index]) {
					var bonusProduct = bonusProducts[index];
					var productImageURL = bonusProduct.productexternalid != 0 ? 'https://' + SitePreferences.amplienceHost +'/i/hmk/'+ bonusProduct.productexternalid+'?h=1220&w=1350' : '';
					var productURL = util.appendParamsToUrl(Urls.getHttpProductUrl, {"pid": bonusProduct.variantid});
					if(typeof utag != undefined && utag != null ){ 
						utag.link({ 
							"enhanced_actions"		:	"add", 
							"product_id"			:	[bonusProduct.productid],
				            "product_name"			:	[bonusProduct.productname],
				            "product_brand"			:	[bonusProduct.brand],
				            "product_quantity"		:	[bonusProduct.quantity],
				           	"product_price"			:	["0.00"],
				            "product_unit_price"	:	["0.00"],
				            "product_category"		:	[bonusProduct.productcategory],
				            "product_sku"			:	[bonusProduct.productsku],
				            "product_child_sku"		:	[bonusProduct.productchildsku], 
				            "product_img_url"		:	[productImageURL],
				            "product_url"			:	[productURL],
				            "product_size"			:	[bonusProduct.productsize]
						});
					}					
					index++;
				}
			}
		}
	}
	function removeProducttUtagByJson (productsJson) {
		if(utag) {		
			var index = 0;
			if(productsJson != null){
				while (productsJson[index]) {
					var product = productsJson[index];
					var productImageURL = product.productexternalid != 0 ? 'https://' + SitePreferences.amplienceHost +'/i/hmk/'+ product.productexternalid+'?h=1220&w=1350' : '';
					var productURL = util.appendParamsToUrl(Urls.getHttpProductUrl, {"pid": product.variantid});
					if(typeof utag != undefined && utag != null ){ 
						utag.link({ 
							"enhanced_actions"		:	"remove", 
							"product_id"			:	[product.productid],
				            "product_name"			:	[product.productname],
				            "product_brand"			:	[product.brand],
				            "product_quantity"		:	[product.quantity.toString()],
				           	"product_price"			:	["0.00"],
				            "product_unit_price"	:	["0.00"],
				            "product_category"		:	[product.productcategory],
				            "product_sku"			:	[product.productsku],
				            "product_child_sku"		:	[product.productchildsku], 
				            "product_img_url"		:	[productImageURL],
				            "product_url"			:	[productURL],
				            "product_size"			:	[product.productsize]
						});
					}
					
					index++;
				}
			}
		}
	}
	
	function addHiddenInput( name ){
		var $form =  $('#cart-items-form, #cart-items-form-os');
	    $form.append( "<input type='hidden' class='form_submittype' name='" + name + "' value='true'>" );
	  }
	

    $("body").on('click', '.continue-with-select-store', function (e) {
        var selectedStore = $('.selected').attr('data-storeid');
        var pid = $('input[name="pid"]').val();
        var format = 'ajax';
        addProductSelectStore(selectedStore, pid, format);
    });
    global.addPersonaliCartRescue = addPersonaliCartRescue;
},

openFSPDialog: function (params) {
	
	var $fspProduct = $('#fsp-product-dialog');
	// create the dialog
	dialog.open({
		target: $fspProduct,
		url: params.url,
		options: {
			width: 'auto', 
			maxWidth: 920,
			height: 'auto',				
			modal: true,
			fluid: true, 
			resizable: false,
			draggable: true,
			responsive: true,
			autoResize:true,
			open: function () {
				
				$(".fsp-checkboxes").each(function() {
					if($(this).attr("data-optFirstSelectionProgram").length > 0) {
						var optFirstSelectionProgram = $(this).attr("data-optFirstSelectionProgram");
						if(optFirstSelectionProgram == "true") { 
							$(this).prop('checked', 'checked');
			            } else if(optFirstSelectionProgram == "false"){
			            	$(this).removeAttr('checked');
			            }
					}
		        });
			}
			
		},
		callback: function () {
			cart.initFSPModalEvents();
			util.uniform();
			$(".ui-dialog").addClass('fsp-discount-popup');
		}
	});			
},

checkStatusOfCheckboxes: function() {
	var isCheckBoxesChecked = false;
	$(".fsp-checkboxes").each(function() {
		if($(this).is(':checked')) {
			isCheckBoxesChecked = true;
			return false;
        }
    });
	return isCheckBoxesChecked;
},

checkFSPValidations: function(origin) {
	   var isCheckBoxesChecked = cart.checkStatusOfCheckboxes();
	   var isTermsConditionsChecked = $('#termsandconditions').is(':checked');
	   if(isTermsConditionsChecked && isCheckBoxesChecked){
		   $("#fsp-modal-button").removeClass('btn-fsp-disabled');
		   $("#fsp-modal-button").addClass('btn-default');
		   $(".checkbox-terms-conditions").removeClass('error');
		   $(".fsp-submit-button").removeClass('error');
		   return true;
	   }else if(isTermsConditionsChecked && !isCheckBoxesChecked){
		   $("#fsp-modal-button").removeClass('btn-default');
		   $("#fsp-modal-button").addClass('btn-fsp-disabled');
		   $(".checkbox-terms-conditions").removeClass('error');
		   $(".fsp-submit-button").removeClass('error');
		   return false;
	   } 
	   else {
		   $("#fsp-modal-button").removeClass('btn-default');
		   $("#fsp-modal-button").addClass('btn-fsp-disabled');
		   if(origin == 'terms' || origin == 'button') {
			   $(".checkbox-terms-conditions").addClass('error');
			   $(".fsp-submit-button").addClass('error');
		   }
		   return false;
	   }
	   return;
},

initFSPModalEvents: function () {

		$('#fsp-modal-button').on('click', function (e) {
	    	e.preventDefault();
	    
	    	var validationStatus = cart.checkFSPValidations('button');
	    	
	    	if(validationStatus){
	    		
	    	    $("#fsp-modal-button").removeClass('btn-default');
	  		    $("#fsp-modal-button").addClass('btn-fsp-disabled');
	  		    
		    	var fspProdctsArray = [];
				$(".fsp-checkboxes").each(function() {
					if($(this).is(':checked')) {
						var productId = $(this).attr("data-pid");
						var fspdiscount = $(this).attr("data-fspdiscount");	
						
						var fspObject = new Object();
						fspObject.productId = productId;
						fspObject.fspdiscount = fspdiscount;
						fspProdctsArray.push(fspObject);
						
		            }
		        });
				
		 		// make the server call
		 		$.ajax({
		 			type: 'POST',
		 			dataType: 'json',
		 			cache: false,
		 			contentType: 'application/json',
		 			url: Urls.addFSPPriceAdjustments,
		 			data: JSON.stringify(fspProdctsArray)
		 		})
		 		.done(function () {
		 			dialog.close();
		 			page.refresh();
		 		})
		 		.fail(function (xhr, textStatus) {
		 			// failed
		 			if (textStatus === 'parsererror') {
		 				window.alert(Resources.BAD_RESPONSE);
		 			} else {
		 				window.alert(Resources.SERVER_CONNECTION_ERROR);
		 			}
		 		});
	    	}
	    });
		
		$('#termsandconditions').click(function () {
			cart.checkFSPValidations('terms');
		});
		
		$('#no-thanks').click(function () {
			dialog.close();
		});
		
		$(document).on('change', '[type=checkbox]', function(event) {
			cart.checkFSPValidations('product');
		});

}

};
module.exports = cart;