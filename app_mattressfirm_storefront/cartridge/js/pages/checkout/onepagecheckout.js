'use strict';

var ajax = require('../../ajax'),
    formPrepare = require('./formPrepare'),
    progress = require('../../progress'),
    util = require('../../util'),
    page = require('../../page'),
    dialog = require('../../dialog'),
    deliverycalender = require('./deliverycalender');

/**
 * @function
 * @description updates the order summary based on a possibly recalculated basket after a shipping promotion has been applied
 */
function updateSummary() {
    var $summary = $('#secondary.summary');
    // indicate progress
    progress.show($summary);

    // load the updated summary area
    $summary.load(Urls.summaryRefreshURL, function () {
        // hide edit shipping method link
        $summary.fadeIn('fast');
        $summary.find('.checkout-mini-cart .minishipment .header a').hide();
        $summary.find('.order-totals-table .order-shipping .label a').hide();
        $('#total_amount').text($summary.find('.order-value').text());
        var totalAmount = $summary.find('.order-value').text();
        $("#transAmount1").attr("value", totalAmount.replace(/[$,]/g,''));
        $('#total_amount_monthly').text($summary.find('.order-value').text());
        //$('.checkout-mini-cart-wrapper').mCustomScrollbar();
        $('.checkout-mini-cart').mCustomScrollbar();
        util.uniform();
        isValidBillingForm();
        $(window).scrollTop($(window).scrollTop()+1);
    });
}

function updateOPCdeliverySummarySection(zipcode) {
	 var deliverySummarySection = $('#deliverysummaryblock');
	    // indicate progress
	 $('.loader-checkout-delivery-date-section').show();
	 var url = util.appendParamsToUrl(Urls.updateDeliverySummary, {zipCode: zipcode});
     deliverySummarySection.load(url, function (responseTxt, statusTxt, xhr) {
         $('.loader-checkout-delivery-date-section').hide();
         updateSummary();
         util.uniform();
         if(statusTxt == "success") {
        	 	
        	 	if ($("#deliverysummaryblock").find("div.restricted-state, div.block-checkout-nationwide-shipping").length > 0) {
        	 		$('#restrict_submit_order').show();
        	 		$('#minisummarySubmitOrderButton').attr('disabled', 'disabled');
        	 		$('#submit_order').hide();
        	 		$('#synchrony_submit_order').hide();
        	 	} else {
        	 		var selectedPaymentMethod = $('.payment-method-options').find(':checked').val();
        	 		$('#restrict_submit_order').hide();
        	 		if (selectedPaymentMethod=='CREDIT_CARD') {
        	 			$('#submit_order').show();
        	 			$('#synchrony_submit_order').hide();
        	 		} else if (selectedPaymentMethod=='SYNCHRONY_FINANCIAL') {
        	 			$('#submit_order').hide();
        	 			$('#synchrony_submit_order').show();
        	 		}
        	 	}
        	 	//On zipcode focus out When delivery zone changes we should uncheck scheduleLater checkbox which will make form invalid.
        	 	//Why?
        	 	//Because scheduleLater is no longer visible and User should open popup to schedule his delivery which make form valid.
        	 	if ($('#dwfrm_billing_billingAddress_scheduleLater').parent().hasClass('checked') == true) {
					$('#dwfrm_billing_billingAddress_scheduleLater').click();
				}
                if ($('#dwfrm_billing_billingAddress_scheduleLater').length > 0) {
                	isValidBillingForm();
                }
             }
         });
}

function resetdeliverySummarySection() {
	 var deliverySummarySection = $('#deliverysummaryblock');
	    // indicate progress
	 $('.loader-checkout-delivery-date-section').show();
	 var url = util.appendParamsToUrl(Urls.resetDeliverySummary);
    deliverySummarySection.load(url, function (responseTxt, statusTxt, xhr) {
        $('.loader-checkout-delivery-date-section').hide();
        updateSummary();
        util.uniform();
        if(statusTxt == "success") {
       	 	if ($("#deliverysummaryblock").find("div.restricted-state, div.block-checkout-nationwide-shipping").length > 0) {
       	 		$('#restrict_submit_order').show();
       	 		$('#minisummarySubmitOrderButton').attr('disabled', 'disabled');
       	 		$('#submit_order').hide();
       	 		$('#synchrony_submit_order').hide();
       	 	} else {
       	 		var selectedPaymentMethod = $('.payment-method-options').find(':checked').val();
       	 		$('#restrict_submit_order').hide();
       	 		if (selectedPaymentMethod=='CREDIT_CARD') {
       	 			$('#submit_order').show();
       	 			//$('#billingSubmitButton').show();
       	 			$('#synchrony_submit_order').hide();
       	 		} else if (selectedPaymentMethod=='SYNCHRONY_FINANCIAL') {
       	 			$('#submit_order').hide();
       	 			$('#synchrony_submit_order').show();
       	 		}
       	 	}
       	 	//On zipcode focus out When delivery zone changes we should uncheck scheduleLater checkbox which will make form invalid.
       	 	//Because scheduleLater is no longer visible and User should open popup to schedule his delivery which make form valid.
       	 	if ($('#dwfrm_billing_billingAddress_scheduleLater').parent().hasClass('checked') == true) {
					$('#dwfrm_billing_billingAddress_scheduleLater').click();
				}
               if ($('#dwfrm_billing_billingAddress_scheduleLater').length > 0) {
               	isValidBillingForm();
               }
            }
        });
}

$(document).on('change', '#dwfrm_billing_billingAddress_scheduleLater , #dwfrm_singleshipping_shippingAddress_addressFields_states_state', function() {
	if($('#dwfrm_billing_billingAddress_scheduleLater:checkbox:checked').length > 0){
		utag.link({ "eventCategory" : ["ATP"], "eventLabel" : ["Schedule_Later"], "eventAction" : ["click"] });
	}
	isValidBillingForm();
});

$("form#dwfrm_billing input[type=text],input[type=tel]").each(function(){
	$(this).on('focusout', function() {
	 	isValidBillingForm();
	});
 });


function LoadShippingCityStateDropdowns(data) {
    var checkoutForm = $("form.address");
    var postalCode = checkoutForm.find("input[name$='_shippingAddress_addressFields_postal']");
    var city = checkoutForm.find('input[name$="_shippingAddress_addressFields_city"]');
    var state = checkoutForm.find('select[name$="_shippingAddress_addressFields_states_state"]');
    var cityJSON = checkoutForm.find("input.cityJSON");
    if (data !== null) {
        city.empty();
        for (var __i = 0; __i < data.cities.length; __i++) {
            
            city.val(data.cities[__i].city);

            if (__i === 0) {
                state.val(data.cities[__i].state);

                // Reset any error conditions on the state field
                state.closest(".form-row").removeClass("error").find(".error-message").hide();

                // State selected so hide drop down
                $(".state-col .dk_options_inner").css("display","none");
            }
        }
        // Enable the city/state select elements so they are submitted with form.
        city.removeAttr("disabled");
        state.removeAttr("disabled");
    }
}

function GetShippingCityStateFromZip(zipCode, updateSummaryFlag) {
    var checkoutForm = $("form.address");
    var postalCode = checkoutForm.find("input[name$='_shippingAddress_addressFields_postal']");
    var city = checkoutForm.find('input[name$="_shippingAddress_addressFields_city"]');
    var state = checkoutForm.find('select[name$="_shippingAddress_addressFields_states_state"]');
    var country = checkoutForm.find('select[name$="_shippingAddress_addressFields_country"]');
    var cityJSON = checkoutForm.find("input.cityJSON");
    // 1. Verify a valid US 5-digit ZIP first, and that the same ZIP was not entered again
    if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCode)) { //&& zipCode != app.clientcache.SESSION_ZIP){

        postalCode.removeClass('error');
        postalCode.next('span.error').remove();
        $.ajax({
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: Urls.cityAndStateForZip,
            async: false,
            cache: false,
            data: {"zipcode": zipCode},

            beforeSend: function () {
                progress.show(".zip-city-state");
            },
            success: function (data) {
                if (data != null && data.cities.length > 0) {

                    LoadShippingCityStateDropdowns(data);

                    $.uniform.update(state);
                    $.uniform.update(city);

                    $('#dwfrm_singleshipping_shippingAddress_addressFields_city-error').remove();

                }
                if (updateSummaryFlag) {
                	updateSummary();
				}                
            },
            error: function (data) {
                //CityFailOver("");
            },
            complete: function () {
                progress.hide(".zip-city-state");
            }
        });
    }
}

function LoadBillingCityStateDropdowns(data) {
    var checkoutForm = $("form.address");
    var postalCode = checkoutForm.find("input[name$='_billingAddress_addressFields_postal']");
    var city = checkoutForm.find('input[name$="_billingAddress_addressFields_city"]');
    var state = checkoutForm.find('select[name$="_billingAddress_addressFields_states_state"]');
    var cityJSON = checkoutForm.find("input.cityJSON");
    if (data !== null) {
        city.empty();
        for (var __i = 0; __i < data.cities.length; __i++) {
            
            city.val(data.cities[__i].city);

            if (__i === 0) {
                state.val(data.cities[__i].state);

                // Reset any error conditions on the state field
                state.closest(".form-row").removeClass("error").find(".error-message").hide();

                // State selected so hide drop down
                $(".state-col .dk_options_inner").css("display","none");
            }
        }
        // Enable the city/state select elements so they are submitted with form.
        city.removeAttr("disabled");
        state.removeAttr("disabled");
    }
}

function GetBillingCityStateFromZip(zipCode) {
    var checkoutForm = $("form.address");
    var postalCode = checkoutForm.find("input[name$='_billingAddress_addressFields_postal']");
    var city = checkoutForm.find('input[name$="_billingAddress_addressFields_city"]');
    var state = checkoutForm.find('select[name$="_billingAddress_addressFields_states_state"]');
    var country = checkoutForm.find('select[name$="_billingAddress_addressFields_country"]');
    var cityJSON = checkoutForm.find("input.cityJSON");
    // 1. Verify a valid US 5-digit ZIP first, and that the same ZIP was not entered again
    if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCode)) { //&& zipCode != app.clientcache.SESSION_ZIP){

        postalCode.removeClass('error');
        postalCode.next('span.error').remove();
        $.ajax({
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: Urls.cityAndStateForZip,
            async: true,
            cache: false,
            data: {"zipcode": zipCode},

            beforeSend: function () {
                progress.show(".zip-city-state");
            },
            success: function (data) {
                if (data != null && data.cities.length > 0) {

                    LoadBillingCityStateDropdowns(data);

                    $.uniform.update(state);
                    $.uniform.update(city);

                    $('#dwfrm_billing_billingAddress_addressFields_city-error').remove();

                } 
            },
            error: function (data) {
                //CityFailOver("");
            },
            complete: function () {
                progress.hide(".zip-city-state");
            }
        });
    }
}

//credit card validations
function initCreditCardValidation() {
	
	var $inputCC = $('#CreditCardBlock input[name*="_creditCard_number"]');
	$('#dwfrm_billing_paymentMethods_creditCard_owner').attr("placeholder", "Cardholder name");
	$('#dwfrm_billing_paymentMethods_creditCard_number').attr("placeholder", "Card number");
	$('#dwfrm_billing_paymentMethods_creditCard_expirydate').attr("placeholder", "MM/YY");
	$('#dwfrm_billing_paymentMethods_creditCard_cvn').attr("placeholder", "CVC");	
	
	$inputCC.on('focusout', function (event) {
		var type= GetCardType($inputCC.val().replace(/\s+/g, ''));
		if(type=="Visa")
		{
			$(this).addClass('Visa');
			$(this).removeClass('Discover');
			$(this).removeClass('MasterCard');
			$(this).removeClass('Amex');
			$(this).parent().addClass('Visa-wrapper');
			$(this).parent().removeClass('Discover-wrapper');
			$(this).parent().removeClass('MasterCard-wrapper');
			$(this).parent().removeClass('Amex-wrapper');
			
			$( "input[name^='dwfrm_billing_paymentMethods_creditCard_cvn']" ).attr("maxlength", "3");
		}
		else if(type=="MasterCard")
		{
			$(this).addClass('MasterCard');
			$(this).removeClass('Visa');
			$(this).removeClass('Discover');
			$(this).removeClass('Amex');
			$(this).parent().addClass('MasterCard-wrapper');
			$(this).parent().removeClass('Visa-wrapper');
			$(this).parent().removeClass('Discover-wrapper');
			$(this).parent().removeClass('Amex-wrapper');
			$( "input[name^='dwfrm_billing_paymentMethods_creditCard_cvn']" ).attr("maxlength", "3");
		}
		else if(type=="Amex")
		{
			$(this).addClass('Amex');
			$(this).removeClass('Visa');
			$(this).removeClass('MasterCard');
			$(this).removeClass('Discover');
			$(this).parent().addClass('Amex-wrapper');
			$(this).parent().removeClass('Visa-wrapper');
			$(this).parent().removeClass('MasterCard-wrapper');
			$(this).parent().removeClass('Discover-wrapper');
			
			$( "input[name^='dwfrm_billing_paymentMethods_creditCard_cvn']" ).attr("maxlength", "4");
		}
		else if(type=="Discover")
		{
			$(this).addClass('Discover');
			$(this).removeClass('Visa');
			$(this).removeClass('MasterCard');
			$(this).removeClass('Amex');
			$(this).parent().addClass('Discover-wrapper');
			$(this).parent().removeClass('Visa-wrapper');
			$(this).parent().removeClass('MasterCard-wrapper');
			$(this).parent().removeClass('Amex-wrapper');
			$( "input[name^='dwfrm_billing_paymentMethods_creditCard_cvn']" ).attr("maxlength", "3");
		}
		else
		{
			$(this).removeClass('Visa');
			$(this).removeClass('MasterCard');
			$(this).removeClass('Amex');
			$(this).removeClass('Discover');
			$(this).parent().removeClass('Visa-wrapper');
			$(this).parent().removeClass('MasterCard-wrapper');
			$(this).parent().removeClass('Amex-wrapper');
			$(this).parent().removeClass('Discover-wrapper');
			$( "input[name^='dwfrm_billing_paymentMethods_creditCard_cvn']" ).attr("maxlength", "3");
		}
		if(type.length > 1){				
			$("#dwfrm_billing_paymentMethods_creditCard_type").val(type);
			if ($(".ccnumber .errorccnumber").length > 0) {
				$(".ccnumber span.errorccnumber").remove();
			}
            if ($('.mobile-checkout-process-ab').length > 0) {
                var isCreditCardValid = isValidCreditCard($inputCC.val());
				if (!isCreditCardValid && $(".ccnumber .errorccnumber").length < 1) {
					$inputCC.parent('div').removeClass(type + '-wrapper');
                    $(".ccnumber .field-wrapper").append("<span class='error errorccnumber'>" + Resources.VALIDATE_CREDITCARD + "</span>");
                }
            }
		}
		else
		{
			$("#dwfrm_billing_paymentMethods_creditCard_type").val('Visa'); // default value
			if ($inputCC.val().length > 0) {
				if ($(".ccnumber .errorccnumber").length < 1) {
					$(".ccnumber .field-wrapper").append("<span class='error errorccnumber'>" + Resources.VALIDATE_CREDITCARD + "</span>");
				}
			}
			else if ($(".ccnumber .errorccnumber").length > 0) {
				$(".ccnumber span.errorccnumber").remove();
			}
			isValidBillingForm();
		}
	});
	$inputCC.on('keypress keydown focusout blur',  function(e){
		
		if ($(".ccnumber .error-message").length > 0) {
			$(".ccnumber div.error-message").remove();
		}
		
	});
	
	 // Default credit card mask
	  var creditCardNumberMask='XXXX XXXX XXXX XXXX';

	  $inputCC.attr("maxlength", creditCardNumberMask.length);
	  $inputCC.attr("x-autocompletetype", "cc-number");
	  $inputCC.attr("autocompletetype", "cc-number");
	  $inputCC.attr("autocorrect", "off");
	  $inputCC.attr("spellcheck", "off");
	  $inputCC.attr("autocapitalize", "off");
	  //
	  // Events
	  //
	  $inputCC.keydown(function(e) {
	    handleMaskedNumberInputKey(e, creditCardNumberMask);
	  });
	  $inputCC.keyup(function(e) {    
		if($inputCC.val().length > 2)
		{
			var card = cardFromNumber($inputCC.val());	
			creditCardNumberMask = card.format;
			$inputCC.attr("maxlength", creditCardNumberMask.length);		
		}
	  });
	  
	  $inputCC.on('paste blur', function(e) {
	    setTimeout(function() {
			
			if($inputCC.val().length > 1)
			{
				var card = cardFromNumber($inputCC.val());	
				if (typeof card != 'undefined') {
					creditCardNumberMask = card.format;
				}
				$inputCC.attr("maxlength", creditCardNumberMask.length);			
			}
			
			var numbersOnly = numbersOnlyString($inputCC.val());
			var formattedNumber = applyFormatMask(numbersOnly, creditCardNumberMask);
			$inputCC.val(formattedNumber);
	      
	    }, 1);
	  });
	
	// expiry date mask and input validation
	$("#dwfrm_billing_paymentMethods_creditCard_expirydate")
    .attr("maxlength", "5")
    .on('keydown',  function(e){
        var val = this.value;
        var temp_val;
        if(!isNaN(val)) {
            if(val > 1 && val < 10 && val.length == 1) {
                temp_val = "0" + val + "/";
                $(this).val(temp_val);
            }
            else if (val >= 1 && val < 10 && val.length == 2 && e.keyCode != 8) {
                temp_val = val + "/";
                $(this).val(temp_val);                        
            }
            else if(val > 9 && val.length == 2 && e.keyCode != 8) {
                temp_val = val + "/";
                $(this).val(temp_val);
            }
        }

        -1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault();

    });
	    
    $("#dwfrm_billing_paymentMethods_creditCard_expirydate").on('paste', function(e) {
	    setTimeout(function() {				
	    	var date= $("#dwfrm_billing_paymentMethods_creditCard_expirydate").val();
			var numbersOnly = numbersOnlyString(date);				
			var mask = "XX/XX"
			var formattedNumber = applyFormatMask(numbersOnly, mask);
			$("#dwfrm_billing_paymentMethods_creditCard_expirydate").val(formattedNumber);
	      
	    }, 1);
	  });


	$( "input[name^='dwfrm_billing_paymentMethods_creditCard_cvn']" )	
	.on('input propertychange focusout',  function(e){
		if ($(".cvn .error-message").length > 0) {
			$(".cvn div.error-message").remove();
		}
        isValidBillingForm();
        if((e.type == "focusout") && ($( "input[name^='dwfrm_billing_paymentMethods_creditCard_cvn']" ).val().trim() != "")) {
          utag.link({eventCategory: 'Checkout',eventLabel: 'Creditcard_Code', eventAction: 'Form' });
        }
	});
	
	$( "input[name^='dwfrm_billing_paymentMethods_creditCard_expirydate']" )	
	.on('input propertychange focusout',  function(e){
		if ($(".ccdate .error-message").length > 0) {
			$(".ccdate div.error-message").remove();
		}
		isValidBillingForm();
		if((e.type == "focusout") && ($("input[name^='dwfrm_billing_paymentMethods_creditCard_expirydate']").val().trim() != "")) {
		  utag.link({eventCategory: 'Checkout',eventLabel: 'Creditcard_ExpiryDate', eventAction: 'Form' });
		}
	});
	
	$( "input[name^='dwfrm_billing_paymentMethods_creditCard_cvn']" )
	.attr("maxlength", "4")
	.on('input propertychange',  function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
        isValidBillingForm();
   });


	$('#dwfrm_billing_paymentMethods_creditCard_expirydate').on('focusout', function (event) {
		var date= $(this).val();
		if (IsValidExpiryDate(date))
		{
			var datepart = date.split('/');
			$("#dwfrm_billing_paymentMethods_creditCard_expiration_month").val(datepart[0]);
			$("#dwfrm_billing_paymentMethods_creditCard_expiration_year").val('20' + datepart[1]);

			if ($(".expirydate .errordate").length > 0) {
				$(".expirydate span.errordate").remove();
			}
		}
		else
		{    	    		
			if (date.length > 0 ) {
				if ($(".expirydate .errordate").length < 1) {
					$(".expirydate .field-wrapper").append("<span class='error errordate'>" + Resources.VALIDATE_DATE + "</span>");	
				}				
			}
			else if ($(".expirydate .errordate").length > 0) {
				$(".expirydate span.errordate").remove();
			}
			isValidBillingForm();
            $('#billingSubmitButton').attr('disabled', 'disabled');
            $('#minisummarySubmitOrderButton').attr('disabled', 'disabled');
		}
	});

	var $selectPaymentMethod = $('.payment-method-options');
	var selectedPaymentMethod = $selectPaymentMethod.find(':checked').val();
	/*if($(".mobile-checkout-process-ab").length > 0) {
		$('.new-checkout-mobile-redesign .details').click(function() {
			$(this).siblings().find("span").toggleClass("checked");
			$(".CREDIT_CARD .radio span").removeClass("checked");
		});
	}*/
	$selectPaymentMethod.on('click', 'input[type="radio"]', function () {
		if($(this).val()=='CREDIT_CARD'){				
			if ($("#deliverysummaryblock").find("div.restricted-state, div.block-checkout-nationwide-shipping").length == 0) {
				$('#submit_order').show();
				$('#synchrony_submit_order').hide(); 
			}
			updatePaymentMethod('CREDIT_CARD');
		}
		else {
			if ($("#deliverysummaryblock").find("div.restricted-state, div.block-checkout-nationwide-shipping").length == 0) {
				$('#submit_order').hide();
				$('#synchrony_submit_order').show();
			}
			updatePaymentMethod('SYNCHRONY_FINANCIAL');
		}
		isValidBillingForm();
	});
	
	$(document).on('click','a.PM-radio-details', function(e) {
		$(this).siblings('.parent-radio').find('input[type=radio]').trigger('click');
		$('#is-CREDIT_CARD').parent().removeClass('checked');
		$('#is-SYNCHRONY_FINANCIAL').parent().removeClass('checked');
		$(this).siblings('.parent-radio').find('input[type=radio]').parent().addClass('checked');
	});
	
	
	if(selectedPaymentMethod =='CREDIT_CARD') {
		if ($("#deliverysummaryblock").find("div.restricted-state, div.block-checkout-nationwide-shipping").length == 0) {
			$('#submit_order').show();
			$('#synchrony_submit_order').hide();
		}
	}
	else
	{
		if ($("#deliverysummaryblock").find("div.restricted-state, div.block-checkout-nationwide-shipping").length == 0) {
			$('#submit_order').hide();
			$('#synchrony_submit_order').show();
		}
	}
	isValidBillingForm();
}

function isValidBillingForm () {
	if ($('#dwfrm_billing').length > 0) {
		//Disable amazon submit order button when form is invalid
		var scheduleLater = $('#dwfrm_billing_billingAddress_scheduleLater');
		var checkoutBlocked = false;
		if (scheduleLater.length > 0 && !scheduleLater[0].checked) {
			checkoutBlocked = true;
		}
		if(SessionAttributes.AMAZON_CHECKOUT) {
			if ($('#dwfrm_billing').validate().checkForm() && $('.count-reg').length < 1) {
		        $('.amazon-submit-order ').removeAttr('disabled');
		        $('.amazon-submit-order-minisummary').removeAttr('disabled');
		       } else {
		    	   $('.amazon-submit-order').attr('disabled', 'disabled');
		    	   $('.amazon-submit-order-minisummary').attr('disabled', 'disabled');
		     }
		} else {

		//validation of normal checkout
	    if ($('#dwfrm_billing').validate().checkForm() && !checkoutBlocked && $('.count-reg').length < 1) {
			var isCCPaymentMethod = $('#is-CREDIT_CARD').parent().hasClass('checked');
			var hasValidExpiryDate = IsValidExpiryDate($('#dwfrm_billing_paymentMethods_creditCard_expirydate').val());
			if ($("#deliverysummaryblock").find("div.restricted-state, div.block-checkout-nationwide-shipping").length == 0
				&& (!isCCPaymentMethod || hasValidExpiryDate)) {
					$('#billingSubmitButton').removeAttr('disabled').removeClass('disabled-btn');
	    			if(!$(".mobile-checkout-process-ab").length > 0) {
	    				$('#synchronyDigitalBuyButton').removeAttr('disabled');
	    			}
	            	$('#minisummarySubmitOrderButton').removeAttr('disabled').removeClass('disabled-btn');
				}
			else if(isCCPaymentMethod && !hasValidExpiryDate) {
				setTimeout(function() {
					$('#minisummarySubmitOrderButton').attr('disabled', 'disabled');
					$('#billingSubmitButton').attr('disabled', 'disabled');
					$('#synchronyDigitalBuyButton').attr('disabled', 'disabled');
				}, 50);
			}
	       } else {
	    	   setTimeout(function() {
	    	   	$('#minisummarySubmitOrderButton').attr('disabled', 'disabled');
	            $('#billingSubmitButton').attr('disabled', 'disabled');
	            $('#synchronyDigitalBuyButton').attr('disabled', 'disabled');
	    	   }, 50);
	       }
	   }
	}
}
$(document).on('click','.use-my-card',function(e){
	e.preventDefault();
	$(this).addClass('disabled-btn');
	$(this).attr('disabled', 'disabled');
	$('#synchronyDigitalBuyButton').removeAttr('disabled');
	$('#synchronyDigitalBuyButton').trigger('click');
	return true;
});

$(document).on('focusout','#dwfrm_synchrony_billToAccountNumber', function() {
	isValidBillingForm();
	if ($(this).val() == '') {
	    var $continue = $('#synchronyDigitalBuyButton');
	    $continue.attr('disabled', 'disabled'); 
	}
	if($("#dwfrm_synchrony_billToAccountNumber").val().trim() != "") {
	  utag.link({eventCategory: 'Checkout',eventLabel: 'Account_Number', eventAction: 'Form' });
	}
});
//Checkout flow coupon code apply and error handling
$("body").on('click',"#add-coupon-code",  function (e) {
    e.preventDefault();    
    var $couponCode = $('#dwfrm_cart_couponCode');
    var code = $couponCode.val();
    if (code.length === 0) {
    	$('#dwfrm_cart_couponCode').val('');
    	$('#dwfrm_cart_couponCode').addClass('error');
    	$('#dwfrm_cart_couponCode').nextAll('.error:first').text(Resources.COUPON_CODE_MISSING);
        return;
    }

    var url = util.appendParamsToUrl(Urls.addCouponCheckout, {couponCode: code, format: 'ajax'});
        $.getJSON(url, function (data) {
            var fail = false;
            var msg = "";
            if (!data) {
                msg = Resources.BAD_RESPONSE;
                fail = true;
            }
            if (data.CouponError == 'NO_ACTIVE_PROMOTION') {
            	msg = Resources.NO_ACTIVE_PROMOTION;
            	var msg = msg.replace("temp", data.CouponCode);
            } else if (data.CouponError == 'COUPON_CODE_MISSING') {
            	msg = Resources.COUPON_CODE_MISSING;
            } else if (data.couponStatus== 'COUPON_CODE_ALREADY_IN_BASKET') {
            	msg = Resources.COUPON_CODE_ALREADY_IN_BASKET;
            	var msg = msg.replace("temp", data.CouponCode);
            } else if (data.couponStatus== 'COUPON_ALREADY_IN_BASKET') {
            	msg = Resources.COUPON_ALREADY_IN_BASKET;
            	var msg = msg.replace("temp", data.CouponCode);
            } else if (data.couponStatus== 'COUPON_CODE_ALREADY_REDEEMED') {
            	msg = Resources.COUPON_CODE_ALREADY_REDEEMED;
            	var msg = msg.replace("temp", data.CouponCode);
            } else if (data.couponStatus== 'COUPON_CODE_UNKNOWN') {
            	msg = Resources.COUPON_CODE_UNKNOWN;
            	var msg = msg.replace("temp", data.CouponCode);
            } else if (data.couponStatus== 'COUPON_DISABLED') {
            	msg = Resources.COUPON_DISABLED;
            	var msg = msg.replace("temp", data.CouponCode);
            } else if (data.couponStatus== 'REDEMPTION_LIMIT_EXCEEDED') {
            	msg = Resources.REDEMPTION_LIMIT_EXCEEDED;
            	var msg = msg.replace("temp", data.CouponCode);
            } else if (data.couponStatus== 'CUSTOMER_REDEMPTION_LIMIT_EXCEEDED') {
            	msg = Resources.CUSTOMER_REDEMPTION_LIMIT_EXCEEDED;
            	var msg = msg.replace("temp", data.CouponCode);
            } else if (data.couponStatus== 'TIMEFRAME_REDEMPTION_LIMIT_EXCEEDED') {
            	msg = Resources.TIMEFRAME_REDEMPTION_LIMIT_EXCEEDED;
            	var msg = msg.replace("temp", data.CouponCode);
            } else if (data.couponStatus== 'NO_ACTIVE_PROMOTION') {
            	msg = Resources.NO_ACTIVE_PROMOTION;
            	var msg = msg.replace("temp", data.CouponCode);
            }
            //Scenarios, whether a promo percentage or bonus product added or whether a promo is applied/not applied
            if(data.CouponApplied == false){
            	page.refresh();
            } else if (data.success && data.CouponApplied && data.CouponType == "BONUS" && msg == "") {
                $(function(){
                	page.refresh();
                	// #coupon-success-message-checkout is the id of overlay which comes after bonus product addition in basket via coupon code.
                	/*$('#coupon-success-message-checkout').show();
				    setTimeout(function() {
				    	$('#coupon-success-message-checkout').fadeOut('fast');
					    page.refresh();
					}, 2000);*/
				});
            }else if (data.success && data.CouponApplied && data.CouponType != "BONUS" && msg == "") {
            	$(function(){
            		page.refresh();
            		//#promo-success-message-checkout is the id of overlay which comes after discount applied on basket via coupon code.
                   /* $('#promo-success-message-checkout').show();
				    setTimeout(function() {
				    	$('#promo-success-message-checkout').fadeOut('fast');
					    page.refresh();
					}, 2000);*/
				});
            } else if (msg != "") {
            	$('#dwfrm_cart_couponCode').val('');
            	$('#dwfrm_cart_couponCode').addClass('error');
            	$('#dwfrm_cart_couponCode').nextAll('.error:first').text(msg);
            }
            
            if(data.action != null && data.action === 'addcoupon' && data.qualifiedBonusProducts != null) {        			
            	addProducttUtagByJson(data.qualifiedBonusProducts);
    		}
        });
});

$(document.body).on("click",".checkout-mini-cart-wrapper #deleteCoupon", function(e){
	e.preventDefault();	
	
	//append hidden field 	
	var $form =  $('#cart-items-form, #cart-items-form-os');
	$form.append( "<input type='hidden' class='form_submittype' name='" + this.name + "' value='true'>" );
	
	var ajaxdest = $form.attr('action');
	var url = util.appendParamsToUrl(ajaxdest, {format: 'ajax'});
	var formData = $(":input").serialize(); //$form.serialize();
	$.post( url, formData )
	.done(function( data ){	
		if(data.action != null && data.qualifiedBonusProducts!=null && data.action === 'deleteCoupon') {
			removeProducttUtagByJson(data.qualifiedBonusProducts);			
		}
		page.refresh();
	});	
});

function removeProducttUtagByJson (productsJson) {
	if(utag) {		
		var index = 0;
		if(productsJson != null){
			while (productsJson[index]) {
				var product = productsJson[index];
				var productImageURL = product.productexternalid != 0 ? 'https://' + SitePreferences.amplienceHost +'/i/hmk/'+ product.productexternalid+'?h=1220&w=1350' : '';
				var productURL = util.appendParamsToUrl(Urls.getHttpProductUrl, {"pid": product.variantid});
				if(typeof utag != undefined && utag != null ){ 
					utag.link({ 
						"enhanced_actions"		:	"remove", 
						"product_id"			:	[product.productid],
			            "product_name"			:	[product.productname],
			            "product_brand"			:	[product.brand],
			            "product_quantity"		:	[product.quantity.toString()],
			           	"product_price"			:	["0.00"],
			            "product_unit_price"	:	["0.00"],
			            "product_category"		:	[product.productcategory],
			            "product_sku"			:	[product.productsku],
			            "product_child_sku"		:	[product.productchildsku], 
			            "product_img_url"		:	[productImageURL],
			            "product_url"			:	[productURL],
			            "product_size"			:	[product.productsize]
					});
				}
				
				index++;
			}
		}
	}
}

//MAT-1468	Issues with hitting enter for promo codes
//trigger events on enter
$(document.body).on('keydown','input[name$="_couponCode"]' , function (e) {
   e.stopPropagation();
   if (e.which === 13) {
		e.preventDefault();
		$("#add-coupon-code").click();
	}
});

$(document).on('click','#amazon-submit-order-main-button', function (e) {
	   	e.preventDefault();
		var amazonSubmitButton = $('#amazon-submit-order-main-button');
	   	if(SitePreferences.enableATPOnOrderSubmit == true){
	   		confirmATPAvailablityAmazonCheckout(Urls.billing);
	   	}else{
			amazonSubmitButton.parent().append('<input type="hidden" name="dwfrm_billing_save" value="true" />');
			$('#dwfrm_billing.checkout-billing').submit();
		}	
});

$(document).on('click','.amazon-submit-order-minisummary', function (e) {
   	e.preventDefault();
	$(".amazon-submit-order").click();
});

function confirmATPAvailablityAmazonCheckout(redirectURL){
	var deliveryData = $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryDate');
	var amazonSubmitButton = $('#amazon-submit-order-main-button');
	if (deliveryData.length > 0 && deliveryData.val() != '') {
		var res = deliveryData.val().split(",");
		var params = {
				format: 'ajax',
				deliveryDateData: res[0].trim(),
				deliveryTimeData: res[1].trim()
		};
		var url = util.appendParamsToUrl(Urls.confirmATPAvailablityAmazon, params);
		$.getJSON(url).then(function (data) {
			if (data && data.success) {
				if(typeof data.showATPCalendar !== 'undefined' && data.showATPCalendar == true){
					window.location.href = util.appendParamsToUrl(redirectURL, {deliveryScheduleTimeout : 'true'});
				} else {
					amazonSubmitButton.parent().append('<input type="hidden" name="dwfrm_billing_save" value="true" />');
					$('#dwfrm_billing.checkout-billing').submit();
				}
			}else {
				amazonSubmitButton.parent().append('<input type="hidden" name="dwfrm_billing_save" value="true" />');
				$('#dwfrm_billing.checkout-billing').submit();
			}
		});
	} else {
		amazonSubmitButton.parent().append('<input type="hidden" name="dwfrm_billing_save" value="true" />');
		$('#dwfrm_billing.checkout-billing').submit();
	}
}

// Verify ATP delivery zone
function verifyDeliveryZone(zipCode){
	
	var zoneChanged = false;	
	var params = {
            format: 'ajax',
            zipCode: zipCode
    };
	progress.show();
	$.ajax({
		url: util.appendParamsToUrl(Urls.verifyATPDeliveryZone, params),
		type: 'get',
		async: false,
		success: function (response) {
	    	if (response) {
	    		zoneChanged = true;
			}		    	
		},
		failure: function () {
			window.alert(Resources.SERVER_ERROR);
			
		}
	});
		
	return zoneChanged;
	
}


function deliverySelection() {
	if ($('#zipcodeUpdate').hasClass( "disabled" )) {
		$('#zipcodeUpdate').removeClass("disabled");
	}
    $('.preferred-contact-wrapper').hide();
    $('.delivery-dates-wrapper').show();

    if (SitePreferences.isAtpDeliveryScheduleEnabled) {
        if ($('.schedule').hasClass('slick-initialized')) {
            $('.schedule').slick('unslick');
        }
        $('.schedule').slick({
            infinite: false,
            adaptiveHeight: true,
            slidesToScroll: 1,
            slidesToShow: 1,
            swipe: false,
            prevArrow: $('.slick-prev-arrow'),
            nextArrow: $('.slick-next-arrow'),
            accessibility: false,
            draggable: false
        });
        $('.slick-prev-arrow').attr('tabindex',0);
        $('.slick-next-arrow').attr('tabindex',0);
        $('.slick-prev-arrow').click(function(e) {
        	e.preventDefault();
            $('.schedule').slick('slickGoTo', parseInt($('.schedule').slick('slickCurrentSlide')) - 1);
         });
        $('.slick-next-arrow').on('click', function (e) {
        	e.preventDefault();
            var currentIndex = $('.schedule').slick('slickCurrentSlide');
            var slickSliderLength = $('.sched-date-row.slick-slide').length;
            var enableFutureAtpDeliverySchedule = SitePreferences.enableFutureAtpDeliverySchedule;
            if (currentIndex == (slickSliderLength - 1) && $(this).data('current-week-date') != false && !enableFutureAtpDeliverySchedule) {
                var params = {
                        format: 'ajax',
                        currentWeekDate: $(this).data('current-week-date')
                };
                progress.show($('#main'));
                ajax.load({
                    url: util.appendParamsToUrl(Urls.getATPDeliveryDatesForNextWeek, params),
                    callback: function (response) {
                        if (response != '') {
                            $('.schedule').slick('slickAdd', response, currentIndex, true);
                            if ($('.slick-next-arrow').data('current-week-date') == false) {
                                $('.schedule').slick('slickRemove', (currentIndex + 1));
                            }
                        }
                        progress.hide();
                        $('.slick-prev-arrow').attr('tabindex',0);
                        $('.slick-next-arrow').attr('tabindex',0);
                                                
                    }
                });
            }
            $('.slick-prev-arrow').attr('tabindex',0);
			$('.slick-next-arrow').attr('tabindex',0);
        });
        var slickPreviousDiv = null;
        var slickNextDiv = null;
        var currentSlide = 0;
        $('.slick-next-arrow').keypress(function (ev) {
			if (ev.keyCode == 13 || ev.which == 13) {
				$('.sched-date-row').find('.time-slot').removeAttr("tabindex");
				if (slickNextDiv != null) {
					slickNextDiv.attr("tabindex", 0);
				}
				currentSlide = $('.schedule').slick('slickCurrentSlide');
				var $this = $('.schedule').find("[data-slick-index=" + currentSlide + "]").find('.time-slot');
				$this.removeAttr("tabindex");
				slickPreviousDiv = $this;
				$(this).trigger('click');
			}
		});
        $('.slick-prev-arrow').keypress(function (ev) {
			if (ev.keyCode == 13 || ev.which == 13) {
				$('.sched-date-row').find('.time-slot').removeAttr("tabindex");
				currentSlide = $('.schedule').slick('slickCurrentSlide');
				var $this = $('.schedule').find("[data-slick-index=" + currentSlide + "]").find('.time-slot');
				$this.removeAttr("tabindex");
				slickNextDiv = $this;
				slickPreviousDiv.attr("tabindex",0);
				$('.slick-prev-arrow').attr('tabindex',0);
				$('.slick-next-arrow').attr('tabindex',0);
				$('.slick-prev-arrow')[0].click();
			}
		});
	} else {
        if ($('.sched-delivery').hasClass('slick-initialized')) {
            $('.sched-delivery').slick('unslick');
        }
        $('.sched-delivery').slick({
            infinite: false,
            adaptiveHeight: true
        });
    }
    
}

function initialDeliveryDates() {
	deliverySelection();

    var addressForm = $('.delivery-dates');
    function changeDeliveryDate(obj) {
        if (!($(obj).hasClass("full"))) {
            /** grab time slot clicked and mark correct day and time slot as selected **/
            $(".time-slot").removeClass("selected");
            $(".header").removeClass("selected");
            $(obj).addClass("selected");
            $(obj).parent().children(".header").addClass("selected");

            /** build text block at bottom stating what date and time was chosen **/
            if (SitePreferences.isAtpDeliveryScheduleEnabled) {
                $(".currently-chosen").removeClass("hide");
                var textdate = $(obj).parent().find(".weekday").data('date');
                //textdate += ", " + $(this).parent().find(".month-day").text();
                textdate += '<span> from </span>' + $(obj).text().replace('-', 'to').trim();
                $(".chosen-time").html(textdate);

                /** set pdict fleetwiseToken to currently selected value **/
                $('#dwfrm_singleshipping_shippingAddress_addressFields_fleetwiseToken').val($(obj).data("fleetwisetoken"));
                var deliveryTime = $(obj).text().replace(RegExp(' ', 'g'), '');
                $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryDate').val($(obj).parent().find(".weekday-timeval").text() + ', ' + deliveryTime);
                $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryTime').val(deliveryTime);
            } else {
            	$(".currently-chosen").removeClass("hide");
                $(".currently-chosen").addClass("visible");
                var textdate = $(obj).parent().find(".weekday").text();
                //textdate += ", " + $(this).parent().find(".month-day").text();
                textdate += ", between " + $(obj).text().replace("-","and").replace(RegExp(" PM", "g"), "pm").replace(RegExp(" AM", "g"), "am").trim();
                $(".chosen-time").text(textdate);

                /** set pdict fleetwiseToken to currently selected value **/
                $('#dwfrm_singleshipping_shippingAddress_addressFields_fleetwiseToken').val($(obj).data("fleetwisetoken"));
                $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryDate').val(textdate);
            }
        }
    }
    
    
    addressForm.on("click", ".time-slot", function () {
		changeDeliveryDate(this);
	});
	addressForm.on("keypress", ".time-slot", function (ev) {
		if (ev.keyCode == 13 || ev.which == 13) {
			changeDeliveryDate(this);
		}
	});
    addressForm.on("focus", ".time-slot", function () {
        var dateValue = $($(this).siblings()[0]).attr('data-date');
        dateValue = dateValue.trim();
        var timeValue = $(this).text();
        timeValue = timeValue.trim();
        var timeSlotAriaLabel = dateValue + " " + timeValue;
        $(this).attr("aria-label", timeSlotAriaLabel);
        $(this).parents('.sched-date-row').attr("aria-hidden", false);
    });
    
    //default select first slot
    $(".sched-date").find(".time-slot").each(function(){
    	$(this).click();
    	return false; // to exit each
	});
    
}

function openDeliveryModal(timeoutFlag){
	  var url = util.appendParamsToUrl( Urls.deliveryDatesCalender, {timeoutCheck: timeoutFlag});
	    var dialogueClass= 'redCarpet-Delivery-Section-Popup';
	    if(SitePreferences.enableDeliveryTiers) {
	    	dialogueClass = dialogueClass + ' delivery-tier-popup';
	    }
		$.ajax({
			url: url,
			type: 'GET'
		})
		// success
		.done(function (response) {
			dialog.open({
				html: $(response),
				options: {
					autoOpen: true,
					dialogClass: dialogueClass,
					close: function () {
						if ($('#noDeliverySlots').length == 1 && $('#dwfrm_billing_billingAddress_scheduleLater').parent().hasClass('checked') == false) {
							$('#dwfrm_billing_billingAddress_scheduleLater').click();
							isValidBillingForm();
						}
					}
				}
				
			});
			if (SitePreferences.enableATPDeliveryCalendar && $('.mobile-delivery-calander').length === 0) {
				deliverycalender.dSCalendarSlider();
				makeUniqueIDsForRadioButtons();
			}
			initialDeliveryDates();
			
			var atpFailedCoreProductsArray = $("#atpFailedCoreProductsArrayAdresses").attr("data-atp-failedcoreproductsarray");
			if (atpFailedCoreProductsArray && atpFailedCoreProductsArray != "null") {
				var sliceArray = atpFailedCoreProductsArray.slice(1,-1);
				utag.link({ "eventCategory" : ["ATP"], "eventLabel" : sliceArray.split(","), "eventAction" : ["fail"]});
			}
			
		})
		.error(function (xhr, status, error) {
	    });		
}

$(document).on('click','.ui-widget-overlay', function() { 
    dialog.close(); 
}); 

$(document).on('click', '#chooseMyDelivery',function (e) {
    e.preventDefault();
    openDeliveryModal(false);
});



$(document).on('click', '#schduleMyDelivery',function (e) {
    e.preventDefault();
    var deliverySummarySection = $('#deliverysummaryblock');
    // indicate progress
    //progress.show(deliverySummarySection);
    var deliveryDate = $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryDate').val();
    var deliveryTime = $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryTime').val();
    var deliverSchduler = $('[name=dwfrm_singleshipping_shippingAddress_scheduleWithRep]');
    if (deliverSchduler.length > 0 && deliverSchduler.val() != '') {
    	var deliverSchdulerValue = deliverSchduler.val();
    	var url = util.appendParamsToUrl(Urls.updateDeliverySummary, {deliveryDateSelected: true,DeliveryDate: deliveryDate, DeliveryTime: deliveryTime , DeliverSchdulerValue: deliverSchdulerValue});
    } else {
    	var url = util.appendParamsToUrl(Urls.updateDeliverySummary, {deliveryDateSelected: true,DeliveryDate: deliveryDate, DeliveryTime: deliveryTime});
    }

	deliverySummarySection.load(url, function (responseTxt, statusTxt, xhr) {
			// update order summary section 
			updateSummary();
			util.uniform();
			 if(statusTxt == "success") {
				 isValidBillingForm();
	          }
	    });
    dialog.close();
});
$(document).on('click', '#noDeliverySlots',function (e) {
    dialog.close();
});
//#promo-success-message-checkout is the id of overlay which comes after discount applied on basket via coupon code.
$('body').on('click tap', '#promo-success-message-checkout', function (e) {
       e.preventDefault();
       $('#promo-success-message-checkout').hide();
});

// #coupon-success-message-checkout is the id of overlay which comes after bonus product addition in basket via coupon code.
$('body').on('click tap', '#coupon-success-message-checkout', function (e) {
    e.preventDefault();
    $('#coupon-success-message-checkout').hide();
});

// update payment method selection
function updatePaymentMethod (paymentMethodID) {
    var $paymentMethods = $('.payment-method');
    $paymentMethods.removeClass('payment-method-expanded');

    var $selectedPaymentMethod = $paymentMethods.filter('[data-method="' + paymentMethodID + '"]');
    if ($selectedPaymentMethod.length === 0) {
        $selectedPaymentMethod = $('[data-method="Custom"]');
    }
    $selectedPaymentMethod.addClass('payment-method-expanded');
    
    //This check will run only for new mobile redesign 
    if($(".mobile-checkout-process-ab").length > 0) {
	    $('.mobile-payment-tabs').removeClass('active');
	    $selectedPaymentMethod.closest('.mobile-payment-tabs').addClass('active');
    }
    // ensure checkbox of payment method is checked
    $('input[name$="_selectedPaymentMethodID"]').removeAttr('checked');
    $('input[value=' + paymentMethodID + ']').prop('checked', 'checked');
    if (paymentMethodID === 'SYNCHRONY_FINANCIAL') {
        // get token
        ajax.getJson({
            url: Urls.getToken,
            callback: function (data) {
                $('input[name="clientToken"]').val(data.token);
                console.log("SYNCHRONY_FINANCIAL Payment method selected");
                isValidBillingForm();
                if($(".mobile-checkout-process-ab").length > 0) {
                	$("#synchronyDigitalBuyButton").attr('disabled','disabled');
                }
            }
        });
        /*maintain scroll position when synchrony selected */
        if(!$('#dwfrm_singleshipping_shippingAddress_useAsBillingAddress').parent().hasClass('checked')) {
        	$('body').animate({
    	        scrollTop: $(".SYNCHRONY_FINANCIAL").offset().top - 40
    	    }, 500);
        }
        	    
    } else if (paymentMethodID === 'CREDIT_CARD') {
        // get token
    	var url = util.appendParamsToUrl(Urls.setPaymentMethod, {CREDIT_CARD: 'CREDIT_CARD'});
        ajax.getJson({
            url: url,
            callback: function (data) {
                console.log("CREDIT_CARD Payment method selected");
                isValidBillingForm();
            }
        });
    }
}

function IsValidExpiryDate(date) {	
	var re = new RegExp("^(0[1-9]|1[0-2]|[1-9])\/(1[7-9]|[2-9][0-9])$");
	if (date.match(re) != null) {
		var datepart = date.split('/');
		var month = parseInt(datepart[0], 10),
		year = 2000 + parseInt(datepart[1], 10),
		currentMonth = new Date().getMonth() + 1,
		currentYear  = new Date().getFullYear();
		 if (year < currentYear || (year == currentYear && month < currentMonth)) {
			 return false;
		 }
		 return true;	
	}
    return false;
}

function GetCardType(number)
{
	// visa
	var re = new RegExp("^4[0-9]{12}(?:[0-9]{3})?$");
	if (number.match(re) != null)
		return "Visa";

	// Master card 
	var re = new RegExp("^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$");
	if (number.match(re) != null) 
		return "MasterCard";

	// AMEX
	re = new RegExp("^3[47][0-9]{13}$");
	if (number.match(re) != null)
		return "Amex";

	// Discover
	re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
	if (number.match(re) != null)
		return "Discover";

	return "";
}

// clear billing form
function clearBillingForm(){	
    
    $("input[name$='billing_billingAddress_addressFields_firstName']").val('');
    $("input[name$='billing_billingAddress_addressFields_lastName']").val('');
    $("input[name$='billing_billingAddress_addressFields_address1']").val('');
    $("input[name$='billing_billingAddress_addressFields_address2']").val('');
    $("input[name$='billing_billingAddress_addressFields_phone']").val('');    
    $("input[name$='billing_billingAddress_addressFields_postal']").val('');
    $('input[name$="billing_billingAddress_addressFields_city"]').val('');
    $('select[name$="billing_billingAddress_addressFields_states_state"]').val('');
}

// copy shipping address into billing address 
function useShippingAsBilling(checkoutForm){
	
	var firstName = checkoutForm.find("input[name$='singleshipping_shippingAddress_addressFields_firstName']").val();
	var lastName = checkoutForm.find("input[name$='singleshipping_shippingAddress_addressFields_lastName']").val();
	var address1 = checkoutForm.find("input[name$='singleshipping_shippingAddress_addressFields_address1']").val();
	var address2 = checkoutForm.find("input[name$='singleshipping_shippingAddress_addressFields_address2']").val();
	var phone = checkoutForm.find("input[name$='singleshipping_shippingAddress_addressFields_phone']").val();
    var postalCode = checkoutForm.find("input[name$='singleshipping_shippingAddress_addressFields_postal']").val();
    var city = checkoutForm.find('input[name$="singleshipping_shippingAddress_addressFields_city"]').val();
    var state = checkoutForm.find('select[name$="singleshipping_shippingAddress_addressFields_states_state"]').val();
    $("input[name$='billing_billingAddress_addressFields_firstName']").val(firstName);
    $("input[name$='billing_billingAddress_addressFields_lastName']").val(lastName);
    $("input[name$='billing_billingAddress_addressFields_address1']").val(address1);
    $("input[name$='billing_billingAddress_addressFields_address2']").val(address2);
    $("input[name$='billing_billingAddress_addressFields_phone']").val(phone);    
    $("input[name$='billing_billingAddress_addressFields_postal']").val(postalCode);
    $('input[name$="billing_billingAddress_addressFields_city"]').val(city);
    $('select[name$="billing_billingAddress_addressFields_states_state"]').val(state);
}

function useShippingAsBillingAB() {
	var firstName = $("#savedFirstName").val();
	var lastName = $("#savedLastName").val();
	var address1 = $("#saveAddressOne").val();
	var address2 = $("#saveAddressTwo").val();
	var city = $("#saveCityName").val();
	var state = $("#saveStateName").val();
	var postalCode = $("#savedZippedCode").val();
	var phone = $("#savedPhoneNumber").val();
    $("input[name$='billing_billingAddress_addressFields_firstName']").val(firstName);
    $("input[name$='billing_billingAddress_addressFields_lastName']").val(lastName);
    $("input[name$='billing_billingAddress_addressFields_address1']").val(address1);
    $("input[name$='billing_billingAddress_addressFields_address2']").val(address2);
    $("input[name$='billing_billingAddress_addressFields_phone']").val(phone);    
    $("input[name$='billing_billingAddress_addressFields_postal']").val(postalCode);
    $('input[name$="billing_billingAddress_addressFields_city"]').val(city);
    $('select[name$="billing_billingAddress_addressFields_states_state"]').val(state);
}

function initAddressValidation(){
	$('#dwfrm_singleshipping_shippingAddress_addressFields_address1').addClass("apopobox");	
	if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test($('.opcpostal input').val())) {
        updateOPCdeliverySummarySection($('.opcpostal input').val());
	}
	
	$('.opcpostal input').on("change",function(){
		var zipCode = $(this).val();
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCode)) {
	        updateOPCdeliverySummarySection(zipCode);
		} else {
			var $continue = $('.form-row-button button');
			$continue.attr('disabled', 'disabled'); 
		}
    });
 
	$("#dwfrm_singleshipping_shippingAddress_addToSubscription").on("change",function(){    	
		if ($("#dwfrm_singleshipping_shippingAddress_addToSubscription").val().trim() != "") {
			if ($('.new-customer-info-page').length > 0) {
				utag.link({eventCategory: 'Cart CTA',eventLabel: 'Opt Out of Email List', eventAction: 'Customer Info' });
			} else if ($('.isMobileDevice').length > 0 && $('.isMobileDevice').val() == 'true') {
				utag.link({eventCategory: 'Cart CTA - old',eventLabel: 'Opt Out of Emails', eventAction: 'Delivery Adddress' });
			}
	    }
	});

    $("#dwfrm_singleshipping_shippingAddress_email_emailAddress").on("blur",function(){    	
		if ($("#dwfrm_singleshipping_shippingAddress_email_emailAddress").val().trim() != "") {
			if ($('.new-customer-info-page').length > 0) {
				utag.link({eventCategory: 'Cart CTA',eventLabel: 'Enter Email Address', eventAction: 'Customer Info' });
			} else if ($('.isMobileDevice').length > 0 && $('.isMobileDevice').val() == 'true') {
				utag.link({eventCategory: 'Cart CTA - old',eventLabel: 'Enter Email Address', eventAction: 'Delivery Adddress' });
			} else {
				utag.link({eventCategory: 'Checkout',eventLabel: 'Email_Address', eventAction: 'Form' });
			}
	    }
    });

    $("#dwfrm_singleshipping_shippingAddress_addressFields_firstName").on("blur",function(){
    	if($("#dwfrm_singleshipping_shippingAddress_addressFields_firstName").val().trim() != "") {
    	utag.link({eventCategory: 'Checkout',eventLabel: 'First_Name', eventAction: 'Form' });
    	}
    });
    $("#dwfrm_singleshipping_shippingAddress_addressFields_lastName").on("blur",function(){
    	if($("#dwfrm_singleshipping_shippingAddress_addressFields_lastName").val().trim() != "") {
    	utag.link({eventCategory: 'Checkout',eventLabel: 'Last_Name', eventAction: 'Form' });
    	}
    });
    $("#dwfrm_singleshipping_shippingAddress_addressFields_address1").on("blur",function(){
    	if($("#dwfrm_singleshipping_shippingAddress_addressFields_address1").val().trim() != "") {
			if ($('.new-customer-info-page').length > 0) {
				utag.link({ eventCategory: 'Cart CTA', eventLabel: 'Enter Delivery Address', eventAction: 'Customer Info' });
			} else if ($('.isMobileDevice').length > 0 && $('.isMobileDevice').val() == 'true') {
				utag.link({eventCategory: 'Cart CTA - old',eventLabel: 'Enter Delivery Address', eventAction: 'Delivery Adddress' });
			} else {
				utag.link({ eventCategory: 'Checkout', eventLabel: 'Address_Address1', eventAction: 'Form' });
			}
		}
    });
    $("#dwfrm_singleshipping_shippingAddress_addressFields_city").on("blur",function(){
    	if($("#dwfrm_singleshipping_shippingAddress_addressFields_city").val().trim() != "") {
    	utag.link({eventCategory: 'Checkout',eventLabel: 'Address_City', eventAction: 'Form' });
    	}
    });
    $("#dwfrm_singleshipping_shippingAddress_addressFields_states_state").on("blur",function(){
    	if($("#dwfrm_singleshipping_shippingAddress_addressFields_states_state").val().trim() != "") {
    	utag.link({eventCategory: 'Checkout',eventLabel: 'Address_State', eventAction: 'Form' });
    	}
    }); 
    $("#dwfrm_singleshipping_shippingAddress_addressFields_postal").on("blur",function(){
    	if($("#dwfrm_singleshipping_shippingAddress_addressFields_postal").val().trim() != "") {
    	utag.link({eventCategory: 'Checkout',eventLabel: 'Address_Postal', eventAction: 'Form' });
    	}
    });
    $("#dwfrm_singleshipping_shippingAddress_addressFields_phone").on("blur",function(){ 
    	if($("#dwfrm_singleshipping_shippingAddress_addressFields_phone").val().trim() != "") {
			if ($('.new-customer-info-page').length > 0) {
				utag.link({ eventCategory: 'Cart CTA', eventLabel: 'Enter Phone', eventAction: 'Customer Info' });
			} else if ($('.isMobileDevice').length > 0 && $('.isMobileDevice').val() == 'true') {
				utag.link({eventCategory: 'Cart CTA - old',eventLabel: 'Enter Phone', eventAction: 'Delivery Adddress' });
			} else {
				utag.link({ eventCategory: 'Checkout', eventLabel: 'Address_Phone', eventAction: 'Form' });
			}
		}
    });
    $("#dwfrm_singleshipping_shippingAddress_useAsBillingAddress").on("blur",function(){
    	if($("#dwfrm_singleshipping_shippingAddress_useAsBillingAddress").val().trim() != "") {
    	utag.link({eventCategory: 'Checkout',eventLabel: 'UseAs_BillingAddress', eventAction: 'Check Box' });
    	}
	});

    $("#is-CREDIT_CARD").on("blur",function(){
    	if($("#is-CREDIT_CARD").val().trim() != "") {
    	utag.link({eventCategory: 'Checkout',eventLabel: 'Creditcard', eventAction: 'Radio Button' });
    	}
    });
    $("#dwfrm_billing_paymentMethods_creditCard_number").on("blur",function(){
    	if($("#dwfrm_billing_paymentMethods_creditCard_number").val().trim() != "") {
			if ($('.isMobileDevice').length > 0 && $('.isMobileDevice').val() == 'true') {
				utag.link({ eventCategory: 'Cart CTA - old', eventLabel: 'Enter Credit Card Info', eventAction: 'Payment Method' });
			} else {
				utag.link({ eventCategory: 'Checkout', eventLabel: 'Creditcard_Number', eventAction: 'Form' });
			}
    	}
    });
    //$("#dwfrm_billing_paymentMethods_creditCard_expirydate") yes
    //$("#dwfrm_billing_paymentMethods_creditCard_cvn") yes
    $("#is-SYNCHRONY_FINANCIAL").on("blur",function(){
		if ($("#is-SYNCHRONY_FINANCIAL").val().trim() != "") {
			utag.link({ eventCategory: 'Checkout', eventLabel: 'SynchronyFinancial', eventAction: 'Radio Button' });
    	}
	});

	$('#is-SYNCHRONY_FINANCIAL').on('click', function () {
		if ($('.mobile-checkout-process-ab').length > 0) {
			utag.link({ eventCategory: 'Cart CTA', eventLabel: 'Pay Monthly Radio Button', eventAction: 'Payment' });
		} else if ($('.isMobileDevice').length > 0 && $('.isMobileDevice').val() == 'true') {
			utag.link({ eventCategory: 'Cart CTA - old', eventLabel: 'Select Monthly Payments tab', eventAction: 'Payment Method' });
		}
	});

	$('.edit-item').on('click', function () {
		if ($('.isMobileDevice').length > 0 && $('.isMobileDevice').val() == 'true') {
			utag.link({ eventCategory: 'Cart CTA - old', eventLabel: 'Edit Items', eventAction: 'Review & Submit' });
		}
	});

	$('.edit-delivery-date').on('click', function () {
		if ($('.isMobileDevice').length > 0 && $('.isMobileDevice').val() == 'true') {
			utag.link({ eventCategory: 'Cart CTA - old', eventLabel: 'Edit Delivery Date', eventAction: 'Review & Submit' });
		}
	});

	$('.edit-delivery-address').on('click', function () {
		if ($('.isMobileDevice').length > 0 && $('.isMobileDevice').val() == 'true') {
			utag.link({ eventCategory: 'Cart CTA - old', eventLabel: 'Edit Delivery Address', eventAction: 'Review & Submit' });
		}
	});

	$('.edit-payment-method').on('click', function () {
		if ($('.isMobileDevice').length > 0 && $('.isMobileDevice').val() == 'true') {
			utag.link({ eventCategory: 'Cart CTA - old', eventLabel: 'Edit Payment Method', eventAction: 'Review & Submit' });
		}
	});

	$('#orderSubmit').on('click', function () {
		if ($('.isMobileDevice').length > 0 && $('.isMobileDevice').val() == 'true') {
			utag.link({ eventCategory: 'Cart CTA - old', eventLabel: 'Submit Order', eventAction: 'Review & Submit' });
		}
	});
	// Old Mobile checkout submit
	$('form#mobile-submit-order').submit(function (e) {
		if(SitePreferences.enableATPOnOrderSubmit == true){
			$('#mobile-submit-order').off('submit');
			e.preventDefault();
			confirmATPAvailablityMobile($(this), Urls.COShippingMethodStart);
			 // Submit after checking the ATP
		}else{
			return true;
		}		
	});	
	// New Mobile checkout submit A/B
	$('div.mobile-checkout-process-ab form#dwfrm_billing.checkout-billing').submit(function (e) {
		
		var submitOrderButtonClicked = false;
		var submitButton = $('#billingSubmitButton');
		if(submitButton.length > 0){			
			if(submitButton.attr("clicked")){
				submitOrderButtonClicked = true;
				submitButton.parent().append('<input type="hidden" name="dwfrm_billing_mobileOrderAB" value="true" />');
				submitButton.removeAttr("clicked");
			}
		}
		if(submitOrderButtonClicked && SitePreferences.enableATPOnOrderSubmit == true){
			$(this).off('submit');
			e.preventDefault();
			confirmATPAvailablityMobile($(this), Urls.redirectToDeliveryAB);
			 // Submit after checking the ATP
		}else{
			return true;
		}		
	});
	// Old Desktop Checkout
	$('div#submit_order.button-regular-checkout').closest('form#dwfrm_billing.checkout-billing').submit(function (e) {
		if(SitePreferences.enableATPOnOrderSubmit == true){
			e.preventDefault();
			var submitButton;
			var selectedPaymentMethod = $('.payment-method-options').find(':checked').val(); 		
	 		if (selectedPaymentMethod=='CREDIT_CARD') {
	 			submitButton = $("#billingSubmitButton");
	 		} else if (selectedPaymentMethod=='SYNCHRONY_FINANCIAL') {
	 			submitButton = $("#synchronyDigitalBuyButton");
	 		} else {
	 			submitButton = $("#billingSubmitButton");
	 		}
			confirmATPAvailablity($(this),submitButton);
			 // Submit after checking the ATP
		}else{
			return true;
		}		
	});
	// Old Desktop Checkout(synchrony)
	/*$('div#synchrony_submit_order.button-regular-checkout').closest('form#dwfrm_billing.checkout-billing').submit(function (e) {
		if(SitePreferences.enableATPOnOrderSubmit == true){
			e.preventDefault();
			confirmATPAvailablity($(this),$("#synchronyDigitalBuyButton"));
			 // Submit after checking the ATP
		}else{
			return true;
		}		
	});*/
	
	
    $(".link-login-mob-show a").on("click",function(){
    	$(".mobile-top-tabs-holder li").removeClass("active");
    	$(this).parent().addClass("active");
    	$(".mobile-checkout-process").fadeOut();
    	if($(this).hasClass("customerportal")) {
    		window.location.href = $(this).attr('data-url');
    		return false;
    	}
    	else {
        	$(".mobile-login-template").fadeIn();
    	}
    });
    
    $(".link-checkout-mob-show a").on("click",function(){
    	$(".mobile-top-tabs-holder li").removeClass("active");
    	$(this).parent().addClass("active");
    	$(".mobile-login-template").fadeOut();
    	$(".mobile-checkout-process").fadeIn();
    });
    /*$(".btn-mobile-next-step button").on("click",function(){
    	$(".page-title").fadeOut();
    	$(".mobile-top-tabs-holder").fadeOut();
    	$(".mobile-shipping-address").fadeOut();
    	$(".mobile-payment-and-billing").fadeIn();
    	$(".btn-mobile-next-step").hide();
    	window.scrollTo(0, 0);
    });*/
    
    var heightTooltipCVN = $(".checkout-security-code-tooltip .tooltip-holder").height();
    var heightTooltipHELP = $(".checkout-phone-help-tooltip .tooltip-holder").height();
    $(".checkout-security-code-tooltip .tooltip-holder").css("marginTop",-( heightTooltipCVN / 2));
    $(".checkout-phone-help-tooltip .tooltip-holder").css("marginTop", -(heightTooltipHELP / 2));
    if($(window).width() < 768) {
    	$(".cvn .icon").on({
    	    mouseover: function(e) {
    	    	e.preventDefault();
    		 	e.stopPropagation();
    	    	$(".cvn").find('.tooltip-content').addClass('show');
    	    },
    	    mouseout: function(e) {
    	    	e.preventDefault();
    		 	e.stopPropagation();
    	    	$(".cvn").find('.tooltip-content').removeClass('show');
    	    }
    	});
    }
    if($(".mobile-checkout-process-ab").length == 0) {
	    $(document).on("click touchstart",".mobile-payment-tabs",function(){
	    	$(".mobile-payment-tabs").removeClass("active");    	 
	    	$(this).addClass("active");
	    });
    }
    
    function selectDefaultPaymentMethodAB() {
    	var selectedPaymentMethod = ''; 		
		if($('#is-CREDIT_CARD').parent().hasClass('checked')) {
			selectedPaymentMethod = $('#is-CREDIT_CARD').val();
		} else if($('#is-SYNCHRONY_FINANCIAL').parent().hasClass('checked')) {
			selectedPaymentMethod = $('#is-SYNCHRONY_FINANCIAL').val();
		}
		if (selectedPaymentMethod=='CREDIT_CARD') {
			$(".mobile-payment-tabs").removeClass("active");
			$(".CREDIT_CARD.mobile-payment-tabs").addClass("active");
		} else if (selectedPaymentMethod=='SYNCHRONY_FINANCIAL') {
			$(".mobile-payment-tabs").removeClass("active");
			$(".SYNCHRONY_FINANCIAL.mobile-payment-tabs").addClass("active");
		}
    }
    //Activate right payment method tab in mobile view
    if ($('.payment-method-options').length > 0) {
    	if ($('.mobile-checkout-process-ab').length > 0) {
    		selectDefaultPaymentMethodAB();
		} else {
			var selectedPaymentMethod = $('.payment-method-options').find(':checked').val(); 		
			if (selectedPaymentMethod=='CREDIT_CARD') {
				$(".mobile-payment-tabs").removeClass("active");
				$(".CREDIT_CARD.mobile-payment-tabs").addClass("active");
			} else if (selectedPaymentMethod=='SYNCHRONY_FINANCIAL') {
				$(".mobile-payment-tabs").removeClass("active");
				$(".SYNCHRONY_FINANCIAL.mobile-payment-tabs").addClass("active");
			}
		}
    }
	
    if ($('#useasbillingaddress .input-checkbox').is(":checked")) {
        $('#billingaddress').fadeOut('slow');
		$('.mobile-billing-save-address').fadeIn('slow');
		if ($('.mobile-checkout-process-ab').length > 0) { 
			useShippingAsBillingAB();
			displayBillingAddressAB();
	
		} else {
			var checkoutForm = $("form.address");
			useShippingAsBilling(checkoutForm);
			displayBillingAddress();
		}
        isValidBillingForm();
    }
	// use shipping address as billing address
	$('#useasbillingaddress .input-checkbox').change(function(){
        if (this.checked) {
			if ($('.mobile-checkout-process-ab').length > 0) {
				useShippingAsBillingAB();
			} else {
				var checkoutForm = $("form.address");
				useShippingAsBilling(checkoutForm);
			}
            $('#billingaddress').fadeOut('slow');
            $('.mobile-billing-save-address').fadeIn('slow');
            isValidBillingForm();
        }
        else {
        	clearBillingForm();
            $('#billingaddress').fadeIn('slow');
            $('.mobile-billing-save-address').fadeOut('slow');
            isValidBillingForm();
        }                   
    });
	
	$('.opcphones input').blur(function (e) {
		var rgx = /^\(?([2-9][0-8][0-9])\)?[\-\. ]?([2-9][0-9]{2})[\-\. ]?([0-9]{4})(\s*x[0-9]+)?$/;
		var value= $(this).val();		
		var isValid = rgx.test($.trim(value));
		
		if (!isValid) {
			if ($(".phone").parent().find(".error").length < 1) {
				$(".phone").parent().append("<span class='error'>" + Resources.INVALID_PHONE + "</span>");
				$(".phone").addClass("error");
			}
			
		} else {
			if ($(".phone").parent().find(".error").length > 0) {
				$(".phone").parent().find("span.error").remove();
				$(".phone").removeClass("error");
			}
		}
	});
	// add phone mask to phone
	$('.opcphones input').inputmask("(999) 999-9999", { showMaskOnHover: false, showMaskOnFocus: false });	

	//billing submit button for one page checkout
    $('#billingSubmitButton').on('click tap', function (e) {

		//if payment method is credit card, validate expiry date
		if ($('#is-CREDIT_CARD').parent().hasClass('checked') 
			&& !IsValidExpiryDate($('#dwfrm_billing_paymentMethods_creditCard_expirydate').val())){
			return false;
		}	

    	if ($('#useasbillingaddress .input-checkbox').is(":checked")) {
			if ($('.mobile-checkout-process-ab').length > 0) {
				utag.link({ eventCategory: 'Cart CTA', eventLabel: 'Submit Order CTA', eventAction: 'Payment' });
				useShippingAsBillingAB();
    		} else {
				var checkoutForm = $("form.address");
				useShippingAsBilling(checkoutForm);
    		}
        }
    	
    	var $inputCC = $('#CreditCardBlock input[name*="_creditCard_number"]');
    	if($inputCC && $inputCC.val()) {
    		$inputCC.val($inputCC.val().replace(/\s+/g, ''));
    	}    	
    	
    	//set credit card holder name from billing first name and last name
        var firstName = $("#dwfrm_billing_billingAddress_addressFields_firstName").val();
        var lastName = $("#dwfrm_billing_billingAddress_addressFields_lastName").val();
        $('#dwfrm_billing_paymentMethods_creditCard_owner').val(firstName + ' ' + lastName);
        var valid = jQuery('#dwfrm_billing').validate().checkForm();  
	    if (valid) {
	    	$(this).addClass('disabled-btn');
	    	$('#minisummarySubmitOrderButton').addClass('disabled-btn');
	    }
	    $(this).attr("clicked", "true");
    });
    
    
    $(document).on('click','#minisummarySubmitOrderButton', function (e) {
		$(this).addClass('disabled-btn');  
		$('#billingSubmitButton').addClass('disabled-btn');
		var selectedPaymentMethod = $('.payment-method-options').find(':checked').val(); 		
		if (selectedPaymentMethod=='CREDIT_CARD') {
			$("#billingSubmitButton").trigger("click");
		} else if (selectedPaymentMethod=='SYNCHRONY_FINANCIAL') {
			$("#synchronyDigitalBuyButton").trigger("click");
		} else {
			$("#billingSubmitButton").trigger("click");
		}
	});
    
    $('#synchronyDigitalBuyButton').on('synchrony.confim.delivery.date', function (e) {
    	if(SitePreferences.enableATPOnOrderSubmit == true){
    		var selectedPaymentMethod = $('.payment-method-options').find(':checked').val(); 		
     		if (selectedPaymentMethod=='SYNCHRONY_FINANCIAL') {
     			openDeliveryModal(true);
     			resetdeliverySummarySection();
     		}
    	}
    });

//    $('#mobileShippingSubmitButton').on('click tap', function (e) {    	
//    	
//    	var zipCode = $("#dwfrm_singleshipping_shippingAddress_addressFields_postal").val();
//    	progress.show();
//    	var zoneChanged = verifyDeliveryZone(zipCode);
//    	if (zoneChanged) {    		
//    		e.preventDefault();
//    		window.location.href = util.appendParamsToUrl(Urls.COShippingMethodStart, {deliveryScheduleChanged : 'true'});
//    	}
//    });
    
    function confirmATPAvailablity(targetFrm, submitbutton){    	
    	// TODO: Check for the RCD Delivery Dates Div on the Page before calling
		var url = util.appendParamsToUrl(Urls.confirmATPAvailablity, {format:'ajax'});	 			
		$.getJSON(url).then(function (data) {
			if (data && data.success) {
				if(typeof data.showATPCalendar !== 'undefined' && data.showATPCalendar == true){
					openDeliveryModal(true);
				    resetdeliverySummarySection();
				    submitbutton.removeClass("disabled-btn");
				} else { 
					targetFrm.off('submit');
					submitbutton.trigger("click");
				}
			}else {
				targetFrm.off('submit');
				submitbutton.trigger("click");
			}
		});
    }
        
    function confirmATPAvailablityMobile(targetFrm, redirectURL){   	
    	var url = util.appendParamsToUrl(Urls.confirmATPAvailablity, {format:'ajax'});	 			
		$.getJSON(url).then(function (data) {
			if (data && data.success) {
				if(typeof data.showATPCalendar !== 'undefined' && data.showATPCalendar == true){
					window.location.href = util.appendParamsToUrl(redirectURL, {deliveryScheduleTimeout : 'true'});					
				} else { 						
					targetFrm.submit();
				}
			}else { 						
				targetFrm.submit();
			}
		});
    }
    //saved address values 
	function displayBillingAddress(){
    	var savedFirstName = $("#dwfrm_singleshipping_shippingAddress_addressFields_firstName").val();
        var savedLastName = $("#dwfrm_singleshipping_shippingAddress_addressFields_lastName").val();
        var savefullName = savedFirstName + " " + savedLastName;
        var saveAddressOne = $("#dwfrm_singleshipping_shippingAddress_addressFields_address1").val();
        var saveAddressTwo = $("#dwfrm_singleshipping_shippingAddress_addressFields_address2").val();
        var saveCityName = $("#dwfrm_singleshipping_shippingAddress_addressFields_city").val();
        var saveStateName = $("#dwfrm_singleshipping_shippingAddress_addressFields_states_state").val();
        var savedZippedCode = $("#dwfrm_singleshipping_shippingAddress_addressFields_postal").val();
        var savedPhoneNumber = $("#dwfrm_singleshipping_shippingAddress_addressFields_phone").val();  
        var cityStateZip = saveCityName + ", " + saveStateName + " " + savedZippedCode;
        $(".mobile-saved-name").html("<strong>" + savefullName + "</strong>");
        $(".mobile-save-address").html("<span>" + saveAddressOne + "</span>");
        if(saveAddressTwo){
        	$(".mobile-save-address1").html("<span>" + saveAddressTwo + "</span>");
        }
        $(".mobile-save-city-name").html("<span>" + cityStateZip + "</span>");
        $(".mobile-save-phone-number").html("<span>" + savedPhoneNumber + "</span>");
    }
}

function displayBillingAddressAB(){
	var savedFirstName = $("#savedFirstName").val();
	var savedLastName = $("#savedLastName").val();
	var savefullName = savedFirstName + " " + savedLastName;
	var saveAddressOne = $("#saveAddressOne").val();
	var saveAddressTwo = $("#saveAddressTwo").val();
	var saveCityName = $("#saveCityName").val();
	var saveStateName = $("#saveStateName").val();
	var savedZippedCode = $("#savedZippedCode").val();
	var savedPhoneNumber = $("#savedPhoneNumber").val();
	var emailAddress = $("#savedEmail").val();
	var cityStateZip = saveCityName + ", " + saveStateName + " " + savedZippedCode;
	$(".mobile-saved-name").html("<strong>" + savefullName + "</strong>");
	$(".mobile-save-address").html("<span>" + saveAddressOne + "</span>");
	if(saveAddressTwo && saveAddressTwo != 'null'){
		$(".mobile-save-address1").html("<span>" + saveAddressTwo + "</span>");
	} else {
		$(".mobile-save-address1").html("<span>" + '' + "</span>");
	}
	$(".mobile-save-city-name").html("<span>" + cityStateZip + "</span>");
	$(".mobile-save-phone-number").html("<span>" + savedPhoneNumber + "</span>");
	$(".mobile-save-email-mob-ab").html("<span>" + emailAddress + "</span>");
}

	var KEYS = {
		  "0" : 48,
		  "9" : 57,
		  "NUMPAD_0" : 96,
		  "NUMPAD_9" : 105,
		  "DELETE" : 46,
		  "BACKSPACE" : 8,
		  "ARROW_LEFT" : 37,
		  "ARROW_RIGHT" : 39,
		  "ARROW_UP" : 38,
		  "ARROW_DOWN" : 40,
		  "HOME" : 36,
		  "END" : 35,
		  "TAB" : 9,
		  "A" : 65,
		  "X" : 88,
		  "C" : 67,
		  "V" : 86
		};



	var defaultFormat = "XXXX XXXX XXXX XXXX";

	var cards = [
     {
       type: 'amex',
       pattern: /^3[47]/,
       format: 'XXXX XXXXXX XXXXX',
       length: [15],
       cvcLength: [4],
       luhn: true
     }, {
       type: 'discover',
       pattern: /^(6011|65|64[4-9]|622)/,
       format: defaultFormat,
       length: [16],
       cvcLength: [3],
       luhn: true
     },{
       type: 'maestro',
       pattern: /^(5018|5020|5038|6304|6703|6708|6759|676[1-3])/,
       format: defaultFormat,
       length: [12, 13, 14, 15, 16, 17, 18, 19],
       cvcLength: [3],
       luhn: true
     }, {
       type: 'mastercard',
       pattern: /^(5[1-5]|677189)|^(222[1-9]|2[3-6]\d{2}|27[0-1]\d|2720)/,
       format: defaultFormat,
       length: [16],
       cvcLength: [3],
       luhn: true
     },{
       type: 'visaelectron',
       pattern: /^4(026|17500|405|508|844|91[37])/,
       format: defaultFormat,
       length: [16],
       cvcLength: [3],
       luhn: true
     }, {
       type: 'visa',
       pattern: /^4/,
       format: defaultFormat,
       length: [13, 16, 19],
       cvcLength: [3],
       luhn: true
     }
   ];


	/**
	 *
	 *
	 * @param e
	 * @param mask
	 */
	function handleMaskedNumberInputKey(e, mask) {
	 
	  var keyCode = e.which || e.keyCode;

	  var element = e.target;

	  var caretStart = caretStartPosition(element);
	  var caretEnd = caretEndPosition(element);


	  // Calculate normalised caret position
	  var normalisedStartCaretPosition = normaliseCaretPosition(mask, caretStart);
	  var normalisedEndCaretPosition = normaliseCaretPosition(mask, caretEnd);


	  var newCaretPosition = caretStart;

	  var isNumber = keyIsNumber(e);
	  var isDelete = keyIsDelete(e);
	  var isBackspace = keyIsBackspace(e);

	  if (isNumber || isDelete || isBackspace) {
	    e.preventDefault();
	    var rawText = $(element).val();
	    var numbersOnly = numbersOnlyString(rawText);

	    var digit = digitFromKeyCode(keyCode);

	    var rangeHighlighted = normalisedEndCaretPosition > normalisedStartCaretPosition;

	    // Remove values highlighted (if highlighted)
	    if (rangeHighlighted) {
	      numbersOnly = (numbersOnly.slice(0, normalisedStartCaretPosition) + numbersOnly.slice(normalisedEndCaretPosition));
	    }

	    // Forward Action
	    if (caretStart != mask.length) {

	      // Insert number digit
	      if (isNumber && rawText.length <= mask.length) {
	        numbersOnly = (numbersOnly.slice(0, normalisedStartCaretPosition) + digit + numbersOnly.slice(normalisedStartCaretPosition));
	        newCaretPosition = Math.max(
	          denormaliseCaretPosition(mask, normalisedStartCaretPosition + 1),
	          denormaliseCaretPosition(mask, normalisedStartCaretPosition + 2) - 1
	        );
	      }

	      // Delete
	      if (isDelete) {
	        numbersOnly = (numbersOnly.slice(0, normalisedStartCaretPosition) + numbersOnly.slice(normalisedStartCaretPosition + 1));
	      }

	    }

	    // Backward Action
	    if (caretStart != 0) {

	      // Backspace
	      if(isBackspace && !rangeHighlighted) {
	        numbersOnly = (numbersOnly.slice(0, normalisedStartCaretPosition - 1) + numbersOnly.slice(normalisedStartCaretPosition));
	        newCaretPosition = denormaliseCaretPosition(mask, normalisedStartCaretPosition - 1);
	      }
	    }


	    $(element).val(applyFormatMask(numbersOnly, mask));

	    setCaretPosition(element, newCaretPosition);
	  }
	};



	/**
	 * Get the caret start position of the given element.
	 *
	 * @param element
	 * @returns {*}
	 */
	function caretStartPosition(element) {
	  if(typeof element.selectionStart == "number") {
	    return element.selectionStart;
	  }
	  return false;
	};


	/**
	 * Gte the caret end position of the given element.
	 *
	 * @param element
	 * @returns {*}
	 */
	function caretEndPosition(element) {
	  if(typeof element.selectionEnd == "number") {
	    return element.selectionEnd;
	  }
	  return false;
	};


	/**
	 * Set the caret position of the given element.
	 *
	 * @param element
	 * @param caretPos
	 */
	function setCaretPosition(element, caretPos) {
	  if(element != null) {
	    if(element.createTextRange) {
	      var range = element.createTextRange();
	      range.move('character', caretPos);
	      range.select();
	    } else {
	      if(element.selectionStart) {
	        element.focus();
	        element.setSelectionRange(caretPos, caretPos);
	      } else {
	        element.focus();
	      }
	    }
	  }
	};


	/**
	 * Normalise the caret position for the given mask.
	 *
	 * @param mask
	 * @param caretPosition
	 * @returns {number}
	 */
	function normaliseCaretPosition(mask, caretPosition) {
	  var numberPos = 0;
	  if(caretPosition < 0 || caretPosition > mask.length) { return 0; }
	  for(var i = 0; i < mask.length; i++) {
	    if(i == caretPosition) { return numberPos; }
	    if(mask[i] == "X") { numberPos++; }
	  }
	  return numberPos;
	};


	/**
	 * Denormalise the caret position for the given mask.
	 *
	 * @param mask
	 * @param caretPosition
	 * @returns {*}
	 */
	function denormaliseCaretPosition(mask, caretPosition) {
	  var numberPos = 0;
	  if(caretPosition < 0 || caretPosition > mask.length) { return 0; }
	  for(var i = 0; i < mask.length; i++) {
	    if(numberPos == caretPosition) { return i; }
	    if(mask[i] == "X") { numberPos++; }
	  }
	  return mask.length;
	};



	/**
	 *
	 *
	 * @param e
	 */
	function filterNumberOnlyKey(e) {
	  var isNumber = keyIsNumber(e);
	  var isDeletion = keyIsDeletion(e);
	  var isArrow = keyIsArrow(e);
	  var isNavigation = keyIsNavigation(e);
	  var isKeyboardCommand = keyIsKeyboardCommand(e);
	  var isTab = keyIsTab(e);

	  if(!isNumber && !isDeletion && !isArrow && !isNavigation && !isKeyboardCommand && !isTab) {
	    e.preventDefault();
	  }
	};


	/**
	 *
	 *
	 * @param keyCode
	 * @returns {*}
	 */
	function digitFromKeyCode(keyCode) {

	  if(keyCode >= KEYS["0"] && keyCode <= KEYS["9"]) {
	    return keyCode - KEYS["0"];
	  }

	  if(keyCode >= KEYS["NUMPAD_0"] && keyCode <= KEYS["NUMPAD_9"]) {
	    return keyCode - KEYS["NUMPAD_0"];
	  }

	  return null;
	};



	/**
	 * Get the key code from the given event.
	 *
	 * @param e
	 * @returns {which|*|Object|which|which|string}
	 */
	function keyCodeFromEvent(e) {
	  return e.which || e.keyCode;
	};


	/**
	 * Get whether a command key (ctrl of mac cmd) is held down.
	 *
	 * @param e
	 * @returns {boolean|metaKey|*|metaKey}
	 */
	function keyIsCommandFromEvent(e) {
	  return e.ctrlKey || e.metaKey;
	};


	/**
	 * Is the event a number key.
	 *
	 * @param e
	 * @returns {boolean}
	 */
	function keyIsNumber(e) {
	  return keyIsTopNumber(e) || keyIsKeypadNumber(e);
	};


	/**
	 * Is the event a top keyboard number key.
	 *
	 * @param e
	 * @returns {boolean}
	 */
	function keyIsTopNumber(e) {
	  var keyCode = keyCodeFromEvent(e);
	  return keyCode >= KEYS["0"] && keyCode <= KEYS["9"];
	};


	/**
	 * Is the event a keypad number key.
	 *
	 * @param e
	 * @returns {boolean}
	 */
	function keyIsKeypadNumber(e) {
	  var keyCode = keyCodeFromEvent(e);
	  return keyCode >= KEYS["NUMPAD_0"] && keyCode <= KEYS["NUMPAD_9"];
	};


	/**
	 * Is the event a delete key.
	 *
	 * @param e
	 * @returns {boolean}
	 */
	function keyIsDelete(e) {
	  return keyCodeFromEvent(e) == KEYS["DELETE"];
	};


	/**
	 * Is the event a backspace key.
	 *
	 * @param e
	 * @returns {boolean}
	 */
	 function keyIsBackspace(e) {
	  return keyCodeFromEvent(e) == KEYS["BACKSPACE"];
	};


	/**
	 * Is the event a deletion key (delete or backspace)
	 *
	 * @param e
	 * @returns {boolean}
	 */
	function keyIsDeletion(e) {
	  return keyIsDelete(e) || keyIsBackspace(e);
	};


	/**
	 * Is the event an arrow key.
	 *
	 * @param e
	 * @returns {boolean}
	 */
	function keyIsArrow(e) {
	  var keyCode = keyCodeFromEvent(e);
	  return keyCode >= KEYS["ARROW_LEFT"] && keyCode <= KEYS["ARROW_DOWN"];
	};


	/**
	 * Is the event a navigation key.
	 *
	 * @param e
	 * @returns {boolean}
	 */
	function keyIsNavigation(e) {
	  var keyCode = keyCodeFromEvent(e);
	  return keyCode == KEYS["HOME"] || keyCode == KEYS["END"];
	};


	/**
	 * Is the event a keyboard command (copy, paste, cut, highlight all)
	 *
	 * @param e
	 * @returns {boolean|metaKey|*|metaKey|boolean}
	 */
	function keyIsKeyboardCommand(e) {
	  var keyCode = keyCodeFromEvent(e);
	  return keyIsCommandFromEvent(e) &&
	    (
	      keyCode == KEYS["A"] ||
	      keyCode == KEYS["X"] ||
	      keyCode == KEYS["C"] ||
	      keyCode == KEYS["V"]
	    );
	};


	/**
	 * Is the event the tab key?
	 *
	 * @param e
	 * @returns {boolean}
	 */
	function keyIsTab(e) {
	  return keyCodeFromEvent(e) == KEYS["TAB"];
	};


	/**
	 * Strip all characters that are not in the range 0-9
	 *
	 * @param string
	 * @returns {string}
	 */
	function numbersOnlyString(string) {
	  var numbersOnlyString = "";
	  for(var i = 0; i < string.length; i++) {
	    var currentChar = string.charAt(i);
	    var isValid = !isNaN(parseInt(currentChar));
	    if(isValid) { numbersOnlyString += currentChar; }
	  }
	  return numbersOnlyString;
	};


	/**
	 * Apply a format mask to the given string
	 *
	 * @param string
	 * @param mask
	 * @returns {string}
	 */
	function applyFormatMask(string, mask) {
	  var formattedString = "";
	  var numberPos = 0;
	  for(var j = 0; j < mask.length; j++) {
	    var currentMaskChar = mask[j];
	    if(currentMaskChar == "X") {
	      var digit = string.charAt(numberPos);
	      if(!digit) {
	        break;
	      }
	      formattedString += string.charAt(numberPos);
	      numberPos++;
	    } else {
	      formattedString += currentMaskChar;
	    }
	  }
	  return formattedString;
	};
	function cardFromNumber(num) {
		var card, j, len;
		num = (num + '').replace(/\D/g, '');
		for (j = 0, len = cards.length; j < len; j++) {
		  card = cards[j];
		  if (card.pattern.test(num)) {
		    return card;
		  }
		}
	};

	/**
	 * Luhn algorithm for credit card validation
	 * https://gist.github.com/DiegoSalazar/4075533
	 *
	 * @param string
	 * @returns {boolean}
	 */
    function isValidCreditCard(value) {
		// return false if input value is empty
		if (!value) return false;
		// accept only digits, dashes or spaces
		if (/[^0-9-\s]+/.test(value)) return false;
		// The Luhn Algorithm
		var nCheck = 0, nDigit = 0, bEven = false;
		value = value.replace(/\D/g, "");
		for (var n = value.length - 1; n >= 0; n--) {
			var cDigit = value.charAt(n),
				nDigit = parseInt(cDigit, 10);

			if (bEven) {
				if ((nDigit *= 2) > 9) nDigit -= 9;
			}
			nCheck += nDigit;
			bEven = !bEven;
		}
		return (nCheck % 10) == 0;
	}

	var iteration = 0;
	var myVar = setInterval(function(){ 
		if(iteration == 5 || $('#_GUARANTEE_SealSpan_frame').length > 0) {
			$('#_GUARANTEE_SealSpan_frame').remove();
			myStopFunction();
		}
		iteration++;
		}, 2000);


	function myStopFunction() {
	  clearInterval(myVar);
	}
	
	function addProducttUtagByJson (productsJson) {
		var index = 0;
		if(productsJson != null){
			while (productsJson[index]) {
				var product = productsJson[index];
				var productImageURL = product.productexternalid != 0 ? 'https://' + SitePreferences.amplienceHost +'/i/hmk/'+ product.productexternalid+'?h=1220&w=1350' : '';
				var productURL = util.appendParamsToUrl(Urls.getHttpProductUrl, {"pid": product.variantid});
				if(typeof utag != undefined && utag != null ){ 
					utag.link({ 
						"enhanced_actions"		:	"add", 
						"product_id"			:	[product.productid],
			            "product_name"			:	[product.productname],
			            "product_brand"			:	[product.brand],
			            "product_quantity"		:	[product.quantity.toString()],
			           	"product_price"			:	["0.00"],
			            "product_unit_price"	:	["0.00"],
			            "product_category"		:	[product.productcategory],
			            "product_sku"			:	[product.productsku],
			            "product_child_sku"		:	[product.productchildsku], 
			            "product_img_url"		:	[productImageURL],
			            "product_url"			:	[productURL],
				        "product_size"			:	[product.productsize]
					});
				}
					
				index++;
			}
		}
	}
	
function getDateSuffix(day) {
	if (day > 3 && day < 21) return 'th'; 
	switch (day % 10) {
		case 1:  return "st";
		break;
	 		case 2:  return "nd";
		break;
			case 3:  return "rd";
			break;
		default: return "th";
	}	
}

function formatPhoneNumber(phoneNumberString) {
    var cleaned = ('' + phoneNumberString).replace(/\D/g, '')
    var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/)
    if (match) {
      return '(' + match[1] + ') ' + match[2] + '-' + match[3]
    }
    return null
  }

function formatOrderDateConfirmationPage() {
  if($('.order-date .value').length) {
      var confirmationOrderDate= $('.order-date .value').text().split(' ');        
      var date = confirmationOrderDate[1];
      date = date.substr(0,date.length-1);
      var arrivalDateSuffix = getDateSuffix(date);
      
      var finalFormattedDate = confirmationOrderDate[0]+" "+ date+ ""+ arrivalDateSuffix + ", "+ confirmationOrderDate[2];
      $('.order-date .value').html(finalFormattedDate);        
  }
}

function formatPhoneNumberConfirmationPage() {
	if($("#formattedPhoneNumber").length){
		var formattedPhone = formatPhoneNumber($("#formattedPhoneNumber").text());
		  $("#formattedPhoneNumber").html(formattedPhone);
	}  
}

function makeUniqueIDsForRadioButtons() {
	var uniqueID = $($('#schedule-with-rep :input')[$('#schedule-with-rep :input').length-1]).attr('id') +'_later';
	$($('#schedule-with-rep :input')[$('#schedule-with-rep :input').length-1]).attr('id',uniqueID);
}

function getCitiesAndStatesForZipCode(zipCodeParam){
    var zipCode = zipCodeParam;
    var result = null;
    if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCode)) {            
        $.ajax({
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            type: "get",
            url: Urls.getCitiesAndStatesForZipCode,
            async: false,
            cache: false,
            data: {"zipcode": zipCode},
            success: function (response) {            	
            	if (response != null) {        		    
        		    result = response;
        	    }
            },
            error: function () {                
            }
        });
        return result;
    }	
}

function validateShippingAddress() {
    var zipCode = $('#dwfrm_singleshipping_shippingAddress_addressFields_postal').val();
	var	city = $('#dwfrm_singleshipping_shippingAddress_addressFields_city').val();
	var state = $('#dwfrm_singleshipping_shippingAddress_addressFields_states_state').val();
	var isValidAddress = false;
    if ((/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCode)) && (city.trim()) && (state.trim())) {                       
    	var isValidCity = false, isValidState = false;
        var citiesAndStatesForZipCode = getCitiesAndStatesForZipCode(zipCode); 
    	if (citiesAndStatesForZipCode != null && citiesAndStatesForZipCode.cities.length > 0) {                 		
		    for (var i = 0; i < citiesAndStatesForZipCode.cities.length; i++){
		        var data = citiesAndStatesForZipCode.cities[i];
		        if (city.trim().toString().toLowerCase() === data.city.toLowerCase()) {
		        	isValidCity = true;
				}
		        if (state.toString().toLowerCase() === data.state.toLowerCase()) { 
		        	isValidState = true;
				}      
		    }    		    
		    if (!isValidCity) {
				//display error for city
		    	if ($(".ship-city .error.count-reg.reg-error-singleshippingcity").length < 1) {
		            $(".ship-city .field-wrapper").append("<span class='error count-reg reg-error-singleshippingcity'>Please enter a valid city</span>");
		            $("#dwfrm_singleshipping_shippingAddress_addressFields_city").addClass("error");
		        }
			} else {
		    	 if ($(".ship-city .error.count-reg.reg-error-singleshippingcity").length > 0) {
		             $(".ship-city .error.count-reg.reg-error-singleshippingcity").remove();
		         }
		    }    		    
		    if (!isValidState) {
				//display error for state
		    	if ($(".ship-state .error").length < 1) {
		            $(".ship-state .field-wrapper").append("<span class='error'>Please select a valid state </span>");
		            $("#dwfrm_singleshipping_shippingAddress_addressFields_states_state").addClass("error");
		        }    
			} else {
		    	 if ($(".ship-state .error").length > 0) {
		             $(".ship-state .error").remove();
		         }
		    }
		    if ($(".ship-zipcode .error.errorZipValidation").length > 0) {
	            $(".ship-zipcode .error.errorZipValidation").remove();
	        }
		} else {
	    	if ($(".ship-zipcode .error.errorZipValidation").length < 1) {
	            $(".ship-zipcode .field-wrapper").append("<span class='error errorZipValidation'>Please enter a valid ZIP Code</span>");
	            $("#dwfrm_singleshipping_shippingAddress_addressFields_postal").addClass("error");
	        }
	    }        	
    	if(isValidCity == true && isValidState == true) {
    		isValidAddress = true;            		      		       		
    	}else {
    		isValidAddress = false;            		
    	}            
        return isValidAddress; 
	} else {
		return isValidAddress;
	}
}

if ($('.new-customer-info-page').length > 0) {
	$(document).on('click', '#mobileShippingSubmitButton', function (e) {
		utag.link({ eventCategory: 'Cart CTA', eventLabel: 'Continue to Shipping CTA', eventAction: 'Customer Info' });
		var isValidShippingAddress = false;
		var isValidForm = false;
		$("form.customer-info-form input[type=text],input[type=email],input[type=tel],select[id=dwfrm_singleshipping_shippingAddress_addressFields_states_state]").each(function(){
			$(this).focus().trigger('focusout');
			});
		isValidForm = $('form#dwfrm_singleshipping').validate().valid();
		if(isValidForm) {
			$('#mobileShippingSubmitButton').trigger('focus');
		} else {
			var focusFirstErrorField;
			$("form.customer-info-form input[type=text],input[type=email],input[type=tel],select[id=dwfrm_singleshipping_shippingAddress_addressFields_states_state]").each(function(){
				if($(this).hasClass('error')) {
					focusFirstErrorField = $(this);
					return false;
				}
			});
			focusFirstErrorField.on('focus', function() {
			   document.body.scrollTop += this.getBoundingClientRect().top - 10;
			});
			focusFirstErrorField.trigger('focus');
			return false;
		}
		isValidShippingAddress = validateShippingAddress();
		if (isValidShippingAddress && (isValidForm)) {
			$(this).trigger('focus');
			return true;
		} else {
			return false;
		}
	});
}

function updateErrorText($this, error) {
	if($this.attr('type') == "email") {
		error.html(Resources.VALIDATE_EMAIL_REQUIRED);
	} else {
		var temp = $this.attr('id').split('_');
		var id = temp[temp.length-1];
		if(id == "firstName") {
			error.html(Resources.VALIDATE_FIRSTNAME);
		} else if(id == "lastName") {
			error.html(Resources.VALIDATE_LASTNAME);
		} else if(id == "address1") {
			error.html(Resources.VALIDATE_ADDRESS1);
		} else if(id == "city") {
			error.html(Resources.VALIDATE_CITY);
		} else if(id == "state") {
			error.html(Resources.VALIDATE_STATE);
		} else if(id == "postal") {
			error.html(Resources.VALIDATE_POSTAL);
		} else if(id == "phone") {
			error.html(Resources.VALIDATE_PHONE);
		}
	}
	return true;
}

function initCheckoutMobileRedesign() {
	if ($('.new-customer-info-page').length > 0 || $('.customer-info-update-form').length > 0) {
		$("input[id=dwfrm_singleshipping_shippingAddress_email_emailAddress],input[id=dwfrm_singleshipping_shippingAddress_addressFields_firstName],input[id=dwfrm_singleshipping_shippingAddress_addressFields_lastName],input[id=dwfrm_singleshipping_shippingAddress_addressFields_address1],input[id=dwfrm_singleshipping_shippingAddress_addressFields_address2],input[id=dwfrm_singleshipping_shippingAddress_addressFields_city],input[id=dwfrm_singleshipping_shippingAddress_addressFields_phone],input[id=dwfrm_singleshipping_shippingAddress_addressFields_postal],select[id=dwfrm_singleshipping_shippingAddress_addressFields_states_state]").each(function(){
			var contrlId = '#' + $(this)[0].id;
			$(document).on('focusout', contrlId, function (e) {
			 	e.preventDefault();
			 	e.stopPropagation();
			 var validField = $(this).valid();
			 if($(this).attr('type') == 'tel' && $(this).hasClass('postal')) {
				 var zipCode = $(this).val();
				 validField = /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCode);
				 if(!validField) {
					 $(this).addClass('error');
					 if (zipCode != "" && $('.mobile-checkout-process-ab').length > 0) {
						if ($('#dwfrm_singleshipping_shippingAddress_addressFields_postal-error').length < 1) {
							$(".ship-zipcode .field-wrapper").append("<span class='error errorZipValidation'>" + Resources.INVALID_ZIP + "</span>");
							$("#dwfrm_singleshipping_shippingAddress_addressFields_postal").addClass("error");
						} else {
							if ($(".ship-zipcode .error.errorZipValidation").length > 0) {
								$(".ship-zipcode .error.errorZipValidation").remove();
							}
							$('#dwfrm_singleshipping_shippingAddress_addressFields_postal-error').text(Resources.INVALID_ZIP);
							$('#dwfrm_singleshipping_shippingAddress_addressFields_postal-error').show();
						}
					}
			 } else {
				if ($(".ship-zipcode .error.errorZipValidation").length > 0) {
					$(".ship-zipcode .error.errorZipValidation").remove();
				}
			 $(this).removeClass('error');
				 }
			 }
			 if (!validField) {
					$("#" + $(this).attr('id') + "_label").addClass('label_error');
					//remove duplicate errors
					if($(this).siblings('.error').length == 2) {
						$($(this).siblings('.error')[1]).remove();
					}
					var error = $($(this).siblings('.error')[0]);
					if(error.length == 1 && error.text() == Resources.VALIDATE_REQUIRED) {
						updateErrorText($(this), error);
					}
				} else {
					$("#" + $(this).attr('id') + "_label").removeClass('label_error');
				}
			});
		});
		$('#dwfrm_singleshipping_shippingAddress_addressFields_address1').addClass("apopobox");
		$('.opcphones input').inputmask("(999) 999-9999", { showMaskOnHover: false, showMaskOnFocus: false });

	}
	
	
	function getSelectedPaymentMethod(){
		var selectedPaymentMethod = '';
		if($('#is-CREDIT_CARD').parent().hasClass('checked')) {
		selectedPaymentMethod = $('#is-CREDIT_CARD').val();
		} else if($('#is-SYNCHRONY_FINANCIAL').parent().hasClass('checked')) {
		selectedPaymentMethod = $('#is-SYNCHRONY_FINANCIAL').val();
		}
		return selectedPaymentMethod;
	}
	
	$(document).on('click', '.editLinkToCart', function (e) {
		var paymentMethod = getSelectedPaymentMethod();
        if ($('#editLinkToCart').length > 0) {
			utag.link({eventCategory: 'Cart CTA', eventAction: 'Payment', eventLabel: 'Update Items CTA'});
			if (paymentMethod == 'CREDIT_CARD' && jQuery('#dwfrm_billing').validate().checkForm()) {
				$('#editLinkToCart').trigger('click');
        	} else {
        		window.location.href = window.location.href = Urls.redirectToCart;
        	}
            return true;
        }
    });

    $(document).on('click', '.editLinkToDelivery', function (e) {
    	var paymentMethod = getSelectedPaymentMethod();
		if ($('#editLinkToDelivery').length > 0) {
			utag.link({eventCategory: 'Cart CTA', eventAction: 'Payment', eventLabel: 'Update Delivery CTA'});
			if (paymentMethod == 'CREDIT_CARD' && jQuery('#dwfrm_billing').validate().checkForm()) {
        		$('#editLinkToDelivery').trigger('click');
	        } else {
	    		window.location.href = window.location.href = Urls.redirectToDeliveryAB;
	    	}
            return true;
        }
    });

}

$(document).on('click', '#mobileUpdateCustomerButton', function (e) {
	e.preventDefault();
	var isValidShippingAddress = false;
	var invalidInputs = [];
	var isValidForm = false;
	if($(".inMarketZipError").length > 0) {
		$(".inMarketZipError").text('');
	}
	$("input[id=dwfrm_singleshipping_shippingAddress_email_emailAddress],input[id=dwfrm_singleshipping_shippingAddress_addressFields_firstName],input[id=dwfrm_singleshipping_shippingAddress_addressFields_lastName],input[id=dwfrm_singleshipping_shippingAddress_addressFields_address1],input[id=dwfrm_singleshipping_shippingAddress_addressFields_address2],input[id=dwfrm_singleshipping_shippingAddress_addressFields_city],input[id=dwfrm_singleshipping_shippingAddress_addressFields_phone],input[id=dwfrm_singleshipping_shippingAddress_addressFields_postal],select[id=dwfrm_singleshipping_shippingAddress_addressFields_states_state]").each(function(){
		$(this).focus().trigger('focusout');
		if ($('.customer-info-update-form').length > 0 && $(this).hasClass("error")) {
			invalidInputs.push($(this)[0].id);
		}
	  });
	if (invalidInputs.length > 0) {
		isValidForm = false;
	} else {
		isValidForm = $('form#dwfrm_singleshipping').validate().valid();
	}
	isValidShippingAddress = validateShippingAddress();
	if (isValidShippingAddress && (isValidForm)) {
		$(this).trigger('focus');
		var email = $('#dwfrm_singleshipping_shippingAddress_email_emailAddress').val();
		var firstName = $('#dwfrm_singleshipping_shippingAddress_addressFields_firstName').val();
		var lastName = $('#dwfrm_singleshipping_shippingAddress_addressFields_lastName').val();
		var addressLine1 = $('#dwfrm_singleshipping_shippingAddress_addressFields_address1').val();
		var addressLine2 = $('#dwfrm_singleshipping_shippingAddress_addressFields_address2').val();
		var city = $('#dwfrm_singleshipping_shippingAddress_addressFields_city').val();
		var state = $('#dwfrm_singleshipping_shippingAddress_addressFields_states_state').val();
		var zipCode = $('#dwfrm_singleshipping_shippingAddress_addressFields_postal').val();
		var phone = $('#dwfrm_singleshipping_shippingAddress_addressFields_phone').val();

		var url = util.appendParamsToUrl(Urls.updateCustomerInfo, {"email": email, "firstName": firstName, "lastName": lastName, "address1": addressLine1, "address2": addressLine2, "city": city, "state": state, "zipCode": zipCode, "phone": phone});
		$.getJSON(url, function (response, status, xhr) {
			if (response.inMarketZip == false) {
				$(".inMarketZipError").text('Can not deliver to ' + zipCode);
			} else if (status && status == 'success') {
				if (response) {
					$("#savedEmail").val(email);
					$("#savedFirstName").val(firstName);
					$("#savedLastName").val(lastName);
					$("#saveAddressOne").val(addressLine1);
					if(addressLine2 && addressLine2 != 'null'){
						$("#saveAddressTwo").val(addressLine2);
					} else {
						$("#saveAddressTwo").val('');
					}
					$("#saveCityName").val(city);
					$("#saveStateName").val(state);
					$("#savedZippedCode").val(zipCode);
					$("#savedPhoneNumber").val(phone);
					$('#useasbillingaddress .input-checkbox').trigger('change');
					displayBillingAddressAB();
					$('.customerInfoAB .ui-dialog-titlebar-close').trigger('click');
					if (response.deliveryZoneChanged) {
						$('.editLinkToDelivery').trigger('click');
					} else {
						$('.taxWithZip').each(function () {
							if ($(this)[0].innerHTML.toLowerCase().includes('tax')) {
								$(this)[0].innerHTML = 'Tax (' + zipCode + ')';
							}
						});
					}
				}
			}
		});
	} else {
		$('#'+invalidInputs[0]).focus();
		return false;
	}
});

if ($('.mobile-checkout-process-ab').length > 0) {
	$(document).on('click', '.edit-customer-info-link', function () {
		utag.link({eventCategory: 'Cart CTA', eventAction: 'Payment', eventLabel: 'Update Information CTA'});
		loadCustomerInfo();
	});
}

function loadCustomerInfo() {
	ajax.load({
	        url: util.appendParamsToUrl(Urls.getCustomerInfoModal),
	        callback: function (response) {
	            if (response != '') {
	            	initCustomerInfoModal(response);
					initCheckoutMobileRedesign();
	            }
	        }
	    });
}

/**
 * @function
 * @description create and show mobile delivery calander for mobile
 */
function initCustomerInfoModal (customerData) {
	if (customerData.length > 0) {
		
		var html = '<div class="live-content-wrapper" role="dialog">' + customerData + '</div>';
		
		dialog.open({
            html: html,
            options: {
                maxHeight: 635,
                dialogClass: 'customerInfoAB',
                title: 'Customer Info'
            }
        });
	}
}

function setDeliverTierBasedOnCustomerSelection(params) {
	ajax.getJson({
        url: util.appendParamsToUrl(Urls.setDeliverTierBasedOnCustomerSelection, params),
        callback: function (response) {
        	if (response && response.shippingMethodUpdated == 'True') {
        		if($('.currentPage').length > 0 && $('.currentPage').val() == "deliveryInfo" && $('.isMobileDevice').length > 0 && ($('.isMobileDevice').val() == "false" || $('#isPaypal').data('ispaypal') == 'yes'  || $('.isAmazonPay').val() == 'yes')) {
        			page.refresh();
        		} else if($('.currentPage').length > 0 && $('.currentPage').val() == "addresses") {
        			updateSummary();
        		}
            }
        }
    });
}

$(document).on("click",".tier",function(e) {
	var $li = $(this);
	var silverTier = $("li.tier.0");
	var goldTier = $("li.tier.1");
	var platinumTier = $("li.tier.2");
	if($li.hasClass('selected') == false) {
    	$li.parent().children().removeClass('selected');
    	var silverTierArialabel = silverTier.attr("aria-label").replace(/selected/g,'');
		var goldTierArialabel = goldTier.attr("aria-label").replace(/selected/g,'');
		var platinumTierArialabel = platinumTier.attr("aria-label").replace(/selected/g,'');
		silverTier.attr("aria-label" , silverTierArialabel);
		goldTier.attr("aria-label" , goldTierArialabel);
		platinumTier.attr("aria-label" , platinumTierArialabel);
		var currArialabel = $li.attr("aria-label");
    	$li.attr("aria-label" , "selected " + currArialabel);
    	$li.addClass("selected");
    	if($("li.tier.0").hasClass("selected")) {
    		$(".silver-alert").focus();
    	}
		var params = {"pid": $li.data('pid'), "shippingMethod": $li.attr("data-method"), "shippingCost":$li.attr("data-price"), "format": "ajax"}
    	setDeliverTierBasedOnCustomerSelection(params);
	}
});

$(document).ready(function () {
	$(".mobile-pt-checkout-container .login-box.login-account .remind-me").find("button").wrap("<div class='mobile-signin-button'></div>");
    $(".mobile-payment-method-tabs .CREDIT_CARD").addClass("active");
    $(".mobile-payment-method-tabs .CREDIT_CARD,.mobile-payment-method-tabs .SYNCHRONY_FINANCIAL").addClass("mobile-payment-tabs");
    if ($('.mobile-checkout-process-ab').length > 0) {
    	var $inputCC = $("#dwfrm_billing_paymentMethods_creditCard_number");
		var $inputCCType = $("#dwfrm_billing_paymentMethods_creditCard_type");
		if ($inputCC.length > 0 && $inputCCType.length > 0 && $inputCC.val().length > 0 && $inputCCType.val().length > 0) {
			$inputCC.parent('div').addClass($inputCCType.val() + '-wrapper')
		}
		initPaymentMethodRadioBtns();
		$("#synchronyDigitalBuyButton").attr('disabled','disabled');
	}
    if(SessionAttributes.AMAZON_CHECKOUT) {
    	isValidBillingForm();
    }
});

function initPaymentMethodRadioBtns() {
	var i = 0;
	var watcher = setInterval(function() { 
		if($('#is-CREDIT_CARD').parent().hasClass('checked')) {
			$(".mobile-payment-tabs").removeClass("active");
			$('#is-CREDIT_CARD').closest('.mobile-payment-tabs').addClass('active')
			$('#is-CREDIT_CARD').closest('.mobile-payment-tabs').find('.payment-method').addClass('payment-method-expanded')
			stopWatcher();
		} else if($('#is-SYNCHRONY_FINANCIAL').parent().hasClass('checked')) {
			$(".mobile-payment-tabs").removeClass("active");
		 	$('#is-SYNCHRONY_FINANCIAL').closest('.mobile-payment-tabs').addClass('active')
		 	$('#is-SYNCHRONY_FINANCIAL').closest('.mobile-payment-tabs').find('.payment-method').addClass('payment-method-expanded')
			
			stopWatcher();
		} else if(i == 10) {
			stopWatcher();
		}
		i++;
	}, 1000);
	
	function stopWatcher() {
	  clearInterval(watcher);
	}
}



exports.init = function () {
	var billtoAccountVal = $('#dwfrm_synchrony_billToAccountNumber').val();
	if(billtoAccountVal == 'null'){
		$('#dwfrm_synchrony_billToAccountNumber').val('');
	}
	
	initCreditCardValidation();
	initAddressValidation();
	
	if($('.error-form-inside').attr("data-attribute") == 'checkout_fail') {		
		utag.link({eventCategory: 'Checkout',eventLabel: 'Checkout_Fail', eventAction : 'submit'});
	}
	
	formatOrderDateConfirmationPage();
    formatPhoneNumberConfirmationPage(); 
	initCheckoutMobileRedesign();
}
