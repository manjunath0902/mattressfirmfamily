'use strict';
var ajax = require('../../ajax'),
	util = require('../../util');

/**
 * @function
 * @description Method to intialize delivery calander for desktop and mobile.
 * entered address information as URL request parameters.
 */
function dSCalendarSlider() {
	if ($(".ds-calendar-wrap").length > 0) {
			// selecter to identify mobile or desktop
		$( ".ds-calendar-wrap .js-schedule-choices .form-row > label span").text("Choose Your Delivery Date");
			var $mobileCalendar = $('.mobile-delivery-calander');
			$(".ds-calendar-wrap").not('.slick-initialized').slick({
				 slide: '.card',
				 infinite: false,
		         adaptiveHeight: true,
		         slidesToScroll: 1,
		         slidesToShow: 1,
		         swipe: false,
		         arrows: true,
		         accessibility: false,
		         fade: true,
		         draggable: true
		     });
			
		// trigger the click event to set value for schedule delivery in desktop case
			if ($mobileCalendar.length === 0) {
			var deliveryDate = $('[name=dwfrm_singleshipping_shippingAddress_scheduleWithRep]').first();    
		    deliveryDate.trigger("click");
		 }
		 $('.date-wrap').off('click keydown');
		 $('.date-wrap').on('click keydown', function (event) {
			 var slickActiveIndex = $(this).closest('.slick-slide').data('slick-index');
			 if (event.key === 'Tab' && slickActiveIndex > -1) {
				     var $currentSlide = $(this).closest('.slick-active');
					 if ($(this).find('span').hasClass('month-first-date') && $('.ds-calendar-wrap .slick-prev').length > 0 && !$('.ds-calendar-wrap .slick-prev').hasClass('slick-disabled') && event.shiftKey) {
						 event.preventDefault();
						 if ($currentSlide.length > 0 && $currentSlide.prev('.slick-slide').length > 0) {
							 $('.ds-calendar-wrap .slick-prev').trigger('click');
							 $currentSlide.prev('.slick-slide').find('.date-wrap').last().focus();	
						 }
					 } else if ($(this).find('span').hasClass('month-last-date') && $('.ds-calendar-wrap .slick-next').length > 0 && !$('.ds-calendar-wrap .slick-next').hasClass('slick-disabled') && !event.shiftKey){
						 event.preventDefault();
						 $('.slick-next.slick-arrow').click();
						 $($('.month-year-wrap')[slickActiveIndex+1]).focus();
						 //$currentSlide.next('.slick-slide').find('.date-wrap').first().focus();
					 }
			 }
			// in case keypress we need to only allow enter key
			if (event.type == 'click' || event.keyCode == 13 || event.which == 13) {
				selectDeliveryDate($(this));
			}
			
		 });
		 
		$(document).off('click keypress','.size-select-area .fake-input');
		$(document).on("click keypress",'.size-select-area .fake-input', function(event) { 
			event.preventDefault();
			// in case keypress we need to only allow enter key
			if (event.type == 'keypress' && (event.keyCode != 13 || event.which != 13)) {
				return;
			}
			var $target =  $(this).closest('.size-select-area');
			var hasClass =  $target.hasClass('active-drop');
		    $( ".size-select-area").removeClass('active-drop');
		    if(hasClass)
		    {
		    	$target.removeClass('active-drop');
		    }
		    else
		    {
		    	$target.addClass('active-drop');
		    }		    
		});
		//also add on .redCarpet-Delivery-Section-Popup element for popup on billing page in simple checkout.
		$("#wrapper,.redCarpet-Delivery-Section-Popup").click( function(event){
		    if ( !$(event.target).closest('#scheduler-dropdown').length ) {
		     $( "#scheduler-dropdown .size-select-area").removeClass('active-drop');
		    }
		});
		
		$(document).off('click keypress','.li-select');
		$(document).on("click keypress",'.li-select', function(event) {
			if (event.type == 'keypress' && (event.keyCode != 13 || event.which != 13)) {
				return;
			}
			var deliveryTimeRange = ''
			if($(this).data('starttime') != undefined && $(this).data('endtime') != undefined  ) {
				deliveryTimeRange = $(this).data('starttime')+' - '+$(this).data('endtime')
				$('.fake-input').html(deliveryTimeRange);
			}
			//Can be skipped but reseting it on save end
			if($(this).data('datetoshow') != undefined) {
				$('#selectedSlotDate').html($(this).data('datetoshow'));
			}
			$(this).addClass("selected").siblings().removeClass("selected");
			var $target =  $(this).closest('.size-select-area');
			if($target.hasClass('active-drop')){
				$target.removeClass('active-drop');
			}
			//Set the delivery date and time in form hidden fields and save delivery schedule 
			var deliveryTime = deliveryTimeRange.replace(RegExp(' ', 'g'), '');
            var weekdayTimeVal = $(this).data('slotdate');
            $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryDate').val(weekdayTimeVal + ', ' + deliveryTime);
            $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryTime').val(deliveryTime);
            setDeliveryDate();
		});
		
		// handler to choose mobile delivery date in deliver calendar Popup.
		$(document).off('click keypress','.btn-choose-mobilecalender-date');
		$(document).on('click keypress','.btn-choose-mobilecalender-date',function() {
			if (event.type == 'keypress' && (event.keyCode != 13 || event.which != 13)) {
				return;
			}
			 var isPaypal = $("#isPaypal").attr("data-ispaypal");
			 var $selectedSlot = $('.ds-calendar-wrap .date-wrap .active-slot');
			 if ($selectedSlot.length > 0) {
				 var $selectedSlotData = $selectedSlot.closest("td");
				 if ( $selectedSlotData.length > 0 && $selectedSlotData.data("slot-key") !=undefined && $selectedSlotData.data("mobile-date") !=undefined) {
					var deliveryDate = $selectedSlotData.data("mobile-date").trim();
					var selectedDate = $selectedSlotData.data("slot-key");
					var mobileDateToShow = $selectedSlotData.data("mobiledate-toshow");
					$('#delivery_dates').attr('data-slotkey',selectedDate);
					$('#delivery_dates #delivery_date').attr('data-delivery-date',deliveryDate);
					$('#delivery_dates #delivery_date').text(mobileDateToShow);
			        $("#timeslots").find(".timeslot").each(function(){

			        		if (selectedDate == $(this).attr('name')) {
			        			$(this).show();
			        			$(this).addClass('active');
							} else {
								$(this).hide();
								$(this).removeClass('active');
							}
			    	});
			        var selectedTime = $(".delivery_timeSlot.active option:selected").text().replace(RegExp(' ', 'g'), '').trim();
			        $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryTime').val(selectedTime);		
			    	$('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryDate').val(deliveryDate + ', ' + selectedTime );
					$('form#dwfrm_singleshipping_shippingAddress').find('button').removeClass('disabled');

					if (SessionAttributes.AMAZON_CHECKOUT == true) {
						 setDeliveryDate();
				    }
					if (isPaypal && isPaypal == 'yes' || SessionAttributes.AMAZON_CHECKOUT == true) {
						$('#deliveryTime').val(selectedTime);
				    	$('#deliveryDate').val(deliveryDate + ', ' + selectedTime);
					}
				 }
			 }
			 // call delay function to add and remove class after delay.
			 addRemoveClassOnDelay($('#delivery_dates #delivery_date'),'red',2000);
			 // close calander dialog after selecting date.
			 if ($('.mobile-calande-wrap.ui-dialog-content').length > 0) {
				 $('.mobile-calande-wrap.ui-dialog-content').dialog("close");
			 }
		});
		
		// set selected date and show delivery slots widget on page load and move slick slide to selected date.
		 var $selectedSlot = $('.ds-calendar-wrap .date-wrap .active-slot');
		 if ($selectedSlot.length > 0) {
			 // move slick to selected slot slide
			 var slickActiveIndex = $selectedSlot.closest('.slick-slide').data('slick-index');
			 if (slickActiveIndex) {
				 $('.ds-calendar-wrap').slick('slickGoTo', slickActiveIndex);
			 }
			 if($selectedSlot.closest("td").length > 0 && $selectedSlot.closest("td").data("slot-key") !=undefined) {
				 // show delivery dates 
				 if ($mobileCalendar.length === 0) {
					loadSelectedDateSection($selectedSlot.closest("td").data("slot-key"));
				}
				 showSelectedDeliveryDate($selectedSlot);
			 }
		 }
	}
	removeDuplicateIDsOfOldDeliveryScheduler();
}

/**
 * @function
 * @Fixing ADA issues 
 */
function removeDuplicateIDsOfOldDeliveryScheduler() {
	if ($("div[id=uniform-dwfrm_singleshipping_shippingAddress_scheduleWithRep]").length>1) {
		var newID = $("div[id=uniform-dwfrm_singleshipping_shippingAddress_scheduleWithRep]").last().attr("id") + "_later";
		$($("div[id=uniform-dwfrm_singleshipping_shippingAddress_scheduleWithRep]").last()).attr("id",newID);
	}
}

/**
 * @function
 * @description Change delivery date.
 * @Parms selector to select the date
 */
function selectDeliveryDate(deliveryDate) {
	 var $mobileCalendar = $('.mobile-delivery-calander');
	// stop ajax call for slots on unavailable and active slots.
	 if (!deliveryDate.find('span').hasClass('no-slot-available') && !deliveryDate.find('span').hasClass('active-slot')) {
		 $('.date-wrap').removeClass("animate-date");
		 $('.date-wrap').find('span').removeClass("active-slot");
		 deliveryDate.addClass("animate-date");
		 deliveryDate.find('span').addClass("active-slot");
		 if(deliveryDate.closest("td").length>0 && deliveryDate.closest("td").data("slot-key") !=undefined) {
			 // load selected delivery slots section in desktop case
				 if ($mobileCalendar.length === 0) {
					 loadSelectedDateSection(deliveryDate.closest("td").data("slot-key"));
			 } else {
				 
			 }
			 // show date on the top of the products lines on delivery selection.
			 showSelectedDeliveryDate(deliveryDate);
		 }
		 setTimeout(function(){
			 $('.date-wrap').removeClass("animate-date");
	    }, 1000);
	 }
}

/**
 * @function
 * @description Add a class and remove it after some delay.
 * @Parms selector to add/remove class type:JQuery Element, className to add/remove type:string, timeDelay to add/remove type:number
 */
function addRemoveClassOnDelay(selector, className, timeDelay) {
	 if (selector.length > 0) {
			!selector.hasClass(className) ? selector.addClass(className) : null;
			setTimeout(function(){
				selector.removeClass(className);
			}, timeDelay);
		}
}

/**
 * @function
 * @description show date above the red carpet lines in mobile and below the calander popup in mobile.
 * @Parms selectedSlot expected selected span(with class=.active-slot
 */
function showSelectedDeliveryDate(selectedSlot) {
	 var $mobileCalander = $('.mobile-delivery-calander');
	 var isPaypal = $("#isPaypal").attr("data-ispaypal");
	 if (isPaypal && isPaypal == 'yes' || SessionAttributes.AMAZON_CHECKOUT == true) {
		 $timeBoard = ($mobileCalander.length > 0) ? $('#SelectedDateWrapper .selected-date') : $("#redcarpet-delivery-date");
	 } else {
		 var $timeBoard = ($mobileCalander.length > 0) ? $('#SelectedDateWrapper .selected-date') : $('.chosen-time'); 
	 }
	 if ($timeBoard.length > 0 && selectedSlot.closest("td").data("date-to-show") !=undefined) {
		 var date = selectedSlot.closest("td").data("date-to-show");
		 var dateString = ($mobileCalander.length > 0) ? date : 'Arrives '+ date + ' (or choose another date below)';
		 $timeBoard.html(dateString);
	 }
}

/**
 * @function
 * @description set the values in the shipping address and save at backend by calling setDeliveryDate.
 * @Parms deliveryDate expects 	"weekdayTimeVal + ', ' + deliveryTime" and deliveryTime expected time.
 */
function setAndSaveDeliveryDates(deliveryDate, deliveryTime) {
	$('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryDate').val(deliveryDate.trim());
    $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryTime').val(deliveryTime.trim());
    setDeliveryDate();
}

function loadSelectedDateSection(slotKey) {
	var params = {
            format: 'ajax',
            slotKey: slotKey
    };
	ajax.load({
	        url: util.appendParamsToUrl(Urls.getSelectedATPSlots, params),
	        callback: function (response) {
	            if (response != '') {
	            	if ($('#SelectDateWrapper').length > 0) {
	            		$('#SelectDateWrapper').html(response);
	            		// adding and remove class on delay.
	            		addRemoveClassOnDelay($('#SelectDateWrapper #selectedSlotDate'),'red',2000);
	            	}
	            	//Set the delivery date and time in form hidden fields and save delivery schedule
	            	//.single-slot represent single slot available in date.
	            	var selectedSlot = '';
	            	if ($('.single-slot').length > 0) {
	            		selectedSlot = $('.single-slot');
	            	} else if($('.select-item.li-select.selected').length > 0) {
	            		selectedSlot = $('.select-item.li-select.selected');
	            	}
	    			if(selectedSlot) {
		    			var deliveryTimeRange = selectedSlot.data('starttime')+' - '+selectedSlot.data('endtime')
		            	var deliveryTime = deliveryTimeRange.replace(RegExp(' ', 'g'), '');
		                var weekdayTimeVal = selectedSlot.data('slotdate');
		                var deliveryDate = weekdayTimeVal + ', ' + deliveryTime;
		                setAndSaveDeliveryDates(deliveryDate,deliveryTime);
	    			}
	            }
	        }
	    });
}
/**
 * @function
 * @description intialize mobile calander on delivery date click event
 */
function loadMobileDeliveryCalander(slotKey) {
	var params = {
            format: 'ajax',
            slotKey: slotKey
    };
	ajax.load({
	        url: util.appendParamsToUrl(Urls.getMobileCalander, params),
	        callback: function (response) {
	            if (response != '') {
	            	initMobileCalander(response);
	            }
	        }
	    });
}

/**
 * @function
 * @description create and show mobile delivery calander for mobile
 */
function initMobileCalander (calenderData) {
	if (calenderData.length > 0) {
		var newCalender_mob_ab = "";
		if($(".new-delivery-info").length > 0) {
			newCalender_mob_ab = "mobile-slot-calendar new-calender";
		}
		else {
			newCalender_mob_ab = "mobile-slot-calendar";
		}
		$('<div id="MobileCalanderWrapper" class="mobile-calande-wrap"></div>').appendTo('body')
	    .html(calenderData)
	    .dialog({
	      modal: true,
	      zIndex: 10000,
	      autoOpen: true,
	      dialogClass: newCalender_mob_ab,
	      width: 'auto',
	      modal: true,
	      resizable: false,
	      closeOnEscape: false,
	      open: function(event, ui) {
	          $(".ui-dialog-titlebar-close").hide();
	          $(".ui-dialog-titlebar.ui-widget-header.ui-corner-all.ui-helper-clearfix").hide();
	          dSCalendarSlider();
	      },
	      close: function(event, ui) {
	        $(this).remove();
	      }
	    });
	}
}
/**
 * Save the Delivery Date
 * @returns
 */
function setDeliveryDate() {
	//Added this check to stop calling this function for one page Checkout 
	if(window.location.href.indexOf('addresses') >0 && !SessionAttributes.AMAZON_CHECKOUT) { 
		return;
	}
	var buttonName = 'dwfrm_singleshipping_shippingAddress_saveDeliveryDate';
	var formId = 'dwfrm_singleshipping_shippingAddress';
    var options = {
        url: $('#'+formId).attr('action'),
        data: $('#'+formId).serialize() + '&' + buttonName + '=x',
        type: 'POST'
    };
    //In amazon checkout page disable button until delivery selection updates in backend
    if (SessionAttributes.AMAZON_CHECKOUT == true && $('.amazon-submit-order').length > 0) {
    	$('.amazon-submit-order').attr("disabled", "disabled");
    	$('.amazon-submit-order-minisummary').attr("disabled", "disabled");
    }
    $.ajax(options).done(function (data) {
    	if (SessionAttributes.AMAZON_CHECKOUT == true && $('.amazon-submit-order').length > 0) {
        	if ($('#dwfrm_billing').length > 0 && $('#dwfrm_billing').validate().checkForm() && $('.count-reg').length < 1) {
		        $('.amazon-submit-order').removeAttr('disabled');
		        $('.amazon-submit-order-minisummary').removeAttr('disabled');
		       } else {
		    	   $('.amazon-submit-order').attr("disabled", "disabled");
		       	   $('.amazon-submit-order-minisummary').attr("disabled", "disabled");
		     }
		}
    	if (typeof(data) !== 'string') {
            if (data.success) {
            	//console.log("success");
            } else {
                //window.alert(data.message);
                return false;
            }
        }
    });
}
exports.dSCalendarSlider = dSCalendarSlider;
exports.loadMobileDeliveryCalander = loadMobileDeliveryCalander;