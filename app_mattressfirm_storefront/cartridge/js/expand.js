(function ($) {
  var expand = {
    btn: $('.expand'),
    open: 'open',

    init: function () {
      expand.setupListeners();
      expand.desktopOpen();
    },

    setupListeners: function() {
      expand.btn.on('click', function() {
        var data = $(this).data('expand'),
            content = $('[data-expand-open="' + data + '"]');

        $(this).toggleClass(expand.open);
        content.slideToggle();
      });
    },

    desktopOpen: function() {
      var w = window.innerWidth;
      if(w <= 647) {
        expand.btn.removeClass(expand.open);
      }
    }
  };

  $(document).ready(function () {
    expand.init();
  });
})(jQuery);