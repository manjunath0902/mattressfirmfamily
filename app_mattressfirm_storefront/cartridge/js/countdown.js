(function ($) {
	var countdown = {

		init: function () {
			if($('#timer-1').length) {
				var time = $('#timer-1-countdown-time').text();
				countdown.timer('timer-1', time);
			}

			if ($('#timer-2').length) {
				var time = $('#timer-2-countdown-time').text();
				countdown.timer('timer-2', time);
			}
		},

		timer: function (el, time) {
			var countDownDate = new Date(time);

			var x = setInterval(function () {
				var now = new Date().getTime();
				var distance = countDownDate - now;

				var days = Math.floor(distance / ((1000 * 60 * 60 * 24))) * 24;
				var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)) + days;
				var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				var seconds = Math.floor((distance % (1000 * 60)) / 1000);


				document.getElementById(el).innerHTML = hours + "hrs " +
					minutes + "min " + seconds + "sec ";

				if (distance < 0) {
					clearInterval(x);
					document.getElementById(el).innerHTML = "EXPIRED";
				}
			}, 1000);
		}
	};

	$(document).ready(function () {
		countdown.init();
	});
})(jQuery);
