(function ($) {
	var pdpReviews = {
    tabs: $('#pdp-tabs'),
    stars: $('#pdp-reviews'),
    reviews: $('#BVRRContainer'),
    
		init: function () {
			pdpReviews.setupListeners();
		},

		setupListeners: function () {
	      pdpReviews.stars.click(function(e) {
					e.preventDefault();
					pdpReviews.openTab();
	        $('html, body').animate({	          
	          scrollTop: (pdpReviews.reviews.length < 1) ? ($("#BVRRContainer").offset().top - 125) : (pdpReviews.reviews.offset().top - 125)
	        }, 500);
	      });
		},

		openTab: function() {
			var all = $('.pdp-tabs li, .pdp-tabs-content li'),
				tab = $('.pdp-tabs li:nth-child(3)'),
				content = $('.pdp-tabs-content-reviews'),
				reviewcontent = $('.pdp-tab-content.expand-content.expand-mobile.pdp-reviews-content');

			if(tab && tab.text() && tab.text() == 'Reviews'){
				
				all.removeClass('is-active');

				tab.addClass('is-active');
				content.addClass('is-active').show();
				reviewcontent.show();
			}
		}
	};

	$(document).ready(function () {
		pdpReviews.init();
		
		$('#pdp-reviews').on('click',function(){
			if (typeof (utag) != "undefined") {
				utag.link({eventCategory: 'Link - Product Detail', eventAction: 'Product Detail Column - Reviews', eventLabel: 'Rating Scroll Click'});
			}
		});			
	});
	$(document).on('click','.bv-write-review', function(){
		if (typeof (utag) != "undefined") {
			utag.link({eventCategory: 'Link - Product Detail', eventAction: 'Review Section -Write a Review', eventLabel: 'Write a Review'});
		}
	});
	$(document).on('click','.bv-content-feedback-btn-container', function(e){
		if (typeof (utag) != "undefined") {
			var selectedOption = e.target.innerText.toLowerCase();
			if(selectedOption.indexOf('yes')>-1) {
				utag.link({eventCategory: 'Link - Product Detail', eventAction: 'Review Section - Helpful', eventLabel: 'Yes'});
			}
			else if(selectedOption.indexOf('no')>-1) {
				utag.link({eventCategory: 'Link - Product Detail', eventAction: 'Review Section - Helpful', eventLabel: 'No'});
			}
		}
	});
	$(document).on('click','.bv-content-feedback-vote.bv-content-feedback-vote-active', function(e){
		if (typeof (utag) != "undefined") {
			var selectedOption = e.target.innerText.toLowerCase();
			if(selectedOption.indexOf('report')>-1) {
				utag.link({eventCategory: 'Link - Product Detail', eventAction: 'Review Section - Helpful', eventLabel: 'Report'});
			}
		}
	});
	
	$(document).on('click','#bv-dropdown-select-reviews-sortby .bv-dropdown-item', function(){
		if (typeof (utag) != "undefined") {
			utag.link({eventCategory: 'Link - Product Detail', eventAction: 'Review Section - Sort by', eventLabel: $(this).text().trim()});
		}
	});
	$(document).on('click','.bv-filter-dropdown .bv-dropdown-item', function(){
		if (typeof (utag) != "undefined") {
			utag.link({eventCategory: 'Link - Product Detail', eventAction: 'Review Section - Filter by Rating', eventLabel: $(this).text().trim()});
		}
	});
})(jQuery);