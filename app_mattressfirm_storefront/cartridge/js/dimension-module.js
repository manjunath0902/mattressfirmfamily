function positionCount(selectedPosition) {
	 if(selectedPosition) {
	    	var count = 0;
	    	if (selectedPosition.hasOwnProperty('HEADUP') || selectedPosition.hasOwnProperty('HeadUp')) {count++;}
	    	if (selectedPosition.hasOwnProperty('FEETUP') || selectedPosition.hasOwnProperty('FeetUp')) {count++;}
	    	if (selectedPosition.hasOwnProperty('FLAT') || selectedPosition.hasOwnProperty('Flat')) {count++;}
	    	if (selectedPosition.hasOwnProperty('ZEROGRAVITY') || selectedPosition.hasOwnProperty('ZeroGravity')) {count++;}
	    }
	 return count;
}
function dimensionModule() {
		 
	 var ddData2 = [{
			text: "Head Up",
			value: "tab-1",
			selected: true,
			description: ""
	 	},
		{
		    text: "Feet Up",
			value: "tab-2",
			selected: false,
			description: ""
		},
		{
		    text: "Flat",
			value: "tab-3",
			selected: false,
			description: ""
		},
		{
		    text: "Zero Gravity",
			value: "tab-4",
			selected: false,
			description: ""
		}
	];
	 
	 var vardimensions = window.pdpDimensionsJson;
	 var selectedVariat = "";
	 

	var ddData = [];
    var inc = 1;
    $.each(vardimensions, function (key, data) {
	    var ddText = key;
	    var ddValue = "tab-" + inc ;
	    if(key == "Queen") {
	    	var ddSelected = true;
	    }
	    else {
	    	var ddSelected = false;
	    }
        
        var ddDescription = vardimensions[key].Name;

        var item = {}
        item ["text"] = ddText;
        item ["value"] = ddValue;
        item ["selected"] = ddSelected;
        item ["description"] = ddDescription;
        ddData.push(item);
        inc++;
	})
	  $('#dd-variant-dropdown').ddslick({
	          data: ddData,
	          width: 272,
	          onSelected: function(selectedData) {
		          var plocation = '#' + selectedData.selectedData.value;
		          var selectedvar = selectedData.selectedData.text;
		          selectedVariat = selectedvar;
		          $(".tab-content2").hide();
		          $(plocation).show();
		          var calHeight = positionCount(vardimensions[selectedVariat]);
		          var lbsLabel;
		          if($('.dd-options-wrap').length == 0) {
		        	  $( "#dd-variant-dropdown ul" ).wrap( "<div class='dd-options-wrap'></div>" );
		          }
		          
		          if(vardimensions[selectedvar].hasOwnProperty('ZeroGravity')) {
		        	  $('.zero-g').show();
		        	  $('#dd-position-dropdown ul.dd-options').children().last().show();
		        	  $('#dd-position-dropdown .dd-options-wrap2').css('height',45*calHeight);
		          }
		          else {
		        	  $('.zero-g').hide();
		        	  $('#dd-position-dropdown ul.dd-options').children().last().hide();
		        	  $('#dd-position-dropdown .dd-options-wrap2').css('height',45*calHeight);
		          }
		          if(vardimensions[selectedvar].hasOwnProperty('HeadUp')) {
		        	  $('.heads-up').show();
		        	  $('#dd-position-dropdown ul.dd-options li:nth-child(1)').show();
		        	  $('#dd-position-dropdown .dd-options-wrap2').css('height',45*calHeight);
		          }
		          else {
	        		  $('.heads-up').hide();
		        	  $('#dd-position-dropdown ul.dd-options li:nth-child(1)').hide();
		        	  $('#dd-position-dropdown .dd-options-wrap2').css('height',45*calHeight);
		        			        	  
		          }
		          if(vardimensions[selectedvar].hasOwnProperty('FeetUp')) {
		        	  $('.feet-up').show();
		        	  $('#dd-position-dropdown ul.dd-options').children("li:nth-child(2)").show();
		        	  $('#dd-position-dropdown .dd-options-wrap2').css('height',45*calHeight);
		          }
		          else {
	        		  $('.feet-up').hide();		        	  
		        	  $('#dd-position-dropdown .dd-options-wrap2').css('height',45*calHeight);
		        	  $('#dd-position-dropdown ul.dd-options').children("li:nth-child(2)").hide();
		          }
		          if(vardimensions[selectedvar].hasOwnProperty('Flat')) {
		        	  $('.flat').show();
		        	  $('#dd-position-dropdown ul.dd-options').children("li:nth-child(3)").show();
		        	  $('#dd-position-dropdown .dd-options-wrap2').css('height',45*calHeight);
		          }
		          else {
	        		  $('.flat').hide();		        	  
		        	  $('#dd-position-dropdown .dd-options-wrap2').css('height',45*calHeight);
		        	  $('#dd-position-dropdown ul.dd-options').children("li:nth-child(3)").hide();
		          }
		          if(vardimensions[selectedVariat].hasOwnProperty('HeadUp')) {
		  	    		$(".heads-up").addClass("active-base");
		  	    		$(".feet-up").removeClass("active-base");
		  	    		$(".flat").removeClass("active-base");
		  	    		$(".zero-g").removeClass("active-base");
			  	   }
			  	    else if(vardimensions[selectedVariat].hasOwnProperty('FeetUp')) {
			  	    	$(".heads-up").removeClass("active-base");
		  	    		$(".feet-up").addClass("active-base");
		  	    		$(".flat").removeClass("active-base");
		  	    		$(".zero-g").removeClass("active-base");
			  	    }
			  	    else if(vardimensions[selectedVariat].hasOwnProperty('Flat')) {
			  	    	$(".heads-up").removeClass("active-base");
		  	    		$(".feet-up").removeClass("active-base");
		  	    		$(".flat").addClass("active-base");
		  	    		$(".zero-g").removeClass("active-base");
			  	    }
			  	    else {
			  	    	$(".heads-up").removeClass("active-base");
		  	    		$(".feet-up").removeClass("active-base");
		  	    		$(".flat").removeClass("active-base");
		  	    		$(".zero-g").addClass("active-base");
			  	    }
		          setTimeout(function(){
		          if(vardimensions[selectedVariat].hasOwnProperty('HeadUp')) {
		    		  $('#dd-position-dropdown').ddslick('select', {index: 0 });
		      	   }
		          else if(vardimensions[selectedVariat].hasOwnProperty('FeetUp')) {
		        	  $('#dd-position-dropdown').ddslick('select', {index: 1 });
			  	    }
			  	    else if(vardimensions[selectedVariat].hasOwnProperty('Flat')) {
			  	    	 $('#dd-position-dropdown').ddslick('select', {index: 2 });
			  	    }
			  	    else {
			  	    	 $('#dd-position-dropdown').ddslick('select', {index: 3 });
			  	    }
		          }, 1000);
		          if(vardimensions.hasOwnProperty(selectedvar)) {					  
		        	  $( ".dimension-wrapper .d-width" ).text(vardimensions[selectedvar].Width);
					  $( ".dimension-wrapper .d-length" ).text(vardimensions[selectedvar].Length);

					  if (vardimensions[selectedvar].Weight != null || vardimensions[selectedvar].Weight != undefined) {
						if (vardimensions[selectedvar].Weight.length) { 
							lbsLabel = ' lbs.';
							$(".d-weight-label").html("Weight Capacity");
						} else {
							lbsLabel = '';
							$(".d-weight-label").empty();

						}
						
						$(".dimension-wrapper .d-weight").html(vardimensions[selectedvar].Weight + lbsLabel);
					  } else {
						  $(".dimension-wrapper .d-weight").empty();
						  $(".d-weight-label").empty();
					  }

		        	  var getPositionClass = document.getElementsByClassName("active-base");
		        	  var CgetPositionClass=$(getPositionClass);
		        	  var getPositionName = $(CgetPositionClass).attr('data-position-attribute');
	        		  $( ".dimension-wrapper .d-maxheight" ).text(vardimensions[selectedvar][getPositionName].MaxHeight);
	        		  $( ".dimension-wrapper .d-minheight" ).text(vardimensions[selectedvar][getPositionName].MinHeight);
		          }	
	          }
          });
	  $('#dd-position-dropdown').ddslick({
		  data: ddData2,
          width: 295,
          onSelected: function(selectedData) {
        	  var plocation = '#' + selectedData.selectedData.value;
	          var selectedvar = selectedData.selectedData.text;
	          var calHeight = positionCount(vardimensions[selectedVariat]);
	          $(".tab-content2").hide();
	          $(plocation).show();
	          if($('.dd-options-wrap2').length == 0) {
	        	  $( "#dd-position-dropdown ul" ).wrap( "<div class='dd-options-wrap2'></div>" );
	          }	
	          if(vardimensions[selectedVariat].hasOwnProperty('ZeroGravity')) {
	        	  $('#dd-position-dropdown ul.dd-options').children().last().show();
	        	  $('#dd-position-dropdown .dd-options-wrap2').css('height',45*calHeight);
	          }
	          else {
	        	  $('#dd-position-dropdown ul.dd-options').children().last().hide();
	        	  $('#dd-position-dropdown .dd-options-wrap2').css('height',45*calHeight);
	          }
	          if(vardimensions[selectedVariat].hasOwnProperty('HeadUp')) {
	        	  $('.heads-up').show();
	        	  $('#dd-position-dropdown ul.dd-options li:nth-child(1)').show();
	        	  $('#dd-position-dropdown .dd-options-wrap2').css('height',45*calHeight);
	          }
	          else {
        		  $('.heads-up').hide();
	        	  $('#dd-position-dropdown ul.dd-options li:nth-child(1)').hide();
	        	  $('#dd-position-dropdown .dd-options-wrap2').css('height',45*calHeight);		        	  
	          }
	          if(vardimensions[selectedVariat].hasOwnProperty('FeetUp')) {
	        	  $('.flat').show();
	        	  $('#dd-position-dropdown ul.dd-options').children("li:nth-child(2)").show();
	        	  $('#dd-position-dropdown .dd-options-wrap2').css('height',45*calHeight);
	          }
	          else {
        		  $('.feet-up').hide();		        	  
	        	  $('#dd-position-dropdown .dd-options-wrap2').css('height',45*calHeight);
	        	  $('#dd-position-dropdown ul.dd-options').children("li:nth-child(2)").hide();
	          }
	          if(vardimensions[selectedVariat].hasOwnProperty('Flat')) {
	        	  $('.flat').show();
	        	  $('#dd-position-dropdown ul.dd-options').children("li:nth-child(3)").show();
	        	  $('#dd-position-dropdown .dd-options-wrap2').css('height',45*calHeight);
	          }
	          else {	        	 
        		  $('.flat').hide();		        	  
	        	  $('#dd-position-dropdown .dd-options-wrap2').css('height',45*calHeight);
	        	  $('#dd-position-dropdown ul.dd-options').children("li:nth-child(3)").hide();	        			        	  
	          }
	          if(selectedvar == 'Head Up') {
	        	  $('.heads-up-img').show();
	    		  $('.feet-up-img').hide();$('.flat-img').hide();$('.gravity-img').hide();
	    		  $( ".dimension-wrapper .d-maxheight" ).text(vardimensions[selectedVariat].HeadUp.MaxHeight);
	    		  $( ".dimension-wrapper .d-minheight" ).text(vardimensions[selectedVariat].HeadUp.MinHeight);
	    		  $(".attributes-wrapper.mt-20").show();
	    		  $(".attributes-wrapper.mt-20.flat-d").hide();
	    		  $(".attributes-wrapper.mt-20.zero-gravity").hide();
	          }
	          else if(selectedvar == 'Feet Up') {
	        	  $('.feet-up-img').show();
	    		  $('.heads-up-img').hide();$('.flat-img').hide();$('.gravity-img').hide();
	    		  $( ".dimension-wrapper .d-maxheight" ).text(vardimensions[selectedVariat].FeetUp.MaxHeight);
	    		  $( ".dimension-wrapper .d-minheight" ).text(vardimensions[selectedVariat].FeetUp.MinHeight);
	    		  $(".attributes-wrapper.mt-20").show();
	    		  $(".attributes-wrapper.mt-20.flat-d").hide();
	    		  $(".attributes-wrapper.mt-20.zero-gravity").hide();
	          }
	        	  
	          else if(selectedvar == 'Flat') {
	        	  $('.flat-img').show();
	    		  $('.heads-up-img').hide();$('.feet-up-img').hide();$('.gravity-img').hide();
	    		  $( ".dimension-wrapper .d-maxheight" ).text(vardimensions[selectedVariat].Flat.MaxHeight);
	    		  $( ".dimension-wrapper .d-minheight" ).text(vardimensions[selectedVariat].Flat.MinHeight);
	    		  $(".attributes-wrapper.mt-20").hide();
	    		  $(".attributes-wrapper.mt-20.flat-d").show();
	    		  $(".attributes-wrapper.mt-20.zero-gravity").hide();
	          }
	        	  
	          else {
	        	  $('.gravity-img').show();
	    		  $('.heads-up-img').hide();$('.feet-up-img').hide();$('.flat-img').hide();
	    		  $( ".dimension-wrapper .d-maxheight" ).text(vardimensions[selectedVariat].ZeroGravity.MaxHeight);
	    		  $( ".dimension-wrapper .d-minheight" ).text(vardimensions[selectedVariat].ZeroGravity.MinHeight);
	    		  $(".attributes-wrapper.mt-20").hide();
	    		  $(".attributes-wrapper.mt-20.flat-d").hide();
	    		  $(".attributes-wrapper.mt-20.zero-gravity").css('display', 'flex');
	          }
          }
	  });
  	   
	  $('#dd-variant-dropdown').click(function () {
			$('#dd-variant-dropdown').find('.dd-select').toggleClass('no-border');
			$('#dd-variant-dropdown').toggleClass('dd-open');
	  });
	  $('#dd-position-dropdown').click(function () {
			$('#dd-position-dropdown').find('.dd-select').toggleClass('no-border');
			$('#dd-position-dropdown').toggleClass('dd-open');
	  });
	  $("#wrapper").click( function(event){
		if ( !$(event.target).closest('#dd-variant-dropdown').length ) {
			$('#dd-variant-dropdown').find('.dd-select').removeClass('no-border');
			$('#dd-variant-dropdown').removeClass('dd-open');
		}
	  });
	  $("#wrapper").click( function(event){
		if ( !$(event.target).closest('#dd-variant-dropdown').length ) {
			$('#dd-position-dropdown').find('.dd-select').removeClass('no-border');
			$('#dd-position-dropdown').removeClass('dd-open');
		}
	  });
	  $('.heads-up').on('click tap', function (e) { 
		  e.preventDefault();
		  $('.heads-up-img').show();
		  $('.feet-up-img').hide();$('.flat-img').hide();$('.gravity-img').hide();
		  $(".heads-up").addClass("active-base");
		  $(".feet-up, .flat, .zero-g").removeClass("active-base");
		  $( ".dimension-wrapper .d-maxheight" ).text(vardimensions[selectedVariat].HeadUp.MaxHeight);
		  $( ".dimension-wrapper .d-minheight" ).text(vardimensions[selectedVariat].HeadUp.MinHeight);
		  $(".attributes-wrapper.mt-20").show();
		  $(".attributes-wrapper.mt-20.flat-d").hide();
		  $(".attributes-wrapper.mt-20.zero-gravity").hide();
	  });
	  $('.feet-up').on('click tap', function (e) { 
		  e.preventDefault();
		  $('.feet-up-img').show();
		  $('.heads-up-img').hide();$('.flat-img').hide();$('.gravity-img').hide();
		  $(".feet-up").addClass("active-base");
		  $(".heads-up, .flat, .zero-g").removeClass("active-base");
		  $( ".dimension-wrapper .d-maxheight" ).text(vardimensions[selectedVariat].FeetUp.MaxHeight);
		  $( ".dimension-wrapper .d-minheight" ).text(vardimensions[selectedVariat].FeetUp.MinHeight);
		  $(".attributes-wrapper.mt-20").show();
		  $(".attributes-wrapper.mt-20.flat-d").hide();
		  $(".attributes-wrapper.mt-20.zero-gravity").hide();
	  });
	  $('.flat').on('click tap', function (e) { 
		  e.preventDefault();
		  $('.flat-img').show();
		  $('.heads-up-img').hide();$('.feet-up-img').hide();$('.gravity-img').hide();
		  $(".flat").addClass("active-base");
		  $(".feet-up, .heads-up, .zero-g").removeClass("active-base");
		  $( ".dimension-wrapper .d-maxheight" ).text(vardimensions[selectedVariat].Flat.MaxHeight);
		  $( ".dimension-wrapper .d-minheight" ).text(vardimensions[selectedVariat].Flat.MinHeight);
		  $(".attributes-wrapper.mt-20").hide();
		  $(".attributes-wrapper.mt-20.flat-d").show();
		  $(".attributes-wrapper.mt-20.zero-gravity").hide();
	  });
	  $('.zero-g').on('click tap', function (e) { 
		  e.preventDefault();
		  $('.gravity-img').show();
		  $('.heads-up-img').hide();$('.feet-up-img').hide();$('.flat-img').hide();
		  $(".zero-g").addClass("active-base");
		  $(".feet-up, .flat, .heads-up").removeClass("active-base");
		  $( ".dimension-wrapper .d-maxheight" ).text(vardimensions[selectedVariat].ZeroGravity.MaxHeight);
		  $( ".dimension-wrapper .d-minheight" ).text(vardimensions[selectedVariat].ZeroGravity.MinHeight);
		  $(".attributes-wrapper.mt-20").hide();
		  $(".attributes-wrapper.mt-20.flat-d").hide();
		  $(".attributes-wrapper.mt-20.zero-gravity").show();
	  });
}
exports.init = function() {
	dimensionModule();
};