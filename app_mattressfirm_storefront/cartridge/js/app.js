/**
 *    (c) 2009-2014 Demandware Inc.
 *    Subject to standard usage terms and conditions
 *    For all details and documentation:
 *    https://bitbucket.com/demandware/sitegenesis
 */

'use strict';

var countries = require("./countries"),
	dialog = require("./dialog"),
	expand = require("./expand"),
	flipclock = require("./flipclock"),
	headerMenu = require("./header-menu"),
	pdpTabs = require("./pdp-tabs"),
	deliveryCountdown = require("./delivery-countdown"),
	countdown = require("./countdown"),
	pdpReveiws = require("./pdp-reviews"),
	sliders = require("./sliders"),
	smoothscroll = require("./smoothscroll"),
	minicart = require("./minicart"),
	mobileMenu = require("./mobile-menu"),
	headerBanner = require("./header-banner"),
	stickyNav = require("./sticky-nav"),
	page = require("./page"),
	rating = require("./rating"),
	searchplaceholder = require("./searchplaceholder"),
	searchsuggest = require("./searchsuggest"),
	searchsuggestbeta = require("./searchsuggest-beta"),
	tooltip = require("./tooltip"),
	scrollTop = require("./scroll-top"),
	hoverIntent = require("./hoverintent"),
	util = require("./util"),
	validator = require("./validator"),
	googleAnalytics = require("./google-analytics"),
	content = require("./content"),
	responsiveSlots = require("./responsiveslots/responsiveSlots"),
	amplience = require("./amplience"),
	zipzone = require("./zipzone"),
	sfemaildialog = require("./sfemaildialog"),
	mobilegeolocator = require("./mobilegeolocator"),
	exacttargetnew = require("./exacttargetnew"),
	progress = require("./progress"),
	addresslookup = require("./addresslookup"),
	addToCart = require("./pages/product/addToCart"),
	tealium = require("./tealium"),
	browserGeoLocation = require("./browserGeoLocation"),
	emailsignup = require("./emailsignup"),
	geolocation = require("./geolocation"),
	brandlandingcontent = require("./brand-landing-content"),
	dimensionmodule = require("./dimension-module"),
	progressive = require('./pages/product/progressive'),
	deliverytracking = require("./deliverytracking");

// if jQuery has not been loaded, load from google cdn
if (!window.jQuery) {
	var s = document.createElement('script');
	s.setAttribute('src', 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js');
	s.setAttribute('type', 'text/javascript');
	document.getElementsByTagName('head')[0].appendChild(s);
}

require('./jquery-ext')();
require('./cookieprivacy')();
//require('./captcha')();

function setRenderedStoresSlickHeight() {
	var max = -1;
	$('#try-in-store-results li').each(function () {
		var h = $(this).prop('scrollHeight') - $(this).position().top;
		max = h > max ? h : max;
	});
	$('#try-in-store-results li').each(function () {
		$(this).css('height', max);
	});
}
function spotlightFeature() {
	$('.features-wrap').slick({
		slidesToShow: 4,
		slidesToScroll: 4,
		slide: '.feature',
		focusOnSelect: true,
		lazyLoad: 'ondemand',
		infinite: false,
		dots: true,
		responsive: [
			{
				breakpoint: 1199,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					arrows: false,
					dots: true,
				}
			},
			{
				breakpoint: 767,
				settings: {
					arrows: false,
					slidesToShow: 1.3,
					accessibility: true,
					slide: '.feature',
					slidesToScroll: 1,
					centerMode: true,
					centerPadding: '15px',
				}
			}, {
				breakpoint: 380,
				settings: {
					centerMode: false,
					arrows: false,
					slidesToShow: 1.3,
					slidesToScroll: 1,
					accessibility: true,
					slide: '.feature',

				}
			}
		]
	});
	//	if ($(window).width() < 767) {
	//		$('.pdp-info-cards').slick({
	//			slidesToShow: 1,
	//			slidesToScroll: 1,
	//			slide: '.info-card',
	//			focusOnSelect: true,
	//			infinite: false,
	//			dots:true,
	//			arrows:false,
	//			adaptiveHeight: true,
	//		});
	//	}

	//hover replace image
	$(".pdp-info-cards .info-card .image-holder img.white-icon").css("display", "none")
	$(".pdp-info-cards .info-card").hover(function () {
		$(this).find(".charcoal-icon").css("display", "none");
		$(this).find(".white-icon").css({ "display": "block", "margin": "0 auto" });
	}, function () {
		$(this).find(".charcoal-icon").css({ "display": "block", "margin": "0 auto" });
		$(this).find(".white-icon").css("display", "none");
	});
	$(".pdp-info-cards .info-card").click(function () {
		if (typeof (utag) != "undefined") {
			utag.link({eventCategory: 'Link - Product Detail', eventAction: 'Why Mattress Firm', eventLabel: $(this).find('h4').length > 0 ? $(this).find('h4').text():''});
		}	
	});
	$(".feature").hover(function () {
		$(this).find(".red-svg").css("display", "none");
		$(this).find(".white-svg").css("display", "block");
		if (typeof (utag) != "undefined") {
			utag.link({eventCategory: 'Link - Product Detail', eventAction: 'Features Spotlight - Element', eventLabel: $(this).find('h3').length > 0 ? $(this).find('h3').text():''});
		}	
	}, function () {
		$(this).find(".red-svg").css("display", "block");
		$(this).find(".white-svg").css("display", "none");
	});
	
	$(".slick-dots li").click(function() {
		if (typeof (utag) != "undefined") {
			utag.link({eventCategory: 'Link - Product Detail', eventAction: 'Features Spotlight - Bullet Direction', eventLabel: $(this).length>0 && Number($(this).text())>1 ? 'Right' :'left'});
		}
	})
	
	$(".features-wrap .slick-next.slick-arrow").click(function() {
		if (typeof (utag) != "undefined") {
			utag.link({eventCategory: 'Link - Product Detail', eventAction: 'Features Spotlight - Slider Direction', eventLabel: 'Right'});
		}
	})
	
	$(".features-wrap .slick-prev.slick-arrow").click(function() {
		if (typeof (utag) != "undefined") {
			utag.link({eventCategory: 'Link - Product Detail', eventAction: 'Features Spotlight - Slider Direction', eventLabel: 'Left'});
		}
	})
}

function initializeEvents() {
	$('.fc-products__product').on('click', '.add-to-cart', addToCart.addToCart);
	var controlKeys = ['8', '13', '46', '45', '36', '35', '38', '37', '40', '39'];
	$('body')
		.on('keydown', 'textarea[data-character-limit]', function (e) {
			var text = $.trim($(this).val()),
				charsLimit = $(this).data('character-limit'),
				charsUsed = text.length;

			if ((charsUsed >= charsLimit) && (controlKeys.indexOf(e.which.toString()) < 0)) {
				e.preventDefault();
			}
		}).on('change keyup mouseup', 'textarea[data-character-limit]', function () {
			var text = $.trim($(this).val()),
				charsLimit = $(this).data('character-limit'),
				charsUsed = text.length,
				charsRemain = charsLimit - charsUsed;

			if (charsRemain < 0) {
				$(this).val(text.slice(0, charsRemain));
				charsRemain = 0;
			}

			$(this).next('div.char-count').find('.char-remain-count').html(charsRemain);
		});

	/**
	 * initialize search suggestions, pending the value of the site preference(enhancedSearchSuggestions)
	 * this will either init the legacy(false) or the beta versions(true) of the the search suggest feature.
	 * */
	var $searchContainer = $('.header-search');

	// add support for new HP home search and menu search forms
	//if (parseInt($(window).width()) < 767) {
	//$searchContainer = $('.header-mobile-search.home, .header-search.home');
	//}
	if (SitePreferences.LISTING_SEARCHSUGGEST_LEGACY) {
		searchsuggest.init($searchContainer, Resources.SIMPLE_SEARCH);
	} else {
		searchsuggestbeta.init($searchContainer, Resources.SIMPLE_SEARCH);
	}
	$('.secondary-navigation .toggle').click(function () {
		$(this).toggleClass('expanded').next('ul').toggle();
	});
	// add generic toggle functionality
	$('.toggle').next('.toggle-content').hide();
	$('.toggle').click(function () {
		$(this).toggleClass('expanded').next('.toggle-content').toggle();
	});

	// Refinements Truncate functionality
	$(".hideRefinement").hide();
	$(".refinements-see-less").hide();
	$(".refinements-see-more").on("click", function () {
		$(this).parents('ul').find(".hideRefinement").show();
		$(this).parents('ul').find(".refinements-see-less").show();
		$(this).hide();
	});
	$(".refinements-see-less").on("click", function () {
		$(this).parents('ul').find(".hideRefinement").hide();
		$(this).parents('ul').find(".refinements-see-more").show();
		$(this).hide();

	});

	if ($('.pdp-info-cards').length > 0) {
		$(".pdp-info-cards .info-card .image-holder img.white-icon").css("display", "none");
		$(".pdp-info-cards .info-card").hover(function () {
			$(this).find(".charcoal-icon").css("display", "none");
			$(this).find(".white-icon").css({ "display": "block" });
		}, function () {
			$(this).find(".charcoal-icon").css({ "display": "block" });
			$(this).find(".white-icon").css("display", "none");
		});
	}
	var $deliveryDates = $('#delivery-dates-container');
	var deliveryConfig = function () {
		//Select Delivery Date Checking
		if ($deliveryDates.find("input[type='radio']").length > 0) {
			if (!$deliveryDates.find("input[type='radio']").is(":checked")) {
				$('form#dwfrm_singleshipping_shippingAddress').find('button').addClass('disabled');
			} else {
				if ($deliveryDates.find("input[value='scheduledelivery']").is(":checked")) {
					if ($('.delivery-dates-wrapper').find('.time-slot').hasClass('selected')) {
						$('form#dwfrm_singleshipping_shippingAddress').find('button').removeClass('disabled');
						$deliveryDates.find('.error').hide();
					} else {
						$('form#dwfrm_singleshipping_shippingAddress').find('button').addClass('disabled');
					}
				} else {
					$('form#dwfrm_singleshipping_shippingAddress').find('button').removeClass('disabled');
					$deliveryDates.find('.error').hide();
				}
			}
		}
	}
	deliveryConfig();
	$deliveryDates.find("input[type='radio']").change(function () {
		deliveryConfig();
	});
	$('.delivery-dates').on('click focusin', '.time-slot', function () {
		setTimeout(function () {
			deliveryConfig();
		}, 500);
	});
	$('form#dwfrm_singleshipping_shippingAddress').submit(function () {
		if ($(this).find('button').hasClass('disabled'))
			$deliveryDates.find('.error').show();
	});
	$('form#dwfrm_singleshipping_shippingAddress button').click(function (e) {
		if ($(this).hasClass('disabled')) {
			e.preventDefault();
			$deliveryDates.find('.error').show();
		}
	});
	// subscribe email box
	var $subscribeEmail = $('.subscribe-email');
	if ($subscribeEmail.length > 0) {
		$subscribeEmail.focus(function () {
			var val = $(this.val());
			if (val.length > 0 && val !== Resources.SUBSCRIBE_EMAIL_DEFAULT) {
				return; // do not animate when contains non-default value
			}

			$(this).animate({ color: '#999999' }, 500, 'linear', function () {
				$(this).val('').css('color', '#333333');
			});
		}).blur(function () {
			var val = $.trim($(this.val()));
			if (val.length > 0) {
				return; // do not animate when contains value
			}
			$(this).val(Resources.SUBSCRIBE_EMAIL_DEFAULT)
				.css('color', '#999999')
				.animate({ color: '#333333' }, 500, 'linear');
		});
	}
	$('#zipfield').on('focus', function () {
		$(this).select();
		this.setSelectionRange(0, 5);
	});
	$('#main').on('focus', '#pdp-delivery-zip', function () {
		$(this).select();
		this.setSelectionRange(0, 5);
		setTimeout(function () {
			$('#pdp-delivery-zip').get(0).setSelectionRange(0, 5);
		}, 1);
	});

	$('#main').on('click', '.details', function (e) {
		e.preventDefault();
		var params = {
			dClass: 'deliver-option-class',
			dUrl: $(e.target).attr('href')
		};
		dialogify(params);
	});
	$('#main').on('click', '.details_ab', function (e) {
		e.preventDefault();
		var params = {
			dClass: 'deliver-option-class-ab',
			dUrl: $(e.target).attr('href')
		};
		dialogify(params);
	});
	$('.privacylink').on('click', function (e) {
		e.preventDefault();
		var params = {
			dUrl: $(e.target).attr('href'),
			dheight: 600,
			dTitle: $(e.target).attr('title')
		};
		dialogify(params);
	});
	$('.privacy-policy').on('click', function (e) {
		e.preventDefault();
		var params = {
			dUrl: $(e.target).attr('href'),
			dheight: 620,
			dTitle: $(e.target).attr('title'),
			dClass: 'privacy-policy-class'
		};
		dialogify(params);
	});
	
	if (SessionAttributes.AMAZON_CHECKOUT == true) {
		$('.amazonphone  #dwfrm_billing_billingAddress_addressFields_phone').on('input focusout', function(e) {
			e.preventDefault();
			e.stopPropagation();
			var rgx = /^\(?([2-9][0-8][0-9])\)?[\-\. ]?([2-9][0-9]{2})[\-\. ]?([0-9]{4})(\s*x[0-9]+)?$/;
			var value= $(this).val();
			var isValid = rgx.test($.trim(value));
			
			if(value.replace(/[^\w\s]/gi, '').length == 0) {
				$('.amazon-phone').addClass("red");
				$(".amazonphone .phone").parent().find("span.error").hide();
				$(".amazon-missing-phone").show();
				$(".amazon-missing-phone").addClass("red");
			} else {
				$(".amazon-missing-phone").hide();
			}
			isValid = rgx.test($.trim(value));
			
			if (!isValid && value.replace(/[^\w\s]/gi, '').length > 0) {
				$('.amazon-phone').addClass("red");
				if ($(".amazonphone .phone").parent().find("span.error").length < 1 && $(".amazon-missing-phone").is(":visible") == false) {
					$(".amazonphone .phone").parent().append("<span class='error'>" + Resources.INVALID_PHONE + "</span>");
					$(".amazonphone .phone").addClass("error");
					$(".amazonphone .phone").parent().find("span.error").show();
				} else {
					if($(".amazon-missing-phone").is(":visible") == false) {
						$(".amazonphone .phone").parent().find("span.error").show();
					}
				}
			} else if (isValid) {
				$('.amazon-phone').removeClass("red");
				if ($(".amazonphone .phone").parent().find("span.error").length > 0) {
					$(".amazonphone .phone").parent().find("span.error").remove();
					$(".amazonphone .phone").removeClass("error");
				}
			}
			
			if(isValid) {
				$(".amazon-submit-order").removeAttr("disabled");
			} else {
				$(".amazon-submit-order").attr("disabled", "disabled");
			}
		});
		
		if($('.amazonphone').length) {
			$('.amazonphone input').inputmask("(999) 999-9999", { showMaskOnHover: false, showMaskOnFocus: false });
			$('.amazonphone  #dwfrm_billing_billingAddress_addressFields_phone').trigger('focusout');
		}
	}
	
	$('.dialogify').on('click', function (e) {
		e.preventDefault();
		var params = {
			dUrl: $(e.target).attr('href'),
			dClass: $(e.target).attr('data-class'),
			dheight: $(e.target).attr('data-height'),
			dTitle: $(e.target).attr('title')
		};
		dialogify(params);
	});
	var dialogify = function (params) {
		dialog.open({
			url: params.dUrl,
			options: {
				dialogClass: params.dClass,
				height: params.dheight,
				title: params.dTitle,
				open: function () {
					var dialog = this;
					$('.ui-widget-overlay').on('click', function () {
						$(dialog).dialog('close');
					});
					if (params.dHideTitle) {
						$(".ui-dialog-title").hide();
						$(".ui-dialog-titlebar").css('height', 'auto');
					}
					// ASST-2 PDP - Chat on availability message modal not functioning 
					$(document).on('click', '#contentChatWithUs', function (e){
						e.preventDefault();
						if ($('#helpButtonSpan').length > 0) {
							$('#helpButtonSpan').click();
						}
					});
					
					$(document).on('click', '.contentChatWithUs', function (e){
						e.preventDefault();
						if ($('#helpButtonSpan').length > 0) {
							$('#helpButtonSpan').click();
						}
					});
				}
			}
		});
	};
	var amazonTimer;
	var idleTimer = null;
	var idleState = false;
	var idleWait = 300000;

	if (SessionAttributes.AMAZON_CHECKOUT == true && Globals.isBasket == true && $('.amazon-session-expired').length > 0) {
		//reseting timer variables
		sessionStorage.removeItem("amazonSessionRemainingMins");
		sessionStorage.removeItem("amazonSessionRemainingSec");
		clearTimeout(amazonTimer);
		clearTimeout(idleTimer);
		amazonPaySessionMonitor();
	}

	function amazonPaySessionMonitor() {

		(function ($) {

			$(document).ready(function () {

				$('*').bind('mousemove keydown scroll', function () {

					clearTimeout(idleTimer);

					if (idleState == true) {

						// Reactivated event
						//console.log("Welcome Back.");
					}

					idleState = false;

					idleTimer = setTimeout(function () {

						// Idle Event
						//console.log("You've been idle for " + idleWait/1000 + " seconds.");
						var amazonDialog = require("./dialog");
						var amazonSessionExpiredHtml = $('.amazon-session-expired').html();
						$(document).on('click', '.conitnue-session', function () {
							amazonDialog.close();
							sessionStorage.removeItem("amazonSessionRemainingMins");
							sessionStorage.removeItem("amazonSessionRemainingSec");
							clearTimeout(amazonTimer);
							clearTimeout(idleTimer);
						});
						amazonDialog.open({
							html: amazonSessionExpiredHtml,
							options: {
								autoOpen: true,
								dialogClass: 'amazon-session-expired',
								close: function () {

								}
							}
						}, startAmazonTimer());
						idleState = true;
					}, idleWait);
				});

				$("body").trigger("mousemove");

			});
		})(jQuery)
		undefined
	}

	function startAmazonTimer() {
		var dt = new Date();
		dt.setMinutes(dt.getMinutes() + 3);
		var countDownDate = dt.getTime();
		if (parseInt(sessionStorage.getItem("amazonSessionRemainingMins")) <= 0 && parseInt(sessionStorage.getItem("amazonSessionRemainingSec")) <= 0) {
			//reseting timer variables
			sessionStorage.removeItem("amazonSessionRemainingMins");
			sessionStorage.removeItem("amazonSessionRemainingSec");
			//As user is on cart page thats why resetting the timer
			clearTimeout(amazonTimer);
		}

		// Update the count down every 1 second
		if (sessionStorage.getItem("amazonSessionRemainingMins") == null || sessionStorage.getItem("amazonSessionRemainingSec") == null) {
			amazonTimer = setInterval(function () {

				// Get today's date and time
				var now = new Date().getTime();

				// Find the distance between now and the count down date
				var distance = countDownDate - now;

				// Time calculations for days, hours, minutes and seconds
				//var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				//var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				var seconds = Math.floor((distance % (1000 * 60)) / 1000);
				if (minutes >= 0) {
					sessionStorage.setItem("amazonSessionRemainingMins", minutes);
				}
				if (seconds >= 0) {
					sessionStorage.setItem("amazonSessionRemainingSec", seconds);
				}
				// If the count down is over, write some text
				if (distance < 1 && $('.amazon-session-expired').length) {
					var amazonDialog = require("./dialog");
					clearInterval(amazonTimer);
					amazonDialog.close();
					if (SessionAttributes.AMAZON_CHECKOUT == true && Globals.isBasket == true && $('.amazon-session-expired').length > 0) {
						window.location.href = Urls.redirectToCart + "?amazonsessionexpired=true";
					}
				} else {
					// Output the result in an element with class="remaining-time" for amazon session to be expired
					$('.remaining-time').html((sessionStorage.getItem("amazonSessionRemainingMins") + ":" + ("0" + sessionStorage.getItem("amazonSessionRemainingSec")).slice(-2)));
				}
			}, 1000);
		}
	}


	// DROPDOWN FUNCTIONALITY
	if (parseInt($(window).width()) < 768) {
		$('.dropdown-trigger').click(function () {
			$(this).parents('ul').find('.dropdown-list').slideToggle(150);
			$(this).toggleClass('opened');
		});

		$('.header-top-nav li.header-tooltip').click(function () {
			if ($(this).find('span.arrow').length > 0) {
				$(this).find('span.arrow').remove();
			} else {
				$(this).prepend('<span class="arrow"></span>');
			}
			$(this).find('ul').slideToggle(200);
		});
	}
	// END DROPDOWN FUNCTIONALITY

	// Top Header Promo Bar


	var promoContainer = $('.promo-container');
	// if container has fading class, fade promo elements
	if (promoContainer.hasClass('fading')) {
		function initPromoBanner() {
			var items = $('.promo-container ul li');

			// basic function to set next element active, or set first element as active if next doesn't exist
			var timer = function () {
				var first = $('.promo-container ul li:first-child');
				var current = $('.promo-container ul li.active');
				var next = current.next();
				current.removeClass('active');
				if (next.length === 0) {
					first.addClass('active');
				} else {
					next.addClass('active');
				}
			}

			var setTimer = null;
			// run initially
			setTimer = setInterval(timer, 5000);

			items.hover(function () {
				// pause promobar timer
				clearInterval(setTimer);
			}, function () {
				// resume promobar timer
				setTimer = setInterval(timer, 5000);
			});
		}

		// init function on page load
		initPromoBanner();
	}
	


	// Footer chat button
	$('#contentChatWithUs').on('click', function (e) {
		//$('.header-chat a[id^="liveagent_button_online"]').trigger('click');
		/* Trigering New Chat from Footer Link*/
		e.preventDefault();
		if ($('#helpButtonSpan').length > 0) {
			$('#helpButtonSpan').click();
		}
	});

	// main menu toggle
	$('.menu-toggle').on('click', function () {
		$('#wrapper').toggleClass('menu-active');
		$('footer').toggleClass('footer-active');
	});
	$('.menu-category li .menu-item-toggle').on('click touchstart', function (e) {
		if (util.isMobileSize()) {
			var $parentLi = $(e.target).closest('li');
			e.preventDefault();
			$parentLi.find('.menu-item-toggle i').toggleClass('fa-angle-right fa-angle-down active');
			$parentLi.find('.level-2').toggle();
			if ($parentLi.hasClass('active')) {
				if ($(e.target).hasClass('arrow')) {
					$parentLi.toggleClass('active');
					return false;
				}
				window.location.href = $parentLi.find('a').attr('href');
			} else {
				$parentLi.siblings('li').removeClass('active').find('.menu-item-toggle i').removeClass('fa-angle-down active').addClass('fa-angle-right');
				$parentLi.siblings('li').find('li.active').removeClass('active').find('.menu-item-toggle i').removeClass('fa-angle-down active').addClass('fa-angle-right');
				$parentLi.toggleClass('active');
				$parentLi.siblings('li').find('.level-2').hide();
				$(e.target).find('i').toggleClass('fa-angle-right fa-angle-down active');
			}
		}
	});
	//top navigation pause
	var hoverconfig = {
		over: function () {
			$(this).find('div.level-2').css('display', 'block');
			$(this).children('a.has-sub-menu').addClass('open');
			$(this).addClass('open');
			var divwidth = $(this).find('div.level-2').width();
			var offset = $(this).find('div.level-2').offset();
			var positionleft = offset.left;
			var screenwidth = $(window).width();
			$('#grid-sort-header').blur();
			$('#grid-paging-header').blur();
			var positionright = screenwidth - (positionleft + divwidth);
			if (screenwidth > 767 && screenwidth < 1025) {
				if (positionright < 50) {
					$(this).find('div.level-2').css('right', 0);
				}
				return false;
			}
		},
		out: function () {
			$(this).find('div.level-2').css('display', 'none');
			$(this).children('a.has-sub-menu').removeClass('open');
		},
		timeout: 100,
		sensitivity: 3,
		interval: 100
	};

	var touchConfig = function () {
		$(document).off('touchend', '.menu-category > li > a');
		// fix for touch tablet devices: prevent first click
		if (util.isMobile() == true && screen.width > util.mobileWidth) {
			$(document).on('touchend', '.menu-category > li > a', function (e) {
				if ($(this).siblings('.level-2').is(':hidden') == true) {
					e.preventDefault();
					// hide all opened menus
					$('.menu-category > li').find('.level-2').css('display', 'none');
					$('.menu-category > li').children('a.has-sub-menu').removeClass('open');
					//open current submenu
					$(this).siblings('.level-2').css('display', 'block');
					$(this).addClass('open');
					var divwidth = $(this).siblings('.level-2').width();
					var offset = $(this).siblings('.level-2').offset();
					var positionleft = offset.left;
					var screenwidth = screen.width;
					var positionright = screenwidth - (positionleft + divwidth);
					if (screenwidth > 767 && screenwidth < 1025) {
						if (positionright < 50) {
							$(this).siblings('.level-2').css('right', 0);
						}
						return false;
					}
					//console.log('ddd' + positionright + 'ddd' + screenwidth);
				}
			})
		}
	};

	touchConfig();

	if (screen.width > util.mobileWidth) {
		$('.level-1 li').hoverIntent(hoverconfig);
	}
	$(window).resize(function () {
		if (screen.width > util.mobileWidth) {
			$('.level-1 li').removeClass('active').hoverIntent(hoverconfig);
		} else {
			$('.level-1 li').unbind("mouseenter").unbind("mouseleave");
			$('.level-1 li').removeProp('hoverIntentT');
			$('.level-1 li').removeProp('hoverIntentS');
		}
		setRenderedStoresSlickHeight();
		touchConfig();
	});
	$('.print-page').on('click', function () {
		window.print();
		return false;
	});
	function validatesignupEmail(email) {
		var $continue = $('.home-email');
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		if (!emailReg.test(email)) {
			//$continue.attr('disabled', 'disabled');
			if ($(".emailsignup #email-alert-address-error").length < 1) {
				if ($(".emailsignup .error2").length > 0) {
					$(".emailsignup .error2").remove();
				}
				//$(".emailsignup form").append("<span id='email-alert-address-error' class='error'>" + Resources.VALIDATE_EMAIL + "</span>");
				$(".emailsignup form").find("#email-alert-address").val("");
				$(".emailsignup form").find("#email-alert-address").attr("placeholder", Resources.VALIDATE_EMAIL);
			} else {
				if ($(".emailsignup .error2").length > 0) {
					$(".emailsignup .error2").remove();
				}
				if ($(".emailsignup #email-alert-address-error").text().length == 0) {
					$(".emailsignup #email-alert-address-error").html(Resources.VALIDATE_EMAIL);
					//$(".emailsignup form").append("<span id='email-alert-address-error' class='error2'>" + Resources.VALIDATE_EMAIL + "</span>");
					$(".emailsignup form").find("#email-alert-address").val("");
					$(".emailsignup form").find("#email-alert-address").attr("placeholder", Resources.VALIDATE_EMAIL);
				} else {
					//console.log('22' + $(".emailsignup #email-alert-address-error").length);
				}
			}
			$("#email-alert-address-error").css('display', 'block');
			return false;
		} else {
			//$continue.removeAttr('disabled');
			if ($(".emailsignup #email-alert-address-errorr").length > 0) {
				$(".emailsignup form #email-alert-address-error").remove();
			}
			if ($(".emailsignup .error2").length > 0) {
				$(".emailsignup .error2").remove();
			}
			return true;
		}
	}

	/*
	* Extend email signup validation
	*/
	$('#email-alert-signup').validate({
		focusInvalid: false,
		errorPlacement: function (error, element) {
			element.attr("placeholder", "Please enter your email address");
		}
	});

	var emailSignUpFlag = false;
	/*email signup Ajax*/
	$('#email-alert-signup').on('submit', function (e) {
		e.preventDefault();
		var $this = $(this),
			emailSignupInput = $(this).find('#email-alert-address'),
			validationErrorBlock = $('#email-alert-address-error');
		var signupInputValue = $(this).find(emailSignupInput).val();
		var validatetest = validatesignupEmail(signupInputValue);
		if (validatetest && signupInputValue.length > 1 && !emailSignUpFlag) {
			emailSignUpFlag = true;
			var _zipCode = $('#footer-social-zipCode').val();
			var _siteId = $('#footer-social-siteId').val();
			var _leadSource = $('#footer-social-leadSource').val();
			var _optOutFlag = $('#footer-social-optOutFlag').val();

			if ($.cookie) {
				var _gclid = $.cookie('_ga');
				console.log(_gclid);
			}
			var params = { emailAddress: signupInputValue, zipCode: _zipCode, leadSource: _leadSource, siteId: _siteId, optOutFlag: _optOutFlag, gclid: _gclid };

			$.ajax({
				url: Urls.sfEmailSubscription,
				data: params,
				type: 'post',
				success: function (data) {
					$this.hide().parent().append($(data));
				},
				error: function (errorThrown) {
					console.log(errorThrown);
				}
			});
		}
	});
	// Execute a function when the user releases a enter key on the keyboard
	$(document).on("keyup", '#signUpPDP', function (event) {
		// Number 13 is the "Enter" key on the keyboard
		if (event.keyCode === 13) {
			// Cancel the default action, if needed
			event.preventDefault();
			// Trigger the button element with a click
			$(this).click();
		}
	});
	//MAT-1594 BTS Promo Customer can enter email address on PDP (Promo Live 7/17/2019)
	$(document).on('click', '#signUpPDP', function (e) {
		e.preventDefault();
		var $this = $(this),
			validationErrorBlock = $('#email-alert-address-error');
		var signupInputValue = $('#pdp-email-alert-address').val();
		var validatetest = validatesignupEmail(signupInputValue);
		if (validatetest && signupInputValue.length > 1) {
			emailSignUpFlag = true;
			var _zipCode = $('#footer-social-zipCode').val();
			var _siteId = $('#footer-social-siteId').val();
			var _leadSource = $('#footer-social-leadSource-email-promo').val();
			var _optOutFlag = $('#footer-social-optOutFlag').val();
			var pid = $('#pid').val();

			if ($.cookie) {
				var _gclid = $.cookie('_ga');
				console.log(_gclid);
			}
			var params = { emailAddress: signupInputValue, zipCode: _zipCode, leadSource: _leadSource, siteId: _siteId, optOutFlag: _optOutFlag, gclid: _gclid, pid: pid };

			$.ajax({
				url: Urls.sfEmailSubscription,
				data: params,
				type: 'post',
				success: function (data) {
					if ($(data).closest('.pdperror').length > 0) {
						var pdpSignupError = $($(data).closest('.pdperror')[0]).html();
						if ($('#pdp-email-alert-address').next('span.error').length) {
							$('#pdp-email-alert-address').next().show().html(pdpSignupError)
						} else {
							$('#pdp-email-alert-address').after("<span id='pdp-email-alert-exist' class='error' style='display:block;'>" + pdpSignupError + "</span>");
						}
					} else {
						window.location.reload();
					}
				},
				error: function (errorThrown) {
					if ($('#pdp-email-alert-address').next('span.error').length) {
						$('#pdp-email-alert-address').next().show().html("Email already exist")
					} else {
						$('#pdp-email-alert-address').after("<span id='pdp-email-alert-exist' class='error' style='display:block;'>Email already exists</span>");
					}
				}
			});
		} else {
			//highlighting email input field and adding error class
			$('#pdp-email-alert-address').addClass('error');
			if ($('#pdp-email-alert-address').next('span.error').length) {
				$('#pdp-email-alert-address').next().show().html("Please enter a valid email address.")
			} else {
				$('#pdp-email-alert-address').after("<span id='pdp-email-alert-exist' class='error' style='display:block;'>Please enter a valid email address.</span>");
			}
		}
	});

	$(document).on('input propertychange', '#pdp-email-alert-address', function () {
		$('#pdp-email-alert-exist').remove();
		if (/^[\w.%+-]+@[\w.-]+\.[\w]{2,6}$/.test($('#pdp-email-alert-address').val())) {
			$('#signUpPDP').removeClass('btn-disabled');
			$('#pdp-email-alert-address').removeClass('error');
			if ($('#pdp-email-alert-address').next('span.error').length) {
				$('#pdp-email-alert-address').next().hide();
			}
		} else {
			$('#signUpPDP').addClass('btn-disabled');
		}
	});
	/* Chat link click tracking */
	$("a[id^='liveagent_button_']").each(function () {
		$(this).on('click', function (e) {
			utag.link({ event_name: "chat_link" });
		});
	});
	$('.primary-logo').focus();

	/*	Moving Logic to emailsignup.js file
		  function validateSubEmail(email) {
			var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			if (!emailReg.test(email)) {
				if(!$("#sub-email-address").hasClass('error')){
					$("#sub-email-address").addClass('error');
				}
				$("#sub-email-address").val("");
				$("#sub-email-address").attr("placeholder", Resources.VALIDATE_EMAIL);
				$("#sub-email-address-error").css('display','block');
				return false;
			} else {
				if($("#sub-email-address").hasClass('error')){
					$("#sub-email-address").removeClass('error');
				}
				return true;
			}
		}

		$('#gen-email-alert-signup').validate({
			focusInvalid: false,
			errorPlacement: function(error, element) {
				element.attr("placeholder", Resources.VALIDATE_EMAIL_REQUIRED);
			}
		});

		$(document).on('submit','#gen-email-alert-signup', function (e) {
			e.preventDefault();
			var $this = $(this),
				emailSignupInput = $('#sub-email-address'),
				validationErrorBlock = $('#sub-email-address-error');
			var signupInputValue = $(emailSignupInput).val();
			//var validatetest = validatesignupEmail(signupInputValue);
			var validatetest = validateSubEmail(signupInputValue);
			if (validatetest && signupInputValue.length > 1 ) {
				var _zipCode = $('#footer-social-zipCode').val();
				var _siteId = $('#footer-social-siteId').val();
				var _leadSource = $('#lead-source').val();
				var _optOutFlag = $('#footer-social-optOutFlag').val();
				var _signupVersion = $('#signup-version').val();
				var _successAsset = $('#success-asset').val();
				var _emailErrorAsset = $('#email-error-asset').val();

				if ($.cookie) {
					var _gclid = $.cookie('_ga');
					console.log(_gclid);
				}
				var params = {emailAddress: signupInputValue, zipCode: _zipCode, leadSource: _leadSource, siteId: _siteId, optOutFlag: _optOutFlag, gclid: _gclid, signupVersion: _signupVersion, successAsset: _successAsset, emailErrorAsset: _emailErrorAsset};

				$.ajax({
					url: Urls.sfEmailSubscription,
					data: params,
					type: 'post',
					success: function (data) {
						$this.hide().parent().append($(data));
					},
					error: function (errorThrown) {
						console.log(errorThrown);
					}
				});
			}
		});

		*/

	/**
	 *  Buy online pick up in store (BOPIS)  and Try in Store(TIS) functionality
	 */

	// BOPIS / Try In Store init zip form
	/* $('.delivery-options-container').on('click', '#instorepickup', function (e) {
		e.preventDefault();
		if ($(this).closest('label').children(".details-container").length == 0) {
			$('.delivery-options-container .store-picker > a').click();
		}
	}); */
	function initStorePicker(e) {
		// tealium.trigger("Try-in-store", "Initiate try in store", "", $(e.toElement).attr('data-tealium'));
		var customclasses = SitePreferences.CURRENT_SITE === 'Mattress-Firm' && typeof window.pageContext !== 'undefined' && window.pageContext.ns === 'finder' || 'mattressmatcherresults' ? 'finder-try-in-store' : '';
		dialog.open({
			url: $(e.target).attr('href'),
			options: {
				dialogClass: customclasses,
				open: function () {
					var $storePickerForm = $('#dialog-container').find('form');
					var $spSubmit = $($storePickerForm.find('[name="submit"]'));
					var $spZipCodeInput = $($storePickerForm.find('[name="dwfrm_storepicker_postalCode"]'));
					$spZipCodeInput.select();
					$($spSubmit).on('click', function (e) {
						e.preventDefault();
						progress.show($('#dialog-container'));
						getStores($storePickerForm);
					});
				},
				width: '820',
				title: $(e.target).attr('title'),
				draggable: false
			}
		});
	}
	// Triggers
	$('#main').on('click', '.store-picker a:not(".get-directions")', function (e) {
		e.preventDefault();
		initStorePicker(e);
	});
	if ($(window).width() < 768) {
		$('.featured-product-v3 .store-picker a:not(".get-directions")').off().on('click', function () {
			setTimeout(function () {
				$(".finder-try-in-store").addClass("slideup");
			}, 2000);
		});
		$(document).on('click', '.finder-try-in-store .ui-icon-closethick', function () {
			$(".finder-try-in-store").removeClass("slideup");
		});
		$(document).on('click', '.finder-try-in-store .try-in-store-list li button', function () {
			setTimeout(function () {
				$(".finder-try-in-store").addClass("dynamicheight");
			}, 3000);
		});
		$(document).on('click', '.finder-try-in-store #change-location', function () {
			$(".finder-try-in-store").removeClass("dynamicheight");
		});
	}
	// Helper functions
	function setPreferredStore(storeId) {
		User.storeId = storeId;
		$.ajax({
			url: Urls.setPreferredStore2,
			type: 'POST',
			data: { storeId: storeId }
		});
	}

	// Helper functions
	function setPreferredStoreAB(storeId) {
		User.storeId = storeId;
		$.ajax({
			url: Urls.setPreferredStoreAB,
			type: 'POST',
			data: { storeId: storeId },
			success: function (html) {
				window.location.reload();
			}
		});
	}

	function getStores(storePickerForm) {
		// tealium.trigger("Try-in-store", "Enter zip code", "", storePickerForm.find('input[id*=postalCode]').val());
		$.ajax({
			type: 'POST',
			url: storePickerForm.attr("action"),
			data: storePickerForm.serialize(),
			dataType: 'html',
			success: function (html) {
				renderStores(html);
			}.bind(this),
			failure: function () {
				window.alert(Resources.SERVER_ERROR);
			}
		});
	}
	function renderStores(html) {
		// replace dialog with updated content
		$('#dialog-container').html(html);
		// init all our variables after content is replaced
		var $dialog = $('#dialog-container');
		var $changeLink = $dialog.find('#change-location');
		var $storeZipForm = $dialog.find('form#StorePickerZip');
		var $preferredStoreForm = $dialog.find('#SetPreferredStore');
		var $psSubmit = $preferredStoreForm.find('button[type="submit"]');
		var $spSubmit = $storeZipForm.find('[name="submit"]');
		var $nextSlick = $dialog.find('.slick-next');

		$storeZipForm.hide();
		// tealium.init();
		// disable selectability of unavailable stores
		$('.store-result.unavailable').find('input[type="radio"]').on('click', function () {
			return false;
		});
		// if no preferred store is picked, disable the continue button
		if ($preferredStoreForm.find('.preferred-store').length == 0) {
			$psSubmit.attr('disabled', true);
		}
		// slick slider for new mattressfinder result page


		if ($('.finder-try-in-store').length > 0) {
			$('.finder-try-in-store ul.try-in-store-storelist').slick({
				infinite: false,
				slidesToScroll: 1,
				slidesToShow: 2.5,
				accessibility: true,
				draggable: true,
				arrows: false,
				responsive: [{
					breakpoint: 1024,
					settings: {
						slidesToShow: 2.5,
						slidesToScroll: 1,
						infinite: false,
						accessibility: true,
						draggable: true,
						arrows: false
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 1.5,
						slidesToScroll: 1,
						infinite: false,
						accessibility: true,
						draggable: true,
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 1.5,
						slidesToScroll: 1,
						infinite: false,
						accessibility: true,
						draggable: true,
					}
				},
				{
					breakpoint: 320,
					settings: {
						slidesToShow: 1.5,
						slidesToScroll: 1,
						infinite: false,
						accessibility: true,
						draggable: true,

					}
				}]
			});
		}

		$('ul.try-in-store-storelist').slick({
			infinite: false,
			slidesToScroll: 1,
			slidesToShow: 4,
			accessibility: false,
			draggable: false,
			responsive: [{
				breakpoint: 1024,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 1,
					infinite: false,
					accessibility: false,
					draggable: false,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 1,
					infinite: false,
					accessibility: false,
					draggable: false,
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: false,
					accessibility: false,
					draggable: false,
				}
			},
			{
				breakpoint: 320,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: false,
					accessibility: false,
					draggable: false,

				}
			}]
		});

		// return to zip form when change location link is clicked
		$changeLink.on('click', function (e) {
			e.preventDefault();
			$preferredStoreForm.hide();
			$storeZipForm.show();
			$("form#StorePickerZip :input:visible:enabled:first").focus();
			$("form#StorePickerZip :input:visible:enabled:first").select();
			$($spSubmit).on('click', function (e) {
				e.preventDefault();
				progress.show($dialog);
				getStores($storeZipForm);
			});
		});
		var requestAgain = false;
		$('ul.try-in-store-storelist').on('click', '.slick-next', function (e) {
			requestAgain = true;
			if (requestAgain === true) {
				var stores = $('#total-stores').val();
				var slickLength = $('ul.try-in-store-storelist').find('.slick-slide').length;
				if (slickLength < stores) {
					var storesArr = [];
					$('ul.try-in-store-storelist label').each(function () {
						var $li = $(this);
						storesArr.push($li.attr("for"));
					});
					var params = {
						requestAgain: requestAgain
					};
					var jsonObj = new Object();
					jsonObj.stores = storesArr;
					$.ajax({
						url: util.appendParamsToUrl(Urls.getMoreStoresForBopis, params),
						type: 'POST',
						data: JSON.stringify(jsonObj),
						success: function (response) {
							$(response).find('ul.try-in-store-storelist li').each(function () {
								$('ul.try-in-store-storelist').slick('slickAdd', $(this))
							});
							setRenderedStoresSlickHeight();
						}.bind(this),
						async: false
					});
				}
			}
		});

		// update classes for styling when stores are selected
		$($preferredStoreForm.find('.select-preferred-store input')).change(function () {
			// tealium.trigger("Try-in-store", "Select Preferred Store", "", $(this).attr('data-tealium'));
			var $buttonContainer = $(this).closest('div');
			var $buttonText = $buttonContainer.find('.select-preferred-store-text');
			var $storeContainer = $(this).closest('li');
			setPreferredStore(this.value);
			// reset other store containers
			$preferredStoreForm.find('.select-preferred-store-text').text(Resources.SELECT_STORE);
			$preferredStoreForm.find('li').removeClass('preferred-store');
			// update the current
			$buttonText.text(Resources.PREFERRED_STORE);
			$storeContainer.addClass('preferred-store');
			$psSubmit.removeAttr('disabled');
		});

		// update classes for styling when stores are selected
		$($preferredStoreForm.find('.select-preferred-store-ab input')).change(function () {
			// tealium.trigger("Try-in-store", "Select Preferred Store", "", $(this).attr('data-tealium'));
			var $buttonContainer = $(this).closest('div');
			var $buttonText = $buttonContainer.find('.select-preferred-store-text');
			var $storeContainer = $(this).closest('li');

			// reset other store containers
			$preferredStoreForm.find('.select-preferred-store-text').text(Resources.SELECT_STORE);
			$preferredStoreForm.find('li').removeClass('preferred-store');
			// update the current
			$buttonText.text(Resources.PREFERRED_STORE);
			$storeContainer.addClass('preferred-store');
			$psSubmit.removeAttr('disabled');
			setPreferredStoreAB(this.value);
		});

		$($psSubmit).on('click', function (e) {
			var action = $(this).attr("name");
			e.preventDefault();
			$.ajax({
				type: 'POST',
				url: $preferredStoreForm.attr("action"),
				data: $preferredStoreForm.serialize(),
				dataType: 'html',
				success: function (html) {
					var selectedStore = $('#dialog-container').find('.preferred-store input').val();
					setPreferredStore(selectedStore);
					/*SHOP-2824 Removed that AJAX Call to avoid ATP call for Try in Store
					$.ajax({
						url: Urls.getAvailablePickupStore,
						type: 'GET'
					});*/
					dialog.close();
					if ($('#data-variation-url') && $('#data-variation-url').length) {
						window.location.reload();//window.location = $('#data-variation-url').html();
					} else {
						window.location.reload();
					}
				}.bind(this),
				failure: function () {
					window.alert(Resources.SERVER_ERROR);
				}
			});
		});
		setRenderedStoresSlickHeight();
	}

	// This function is for the event handling on the cart page for when user swaps between shipping and bopis
	$('.delivery-options-container').find('.input-radio').on("change", function (e) {
		var deliveryoption = $(this).val();
		var storeId = $(this).attr('data-store-id');
		var selectedStoreDiv = $(this).closest('label').children(".details-container").length;
		$.ajax({
			url: Urls.changeDeliveryOption,
			type: 'POST',
			data: { deliveryoption: deliveryoption, storeId: storeId },
			success: function (data) {
				if (deliveryoption == 'shipped') {
					$('.storeaddress').addClass('hide');
				} else if ((deliveryoption == 'instorepickup') && selectedStoreDiv == 0) {
					$('.store-picker a').click();
				}
			},
			async: false
		});

	});
	$("button[name='dwfrm_login_register']").on("click", function () {
		window.location.href = Urls.accountRegister;
	});
}
/**
 * @private
 * @function
 * @description Adds class ('js') to html for css targeting and loads js specific styles.
 */
function initializeDom() {
	// add class to html for css targeting
	$('html').addClass('js');
	if (SitePreferences.LISTING_INFINITE_SCROLL) {
		$('html').addClass('infinite-scroll');
	}
	// load js specific styles
	util.limitCharacters();
}

$("#wrapper").click(function (event) {
	if (!$(event.target).closest('#mini-cart-ATC .justaddedproduct-wrapper').length && !$(event.target).closest('.embeddedServiceInvitation').length) {
		$('html').removeClass('mini-cart-active');
		$('body').removeClass('mini-cart-active');
		$('html').removeClass('scroll-overflow');
		$('body').removeClass('scroll-overflow');
	}
});

/*Add accessory cas*/

$(document).on("click", ".add-accessory-case .heading-byb", function (e) {
	e.preventDefault();
	$('.build-your-bed .clearance-ad').slideUp("slow");
	$('.build-your-bed #accordion').slideDown("slow");
	$('.build-your-bed .easy-steps').slideUp("slow");
	$('.build-your-bed.add-accessory-case').addClass('accessory-case-open');
});

$(document).on("click", ".add-accessory-case.accessory-case-open .heading-byb", function (e) {
	e.preventDefault();
	$('.build-your-bed .clearance-ad').slideDown("slow");
	$('.build-your-bed #accordion').slideUp("slow");
	$('.build-your-bed .easy-steps').slideDown("slow");
	$('.build-your-bed.add-accessory-case').removeClass('accessory-case-open');
});

/**
 * @private
 * @function
 * @description Prevents opening chat links in new tab.
 */
function removeAnchorTarget() {
	MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

	var observer = new MutationObserver(function (mutations, observer) {
		var searchString = Urls.HTTPHOST;

		var links = $(".sidebarBody").find('a');

		for (var i = 0; i < links.length; i++) {

			var thisLink = links[i];

			if (thisLink.href.toLowerCase().indexOf(searchString) > -1) {
				if (thisLink.hasAttribute("target")) {
					thisLink.removeAttribute("target");
				}

			}

		}
	});

	observer.observe(document, {
		subtree: true,
		attributes: true
	});
}

function openHawaiiPopup() {
	var hawaiDialog = require("./dialog");
	var hawaiiPopupHtml = $('.hawaii_popup_desktop').html();
	if (hawaiiPopupHtml) {
		if (hawaiiPopupHtml.length > 0) {
			$(document).on('click', '.hawaii-intercept-close', function () {
				$('.hawaii-popup-dialog').hide();
				$('.ui-widget-overlay').hide();
				hawaiDialog.close();
				sfemaildialog.init(10);
			});
			hawaiDialog.open({
				html: hawaiiPopupHtml,
				options: {
					autoOpen: true,
					dialogClass: 'hawaii-popup-dialog',
					close: function () {

					}
				}
			});
		}
	}
}
function headerCountdownPadding() {
	if ($('.header-countdown').length == 0) {
		$('#main').css('padding-top', '152px');
		$('.pt_checkout #main').css('padding-top', '0px');
		$('.pt_cart #main').css('padding-top', '0px');
		$('.pt_order-confirmation #main').css('padding-top', '152px');
	} else {
		if (typeof window.pageContext !== undefined && window.pageContext && window.pageContext.ns == 'product') {
			$('#main').css('padding-top', '152px');
		}
	}
}


/* Moving Logic to geolocation.js file
function updateCustomerZipCode (url,data) {
	$.ajax({
		url: url,
		type: 'get',
		data: data,
		success: function (data) {
		   if(data){
			   window.location.reload();
		   }
		},
		error: function (error) {
			var msg = error;
			//initDeliveryLocationEvents();
		}
	});
}

function initDeliveryLocationEvents() {
	if(SitePreferences.isGeoLocationSearchEnabled) {
		//get brand content asset delivery
		$.ajax({
			url: Urls.getDeliveryLocationShopByBrand,
			type: 'get',
			success: function (data) {
			   if(data){
				   $('.geo-content-selector').html(data);
			   }
			},
			error: function (error) {
				var msg = error;
			}
	     });

		//get home content asset delivery
		$.ajax({
			url: Urls.getDeliveryLocationHome,
			type: 'get',
			success: function (data) {
			   if(data){
				   $('.geo-home-selector').html(data);
			   }
			},
			error: function (error) {
				var msg = error;
			}
	     });

		$.ajax({
			url: Urls.GetDeliveryLocationMarketLanding,
			type: 'get',
			success: function (data) {
			   if(data){
				   $('#geo-mark-selector').html(data);
			   }
			},
			error: function (error) {
				var msg = error;
			}
	     });
	}else{
		$('.geo-content-selector').css('display','none');
	}

	//brand content asset zip change
	$(document).on('click','.btn-change-zip-asset', function (e) {
		e.preventDefault();
		$(".delivery-shipping-asset").hide();
		$(".previous-location-asset").css('display','flex');
	});
	// zipcode selection on focus
	$(document).on('focus','#home-customer-zip', function (e) {
		$(this).select();
		this.setSelectionRange(0,5);
	});
	$(document).on('focus','#store-customer-zip', function (e) {
		$(this).select();
		this.setSelectionRange(0,5);
	});
	$("#store-customer-zip").on('keydown', function (e) {
	   e.stopPropagation();
	   if (e.which === 13) {
			e.preventDefault();
			$(".btn-update-store").click();
		}
	});
	$(document).on('focus','#store-customer-zip-mobile', function (e) {
		$(this).select();
		this.setSelectionRange(0,5);
		$(window).scrollTop(0);
	});
	$(document).on('focus','#header-customer-zip-delivery', function (e) {
		$(this).select();
		this.setSelectionRange(0,5);
	});
	$(document).on('focus','#asset-customer-zip', function (e) {
		$(this).select();
		this.setSelectionRange(0,5);
	});
	$(document).on('focus','#plp-customer-zip', function (e) {
		$(this).select();
		this.setSelectionRange(0,5);
	});
	$(document).on('focus','#header-customer-zip-delivery-mobile', function (e) {
		$(this).select();
		this.setSelectionRange(0,5);
	});
	$(document).on('focus','#plp-customer-zip-mobile', function (e) {
		$(this).select();
		this.setSelectionRange(0,5);
	});
	//close buttons on tooltips
	$(document).on('click','.geo-close-btn', function (e) {
		e.preventDefault();
		$('.store-details.header-tooltip ul').hide();
	});
	$(document).on('touchstart','.geo-close-btn', function (e) {
		e.preventDefault();
		$('.store-details.header-tooltip ul').hide();
	});

	$(document).on('click','.geo-shipping-close-btn', function () {
		$('.shipping-details.header-tooltip ul').hide();
	});
	$(document).on('touchstart','.geo-shipping-close-btn', function (e) {
		e.preventDefault();
		$('.shipping-details.header-tooltip ul').hide();
	});
	$(document).on('click','.store-details .btn.primary', function () {
		$('.store-details.header-tooltip ul').show();
	});
	$(document).on('click','.shipping-details .btn.primary', function () {
		$('.header-tooltip ul').show();
	});

	$(document).on('touchstart','.mobile-find-store .store-details .btn', function (e) {
		e.preventDefault();
		if(utag) {
			utag.link({
				"eventCategory" : ["Mobile Find a Store"],
				"eventLabel" : ["Mobile Find a Store Clicked"],
				"eventAction" : ["click"]
			});
		}
	});

	//image hover
	$( ".deliver-ship" ).hover(function() {
	  $( ".deliver-ship__icon-active" ).css("display" , "none");
	  $( ".deliver-ship__icon" ).css("display" , "block");
	}, function(){
		$( ".deliver-ship__icon-active" ).css("display" , "block");
		$( ".deliver-ship__icon" ).css("display" , "none");
	});

	$( ".find-store" ).hover(function() {
		  $( ".find-store__icon" ).css("display" , "none");
		  $( ".find-store__icon-active" ).css("display" , "block");
	}, function(){
		$( ".find-store__icon" ).css("display" , "block");
		$( ".find-store__icon-active" ).css("display" , "none");
		});
	//home content asset zip change
	$(document).on('click','.btn-change-zip-home', function (e) {
		e.preventDefault();
		$(".delivery-shipping-home").hide();
		$(".previous-location-home").show();
	});

	//zip change pdp
	$(document).on('click','.btn-change-zip-pdp', function (e) {
		e.preventDefault();
		$(".delivery-shipping-pdp").hide();
		$(".previous-location-pdp").show();
		$(".in-stock-msg").hide();
		$("#pdp-customer-zip").select();
		$(".details-link").hide();
		$(".availability-web").hide();
	});
	//try in store pdp
	$(document).on('click','.storename-wrap', function (e) {
		e.preventDefault();
		$(".store-info").toggle();
		$(".change-tryinstore").toggle();
		$("a.regular").toggle();
		$("i.fa-angle-up").toggleClass('flip');
	});

	//brand content asset zip update
	$(document).on('click','.btn-update-asset', function (e) {
		e.preventDefault();
		var zipCodeVal = $('#asset-customer-zip').val();
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) {
			var url = Urls.setCustomerZipCode;
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location-asset .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location-asset .btn-update-asset");
	        }
		}
	});

	//home content asset zip update
	$(document).on('click','.btn-update-home', function (e) {
		e.preventDefault();
		var zipCodeVal = $('#home-customer-zip').val();
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) {
			var url = Urls.setCustomerZipCode;
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location-home .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location-home .btn-update-home");
	        }
		}
	});

	//pdp zip update
	$(document).on('click','.btn-update-pdp', function (e) {
		e.preventDefault();
		var zipCodeVal = $('#pdp-customer-zip').val();
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) {
			var url = Urls.setCustomerZipCode;
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location-pdp .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location-pdp .btn-update-pdp");
	        }
		}
	});

	//plp desktop zip update
	$(document).on('click','.btn-update-plp', function (e) {
		e.preventDefault();
		var zipCodeVal = $('#plp-customer-zip').val();
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) {
			var url = Urls.setCustomerZipCode;
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location .btn-update-plp");
	        }
		}
	});

	//plp desktop zip change
	$(document).on('click','.btn-change-zip-plp', function (e) {
		e.preventDefault();
		$(".delivery-shipping").hide();
		$(".geo-main-wrapper.desktop-plp").hide();
		$(".previous-location").show(100);
	});

	//plp mobile zip update
	$(document).on('click','.btn-update-plp-mobile', function (e) {
		e.preventDefault();
		var zipCodeVal = $('#plp-customer-zip-mobile').val();
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) {
			var url = Urls.setCustomerZipCode;
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location .btn-update-plp-mobile");
	        }
		}
	});

	//plp mobile zip change
	$(document).on('click','.btn-change-zip-plp-mobile', function (e) {
		e.preventDefault();
		$(".delivery-shipping-mobile").hide();
		$(".geo-main-wrapper.desktop-plp").hide();
		$(".previous-location-mobile").show(100);
	});

	//header store zip change
	$(document).on('click','.btn-change-zip-header-store', function (e) {
		e.preventDefault();
		$(".store-delivery-shipping").hide();
		$(".store-previous-location").show(100);
	});

	//header store desktop zip update
	$(document).on('click','.btn-update-store', function (e) {
		e.preventDefault();
		var zipCodeVal = $('#store-customer-zip').val();
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) {
			var url = Urls.setCustomerZipCode;
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location .btn-update-store");
	        }
		}
	});
	//MAT-1770 - Marketing page - Slide up and down Zip code change portion
	$(document).on('click','.change-location', function (e) {
		$('.location-address-text').slideUp(100);
		$('.location-form-wrap').slideDown(500);
	});

	$(document).on('click','.location-button', function (e) {
		e.preventDefault();
		var zipCodeVal = $('#mark-customer-zip').val();
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) {
			var url = Urls.setCustomerZipCode;
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
			$('.location-form-wrap').slideUp(100);
			$('.location-address-text').slideDown(500);
		}else{
			//display error for zip
	    	if ($(".location-form-wrap .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".location-form-wrap .location-button");
	        }
		}
	});

	//header store mobile zip change
	$(document).on('click','.btn-change-zip-header-store-mobile', function (e) {
		e.preventDefault();
		$(".store-mobile-delivery-shipping").hide();
		$(".store-mobile-previous-location").show(100);
	});

	//header store mobile zip update
	$(document).on('click','.btn-update-store-mobile', function (e) {
		e.preventDefault();
		var zipCodeVal = $('#store-customer-zip-mobile').val();
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) {
			var url = Urls.setCustomerZipCode;
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".store-mobile-previous-location .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".store-mobile-previous-location .btn-update-store-mobile");
	        }
		}
	});

	//header delivery desktop zip update
	$(document).on('click','.btn-update-header', function (e) {
		e.preventDefault();
		var zipCodeVal = $('#header-customer-zip-delivery').val();
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) {
			var url = Urls.setCustomerZipCode;
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location .btn-update-header");
	        }
		}

	});

	//header delivery mobile zip update
	$(document).on('click','.btn-update-header-mobile', function (e) {
		e.preventDefault();
		var zipCodeVal = $('#header-customer-zip-delivery-mobile').val();
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) {
			var url = Urls.setCustomerZipCode;
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location .btn-update-header-mobile");
	        }
		}

	});

	//user current location
	$(document).on('click touchstart','.current-location-selector', function (e) {
        e.preventDefault();
        var zipCodeVal = userCurrentGeoLocation.init();
    });

	//cookies locations
	$(document).on('click','.last-visited-zip-selector', function (e) {
		e.preventDefault();
		var zipCodeVal = $(this).attr('data-zipcode');
		var url = Urls.setCustomerZipCode;
		var data = {zipCode: zipCodeVal};
		updateCustomerZipCode(url,data);

	});
}
*/

var pages = {
	account: require('./pages/account'),
	cart: require('./pages/cart'),
	checkout: require('./pages/checkout'),
	compare: require('./pages/compare'),
	product: require('./pages/product'),
	registry: require('./pages/registry'),
	search: require('./pages/search'),
	storefront: require('./pages/storefront'),
	wishlist: require('./pages/wishlist'),
	storelocator: require('./pages/storelocator'),
	finder_v3: require('./finder_v3'),
	finder: require('./finder'),
	mattressmatcherresults: require('./mattressmatcherresults'),
	makepayment: require('./pages/makepayment'),
};

var app = {
	init: function () {
		if (document.cookie.length === 0) {
			$('<div/>').addClass('browser-compatibility-alert').append($('<p/>').addClass('browser-error').html(Resources.COOKIES_DISABLED)).appendTo('#browser-check');
		}
		if (SitePreferences.isBrowserGeoEnabled) {
			browserGeoLocation.init();
		}
		hoverIntent.init();
		initializeDom();
		initializeEvents();
		brandlandingcontent.init();
		//initDeliveryLocationEvents();

		// init specific global components
		countries.init();
		tooltip.init();
		scrollTop.init();
		minicart.init();
		validator.init();
		rating.init();
		searchplaceholder.init();

		// Do not run on search page
		if (window.pageContext && window.pageContext.type && window.pageContext.type !== 'search') {
			util.uniform();
		}

		content.init();
		amplience.initZoomViewer();
		if ($('.pdp-main-ab').length) {
			spotlightFeature();
			dimensionmodule.init();
						
		}
		zipzone.init();
		//sfemaildialog.init();
		mobilegeolocator.initModal();
		exacttargetnew.init();
		
		// execute page specific initializations
		$.extend(page, window.pageContext);
		var ns = page.ns;
		if (ns && pages[ns] && pages[ns].init) {
			pages[ns].init();
		}
		// Initialize the Google Analytics library
		if (typeof window.DW.googleAnalytics === 'object') {
			googleAnalytics.init(ns, window.DW.googleAnalytics.config);
		}
		responsiveSlots.init(ns || '');

		//Open Chat Widet Upon clicking header and footer links
		$("#contentChatWithUsNew, .contentChatWithUs").click(function (e) {
			e.preventDefault();
			if ($('#helpButtonSpan').length > 0) {
				$('#helpButtonSpan').click();
			}
		});
		if ($('.checkout-mini-cart-wrapper').length > 0) {
			//$('.checkout-mini-cart-wrapper').mCustomScrollbar();
		}
		//Prevents opening chat links in new window.
		removeAnchorTarget();
		if (SitePreferences.isGoogleAddressLookUpEnabled && window.pageContext && window.pageContext.ns && window.pageContext.ns == 'checkout') {
			addresslookup.initialize();
		}

		//open hawaii popup
		/* 
		 * SHOP-2635 [Hawaii] Do Not Cache Customer Group
		 * Globals.isHawaiCustomer in Footer is being cached on homePage, so will use User.isHawaiiCustomerGroup
		 * on homePage to show Hawaii Popup that is coming from remote include cache will not effect it.
		 */
		var isHomePage = (typeof window.pageContext !== 'undefined' && window.pageContext.ns === 'storefront');
		var isHawaiCustomer = ((Globals.isHawaiCustomer && !isHomePage) || (isHomePage && User.isHawaiiCustomerGroup));
		if (SitePreferences.enableHawaiiModal && isHawaiCustomer) {
			openHawaiiPopup();
		}
		else {
			sfemaildialog.init();
		}
		emailsignup.init();
		geolocation.init();
		if (SessionAttributes.AMAZON_CHECKOUT && $('#orderconfirmationpage').length && $('#orderconfirmationpage').val() == "true") {
			realTimeOrderCreation();
		}
		headerCountdownPadding();
		
		if($('.isDeliveryTrackingPage').length > 0 && $('.isDeliveryTrackingPage').val() == "true") {
			deliverytracking.init();
		}
	}
};

// general extension functions
(function () {
	String.format = function () {
		var s = arguments[0];
		var i, len = arguments.length - 1;
		for (i = 0; i < len; i++) {
			var reg = new RegExp('\\{' + i + '\\}', 'gm');
			s = s.replace(reg, arguments[i + 1]);
		}
		return s;
	};
})();
function realTimeOrderCreation() {
	//Order number will be fetched from session.custom.amazonOrderNumber
	$.ajax({
		url: Urls.realTimeOrderCreationInAX,
		type: 'get',
		success: function (response) {
			console.log("Real time Success");
		},
		failure: function () {
			console.log("Real time failed");
		}
	});
}
// initialize app
$(document).ready(function () {
	app.init();
	if ($('.pt_cart').length > 0 || $('.pt_checkout').length > 0) {
		$('.scrollToTop').remove();
	}
	$('.primary-logo').focus();
	$(document).on('click', '.ui-widget-overlay', function () {
		$('.ui-dialog').hide();
		$('.ui-widget-overlay').hide();
	});
	checkoutFormIds();
	$("#add-to-cart").removeClass('disabled-btn');
	$("#checkout-button-ID").removeClass('disabled-btn');
	$("#checkout-button-ID-ab").removeClass('disabled-btn');
	$("#shippingMethodContinueCheckout").removeClass('disabled-btn');
	/*if($("#sub-email-address").hasClass('error')){
		$("#sub-email-address").removeClass('error');
	}*/
});

//This is done for IPhone devices, iphone11 and Macbook doesn't load JavaScript on cart and Checkout page when back button clicked because page loads from cache
window.onpageshow = function(event) {
	//if page is loaded from cache then forcefully enable buttons
	if (event.persisted && performance.navigation.type == 2 && (window.pageContext.ns == "cart"  || window.pageContext.ns == "checkout" )) {
		location.reload(true);
	}
};

function checkoutFormIds() {
	$(document).on('keyup', '#dwfrm_billing_billingAddress_addressFields_city,#dwfrm_billing_billingAddress_addressFields_address2,#dwfrm_billing_billingAddress_addressFields_address1,#dwfrm_billing_billingAddress_addressFields_lastName,#dwfrm_billing_billingAddress_addressFields_firstName,#dwfrm_singleshipping_shippingAddress_addressFields_address2,#dwfrm_singleshipping_shippingAddress_addressFields_city,#dwfrm_singleshipping_shippingAddress_addressFields_address1,#dwfrm_singleshipping_shippingAddress_email_emailAddress', function () {
		var variableName = $(this).attr('id');
		var shortVariableName = variableName.split('_');
		var type = shortVariableName[1];
		shortVariableName = shortVariableName[shortVariableName.length - 1];
		validateSting("#" + variableName, shortVariableName, type);
	})


}
function validateSting(val, name, type) {
	if (validateByRegex($(val).val())) {
		if ($('val,.reg-error-' + type + name).length > 0 || $(val).val() == "") {
			$(val).parent().find('span').remove();
		}

	} else {
		if ($(val).val() != "" && $('val,.reg-error-' + type + name).length < 1) {
			$(val).parent().append("<span class='error count-reg reg-error-" + type + name + "'>" + 'Please remove invalid characters' + "</span>");
		}
		if ($(val).val() == "") {
			$(val).parent().find('span').remove();
		}
	}

}
function validateByRegex(val) {
	var re = /^[\w\. \[\]!&()+,\-?=%@_{}#_{},#$ÀÁÂÄàáâäÈÉÊèéêëÌÍÎìíîïÒÓÔÖòóôöÙÚÛÜùúûüÇçÑñ¿¡]+$/;
	return re.test(val);
}

if ($(".product-financing-custom").length == 2) {
	$(".product-financing-custom.no-credit").addClass('allignTop');
}
//Add Promo Code on Order Summary
$(document).on('click', 'button#add-promo-code-label-id', function (e) {
	e.preventDefault();
	$('#add-promo-code-label-id').addClass('visually-hidden');
	$('#add-promo-code-input-id').removeClass('visually-hidden');
	$("#dwfrm_cart_couponCode").focus();
});
if (screen.width < 767 && $(".new-cart-mob .item-total-price-mobile-show .price-standard").length > 0) {
	$(".new-cart-mob .item-total-price-mobile-show .price-standard").parents(".cart-row").find($(".item-price .price-promotion .price-standard")).css("margin-top" , "9px"); 
}
var foundationSelector = $(".recycling-fee.mob-recycle").prev(".price-option.mob-price-options");
if (screen.width < 767) {
	if(foundationSelector.length > 0) {
		foundationSelector.parents(".item-quantity").find($(".recycling-fee.mob-recycle")).css("margin-top" , "0px");
	}
}
if (screen.width < 767 && $(".promo-code-label-wrapper .cancel").length > 0) {
	$(document).on('click', '.promo-code-label-wrapper .cancel', function (e) {
		e.preventDefault();
		$('#add-promo-code-label-id').removeClass('visually-hidden');
		$('#add-promo-code-input-id').addClass('visually-hidden');
		if (screen.width < 767 && $(".cart-coupon-code .error").length > 0) {
			$("div.error").css("display" , "none");
			$("input").removeClass("error");
			$(".promo-code-label-wrapper label").css("color" , "#2d2926");
		}
	});
}
if (screen.width > 767) {
	if ($('.header-top-new div').length > 0) {
		$('.pt_cart #main').css('padding-top', '60px');
		$('.pt_cart #main .cart-empty').css('padding-top', '60px');
	}
	if ($('.header-top-new div').length > 0 && $('.header-countdown').length == 0) {
		$('.pt_product-search-result #main').css('padding-top', '214px');
	}
}
$(document).on('click', '#header-mobile-toggle', function (e) {
	if ($(this).hasClass('is-open')) {
		$('header.header-main').css('position', 'initial');
	} else {
		$('header.header-main').css('position', 'fixed');
	}
});

$(window).scroll(function () {
	var currentScroll = $(document).scrollTop(); // get current position
	if (currentScroll > 100) {
		$('.checkout-mini-cart-wrapper').addClass("order-summary-fixed");
	} else {
		$('.checkout-mini-cart-wrapper').removeClass("order-summary-fixed");
	}
});
$(".geolocation-dialog > .close").click(function () {
	$(".geolocation-dialog").css("display", "none");
	$('header.header-main').css('top', '0px');
});
var windowOffset = $(document).scrollTop();
if ($('.geolocation-dialog').css('display') != 'none' && $('.geolocation-dialog').length > 0) {
	$('header.header-main').css('top', '190px');
}
if ($('.header-top-new div').length == 0) {
	$('.header-top-promo').css('top', '152px');
}

$('#checkout-button-ID').on('click tap', function (e) {
	utag.link({eventCategory: 'Cart CTA - old',eventLabel: 'Checkout CTA', eventAction: 'Cart' });
	$(this).addClass('disabled-btn');
});
$('#checkout-button-ID-ab').on('click tap', function (e) {
	$(this).addClass('disabled-btn');
});

$('#shippingMethodContinueCheckout').on('click tap', function (e) {
	$(this).addClass('disabled-btn');
});

$('button[name=dwfrm_cart_checkoutCartRedesign]').on('click tap', function (e) {
    $(this).addClass('disabled-btn');
    utag.link({eventCategory: 'Cart CTA',eventLabel: 'Checkout CTA', eventAction: 'Cart' });
});

$(document).on('click', '.amazon-btn-right', function () {
	if ($('.new-cart-mob').length > 0) {
		utag.link({ eventCategory: 'Cart CTA', eventLabel: 'Amazon Pay CTA', eventAction: 'Cart' });
	} else if ($('.isMobileDevice').length > 0 && $('.isMobileDevice').val() == 'true') {
		utag.link({ eventCategory: 'Cart CTA - old', eventLabel: 'Select Amazon Pay', eventAction: 'Cart' });
	  }
});

$(document).on('click tap', 'button[name=dwfrm_singleshipping_shippingAddress_saveDeliveryDateAB]', function (e) {
	$(this).addClass('disabled-btn');
	utag.link({eventCategory: 'Cart CTA',eventLabel: 'Continue to Billing CTA', eventAction: 'Shipping & Delivery' });
});

$(document).on('click', '#delivery_dates', function () {
	if ($('.new-delivery-info').length) {
		utag.link({ eventCategory: 'Cart CTA', eventLabel: 'Select Delivery Date/Time', eventAction: 'Shipping & Delivery' });
	}
});

/*Sticky navigation for comfort landing page
 * ========================================*/
if ($(window).width() > 1031) {
	$(window).scroll(function () {
		var divTopElem = $('.tabs-shop-by-comfort');

		// Only for Shop by comfort page
		if (divTopElem.length > 0) {
			var divTop = divTopElem.offset().top;
			if ($(window).scrollTop() >= 532) {
				$('.tabs-shop-by-comfort').addClass('fixed-header');
			}
			else {
				$('.tabs-shop-by-comfort').removeClass('fixed-header');
			}
		}

	});
}

/* Attach Active class on navigation links on click
 * ================================================*/
$('.tabs-shop-by-comfort li > div').click(function () {

	var sectionname = $(this).attr("href");

	$('html,body').animate({ scrollTop: $(sectionname).offset().top - 80 }, 500);
	$('.tabs-shop-by-comfort li div').each(function () {
		$(this).parent().removeClass('activeLinkitem');
	});

	$(this).parent().addClass('activeLinkitem');
});

/* Attach Active class on navigation links on window scroll
 * ========================================================*/

$(window).scroll(function () {
	if ($('.tabs-shop-by-comfort').length > 0) {
		var scrollDistance = $(window).scrollTop();
		var navHeight = $('.tabs-shop-by-comfort').outerHeight();
		$('.shop-matress-comfort').each(function (i) {
			if ($(this).position().top < scrollDistance + navHeight) {
				$('.tabs-shop-by-comfort  li.activeLinkitem').removeClass('activeLinkitem');
				$('.tabs-shop-by-comfort  li').eq(i).addClass('activeLinkitem');
			}
		})
	}
}).scroll();

if ($('.tabs-shop-by-comfort').length > 0) {
	var imagepath = $('#imagepath').val();
	var ddData = [{
		text: "Extra Firm",
		value: "extra-firm",
		selected: false,
		description: "",
		imageSrc: imagepath.replace('temp', 'icon-black-extra-firm')
	},
	{
		text: "Firm",
		value: "firm",
		selected: false,
		description: "",
		imageSrc: imagepath.replace('temp', 'icon-black-firm')
	},
	{
		text: "Medium",
		value: "medium",
		selected: false,
		description: "",
		imageSrc: imagepath.replace('temp', 'icon-black-medium')
	},
	{
		text: "Plush",
		value: "plush",
		selected: false,
		description: "",
		imageSrc: imagepath.replace('temp', 'icon-black-plush')
	},
	{
		text: "Ultra Plush",
		value: "ultra-plush",
		selected: false,
		description: "",
		imageSrc: imagepath.replace('temp', 'icon-black-ultra-plush')
	}
	];
}

$(document).ready(function () {
	if ($('.tabs-shop-by-comfort').length > 0) {
		$('#comfortlevelMobile').ddslick({
			data: ddData,
			selectText: "Explore Comfort Levels",
			imagePosition: "left",
			onSelected: function (selectedData) {
				var plocation = '#' + selectedData.selectedData.value;
				$('html,body').animate({ scrollTop: $(plocation).offset().top - 150 }, 500);
			}
		});
		
		$('.dd-option-image').attr('alt', 'Option icon');
	}
});

$(document).scroll(function () {
	if ($('.tabs-shop-by-comfort').length > 0) {
		var y = $(this).scrollTop();
		if (y > 800) {
			$('.mobile-cl-selection').fadeIn();
		} else {
			$('.mobile-cl-selection').fadeOut();
		}
	}
});

$('.quote').slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	slide: '.inner-main',
	focusOnSelect: true,
	infinite: true,
	dots: true,
	arrows: false,
	autoplay: true,
	autoplaySpeed: 3000,
	adaptiveHeight: true,
});

$('.variation-carousal.AB-s').on('click tap', function (e) {
	e.preventDefault();
	$('.variations-container.BF').hide();
	$('.variations-container.BS').hide();
	$('.variations-container.BSH').hide();
	$('.variations-container.AB').show();
	$('.Q-section-1').show();
	$('.quote').slick('setPosition'); 
	$('.Q-section-2').hide();
	$('.Q-section-3').hide();
	$('.Q-section-4').hide();
	//Recmendation show/hide
	if ($('#Adjustable-Bases').length > 0) {
		$('#Adjustable-Bases').show();
	}
	if ($('#Beds-and-Frames').length > 0) {
		$('#Beds-and-Frames').hide();
	}
	if ($('#Box-Springs').length > 0) {
		$('#Box-Springs').hide();
	}
	if ($('#Bed-Sets-and-Headboards').length > 0) {
		$('#Bed-Sets-and-Headboards').hide();
	}
	$(".search-result-items").slick("setPosition");
});
$('.variation-carousal.BF-s').on('click tap', function (e) {
	e.preventDefault();
	$('.variations-container.AB').hide();
	$('.variations-container.BS').hide();
	$('.variations-container.BSH').hide();
	$('.variations-container.BF').show();
	$('.Q-section-1').hide();
	$('.Q-section-2').show();
	$('.quote').slick('setPosition');
	$('.Q-section-3').hide();
	$('.Q-section-4').hide();
	$(".variation-carousal.AB-s").removeClass("selected");
	//Recmendation show/hide
	if ($('#Adjustable-Bases').length > 0) {
		$('#Adjustable-Bases').hide();
	}
	if ($('#Beds-and-Frames').length > 0) {
		$('#Beds-and-Frames').show();
	}
	if ($('#Box-Springs').length > 0) {
		$('#Box-Springs').hide();
	}
	if ($('#Bed-Sets-and-Headboards').length > 0) {
		$('#Bed-Sets-and-Headboards').hide();
	}
	$(".search-result-items").slick("setPosition");
});
$('.variation-carousal.BS-s').on('click tap', function (e) {
	e.preventDefault();
	$('.variations-container.AB').hide();
	$('.variations-container.BF').hide();
	$('.variations-container.BSH').hide();
	$('.variations-container.BS').show();
	$('.Q-section-1').hide();
	$('.Q-section-2').hide();
	$('.Q-section-3').show();
	$('.quote').slick('setPosition');
	$('.Q-section-4').hide();
	$(".variation-carousal.AB-s").removeClass("selected");
	//Recmendation show/hide
	if ($('#Adjustable-Bases').length > 0) {
		$('#Adjustable-Bases').hide();
	}
	if ($('#Beds-and-Frames').length > 0) {
		$('#Beds-and-Frames').hide();
	}
	if ($('#Box-Springs').length > 0) {
		$('#Box-Springs').show();
	}
	if ($('#Bed-Sets-and-Headboards').length > 0) {
		$('#Bed-Sets-and-Headboards').hide();
	}
	$(".search-result-items").slick("setPosition");
});
$('.variation-carousal.BSH-s').on('click tap', function (e) {
	e.preventDefault();
	$('.variations-container.AB').hide();
	$('.variations-container.BF').hide();
	$('.variations-container.BS').hide();
	$('.variations-container.BSH').show();
	$('.Q-section-1').hide();
	$('.Q-section-2').hide();
	$('.Q-section-3').hide();
	$('.Q-section-4').show();
	$('.quote').slick('setPosition');
	$(".variation-carousal.AB-s").removeClass("selected");
	//Recmendation show/hide
	if ($('#Adjustable-Bases').length > 0) {
		$('#Adjustable-Bases').hide();
	}
	if ($('#Beds-and-Frames').length > 0) {
		$('#Beds-and-Frames').hide();
	}
	if ($('#Box-Springs').length > 0) {
		$('#Box-Springs').hide();
	}
	if ($('#Bed-Sets-and-Headboards').length > 0) {
		$('#Bed-Sets-and-Headboards').show();
	}
	$(".search-result-items").slick("setPosition");
});
$(".variation-carousal.AB-s .inner").hover(function () {
	$(".adjustable-base-head-move").css("display", "none");
	$(".adjustable-base-head-move-current").css("display", "block");
}, function () {
	if (!$(this).is(":focus")) {
		$(".adjustable-base-head-move").css("display", "block");
		$(".adjustable-base-head-move-current").css("display", "none");
	}
});
$(".variation-carousal.AB-s .inner").focus(function () {
	$(".adjustable-base-head-move").css("display", "none");
	$(".adjustable-base-head-move-current").css("display", "block");
});
$(".variation-carousal.AB-s .inner").blur(function () {
	$(".adjustable-base-head-move").css("display", "block");
	$(".adjustable-base-head-move-current").css("display", "none");
});
$(".variation-carousal.BF-s .inner").hover(function () {
	$(".bed-sets").css("display", "none");
	$(".bed-sets-current").css("display", "block");
}, function () {
	if (!$(this).is(":focus")) {
		$(".bed-sets").css("display", "block");
		$(".bed-sets-current").css("display", "none");
	}
});
$(".variation-carousal.BF-s .inner").focus(function () {
	$(".bed-sets").css("display", "none");
	$(".bed-sets-current").css("display", "block");
});
$(".variation-carousal.BF-s .inner").blur(function () {
	$(".bed-sets").css("display", "block");
	$(".bed-sets-current").css("display", "none");
});
$(".variation-carousal.BS-s .inner").hover(function () {
	$(".box-spring").css("display", "none");
	$(".box-spring-current").css("display", "block");
}, function () {
	if (!$(this).is(":focus")) {
		$(".box-spring").css("display", "block");
		$(".box-spring-current").css("display", "none");
	}
});
$(".variation-carousal.BS-s .inner").focus(function () {
	$(".box-spring").css("display", "none");
	$(".box-spring-current").css("display", "block");
});
$(".variation-carousal.BS-s .inner").blur(function () {
	$(".box-spring").css("display", "block");
	$(".box-spring-current").css("display", "none");
});
$(".variation-carousal.BSH-s .inner").hover(function () {
	$(".headboard").css("display", "none");
	$(".headboard-current").css("display", "block");
}, function () {
	if (!$(this).is(":focus")) {
		$(".headboard").css("display", "block");
		$(".headboard-current").css("display", "none");
	}
});
$(".variation-carousal.BSH-s .inner").focus(function () {
	$(".headboard").css("display", "none");
	$(".headboard-current").css("display", "block");
});
$(".variation-carousal.BSH-s .inner").blur(function () {
	$(".headboard").css("display", "block");
	$(".headboard-current").css("display", "none");
});

/**/
$('.header-help.header-tooltip').on('mouseenter', function () {
	$('.header-help.header-tooltip').attr('aria-expanded', 'true');
});
$('.header-help.header-tooltip').on('mouseleave', function () {
	$('.header-help.header-tooltip').attr('aria-expanded', 'false');
});
