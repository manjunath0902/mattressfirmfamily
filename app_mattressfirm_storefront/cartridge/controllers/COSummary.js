'use strict';

/**
 * This controller implements the last step of the checkout. A successful handling
 * of billing address and payment method selection leads to this controller. It
 * provides the customer with a last overview of the basket prior to confirm the
 * final order creation.
 *
 * @module controllers/COSummary
 */

/* API Includes */
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');
var PriceAdjustment = require('dw/order/PriceAdjustment');
var Money = require('dw/value/Money');
var OrderMgr = require('dw/order/OrderMgr');

/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');

//Include Commerce Link Module
var CommerceLinkFactory = require('int_commercelink/cartridge/scripts/utils/CommerceLinkFactory');
var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;
var Cart = app.getModel('Cart');
var apController = require('int_amazonpayments/cartridge/controllers/AmazonPayments'),
apModule = require('int_amazonpayments/cartridge/scripts/AmazonPaymentsModule');
var emailHelper = require("app_storefront_controllers/cartridge/scripts/util/SFEmailSubscriptionHelper").SFEmailSubscriptionHelper;
var UtilFunctions = require('~/cartridge/scripts/util/UtilFunctions').UtilFunctions;
var ProductUtils = require('app_storefront_core/cartridge/scripts/product/ProductUtils.js');
var COShippingMethodController = require('app_mattressfirm_storefront/cartridge/controllers/COShippingMethod');
var StringHelpers = require('app_storefront_core/cartridge/scripts/util/StringHelpers');

/**
 * Renders the summary page prior to order creation.
 */
function start(context) {
    var cart = Cart.get();
    if(cart){
    // Checks whether all payment methods are still applicable. Recalculates all existing non-gift certificate payment
    // instrument totals according to redeemed gift certificates or additional discounts granted through coupon
    // redemptions on this page.
    var COBilling = require('~/cartridge/controllers/COBilling');
    if (!COBilling.ValidatePayment(cart)) {
        COBilling.Start();
        return;
    } else {

        Transaction.wrap(function () {
            cart.calculate();
        });

        Transaction.wrap(function () {
            if (!cart.calculatePaymentTransactionTotal()) {
                COBilling.Start();
            }
        });

        var pageMeta = require('app_storefront_controllers/cartridge/scripts/meta');
        pageMeta.update({pageTitle: Resource.msg('summary.meta.pagetitle', 'checkout', 'SiteGenesis Checkout')});
        app.getView({
            Basket: cart.object
        }).render('checkout/summary/summary');
    }
    } else {
        app.getController('Cart').Show();
        return;
    }
}

/**
 * This function is called when the "Place Order" action is triggered by the
 * customer.
 */
function submit() {
	//Convert Bundle Items into Product Line Items
	var cart = Cart.get();
	if(cart){
	var isBundledOrder = createBundleLineItems(cart);
	
	// Calls the COPlaceOrder controller that does the place order action and any payment authorization.
    // COPlaceOrder returns a JSON object with an order_created key and a boolean value if the order was created successfully.
    // If the order creation failed, it returns a JSON object with an error key and a boolean value.
	var COPlaceOrder = require('~/cartridge/controllers/COPlaceOrder');
    var placeOrderResult = COPlaceOrder.Start();
    if (!empty(placeOrderResult) && placeOrderResult.error) {
        cart = Cart.get();
        
        //Re-engineer what have done in createBundleLineItems(cart); for bundle items
        var isReEngineered = revertBundleLineItems(cart);
        
        var COBilling = require('~/cartridge/controllers/COBilling');

        if (!COBilling.ValidatePayment(cart)) {
            COBilling.Start();
            return;
        } else {
            Transaction.wrap(function () {
                cart.calculate();
            });
            var payWithAmazon = false;
            if (apController.SetCheckoutMethod()) {
                payWithAmazon = true;
            }
            if (!payWithAmazon && !empty(cart.getPaymentInstruments('PayPal'))) {
                response.redirect(URLUtils.https('COPayPal-Start', 'stage', 'placeOrder'));
                return;
            }
            //get userGent Mobile|Desktop
            var isMobileDevice = StringHelpers.isMobileDevice(request.httpUserAgent);
            if (isMobileDevice) {
                app.getView({
                    Basket: cart.object,
                    PlaceOrderError: placeOrderResult.PlaceOrderError
                }).render('checkout/billing/billing_mob_ab');
            } else {
                app.getView({
                    Basket: cart.object,
                    PlaceOrderError: placeOrderResult.PlaceOrderError
                }).render('checkout/billing/billing');
            }
        }
    } else if (!empty(placeOrderResult) && placeOrderResult.order_created) {
    	//Real time Order Creation
    	var clOrderCreationStatus = dw.system.HookMgr.callHook(CommerceLinkFactory.HOOKS.ORDER.CREATE.ID, CommerceLinkFactory.HOOKS.ORDER.CREATE.SCRIPT, placeOrderResult.Order);
        var createOrderResponseDetails = clOrderCreationStatus.getDetail(CommerceLinkFactory.STATUS_CODES.RESPONSE_OBJECT);
        
        showConfirmation(placeOrderResult.Order);
    } else if (!empty(placeOrderResult) && placeOrderResult.missingShippingAddress) {
        response.redirect(URLUtils.https('Cart-Show'));
        return;
    }
    } else {
        response.redirect(URLUtils.https('Cart-Show'));
        return;
    }
}

function amazonSubmit() {
    var cart = Cart.get();
	if(cart){
	    var isBundledOrder = createBundleLineItems(cart);
        customer = session.custom.amazonCustomer;
		var COPlaceOrder = require('~/cartridge/controllers/COPlaceOrder');
		var placeOrderResult = COPlaceOrder.Start();
        if (placeOrderResult.error) {
    		if (placeOrderResult.amazonAuthError && !empty(placeOrderResult.amazonAuthError.message)) {
    		switch(placeOrderResult.amazonAuthError.code) {
    			case "TransactionTimedOut":
    				//response.redirect(URLUtils.https('COBilling-Start'));
    				var ErrorArray = [{"heading" : Resource.msg('amazon.amazonrejected.heading', 'apcheckout', null)}, {"msg" : Resource.msg('amazon.amazonrejected.msg', 'apcheckout', null)}];
        			session.custom.AmazonAuthErrorMessage = ErrorArray;
            		response.redirect(URLUtils.https('Cart-Show', 'AmazonAuthError', true));
    				/*var ErrorArray = [{"orderNo" : placeOrderResult.orderNo}, {"orderCreationDate" : placeOrderResult.orderCreationDate}];
    				session.custom.AmazonTimeoutError = ErrorArray;
        			response.redirect(URLUtils.https('COBilling-AmazonOrderTimeout'));*/
        			break;
        		case "AmazonRejected":
        			var ErrorArray = [{"heading" : Resource.msg('amazon.amazonrejected.heading', 'apcheckout', null)}, {"msg" : Resource.msg('amazon.amazonrejected.msg', 'apcheckout', null)}];
        			session.custom.AmazonAuthErrorMessage = ErrorArray;
            		response.redirect(URLUtils.https('Cart-Show', 'AmazonAuthError', true));
            		break;
        		case "InvalidPaymentMethod":
        			var ErrorArray = [{"heading" : Resource.msg('amazon.invalidpaymentmethod.heading', 'apcheckout', null)}, {"msg" : Resource.msg('amazon.invalidpaymentmethod.msg', 'apcheckout', null)}];
        			session.custom.AmazonAuthErrorMessage = ErrorArray;
        			response.redirect(URLUtils.https('COBilling-Start', 'AmazonAuthError', true));
        			break;
        		case "PaymentMethodNotAllowed":
        			var ErrorArray = [{"heading" : Resource.msg('amazon.PaymentMethodNotAllowed.heading', 'apcheckout', null)}, {"msg" : Resource.msg('amazon.PaymentMethodNotAllowed.msg', 'apcheckout', null)}];
        			session.custom.AmazonAuthErrorMessage = ErrorArray;
        			response.redirect(URLUtils.https('COBilling-Start', 'AmazonAuthError', true));
        			break;	
        		default:
        			var ErrorArray = [{"heading" : Resource.msg('amazon.unknownerror.heading', 'apcheckout', null)}, {"msg" : Resource.msg('amazon.unknownerror.msg', 'apcheckout', null)}];
    				session.custom.AmazonAuthErrorMessage = ErrorArray;
        			response.redirect(URLUtils.https('COBilling-Start', 'AmazonAuthError', true));
        			break;
        		}
    		} else if (!empty(placeOrderResult.amazonResultError)) {
    			var ErrorArray = [{"heading" : Resource.msg('amazon.unknownerror.heading', 'apcheckout', null)}, {"msg" : Resource.msg('amazon.unknownerror.msg', 'apcheckout', null)}];
    			session.custom.AmazonResultErrorMessage = ErrorArray;
    			response.redirect(URLUtils.https('COBilling-Start', 'AmazonResultError', true));
        		return {};
    		}
        } else if (placeOrderResult.order_created) {
            if(!empty(placeOrderResult.Order) && placeOrderResult.Order.custom.amazonCapturedAmount > 0) {
                Transaction.wrap(function () {
                    placeOrderResult.Order.setPaymentStatus(dw.order.Order.PAYMENT_STATUS_PAID);
                });
            }
        	var isEmailSubscribed = app.getForm('billing').object.billingAddress.addToEmailList.value;
        	if(isEmailSubscribed) {
        		subscribeEmail(session.custom.amazonCustomer.email);
        	}
          //Real time Order Creation
        	/*var clOrderCreationStatus = dw.system.HookMgr.callHook(CommerceLinkFactory.HOOKS.ORDER.CREATE.ID, CommerceLinkFactory.HOOKS.ORDER.CREATE.SCRIPT, placeOrderResult.Order);
            var createOrderResponseDetails = clOrderCreationStatus.getDetail(CommerceLinkFactory.STATUS_CODES.RESPONSE_OBJECT);
            */
            showConfirmation(placeOrderResult.Order);
        }
    } else {
        app.getController('Cart').Show();
        return;
    }
}

function subscribeEmail(emailAddress) {
	var gaCookie = request.getHttpCookies()['_ga'];
	var gclid;
	if (gaCookie) {
		gclid = gaCookie.value;
	}
	var emailParams = {
		emailAddress: emailAddress, 
		zipCode:session.custom.customerZip, 
		leadSource: 'checkout', 
		siteId: dw.system.Site.getCurrent().getID(), 
		optOutFlag: !session.forms.singleshipping.shippingAddress.addToSubscription.value,
		gclid: gclid,
		dwsid: session.sessionID
	};
	
	var returnResult = emailHelper.sendSFEmailInfo(emailParams);
	if (returnResult.Status == 'SERVICE_ERROR'){
		var returnResult = emailHelper.sendFailSafe(emailParams, returnResult.ErrorCode);
	}
}
/**
 * This function is called when the "Place Order" action is triggered by the
 * customer.
 */
function submitMobileOrder() {	
	var cart = Cart.get();
	if(cart){
			var isOnlyRedCarpetItemsInCart = !empty(cart.object) ? mattressPipeletHelper.getOrderIsRedCarpetStatus(cart.object) : false;
            var addressAtpInfoAttr = '';
            if (isOnlyRedCarpetItemsInCart) {
	            addressAtpInfoAttr = UtilFunctions.getAddressAtpInfoAttr_FromShipment(cart);
	            //2049-12-31T06:11:55.699Z|8:00am-8:00pm|DZL002446249|77025
	            Transaction.wrap(function () {
	            	cart.object.custom.confirmationDeliveryDateTimeZoneZip  = addressAtpInfoAttr;
	            	cart.object.custom.addressesDeliveryDateTimeZoneZip 	= addressAtpInfoAttr;
	             });
            }
      		submit();
    } else {
        response.redirect(URLUtils.https('Cart-Show'));
        return;
    }
}

/**
 * Renders the order confirmation page after successful order
 * creation. If a nonregistered customer has checked out, the confirmation page
 * provides a "Create Account" form. This function handles the
 * account creation.
 */
function showConfirmation(order) {
    if (!customer.authenticated) {
        // Initializes the account creation form for guest checkouts by populating the first and last name with the
        // used billing address.
        var customerForm = app.getForm('profile.customer');
        customerForm.setValue('firstname', order.billingAddress.firstName);
        customerForm.setValue('lastname', order.billingAddress.lastName);
        customerForm.setValue('email', order.customerEmail);
    }

    app.getForm('profile.login.passwordconfirm').clear();
    app.getForm('profile.login.password').clear();

    var pageMetaData = request.pageMetaData;
    pageMetaData.title = Resource.msg('confirmation.meta.pagetitle', 'checkout', 'SiteGenesis Checkout Confirmation');
    var isOnlyRedCarpetItemsInCart = mattressPipeletHelper.getOrderIsRedCarpetStatus(order);
    var template = 'checkout/confirmation/confirmation_redesign';
    //get userGent Mobile|Desktop
    var isMobileDevice = StringHelpers.isMobileDevice(request.httpUserAgent);
    if (isMobileDevice) {
    	template = 'checkout/confirmation/confirmation_redesign_mob_ab';
    }
    app.getView({
        Order: order,
        IsOnlyRedCarpetItemsInCart: isOnlyRedCarpetItemsInCart,
        ContinueURL: URLUtils.https('Account-RegistrationForm') // needed by registration form after anonymous checkouts
    }).render(template);
    /*app.getView({
        Order: order,
        IsOnlyRedCarpetItemsInCart: isOnlyRedCarpetItemsInCart,
        ContinueURL: URLUtils.https('Account-RegistrationForm') // needed by registration form after anonymous checkouts
    }).render('checkout/confirmation/confirmation_redesign'); */
}

/*
 * Re-Create Bundle Products Line Items
 */
function createBundleLineItems(cart) {
    var order = cart.object;
    var bundledOrder = false;
    var shipmentsIterator = order.getShipments().iterator();
    var bundlePromotionsCollection = new Array();    
    Transaction.wrap(function () {
        try{
            while (shipmentsIterator.hasNext()) {
                shipIt = shipmentsIterator.next();
                var productLineItems = shipIt.getProductLineItems();
                //collect promotions from bundle item
                for each(var productLineItem in productLineItems) { 
                 if(productLineItem.product.bundle) {
                    if (!productLineItem.getPriceAdjustments().empty) {
                        var bundlePriceAdjustments = productLineItem.getPriceAdjustments().iterator();                        
                        while (bundlePriceAdjustments.hasNext()) {
                          var bundlePriceAdjustment = bundlePriceAdjustments.next();                        
                          var jsonData = new Object();
                          jsonData.bundleId = productLineItem.product.ID;
                          jsonData.promoId = bundlePriceAdjustment.promotionID;
                          jsonData.priceAdjustment = bundlePriceAdjustment.basePrice.value;
                          jsonData.taxClassInfo = productLineItem.taxClassID;
                          bundlePromotionsCollection.push(jsonData);                              
                        }                    
                    }
                 }
                }                
                for each(var productLineItem in productLineItems) {
                    //Convert Bundle LineItems into Product Line Items
                    if(productLineItem.product.bundle) {
                        var bundleItems = productLineItem.product.bundledProducts;
                        var singleBundleItem, createProductLineItem, product, quantity, price, optionModel = null;
                        var bundleProduct = dw.catalog.ProductMgr.getProduct(productLineItem.product.ID)
                        var bundlePricingModel = ProductUtils.getPricing(bundleProduct);
                        var bundlePriceDiff = bundlePricingModel.standard - bundlePricingModel.sale;
                        var bundleDiscountPercent, bundlePriceAdjustment, bundleItemsTotalPrice = 0, productTotalPrice = 0;
                        var mainBundleQuantity = productLineItem.getQuantityValue();
                        if (bundlePriceDiff != 0) {
                            bundleDiscountPercent = bundlePriceDiff / bundlePricingModel.standard * 100;
                            bundleDiscountPercent = new dw.campaign.PercentageDiscount(dw.util.Decimal(bundleDiscountPercent).round(2));
                        }
                        for (var i=0; i<bundleItems.length; i++) {
                            singleBundleItem = bundleItems[i];
                            product = dw.catalog.ProductMgr.getProduct(singleBundleItem.ID);
                            optionModel = singleBundleItem.optionModel
                            //createProductLineItem = order.createProductLineItem(product, optionModel, shipIt);
                            //quantity = productLineItem.getQuantityValue();
                            quantity = productLineItem.getQuantityValue() * productLineItem.product.getBundledProductQuantity(product).value;
                            price = product.priceModel.price;
                            productTotalPrice = price.valueOrNull * quantity;
                            var AddProductToBasketResult = new dw.system.Pipelet('AddProductToBasket').execute({
                                Basket: order,
                                Product: product,
                                ProductOptionModel: optionModel,
                                Quantity: quantity,
                                Category: product.categorized ? product.categories[0] : null
                            });

                            if (AddProductToBasketResult.result === PIPELET_ERROR) {
                                return;
                            }
                            if (bundlePriceDiff != 0) {
                                bundleItemsTotalPrice = dw.util.Decimal(bundleItemsTotalPrice + (productTotalPrice - (bundleDiscountPercent.percentage / 100 * productTotalPrice))).round(2);
                                if ((i == bundleItems.length - 1) && (bundlePricingModel.sale != bundleItemsTotalPrice)) {
                                    var productDiscount = Number(dw.util.Decimal((bundleDiscountPercent.percentage / 100 * productTotalPrice) - (mainBundleQuantity * bundlePricingModel.sale - bundleItemsTotalPrice)).round(2)) * -1;
                                    bundlePriceAdjustment = AddProductToBasketResult.ProductLineItem.createPriceAdjustment(productLineItem.product.ID + "-BundlePromo-" + (i + 1));
                                    bundlePriceAdjustment.setLineItemText(Resource.msg('checkout.bundle.priceadjustment', 'checkout', 'Bundle Product Price Adjustment'));
                                    bundlePriceAdjustment.setPriceValue(productDiscount);
                                } else {
                                    bundlePriceAdjustment = AddProductToBasketResult.ProductLineItem.createPriceAdjustment(productLineItem.product.ID + "-BundlePromo-" + (i + 1), bundleDiscountPercent);
                                    bundlePriceAdjustment.setLineItemText(Resource.msg('checkout.bundle.priceadjustment', 'checkout', 'Bundle Product Price Adjustment'));    
                                }
                                bundlePriceAdjustment.setTaxClassID(singleBundleItem.taxClassID);                             
                                var lineItemTax = new Money(0, empty(session.getCurrency().getCurrencyCode()) ? "USD" : session.getCurrency().getCurrencyCode());
                                bundlePriceAdjustment.setTax(lineItemTax);
                                bundlePriceAdjustment.updateTaxAmount(lineItemTax);
                            } else {
                                AddProductToBasketResult.ProductLineItem.setPriceValue(price.valueOrNull);
                            }
                            //Set Bundle Item Attributes
                            AddProductToBasketResult.ProductLineItem.custom.isBundledItem = true;
                            var mainBundleID = productLineItem.product.ID;                            
                            AddProductToBasketResult.ProductLineItem.custom.mainBundleID = mainBundleID;
                            AddProductToBasketResult.ProductLineItem.custom.mainBundleQuantity = productLineItem.getQuantityValue(); 
                        }
                        order.removeProductLineItem(productLineItem);                        
                        bundledOrder = true;
                    }
                }
            }
            if(bundledOrder) {
            	cart.calculate();
            	if(bundlePromotionsCollection != null && bundlePromotionsCollection.length > 0) {
            	   applyBundlePromo(cart,bundlePromotionsCollection);
                } else {
                    if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
                        var inMarketShipment = cart.object.getShipment('In-Market');
                        if(!empty(inMarketShipment)) {
        					defaultDeliveryTier = inMarketShipment.getShippingMethodID();
                        }
                        mattressPipeletHelper.splitShipmentsByType(cart);
                        cart.removeEmptyShipments();
	                    cart.updateShipmentShippingMethod(inMarketShipment.getID(), defaultDeliveryTier, null, null);
                    } else {
                        mattressPipeletHelper.splitShipmentsByType(cart);
                    }
            	}
            }
        } catch (e) {
             var error = e;       
        }
    });           
    return bundledOrder;
}

/*
 * Apply Bundle Promotions
 */
function applyBundlePromo(cart,bundlePromoCollection) {	
    	var bundlePromotionsCollection = bundlePromoCollection;
    	var order = cart.object;
    	var bundledOrder = false;
    	var shipmentsIterator = order.getShipments().iterator();    
    	Transaction.wrap(function () {
        try{
            while (shipmentsIterator.hasNext()) {
                shipIt = shipmentsIterator.next();
                var productLineItems = shipIt.getProductLineItems();                                
                for each(var item in  bundlePromotionsCollection) {
                    for each (var productLineItem in productLineItems) {
                         if(item.bundleId == productLineItem.custom.mainBundleID) {
                             var newAdjustment = productLineItem.createPriceAdjustment(item.promoId+"-BundlePromo");                              
                             newAdjustment.setTaxClassID(item.taxClassInfo);                             
                             var lineItemTax = new Money(0, empty(session.getCurrency().getCurrencyCode()) ? "USD" : session.getCurrency().getCurrencyCode());
                             newAdjustment.setTax(lineItemTax);
                             newAdjustment.updateTaxAmount(lineItemTax);                         
                             newAdjustment.setPriceValue(item.priceAdjustment);                                 
                             break;
                         }                            
                    }                 
                }
            }
            mattressPipeletHelper.splitShipmentsByType(cart);            
        } catch (e) {
             var error = e;             
        }
    });
}
/*
 * Re-Create Bundle Products Line Items
 */
function revertBundleLineItems(cart) {
    var order = cart.object;
    var shipmentsIterator = order.getShipments().iterator();
    var cartItemsHashMap = new dw.util.HashMap();
    
    Transaction.wrap(function () {
        try{
            while (shipmentsIterator.hasNext()) {
                shipIt = shipmentsIterator.next();
                var productLineItems = shipIt.getProductLineItems();
                var isBundleCreated = false;
                var createProductLineItem, product, quantity, price, optionModel = null;
                for each(var productLineItem in productLineItems) {                	
                	//Convert Product Line Items into Bundle Product
                    if(productLineItem.custom.isBundledItem) {    					
    					var bundleKey = productLineItem.custom.mainBundleID;
    					var bundleValue = productLineItem.getQuantityValue();
    					if (!cartItemsHashMap.containsKey(bundleKey)) {
    						cartItemsHashMap.put(bundleKey,bundleValue);
    					}                   	
                    	order.removeProductLineItem(productLineItem);
                    }                	
                }
                
            }
            
        	for each(var cartItem in cartItemsHashMap.entrySet().iterator()) {            	
            	var mainBundleId = cartItem.key;            	
                product = dw.catalog.ProductMgr.getProduct(mainBundleId);
                optionModel = product.getOptionModel();
                quantity = cartItem.value;                    
                price = product.priceModel.price;
                //createProductLineItem = order.createProductLineItem(product, optionModel, shipIt);
                var addProductToBasketResult = new dw.system.Pipelet('AddProductToBasket').execute({
                    Basket: order,
                    Product: product,
                    ProductOptionModel: optionModel,
                    Quantity: quantity,
                    Category: product.categorized ? product.categories[0] : null
                });
                if (addProductToBasketResult.result === PIPELET_ERROR) {
                    return false;
                }
                
                //set Quantity, Price and Category
                //createProductLineItem.setQuantityValue(quantityValue.valueOf());
                addProductToBasketResult.ProductLineItem.setPriceValue(price.valueOrNull);
                
                isBundleCreated = true;
            }
                
            //Calculate Again After Re-Creation of Bundle Items
            cart.calculate();
            
            return true;            
            
        } catch (e) {
           var error = e;
           return false             
        }
    }); 
}

function realTimeOrderCreationInAX() {
	try {
		var orderNo = session.custom.amazonOrderNumber;
		var order = OrderMgr.getOrder(orderNo);
		//Real time Order Creation
		var clOrderCreationStatus = dw.system.HookMgr.callHook(CommerceLinkFactory.HOOKS.ORDER.CREATE.ID, CommerceLinkFactory.HOOKS.ORDER.CREATE.SCRIPT, order);
	    var createOrderResponseDetails = clOrderCreationStatus.getDetail(CommerceLinkFactory.STATUS_CODES.RESPONSE_OBJECT);
	    session.custom.amazonOrderNumber = null;
	    return true;
	} catch (e) {
	    return null;
	}
}

/*
 * Module exports
 */

/*
 * Web exposed methods
 */
/** @see module:controllers/COSummary~Start */
exports.Start = guard.ensure(['https'], start);
/** @see module:controllers/COSummary~Submit */
exports.SubmitMobileOrder = guard.ensure(['https'], submitMobileOrder);
exports.Submit = guard.ensure(['https', 'post'], submit);
exports.AmazonSubmit = guard.ensure(['https', 'post'], amazonSubmit);

/*
 * Local method
 */
exports.ShowConfirmation = showConfirmation;
exports.RealTimeOrderCreationInAX = guard.ensure(['get'], realTimeOrderCreationInAX);
