'use strict';

/**
 * Controller for the billing logic. It is used by both the single shipping and the multishipping
 * functionality and is responsible for payment method selection and entering a billing address.
 *
 * @module controllers/COBilling
 */

/* API Includes */
var GiftCertificate = require('dw/order/GiftCertificate');
var GiftCertificateMgr = require('dw/order/GiftCertificateMgr');
var GiftCertificateStatusCodes = require('dw/order/GiftCertificateStatusCodes');
var PaymentInstrument = require('dw/order/PaymentInstrument');
var PaymentMgr = require('dw/order/PaymentMgr');
var ProductListMgr = require('dw/customer/ProductListMgr');
var Resource = require('dw/web/Resource');
var Status = require('dw/system/Status');
var StringUtils = require('dw/util/StringUtils');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');
var Countries = require('app_storefront_core/cartridge/scripts/util/Countries');
var Synchrony = require('int_synchrony/cartridge/scripts/util/Form');
var Logger = require('dw/system/Logger');
var pipeletHelper = require('bc_sleepysc/cartridge/scripts/sleepys/util/SleepysPipeletsHelper').SleepysPipeletHelper;
var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;
var UtilFunctionsMF = require('app_mattressfirm_storefront/cartridge/scripts/util/UtilFunctions').UtilFunctions; 
/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var StringHelpers = require('app_storefront_core/cartridge/scripts/util/StringHelpers');
var emailHelper = require("app_storefront_controllers/cartridge/scripts/util/SFEmailSubscriptionHelper").SFEmailSubscriptionHelper;
var apController = require('int_amazonpayments/cartridge/controllers/AmazonPayments');
var apModule = require('int_amazonpayments/cartridge/scripts/AmazonPaymentsModule');
var Site = require('dw/system/Site');
var UtilFunctions = require('~/cartridge/scripts/util/UtilFunctions').UtilFunctions;
var COShippingMethodController = require('app_mattressfirm_storefront/cartridge/controllers/COShippingMethod');

function addEmptyCCTypeByDefault(applicablePaymentCards) {
    var defaultCC = new Object();
    defaultCC.active = true;
    defaultCC.cardType = '';
    defaultCC.name = 'Select';
    applicablePaymentCards.addAt(0, defaultCC)
}

/**
 * Initializes the address form. If the customer chose "use as billing
 * address" option on the single shipping page the form is prepopulated with the shipping
 * address, otherwise it prepopulates with the billing address that was already set.
 * If neither address is available, it prepopulates with the default address of the authenticated customer.
 */
function initAddressForm(cart) {

    if (app.getForm('singleshipping').object.shippingAddress.useAsBillingAddress.value === true) {
        app.getForm('billing').object.billingAddress.addressFields.firstName.value = app.getForm('singleshipping').object.shippingAddress.addressFields.firstName.value;
        app.getForm('billing').object.billingAddress.addressFields.lastName.value = app.getForm('singleshipping').object.shippingAddress.addressFields.lastName.value;
        app.getForm('billing').object.billingAddress.addressFields.address1.value = app.getForm('singleshipping').object.shippingAddress.addressFields.address1.value;
        app.getForm('billing').object.billingAddress.addressFields.address2.value = app.getForm('singleshipping').object.shippingAddress.addressFields.address2.value;
        app.getForm('billing').object.billingAddress.addressFields.city.value = app.getForm('singleshipping').object.shippingAddress.addressFields.cities.city.value;
        app.getForm('billing').object.billingAddress.addressFields.postal.value = app.getForm('singleshipping').object.shippingAddress.addressFields.postal.value;
        app.getForm('billing').object.billingAddress.addressFields.phone.value = app.getForm('singleshipping').object.shippingAddress.addressFields.phone.value;
        app.getForm('billing').object.billingAddress.addressFields.states.state.value = app.getForm('singleshipping').object.shippingAddress.addressFields.states.state.value;
        app.getForm('billing').object.billingAddress.addressFields.country.value = app.getForm('singleshipping').object.shippingAddress.addressFields.country.value;
        app.getForm('billing').object.billingAddress.addressFields.phone.value = app.getForm('singleshipping').object.shippingAddress.addressFields.phone.value;
    } else if (cart.getBillingAddress() !== null) {
        app.getForm('billing.billingAddress.addressFields').copyFrom(cart.getBillingAddress());
        app.getForm('billing.billingAddress.addressFields.states').copyFrom(cart.getBillingAddress());
    } else if (customer.authenticated && customer.profile.addressBook.preferredAddress !== null) {

        app.getForm('billing.billingAddress.addressFields').copyFrom(customer.profile.addressBook.preferredAddress);
        app.getForm('billing.billingAddress.addressFields.states').copyFrom(customer.profile.addressBook.preferredAddress);
    }
}

/**
 * Initializes the email address form field. If there is already a customer
 * email set at the basket, that email address is used. If the
 * current customer is authenticated the email address of the customer's profile
 * is used.
 */
function initEmailAddress(cart) {
    if (cart.getCustomerEmail() !== null) {
        app.getForm('singleshipping').object.shippingAddress.email.emailAddress.value = cart.getCustomerEmail();
    } else if (customer.authenticated && customer.profile.email !== null) {
        app.getForm('singleshipping').object.shippingAddress.email.emailAddress.value = customer.profile.email;
    } else {
        app.getForm('singleshipping').object.shippingAddress.email.emailAddress.value = session.forms.singleshipping.shippingAddress.email.emailAddress.value;
    }
}

/**
 * Updates data for the billing page and renders it.
 * If payment method is set to gift certificate, gets the gift certificate code from the form.
 * Updates the page metadata. Gets a view and adds any passed parameters to it. Sets the Basket and ContinueURL properties.
 * Renders the checkout/billing/billing template.
 * @param {module:models/CartModel~CartModel} cart - A CartModel wrapping the current Basket.
 * @param {object} params - (optional) if passed, added to view properties so they can be accessed in the template.
 */
function returnToForm(cart, params) {
    var pageMeta = require('app_storefront_controllers/cartridge/scripts/meta');
    
    // if the payment method is set to gift certificate get the gift certificate code from the form
    if (!empty(cart.getPaymentInstrument()) && cart.getPaymentInstrument().getPaymentMethod() === PaymentInstrument.METHOD_GIFT_CERTIFICATE) {
        app.getForm('billing').copyFrom({
            giftCertCode: cart.getPaymentInstrument().getGiftCertificateCode()
        });
    }

    pageMeta.update({
        pageTitle: Resource.msg('billing.meta.pagetitle', 'checkout', 'SiteGenesis Checkout')
    });
    
    //CPP-213 Add-current-promotional-financing-offers-to-checkout
    //Detect synchrony offer based on basket subtotal. CPP-213 ticket
    var basketSubTotal =  cart.object.getAdjustedMerchandizeTotalPrice(true).add(cart.object.giftCertificateTotalPrice).value.toFixed();
    var synchronyActiveFinancingObj = "";
    //********************************************************************************************************************************//
    //If synchronyFinancingOptions.length == 0, then By default 6-12 months financing options will be shown in synchrony form
	//Synchrony 24, 36, 48 or 60 months financing starts atleast from 999$ purchase
    //Check custom preference synchronyFinancingOptions check which one qualify then show that specific radio button on synchrony form
    //********************************************************************************************************************************//
    var synchronyFinancingOptions = Site.getCurrent().getCustomPreferenceValue('synchronyFinancingOptions');
    if (synchronyFinancingOptions.length > 0 && basketSubTotal >= 999) {
    	var promoCodes = [];
    	var promoAmount = [];
    	var temp;
    	for(var i=0; i<synchronyFinancingOptions.length; i++) {
    		temp = synchronyFinancingOptions[i].value.split('_');
    		promoCodes.push(temp[0]);
    	    promoAmount.push(parseInt(temp[1].replace('$')));
    	}
    	//*****************************************************************************************************//
    	//As custom preference must have ascending values of qualifying amount, thats why we are running loop 
    	//in opposite direction to find out best available financing offer for the customer.
    	//*****************************************************************************************************//
    	for (var i=(synchronyFinancingOptions.length-1); i >= 0; i--) {
    		if(basketSubTotal >= promoAmount[i]) {
    			synchronyActiveFinancingObj = synchronyFinancingOptions[i];
	    		break;
    		}
    	}
	}
		var defaultDeliveryTier = "";
		if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
			var inMarketShipment = cart.object.getShipment('In-Market');
			if(!empty(inMarketShipment)) {
				defaultDeliveryTier = inMarketShipment.getShippingMethodID();
			}
			if(!empty(params) && !empty(params.PayWithAmazon) && params.PayWithAmazon) { 
				//Retain silver/gold/Platinum if user already selected one of them
				defaultDeliveryTier = COShippingMethodController.SetDeliveryTierShippingMethod(defaultDeliveryTier);
			}
		}
	var validShipmentsCount = UtilFunctions.validShipmentsCount(cart.object);
    if (params) {
        app.getView(require('app_storefront_controllers/cartridge/scripts/object').extend(params, {
        	SynchronyActiveFinancingObj: synchronyActiveFinancingObj,
			DefaultDeliveryTier : defaultDeliveryTier,
			ValidShipmentsCount: validShipmentsCount,
        	Basket: cart.object,
            ContinueURL: URLUtils.https('COBilling-Billing')
        })).render('checkout/billing/billing');
    } else {
        app.getView({
        	SynchronyActiveFinancingObj: synchronyActiveFinancingObj,
            Basket: cart.object,
            ContinueURL: URLUtils.https('COBilling-Billing')
        }).render('checkout/billing/billing');
    }
}

/**
 * Updates data for the billing page and renders it.
 * If payment method is set to gift certificate, gets the gift certificate code from the form.
 * Updates the page metadata. Gets a view and adds any passed parameters to it. Sets the Basket and ContinueURL properties.
 * Renders the checkout/billing/billing template.
 * @param {module:models/CartModel~CartModel} cart - A CartModel wrapping the current Basket.
 * @param {object} params - (optional) if passed, added to view properties so they can be accessed in the template.
 */
function returnToFormMobileAB(cart, params) {
    var pageMeta = require('app_storefront_controllers/cartridge/scripts/meta');
    
    // if the payment method is set to gift certificate get the gift certificate code from the form
    if (!empty(cart.getPaymentInstrument()) && cart.getPaymentInstrument().getPaymentMethod() === PaymentInstrument.METHOD_GIFT_CERTIFICATE) {
        app.getForm('billing').copyFrom({
            giftCertCode: cart.getPaymentInstrument().getGiftCertificateCode()
        });
    }

    pageMeta.update({
        pageTitle: Resource.msg('billing.meta.pagetitle', 'checkout', 'SiteGenesis Checkout')
    });
    
    //CPP-213 Add-current-promotional-financing-offers-to-checkout
    //Detect synchrony offer based on basket subtotal. CPP-213 ticket
    var basketSubTotal =  cart.object.getAdjustedMerchandizeTotalPrice(true).add(cart.object.giftCertificateTotalPrice).value.toFixed();
    var synchronyActiveFinancingObj = "";
    //********************************************************************************************************************************//
    //If synchronyFinancingOptions.length == 0, then By default 6-12 months financing options will be shown in synchrony form
	//Synchrony 24, 36, 48 or 60 months financing starts atleast from 999$ purchase
    //Check custom preference synchronyFinancingOptions check which one qualify then show that specific radio button on synchrony form
    //********************************************************************************************************************************//
    var synchronyFinancingOptions = Site.getCurrent().getCustomPreferenceValue('synchronyFinancingOptions');
    if (synchronyFinancingOptions.length > 0 && basketSubTotal >= 999) {
    	var promoCodes = [];
    	var promoAmount = [];
    	var temp;
    	for(var i=0; i<synchronyFinancingOptions.length; i++) {
    		temp = synchronyFinancingOptions[i].value.split('_');
    		promoCodes.push(temp[0]);
    	    promoAmount.push(parseInt(temp[1].replace('$')));
    	}
    	//*****************************************************************************************************//
    	//As custom preference must have ascending values of qualifying amount, thats why we are running loop 
    	//in opposite direction to find out best available financing offer for the customer.
    	//*****************************************************************************************************//
    	for (var i=(synchronyFinancingOptions.length-1); i >= 0; i--) {
    		if(basketSubTotal >= promoAmount[i]) {
    			synchronyActiveFinancingObj = synchronyFinancingOptions[i];
	    		break;
    		}
    	}
	}
    
    if (params) {
        app.getView(require('app_storefront_controllers/cartridge/scripts/object').extend(params, {
        	SynchronyActiveFinancingObj: synchronyActiveFinancingObj,
        	Basket: cart.object,
            ContinueURL: URLUtils.https('COBilling-Billing')
        })).render('checkout/billing/billing_mob_ab');
    } else {
        app.getView({
        	SynchronyActiveFinancingObj: synchronyActiveFinancingObj,
            Basket: cart.object,
            ContinueURL: URLUtils.https('COBilling-Billing')
        }).render('checkout/billing/billing_mob_ab');
    }
}

/**
 * Updates cart calculation and page information and renders the billing page.
 * @transactional
 * @param {module:models/CartModel~CartModel} cart - A CartModel wrapping the current Basket.
 * @param {object} params - (optional) if passed, added to view properties so they can be accessed in the template.
 */
function start(cart, params) {

    Transaction.wrap(function () {
        cart.calculate();
    });
  //commented because this is not setting up title of the page
   /* var pageMeta = require('app_storefront_controllers/cartridge/scripts/meta');
    pageMeta.update({
        pageTitle: Resource.msg('billing.meta.pagetitle', 'checkout', 'SiteGenesis Checkout')
    });*/
    var pageMetaData = request.pageMetaData;
	pageMetaData.title = Resource.msg('singleshipping.meta.pagetitle', 'checkout', null);
    
    var payWithAmazon = false;

    if (apController.SetCheckoutMethod()) {
        payWithAmazon = true;
        session.custom.deliveryDatesArr = null;
    }

    if (payWithAmazon) {
        apModule.removeGiftCertificates(cart.object);

        if (session.forms.apShippingForm.billingAgreementID.value !== 'null') {
            var billingAgreementStatus = null,
                billingAgreementAction = apModule.billingAgreementAction(session.forms.apShippingForm.billingAgreementID.value, session.forms.apShippingForm.addressConsentToken.value);

            if (!billingAgreementAction.error) {
                billingAgreementStatus = billingAgreementAction.status;
            }

            params.Status = billingAgreementStatus;
        }
    }
    params.PayWithAmazon = payWithAmazon;

	
	returnToForm(cart, params);
}


/**
 * Updates cart calculation and page information and renders the billing page.
 * @transactional
 * @param {module:models/CartModel~CartModel} cart - A CartModel wrapping the current Basket.
 * @param {object} params - (optional) if passed, added to view properties so they can be accessed in the template.
 */
function startMobileAB(cart, params) {

    Transaction.wrap(function () {
        cart.calculate();
    });
  //commented because this is not setting up title of the page
   /* var pageMeta = require('app_storefront_controllers/cartridge/scripts/meta');
    pageMeta.update({
        pageTitle: Resource.msg('billing.meta.pagetitle', 'checkout', 'SiteGenesis Checkout')
    });*/
    var pageMetaData = request.pageMetaData;
	pageMetaData.title = Resource.msg('payment.meta.pagetitle', 'checkout', null);
    
    var payWithAmazon = false;

    if (apController.SetCheckoutMethod()) {
        payWithAmazon = true;
        session.custom.deliveryDatesArr = null;
    }

    if (payWithAmazon) {
        apModule.removeGiftCertificates(cart.object);

        if (session.forms.apShippingForm.billingAgreementID.value !== 'null') {
            var billingAgreementStatus = null,
                billingAgreementAction = apModule.billingAgreementAction(session.forms.apShippingForm.billingAgreementID.value, session.forms.apShippingForm.addressConsentToken.value);

            if (!billingAgreementAction.error) {
                billingAgreementStatus = billingAgreementAction.status;
            }

            params.Status = billingAgreementStatus;
        }
    }
    params.PayWithAmazon = payWithAmazon;

	
    returnToFormMobileAB(cart, params);
}

/**
 * Initializes the credit card list by determining the saved customer payment methods for the current locale.
 * @param {module:models/CartModel~CartModel} cart - A CartModel wrapping the current Basket.
 * @return {object} JSON object with members ApplicablePaymentMethods and ApplicableCreditCards.
 */
function initCreditCardList(cart) {
    var paymentAmount = cart.getNonGiftCertificateAmount();
    var countryCode;
    var applicablePaymentMethods;
    var applicablePaymentCards;
    var applicableCreditCards;

    countryCode = Countries.getCurrent({
        CurrentRequest: {
            locale: request.locale
        }
    }).countryCode;

    applicablePaymentMethods = PaymentMgr.getApplicablePaymentMethods(customer, countryCode, paymentAmount.value);
    applicablePaymentCards = PaymentMgr.getPaymentMethod(PaymentInstrument.METHOD_CREDIT_CARD).getApplicablePaymentCards(customer, countryCode, paymentAmount.value);

    try {
        addEmptyCCTypeByDefault(applicablePaymentCards);
    }
    catch(e) {
         Logger.error('Failed to add empty CC type by default ' + e.toString());
    }
    app.getForm('billing').object.paymentMethods.creditCard.type.setOptions(applicablePaymentCards.iterator());

    applicableCreditCards = null;

    if (customer.authenticated) {
        var profile = app.getModel('Profile').get();
        if (profile) {
            applicableCreditCards = profile.validateWalletPaymentInstruments(countryCode, paymentAmount.getValue()).ValidPaymentInstruments;
            try {
                addEmptyCCTypeByDefault(applicablePaymentCards);
            }
            catch(e) {
                 Logger.error('Failed to add empty CC type by default ' + e.toString());
            }
        }
    }
    
    return {
        ApplicablePaymentMethods: applicablePaymentMethods,
        ApplicableCreditCards: applicableCreditCards
    };
}

/**
 * Starting point for billing. After a successful shipping setup, both COShipping
 * and COShippingMultiple call this function.
 */
function publicStart(amazonAuthError) {
    var cart = app.getModel('Cart').get();
    session.custom.checkoutStep = 'payment';
    var params = request.httpParameterMap;
    session.custom.aPaySlotTimeOutCheck = (params.deliveryScheduleTimeout.submitted && params.deliveryScheduleTimeout.stringValue === 'true') ? true : false;
    if (cart) {
    	if (dw.system.Site.getCurrent().getCustomPreferenceValue('synchronyEnabled') ) {
    		var Synchrony = require('*/cartridge/controllers/CODigitalBuy');
    		Synchrony.preFilledSynchronyDigitalBuyForm();
    	}
    	var payWithAmazon = false;
    	if (apController.SetCheckoutMethod()) {
    		payWithAmazon = true;
    	}
    	if(!payWithAmazon && !empty(cart.getPaymentInstruments('PayPal'))){
    		resetPaymentForms();
    	    session.forms.singleshipping.clearFormElement();
    	    session.forms.multishipping.clearFormElement();
    	    session.forms.billing.clearFormElement();
    		session.forms.billing.paymentMethods.selectedPaymentMethodID.value = 'CREDIT_CARD';
    		session.forms.singleshipping.shippingAddress.useAsBillingAddress.value = true;
    	}
    	session.forms.cart.coupons.copyFrom(cart.object.couponLineItems);

        // Initializes all forms of the billing page including: - address form - email address - coupon form
        //initAddressForm(cart);
//        Synchrony.fillForm();
        initEmailAddress(cart);

        var creditCardList = initCreditCardList(cart);
        var applicablePaymentMethods = creditCardList.ApplicablePaymentMethods;

        var billingForm = app.getForm('billing').object;
        var paymentMethods = billingForm.paymentMethods;
        if (paymentMethods.valid) {
            paymentMethods.selectedPaymentMethodID.setOptions(applicablePaymentMethods.iterator());
        } else {
            paymentMethods.clearFormElement();
        }

        app.getForm('billing.couponCode').clear();
        app.getForm('billing.giftCertCode').clear();

        start(cart, {ApplicableCreditCards: creditCardList.ApplicableCreditCards});
    } else {
        app.getController('Cart').Show();
    }
}

function setPaymentMethod () {
	var params = request.httpParameterMap;
	var obj = new Object();
	if (!empty(params.CREDIT_CARD.stringValue)) {
		session.forms.billing.paymentMethods.selectedPaymentMethodID.value = 'CREDIT_CARD';
		obj.success = "true";
	}
	app.getView({
		obj: obj
    }).render('util/json');
}

/**
 * Adjusts gift certificate redemptions after applying coupon(s), because this changes the order total.
 * Removes and then adds currently added gift certificates to reflect order total changes.
 */
function adjustGiftCertificates() {
    var i, j, cart, gcIdList, gcID, gc;
    cart = app.getModel('Cart').get();

    if (cart) {
        gcIdList = cart.getGiftCertIdList();

        Transaction.wrap(function () {
            for (i = 0; i < gcIdList.length; i += 1) {
                cart.removeGiftCertificatePaymentInstrument(gcIdList[i]);
            }

            gcID = null;

            for (j = 0; j < gcIdList.length; j += 1) {
                gcID = gcIdList[j];

                gc = GiftCertificateMgr.getGiftCertificateByCode(gcID);

                if ((gc) && // make sure exists
                (gc.isEnabled()) && // make sure it is enabled
                (gc.getStatus() !== GiftCertificate.STATUS_PENDING) && // make sure it is available for use
                (gc.getStatus() !== GiftCertificate.STATUS_REDEEMED) && // make sure it has not been fully redeemed
                (gc.balance.currencyCode === cart.getCurrencyCode())) {// make sure the GC is in the right currency
                    cart.createGiftCertificatePaymentInstrument(gc);
                }
            }
        });
    }
}

/**
 * Used to adjust gift certificate totals, update page metadata, and render the billing page.
 * This function is called whenever a billing form action is handled.
 * @see {@link module:controllers/COBilling~returnToForm|returnToForm}
 * @see {@link module:controllers/COBilling~adjustGiftCertificates|adjustGiftCertificates}
 * @see {@link module:controllers/COBilling~billing|billing}
 */
function handleCoupon() {
    var CouponError;
    // @FIXME what is that used for?
    if (empty(CouponError)) {
        /*
         * Adjust gift certificate redemptions as after applying coupon(s),
         * order total is changed. AdjustGiftCertificate pipeline removes and
         * then adds currently added gift certificates to reflect order total
         * changes.
         */
        adjustGiftCertificates();
    }

    returnToForm(app.getModel('Cart').get());
}

/**
 * Redeems a gift certificate. If the gift certificate was not successfully
 * redeemed, the form field is invalidated with the appropriate error message.
 * If the gift certificate was redeemed, the form gets cleared. This function
 * is called by an Ajax request and generates a JSON response.
 * @param {String} giftCertCode - Gift certificate code entered into the giftCertCode field in the billing form.
 * @returns {object} JSON object containing the status of the gift certificate.
 */
function redeemGiftCertificate(giftCertCode) {
    var cart, gc, newGCPaymentInstrument, gcPaymentInstrument, status, result;
    cart = app.getModel('Cart').get();

    if (cart) {
        // fetch the gift certificate
        gc = GiftCertificateMgr.getGiftCertificateByCode(giftCertCode);

        if (!gc) {// make sure exists
            result = new Status(Status.ERROR, GiftCertificateStatusCodes.GIFTCERTIFICATE_NOT_FOUND);
        } else if (!gc.isEnabled()) {// make sure it is enabled
            result = new Status(Status.ERROR, GiftCertificateStatusCodes.GIFTCERTIFICATE_DISABLED);
        } else if (gc.getStatus() === GiftCertificate.STATUS_PENDING) {// make sure it is available for use
            result = new Status(Status.ERROR, GiftCertificateStatusCodes.GIFTCERTIFICATE_PENDING);
        } else if (gc.getStatus() === GiftCertificate.STATUS_REDEEMED) {// make sure it has not been fully redeemed
            result = new Status(Status.ERROR, GiftCertificateStatusCodes.GIFTCERTIFICATE_INSUFFICIENT_BALANCE);
        } else if (gc.balance.currencyCode !== cart.getCurrencyCode()) {// make sure the GC is in the right currency
            result = new Status(Status.ERROR, GiftCertificateStatusCodes.GIFTCERTIFICATE_CURRENCY_MISMATCH);
        } else {
            newGCPaymentInstrument = Transaction.wrap(function () {
                gcPaymentInstrument = cart.createGiftCertificatePaymentInstrument(gc);
                cart.calculate();
                return gcPaymentInstrument;
            });

            status = new Status(Status.OK);
            status.addDetail('NewGCPaymentInstrument', newGCPaymentInstrument);
            result = status;
        }
    } else {
        result = new Status(Status.ERROR, 'BASKET_NOT_FOUND');
    }
    return result;
}

/**
 * Updates credit card information from the httpParameterMap and determines if there is a currently selected credit card.
 * If a credit card is selected, it adds the the credit card number to the billing form. Otherwise, the {@link module:controllers/COBilling~publicStart|publicStart} method is called.
 * In either case, it will initialize the credit card list in the billing form and call the {@link module:controllers/COBilling~start|start} function.
 */
function updateCreditCardSelection() {
    var cart, applicableCreditCards, UUID, selectedCreditCard, instrumentsIter, creditCardInstrument;
    cart = app.getModel('Cart').get();

    applicableCreditCards = initCreditCardList(cart).ApplicableCreditCards;

    UUID = request.httpParameterMap.creditCardUUID.value || request.httpParameterMap.dwfrm_billing_paymentMethods_creditCardList.stringValue;

    selectedCreditCard = null;
    if (UUID && applicableCreditCards && !applicableCreditCards.empty) {

        // find credit card in payment instruments
        instrumentsIter = applicableCreditCards.iterator();
        while (instrumentsIter.hasNext()) {
            creditCardInstrument = instrumentsIter.next();
            if (UUID.equals(creditCardInstrument.UUID)) {
                selectedCreditCard = creditCardInstrument;
            }
        }

        if (selectedCreditCard) {
            app.getForm('billing').object.paymentMethods.creditCard.number.value = selectedCreditCard.creditCardNumber;
        } else {
            publicStart();
        }
    } else {
        publicStart();
    }

    app.getForm('billing.paymentMethods.creditCard').copyFrom(selectedCreditCard);

    initCreditCardList(cart);
    start(cart);
}

/**
 * Clears the form element for the currently selected payment method and removes the other payment methods.
 *
 * @return {Boolean} Returns true if payment is successfully reset. Returns false if the currently selected payment
 * method is bml and the ssn cannot be validated.
 */
function resetPaymentForms() {

    var cart = app.getModel('Cart').get();

    var status = Transaction.wrap(function () {
        if (app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value.equals('PayPal')) {
            app.getForm('billing').object.paymentMethods.creditCard.clearFormElement();
            app.getForm('billing').object.paymentMethods.bml.clearFormElement();
            cart.removePaymentInstruments(cart.getPaymentInstruments('SYNCHRONY_FINANCIAL'));
            cart.removePaymentInstruments(cart.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD));
            cart.removePaymentInstruments(cart.getPaymentInstruments(PaymentInstrument.METHOD_BML));
            cart.removePaymentInstruments(cart.getPaymentInstruments('AMAZON_PAYMENTS'));
        } else if (app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value.equals(PaymentInstrument.METHOD_CREDIT_CARD)) {
            app.getForm('billing').object.paymentMethods.bml.clearFormElement();
            cart.removePaymentInstruments(cart.getPaymentInstruments('SYNCHRONY_FINANCIAL'));
            cart.removePaymentInstruments(cart.getPaymentInstruments(PaymentInstrument.METHOD_BML));
            cart.removePaymentInstruments(cart.getPaymentInstruments('PayPal'));
            //remove front end formatting from credit card 
            if(!empty(session.forms.billing.paymentMethods.creditCard.number.value)){
            	session.forms.billing.paymentMethods.creditCard.number.value = session.forms.billing.paymentMethods.creditCard.number.value.replace(/\s+/g, '');
            }
            cart.removePaymentInstruments(cart.getPaymentInstruments('AMAZON_PAYMENTS'));
            
        } else if (app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value.equals(PaymentInstrument.METHOD_BML)) {
            app.getForm('billing').object.paymentMethods.creditCard.clearFormElement();

            if (!app.getForm('billing').object.paymentMethods.bml.ssn.valid) {
                return false;
            }
            cart.removePaymentInstruments(cart.getPaymentInstruments('SYNCHRONY_FINANCIAL'));
            cart.removePaymentInstruments(cart.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD));
            cart.removePaymentInstruments(cart.getPaymentInstruments('PayPal'));
            cart.removePaymentInstruments(cart.getPaymentInstruments('AMAZON_PAYMENTS'));
        }
        else if (app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value.equals('SYNCHRONY_FINANCIAL')) {
            app.getForm('billing').object.paymentMethods.bml.clearFormElement();
            app.getForm('billing').object.paymentMethods.creditCard.clearFormElement();
            
            cart.removePaymentInstruments(cart.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD));
            cart.removePaymentInstruments(cart.getPaymentInstruments(PaymentInstrument.METHOD_BML));
            cart.removePaymentInstruments(cart.getPaymentInstruments('PayPal'));
            cart.removePaymentInstruments(cart.getPaymentInstruments('AMAZON_PAYMENTS'));
        } else if (app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value.equals('AMAZON_PAYMENTS')) {
        	app.getForm('billing').object.paymentMethods.creditCard.clearFormElement();
        	app.getForm('billing').object.paymentMethods.bml.clearFormElement();
        	cart.removePaymentInstruments(cart.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD));
        	cart.removePaymentInstruments(cart.getPaymentInstruments(PaymentInstrument.METHOD_BML));
        	cart.removePaymentInstruments(cart.getPaymentInstruments('PayPal'));
        	cart.removePaymentInstruments(cart.getPaymentInstruments('SYNCHRONY_FINANCIAL'));
        }
        return true;
    });

    return status;
}

/**
 * Validates the billing form.
 * @returns {boolean} Returns true if the billing address is valid or no payment is needed. Returns false if the billing form is invalid.
 */
function validateBilling() {
    if (!app.getForm('billing').object.billingAddress.valid) {
        return false;
    }

    if (!empty(request.httpParameterMap.noPaymentNeeded.value)) {
        return true;
    }

    if (!empty(app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value) && app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value.equals(PaymentInstrument.METHOD_CREDIT_CARD)) {
        if (!app.getForm('billing').object.valid) {
            return false;
        }
    }

    return true;
}

/**
 * Handles the selection of the payment method and performs payment method-specific
 * validation and verification on the entered form fields. If the
 * order total is 0 (if the user has product promotions) then we do not
 * need a valid payment method.
 */
function handlePaymentSelection(cart) {
    var result;
    if (empty(app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value)) {
        if (cart.getTotalGrossPrice() > 0) {
            result = {
                error: true
            };
        } else {
            result = {
                ok: true
            };
        }
    }

    // skip the payment handling if the whole payment was made using gift cert
    if (app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value.equals(PaymentInstrument.METHOD_GIFT_CERTIFICATE)) {
        result = {
            ok: true
        };
    }

    if (empty(PaymentMgr.getPaymentMethod(app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value).paymentProcessor)) {
        result = {
            error: true,
            MissingPaymentProcessor: true
        };
    }
    if (!result) {
        result = app.getModel('PaymentProcessor').handle(cart.object, app.getForm('billing').object.paymentMethods.selectedPaymentMethodID.value);
    }
    return result;
}

/**
 * Gets or creates a billing address and copies it to the billingaddress form. Also sets the customer email address
 * to the value in the billingAddress form.
 * @transaction
 * @param {module:models/CartModel~CartModel} cart - A CartModel wrapping the current Basket.
 * @returns {boolean} true
 */
function handleBillingAddress(cart) {

    var billingAddress = cart.getBillingAddress();
    Transaction.wrap(function () {

        if (!billingAddress) {
            billingAddress = cart.createBillingAddress();
        }

        app.getForm('billing.billingAddress.addressFields').copyTo(billingAddress);
        app.getForm('billing.billingAddress.addressFields.states').copyTo(billingAddress);
        billingAddress.setPhone(StringHelpers.formatAXPhoneNumber(session.forms.billing.billingAddress.addressFields.phone.value));
        var address2 = billingAddress.address2;
        if (address2 == null || address2 == 'null') {
            billingAddress.setAddress2('');
        }

        billingAddress.setCity(session.forms.billing.billingAddress.addressFields.city.value);
        billingAddress.setCountryCode('US');
        //cart.setCustomerEmail(app.getForm('billing').object.billingAddress.email.emailAddress.value);
    });

    return true;
}

/**
 * Checks if there is currently a cart and if one exists, gets the customer address from the httpParameterMap and saves it to the customer address book.
 * Initializes the list of credit cards and calls the {@link module:controllers/COBilling~start|start} function.
 * If a cart does not already exist, calls the {@link module:controllers/Cart~Show|Cart controller Show function}.
 */
function updateAddressDetails() {
    var cart, address, billingAddress;
    cart = app.getModel('Cart').get();

    if (cart) {

        address = customer.getAddressBook().getAddress(empty(request.httpParameterMap.addressID.value) ? request.httpParameterMap.dwfrm_billing_addressList.value : request.httpParameterMap.addressID.value);

        app.getForm('billing.billingAddress.addressFields').copyFrom(address);
        app.getForm('billing.billingAddress.addressFields.states').copyFrom(address);

        billingAddress = cart.getBillingAddress();

        app.getForm('billing.billingAddress.addressFields').copyTo(billingAddress);

        initCreditCardList(cart);
        start(cart);
    } else {
        //@FIXME redirect
        app.getController('Cart').Show();
    }
}

/**
 * Form handler for the billing form. Handles the following actions:
 * - __applyCoupon__ - gets the coupon to add from the httpParameterMap couponCode property and calls {@link module:controllers/COBilling~handleCoupon|handleCoupon}
 * - __creditCardSelect__ - calls the {@link module:controllers/COBilling~updateCreditCardSelection|updateCreditCardSelection} function.
 * - __paymentSelect__ - calls the {@link module:controllers/COBilling~publicStart|publicStart} function.
 * - __redeemGiftCert__ - redeems the gift certificate entered into the billing form and returns to the cart.
 * - __save__ - validates payment and address information and handles any errors. If the billing form is valid,
 * saves the billing address to the customer profile, sets a flag to indicate the billing step is successful, and calls
 * the {@link module:controllers/COSummary~start|COSummary controller Start function}.
 * - __selectAddress__ - calls the {@link module:controllers/COBilling~updateAddressDetails|updateAddressDetails} function.
 */
function billing() {
var form = app.getForm('billing');
var cart = app.getModel('Cart').get();

	if(!empty(cart)) {
    app.getForm('billing').handleAction({
        applyCoupon: function () {
            var couponCode = request.httpParameterMap.couponCode.stringValue || request.httpParameterMap.dwfrm_billing_couponCode.stringValue;

            // TODO what happened to this start node?
            app.getController('Cart').AddCoupon(couponCode);

            handleCoupon();
            return;
        },
        creditCardSelect: function () {
            updateCreditCardSelection();
            return;
        },
        paymentSelect: function () {
            // ToDo - pass parameter ?
            publicStart();
            return;
        },
        redeemGiftCert: function () {
            var status = redeemGiftCertificate(app.getForm('billing').object.giftCertCode.htmlValue);
            if (!status.isError()) {
                returnToForm(app.getModel('Cart').get(), {
                    NewGCPaymentInstrument: status.getDetail('NewGCPaymentInstrument')
                });
            } else {
                returnToForm(app.getModel('Cart').get());
            }

            return;
        },
        save: function () {
            try
            {
                var cart = app.getModel('Cart').get();
                var payWithAmazon = false;
                if (apController.SetCheckoutMethod()) {
                    payWithAmazon = true;
                }
                var isMobileDevice = StringHelpers.isMobileDevice(request.httpUserAgent);
                
                var isOnlyRedCarpetItemsInCart = !empty(cart.object) ? mattressPipeletHelper.getOrderIsRedCarpetStatus(cart.object) : false;
                var addressAtpInfoAttr = '';
                //This if check runs only of credit card checkout
                if (isOnlyRedCarpetItemsInCart && !isMobileDevice && !payWithAmazon) {
                    addressAtpInfoAttr = UtilFunctions.getAddressAtpInfoAttr_FromShipment(cart);
                    //2049-12-31T06:11:55.699Z|8:00am-8:00pm|DZL002446249|77025
                    Transaction.wrap(function () {
                        cart.object.custom.confirmationDeliveryDateTimeZoneZip = addressAtpInfoAttr;
                        cart.object.custom.addressesDeliveryDateTimeZoneZip = addressAtpInfoAttr;
                    });
                }
                
                //reseting AmazonAuthErrorMessage and AmazonResultErrorMessage sessions
                session.custom.AmazonAuthErrorMessage=null;
                session.custom.AmazonResultErrorMessage = null;
                
                
                if (payWithAmazon) {
                    if (isOnlyRedCarpetItemsInCart) {
                        if(isMobileDevice) {
                            var params = request.httpParameterMap;
                            var deliveryDate = request.httpParameterMap.deliveryDate.value.trim();
                            var deliveryTime = request.httpParameterMap.deliveryTime.value.trim();
                            
                            session.forms.singleshipping.shippingAddress.addressFields.deliveryDate.htmlValue = deliveryDate;
                            session.forms.singleshipping.shippingAddress.addressFields.deliveryDate.value = deliveryDate;
                            session.forms.singleshipping.shippingAddress.addressFields.deliveryTime.htmlValue = deliveryTime;
                            session.forms.singleshipping.shippingAddress.addressFields.deliveryTime.value = deliveryTime;
                            session.forms.singleshipping.shippingAddress.scheduleWithRep.value = 'scheduledelivery'
                            
                            var COShippingController = require('~/cartridge/controllers/COShipping');
                            COShippingController.UpdateDeliveryDateSelection(cart);
                            addressAtpInfoAttr = UtilFunctions.getAddressAtpInfoAttr_FromShipment(cart);
                            //2049-12-31T06:11:55.699Z|8:00am-8:00pm|DZL002446249|77025
                            Transaction.wrap(function () {
                                cart.object.custom.confirmationDeliveryDateTimeZoneZip = addressAtpInfoAttr;
                                cart.object.custom.addressesDeliveryDateTimeZoneZip = addressAtpInfoAttr;
                                cart.object.custom.selectedDeliveryDateTimeZoneZip = addressAtpInfoAttr;
                            });
                        } else {
                            addressAtpInfoAttr = UtilFunctions.getAddressAtpInfoAttr_FromShipment(cart);
                            //set addressesDeliveryDateTimeZoneZip for amazon scenario
                            Transaction.wrap(function () {
                                cart.object.custom.confirmationDeliveryDateTimeZoneZip = addressAtpInfoAttr;
                                cart.object.custom.addressesDeliveryDateTimeZoneZip = addressAtpInfoAttr;
                                cart.object.custom.selectedDeliveryDateTimeZoneZip = addressAtpInfoAttr;
                            });
                        }
                    }
                    var amazonPhoneNum = app.getForm('billing').object.billingAddress.addressFields.phone.value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
                    amazonPhoneNum = amazonPhoneNum.replace(' ', '');
                    if(amazonPhoneNum.length < 10) {
                        response.redirect(URLUtils.https('COBilling-Start'));
                        return;
                    }
                    session.forms.billing.billingAddress.addressFields.phone.value = amazonPhoneNum;
                    app.getForm('singleshipping.shippingAddress.addressFields.phone').value = amazonPhoneNum;
                    session.forms.singleshipping.shippingAddress.addressFields.phone.value = amazonPhoneNum;
                    if (!resetPaymentForms()) {
                        returnToForm(cart, {PayWithAmazon: payWithAmazon});
                        return;
                    } else {
                        if (payWithAmazon) {
                            apController.UpdateBillingAddress(cart.object, null);
                            copyBillingAddressToShippingAddress(cart.object);
                        } else if (!handleBillingAddress(cart)) {
                            returnToForm(cart);
                            return;
                        } if (!validateShippingAddress() || handlePaymentSelection(cart).error) {
                            returnToForm(cart, {PayWithAmazon: payWithAmazon});
                            return;
                        } if (customer.authenticated && app.getForm('billing').object.billingAddress.addToAddressBook.value) {
                            app.getModel('Profile').get(customer.profile).addAddressToAddressBook(cart.getBillingAddress());
                        }  
                    app.getForm('billing').object.fulfilled.value = true;
                    var defaultDeliveryTier = "";
                    if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
                        var inMarketShipment = cart.object.getShipment('In-Market');
                        if(!empty(inMarketShipment)) {
                            defaultDeliveryTier = inMarketShipment.getShippingMethodID();
                        }
                    }
                    var COShippingController = require('~/cartridge/controllers/COShipping');
                    COShippingController.HandleShippingSettings(cart);
                    
                    if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
                        //Retain silver/gold/Platinum if user already selected one of them
                        defaultDeliveryTier = COShippingMethodController.SetDeliveryTierShippingMethod(defaultDeliveryTier);
                    }
                    var COSummary = require('~/cartridge/controllers/COSummary');
                        COSummary.AmazonSubmit();
                        return;
                    }
                }
                
                if (!validateShippingAddress()) {
                    //returnToForm(cart);
                    response.redirect(URLUtils.https('COBilling-Start'));
                    return;
                } else {
                    var defaultDeliveryTier = "";
                    if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
                        var inMarketShipment = cart.object.getShipment('In-Market');
                        if(!empty(inMarketShipment)) {
                            defaultDeliveryTier = inMarketShipment.getShippingMethodID();
                        }
                    }

                    var COShippingController = require('~/cartridge/controllers/COShipping');
                    COShippingController.HandleShippingSettings(cart);

                    if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
                        //Retain silver/gold/Platinum if user already selected one of them
                        defaultDeliveryTier = COShippingMethodController.SetDeliveryTierShippingMethod(defaultDeliveryTier);
                    }

                }

                if (!validateBillingAddress() || !resetPaymentForms() || !handleBillingAddress(cart) || // Performs validation steps, based upon the entered billing address
                        // and address options.
                        handlePaymentSelection(cart).error) {// Performs payment method specific checks, such as credit card verification.
                            returnToForm(cart);
                            return;
                } else {

                    // Mark step as fulfilled
                    app.getForm('billing').object.fulfilled.value = true;

                    // A successful billing page will jump to the next checkout step.                   
                    var COSummary = require('~/cartridge/controllers/COSummary');
                    COSummary.Submit(); 
                    return;
                }   
            }  
            catch(e)
            {
                var message = 'COBilling @ save method - Error occurred during order submission';	
                if (e && e.message) 	
                {	
                    message += ': ' + e.message;	
                }	
                Logger.info(message);	
                app.getView().render('error/checkoutgeneralerror');
            }

            return;
        },
        mobileOrder: function () {
            try{
                var cart = app.getModel('Cart').get();
			
                if (!validateBillingAddress() || !resetPaymentForms() || !handleBillingAddress(cart) || // Performs validation steps, based upon the entered billing address
                        // and address options.
                        handlePaymentSelection(cart).error) {// Performs payment method specific checks, such as credit card verification.
                            returnToForm(cart);
                            return;
                } else {
    
                    // Mark step as fulfilled
                    app.getForm('billing').object.fulfilled.value = true;
    
                    // A successful billing page will jump to the next checkout step.                    
                    var COSummary = require('~/cartridge/controllers/COSummary');
                    COSummary.Start(); 
                }
            }
            catch(e)
            {
                var message = 'COBilling @ mobileOrder method - Error occurred during mobile order submission';	
                if (e && e.message) 	
                {	
                    message += ': ' + e.message;	
                }	
                Logger.info(message);	
                app.getView().render('error/checkoutgeneralerror');
            }
            
            return;
        },
        mobileOrderAB: function () {
            try
            {
                var cart = app.getModel('Cart').get();
                if(!empty(cart)) {
                    if (!validateBillingAddress() || !resetPaymentForms() || !handleBillingAddress(cart) || // Performs validation steps, based upon the entered billing address
                        // and address options.
                        handlePaymentSelection(cart).error) {// Performs payment method specific checks, such as credit card verification.
                        response.redirect(URLUtils.https('COBilling-ContinueToBilling'));
                        return;
                     } else {
                        // Mark step as fulfilled
                        app.getForm('billing').object.fulfilled.value = true;
        
                        // A successful billing page will jump to the next checkout step.                    
                        var COSummary = require('~/cartridge/controllers/COSummary');
                        COSummary.SubmitMobileOrder(); 
                        return;
                    }
                } else {
                    response.redirect(URLUtils.https('Cart-Show'));
                    return;
                }
            }
            catch(e)
            {
                var message = 'COBilling @ mobileOrderAB method - Error occurred during mobile order submission';	
                if (e && e.message) 	
                {	
                    message += ': ' + e.message;	
                }	
                Logger.info(message);	
                app.getView().render('error/checkoutgeneralerror');
            }
        },
        editLinkToCart: function () {
            var form = session.forms.billing.paymentMethods;
            if(!empty(session.forms.billing.paymentMethods.creditCard.number.value)) {
                session.forms.billing.paymentMethods.creditCard.number.value = session.forms.billing.paymentMethods.creditCard.number.value.replace(/\s+/g, '');
                form.creditCard.number.setValue(session.forms.billing.paymentMethods.creditCard.number.value);
            }
            if(!empty(session.forms.billing.paymentMethods.creditCard.expirydate.value)){
                form.creditCard.expirydate.setValue(session.forms.billing.paymentMethods.creditCard.expirydate.value);
            }
            if(!empty(session.forms.billing.paymentMethods.creditCard.cvn.value)) {
                form.creditCard.cvn.setValue(session.forms.billing.paymentMethods.creditCard.cvn.value);
            }
            response.redirect(URLUtils.https('Cart-Show'));
            return;
        },
        editLinkToDelivery: function() {
            var form = session.forms.billing.paymentMethods;
            if(!empty(session.forms.billing.paymentMethods.creditCard.number.value)) {
                session.forms.billing.paymentMethods.creditCard.number.value = session.forms.billing.paymentMethods.creditCard.number.value.replace(/\s+/g, '');
                form.creditCard.number.setValue(session.forms.billing.paymentMethods.creditCard.number.value);
            }
            if(!empty(session.forms.billing.paymentMethods.creditCard.expirydate.value)){
                form.creditCard.expirydate.setValue(session.forms.billing.paymentMethods.creditCard.expirydate.value);
            }
            if(!empty(session.forms.billing.paymentMethods.creditCard.cvn.value)) {
                form.creditCard.cvn.setValue(session.forms.billing.paymentMethods.creditCard.cvn.value);
            }
            response.redirect(URLUtils.https('COShipping-GoToDeliveryPage'));
            return;
        },
        selectAddress: function () {
            updateAddressDetails();
            return;
        }
    });
  } else {
      response.redirect(URLUtils.https('Cart-Show'));
      return;
  }

}

/**
 * Handle coupon code deletion from billing
 */
function handleCouponCheckout() {
    var cartForm = app.getForm('cart');
    var cart = app.getModel('Cart').get();
    cartForm.handleAction({
        error: function () {
            response.redirect(URLUtils.https('COShippingMethod-Start'));
            return;
        },
        deleteCoupon: function (formgroup) {
        	var abc = 0;
            Transaction.wrap(function () {
                cart.removeCouponLineItem(formgroup.getTriggeredAction().object);
            });
            response.redirect(request.httpReferer);
            return ;
        }
    });
}

function copyBillingAddressToShippingAddress(basket) {
	var billingAddress = basket.billingAddress;
	if(!empty(billingAddress)) {
		for each(var shipment in basket.shipments) {
			if (shipment.productLineItems.length) {
				var shippingAddress = shipment.shippingAddress;
			    // if the shipment has no shipping address yet, create one
			    if (shippingAddress == null) {
			        shippingAddress = shipment.createShippingAddress();
			    }
			    if (empty(shippingAddress.firstName) || empty(shippingAddress.lastName) || empty(shippingAddress.address1) || empty(shippingAddress.city)) {
				    // copy the address details
				    var fullName, firstName, lastName;
				    Transaction.wrap(function () {
					    shippingAddress.setFirstName(billingAddress.firstName);
					    shippingAddress.setLastName(billingAddress.lastName);
					    shippingAddress.setAddress1(billingAddress.address1);
					    shippingAddress.setAddress2(billingAddress.address2);
					    shippingAddress.setCity(billingAddress.city);
					    shippingAddress.setPostalCode(billingAddress.postalCode);
					    shippingAddress.setStateCode(billingAddress.stateCode);
					    shippingAddress.setCountryCode(billingAddress.countryCode.value);
					    shippingAddress.setPhone(billingAddress.phone);
				    });
			    }
			}
		}
	
    app.getForm('singleshipping.shippingAddress.addressFields.firstName').value = billingAddress.firstName;
    session.forms.singleshipping.shippingAddress.addressFields.firstName.value = billingAddress.firstName;
    
	app.getForm('singleshipping.shippingAddress.addressFields.lastName').value = billingAddress.lastName;
    session.forms.singleshipping.shippingAddress.addressFields.lastName.value = billingAddress.lastName;

	app.getForm('singleshipping.shippingAddress.addressFields.postal').value = billingAddress.postalCode;
	session.forms.singleshipping.shippingAddress.addressFields.postal.value = billingAddress.postalCode;
	
	//saving postal code in billing form
	app.getForm('billing.billingAddress.addressFields.postal').value = billingAddress.postalCode;
	session.forms.billing.billingAddress.addressFields.postal.value = billingAddress.postalCode;
	
	app.getForm('singleshipping.shippingAddress.addressFields.address1').value = billingAddress.address1;
	session.forms.singleshipping.shippingAddress.addressFields.address1.value = billingAddress.address1;

	app.getForm('singleshipping.shippingAddress.addressFields.address2').value = billingAddress.address2;
	session.forms.singleshipping.shippingAddress.addressFields.address2.value = billingAddress.address2;
	
	app.getForm('singleshipping.shippingAddress.addressFields.city').value = billingAddress.city;
	session.forms.singleshipping.shippingAddress.addressFields.city.value = billingAddress.city;
	
	app.getForm('singleshipping.shippingAddress.addressFields.states.state').value = billingAddress.stateCode;
	session.forms.singleshipping.shippingAddress.addressFields.states.state.value = billingAddress.stateCode;
	
    app.getForm('singleshipping.shippingAddress.addressFields.country').value = billingAddress.countryCode.value;
	session.forms.singleshipping.shippingAddress.addressFields.country.value = billingAddress.countryCode.value;
    
	}
}
/**
* Gets the gift certificate code from the httpParameterMap and redeems it. For an ajax call, renders an empty JSON object.
* Otherwise, renders a JSON object with information about the gift certificate code and the success and status of the redemption.
*/
function redeemGiftCertificateJson() {
    var giftCertCode, giftCertStatus;

    giftCertCode = request.httpParameterMap.giftCertCode.stringValue;
    giftCertStatus = redeemGiftCertificate(giftCertCode);

    let responseUtils = require('app_storefront_controllers/cartridge/scripts/util/Response');

    if (request.httpParameterMap.format.stringValue !== 'ajax') {
        // @FIXME we could also build an ajax guard?
        responseUtils.renderJSON({});
    } else {
        responseUtils.renderJSON({
            status: giftCertStatus.code,
            success: !giftCertStatus.error,
            message: Resource.msgf('billing.' + giftCertStatus.code, 'checkout', null, giftCertCode),
            code: giftCertCode
        });
    }
}

/**
 * Removes gift certificate from the basket payment instruments and
 * generates a JSON response with a status. This function is called by an Ajax
 * request.
 */
function removeGiftCertificate() {
    if (!empty(request.httpParameterMap.giftCertificateID.stringValue)) {
        var cart = app.getModel('Cart').get();

        Transaction.wrap(function () {
            cart.removeGiftCertificatePaymentInstrument(request.httpParameterMap.giftCertificateID.stringValue);
            cart.calculate();
        });
    }

    publicStart();
}

/**
 * Updates the order totals and recalculates the basket after a coupon code is applied.
 * Renders the checkout/minisummary template, which includes the mini cart order totals and shipment summary.
 */
function updateSummary() {

    var cart = app.getModel('Cart').get();
    var zipCode = session.forms.singleshipping.shippingAddress.addressFields.postal.value;
	var isRestrictedCheckout = mattressPipeletHelper.getIsRestrictedCheckout(zipCode);
	
	var productsGeoAvailabilityMap = UtilFunctionsMF.buildProductsGeoAvailabilityMap(cart.object);
	var allProductsGeoAvailable = UtilFunctionsMF.checkAllProductsGeoAvailable(cart.object,productsGeoAvailabilityMap);
	
	var geolocationZip = request.getGeolocation().getPostalCode();
    var postalCode = empty(session.custom.customerZip) ? geolocationZip : session.custom.customerZip;
   
    var defaultDeliveryTier = "";
	if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
		var inMarketShipment = cart.object.getShipment('In-Market');
		if(!empty(inMarketShipment)) {
			defaultDeliveryTier = inMarketShipment.getShippingMethodID();
		}
	}

    if(!empty(postalCode)){
		session.custom.customerZip = postalCode;
		COShippingMethodController.CalculateShippingAndTaxForZip(postalCode);
    }

    if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
		//Retain silver/gold/Platinum if user already selected one of them
		defaultDeliveryTier = COShippingMethodController.SetDeliveryTierShippingMethod(defaultDeliveryTier);
	}


    Transaction.wrap(function () {
        cart.calculate();
    });

    app.getView({
        checkoutstep: 4,
        Basket: cart.object,
        isRestrictedCheckout: isRestrictedCheckout.status,
        AllProductsGeoAvailable: allProductsGeoAvailable
    }).render('checkout/minisummary');
}

/**
 * Renders a form dialog to edit an address. The dialog is supposed to be opened
 * by an Ajax request and ends in templates, which trigger a certain JavaScript
 * event. The calling page of this dialog is responsible for handling these
 * events.
 */
function editAddress() {

    app.getForm('billing').objectaddress.clearFormElement();

    var address = customer.getAddressBook().getAddress(request.httpParameterMap.addressID.stringValue);

    if (address) {
        app.getForm('billinaddress').copyFrom(address);
        app.getForm('billingaggdress.states').copyFrom(address);
    }

    app.getView({
        ContinueURL: URLUtils.https('COBilling-EditBillingAddress')
    }).render('checkout/billing/billingaddressdetails');
}

/**
 * Form handler for the returnToForm form.
 * - __apply __ - attempts to save billing address information to the platform. If there is an error, renders the
 * components/dialog/dialogapply template. If it is successful, sets the ContinueURL to {@link module:controllers/COBilling~EditBillingAddress|EditBillingAddress} and renders the
 * checkout/billing/billingaddressdetails template.
 * - __remove __ - Checks if the customer owns any product lists. If they do not, removes the address from the customer address book
 * and renders the components/dialog/dialogdelete template.
 * If they do own product lists, sets the ContinueURL to {@link module:controllers/COBilling~EditBillingAddress|EditBillingAddress} and renders the checkout/billing/billingaddressdetails template.
 */
function editBillingAddress() {

    app.getForm('returnToForm').handleAction({
        apply: function () {
            if (!app.getForm('billingaddress').copyTo(app.getForm('billingaddress').object)) {
                app.getView({
                    ContinueURL: URLUtils.https('COBilling-EditBillingAddress')
                }).render('checkout/billing/billingaddressdetails');
            } else {
                app.getView().render('components/dialog/dialogapply');
            }
        },
        remove: function () {
            if (ProductListMgr.getProductLists(app.getForm('billing').objectaddress.object).isEmpty()) {
                customer.getAddressBook().removeAddress(app.getForm('billing').objectaddress.object);
                app.getView().render('components/dialog/dialogdelete');
            } else {
                app.getView({
                    ContinueURL: URLUtils.https('COBilling-EditBillingAddress')
                }).render('checkout/billing/billingaddressdetails');
            }
        }
    });
}

/**
 * Returns information of a gift certificate including its balance as JSON
 * response. Required to check the remaining balance.
 */
function getGiftCertificateBalance() {
    var giftCertificate = GiftCertificateMgr.getGiftCertificateByCode(request.httpParameterMap.giftCertificateID.value);
    var responseUtils = require('app_storefront_controllers/cartridge/scripts/util/Response');

    if (giftCertificate && giftCertificate.isEnabled()) {
        responseUtils.renderJSON({
            giftCertificate: {
                ID: giftCertificate.getGiftCertificateCode(),
                balance: StringUtils.formatMoney(giftCertificate.getBalance())
            }
        });
    } else {
        responseUtils.renderJSON({
            error: Resource.msg('billing.giftcertinvalid', 'checkout', null)
        });
    }
}

/**
 * Selects a customer credit card and returns the details of the credit card as
 * JSON response. Required to fill credit card form with details of selected
 * credit card.
 */
function selectCreditCard() {
    var cart, applicableCreditCards, selectedCreditCard, instrumentsIter, creditCardInstrument;
    cart = app.getModel('Cart').get();

    applicableCreditCards = initCreditCardList(cart).ApplicableCreditCards;
    selectedCreditCard = null;

    // ensure mandatory parameter 'CreditCardUUID' and 'CustomerPaymentInstruments'
    // in pipeline dictionary and collection is not empty
    if (request.httpParameterMap.creditCardUUID.value && applicableCreditCards && !applicableCreditCards.empty) {

        // find credit card in payment instruments
        instrumentsIter = applicableCreditCards.iterator();
        while (instrumentsIter.hasNext()) {
            creditCardInstrument = instrumentsIter.next();
            if (request.httpParameterMap.creditCardUUID.value.equals(creditCardInstrument.UUID)) {
                selectedCreditCard = creditCardInstrument;
            }
        }

        if (selectedCreditCard) {
            app.getForm('billing').object.paymentMethods.creditCard.number.value = selectedCreditCard.getCreditCardNumber();
        }
    }

    app.getView({
        SelectedCreditCard: selectedCreditCard
    }).render('checkout/billing/creditcardjson');
}

/**
 * Revalidates existing payment instruments in later checkout steps.
 *
 * @param {module:models/CartModel~CartModel} cart - A CartModel wrapping the current Basket.
 * @return {Boolean} true if existing payment instruments are valid, false if not.
 */
function validatePayment(cart) {
    var paymentAmount, countryCode, invalidPaymentInstruments, result;
    if (app.getForm('billing').object.fulfilled.value) {
        paymentAmount = cart.getNonGiftCertificateAmount();
        countryCode = Countries.getCurrent({
            CurrentRequest: {
                locale: request.locale
            }
        }).countryCode;

        invalidPaymentInstruments = cart.validatePaymentInstruments(customer, countryCode, paymentAmount.value).InvalidPaymentInstruments;

        // Needed to wrap this with a transaction because of errors with calculatePaymentTransactionTotal
        Transaction.wrap( function () {
        if (!invalidPaymentInstruments && cart.calculatePaymentTransactionTotal()) {
            result = true;
        } else {
            app.getForm('billing').object.fulfilled.value = false;
            result = false;
        }
        });

    } else {
        result = false;
    }
    return result;
}

/**
 * Attempts to save the used credit card in the customer payment instruments.
 * The logic replaces an old saved credit card with the same masked credit card
 * number of the same card type with the new credit card. This ensures creating
 * only unique cards as well as replacing expired cards.
 * @transactional
 * @return {Boolean} true if credit card is successfully saved.
 */
function saveCreditCard() {
    var i, creditCards, newCreditCard;

    if (customer.authenticated && app.getForm('billing').object.paymentMethods.creditCard.saveCard.value) {
        creditCards = customer.getProfile().getWallet().getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);

        Transaction.wrap(function () {
            newCreditCard = customer.getProfile().getWallet().createPaymentInstrument(PaymentInstrument.METHOD_CREDIT_CARD);

            // copy the credit card details to the payment instrument
            newCreditCard.setCreditCardHolder(app.getForm('billing').object.paymentMethods.creditCard.owner.value);
            newCreditCard.setCreditCardNumber(app.getForm('billing').object.paymentMethods.creditCard.number.value);
            newCreditCard.setCreditCardExpirationMonth(app.getForm('billing').object.paymentMethods.creditCard.expiration.month.value);
            newCreditCard.setCreditCardExpirationYear(app.getForm('billing').object.paymentMethods.creditCard.expiration.year.value);
            newCreditCard.setCreditCardType(app.getForm('billing').object.paymentMethods.creditCard.type.value);

            for (i = 0; i < creditCards.length; i++) {
                var creditcard = creditCards[i];

                if (creditcard.maskedCreditCardNumber === newCreditCard.maskedCreditCardNumber && creditcard.creditCardType === newCreditCard.creditCardType) {
                    customer.getProfile().getWallet().removePaymentInstrument(creditcard);
                }
            }
        });

    }
    return true;
}

function resetDeliverySummary() {
	var cart = app.getModel('Cart').get();
	var basket = cart.object;
	var isRestrictedCheckout = mattressPipeletHelper.getIsRestrictedCheckout(session.custom.customerZip);
	 var isOnlyRedCarpetItemsInCart = !empty(cart.object) ? mattressPipeletHelper.getOrderIsRedCarpetStatus(cart.object) : false;
	   
	if (!empty(cart.getShipments())) {
		// to do remove shipments
		var allShipments= cart.getShipments().iterator();
		 while (allShipments.hasNext()) {
			var shipment =allShipments.next();
			if (shipment.ID != "storePickup" && (shipment.custom.shipmentType == 'In-Market')) {
				if (!empty(shipment.custom.deliveryDate) || shipment.custom.deliveryDate != null ) {
					Transaction.wrap(function () {
						shipment.custom.deliveryDate = null;
						shipment.custom.deliveryTime = null;
						session.custom.deliveryDate = null;
						session.custom.deliveryTime = null;
						var pliIterator = shipment.getProductLineItems().iterator();
						while (pliIterator.hasNext()) {
							var pli = pliIterator.next();
							pli.custom.deliveryDate = null;
							pli.custom.InventLocationId = null;
							pli.custom.secondaryWareHouse = null;
							pli.custom.mfiDSZoneLineId = null;
							pli.custom.mfiDSZipCode = null;
							session.custom.deliveryZone = null;
							var optionLineItems = pli.optionProductLineItems;
							for (var j = 0; j < optionLineItems.length; j++) {
								var oli = optionLineItems[j];
								if (oli.productID != 'none') {
									oli.custom.InventLocationId = null;
								}
							}
						}
				    });
				}				
			}
		 }
		var defaultDeliveryTier = "";
		if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
			var inMarketShipment = cart.object.getShipment('In-Market');
			if(!empty(inMarketShipment)) {
				defaultDeliveryTier = inMarketShipment.getShippingMethodID();
			}
		}

		COShippingMethodController.CalculateShippingAndTaxForZip(session.custom.customerZip);
		if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
			   //Retain silver/gold/Platinum if user already selected one of them
		      defaultDeliveryTier = COShippingMethodController.SetDeliveryTierShippingMethod(defaultDeliveryTier);
		}
	}
	var productsGeoAvailabilityMap = UtilFunctionsMF.buildProductsGeoAvailabilityMap(cart.object);
	var allProductsGeoAvailable = UtilFunctionsMF.checkAllProductsGeoAvailable(cart.object,productsGeoAvailabilityMap);

    app.getView({
        DeliveryZoneChanged: true,
        DeliveryDateSelected: false,
        isRestrictedCheckout: isRestrictedCheckout.status,
        AllProductsGeoAvailable : allProductsGeoAvailable,
        restrictedState: isRestrictedCheckout.restrictedState,
        Basket : basket,
        isOnlyRedCarpetItemsInCart: isOnlyRedCarpetItemsInCart
    }).render('checkout/components/opc-deliverysummary');
	
}

function updateDeliverySummary() {
	var cart = app.getModel('Cart').get();
	var basket = cart.object;
	var params = request.httpParameterMap;
	var deliveryDateSelected = false;
	var deliveryZoneChanged = false;
	var tempScheduleWithRepValue = '';
    var isOnlyRedCarpetItemsInCart = !empty(cart.object) ? mattressPipeletHelper.getOrderIsRedCarpetStatus(cart.object) : false;
    
    if (isOnlyRedCarpetItemsInCart) {
	    var deliveryDate = params.DeliveryDate.stringValue ? params.DeliveryDate.stringValue : 'null';
		var deliveryTime = params.DeliveryTime.stringValue ? params.DeliveryTime.stringValue : 'null';
		var deliveryZone = session.custom.deliveryZone ? session.custom.deliveryZone : 'null';
	    if(deliveryDate !='null' && deliveryTime !='null') {
	    	//2049-12-31T06:11:55.699Z|8:00am-8:00pm|DZL002446249|77025
			var addressAtpInfoAttr = mattressPipeletHelper.getISODate(deliveryDate) + '|' + deliveryTime +'|' + deliveryZone +'|'+ session.custom.customerZip;
			//Update with latest date and time, selected on checkout page
			Transaction.wrap(function () {
				cart.object.custom.addressesDeliveryDateTimeZoneZip = addressAtpInfoAttr;
			});
	    }
    }
	
	if (!params.zipCode.empty && params.zipCode.stringValue.length == 5) {
		var zipCode = params.zipCode.stringValue;
		var hasZIPchanged = session.custom.customerZip != zipCode;
		
		var isRestrictedCheckout = mattressPipeletHelper.getIsRestrictedCheckout(zipCode);
		
		 if (hasZIPchanged) {
		 	session.forms.singleshipping.shippingAddress.addressFields.postal.value = zipCode;
			deliveryZoneChanged = isDeliveryZoneChanged(zipCode);
		}
		if (deliveryZoneChanged) {
			// to do remove shipments
			var allShipments= cart.getShipments().iterator();
			 while (allShipments.hasNext()) {
				var shipment =allShipments.next();
				if (shipment.ID != "storePickup" && (shipment.custom.shipmentType == 'In-Market')) {
					if (!empty(shipment.custom.deliveryDate) || shipment.custom.deliveryDate != null ) {
						Transaction.wrap(function () {
							shipment.custom.deliveryDate = null;
							shipment.custom.deliveryTime = null;
							session.custom.deliveryDate = null;
							session.custom.deliveryTime = null;
							var pliIterator = shipment.getProductLineItems().iterator();
							while (pliIterator.hasNext()) {
								var pli = pliIterator.next();
								pli.custom.deliveryDate = null;
								pli.custom.InventLocationId = null;
								pli.custom.secondaryWareHouse = null;
								pli.custom.mfiDSZoneLineId = null;
								pli.custom.mfiDSZipCode = null;
								session.custom.deliveryZone = null;
								var optionLineItems = pli.optionProductLineItems;
								for (var j = 0; j < optionLineItems.length; j++) {
									var oli = optionLineItems[j];
									if (oli.productID != 'none') {
										oli.custom.InventLocationId = null;
									}
								}
							}
					    });
					}				
				}
			 }
			var defaultDeliveryTier = "";
			if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
				var inMarketShipment = cart.object.getShipment('In-Market');
				if(!empty(inMarketShipment)) {
					defaultDeliveryTier = inMarketShipment.getShippingMethodID();
				}
			}

			COShippingMethodController.CalculateShippingAndTaxForZip(session.custom.customerZip);
			if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
				   //Retain silver/gold/Platinum if user already selected one of them
			      defaultDeliveryTier = COShippingMethodController.SetDeliveryTierShippingMethod(defaultDeliveryTier);
			}
		}
		var productsGeoAvailabilityMap = UtilFunctionsMF.buildProductsGeoAvailabilityMap(cart.object);
		var allProductsGeoAvailable = UtilFunctionsMF.checkAllProductsGeoAvailable(cart.object,productsGeoAvailabilityMap);

	    app.getView({
	        DeliveryZoneChanged: deliveryZoneChanged,
	        DeliveryDateSelected: false,
	        isRestrictedCheckout: isRestrictedCheckout.status,
	        AllProductsGeoAvailable : allProductsGeoAvailable,
	        restrictedState: isRestrictedCheckout.restrictedState,
	        Basket : basket,
	        isOnlyRedCarpetItemsInCart: isOnlyRedCarpetItemsInCart
	    }).render('checkout/components/opc-deliverysummary');
	}
	
	
	if(!params.deliveryDateSelected.empty) {
		deliveryDateSelected = params.deliveryDateSelected;
		var deliveryDate = params.DeliveryDate.stringValue;
		var deliveryTime = params.DeliveryTime.stringValue;
		app.getForm('singleshipping').object.shippingAddress.addressFields.deliveryDate.value = deliveryDate;
		app.getForm('singleshipping').object.shippingAddress.addressFields.deliveryTime.value = deliveryTime;
	    // set the value when user select the delivery slot on the billing page(in case dont get slots on delivery info) and then remove value at the end of the function.
	    if (params.DeliverSchdulerValue.submitted && !params.DeliverSchdulerValue.empty && params.DeliverSchdulerValue.stringValue === 'scheduledelivery') {
	    	tempScheduleWithRepValue = session.forms.singleshipping.shippingAddress.scheduleWithRep.value;
	    	session.forms.singleshipping.shippingAddress.scheduleWithRep.value = params.DeliverSchdulerValue.stringValue;
	    }
		 var COShippingController = require('~/cartridge/controllers/COShipping');
		 COShippingController.PrepareShipments();
		 COShippingController.UpdateDeliveryDateSelection(cart);
		 // then make session form value empty.
		if (params.DeliverSchdulerValue.submitted && !params.DeliverSchdulerValue.empty && params.DeliverSchdulerValue.stringValue === 'scheduledelivery') {
	    	session.forms.singleshipping.shippingAddress.scheduleWithRep.value = tempScheduleWithRepValue;
	    }
		var defaultDeliveryTier = "";
		if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
			var inMarketShipment = cart.object.getShipment('In-Market');
			if(!empty(inMarketShipment)) {
				defaultDeliveryTier = inMarketShipment.getShippingMethodID();
			}
		}

		COShippingMethodController.CalculateShippingAndTaxForZip(session.custom.customerZip);

		if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
			//Retain silver/gold/Platinum if user already selected one of them
		    defaultDeliveryTier = COShippingMethodController.SetDeliveryTierShippingMethod(defaultDeliveryTier);
		}
		 var productsGeoAvailabilityMap = UtilFunctionsMF.buildProductsGeoAvailabilityMap(cart.object);
		 var allProductsGeoAvailable = UtilFunctionsMF.checkAllProductsGeoAvailable(cart.object,productsGeoAvailabilityMap);
		 app.getView({
			 DeliveryZoneChanged: deliveryZoneChanged,
			 DeliveryDateSelected: deliveryDateSelected,
		     Basket : basket,
		     AllProductsGeoAvailable : allProductsGeoAvailable,
		     isOnlyRedCarpetItemsInCart: isOnlyRedCarpetItemsInCart
		 }).render('checkout/components/opc-deliverysummary');	 
	}
}

// Verify ATP delivery zone
function isDeliveryZoneChanged(zipCode) {
	
	var deliveryDate = session.custom.deliveryDate;
	var deliveryTime = session.custom.deliveryTime;
	var deliveryZone = session.custom.deliveryZone;
	
	var isZoneChanged = false;
	//verify shipping match with delivery zip or not  
	var hasZIPchanged = session.custom.customerZip != zipCode;
	
	var cart = app.getModel('Cart').get();
	var defaultDeliveryTier = "";
	if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
		var inMarketShipment = cart.object.getShipment('In-Market');
		if(!empty(inMarketShipment)) {
			defaultDeliveryTier = inMarketShipment.getShippingMethodID();
		}
	}

	COShippingMethodController.CalculateShippingAndTaxForZip(zipCode);

	if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
		//Retain silver/gold/Platinum if user already selected one of them
	    defaultDeliveryTier = COShippingMethodController.SetDeliveryTierShippingMethod(defaultDeliveryTier);
	}
	
	if (hasZIPchanged) {                    
		session.custom.customerZip = zipCode;
		isZoneChanged = true;	
		var cart = app.getModel('Cart').get();	
		var deliveryDatesArr = null;
		var StorePicker = require('app_storefront_controllers/cartridge/controllers/StorePicker');
		if (deliveryDate != 'null' && !empty(deliveryDate)) {
			
			if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule') ) {	
				deliveryDatesArr = StorePicker.GetDeliveryZone(cart, zipCode, deliveryDate);
			}	
			if (!empty(deliveryDatesArr)) {			
				
				var key = new Date(deliveryDatesArr[0]).toDateString().replace(' ', '', 'g');
				var deliveryTimeSlots = session.custom.deliveryDates[key]['timeslots'];
				for (var j = 0; j < deliveryTimeSlots.length; j++) {    						
					if (deliveryTimeSlots[j]['available'] == 'yes' && deliveryTimeSlots[j]['slots'] > 0) {
						var startTime = deliveryTimeSlots[j]['startTime'];
						var endTime = deliveryTimeSlots[j]['endTime'];
						var slot = StringUtils.formatDate(startTime, "h:mma").toString() +'-' + StringUtils.formatDate(endTime, "h:mma").toString();
						if ((deliveryTime.toLowerCase().replace(' ', '', 'g') == slot.toLowerCase()) && (deliveryZone == deliveryTimeSlots[j]['mfiDSZoneLineId'])) {						
							isZoneChanged = false;
							break;
						}			
					}
				}				
			}
			
		} 
	}
	if (isZoneChanged) {
		session.custom.deliveryDate = null;
		session.custom.deliveryTime = null;
		session.custom.deliveryZone = null;
	}
	return isZoneChanged;
}

//Verify ATP delivery zone
function verifyDeliveryZone() {
	var params = request.httpParameterMap;	
	var zipCode = params.zipCode.stringValue;
	var format = params.format.stringValue;
	var deliveryDate = session.custom.deliveryDate;
	var deliveryTime = session.custom.deliveryTime;
	var deliveryZone = session.custom.deliveryZone;
	
	var result = false;
	//verify shipping match with delivery zip or not  
	var hasZIPchanged = session.custom.customerZip != zipCode;
	
	if (hasZIPchanged) {                    
		session.custom.customerZip = zipCode;
		result = true;
		var cart = app.getModel('Cart').get();	
		var deliveryDatesArr = null;
		var StorePicker = require('app_storefront_controllers/cartridge/controllers/StorePicker');
		if (deliveryDate != 'null' && !empty(deliveryDate)) {
			
			if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule') ) {	
				deliveryDatesArr = StorePicker.GetDeliveryZone(cart, zipCode, deliveryDate);
			}	
			if (!empty(deliveryDatesArr)) {			
				
				var key = new Date(deliveryDatesArr[0]).toDateString().replace(' ', '', 'g');
				var deliveryTimeSlots = session.custom.deliveryDates[key]['timeslots'];
				for (var j = 0; j < deliveryTimeSlots.length; j++) {    						
					if (deliveryTimeSlots[j]['available'] == 'yes' && deliveryTimeSlots[j]['slots'] > 0) {
						var startTime = deliveryTimeSlots[j]['startTime'];
						var endTime = deliveryTimeSlots[j]['endTime'];
						var slot = StringUtils.formatDate(startTime, "h:mma").toString() +'-' + StringUtils.formatDate(endTime, "h:mma").toString();
						if ((deliveryTime.toLowerCase().replace(' ', '', 'g') == slot.toLowerCase()) && (deliveryZone == deliveryTimeSlots[j]['mfiDSZoneLineId'])) {						
							result = false;
							break;
						}			
					}
				}				
			}
			
		}	
	}
	if (result) {
		session.custom.deliveryDate = null;
		session.custom.deliveryTime = null;
		session.custom.deliveryZone = null;
	}
	app.getView({
        JSONData: result
    }).render('util/output');
	 
}

/**
 * Validates the billing address.
 * @returns {boolean} Returns true if the address is valid. Returns false if the address is invalid.
 */
function validateBillingAddress() {
	var isValidAddress = true, isValidBillingCity = false, isValidBillingState = false, isValidBillingZipCode = false;
 
    // validate billing address
	try {	
	
	    var billingZipCode = app.getForm('billing.billingAddress.addressFields.postal').value();
	    var citiesAndStateJSON = mattressPipeletHelper.getCitiesAndStatesForZipCode(billingZipCode);
	    
		if (!empty(citiesAndStateJSON)) {
			var cityData = JSON.parse(citiesAndStateJSON);   
		    for (var i = 0; i < cityData.cities.length; i++){
		        var data = cityData.cities[i];
		        if (app.getForm('billing.billingAddress.addressFields.city').value().toLowerCase() === data.city.toLowerCase()) {
		        	isValidBillingCity = true;
				}
		        if (app.getForm('billing.billingAddress.addressFields.states.state').value().toLowerCase() === data.state.toLowerCase()) {
		        	isValidBillingState = true;
				}      
		    }
		    isValidBillingZipCode = true;
	    }
		
	
		if (!isValidBillingZipCode) {
	       app.getForm('billing.billingAddress.addressFields.postal').invalidate();
	        isValidBillingCity = true;
	        isValidBillingState = true;
	        isValidAddress = false;
	    }
		
	    if (!isValidBillingCity) {
	    	app.getForm('billing.billingAddress.addressFields.city').invalidate();
	        isValidAddress = false;
	    }
	    
	    if (!isValidBillingState) {
	    	app.getForm('billing.billingAddress.addressFields.states.state').invalidate();
	        isValidAddress = false;     
	    }
	} 
	catch(e) {
	    Logger.error('Billing address validation failed' + e.toString());
	}
	
    return isValidAddress;
}

/**
 * Validates the shipping address.
 * @returns {boolean} Returns true if the address is valid. Returns false if the address is invalid.
 */
function validateShippingAddress() {
	var isValidAddress = true, 
		isValidShippingCity = false, 
		isValidShippingState = false, 
		isValidShippingZipCode = false;
		
	// validate shipping address
	try {	
		var shippingZipCode = app.getForm('singleshipping.shippingAddress.addressFields.postal').value();
		var citiesAndStateJSON = mattressPipeletHelper.getCitiesAndStatesForZipCode(shippingZipCode);
		if (!empty(citiesAndStateJSON)) {
			var cityData = JSON.parse(citiesAndStateJSON);   
	        for (var i = 0; i < cityData.cities.length; i++){
	            var data = cityData.cities[i];
	            var cityVal = app.getForm('singleshipping.shippingAddress.addressFields.city').value();
	            var stateVal = app.getForm('singleshipping.shippingAddress.addressFields.states.state').value();
	            if (app.getForm('singleshipping.shippingAddress.addressFields.city').value().toLowerCase() === data.city.toLowerCase()) {
	            	isValidShippingCity = true;
	    		}
	            if (app.getForm('singleshipping.shippingAddress.addressFields.states.state').value().toLowerCase() === data.state.toLowerCase()) {
	            	isValidShippingState = true;
	    		}      
	        }          
	        isValidShippingZipCode = true;
		}
		
		if (!isValidShippingZipCode) {
	        app.getForm('singleshipping.shippingAddress.addressFields.postal').invalidate();
	        isValidShippingCity = true;
	        isValidShippingState = true;
	        isValidAddress = false;
	    }
		
		if (!isValidShippingCity) {
	        app.getForm('singleshipping.shippingAddress.addressFields.city').invalidate();
	        isValidAddress = false;
	    }
	    
	    if (!isValidShippingState) {
	        app.getForm('singleshipping.shippingAddress.addressFields.states.state').invalidate();
	        isValidAddress = false;
	    }    
	} 
	catch(e) {
	    Logger.error('Shipping address validation failed' + e.toString());
	}
	
    return isValidAddress;
}


//Capture checkout email 
function captureCheckoutEmail(){    
  
  var params = request.httpParameterMap;    
  var emailAddress = params.emailAddress.stringValue;    
  var optFlag = params.optFlag.stringValue;
  var zipCode = session.custom.customerZip;
  
  var gaCookie = request.getHttpCookies()['_ga'];
  var gclid;
  if (gaCookie) {
      gclid = gaCookie.value;
  }
  
  var emailParams = {
          emailAddress: emailAddress, 
          zipCode: zipCode, 
          leadSource: 'checkout', 
          siteId: dw.system.Site.getCurrent().getID(), 
          optOutFlag: !optFlag,
          gclid: gclid,
          dwsid: session.sessionID
  };
  
  var returnResult = emailHelper.sendSFEmailInfo(emailParams);
  if (returnResult.Status == 'SERVICE_ERROR'){
      var returnResult = emailHelper.sendFailSafe(emailParams, returnResult.ErrorCode);
  }
  session.custom.returnResult1=returnResult.Status;
  app.getView({
   JSONData: returnResult.Status
}).render('util/output');
  
}

function amazonOrderTimeout() {
	var cart = app.getModel('Cart').get();
	var basket = cart.object;
	app.getView({
	     Basket : basket
	 }).render('checkout/amazontimeout');
	
}

function continueToBilling() {
	var selectedDeliveryDate = mattressPipeletHelper.getISODate(session.forms.singleshipping.shippingAddress.addressFields.deliveryDate.htmlValue);
	var selectedDeliveryTime = session.forms.singleshipping.shippingAddress.addressFields.deliveryTime.htmlValue; 
	var cart = app.getModel('Cart').get();
	session.custom.checkoutStep = 'payment';
    if (!empty(cart)) {
    	if (dw.system.Site.getCurrent().getCustomPreferenceValue('synchronyEnabled') ) {
    		resetPaymentForms();
    		var Synchrony = require('*/cartridge/controllers/CODigitalBuy');
    		Synchrony.preFilledSynchronyDigitalBuyForm();
    	}
    	var payWithAmazon = false;
    	if (apController.SetCheckoutMethod()) {
    		payWithAmazon = true;
    	}
    	if(!payWithAmazon && !empty(cart.getPaymentInstruments('PayPal'))){
    		resetPaymentForms();
    	    session.forms.singleshipping.clearFormElement();
    	    session.forms.multishipping.clearFormElement();
    	    session.forms.billing.clearFormElement();
    		session.forms.billing.paymentMethods.selectedPaymentMethodID.value = 'CREDIT_CARD';
    		session.forms.singleshipping.shippingAddress.useAsBillingAddress.value = true;
    	}
    	session.forms.cart.coupons.copyFrom(cart.object.couponLineItems);

        // Initializes all forms of the billing page including: - address form - email address - coupon form
        //initAddressForm(cart);
//        Synchrony.fillForm();
        initEmailAddress(cart);

        var creditCardList = initCreditCardList(cart);
        var applicablePaymentMethods = creditCardList.ApplicablePaymentMethods;

        var billingForm = app.getForm('billing').object;
        var paymentMethods = billingForm.paymentMethods;
        if (paymentMethods.valid) {
            paymentMethods.selectedPaymentMethodID.setOptions(applicablePaymentMethods.iterator());
        } else {
            //paymentMethods.clearFormElement();
        }

        app.getForm('billing.couponCode').clear();
        app.getForm('billing.giftCertCode').clear();

        startMobileAB(cart, {ApplicableCreditCards: creditCardList.ApplicableCreditCards});
    } else {
    	response.redirect(URLUtils.https('Cart-Show'));
        return;
    }

}


/*
* Module exports
*/

/*
* Web exposed methods
*/
/** Starting point for billing.
 * @see module:controllers/COBilling~publicStart */
exports.Start = guard.ensure(['https'], publicStart);
/** Redeems gift certificates.
 * @see module:controllers/COBilling~redeemGiftCertificateJson */
exports.RedeemGiftCertificateJson = guard.ensure(['https', 'get'], redeemGiftCertificateJson);
/** Removes gift certificate from the basket payment instruments.
 * @see module:controllers/COBilling~removeGiftCertificate */
exports.RemoveGiftCertificate = guard.ensure(['https', 'get'], removeGiftCertificate);
/** Updates the order totals and recalculates the basket.
 * @see module:controllers/COBilling~updateSummary */
exports.UpdateSummary = guard.ensure(['https', 'get'], updateSummary);
/** Updates the order totals and recalculates the basket.
 * @see module:controllers/COBilling~updateOPCdeliverySummarySection */
exports.UpdateDeliverySummary = guard.ensure(['https', 'get'], updateDeliverySummary);
/** Gets the customer address and saves it to the customer address book.
 * @see module:controllers/COBilling~updateAddressDetails */
exports.UpdateAddressDetails = guard.ensure(['https', 'get'], updateAddressDetails);
/** Renders a form dialog to edit an address.
 * @see module:controllers/COBilling~editAddress */
exports.EditAddress = guard.ensure(['https', 'get'], editAddress);
/** Returns information of a gift certificate including its balance as JSON response.
 * @see module:controllers/COBilling~getGiftCertificateBalance */
exports.GetGiftCertificateBalance = guard.ensure(['https', 'get'], getGiftCertificateBalance);
/** Selects a customer credit card and returns the details of the credit card as JSON response.
 * @see module:controllers/COBilling~selectCreditCard */
exports.SelectCreditCard = guard.ensure(['https', 'get'], selectCreditCard);
/** Adds the currently selected credit card to the billing form and initializes the credit card selection list.
 * @see module:controllers/COBilling~updateCreditCardSelection */
exports.UpdateCreditCardSelection = guard.ensure(['https', 'get'], updateCreditCardSelection);
/** Form handler for the billing form.
 * @see module:controllers/COBilling~billing */
exports.Billing = guard.ensure(['https'], billing);
/** Form handler for the returnToForm form.
 * @see module:controllers/COBilling~editBillingAddress */
exports.EditBillingAddress = guard.ensure(['https', 'post'], editBillingAddress);
/** Form handler for the singleshipping form.
 * @see module:controllers/COBilling~handleCouponCheckout */
exports.HandleCouponCheckout = guard.ensure(['https', 'post'], handleCouponCheckout);

exports.CaptureCheckoutEmail = guard.ensure(['https', 'post'], captureCheckoutEmail);

/*
 * Local methods
 */
/** Saves the credit card used in the billing form in the customer payment instruments.
 * @see module:controllers/COBilling~saveCreditCard */
exports.SaveCreditCard = saveCreditCard;
/** Revalidates existing payment instruments in later checkout steps.
 * @see module:controllers/COBilling~validatePayment */
exports.ValidatePayment = validatePayment;
/** Handles the selection of the payment method and performs payment method specific validation and verification upon the entered form fields.
 * @see module:controllers/COBilling~handlePaymentSelection */
exports.HandlePaymentSelection = handlePaymentSelection;

exports.ResetPaymentForms = resetPaymentForms;

exports.ReturnToForm = returnToForm;

exports.handleBillingAddress = handleBillingAddress;

exports.VerifyDeliveryZone = guard.ensure(['https', 'get'], verifyDeliveryZone);

exports.AmazonOrderTimeout = guard.ensure(['https'], amazonOrderTimeout);

exports.ContinueToBilling = guard.ensure(['https'], continueToBilling);

exports.SetPaymentMethod = guard.ensure(['https', 'get'], setPaymentMethod);

exports.ValidateBillingAddress = guard.ensure(['https'], validateBillingAddress);

exports.IsDeliveryZoneChanged = guard.ensure(['https'], isDeliveryZoneChanged);

exports.ResetDeliverySummary = guard.ensure(['https', 'get'], resetDeliverySummary);
