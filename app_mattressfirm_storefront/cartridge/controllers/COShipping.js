'use strict';

/**
 * Controller for the default single shipping scenario.
 * Single shipping allows only one shipment, shipping address, and shipping method per order.
 *
 * @module controllers/COShipping
 */

/* API Includes */
var CustomerMgr = require('dw/customer/CustomerMgr');
var HashMap = require('dw/util/HashMap');
var Resource = require('dw/web/Resource');
var ShippingMgr = require('dw/order/ShippingMgr');
var Site = require('dw/system/Site');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');
var StoreMgr = require('dw/catalog/StoreMgr');
var StringUtils = require('dw/util/StringUtils');

/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var pipeletHelper = require('bc_sleepysc/cartridge/scripts/sleepys/util/SleepysPipeletsHelper').SleepysPipeletHelper;
var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;
var emailHelper = require("app_storefront_controllers/cartridge/scripts/util/SFEmailSubscriptionHelper").SFEmailSubscriptionHelper;
var storePicker = require('app_storefront_controllers/cartridge/controllers/StorePicker');
var StringHelpers = require('app_storefront_core/cartridge/scripts/util/StringHelpers');
var UtilFunctions = require('~/cartridge/scripts/util/UtilFunctions').UtilFunctions;
var apController = require('int_amazonpayments/cartridge/controllers/AmazonPayments');
var apModule = require('int_amazonpayments/cartridge/scripts/AmazonPaymentsModule');
var COShippingMethodController = require('app_mattressfirm_storefront/cartridge/controllers/COShippingMethod');

/**
 * Prepares shipments. Theis function separates gift certificate line items from product
 * line items. It creates one shipment per gift certificate purchase
 * and removes empty shipments. If in-store pickup is enabled, it combines the
 * items for in-store pickup and removes them.
 * This function can be called by any checkout step to prepare shipments.
 *
 * @transactional
 * @return {Boolean} true if shipments are successfully prepared, false if they are not.
 */
function prepareShipments() {
    var cart, homeDeliveries;
    cart = app.getModel('Cart').get();

    homeDeliveries = Transaction.wrap(function () {

        homeDeliveries = false;
        cart.updateGiftCertificateShipments();
        cart.removeEmptyShipments();

        if (Site.getCurrent().getCustomPreferenceValue('enableStorePickUp') && session.custom.deliveryOptionChoice == 'instorepickup') {
            homeDeliveries = cart.consolidateInStoreShipments();
            session.forms.singleshipping.inStoreShipments.shipments.clearFormElement();
            app.getForm(session.forms.singleshipping.inStoreShipments.shipments).copyFrom(cart.getShipments());

        } else if (session.custom.wasbopis > 0 && session.custom.deliveryOptionChoice == 'shipped') {
            session.forms.singleshipping.clearFormElement();
            session.custom.wasbopis = 0;
            homeDeliveries = true;
            // clean up bopis shipments and pli's
            app.getController('Cart').ChangeDeliveryOptionInternal('shipped');
          	mattressPipeletHelper.splitShipmentsByType(cart);
          	cart.removeEmptyShipments();

        } else {
            homeDeliveries = true;
        }
        
        return homeDeliveries;
    });

    return homeDeliveries;
}

function updateDeliveryDateSelection(cart) {
	var deliveryDate = mattressPipeletHelper.getISODate(session.forms.singleshipping.shippingAddress.addressFields.deliveryDate.htmlValue);
	var deliveryTime = session.forms.singleshipping.shippingAddress.addressFields.deliveryTime.htmlValue;
    var inMarketShipment = cart.getShipment('In-Market');
    var isAtpDeliveryScheduleEnabled = false;
    if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule')) {
    	isAtpDeliveryScheduleEnabled = true;
    } else if(session.forms.singleshipping.shippingAddress.scheduleWithRep.value == 'scheduledelivery') {
    	deliveryTime = false;
    }
    if (deliveryDate && inMarketShipment && !isAtpDeliveryScheduleEnabled) {
        Transaction.wrap(function () {
            
            inMarketShipment.custom.deliveryDate = deliveryDate;
            var pliIterator = inMarketShipment.getProductLineItems().iterator();
            while (pliIterator.hasNext()) {
                var pli = pliIterator.next();
                pli.custom.deliveryDate = deliveryDate;
            }
            
        });
    } else if (isAtpDeliveryScheduleEnabled && session.forms.singleshipping.shippingAddress.scheduleWithRep.value == 'contactme') {
    	var preferredcontact = session.forms.singleshipping.shippingAddress.preferredcontacts.preferredcontact.htmlValue;
	    var shipItr = cart.getShipments().iterator();
	    try {
	    	Transaction.wrap(function () {
				while (shipItr.hasNext()) {
					var shipment = shipItr.next();
					shipment.getShippingAddress().custom.preferred_contact_method = preferredcontact;
					shipment.custom.deliveryDate = null;
					shipment.custom.deliveryTime = null;
					session.custom.deliveryDate = null;
					session.custom.deliveryTime = null;
					var pliIterator = shipment.getProductLineItems().iterator();
					while (pliIterator.hasNext()) {
						var pli = pliIterator.next();
						pli.custom.deliveryDate = null;
						pli.custom.InventLocationId = null;
						pli.custom.secondaryWareHouse = null;
						pli.custom.mfiDSZoneLineId = null;
						pli.custom.mfiDSZipCode = null;
						session.custom.deliveryZone = null;
						var optionLineItems = pli.optionProductLineItems;
						for (var j = 0; j < optionLineItems.length; j++) {
							var oli = optionLineItems[j];
							if (oli.productID != 'none') {
								oli.custom.InventLocationId = null;
							}
						}
					}
				}
	    	});
	    } catch (e) {
    		var error = e;
    	}
    } else if (isAtpDeliveryScheduleEnabled && session.forms.singleshipping.shippingAddress.scheduleWithRep.value == 'scheduledelivery') {
    	try {
    		    	    
    		Transaction.wrap(function () {
        		var shipItr = cart.getShipments().iterator();
        		var chicagoInventLocationId = UtilFunctions.chicagoInventLocationId();
                while (shipItr.hasNext()) {
                	var shipment = shipItr.next();
                	if (shipment.ID != "storePickup" && (shipment.custom.shipmentType == 'In-Market')) {
                		shipment.custom.deliveryDate = deliveryDate;
                		shipment.custom.deliveryTime = deliveryTime;
                		session.custom.deliveryDate = deliveryDate;
						session.custom.deliveryTime = deliveryTime;
						
    	                var pliIterator = shipment.getProductLineItems().iterator();
    	                var nonISODeliveryDate = deliveryDate.replace('-','/', 'g').split('T');
    	                 
    	            	while (pliIterator.hasNext()) {
    	            		var pli = pliIterator.next();
    	                	pli.custom.deliveryDate = deliveryDate;
    	                	if (shipment.custom.deliveryDate && dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule')) {
    	                		var key = new Date(deliveryDate).toDateString().replace(' ', '', 'g');
    	                		var deliveryTimeSlots = session.custom.deliveryDates[key]['timeslots'];
    	                		var deliveryTimeSlot = false;
    	                		for (var i = 0; i < deliveryTimeSlots.length; i++) {
    	                			var startTime = deliveryTimeSlots[i]['startTime'].toString();
    	                			var endTime = deliveryTimeSlots[i]['endTime'].toString();
    	                			var deliveryTimes = deliveryTime.replace('pm',' pm', 'g').replace('am',' am', 'g').split('-');
    	                			var deliveryStartTime = nonISODeliveryDate[0] + ' ' + deliveryTimes[0];
    	                			var deliveryEndTime = nonISODeliveryDate[0] + ' ' + deliveryTimes[1];
    	                			deliveryStartTime = new Date(deliveryStartTime).toString();
    	                			deliveryEndTime = new Date(deliveryEndTime).toString();
    	                			if (startTime == deliveryStartTime && endTime == deliveryEndTime) {
    	                				deliveryTimeSlot = i;
    	                				break;
    	                			}
    	                		}
    	                		
    	                		if (deliveryTimeSlot !== false) {
    	                			pli.custom.InventLocationId = session.custom.deliveryDates[key]['timeslots'][deliveryTimeSlot]['location1'];
        	            			pli.custom.secondaryWareHouse = session.custom.deliveryDates[key]['timeslots'][deliveryTimeSlot]['location2'];
        	            			pli.custom.mfiDSZoneLineId = session.custom.deliveryDates[key]['timeslots'][deliveryTimeSlot]['mfiDSZoneLineId'];
        	            			pli.custom.mfiDSZipCode = session.custom.deliveryDates[key]['timeslots'][deliveryTimeSlot]['mfiDSZipCode'];
        	            			session.custom.deliveryZone = session.custom.deliveryDates[key]['timeslots'][deliveryTimeSlot]['mfiDSZoneLineId'];
        	            			var optionLineItems = pli.optionProductLineItems;
        	            			for (var j = 0; j < optionLineItems.length; j++) {
        	            				var oli = optionLineItems[j];
        	            				if (oli.productID != 'none') {
        	            					oli.custom.InventLocationId = session.custom.deliveryDates[key]['timeslots'][deliveryTimeSlot]['location1'];
        	            				}
        	            			}
        	            			if (!empty(pli.product) && UtilFunctions.isParcelableAndCore(pli.product.manufacturerSKU) && !empty(chicagoInventLocationId)) {
        	            				pli.custom.InventLocationId = chicagoInventLocationId;
        	            			}
    	                		}
    	                	}
    	            	}
                	}
                }
            });
    	} catch (e) {
    		var error = e;
    	}
    }else if (isAtpDeliveryScheduleEnabled && empty(session.forms.singleshipping.shippingAddress.addressFields.deliveryDate.value)) {
    	
    	var shipItr = cart.getShipments().iterator();
	    try {
	    	Transaction.wrap(function () {
				while (shipItr.hasNext()) {
					var shipment = shipItr.next();
					
				    shipment.custom.deliveryDate = null;
					shipment.custom.deliveryTime = null;
					session.custom.deliveryDate = null;
					session.custom.deliveryTime = null;
					var pliIterator = shipment.getProductLineItems().iterator();
					while (pliIterator.hasNext()) {
						var pli = pliIterator.next();
						pli.custom.deliveryDate = null;
						pli.custom.InventLocationId = null;
						pli.custom.secondaryWareHouse = null;
						pli.custom.mfiDSZoneLineId = null;
						pli.custom.mfiDSZipCode = null;
						session.custom.deliveryZone = null;
						var optionLineItems = pli.optionProductLineItems;
						for (var j = 0; j < optionLineItems.length; j++) {
							var oli = optionLineItems[j];
							if (oli.productID != 'none') {
								oli.custom.InventLocationId = null;
							}
						}
					}
				}
	    	});
	    } catch (e) {
    		var error = e;
    	}
    }
}

/**
 * Starting point for the single shipping scenario. Prepares a shipment by removing gift certificate and in-store pickup line items from the shipment.
 * Redirects to multishipping scenario if more than one physical shipment is required and redirects to billing if all line items do not require
 * shipping.
 *
 * @transactional
 */
function start(amazonAuthError) {
    var cart, pageMeta, homeDeliveries;
    cart = app.getModel('Cart').get();
    
    if (session.custom.amazonGuestCheckout && !session.custom.closeBA) {
        apController.StoreOpenBillingAgreementID(cart);
    }

    if (cart) {
    	var payWithAmazon = false;
    	if (apController.SetCheckoutMethod()) {
    		payWithAmazon = true;
    	}
    	if(!payWithAmazon && !empty(cart.getPaymentInstruments('PayPal'))){
    		var COBilling = require('~/cartridge/controllers/COBilling');
            COBilling.ResetPaymentForms();
    	    session.forms.singleshipping.clearFormElement();
    	    session.forms.multishipping.clearFormElement();
    	    session.forms.billing.clearFormElement();
    		session.forms.billing.paymentMethods.selectedPaymentMethodID.value = 'CREDIT_CARD';
    		session.forms.singleshipping.shippingAddress.useAsBillingAddress.value = true;
    	}
        // Redirects to multishipping scenario if more than one physical shipment is contained in the basket.
        //physicalShipments = cart.getPhysicalShipments();
       // if (!(Site.getCurrent().getCustomPreferenceValue('enableMultiShipping') && physicalShipments && physicalShipments.size() > 1 )) {
       // Will use multi-ship for all orders as long as the site preference is set.
        if ( !(Site.getCurrent().getCustomPreferenceValue('enableMultiShipping')) ) {
            // Initializes the singleshipping form and prepopulates it with the shipping address of the default
            // shipment if the address exists, otherwise it preselects the default shipping method in the form.
            if (cart.getDefaultShipment().getShippingAddress()) {
                app.getForm(session.forms.singleshipping.shippingAddress.addressFields).copyFrom(cart.getDefaultShipment().getShippingAddress());
                app.getForm(session.forms.singleshipping.shippingAddress.addressFields.states).copyFrom(cart.getDefaultShipment().getShippingAddress());
                app.getForm(session.forms.singleshipping.shippingAddress).copyFrom(cart.getDefaultShipment());
            } else {
                if (customer.authenticated && customer.registered && customer.addressBook.preferredAddress) {
                    app.getForm(session.forms.singleshipping.shippingAddress.addressFields).copyFrom(customer.addressBook.preferredAddress);
                    app.getForm(session.forms.singleshipping.shippingAddress.addressFields.states).copyFrom(customer.addressBook.preferredAddress);
                }
            }

            session.forms.singleshipping.shippingAddress.email.emailAddress.value = cart.getCustomerEmail();
            
            var isInstorePickup = false;
            var storeId = '';
            var plisIterator = cart.getAllProductLineItems().iterator();
            while (plisIterator.hasNext()) {
                var pli = plisIterator.next();
                if (!empty(pli.custom.storePickupStoreID)) {
                    // this is in store pickup
                    isInstorePickup = true;
                    storeId = pli.custom.storePickupStoreID;
                }
            }
            
            if (empty(storeId)) {
            	if (session.customer.registered && !empty(session.customer.profile.custom.preferredStore)) {
					storeId = session.customer.profile.custom.preferredStore;
				} else if (session.custom.preferredStore) {
					storeId = session.custom.preferredStore;
				}
            }
            
            if (session.custom.deliveryOptionChoice == 'instorepickup' && !empty(storeId)) {
                // set select shipment to instore pickup
                session.forms.singleshipping.shippingAddress.shippingMethodID.value = 'storePickup';
                var store = StoreMgr.getStore(storeId);
                app.getForm('singleshipping').object.shippingAddress.addressFields.address1.value = store.address1;
                app.getForm('singleshipping').object.shippingAddress.addressFields.address2.value = store.address2;
                app.getForm('singleshipping').object.shippingAddress.addressFields.cities.city.value = store.city;
                app.getForm('singleshipping').object.shippingAddress.addressFields.postal.value = store.postalCode;
                app.getForm('singleshipping').object.shippingAddress.addressFields.states.state.value = store.stateCode;
                app.getForm('singleshipping').object.shippingAddress.addressFields.country.value = store.countryCode.value;
            } else {
            	app.getController('Cart').ChangeDeliveryOptionInternal('shipped');
            	session.custom.deliveryOptionChoice = 'shipped';
            	session.forms.singleshipping.shippingAddress.shippingMethodID.value = cart.getDefaultShipment().getShippingMethodID();
            	
            	// If customer updates the zip code, old address line field is invalid
            	if (session.custom.customerZipUpdated && session.custom.customerZipUpdated == true){
            		
            		var shopForm = app.getForm('singleshipping');
            		
            		if (shopForm){
            			shopForm.object.shippingAddress.addressFields.address1.value = "";
            			shopForm.object.shippingAddress.addressFields.address2.value = "";
            		}	
            		session.custom.customerZipUpdated = false;
                    
            	}
            	
            }
            var defaultDeliveryTier = "";
	 	    if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
	 		    var inMarketShipment = cart.object.getShipment('In-Market');
	 			if(!empty(inMarketShipment)) {
	 				defaultDeliveryTier = inMarketShipment.getShippingMethodID();
	 			}
	 	    }
            // Prepares shipments.
            homeDeliveries = prepareShipments();
            
            //set silver/gold/platinum shipping method based criteria
	    	if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
	    		defaultDeliveryTier = COShippingMethodController.SetDeliveryTierShippingMethod(defaultDeliveryTier);
	 	    }
	    	
            initEmailAddress(cart);
            var showMattressRemovalOption = pipeletHelper.showMatressRemovalOptionCheck(cart);
            var mattressRemovalInBasket = cart.getProductLineItems('DWREMOVEBED').size() > 0 ? true : false;
            Transaction.wrap(function () {
                cart.calculate();
            });
            // Go to billing step, if we have no product line items, but only gift certificates in the basket, shipping is not required.
            if (cart.getProductLineItems().size() === 0) {                
                var COBilling = require('~/cartridge/controllers/COBilling');
                COBilling.Start();
            } else {
            	//commented because this is not setting up title of the page
                /*pageMeta = require('app_storefront_controllers/cartridge/scripts/meta');
                pageMeta.update({
                    pageTitle: Resource.msg('singleshipping.meta.pagetitle', 'checkout', 'SiteGenesis Checkout')
                });*/
            	var pageMetaData = request.pageMetaData;
            	pageMetaData.title = Resource.msg('singleshipping.meta.pagetitle', 'checkout', null);
                
            	if (payWithAmazon) {
            		 apModule.removeGiftCertificates(cart.object);
            	}

            	app.getView({
                    ContinueURL: URLUtils.https('COShipping-SingleShipping'),
                    Basket: cart.object,
                    HomeDeliveries: homeDeliveries,
                    ShowMattressRemovalOption: showMattressRemovalOption,
                    MattressRemovalInBasket: mattressRemovalInBasket,
                    PayWithAmazon: payWithAmazon,
            		AmazonAuthError: amazonAuthError
                }).render('checkout/shipping/singleshipping');
            }
        } else {
            //app.getController('COShippingMultiple').Start();
            return;
        }
    } else {
        app.getController('Cart').Show();
        return;
    }

}

/**
 * Handles the selected shipping address and shipping method. Copies the
 * address details and gift options to the basket's default shipment. Sets the
 * selected shipping method to the default shipment.
 *
 * @transactional
 * @param {module:models/CartModel~CartModel} cart - A CartModel wrapping the current Basket.
 */
function handleShippingSettings(cart) {

    Transaction.wrap(function () {
        var defaultShipment, shippingAddress, validationResult, BasketStatus, EnableCheckout;
        defaultShipment = cart.getDefaultShipment();
        shippingAddress = cart.createShipmentShippingAddress(defaultShipment.getID());
        
        // Handle InStore Pickup - shipping address will be store address
        if (session.custom.deliveryOptionChoice == 'instorepickup') {
        	try {
        		var storeId = '';
	        	if (session.customer.registered && !empty(session.customer.profile.custom.preferredStore)) {
	        		storeId = session.customer.profile.custom.preferredStore;
	        	} else {
	        		storeId = session.custom.preferredStore;
	        	}
	        	
	            var plisIterator = cart.getAllProductLineItems().iterator();
	            while (plisIterator.hasNext()) {
	                var pli = plisIterator.next();
	                if (!empty(pli.productID) && pli.productID != 'none') {
		                if (empty(storeId)) {
		                	storeId = pli.custom.storePickupStoreID;
		                }
		                storeResponse = session.custom['storeId' + storeId];
		                if (empty(storeResponse)) {
		                	var key = pli.productID + "-" + storeId;
			                var storeResponse = session.custom[key];
		                }
		                pli.custom.InventLocationId = storeResponse.Location1;
		                pli.custom.secondaryWareHouse = storeResponse.location2;
		                pli.custom.mfiATPLeadDate = storeResponse.MFIATPLeadDate;
	                }
	            }
	            var store = StoreMgr.getStore(storeId);
	            shippingAddress.setAddress1(store.address1);
	            shippingAddress.setAddress2(store.address2);
	            shippingAddress.setCity(store.city);
	            shippingAddress.setPostalCode(store.postalCode);
	            shippingAddress.setStateCode(store.stateCode);
	            shippingAddress.setCountryCode(store.countryCode.value);
	            shippingAddress.setPhone(StringHelpers.formatAXPhoneNumber(store.phone));
	            shippingAddress.setFirstName(session.forms.singleshipping.shippingAddress.addressFields.firstName.value);
	            shippingAddress.setLastName(session.forms.singleshipping.shippingAddress.addressFields.lastName.value);
	            shippingAddress.setPhone(session.forms.singleshipping.shippingAddress.addressFields.phone.value);
	            cart.setCustomerEmail(session.forms.singleshipping.shippingAddress.email.emailAddress.value);
        	} catch (e) {
        		var error = e;
        		session.custom.deliveryOptionChoice = 'shipped';
        		response.redirect(URLUtils.https('COShipping-Start'));
        	}
        } else {
            shippingAddress.setFirstName(session.forms.singleshipping.shippingAddress.addressFields.firstName.value);
            shippingAddress.setLastName(session.forms.singleshipping.shippingAddress.addressFields.lastName.value);
            shippingAddress.setAddress1(session.forms.singleshipping.shippingAddress.addressFields.address1.value);
            shippingAddress.setAddress2(session.forms.singleshipping.shippingAddress.addressFields.address2.value);
            shippingAddress.setCity(session.forms.singleshipping.shippingAddress.addressFields.city.value);
            shippingAddress.setPostalCode(session.forms.singleshipping.shippingAddress.addressFields.postal.value);
            shippingAddress.setStateCode(session.forms.singleshipping.shippingAddress.addressFields.states.state.value);
            shippingAddress.setCountryCode('US');
            shippingAddress.setPhone(StringHelpers.formatAXPhoneNumber(session.forms.singleshipping.shippingAddress.addressFields.phone.value));
            defaultShipment.setGift(session.forms.singleshipping.shippingAddress.isGift.value);
            defaultShipment.setGiftMessage(session.forms.singleshipping.shippingAddress.giftMessage.value);
            cart.setCustomerEmail(session.forms.singleshipping.shippingAddress.email.emailAddress.value);
        }

        session.forms.singleshipping.shippingAddress.useAsBillingAddress.value = session.forms.singleshipping.shippingAddress.useAsBillingAddress.value;
        // For default shipment only
        var cartDefaultShipmentID = cart.getDefaultShipment().getID();
        cart.updateShipmentShippingMethod(cartDefaultShipmentID, session.forms.singleshipping.shippingAddress.shippingMethodID.value, null, null);
        if (session.custom.deliveryOptionChoice == 'instorepickup') {
            var storePickupShipment = cart.getShipment(cartDefaultShipmentID);
            storePickupShipment.custom.storeId = storeId;
        }
        
//        pipeletHelper.updateBasketWithSelectedDate(session.forms.singleshipping.shippingAddress.addressFields.deliveryDate.htmlValue, session.forms.singleshipping.shippingAddress.addressFields.fleetwiseToken.htmlValue, cart.object);
        
        if (session.forms.singleshipping.pickups.value > 0 && cart.getProductLineItems(dw.system.Site.getCurrent().getPreferences().getCustom()["mattressPickupID"]).size() == 0) {
            var Product = app.getModel('Product');
            var product = Product.get(dw.system.Site.getCurrent().getPreferences().getCustom()["mattressPickupID"]);
            var productOptionModel  = product.updateOptionSelection(request.httpParameterMap);
            cart.addProductItem(product.object, session.forms.singleshipping.pickups.value, null, productOptionModel);
        }
        
        if (session.custom.deliveryOptionChoice != 'instorepickup') {
        	var forceServiceCall = true;
            mattressPipeletHelper.splitShipmentsByType(cart.object, forceServiceCall);
        }
        
        cart.calculate();
                
        var hasZIPchanged = session.custom.customerZip != shippingAddress.getPostalCode();
        if (hasZIPchanged) {
            pipeletHelper.setCustomerZone(shippingAddress.getPostalCode());
        }

        validationResult = cart.validateForCheckout();

        // TODO - what are those variables used for, do they need to be returned ?
        BasketStatus = validationResult.BasketStatus;
        EnableCheckout = validationResult.EnableCheckout;

    });

    return;
}

/**
 * Updates shipping address for the current customer with information from the singleshipping form. If a cart exists, redirects to the
 * {@link module:controllers/COShipping~start|start} function. If one does not exist, calls the {@link module:controllers/Cart~Show|Cart controller Show function}.
 *
 * @transactional
 */
function updateAddressDetails() {
    var cart, addressID, segments, lookupCustomer, lookupID, address, defaultShipment, shippingAddress, profile;
    //Gets an empty cart object from the CartModel.
    cart = app.getModel('Cart').get();

    if (cart) {
        addressID = !request.httpParameterMap.addressID.value ? request.httpParameterMap.dwfrm_singleshipping_addressList.value : request.httpParameterMap.addressID.value;
        segments = addressID.split('??');

        lookupCustomer = customer;
        lookupID = addressID;

        if (segments.length > 1) {
            profile = CustomerMgr.queryProfile('email = {0}', segments[0]);
            lookupCustomer = profile.getCustomer();
            lookupID = segments[1];
        }

        address = lookupCustomer.getAddressBook().getAddress(lookupID);
        app.getForm(session.forms.singleshipping.shippingAddress.addressFields).copyFrom(address);
        app.getForm(session.forms.singleshipping.shippingAddress.addressFields.states).copyFrom(address);

        Transaction.wrap(function () {
            defaultShipment = cart.getDefaultShipment();
            shippingAddress = cart.createShipmentShippingAddress(defaultShipment.getID());

            shippingAddress.setFirstName(session.forms.singleshipping.shippingAddress.addressFields.firstName.value);
            shippingAddress.setLastName(session.forms.singleshipping.shippingAddress.addressFields.lastName.value);
            shippingAddress.setAddress1(session.forms.singleshipping.shippingAddress.addressFields.address1.value);
            shippingAddress.setAddress2(session.forms.singleshipping.shippingAddress.addressFields.address2.value);
            shippingAddress.setCity(session.forms.singleshipping.shippingAddress.addressFields.city.value);
            shippingAddress.setPostalCode(session.forms.singleshipping.shippingAddress.addressFields.postal.value);
            shippingAddress.setStateCode(session.forms.singleshipping.shippingAddress.addressFields.states.state.value);
            shippingAddress.setCountryCode(session.forms.singleshipping.shippingAddress.addressFields.country.value);
            shippingAddress.setPhone(session.forms.singleshipping.shippingAddress.addressFields.phone.value);
            defaultShipment.setGift(session.forms.singleshipping.shippingAddress.isGift.value);
            defaultShipment.setGiftMessage(session.forms.singleshipping.shippingAddress.giftMessage.value);
        });

        start();
    } else {
        app.getController('Cart').Show();
    }
}

/**
 * Form handler for the singleshipping form. Handles the following actions:
 * - __save__ - saves the shipping address from the form to the customer address book. If in-store
 * shipments are enabled, saves information from the form about in-store shipments to the order shipment.
 * Flags the save action as done and calls the {@link module:controllers/Cart~Show|Cart controller Show function}.
 * If it is not able to save the information, calls the {@link module:controllers/Cart~Show|Cart controller Show function}.
 * - __selectAddress__ - updates the address details and page metadata, sets the ContinueURL property to COShipping-SingleShipping,  and renders the singleshipping template.
 * - __shipToMultiple__ - calls the {@link module:controllers/COShippingMultiple~Start|COShippingMutliple controller Start function}.
 * - __error__ - calls the {@link module:controllers/COShipping~Start|COShipping controller Start function}.
 */
function singleShipping() {
    var singleShippingForm = app.getForm('singleshipping');
    singleShippingForm.handleAction({
        save: function () {
            var cart = app.getModel('Cart').get();

            if (cart) {
            	
            	if (!validateShippingAddress()) {
            		returnToForm(cart);
                    return;
            	} else {
	                var zipCode = session.forms.singleshipping.shippingAddress.addressFields.postal.value;
                    var zipResult =  verifyDeliveryZone (zipCode);
                    
                    var defaultDeliveryTier = "";
        			if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
        				var inMarketShipment = cart.object.getShipment('In-Market');
        				if(!empty(inMarketShipment)) {
        					defaultDeliveryTier = inMarketShipment.getShippingMethodID();
        				}
        			}
                    
                    handleShippingSettings(cart);
	
                    if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
        				var payWithAmazon = false;
        	            if (apController.SetCheckoutMethod()) {
        	            	payWithAmazon = true;
        	            }
        		        if(!payWithAmazon && empty(cart.getPaymentInstruments('PayPal'))) {
        			      //Retain silver/gold/Platinum if user already selected one of them
        			      defaultDeliveryTier = COShippingMethodController.SetDeliveryTierShippingMethod(defaultDeliveryTier);
        		        }
        			}
	                // Attempts to save the used shipping address in the customer address book.
	                if (customer.authenticated && session.forms.singleshipping.shippingAddress.addToAddressBook.value) {
	                    app.getModel('Profile').get(customer.profile).addAddressToAddressBook(cart.getDefaultShipment().getShippingAddress());
	                }
	                // Binds the store message from the user to the shipment.
	                if (Site.getCurrent().getCustomPreferenceValue('enableStorePickUp') && session.custom.deliveryOptionChoice == 'instorepickup') {
	
	                    if (!app.getForm(session.forms.singleshipping.inStoreShipments.shipments).copyTo(cart.getShipments())) {
	                        require('./Cart').Show();
	                        return;
	                    }
	                }
	                
	                if (session.forms.singleshipping.shippingAddress.addToSubscription.value && dw.system.Site.getCurrent().getID() != 'Mattress-Firm') {
	                    var emailAddress = session.forms.singleshipping.shippingAddress.email.emailAddress.value;
	                    var returnResult = pipeletHelper.sendEmailInfo(emailAddress, "checkout");
	                }
	                
	                var gaCookie = request.getHttpCookies()['_ga'];
	                var gclid;
	                if (gaCookie) {
	                	gclid = gaCookie.value;
	                }
	
	        		var emailParams = {
	        				emailAddress: session.forms.singleshipping.shippingAddress.email.emailAddress.value, 
	        				zipCode: session.forms.singleshipping.shippingAddress.addressFields.postal.value, 
	        				leadSource: 'checkout', 
	        				siteId: dw.system.Site.getCurrent().getID(), 
	        				optOutFlag: !session.forms.singleshipping.shippingAddress.addToSubscription.value,
	        				gclid: gclid,
	        				dwsid: session.sessionID
	        		};
	                
	    			var returnResult = emailHelper.sendSFEmailInfo(emailParams);
	    			if (returnResult.Status == 'SERVICE_ERROR'){
	    				var returnResult = emailHelper.sendFailSafe(emailParams, returnResult.ErrorCode);
	    			}
	    			
	    			/*	check if checkout is not allowed for this Shipping Address	*/
	    			zipCode = session.forms.singleshipping.shippingAddress.addressFields.postal.value;
	    			var isRestrictedCheckout = mattressPipeletHelper.getIsRestrictedCheckout(zipCode);
	    			if (isRestrictedCheckout.status) {
	    				app.getView({
							Basket : cart.object,
							restrictedState : isRestrictedCheckout.restrictedState
						}).render('checkout/shipping/mobile_opc_restrictcheckout');
	    			} else {
	    				var payWithAmazon = false;
	    		    	if (apController.SetCheckoutMethod()) {
	    		    		payWithAmazon = true;
	    		    	}
	    		    	if(!payWithAmazon && !empty(cart.getPaymentInstruments('PayPal'))){
		                    app.getForm(session.forms.singleshipping.shippingAddress.addressFields).copyTo(cart.getBillingAddress());
		                    app.getForm(session.forms.singleshipping.shippingAddress.addressFields.states).copyTo(cart.getBillingAddress());
		                    Transaction.wrap(function () {
		                        var city = session.forms.singleshipping.shippingAddress.addressFields.cities.city.value;
		                        cart.getBillingAddress().setCity(city);
		                    });
		                }
		                
		                // Mark step as fulfilled.
		                session.forms.singleshipping.fulfilled.value = true;
		                
		                // Customer address postal zip code changed
                        session.custom.customerZipChanged = zipResult;
                        
                        var productsGeoAvailabilityMap = UtilFunctions.buildProductsGeoAvailabilityMap(cart.object);
            			var allProductsGeoAvailable = UtilFunctions.checkAllProductsGeoAvailable(cart.object,productsGeoAvailabilityMap);
	                     
                        if (zipResult || !allProductsGeoAvailable){
                            response.redirect(URLUtils.https('COShippingMethod-Start', 'deliveryScheduleChanged', 'true'));
                        }else {
                            response.redirect(URLUtils.https('COBilling-Start'));
                        }
	    			}
            	}
                
            } else {
                // @FIXME redirect
                app.getController('Cart').Show();
                return;
            }
        },
        selectAddress: function () {
            updateAddressDetails(app.getModel('Cart').get());

           /* var pageMeta = require('app_storefront_controllers/cartridge/scripts/meta');
            pageMeta.update({
                pageTitle: Resource.msg('singleshipping.meta.pagetitle', 'checkout', 'SiteGenesis Checkout')
            });*/
            var pageMetaData = request.pageMetaData;
        	pageMetaData.title = Resource.msg('singleshipping.meta.pagetitle', 'checkout', null);
            app.getView({
                ContinueURL: URLUtils.https('COShipping-SingleShipping')
            }).render('checkout/shipping/singleshipping');

            return;
        },
        amazonApply: function() {
        	var cart = app.getModel('Cart').get();
        	if(!empty(cart)) {
        	app.getForm('singleshipping').clear();
        	app.getForm('billing').clear();
        	//setting null to amazonDeliveryPage because this variable used in minisummary to hide shipment section
        	session.custom.amazonDeliveryPage = null;
        	if (!cart) {
        		response.redirect(URLUtils.https('Cart-Show'));
        		return;
        	}
        	var apResponse = {}; 
        	if (session.forms.apShippingForm.billingAgreementID.value !== 'null') {
        		apResponse = apController.GetBillingAgreementDetailsResponse().billingAgreementResult;
        	} else {
        		apResponse = apController.GetOrderReferenceResponse().orderReferenceResult;
        	}
        	if(!empty(apResponse)) {
        		var shippingAddressName = apResponse.address.fullName;
        		if(shippingAddressName.indexOf(" ") != -1) {
	        		var arrVars = shippingAddressName.split(" ");
	        		var lastName = arrVars.pop();
	        		var firstName = arrVars.join(" ");
        		} else {
        			//if fullname is one word then we have to make it same for first and last name
        			var lastName = apResponse.address.fullName;
	        		var firstName = apResponse.address.fullName;
        		}
	        	
            	app.getForm('singleshipping.shippingAddress.addressFields.firstName').value = firstName;
	            session.forms.singleshipping.shippingAddress.addressFields.firstName.value = firstName;
	            
            	app.getForm('singleshipping.shippingAddress.addressFields.lastName').value = lastName;
	            session.forms.singleshipping.shippingAddress.addressFields.lastName.value = lastName;
            
	            var apZip = apResponse.address.postalCode.split('-');
	        	app.getForm('singleshipping.shippingAddress.addressFields.postal').value = apZip[0];
	        	session.forms.singleshipping.shippingAddress.addressFields.postal.value = apZip[0];
	        	session.custom.customerZip = apZip[0];
	        	//saving postal code in billing form
	        	app.getForm('billing.billingAddress.addressFields.postal').value = apZip[0];
	        	session.forms.billing.billingAddress.addressFields.postal.value = apZip[0];
	        	
	        	app.getForm('singleshipping.shippingAddress.addressFields.address1').value = apResponse.address.address1;
	        	session.forms.singleshipping.shippingAddress.addressFields.address1.value = apResponse.address.address1;
	        	if(!empty(apResponse.address.address2)) {
	        		app.getForm('singleshipping.shippingAddress.addressFields.address2').value = apResponse.address.address2;
	        		session.forms.singleshipping.shippingAddress.addressFields.address2.value = apResponse.address.address2;
	        	}
	        	app.getForm('singleshipping.shippingAddress.addressFields.city').value = apResponse.address.city;
	        	session.forms.singleshipping.shippingAddress.addressFields.city.value = apResponse.address.city;
	        	app.getForm('singleshipping.shippingAddress.addressFields.states.state').value = apResponse.address.stateCode;
	        	session.forms.singleshipping.shippingAddress.addressFields.states.state.value = apResponse.address.stateCode;
	            app.getForm('singleshipping.shippingAddress.addressFields.country').value = apResponse.address.countryCode;
	        	session.forms.singleshipping.shippingAddress.addressFields.country.value = apResponse.address.countryCode;
	        	
	        	session.forms.singleshipping.shippingAddress.addressFields.phone.clearFormElement();
	        	session.forms.billing.billingAddress.addressFields.phone.clearFormElement();
	        	
	        	if(!empty(apResponse.address.phone) && apResponse.address.phone.length > 9) {
	        		app.getForm('singleshipping.shippingAddress.addressFields.phone').value = apResponse.address.phone;
		        	session.forms.singleshipping.shippingAddress.addressFields.phone.value = apResponse.address.phone;
		        	app.getForm('billing.billingAddress.addressFields.phone').value = apResponse.address.phone;
		        	session.forms.billing.billingAddress.addressFields.phone.value = apResponse.address.phone;
	        	}
	        	
	        	//This call is necessary, it will create  session.custom.amazonCustomer  which will be used throughout amazon pay checkout.
	        	var apCustomerProfile = apController.GetCustomerProfile();
	        	app.getForm('singleshipping.shippingAddress.email.emailAddress').value =  session.custom.amazonCustomer.email;
	        	session.forms.singleshipping.shippingAddress.email.emailAddress.value = session.custom.amazonCustomer.email;
                app.getForm('apShippingForm.email').value =  session.custom.amazonCustomer.email;
	        	session.forms.apShippingForm.email.value =  session.custom.amazonCustomer.email;
	        	
	        	//****************************************************
	        	copyAPaddressToShipmentAndCartBillingAddress(apResponse);
	        	//****************************************************
        	}
        	apController.HandleShippingSettings(cart, apResponse.address);
            // Attempts to save the used shipping address in the customer address book.
            if (customer.authenticated && !empty(customer.profile.custom.amazonBillingAgreementID) && customer.profile.custom.amazonBillingAgreementID == session.forms.apShippingForm.billingAgreementID.value) {
                var billingAddressForm = app.getForm('billing').object.billingAddress.addressFields,
                emailForm = app.getForm('billing').object.billingAddress.email,
                amazonForm = app.getForm('apShippingForm').object;
                apModule.apCopyShippingAddressToForm(cart.object, apResponse.apAddressBilling, billingAddressForm, emailForm, amazonForm);
            }
            // Attempts to save the used shipping address in the customer address book.
            if (customer.authenticated && session.forms.singleshipping.shippingAddress.addToAddressBook.value) {
                app.getModel('Profile').get(customer.profile).addAddressToAddressBook(cart.getDefaultShipment().getShippingAddress());
            }
            // Binds the store message from the user to the shipment.
            if (Site.getCurrent().getCustomPreferenceValue('enableStorePickUp')) {
                if (!app.getForm(session.forms.singleshipping.inStoreShipments.shipments).copyTo(cart.getShipments())) {
                    require('./Cart').Show();
                    return;
                }
            }
            // Mark step as fulfilled.
            session.forms.singleshipping.fulfilled.value = true;
            response.redirect(URLUtils.https('COBilling-Start'));
        	} else {
        		app.getController('Cart').Show();
        	    return;
        	}
        },
        shipToMultiple: function () {
            app.('COShippingMultiple').Start();
            return;
        },
        error: function () {
            response.redirect(URLUtils.https('Cart-Show'));
            return;
        }
    });
}

function copyAPaddressToShipmentAndCartBillingAddress(apResponse) {
    var cart = app.getModel('Cart').get();
    if (!empty(apResponse) && cart) {
		var shippingAddressName = apResponse.address.fullName;
		if(shippingAddressName.indexOf(" ") != -1) {
    		var arrVars = shippingAddressName.split(" ");
    		var lastName = arrVars.pop();
    		var firstName = arrVars.join(" ");
		} else {
			//if fullname is one word then we have to make it same for first and last name
			var lastName = apResponse.address.fullName;
    		var firstName = apResponse.address.fullName;
		}
		var apZip = apResponse.address.postalCode.split('-');
		//set shipping address
		Transaction.wrap(function () {
	        var defaultShipment, shippingAddress;
	        defaultShipment = cart.getDefaultShipment();
	        shippingAddress = cart.createShipmentShippingAddress(defaultShipment.getID());
	        var shippingMethodID = defaultShipment.getShippingMethodID();
	        
	        shippingAddress.setFirstName(firstName);
	        shippingAddress.setLastName(lastName);
	        shippingAddress.setAddress1(apResponse.address.address1);
	        shippingAddress.setAddress2(apResponse.address.address2);
	        shippingAddress.setCity(apResponse.address.city);
	        shippingAddress.setPostalCode(apZip[0]);
	        shippingAddress.setStateCode(apResponse.address.stateCode);
	        if(!empty(apResponse.address.phone) && apResponse.address.phone.length > 9) {
	        	shippingAddress.setPhone(apResponse.address.phone);
		    } else {
		    	shippingAddress.setPhone('');
        	}
	        shippingAddress.setCountryCode('US');
	        cart.setCustomerEmail(session.custom.amazonCustomer.email);
        });
        var defaultDeliveryTier = "";
        if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
            var inMarketShipment = cart.object.getShipment('In-Market');
            if(!empty(inMarketShipment)) {
                defaultDeliveryTier = inMarketShipment.getShippingMethodID();
            }
        
            //set silver/gold/platinum shipping method based criteria
			defaultDeliveryTier = COShippingMethodController.SetDeliveryTierShippingMethod(defaultDeliveryTier);
		 }
	     //set billing address
		Transaction.wrap(function () {
	        var billingAddress = cart.createBillingAddress();
	        billingAddress.setFirstName(firstName);
	        billingAddress.setLastName(lastName);
			billingAddress.setAddress1(apResponse.address.address1);
			billingAddress.setAddress2(apResponse.address.address2);
			billingAddress.setCity(apResponse.address.city);
			billingAddress.setPostalCode(apZip[0]);
			billingAddress.setStateCode(apResponse.address.stateCode);
            billingAddress.setCountryCode('US');
            if (!empty(apResponse.address.phone) && apResponse.address.phone.length > 9) {
                billingAddress.setPhone(apResponse.address.phone);
            } else {
                billingAddress.setPhone('');
            }
		});
	}
}
//Verify ATP delivery zone
function verifyDeliveryZone(zipCode) {
    
    var deliveryDate = session.custom.deliveryDate;
    var deliveryTime = session.custom.deliveryTime;
    var deliveryZone = session.custom.deliveryZone;
    
    var result = false;
    //verify shipping match with delivery zip or not  
    var hasZIPchanged = session.custom.customerZip != zipCode;
    
    if (hasZIPchanged) {                    
        session.custom.customerZip = zipCode;
        result = true;
        var cart = app.getModel('Cart').get();    
        var deliveryDatesArr = null;
        var StorePicker = require('app_storefront_controllers/cartridge/controllers/StorePicker');
        if (deliveryDate != 'null' && !empty(deliveryDate)) {
            
            if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule') ) {    
                deliveryDatesArr = StorePicker.GetDeliveryZone(cart, zipCode, deliveryDate);
            }    
            if (!empty(deliveryDatesArr)) {            
                
                var key = new Date(deliveryDatesArr[0]).toDateString().replace(' ', '', 'g');
                var deliveryTimeSlots = session.custom.deliveryDates[key]['timeslots'];
                for (var j = 0; j < deliveryTimeSlots.length; j++) {                            
                    if (deliveryTimeSlots[j]['available'] == 'yes' && deliveryTimeSlots[j]['slots'] > 0) {
                        var startTime = deliveryTimeSlots[j]['startTime'];
                        var endTime = deliveryTimeSlots[j]['endTime'];
                        var slot = StringUtils.formatDate(startTime, "h:mma").toString() +'-' + StringUtils.formatDate(endTime, "h:mma").toString();
                        if ((deliveryTime.toLowerCase().replace(' ', '', 'g') == slot.toLowerCase()) && (deliveryZone == deliveryTimeSlots[j]['mfiDSZoneLineId'])) {                        
                            result = false;
                            break;
                        }            
                    }
                }                
            }
            
        }    
    }
    if (result) {
        session.custom.deliveryDate = null;
        session.custom.deliveryTime = null;
        session.custom.deliveryZone = null;
    }
    return result; 
}



/**
 * Initializes the email address form field. If there is already a customer
 * email set at the basket, that email address is used. If the
 * current customer is authenticated the email address of the customer's profile
 * is used.
 */
function initEmailAddress(cart) {
    if (cart.getCustomerEmail() !== null) {
        session.forms.singleshipping.shippingAddress.email.emailAddress.value = cart.getCustomerEmail();
    } else if (customer.authenticated && customer.profile.email !== null) {
        session.forms.singleshipping.shippingAddress.email.emailAddress.value = customer.profile.email;
    }
}

/**
 * Selects a shipping method for the default shipment. Creates a transient address object, sets the shipping
 * method, and returns the result as JSON response.
 *
 * @transaction
 */
function selectShippingMethod() {
    var cart, address, applicableShippingMethods, TransientAddress;
    TransientAddress = app.getModel('TransientAddress');
    cart = app.getModel('Cart').get();

    if (cart) {

        address = new TransientAddress();
        
        if (!empty(request.httpParameterMap.orderReferenceID.stringValue) || !empty(request.httpParameterMap.billingAgreementID.stringValue)) {
	    	var isRecurring = !empty(request.httpParameterMap.billingAgreementID.stringValue),
	    		apResponse = isRecurring ? apController.GetBillingAgreementDetailsResponse() : apController.GetOrderReferenceResponse();
	    	
	    	if (!apResponse.error) {
	            var apAddress = isRecurring ? apResponse.billingAgreementResult.address : apResponse.orderReferenceResult.address;
	            address.countryCode = apAddress.countryCode;
	            address.stateCode = apAddress.stateCode;
	            address.postalCode = apAddress.postalCode;
	            address.city = apAddress.city;
	            address.address1 = apAddress.address1;
	            address.address2 = apAddress.address2;
	            }
        } else {
	        address.countryCode = request.httpParameterMap.countryCode.stringValue;
	        address.stateCode = request.httpParameterMap.stateCode.stringValue;
	        address.postalCode = request.httpParameterMap.postalCode.stringValue;
	        address.city = request.httpParameterMap.city.stringValue;
	        address.address1 = request.httpParameterMap.address1.stringValue;
	        address.address2 = request.httpParameterMap.address2.stringValue;
        }
        applicableShippingMethods = cart.getApplicableShippingMethods(address);

        Transaction.wrap(function () {
            cart.updateShipmentShippingMethod(cart.getDefaultShipment().getID(), request.httpParameterMap.shippingMethodID.stringValue, null, applicableShippingMethods);
            cart.calculate();
        });

        app.getView({
            Basket: cart.object
        }).render('checkout/shipping/selectshippingmethodjson');
    } else {
        app.getView.render('checkout/shipping/selectshippingmethodjson');
    }
}

/**
 * Determines the list of applicable shipping methods for the default shipment of
 * the current basket. The applicable shipping methods are based on the
 * merchandise in the cart and any address parameters included in the request.
 * Changes the shipping method of this shipment if the current method
 * is no longer applicable. Precalculates the shipping cost for each applicable
 * shipping method by simulating the shipping selection i.e. explicitly adds each
 * shipping method and then calculates the cart.
 * The simulation is done so that shipping cost along
 * with discounts and promotions can be shown to the user before making a
 * selection.
 * @transaction
 */
function updateShippingMethodList() {
    var i, cart, address, applicableShippingMethods, shippingCosts, currentShippingMethod, method, TransientAddress, shipmentID, shipment;
    TransientAddress = app.getModel('TransientAddress');
    cart = app.getModel('Cart').get();

    if (cart) {
    	address = new TransientAddress();
        if (!empty(session.forms.apShippingForm.orderReferenceID.value) || !empty(request.httpParameterMap.billingAgreementID.stringValue)) {
            var isRecurring = !empty(request.httpParameterMap.billingAgreementID.stringValue),
                apResponse = isRecurring ? apController.GetBillingAgreementDetailsResponse() : apController.GetOrderReferenceResponse();

            if (!apResponse.error) {
                var apAddress = isRecurring ? apResponse.billingAgreementResult.address : apResponse.orderReferenceResult.address;

                address.countryCode = apAddress.countryCode;
                address.stateCode = apAddress.stateCode;
                address.postalCode = apAddress.postalCode;
                address.city = apAddress.city;
                address.address1 = apAddress.address1;
                address.address2 = apAddress.address2;
            }
        } else {
	        address.countryCode = request.httpParameterMap.countryCode.stringValue;
	        address.stateCode = request.httpParameterMap.stateCode.stringValue;
	        address.postalCode = request.httpParameterMap.postalCode.stringValue;
	        address.city = request.httpParameterMap.city.stringValue;
	        address.address1 = request.httpParameterMap.address1.stringValue;
	        address.address2 = request.httpParameterMap.address2.stringValue;
        }	
        applicableShippingMethods = cart.getApplicableShippingMethods(address);
        shippingCosts = new HashMap();
        
        shipmentID = request.httpParameterMap.shipmentID.stringValue ? request.httpParameterMap.shipmentID.stringValue : cart.getDefaultShipment().getID();
        shipment = cart.getShipment(shipmentID);
        currentShippingMethod = shipment.getShippingMethod() || ShippingMgr.getDefaultShippingMethod();

        // Transaction controls are for fine tuning the performance of the data base interactions when calculating shipping methods
        Transaction.begin();

        for (i = 0; i < applicableShippingMethods.length; i += 1) {
            method = applicableShippingMethods[i];

            cart.updateShipmentShippingMethod(shipmentID, method.getID(), method, applicableShippingMethods);
            cart.calculate();
            shippingCosts.put(method.getID(), cart.preCalculateShipping(method));
        }
        Transaction.rollback();

        Transaction.wrap(function () {
            cart.updateShipmentShippingMethod(shipmentID, currentShippingMethod.getID(), currentShippingMethod, applicableShippingMethods);
            cart.calculate();
        });

        session.forms.singleshipping.shippingAddress.shippingMethodID.value = cart.getDefaultShipment().getShippingMethodID();

        app.getView({
            Basket: cart.object,
            ApplicableShippingMethods: applicableShippingMethods,
            ShippingCosts: shippingCosts,
            Shipment: shipment,
            ShippingMethod: currentShippingMethod
        }).render('checkout/shipping/shippingmethods-noselect');
    } else {
        app.getController('Cart').Show();
    }
}

/**
 * Determines the list of applicable shipping methods for the default shipment of
 * the current customer's basket and returns the response as a JSON array. The
 * applicable shipping methods are based on the merchandise in the cart and any
 * address parameters are included in the request parameters.
 */
function getApplicableShippingMethodsJSON() {
    var cart, address, applicableShippingMethods, TransientAddress;
    TransientAddress = app.getModel('TransientAddress');
    cart = app.getModel('Cart').get();

    address = new TransientAddress();
    if (!empty(request.httpParameterMap.orderReferenceID.stringValue) || !empty(request.httpParameterMap.billingAgreementID.stringValue)) {
        var isRecurring = !empty(request.httpParameterMap.billingAgreementID.stringValue),
            apResponse = isRecurring ? apController.GetBillingAgreementDetailsResponse() : apController.GetOrderReferenceResponse();

        if (!apResponse.error) {
            var apAddress = isRecurring ? apResponse.billingAgreementResult.address : apResponse.orderReferenceResult.address;

            address.countryCode = apAddress.countryCode;
            address.stateCode = apAddress.stateCode;
            var apZip = apAddress.postalCode.split('-');
           
            address.postalCode = apZip[0];
            session.custom.customerZip = apZip[0];
            
            //saving postal code in shipping form
            app.getForm('singleshipping.shippingAddress.addressFields.postal').value = apZip[0];
        	session.forms.singleshipping.shippingAddress.addressFields.postal.value = apZip[0];
        	
        	//saving postal code in billing form
        	app.getForm('billing.billingAddress.addressFields.postal').value = apZip[0];
        	session.forms.billing.billingAddress.addressFields.postal.value = apZip[0];
            
            address.city = apAddress.city;
            address.address1 = apAddress.address1;
            address.address2 = apAddress.address2;
        }
    } else {
	    address.countryCode = request.httpParameterMap.countryCode.stringValue;
	    address.stateCode = request.httpParameterMap.stateCode.stringValue;
	    address.postalCode = request.httpParameterMap.postalCode.stringValue;
	    address.city = request.httpParameterMap.city.stringValue;
	    address.address1 = request.httpParameterMap.address1.stringValue;
	    address.address2 = request.httpParameterMap.address2.stringValue;
    }
    applicableShippingMethods = cart.getApplicableShippingMethods(address);
    var productsGeoAvailabilityMap = UtilFunctions.buildProductsGeoAvailabilityMap(cart.object);
	var allProductsGeoAvailable = UtilFunctions.checkAllProductsGeoAvailable(cart.object,productsGeoAvailabilityMap);
	var geoAvailability = {};
	geoAvailability.allProductsGeoAvailable = allProductsGeoAvailable;
	geoAvailability.zipcode = session.custom.customerZip;
	var geoAvailabilityString = JSON.stringify(geoAvailability);
    app.getView({
        ApplicableShippingMethods: applicableShippingMethods,
        StateCode: address.stateCode,
        GeoAvailability: geoAvailabilityString
    }).render('checkout/shipping/shippingmethodsjson');
}

/**
 * Renders a form dialog to edit an address. The dialog is opened
 * by an Ajax request and ends in templates, which trigger a JavaScript
 * event. The calling page of this dialog is responsible for handling these
 * events.
 */
function editAddress() {

    session.forms.shippingaddress.clearFormElement();

    var shippingAddress = customer.getAddressBook().getAddress(request.httpParameterMap.addressID.stringValue);

    if (shippingAddress) {
        app.getForm(session.forms.shippingaddress).copyFrom(shippingAddress);
        app.getForm(session.forms.shippingaddress.states).copyFrom(shippingAddress);
    }

    app.getView({
        ContinueURL: URLUtils.https('COShipping-EditShippingAddress')
    }).render('checkout/shipping/shippingaddressdetails');
}

/**
 * Form handler for the shippingAddressForm. Handles the following actions:
 *  - __apply__ - if form information cannot be copied to the platform, it sets the ContinueURL property to COShipping-EditShippingAddress and
 *  renders the shippingAddressDetails template. Otherwise, it renders the dialogapply template.
 *  - __remove__ - removes the address from the current customer's address book and renders the dialogdelete template.
 */
function editShippingAddress() {
    var shippingAddressForm, formResult;
    shippingAddressForm = app.getForm('shippingaddress');
    formResult = shippingAddressForm.handleAction({
        apply: function () {
            var object = {};
            // @FIXME what is this statement used for?
            if (!app.getForm(session.forms.shippingaddress).copyTo(object) || !app.getForm(session.forms.shippingaddress.states).copyTo(object)) {
                app.getView({
                    ContinueURL: URLUtils.https('COShipping-EditShippingAddress')
                }).render('checkout/shipping/shippingaddressdetails');
            } else {
                app.getView().render('components/dialog/dialogapply');
            }
        },
        remove: function () {
            customer.getAddressBook().removeAddress(session.forms.shippingaddress.object);

            app.getView().render('components/dialog/dialogdelete');
            return;
        }
    });

    return;
}

function getDataFromZip() {
    var cart, address;
    cart = app.getModel('Cart').get();
    var zipCode = request.httpParameterMap.zipcode.value;
    var result;

	var defaultDeliveryTier = "";
	if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
		var inMarketShipment = cart.object.getShipment('In-Market');
		if(!empty(inMarketShipment)) {
			defaultDeliveryTier = inMarketShipment.getShippingMethodID();
		}
	}
    var citiesAndStateJSON = mattressPipeletHelper.getCitiesAndStatesForZipCode(zipCode);

    Transaction.wrap(function () {
        var defaultShipment, shippingAddress;
        defaultShipment = cart.getDefaultShipment();
      	shippingAddress = cart.createShipmentShippingAddress(defaultShipment.getID());
      	var shippingMethodID = defaultShipment.getShippingMethodID();
        var city = "";
        var state = "";
    	if (!empty(citiesAndStateJSON)) {
    		var cityData = JSON.parse(citiesAndStateJSON);   
            for (var i = 0; i < cityData.cities.length; i++){
                var data = cityData.cities[i];
                if (!empty(data.state)) {
                	state = data.state.toUpperCase();
                	city =  data.city;
                	break;
        		}
            }
            
            if(!empty(state))
	        {
	        	shippingAddress.setCity(city);
	 	        shippingAddress.setPostalCode(zipCode);
	 	        shippingAddress.setStateCode(state);
	 	        shippingAddress.setCountryCode('US');
	 	        cart.updateShipmentShippingMethod(defaultShipment.getID(), shippingMethodID, null, null);
	 	        var forceServiceCall = true;
	            mattressPipeletHelper.splitShipmentsByType(cart.object, forceServiceCall);
	        }
		}
    	else {
    		shippingAddress.setCity(city);
 	        shippingAddress.setPostalCode(zipCode);
 	        shippingAddress.setStateCode(state);
 	        shippingAddress.setCountryCode('US');
 	        cart.updateShipmentShippingMethod(defaultShipment.getID(), shippingMethodID, null, null);
	        var forceServiceCall = true;
            mattressPipeletHelper.splitShipmentsByType(cart.object, forceServiceCall);
    	}

    	cart.removeEmptyShipments();
    	cart.calculate();
    });

	if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
		//Retain silver/gold/Platinum if user already selected one of them
		defaultDeliveryTier = COShippingMethodController.SetDeliveryTierShippingMethod(defaultDeliveryTier);
	}

    session.forms.singleshipping.shippingAddress.addressFields.cityJSON.value = citiesAndStateJSON;
    app.getView({
        JSONData: citiesAndStateJSON
    }).render('util/output');
}

function keepAddress() {
    // Commented out to be replaced with new Delivery Calendar service
    /*
    var cart, address;
    cart = app.getModel('Cart').get();
    var zipCode = request.httpParameterMap.zipcode.value;
    var resultATP, resultUpdate, resultDeliveries, resultStructure;
    
    var addressLine1 = session.forms.singleshipping.shippingAddress.addressFields.address1.htmlValue;
    var addressLine2 = session.forms.singleshipping.shippingAddress.addressFields.address2.htmlValue;
    var city = session.forms.singleshipping.shippingAddress.addressFields.cities.city.htmlValue;
    var state = session.forms.singleshipping.shippingAddress.addressFields.states.state.htmlValue;
    var zip = session.forms.singleshipping.shippingAddress.addressFields.zip.htmlValue;
    
    var address = pipeletHelper.createAddressObject(addressLine1, addressLine2, null, city, state, zip);
    
    resultATP = pipeletHelper.checkATP(address, cart.object);
    resultUpdate = pipeletHelper.updateBasketFromCheckATPLineItems(resultATP.ATPLineItems, cart.object);
    if (resultUpdate.GetDeliverySchedule) {
        resultDeliveries = pipeletHelper.getDeliveries(cart.object, address, resultATP.MaxATP, null, null);
        if (!empty(resultDeliveries.Result)) {
            resultStructure = pipeletHelper.structureDeliveryDatesForUI(resultDeliveries.Result);
        }
    } else {
        pipeletHelper.updateBasketForGetDeliveriesFallback(cart.object);
    }
    */
    app.getView({
        /*
        ATPLineItems: resultATP.ATPLineItems,
        DateList: resultStructure.DateListObject,
        DateListJSON: resultStructure.JSON
        */
    }).render('checkout/components/delivery-schedule-mocked');
}


/**
 * Validates the shipping address.
 * @returns {boolean} Returns true if the address is valid. Returns false if the address is invalid.
 */
function validateShippingAddress() {
	var isValidAddress = true, 
		isValidShippingCity = false, 
		isValidShippingState = false, 
		isValidShippingZipCode = false;
		
	// validate shipping address
	var shippingZipCode = app.getForm('singleshipping.shippingAddress.addressFields.postal').value();
	var citiesAndStateJSON = mattressPipeletHelper.getCitiesAndStatesForZipCode(shippingZipCode);
	if (!empty(citiesAndStateJSON)) {
		var cityData = JSON.parse(citiesAndStateJSON);   
        for (var i = 0; i < cityData.cities.length; i++){
            var data = cityData.cities[i];
            if (app.getForm('singleshipping.shippingAddress.addressFields.city').value().toLowerCase() === data.city.toLowerCase()) {
            	isValidShippingCity = true;
    		}
            if (app.getForm('singleshipping.shippingAddress.addressFields.states.state').value().toLowerCase() === data.state.toLowerCase()) {
            	isValidShippingState = true;
    		}      
        }          
        isValidShippingZipCode = true;
	}
	
	if (!isValidShippingZipCode) {
        app.getForm('singleshipping.shippingAddress.addressFields.postal').invalidate();
        isValidShippingCity = true;
        isValidShippingState = true;
        isValidAddress = false;
    }
	
	if (!isValidShippingCity) {
        app.getForm('singleshipping.shippingAddress.addressFields.city').invalidate();
        isValidAddress = false;
    }
    
    if (!isValidShippingState) {
        app.getForm('singleshipping.shippingAddress.addressFields.states.state').invalidate();
        isValidAddress = false;
    }
    return isValidAddress;
}

// render shipping address template

function returnToForm(cart) {
    if (cart) {     
    	var pageMetaData = request.pageMetaData;
    	pageMetaData.title = Resource.msg('singleshipping.meta.pagetitle', 'checkout', null);
        app.getView({
            ContinueURL: URLUtils.https('COShipping-SingleShipping'),
            Basket: cart.object           
        }).render('checkout/shipping/singleshipping');

    } else {
        app.getController('Cart').Show();
        return;
    }
}

function getCityStateByZipCode() {
    //get city state based on postalCode
    var postalCode = session.custom.customerZip;
    if (!empty(postalCode)) {
        var citiesAndStateJSON = mattressPipeletHelper.getCitiesAndStatesForZipCode(postalCode);
        var city = "";
        var state = "";
        if (!empty(citiesAndStateJSON)) {
            var cityData = JSON.parse(citiesAndStateJSON);
            for (var i = 0; i < cityData.cities.length; i++) {
                var data = cityData.cities[i];
                if (!empty(data.state)) {
                    state = data.state.toUpperCase();
                    city = data.city;
                    break;
                }
            }
            if (!empty(city)) {
                session.forms.singleshipping.shippingAddress.addressFields.city.value = city;
            }
            if (!empty(state)) {
                session.forms.singleshipping.shippingAddress.addressFields.states.state.value = state;
            }
            session.forms.singleshipping.shippingAddress.addressFields.postal.value = postalCode;
        }
    }
}

function customerInfo() {
    var cart = app.getModel('Cart').get();
    session.custom.checkoutStep = 'customerinfo';
	if (cart) {
        session.forms.cart.coupons.copyFrom(cart.object.couponLineItems);

        var pageMetaData = request.pageMetaData;
        pageMetaData.title = Resource.msg('checkout.customer.info.meta.pagetitle', 'checkout', null);

        initEmailAddress(cart);
//        if (!empty(cart.getDefaultShipment().getShippingAddress())) {
//            app.getForm(session.forms.singleshipping.shippingAddress.addressFields).copyFrom(cart.getDefaultShipment().getShippingAddress());
//            app.getForm(session.forms.singleshipping.shippingAddress.addressFields.states).copyFrom(cart.getDefaultShipment().getShippingAddress());
//            app.getForm(session.forms.singleshipping.shippingAddress).copyFrom(cart.getDefaultShipment());
//        } else 
        if (customer.authenticated && customer.registered) {
            if (customer.addressBook.preferredAddress) {
                app.getForm(session.forms.singleshipping.shippingAddress.addressFields).copyFrom(customer.addressBook.preferredAddress);
                app.getForm(session.forms.singleshipping.shippingAddress.addressFields.states).copyFrom(customer.addressBook.preferredAddress);
            } else {
                session.forms.singleshipping.shippingAddress.addressFields.firstName.value = customer.profile.firstName;
                session.forms.singleshipping.shippingAddress.addressFields.lastName.value = customer.profile.lastName;
                getCityStateByZipCode();
            }
        } else {
            getCityStateByZipCode();
        }
        //Checking core products geo-availbility
        var productsGeoAvailabilityMap = UtilFunctions.buildProductsGeoAvailabilityMap(cart.object);
		var allProductsGeoAvailable = UtilFunctions.checkAllProductsGeoAvailable(cart.object,productsGeoAvailabilityMap);
	    
        app.getView({
            ContinueURL: URLUtils.https('COShipping-ContinueToShipping'),
            Basket: cart.object,
            AllProductsGeoAvailable : allProductsGeoAvailable
        }).render('checkout/shipping/customer_info');
    } else {
    	response.redirect(URLUtils.https('Cart-Show'));
        return;
	}
}

function customerInfoModal() {
	var cart = app.getModel('Cart').get();
	if (cart) {
        if (cart.getCustomerEmail() !== null) {
            session.forms.singleshipping.shippingAddress.email.emailAddress.value = cart.getCustomerEmail();
        }
        if (!empty(cart.getDefaultShipment().getShippingAddress())) {
            app.getForm(session.forms.singleshipping.shippingAddress.addressFields).copyFrom(cart.getDefaultShipment().getShippingAddress());
            app.getForm(session.forms.singleshipping.shippingAddress.addressFields.states).copyFrom(cart.getDefaultShipment().getShippingAddress());
            app.getForm(session.forms.singleshipping.shippingAddress).copyFrom(cart.getDefaultShipment());
        }
        app.getView({
            ContinueURL: URLUtils.https('COShipping-UpdateCustomerInfo')
        }).render('checkout/shipping/customer_info_mobile_redesign_modal_mob_ab');
    } else {
    	response.redirect(URLUtils.https('Cart-Show'));
        return;
	}
}

function setDeliverTier(shippingMethodID) {
	var productId ='';
	var SM_ID ='';
	var shippingCost;
	var cart = app.getModel('Cart').get();
    var basket = cart.object;
    var allShippingMethodsCollection : Collection = null;
    var allShippingMethodsMap = null;
    var inMarketShipment = basket.getShipment('In-Market');
    
    // Get all the shipping methods and place them in a hashmap to make it easier to retrieve a specific one.
    allShippingMethodsCollection = dw.order.ShippingMgr.allShippingMethods;
    allShippingMethodsMap = new HashMap();
    for each (var SM : ShippingMethod in allShippingMethodsCollection) {
        allShippingMethodsMap.put(SM.ID, SM);
    }
	
	//Set Delivery Tier based on customer selection in basket
    var params = request.httpParameterMap;
    if(params.format.stringValue == "ajax") {
	    productId = params.pid.stringValue ? params.pid.stringValue : '';
	    SM_ID = params.shippingMethod.stringValue ? params.shippingMethod.stringValue : '';
	    shippingCost = params.shippingCost.stringValue ? params.shippingCost.stringValue : '';
    } else {
    	SM_ID = shippingMethodID;
    	var shipInfo = allShippingMethodsMap.get(SM_ID);
    	productId = shipInfo.custom.deliveryTierSkuId;
    	shippingCost = dw.order.ShippingMgr.getShippingCost(shipInfo,cart.object.totalNetPrice).value;
    	
    }
    
    if (!empty(cart) && !empty(productId)) {
    	
    	var product = dw.catalog.ProductMgr.getProduct(productId);
        if (!empty(product) && !empty(inMarketShipment) && inMarketShipment.productLineItems.length > 0) {
        	var productLineItems = basket.productLineItems;
            Transaction.wrap(function () {
                try{
                    for each(var productLineItem in productLineItems) {
                        if (productLineItem.custom.isDeliveryTier) {
                            basket.removeProductLineItem(productLineItem);
                        }
                    }
                    cart.calculate();
                    var AddProductToBasketResult = new dw.system.Pipelet('AddProductToBasket').execute({
                        Basket: cart.object,
                        Product: product,
                        Quantity: 1,
                        Category: product.categorized ? product.categories[0] : null
                    });
                    if (AddProductToBasketResult.result === PIPELET_ERROR) {
                        return false;
                    } else {
                    	var finalShippingCost = shippingCost;
                    	var coreProduct = inMarketShipment.productLineItems[0];
                    	AddProductToBasketResult.ProductLineItem.custom.deliveryDate = coreProduct.custom.deliveryDate;
                    	AddProductToBasketResult.ProductLineItem.custom.InventLocationId = coreProduct.custom.InventLocationId;
                    	AddProductToBasketResult.ProductLineItem.custom.manufacturerSKU = AddProductToBasketResult.ProductLineItem.product.manufacturerSKU;
                    	AddProductToBasketResult.ProductLineItem.custom.mfiDSZipCode = coreProduct.custom.mfiDSZipCode;
                    	AddProductToBasketResult.ProductLineItem.custom.mfiDSZoneLineId = coreProduct.custom.mfiDSZoneLineId;
                        AddProductToBasketResult.ProductLineItem.custom.isDeliveryTier = true;
                        AddProductToBasketResult.ProductLineItem.setPriceValue(Number(finalShippingCost));
                        
                    }
                } catch (e) {
                    var error = e;
                    
               }
                
	            defaultShipment = cart.getDefaultShipment();
	            newShippingMethod = allShippingMethodsMap.get(SM_ID);
	            //defaultShipment.setShippingMethod(newShippingMethod);
	            //inMarketShipment.setShippingMethod(newShippingMethod);
	            var forceServiceCall = true;
	            mattressPipeletHelper.splitShipmentsByType(cart.object, forceServiceCall);
	            cart.removeEmptyShipments();
	            //ToDo - update shipping method in cart.
	           //cart.updateShipmentShippingMethod(defaultShipment.getID(), SM_ID, null, null);
	            cart.updateShipmentShippingMethod(inMarketShipment.getID(), SM_ID, null, null);
	            //cart.updateShipmentShippingMethod(defaultShipment.getID(), newShippingMethod.getID(), newShippingMethod, allShippingMethodsCollection);
	           // defaultShipment.setShippingMethod(newShippingMethod);
	            //cart.updateShipmentShippingMethod(inMarketShipment.getID(), newShippingMethod.getID(), newShippingMethod, allShippingMethodsCollection);
	            //inMarketShipment.setShippingMethod(newShippingMethod);
	           
	            cart.calculate();
            });
            if(params.format.stringValue == "ajax") {
	            var responseData = {
	            		shippingMethodUpdated: 'True',
	            		shippingCost: basket.adjustedShippingTotalNetPrice.value,
	            		newShippingMethodID: SM_ID
	            		};
	            		
	            require('app_storefront_controllers/cartridge/scripts/util/Response').renderJSON(responseData);
            } else {
            	return true;
            }
             
        } else {
            return false;
                }
    } else {
        return false;
	}
}


function updateCustomerInfo() {
    var params = request.httpParameterMap;
    var cart = app.getModel('Cart').get();
    var deliveryZoneChanged = false;
    var existingZip = session.custom.customerZip;
    
    session.custom.customerZip = params.zipCode.stringValue;
    //these two functions need session.custom.customerZip value so updating it temporarily
	var productsGeoAvailabilityMap = UtilFunctions.buildProductsGeoAvailabilityMap(cart.object);
	var allProductsGeoAvailable = UtilFunctions.checkAllProductsGeoAvailable(cart.object,productsGeoAvailabilityMap);
    
	if (allProductsGeoAvailable == false && !empty(cart)) {
    	//Out of market zipCode message handled in COShipping-CustomerInfo, thats why redirecting back
		session.custom.customerZip= existingZip; //revert the zipCode current value to the previous value
		var responseData = {
			 inMarketZip: allProductsGeoAvailable
			 };
        require('app_storefront_controllers/cartridge/scripts/util/Response').renderJSON(responseData);
    } else if (!empty(cart)) {
        if (!params.zipCode.empty && params.zipCode.stringValue.length == 5) {
        	session.custom.customerZip= existingZip; //revert the zipCode current value to the previous value
            var zipCode = params.zipCode.stringValue;
            var hasZIPchanged = session.custom.customerZip != zipCode;
            if (hasZIPchanged) {
                session.forms.singleshipping.shippingAddress.addressFields.postal.value = zipCode;
                var COBilling = require('~/cartridge/controllers/COBilling');
                deliveryZoneChanged = COBilling.IsDeliveryZoneChanged(zipCode);
                if (deliveryZoneChanged) {
                    var inMarketShipment = cart.getShipment('In-Market');
                    if (!empty(inMarketShipment)) {
                        Transaction.wrap(function () {
                            inMarketShipment.custom.deliveryDate = null;
                            inMarketShipment.custom.deliveryTime = null;
                            session.custom.deliveryDate = null;
                            session.custom.deliveryTime = null;
                            var pliIterator = inMarketShipment.getProductLineItems().iterator();
                            while (pliIterator.hasNext()) {
                                var pli = pliIterator.next();
                                pli.custom.deliveryDate = null;
                                pli.custom.InventLocationId = null;
                                pli.custom.secondaryWareHouse = null;
                                pli.custom.mfiDSZoneLineId = null;
                                pli.custom.mfiDSZipCode = null;
                                session.custom.deliveryZone = null;
                                var optionLineItems = pli.optionProductLineItems;
                                for (var j = 0; j < optionLineItems.length; j++) {
                                    var oli = optionLineItems[j];
                                    if (oli.productID != 'none') {
                                        oli.custom.InventLocationId = null;
                                    }
                                }
                            }
                        });
                    }
                }
           }
        }
        var shippingForm = session.forms.singleshipping.shippingAddress;
        shippingForm.addressFields.firstName.setValue(params.firstName);
        shippingForm.addressFields.lastName.setValue(params.lastName);
        shippingForm.addressFields.address1.setValue(params.address1);
        shippingForm.addressFields.address2.setValue(params.address2);
        shippingForm.addressFields.city.setValue(params.city);
        shippingForm.addressFields.postal.setValue(params.zipCode);
        shippingForm.addressFields.states.state.setValue(params.state);
        shippingForm.addressFields.phone.setValue(params.phone);
        shippingForm.email.emailAddress.setValue(params.email);
        var customerShippingAddress = session.forms.singleshipping.shippingAddress.addressFields;
        if (customerShippingAddress.address2.value == null) {
            customerShippingAddress.address2.setValue('');
        }
        
        var defaultDeliveryTier = "";
 	    if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
 		    var inMarketShipment = cart.object.getShipment('In-Market');
 			if(!empty(inMarketShipment)) {
 				defaultDeliveryTier = inMarketShipment.getShippingMethodID();
 			}
 	    }
 	    
        Transaction.wrap(function () {
        var defaultShipment, shippingAddress;
        defaultShipment = cart.getDefaultShipment();
        shippingAddress = cart.createShipmentShippingAddress(defaultShipment.getID());
        var shippingMethodID = defaultShipment.getShippingMethodID();
        
        shippingAddress.setFirstName(customerShippingAddress.firstName.value);
        shippingAddress.setLastName(customerShippingAddress.lastName.value);
        shippingAddress.setAddress1(customerShippingAddress.address1.value);
        shippingAddress.setAddress2(customerShippingAddress.address2.value);
        shippingAddress.setCity(customerShippingAddress.city.htmlValue);
        shippingAddress.setPostalCode(customerShippingAddress.postal.value); 
        shippingAddress.setStateCode(customerShippingAddress.states.state.value);
        shippingAddress.setPhone(customerShippingAddress.phone.value);
        shippingAddress.setCountryCode('US');
        cart.setCustomerEmail(session.forms.singleshipping.shippingAddress.email.emailAddress.value);
        cart.updateShipmentShippingMethod(defaultShipment.getID(), shippingMethodID, null, null);
        var forceServiceCall = true;
        mattressPipeletHelper.splitShipmentsByType(cart.object, forceServiceCall);
        cart.removeEmptyShipments();
        cart.calculate();
        });
        
        //set silver/gold/platinum shipping method based criteria
    	if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
    		defaultDeliveryTier = COShippingMethodController.SetDeliveryTierShippingMethod(defaultDeliveryTier);
 	    }
        
        session.custom.customerZip = customerShippingAddress.postal.value;
        var productsGeoAvailabilityMap = UtilFunctions.buildProductsGeoAvailabilityMap(cart.object);
    	var allProductsGeoAvailable = UtilFunctions.checkAllProductsGeoAvailable(cart.object,productsGeoAvailabilityMap);
        
        var responseData = {
        		shippingUpdated: 'Success',
        		inMarketZip: allProductsGeoAvailable,
        		deliveryZoneChanged: deliveryZoneChanged
        		};
        require('app_storefront_controllers/cartridge/scripts/util/Response').renderJSON(responseData);
    } else {
       response.redirect(URLUtils.https('Cart-Show'));
       return;
	}	
}


function continueToShipping(e) {
	var cart, formResult;
    cart = app.getModel('Cart').get();
    session.custom.checkoutStep = 'shipping';
    if(!empty(cart)) {
	    var singleShippingForm = app.getForm('singleshipping');
        var customerShippingAddress = session.forms.singleshipping.shippingAddress.addressFields;
        if (customerShippingAddress.address2.value == null) {
            customerShippingAddress.address2.setValue('');
        }
		formResult = singleShippingForm.handleAction({
		'savemobileab': function () {
			session.custom.customerZip = customerShippingAddress.postal.value;
			var productsGeoAvailabilityMap = UtilFunctions.buildProductsGeoAvailabilityMap(cart.object);
			var allProductsGeoAvailable = UtilFunctions.checkAllProductsGeoAvailable(cart.object,productsGeoAvailabilityMap);
		    if (allProductsGeoAvailable == false) {
		    	session.forms.singleshipping.shippingAddress.addressFields.city.clearFormElement();
		    	session.forms.singleshipping.shippingAddress.addressFields.states.state.clearFormElement();
		    	//Out of market zipCode message handled in COShipping-CustomerInfo, thats why redirecting back
		    	response.redirect(URLUtils.https('COShipping-CustomerInfo'));
				return;
		    } else {
		    	var defaultDeliveryTier = "";
		 	    if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
		 		    var inMarketShipment = cart.object.getShipment('In-Market');
		 			if(!empty(inMarketShipment)) {
		 				defaultDeliveryTier = inMarketShipment.getShippingMethodID();
		 			}
		 	    }
		 	    
		        //set shipping address
		    	Transaction.wrap(function () {
	                var defaultShipment, shippingAddress;
	                defaultShipment = cart.getDefaultShipment();
	                shippingAddress = cart.createShipmentShippingAddress(defaultShipment.getID());
	                var shippingMethodID = defaultShipment.getShippingMethodID();
	                
	                shippingAddress.setFirstName(customerShippingAddress.firstName.value);
	                shippingAddress.setLastName(customerShippingAddress.lastName.value);
	                shippingAddress.setAddress1(customerShippingAddress.address1.value);
	                shippingAddress.setAddress2(customerShippingAddress.address2.value);
	                shippingAddress.setCity(customerShippingAddress.city.htmlValue);
	                shippingAddress.setPostalCode(customerShippingAddress.postal.value);
	                shippingAddress.setStateCode(customerShippingAddress.states.state.value);
	                shippingAddress.setPhone(customerShippingAddress.phone.value);
	                shippingAddress.setCountryCode('US');
	                cart.setCustomerEmail(session.forms.singleshipping.shippingAddress.email.emailAddress.value);
	                cart.updateShipmentShippingMethod(defaultShipment.getID(), shippingMethodID, null, null);
	                var forceServiceCall = true;
	                mattressPipeletHelper.splitShipmentsByType(cart.object, forceServiceCall);
	                cart.removeEmptyShipments();
	                cart.calculate();
		    	});
		    	//set silver/gold/platinum shipping method based criteria
		    	if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
		    		defaultDeliveryTier = COShippingMethodController.SetDeliveryTierShippingMethod(defaultDeliveryTier);
		 	    }
		         //set billing address
				Transaction.wrap(function () {
			        var billingAddress = cart.createBillingAddress();
			        billingAddress.setFirstName(customerShippingAddress.firstName.value);
			        billingAddress.setLastName(customerShippingAddress.lastName.value);
					billingAddress.setAddress1(customerShippingAddress.address1.value);
					billingAddress.setAddress2(customerShippingAddress.address2.value);
					billingAddress.setCity(customerShippingAddress.city.htmlValue);
					billingAddress.setPostalCode(customerShippingAddress.postal.value);
					billingAddress.setStateCode(customerShippingAddress.states.state.value);
					billingAddress.setCountryCode('US');
					billingAddress.setPhone(customerShippingAddress.phone.value);
				});
				session.custom.customerZip = customerShippingAddress.postal.value;
				response.redirect(URLUtils.https('COShipping-GoToDeliveryPage'));
				return;
			}
		}
		});
    } else {
    	response.redirect(URLUtils.https('Cart-Show'));
        return;
	}
}

function goToDeliveryPage(){
    var cart = app.getModel('Cart').get();
    session.custom.checkoutStep = 'shipping';
    if(!empty(cart)) {
    	var params = request.httpParameterMap;
    	var timeoutCheck = (params.deliveryScheduleTimeout.submitted && params.deliveryScheduleTimeout.stringValue === 'true') ? true : false;
	
		var deliveryDatesArr;
		//avoid atp call if restricted zip entered
		if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule')) {
	        	var postalCode = session.forms.singleshipping.shippingAddress.addressFields.postal.value;
	        	deliveryDatesArr = storePicker.GetATPDeliveryDates(cart, postalCode);
	        	session.custom.deliveryDatesArr = deliveryDatesArr;
	        	var isMobileDevice = StringHelpers.isMobileDevice(request.httpUserAgent);
	        	//This code will run for moble only
	        	if (isMobileDevice) {
	            	UtilFunctions.setMobileDefaultDate(cart.object, deliveryDatesArr);
	        	}
	    	} else {
	        	deliveryDatesArr = mattressPipeletHelper.getDeliveryDates(cart);
	    }
		var pageMetaData = request.pageMetaData;
	    pageMetaData.title = Resource.msg('checkout.customer.delivery.meta.pagetitle', 'checkout', null);
        var defaultDeliveryTier = "";
	    if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
		    var inMarketShipment = cart.object.getShipment('In-Market');
			if(!empty(inMarketShipment)) {
				defaultDeliveryTier = inMarketShipment.getShippingMethodID();
			}
			defaultDeliveryTier = COShippingMethodController.SetDeliveryTierShippingMethod(defaultDeliveryTier);
	    }
	    var validShipmentsCount = UtilFunctions.validShipmentsCount(cart.object);
        app.getView({
	            ContinueURL: URLUtils.https('COShippingMethod-SingleShipping'),
	            Basket: cart.object,
                DeliveryDatesArr: deliveryDatesArr,
                DefaultDeliveryTier: defaultDeliveryTier,
                TimeoutCheck: timeoutCheck,
                ValidShipmentsCount: validShipmentsCount
	    }).render('checkout/shipping/shippingmethodstep_mob_ab');
	} else {
    	response.redirect(URLUtils.https('Cart-Show'));
        return;
	}
}

/*
* Module exports
*/

/*
* Web exposed methods
*/
/** Starting point for the single shipping scenario.
 * @see module:controllers/COShipping~start */
exports.Start = guard.ensure(['https'], start);
/** Selects a shipping method for the default shipment.
 * @see module:controllers/COShipping~selectShippingMethod */
exports.SelectShippingMethod = guard.ensure(['https', 'get'], selectShippingMethod);
/** Determines the list of applicable shipping methods for the default shipment of the current basket.
 * @see module:controllers/COShipping~updateShippingMethodList */
exports.UpdateShippingMethodList = guard.ensure(['https', 'get'], updateShippingMethodList);
/** Determines the list of applicable shipping methods for the default shipment of the current customer's basket and returns the response as a JSON array.
 * @see module:controllers/COShipping~getApplicableShippingMethodsJSON */
exports.GetApplicableShippingMethodsJSON = guard.ensure(['https', 'get'], getApplicableShippingMethodsJSON);
/** Renders a form dialog to edit an address.
 * @see module:controllers/COShipping~editAddress */
exports.EditAddress = guard.ensure(['https', 'get'], editAddress);
/** Updates shipping address for the current customer with information from the singleshipping form.
 * @see module:controllers/COShipping~updateAddressDetails */
exports.UpdateAddressDetails = guard.ensure(['https', 'get'], updateAddressDetails);
/** Form handler for the singleshipping form.
 * @see module:controllers/COShipping~singleShipping */
exports.SingleShipping = guard.ensure(['https', 'post'], singleShipping);
/** Form handler for the shippingAddressForm.
 * @see module:controllers/COShipping~editShippingAddress */
exports.EditShippingAddress = guard.ensure(['https', 'post'], editShippingAddress);
exports.GetDataFromZip = guard.ensure(['https', 'get'], getDataFromZip);
exports.KeepAddress = guard.ensure(['https', 'post'], keepAddress);

/*
 * Local methods
 */
exports.PrepareShipments = prepareShipments;
exports.UpdateDeliveryDateSelection = updateDeliveryDateSelection;
exports.HandleShippingSettings = handleShippingSettings;
exports.CustomerInfo = guard.ensure(['https', 'get'], customerInfo);
exports.ContinueToShipping = guard.ensure(['https'], continueToShipping);
exports.GoToDeliveryPage = guard.ensure(['https'], goToDeliveryPage);
exports.CustomerInfoModal = guard.ensure(['https', 'get'], customerInfoModal);
exports.UpdateCustomerInfo = guard.ensure(['https'], updateCustomerInfo);
exports.SetDeliverTier = guard.ensure(['https'], setDeliverTier);
