'use strict';

/**
 * Controller for the default single shipping scenario.
 * Single shipping allows only one shipment, shipping address, and shipping method per order.
 *
 * @module controllers/COShippingMethod
 */

/* API Includes */
var Resource = require('dw/web/Resource');
var Site = require('dw/system/Site');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;
var storePicker = require('app_storefront_controllers/cartridge/controllers/StorePicker');
var StringUtils = require('dw/util/StringUtils');
var COShippingController = require('~/cartridge/controllers/COShipping');
var emailHelper = require("app_storefront_controllers/cartridge/scripts/util/SFEmailSubscriptionHelper").SFEmailSubscriptionHelper;
var pipeletHelper = require('bc_sleepysc/cartridge/scripts/sleepys/util/SleepysPipeletsHelper').SleepysPipeletHelper;
var StringHelpers = require('app_storefront_core/cartridge/scripts/util/StringHelpers');
var UtilFunctions = require('~/cartridge/scripts/util/UtilFunctions').UtilFunctions;
var COShippingMethodController = require('app_mattressfirm_storefront/cartridge/controllers/COShippingMethod');
/** 
 * @function
 * @description This function accepts zipcode
 * it calculates the shipping and tax for the given zipcode
 * @param params (zipcode)
 */
function calculateShippingAndTaxForZip(zipCode)
{
	var cart = app.getModel('Cart').get();
	
	if(!empty(cart) && cart != null){
		Transaction.wrap(function () {
	        var defaultShipment, shippingAddress;
	        defaultShipment = cart.getDefaultShipment();
	      	shippingAddress = cart.createShipmentShippingAddress(defaultShipment.getID());
	      	var shippingMethodID = defaultShipment.getShippingMethodID();	       
	        var citiesAndStateJSON = mattressPipeletHelper.getCitiesAndStatesForZipCode(zipCode);   
	        var city = "";
	        var state = "";
	    	if (!empty(citiesAndStateJSON)) {
	    		var cityData = JSON.parse(citiesAndStateJSON);   
	            for (var i = 0; i < cityData.cities.length; i++){
	                var data = cityData.cities[i];
	                if (!empty(data.state)) {
	                	state = data.state.toUpperCase();
	                	city =  data.city;
	                	break;
	        		}
	            }
	            
	            if(!empty(state))
		        {
		        	shippingAddress.setCity(city);
		 	        shippingAddress.setPostalCode(zipCode);
		 	        shippingAddress.setStateCode(state);
		 	        shippingAddress.setCountryCode('US');				       
		 	        cart.updateShipmentShippingMethod(defaultShipment.getID(), shippingMethodID, null, null);
		 	        var forceServiceCall = true;
		            mattressPipeletHelper.splitShipmentsByType(cart.object, forceServiceCall);
		        }
			}
	    	else {
	    		shippingAddress.setCity(city);
	 	        shippingAddress.setPostalCode(zipCode);
	 	        shippingAddress.setStateCode(state);
	 	        shippingAddress.setCountryCode('US');				       
	 	        cart.updateShipmentShippingMethod(defaultShipment.getID(), shippingMethodID, null, null);
	 	        var forceServiceCall = true;
	            mattressPipeletHelper.splitShipmentsByType(cart.object, forceServiceCall);
	    	}        
			
			cart.removeEmptyShipments();
    		cart.calculate();
	 
	    });
	}	
	 return;
}

/**
 * @function 
 * @description This function is called when user gets authorisation from paypal
 * it shows up the review payment page for paypal
 */
function start() {
	session.custom.synchronyDigitalBuyOrderNo = null;
	session.custom.amazonGuestCheckout = false;
	session.custom.deliveryDatesArr = null;
	
    var cart, pageMeta, homeDeliveries, dateListObject;
    cart = app.getModel('Cart').get();
    var params = request.httpParameterMap;
    session.custom.payPalSlotTimeOutCheck = (params.deliveryScheduleTimeout.submitted && params.deliveryScheduleTimeout.stringValue === 'true') ? true : false
    if (cart) {
    	var deliveryDate = session.custom.deliveryDate;
		var deliveryTime = session.custom.deliveryTime;
	    session.forms.cart.coupons.copyFrom(cart.object.couponLineItems);
		
	    if (empty(session.custom.deliveryOptionChoice)) {
	        session.custom.deliveryOptionChoice = 'shipped';
	    }

        if (cart.getDefaultShipment().getShippingAddress()) {
            app.getForm(session.forms.singleshipping.shippingAddress.addressFields).copyFrom(cart.getDefaultShipment().getShippingAddress());
            app.getForm(session.forms.singleshipping.shippingAddress.addressFields.states).copyFrom(cart.getDefaultShipment().getShippingAddress());
            app.getForm(session.forms.singleshipping.shippingAddress).copyFrom(cart.getDefaultShipment());
        }
        if (cart.getBillingAddress()) {
            app.getForm(session.forms.billing.billingAddress.addressFields).copyFrom(cart.getBillingAddress());
            app.getForm(session.forms.billing.billingAddress.addressFields.states).copyFrom(cart.getBillingAddress());
            app.getForm(session.forms.billing.billingAddress).copyFrom(cart.getBillingAddress());
        }

        session.forms.singleshipping.shippingAddress.addressFields.phone.value = '';
        session.forms.singleshipping.shippingAddress.email.emailAddress.value = cart.object.customerEmail; //fix
        session.forms.billing.billingAddress.email.emailAddress.value = cart.object.customerEmail;
        session.forms.singleshipping.shippingAddress.useAsBillingAddress.value = false;
        session.custom.customerZip = session.forms.singleshipping.shippingAddress.addressFields.postal.value;

		var paypalZip = session.forms.singleshipping.shippingAddress.addressFields.postal.value.split('-');
		session.forms.singleshipping.shippingAddress.addressFields.postal.value = paypalZip[0];
		session.custom.customerZip = paypalZip[0];

		//Restrict Hawaii, Alaska or Puerto Rico checkout
		var isRestrictedCheckout = mattressPipeletHelper.getIsRestrictedCheckout(session.custom.customerZip);
		var inMarketShipment = cart.object.getShipment('In-Market');
		if (isRestrictedCheckout.status && !empty(inMarketShipment)) {
			response.redirect(URLUtils.https('Cart-Show', 'restrictpaypal', 'true'));
			return;
		}

		//Restrict out of market zip codes
		var productsGeoAvailabilityMap = UtilFunctions.buildProductsGeoAvailabilityMap(cart.object);
		var allProductsGeoAvailable = UtilFunctions.checkAllProductsGeoAvailable(cart.object,productsGeoAvailabilityMap);
		if (allProductsGeoAvailable == false) {
			session.forms.singleshipping.shippingAddress.addressFields.city.clearFormElement();
			session.forms.singleshipping.shippingAddress.addressFields.states.state.clearFormElement();
			//Out of market zipCode message handled in COShipping-CustomerInfo, thats why redirecting back
			response.redirect(URLUtils.https('Cart-Show', 'paypaloom', 'true'));
			return;
		}

        Transaction.wrap(function () {
            cart.getDefaultShipment().getShippingAddress().setPhone('');
       });
	   var defaultDeliveryTier = "";
	   if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
		   var inMarketShipment = cart.object.getShipment('In-Market');
		   if(!empty(inMarketShipment)) {
			   defaultDeliveryTier = inMarketShipment.getShippingMethodID();
		   }
	   }
        
        calculateShippingAndTaxForZip(session.custom.customerZip);
        
            
        if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule')) {
        	var dateString = 'null';
			var timeString = 'null';
			var zoneId = 'null';
			var zonZipCode = 'null';
        	var postalCode = session.forms.singleshipping.shippingAddress.addressFields.postal.value;
        	session.custom.customerZip = postalCode;
        	var deliveryDatesArr = storePicker.GetATPDeliveryDates(cart, postalCode);
        	session.custom.deliveryDatesArr = deliveryDatesArr;
        	
			if (!empty(deliveryDatesArr)) {						
				var slotAvailable = 0;
				for (var i = 0; i < deliveryDatesArr.length; i++) {	
					
					if (slotAvailable > 0) {
						break;
					}
					var key = new Date(deliveryDatesArr[i]).toDateString().replace(' ', '', 'g');
					var deliveryTimeSlots = session.custom.deliveryDates[key]['timeslots'];	
					
					for (var j = 0; j < deliveryTimeSlots.length; j++) {    						
						if (deliveryTimeSlots[j]['available'] == 'yes' && deliveryTimeSlots[j]['slots'] > 0) {
							var startTime = deliveryTimeSlots[j]['startTime'];
                			var endTime = deliveryTimeSlots[j]['endTime'];
    						deliveryTime = StringUtils.formatDate(startTime, "h:mma").toString() +'-' + StringUtils.formatDate(endTime, "h:mma").toString();
    						deliveryTime = deliveryTime.replace('PM',' pm', 'g').replace('AM',' am', 'g');
    						deliveryDate = deliveryDatesArr[i];
    						
    						
    						
    						dateString = !empty(deliveryDate) ? new Date(deliveryDate).toISOString() : 'null';
    						timeString = !empty(startTime) && !empty(endTime) ? deliveryTime : 'null';
    						zoneId = !empty(deliveryTimeSlots[j]['mfiDSZoneLineId'])  ? deliveryTimeSlots[j]['mfiDSZoneLineId'] : 'null';
    						zonZipCode = !empty(deliveryTimeSlots[j].mfiDSZipCode) ? deliveryTimeSlots[j].mfiDSZipCode : 'null';
    						
    				
    						session.custom.deliveryDate = deliveryDate;
    						session.custom.deliveryTime = deliveryTime.replace('PM',' pm', 'g').replace('AM',' am', 'g');
    						session.custom.deliveryZone = deliveryTimeSlots[j]['mfiDSZoneLineId'];
    						slotAvailable++;
    						break;
						}
            		}
					
				}
			}
			else{
				session.custom.deliveryDate =  '';
				session.custom.deliveryTime = '';
				deliveryDate = '';
				deliveryTime = '';
				
			}
   		 var isOnlyRedCarpetItemsInCart = !empty(cart.object) ? mattressPipeletHelper.getOrderIsRedCarpetStatus(cart.object) : false;
         if (isOnlyRedCarpetItemsInCart) {
			var addressAtpInfoAttr  =  dateString + '|' + timeString +'|' + zoneId +'|'+ zonZipCode;
			//Set Delivery Zip Slots Tracking on Page Load
			if(empty(cart.object.custom.defaultDeliveryDateTimeZoneZip)) {
				//2049-12-31T06:11:55.699Z|8:00am-8:00pm|DZL002446249|77025
				Transaction.wrap(function () {
					cart.object.custom.defaultDeliveryDateTimeZoneZip = addressAtpInfoAttr;
				});
			}
				//2049-12-31T06:11:55.699Z|8:00am-8:00pm|DZL002446249|77025
		    Transaction.wrap(function () {
		    	cart.object.custom.selectedDeliveryDateTimeZoneZip = addressAtpInfoAttr;
		    });
         }
		    
        } else {
			var deliveryDatesArr = mattressPipeletHelper.getDeliveryDates(cart); 
		}
			
        var pageMetaData = request.pageMetaData;
    	pageMetaData.title = Resource.msg('checkout.paypal.metadata.title', 'checkout', null);
		defaultDeliveryTier = COShippingMethodController.SetDeliveryTierShippingMethod(defaultDeliveryTier);
		var validShipmentsCount = UtilFunctions.validShipmentsCount(cart.object);
		app.getView({
            ContinueURL: URLUtils.https('COPayPal-SubmitOrder'),
            Basket: cart.object,
			DeliveryDate: deliveryDate,
			DeliveryTime: deliveryTime,
			DeliveryDatesArr: deliveryDatesArr,
			DefaultDeliveryTier: defaultDeliveryTier,
			IsOnlyRedCarpetItemsInCart: isOnlyRedCarpetItemsInCart,
			ValidShipmentsCount: validShipmentsCount,
            isRestrictedCheckout: isRestrictedCheckout.status,
            isPaypal: true
        }).render('checkout/shipping/shippingmethodsteppaypal');
	    } else {
	        app.getController('Cart').Show();
	        return;
	    }

}

/** 
 * @function
 * @description This function accepts email address
 * it subcribes the given email for getting updates
 * @param params (email)
 */
function subscribeEmail(emailAddress) {
	  var gaCookie = request.getHttpCookies()['_ga'];
	  var gclid;
	  if (gaCookie) {
	  	gclid = gaCookie.value;
	  }
	var emailParams = {
			emailAddress: emailAddress, 
			zipCode: session.forms.singleshipping.shippingAddress.addressFields.postal.value, 
			leadSource: 'checkout', 
			siteId: dw.system.Site.getCurrent().getID(), 
			optOutFlag: !session.forms.singleshipping.shippingAddress.addToSubscription.value,
			gclid: gclid,
			dwsid: session.sessionID
	};
    
	var returnResult = emailHelper.sendSFEmailInfo(emailParams);
	if (returnResult.Status == 'SERVICE_ERROR'){
		var returnResult = emailHelper.sendFailSafe(emailParams, returnResult.ErrorCode);
	}
}

/**
 * @function 
 * @description This function is called when user places an order for paypal
 * it submits the order at demandware side
 */
function submitOrder() {
    var singleShippingForm = app.getForm('singleshipping');
    singleShippingForm.handleAction({
        save: function () {
        	
            var cart = app.getModel('Cart').get();
            var isMobile = request.httpParameterMap.isMobile.value;
            if(!empty(isMobile) && isMobile == "true") {
                var deliveryDate = request.httpParameterMap.deliveryDate.value;
                var deliveryTime = request.httpParameterMap.deliveryTime.value;
                
        		session.forms.singleshipping.shippingAddress.addressFields.deliveryDate.htmlValue = deliveryDate;
        		session.forms.singleshipping.shippingAddress.addressFields.deliveryDate.value = deliveryDate;
        		session.forms.singleshipping.shippingAddress.addressFields.deliveryTime.htmlValue = deliveryTime;
        		session.forms.singleshipping.shippingAddress.addressFields.deliveryTime.value = deliveryTime;
        		session.forms.singleshipping.shippingAddress.scheduleWithRep.value = 'scheduledelivery'
            }
            
            if (cart) {
            	try {
            		var paypalPhoneNumber = StringHelpers.formatAXPhoneNumber(app.getForm('singleshipping').object.shippingAddress.addressFields.phone.value);
            		var isEmailSubscribed = app.getForm('singleshipping').object.shippingAddress.addToSubscription.value;
            		session.forms.singleshipping.shippingAddress.addressFields.phone.value = paypalPhoneNumber;

            		Transaction.wrap(function () {
            			var shipments =  cart.getShipments();
                		for(var i = 0; i < shipments.length ; i++){
            		        shipments[i].getShippingAddress().setPhone(paypalPhoneNumber);
                		}
                		
                		cart.getBillingAddress().setPhone(paypalPhoneNumber);
            			cart.getBillingAddress().setCountryCode('US');
            		});
            		
            		session.forms.singleshipping.shippingAddress.addToSubscription.value = isEmailSubscribed;
            		
            		if(isEmailSubscribed) {
            			subscribeEmail(session.forms.singleshipping.shippingAddress.email.emailAddress.value);
            		}
            		
            		COShippingController.UpdateDeliveryDateSelection(cart);
            		 var isOnlyRedCarpetItemsInCart = !empty(cart.object) ? mattressPipeletHelper.getOrderIsRedCarpetStatus(cart.object) : false;
                     var addressAtpInfoAttr = '';
                     if (isOnlyRedCarpetItemsInCart) {
         	            addressAtpInfoAttr = UtilFunctions.getAddressAtpInfoAttr_FromShipment(cart);
         	            //2049-12-31T06:11:55.699Z|8:00am-8:00pm|DZL002446249|77025
         	            Transaction.wrap(function () {
                        	cart.object.custom.confirmationDeliveryDateTimeZoneZip = addressAtpInfoAttr;
                        	cart.object.custom.addressesDeliveryDateTimeZoneZip = addressAtpInfoAttr;
            		    	cart.object.custom.selectedDeliveryDateTimeZoneZip = addressAtpInfoAttr;
                         });
                    }
            		// Mark step as fulfilled
            	    app.getForm('billing').object.fulfilled.value = true;

            	    
            	    // A successful billing page will jump to the next checkout step.               
            	    var COSummary = require('~/cartridge/controllers/COSummary');
            	    COSummary.Submit(); 

                } catch (e) {
                    return;
                }
            } else {
            	 response.redirect(URLUtils.https('Cart-Show'));
                 return;
            }
        },
        error: function () {
            response.redirect(URLUtils.https('Cart-Show'));
            return;
        }
    });
}

/*
* Module exports
*/

/*
* Web exposed methods
*/
/** Starting point for the single shipping scenario.
 * @see module:controllers/COShippingMethod~start */
exports.Start = guard.ensure(['https'], start);
exports.SubmitOrder = guard.ensure(['https', 'post'], submitOrder);

