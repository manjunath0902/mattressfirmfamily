(function(app, $) {
	var _app = app;
	
	var obj = $('#emailSubscription');
	var height = parseInt(obj.data('height'));
	var width = parseInt(obj.data('width'));
	var url = obj.data('url');
	var wait = parseInt(obj.data('wait')) * 1000;
	var cookieExpiration = parseInt(obj.data('cookieExpiration'));
	var siteId = obj.data('siteId');
	var cookieName = siteId + '_email_sub';
	
	var customerAuthenticated = obj.data('customerAuthenticated') == true;
	var customerSubscribed = obj.data('customerSubscribed') == true;
	var callFromEmail = obj.data('callFromEmail') == true;
	
	if (customerSubscribed || callFromEmail) {
		
		$.cookie(cookieName, '1', {expires : 365, path : '/'});
		
	} else if ( $.cookie(cookieName) == null && (!customerAuthenticated || !customerSubscribed)) {

		var options = $.extend(true, {}, app.dialog.settings, {
			autoOpen : true,
			height : height,
			width : width,
			title : 'Email Subscription',
			resizable : false
		});
		
		setTimeout(function() {
			app.dialog.open({url : url, options : options});

			$('.ui-widget-overlay').on('click', function() {
				$('body').find('.ui-dialog-titlebar-close').trigger('click');
			});

		}, wait);
        $.cookie(cookieName, '1', {expires : cookieExpiration, path : '/'});
    }

}(window.app = window.app || {}, jQuery));

function submitEmailSubscriptionForm() {

	var _email = $('#email').val();
	var _pageSource = $('#pageSourceModal').val();

	var obj = $('#emailSubscription');
	var siteId = obj.data('siteId');
	var cookieName = siteId + '_email_sub';

	if(/^[a-zA-Z0-9]+(?:(\.|_)[A-Za-z0-9!#$%&'*+/=?^`{|}~-]+)*@(?!([a-zA-Z0-9]*\.[a-zA-Z0-9]*\.[a-zA-Z0-9]*\.))(?:[A-Za-z0-9](?:[a-zA-Z0-9-]*[A-Za-z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/.test(_email)){
		$.ajax({
			url: app.urls.emailSignup,
			data: {ajaxEmail : _email, modal : true, pageSource : _pageSource},
			success: function(html){
				$.cookie(cookieName, '1', {expires : 365, path : '/'});
				$('#email-subscription-content').html(html);
				$('#startShoppingBtn').show();
			}
		});
	} else {
		$('.email-container').find('.error').remove().end().append(
			$('<div/>', {"class":"error"}).html('Please enter a valid email address')
		);
	}
}