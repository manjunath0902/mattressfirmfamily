# CCH SureTax SFRA Integration - Salesforce Commerce Cloud
SFCC/DWRE Link Cartridge for CCH SureTax SFRA from Wolters Kluwer.

#### Pre-Requisite

*Follow all directions for int_suretax installation (Skip SiteGenesis specific steps if only installing for SFRA)*

#### Setup

1. Import the "int_suretax_sfra" cartridge into the Salesforce Commerce Cloud Studio Workspace.
    1. Open Salesforce Commerce Cloud Studio
    2. Click File -> Import -> General -> Existing Projects Into Workspace
    3. Browse to the directory where you saved the "int_suretax" cartridge.
    4. Click Finish.
    5. Click OK when prompted to link the cartridge to the sandbox and upload.

#### Configuration 
1. Add “int_suretax_sfra” to the effective cartridge path
    1. Log into the Salesforce Commerce Cloud Business Manager.
    2. Click Administration -> Sites -> Manage Sites
    3. Select the desired site
    4. Click on the Settings tab.
    5. Add "int_suretax_sfra:" to the start of the "Cartridges" field (left of app_storefront_base).
    6. Click Apply


#### Custom Code for SFRA
The int_suretax_sfra cartridge uses int_suretax, hooks, and SFRA controller override.

This cartridge (int_suretax_sfra) can be used ‘as is’ if it is installed to the left in the cartridge path, but below are changes that can be made to your custom cartridge if needed.

##### Hooks:

Hooks are used in the following:
* dw.order.calculatetax
* dw.order.calculate
* dw.order.calculateShipping

The above hooks use an updated version of calculate.js found in ‘int_suretax_sfra/cartridge/scripts/hooks/cart/calculate.js’ to override the same file in app_storefront_base.

Custom code is found in 2 places in calculate.js:

1. Around line 23 of calculate.js, add require line for "var SureTax..." (see code below):
```Javascript

// module
var SureTax = require("int_suretax/cartridge/scripts/suretax/SureTax");

```

2. Around line 86 in Calculate Tax section of calculate.js add lines for "SURE TAX CALCULATION STARTS" (see code below):

```Javascript
    // ===================================================
    // =====         CALCULATE TAX                   =====
    // ===================================================
    //======= SURE TAX CALCULATION STARTS ============
    if (dw.system.Site.getCurrent().getCustomPreferenceValue('STEnable')) {
    	var customerNo = "";
    	if (basket && basket.customer && basket.customer.profile) {
    		customerNo = basket.customer.profile.customerNo;
    	}
        SureTax.calculateTax(basket, null, customerNo, null, null,'SFRA');
    //======= SURE TAX CALCULATION ENDS ============
    } else {
    	HookMgr.callHook('dw.order.calculateTax', 'calculateTax', basket);
    }

    // ===================================================
    // =====         CALCULATE BASKET TOTALS         =====
    // ===================================================

    basket.updateTotals();

    // ===================================================
    // =====            DONE                         =====
    // ===================================================
```

**Controllers:**

PlaceOrder function in CheckoutServices.js controller in app_storefront_base is replaced by custom code to finalize invoices in CCH SureTax after placing order.  Code for replacing PlaceOrder function is found in ‘int_suretax_sfra/cartridge/controllers/CheckoutServices.js’.  This will work without any modifications to your cartridge unless the same function is replaced already.  If you need to create custom code for your cartridge, you will need to follow below directions.

Custom Code differences between int_suretax_sfra’s CheckoutServices.js and app_storefront_base’s version are found in 2 places (code specific to PlaceOrder function ‘replace’ has been left out in below examples, but complete code can be seen in CheckoutServices.js:

1. int_suretax_sfra’s CheckoutServices.js has 2 require lines at the top of the file for "var dwLogger..." and "var SureTax..." (see code below):

```Javascript

var dwLogger = require("dw/system").Logger;
var SureTax = require("int_suretax/cartridge/scripts/suretax/SureTax");

```

2. int_suretax_sfra’s CheckoutServices.js, see the else statement after ‘var placeOrder Result (see code below):

```Javascript
    // Places the order
    var placeOrderResult = COHelpers.placeOrder(order, fraudDetectionStatus);
    if (placeOrderResult.error) {
        res.json({
            error: true,
            errorMessage: Resource.msg('error.technical', 'checkout', null)
        });
        return next();
    } else {
    	// SURE TAX FINALIZE start 
    	// this is needed here, In SG we have finalize after getting placeOrderResult
    	// no error with order place - so update sure tax transaction
        Transaction.wrap(function () { 
        	var ConfigUtils = new (require("int_suretax/cartridge/scripts/util/ConfigUtils"));
        	var customerNo ="";
        	if (order.customer && order.customer.profile) {
        		customerNo = order.customer.profile.customerNo;
        	}
        	SureTax.calculateTax(currentBasket, ConfigUtils.settings.Finalize, customerNo, order.orderNo, order,'SFRA'); 
        });
    	// SURE TAX FINALIZE end
    }
```