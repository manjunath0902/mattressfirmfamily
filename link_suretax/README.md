# CCH SureTax Integration - Salesforce Commerce Cloud
SFCC/DWRE Link Cartridge for CCH SureTax from Wolters Kluwer for SGJC (SiteGenesis) and SFRA (StoreFront Reference Architecture).

#### Installation for SiteGenesis

1. Follow directions in 'cartridge/int_suretax/README.md' or 'documentation/CCH SureTax Integration Guide.docx' (Ignore SFRA specific steps)

#### Installation for SFRA

1. Follow directions in 'cartridge/int_suretax/README.md' or 'documentation/CCH SureTax Integration Guide.docx' (Ignore SiteGenesis specific steps)
2. Follow directions in cartridge/int_suretax_sfra/README.md or 'documentation/CCH SureTax SFRA Integration Guide.docx'.

#### Installation for SFRA and SiteGenesis

1. Follow directions in 'cartridge/int_suretax/README.md' or 'documentation/CCH SureTax Integration Guide.docx' (Include SiteGenesis specific steps for your SiteGenesis based sites)
2. Follow directions in cartridge/int_suretax_sfra/README.md or 'documentation/CCH SureTax SFRA Integration Guide.docx' (For your SFRA based sites).

#### Known Issues and Limitations
1. N/A