# README 

This repository is digested by the [sleepys](/mediahive/sleepys/wiki/Development_Workflow/#markdown-header-1800mattress) repository as a git-submodule; the steps that were taken to achieve this (which should not need to be repeated) were:

```
$ cd sleepys
$ git checkout development
$ git submodule add git@bitbucket.org:mediahive/1800mattress.git cartridges/app_1800mattress
$ git add .gitmodules cartridges/app_1800mattress
$ git commit -am "Add app_1800mattress cartridge as git-submodule"
$ git push
```

Note: With the addition of the 1800mattress submodule the Jenkins CI configuration for the sleepys job had to be updated to include a new "Invoke Ant" build step with a `sass.compile` target for the app_1800mattress build file.