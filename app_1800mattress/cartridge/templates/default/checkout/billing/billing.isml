<isdecorate template="checkout/pt_checkout">
<isinclude template="util/modules"/>

    <iscomment>
        This template visualizes the shipping address, billing and payment method.
    </iscomment>

    <iscomment>Report this checkout step</iscomment>
    <isreportcheckout checkoutstep="4" checkoutname="${'Billing'}"/>

    <iscomment>Checkout header </iscomment>
    <div>
		<h2 class="page-title">${Resource.msg('checkout.title','checkout',null)}</h2>
		<div class="already-login">
	     <isif condition="${!customer.authenticated}">
	        <span><span class="icon-arrow_forward"></span>${Resource.msg('opc.checkout.alreadylogin','checkout',null)}</span>
	        <a href="${URLUtils.url('Login-Show')}" id="alreadycustomer" 
	            title="Login">
	            ${Resource.msg('opc.checkout.login','checkout',null)}
	        </a>
	     <iselse>
	     ${Resource.msgf('opc.welcome.login','checkout',null, customer.profile.firstName, customer.profile.lastName)}
	     </isif>   
	    </div>
	</div>
	
	<iscomment>Added for PayPal</iscomment>
    <isif condition="${!empty(pdict.BillingError)}">
            <div class="error-form">
                <isprint value="${pdict.BillingError}"/>
            </div>
        <iselse>
            <div class="error-form" style="display:none;"></div>
    </isif>
    
    <isif condition="${!empty(pdict.CurrentHttpParameterMap.error.value) && pdict.CurrentHttpParameterMap.error.value == 1}">
            <div class="error-form">
                ${Resource.msg('synchrony.callBackError', 'synchrony', null)}
            </div>
        <iselse>
            <div class="error-form" style="display:none;"></div>
    </isif>
    <isif condition="${pdict.PlaceOrderError != null}">
		<div class="error-form">
			<div class="error-form-inside">
				${Resource.msg(pdict.PlaceOrderError.code,'checkout',null)}
			</div>
		</div>
	</isif>
	<form action="${URLUtils.continueURL()}" method="post" id="${pdict.CurrentForms.billing.htmlName}" class="checkout-billing address">
		<div>	
			<div class="customer-info">
				
				<isset name="gcPITotal" value="${0}" scope="pdict"/>
				<isset name="OrderTotal" value="${pdict.Basket.totalGrossPrice.value}" scope="pdict"/>
				
				<isinclude template="checkout/billing/paymentmethods"/>
				
				<input type="hidden" name="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName}" />
				<div class="label-above">
					<isbonusdiscountlineitem p_alert_text="${Resource.msg('billing.bonusproductalert','checkout',null)}" p_discount_line_item="${pdict.BonusDiscountLineItem}"/>
				</div>
				 
				<legend>
					<span class="title">${Resource.msg('billing.addressheader','checkout',null)}</span>
					<span class="label"><input type=checkbox id="useShipping" tabindex="1">${Resource.msg('billing.useshippingaddress','checkout',null)}</span>
				</legend>
				<div class="billing-info">
			    	<div class="form-inline two-fields">
				    	<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.firstName}" type="input" rowclass="one" attribute1="tabindex" value1="1"/>
				    	<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.lastName}" type="input" rowclass="two" attribute1="tabindex" value1="1"/>
			    	</div>
			
			    	<div class="form-inline two-fields">
						<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.phone}" type="input" rowclass="one" attribute1="tabindex" value1="1"/>
						<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.mobilePhone}" xhtmlclass="phone" type="input" rowclass="two" attribute1="tabindex" value1="1"/>
					</div>

			    	<isif condition="${pdict.CurrentCustomer && pdict.CurrentCustomer.profile}">
						<isscript>
							pdict.CurrentForms.singleshipping.shippingAddress.email.emailAddress.htmlValue = pdict.CurrentCustomer.profile.email;
						</isscript>
					</isif>
						 	    
						<isif condition="${!empty(pdict.CurrentCustomer.authenticated) && pdict.CurrentCustomer.authenticated == true}"> 
								<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.email.emailAddress}" xhtmlclass="email" type="input" attribute1="disabled" value1="disabled" attribute2="tabindex" value2="1"/>
							<iselse>
								<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.email.emailAddress}" xhtmlclass="email" type="input" attribute1="tabindex" value1="1"/>
						</isif>			
					
					<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.companyName}" type="input" attribute1="tabindex" value1="1"/>
			
					<div class="addresswrapper">
							
				    	<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.address1}" type="input" attribute1="tabindex" value1="1"/>
				    	<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.address2}" type="input" attribute1="tabindex" value1="1"/>
				
				    	<div class="form-inline three-fields clearfix">
				
							<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.zip}" rowclass="one" type="input" attribute1="tabindex" value1="1"/>
						 	<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.city}" rowclass="one city-text-col" type="input" xhtmlclass="city" attribute1="tabindex" value1="1"/>			  	
							<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.states.state}" rowclass="one state-col hidden" type="select" xhtmlclass="state"  attribute1="tabindex" value1="1"/>		  	
							<input class="country" type="hidden" name="${pdict.CurrentForms.billing.billingAddress.addressFields.country.htmlName}" value="US" />
							
				    	</div>
					</div>
				</div>
			</div>
			<iscomment><a class="checkout-button button black rounded back pull-left" href="${URLUtils.https('COShipping-Start')}" title="Back to Shipping"> Back</a></iscomment>
			<div>	
				<button class="checkout-button" type="submit" name="${pdict.CurrentForms.billing.save.htmlName}" value="${Resource.msg('global.continuecheckoutbrief','locale',null)}" tabindex="1"><span>${Resource.msg('global.continuetorevieworder','locale',null)} </span></button>
			</div>
	
		</div>
	
		<input type="hidden" name="${pdict.CurrentForms.billing.secureKeyHtmlName}" value="${pdict.CurrentForms.billing.secureKeyValue}"/>
	
	</form>
	
	<isscript>
		importScript("util/ViewHelpers.ds");
		var stateForm = pdict.CurrentForms.billing.billingAddress.addressFields.states;
		var countryField = pdict.CurrentForms.billing.billingAddress.addressFields.country;
		var countries = ViewHelpers.getCountriesAndRegions(countryField, stateForm, "forms");
		var json = JSON.stringify(countries);
	</isscript>
	<script>if (window.app) {app.countries = <isprint value="${json}" encoding="off"/>;}</script>
	
	<isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('CsDeviceFingerprintEnabled')}">
		<isinclude url="${URLUtils.url('Cybersource-IncludeDigitalFingerprint')}"/>
	</isif>

</isdecorate>