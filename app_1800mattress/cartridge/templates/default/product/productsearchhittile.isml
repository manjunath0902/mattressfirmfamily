<iscache type="relative" hour="24" varyby="price_promotion"/>
<iscomment>
	This template renders a product in the search result using a ProductSearchHit object.
</iscomment>

<isif condition="${!empty(pdict.Product) && pdict.Product != null }">

	<iscomment>Set the product of the product search hit locally for reuse</iscomment>
	<isset name="Product" value="${pdict.Product}" scope="page"/>
	<isset name="OrgProduct" value="${null}" scope="page"/>
	
	<iscomment>
		Get the colors selectable from the current product master or variant range if we
		need to determine them based on a search result.
	</iscomment>
	<isscript>
		var selectableColors : dw.util.Collection = new dw.util.ArrayList();
		var varAttrColor : dw.catalog.ProductVariationAttribute = null;
		
		if( Product.master )
		{
			var varModel : dw.catalog.ProductVariationModel = Product.variationModel;
			varAttrColor = varModel.getProductVariationAttribute("color");
	
			if( varAttrColor != null )
			{
				selectableColors = Product.variationModel.getFilteredValues( varAttrColor );
				
			}
		}
		
		var firstProduct : dw.catalog.Product = pdict.FirstProduct;
		if( firstProduct && firstProduct.variant && varAttrColor != null )
		{
			selectedColor = firstProduct.variationModel.getSelectedValue( varAttrColor );
		}
	</isscript>

    <iscomment>
		Generate link to product detail page: by default it's just the product of the product search hit.
		If a color variation is available, the first color is used as link URL.
	</iscomment>
	<isset name="firstColorVariation" value="${null}" scope="page"/>
	<isset name="productUrl" value="${URLUtils.http('Product-Show', 'pid', Product.ID)}" scope="page"/>
	<isif condition="${!empty(selectableColors) && selectableColors.size() > 0 && !empty(varAttrColor)}">
		
		<iscomment>if we have a selected color, this color is highlighted otherwise the first selectable color is hightlighted</iscomment>
		<isif condition="${!empty(selectedColor)}">
			<isset name="firstColorVariation" value="${selectedColor}" scope="page"/>
		<iselse>
			<isset name="firstColorVariation" value="${selectableColors.get(0)}" scope="page"/>
		</isif>
		<isif condition="${!empty(varAttrColor)}">
			<isset name="productUrl" value="${pdict.Product.variationModel.url('Product-Show', varAttrColor, firstColorVariation)}" scope="page"/>
		</isif>
		
	</isif>

    <iscomment>append the paging parameters to the product URL</iscomment>
	<iscomment><isset name="productUrl" value="${productUrl.append('start', pdict.CurrentHttpParameterMap.starting.stringValue)}" scope="page"/></iscomment>

	<iscomment>append the search query parameters to the product URL</iscomment>
	<isif condition="${!empty(pdict.ProductSearchResult)}">
		<isset name="productUrl" value="${productUrl}" scope="page"/>
	</isif>

<div class="product-tile" id="${Product.UUID}"  data-itemid="${Product.ID}" data-category="${empty(Product.primaryCategory)?Product.categories[0].displayName:Product.primaryCategory.displayName}" data-sku="${Product.ID}" data-price="${Product.priceModel.price}" data-brand="${Product.manufacturerName}" data-title="${Product.name}">

<a class="thumb-link" href="${productUrl}" title="${Product.name}">
<iscomment>Image</iscomment>
<iscomment>++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++</iscomment>

<div class="product-image"><!-- dwMarker="product" dwContentID="${Product.UUID}" -->

	<iscomment>Render the thumbnail</iscomment>
	
		<iscomment>Determine the correct image, either first displayed color variation or default product thumbnail</iscomment>
		<isif condition="${!empty(selectableColors) && selectableColors.size() > 0}">
			<isif condition="${!empty(selectedColor)}">
				<isset name="firstColorVariation" value="${selectedColor}" scope="page"/>
			<iselse>
				<isset name="firstColorVariation" value="${selectableColors.get(0)}" scope="page"/>
			</isif>
			<isset name="image" value="${firstColorVariation.getImage('medium')}" scope="page"/>
			
			
		<iselse>
			<isset name="image" value="${Product.getImage('medium',0)}" scope="page"/>
		</isif>
		
		<iscomment>If image couldn't be determined, display a "no image" medium</iscomment>
		<isscript>
	       var sash : String = pdict.Product.custom.plp_sash;
		   importScript( "app_sleepys:common/AmplienceSash.ds");
		   sash = getProductSash(sash, "&");
	    </isscript>
		<isset name="thumbnailUrl" value="${pdict.CurrentRequest.httpProtocol+'://i1.adis.ws/i/hmk/'+Product.custom.external_id+'?$product_listing_page$'+sash}" scope="page"/>
		<isset name="imageAlt" value="${Product.name}" scope="page"/>
		<isset name="imageTitle" value="${Product.name}" scope="page"/>
		<isset name="thumbnailAlt" value="${Product.name}" scope="page" />
		<isset name="thumbnailTitle" value="${Product.name}" scope="page" />
		
		
			<img src="${thumbnailUrl}" alt="${thumbnailAlt}" title="${thumbnailTitle}" />

	
</div>

<iscomment>Product Name</iscomment>
<iscomment>++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++</iscomment>

<div class="product-name">
	<h2>
		<span class="name-link">
			<isprint value="${Product.name}"/>
		</span>
	</h2>
</div>
</a>	
				
<iscomment>Pricing</iscomment>
<iscomment>++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++</iscomment>

<div class="product-pricing">
    <isscript>
        importScript( "app_sleepys:common/GetSalePriceListId.ds");
		var salePriceListId : String = getSalePriceListId();
    </isscript>

	<iscomment>Renders pricing information depending on the option or variation model, as well as scaled prices and so on.</iscomment>
	<isset name="currencyCode" value="${dw.system.Site.getCurrent().currencyCode}" scope="page"/>
	
		<iscomment>If this is a Product Set</iscomment>
	<isif condition="${Product.productSet}">
		<span class="product-set-price">${Resource.msg('global.buyall','locale',null)}</span>
		
		<iscomment>If this is a master product with a price range</iscomment>
	<iselseif condition="${Product.master && pdict.CurrentHttpParameterMap.pricerange.stringValue == 'true'}">
			<isscript>
			    
	
				//  Get min - max sale price and min - max standard price.
				var listPrice = "listPriceDefault" in dw.system.Site.current.preferences.custom ? dw.system.Site.getCurrent().getCustomPreferenceValue("listPriceDefault") : "800-list-prices";
				//var salesPrice = "listSalesPriceDefault" in dw.system.Site.current.preferences.custom ? dw.system.Site.getCurrent().getCustomPreferenceValue("listSalesPriceDefault") : "800-sales-prices";
				var standardMin: Money = Product.priceModel.getMinPriceBookPrice(listPrice);
				var standardMax: Money = Product.priceModel.getMaxPriceBookPrice(listPrice);
				var saleMin: Money = Product.priceModel.getMinPriceBookPrice(salePriceListId);
				var saleMax: Money = Product.priceModel.getMaxPriceBookPrice(salePriceListId);
				
				var hasSalePrices: Boolean = false;
				if(!empty(saleMin) && !empty(saleMax)) {
					if (standardMin.value > saleMin.value || standardMax.value > saleMax.value) {
						hasSalePrices = true;
					}
				}
			</isscript>	
			<iscomment>If there are sale prices effecting the price range</iscomment>
		<isif condition="${hasSalePrices}">
			<span class="product-standard-price price-range has-sale-price"><isprint value="${dw.util.StringUtils.formatMoney(dw.value.Money(standardMin.value, currencyCode))}" /> - <isprint value="${dw.util.StringUtils.formatMoney(dw.value.Money(standardMax.value, currencyCode))}" /></span>
			<isif condition="${saleMin < saleMax}"> 
				<span class="product-sales-price price-range"><isprint value="${dw.util.StringUtils.formatMoney(dw.value.Money(saleMin.value, currencyCode))}" /> - <isprint value="${dw.util.StringUtils.formatMoney(dw.value.Money(saleMax.value, currencyCode))}" /></span>
			<iselse>
				<span class="product-sales-price price-range"><isprint value="${dw.util.StringUtils.formatMoney(dw.value.Money(saleMin.value, currencyCode))}" /></span>
			</isif>
		<iselse>
			<span class="product-standard-price price-range"><isprint value="${dw.util.StringUtils.formatMoney(dw.value.Money(standardMin.value, currencyCode))}" /> - <isprint value="${dw.util.StringUtils.formatMoney(dw.value.Money(standardMax.value, currencyCode))}" /></span>
		</isif>
	
		<iscomment>If this is a variaiton product or a master with a single price.</iscomment>
	<iselse>						
		<iscomment>For Product master without a price range get the pricing from firstRepresentedProduct </iscomment>
		<isif condition="${Product.master && pdict.CurrentHttpParameterMap.pricerange.stringValue != 'true'}"/>
			<iscomment>Preserve current product instance</iscomment>
			<isset name="OrgProduct" value="${Product}" scope="page"/>
			<isset name="Product" value="${pdict.FirstProduct}" scope="page"/>
		</isif>
		<iscomment>
			Regular pricing through price model of the product. If the product is an option product,
			we have to initialize the product price model with the option model.
		</iscomment>
		<isif condition="${Product.optionProduct}">
			<isset name="PriceModel" value="${Product.getPriceModel(Product.getOptionModel())}" scope="page"/>
		<iselse>
			<isset name="PriceModel" value="${Product.getPriceModel()}" scope="page"/>
		</isif>

		<isscript>
			//  Get sale price and standard price.
			var listPrice = "listPriceDefault" in dw.system.Site.current.preferences.custom ? dw.system.Site.getCurrent().getCustomPreferenceValue("listPriceDefault") : "800-list-prices";
			var standardPrice: Money = Product.priceModel.getMinPriceBookPrice(listPrice);
			
			//var salePrice: Money = PriceModel.getMinPriceBookPrice(prefix + "-sale-prices");
		    var salePrice: Money = Product.priceModel.getMinPriceBookPrice(salePriceListId);
			
			var hasSalePrice: Boolean = false;
			if (!empty(salePrice)) {
				if (standardPrice.value > salePrice.value) {
					hasSalePrice = true;
				}
			}
		</isscript>

		<isif condition="${hasSalePrice}">
			<span class="product-standard-price has-sale-price" title="Regular Price"><isprint value="${dw.util.StringUtils.formatMoney(dw.value.Money(standardPrice.value, currencyCode))}" /></span>
			<span class="product-sales-price" title="Sale Price"><isprint value="${dw.util.StringUtils.formatMoney(dw.value.Money(salePrice.value, currencyCode))}" /></span>
		<iselse>
			<span class="product-standard-price" title="Regular Price"><isprint value="${dw.util.StringUtils.formatMoney(dw.value.Money(standardPrice.value, currencyCode))}" /></span>
		</isif>
		
		<isif condition="${!empty(OrgProduct)}">
			<iscomment>Restore current product instance</iscomment>
			<isset name="Product" value="${OrgProduct}" scope="page"/>
			<isset name="OrgProduct" value="${null}" scope="page"/>
		</isif>
	</isif>
		
</div>


<iscomment>Reviews</iscomment>
<iscomment>++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++</iscomment>

<div class="rating-block">
	<isif condition="${!Product.productSet}">	
		<isinclude template="product/components/reviewsmini"/>
	</isif>
	<isset name="bvCode" value="${dw.system.Site.getCurrent().getCustomPreferenceValue("bvRRDisplayCodeMapping")[0]}" scope="page"/>
	<isinclude template="bv/display/rr/inlineratings"/>
</div>

<iscomment>Promotion</iscomment>
<iscomment>++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++</iscomment>

<iscomment>Render information on active product promotions</iscomment>
<isset name="promos" value="${dw.campaign.PromotionMgr.activeCustomerPromotions.getProductPromotions(Product)}" scope="page"/>
<isif condition="${!empty(promos)}">
	<div class="product-promo">
		<isloop items="${promos}" var="promo" status="promoloopstate">
		<isif condition="${promo.promotionClass != 'ORDER'}">
			<div class="promotional-message <isif condition="${promoloopstate.first}"> first <iselseif condition="${promoloopstate.last}"> last</isif>">
				<isprint value="${promo.calloutMsg}" encoding="off"/>
			</div>
		</isif>
		</isloop>
	</div>
</isif>
	
	
<iscomment>Compare</iscomment>
<iscomment>++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++</iscomment>
	
<iscomment>
	Render the compare checkbox, if search result is based on a category context.
	The checkbox is displayed if either the category or one of its parent categories
	has the custom attribute "enableCompare" set to true.
</iscomment>
<isscript>importScript("app_sleepys:product/ProductUtils.ds")</isscript>

<iscomment>
	Dan requested to not show the compare functionality in 1800mattress.  
	compare could be added at a later date however.  So the following code is skipped
</iscomment>
<isif condition="${false}"> 
	<isif condition="${!empty(pdict.CurrentHttpParameterMap.cgid.value) && ProductUtils.isCompareEnabled(pdict.CurrentHttpParameterMap.cgid.value)}">
	       <div class="product-compare clearfix">        	
			<isif condition="${!Product.productSet && !Product.bundle}">
				<isscript>
					// mark the compare checkbox checked if the current product is 
					// in the current comparison list for the current category
					
					importScript( "app_sleepys:catalog/libCompareList.ds" );
					importScript( "app_sleepys:common/libJson.ds" );
	
					var comparison = GetProductCompareList();
					// Set the category
					if(!empty(pdict.ProductSearchResult && !empty(pdict.ProductSearchResult.category) ))
						comparison.setCategory(pdict.ProductSearchResult.category.ID);
	
					var comparisonProducts = null;
					
					if (comparison) {
						comparisonProducts = comparison.getProducts();
					}
					
					var checkedStr = '';
					
					if (!empty(comparisonProducts)) {								
						var pIt = comparisonProducts.iterator();
						var productId = null;
						
						while (pIt.hasNext()) {
							productId = pIt.next();									
							if (productId == Product.ID) {
								checkedStr = 'checked=true';
								break;
							}
						}
					}		    				
	   			</isscript>			
				<input type="checkbox" class="compare-check" id="${'cc-'+Product.UUID}" ${checkedStr}/>				
				<iscomment>Changed language length for portrait width</iscomment>
				<label for="${'cc-'+Product.UUID}">Compare</label>				
			</isif>
		</div>
	
	</isif>
</isif>    
 
 <iscomment>Swatches</iscomment>
<iscomment>++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++</iscomment>

<iscomment>
	Render the color swatch secion for a product search hit. The swatches depend on the products the search hit actually represents.
	Doesn't necessarily equal all variants of a product master, since only a sub set of variants could be represented by the search
	hit (a variant group). We show color swatches only for variant groups represented by a product search hit.
</iscomment>
<isif condition="${!empty(selectableColors) && selectableColors.size() > 1 && !empty(varAttrColor)}">

	<div class="product-swatches">

		<iscomment>render a link to the palette and hide the actual palette if there are more than five colors contained</iscomment>
		<isif condition="${selectableColors.size() > 5}">
			<a class="product-swatches-all">${Resource.msg('productresultarea.viewallcolors','search',null)}</a> (<isprint value="${selectableColors.size()}"/>)
		</isif>

		<iscomment>render the palette, the first swatch is always preselected</iscomment>
		<ul class="swatch-list<isif condition="${selectableColors.size() > 5}"> swatch-toggle</isif>">	

			<isloop items="${selectableColors}" var="colorValue" status="varloop">

				<iscomment>Determine the swatch and the thumbnail for this color</iscomment>
				<isset name="colorSwatch" value="${colorValue.getImage('swatch')}" scope="page"/>
				<isset name="colorThumbnail" value="${colorValue.getImage('medium')}" scope="page"/>
				
				<iscomment>If images couldn't be determined, display a "no image" thumbnail</iscomment>

				<isif condition="${!empty(colorSwatch)}">
					<isset name="swatchUrl" value="${colorSwatch.getURL()}" scope="page"/>
					<isset name="swatchAlt" value="${colorSwatch.alt}" scope="page"/>
					<isset name="swatchTitle" value="${colorSwatch.title}" scope="page"/>
				<iselse>
					<isset name="swatchUrl" value="{pdict.CurrentRequest.httpProtocol}://i1.adis.ws/i/hmk/img404.jpg?$shoppingcartlineitem75x75$" scope="page"/>
					<isset name="swatchAlt" value="${colorValue.displayValue}" scope="page"/>
					<isset name="swatchTitle" value="${colorValue.displayValue}" scope="page"/>
				</isif>
				<isif condition="${!empty(colorThumbnail)}">
					<isset name="thumbnailUrl" value="${colorThumbnail.getURL()}" scope="page"/>
					<isset name="thumbnailAlt" value="${colorThumbnail.alt}" scope="page"/>
					<isset name="thumbnailTitle" value="${colorThumbnail.title}" scope="page"/>
				<iselse>
					<isset name="thumbnailUrl" value="${pdict.CurrentRequest.httpProtocol}://i1.adis.ws/i/hmk/img404.jpg?$shoppingcartlineitem75x75$" scope="page"/>
					<isset name="thumbnailAlt" value="${colorValue.displayValue}" scope="page"/>
					<isset name="thumbnailTitle" value="${colorValue.displayValue}" scope="page"/>
				</isif>
				
				<isif condition="${!empty(selectedColor)}">
					<isset name="preselectCurrentSwatch" value="${colorValue.value == selectedColor.value}" scope="page"/>
				<iselse>
					<isset name="preselectCurrentSwatch" value="${varloop.first}" scope="page"/>
				</isif>
				
				<iscomment>build the proper URL and append the search query parameters</iscomment>
			    <isset name="swatchproductUrl" value="${Product.variationModel.url('Product-Show', varAttrColor, colorValue.value)}" scope="page"/>
			    <isif condition="${!empty(pdict.ProductSearchResult)}">
				    <isset name="swatchproductUrl" value="${pdict.ProductSearchResult.url( swatchproductUrl )}" scope="page"/>
			    </isif>

				<iscomment>render a single swatch, the url to the proper product detail page is contained in the href of the swatch link</iscomment>
				<li>
					<a href="${swatchproductUrl}" class="swatch ${(preselectCurrentSwatch) ? 'selected' : ''}" title="<isprint value="${colorValue.displayValue}"/>">
						<img src="${swatchUrl}" alt="${swatchAlt}" title="${swatchTitle}" data-thumb='{"src":"${thumbnailUrl}","alt":"${thumbnailAlt}","title":"${thumbnailTitle}"}'/>
					</a>
				</li>

			</isloop>

		</ul>
		
	</div>
			
</isif>
	
</div><!--  END: .product-tile -->
	
</isif>