(function(app, $) {
	var _app = app;

	/***************************************************************************
	 * CONTENT
	 */
	
	$(".expandable h2").on("click", function(e) {
		if ($(this).parent().find(".contents").hasClass("visible")) {			
			$(this).parent().find(".contents").removeClass("visible");
		} else {
			$(this).parent().find(".contents").addClass("visible");
		}
	});	
	
	/***************************************************************************
	 * HEADER
	 */
	$(".mattress-finder a").on("click", function(e) {
		e.preventDefault();
		window.location = _app.urls.mattressFinder;
	});

	$('[id="CustomerZone"]').on('click', 'a', function(e){
		// if ($(this).is(':visible')) {
		$(this).hide();
		// }
	}).on('focusout', 'input', function(e){
		// if ($(this).siblings('a').is(':hidden')) {
		$(this).hide().siblings('a').show();
		// }
	});
	
	/***************************************************************************
	 * HOME PAGE
	 */
	$(".slider-control").on("click", function() {
		if ($(".slider").is(":visible")) {
			$(".slider").slideUp(100);
		} else {
			$(".slider").slideDown(100);
		}
	});

	$(".find-mattress-dropdown").on("click", ".slider li", function() {
		$(".find-mattress-dropdown .selected").removeClass("selected");
		var _imageBed = $(".image-bed");
		_imageBed.removeAttr("class");
		_imageBed.addClass("image-bed bed" + $(this).attr("data"));
		$(".mattress-size").text($(this).text());
		$(".slider").slideUp(100);
	});

	$(".find-mattress-get-started").on("click", function() {
		if ((_app.urls.mattressFinder !== null) && (_app.urls.mattressFinder.length > 0)) {
			window.location = _app.urls.mattressFinder + (($(".mattress-size").text() != "Select a size") ? "?size=" + $(".mattress-size").text() : "");
		}
	});

	$(".slider").on("mouseleave", function() {
		$(".slider").slideUp(100);
	});

	$(".choice").on("click", function() {
		// mark specific item as selected
		$(this).parent().find(".circle").removeClass("selected");
		$(this).find(".circle").addClass("selected");
		// find what was category was just updated
		var _step = $(this).parent().parent().attr("data");
		// unselect all steps
		$(".step-column").removeClass("selected");
		// hide all categories
		if (_step == "size") {
			// show size step as completed
			if (!$(".size .status").hasClass("completed")) {
				$(".size .status").addClass("completed");
			}
			// select next step
			$(".step-column.position").addClass("selected");
			// make next category visible
			$(".mattress-selection-box." + _step).slideUp();
		} else if (_step == "position") {
			// show position step as completed
			if (!$(".position .status").hasClass("completed")) {
				$(".position .status").addClass("completed");
			}
			// select next step
			$(".step-column.comfort").addClass("selected");
			// make next category visible
			$(".mattress-selection-box." + _step).slideUp();
		} else if (_step == "comfort") {
			// show comfort step as completed
			if (!$(".comfort .status").hasClass("completed")) {
				$(".comfort .status").addClass("completed");
			}
			$(".mattress-selection-box." + _step).slideUp();
		}
		if ($(".size .status").hasClass("completed")) {
			if (!$(".find-my-mattress .title").hasClass("visible")) {
				$(".find-my-mattress .title").addClass("visible");
			}
		}
	});

	$(".questions-completed .find-my-mattress").on("click", function() {
		if ($(".find-my-mattress .title").hasClass("visible")) {
			var _size = $(".size .circle.selected").attr("data");
			var _comfort = $(".comfort .circle.selected").attr("data");
			var zoneCode = $('#CustomerZone input[name=postalCode]').data('zoneCode') != null ? $('#CustomerZone input[name=postalCode]').data('zoneCode').replace(',' , '|') : '';
			var _urlString = "?prefn1=comfort_level&prefv1=" + _comfort + "&prefn2=size&prefv2=" + _size;
			if (zoneCode != '') {
			    _urlString += "&prefn3=zone_code&prefv3="+zoneCode;
			}
			//alert(_urlString);
			window.location = _app.urls.mattresses + _urlString;
		}
	});

	$(".step-column").on("click", function() {
		$(this).parent().find(".step-column").removeClass("selected");
		$(this).addClass("selected");
		var _switchToStep = "." + $(this).attr("data");
		$(".mattress-selection-box" + _switchToStep).slideDown();
	});
	
	$(".promo-row-1 .wrapper a, .promo-row-2 .wrapper a, .occasion .wrapper a").on("click", function(e) {
		var zoneCode = $('#CustomerZone input[name=postalCode]').data('zoneCode') != null ? $('#CustomerZone input[name=postalCode]').data('zoneCode').replace(',' , '|') : '';
		if (zoneCode != '') {
		    e.preventDefault();
		    var url = $(this).attr("href");
		    if (url.indexOf('search?q=') > -1) {
		    	url += "&prefn1=zone_code&prefv1="+zoneCode;
		    } else {
		    	url += "#prefn1=zone_code&prefv1="+zoneCode;
		    }
		    window.location = url;
		}
	});

	$('#CustomerZone').on('click', 'a', function(e) {
		$(this).parent().find('input').attr('placeholder', $(this).html());
	});
	
	/***************************************************************************
	 * PLP PAGE
	 */
	
	_app.showAllProducts = function(e) {		
		var url = _app.urls.clearZipZone;

		$.ajax({
			url : url,
			type : 'post',
			dataType : 'html',
			beforeSend : function() {
				_app.progress.show();
			},
			success : function(data) {
				//always reset to page 1
				window.location.href = resetPaging(window.location.href);;
				window.location.reload();
			},
			complete : function() {
				_app.progress.hide();
			}
		});
	} 

}(window.app = window.app || {}, jQuery));

var resetPaging = function(hash){
	return hash.replace(/start=\d+/g,'start=0');
}

var changeZipClick = function(obj) {
	$(".zip-display").hide();
	$(".zip-input").show();
	$(".zip-change").hide();
	$(".zip-update").show();
	$(".zip-input input").select();
}

var updateZipClick = function(obj) {
	var _zip = $(".zip-input input").val();
	// 1. Verify a valid US 5-digit ZIP first, and that the same ZIP
	// was not entered again
	if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(_zip)) {
		var data = {
			zip : _zip
		};
		var url = app.urls.initZoneCheck;

		$.ajax({
			url : url,
			type : 'post',
			data : data,
			dataType : 'html',
			beforeSend : function() {
				app.progress.show();
			},
			success : function(data) {

				// Check what was returned
				if ($.trim(data) == "invalidZip") {
					//$cache.locationParent.find('.p-code').addClass('invalidZip').val("Invalid Zip Code");
					$('.zip-invalid').show();
				} else {
					
					$('#CustomerZone').html(data);

					var hash = window.location.hash;
					var url = window.location.href;
					
					//always reset to page 1
					hash = resetPaging(hash);
					url = resetPaging(url);		
					
					if (hash.length < 1 && url.indexOf('zone_code') == -1) {
					
						//1. Retrieve Zone_Code from DOM
						var zoneCode = $('#CustomerZone input[name=postalCode]').data('zoneCode').replace(',' , '|');
					
						//2. Add hash
						var newUrl = url + '#prefn1=zone_code&prefv1='+zoneCode;
					
					} else {
					
						//Verify if zone_code exists in the hash
						if (hash.indexOf('zone_code') > -1) {

							//1. Define 'zone_code' parameter name
							var parameterName = hash.substring(hash.indexOf('zone_code')-7, hash.indexOf('zone_code')-1);
							parameterName = parameterName.replace('n', 'v');
							
							//2. Define initial position of zone_code parameter value
							var iniPosValue = hash.indexOf(parameterName+'=')+7;
	
							//3. Retrieve zone_code value from hash
							var value = hash.substring(iniPosValue);
	
							//4. Verify if parameter is the last one
							if (value.indexOf('&') > -1) {
								value = value.substring(0, value.indexOf('&')-1);
							}

							//5. Retrieve Zone_Code from DOM
							var zoneCode = $('#CustomerZone input[name=postalCode]').data('zoneCode').replace(',' , '|');
	
							//6. Replace parameter on hash
							var newUrl = url.replace(value, zoneCode);
						
						//Verify if zone_code exists in the url
						} else if (url.indexOf('zone_code') > -1) {
							
							//replace zone_code in the current url
							
							//1. Define 'zone_code' parameter name
							var parameterName = url.substring(url.indexOf('zone_code')-7, url.indexOf('zone_code')-1);
							parameterName = parameterName.replace('n', 'v');
							
							//2. Define initial position of zone_code parameter value
							var iniPosValue = url.indexOf(parameterName+'=')+7;
	
							//3. Retrieve zone_code value from hash
							var value = url.substring(iniPosValue);
	
							//4. Verify if parameter is the last one
							if (value.indexOf('&') > -1) {
								value = value.substring(0, value.indexOf('&')-1);
							}

							//5. Retrieve Zone_Code from DOM
							var zoneCode = $('#CustomerZone input[name=postalCode]').data('zoneCode').replace(',' , '|');
	
							//6. Replace parameter on hash
							var newUrl = url.replace(value, zoneCode);
							
						} else {

							//1. Retrieve Zone_Code from DOM
							var zoneCode = $('#CustomerZone input[name=postalCode]').data('zoneCode').replace(',' , '|');
						
							//2. Add hash
							var newUrl = url + '&prefn1=zone_code&prefv1='+zoneCode;
							
						}
					}	
					
					//changes zone code value on search field
					$('input[name=prefv1]').val(zoneCode);
					
					if (window.location.href == newUrl){
						window.location.reload();
					} else { 
						window.location.href = newUrl;
					} 	
				}
			},
			complete : function() {
				app.progress.hide();
			}
		});
	}
};