# README 

This repository is digested by the [sleepys](/mediahive/sleepys/wiki/Development_Workflow/#markdown-header-md) repository as a git-submodule; the steps that were taken to achieve this (which should not need to be repeated) were:

```
$ cd sleepys
$ git checkout development
$ git submodule add git@bitbucket.org:mediahive/mattressdiscounters.git cartridges/app_md
$ git add .gitmodules cartridges/app_md
$ git commit -am "Initial commit of app_md cartridge as submodule"
$ git push -u origin development
```

Note: With the addition of the mattressdiscounters submodule, the Jenkins CI configuration for the sleepys job had to be updated to include a new "Invoke Ant" build step with a `sass.compile` target for the app_md build file.