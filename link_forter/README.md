# Forter Integration - Salesforce Commerce Cloud
SFCC/DWRE Link Cartridge of Forter for SGJC (SiteGenesis) and SFRA (StoreFront Reference Architecture).

#### Installation for SiteGenesis

1. Follow directions in 'cartridgs/documentation/Forter LINK Integration Documentation.docx' (Ignore SFRA specific steps)

#### Installation for SFRA

1. ollow directions in 'cartridgs/documentation/Forter LINK Integration Documentation.docx' (Ignore SiteGenesis specific steps)

