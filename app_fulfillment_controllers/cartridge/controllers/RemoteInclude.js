'use strict';

/**
 * Controller 
 *
 * @module controllers/RemoteInclude
 */

/* API Includes */
var Logger = require('dw/system/Logger');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');


// Receives the AJAX submission of dispatch tracking and responds
function renderTemplate() {
	var params = request.httpParameterMap;
	var template : String = params.template.stringValue;
	var uniqueValue : String = params.uniqueValue.stringValue;
	

	app.getView({
			uniqueValue: uniqueValue,
	}).render(template);				
}



/* Exports of the controller */

/** @see {@link module:controllers/RemoteInclude~renderTemplate} */
exports.RenderTemplate = guard.ensure(['get'], renderTemplate);


