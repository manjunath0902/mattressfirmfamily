'use strict';

/**
 * Controller that renders the account overview, manages customer registration and password reset,
 * and edits customer profile information.
 *
 * @module controllers/Account
 */

/* API includes */
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var Transaction = require('dw/system/Transaction');
var Status = require('dw/system/Status');
var pageMeta = require('~/cartridge/scripts/meta');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var Pipelet = require('dw/system/Pipelet');
var guard = require('~/cartridge/scripts/guard');
var Transaction = require('dw/system/Transaction');
var ShippingLocation = require('dw/order/ShippingLocation');
var TaxMgr = require('dw/order/TaxMgr');
var Logger = require('dw/system/Logger');
var File = require('dw/io/File');
var FileReader = require ('dw/io/FileReader');
var CSVStreamReader = require ('dw/io/CSVStreamReader');
var CustomObjectMgr = require ('dw/object/CustomObjectMgr');
var HashMap = require('dw/util/HashMap');
var OrderMgr = require('dw/order/OrderMgr');
var ProductSearchModel = require('dw/catalog/ProductSearchModel');
var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;
var ATPService = require('~/cartridge/scripts/util/ATPService');
var OrderFactory = require('app_fulfillment_core/cartridge/scripts/util/OrderFactory');
var Email = app.getModel('Email');
var ShippingMgr = require('dw/order/ShippingMgr');

/* include Script Modules */
var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;

var params = request.httpParameterMap;
var existingOrderIds = [];
var eventDeletedMessage = 'This Event does not exist anymore or it might have been deleted because of expiry set by the Administrator';
var duplicateEventMsg = 'An event with the same Event Name, Event Date and Store Id already exists';
/*
 * This function checks if:
 * 1. Product Id is missing
 * 2. Quantity is missing or 0 or (NaN)
 * 3. First Name is missing
 * 4. Last Name is missing
 * 5. Address is missing
 * 6. Product does not exist in Catalog
 *
 * */
function validateOrderRow(orderRow, productIndex) {
	var noProductIdPresent	= true,
		productIdMissing	= false,
		unrealQuantity		= false,
		nameMissing			= false,
		addressMissing		= false,
		nonExistingProductId= false,
		error				= false;

	// First Name / Last Name
    if (empty(orderRow[1]) || empty(orderRow[3])) {
    	nameMissing = true;
    	error = true;
    }

    // Address, City, State, Zip
    if (empty(orderRow[4]) || empty(orderRow[6]) || empty(orderRow[7]) || empty(orderRow[8])) {
    	addressMissing = true;
    	error = true;
    }

    var noOfProductsToReadFromCSV = 20;	// This will read 10 products from CSV as the j is incremented by 2, each time
	for (var j = 0; j < noOfProductsToReadFromCSV; j += 2) {
		var itemIndex =  j + productIndex;
		var pid = orderRow[itemIndex];
		var product : dw.catalog.Product;
		if (!empty(pid) && pid != null) {
			noProductIdPresent = false;
			var quantity : Number = new Number(orderRow[itemIndex+1]);
			// Incorrect quantity
			if (isNaN(quantity) || quantity < 1 || empty(quantity)) {
				unrealQuantity = true;
				error = true;
			}

			var psm = new ProductSearchModel();
			psm.setRefinementValues("manufacturerSKU", pid);
			psm.search();
			var prdItr : dw.util.Iterator = psm.getProductSearchHits();

			if(prdItr.hasNext()) {
				product = prdItr.next();
				pid = product.getFirstRepresentedProductID();
			}

			var productModel = app.getModel('Product');
			product = productModel.get(pid);

			// Incorrect product Id
		    if (empty(product) || !('object' in product)) {
		    	nonExistingProductId = true;
		    	error = true;
		    }
		}
	}
	// No Product Id was given in the Order
	if (noProductIdPresent) {
		productIdMissing = true;
		error = true;
	}

	return {
		error				: error,
		productIdMissing	: productIdMissing,
		unrealQuantity		: unrealQuantity,
		nameMissing			: nameMissing,
		addressMissing		: addressMissing,
		nonExistingProductId: nonExistingProductId
	}
}

function importOrders(args)
{
	var CustomObjectMgr	= require ('dw/object/CustomObjectMgr');
	var eventId = args.eventId,
		storeId = args.storeId,
		servicePriceObj = null;

	var Product = app.getModel('Product');
	var fileMap : LinkedHashMap = params.processMultipart( getFormFiles );
	if( fileMap && fileMap["ordersfile"] ) {
		var ordersFile = fileMap["ordersfile"];
		var filePath =  ordersFile.fullPath;
		var filereader :  dw.io.Reader = new FileReader(ordersFile);
		var csvstream : dw.io.CSVStreamReader = new CSVStreamReader(filereader);
		var orderRow = csvstream.readNext();
		var ordersMissingProductIds		= [],
			ordersWithUnrealQuantities	= [],
			ordersMissingName			= [],
			ordersMissingAddress		= [],
			orderWithNonExistingProducts= [];
		var i=0;
		var orderIdMissing = false;

		while(orderRow != null)
		{
			if (empty(orderRow[0])) {
				orderRow = csvstream.readNext();
				orderIdMissing = true;
				continue;
			}

			var cart = app.getModel('Cart').goc();

			if(i != 0)
			{
				var orderData =  getOrderData(orderRow);
				var orderId =  orderData['orderId'];

				var getExistingOrderIdObj = getExistingOrderId(orderId, eventId, true);	// check order existence globally (in all Events)

				if(!empty(orderId) && !getExistingOrderIdObj.orderExists)
				{
					var productIndex =  12;
					//var orderItems =  orderRow[12];

					var validationResult = validateOrderRow(orderRow, productIndex);

					if(validationResult.error) {	// Do not place this Order
						if (validationResult.productIdMissing)
							ordersMissingProductIds.push(orderId);
						if (validationResult.unrealQuantity)
							ordersWithUnrealQuantities.push(orderId);
						if (validationResult.nameMissing)
							ordersMissingName.push(orderId);
						if (validationResult.addressMissing)
							ordersMissingAddress.push(orderId);
						if (validationResult.nonExistingProductId)
							orderWithNonExistingProducts.push(orderId);
						orderRow = csvstream.readNext();
						continue;
					}

					var noOfProductsToReadFromCSV = 20;	// This will read 10 products from CSV as the j is incremented by 2, each time
					for (var j = 0; j < noOfProductsToReadFromCSV; j += 2) {

						var itemIndex =  j+productIndex;
						var sku = orderRow[itemIndex];
						var quantity : Number = new Number(orderRow[itemIndex+1]);
						if(!empty(sku) && sku != null)
						{
							var pid = sku;

							var psm = new ProductSearchModel();
							psm.setRefinementValues("manufacturerSKU", pid);
							psm.search();
							var prdItr : dw.util.Iterator = psm.getProductSearchHits();

							if(prdItr.hasNext()) {
								product = prdItr.next();
								pid = product.getFirstRepresentedProductID();
							}

						    var productModel = Product.get(pid);
						    if (!empty(productModel) && 'object' in productModel) {
						        var product = productModel.object;
						        if(product != null)
						        {
						        	var AddProductToBasketResult = new Pipelet('AddProductToBasket').execute({
							            Basket: cart.object,
							            Product: product,
							            ProductOptionModel: null,
							            Quantity: quantity,
							            Category: null
							        });
						        }
						    }
						}
						/*// un-commenting this else will not add the products in Basket which were found after an empty productId cell in CSV
						else {
							break;
						}*/
					}
				}

				addShippingAddress(cart, orderData);

				var forceServiceCall = true;
	            mattressPipeletHelper.splitShipmentsByType(cart.object, forceServiceCall);

				for each(var shipment in cart.object.shipments) {
					var pliIterator = shipment.getProductLineItems().iterator();
					while (pliIterator.hasNext()) {
		        		var pli : Product = pliIterator.next();
		        	if( pli.product.custom.shippingInformation.toLowerCase() == 'core' ) {
		        			args['serviceSKU'] = 'tpf127550';
		        			servicePriceObj = getServicePrice(args);
		        		}
		        	}
				}
				
				calculateShippingCosts(cart, servicePriceObj);
				calculateTax(cart.object);
				cart.updateTotals();
				if (cart.object.allProductLineItems.length > 0) {
		            try {
						Transaction.wrap(function () {
						   var order = cart.createOrder();
						   order.custom.eventId =  eventId;
						   order.custom.fulfillmentOrderId = orderId;	// CFS POS order id (orderId coming from CSV file)
						   order.custom.InventLocationId = storeId;
						   
						   var shippingAddress = order.getDefaultShipment().getShippingAddress();
						   shippingAddress.custom.email = orderData['email'];
						   shippingAddress.custom.mobilePhone = orderData['secondryPhoneNo'];
						});

				   }
				   catch(e) {
				    	Logger.error('Order creation failed: ' + e.message);
				    }
				}
			}
			var i=i+1;
			orderRow = csvstream.readNext();

		}

		ordersFile.remove();

		// Creating the message to inform customer about skipped Orders with reasons
		var skippedOrdersMessage = '';
		if (ordersMissingProductIds.length > 0) {
			skippedOrdersMessage += '<li>Orders with missing Product Ids: ' + ordersMissingProductIds.join(", ") + '</li>';
		}
		if (ordersWithUnrealQuantities.length > 0) {
			skippedOrdersMessage += '<li>Orders with incorrect Product Quantities: ' + ordersWithUnrealQuantities.join(", ") + '</li>';
		}
		if (ordersMissingName.length > 0) {
			skippedOrdersMessage += '<li>Orders with missing First name / Last name: ' + ordersMissingName.join(", ") + '</li>';
		}
		if (ordersMissingAddress.length > 0) {
			skippedOrdersMessage += '<li>Orders with missing Address information: ' + ordersMissingAddress.join(", ") + '</li>';
		}
		if (orderWithNonExistingProducts.length > 0) {
			skippedOrdersMessage += '<li>Orders with incorrect Product Ids: ' + orderWithNonExistingProducts.join(", ") + '</li>';
		}
		if (getExistingOrderIdObj != null && !empty(getExistingOrderIdObj.existingOrderIds)) {
			skippedOrdersMessage += '<li>Orders already uploaded: ' + getExistingOrderIdObj.existingOrderIds + '</li>';
		}
		if (orderIdMissing) {
			skippedOrdersMessage += '<li>One or more Orders are not uploaded as they have missing Order Id</li>';
		}
		if(!empty(skippedOrdersMessage)) {
			skippedOrdersMessage = '<p>Following Orders could not be uploaded for event ' + args.eventName + ':</p> <ul>' + skippedOrdersMessage + '</ul>';
			Logger.info(skippedOrdersMessage);
		}
 
		return skippedOrdersMessage;
	}
	//app.getView().render('fulfillment/createevent');
}
function getFormFiles( field, contentType, fileName ) {
	if( empty(fileName) ) {
		return null;
	}

   /* if(contentType.indexOf("csv/") < 0) return null;
    */
    var uniqueness = GenerateUniqueNumString(9);

    return new File( File.IMPEX + "/src/" + uniqueness + "-" + fileName.toLowerCase() );
}
/*
 * This function checks if an Event already exist in the Business Manager (Custom Object Editor) with same 1. Event Name, 2. Date & 3. Store
 * Returns the count of the such found events and the ID of Event CustomObject
 * */
function duplicateEventCount(newEventName, newEventDate, newStoreId) {
	var qStrDuplicateEvent = 'custom.eventName ILIKE {0} AND custom.eventDate = {1} AND custom.storeId = {2}';
	var existingEventsItr : SeekableIterator = CustomObjectMgr.queryCustomObjects('fulfillment_events', qStrDuplicateEvent, 'creationDate DESC', newEventName, newEventDate, newStoreId);
	if (existingEventsItr.hasNext()) {	// Existing Event found
		var event = existingEventsItr.next();
		return {
			eventCount	: existingEventsItr.count,
			eventId		: event.custom.ID
		}
	}
	return {
		eventCount	: 0,
		eventId		: null
	}
}

function handleCreateEvent()
{
		app.getForm('createevent').handleAction({
        save: function () {
        	var queryString = 'custom.eventName = {0} AND custom.storeId = {1} AND custom.eventDate = {2}';
        	var newEventName = app.getForm('createevent.event.eventname').value();
        	var newStoreId = params.storeId.stringValue;
        	var newEventDate = new Date(app.getForm('createevent.event.eventdate').value());

        	// Check for existing Event with same: Name, Date, StoreId
        	var eventCount_ID = duplicateEventCount(newEventName, newEventDate, newStoreId);
        	if(eventCount_ID.eventCount > 0) {
            	app.getController('Fulfillment').Dashboard(duplicateEventMsg); // Do not create a Duplicate Event
            	return;        		
        	}
         	Transaction.wrap(function() {
        		var fulfillment_event =  CustomObjectMgr.createCustomObject('fulfillment_events', dw.util.UUIDUtils.createUUID());
        		fulfillment_event.custom.eventName	= newEventName;
        		fulfillment_event.custom.eventDate	= newEventDate;
        		fulfillment_event.custom.storeId	= newStoreId;
        		fulfillment_event.custom.userId		= customer.ID,
        		args = {'eventId':fulfillment_event.custom.ID,'storeId':fulfillment_event.custom.storeId, "eventName":fulfillment_event.custom.eventName};

        		skippedOrdersMessage = importOrders(args);
        		app.getForm('createevent').clear();
        	});
        	Logger.error("handleCreateEvent" + skippedOrdersMessage);
    		app.getController('Fulfillment').Dashboard(skippedOrdersMessage);
    		return;
        	//response.redirect(URLUtils.https('Fulfillment-Dashboard'));
        },
        cancel: function (){
        	response.redirect(URLUtils.https('Fulfillment-Dashboard'));
        },
        error: function () {
        	var storeIdsArr = getStoreIdsForUser();

        	app.getView({
        		storeIdsArr : storeIdsArr,
            }).render('fulfillment/createevent');
           }
    });

}

function createEvent()
{
	app.getForm('createevent').clear();
	var storeIdsArr = getStoreIdsForUser();

	app.getView({
		storeIdsArr : storeIdsArr
    }).render('fulfillment/createevent');
}

/**
 * Edit event details
 */
function editEvent()
{
	var eventId;
	if (!empty(request.httpParameterMap.eid.value)) {
		eventId = request.httpParameterMap.eid.value.toString();
	}

	var queryString = 'custom.ID = {0}';
	var eventToEdit = CustomObjectMgr.queryCustomObject('fulfillment_events', queryString, eventId);

	if (!empty(eventToEdit) && (eventToEdit.custom.userId == customer.ID || customer.profile.custom.accountType == "Admin")) {
		app.getForm('createevent.event').copyFrom(eventToEdit);
		var ordersIter : dw.util.SeekableIterator = OrderMgr.queryOrders('custom.eventId={0}', 'custom.eventId asc', eventToEdit.custom.ID);
		var storeEditable = ordersIter && ordersIter.count <= 0;
		if (storeEditable) {
			var storeIdsArr = getStoreIdsForUser();
		}
		app.getView({
			storeEditable	: storeEditable,
			storeIdsArr		: storeIdsArr,
			eventToEdit		: eventToEdit
		}).render('fulfillment/editevent');
	} else {
		response.redirect(URLUtils.https('Fulfillment-Dashboard', 'eventDeleted', true));
		return;
	}
}

/**
 * Edit event details Form handler
 */
function handleEditEvent()
{
	var eventId = params.eventId.stringValue;
	var queryString = 'custom.ID = {0}';
	var eventToEdit = CustomObjectMgr.queryCustomObject('fulfillment_events', queryString, eventId);
	var newStoreId	= params.storeId.stringValue;

	// If the Event does not exist (deleted by Job: DropOldEvents)
	if (empty(eventToEdit)) {
		response.redirect(URLUtils.https('Fulfillment-Dashboard', 'eventDeleted', true));
		return;
	}

    app.getForm('createevent').handleAction({
        cancel: function () {
            app.getForm('createevent').clear();
            response.redirect(URLUtils.https('Fulfillment-Dashboard'));
        },
        update: function () {
        	var skippedOrdersMessage = '';
        	var newEventName = app.getForm('createevent.event.eventname').value();
        	var newEventDate = new Date(app.getForm('createevent.event.eventdate').value());

        	// Check for existing Event with same: Name, Date, StoreId
        	var eventCount_ID = duplicateEventCount(newEventName, newEventDate, newStoreId);
        	if(eventCount_ID.eventCount > 0 && eventCount_ID.eventId != eventToEdit.custom.ID) { // If an Event is being edited then the current event should not be considered as a duplicate event.
            	app.getController('Fulfillment').Dashboard(duplicateEventMsg); // Do not create a Duplicate Event
            	return;        		
        	}
        	Transaction.wrap(function() {
	    		eventToEdit.custom.eventName = newEventName;
	    		eventToEdit.custom.eventDate = newEventDate;
	    		if (!empty(newStoreId)) {
	    			eventToEdit.custom.storeId = newStoreId;
	    		}
	    		args = {'eventId':eventId,'storeId':eventToEdit.custom.storeId, "eventName":eventToEdit.custom.eventName};
	    		skippedOrdersMessage = importOrders(args);
	    	});
    		app.getController('Fulfillment').Dashboard(skippedOrdersMessage);
	    	//response.redirect(URLUtils.https('Fulfillment-Dashboard'));
        },
	    error: function () {
            response.redirect(URLUtils.https('Fulfillment-EditEvent'));
    	}
    });
}

function addShippingAddress(cart, orderData) {


	 Transaction.wrap(function () {
	        var defaultShipment, shippingAddress;
	        defaultShipment = cart.getDefaultShipment();
	        var shippingMethodID = defaultShipment.getShippingMethodID();
	        shippingAddress = cart.createShipmentShippingAddress(defaultShipment.getID());
	        shippingAddress.setFirstName(orderData['firstName'].substring(0,25));
	        shippingAddress.setLastName(orderData['lastName'].substring(0,25));
	        shippingAddress.setAddress1(orderData['address1']);
	        shippingAddress.setAddress2(orderData['address2']);
	        shippingAddress.setCity(orderData['city']);
	        shippingAddress.setPostalCode(orderData['zip']);
	        shippingAddress.setStateCode(orderData['state']);
	        shippingAddress.setCountryCode(orderData['countryCode']);
	        shippingAddress.setPhone(orderData['phoneNo']);
	        shippingAddress.custom.mobilePhone = orderData['secondryPhoneNo'];
	        shippingAddress.custom.email = orderData['email'];
	        cart.updateShipmentShippingMethod(defaultShipment.getID(), shippingMethodID, null, null);
	        cart.calculate();
	    });
	 return;
}
function GenerateUniqueNumString(len) {
    var now : Date = new Date();
    var msNow : Number = now.getTime();

    var uString : String = msNow.toString();

    if( len < uString.length)
    {
        uString = uString.substring(uString.length-len);
    }

    return(uString);
}

function getStoreIdsForUser()
{
	var storeIdsArr = [];
	if(customer.profile.custom.accountType == "Admin")
	{
		var storeIds = CustomObjectMgr.getAllCustomObjects('fulfillment_storeid');
		while (storeIds.hasNext()) {
			 var storeId = storeIds.next();
			 storeIdsArr.push(storeId.custom.storeId);
		}
	}
	else if(!empty(customer.profile.custom.fulfillment_StoreIds))
	{
		var storeIdsObj = JSON.parse(customer.profile.custom.fulfillment_StoreIds);
		for (var key in storeIdsObj) {
			var storeObj = CustomObjectMgr.getCustomObject('fulfillment_storeid', storeIdsObj[key]);
			if (!empty(storeObj)) {	// If a store with this ID exists
				storeIdsArr.push(storeIdsObj[key]);
			}
		}
	}

	return storeIdsArr;
}
function getOrderData(orderRow)
{
	var orderData = [];
	orderData['orderId'] = orderRow[0];
	orderData['firstName'] =  orderRow[1];
	orderData['middleName'] =  orderRow[2];
	orderData['lastName'] =  orderRow[3];
	orderData['address1'] = orderRow[4];
	orderData['address2'] = orderRow[5];
	orderData['city'] = orderRow[6];
	orderData['state'] = orderRow[7];
	var zip = !empty(orderRow[8])?orderRow[8].toString():"";
	var leadingZeros = '';
	if(!empty(orderRow[8])){
		for (var i=orderRow[8].toString().length; i<5; i++) {
			leadingZeros += '0';
		}
		zip = leadingZeros + zip;
	}
	orderData['zip'] = zip;
	orderData['email'] = orderRow[9];
	orderData['phoneNo'] = orderRow[10];
	orderData['secondryPhoneNo'] = orderRow[11];
	orderData['countryCode'] = 'US';
	return orderData;
}

/*
 * This function checks if an Order with the provided orderId already exists in the BM/Event (depending on third input parameter)
 * 
 * Input Params:
 * 	orderId	: CFS POS order id (coming from the CSV file)
 * 	eventId	: ID of the Event that is being created / edited
 * 	global	: true if the order existence has to be checked in all Events (Business Manager), false to check the in only current Event
 * 
 * Output Params:
 * 	existingOrderIds	: comma separated string of existing orderIds 
 * 	orderExists			: true if the order with given orderId exists, false otherwise
 * */
function getExistingOrderId(orderId, eventId, global) {

	var orderExists : Boolean = false;

	if (global) {
		// Order with the given orderId should not already be present in any Event (i.e. it should not be present in Business Manager)
		var qStr : String = "custom.fulfillmentOrderId = {0}";
		var orderDetails : SeekableIterator = OrderMgr.queryOrder(qStr, orderId, eventId);
	} else {
		// Order with the given orderId should not already be present in this current Event
		var qStr : String = "custom.fulfillmentOrderId = {0} AND custom.eventId={1}";
		var orderDetails : SeekableIterator = OrderMgr.queryOrder(qStr, orderId, eventId);
	}

	if(!empty(orderDetails)) {
		orderExists = true;
		existingOrderIds.push(orderId);
	}

	return {
		"existingOrderIds":existingOrderIds.join(", "),
		"orderExists":orderExists
		};
}

function dashboard(errorMsgs)
{
	var eventDeleted = params.eventDeleted.booleanValue;
	if (eventDeleted) {
		errorMsgs = eventDeletedMessage;
	}
	var eventsData = getFulfillmentEventsOrders();
	pageMeta.update(dw.content.ContentMgr.getContent('fulfillment-dashboard'));
	app.getView({
		count			: eventsData.count,
		eventsDataStr	: eventsData.eventsDataStr,
		skippedOrdersMessage : errorMsgs
    }).render('fulfillment/dashboard');
}

function getEventsForThisPage()
{
	if (params.format.stringValue === 'ajax' && params.eventsDataStrChunk.stringValue) {
        app.getView({
    		eventsDataStrChunk	: params.eventsDataStrChunk.stringValue
        }).render('fulfillment/eventslisting');
    }
}

function canBeReschedule(dwOrder){
	var defaultShipment, shippingAddress;
    defaultShipment = dwOrder.getDefaultShipment();
    var orderShippingMethodID = app.getController('SearchOrders').GetShippingMethod(dwOrder);	// Pick the nationwide shipment instead of defaultShipment
    var orderShipment;
	for each (var shipment in dwOrder.shipments) {
		if (!empty(shipment.shippingMethodID) && shipment.productLineItems.length > 0 && shipment.shippingMethodID.equals(orderShippingMethodID)) {
			orderShipment = shipment;
			break;
		}
	}
	
	var rescheduleTimeDifference = new Number(dw.system.Site.getCurrent().getCustomPreferenceValue('hoursToShowRescheduling'));
    if(dwOrder.getConfirmationStatus() == dw.order.Order.CONFIRMATION_STATUS_CONFIRMED && 
    	( dwOrder.getExportStatus() == dw.order.Order.EXPORT_STATUS_READY || dwOrder.getExportStatus() == dw.order.Order.EXPORT_STATUS_EXPORTED) ) {
		var orderDeliveryDate = new Date(orderShipment.custom.deliveryDate);
		var orderDeliveryTimeRange = orderShipment.custom.deliveryTime; // e.g. '08:30am-09:00pm'
		try {
			if (!empty(orderDeliveryTimeRange)) {
				var startTime = orderDeliveryTimeRange.split('-')[0];
				var hours = new Number(startTime.split(':')[0]);
				var mins = new Number(startTime.split(':')[1].substring(0,2));
				var isPM = startTime.split(':')[1].substring(2) == 'pm' ? true : false;
				if (isPM) {
					if (hours < 12)
						hours += 12;
				}
				orderDeliveryDate.setHours(hours,mins,0,0);	// Add the start time in the date and then check the difference
			} else {
				orderDeliveryDate.setHours(0,0,0,0);	// If the Delivery Time was not present then check from start of the day
			}
		} catch (e) {
			Logger.error('Error occurred while setting time: '+e.message);
			orderDeliveryDate.setHours(0,0,0,0);	// If the Delivery Time was not present then check from start of the day
		}
	    
		var currentDate = new Date();
		var hoursDiff = Math.ceil((orderDeliveryDate - currentDate) / (1000 * 60 * 60));
		Logger.debug('orderNo: '+dwOrder.orderNo+', orderDeliveryDate: '+orderDeliveryDate+', currentDate: '+currentDate+', hoursDiff: '+hoursDiff+', rescheduleTimeDifference: '+rescheduleTimeDifference);
		if (hoursDiff > rescheduleTimeDifference) {
			return true;
		} else {
			return false;
		}
    }
	Logger.debug('orderDeliveryDate: '+orderDeliveryDate+', currentDate: '+currentDate+', hoursDiff'+hoursDiff+', rescheduleTimeDifference: '+rescheduleTimeDifference);
	return false;
}
function getFulfillmentEventsOrders(orderNo)
{
	var qMap = new HashMap();

	

	if(customer.profile.custom.accountType != "Admin")
	{
		qMap.put('custom.userId', customer.ID);
	}
	var eventsIter : dw.util.SeekableIterator = CustomObjectMgr.queryCustomObjects('fulfillment_events', qMap, 'custom.eventDate DESC');
	var eventsOrders = new Object();
	var eventObj = new Object();
	var eventsObjs = new Object();
	var orderObj = new Object();
	var ordersObjs = new Object();
	var eventsArray = [];
	while(eventsIter.hasNext())
	{

		eventObj = new Object();
		var event = eventsIter.next();
		eventObj['eventID'] = event.custom.ID;
		eventObj['eventName'] = event.custom.eventName;
		eventObj['eventDate'] = event.custom.eventDate;
		eventObj['storeId'] = event.custom.storeId;
		eventObj['userId'] = event.custom.userId;

		if(!orderNo) {
			var ordersIter : dw.util.SeekableIterator = OrderMgr.queryOrders('custom.eventId={0} AND status!={1} AND status!={2} AND status!={3}', 'creationDate desc', event.custom.ID,
	                dw.order.Order.ORDER_STATUS_REPLACED, dw.order.Order.ORDER_STATUS_FAILED, dw.order.Order.ORDER_STATUS_CANCELLED);
		}else {
			var ordersIter : dw.util.SeekableIterator = OrderMgr.queryOrders('custom.eventId={0} AND status!={1} AND status!={2} AND status!={3} AND orderNo!={4}', 'creationDate desc', event.custom.ID,
	                dw.order.Order.ORDER_STATUS_REPLACED, dw.order.Order.ORDER_STATUS_FAILED, dw.order.Order.ORDER_STATUS_CANCELLED, orderNo);
		}

		var ordersArray = [];
		while(ordersIter.hasNext())
		{
			orderObj = new Object();
			var order = ordersIter.next();
			orderObj['orderNo'] = order.orderNo;
			orderObj['status'] = order.status.value;
			orderObj['paymentStatus'] =  order.paymentStatus.value;
			orderObj['exportStatus'] =  order.exportStatus.value;
			orderObj['shippingStatus'] =  order.shippingStatus.value;
			orderObj['confirmationStatus'] =  order.confirmationStatus.value;
			orderObj['creationDate'] =  order.creationDate;

			var defaultShipment, shippingAddress;
	        defaultShipment = order.getDefaultShipment();
	        shippingAddress = defaultShipment.getShippingAddress();
	        if(shippingAddress) {
	            var phoneNo = shippingAddress.getPhone();
		        var customerName = shippingAddress.getFullName();
		        orderObj['phoneNo'] = phoneNo;
		    	orderObj['customerName'] = customerName;
	        }

	        orderObj['deliveryDate'] =  defaultShipment.custom.deliveryDate;
	        orderObj['deliveryTime'] =  defaultShipment.custom.deliveryTime;
	        orderObj['orderTotal'] =  order.totalGrossPrice.value;
	        orderObj['items'] = order.productLineItems.length;
	        orderObj['itemsQuantities'] = order.productQuantityTotal;
	        orderObj['defaultShipment'] = order.defaultShipment;
	        orderObj['shipments'] = order.shipments;
	        var dt = getOrderDeliveryType(order);
	        orderObj['deliveryType'] = (dt=="Core")?"Local":dt;
	        orderObj['reSchedule'] = canBeReschedule(order);
	        
	        for each (var shipment in order.shipments) {
        		if (!empty(shipment.shippingMethodID) && shipment.productLineItems.length > 0) {
        			var shippingMethod = shipment.shippingMethodID;
        		}
        	}
	        orderObj['shippingMethod'] = !empty(shippingMethod) ? shippingMethod : '';
	        ordersArray.push(orderObj);

		}
	 	eventObj['orders'] = ordersArray;
	 	eventsArray.push(eventObj);

	}
	eventsOrders['eventsObjs'] = eventsArray;
	var eventsDataStr = JSON.stringify(eventsOrders);
	return {
		count			: eventsIter.count,
		eventsDataStr	: eventsDataStr
	}
}

function getOrderDeliveryType(order)
{
	var delivery = "";

	if(order && order != null)
	{
		var productLineItems = order.getProductLineItems();

		if(productLineItems && productLineItems.length > 0)
		{
			for each (var pli in productLineItems) {

				if(pli.product.custom.shippingInformation != null && pli.product.custom.shippingInformation.length > 0)
				{
					if(delivery != "" && delivery != pli.product.custom.shippingInformation)
					{
						delivery = "Mixed";
					}
					else
					{
						delivery = pli.product.custom.shippingInformation;
					}

				}

			}

		}
	}

	return delivery;
}

function getShipmentDeliveryType(shipment)
{
	var delivery = "";

	if(shipment && shipment != null) {
		var pliIterator = shipment.getProductLineItems().iterator();

		while (pliIterator.hasNext()) {
			var pli : Product = pliIterator.next();
		
			if(pli.product.custom.shippingInformation != null && pli.product.custom.shippingInformation.length > 0) {
				if(delivery != "" && delivery != pli.product.custom.shippingInformation) {
					delivery = "Mixed";
				} else {
					delivery = pli.product.custom.shippingInformation;
				}
	
			}
		}
	}

	return delivery;
}

function getEvent(eventId) {
	if (!empty(eventId)) {
		if(customer.profile.custom.accountType != "Admin")
		{
			var qStr = 'custom.userId={0} and custom.ID={1}';
			var eventObj = CustomObjectMgr.queryCustomObject('fulfillment_events', qStr, customer.ID, eventId);
		}
		else
		{
			var qStr = 'custom.ID={0}';
			var eventObj = CustomObjectMgr.queryCustomObject('fulfillment_events', qStr, eventId);
		}
		return eventObj;
	}
}

/*
 * This function returns the dw.order object based on the orderId & eventId
 * */
function getOrder(orderId, eventId) {
	if (!empty(orderId) && !empty(eventId)) {
		var orderObj = OrderMgr.queryOrder('orderNo={0} AND custom.eventId={1}', orderId, eventId);
		return orderObj;
	}
}

function eventOrderSummary()
{
	var orderId =  params.orderId.stringValue;
	var eventId =  params.eventId.stringValue;
	var showScheduleThisTime = params.showScheduleThisTime.booleanValue;
	pageMeta.update(dw.content.ContentMgr.getContent('fulfillment-order-summary'));

	var eventObj = getEvent(eventId);
	if(empty(eventObj)) {
		response.redirect(URLUtils.https('Fulfillment-Dashboard', 'eventDeleted', true));
		return;
	}
	var orderObj = getOrder(orderId, eventId);
	if(empty(orderObj)) {
		response.redirect(URLUtils.https('Fulfillment-Dashboard', 'eventDeleted', true));
		return;
	}
	var orderStep = getOrderStep(orderObj, true);
	var orderObjShippingMethod = app.getController('SearchOrders').GetShippingMethod(orderObj);//getShippingMethod(orderObj);
	var orderDeliveryType = getOrderDeliveryType(orderObj)
	
	var osm = app.getController('SearchOrders').GetShippingMethod(orderObj);
	if(orderStep == 3) { // The selected order is already submitted, so redirect to OrderConfirmation page
		response.redirect(URLUtils.https('Fulfillment-OrderConfirmation', 'orderIds', orderId, 'eventId', eventId));
	} else if((empty(showScheduleThisTime) || !showScheduleThisTime) && orderStep == 2 && (orderObjShippingMethod == 'storePickup' || orderObjShippingMethod == 'dropship')) { 
		// Order is already scheduled (Dropship and Warehouse orders cannot be rescheduled)
		response.redirect(URLUtils.https('Fulfillment-Billing', 'orderId', orderId, 'eventId', eventId));
	} else if(orderDeliveryType == 'Mixed' && osm == 'storePickup' && orderStep == 2) {
		response.redirect(URLUtils.https('Fulfillment-Billing', 'orderId', orderId, 'eventId', eventId));
	}
	app.getView({
		accountType	: customer.profile.custom.accountType,
		eventObj	: eventObj,
		orderObj	: orderObj,
		orderStep	: orderStep,
		orderDeliveryType : orderDeliveryType,
		orderObjShippingMethod	: orderObjShippingMethod
	}).render('fulfillment/order/ordersummary');
}

function getEventOrderSummary()
{
	var orderId =  params.orderId.stringValue;
	var eventId =  params.eventId.stringValue;
	session.custom.orderId = orderId;
	session.custom.eventId = eventId;

	var orderObj = new Object();

	var eventObj = getEvent(eventId);
	if(empty(eventObj)) {
		response.redirect(URLUtils.https('Fulfillment-Dashboard', 'eventDeleted', true));
		return;
	}
	var dwOrder = getOrder(orderId, eventId);
	if(empty(dwOrder)){
		response.redirect(URLUtils.https('Fulfillment-Dashboard', 'eventDeleted', true));
		return;
	}
	orderObj['order'] = dwOrder;

	 var dt = getOrderDeliveryType(orderObj.order);
     orderObj['deliveryType'] = (dt=="Core")?"Local":dt;
     var order = orderObj.order;
     
     orderObj['localDeliveryProductsQuantity'] = 0;
     orderObj['localDeliveryProductsItemsQuantity'] = 0;
     orderObj['dropShipDeliveryProductsQuantity'] = 0;
     orderObj['dropShipDeliveryProductsItemsQuantity'] = 0;
	 for each(var pli in order.productLineItems) {
		 if (pli.product.custom.shippingInformation.toLowerCase() == 'core') {
			 orderObj['localDeliveryProductsQuantity']++;
			 orderObj['localDeliveryProductsItemsQuantity'] += pli.quantityValue;
		 } else {
			 orderObj['dropShipDeliveryProductsQuantity']++;
		     orderObj['dropShipDeliveryProductsItemsQuantity'] += pli.quantityValue;
		 }
	 }
	var staticSlots = dw.system.Site.getCurrent().getCustomPreferenceValue('staticATPSlots');
	var deliveryDatesObj = ATPService.getATPDeliveryDatesForNextWeek(orderObj.order, staticSlots), // 2nd param true to get static data
		orderSummary = {
			"orderObj"			: orderObj, 
			"eventObj"			: eventObj,
			"accountType"		: customer.profile.custom.accountType,
			"deliveryDatesArr"	: deliveryDatesObj.deliveryDatesArr,
			"deliveryDates"		: deliveryDatesObj.deliveryDates
		};

	// Get the Warehouse for Pickup
	var shippingAddress = orderObj['order'].getDefaultShipment().getShippingAddress();
	var customerZip = shippingAddress.getPostalCode();
	var storeParams = {
			customerZip	: customerZip,
			warehouseId	: deliveryDatesObj.firstAvailableDateObj.warehouseId
	}
	var store = app.getController('WarehousePicker').GetNearbyStores(storeParams);
	//var warehouseId = !empty(store) && 'key' in store ? store.key.custom.warehouseId : '';
	var warehouseId = deliveryDatesObj.firstAvailableDateObj.warehouseId;	// Location1 received in ATP service response will be used as warehouseId
	var serviceParams = {
			customerZip	: customerZip,
			warehouseId	: warehouseId,
			order		: orderObj['order'],
			firstAvailableDateObj : deliveryDatesObj.firstAvailableDateObj,	// First available date in the ATP slots
			isLocal		: dw.system.Site.getCurrent().getCustomPreferenceValue('staticATPSlots')	// true to get static data
	}
	// Call the ATP service to get availability date of the Warehouse
	var availabilityObj = app.getController('WarehousePicker').GetATPAvailabilityMessage(serviceParams);
	var orderStep = getOrderStep(orderObj['order'], true);

	app.getView({
		store			: store,
		availabilityObj	: availabilityObj,
		orderSummary	: orderSummary,
		orderStep		: orderStep,
		accountType     : customer.profile.custom.accountType
    }).render('fulfillment/order/detail');
}

function scheduleDelivery() {

	/*
	 * To-Do: Investigate whether we need to attach a Shipping Method explicitly through some logic
	 * */

	//mattressPipeletHelper.getISODate('08 Jul 2018, Sun');
	//'8:30am-9:00pm';
	var orderId 					= params.orderId ? params.orderId.stringValue : session.custom.orderId;
	var fulfillmentDeliveryDate 	= !params.deliveryDate.empty ? mattressPipeletHelper.getISODate(params.deliveryDate.stringValue): '';
	var fulfillmentDeliveryTime 	=  !params.deliveryTime.empty ? params.deliveryTime.stringValue: '';

	var orderObj = OrderMgr.queryOrder('orderNo={0}', orderId);
	if(empty(orderObj)) {
		response.redirect(URLUtils.https('Fulfillment-Dashboard', 'eventDeleted', true));
		return;
	}
	var eventObj = getEvent(orderObj.custom.eventId);
	if(empty(eventObj)) {
		response.redirect(URLUtils.https('Fulfillment-Dashboard', 'eventDeleted', true));
		return;
	}
	var msgObj = new Object();
	if (orderObj.confirmationStatus == dw.order.Order.CONFIRMATION_STATUS_CONFIRMED && 
		orderObj.exportStatus == dw.order.Order.EXPORT_STATUS_READY) {
		
		msgObj['noOrdersPendingMessage'] = "You don't have any pending orders to submit.";
		response.getWriter().println(JSON.stringify(msgObj));
		return;
	}
	// This order is already exported and cannot be scheduled locally anymore
	if(orderObj.getExportStatus().value == dw.order.Order.EXPORT_STATUS_EXPORTED) {
		response.getWriter().println('EXPORT_STATUS_EXPORTED');	// Redirect to Order Confirmation page
		return;
	}
	
	var shipments = orderObj.shipments;
	for each(var shipment in shipments) {

		try {
			Transaction.wrap(function () {
				var sdt = getShipmentDeliveryType(shipment);
				if(sdt == 'Drop Ship') {
					shipment.custom.deliveryDate = mattressPipeletHelper.getISODate('31 Dec 2049, Sun');
					shipment.custom.deliveryTime = '08:30am-09:00pm';
					
					session.custom.deliveryDate = mattressPipeletHelper.getISODate('31 Dec 2049, Sun');;
					session.custom.deliveryTime = '08:30am-09:00pm';
				}else {
				
					shipment.custom.deliveryDate = fulfillmentDeliveryDate;
					shipment.custom.deliveryTime = fulfillmentDeliveryTime;
					
					session.custom.deliveryDate = fulfillmentDeliveryDate;
					session.custom.deliveryTime = fulfillmentDeliveryTime;
					var nonISODeliveryDate = fulfillmentDeliveryDate.replace('-','/', 'g').split('T');
				}
				var pliIterator = shipment.getProductLineItems().iterator();
            	while (pliIterator.hasNext()) {

            		var pli : Product = pliIterator.next();
                	pli.custom.deliveryDate = shipment.custom.deliveryDate;

                	if (shipment.custom.deliveryDate && dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule')) {
                		if(sdt == 'Drop Ship') {
                			pli.custom.InventLocationId = dw.system.Site.getCurrent().getCustomPreferenceValue('InventLocationId');
                		}else {
                			pli.custom.InventLocationId = dw.system.Site.getCurrent().getCustomPreferenceValue('InventLocationId');
	                		pli.custom.InventLocationId = params.location1.stringValue;
	            			pli.custom.secondaryWareHouse = params.location2.stringValue;
	            			pli.custom.mfiDSZoneLineId = params.mfiDSZoneLineId.stringValue;
	            			pli.custom.mfiDSZipCode = params.mfiDSZipCode.stringValue;
	            			session.custom.deliveryZone = params.mfiDSZoneLineId.stringValue;
                		}
            			var optionLineItems = pli.optionProductLineItems;

            			for (var j = 0; j < optionLineItems.length; j++) {

            				var oli = optionLineItems[j];

            				if (oli.productID != 'none') {
            					oli.custom.InventLocationId = params.location1.stringValue;
            				}
            			}
                	}
				}
			});
		} catch(e) {
			Logger.error("Error while adding delivery Dates: " + e.message);
		}
	}
}

function rescheduledOrdersOnLogin() {

	var qMap = new HashMap();
	qMap.put('custom.userId', customer.ID);

	var eventsIter : dw.util.SeekableIterator = CustomObjectMgr.queryCustomObjects('fulfillment_events', qMap, 'creationDate DESC');

	while(eventsIter.hasNext()) {
		var event = eventsIter.next();
		var ordersIter : dw.util.SeekableIterator = OrderMgr.queryOrders('custom.eventId={0} AND confirmationStatus!={1} AND exportStatus!={2} AND (status={3} or status={4}) AND status!={5}', 'creationDate desc',
				event.custom.ID, dw.order.Order.CONFIRMATION_STATUS_CONFIRMED, dw.order.Order.EXPORT_STATUS_READY, dw.order.Order.ORDER_STATUS_CREATED, dw.order.Order.ORDER_STATUS_NEW, dw.order.Order.ORDER_STATUS_OPEN  );
		var args = new Object();
		args.storeId = event.custom.storeId;
		
		var ordersArray = [];
		while(ordersIter.hasNext()) {

			var order = ordersIter.next(),
			defaultShipment = order.getDefaultShipment();

			//deliveryDatesArr = ATPService.getATPDeliveryDatesForNextWeek(order),
			//deliveryDatesSession = session.custom.deliveryDates;

			if( !empty(defaultShipment.custom.deliveryDate) &&  defaultShipment.custom.deliveryDate != null) {
//				var orderDeliveryDate = new Date(defaultShipment.custom.deliveryDate).toDateString().replace(/,/g,"").replace(' ', '', 'g');
//				var orderDeliveryTime = defaultShipment.custom.deliveryTime.replace(/[am,pm]/g,"");
//				[start, end] = orderDeliveryTime.split("-");
//				var orderEndUnix = Date.parse(new Date(defaultShipment.custom.deliveryDate).toLocaleDateString() +" "+end+" GMT");
//				var endArr = [];

				try{
					Transaction.wrap(function () {

//						if(orderDeliveryDate in deliveryDatesSession){
//							var timeSlots = deliveryDatesSession[orderDeliveryDate].timeslots;
//
//							for (var i=0; i<timeSlots.length; i++) {
//								var timeSlotEndUnix = Date.parse(timeSlots[i].endTime);
//								endArr.push(timeSlotEndUnix);
//							}

						//	if( startArr.indexOf(orderEndUnix) < 0) {
								var shipments = order.shipments;
								for each(var shipment in shipments) {
									shipment.custom.deliveryDate = "";
									shipment.custom.deliveryTime = "";

									if (shipment.shippingMethodID == 'storePickup') {	// Only reset the shipping method of shipment that was scheduled as Warehouse Pickup
										// Set Default Shipping Method to make sure the attached shipping method (storePickup) is removed
										try {
							            	var allShippingMethods = ShippingMgr.getAllShippingMethods();
							            	for each (var shippingMethod in allShippingMethods) {
							        			if (shippingMethod.isDefaultMethod()) {
							        				shipment.setShippingMethod(shippingMethod);
							        				var cart = app.getModel('Cart').goc();
							        				cart.calculate(); // To-do: Find out if this function needs to be run for 'order' instead of 'cart'
							        				Logger.error('Default Shipping method set on Login: '+shippingMethod.ID);
							                    }
							            	}
										} catch (e) {
											Logger.error('Failed to set default Shipping Method on Login: ' + e.toString());
										}

										var pliIterator = shipment.getProductLineItems().iterator();

										while (pliIterator.hasNext()) {

											var pli : Product = pliIterator.next();
											pli.custom.InventLocationId = "";
					            			pli.custom.secondaryWareHouse = "";
					            			pli.custom.mfiDSZoneLineId = "";
					            			pli.custom.mfiDSZipCode = "";

					            			var optionLineItems = pli.optionProductLineItems;

					            			for (var j = 0; j < optionLineItems.length; j++) {
					            				var oli = optionLineItems[j];
					            				if (oli.productID != 'none') {
					            					oli.custom.InventLocationId = "";
					            				}
					            			}
					        				// Apply the delivery charges of Local Delivery on setting the shipping method back to Local Delivery (Nationwide)
											var servicePriceObj;
				        		        	if( pli.product.custom.shippingInformation.toLowerCase() == 'core' ) {
			        		        			args['serviceSKU'] = 'tpf127550';
			        		        			servicePriceObj = getServicePrice(args);
												if (!empty(servicePriceObj)) {
						        		        	calculateShippingCosts(order, servicePriceObj, true); // 3rd param true is the 1st param is 'dw.order' object instead of 'cart'
							        				calculateTax(order);
							        				order.updateTotals();
												}
			        		        		}
										}
									}
								}	// End Shipments Loop
							//}

						//}
					});
				} catch(e) {
					Logger.error('Order reschedule failed: ' + e.toString());
					return false;
				}
			}
		}
	}
	return true;
}

/**
 * Edit customer information on order details page
 */
function editCustomerInfo() {
	var orderId = '',
		eventId = '',
		orderObj = '';
	if (!empty(params.orderId.stringValue) && !empty(params.eventId.stringValue)) {
		orderId = params.orderId.stringValue;
		eventId = params.eventId.stringValue;

		var eventObj = getEvent(eventId);
		if(empty(eventObj)) {
			response.getWriter().println('eventDeleted'); // Redirect from Ajax to Dashboard
			//response.redirect(URLUtils.https('Fulfillment-Dashboard', 'eventDeleted', true));
			return;
		}
		var orderObj = OrderMgr.queryOrder('orderNo={0}', orderId);
		orderObj = OrderMgr.queryOrder('orderNo={0} AND custom.eventId={1}', orderId, eventId);
		if(empty(orderObj)) {
			response.getWriter().println('eventDeleted'); // Redirect from Ajax to Dashboard
			//response.redirect(URLUtils.https('Fulfillment-Dashboard', 'eventDeleted', true));
			return;
		}
		
		if (!empty(orderObj) || orderObj != null) {
			// Populate the form with customer info taken from the orderObj
			var shippingAddress = orderObj.getDefaultShipment().getShippingAddress();
			app.getForm('editcustomerinfo').object.customer.firstname.value		= shippingAddress.getFirstName();
			app.getForm('editcustomerinfo').object.customer.lastname.value		= shippingAddress.getLastName();
			app.getForm('editcustomerinfo').object.address.address1.value		= shippingAddress.getAddress1();
			app.getForm('editcustomerinfo').object.address.address2.value		= shippingAddress.getAddress2();
			app.getForm('editcustomerinfo').object.address.city.value			= shippingAddress.getCity();
			app.getForm('editcustomerinfo').object.address.states.state.value	= shippingAddress.getStateCode();
			app.getForm('editcustomerinfo').object.address.postal.value			= shippingAddress.getPostalCode();
			app.getForm('editcustomerinfo').object.customer.phone.value			= shippingAddress.getPhone();
			app.getForm('editcustomerinfo').object.customer.email.value			= shippingAddress.custom.email;

			app.getView({
				ContinueURL: URLUtils.https('Fulfillment-HandleEditCustomerInfo', 'eventId', eventId, 'orderId', orderId)
			}).render('fulfillment/order/editcustomerinfo');
		} else {
			response.redirect(URLUtils.https('Fulfillment-OrderSummary', 'eventId', eventId, 'orderId', orderId));
		}
	}
}

/**
 * Edit customer information on order details page Form handler
 */
function handleEditCustomerInfo()
{
	app.getForm('editcustomerinfo').handleAction({
        cancel: function () {
        	var orderId = '',
	    		eventId = '';
	    	if (!empty(params.orderId.stringValue) && !empty(params.eventId.stringValue)) {
	    		orderId = params.orderId.stringValue;
	    		eventId = params.eventId.stringValue;
	    		app.getForm('editcustomerinfo').clear();
	            response.redirect(URLUtils.https('Fulfillment-OrderSummary', 'eventId', eventId, 'orderId', orderId));
	    	}
        },
        save: function () {
        	var orderId = '',
	    		eventId = '';
        	if (!empty(params.orderId.stringValue) && !empty(params.eventId.stringValue)) {
				orderId = params.orderId.stringValue;
				eventId = params.eventId.stringValue;
				var eventObj = getEvent(eventId);
				if(empty(eventObj)) {
					response.redirect(URLUtils.https('Fulfillment-Dashboard', 'eventDeleted', true));
					return;
				}
				var orderObj = OrderMgr.queryOrder('orderNo={0} AND custom.eventId={1}', orderId, eventId);

				if (!empty(orderObj) || orderObj != null) {
		        	Transaction.wrap(function () {
		        		var orderShipments = orderObj.shipments;
		        		for each (var shipment in orderShipments) {
			        		var shippingAddress = shipment.getShippingAddress();
				 	        shippingAddress.setFirstName(app.getForm('editcustomerinfo.customer.firstname').value());
				 	        shippingAddress.setLastName(app.getForm('editcustomerinfo.customer.lastname').value());
				 	        shippingAddress.setAddress1(app.getForm('editcustomerinfo.address.address1').value());
				 	        if (!empty(app.getForm('editcustomerinfo.address.address2').value())) {
				 	        	shippingAddress.setAddress2(app.getForm('editcustomerinfo.address.address2').value());
				 	        }
				 	        shippingAddress.setCity(app.getForm('editcustomerinfo.address.city').value());
				 	        shippingAddress.setPostalCode(app.getForm('editcustomerinfo.address.postal').value());
				 	        shippingAddress.setStateCode(app.getForm('editcustomerinfo.address.states.state').value());
				 	        shippingAddress.setPhone(app.getForm('editcustomerinfo.customer.phone').value());
				 	        shippingAddress.custom.email = app.getForm('editcustomerinfo.customer.email').value();		        			
		        		}
			 	        app.getForm('editcustomerinfo').clear();
			 	    });
				} else {
					response.redirect(URLUtils.https('Fulfillment-Dashboard', 'eventDeleted', true));
					return;
				}
				response.redirect(URLUtils.https('Fulfillment-OrderSummary', 'eventId', eventId, 'orderId', orderId));
	        }
        },
	    error: function () {
	    	var orderId = '',
    			eventId = '';
	    	if (!empty(params.orderId.stringValue) && !empty(params.eventId.stringValue)) {
	    		orderId = params.orderId.stringValue;
	    		eventId = params.eventId.stringValue;
	    		response.redirect(URLUtils.https('Fulfillment-OrderSummary', 'eventId', eventId, 'orderId', orderId));
	    	}
        }
    });
}

function billing() {
	var orderId 		= (params.orderId.stringValue)?params.orderId.stringValue :session.custom.orderId,
		eventId 		= (params.eventId.stringValue)?params.eventId.stringValue: session.custom.eventId,
		eventsData 		= getFulfillmentEventsOrders(),
		orderStep 		= "",
		orderTotals		= 0,
		orderItems		= 0,
		flag			= false,
		servicePriceObj	= "",
		args 			= new Object();
	pageMeta.update(dw.content.ContentMgr.getContent('fulfillment-billing'));
	
	if(orderId != null && eventId != null) {
		var eventObj = getEvent(eventId);
		if(empty(eventObj)) {
			response.redirect(URLUtils.https('Fulfillment-Dashboard', 'eventDeleted', true));
			return;
		}
		var	orderObj 		= getOrder(orderId, eventId);
		var	shipments 		= orderObj.shipments,
			defaultShipment = orderObj.defaultShipment,
			eventsData 		= getFulfillmentEventsOrders(orderId);

		var orderObjShippingMethod = app.getController('SearchOrders').GetShippingMethod(orderObj);//getShippingMethod(orderObj);
		args['storeId'] = eventObj.custom.storeId;
		var orderStep = getOrderStep(orderObj, true);
		
		if(orderStep == 3) { // The selected order is already submitted, so redirect to OrderConfirmation page
			response.redirect(URLUtils.https('Fulfillment-OrderConfirmation', 'orderIds', orderId, 'eventId', eventId));
			return;
		}
		
		var cart = app.getModel('Cart').get(orderObj);

		for each(var shipment in cart.object.shipments) {
			var pliIterator = shipment.getProductLineItems().iterator();
			while (pliIterator.hasNext()) {
        		var pli : Product = pliIterator.next();
	        	if(shipment.shippingMethodID == 'storePickup') {
        			args['serviceSKU'] = 'tpf127542';
        			servicePriceObj = getServicePrice(args);
        		}
        	}
		}

		if(!empty(servicePriceObj)) {
			Transaction.wrap(function () {
				calculateShippingCosts(cart, servicePriceObj);
				calculateTax(cart.object);
				cart.updateTotals();
			});
		}
	}
	
	if((customer.profile.custom.accountType != "Admin" && !empty(eventObj) && customer.ID != eventObj.custom.userId)) {
		response.redirect(URLUtils.https('Fulfillment-Dashboard'));
	}
	
	var	eventsArray 	= [],
		eventsDataObj 	= JSON.parse(eventsData.eventsDataStr),
		eventsArr 		= eventsDataObj.eventsObjs,
		totalOrdersReadyToProcess = 0,
		servicePrice = "";

	eventsArr.forEach(function(event) {

		var ordersArray = [];
		var eventObj = new Object();

		eventObj['eventID'] = event.eventID;
		eventObj['eventName'] = event.eventName;
		eventObj['eventDate'] = event.eventDate;

		event.orders.forEach(function(order) {
			var orderObj = OrderMgr.getOrder(order.orderNo);
			var shippingMethod = '';
			// Add Shipping Method to orderObj
	        for each (var shipment in orderObj.shipments) {
        		if (!empty(shipment.shippingMethodID) && shipment.productLineItems.length > 0) {
        			shippingMethod = shipment.shippingMethodID;
        			break;
        		}
        	}

	        if( (!empty(order.deliveryDate) || order.deliveryDate != null) && 
	        	(order.status == dw.order.Order.ORDER_STATUS_NEW || order.status == dw.order.Order.ORDER_STATUS_CREATED) && 
	        	order.confirmationStatus != dw.order.Order.CONFIRMATION_STATUS_CONFIRMED && 
	        	order.exportStatus != dw.order.Order.EXPORT_STATUS_READY) {

				var orderObj = new Object();
				orderObj['orderNo'] = order.orderNo;
				orderObj['status'] = order.status;
				orderObj['paymentStatus'] =  order.paymentStatus;
				orderObj['exportStatus'] =  order.exportStatus;
				orderObj['phoneNo'] = order.phoneNo;
		    	orderObj['customerName'] = order.customerName;
		        orderObj['deliveryDate'] =  order.deliveryDate;
		        orderObj['deliveryTime'] =  order.deliveryTime;

		        if(!empty(order.deliveryDate)) {
		        	totalOrdersReadyToProcess++;
		        }
		        if(!flag) {
					orderStep = getOrderStep( getOrder(order.orderNo, event.eventID), true);
					flag = true;
				}
		        orderObj['orderTotal'] =  order.orderTotal;
		        orderObj['items'] = order.items;
		        orderObj['itemsQuantities'] = order.itemsQuantities;
		        orderObj['deliveryType'] = order.deliveryType;
		        orderObj['shippingMethod'] = !empty(shippingMethod) ? shippingMethod : '';
		        orderObj['defaultShipment'] = defaultShipment;
		        orderObj['orderStep'] = getOrderStep(orderObj);
		        
		        if(order.deliveryType == 'Mixed') {
		        	var eOrder = OrderMgr.getOrder(order.orderNo);
		        	var shipments = eOrder.shipments;
		        	for each(var shipment in shipments) {
			        	var deliveryD = false;
		        		var pliIterator = shipment.getProductLineItems().iterator();
		        		while (pliIterator.hasNext()) {
		        			var pli : Product = pliIterator.next();
		        			if(pli.product.custom.shippingInformation == 'Core') {
		        				orderObj['deliveryDate'] =  shipment.custom.deliveryDate;
		        				orderObj['deliveryTime'] =  shipment.custom.deliveryTime;
		        				deliveryD = true;
		        				break;
		        			}
		        		}
		        		if(deliveryD) {
		        			break;
		        		}
		        	}
		        }
		        
		        ordersArray.push(orderObj);

			}
		});

		if(!empty(ordersArray)) {
			eventObj['orders'] = ordersArray;
		 	eventsArray.push(eventObj);
		}
	});

	var ordersNotPlaced = params.ordersNotPlaced.stringValue;

	app.getView({
		orderSummary : {
			eventObj		: eventObj,
			orderObj		: orderObj,
			orderObjShippingMethod : orderObjShippingMethod,
			orderStep		: orderStep,
			eventsArray		: eventsArray,
			totals			: {totalOrdersReadyToProcess: totalOrdersReadyToProcess},
			ordersNotPlaced	: ordersNotPlaced,
			ContinueURL		: URLUtils.https('Fulfillment-HandleUsaepaypayment')
		}
	}).render('fulfillment/order/billing');
}

function getShippingMethod(order) {
	var shippingMethod = '';
	for each (var shipment in order.shipments) {
		if (!empty(shipment.shippingMethodID) && shipment.productLineItems.length > 0) {
			shippingMethod = shipment.shippingMethodID;
			break;
		}
	}
	return shippingMethod;
}
/*
 * This function returns the OrderStep number, that is used for showing the order step in Navigation Banner on Order Detail Page
 * Input parameters:
 * 	order : object
 * 	dwOrderObj : boolean, true if the param (order) is an instance of dw.order.Order
 * */
function getOrderStep(order, dwOrderObj) {
	/*
 	CONFIRMATION_STATUS_NOTCONFIRMED  :  Number = 0 | constant for when Confirmation Status is Not Confirmed
	CONFIRMATION_STATUS_CONFIRMED  :  Number = 2 | constant for when Confirmation Status is Confirmed

	EXPORT_STATUS_NOTEXPORTED  :  Number = 0 | constant for when Export Status is Not Exported
	EXPORT_STATUS_EXPORTED  :  Number = 1 | constant for when Export Status is Exported
	EXPORT_STATUS_READY  :  Number = 2 | constant for when Export Status is ready to be exported.
	EXPORT_STATUS_FAILED  :  Number = 3 | constant for when Export Status is Failed

	ORDER_STATUS_CREATED  :  Number = 0 | constant for when Order Status is Created
	ORDER_STATUS_NEW  :  Number = 3 | constant for when Order Status is New
	ORDER_STATUS_OPEN  :  Number = 4 | constant for when Order Status is Open
	ORDER_STATUS_COMPLETED  :  Number = 5 | constant for when Order Status is Completed
	ORDER_STATUS_CANCELLED  :  Number = 6 | constant for when Order Status is Cancelled
	ORDER_STATUS_REPLACED  :  Number = 7 | constant for when Order Status is Replaced
	ORDER_STATUS_FAILED  :  Number = 8 | constant for when Order Status is Failed

	PAYMENT_STATUS_NOTPAID  :  Number = 0 | constant for when Payment Status is Not Paid
	PAYMENT_STATUS_PARTPAID  :  Number = 1 | constant for when Payment Status is Part Paid
	PAYMENT_STATUS_PAID  :  Number = 2 | constant for when Payment Status is Paid

	SHIPPING_STATUS_NOTSHIPPED  :  Number = 0 | constant for when Shipping Status is Not shipped
	SHIPPING_STATUS_PARTSHIPPED  :  Number = 1 | constant for when Shipping Status is Part Shipped
	SHIPPING_STATUS_SHIPPED  :  Number = 2 | constant for when Shipping Status is Shipped
	*/

	var orderStep : Number = -1;
	if (!empty(order)) {
		orderStep = 0;	// Order does not exist
		if (order.status == dw.order.Order.ORDER_STATUS_CREATED) // CREATED
			orderStep = 1; // New on Order Detail page
		if (dwOrderObj) {
			// Finding out Shipping Method
			var shippingMethod = getShippingMethod(order);
		    // Finding out Order Schedule
		    var orderScheduledFound = false;
		    for each (var shipment in order.shipments) {
				if (!empty(shipment.custom.deliveryDate) && !empty(shipment.custom.deliveryTime)) {
					orderScheduledFound = true;
				}
			}

		    if (shippingMethod == 'storePickup' || orderScheduledFound)
		    	orderStep = 2; // Scheduled on Order Detail page
		} else {
			if ((!empty(order['deliveryDate']) && !empty(order['deliveryTime'])) || order['shippingMethod'] == 'storePickup')
				orderStep = 2; // Scheduled on Order Detail page
		}
		if ((order.status == dw.order.Order.ORDER_STATUS_NEW || order.status == dw.order.Order.ORDER_STATUS_OPEN) &&
			(order.exportStatus == dw.order.Order.EXPORT_STATUS_EXPORTED || order.exportStatus == dw.order.Order.EXPORT_STATUS_READY) &&
			(order.confirmationStatus == dw.order.Order.CONFIRMATION_STATUS_CONFIRMED)) // status = NEW or OPEN & exportStatus = READY or EXPORTED & confirmationStatus = CONFIRMED
			orderStep = 3; // Submitted on Order Detail page
		if (order.paymentStatus == dw.order.Order.PAYMENT_STATUS_PAID) // PAID
			orderStep = 4; // Paid on Order Detail page
	}
	return Number(orderStep);
}

function handleUsaepaypayment() {

//	 app.getForm('paymentfulfillment').handleAction({
//	        cancel:	function () {
				var orderId = session.custom.orderId,
					eventId = session.custom.eventId;

//	        	response.redirect(URLUtils.https('Fulfillment-OrderSummary', 'eventId', eventId, 'orderId', orderId));
//	        },
//	        save:	function() {
	        	var orderNo = params.orderNo,
	    		args = new Object();

		    	var order = OrderMgr.getOrder(orderId);
		    	args['Order'] = order;

		    	var USAePayHandler = dw.system.HookMgr.callHook("app.payment.processor.USAePay", 'Handle', args);
		    	if(USAePayHandler.error){
		    		response.redirect(URLUtils.https('Fulfillment-Billing'));
		    	}
		    	if(USAePayHandler.success) {
		    		var USAePayAuthtorize = dw.system.HookMgr.callHook("app.payment.processor.USAePay", 'Authtorize', order.orderNo);
		    		var result = JSON.parse(USAePayAuthtorize);
		    	}

		    	if( empty(result.error_code) || result.error_code == null ) {
		    		var USAePayCapture = dw.system.HookMgr.callHook("app.payment.processor.USAePay", 'Capture', order.orderNo);
		    	}
//		    	var orderId 	= params.orderId.stringValue,
//    				eventId 	= params.eventId.stringValue;

		    	//response.redirect(URLUtils.https('Fulfillment-OrderSummary', 'eventId', eventId, 'orderId', orderId));
//	        },
//	        error:	function() {
//	        	var orderId 	= params.orderId.stringValue,
//    				eventId 	= params.eventId.stringValue;
//
	        	response.redirect(URLUtils.https('Fulfillment-OrderConfirmation'));
//	        }
	// });

}
function orderConfirmation() {

	var eventsArray		= [],
		eventsData 		= getFulfillmentEventsOrders(),
		eventsDataObj 	= JSON.parse(eventsData.eventsDataStr),
		eventsArr 		= eventsDataObj.eventsObjs,
		totalOrdersReadyToProcess = 0,
		noOrdersPendingMessage			= params.noOrdersPendingMessage.stringValue,
		orderStep = "",
		shipmentObj = new Object();
	pageMeta.update(dw.content.ContentMgr.getContent('fulfillment-order-confirmation'));
	
	var confirmedOrderTotals = 0,
		confirmedOrderItems = 0,
		flag = false,
		ordersSuccessfullyPlaced = [];
	
	if(!empty(params.orderIds.stringValue)) {
		ordersSuccessfullyPlaced = params.orderIds.stringValue.split(",");
	} else if (!empty(session.custom.ordersSuccessfullyPlaced)) {
		ordersSuccessfullyPlaced = session.custom.ordersSuccessfullyPlaced.split(",");	
	}	
	
	for each(var event in eventsArr) {
		
		if(customer.profile.custom.accountType != "Admin" && customer.ID != event.userId){
			response.redirect(URLUtils.https('Fulfillment-Dashboard'));
			break;
		}
		
		var ordersArray = [],
			confirmedOrderArray = [];
		var eventObj = new Object();

		eventObj['eventID'] = event.eventID;
		eventObj['eventName'] = event.eventName;
		eventObj['eventDate'] = event.eventDate;

		event.orders.forEach(function(order) {

			var eOrder = OrderMgr.getOrder(order.orderNo);
			// Add Shipping Method to orderObj
			var shippingMethod = '';
	        for each (var shipment in eOrder.shipments) {
        		if (!empty(shipment.shippingMethodID) && shipment.productLineItems.length > 0) {
        			shippingMethod = shipment.shippingMethodID;
        		}
        	}

			if( (!empty(order.deliveryDate) || order.deliveryDate != null) && 
				(order.status == dw.order.Order.ORDER_STATUS_NEW || order.status == dw.order.Order.ORDER_STATUS_CREATED) && 
				order.confirmationStatus != dw.order.Order.CONFIRMATION_STATUS_CONFIRMED && 
				(order.exportStatus != dw.order.Order.EXPORT_STATUS_READY || order.exportStatus != dw.order.Order.EXPORT_STATUS_EXPORTED)) {

				var orderObj = new Object();
					orderObj['orderNo'] = order.orderNo;
					orderObj['status'] = order.status;
					orderObj['creationDate'] = order.creationDate;
					orderObj['paymentStatus'] =  order.paymentStatus;
					orderObj['exportStatus'] =  order.exportStatus;
					orderObj['phoneNo'] = order.phoneNo;
			    	orderObj['customerName'] = order.customerName;
			        orderObj['deliveryDate'] =  order.deliveryDate;
			        orderObj['deliveryTime'] =  order.deliveryTime;

		        if(!empty(order.deliveryDate)) {
		        	totalOrdersReadyToProcess++;
		        }
		        orderObj['orderTotal'] =  order.orderTotal;
		        orderObj['items'] = order.items;
		        orderObj['itemsQuantities'] = order.itemsQuantities;
		        orderObj['deliveryType'] = order.deliveryType;
		        orderObj['orderStep'] = getOrderStep(orderObj);
		        orderObj['shippingMethod'] = !empty(shippingMethod) ? shippingMethod : '';
		        orderObj['shipmentObj'] = !empty(shipmentObj) ? shipmentObj : '';
		        orderObj['defaultShipment'] = order.defaultShipment;
		        
		        if(order.deliveryType == 'Mixed') {
		        	var eOrder = OrderMgr.getOrder(order.orderNo);
		        	var shipments = eOrder.shipments;
		        	for each(var shipment in shipments) {
			        	var deliveryD = false;
		        		var pliIterator = shipment.getProductLineItems().iterator();
		        		while (pliIterator.hasNext()) {
		        			var pli : Product = pliIterator.next();
		        			if(pli.product.custom.shippingInformation == 'Core') {
		        				orderObj['deliveryDate'] =  shipment.custom.deliveryDate;
		        				orderObj['deliveryTime'] =  shipment.custom.deliveryTime;
		        				deliveryD = true;
		        				break;
		        			}
		        		}
		        		if(deliveryD) {
		        			break;
		        		}
		        	}
		        }
		        
		        ordersArray.push(orderObj);
			}
			else if( (!empty(order.deliveryDate) || order.deliveryDate != null)	&& 
					(ordersSuccessfullyPlaced.indexOf(order.orderNo) > -1 && 
					order.confirmationStatus == dw.order.Order.CONFIRMATION_STATUS_CONFIRMED && 
					(order.exportStatus == dw.order.Order.EXPORT_STATUS_READY || order.exportStatus == dw.order.Order.EXPORT_STATUS_EXPORTED)) ) {

				if(!flag) {
					orderStep = getOrderStep( getOrder(order.orderNo, event.eventID), true);
					flag = true; // orderStep was needed just once
				}
				var confirmedOrderObj = new Object();
				confirmedOrderObj['orderNo'] = order.orderNo;
				confirmedOrderObj['status'] = order.status;
				confirmedOrderObj['creationDate'] = order.creationDate;
				confirmedOrderObj['paymentStatus'] =  order.paymentStatus;
				confirmedOrderObj['exportStatus'] =  order.exportStatus;
				confirmedOrderObj['phoneNo'] = order.phoneNo;
				confirmedOrderObj['customerName'] = order.customerName;
				confirmedOrderObj['deliveryDate'] =  order.deliveryDate;
				confirmedOrderObj['deliveryTime'] =  order.deliveryTime;
	            confirmedOrderObj['orderTotal'] =  order.orderTotal;
	            confirmedOrderObj['items'] = order.items;
	            confirmedOrderObj['itemsQuantities'] = order.itemsQuantities;
		        confirmedOrderObj['deliveryType'] = order.deliveryType;
		        confirmedOrderObj['orderStep'] = getOrderStep(confirmedOrderObj);
		        confirmedOrderObj['shippingMethod'] = !empty(shippingMethod) ? shippingMethod : '';
		        confirmedOrderObj['shipmentObj'] = !empty(shipmentObj) ? shipmentObj : '';
		        confirmedOrderObj['defaultShipment'] = order.defaultShipment;

		        if(order.deliveryType == 'Mixed') {
		        	var eOrder = OrderMgr.getOrder(order.orderNo);
		        	
		        	var shipments = eOrder.shipments;
		        	for each(var shipment in shipments) {
			        	var deliveryD = false;
		        		var pliIterator = shipment.getProductLineItems().iterator();

		        		while (pliIterator.hasNext()) {
		        			var pli : Product = pliIterator.next();
		        		
		        			if(pli.product.custom.shippingInformation == 'Core') {
		        				confirmedOrderObj['deliveryDate'] =  shipment.custom.deliveryDate;
		        				confirmedOrderObj['deliveryTime'] =  shipment.custom.deliveryTime;
		        				
		        				deliveryD = true;
		        				break;
		        				
		        			}
		        		}
		        		if(deliveryD) {
		        			break;
		        		}
		        	}
		        	
		        }
		        
		        confirmedOrderArray.push(confirmedOrderObj);

		        confirmedOrderTotals = order.orderTotal+confirmedOrderTotals;
		        confirmedOrderItems = order.itemsQuantities+confirmedOrderItems;
			}
		});
		if(!empty(ordersArray) || !empty(confirmedOrderArray)) {
			eventObj['orders'] = ordersArray;	// Additional Orders
			eventObj['confirmedOrders'] = confirmedOrderArray;
		 	eventsArray.push(eventObj);
		}
	}

	session.custom.orderId = null;
	session.custom.eventId = null;

	app.getView({
		orderSummary : {
			orderStep		:orderStep,
			eventsArray		:eventsArray,
			noOrdersPendingMessage : noOrdersPendingMessage,
			totals			:{
					totalOrdersReadyToProcess	:totalOrdersReadyToProcess,
					confirmedOrderTotals		:(Math.round(confirmedOrderTotals*100)/100).toFixed(2),
					confirmedOrderItems			:confirmedOrderItems
				}
			}
	}).render('fulfillment/order/orderconfirmation');
}

function removeExistingPaymentInstruments(cart) {

	if (cart!=null) {
		var ccPaymentInstrs : dw.util.Collection = cart.getPaymentInstruments();
		if ( !empty(ccPaymentInstrs) && ccPaymentInstrs.length > 0 ) {
			var iter : dw.util.Iterator = ccPaymentInstrs.iterator();
			var existingPI : dw.order.OrderPaymentInstrument = null;
			while( iter.hasNext() )
			{
				existingPI = iter.next();
				if (existingPI.paymentMethod != null) {
					cart.removePaymentInstrument( existingPI );
				}
			}
		}
	}
}

function submitCSFOrder() {
	var additionalOrdersSelectedForPayment = '';
	var params = request.httpParameterMap;
	if (params.additionalOrdersSelectedForPayment.stringValue) {
		additionalOrdersSelectedForPayment = params.additionalOrdersSelectedForPayment.stringValue;
	}

	var ordersToBePlaced = !empty(additionalOrdersSelectedForPayment) ? additionalOrdersSelectedForPayment.split(',') : [];
	var orderId = params.orderId.stringValue;
	if(empty(ordersToBePlaced)) ordersToBePlaced.push('');
	
	if (orderId != 'null') {
		ordersToBePlaced.push(orderId);
	}
	
	for (var i=0; i<ordersToBePlaced.length; i++) {
		if (ordersToBePlaced[i] == '' || ordersToBePlaced[i] == null) {
			continue;
		}
		orderId = ordersToBePlaced[i];
		var order = OrderMgr.getOrder(orderId);
		
		if(order.confirmationStatus == dw.order.Order.CONFIRMATION_STATUS_CONFIRMED && 
			(order.exportStatus == dw.order.Order.EXPORT_STATUS_READY || order.exportStatus == dw.order.Order.EXPORT_STATUS_EXPORTED) ) {
			
			var index = ordersToBePlaced.indexOf(orderId);
			if (index !== -1){ 
				ordersToBePlaced.splice(index, 1);
				i = 0;
			}			
		}
	}
	Logger.error('Orders to be placed after splice: '+ordersToBePlaced);
	 if(!empty(ordersToBePlaced) && ordersToBePlaced.length == 1)  {
		var paramsObj = {
				allOrdersPlaced	: false,
				eventId			: params.eventId.stringValue,
				orderId			: orderId,
				noOrdersPendingMessage	: "You don't have any pending orders to submit."
			};
		response.getWriter().println(JSON.stringify(paramsObj));	// Hit Fulfillment-Billing and show error message through Ajax call (billing.js)
		return;
	}
	var ordersNotPlaced = '';
	var ordersSuccessfullyPlaced = [];
	var someOrdersFailedSubmission = false;
	// Process Bulk Payment / Submit Order
	for (var i=0; i<ordersToBePlaced.length; i++) {
		if (ordersToBePlaced[i] == '' || ordersToBePlaced[i] == null) {
			continue;
		}
		var orderId = ordersToBePlaced[i];
		var order = OrderMgr.getOrder(orderId);
		
		if(order.confirmationStatus == dw.order.Order.CONFIRMATION_STATUS_CONFIRMED && 
			order.exportStatus == dw.order.Order.EXPORT_STATUS_READY){
			continue;
		}
		var cart = app.getModel('Cart').get(order);

		// Remove existing Payment Instruments present in cart
		try {
			Transaction.wrap(function () {
				removeExistingPaymentInstruments(cart);
			});
		} catch (e) {
			Logger.error('Error occured in removing Payment Instrument: ' + e.message);
		}
		// Attach Payment Instrument to Cart
		var paymentMethod :String = 'BML'; // This needs to be dynamic
		if (!empty(paymentMethod) && paymentMethod.equals(dw.order.PaymentInstrument.METHOD_BML)) {
			Transaction.wrap(function () {
				var PaymentMgr 			= require('dw/order/PaymentMgr');
				var	paymentInstrument	= cart.createPaymentInstrument(dw.order.PaymentInstrument.METHOD_BML, order.getTotalGrossPrice());
				var paymentMethodId		= paymentInstrument.getPaymentMethod();
				var paymentMethod		= PaymentMgr.getPaymentMethod(paymentMethodId);
				var	paymentProcessor	= paymentMethod.getPaymentProcessor();
				paymentInstrument.paymentTransaction.transactionID		= order.orderNo;
				paymentInstrument.paymentTransaction.paymentProcessor	= paymentProcessor;
			});
		}
		// Payment Instrument attachment complete

		// Concatenate Address2 in Address1
		Transaction.wrap(function () {
	        var orderShipments = order.shipments;
	        for each (var shipment in orderShipments) {
        		var shippingAddress = shipment.getShippingAddress();
        		if (!empty(shippingAddress.getAddress2())) {
    		        shippingAddress.setAddress1(shippingAddress.getAddress1() + ', ' + shippingAddress.getAddress2());
    	        }
    		}
	    });
		
		var orderPlacementStatus = Transaction.wrap(function () {
	        if (OrderMgr.placeOrder(order) === Status.ERROR) {
	            OrderMgr.failOrder(order);
	            return false;
	        }
	        return true;
	    });

	    if (orderPlacementStatus === Status.ERROR) {
	    	someOrdersFailedSubmission = true;
	    	ordersNotPlaced += ', ' + orderId;
	    	// Stay on Billing screen in case of error
	    	Logger.error('@Error occured in placing order: '+orderId);
	    } else {	// Order was placed successfully
	    	try {
		    	Transaction.wrap(function () {
					order.setExportStatus(dw.order.Order.EXPORT_STATUS_READY);
					order.setConfirmationStatus(dw.order.Order.CONFIRMATION_STATUS_CONFIRMED);
					
					var now = new Date();
					now.setHours(0,0,0,0);
					order.custom.submissionDate = now;	// Submission Date will be used in dropping/deleting the Events older than 'noOfDays'
			    });
			    ordersSuccessfullyPlaced.push(orderId);
	    	} catch (e) {
	    		// To-do: Display message on Billing page for the orders whose statuses were not updated in try block
	    		var msg = e.message;
	    		Logger.error('Order Attributes (Export Status, Confirmation Status, Submission Date) could not updated for OrderId: '+orderId+'. Error reason: ' + msg);
	    	}
	    }
	}
	if (!empty(ordersNotPlaced) && someOrdersFailedSubmission) { // In case one or more orders failed to place
		var paramsObj = {
				allOrdersPlaced	: false,
				eventId			: params.eventId.stringValue,
				orderId			: orderId,
				ordersNotPlaced	: ordersNotPlaced,
				orderIds : ordersSuccessfullyPlaced.join(',')
			};
		response.getWriter().println(JSON.stringify(paramsObj));	// Hit Fulfillment-Billing and show error message through Ajax call (billing.js)
	} else {	// Success: All orders were placed
		var paramsObj = {
				allOrdersPlaced	: true,
				orderIds : ordersSuccessfullyPlaced.join(',')
			};
		response.getWriter().println(JSON.stringify(paramsObj));	// Hit Fulfillment-OrderConfirmation through Ajax call (billing.js)
	}
	session.custom.ordersSuccessfullyPlaced = ordersSuccessfullyPlaced.join(',');
    // session.custom.ordersSuccessfullyPlaced will be used to render OrderConfirmation screen and generate Receipt
	SendDealerOrderConfirmationEmail();
}

function getServicePrice(args) {
	var storeObj = CustomObjectMgr.getCustomObject('fulfillment_storeid', args.storeId);
	if (!empty(storeObj)) {
		var priceGroup = storeObj.custom.priceGroup;
		var serivceProducts = dw.catalog.CatalogMgr.getCategory('services').getProducts();

		for each( var product in serivceProducts) {
			if(product.getID() == args.serviceSKU+"-"+priceGroup ) {
				var shipmentPrice = product.getPriceModel().getPriceBookPrice('SubPriceBook').getValue();
				break;
			}
		}
		return {"shipmentPrice":shipmentPrice, "serviceSKU":args.serviceSKU};
	}
	return null;
}

function calculateShippingCosts(cart, servicePriceObj, isOrder) {
	if (isOrder) {
		var shipments : dw.order.Shipment = cart.shipments.iterator();	// dw.order.Order object is received in param: cart
	} else {
		var shipments : dw.order.Shipment = cart.object.shipments.iterator();
	}
	while(shipments.hasNext()) {

		var shipment : dw.order.Shipment = shipments.next();
		if(shipment.productLineItems.length == 0) {
			shipment.shippingLineItems[0].setPriceValue(0);
			continue;
		}
		var shippingMethod : ShippingMethod = shipment.shippingMethod;
		
		if (!empty(servicePriceObj) && shippingMethod != null && shippingMethod.ID != 'dropship') {
			shipment.custom.deliveryProduct = servicePriceObj.serviceSKU;
			shipment.custom.shipmentType = shippingMethod.getDisplayName();
			
			var shippingCost : dw.value.Money = new dw.value.Money(servicePriceObj.shipmentPrice, dw.system.Site.current.currencyCode);
			shipment.shippingLineItems[0].setPriceValue(shippingCost.value);
			Logger.error('Local Delivery Charges applied on Login');
		} else if (shippingMethod != null && shippingMethod.ID == 'dropship') {			
			shipment.custom.shipmentType = shippingMethod.getDisplayName();
			
			var shippingCost : dw.value.Money = new dw.value.Money(0, dw.system.Site.current.currencyCode);
			shipment.shippingLineItems[0].setPriceValue(shippingCost.value);
		}
	}
}

function formatTime(date) {
	  var hours = date.getHours();
	  var minutes = date.getMinutes();
	  var ampm = hours >= 12 ? 'pm' : 'am';
	  hours = hours % 12;
	  hours = hours ? hours : 12; // the hour '0' should be '12'
	  minutes = minutes < 10 ? '0'+minutes : minutes;
	  var strTime = hours + ':' + minutes + '' + ampm;
	  return strTime;
}

function calculateTax(basket) {

	/* removing as part of the MattressFirm rebuild
	var sleepysTaxObject = new SleepysTaxObject();
	sleepysTaxObject.calculateTax(basket);
	 */

	var shipments = basket.getShipments().iterator();
	while (shipments.hasNext()) {
		var shipment = shipments.next();

		// first we reset all tax fields of all the line items
		// of the shipment
		var shipmentLineItems = shipment.getAllLineItems().iterator();
		while (shipmentLineItems.hasNext()) {
			var _lineItem = shipmentLineItems.next();
			// do not touch tax rate for fix rate items
			if (_lineItem.taxClassID === TaxMgr.customRateTaxClassID) {
				_lineItem.updateTax(_lineItem.taxRate);
			} else {
				_lineItem.updateTax(null);
			}
		}

		// identify the appropriate tax jurisdiction
		var taxJurisdictionID = null;

		// if we have a shipping address, we can determine a tax jurisdiction for it
		if (shipment.shippingAddress !== null) {
			var location = new ShippingLocation(shipment.shippingAddress);
			taxJurisdictionID = TaxMgr.getTaxJurisdictionID(location);
		}

		if (taxJurisdictionID === null) {
			taxJurisdictionID = TaxMgr.defaultTaxJurisdictionID;
		}

		// if we have no tax jurisdiction, we cannot calculate tax
		if (taxJurisdictionID === null) {
			continue;
		}

		// shipping address and tax juridisction are available
		var shipmentLineItems2 = shipment.getAllLineItems().iterator();
		while (shipmentLineItems2.hasNext()) {
			var lineItem = shipmentLineItems2.next();
			var taxClassID = lineItem.taxClassID;

			Logger.debug('1. Line Item {0} with Tax Class {1} and Tax Rate {2}', lineItem.lineItemText, lineItem.taxClassID, lineItem.taxRate);

			// do not touch line items with fix tax rate
			if (taxClassID === TaxMgr.customRateTaxClassID) {
				continue;
			}

			// line item does not define a valid tax class; let's fall back to default tax class
			if (taxClassID === null) {
				taxClassID = TaxMgr.defaultTaxClassID;
			}

			// if we have no tax class, we cannot calculate tax
			if (taxClassID === null) {
				Logger.debug('Line Item {0} has invalid Tax Class {1}', lineItem.lineItemText, lineItem.taxClassID);
				continue;
			}

			// get the tax rate
			var taxRate = TaxMgr.getTaxRate(taxClassID, taxJurisdictionID);
			// w/o a valid tax rate, we cannot calculate tax for the line item
			if (taxRate === null) {
				continue;
			}

			// calculate the tax of the line item
			lineItem.updateTax(taxRate);
			Logger.debug('2. Line Item {0} with Tax Class {1} and Tax Rate {2}', lineItem.lineItemText, lineItem.taxClassID, lineItem.taxRate);
		}
	}

	// besides shipment line items, we need to calculate tax for possible order-level price adjustments
	// this includes order-level shipping price adjustments
	if (!basket.getPriceAdjustments().empty || !basket.getShippingPriceAdjustments().empty) {
		// calculate a mix tax rate from
		var basketPriceAdjustmentsTaxRate = (basket.getMerchandizeTotalGrossPrice().value / basket.getMerchandizeTotalNetPrice().value) - 1;

		var basketPriceAdjustments = basket.getPriceAdjustments().iterator();
		while (basketPriceAdjustments.hasNext()) {
			var basketPriceAdjustment = basketPriceAdjustments.next();
			basketPriceAdjustment.updateTax(basketPriceAdjustmentsTaxRate);
		}

		var basketShippingPriceAdjustments = basket.getShippingPriceAdjustments().iterator();
		while (basketShippingPriceAdjustments.hasNext()) {
			var basketShippingPriceAdjustment = basketShippingPriceAdjustments.next();
			basketShippingPriceAdjustment.updateTax(basketPriceAdjustmentsTaxRate);
		}
	}
}

function rescheduleOrder() {
	var orderObj 					= new Object();
	var orderId 					= (params.orderId.stringValue)?params.orderId.stringValue:session.custom.orderId,
		eventId 					= (params.eventId.stringValue)?params.eventId.stringValue:session.custom.eventId,
		eventsArray 				= [],
		eventObj 					= getEvent(eventId);
	if(empty(eventObj)) {
		response.redirect(URLUtils.https('Fulfillment-Dashboard', 'eventDeleted', true));
		return;
	}
	var tempOrderObj = getOrder(orderId, eventId);
	if(empty(tempOrderObj)) {
		response.redirect(URLUtils.https('Fulfillment-Dashboard', 'eventDeleted', true));
		return;
	}
	orderObj['order'] = tempOrderObj;
	pageMeta.update(dw.content.ContentMgr.getContent('fulfillment-reschedule-order'));
		
	var orderStep = getOrderStep(orderObj['order'], true);
	// If the user did not came from the Reschedule Link, then perform the following redirections
	var comingFromRescheduleLink = params.comingFromRescheduleLink.stringValue;
	if (empty(comingFromRescheduleLink) || !comingFromRescheduleLink) {
		var orderObjShippingMethod = getShippingMethod(orderObj['order']);
		if(orderStep == 3 && !params.rescheduled.booleanValue) { // The selected order is already submitted, so redirect to OrderConfirmation page
			response.redirect(URLUtils.https('Fulfillment-OrderConfirmation', 'orderId', orderId, 'eventId', eventId));
		} else if(orderStep == 2 && (orderObjShippingMethod == 'storePickup' || orderObjShippingMethod == 'dropship') && !params.local.booleanValue) { 
			// Order is already scheduled (Dropship and Warehouse orders cannot be rescheduled)
			response.redirect(URLUtils.https('Fulfillment-Billing', 'orderId', orderId, 'eventId', eventId));
		}
	}
	
	var dt = getOrderDeliveryType(orderObj.order);
	orderObj['deliveryType'] = (dt=="Core")?"Local":dt;
	
	var staticSlots = dw.system.Site.getCurrent().getCustomPreferenceValue('staticATPSlots');
	var deliveryDatesObj = ATPService.getATPDeliveryDatesForNextWeek(orderObj.order, staticSlots); // 2nd param true to get static data

	var order = orderObj.order;
	 orderObj['localDeliveryProductsQuantity'] = 0;
     orderObj['localDeliveryProductsItemsQuantity'] = 0;
     orderObj['dropShipDeliveryProductsQuantity'] = 0;
     orderObj['dropShipDeliveryProductsItemsQuantity'] = 0;
	 for each(var pli in order.productLineItems) {
		 if (pli.product.custom.shippingInformation.toLowerCase() == 'core') {
			 orderObj['localDeliveryProductsQuantity']++;
			 orderObj['localDeliveryProductsItemsQuantity'] += pli.quantityValue;
		 } else {
			 orderObj['dropShipDeliveryProductsQuantity']++;
		     orderObj['dropShipDeliveryProductsItemsQuantity'] += pli.quantityValue;
		 }
	 }
	 
	// Get shipment of Order
	var orderShippingMethodID = app.getController('SearchOrders').GetShippingMethod(orderObj.order);	// Pick the nationwide shipment instead of defaultShipment
	for each (var shipment in orderObj.order.shipments) {
		if (!empty(shipment.shippingMethodID) && shipment.productLineItems.length > 0 && shipment.shippingMethodID.equals(orderShippingMethodID)) {
			orderObj['shipment'] = shipment;
			//break;
		}
	}
	 
	if(params.local.stringValue == "true"){
		app.getView({
			orderSummary : {
				'eventObj'			:eventObj,
				'orderObj'			:orderObj,
				'orderStep'			:getOrderStep(orderObj['order'], true),
				'deliveryDatesArr'	:deliveryDatesObj.deliveryDatesArr,
				'deliveryDates'		:deliveryDatesObj.deliveryDates,
				'isLocal'			:params.local.stringValue
				}
		}).render('fulfillment/order/reschedulinglocal');
	}else {
		app.getView({
			orderSummary : {
				'eventObj'			:eventObj,
				'orderObj'			:orderObj,
				'orderStep'			:getOrderStep(orderObj['order'], true),
				'deliveryDatesArr'	:deliveryDatesObj.deliveryDatesArr,
				'deliveryDates'		:deliveryDatesObj.deliveryDates,
				'rescheduled'		:(params.rescheduled.stringValue)?params.rescheduled.stringValue:false,
				'reschedulefailed'	:(params.reschedulefailed.stringValue)?params.reschedulefailed.stringValue:false
				}
		}).render('fulfillment/order/rescheduling');
	}
}

/*
 * Exported order cannot be rescheduled to some date in past of it's already scheduled delivery date.
 * If a user tries to reschedule an exported order to previous date of it's delivery date then he is taken directly to 
 * Order Confirmation page and show the order's delivery date.
 * */
function isTheRescheduleDateValidForThisOrder(orderObj, scheduleDate) {
	// Get shipment of Order
	var orderShippingMethodID = app.getController('SearchOrders').GetShippingMethod(orderObj);	// Pick the nationwide shipment instead of defaultShipment
	var orderShipment;
	for each (var shipment in orderObj.shipments) {
		if (!empty(shipment.shippingMethodID) && shipment.productLineItems.length > 0 && shipment.shippingMethodID.equals(orderShippingMethodID)) {
			orderShipment = shipment;
			break;
		}
	}
	
	// If the order is already exported to AX
	if(orderObj.getConfirmationStatus() == dw.order.Order.CONFIRMATION_STATUS_CONFIRMED && 
		orderObj.getExportStatus() == dw.order.Order.EXPORT_STATUS_EXPORTED) {
		var orderDeliveryDate = new Date(orderShipment.custom.deliveryDate); // Get order delivery date from shipment
		orderDeliveryDate.setHours(0,0,0,0);
		var newDeliveryDate = new Date(scheduleDate);
		newDeliveryDate.setHours(0,0,0,0);
		if (newDeliveryDate <= orderDeliveryDate) { // Order cannot be rescheduled to a previous date then it's already scheduled delivery date
			return false;
		}
	}
	return true;
}

function updateOrderSchedule() {
	var args = new Object();
	var orderId = params.orderId.stringValue;
	var eventId = params.eventId.stringValue;
	var eventObj = getEvent(eventId);
	if(empty(eventObj)) {
		response.redirect(URLUtils.https('Fulfillment-Dashboard', 'eventDeleted', true));
		return;
	}
	var	orderObj = getOrder(orderId, eventId);
	if(empty(orderObj)) {
		response.redirect(URLUtils.https('Fulfillment-Dashboard', 'eventDeleted', true));
		return;
	}
	var shipments = orderObj.shipments;
	var orderUpdated = false; // flag
	
	if(!isTheRescheduleDateValidForThisOrder(orderObj, params.deliveryDate.stringValue)) { // Not eligible to be scheduled for this date
		response.getWriter().println(orderObj.getExportStatus().value);	// Redirect to Order Confirmation page
		return;
	}

	Transaction.begin();
	for each(var shipment in shipments) {
		try {
				var sdt = getShipmentDeliveryType(shipment);
				if (sdt == 'Drop Ship') {
					shipment.custom.deliveryDate = mattressPipeletHelper.getISODate('31 Dec 2049, Sun');
					shipment.custom.deliveryTime = '08:30am-09:00pm';
					
					session.custom.deliveryDate = mattressPipeletHelper.getISODate('31 Dec 2049, Sun');;
					session.custom.deliveryTime = '08:30am-09:00pm';
				} else {
					var fulfillmentDeliveryDate = mattressPipeletHelper.getISODate(params.deliveryDate.stringValue);
					var fulfillmentDeliveryTime = params.deliveryTime.stringValue;
				
					shipment.custom.deliveryDate = fulfillmentDeliveryDate;
					shipment.custom.deliveryTime = fulfillmentDeliveryTime;

					session.custom.deliveryDate = fulfillmentDeliveryDate;
					session.custom.deliveryTime = fulfillmentDeliveryTime;

					var nonISODeliveryDate = fulfillmentDeliveryDate.replace('-','/', 'g').split('T');
				}
				var pliIterator = shipment.getProductLineItems().iterator();
            	while (pliIterator.hasNext()) {

            		var pli : Product = pliIterator.next();
                	pli.custom.deliveryDate = shipment.custom.deliveryDate;

                	if (shipment.custom.deliveryDate && dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule')) {
                		
                		if (sdt == 'Drop Ship') {
                			pli.custom.InventLocationId = dw.system.Site.getCurrent().getCustomPreferenceValue('InventLocationId');
                		} else {
                			pli.custom.InventLocationId = dw.system.Site.getCurrent().getCustomPreferenceValue('InventLocationId');
                			pli.custom.mfiOldDSZoneLineId = pli.custom.mfiDSZoneLineId
                			pli.custom.InventLocationId = params.location1.stringValue;
            				pli.custom.secondaryWareHouse = params.location2.stringValue;
            				pli.custom.mfiDSZoneLineId = params.mfiDSZoneLineId.stringValue;
            				pli.custom.mfiDSZipCode = params.mfiDSZipCode.stringValue;
            				session.custom.deliveryZone = params.mfiDSZoneLineId.stringValue;
                		}
                		
            			var optionLineItems = pli.optionProductLineItems;

            			for (var j = 0; j < optionLineItems.length; j++) {

            				var oli = optionLineItems[j];

            				if (oli.productID != 'none') {
            					oli.custom.InventLocationId = params.location1.stringValue;
            				}
            			}
                	}
            	}
            	orderUpdated = true;
		} catch(e) {
			// Failed to add Delivery Dates
			Logger.error("Error while adding devlivery Dates: "+ e.message);
		}
	}

	if (orderObj.getExportStatus().value == dw.order.Order.EXPORT_STATUS_EXPORTED ) {
		args['order'] = orderObj;
		var orderJSON = OrderFactory.buildCreateOrderRequestObject(args);
		var updateOrderDeliverySchedule = ATPService.updateDelivery(orderJSON);
		if (updateOrderDeliverySchedule) {
			Transaction.commit();
			response.getWriter().println(orderObj.getExportStatus().value);	// Success
			return;
		} else {
			Transaction.rollback();
			response.getWriter().println('failed'); // Failed
			return;
		}
	}
	if (orderUpdated) { // In case of local reschedule
		response.getWriter().println(orderObj.getExportStatus().value);	// Success
		Transaction.commit();
		return;
	} else {
		response.getWriter().println('failed'); // Failed
		Transaction.rollback();
		return;
	}
}

function SendDealerOrderConfirmationEmail() {
	var EmailModel = app.getModel('Email'),
		email = customer.profile.email;	// Email of Admin/Dealer, e.g. dealer@example.com.
	var submittedOrders = getSubmittedOrders();

	var emailObj =  EmailModel.get('fulfillment/order/receipt', email),
		emailSubject = Resource.msg('dealerorderconf.orderconfirmation', 'email', null);

	emailObj.setSubject(emailSubject);
	emailObj.send({
		ReceiptOrdersArr			: submittedOrders.receiptOrdersArr,
		OrderTotalConfirmedItems	: submittedOrders.OrderTotalConfirmedItems
	});
}

function SendCustomerOrderConfirmationEmail() {
    var EmailModel = app.getModel('Email');
    var email = ''; // Email of customer, e.g. customer@example.com.

		// Customer Order Confirmation
		var customerOrderConfEmail = function() {
			var emailObj =  EmailModel.get('mail/customerorderconfirmation', email),
					emailSubject = Resource.msg('order.orderconfirmation-email.001', 'order', null);

			emailObj.setSubject(emailSubject);
			emailObj.send({
				CreationDate: '08/24/2018',
				OrderNumber: '00012001',
				CustomerName: 'Joe Madison',
				EventName: 'Liberty High School Marching Band',
				EventDate: '07/29/2018',
				CustomerPhone: '424-652-1234',
				CustomerEmail: 'jmadison@aol.com',
				DeliveryAddress1: '158 Market St',
				DeliveryAddress2: 'Apt2',
				DeliveryCity: 'Barstow',
				DeliveryState: 'CA',
				DeliveryPostalCode: '94901',
				DeliveryDate: 'Saturday, September 14, 2018',
				DeliveryTime: '11:00am - 3:00pm',
				Product1Name: 'Amazon Only Otis Bed Haley 90 Futon Queen Matt',
				Product1DeliveryType: 'Local Delivery',
				Product1Qty: 1,
				Product1Price: '$499.50',
				Product2Name: 'Luxury Memory Foam 2 Pack Pillow',
				Product2DeliveryType: 'Drop Ship',
				Product2Qty: 1,
				Product2Price: '$49.50',
				Product3Name: 'Sleepy\'s Foundation Box Spring',
				Product3DeliveryType: 'Warehouse pick up',
				Product3Qty: 1,
				Product3Price: '$99.50',
				IsLocalDelivery: 1,
				LocalDeliveryQty: 1,
				IsDropShip: 1,
				DropShipQty: 1,
				IsWarehousePickup: 1,
				WarehousePickupQty: 1,
				WarehouseAvailableDate: 'Sept. 17 2018 9:00am',
				WarehouseName: 'MattressFirm Warehouse Victorville',
				WarehouseAddress1: '12544 Amargosa Rd',
				WarehouseCity: 'Victorville',
				WarehouseState: 'CA',
				WarehousePostalCode: '92392'
			});
		}

		function init() {
			customerOrderConfEmail();
		};

		init();
}

/* Returns the Submitted Orders and their total.
 * This function will be used for sending email to Admin/Dealer and to generate Receipt
*/
function getSubmittedOrders() {
	var par = params;
	var ordersSuccessfullyPlacedIDs = [];
	if(!params.orderIds.empty) {
		ordersSuccessfullyPlacedIDs = params.orderIds.stringValue.split(",");
		session.custom.ordersSuccessfullyPlaced = params.orderIds.stringValue;
	} else if (!empty(session.custom.ordersSuccessfullyPlaced)) {
		ordersSuccessfullyPlacedIDs = session.custom.ordersSuccessfullyPlaced.split(",");	
	}
	
	if (empty(ordersSuccessfullyPlacedIDs)) {
		app.getController('Fulfillment').Dashboard("You don't have any orders for downloading");
		return;
	}
	var dwOrdersArr = [];
	for (var i=0; i<ordersSuccessfullyPlacedIDs.length; i++) {
		if (!empty(ordersSuccessfullyPlacedIDs[i])) {
			var order = OrderMgr.getOrder(ordersSuccessfullyPlacedIDs[i]);
			if (!empty(order)) {
				var eventId = order.custom.eventId;
				if (!empty(eventId)) {
					var eventObj = getEvent(eventId);
					if (empty(eventObj)) {
						response.redirect(URLUtils.https('Fulfillment-Dashboard', 'eventDeleted', true));
						return;
					}
				}
				dwOrdersArr.push(order);
			}
		}
	}
	var receiptOrdersArr = [];
	var OrderTotalConfirmedItems : Number = 0;
	for (var i=0; i<dwOrdersArr.length; i++) {
		var dwOrder = dwOrdersArr[i];
		var event = CustomObjectMgr.getCustomObject('fulfillment_events', dwOrder.custom.eventId);
		if (empty(event)) {
			response.redirect(URLUtils.https('Fulfillment-Dashboard', 'eventDeleted', true));
			return;
		}
		if (empty(dwOrder)) {
			response.redirect(URLUtils.https('Fulfillment-Dashboard', 'eventDeleted', true));
			return;
		}
		var deliveryType = getOrderDeliveryType(dwOrder);
	    if (deliveryType.toLowerCase() == 'core') {
	    	deliveryType = 'Local';
	    }
		var shippingMethod = '';
        for each (var shipment in dwOrder.shipments) {
    		if (!empty(shipment.shippingMethodID) && shipment.productLineItems.length > 0) {
    			shippingMethod = shipment.shippingMethodID;
    		}
    	}
		/*if (shippingMethod == 'storePickup') {
	    	deliveryType = 'Warehouse Pickup';
	    }*/
		var deliveryDetails = '';	// for Delivery: Date & Time. for Warehouse pickup: Date
		var deliveryDate, deliveryTime;
		for each (var shipment in dwOrder.shipments) {
			/*if (deliveryType.toLowerCase() != 'drop ship') {
				if (!empty(shipment.custom.deliveryDate)) {
					deliveryDetails = new Date(shipment.custom.deliveryDate).toLocaleDateString();
					if (shippingMethod != 'storePickup' && deliveryType != 'Mixed') {
						deliveryDetails += ', ' + shipment.custom.deliveryTime.toString();
						break;
					}
				}
			}*/
			if (!empty(shipment.custom.deliveryDate)) {
				deliveryDate = new Date(shipment.custom.deliveryDate).toLocaleDateString();
			}
			if (!empty(shipment.custom.deliveryTime)) {
				deliveryTime = shipment.custom.deliveryTime.toString();
			}
				
			if (deliveryType == 'Warehouse Pickup') {
				if (!empty(shipment.custom.deliveryDate)) {
					deliveryDetails = new Date(shipment.custom.deliveryDate).toLocaleDateString();
					break;
				}
			} else {
				if (!empty(shipment.custom.deliveryDate) && !empty(shipment.custom.deliveryTime)) {
					deliveryDetails = new Date(shipment.custom.deliveryDate).toLocaleDateString();
					if (shipment.custom.deliveryTime.toString() != '12:01am-12:01am') {
						deliveryDetails += ', ' + shipment.custom.deliveryTime.toString();
					}
					break;
				}
			}
		}
		
		if (deliveryType.toLowerCase() == 'mixed') {
			for each (var dShipment in dwOrder.shipments) {
				var pliIterator = dShipment.getProductLineItems().iterator();
        		while (pliIterator.hasNext()) {
        			var pli : Product = pliIterator.next();
        			if(pli.product.custom.shippingInformation == 'Core') {
        				deliveryDetails = new Date(dShipment.custom.deliveryDate).toLocaleDateString() + ', ' + dShipment.custom.deliveryTime.toString();
        				deliveryDate = new Date(dShipment.custom.deliveryDate).toLocaleDateString();
        				deliveryTime = dShipment.custom.deliveryTime.toString();
        				break;
        			}
        		}
			}
	    }
		
		var shippingAddress = dwOrder.getDefaultShipment().getShippingAddress();
		var customerName = !empty(shippingAddress) ? shippingAddress.getFullName() : '';
		var orderObj = {
				Number			: dwOrder.orderNo,	// Demandware Order No.
				EventName		: event.custom.eventName,
				SubmittedDate	: new Date(),		// Submitted Date (today's date when the orders are submitted)
				DeliveryDate	: deliveryDate,
				DeliveryTime	: deliveryTime,
				ShippingMethod	: shippingMethod,
				DeliveryDetails	: deliveryDetails,	// Delivery Date & Time
				DeliveryType	: deliveryType,		//
				CustomerName	: customerName,		// End Customer Name
				Qty				: dwOrder.getProductQuantityTotal(),
				Total			: dwOrder.getTotalGrossPrice().getValue()
		}
		OrderTotalConfirmedItems += dwOrder.getTotalGrossPrice().getValue();
		receiptOrdersArr.push(orderObj);
	}
	return {
		receiptOrdersArr			: receiptOrdersArr,
		OrderTotalConfirmedItems	: OrderTotalConfirmedItems
	}
}

function printReceipt() {

	var submittedOrders = getSubmittedOrders();
	if (empty(submittedOrders)) {
		return;
	}
	app.getView({
		displayPrinterIcon			: true,
		ReceiptOrdersArr			: submittedOrders.receiptOrdersArr,
		OrderTotalConfirmedItems	: submittedOrders.OrderTotalConfirmedItems
	}).render('fulfillment/order/receipt');
}

/* Web exposed methods */

exports.HandleCreateEvent = guard.ensure(['post', 'https', 'loggedIn'], handleCreateEvent);
exports.CreateEvent = guard.ensure(['get', 'https', 'loggedIn'], createEvent, params);
exports.Dashboard = guard.ensure(['https', 'loggedIn'], dashboard);
exports.EditEvent = guard.ensure(['get', 'https', 'loggedIn'], editEvent, params);
exports.HandleEditEvent = guard.ensure(['post', 'https', 'loggedIn'], handleEditEvent);
exports.OrderSummary = guard.ensure(['get', 'https', 'loggedIn'], eventOrderSummary);
exports.ScheduleDelivery = guard.ensure(['post','https','loggedIn'], scheduleDelivery);
exports.GetEventsForThisPage = guard.ensure(['post', 'https', 'loggedIn'], getEventsForThisPage, params);
exports.GetEventOrderSummary = guard.ensure(['get', 'https', 'loggedIn'], getEventOrderSummary);
exports.RescheduledOrdersOnLogin = guard.ensure(['https','loggedIn'], rescheduledOrdersOnLogin);
exports.EditCustomerInfo = guard.ensure(['get', 'https', 'loggedIn'], editCustomerInfo, params);
exports.HandleEditCustomerInfo = guard.ensure(['post', 'https', 'loggedIn'], handleEditCustomerInfo);
exports.Billing = guard.ensure(['https','loggedIn'], billing);
exports.HandleUsaepaypayment = guard.ensure(['post', 'https', 'loggedIn'], handleUsaepaypayment);
exports.OrderConfirmation = guard.ensure(['get', 'https', 'loggedIn'], orderConfirmation);
exports.RescheduleOrder = guard.ensure(['get', 'https', 'loggedIn'],rescheduleOrder);
exports.UpdateOrderSchedule = guard.ensure(['post','https','loggedIn'],updateOrderSchedule);
exports.SubmitCSFOrder = guard.ensure(['https', 'loggedIn'], submitCSFOrder);
exports.SendCustomerOrderConfirmationEmail = guard.ensure(['https'], SendCustomerOrderConfirmationEmail);
exports.SendDealerOrderConfirmationEmail = guard.ensure(['https'], SendDealerOrderConfirmationEmail);
exports.PrintReceipt = guard.ensure(['https', 'loggedIn'], printReceipt);

/* Private Methods to access in code */
exports.GetShippingMethod = getShippingMethod;
exports.GetOrderDeliveryType = getOrderDeliveryType;
exports.GetShipmentDeliveryType = getShipmentDeliveryType;
exports.GetServicePrice = getServicePrice;
exports.CalculateShippingCosts = calculateShippingCosts;
exports.CalculateTax = calculateTax;
exports.CanBeReschedule = canBeReschedule;