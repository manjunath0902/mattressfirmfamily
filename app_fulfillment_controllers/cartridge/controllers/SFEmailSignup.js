'use strict';

/**
 * Controller 
 *
 * @module controllers/SFEmailSignup
 */

/* API includes */
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var StringUtils = require('dw/util/StringUtils');
var Transaction = require('dw/system/Transaction');

/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var emailHelper = require("~/cartridge/scripts/util/SFEmailSubscriptionHelper").SFEmailSubscriptionHelper; 

function showForm() 
{
	app.getView('EmailSignup').render('emailsubscriptions/SFEmailModalForm');
}

// Recieves the AJAX submission of email address and responds
function submitEmail() {
	var params = request.httpParameterMap;
	var emailAddress : String = params.emailAddress.stringValue;
	var leadSource : String = params.leadSource.stringValue;
	var zipCode : String = params.zipCode.stringValue;
	var siteId : String = params.siteId.stringValue;
	var optOutFlag = params.optOutFlag.booleanValue;
	var gclid = params.gclid.stringValue;

	//save e-mail address using lower case
	emailAddress = emailAddress.toLowerCase();
	// REGEX to check email
	if (!/^[a-zA-Z0-9]+(?:(\.|_)[A-Za-z0-9!#$%&'*+/=?^`{|}~-]+)*@(?!([a-zA-Z0-9]*\.[a-zA-Z0-9]*\.[a-zA-Z0-9]*\.))(?:[A-Za-z0-9](?:[a-zA-Z0-9-]*[A-Za-z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/.test(emailAddress)) {
	// email falied validation
	}
	var emailParams = {
			emailAddress: emailAddress, 
			zipCode: zipCode, 
			leadSource: leadSource, 
			siteId: siteId, 
			optOutFlag: optOutFlag,
			gclid: gclid,
			dwsid: session.sessionID
			};
	
	var returnResult = emailHelper.sendSFEmailInfo(emailParams);
	
	if (leadSource == 'modal') {
		var responseTemplate = 'SFEmailResponseModal';
	} else {
		var responseTemplate = 'SFEmailResponseFooter';
	}
	
	if (leadSource == 'mattress_finder') {
		if (returnResult.Status == 'SERVICE_ERROR'){
			emailHelper.sendFailSafe(emailParams, returnResult.ErrorCode);
		}
		app.getView({
			JSONResponse: returnResult
		}).render('util/responsejson');
	} else {
		if (returnResult.Status == 'SERVICE_ERROR'){
			var returnResult = emailHelper.sendFailSafe(emailParams, returnResult.ErrorCode);
			app.getView('EmailSignup', {SFEmailStatus : returnResult.Status, SFEmailMsg : '', SFEmailErrorCode: ''}).render('emailsubscriptions/' + responseTemplate);
		} else {
			app.getView('EmailSignup', {SFEmailStatus : returnResult.Status, SFEmailMsg : returnResult.Msg, SFEmailErrorCode: returnResult.ErrorCode}).render('emailsubscriptions/' + responseTemplate);
		}
	}
	
}

function processFailSafe() {
	var returnResult = emailHelper.processFailSafe();
	return returnResult;
}

exports.Submit = guard.ensure(['post'], submitEmail);
exports.ProcessFailSafe = processFailSafe;
exports.Form = guard.ensure(['get'], showForm);
