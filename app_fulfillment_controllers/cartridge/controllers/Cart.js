'use strict';

/**
 * Controller that adds and removes products and coupons in the cart.
 * Also provides functions for the continue shopping button and minicart.
 *
 * @module controllers/Cart
 */

/* API Includes */
var ArrayList = require('dw/util/ArrayList');
var ISML = require('dw/template/ISML');
var ProductListMgr = require('dw/customer/ProductListMgr');
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');
var Logger = require('dw/system/Logger');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var pipeletHelper = require('bc_sleepysc/cartridge/scripts/sleepys/util/SleepysPipeletsHelper').SleepysPipeletHelper;
var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;

/**
 * Redirects the user to the last visited catalog URL if known, otherwise redirects to
 * a hostname-only URL if an alias is set, or to the Home-Show controller function in the default
 * format using the HTTP protocol.
 */
function continueShopping() {

    var location = require('~/cartridge/scripts/util/Browsing').lastCatalogURL();

    if (location) {
        response.redirect(location);
    } else {
        response.redirect(URLUtils.httpHome());
    }
}

/**
 * Invalidates the login and shipment forms. Renders the checkout/cart/cart template.
 */
function show() {
    var cartForm = app.getForm('cart');
    app.getForm('login').invalidate();

    cartForm.get('shipments').invalidate();
    var cart = app.getModel('Cart').get();
    var cartAsset = app.getModel('Content').get('cart');

    pageMeta = require('~/cartridge/scripts/meta');
    pageMeta.update(cartAsset);
    
    // calculate shipping and tax on cart page
    if (dw.system.Site.getCurrent().getID() == 'Mattress-Firm') {
	    var geolocationZip = request.getGeolocation().getPostalCode();  
	    var postalCode = empty(session.custom.customerZip) ? geolocationZip : session.custom.customerZip;
	    if(!empty(postalCode)){
			session.custom.customerZip = postalCode;  
			var COShippingMethodController = require('app_mattressfirm_storefront/cartridge/controllers/COShippingMethod');
			COShippingMethodController.CalculateShippingAndTaxForZip(postalCode);
	    }
    }
    
    if (cart) {
        Transaction.wrap(function () {
            pipeletHelper.clearAllShipmentSchedules(cart);
            if (session.custom.deliveryOptionChoice == 'instorepickup') {
            	mattressPipeletHelper.splitShipmentsByType(cart);
            }
            cart.calculate();
        });
    }
    app.getView('Cart', {
        cart: app.getModel('Cart').get(),
        RegistrationStatus: false
    }).render('checkout/cart/cart');

}

/**
 * Handles the form actions for the cart.
 * - __addCoupon(formgroup)__ - adds a coupon to the basket in a transaction. Returns a JSON object with parameters for the template.
 * - __calculateTotal__ - returns the cart object.
 * - __checkoutCart__ - validates the cart for checkout. If valid, redirect to the COCustomer-Start controller function to start the checkout. If invalid returns the cart and the results of the validation.
 * - __continueShopping__ - calls the {@link module:controllers/Cart~continueShopping|continueShopping} function and returns null.
 * - __deleteCoupon(formgroup)__ - removes a coupon from the basket in a transaction. Returns a JSON object with parameters for the template
 * - __deleteGiftCertificate(formgroup)__ - removes a gift certificate from the basket in a transaction. Returns a JSON object with parameters for the template.
 * - __deleteProduct(formgroup)__ -  removes a product from the basket in a transaction. Returns a JSON object with parameters for the template.
 * - __editLineItem(formgroup)__ - gets a ProductModel that wraps the pid (product ID) in the httpParameterMap and updates the options to select for the product. Updates the product in a transaction.
 * Renders the checkout/cart/refreshcart template. Returns null.
 * - __login__ - calls the Login controller and returns a JSON object with parameters for the template.
 * - __logout__ - logs the customer out and returns a JSON object with parameters for the template.
 * - __register__ - calls the Account controller StartRegister function. Updates the cart calculation in a transaction and returns null.
 * - __unregistered__ - calls the COShipping controller Start function and returns null.
 * - __updateCart__ - In a transaction, removes zero quantity line items, removes line items for in-store pickup, and copies data to system objects based on the form bindings.
 * Returns a JSON object with parameters for the template.
 * - __error__ - returns null.
 *
 * __Note:__ The CartView sets the ContinueURL to this function, so that any time URLUtils.continueURL() is used in the cart.isml, this function is called.
 * Several actions have <b>formgroup</b> as an input parameter. The formgroup is supplied by the {@link module:models/FormModel~FormModel/handleAction|FormModel handleAction} function in the FormModel module.
 * The formgroup is session.forms.cart object of the triggered action in the form definition. Any object returned by the function for an action is passed in the parameters to the cart template
 * and is accessible using the $pdict.property syntax. For example, if a function returns {CouponStatus: status} is accessible via ${pdict.CouponStatus}
 * Most member functions return a JSON object that contains {cart: cart}. The cart property is used by the CartView to determine the value of
 * $pdict.Basket in the cart.isml template.
 *
 * For any member function that returns an object, the page metadata is updated, the function gets a ContentModel that wraps the cart content asset,
 * and the checkout/cart/cart template is rendered.
 *
 */
function submitForm() {
    // There is no existing state, so resolve the basket again.
    var cart, formResult, cartForm, cartAsset, pageMeta;
    cartForm = app.getForm('cart');
    cart = app.getModel('Cart').goc();

    formResult = cartForm.handleAction({
        //Add a coupon if a coupon was entered correctly and is active.
        'addCoupon': function (formgroup) {
            var status, result, couponType;
            var basket = cart.object;
            if (formgroup.couponCode.htmlValue) {
            	
            	formgroup.couponCode.htmlValue = formgroup.couponCode.htmlValue.toUpperCase();
                status = Transaction.wrap(function () {
                    return cart.addCoupon(formgroup.couponCode.htmlValue);
                });

                if (status) {
                	Transaction.wrap(function () {
                		cart.removePersonaliCartRescue();
                		cart.removePersonaliFromLineItems();
                	});
                	var couponApplied = false;
                	for each(var couponLineItem in basket.couponLineItems) {
                    	if (couponLineItem.couponCode.toUpperCase() == formgroup.couponCode.htmlValue.toUpperCase()) {
                    		couponApplied = couponLineItem.applied;
                    		if(couponApplied) {
                			  var priceAdjustment1 = couponLineItem.getPriceAdjustments().iterator();
                			  while (priceAdjustment1.hasNext()) {
                			   var priceAdjustment2 = priceAdjustment1.next();
                			   couponType = priceAdjustment2.appliedDiscount.type;
                      		  }
                    		}	
                    	}
                    }
                	if (couponApplied) {
	                    result = {
	                        cart: cart,
	                        CouponStatus: status.CouponStatus,
	                        CouponApplied: couponApplied,
	                        CouponType: couponType
	                    };
                	} else {
                		result = {
    	                        cart: cart,
    	                        CouponStatus: status.CouponStatus,
    	                        CouponApplied: 'none',
    	                        CouponType: 'none'
    	                    };
                	}
                } else {
                    result = {
                        cart: cart,
                        CouponError: 'NO_ACTIVE_PROMOTION',
                        CouponApplied: 'none',
                        CouponType: 'none'
                    };
                }
            } else {
                result = {
                    cart: cart,
                    CouponError: 'COUPON_CODE_MISSING',
                    CouponApplied: 'none',
                    CouponType: 'none'
                };
            }
            var showMattressRemovalOption = pipeletHelper.showMatressRemovalOptionCheck(cart);
            result.RegistrationStatus = false;
            result.ShowMattressRemovalOption = showMattressRemovalOption;

            app.getView('Cart', result).render('checkout/cart/cart');
            return;

        },
        'calculateTotal': function () {
            // Nothing to do here as re-calculation happens during view anyways
            return {
                cart: cart
            };
        },
        'checkoutCart': function () {
        	var mFinderEmptyCartLogger = Logger.getLogger('MattressFinder','Cart');
            mFinderEmptyCartLogger.info("Cart Checkout Cart Starts.");
            var validationResult, result;

            validationResult = cart.validateForCheckout();
            mFinderEmptyCartLogger.info("Cart Validation Result : " + validationResult.EnableCheckout + " .");
            mFinderEmptyCartLogger.info("Basket Status : " + validationResult.BasketStatus.message + " .");
            if (validationResult.EnableCheckout) {
                            	
            	if (dw.system.Site.getCurrent().getID() == 'Mattress-Firm') {
            	response.redirect(URLUtils.https('COShippingMethod-Start'));
            	}
            	else
        		{
        		mFinderEmptyCartLogger.info("Jump to COCustomer-Start.");
        		response.redirect(URLUtils.https('COCustomer-Start'));
        		}

            } else {
            	mFinderEmptyCartLogger.info("Invalid Jump from cart.");
                result = {
                    cart: cart,
                    BasketStatus: validationResult.BasketStatus,
                    EnableCheckout: validationResult.EnableCheckout
                };
            }
            return result;
        },
        'continueShopping': function () {
            continueShopping();
            return null;
        },
        'deleteCoupon': function (formgroup) {
            Transaction.wrap(function () {
                cart.removeCouponLineItem(formgroup.getTriggeredAction().object);
            });

            return {
                cart: cart
            };
        },
        'deleteGiftCertificate': function (formgroup) {
            Transaction.wrap(function () {
                cart.removeGiftCertificateLineItem(formgroup.getTriggeredAction().object);
            });

            return {
                cart: cart
            };
        },
        'deleteProduct': function (formgroup) {
            Transaction.wrap(function () {
            	var triggeredProductLineItem = formgroup.getTriggeredAction().object;
            	if(triggeredProductLineItem.custom.isQuoteItem) {
            		//removing triggered line item
            		cart.removeProductLineItem(triggeredProductLineItem);
            		
            		//removing cart updates related to quotation
            		cart.object.custom.mfiQuotationId = null;
					cart.object.custom.SalesPersonId = null;
					cart.object.custom.CustomerId = null;
					
					//adjusting existing product line items
					var allLineItems = cart.object.allProductLineItems;
					for (var i=0; i<allLineItems.length; i++) {
						var lineItem = allLineItems[i];
						if( lineItem instanceof dw.order.ProductLineItem && lineItem.custom.isQuoteItem) {
							var createdPriceAdjustment = lineItem.getPriceAdjustmentByPromotionID(lineItem.productID + "-quote");
							lineItem.removePriceAdjustment(createdPriceAdjustment);
							lineItem.custom.isQuoteItem = null;
							lineItem.custom.quotePrice = null;
							lineItem.custom.quotediscount = null;
						}
					}
					cart.calculate();
            	}
            	else {
					cart.removePersonaliCartRescue();
            		cart.removeProductLineItem(formgroup.getTriggeredAction().object);
            	}
            });

            return {
                cart: cart
            };
        },
        'editLineItem': function (formgroup) {
            var product, productOptionModel;
            product = app.getModel('Product').get(request.httpParameterMap.pid.stringValue).object;
            productOptionModel = product.updateOptionSelection(request.httpParameterMap);

            Transaction.wrap(function () {
            	cart.removePersonaliCartRescue();
                cart.updateLineItem(formgroup.getTriggeredAction().object, product, request.httpParameterMap.Quantity.doubleValue, productOptionModel);
                cart.calculate();
            });

            ISML.renderTemplate('checkout/cart/refreshcart');
            return null;
        },
        'login': function () {
            // TODO should not be processed here at all
            var success, result;
            success = app.getController('Login').Process();

            if (success) {
                response.redirect(URLUtils.https('COCustomer-Start'));
            } else if (!success) {
                result = {
                    cart: cart
                };
            }
            return result;
        },
        'logout': function () {
            var CustomerMgr = require('dw/customer/CustomerMgr');
            CustomerMgr.logoutCustomer();
            return {
                cart: cart
            };
        },
        'register': function () {
            app.getController('Account').StartRegister();
            Transaction.wrap(function () {
                cart.calculate();
            });

            return null;
        },
        'unregistered': function () {
            app.getController('COShipping').Start();
            return null;
        },
        'updateCart': function () {

            Transaction.wrap(function () {
                var shipmentItem, item;

                // remove zero quantity line items
                for (var i = 0; i < session.forms.cart.shipments.childCount; i++) {
                    shipmentItem = session.forms.cart.shipments[i];

                    for (var j = 0; j < shipmentItem.items.childCount; j++) {
                        item = shipmentItem.items[j];

                        if (item.quantity.value === 0) {
                            cart.removeProductLineItem(item.object);
                        }
                    }
                }

                session.forms.cart.shipments.accept();
                cart.removePersonaliCartRescue();
                cart.updatePersonaliProducts();
                cart.checkInStoreProducts();
            });

            return {
                cart: cart
            };
        },
        'error': function () {
            return null;
        }
    });

    if (formResult) {
    	//Cart page title not working here so thats why moved to Cart-Show method
        /*cartAsset = app.getModel('Content').get('cart');

        pageMeta = require('~/cartridge/scripts/meta');
        pageMeta.update(cartAsset);*/

        response.redirect(URLUtils.https('Cart-Show'));
    }
}

/**
 * Adds or replaces a product in the cart, gift registry, or wishlist.
 * If the function is being called as a gift registry update, calls the
 * {@link module:controllers/GiftRegistry~replaceProductListItem|GiftRegistry controller ReplaceProductListItem function}.
 * The httpParameterMap source and cartAction parameters indicate how the function is called.
 * If the function is being called as a wishlist update, calls the
 * {@link module:controllers/Wishlist~replaceProductListItem|Wishlist controller ReplaceProductListItem function}.
 * If the product line item for the product to add has a:
 * - __uuid__ - gets a ProductModel that wraps the product and determines the product quantity and options.
 * In a transaction, calls the {@link module:models/CartModel~CartModel/updateLineItem|CartModel updateLineItem} function to replace the current product in the line
 * item with the new product.
 * - __plid__ - gets the product list and adds a product list item.
 * Otherwise, adds the product and checks if a new discount line item is triggered.
 * Renders the checkout/cart/refreshcart template if the httpParameterMap format parameter is set to ajax,
 * otherwise renders the checkout/cart/cart template.
 */
function addProduct() {
    var cart = app.getModel('Cart').goc();
    var params = request.httpParameterMap;
    var format = empty(params.format.stringValue) ? '' : params.format.stringValue.toLowerCase();
    var Product = app.getModel('Product');
    var productOptionModel;
    var product;
    var template = 'checkout/cart/minicart';
    var newBonusDiscountLineItem;
    var lastItemAdded = false;
    var lastItemAddedQty = false;

    if (params.source && params.source.stringValue === 'giftregistry' && params.cartAction && params.cartAction.stringValue === 'update') {
        app.getController('GiftRegistry').ReplaceProductListItem();
        return;
    }

    if (params.source && params.source.stringValue === 'wishlist' && params.cartAction && params.cartAction.stringValue === 'update') {
        app.getController('Wishlist').ReplaceProductListItem();
        return;
    }

    // Updates a product line item.
    if (params.uuid.stringValue) {
        var lineItem = cart.getProductLineItemByUUID(params.uuid.stringValue);
        if (lineItem) {
            var productModel = Product.get(request.httpParameterMap.pid.stringValue);
            product = productModel.object;
            var quantity = parseInt(params.Quantity.value);
            productOptionModel = productModel.updateOptionSelection(request.httpParameterMap);

            Transaction.wrap(function () {
                cart.updateLineItem(lineItem, product, quantity, productOptionModel);
            });
            
            if (params.netotiate_offer_id.stringValue && !empty(params.netotiate_offer_id.stringValue)) {
        		var personaliResponse = cart.getPersonaliProductResponse(params.netotiate_offer_id.stringValue);
        		if (personaliResponse !== false) {
        			var adjustedPrice : Number = personaliResponse["adjusted_price"] / 100,
	    				originalPrice : Number = personaliResponse["original_price"] / 100,
	    				offerId : String = params.netotiate_offer_id.stringValue;
	    			if (adjustedPrice && originalPrice && adjustedPrice < originalPrice) {
	    				Transaction.wrap(function () {
	    					cart.removePersonaliCartRescue();
	        				personaliPriceAdjustment(lastItemAdded, offerId, params.Quantity.doubleValue, adjustedPrice, originalPrice);
	    	            });
	    			}
        		}
        		
        	} else if (lineItem.custom['personaliOfferID'] || lineItem.custom['personaliDiscount']) {
        		Transaction.wrap(function () {
        			delete lineItem.custom['personaliOfferID'];
            		delete lineItem.custom['personaliDiscount'];
        		});
        	}

            // If this product is to be picked up in a store -- update store id
            if (!empty(params.storeId.stringValue)) {
                // Determine if products in cart are eligible for in store pickup
                var allProductsAvailableForInStorePickup = product.custom.availableForInStorePickup;
                if (allProductsAvailableForInStorePickup) {
                    var plisIterator = cart.getAllProductLineItems().iterator();
                    while (plisIterator.hasNext()) {
                        var pli = plisIterator.next();
                        if (!(pli.product.custom.availableForInStorePickup)) {
                            allProductsAvailableForInStorePickup = false;
                        }
                    }
                    // all products are eligible
                    if (allProductsAvailableForInStorePickup) {
                        Transaction.wrap(function () {
                            var plisIterator = cart.getAllProductLineItems().iterator();
                            while (plisIterator.hasNext()) {
                                var pli = plisIterator.next();
                                pli.custom.storePickupStoreID = params.storeId.stringValue;
                            }
                         });
                    }
                }
            }
            if (format === 'ajax') {
                template = 'checkout/cart/refreshcart';
            }
        } else {
            app.getView('Cart', {Basket: cart}).render('checkout/cart/cart');
        }
    } else if (params.plid.stringValue) {
        // Adds a product to a product list.
        var productList = ProductListMgr.getProductList(params.plid.stringValue);
        cart.addProductListItem(productList && productList.getItem(params.itemid.stringValue), params.Quantity.doubleValue, params.cgid.value);
    } else {
    	// This session variable is used to print the count of newly added item in cart
    	if(empty(session.custom.recentlyAddedItemCount)) {
    		session.custom.recentlyAddedItemCount = parseInt(0);
    	}
    	
        // Adds a product.
        product = Product.get(params.pid.stringValue);
        var previousBonusDiscountLineItems = cart.getBonusDiscountLineItems();

        if (product.object.isProductSet()) {
            var childPids = params.childPids.stringValue.split(',');
            var childQtys = params.childQtys.stringValue.split(',');
            var counter = 0;

            for (var i = 0; i < childPids.length; i++) {
                var childProduct = Product.get(childPids[i]);

                if (childProduct.object && !childProduct.isProductSet()) {
                    var childProductOptionModel = childProduct.updateOptionSelection(request.httpParameterMap);
                    cart.addProductItem(childProduct.object, parseInt(childQtys[counter], 10), params.cgid.value, childProductOptionModel);
                }
                counter++;
                session.custom.recentlyAddedItemCount = parseInt(session.custom.recentlyAddedItemCount) + 1;
            }
        } else {
        	var productId = product.object.ID;
        	var availabilityOptionId = params.optionValue.stringValue;
        	var customerZip = session.custom.customerZip;
        	var availabilityQty = params.Quantity.intValue; 
            
            //MAT-356            
            var lineItemsOfPid = cart.getProductLineItemByID(params.pid.stringValue);
            var productFoundInCart = false;
            if (lineItemsOfPid.length > 0) {
            	var productModel = Product.get(params.pid.stringValue);
            	var lineItemQuantity = params.Quantity.value;
            	var productToAdd = productModel.object;
	        	productOptionModel = productModel.updateOptionSelection(request.httpParameterMap);
	        	for each(var lineItem in lineItemsOfPid) {
        			var productOptions = productOptionModel.getOptions();
	        		if(productOptions.length >= 0){
	        			var lineItemOptions = lineItem.getOptionProductLineItems();
	        			// productOptions are of that product which is going into cart which is productToAdd
	        			//lineItemOptions, extracting all lineItem iterator's all option products into lineItemOptions
	        			productFoundInCart = compareProductOptionProducts_With_ProductLineItemOptionProducts (productOptions, lineItemOptions, productOptionModel)
		        		if (productFoundInCart) {
		        			lineItemQuantity = parseInt(params.Quantity.value) + lineItem.quantity;
		        			availabilityQty = lineItemQuantity;
		        			session.custom.recentlyAddedItemCount = session.custom.recentlyAddedItemCount + parseInt(params.Quantity.value); 
		        	    	
				            Transaction.wrap(function () {
				            cart.updateLineItem(lineItem, productToAdd, lineItemQuantity, productOptionModel);
				            });
				            break;
		        		}		        			
	        		}
	        	}
	        }
            
            if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpAvailabilityMessage') && !empty(session.custom.ATPAvailability) && !empty(session.custom.ATPAvailability[productId + availabilityOptionId + customerZip + availabilityQty])) {
            	if (!session.custom.ATPCartAvailability) {
            		session.custom.ATPCartAvailability = new Object(); 
            	} 
            	session.custom.ATPCartAvailability[productId + availabilityOptionId + customerZip + availabilityQty] = new Object();
            	session.custom.ATPCartAvailability[productId + availabilityOptionId + customerZip + availabilityQty]['availabilityDate'] = session.custom.ATPAvailability[productId + availabilityOptionId + customerZip + availabilityQty]['availabilityDate'];
            	session.custom.ATPCartAvailability[productId + availabilityOptionId + customerZip + availabilityQty]['dateDifferenceString'] = session.custom.ATPAvailability[productId + availabilityOptionId + customerZip + availabilityQty]['dateDifferenceString'];
            	session.custom.ATPCartAvailability[productId + availabilityOptionId + customerZip + availabilityQty]['availabilityClass'] = session.custom.ATPAvailability[productId + availabilityOptionId + customerZip + availabilityQty]['availabilityClass'];
            	session.custom.ATPCartAvailability[productId + availabilityOptionId + customerZip + availabilityQty]['qty'] = session.custom.ATPAvailability[productId + availabilityOptionId + customerZip + availabilityQty]['qty'];
    		}
            
            if (!productFoundInCart){
	        	productOptionModel = product.updateOptionSelection(request.httpParameterMap);
	            cart.addProductItem(product.object, params.Quantity.doubleValue, params.cgid.value, productOptionModel);
    	    	session.custom.recentlyAddedItemCount = session.custom.recentlyAddedItemCount + parseInt(params.Quantity.value); 
    	    	
	        }
            if (params.source && params.source.stringValue !== 'recommendation') {
                var lastItemAddedCollection = cart.getProductLineItems(product.object.ID);
                if (availabilityOptionId) {
	                for each (var li in lastItemAddedCollection) {
	                	if (li.optionProductLineItems.size() > 0) {
		                	if (li.optionProductLineItems[0].optionValueID == availabilityOptionId) {
		                		lastItemAdded = li;
		                		break;
		                	} 
	                	}
	                }
                } 
                if (!lastItemAdded) {
	                lastItemAdded = lastItemAddedCollection[lastItemAddedCollection.size() -1];
                }
                lastItemAddedQty = params.Quantity.value;
                if (params.netotiate_offer_id.stringValue && !empty(params.netotiate_offer_id.stringValue)) {
            		var personaliResponse = cart.getPersonaliProductResponse(params.netotiate_offer_id.stringValue);
            		if (personaliResponse !== false) {
            			var adjustedPrice : Number = personaliResponse["adjusted_price"] / 100,
            				originalPrice : Number = personaliResponse["original_price"] / 100,
            				offerId : String = params.netotiate_offer_id.stringValue;
            			if (adjustedPrice && originalPrice && adjustedPrice < originalPrice) {
            				Transaction.wrap(function () {
    	        				personaliPriceAdjustment(lastItemAdded, offerId, params.Quantity.doubleValue, adjustedPrice, originalPrice);
    	    	            });
            			}
            		}
            		
            	}
            }
            // If this product is to be picked up in a store -- update store id
            if (!empty(params.storeId.stringValue)) {
                // Determine if products in cart are eligible for in store pickup
                var allProductsAvailableForInStorePickup = product.object.custom.availableForInStorePickup;
                var plisIterator = cart.getAllProductLineItems().iterator();
                while (plisIterator.hasNext()) {
                    var pli = plisIterator.next();
                    if (!(pli.product.custom.availableForInStorePickup)) {
                        allProductsAvailableForInStorePickup = false;
                    }
                }
                // all products are eligible
                if (allProductsAvailableForInStorePickup) {
                    Transaction.wrap(function () {
                        var plisIterator = cart.getAllProductLineItems().iterator();
                        while (plisIterator.hasNext()) {
                            var pli = plisIterator.next();
                            pli.custom.storePickupStoreID = params.storeId.stringValue;
                        }
                     });
                }
            }
            if (!empty(session.custom.customerZone)) {
//            	lastItemAdded.custom.zoneCodeAddedWith = session.custom.customerZone;
            }
        }

        // When adding a new product to the cart, check to see if it has triggered a new bonus discount line item.
        newBonusDiscountLineItem = cart.getNewBonusDiscountLineItem(previousBonusDiscountLineItems);
    }

    if (format === 'ajax') {
        app.getView('Cart', {
            cart: cart,
            BonusDiscountLineItem: newBonusDiscountLineItem,
            LastItem: lastItemAdded,
            LastItemAddedQty: lastItemAddedQty
        }).render(template);
    } else {
        response.redirect(URLUtils.url('Cart-Show'));
    }
}

function compareProductOptionProducts_With_ProductLineItemOptionProducts (productOptions, lineItemOptions, productOptionModel) {
	var optionProductFound = false;
	if (lineItemOptions.length == 0) {
		optionProductFound = true;
	} else {
		for each(var i = 0 ; i < productOptions.length ; i++) {
			optionProductFound = false;
			var selectedOption = productOptionModel.getSelectedOptionValue(productOptions[i]);
			for each(var j = 0 ; j < lineItemOptions.length ; j++) {
				if(lineItemOptions[j].optionValueID == selectedOption.ID) {
					optionProductFound = true;
					break;
				}
			}
			if (!optionProductFound){
				return optionProductFound;
			}
		}
	}
	return optionProductFound;
}

/**
 * Adds multiple products to cart via a JSON string supplied in the parameters.
 * parameter name should be cartPayload and JSON format is as follows:
 * [{"pid":"mfiV000088629","qty":1},{"pid":"mfiV000088630","qty":4},{"pid":"mfiV000088631","qty":5}]
 */
function addMultipleProducts() {
    var Product = app.getModel('Product');
    var cart = app.getModel('Cart').goc();
    var cartPayload = request.httpParameterMap.cartPayload;
    try {
        var jsonPayload = JSON.parse(cartPayload);
        for (var i = 0; i < jsonPayload.length; i++){
            var jsonProduct = jsonPayload[i];
            //var product = Product.get(params.pid.stringValue);
            try {
                var product = Product.get(jsonProduct.pid);
                var productOptionModel = product.updateOptionSelection(request.httpParameterMap);
                cart.addProductItem(product.object, jsonProduct.qty, (!empty(product.object.primaryCategory) ? product.object.primaryCategory.ID : null), productOptionModel);
            } catch (e) {
                Logger.error("Error processing Cart-AddMultipleProducts: Invalid product ID: " + jsonProduct.pid);
            }
        }
    } catch (e) {
        Logger.error("Error processing Cart-AddMultipleProducts: " + e.message);
        show();
        return;
    }
    show();

}
/**
 * Displays the current items in the cart in the minicart panel.
 */
function miniCart() {

    var cart = app.getModel('Cart').get();
    app.getView({
        Basket: cart ? cart.object : null
    }).render('checkout/cart/minicart');

}

/**
 * Adds the product with the given ID to the wish list.
 *
 * Gets a ProductModel that wraps the product in the httpParameterMap. Uses
 * {@link module:models/ProductModel~ProductModel/updateOptionSelection|ProductModel updateOptionSelection}
 * to get the product options selected for the product.
 * Gets a ProductListModel and adds the product to the product list. Renders the checkout/cart/cart template.
 */
function addToWishlist() {
    var productID, product, productOptionModel, productList, Product;
    Product = app.getModel('Product');

    productID = request.httpParameterMap.pid.stringValue;
    product = Product.get(productID);
    productOptionModel = product.updateOptionSelection(request.httpParameterMap);

    productList = app.getModel('ProductList').get();
    productList.addProduct(product.object, request.httpParameterMap.Quantity.doubleValue, productOptionModel);

    app.getView('Cart', {
        cart: app.getModel('Cart').get(),
        ProductAddedToWishlist: productID
    }).render('checkout/cart/cart');

}

/**
 * Adds a bonus product to the cart.
 *
 * Parses the httpParameterMap and adds the bonus products in it to an array.
 *
 * Gets the bonus discount line item. In a transaction, removes the bonus discount line item. For each bonus product in the array,
 * gets the product based on the product ID and adds the product as a bonus product to the cart.
 *
 * If the product is a bundle, updates the product option selections for each child product, finds the line item,
 * and replaces it with the current child product and selections.
 *
 * If the product and line item can be retrieved, recalculates the cart, commits the transaction, and renders a JSON object indicating success.
 * If the transaction fails, rolls back the transaction and renders a JSON object indicating failure.
 */
function addBonusProductJson() {
    var h, i, j, cart, data, productsJSON, bonusDiscountLineItem, product, lineItem, childPids, childProduct, ScriptResult, foundLineItem, Product;
    cart = app.getModel('Cart').goc();
    Product = app.getModel('Product');

    // parse bonus product JSON
    data = JSON.parse(request.httpParameterMap.getRequestBodyAsString());
    productsJSON = new ArrayList();

    for (h = 0; h < data.bonusproducts.length; h += 1) {
        // add bonus product at index zero (front of the array) each time
        productsJSON.addAt(0, data.bonusproducts[h].product);
    }

    bonusDiscountLineItem = cart.getBonusDiscountLineItemByUUID(request.httpParameterMap.bonusDiscountLineItemUUID.stringValue);

    Transaction.begin();
    cart.removeBonusDiscountLineItemProducts(bonusDiscountLineItem);

    for (i = 0; i < productsJSON.length; i += 1) {

        product = Product.get(productsJSON[i].pid).object;
        var quantity = Number(productsJSON[i].qty);
        lineItem = cart.addBonusProduct(bonusDiscountLineItem, product, new ArrayList(productsJSON[i].options), quantity);

        if (lineItem && product) {
            if (product.isBundle()) {

                childPids = productsJSON[i].childPids.split(',');

                for (j = 0; j < childPids.length; j += 1) {
                    childProduct = Product.get(childPids[j]).object;

                    if (childProduct) {

                        // TODO: CommonJSify cart/UpdateProductOptionSelections.ds and import here

                        var UpdateProductOptionSelections = require('app_storefront_core/cartridge/scripts/cart/UpdateProductOptionSelections');
                        ScriptResult = UpdateProductOptionSelections.update({
                            SelectedOptions:  new ArrayList(productsJSON[i].options),
                            Product: childProduct
                        });

                        foundLineItem = cart.getBundledProductLineItemByPID(lineItem.getBundledProductLineItems(),
                            (childProduct.isVariant() ? childProduct.masterProduct.ID : childProduct.ID));

                        if (foundLineItem) {
                            foundLineItem.replaceProduct(childProduct);
                        }
                    }
                }
            }
        } else {
            Transaction.rollback();

            let r = require('~/cartridge/scripts/util/Response');
            r.renderJSON({
                success: false
            });
            return;
        }
    }

    cart.calculate();
    Transaction.commit();

    let r = require('~/cartridge/scripts/util/Response');
    r.renderJSON({
        success: true
    });
}

/**
 * Adds a coupon to the cart using JSON.
 *
 * Gets the CartModel. Gets the coupon code from the httpParameterMap couponCode parameter.
 * In a transaction, adds the coupon to the cart and renders a JSON object that includes the coupon code
 * and the status of the transaction.
 *
 */
function addCouponJson() {
    var couponCode, cart, couponStatus;

    couponCode = request.httpParameterMap.couponCode.stringValue;
    cart = app.getModel('Cart').goc();

    Transaction.wrap(function () {
        couponStatus = cart.addCoupon(couponCode);
    });

    if (request.httpParameterMap.format.stringValue === 'ajax') {
        let r = require('~/cartridge/scripts/util/Response');
        r.renderJSON({
            status: couponStatus.code,
            message: Resource.msgf('cart.' + couponStatus.code, 'checkout', null, couponCode),
            success: !couponStatus.error,
            baskettotal: cart.object.adjustedMerchandizeTotalGrossPrice.value,
            CouponCode: couponCode
        });
    }
}

/**
 * Adds a coupon to the Checkout using JSON.
 *
 * Gets the CartModel. Gets the coupon code from the httpParameterMap couponCode parameter.
 * In a transaction, adds the coupon to the cart and renders a JSON object that includes the coupon code
 * and the status of the transaction.
 *
 */
function addCouponCheckoutJson() {
    var couponStatus, result, code;
    var CouponError = '';
    var couponType='';
    var couponApplied = true;
    cart = app.getModel('Cart').goc();
    var basket = cart.object;
    var couponCode = request.httpParameterMap.couponCode.stringValue;
    if (couponCode) {
        
        couponCode = couponCode.toUpperCase();
       Transaction.wrap(function () {
           couponStatus =  cart.addCoupon(couponCode);
        });

        if (couponStatus) {
            Transaction.wrap(function () {
                cart.removePersonaliCartRescue();
                cart.removePersonaliFromLineItems();
            });
        } else {
                CouponError = 'NO_ACTIVE_PROMOTION';
        }
    } else {
            CouponError = 'COUPON_CODE_MISSING';
    }
    for each(var couponLineItem in basket.couponLineItems) {
    	if (couponLineItem.couponCode.toUpperCase() == couponCode) {
    		couponApplied = couponLineItem.applied;
    		if(couponApplied) {
			  var priceAdjustment1 = couponLineItem.getPriceAdjustments().iterator();
			  while (priceAdjustment1.hasNext()) {
			   var priceAdjustment2 = priceAdjustment1.next();
			   couponType = priceAdjustment2.appliedDiscount.type;
      		  }
    		}
    			
    	}
    }
    if (request.httpParameterMap.format.stringValue === 'ajax') {
        let r = require('app_storefront_controllers/cartridge/scripts/util/Response');
        r.renderJSON({
            couponStatus: couponStatus.CouponStatus.code,
            success: !couponStatus.CouponStatus.error,
            CouponCode: couponCode,
            CouponError: CouponError,
            CouponApplied: couponApplied,
            CouponType: couponType
        });
    }
}

function changePickupLocation() {
    var cart = app.getModel('Cart').goc();
    var params = request.httpParameterMap;
    var format = empty(params.format.stringValue) ? '' : params.format.stringValue.toLowerCase();
    var pid = empty(params.pid.stringValue) ? '' : params.pid.stringValue;
    var preferredStore = app.getController('StorePicker').GetPreferredStore();
   // If this product is to be picked up in a store -- update store id
   if (!empty(params.storeId.stringValue)) {
        Transaction.wrap(function () {
            // update the product line item with the store id
            var plisIterator = cart.getAllProductLineItems().iterator();
            while (plisIterator.hasNext()) {
                var pli = plisIterator.next();
                if (session.custom.isCart){
                    // update all products in the cart
                	pli.custom.storePickupStoreID = preferredStore.ID;
                } else {
                    pli.custom.storePickupStoreID = params.storeId.stringValue;
                }
            }
        });
    }
}

function changeDeliveryOption(option) {
    // change from pickup in store to shipped to you option
    var store = "";
    var cart = app.getModel('Cart').goc();
    var params = request.httpParameterMap;
    var deliveryoption = (!empty(params.deliveryoption.value)) ? params.deliveryoption.stringValue : option;
    session.custom.deliveryOptionChoice = deliveryoption;
    var storeId = (deliveryoption == 'shipped') ? null : params.storeId.stringValue;
    if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule') && storeId !== null) {
    	try {
        	Transaction.wrap(function () {
                var plisIterator = cart.getAllProductLineItems().iterator();
                while (plisIterator.hasNext()) {
                    var pli = plisIterator.next();
                    var key = pli.productID + "-" + storeId;
                    var storeResponse = session.custom[key];
                    if (storeResponse && !empty(storeResponse)) {
                    	pli.custom.InventLocationId = storeResponse.Location1;
                    	pli.custom.secondaryWareHouse = storeResponse.location2;
        	            pli.custom.deliveryDate = storeResponse.SlotDate;
        	            pli.custom.mfiATPLeadDate = storeResponse.MFIATPLeadDate;
                    }
                }
            });
        } catch (e) {
        	var error = e;
        }
    } else {
    	Transaction.wrap(function () {
            var plisIterator = cart.getAllProductLineItems().iterator();
            while (plisIterator.hasNext()) {
                var pli = plisIterator.next();
                pli.custom.storePickupStoreID = storeId;
                pli.custom.secondaryWareHouse = storeId;
            }
        });
         if (request.httpParameterMap.format.stringValue === 'ajax') {
        	if (session.customer.registered && !empty(session.customer.profile.custom.preferredStore)) {
				store = session.customer.profile.custom.preferredStore;
			} else {
				store = (session.custom.preferredStore == null) ? "" : session.custom.preferredStore;
			}
			let r = require('~/cartridge/scripts/util/Response');
			if(empty(store)) {
				r.renderJSON({
       		    	success: false
            	});
			} else {
				r.renderJSON({
       				success: true
    			});
			}
         }
    }
}

function personaliPriceAdjustment(productLineItem, offerId, quantity, adjustedPrice, originalPrice) {
	try {
		productLineItem.custom['personaliDiscount'] = (adjustedPrice - originalPrice);	//	pli.product.priceModel.price.value;
		productLineItem.custom['personaliOfferID'] = offerId;
		// Remove current price adjustment
		var currentPriceAdjustment : PriceAdjustment = productLineItem.getPriceAdjustmentByPromotionID(offerId);
		if(currentPriceAdjustment) {
			productLineItem.removePriceAdjustment(currentPriceAdjustment);
		}
		
		// Create price adjustment
		var priceAdjust = productLineItem.createPriceAdjustment(offerId);
		// Other product-level price adjustments will be removed in CalculateCart.ds
		// priceAdjust.setPriceValue((discountPrice - pli.product.priceModel.price.decimalValue) * params.Quantity.doubleValue);
		priceAdjust.setPriceValue( ( adjustedPrice - originalPrice ) * quantity );
		priceAdjust.setLineItemText(Resource.msg('personali.lineItemText', 'personali', null));
		
		return true;
	} catch (e) {
		var error = e;
		return false;
	}
}

function addCartRescuePriceAdjustments() {
	var cart = app.getModel('Cart').goc();
    var netotiateOfferId = request.httpParameterMap.netotiateOfferId.stringValue;
    if (!empty(netotiateOfferId)) {
    	Transaction.wrap(function () {
    		cart.getPersonaliCartRescueResponse(netotiateOfferId);
    	});
    	return netotiateOfferId;
    }
}

/*
* Module exports
*/

/*
* Exposed methods.
*/
/** Adds a product to the cart.
 * @see {@link module:controllers/Cart~addProduct} */
exports.AddProduct = guard.ensure(['post'], addProduct);
/** Adds multiple products to the cart.
 * @see {@link module:controllers/Cart~addMultipleProducts} */
exports.AddMultipleProducts = guard.ensure(['https'], addMultipleProducts);
/** Invalidates the login and shipment forms. Renders the basket content.
 * @see {@link module:controllers/Cart~show} */
exports.Show = guard.ensure(['https'], show);
/** Form handler for the cart form.
 * @see {@link module:controllers/Cart~submitForm} */
exports.SubmitForm = guard.ensure(['post', 'https'], submitForm);
/** Redirects the user to the last visited catalog URL.
 * @see {@link module:controllers/Cart~continueShopping} */
exports.ContinueShopping = guard.ensure(['https'], continueShopping);
/** Adds a coupon to the cart using JSON. Called during checkout.
 * @see {@link module:controllers/Cart~addCouponJson} */
exports.AddCouponJson = guard.ensure(['get', 'https'], addCouponJson);
/** Adds a coupon to the cart using JSON. Called during checkout.
 * @see {@link module:controllers/Cart~addCouponCheckoutJson} */
exports.AddCouponCheckoutJson = guard.ensure(['get', 'https'], addCouponCheckoutJson);
/** Displays the current items in the cart in the minicart panel.
 * @see {@link module:controllers/Cart~miniCart} */
exports.MiniCart = guard.ensure(['get'], miniCart);
/** Adds the product with the given ID to the wish list.
 * @see {@link module:controllers/Cart~addToWishlist} */
exports.AddToWishlist = guard.ensure(['get', 'https', 'loggedIn'], addToWishlist, {
    scope: 'wishlist'
});
/** Adds bonus product to cart.
 * @see {@link module:controllers/Cart~addBonusProductJson} */
exports.AddBonusProduct = guard.ensure(['post'], addBonusProductJson);
/** Changes pickup location on products  in cart.
 * @see {@link module:controllers/Cart~changePickupLocation} */
exports.ChangePickupLocation = guard.ensure(['post'], changePickupLocation);
/** Changes pickup in store to shipped to you
 * @see {@link module:controllers/Cart~changeDeliveryOption} */
exports.ChangeDeliveryOption = guard.ensure(['post'], changeDeliveryOption);
exports.ChangeDeliveryOptionInternal = changeDeliveryOption;
/** Adds Cart Rescue price adjustments from Personali
 * @see {@link module:controllers/Cart~addCartRescuePriceAdjustments} */
exports.AddCartRescuePriceAdjustments = guard.ensure(['post'], addCartRescuePriceAdjustments);
