'use strict';

/**
 * This controller handles pick up in store related pages.
 *
 * @module controllers/PickUpInStore
 */

/* API Includes */
var Status = require('dw/system/Status');
var StoreMgr = require('dw/catalog/StoreMgr');
var URLUtils = require('dw/web/URLUtils');
var Resource = require('dw/web/Resource');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;

/**
 * Renders the pickup in store form.
 */
function show() {
    app.getForm('pickupinstore').clear();
    app.getView('PickUpInStore', {ContinueURL:URLUtils.https('PickUpInStore-Submit')}).render('content/storepickup');
}

/**
 * The form handler for the contactus form.
 */

function submit() {
    var storePickupForm = app.getForm('pickupinstore');
    var geolocationZip = request.getGeolocation().getPostalCode();
    if(empty(storePickupForm.object.postalCode.value)) {
        storePickupForm.object.postalCode.value = geolocationZip;
    }
    var storeResults = storePickupForm.handleAction({
        search: function (formgroup) {
            // If customer has a preferred store on their profile use it and then set session
            var preferredStore = (customer.authenticated && customer.profile.custom.preferredStore  !== null) ? customer.profile.custom.preferredStore : session.custom.storeId;
            session.custom.storeId = preferredStore;
            var searchKey = formgroup.postalCode.value;
            var storesMgrResult = StoreMgr.searchStoresByPostalCode(formgroup.countryCode.value, searchKey, formgroup.distanceUnit.value, formgroup.maxdistance.value);
            var stores = storesMgrResult.keySet();

            if (empty(stores)) {
                return null;
            } else {
                if(!empty(preferredStore)) {
                    var prefStore = StoreMgr.getStore(preferredStore);
                }
                var storesCount = stores.length;
                if(empty(preferredStore)){
                    var deliveryDates = getDeliveryDates(stores);
                    // limit stores to 4 stores only
                    var storesArray : Array = stores.toArray().slice(0,4);
                    return {'stores': storesArray, 'searchKey': searchKey, 'deliveryDates': deliveryDates, 'pid': formgroup.productId.value };
                } else if(!empty(preferredStore) && stores.contains(prefStore)){
                    var deliveryDates = getDeliveryDates(stores);
                    // limit stores to 4 stores only
                    var storesArray : Array = stores.toArray().slice(0,4);
                    return {'stores': storesArray, 'searchKey': searchKey, 'deliveryDates': deliveryDates, 'pid': formgroup.productId.value };
                } else if (!empty(preferredStore) && !stores.contains(prefStore)){
                    // add the preferred store
                    var storesArray : Array = new Array();
                    storesArray.push(prefStore);
                    // limit stores to 4 stores only
                    for (var i = 0; i < 3; i++) {
                        storesArray.push(stores[i]);
                    }
                    var deliveryDates = getDeliveryDates(storesArray);
                    return {'stores': storesArray, 'searchKey': searchKey, 'deliveryDates': deliveryDates, 'pid': formgroup.productId.value };
                }
            }
        },
        error: function () {
            // No special error handling if the form is invalid.
            return null;
        }
    });

    if (storeResults) {
        var pid = storePickupForm.object.productId.value;

        app.getView('PickUpInStore', {
            ConfirmationMessage: 'edit', StoreResults: storeResults, ProductId: pid
        }).render('content/storepickup');
    } else {
        app.getView('PickUpInStore', {
            ConfirmationMessage: Resource.msg('product.nostoresfound', 'product', null), StoreResults: storeResults, ProductId: pid
        }).render('content/storepickup');
    }
}

function getDeliveryDates(stores) {
    // delivery dates calculation is a mock service only and doesn't return anything
    var object = {};
    //var deliveryDatesArr = mattressPipeletHelper.getDeliveryDates(object);
    var deliveryDatesArr : Array = new Array();
    for (var i = 0; i < stores.length; i++) {
        // this will need to change to work when service is working and not mocked
        var storeDeliveryDate = {};
        storeDeliveryDate.storeID = stores[i].ID;
        storeDeliveryDate.Availability = 'Available Now';
        deliveryDatesArr.push(storeDeliveryDate);
    }
    return deliveryDatesArr;
}
/*
 * Module exports
 */

/*
 * Web exposed methods
 */
/** @see module:controllers/PickUpInStore~show */
exports.Show = guard.ensure(['get'], show);
/** @see module:controllers/PickUpInStore~submit */
exports.Submit = guard.ensure(['post'], submit);

