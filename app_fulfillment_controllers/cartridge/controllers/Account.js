'use strict';

/**
 * Controller that renders the account overview, manages customer registration and password reset,
 * and edits customer profile information.
 *
 * @module controllers/Account
 */

/* API includes */
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var Transaction = require('dw/system/Transaction');
var pageMeta = require('~/cartridge/scripts/meta');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var CustomObjectManager = require('dw/object/CustomObjectMgr');

//var pipeletHelper = require('bc_sleepysc/cartridge/scripts/sleepys/util/SleepysPipeletsHelper').SleepysPipeletHelper;
var emailHelper = require("~/cartridge/scripts/util/SFEmailSubscriptionHelper").SFEmailSubscriptionHelper;

var params = request.httpParameterMap;
/**
 * Gets a ContentModel object that wraps the myaccount-home content asset,
 * updates the page metadata, and renders the account/accountoverview template.
 */
function show() {
    var accountHomeAsset, pageMeta, Content;

    Content = app.getModel('Content');
    accountHomeAsset = Content.get('myaccount-home');

    pageMeta = require('~/cartridge/scripts/meta');
    pageMeta.update(accountHomeAsset);

    app.getView().render('account/accountoverview');
}

/**
 * Clears the profile form and copies customer profile information from the customer global variable
 * to the form. Gets a ContentModel object that wraps the myaccount-personaldata content asset, and updates the page
 * meta data. Renders the account/user/updateprofile template using an anonymous view.
 */
function editProfile(profileUpdated) {
    var pageMeta;
    var accountPersonalDataAsset;
    var Content = app.getModel('Content');

    if (!request.httpParameterMap.invalid.submitted) {
        app.getForm('profile').clear();

        app.getForm('profile.customer').copyFrom(customer.profile);
        app.getForm('profile.login').copyFrom(customer.profile);
        app.getForm('profile.addressbook.addresses').copyFrom(customer.profile.addressBook.addresses);
        if (customer.profile.addressBook.addresses.length > 0) {
        	app.getForm('profile.address').copyFrom(customer.profile.addressBook.addresses[0]);
        	app.getForm('profile.address.states').copyFrom(customer.profile.addressBook.addresses[0]);
        }
        var storeIdsArr = [];
        if(customer.profile.custom.accountType != 'Admin' && !empty(customer.profile.custom.fulfillment_StoreIds))
    	{
    		var storeIdsObj = JSON.parse(customer.profile.custom.fulfillment_StoreIds);
    		for (var key in storeIdsObj) {
    			storeIdsArr.push(storeIdsObj[key]);
    		}
    	}
        if (!empty(storeIdsArr)) {
	        var storeIds = storeIdsArr.toString();
	        storeIds = storeIds.replace(/,/g, ", ");
        }
    }
    accountPersonalDataAsset = Content.get('fulfillment-myaccount-personaldata');

    pageMeta = require('~/cartridge/scripts/meta');
    pageMeta.update(accountPersonalDataAsset);
    // @FIXME bctext2 should generate out of pagemeta - also action?!
    app.getView({
        bctext2: Resource.msg('account.user.registration.editaccount', 'account', null),
        Action: 'edit',
        storeIds: storeIds,
        profileUpdated: profileUpdated,
        ContinueURL: URLUtils.https('Account-EditForm')
    }).render('account/user/updateprofile');
}

/**
 * Handles the form submission on profile update of edit profile. Handles cancel and confirm actions.
 *  - cancel - clears the profile form and redirects to the Account-Show controller function.
 *  - confirm - gets a CustomerModel object that wraps the current customer. Validates several form fields.
 * If any of the profile validation conditions fail, the user is redirected to the Account-EditProfile controller function. If the profile is valid, the user is redirected to the Account-Show controller function.
 */
function editForm() {
    app.getForm('profile').handleAction({
        cancel: function () {
            app.getForm('profile').clear();
            response.redirect(URLUtils.https('Account-Show'));
        },
        confirm: function () {
            var isProfileUpdateValid = true;
            var hasEditSucceeded = false;
            var hasAddressUpdated = false;
            var Customer = app.getModel('Customer');

            if (!Customer.checkUserName()) {
                app.getForm('profile.customer.email').invalidate();
                isProfileUpdateValid = false;
            }
            /*
            if (app.getForm('profile.customer.email').value() !== app.getForm('profile.customer.emailconfirm').value()) {
                app.getForm('profile.customer.emailconfirm').invalidate();
                isProfileUpdateValid = false;
            }

            if (app.getForm('profile.login.password').value() !== app.getForm('profile.login.passwordconfirm').value()) {
                app.getForm('profile.login.passwordconfirm').invalidate();
                isProfileUpdateValid = false;
            }
            */

    		if (isProfileUpdateValid) {
    			var _optOutFlag = true; //!app.getForm('profile.customer.addtoemaillist').value();
            	//only update salesforce contact if opting in, because the default here is box unchecked
            	if (!_optOutFlag) {
                    var gaCookie : Cookie = request.getHttpCookies()['_ga'];
	                var gclid;
	                if (gaCookie) {
	                	gclid = gaCookie.value;
	                }

            		var emailParams = {
            				emailAddress: app.getForm('profile.customer.email').value(),
            				zipCode: session.custom.customerZip,
            				leadSource: 'account',
            				siteId: dw.system.Site.getCurrent().getID(),
            				optOutFlag: _optOutFlag,
            				gclid: gclid,
            				dwsid: session.sessionID
            		};

	    			var returnResult = emailHelper.sendSFEmailInfo(emailParams);
	    			if (returnResult.Status == 'SERVICE_ERROR'){
	    				var returnResult = emailHelper.sendFailSafe(emailParams, returnResult.ErrorCode);
	    			}

            	}
            	Transaction.wrap(function () {
            		app.getForm('profile').object.customer.email.value = customer.profile.email; // Email can't be changed for now
            		app.getForm('profile.customer').copyTo(customer.profile);
            		hasEditSucceeded = true;
    		    });
                var Address = app.getModel('Address');
                var addressForm = app.getForm('profile.address');

                try {
                    Address.update(app.getForm('profile.address.addressid').value(), session.forms.profile.address);
                    hasAddressUpdated = true;
                } catch (e) {
                	hasAddressUpdated = false;
                }
            }

            if (isProfileUpdateValid && hasEditSucceeded && hasAddressUpdated) {
				/*var argCustomerUpdate = {EventType: require('int_forter/cartridge/scripts/lib/forter/ForterConfig.ds').ForterConfig.CUSTOMER_PROFILE_UPDATE};
				var forterCall = require('int_forter/cartridge/scripts/pipelets/forter/ForterCustomerUpdate');
				forterCall.execute(argCustomerUpdate);*/
            	app.getController('Account').EditProfile(true);
            } else {
                response.redirect(URLUtils.https('Account-EditProfile', 'invalid', 'true'));
            }
        }
    });
}

/**
 * Gets the requestpassword form and renders the requestpasswordreset template. This is similar to the password reset
 * dialog, but has a screen-based interaction instead of a popup interaction.
 */
function passwordReset() {
    app.getForm('requestpassword').clear();
    pageMeta.update(dw.content.ContentMgr.getContent('fulfillment-passwordreset'));
    app.getView({
        ContinueURL: URLUtils.https('Account-PasswordResetForm')
    }).render('account/password/requestpasswordreset');
}

/**
 * Handles form submission from dialog and full page password reset. Handles cancel, send, and error actions.
 *  - cancel - renders the given template.
 *  - send - gets a CustomerModel object that wraps the current customer. Gets an EmailModel object that wraps an Email object.
 * Checks whether the customer requested the their login password be reset.
 * If the customer wants to reset, a password reset token is generated and an email is sent to the customer using the mail/resetpasswordemail template.
 * Then the account/password/requestpasswordreset_confirm template is rendered.
 *  - error - the given template is rendered and passed an error code.
 */
function passwordResetFormHandler(templateName, continueURL) {
    var resetPasswordToken, passwordemail;
    pageMeta.update(dw.content.ContentMgr.getContent('fulfillment-passwordreset'));
    
    app.getForm('profile').handleAction({
        cancel: function () {
            app.getView({
                ContinueURL: continueURL
            }).render(templateName);
        },
        send: function () {
            var Customer, resettingCustomer, Email;
            Customer = app.getModel('Customer');
            Email = app.getModel('Email');
            resettingCustomer = Customer.getCustomerByLogin(session.forms.requestpassword.email.htmlValue);

            if (!empty(resettingCustomer)) {
                resetPasswordToken = resettingCustomer.generatePasswordResetToken();

                passwordemail = Email.get('mail/resetpasswordemail', resettingCustomer.object.profile.email);
                passwordemail.setSubject(Resource.msg('resource.passwordassistance', 'email', null));
                passwordemail.send({
                    ResetPasswordToken: resetPasswordToken,
                    Customer: resettingCustomer.object.profile.customer
                });
            }

            //for security reasons the same message will be shown for a valid reset password request as for a invalid one
            app.getView({
                ErrorCode: null,
                ShowContinue: true,
                ContinueURL: continueURL
            }).render('account/password/requestpasswordreset_confirm');
        },
        error: function () {
            app.getView({
                ErrorCode: 'formnotvalid',
                ContinueURL: continueURL
            }).render(templateName);
        }
    });
}

/**
 * The form handler for password resets.
 */
function passwordResetForm() {
    passwordResetFormHandler('account/password/requestpasswordreset', URLUtils.https('Account-PasswordResetForm'));
}

/**
 * Clears the requestpassword form and renders the account/password/requestpasswordresetdialog template.
 */
function passwordResetDialog() {
    // @FIXME reimplement using dialogify
    app.getForm('requestpassword').clear();
    app.getView({
        ContinueURL: URLUtils.https('Account-PasswordResetDialogForm')
    }).render('account/password/requestpasswordresetdialog');
}

/**
 * Handles the password reset form.
 */
function passwordResetDialogForm() {
    // @FIXME reimplement using dialogify
    passwordResetFormHandler('account/password/requestpasswordresetdialog', URLUtils.https('Account-PasswordResetDialogForm'));
}

/**
 * Gets a CustomerModel wrapping the current customer. Clears the resetpassword form. Checks if the customer wants to reset their password.
 * If there is no reset token, redirects to the Account-PasswordReset controller function. If there is a reset token,
 * renders the screen for setting a new password.
 */
function setNewPassword() {
    var Customer, resettingCustomer;
    Customer = app.getModel('Customer');

    app.getForm('resetpassword').clear();
    resettingCustomer = Customer.getByPasswordResetToken(request.httpParameterMap.Token.getStringValue());

    if (empty(resettingCustomer)) {
        response.redirect(URLUtils.https('Account-PasswordReset'));
    } else {
        app.getView({
            ContinueURL: URLUtils.https('Account-SetNewPasswordForm')
        }).render('account/password/setnewpassword');
    }
}

/**
 * Gets a profile form and handles the cancel and send actions.
 *  - cancel - renders the setnewpassword template.
 *  - send - gets a CustomerModel object that wraps the current customer and gets an EmailModel object that wraps an Email object.
 * Checks whether the customer can be retrieved using a reset password token.
 * If the customer does not have a valid token, the controller redirects to the Account-PasswordReset controller function.
 * If they do, then an email is sent to the customer using the mail/setpasswordemail template and the setnewpassword_confirm template is rendered.
 * */
function setNewPasswordForm() {

    app.getForm('profile').handleAction({
        cancel: function () {
            app.getView({
                ContinueURL: URLUtils.https('Account-SetNewPasswordForm')
            }).render('account/password/setnewpassword');
            return;
        },
        send: function () {
            var resettingCustomer, success, passwordchangedmail, Customer, Email;

            Customer = app.getModel('Customer');
            Email = app.getModel('Email');
            resettingCustomer = Customer.getByPasswordResetToken(request.httpParameterMap.Token.getStringValue());

            if (empty(resettingCustomer)) {
                response.redirect(URLUtils.https('Account-PasswordReset'));
            } else {

                if (app.getForm('resetpassword.password').value() !== app.getForm('resetpassword.passwordconfirm').value()) {
                    app.getForm('resetpassword.passwordconfirm').invalidate();
                    app.getView({
                        ContinueURL: URLUtils.https('Account-SetNewPasswordForm')
                    }).render('account/password/setnewpassword');
                } else {

                    success = resettingCustomer.resetPasswordByToken(request.httpParameterMap.Token.getStringValue(), app.getForm('resetpassword.password').value());
                    if (!success) {
                        app.getView({
                            ErrorCode: 'formnotvalid',
                            ContinueURL: URLUtils.https('Account-SetNewPasswordForm')
                        }).render('account/password/setnewpassword');
                    } else {
                        passwordchangedmail = Email.get('mail/passwordchangedemail', resettingCustomer.object.profile.email);
                        passwordchangedmail.setSubject(Resource.msg('resource.passwordassistance', 'email', null));
                        passwordchangedmail.send({
                          Customer: resettingCustomer.object.profile.customer
                        });
                        //pipeletHelper.setNeedsNewPasswordResetFalse(resettingCustomer);

                        app.getView().render('account/password/setnewpassword_confirm');
                    }
                }
            }
        }
    });
}

/**
 * Renders the screen for changing password if the customer is logged in, else it takes the customer to login page.
 */
function changePassword(passwordUpdated) {
    var pageMeta;
    var accountPersonalDataAsset;
    var Content = app.getModel('Content');

    if (!request.httpParameterMap.invalid.submitted) {
        app.getForm('changepassword').clear();
    }
    accountPersonalDataAsset = Content.get('fulfillment-myaccount-changepassword');

    pageMeta = require('app_fulfillment_controllers/cartridge/scripts/meta');
    pageMeta.update(accountPersonalDataAsset);

    app.getView({
    	passwordUpdated	: passwordUpdated,
        ContinueURL		: URLUtils.https('Account-ChangePasswordForm')
    }).render('account/password/changepassword');
}

/**
 * Gets a changepassword form and handles the cancel and apply actions.
 *  - cancel - renders the changepassword template.
 *  - apply - gets a CustomerModel object that wraps the current customer.
 * If the password update succeeds then the controller redirects to Account-Show else it redirects back to Account-ChangePassword
 * */
function changePasswordForm() {
	app.getForm('changepassword').handleAction({
        cancel: function () {
            app.getForm('changepassword').clear();
            response.redirect(URLUtils.https('Account-ChangePassword'));
        },
        apply: function () {
        	var isProfileUpdateValid = true;
            var hasUpdateSucceeded = false;
            var Customer = app.getModel('Customer');

            if (!app.getForm('changepassword.login.currentpassword').value()) {
                app.getForm('changepassword.login.currentpassword').invalidate();
                isProfileUpdateValid = false;
            }

            if (app.getForm('changepassword.login.newpassword').value() !== app.getForm('changepassword.login.newpasswordconfirm').value()) {
                app.getForm('changepassword.login.newpasswordconfirm').invalidate();
                isProfileUpdateValid = false;
            }

            if (isProfileUpdateValid) {
            	hasUpdateSucceeded = Customer.updatePassword(customer.profile.email, app.getForm('changepassword.login.newpassword').value(), app.getForm('changepassword.login.currentpassword').value(), app.getForm('changepassword.login'));
                if (!hasUpdateSucceeded) {
                    app.getForm('changepassword.login.currentpassword').invalidate();
                }
            }

            if (isProfileUpdateValid && hasUpdateSucceeded) {
            	app.getController('Account').ChangePassword(true);
            } else {
                response.redirect(URLUtils.https('Account-ChangePassword', 'invalid', 'true'));
            }
        },
        error: function () {
            response.redirect(URLUtils.https('Account-ChangePassword', 'invalid', 'true'));
        }
    });
}

/**
 * Returns a unique system generated alpha-numeric password
 * */
function generatePassword() {
	var uniquePassword = dw.util.UUIDUtils.createUUID().substring(0, 8);
    if (request.httpParameterMap.format.stringValue === 'ajax') {
        let r = require('~/cartridge/scripts/util/Response');

        r.renderJSON({
            success			: !empty(uniquePassword),
            uniquePassword	: uniquePassword
        });
        return;
    } else {
    	return uniquePassword;
    }
}

/**
 * JavaScript Helper Function to convert an array into Object
 */
function toObject(arr) {
	var obj = {};
	for (var i = 0; i < arr.length; ++i)
		if (arr[i] !== undefined) obj[i] = arr[i];
	return obj;
}

/** Clears the profile form, adds the email address from login as the profile email address,
 *  and renders customer registration page.
 */
function startRegister(newCustomerCreated) {

	pageMeta.update(dw.content.ContentMgr.getContent('fulfillment-create-account'));
    app.getForm('profile').clear();
    app.getForm('profile').object.login.accounttype.value = 'Dealer';//app.getForm('profile').object.login.accounttype.dealer.value;
    if (app.getForm('login.username').value() !== null) {
        app.getForm('profile.customer.email').object.value = app.getForm('login.username').object.value;
    }

    var storeIds = CustomObjectManager.getAllCustomObjects('fulfillment_storeid');
    app.getView({
    	storeIds			: storeIds,
    	newCustomerCreated	: newCustomerCreated,
        ContinueURL			: URLUtils.https('Account-RegistrationForm')
    }).render('account/user/registration');
}

/**
 * Gets a CustomerModel object wrapping the current customer.
 * Gets a profile form and handles the confirm action.
 *  confirm - validates the profile by checking  that the email and password fields:
 *  - match the emailconfirm and passwordconfirm fields
 *  - are not duplicates of existing username and password fields for the profile
 * If the fields are not valid, the registration template is rendered.
 * If the fields are valid, a new customer account is created, the profile form is cleared and
 * the customer is redirected to the Account-Show controller function.
 */
function registrationForm() {
	app.getForm('profile').handleAction({
        confirm: function () {
        	var email, emailConfirmation, profileValidation, password, passwordConfirmation, existingCustomer, Customer, target;

            Customer = app.getModel('Customer');
            email = app.getForm('profile.customer.email').value();
            //emailConfirmation = app.getForm('profile.customer.emailconfirm').value();
            profileValidation = true;

            /*
            if (email !== emailConfirmation) {
                app.getForm('profile.customer.emailconfirm').invalidate();
                profileValidation = false;
            }
            */

            password = app.getForm('profile.login.password').value();
            passwordConfirmation = app.getForm('profile.login.passwordconfirm').value();

            if (password !== passwordConfirmation) {
                app.getForm('profile.login.passwordconfirm').invalidate();
                profileValidation = false;
            }

            // Checks if login is already taken.
            existingCustomer = Customer.getCustomerByLogin(email);
            if (existingCustomer !== null) {
                app.getForm('profile.customer.email').invalidate();
                profileValidation = false;
            }

            if (profileValidation) {
            	// Create Customer
            	profileValidation = Customer.createAccount(email, password, app.getForm('profile'));

            	/*var argCustomerUpdate = {EventType: require('int_forter/cartridge/scripts/lib/forter/ForterConfig.ds').ForterConfig.CUSTOMER_CREATE};
                var forterCall = require('int_forter/cartridge/scripts/pipelets/forter/ForterCustomerUpdate');
                forterCall.execute(argCustomerUpdate);*/
            }

            if (!profileValidation) {
                // TODO redirect
            	var storeIds = CustomObjectManager.getAllCustomObjects('fulfillment_storeid');
                app.getView({
                	storeIdsArr : request.httpParameterMap.store_ids.values,
            		storeIds	: storeIds,
                    ContinueURL	: URLUtils.https('Account-RegistrationForm')
                }).render('account/user/registration');
            } else {
            	var _optOutFlag = true; //!app.getForm('profile.customer.addtoemaillist').value();
                var gaCookie : Cookie = request.getHttpCookies()['_ga'];
                var gclid;
                if (gaCookie) {
                	gclid = gaCookie.value;
                }

        		var emailParams = {
        				emailAddress: email,
        				zipCode: session.custom.customerZip,
        				leadSource: 'account',
        				siteId: dw.system.Site.getCurrent().getID(),
        				optOutFlag: _optOutFlag,
        				gclid: gclid,
        				dwsid: session.sessionID
        		};
        		/*
    			var returnResult = emailHelper.sendSFEmailInfo(emailParams);
    			if (returnResult.Status == 'SERVICE_ERROR'){
    				var returnResult = emailHelper.sendFailSafe(emailParams, returnResult.ErrorCode);
    			}
    			*/

        		var enableCreatedCustomer = app.getForm('profile.login.accountenabled').value() == 'true' ? true : false;
                // Set Account Enabled field, Account Type, First Name & Store IDs of newly created customer
    			Transaction.wrap(function () {
    				var customerCreated : dw.customer.Customer = dw.customer.CustomerMgr.getCustomerByLogin(email);
    				customerCreated.profile.credentials.setEnabledFlag(enableCreatedCustomer);
    				if(customerCreated) {
    		        	customerCreated.profile.custom.accountType			= app.getForm('profile.login.accounttype').value();
    		        	if (app.getForm('profile.login.accounttype').value() != 'Admin') {
	                        // Storing Store IDs in customer profile, as a JSON string
	                        var storeIdsArr = request.httpParameterMap.store_ids.values;
	                        var storeIdsObj = toObject(storeIdsArr);
	                        var storeIDsJSONStr = JSON.stringify(storeIdsObj);
	                        if (!empty(storeIDsJSONStr)) {
	                            customerCreated.profile.custom.fulfillment_StoreIds	= storeIDsJSONStr;
	                        }
    		        	}
    		        }
    		    });

    			if (profileValidation) {
	    			//Add Address
    				app.getForm('profile').object.address.country.value = 'US';
	            	var Address = app.getModel('Address');
	                if(!Address.create(session.forms.profile.address, email)){
	                	var storeIds = CustomObjectManager.getAllCustomObjects('fulfillment_storeid');
	                	app.getView({
	                		storeIds	: storeIds,
	                        ContinueURL: URLUtils.https('Account-RegistrationForm')
	                    }).render('account/user/registration');
	                }
    			}

    			if (customer.registered && customer.authenticated && customer.profile.custom.accountType == 'Admin' && enableCreatedCustomer) {
		            var Customer, Email;
		            Customer = app.getModel('Customer');
		            Email = app.getModel('Email');
		        	// Send Email to the customer with his password
		            var Email = app.getModel('Email');
	                var newaccountemail = Email.get('mail/accountcreatedemail', email);
	                newaccountemail.setSubject(Resource.msg('accountcreated.emailsubject', 'email', null));
	                newaccountemail.send({
	                    Password		: app.getForm('profile.login.password').value(),
	                    CustomerFirstName	: app.getForm('profile.customer.firstname').value(),
	                    CustomerLastName	: app.getForm('profile.customer.lastname').value(),
	                    CustomerEmail	: app.getForm('profile.customer.email').value()
	                });
            	}

                app.getForm('profile').clear();
            	app.getController('Account').StartRegister(true);
            }
        },
        error: function () {
        	var storeIds = CustomObjectManager.getAllCustomObjects('fulfillment_storeid');
        	app.getView({
        		storeIds	: storeIds,
        		ContinueURL	: URLUtils.https('Account-RegistrationForm')
           }).render('account/user/registration');
       }
    });
}

/**
 * Renders the accountnavigation template.
 */
function includeNavigation() {
    app.getView().render('account/accountnavigation');
}

function testSetNewPasswordConfirm() {
    app.getView().render('account/password/setnewpassword_confirm');
}
function testSetNewPassword() {
    app.getView().render('account/password/setnewpassword');
}

/* Web exposed methods */

/** Renders the account overview.
 * @see {@link module:controllers/Account~show} */
exports.Show = guard.ensure(['get', 'https', 'loggedIn'], show);
/** Updates the profile of an authenticated customer.
 * @see {@link module:controllers/Account~editProfile} */
exports.EditProfile = guard.ensure(['https', 'loggedIn'], editProfile);
/** Handles the form submission on profile update of edit profile.
 * @see {@link module:controllers/Account~editForm} */
exports.EditForm = guard.ensure(['post', 'https', 'loggedIn'], editForm);
/** Renders the password reset dialog.
 * @see {@link module:controllers/Account~passwordResetDialog} */
exports.PasswordResetDialog = guard.ensure(['get', 'https'], passwordResetDialog);
/** Renders the password reset screen.
 * @see {@link module:controllers/Account~passwordReset} */
exports.PasswordReset = guard.ensure(['get', 'https'], passwordReset);
/** Handles the password reset form.
 * @see {@link module:controllers/Account~passwordResetDialogForm} */
exports.PasswordResetDialogForm = guard.ensure(['post', 'https'], passwordResetDialogForm);
/** The form handler for password resets.
 * @see {@link module:controllers/Account~passwordResetForm} */
exports.PasswordResetForm = guard.ensure(['post', 'https'], passwordResetForm);
/** Renders the screen for setting a new password.
 * @see {@link module:controllers/Account~setNewPassword} */
exports.SetNewPassword = guard.ensure(['get', 'https'], setNewPassword);
/** Handles the set new password form submit.
 * @see {@link module:controllers/Account~setNewPasswordForm} */
exports.SetNewPasswordForm = guard.ensure(['post', 'https'], setNewPasswordForm);
/** Renders the screen for changing password.
 * @see {@link module:controllers/Account~changePassword} */
exports.ChangePassword = guard.ensure(['https', 'loggedIn'], changePassword);
/** Handles the set change password form submit.
 * @see {@link module:controllers/Account~changePasswordForm} */
exports.ChangePasswordForm = guard.ensure(['post', 'https', 'loggedIn'], changePasswordForm);
/** Generates and returns a unique system generated alpha-numeric password.
 * @see {@link module:controllers/Account~generatePassword} */
exports.GeneratePassword = guard.ensure(['get', 'https', 'loggedIn'], generatePassword, params);
/** Start the customer registration process and renders customer registration page.
 * @see {@link module:controllers/Account~startRegister} */
exports.StartRegister = guard.ensure(['https', 'loggedIn'], startRegister);
/** Handles registration form submit.
 * @see {@link module:controllers/Account~registrationForm} */
exports.RegistrationForm = guard.ensure(['post', 'https', 'loggedIn'], registrationForm);
/** Renders the account navigation.
 * @see {@link module:controllers/Account~includeNavigation} */
exports.IncludeNavigation = guard.ensure(['get'], includeNavigation);

/*Temporary - Just for testing*/
exports.TestSetNewPassword = guard.ensure(['get'], testSetNewPassword);
exports.TestSetNewPasswordConfirm = guard.ensure(['get'], testSetNewPasswordConfirm);
