'use strict';

/* API includes */
var Resource = require('dw/web/Resource');
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var Response = require('~/cartridge/scripts/util/Response');
var CustomerMgr = require('dw/customer/CustomerMgr');
var URLUtils = require('dw/web/URLUtils');
var Transaction = require('dw/system/Transaction');

/* Script Modules */
var pageMeta = require('~/cartridge/scripts/meta');
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var CustomObjectManager = require('dw/object/CustomObjectMgr');
var Logger = require('dw/system/Logger');
var params = request.httpParameterMap;

//var pipeletHelper = require('bc_sleepysc/cartridge/scripts/sleepys/util/SleepysPipeletsHelper').SleepysPipeletHelper;
var emailHelper = require("~/cartridge/scripts/util/SFEmailSubscriptionHelper").SFEmailSubscriptionHelper; 

var params = request.httpParameterMap;

function show() {
	pageMeta.update(dw.content.ContentMgr.getContent('fulfillment-search-manage-users'));
	app.getView().render('account/searchusers');
}

function search() {
	var searchBy = params.searchBy.stringValue;
	var searchValue = params.searchValue.stringValue;

	var result;
	var customers = [];
	if (searchBy == 'name') {
		var query = "(";
		var names = searchValue.split(" ");
		for (var i = 0; i < names.length; i++) {
			query += "firstName ILIKE '*" + names[i] + "*' OR lastName ILIKE '*" + names[i] + "*'";
			if (i < names.length - 1) {
				query += " OR ";
			}
		}
		query += ")";

		result = CustomerMgr.searchProfiles(query, "firstName, lastName", searchValue);
		if (result.count > 0) {
			while (result.hasNext()) {
				var customerObj = result.next();
				customers.push(getCustomerDetails(customerObj));
			}
		}
	} else if(searchBy == 'email') {
		result = CustomerMgr.getCustomerByLogin(searchValue);
		if (result != null) {
			customers.push(getCustomerDetails(result.profile));
		}
	} else {
		response.redirect(URLUtils.https('SearchUsers-Show'));
	}

	Response.renderJSON(customers);
}

/**
 * Clears the profile form and copies customer profile information from the fetched customer variable
 * to the form. Renders the account/user/manageaccount template using an anonymous view.
 */
function EditUserProfile(profileUpdated) {
	var emailOfCustomerToEdit = params.email.stringValue;
	var customerToEdit = CustomerMgr.getCustomerByLogin(emailOfCustomerToEdit);	// Customer to Edit
    if (!empty(customerToEdit)) {
        app.getForm('profile').clear();
        app.getForm('profile.customer').copyFrom(customerToEdit.profile);
        app.getForm('profile.login').copyFrom(customerToEdit.profile);
        app.getForm('profile.addressbook.addresses').copyFrom(customerToEdit.profile.addressBook.addresses);
        if (customerToEdit.profile.addressBook.addresses.length > 0) {
        	app.getForm('profile.address').copyFrom(customerToEdit.profile.addressBook.addresses[0]);
        	app.getForm('profile.address.states').copyFrom(customerToEdit.profile.addressBook.addresses[0]);
        }
        // Get the current Enable status of the customerToEdit, and set on form
        app.getForm('profile').object.login.accountenabled.value = customerToEdit.profile.credentials.getEnabledFlag().toString();
        var storeIdsArr = [];
        if(customerToEdit.profile.custom.accountType != 'Admin' && !empty(customerToEdit.profile.custom.fulfillment_StoreIds)) {
    		var storeIdsObj = JSON.parse(customerToEdit.profile.custom.fulfillment_StoreIds);	
    		for (var key in storeIdsObj) {
    			storeIdsArr.push(storeIdsObj[key]);
    		}
    	}
        if (!empty(storeIdsArr)) {
	        var myStoreIds = storeIdsArr.toString();
	        myStoreIds = myStoreIds.replace(/,/g, ", ");
        }
        var allStoreIds = CustomObjectManager.getAllCustomObjects('fulfillment_storeid');
    }
    app.getView({
        Action				: 'edit',
        customerToEdit		: customerToEdit,
        myStoreIds			: myStoreIds,
        allStoreIds			: allStoreIds,
        profileUpdated		: profileUpdated,
        emailOfCustomerToEdit: emailOfCustomerToEdit,
        ContinueURL			: URLUtils.https('SearchUsers-HandleEditUserProfile','email',emailOfCustomerToEdit)
    }).render('account/user/manageaccount');
}


function handleEditUserProfile() {
    app.getForm('profile').handleAction({
        cancel: function () {
            app.getForm('profile').clear();
            response.redirect(URLUtils.https('EditUserProfile-Show'));
        },
        confirm: function () {
            var isProfileUpdateValid	= true;
            var hasEditSucceeded		= false;
            var hasAddressUpdated		= false;
            var Customer				= app.getModel('Customer');
            var emailOfCustomerToEdit	= params.email.stringValue;
            var customerToEdit			= CustomerMgr.getCustomerByLogin(emailOfCustomerToEdit);	// Customer to Edit

            if (!Customer.checkUserName(emailOfCustomerToEdit)) {
                app.getForm('profile.customer.email').invalidate();
                isProfileUpdateValid = false;
            }
    		if (isProfileUpdateValid) {
    			var _optOutFlag = true; //!app.getForm('profile.customer.addtoemaillist').value();
            	//only update salesforce contact if opting in, because the default here is box unchecked
            	if (!_optOutFlag) {
                    var gaCookie : Cookie = request.getHttpCookies()['_ga'];
	                var gclid;
	                if (gaCookie) {
	                	gclid = gaCookie.value;
	                }
            		
            		var emailParams = {
            				emailAddress: app.getForm('profile.customer.email').value(), 
            				zipCode: session.custom.customerZip, 
            				leadSource: 'account', 
            				siteId: dw.system.Site.getCurrent().getID(), 
            				optOutFlag: _optOutFlag,
            				gclid: gclid,
            				dwsid: session.sessionID
            		};
            		
	    			var returnResult = emailHelper.sendSFEmailInfo(emailParams);
	    			if (returnResult.Status == 'SERVICE_ERROR'){
	    				var returnResult = emailHelper.sendFailSafe(emailParams, returnResult.ErrorCode);
	    			}
	    			
            	}
        		var enableCustomerToEdit = app.getForm('profile.login.accountenabled').value() == 'true' ? true : false;
                // Update Account Enabled flag, First/Last Name & Store IDs of customerToEdit
        		Transaction.wrap(function () {
            		customerToEdit.profile.credentials.setEnabledFlag(enableCustomerToEdit);	// Set the enable flag
            		app.getForm('profile').object.customer.email.value = customerToEdit.profile.email;	// Email can't be changed for now
            		app.getForm('profile.customer').copyTo(customerToEdit.profile);
            		
		        	if (app.getForm('profile.login.accounttype').value() != 'Admin') {
                        // Append new selected Store IDs in already existing Store IDs of customer profile, as a JSON string
		        		var existingStoreIDsObj = JSON.parse(customerToEdit.profile.custom.fulfillment_StoreIds);
		        		var existingStoreIDsArr = [];
		        		// Keep existing Store IDs
		        		for (var key in existingStoreIDsObj) {
		        			existingStoreIDsArr.push(existingStoreIDsObj[key]);
		        		}
		        		var existingAndAdditionalStoreIDsArr = existingStoreIDsArr;
                        var additionalStoreIDsArr = request.httpParameterMap.store_ids.values;
                        // Append the additional Store IDs in existing ones
                        for (var i=0; i<additionalStoreIDsArr.length; i++) {
                        	existingAndAdditionalStoreIDsArr.push(additionalStoreIDsArr[i]);
                        }
                        existingAndAdditionalStoreIDsArr.sort();
                        var existingAndAdditionalStoreIDsObj = arrayToObject(existingAndAdditionalStoreIDsArr);
                        var existingAndAdditionalStoreIDsJSONstr = JSON.stringify(existingAndAdditionalStoreIDsObj);
                        if (!empty(existingAndAdditionalStoreIDsJSONstr)) {
                        	customerToEdit.profile.custom.fulfillment_StoreIds = existingAndAdditionalStoreIDsJSONstr;
                        }
		        	}
            		hasEditSucceeded = true;
    		    });

        		var Address = app.getModel('Address');
                var addressForm = app.getForm('profile.address');
                
                try {
                	// Update the address
                    Address.update(app.getForm('profile.address.addressid').value(), session.forms.profile.address, customerToEdit);
                    hasAddressUpdated = true;
                } catch (e) {
                	Logger.error('Unable to edit customer address: '+e.message);
                	hasAddressUpdated = false;
                }
            }

            if (isProfileUpdateValid && hasEditSucceeded && hasAddressUpdated) {
            	app.getController('SearchUsers').EditUserProfile(true);
            } else {
            	app.getController('SearchUsers').EditUserProfile(false);
            }
        },
        error: function () {
        	app.getController('SearchUsers').EditUserProfile(false);
        }
    });
}

function getCustomerDetails(profile) {
	return {
		id : profile.customer.ID,
		name : profile.firstName + ' ' + profile.lastName,
		email : profile.email,
		customerNo : profile.customerNo,
	}
}

/**
 * JavaScript Helper Function to convert an array into Object
 */
function arrayToObject(arr) {
	var obj = {};
	for (var i = 0; i < arr.length; ++i)
		if (arr[i] !== undefined) obj[i] = arr[i];
	return obj;
}

exports.Show = guard.ensure([ 'get', 'loggedIn' ], show);
exports.Search = guard.ensure([ 'get', 'loggedIn' ], search, params);
exports.EditUserProfile = guard.ensure(['loggedIn' ], EditUserProfile);
exports.HandleEditUserProfile = guard.ensure([ 'post', 'loggedIn' ], handleEditUserProfile);

