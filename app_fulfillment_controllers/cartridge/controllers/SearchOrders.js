'use strict';

var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var URLUtils = require('dw/web/URLUtils');
var OrderMgr = require('dw/order/OrderMgr');
var CustomObjectMgr = require ('dw/object/CustomObjectMgr');
var HashMap = require('dw/util/HashMap');
var params = request.httpParameterMap;
var fulfillment = require('~/cartridge/controllers/Fulfillment');
var pageMeta = require('~/cartridge/scripts/meta');

function show() {
	app.getForm('searchorders').clear();
	app.getView({
		ContinueURL: URLUtils.https('SearchOrders-Search')
	}).render('account/searchorders');
}

function search() {
	var searchOrdersBy = '';
	if(!empty(params['search-orders-by'].stringValue)) {
		searchOrdersBy = params['search-orders-by'].stringValue;
	}
	pageMeta.update(dw.content.ContentMgr.getContent('fulfillment-search-orders'));
	var myForm = app.getForm('searchorders');
	app.getForm('searchorders').handleAction({
		search: function () {
        	var orderResults = [];
        	switch (searchOrdersBy) {
        		// Search by Event Name
        	    case 'search-orders-event':
        	    	var eventName = '';
        	    	if (empty(app.getForm('searchorders.searchvalue.eventname').value())) {
        	    		app.getForm('searchorders.searchvalue.eventname').invalidate();
        	    		break;
        	    	} else {
            	    	eventName = app.getForm('searchorders.searchvalue.eventname').value();
            	    	eventName = '*' + eventName + '*';	// search for Events having this keyword in anywhere in Event Name
        	    	}
        	    	var qEvents : String = "custom.eventName ILIKE {0}";	// perform a case-insensitive search
                	if(customer && customer.authenticated && customer.profile.custom.accountType == "Admin") {	// If the user is Admin then fetch all Events
               	    	var eventsIter : dw.util.SeekableIterator = CustomObjectMgr.queryCustomObjects('fulfillment_events', qEvents, 'custom.eventDate DESC', eventName);
                	} else if (customer && customer.authenticated && customer.profile.custom.accountType == "Dealer") {	// If the user is Dealer then fetch only his Events
                		qEvents += ' AND custom.userId = {1}';
               	    	var eventsIter : dw.util.SeekableIterator = CustomObjectMgr.queryCustomObjects('fulfillment_events', qEvents, 'custom.eventDate DESC', eventName, customer.ID);
                	}
        	    	while(eventsIter.hasNext()) {
        	    		var event = eventsIter.next();
        	    		var ordersIter : dw.util.SeekableIterator = OrderMgr.queryOrders('custom.eventId={0}', 'creationDate desc', event.custom.ID);
        	    		while(ordersIter.hasNext()) {
        	    			var dwOrder = ordersIter.next();
        	    			var orderObj = createOrderObj(dwOrder, event);
        	    			if (!empty(orderObj)) {
        	    				orderResults.push(orderObj);
        	    			}
        	    		}
        	    	}
        	    	break;
    	    	// Search by Order Submitted Date (range)
        	    case 'search-orders-date':
        	    	var dateFrom, dateTo;
        	    	var parameters = params;
        	    	if (!empty(params.dwfrm_searchorders_searchvalue_datefrom.stringValue)) {
        	    		let tempFromDate = new Date(params.dwfrm_searchorders_searchvalue_datefrom.stringValue);
        	    		tempFromDate.setHours(0,0,0,0);	// Search from start of the day
        	    		dateFrom = tempFromDate;
        	    	}
        	    	if (!empty(params.dwfrm_searchorders_searchvalue_dateto.stringValue)) {
        	    		let tempToDate = new Date(params.dwfrm_searchorders_searchvalue_dateto.stringValue);
        	    		tempToDate.setHours(23,59,59,999);	// Search till end of the day
        	    		dateTo = tempToDate;
        	    	}
        	    	if (!empty(dateFrom) && !empty(dateTo)) {
        	    		//var qOrders = 'custom.submissionDate >= {0} AND custom.submissionDate <= {1}';
        	    		var qOrders = 'creationDate >= {0} AND creationDate <= {1}';
        	    		var ordersIter : dw.util.SeekableIterator = OrderMgr.queryOrders(qOrders, 'creationDate desc', dateFrom, dateTo);
        	    		while(ordersIter.hasNext()) {
        	    			var dwOrder = ordersIter.next();
        	    			if (!empty(dwOrder)) {
        	    				var eventId = dwOrder.custom.eventId;
                       	    	var event = CustomObjectMgr.getCustomObject('fulfillment_events', eventId);
                       	    	if (empty(event)) {
                       	    		break;
                       	    	}
        	    				if (customer && customer.authenticated && customer.profile.custom.accountType == "Admin") {	// If the user is Admin show this found order
                    	    		var orderObj = createOrderObj(dwOrder, event);
                	    			if (!empty(orderObj)) {
                	    				orderResults.push(orderObj);
                	    			}
        		        	    } else if (customer && customer.authenticated && customer.profile.custom.accountType == "Dealer") {	// If the user is Dealer then get his Events and see if this order belongs to any of his Event
        		        	    	if (event.custom.userId == customer.ID) {	// If this event belongs to the Dealer
        	            	    		var orderObj = createOrderObj(dwOrder, event);
                    	    			if (!empty(orderObj)) {
                    	    				orderResults.push(orderObj);
                    	    			}
        	            	    	}
        	                	}
                	    	}
        	    		}
        	    	} else {
        	    		break;
        	    	}
        	        break;
        	    // Search by Order Number
        	    case 'search-orders-order-number':
        	    	var orderNoSearched = '';
        	    	if (empty(app.getForm('searchorders.searchvalue.ordernumber').value())) {
        	    		app.getForm('searchorders.searchvalue.ordernumber').invalidate();
        	    		break;
        	    	} else {
        	    		orderNoSearched = app.getForm('searchorders.searchvalue.ordernumber').value();
        	    	}
        	    	var dwOrder = OrderMgr.getOrder(orderNoSearched);
        	    	if (!empty(dwOrder)) {
        	    		var eventId = dwOrder.custom.eventId;
               	    	var event = CustomObjectMgr.getCustomObject('fulfillment_events', eventId);
               	    	if (empty(event)) {
               	    		break;
               	    	}
	        	    	if (customer && customer.authenticated && customer.profile.custom.accountType == "Admin") {	// If the user is Admin just get the order from Business Manager
        	    			var orderObj = createOrderObj(dwOrder, event);
        	    			if (!empty(orderObj)) {
        	    				orderResults.push(orderObj);
        	    			}
		        	    } else if (customer && customer.authenticated && customer.profile.custom.accountType == "Dealer") {	// If the user is Dealer then get his Events and see if this order belongs to any of his Event
	            	    	if (event.custom.userId == customer.ID) {	// If this event belongs to the Dealer
	            	    		var orderObj = createOrderObj(dwOrder, event);
            	    			if (!empty(orderObj)) {
            	    				orderResults.push(orderObj);
            	    			}
	            	    	}
	                	}
        	    	}
        	        break;
        	    // Search by Store ID
        	    case 'search-orders-store-id':
        	    	var storeId = '';
        	    	if (empty(app.getForm('searchorders.searchvalue.storeid').value())) {
        	    		app.getForm('searchorders.searchvalue.storeid').invalidate();
        	    		break;
        	    	} else {
        	    		storeId = app.getForm('searchorders.searchvalue.storeid').value();
        	    	}
        	    	var qStoreId : String = "custom.storeId = {0}";
                	if(customer && customer.authenticated && customer.profile.custom.accountType == "Admin") {	// If the user is Admin then fetch all Events having this Store ID
               	    	var eventsIter : dw.util.SeekableIterator = CustomObjectMgr.queryCustomObjects('fulfillment_events', qStoreId, 'custom.eventDate DESC', storeId);
                	} else if (customer && customer.authenticated && customer.profile.custom.accountType == "Dealer") {	// If the user is Dealer then fetch only his Events
                		qStoreId += ' AND custom.userId = {1}';
               	    	var eventsIter : dw.util.SeekableIterator = CustomObjectMgr.queryCustomObjects('fulfillment_events', qStoreId, 'custom.eventDate DESC', storeId, customer.ID);
                	}
        	    	while(eventsIter.hasNext()) {
        	    		var event = eventsIter.next();
        	    		var ordersIter : dw.util.SeekableIterator = OrderMgr.queryOrders('custom.eventId={0}', 'creationDate desc', event.custom.ID);
        	    		while(ordersIter.hasNext()) {
        	    			var dwOrder = ordersIter.next();
        	    			var orderObj = createOrderObj(dwOrder, event);
        	    			if (!empty(orderObj)) {
        	    				orderResults.push(orderObj);
        	    			}
        	    		}
        	    	}
        	        break;
        	    default:
        	}
        	app.getView({
        		searchPerformed	: true,
        		orderResults	: orderResults,
        		searchedBy		: searchOrdersBy,
        		ContinueURL		: URLUtils.https('SearchOrders-Search')
        	}).render('account/searchorders');
        },
        cancel: function (){
        	app.getForm('searchorders').clear();
        	app.getView({
        		ContinueURL: URLUtils.https('SearchOrders-Search')
        	}).render('account/searchorders');
        },
        error: function () {
        	app.getForm('searchorders').clear();
        	app.getView({
        		ContinueURL: URLUtils.https('SearchOrders-Search')
        	}).render('account/searchorders');
       }
    });
}

function getShippingMethod(order) {
	var shippingMethod = '';
	for each (var shipment in order.shipments) {
		if (!empty(shipment.shippingMethodID) && shipment.productLineItems.length > 0) {
			shippingMethod = shipment.shippingMethodID;
		}
	}
	return shippingMethod;
}

/*
 * Input: 
 * 	dwOrder	: Demandware Order object
 * 	event	: Event object
 * Output
 * 	orderObj : Order object with only required attributes
 * */
function createOrderObj(dwOrder, event) {
	var orderObj = null;
	if (!empty(dwOrder) && !empty(event)) {
		var defaultShipment, shippingAddress;
	    defaultShipment = dwOrder.getDefaultShipment();
	    shippingAddress = defaultShipment.getShippingAddress();
	    var deliveryType = app.getController('Fulfillment').GetOrderDeliveryType(dwOrder);
	    var deliveryDate = defaultShipment.custom.deliveryDate;
		var deliveryTime = defaultShipment.custom.deliveryTime;

		if (deliveryType.toLowerCase() == 'core') {
	    	deliveryType = 'Local';
	    }

		var shippingMethod = getShippingMethod(dwOrder);

		if (deliveryType.toLowerCase() == 'mixed') {
			for each (var dShipment in dwOrder.shipments) {
				var pliIterator = dShipment.getProductLineItems().iterator();
        		while (pliIterator.hasNext()) {
        			var pli : Product = pliIterator.next();
        			if(pli.product.custom.shippingInformation == 'Core') {
        				deliveryDate = dShipment.custom.deliveryDate;
        				deliveryTime = dShipment.custom.deliveryTime;
        				break;
        			}
        		}
			}
	    }
		
		orderObj = {
				status          : dwOrder.status.value,
				exportStatus    : dwOrder.exportStatus.value,
				shippingStatus  : dwOrder.shippingStatus.value,
				confirmationStatus : dwOrder.confirmationStatus.value,
				paymentStatus   : dwOrder.paymentStatus.value,
				orderNo			: dwOrder.orderNo,
				eventName		: event.custom.eventName,
				eventID         : event.custom.ID,
				storeId			: event.custom.storeId,
				submissionDate	: dwOrder.custom.submissionDate,	// Make a new custom attribute in System Object (Order) for submissionDate
				creationDate	: dwOrder.creationDate,
				deliveryDate	: deliveryDate,
				deliveryTime	: deliveryTime,
				deliveryType	: deliveryType,
				shippingMethod  : shippingMethod,
				reSchedule      : fulfillment.CanBeReschedule(dwOrder),
				customerName	: shippingAddress.getFullName(),
				qty				: dwOrder.productQuantityTotal,	// Quantity of all Items (excluding: bundled line items and option line items)
				total			: dwOrder.getTotalGrossPrice().getValue()
		}
	}
	return orderObj;
}
exports.Show = guard.ensure(['get', 'loggedIn'], show);
exports.Search = guard.ensure(['https', 'loggedIn'], search);
exports.GetShippingMethod = getShippingMethod;
