/**
* Controller for retrieving a nearest Warehouse for guest pickup.
*
* @module  controllers/WarehousePicker
*/

'use strict';

/* API Includes */
var HashMap = require('dw/util/HashMap');
var Countries = require('app_fulfillment_core/cartridge/scripts/util/Countries');
var StoreMgr = require('dw/catalog/StoreMgr');
var Site = require('dw/system/Site');
var URLUtils = require('dw/web/URLUtils');
var Transaction = require('dw/system/Transaction');
var Logger = require('dw/system/Logger');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var Basket = dw.order.BasketMgr.getCurrentBasket();
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var OrderMgr = require('dw/order/OrderMgr');
var ShippingMgr = require('dw/order/ShippingMgr');
var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;
var fulfillment = require('~/cartridge/controllers/Fulfillment');
var params = request.httpParameterMap;
var eventDeletedMessage = 'This Event does not exist anymore or it might have been deleted because of expiry set by the Administrator';
var ATPInventoryServices = require("bc_sleepysc/cartridge/scripts/init/services-ATPInventory");

 var wareHoueSlots = [
                      {
                	    "Location1": "001056",
                	    "location2": "001000",
                	    "Zone": "",
                	    "ZoneId": "",
                	    "SlotDate": "9/8/2018 12:00:00 AM",
                	    "StartTime": null,
                	    "EndTime": null,
                	    "Slots": "",
                	    "MFIATPLeadDate": "1/1/1900 12:00:00 AM",
                	    "Available": "YES",
                	    "ATPQuantity": "5"
                	  }
                	];
function getNearbyStores(storeParams) {
	//var customerZip = session.custom.customerZip;
	var customerZip = storeParams.customerZip;
	var maxDistance = !empty(storeParams.maxDistance) ? Number(storeParams.maxDistance) : Number(100);
	var countryCode = Countries.getCurrent({
		CurrentRequest: {
				locale: request.locale
			}
		}).countryCode;
	var distanceUnit = (countryCode == 'US') ? 'mi' : 'km';
	var storeMap = StoreMgr.searchStoresByPostalCode(countryCode, customerZip, distanceUnit, maxDistance);
	var storeArray = [];
	// convert map to array of stores
	for each( var store in storeMap.entrySet()) {
		storeArray.push(store);
	}
	/*var warehouse = {
			id		: store.key.custom.warehouseId,
			name	: store.key.custom.warehouseName,
			address	: store.key.custom.warehouseAddress
	}
	return warehouse;
	*/
	var store;
	for (var i=0; i<storeArray.length; i++) {
		if (!empty(storeParams.warehouseId)) {
			if (!empty(storeArray[i].key.custom.warehouseId) && storeArray[i].key.custom.warehouseId == storeParams.warehouseId) {
				store = storeArray[i];
				break;
			}
		} else if (!empty(storeArray[i].key.custom.warehouseId)) {
			store = storeArray[i];
			break;
		}
	}
	return store;
}

function getATPAvailabilityMessage(serviceParams) {
	
	/*
	 * New Business Requirement:
	 * The first available date received in the ATP slots will be used as the Warehouse pickup date.
	 * If 'firstAvailableDateObj' was received in 'seviceParams' then do not give call to Warehouse Pickup service instead return that date.
	 * */
	if (!serviceParams.isLocal && !empty(serviceParams.firstAvailableDateObj)) {
		return serviceParams.firstAvailableDateObj;
	}
	var customerZip 	= serviceParams.customerZip,
		warehouseId 	= serviceParams.warehouseId,
		order			= serviceParams.order,
		requestedDate	= new Date(),
		todaysDate		= new Date(),
		products		= new Array(),
		availabilityDate = null,
		availabilityClass = 'in-stock-msg',
		isInStock		= false;
	
	session.custom.warehosueId = warehouseId;
	
	if (/*dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpAvailabilityMessage') &&*/ customerZip) {
		if (!session.custom.ATPAvailability) {
			session.custom.ATPAvailability = new Object();
		}
		
		if (!empty(order)) {
			var plis = order.allProductLineItems;
			for (var i = 0; i < plis.length; i++ ) {
				var pli = plis[i];
				products.push({
					'ProductId'	: pli.productID,
					'Quantity'	: pli.quantityValue
				});
			}
		}
		
		try {
			if (!serviceParams.isLocal) {
				requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
				var requestPacket = '{"InventoryType": "warehouse","ZipCode": "' + customerZip + '","RequestedDate": "' + requestedDate + '","StoreId": "' + warehouseId + '","Products": ' + JSON.stringify(products) + '}';
				var service = ATPInventoryServices.ATPInventoryMonthFP;
				Logger.error('\nATP URL: '+service.getURL()+'\nATP RequestPacket (Warehouse): '+requestPacket);
				var result = service.call(requestPacket);
			} else {
				var result = new Object();
				result['error'] = 0;
			}
			if (result.error != 0 || result.errorMessage != null || result.mockResult) {
				session.custom.warehosuePickupDate = null;
				Logger.error('ATP warehouse (error): '+result.errorMessage);
			} else {
				if (serviceParams.isLocal) {
					Logger.error('Static Warehouse Slot: ' + JSON.stringify(wareHoueSlots));
				} else {
					Logger.error('ATP warehouse (Response): '+result.object.text);
				}
				var jsonResponse = (serviceParams.isLocal)?wareHoueSlots:JSON.parse(result.object.text);

				if (jsonResponse.Status) {
					for (var i = 0; i < jsonResponse.Data.length; i++ ) {
						var response = jsonResponse.Data[i];
						var deliveryDate = new Date(response.SlotDate);
						
						if (response.Available.toLowerCase() === 'yes' && Number(response.ATPQuantity) > 0) {
							isInStock = true;
							availabilityDate = deliveryDate;
							session.custom.warehosuePickupDate = deliveryDate;
							break;
						} else {
							session.custom.warehosuePickupDate = null;
						}
					}
				}
			}
		} catch (e) {
			var msg = e;
			session.custom.warehosuePickupDate = null;
			Logger.error('ATP warehouse pickup service failed: ' + msg);
		}
	}
	if (!isInStock) {
		availabilityClass = 'not-available-msg';
	}
	
	var availabilityObj = {
			availabilityDate	: availabilityDate,
			availabilityClass	: availabilityClass,
			warehouseId			: warehouseId,
			isInStock			: isInStock
	}
	
	return availabilityObj;
}

function schedulePickup() {
	var orderId = params.orderId.stringValue; //session.custom.orderId;
	var eventId	= params.eventId.stringValue; //session.custom.eventId;
	// 'session.custom.warehosuePickupDate' is getting its value from getATPAvailabilityMessage()
	var pickupDate = params.warehousePickupDate.stringValue;
	var warehouseId = params.warehouseId.stringValue;

	var warehousePickupDate = pickupDate;
	var queryString = 'custom.ID = {0}';
	var eventObj = CustomObjectMgr.queryCustomObject('fulfillment_events', queryString, eventId);
	if(empty(eventObj)) {
		app.getController('Fulfillment').Dashboard(eventDeletedMessage);
		return;
	}
	
	var orderObj = OrderMgr.queryOrder('orderNo={0}', orderId);
	if(empty(orderObj)) {
		app.getController('Fulfillment').Dashboard(eventDeletedMessage);
		return;
	}
	var shipments = orderObj.shipments;
	
	var servicePriceObj	= "";
	var args 			= new Object();
	
	// Order Level Custom Attributes
	Transaction.wrap(function () {
		orderObj.custom.InventLocationId = eventObj.custom.storeId; // Dealers Location
		var shippingAddress = orderObj.getDefaultShipment().getShippingAddress();
		orderObj.custom.mfiPickupDeliveryName = shippingAddress.getFullName();	// Name of End Customer
	});
	
	args['storeId'] = eventObj.custom.storeId;
	var cart = app.getModel('Cart').get(orderObj);

	
	for each(var shipment in shipments) {
		
		try {
			Transaction.wrap(function () {
				var sdt = fulfillment.GetShipmentDeliveryType(shipment);
				if(sdt == 'Drop Ship') {
					shipment.custom.deliveryDate = mattressPipeletHelper.getISODate('31 Dec 2049, Sun');
					shipment.custom.deliveryTime = '08:30am-09:00pm';
				} else { 
					shipment.custom.deliveryDate = warehousePickupDate;
					shipment.custom.deliveryTime = '12:01am-12:01am'; // not required in case of Warehouse, but required for order export
				}
				var pliIterator = shipment.getProductLineItems().iterator();

            	while (pliIterator.hasNext()) {      		
            		var pli : Product = pliIterator.next();
                	pli.custom.deliveryDate = shipment.custom.deliveryDate;
                	
                	if (shipment.custom.deliveryDate) {
                		// Product Line Item Level Custom Attributes
            			pli.custom.InventLocationId		= warehouseId;
            			pli.custom.secondaryWareHouse	= warehouseId;
            			/*pli.custom.mfiDSZoneLineId = session.custom.deliveryDates[key]['timeslots'][deliveryTimeSlot]['mfiDSZoneLineId'];
            			pli.custom.mfiDSZipCode = session.custom.deliveryDates[key]['timeslots'][deliveryTimeSlot]['mfiDSZipCode'];
            			session.custom.deliveryZone = session.custom.deliveryDates[key]['timeslots'][deliveryTimeSlot]['mfiDSZoneLineId'];*/

            			var optionLineItems = pli.optionProductLineItems;
            			for (var j = 0; j < optionLineItems.length; j++) {
            				var oli = optionLineItems[j];
            				if (oli.productID != 'none') {
            					oli.custom.InventLocationId = warehouseId;
            				}
            			}
                	}
            	}
            	if (sdt != 'Drop Ship') {
	            	// Update Shipping Method to storePickup (Pickup from Warehouse)
	            	var allShippingMethods = ShippingMgr.getAllShippingMethods();
	            	for each (var shippingMethod in allShippingMethods) {
	        			if (shippingMethod.ID.equals('storePickup')) {
	        				shipment.setShippingMethod(shippingMethod);
	        				var cart = app.getModel('Cart').goc();
	        				cart.calculate();
	                    }
	            	}
            	}
			});
			
		} catch (e) {
			Logger.error("Error while adding pickup Date: "+ e.message);
		}
	}
	
	//Calculate service charges & shipping costs
	var cartObj = app.getModel('Cart').get(orderObj);
	for each(var shipment in cartObj.object.shipments) {
		var pliIterator = shipment.getProductLineItems().iterator();
		while (pliIterator.hasNext()) {
    		var pli : Product = pliIterator.next();
        	if(shipment.shippingMethodID == 'storePickup') {
    			args['serviceSKU'] = 'tpf127542';
    			servicePriceObj = fulfillment.GetServicePrice(args);
    		}
    	}
	}

	if(!empty(servicePriceObj)) {
		Transaction.wrap(function () {
			fulfillment.CalculateShippingCosts(cartObj, servicePriceObj);
			fulfillment.CalculateTax(cartObj.object);
			cart.updateTotals();
		});
	}
}

exports.GetNearbyStores = guard.ensure(['get'], getNearbyStores);
exports.GetATPAvailabilityMessage = guard.ensure(['get'], getATPAvailabilityMessage);
exports.SchedulePickup = guard.ensure(['https','loggedIn'], schedulePickup);
