'use strict';

/**
*   Controller that restores the shopping cart based on product ID params.
*   For example:  http://www.mattressfirm.com/default/RestoreCart-Start?qtys=1,2&pids=mfiV000095278,mfiV000088269
*   With SEO URL: http://www.mattressfirm.com/cart-history?qtys=1,2&pids=mfiV000095278,mfiV000088269
*
*   @module controllers/RestoreCart
*/

/* API Includes */
var Transaction = require('dw/system/Transaction');
var Pipelet = require('dw/system/Pipelet');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');

/*
*   Restore the shopping cart based on product ID params
*   http://dev18-web-sleepys.demandware.net/on/demandware.store/Sites-Mattress-Firm-Site/default/RestoreCart-Start?qtys=1,2&pids=mfiV000095278,mfiV000088269
*   http://dev18-web-sleepys.demandware.net/s/Mattress-Firm/cart-history?qtys=1,2&pids=mfiV000095278,mfiV000088269
*/
function start() {

    var params = request.httpParameterMap;

    if (params.qtys.stringValue == null || params.pids.stringValue == null) {
        
        response.redirect(dw.web.URLUtils.https('Cart-Show'));
    
    } else {
    
        var qtys = params.qtys.stringValue.split(',');
        var pids= params.pids.stringValue.split(',');
        var counter = 0;

        if (pids.length > 0 && qtys.length > 0 && pids.length == qtys.length) {
            var cartModel = app.getModel('Cart');
            var Basket = cartModel.goc();
            var cartAsset = app.getModel('Content').get('cart');

            pageMeta = require('~/cartridge/scripts/meta');
            pageMeta.update(cartAsset);

            for (i=0; i < pids.length; i++) {
                var ProductModel = app.getModel('Product');
                var product = ProductModel.get(pids[i]).object;
                var availabilityStatus = product.availabilityModel.availabilityStatus;
                if (availabilityStatus == dw.catalog.ProductAvailabilityModel.AVAILABILITY_STATUS_IN_STOCK || availabilityStatus == dw.catalog.ProductAvailabilityModel.AVAILABILITY_STATUS_PREORDER || availabilityStatus == dw.catalog.ProductAvailabilityModel.AVAILABILITY_STATUS_BACKORDER) {
                    
                    var ProductOptionModel = new Pipelet('UpdateProductOptionSelections').execute({
                        Product: product
                    }).ProductOptionModel;

                    var plis = Basket.getProductLineItems(pids[i]);
                    var qty = parseInt(qtys[counter]);
                    qty = qty > 10 ? 10 : qty;
                    qty = qty <= 0? 1 : qty;

                    if (plis.length > 0) {
                        var pli = plis[0];
                        if (pli.quantity > qty) {
                            continue;
                        } else {
                            Transaction.wrap(function () {
                                Basket.updateLineItem(pli, product, parseInt(qtys[counter]), ProductOptionModel);
                            });
                        }
                    } else {
                        Transaction.wrap(function () {
                            var ProductLineItem = Basket.addProductItem(product, qty, null, ProductOptionModel);
                        });
                    }
                }

                counter = counter + 1;
            }

            Transaction.wrap(function () {
                Basket.calculate();
            });
        }

		//add back the url parameters for tracking and analytic purposes
		//dev18-web-sleepys.demandware.net/s/Mattress-Firm/cart-history?qtys=2,1,1&pids=mfiV000088280,mfiV000088068,mfiV000099237&j=10236538&sfmc_sub=1189802145&l=150937_HTML&u=239435553&mid=1378406&jb=1&utm_source=auto&utm_campaign=shopcart_v2&utm_medium=Email
		//https://development-web-sleepys.demandware.net/s/Mattress-Firm/cart-history?qtys=2,1,1&pids=mfiV000088280,mfiV000088068,mfiV000099237&j=10236538&sfmc_sub=1189802145&l=150937_HTML&u=239435553&mid=1378406&jb=1&utm_source=auto&utm_campaign=shopcart_v2&utm_medium=Email
		var httpQueryString : String = request.httpQueryString;
		var cartShowWParams = dw.web.URLUtils.https('Cart-Show').toString() + "?" + httpQueryString;
        response.redirect(cartShowWParams);
        //app.getController('Cart').Show();
    }
}

/*
* Module exports
*/

/*
* Exposed methods.
*/
/** Adds a product to the cart.
 * @see {@link module:controllers/RestoreCart~start} */
exports.Start = guard.ensure(['get'], start);