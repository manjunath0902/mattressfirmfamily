importPackage( dw.system );
importPackage( dw.util );
importPackage( dw.web ); 
importPackage( dw.io );
importPackage( dw.object );
importPackage( dw.net );
importPackage( dw.catalog );

'use strict';

/**
 * Controller 
 *
 * @module controllers/DispatchTracker
 */

/* API Includes */
var Logger = require('dw/system/Logger');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');


// Receives the AJAX submission of dispatch tracking and responds
function submitOrderNumber() {
	var params = request.httpParameterMap;
	var orderNumber : String = params.orderNumber.stringValue;
	//var CSRFTokenName : String = params.CSRFTokenName.stringValue;
	//var CSRFToken : String = params.CSRFToken.stringValue;
	var CSRFValidate : Bolean = dw.web.CSRFProtection.validateRequest();
	
	if(CSRFValidate){

		var httpClient : HTTPClient = new HTTPClient();
		var dispatchTrackAPIUrl = Site.current.preferences.custom.dispatchTrackAPIUrl;
		var dispatchTrackAPIUser = Site.current.preferences.custom.dispatchTrackAPIUser;
		var dispatchTrackAPIPassword = Site.current.preferences.custom.dispatchTrackAPIPassword;
		var jsonObj : String;
		
		httpClient.open('GET', dispatchTrackAPIUrl + orderNumber,dispatchTrackAPIUser,dispatchTrackAPIPassword);
		httpClient.send();
		 
		if (httpClient.statusCode == 200)
		{
			 jsonObj = httpClient.text;
			 app.getView({
				 JSONData: jsonObj,
			 }).render('util/output');	 
		}
		else
		{
		     // error handling
			 jsonObj = '{"error":"An error occurred with status code '+httpClient.statusCode+'"}';
			 app.getView({
				 JSONData: jsonObj,
			 }).render('util/output');	 
		 }
		
	}else{
	     // error handling
		 jsonObj = '{"error":"CSRF expired"}';
		 app.getView({
			 JSONData: jsonObj,
		 }).render('util/output');			
	}
		
}

//Gets product image id and size
function getProductInfo() {
	var params = request.httpParameterMap;
    var product : Product = ProductMgr.getProduct(params.pid.stringValue);

    
	var jsonObj : String;

	var imageID : String = null;
	if(product.custom.external_id != null){
		imageID = product.custom.external_id;
	}

	var size : String = null;		
	var sizeAttribute : String = null;
	if(product.variationModel != null){	
		sizeAttribute = product.variationModel.getSelectedValue(product.variationModel.getProductVariationAttribute("size"));
		if(sizeAttribute != null){
			size = sizeAttribute.displayValue;
		}
	}
	
	var title : String = null;
	if (product.name != null) {
		title = product.name.replace('&ldquo;',' Inch').replace('"',' Inch');
	}else{
		title = "";
	}

	if(product){
		 jsonObj = '{"ID":"' + params.pid.stringValue + '","imageID":"' + imageID + '","size":"' + size + '","name":"' + title + '"}';
		 app.getView({
			 JSONData: jsonObj,
		 }).render('util/output');		
	}else{
		 jsonObj = '{"error":"Product not found"}';
		 app.getView({
			 JSONData: jsonObj,
		 }).render('util/output');		
	}
}





/* Exports of the controller */

/** @see {@link module:controllers/DispatchTracking~submitOrderNumber} */
exports.SubmitOrderNumber = guard.ensure(['get'], submitOrderNumber);

/** @see {@link module:controllers/DispatchTracking~getProductInfo} */
exports.GetProductInfo = guard.ensure(['get'], getProductInfo);
