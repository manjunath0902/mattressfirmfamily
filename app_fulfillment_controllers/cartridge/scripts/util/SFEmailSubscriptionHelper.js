importPackage( dw.svc );
importPackage( dw.system );
importPackage( dw.object );
importPackage( dw.util );
importPackage( dw.crypto );

var SFEmailSubscriptionHelper = {
	sendSFEmailInfo : function(obj) : Object {
		//importScript("app_storefront_core:util/ViewHelpers.ds");
		//debugHelper();
				
		var emailAddress = obj.emailAddress;
		var zipCode = obj.zipCode;
		var leadSource = obj.leadSource;
		var siteId = obj.siteId;
		var optOutFlag = obj.optOutFlag;
		var leadDate = obj.leadDate;
		var gclid = obj.gclid;
		var dwsid = obj.dwsid;
		
		var service : Service;
		var result  :  Result;
		var recordTypeIdCustomer = Site.current.preferences.custom.sfCustomerRecordTypeID; //'012G00000017CrRIAU';
		var recordTypeIdLead = Site.current.preferences.custom.sfLeadRecordTypeID; //'012G0000001QVPXIA4';
		var returnObject : Object = {};		
		var sitePrefix : String;
		
		//not currently used, but here in case other sites are addded in the future
		//prefix would be used for the custom fields eg. MFRM_Lead_Source__c
		if (siteId == 'Mattress-Firm') {
			sitePrefix = 'MFRM';
		} else if (siteId == 'Tulo') {
			sitePrefix = 'tulo';
		} else {
	    	returnObject.Status = "ERROR";		
			returnObject.ErrorCode = "NOT_MFRM_TULO";
	    	return returnObject;
		}
		
		if (empty(optOutFlag)) {
			optOutFlag = false;
		}
		
		if (empty(obj.leadDate)) {
			leadDate = dw.system.Site.calendar;
			leadDate.setTimeZone("UTC");
		}
				
		if (empty(emailAddress)) {
			returnObject.Status = "ERROR";
			returnObject.ErrorCode = "EMPTY_EMAIL";
			return returnObject;
		}
		emailAddress = emailAddress.toLowerCase().trim();
		
		var strikeIronCode = SFEmailSubscriptionHelper.getStrikeIronCode(emailAddress);
		if (strikeIronCode == 0) {
	    	returnObject.Status = "SERVICE_ERROR";		
	    	returnObject.ErrorCode = "SI_SERVICE_ERROR";
	    	return returnObject;
		}
				
		if (Site.current.preferences.custom.sfAPIInstance.toLowerCase() == 'dev') {
			var sfClientId = Site.current.preferences.custom.sfAPIDevClientID;
			var sfSecret = Site.current.preferences.custom.sfAPIDevSecret;
			var sfUsername = Site.current.preferences.custom.sfAPIDevUsername;
			var sfPassword = Site.current.preferences.custom.sfAPIDevPassword;
		} else {
			var sfClientId = Site.current.preferences.custom.sfAPIProdClientID;
			var sfSecret = Site.current.preferences.custom.sfAPIProdSecret;
			var sfUsername = Site.current.preferences.custom.sfAPIProdUsername;
			var sfPassword = Site.current.preferences.custom.sfAPIProdPassword;			
		}
		//
		//	SF OAuth Authentication
		//
		var sfServiceName = Site.current.preferences.custom.sfAPIServiceName;
		try {
			service = ServiceRegistry.get(sfServiceName);
			service.addParam('grant_type', 'password')
				.addParam('client_id', sfClientId)
				.addParam('client_secret', sfSecret)
				.addParam('username', sfUsername)
				.addParam('password', sfPassword);
			
			var restArgs = {
			 	method: 'POST',
			 	type: 'OAUTH',
			 	body: ''
			}
        } catch (e) {
            returnObject.Status = "SERVICE_ERROR";        
            returnObject.ErrorCode = "SERVICE_ERROR";                
            return returnObject;
        }
			
		try {
			result = service.call(restArgs);
		} catch (e) {
	    	returnObject.Status = "SERVICE_ERROR";		
	    	returnObject.ErrorCode = "SERVICE_ERROR";			
            return returnObject;
		}
		
		if (!result.ok){
			var token : String = '';
			var url   : String = '';
		
	    	returnObject.Status = "SERVICE_ERROR";		
	    	returnObject.ErrorCode = "SERVICE_ERROR";
	    	
	    	return returnObject;
		}
		
	 	var responseOAuth : object = JSON.parse(result.object);
		var token : String = responseOAuth.access_token;
		var url   : String = responseOAuth.instance_url; 		 
		
		//
		//	SF Contacts
		//
		
		var queryContact = "select Id, OtherPostalCode, RecordTypeId, email, LastName, MFRM_Lead_Source__c, MFRM_Lead_Date__c, tulo_Lead_Source__c, tulo_Lead_Date__c, EmailVerificationCode__c, HasOptedOutOfEmail from contact where email = '" + emailAddress + "'"
		result = SFEmailSubscriptionHelper.getData(sfServiceName, url, token, queryContact);		  		 
	    var responseContacts : object = JSON.parse(result.object);
	    
		if (responseContacts) {
	    if (responseContacts.totalSize == 0) {
			if (sitePrefix.toUpperCase() == 'MFRM') {	    	
		    	var restArgs = {
		    		 	method: 'POST',
		    		 	type: 'CREATE',
		    		 	body : {     	
		    		        RecordTypeId: recordTypeIdLead,
		    		        Email: emailAddress,
		    		        LastName: 'Email Lead',
		    		        OtherPostalCode: zipCode,
		    		        MFRM_Lead_Source__c: leadSource,
		    		        MFRM_Lead_Date__c: leadDate.getTime(),
		    		        EmailVerificationCode__c: strikeIronCode,
		    		        HasOptedOutOfEmail: optOutFlag
		    		        //Google_Client_ID__c: gclid,
		    		        //DW_Session_ID__c: dwsid
		    		 	}
		    	 };
			} else if (sitePrefix.toUpperCase() == 'TULO') {	    	
		    	var restArgs = {
		    		 	method: 'POST',
		    		 	type: 'CREATE',
		    		 	body : {     	
		    		        RecordTypeId: recordTypeIdLead,
		    		        Email: emailAddress,
		    		        LastName: 'Email Lead',
		    		        OtherPostalCode: zipCode,
		    		        tulo_Lead_Source__c: leadSource,
		    		        tulo_Lead_Date__c: leadDate.getTime(),
		    		        EmailVerificationCode__c: strikeIronCode,
		    		        HasOptedOutOfEmail: optOutFlag
		    		        //Google_Client_ID__c: gclid,
		    		        //DW_Session_ID__c: dwsid
		    		 	}
		    	 };
			}
			
    	    //
    	    // Create Contact
    	    //
    		service = ServiceRegistry.get(sfServiceName);	// reset the service to remove oauth service params
    		service.URL = url + "/services/data/v40.0/sobjects/Contact/";
    		service.addHeader("Authorization", "Bearer " + token);       
    		service.addHeader("Content-Type", "application/json");
    		
    		result = service.call(restArgs);
    		
    		var contactId = JSON.parse(result.object).id;
    		service.URL = url + "/services/data/v40.0/sobjects/Contact_Web_History__c/";
	    	var restArgs = {
	    		 	method: 'POST',
	    		 	type: 'CREATE',
	    		 	body : {
	    		 		Contact__c: contactId,
	    		 		Google_Client_ID__c: gclid,
	    		 		DW_Session_ID__c: dwsid
	    		 	}
	    	};
	    	result = service.call(restArgs); 		 
        						
	    	returnObject.Status = "OK";		
	    	returnObject.Msg = '(' + strikeIronCode + ')';
			returnObject.ErrorCode = "";
			returnObject.ContactID = contactId;
	    	
	    } else if (responseContacts.totalSize == 1) {	
	    	//if record is customer then don't update zip
	
	    	if (responseContacts.records[0].RecordTypeId == recordTypeIdCustomer) {
	    		zipCode = undefined;
	    		var result = SFEmailSubscriptionHelper.updateContacts(sfServiceName, url, token, responseContacts, strikeIronCode, leadDate, leadSource, optOutFlag, gclid, dwsid, zipCode, sitePrefix);
	    	} else {
	    		var result = SFEmailSubscriptionHelper.updateContacts(sfServiceName, url, token, responseContacts, strikeIronCode, leadDate, leadSource, optOutFlag, gclid, dwsid, zipCode, sitePrefix);
	    	}
			
	    	var siteExists = (sitePrefix.toUpperCase() == 'MFRM' &&  responseContacts.records[0].MFRM_Lead_Source__c != null) || (sitePrefix.toUpperCase() == 'TULO' &&  responseContacts.records[0].tulo_Lead_Source__c != null);
			
	    	if (siteExists) {
		    	returnObject.Status = "ERROR";		   
		    	returnObject.Msg = "EXISTS: " + responseContacts.records[0].Id + ' (' + strikeIronCode + ')' + ' ' + siteId;	
				returnObject.ErrorCode = "exists";
				returnObject.ContactID = result.ContactID;
	    	} else {
		    	returnObject.Status = "OK";		
		    	returnObject.Msg = '(' + strikeIronCode + ')';
				returnObject.ErrorCode = "";
				returnObject.ContactID = result.ContactID;
	    	}
	    } else {

			result = SFEmailSubscriptionHelper.getData(sfServiceName, url, token, queryContact + " and RecordTypeId = '" + recordTypeIdCustomer + "' order by LastModifiedDate desc, " + sitePrefix + "_Lead_Date__c desc limit 1");
			var responseCustomerContacts : object = JSON.parse(result.object);
				    
			
			if (responseCustomerContacts.totalSize == 0) {
				//Its all Lead records, Update them all
	    		var result = SFEmailSubscriptionHelper.updateContacts(sfServiceName, url, token, responseContacts, strikeIronCode, leadDate, leadSource, optOutFlag, gclid, dwsid, zipCode, sitePrefix);
			} else {
				//Update only the customer records, Don't Update zip and leadsource on customer records
				zipCode = undefined;
				var result = SFEmailSubscriptionHelper.updateContacts(sfServiceName, url, token, responseCustomerContacts, strikeIronCode, leadDate, leadSource, optOutFlag, gclid, dwsid, zipCode, sitePrefix);
			}

	    	var siteExists = (sitePrefix.toUpperCase() == 'MFRM' &&  responseContacts.records[0].MFRM_Lead_Source__c != null) || (sitePrefix.toUpperCase() == 'TULO' &&  responseContacts.records[0].tulo_Lead_Source__c != null);
	    	if (siteExists) {
		    	returnObject.Status = "ERROR";		
		    	returnObject.Msg = "MANY EXISTS" + ' (' + strikeIronCode + ')' + ' ' + siteId;
				returnObject.ErrorCode = "exists";
				returnObject.ContactID = result.ContactID;
	    	} else {
		    	returnObject.Status = "OK";		
		    	returnObject.Msg = '(' + strikeIronCode + ')';
				returnObject.ErrorCode = "";
				returnObject.ContactID = result.ContactID;	
	    	}
	    }  
		}
	    return returnObject;
	},

	//**************************************************************************

	getData : function(_sfServiceName, _url, _token, _q) : Result {
		var service = ServiceRegistry.get(_sfServiceName);	// reset the service to remove oauth service params
		var query : String = Encoding.toURI(_q);
		service.URL = _url + "/services/data/v40.0/query?q=" + query;
		service.addHeader("Authorization", "Bearer " + _token);
		var restArgs = {
		 	method: 'GET',
		 	type: 'QUERY',
		 	body: ''
		}
		var result = service.call(restArgs);
		return result;
	},

	//**************************************************************************

	updateContacts : function (_sfServiceName, _url, _token, _responseContacts, _strikeIronCode, _leadDate, _leadSource, _optOutFlag, _gclid, _dwsid, _zipCode, _sitePrefix) : Object {
		
		service = ServiceRegistry.get(_sfServiceName);	// reset the service to remove oauth service params
		service.URL = _url + "/services/data/v40.0/composite/batch";
		service.addHeader("Authorization", "Bearer " + _token);       
		service.addHeader("Content-Type", "application/json");
		
		var restArgs = {
		 	method: 'PATCH',
		 	type: 'BATCH_UPDATE',
		 	body : {
		 		batchRequests : []
		 	}
		};
		
		var batchIndex = 0;
		for (var i in _responseContacts.records) {
			var contactId = _responseContacts.records[i].Id;
			
			var recCalendar : Calendar = dw.system.Site.calendar;
			recCalendar.setTimeZone("UTC");
			try {
				if (_sitePrefix.toUpperCase() == 'MFRM') {
					recCalendar.parseByFormat(_responseContacts.records[i].MFRM_Lead_Date__c,  "yyyy-MM-dd'T'HH:mm:ss");
				} else if (_sitePrefix.toUpperCase() == 'TULO') {
					recCalendar.parseByFormat(_responseContacts.records[i].tulo_Lead_Date__c,  "yyyy-MM-dd'T'HH:mm:ss");
				}
			} catch (e) {
				recCalendar = _leadDate;				
			}

			if (_leadDate.after(recCalendar) || _leadDate.equals(recCalendar) ) {
	    		if (_sitePrefix.toUpperCase() == 'MFRM') {
					var j = { 
							method: 'PATCH',
							url: "v40.0/sobjects/Contact/" + contactId,
					        richInput : {
					        	OtherPostalCode: _zipCode,
					        	MFRM_Lead_Source__c: _leadSource,
					        	MFRM_Lead_Date__c: _leadDate.getTime(),
					        	EmailVerificationCode__c: _strikeIronCode,
					        	HasOptedOutOfEmail: _optOutFlag
					        	//Google_Client_ID__c: _gclid,
					        	//DW_Session_ID__c: _dwsid
					        }
						};
	    		} else if (_sitePrefix.toUpperCase() == 'TULO') {
					var j = { 
							method: 'PATCH',
							url: "v40.0/sobjects/Contact/" + contactId,
					        richInput : {
					        	OtherPostalCode: _zipCode,
					        	tulo_Lead_Source__c: _leadSource,
					        	tulo_Lead_Date__c: _leadDate.getTime(),
					        	EmailVerificationCode__c: _strikeIronCode,
					        	HasOptedOutOfEmail: _optOutFlag
					        	//Google_Client_ID__c: _gclid,
					        	//DW_Session_ID__c: _dwsid
					        }
						};
				}
				restArgs.body.batchRequests[batchIndex++]= j;
				
		    	var webHistoryArgs = {
		    		 	method: 'POST',
						url: "v40.0/sobjects/Contact_Web_History__c/",
						richInput : {
		    		 		Contact__c: contactId,
		    		 		Google_Client_ID__c: _gclid,
		    		 		DW_Session_ID__c: _dwsid
		    		 	}
		    	};
		    	restArgs.body.batchRequests[batchIndex++]= webHistoryArgs;
	    	}

		}
		
		try {
			var newResult = service.call(restArgs);
			// Add ContactID to the result object to pass to SFMC
			newResult = JSON.parse(newResult.object);
			newResult.ContactID = contactId;
		} catch (e) {
			var newResult = {Status: 'ERROR', Msg: e, ErrorCode: '', object: {}};
		}
		return newResult;
	},

	//**************************************************************************
	
	getStrikeIronCode : function(_emailAddress) {
		var service = ServiceRegistry.get("strikeiron.soap");
		var params = {"email": _emailAddress,
					  "userid": Site.current.preferences.custom.siUserID,
					  "password": Site.current.preferences.custom.siPassword,
					  "timeout": Site.current.preferences.custom.siTimeOut
					  };
		
		var soapResult = service.call(params);		
		if (soapResult.status == 'OK') { //if strikeiron service failed then just move on, if not then check verification
			return soapResult.object.Status
		} else {
			return 0;
		}
		
	},
	
	//**************************************************************************

	sendFailSafe : function(obj, errorMsg) : Object {
		var emailAddress = obj.emailAddress;
		var zipCode = obj.zipCode;
		var leadSource = obj.leadSource;
		var optOutFlag = obj.optOutFlag;
		var gclid = obj.gclid;
		var dwsid = obj.dwsid;

		var returnObject : Object = {};
		if (empty(emailAddress)) {
			returnObject.Status = "ERROR";
			return returnObject;
		}
		if (empty(optOutFlag)) {
			optOutFlag = false;
		}
		var yyyyMMdd : String = StringUtils.formatCalendar(new Calendar(),'yyyyMMdd');
		var sysTime : String = StringUtils.formatCalendar(new Calendar(),'yyyyMMdd-HHmmss');
		try{
			var emailSubscriptions : Object = CustomObjectMgr.getCustomObject('SFEmailSubscriptions', yyyyMMdd + '-0');
			var i = 0;
			while(emailSubscriptions !== null && emailSubscriptions.custom.data.length === 200){
				i++;
				emailSubscriptions = CustomObjectMgr.getCustomObject('SFEmailSubscriptions', yyyyMMdd + '-' + i);
			}

			Transaction.wrap(function () {
				if(emailSubscriptions === null){
					emailSubscriptions = CustomObjectMgr.createCustomObject('SFEmailSubscriptions', yyyyMMdd + '-' + i);
				}

				var emailAddresses : Array = new Array();

				for each(var email : String in emailSubscriptions.custom.data) {
					emailAddresses.push(email);
				}
				
				emailAddresses.push(emailAddress + '|' + zipCode + '|' + leadSource + '|' + optOutFlag + '|' + sysTime + '|' + gclid + '|' + dwsid + '|' + errorMsg);

				emailSubscriptions.custom.data = emailAddresses;

				returnObject.Status = 'OK';
			});
		} catch(e){
			Logger.error('sendFailSafe error:' + e);
			returnObject.Status = "ERROR";
			return returnObject;
		}
		return returnObject;
	},
	
	processFailSafe : function () : Object {

		var returnObject : Object = {};

		var sysTime : String = StringUtils.formatCalendar(new Calendar(),'yyyyMMdd-HHmmss');
		try {
			var emailSubscriptions : Object = CustomObjectMgr.getAllCustomObjects('SFEmailSubscriptions');
		} catch (e) {
			returnObject.Status = 'ERROR';
			returnObject.Msg = e;
			return returnObject;
		}
		var cnt = 0;
		
		if (emailSubscriptions !== null) {
	    	while(emailSubscriptions.hasNext()) {
	    		var emailSubscriptionObject = emailSubscriptions.next();	 
	    		var emailAddresses : Array = new Array();
				for each (var edata : String in emailSubscriptionObject.custom.data) {
					cnt = cnt+1;
					
					var dataarr = edata.split('|');
					var recCalendar : Calendar = dw.system.Site.calendar;
					recCalendar.setTimeZone("UTC");
					try {
						recCalendar.parseByFormat(dataarr[4],  "yyyyMMdd-HHmmss");
					} catch (e) {
										
					}
					var emailParams = {
            				emailAddress: dataarr[0], 
            				zipCode: dataarr[1], 
            				leadSource: dataarr[2], 
            				siteId: Site.current.ID, 
            				optOutFlag: dataarr[3],
            				leadDate: recCalendar,
            				gclid: dataarr[5],
            				dwsid: dataarr[6]
            		};
            		
					Transaction.wrap(function () {
						if (cnt <= 2) {
							var result = SFEmailSubscriptionHelper.sendSFEmailInfo(emailParams);
							if (result.Status != 'OK' && result.ErrorCode != 'exists') {
								emailAddresses.push(emailParams.emailAddress + '|' + emailParams.zipCode + '|' + emailParams.leadSource + '|' + emailParams.optOutFlag + '|' + dataarr[4] + '|' + emailParams.gclid + '|' + emailParams.dwsid + '|' + result.ErrorCode + '|' + sysTime);				
							}
						} else {
							emailAddresses.push(edata);
						}
					});
				}
				Transaction.wrap(function () {
					try {
						if (emailAddresses.length == 0) {
							CustomObjectMgr.remove(emailSubscriptionObject);
						} else {
							emailSubscriptionObject.custom.data = emailAddresses;
						}
					} catch (e) {
						Logger.error('sendFailSafe error:' + e);
						returnObject.Status = 'ERROR';
						returnObject.Msg = emailAddresses + e;
						return returnObject;
					}
				});				
			}
		}
		returnObject.Status = 'OK';
		returnObject.Msg = '';
		return returnObject;
	}	
};

exports.SFEmailSubscriptionHelper = SFEmailSubscriptionHelper;
