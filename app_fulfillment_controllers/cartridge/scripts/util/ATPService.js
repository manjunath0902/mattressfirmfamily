'use strict';

/**
 * @module util/ATP Service
 */
var ServiceRegistry = require('dw/svc/ServiceRegistry');
var ATPInventoryServices = require("bc_sleepysc/cartridge/scripts/init/services-ATPInventory");
var Logger = require('dw/system/Logger');

var atpSlots = {
	Message : "Static Slots are being rendered",
	Data : [
            {
                "Location1": "001000",
                "location2": null,
                "Zone": "DZL002093793",
                "ZoneId": "Houston AD",
                "SlotDate": "9/5/2018 12:00:00 AM",
                "StartTime": "08:30 am",
                "EndTime": "09:00 pm",
                "Slots": "144",
                "MFIATPLeadDate": "",
                "Available": "YES",
                "ATPQuantity": "99999"
              },
              {
                "Location1": "001000",
                "location2": null,
                "Zone": "DZL002093732",
                "ZoneId": "Houston SD",
                "SlotDate": "9/5/2018 12:00:00 AM",
                "StartTime": "04:00 pm",
                "EndTime": "10:00 pm",
                "Slots": "0",
                "MFIATPLeadDate": "",
                "Available": "YES",
                "ATPQuantity": "99999"
              },
              {
                "Location1": "001000",
                "location2": null,
                "Zone": "DZL002093854",
                "ZoneId": "Houston MD",
                "SlotDate": "9/5/2018 12:00:00 AM",
                "StartTime": "04:00 pm",
                "EndTime": "10:00 pm",
                "Slots": "0",
                "MFIATPLeadDate": "",
                "Available": "YES",
                "ATPQuantity": "99999"
              },
              {
                "Location1": "001000",
                "location2": null,
                "Zone": "DZL002093794",
                "ZoneId": "Houston AD",
                "SlotDate": "9/6/2018 12:00:00 AM",
                "StartTime": "08:30 am",
                "EndTime": "09:00 pm",
                "Slots": "258",
                "MFIATPLeadDate": "",
                "Available": "YES",
                "ATPQuantity": "99999"
              },
              {
                "Location1": "001000",
                "location2": null,
                "Zone": "DZL002093855",
                "ZoneId": "Houston MD",
                "SlotDate": "9/6/2018 12:00:00 AM",
                "StartTime": "04:00 pm",
                "EndTime": "10:00 pm",
                "Slots": "0",
                "MFIATPLeadDate": "",
                "Available": "YES",
                "ATPQuantity": "99999"
              },
              {
                "Location1": "001000",
                "location2": null,
                "Zone": "DZL002093733",
                "ZoneId": "Houston SD",
                "SlotDate": "9/6/2018 12:00:00 AM",
                "StartTime": "04:00 pm",
                "EndTime": "10:00 pm",
                "Slots": "0",
                "MFIATPLeadDate": "",
                "Available": "YES",
                "ATPQuantity": "99999"
              },
              {
                "Location1": "001000",
                "location2": null,
                "Zone": "DZL002093795",
                "ZoneId": "Houston AD",
                "SlotDate": "9/7/2018 12:00:00 AM",
                "StartTime": "08:30 am",
                "EndTime": "09:00 pm",
                "Slots": "267",
                "MFIATPLeadDate": "",
                "Available": "YES",
                "ATPQuantity": "99999"
              },
              {
                "Location1": "001000",
                "location2": null,
                "Zone": "DZL002093734",
                "ZoneId": "Houston SD",
                "SlotDate": "9/7/2018 12:00:00 AM",
                "StartTime": "04:00 pm",
                "EndTime": "10:00 pm",
                "Slots": "0",
                "MFIATPLeadDate": "",
                "Available": "YES",
                "ATPQuantity": "99999"
              },
              {
                "Location1": "001000",
                "location2": null,
                "Zone": "DZL002093856",
                "ZoneId": "Houston MD",
                "SlotDate": "9/7/2018 12:00:00 AM",
                "StartTime": "04:00 pm",
                "EndTime": "10:00 pm",
                "Slots": "0",
                "MFIATPLeadDate": "",
                "Available": "YES",
                "ATPQuantity": "99999"
              }
            ]
};

/*
 * Input Parameters
 * orderObj				: dw.order object
 * staticSlots			: boolean true if static atp slots are required, false for fetching live ATP slots
 * */
exports.getATPDeliveryDatesForNextWeek = function getATPDeliveryDatesForNextWeek(orderObj, staticSlots) {
	var deliveryDatesArr,
		shippingMethod,
		shipment = orderObj.defaultShipment.shippingAddress;
	
	for each (var oShipment in orderObj.shipments) {
		if (!empty(oShipment.shippingMethodID) && oShipment.productLineItems.length > 0) {
			shippingMethod = oShipment;
		}
	}
	
	if (staticSlots) {
		deliveryDatesArr = getATPDeliveryDates(orderObj, shipment.postalCode, requestedDate, atpSlots);
	} else {
		// If the order is being (re)scheduled locally then the delivery slots should start from (the next day of) Today
		var requestedDate = new Date(); // today
		// If the order was Exported to AX then the delivery slots should start from (the next day of) order Delivery Date
		if (orderObj.exportStatus == dw.order.Order.EXPORT_STATUS_EXPORTED) {
			if(shippingMethod.custom.deliveryDate != null || !empty(shippingMethod.custom.deliveryDate)) {
				requestedDate = new Date(shippingMethod.custom.deliveryDate); // delivery date
			}
		}
		requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
		
		deliveryDatesArr = getATPDeliveryDates(orderObj, shipment.postalCode, requestedDate);
	}
	return deliveryDatesArr;
}

function getATPDeliveryDates(orderObj, postalCode, date, slots) {
	var products = new Array();
	var deliveryDatesArr = new dw.util.ArrayList();
	var deliveryDates = new Object();
	// 'firstAvailableDateObj' will be used as availability date for Warehouse pickup
	var firstAvailableDateObj = {
			availabilityDate	: null,
			availabilityClass	: 'not-available-msg',
			warehouseId			: null,
			isInStock			: false
	}
	var plis = orderObj.productLineItems;
	for (var i = 0; i < plis.length; i++ ) {
		var pli = plis[i];
		if (pli.product.custom.shippingInformation.toLowerCase() == 'core') {
			products.push({
				'ProductId' : pli.productID, //'mfiV000097800',
				'Quantity' : pli.quantityValue //1
			});
		
			var optionLineItems = pli.optionProductLineItems;
			for (var j = 0; j < optionLineItems.length; j++) {
				var oli = optionLineItems[j];
				if (oli.productID != 'none') {
					products.push({
						'ProductId' : oli.productID, //'mfiV000097800',
						'Quantity' : oli.quantityValue //1
					});
				}
			}
		}
	}
	try {
		if (products.length > 0) {
			
			if (arguments.length > 2 && !empty(arguments[2])) {
				var requestedDate = new Date(arguments[2]);
				requestedDate.setDate(requestedDate.getDate() + 1); // Fetch slots from next day of requested date
				requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
			} else {
				var requestedDate = new Date();
				requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
			}
			
			var requestAtpAgain = true,
			requestCount = 0;

			do {
				if (!slots) {	// Real-time ATP slots
					var requestPacket = '{"InventoryType": "Delivery", "ZipCode": "' + postalCode + '","RequestedDate": "' + requestedDate + '", "StoreId": "", "Products": ' + JSON.stringify(products) + '}';
					var svc = ATPInventoryServices.ATPInventoryMonthFP;					
					Logger.error('\nATP URL: ' + svc.getURL() + '\nATP RequestPacket (Delivery): '+requestPacket);

					var serviceRequestTimeSlots = new Date();
					var result = svc.call(requestPacket);
					var serviceResponseTimeSlots = new Date();
					var timeTakenByServiceSlots = (serviceResponseTimeSlots - serviceRequestTimeSlots) / 1000;
					Logger.error('Time taken by ATP slots service to respond (seconds): ' + timeTakenByServiceSlots);

					if (result.error != 0 || result.errorMessage != null || result.mockResult) {
						Logger.error('ATP Delivery (error code): '+result.error+', ATP Delivery (error message): '+result.errorMessage);
					}
				} else {	// Static ATP slots
					var result = new Object();
					result['status'] = 'OK';
				}
				requestCount++;
				if (result.status == 'OK') {
					var jsonResponse;
					if (slots) {	// Static slots
						jsonResponse = slots;
						Logger.error('Displaying Static ATP slots');
					} else {	// Real-time ATP slots
						jsonResponse = JSON.parse(result.object.text);
						Logger.error('\nATP Delivery (response): '+result.object.text);
					}
					var deliveryDateSession = new Object();
					
					var checkForDuplicates = new dw.util.ArrayList();	// key + startTime + endTime
					var checkForDuplicatesKeys = new dw.util.ArrayList();	// key (Date string only, to test if new date is received)
					var slotsAddedCount = 0;	// For tracking the number of ATP time slots added in the array
					var firstAvailableSlotFound = false;	// Available slot is the one with Available = 'Yes', Slots > 0 and ATPQuantity > 0
					if (!slots && !jsonResponse.Status) {	// If Static slots are not enabled and the ATP service did not return Status = true
						//Logger.error('ATP service not available. Message: ' + jsonResponse.Message); // Response is already logged above
						break;
					}
					for (var i = 0; i < jsonResponse.Data.length; i++ ) {
						var response = jsonResponse.Data[i];
						
						var deliveryDate = new Date(response.SlotDate);
						deliveryDate.setHours(10);
						
						var startTime = new Date(deliveryDate.toDateString() + ' ' + response.StartTime);
						var endTime = new Date(deliveryDate.toDateString() + ' ' + response.EndTime);

						// A TimeSlot is not use-able if it does not have Slots and ATPQuantity
						if (Number(response.Slots) <= 0 || Number(response.ATPQuantity) <= 0) {
							response.Available = 'No';
							continue;
						}
						if (!firstAvailableSlotFound && response.Available.toLowerCase() == 'yes') {
							firstAvailableSlotFound = true;
							// Availability date for Warehouse pickup
							firstAvailableDateObj.availabilityDate	= new Date(response.SlotDate);
							firstAvailableDateObj.availabilityClass	= 'in-stock-msg';
							firstAvailableDateObj.warehouseId		= response.Location1;
							firstAvailableDateObj.isInStock			= true;
						}
						
						/*
						 * Check for the 14 days cut off from first slot (i.e. tomorrow)
						 * We do not have to show the slots if no slots were available within first 14 days
						 * */
						var now = new Date(), currentSlotDate = new Date(deliveryDate);
						now.setHours(0,0,0,0);
						currentSlotDate.setHours(0,0,0,0);
						var daysCutOffPassed = ((currentSlotDate-now)/(1000*60*60*24)) > 14 ? true : false;
						if (!firstAvailableSlotFound && daysCutOffPassed) {
							break;
						}
						/*
						 * If the order export status is EXPORTED then take the first 7 slots regardless of the status of slots
						 * If the order is being (re)scheduled locally then find the first available slot and take 7 slots from that point
						 * */
						if ((orderObj.exportStatus == dw.order.Order.EXPORT_STATUS_EXPORTED || firstAvailableSlotFound) && 
								checkForDuplicates.indexOf(key + startTime + endTime) == -1) {
							var key = deliveryDate.toDateString().replace(' ', '', 'g');
							
							if (checkForDuplicatesKeys.indexOf(key) == -1) {
								checkForDuplicatesKeys.add(key);
								slotsAddedCount++;	// new day's slot added in array
								if (slotsAddedCount > 7) {
									break;	// Add up-to 7 days slots only
								}
							}
							
							if ((!deliveryDateSession[key] || !(key in deliveryDateSession)) ) {
								deliveryDateSession[key] = new Object();
								deliveryDateSession[key]['timeslots'] = new dw.util.ArrayList();
								deliveryDatesArr.push(deliveryDate);
							}
							checkForDuplicates.add(key + startTime + endTime);
							if (response.Available.toLowerCase() == 'yes') {
								deliveryDateSession[key]['timeslots'].push({
									'location1' : response.Location1,
									'location2' : response.location2,
									'mfiDSZoneLineId' : response.Zone,
									'mfiDSZipCode' : postalCode,
									'available' : response.Available.toLowerCase(),
									'startTime' : startTime,
									'endTime' : endTime,
									'slots' : Number(response.Slots)
								});
							}
							
							var timeslotsComparator = new dw.util.PropertyComparator ('startTime', 'endTime');
							deliveryDateSession[key]['timeslots'].sort(timeslotsComparator);
							
							if (requestAtpAgain === true && response.Available.toLowerCase() == 'yes' && response.SlotDate !== null) {							
								requestAtpAgain = false;
							}
						}
					}
					
					session.custom.deliveryDates = deliveryDateSession;
					deliveryDates = deliveryDateSession;
					
					/*
					 * If 14 days cut off was passed.
					 * But availability date was not found for Warehouse pickup then iterate in rest of the slots as well.
					 * */
					if (!firstAvailableSlotFound) {
						for (/* Starting from where the upper for loop terminated */; i < jsonResponse.Data.length; i++ ) {
							var responseForPickup = jsonResponse.Data[i];
							if (Number(responseForPickup.Slots) > 0 && Number(responseForPickup.ATPQuantity) > 0 && responseForPickup.Available.toLowerCase() == 'yes') {
								// Availability date for Warehouse pickup
								firstAvailableDateObj.availabilityDate	= new Date(responseForPickup.SlotDate);
								firstAvailableDateObj.availabilityClass	= 'in-stock-msg';
								firstAvailableDateObj.warehouseId		= responseForPickup.Location1;
								firstAvailableDateObj.isInStock			= true;
								break;
							}
						}
					}
				}
				if (requestAtpAgain === true) {
					deliveryDatesArr = new dw.util.ArrayList();
					requestedDate = new Date(requestedDate);
					requestedDate.setDate(requestedDate.getDate() + 7);
					requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
				}
			} while (requestAtpAgain === true && requestCount < 1); 
		}
	} catch (e) {
		Logger.info("ATP Service Exception: " + e.message);
	}
	return {
		'deliveryDatesArr'		: deliveryDatesArr,
		'deliveryDates'			: deliveryDates,
		'firstAvailableDateObj'	: firstAvailableDateObj
	};
}

exports.updateDelivery = function updateDelivery(orderJSON) {
	
	var svc = ATPInventoryServices.UpdateSalesOrderFP;
	
	for (var serviceRequestCount = 1; serviceRequestCount <= 2; serviceRequestCount++) {
		Logger.error('Call#'+serviceRequestCount+', UpdateSalesOrder Service hit: '+ svc.getConfiguration().getCredential().getURL());
		var serviceRequestTime = new Date();
		var result = svc.call(orderJSON);
		var serviceResponseTime = new Date();
		var timeTakenByService = (serviceResponseTime - serviceRequestTime) / 1000;
		Logger.error('Time taken by UpdateSalesOrder service to respond (seconds): ' + timeTakenByService);
		if (result.status == 'OK') {
			Logger.error('UpdateSalesOrder Service Result: '+result.object.text);
			var updateSalesOrderJSONResponse = JSON.parse(result.object.text);
			if (updateSalesOrderJSONResponse.status == 'true') { // status = true
				return true;
			} else {
				return false;
			}
		} else {
			Logger.error('UpdateSalesOrder Service Result (error)# errorCode: '+result.error+', errorMessage: '+result.errorMessage+', status: '+result.status);
			if (result.errorMessage.indexOf('SocketTimeoutException') > -1) {	// result.errorMessage = SocketTimeoutException:Read timed out
				// Give another call to the service (2nd iteration of loop)
				var secondsToDelay = Number(0);
				if (!empty(dw.system.Site.getCurrent().getCustomPreferenceValue('delayInSeconds'))) {
					secondsToDelay = dw.system.Site.getCurrent().getCustomPreferenceValue('delayInSeconds');
				}
				if (serviceRequestCount == 1) {
					Logger.error('Sending 2nd call to UpdateSalesOrder Service due to TimeOut exception occured in 1st call.\nDelay before 2nd call (seconds): '+secondsToDelay);
					// Delay before sending 2nd call to UpdateSalesOrder service
					sleep(secondsToDelay*1000);
				}
			} else {
				return false;
			}
		}
	}
	return false;
}

function sleep(milliseconds) {
	var start = new Date().getTime();
	for (var i = 0; i < 1e7; i++) {
		if ((new Date().getTime() - start) > milliseconds) {
			break;
		}
	}
}
