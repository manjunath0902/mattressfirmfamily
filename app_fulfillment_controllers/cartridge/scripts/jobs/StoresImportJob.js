'use strict';
/**
 * Import Stores from AX by running their service (GetLocationsByMarketId).
 * And update the Custom Object (fulfillment_storeid) in the BM with the data received.
 */

var ServiceRegistry	= require ('dw/svc/ServiceRegistry');
var CustomObjectMgr	= require ('dw/object/CustomObjectMgr');
var Transaction		= require ('dw/system/Transaction');
var Logger			= require ('dw/system/Logger');

function updateCustomObject_Fulfillment_StoreId(responseObj){
	// Update the Custom Object with the data received
	if (!empty(responseObj && responseObj.length)) {
	    for (var i=0; i<responseObj.length; i++) {
	    	Transaction.wrap(function () {
	    		try {
		        	var existingCustomObj = CustomObjectMgr.getCustomObject('fulfillment_storeid', responseObj[i].Location);
		        	if (empty(existingCustomObj)) {
			    		var newFulfillment_StoreId =  CustomObjectMgr.createCustomObject('fulfillment_storeid', responseObj[i].Location);
			    		if (!empty(newFulfillment_StoreId)) {
			    			newFulfillment_StoreId.custom.priceGroup = responseObj[i].PriceGroup;
				    	}
			    	} else {
			    		// Update PriceBook ID of existing Store
			    		existingCustomObj.custom.priceGroup = responseObj[i].PriceGroup;
			    	}
	    		} catch (e) {
	    			var msg = e;
	    			Logger.error('Error occured while updating custom object: ' + msg);
	    		}
		    });
	    }
	}
}

function importStoresService(){
	try{
		var service = ServiceRegistry.get("fulfillment.getlocationsbymarketid.rest");
		//service.setURL('https://sapedgdmz.mfrm.com:7445/api/v1/GetLocationsByMarketId?marketId=072&pg=TP');
		
		var result = service.call();
		if (!(result.error != 0 || result.errorMessage != null || result.mockResult)) { // Success case
			responseObj = JSON.parse(result.object);
			Logger.error('Response received from service (fulfillment.getlocationsbymarketid.rest) was incorrect - Service result: ' + result.object);
			if (!empty(responseObj)) {
				updateCustomObject_Fulfillment_StoreId(responseObj);
			} else {
				Logger.error('Response received from service (fulfillment.getlocationsbymarketid.rest) was incorrect - Service result: ' + result);
			}
		} else { // Error case
		    Logger.error('Error occured in service (fulfillment.getlocationsbymarketid.rest) - Service result: ' + result);
		}
	}
	catch (e) {
		var msg = e;
	    Logger.error('Error executing the Job (ImportStores) - Service result: ' + result);
	}
}
exports.ImportStores = importStoresService;
