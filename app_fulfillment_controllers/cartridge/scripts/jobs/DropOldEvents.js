'use strict';

/**
 * Check for the Events in the Business Manager and drops/deletes the Events which have all the Orders in Submitted and Exported state.
 * And if the submission date of last submitted order is 'noOfDays' (input parameter received from Job in BM) older then delete that specific order.
 */

var CustomObjectMgr	= require ('dw/object/CustomObjectMgr');
var OrderMgr		= require ('dw/order/OrderMgr');
var CustomerMgr		= require ('dw/customer/CustomerMgr');
var Transaction		= require ('dw/system/Transaction');
var Logger			= require ('dw/system/Logger');

/*
 * @params
 * 	input:
 * 		noOfDays: Number no. of days to compare with Submission date of last Order in Event.
 */
function dropOldEvents(jobParams) {
	var noOfDays : Number = !empty(jobParams.noOfDays) && !isNaN(new Number(jobParams.noOfDays))? new Number(jobParams.noOfDays) : new Number(120);
	// Get all Events
	var allEvents : SeekableIterator = CustomObjectMgr.getAllCustomObjects('fulfillment_events');

	while(allEvents.hasNext()) {
		var event = allEvents.next();
		var queryOrders = 'custom.eventId={0}';
		var ordersIter : dw.util.SeekableIterator = OrderMgr.queryOrders(queryOrders, 'creationDate desc', event.custom.ID);
		if (!ordersIter.hasNext()) { // If this Event was empty (Does not contains any orders)
			var dayDifferenceE = 0;
			if (!empty(event.creationDate)) {
				dayDifferenceE = getDayDifferenceFromToday(new Date(event.creationDate));
			}
			// Delete the Event, if it does not have any Orders and the event was created 'noOfDays' ago
			if (dayDifferenceE >= noOfDays) {
				Logger.info(noOfDays+' or more days old Event deleted because No orders were present in it. ' + getEventDetails(event));
				Transaction.wrap(function(){
					CustomObjectMgr.remove(event);
				});
			}
		} else {
			// Check if all the Orders of this Event are submitted
			var allOrdersSubmitted_LastSubmissionDate = getLastSubmissionDate(ordersIter);
			var lastSubmissionDate = null;
			// If all the orders were submitted
			if (allOrdersSubmitted_LastSubmissionDate.allOrdersSubmitted) {
				lastSubmissionDate = new Date(allOrdersSubmitted_LastSubmissionDate.lastSubmissionDate); // Submission date of last submitted order
				var dayDifferenceO = 0;
				if (!empty(lastSubmissionDate)) {
					dayDifferenceO = getDayDifferenceFromToday(lastSubmissionDate);
				}
				
				// Delete the Event, if all of it's Orders are submitted and the last order submitted was older than 'noOfDays'
				if (dayDifferenceO >= noOfDays) {
					Logger.error('Old Event deleted because all of its orders were submitted '+noOfDays+' or more days ago. ' + getEventDetails(event));
					Transaction.wrap(function(){
						CustomObjectMgr.remove(event);
					});
				}
			}
		}
	}
}

/*
 * Check if all the Orders of this Event are submitted. And also return the last Submission date of Order
 */
function getLastSubmissionDate(ordersIter) {
	var allOrdersSubmitted = true;
	var lastSubmissionDate = new Date('01 Jan 1970'); // some date in past
	while(ordersIter.hasNext()) {
		var order = ordersIter.next();
		if (order.confirmationStatus == dw.order.Order.CONFIRMATION_STATUS_CONFIRMED && 
			order.exportStatus == dw.order.Order.EXPORT_STATUS_EXPORTED && 
			!empty(order.custom.submissionDate)) {
			var orderSubmissionDate = new Date(order.custom.submissionDate);
			if (lastSubmissionDate.getTime() < orderSubmissionDate.getTime()) {
				lastSubmissionDate = orderSubmissionDate; // get the latest submission date of order
			}
		} else {
			allOrdersSubmitted = false;
			break;
		}
	}
	return {
		allOrdersSubmitted	: allOrdersSubmitted,
		lastSubmissionDate	: lastSubmissionDate.getTime() == new Date('01 Jan 1970').getTime() ? null : lastSubmissionDate
	}
}

/*
 * Get the no of days between the date today and the date received in input parameter
 */
function getDayDifferenceFromToday(lastSubmissionDate) {
	lastSubmissionDate.setHours(0,0,0,0);
	var now = new Date();
	now.setHours(0,0,0,0);
	var dayDifference : Number = Math.floor((now - lastSubmissionDate) / (1000*60*60*24));
	return dayDifference;
}

function getEventDetails(event) {
	var eventDetails = '\nEvent Details#';
	eventDetails += '\n	Event Name: '+event.custom.eventName;
	eventDetails += '\n	Event Date: '+event.custom.eventDate;
	eventDetails += '\n	Event Creation Date: '+event.creationDate;
/*	var cust = CustomerMgr.getCustomerByCustomerNumber(event.custom.userId);
	if (!empty(cust)) {
		eventDetails += '\nEvent Owner: '+cust.profile.firstName+' '+cust.profile.lastName+' ('+cust.profile.email+')';
	}*/
	
	return eventDetails;
}

exports.DropOldEvents = dropOldEvents;