'use strict';

/**
 * A hello world controller.
 *
 * @module controllers/Hello
 */
importPackage(dw.system);  
importPackage(dw.io);  
importPackage(dw.util);  
importPackage(dw.order);  
importPackage(dw.net);  
importPackage(dw.value);  
importPackage(dw.customer);  

var usaEpayService = require("~/cartridge/scripts/usaEpayService");


exports.Handle = function(args) {
	
	var usaEpayHelper 	= require("~/cartridge/scripts/usaEpayHelper");
	var result 			= usaEpayHelper.Handle(args);
	return result;
}

exports.Authtorize = function(args){
	return usaEpayService.AuthorizePayment(false,'Authtorize', args);
};

exports.Capture = function(args){
	return usaEpayService.AuthorizePayment(false,'Capture', args);
};

exports.Void = function(args){
	return usaEpayService.AuthorizePayment(false,'Void', args);
};
exports.Refund = function(args){

	var args = {
            "refnum": "136245836", // refnum from Authorize call response
            "amount": "5.00"
        };
	
	response.getWriter().println(usaEpayService.AuthorizePayment(false,'Refund', args));
};

//A refund should be used once the transaction you are refunding has settled
exports.Receipt = function(args){

	var args = {
            "refnum": "136245836", // refnum from Authorize call response
            "toemail" : "zeeshan.ahmad@visionet.com"
        };
	
	response.getWriter().println(usaEpayService.AuthorizePayment(false,'Receipt', args));
};

function removeExistingPaymentInstruments(cart) {
	
	if (cart!=null) {
		var ccPaymentInstrs : dw.util.Collection = cart.getPaymentInstruments();
		if (!empty(ccPaymentInstrs) && ccPaymentInstrs.length>0) {
			var iter : dw.util.Iterator = ccPaymentInstrs.iterator();
			var existingPI : dw.order.OrderPaymentInstrument = null;
			while( iter.hasNext() )
			{
				existingPI = iter.next();
				if (existingPI.paymentMethod!=null && PaymentMgr.getPaymentMethod(existingPI.paymentMethod).paymentProcessor.ID.equals('USAePay')) {
					cart.removePaymentInstrument( existingPI );
				}
			}
		}
	}
}


exports.Authtorize.public = true;
exports.Capture.public = true;
exports.Refund.public = true;
exports.Receipt.public = true;
exports.Handle.public = true;
exports.Void.public = true;