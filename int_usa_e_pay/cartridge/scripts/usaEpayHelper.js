/* API Includes */
var app = require('app_fulfillment_controllers/cartridge/scripts/app'),
	Order = app.getModel('Order'),
	Site = require('dw/system/Site'),
	Transaction = require('dw/system/Transaction'),
	PaymentMgr = require('dw/order/PaymentMgr'),
	uasepayLogger = require('~/cartridge/scripts/utils/usaepayLogger');

function Handle(args) {
	var cart = app.getModel('Cart').get(args.Order),
		logLocation = 'usaEpayHelper~Handle';
	
    var paymentMethod :String = 'CREDIT_CARD'; // This needs to be dynamic
	try {
		Transaction.wrap(function () {
			removeExistingPaymentInstruments(cart);
		});
	}catch(e) {
		uasepayLogger.logMessage('Error occured: ' + e, 'error', logLocation);
	}
    if(!empty(paymentMethod) && paymentMethod.equals(dw.order.PaymentInstrument.METHOD_CREDIT_CARD)){
    	return	HandleCreditCard(cart);
	}
}


/**
 * Handle credit card
 */
function HandleCreditCard(cart) {

	var logLocation = 'usaEpayHelper~HandleCreditCard';

//	 var tokenId :String="";
//
//	if(dw.system.Site.getCurrent().getCustomPreferenceValue("WorldPayEnableTokenization") && customer.authenticated){		
//		 var wallet = customer.getProfile().getWallet();
//		
//    	 var paymentInstruments = wallet.getPaymentInstruments(dw.order.PaymentInstrument.METHOD_CREDIT_CARD);
//    	 var matchedPaymentInstrument= require("int_worldpay/cartridge/scripts/common/PaymentInstrumentUtils").getTokenPaymentInstrument(paymentInstruments,session.forms.billing.paymentMethods.creditCard.number.value,session.forms.billing.paymentMethods.creditCard.type.value);
//	     	if(!empty(matchedPaymentInstrument) && !empty(matchedPaymentInstrument.getCreditCardToken())){
//	     		tokenId = matchedPaymentInstrument.getCreditCardToken();
//	     	}
//	
//	}
//	if(empty(tokenId)){
	
//	dwfrm_paymentfulfillment_creditCard_owner=_PROVIDED_
//	dwfrm_paymentfulfillment_creditCard_number=_PROVIDED_
//	dwfrm_paymentfulfillment_creditCard_expiration_month=_PROVIDED_
//	dwfrm_paymentfulfillment_creditCard_expiration_year=_PROVIDED_
//	dwfrm_paymentfulfillment_creditCard_cvn=_PROVIDED_
//	dwfrm_paymentfulfillment_address_address1=la
//	dwfrm_paymentfulfillment_address_suite=sdf
//	dwfrm_paymentfulfillment_address_city=asdfsdf
//	dwfrm_paymentfulfillment_address_states_state=AZ
//	dwfrm_paymentfulfillment_address_postal=85714
//	dwfrm_paymentfulfillment_phone=5209017104
//	dwfrm_paymentfulfillment_save=
//	dwfrm_paymentfulfillment_securekey=509259098
	
		// Card type needs to be dynamic
		var paymentCard = PaymentMgr.getPaymentCard('Visa'); //session.forms.paymentfulfillment.creditCard.type.value
		var CreditCardStatus = paymentCard.verify(session.forms.paymentfulfillment.creditCard.expiration.month.value, 
				session.forms.paymentfulfillment.creditCard.expiration.year.value, session.forms.paymentfulfillment.creditCard.number.value,
				session.forms.paymentfulfillment.creditCard.cvn.value);
	
		var creditCardForm = session.forms.paymentfulfillment.creditCard;
		if( CreditCardStatus == null || !creditCardForm.valid) {
			return {error : true};
		}
		if(CreditCardStatus.status !== dw.system.Status.OK )
		{
			// invalidate the payment card form elements
			var items : Iterator = CreditCardStatus.items.iterator();
			while( items.hasNext() )
			{
				var item : dw.system.StatusItem = items.next();
				
				switch( item.code )
				{
					case dw.order.PaymentStatusCodes.CREDITCARD_INVALID_CARD_NUMBER:
						creditCardForm.number.setValue(creditCardForm.number.htmlValue);
						creditCardForm.number.invalidateFormElement();
						creditCardForm.encryptedData.setValue("");
						continue;
		
					case dw.order.PaymentStatusCodes.CREDITCARD_INVALID_EXPIRATION_DATE:
						creditCardForm.expiration.month.invalidateFormElement();
						creditCardForm.expiration.year.invalidateFormElement();
						creditCardForm.encryptedData.setValue("");
						continue;
		
					case dw.order.PaymentStatusCodes.CREDITCARD_INVALID_SECURITY_CODE:
						creditCardForm.cvn.invalidateFormElement();
						creditCardForm.encryptedData.setValue("");
				}
			}
			return {error : true};
		}	
//   }
		try {
			Transaction.wrap(function () {
				removeExistingPaymentInstruments(cart);
				var paymentInstrument = cart.createPaymentInstrument(dw.order.PaymentInstrument.METHOD_CREDIT_CARD, cart.getNonGiftCertificateAmount());
		
				paymentInstrument.creditCardHolder = session.forms.paymentfulfillment.creditCard.owner.value;
				paymentInstrument.creditCardNumber = session.forms.paymentfulfillment.creditCard.number.value;
				paymentInstrument.creditCardType = session.forms.paymentfulfillment.creditCard.type.value;
				paymentInstrument.creditCardExpirationMonth = session.forms.paymentfulfillment.creditCard.expiration.month.value;
				paymentInstrument.creditCardExpirationYear = session.forms.paymentfulfillment.creditCard.expiration.year.value;
				//paymentInstrument.custom.WorldPayMID = dw.system.Site.getCurrent().getCustomPreferenceValue('WorldPayMerchantCode');
	//			if(!empty(tokenId)){
	//				paymentInstrument.creditCardToken= tokenId;
	//			} else if(session.forms.billing.paymentMethods.creditCard.saveCard.value && dw.system.Site.getCurrent().getCustomPreferenceValue("WorldPayEnableTokenization")){
	//				paymentInstrument.custom.wpTokenRequested =true;
	//			}
			});
			return {success : true};
		}catch(e) {
			uasepayLogger.logMessage('Error occured: ' + e, 'error', logLocation);
			return {error : true};
		}
}

function removeExistingPaymentInstruments(cart) {
	
	if (cart!=null) {
		var ccPaymentInstrs : dw.util.Collection = cart.getPaymentInstruments();
		if ( !empty(ccPaymentInstrs) && ccPaymentInstrs.length > 0 ) {
			var iter : dw.util.Iterator = ccPaymentInstrs.iterator();
			var existingPI : dw.order.OrderPaymentInstrument = null;
			while( iter.hasNext() )
			{
				existingPI = iter.next();
				if (existingPI.paymentMethod!=null && PaymentMgr.getPaymentMethod(existingPI.paymentMethod).paymentProcessor.ID.equals('USAePay')) {
					cart.removePaymentInstrument( existingPI );
				}
			}
		}
	}
}
exports.Handle = Handle;