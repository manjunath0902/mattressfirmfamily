importPackage( dw.system );
importPackage( dw.svc );
importPackage( dw.net );
importPackage( dw.io );
importPackage( dw.crypto );

var usaEpayUtil = function usaEpayUtil(){

	var getbaseUrl = function () {
		 switch(dw.system.System.getInstanceType())
		 {
		 	case dw.system.System.PRODUCTION_SYSTEM:
		 		return Site.getCurrent().preferences.custom.usaePayProductionAPIURL;
		 		break;
		 	default:
		 		return Site.getCurrent().preferences.custom.usaePayStagingAPIURL;		
		 		break;
		 }
	};
	var getEndpoint = function (commandName) {
		 switch(commandName)
		 {
		 	case "Authorize":
		 	case "Capture":
		 	case "Refund":
		 	case "Receipt":
		 	case "Void":
		 		return "transactions";
		 		break;
		 	default:
		 		return "";
		 		break;
		 }
	};
	var hashedString= function(){		
		
		var seed = Site.getCurrent().preferences.custom.usaePaySeed;		
		var apikey = Site.getCurrent().preferences.custom.usaePayAPIKey;		
		var apipin = Site.getCurrent().preferences.custom.usaePayAPIPin;
		var prehash = apikey + seed + apipin;
		var sha : MessageDigest = new MessageDigest(MessageDigest.DIGEST_SHA_256);
		var posthash : Bytes = sha.digest(prehash);
		var apihash = apikey + ":" + 's2/'+ seed + '/' + posthash;
		var authKey = new dw.util.Bytes(apihash);
		var finalkey =  Encoding.toBase64(authKey);
		return finalkey;
	
	};
	var hashedMapString= function(){		
		
		var seed = Site.getCurrent().preferences.custom.usaePaySeed;		
		var apikey = Site.getCurrent().preferences.custom.usaePayMapAPIKey;		
		var apipin = Site.getCurrent().preferences.custom.usaePayMapAPIPin;
		var prehash = apikey + seed + apipin;
		var sha : MessageDigest = new MessageDigest(MessageDigest.DIGEST_SHA_256);
		var posthash : Bytes = sha.digest(prehash);
		var apihash = apikey + ":" + 's2/'+ seed + '/' + posthash;
		var authKey = new dw.util.Bytes(apihash);
		var finalkey =  Encoding.toBase64(authKey);
		return finalkey;
	
	};	
	var buildAuthRequest = function(orderInfo,paymentInstrumentInfo){
		var Order = orderInfo;
		var paymentInstrument = paymentInstrumentInfo;
		var billingAddress = Order.getBillingAddress();		
		var shippingAddress = Order.getDefaultShipment().getShippingAddress();
		
		var authRequest = new Object();
		
		if(!empty(Order)) {
			authRequest.command = "cc:sale";
			authRequest.amount = Order.getTotalGrossPrice().value;		
			authRequest.orderid = Order.orderNo;
			authRequest.invoice = Order.invoiceNo;
			//authRequest.email =  Order.customerEmail;
			authRequest.description = Order.customerEmail;
		
			authRequest.amount_detail = new Object();
			authRequest.amount_detail.tax = Order.getTotalTax().value;
			authRequest.amount_detail.tip = 0;
		}		
		
		if(!empty(paymentInstrument) && !empty(billingAddress)) {
			authRequest.creditcard = new Object();
			authRequest.creditcard.cardholder = paymentInstrument.creditCardHolder;
			authRequest.creditcard.number = paymentInstrument.creditCardNumber;
			authRequest.creditcard.expiration = paymentInstrument.creditCardExpirationMonth.toString()+paymentInstrument.creditCardExpirationYear.toString().substring(2);
			authRequest.creditcard.cvc = session.forms.billing.paymentMethods.creditCard.cvn.value;
			authRequest.creditcard.avs_street = billingAddress.address1;
			authRequest.creditcard.avs_zip = billingAddress.postalCode;
		}				
		
		if(!empty(billingAddress)) {
			authRequest.billing_address = new Object();
			authRequest.billing_address.company = billingAddress.companyName;		
			authRequest.billing_address.firstname = billingAddress.firstName;	
			authRequest.billing_address.lastname = billingAddress.lastName;
			authRequest.billing_address.street = billingAddress.address1;		
			authRequest.billing_address.street2 = billingAddress.address2;		
			authRequest.billing_address.city = billingAddress.city;		
			authRequest.billing_address.state = billingAddress.stateCode;		
			authRequest.billing_address.postalcode = billingAddress.postalCode;	
			authRequest.billing_address.country = billingAddress.countryCode.value;		
			authRequest.billing_address.phone = billingAddress.phone;		
			authRequest.billing_address.fax = "";
		}		
		
		if(!empty(shippingAddress)) {
			authRequest.shipping_address = new Object();
			authRequest.shipping_address.company = shippingAddress.companyName;	
			authRequest.shipping_address.firstname = shippingAddress.firstName;	
			authRequest.shipping_address.lastname = shippingAddress.lastName;	
			authRequest.shipping_address.street = shippingAddress.address1;		
			authRequest.shipping_address.street2 = shippingAddress.address2;	
			authRequest.shipping_address.city = shippingAddress.city;		
			authRequest.shipping_address.state = shippingAddress.stateCode;		
			authRequest.shipping_address.postalcode = shippingAddress.postalCode;	
			authRequest.shipping_address.country = shippingAddress.countryCode.value;	
			authRequest.shipping_address.phone = shippingAddress.phone;		
			authRequest.shipping_address.fax = "";
		}		
		return authRequest;	
	};
	
	return {
        baseUrl: getbaseUrl,
        endPoint: getEndpoint,
        hashedString: hashedString,
        hashedMapString: hashedMapString,
        buildAuthRequest : buildAuthRequest
    };
};
module.exports.usaEpayUtil = usaEpayUtil;