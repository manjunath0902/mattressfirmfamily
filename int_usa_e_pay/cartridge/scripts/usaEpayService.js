
importPackage( dw.system );
importPackage( dw.svc );
importPackage( dw.net );
importPackage( dw.io );
importPackage( dw.util );
var eServiceUtil  = require("~/cartridge/scripts/usaEpayServiceUtil").usaEpayUtil();

function authorizePayment(mockCall, methodName, args) {
	
	var service:Service,
	result:Result,
	returnCode="",
	result = null,
	uasepayLogger = require('~/cartridge/scripts/utils/usaepayLogger'),
	orderMgr = require('dw/order/OrderMgr');
	//eServiceUtil : Object;
	
	//eServiceUtil  = require("~/cartridge/scripts/usaEpayServiceUtil").usaEpayUtil(); 
	service = ServiceRegistry.get("int_usa_e_pay.http.post");
	
	switch ( methodName ) {
    // make a POST request with additional headers, query param, json payload
		case "Authtorize":
			try{
				var PaymentMgr = require('dw/order/PaymentMgr'),
					logLocation = 'usaEpayService~Authtorize';
				var Order = orderMgr.getOrder(args.orderNo);
				var	paymentInstrument = Order.paymentInstrument;
				var	paymentProcessor = PaymentMgr.getPaymentMethod(paymentInstrument.getPaymentMethod()).getPaymentProcessor();				
				service.URL = eServiceUtil.baseUrl()+ eServiceUtil.endPoint("Authorize");
				var requestPayload =  eServiceUtil.buildAuthRequest(Order,paymentInstrument);
				if ( mockCall ) {
					result = service.setMock().call();
				}else {
					result = service.call(requestPayload);
				}
				if(result.status =='OK' && JSON.parse(result.object).error_code == null) {
					//uasepayLogger.logMessage('Authorized successfully.', 'debug', logLocation);
					Transaction.wrap(function () {
						paymentInstrument.paymentTransaction.transactionID = Order.orderNo;
						paymentInstrument.paymentTransaction.paymentProcessor = paymentProcessor;
						var requestResponse = JSON.parse(result.object);
						
						paymentInstrument.paymentTransaction.custom.authAmount = requestResponse.auth_amount;
                    	paymentInstrument.paymentTransaction.custom.approvalStatus = requestResponse.result; 
                    	paymentInstrument.paymentTransaction.custom.authCode = requestResponse.authcode;                    	
                    	var ccCode : HashMap = new HashMap();
        				ccCode.put('Visa', 'VI');
        				ccCode.put('Master', 'MC');
        				ccCode.put('MasterCard', 'MC');
        				ccCode.put('Mastercard', 'MC');
        				ccCode.put('Discover', 'DS');
        				ccCode.put('Amex', 'AX');                    	
                    	paymentInstrument.paymentTransaction.custom.cardType = ccCode.get(paymentInstrument.getCreditCardType());
                    	paymentInstrument.paymentTransaction.custom.requestId = requestResponse.refnum;
                    	paymentInstrument.paymentTransaction.custom.requestToken = requestResponse.key;
                    	paymentInstrument.paymentTransaction.custom.subscriptionID = requestResponse.refnum;
					});
					return result.object;
				}else if( result.status =='OK' && ( !empty(JSON.parse(result.object).error_code) || JSON.parse(result.object).error_code != null ) ) {
					uasepayLogger.logMessage('Error Code is: ' + JSON.parse(result.object).error_code + '\n Error Text is: ' + JSON.parse(result.object).error, 'error', logLocation);
					return JSON.parse(result.object).error;
				}else if(result.status == "ERROR") {					
					return result;
				}
			}catch(e) {
				uasepayLogger.logMessage('Error occured while trying to authorize - ' + e.message, 'error', logLocation);
				return e.message;
			}
		break;
		case "Capture":
			try{
				var Order = orderMgr.getOrder(args.orderNo);
				var	paymentInstrument = Order.paymentInstrument;
				service.URL = eServiceUtil.baseUrl()+ eServiceUtil.endPoint("Authorize");
				var requestPayload =  {
					"command": "cc:capture",
					"refnum": paymentInstrument.paymentTransaction.custom.requestId,//Order.custom.usaepayrefnum,
					"amount": Order.getTotalGrossPrice().value
				};	        	 
				if ( mockCall ) {
					result = service.setMock().call();
				} else {
					result = service.call(requestPayload);
				}
				if(result.status =='OK' && JSON.parse(result.object).error_code == null) {
					//uasepayLogger.logMessage('Caputred successfully.', 'debug', logLocation);
					Transaction.wrap(function () {
						Order.setPaymentStatus(dw.order.Order.PAYMENT_STATUS_PAID);
					});
					return result.object;
				}else if( result.status =='OK' && ( !empty(JSON.parse(result.object).error_code) || JSON.parse(result.object).error_code != null ) ) {
					//uasepayLogger.logMessage('Error Code is: ' + JSON.parse(result.object).error_code + '\n Error Text is: ' + JSON.parse(result.object).error, 'error', logLocation);
					return JSON.parse(result.object).error;
				}else if(result.status == "ERROR") {
					return result;
				}
			}catch(e){
				uasepayLogger.logMessage('Error occured while trying to capture - ' + e.message, 'error', logLocation);
				return e.message;
			}
		break;
			case "Refund":
			try{
				service.URL = eServiceUtil.baseUrl()+ eServiceUtil.endPoint("Authorize");
				var requestPayload =  {
					"command": "cc:refund",
					"refnum": args.refnum,
					"amount": args.amount
				};	        	 
				if ( mockCall ) {
					result = service.setMock().call();
				}else{
					result = service.call(requestPayload);
				}
				if(result.status =='OK' && JSON.parse(result.object).error_code == null) {
					uasepayLogger.logMessage('Refund requested successfully.', 'debug', logLocation);
					return result.object;
				}else if( result.status =='OK' && ( !empty(JSON.parse(result.object).error_code) || JSON.parse(result.object).error_code != null ) ) {
					uasepayLogger.logMessage('Error Code is: ' + JSON.parse(result.object).error_code + '\n Error Text is: ' + JSON.parse(result.object).error, 'error', logLocation);
					return JSON.parse(result.object).error;
				}
			}catch(e){
				uasepayLogger.logMessage('Error occured while trying to refund - ' + e.message, 'error', logLocation);
				return e.message;
			}
		break;
		case "Receipt":
			try{
				service.URL = eServiceUtil.baseUrl()+ eServiceUtil.endPoint("Authorize")+"/"+args.refnum+"/receipts/";
				var requestPayload;
				if( empty(args.toemail) || args.toemail == null ) {
					service.setRequestMethod('GET');
				} else {
					service.URL = eServiceUtil.baseUrl()+ eServiceUtil.endPoint("Authorize")+"/"+args.refnum+"/receipts/email";
					requestPayload =  {
						"toemail": args.toemail,
						"save_email": 1
					};
				}
				if ( mockCall ) {
					result = service.setMock().call();
				}else{
					result = service.call(requestPayload);
				}
				if(result.status =='OK' && JSON.parse(result.object).error_code == null) {
					uasepayLogger.logMessage('Receipt generated successfully.', 'debug', logLocation);
					return JSON.parse(result.object);
				}else if( result.status =='OK' && ( !empty(JSON.parse(result.object).error_code) || JSON.parse(result.object).error_code != null ) ) {
					uasepayLogger.logMessage('Error Code is: ' + JSON.parse(result.object).error_code + '\n Error Text is: ' + JSON.parse(result.object).error, 'error', logLocation);
					return JSON.parse(result.object).error;
				}
			}catch(e){
				uasepayLogger.logMessage('Error occured while trying to refund - ' + e.message, 'error', logLocation);
				return e.message;
			}         
		break;
		case "Void":
			try{
				service.URL = eServiceUtil.baseUrl()+ eServiceUtil.endPoint("Authorize");
				var requestPayload =  {
					"command": "creditvoid",
					"refnum" : args.refnum,
					"amount" : args.amount
				};	        	 
				if ( mockCall ) {
					result = service.setMock().call();
				}else{
					result = service.call(requestPayload);
				}
				if(result.status =='OK' && JSON.parse(result.object).error_code == null) {
					uasepayLogger.logMessage('Refund requested successfully.', 'debug', logLocation);
					return JSON.parse(result.object); 
				}else if( result.status =='OK' && ( !empty(JSON.parse(result.object).error_code) || JSON.parse(result.object).error_code != null ) ) {
					uasepayLogger.logMessage('Error Code is: ' + JSON.parse(result.object).error_code + '\n Error Text is: ' + JSON.parse(result.object).error, 'error', logLocation);
					return JSON.parse(result.object).error;
				}
			}catch(e){
				uasepayLogger.logMessage('Error occured while trying to refund - ' + e.message, 'error', logLocation);
				return e.message;
			}
		default:
			return "argument not valid";
		break;
	}
}

function authorizeMakePayment(mockCall, methodName, args) {
	
	var service:Service,
	result:Result,
	returnCode="",
	result = null,
	uasepayLogger = require('~/cartridge/scripts/utils/usaepayLogger');
	//eServiceUtil : Object;
	
	//eServiceUtil  = require("~/cartridge/scripts/usaEpayServiceUtil").usaEpayUtil(); 
	service = usaEpayMapService;//ServiceRegistry.get("int_usa_e_pay.http.post");
	var voidType = ('usaePayVoidType' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.usaePayVoidType) ? dw.system.Site.getCurrent().getCustomPreferenceValue('usaePayVoidType').value : 'cc:void:release';
	switch ( methodName ) {
    // make a POST request with additional headers, query param, json payload
		case "Authtorize":
			try{
				var logLocation = 'usaEpayService~Authtorize';
				var Order = args.order;
				var	paymentInstrument = Order.paymentInstrument;
				//var	paymentProcessor = PaymentMgr.getPaymentMethod(paymentInstrument.getPaymentMethod()).getPaymentProcessor();
				var billingAddress = Order.billingAddress;
				var completeAddress = billingAddress.address1+" "+billingAddress.address2+" "+billingAddress.city+", "+billingAddress.stateCode+". "+billingAddress.countryCode;
				
				//MAT-1541 - Pay partially if partial payment feature enabled
				var isPartialPaymentsEnabled = ('enablePartialPayments' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.enablePartialPayments) ? true : false;
				var params = request.httpParameterMap;
				var amountToPay;
				if(isPartialPaymentsEnabled){
		    		if(params.PaymentOptions.submitted && params.PaymentOptions=='OtherAmountOption' && params.dwfrm_billingform_PartialPayment_otherAmount.submitted){
		    			amountToPay= params.dwfrm_billingform_PartialPayment_otherAmount.stringValue;
		    		}
		    		else {
		    			amountToPay = Order.AmountDue;		    			
		    		}
	    		}
				else {
					amountToPay = Order.AmountDue;	
				}
				
				service.URL = eServiceUtil.baseUrl()+ eServiceUtil.endPoint("Authorize");
				var requestPayload =  {
						"command": "cc:sale",
						"amount": amountToPay,
						"amount_detail": {
							"tax": 0,
							"tip": 0
						},
						"creditcard": {
							"cardholder": paymentInstrument.creditCardHolder,
							"number": paymentInstrument.creditCardNumber,
							"expiration": paymentInstrument.creditCardExpirationMonth.toString()+paymentInstrument.creditCardExpirationYear.toString(),
							"cvc": paymentInstrument.cvn,
							"avs_street": completeAddress,
							"avs_zip": billingAddress.zipCode
						},
						"orderid": Order.OrderNo
				};	        	 
				if ( mockCall ) {
					result = service.setMock().call();
				}else {
					var requestString = requestPayload.toString();
					result = service.call(requestPayload);
				}
				if(result.status =='OK' && JSON.parse(result.object).error_code == null) {
					//uasepayLogger.logMessage('Authorized successfully.', 'debug', logLocation);
					return result.object;
				}else if( result.status =='OK' && ( !empty(JSON.parse(result.object).error_code) || JSON.parse(result.object).error_code != null ) ) {
					uasepayLogger.logMessage('Error Code is: ' + JSON.parse(result.object).error_code + '\n Error Text is: ' + JSON.parse(result.object).error, 'error', logLocation);
					return result;
				}else if(result.status == "ERROR") {					
					return result;
				}
			}catch(e) {
				uasepayLogger.logMessage('Error occured while trying to authorize - ' + e.message, 'error', logLocation);
				var errResult = {
    				status: "ERROR",
    				errorMessage : e.message
				}; 
				return errResult;
			}
		break;
		case "Capture":
			try{
				var Order = args.order;
				var	paymentInstrument = Order.paymentInstrument;
				service.URL = eServiceUtil.baseUrl()+ eServiceUtil.endPoint("Authorize");
				//MAT-1541 - Pay partially if partial payment feature enabled
				var isPartialPaymentsEnabled = ('enablePartialPayments' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.enablePartialPayments) ? true : false;
				var params = request.httpParameterMap;
				var amountToPay;
				if(isPartialPaymentsEnabled){
		    		if(params.PaymentOptions.submitted && params.PaymentOptions=='OtherAmountOption' && params.dwfrm_billingform_PartialPayment_otherAmount.submitted){
		    			amountToPay= params.dwfrm_billingform_PartialPayment_otherAmount.stringValue;
		    		}
		    		else {
		    			amountToPay = Order.AmountDue;		    			
		    		}
	    		}
				else {
					amountToPay = Order.AmountDue;	
				}
				
				var requestPayload =  {
					"command": "cc:capture",
					"refnum": args.refnum,//Order.custom.usaepayrefnum,
					//"amount": Order.AmountDue
					"amount": amountToPay
				};	        	 
				if ( mockCall ) {
					result = service.setMock().call();
				} else {
					result = service.call(requestPayload);
				}
				if(result.status =='OK' && JSON.parse(result.object).error_code == null) {
					//uasepayLogger.logMessage('Caputred successfully.', 'debug', logLocation);
					return result.object;
				}else if( result.status =='OK' && ( !empty(JSON.parse(result.object).error_code) || JSON.parse(result.object).error_code != null ) ) {
					//uasepayLogger.logMessage('Error Code is: ' + JSON.parse(result.object).error_code + '\n Error Text is: ' + JSON.parse(result.object).error, 'error', logLocation);
					return result;
				}else if(result.status == "ERROR") {
					return result;
				}
			}catch(e){
				uasepayLogger.logMessage('Error occured while trying to capture - ' + e.message, 'error', logLocation);
				var errResult = {
    				status: "ERROR",
    				errorMessage : e.message
				}; 
				return errResult;
			}
		break;
		case "Void":
			try{
				service.URL = eServiceUtil.baseUrl()+ eServiceUtil.endPoint("Authorize");
				var requestPayload =  {
					"command": args.voidType ? args.voidType : voidType,
					"refnum" : args.refnum,
					"amount" : args.amount
				};	        	 
				if ( mockCall ) {
					result = service.setMock().call();
				}else{
					result = service.call(requestPayload);
				}
				if(result.status =='OK' && JSON.parse(result.object).error_code == null) {
					uasepayLogger.logMessage('Refund requested successfully.', 'debug', logLocation);
					return JSON.parse(result.object); 
				}else if( result.status =='OK' && ( !empty(JSON.parse(result.object).error_code) || JSON.parse(result.object).error_code != null ) ) {
					uasepayLogger.logMessage('Error Code is: ' + JSON.parse(result.object).error_code + '\n Error Text is: ' + JSON.parse(result.object).error, 'error', logLocation);
					return result;
				}else if(result.status == "ERROR") {
					return result;
				}
			}catch(e){
				uasepayLogger.logMessage('Error occured while trying to refund - ' + e.message, 'error', logLocation);
				var errResult = {
    				status: "ERROR",
    				errorMessage : e.message
				}; 
				return errResult;
			}
		default:
			return "argument not valid";
		break;
	}
}


var usaEpayMapService = LocalServiceRegistry.createService("int_usa_e_pay_map.http.post", {
	/**
	* createRequest()
	*	This function used for creating and preparing request.
	*/
	createRequest: function(svc, args){
		// Default request method is post
        // No need to setRequestMethod
        if(args) {
            svc.addHeader('Content-Type', 'application/json; charset=utf-8');
    		svc.addHeader('Accept', '*/*');
    		svc.addHeader('Accept-Encoding', 'gzip,deflate');
    		var APISignature = "Basic "+eServiceUtil.hashedMapString();
         	svc.addHeader("Authorization", APISignature);
            return JSON.stringify(args);
        }
        return null;
	},
	/**
	* parseResponse()
	*	This function used for parsing response.
	*/
	parseResponse: function(svc, client) {
		if (client.statusCode == 200) {
			return client.text;
		}
		else {
			return {
				statusCode		: client.statusCode,
				statusMessage	: client.statusMessage,
				errorText		: client.errorText,
				responseMessage : client.text
			};
		}
	},
	/**
	* filterLogMessage()
	*	This function filters logs messages if required.
	*/
	filterLogMessage: function(msg : String) {
		return msg;
	},
	/**
	* getRequestLogMessage()
	*	This function handles how the request is logged.
	*/
	getRequestLogMessage : function(requestObj : Object) : String {
		try {
			var filteredRequestObj = JSON.parse(requestObj);
			if(filteredRequestObj.command) {
				if(filteredRequestObj.command === 'cc:sale') {
					if(filteredRequestObj['creditcard']) {
						filteredRequestObj['creditcard']['cardholder'] = 'Not To Be Displayed Due to PCI Compliance';
						filteredRequestObj['creditcard']['number'] = 'Not To Be Displayed Due to PCI Compliance';
						filteredRequestObj['creditcard']['expiration'] = 'Not To Be Displayed Due to PCI Compliance';
						filteredRequestObj['creditcard']['cvc'] = 'Not To Be Displayed Due to PCI Compliance';
					}
				}
			}
			if(Object.keys(filteredRequestObj).length > 0) {
				var jsonString = JSON.stringify(filteredRequestObj);
				return jsonString;
			}
		} catch(e) {
			var abc = e;
		}
		return requestObj;
	},
	/**
	* getResponseLogMessage()
		*	This function handles how the response is logged.
	*/
	getResponseLogMessage : function(responseObj : Object) : String {
		if (responseObj instanceof dw.net.HTTPClient) {
			try {
				return responseObj.text;
			} catch(e) {}
		}
		return responseObj;
	},
	/**
	* getURL()
		*	This function overrides service URL for site-specific behaviour.
	*/
	getURL : function() : String {
		return;
	},
	/**
	* mockCall()
	*	This function used for returining mocked response when service is mocked.
	*/
	mockCall: function(svc, requestObj){
		return {
			statusCode: 200,
			statusMessage: "Success",
			text: "MOCK RESPONSE (" + svc.URL + ")"
		};
	},
	/**
	* mockFull()
	*	Override this method to mock the entire service call, including the createRequest, execute, and parseResponse phases.
	*/
	mockFull: function(svc, args){
		return;
	}
});

module.exports.AuthorizePayment = authorizePayment;
module.exports.AuthorizeMakePayment = authorizeMakePayment;