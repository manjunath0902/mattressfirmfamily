# Amplience Cartridges for Salesforce Commerce Cloud #

## What is this repository for? ##

* These cartridges provide an integration for Dynamic Media solutions and Content Authoring (legacy) solutions
* For more information see http://amplience.com/products/salesforce-commerce-cloud/
* Note that the Amplience Dynamic Content dynamic content solution uses a separate integration: https://github.com/SalesforceCommerceCloud/link_ampliencedynamiccontent


### Amplience Dynamic Media ###
* Product imagery served by the Amplience Dynamic Media service easily configurable.
* Automated assignment of Amplience product media to SFCC products catalog.
* Configurable ootb production ready PDP viewer
* Support for 720, 360, video, alternate images.

### Amplience Dynamic Media Import ###
* Provides easy and configurable assignment of Amplience product images to DW products.

### Amplience Content Authoring (deprecated) ###
* Provides means for retrieving structured content authored in Amplience content management system, transformingit into ready-to-render form and storing it in Salesforce Commerce Cloud.
* Content authors are given full control overhow the content is to be rendered.
* The content is indexable by both internal Salesforce Commerce Cloud searchengine and web crawlers.

## How do I get set up? ##

### Prerequisites ###
* Access to a Salesforce Commerce Cloud sandbox
* Access to [ Salesforce Commerce Cloud Exchange ](https://xchange.demandware.com/)

### Salesforce Commerce Cloud API version ###
From release 19.1.2 of this cartridge, compatibility mode 19.10 is required.

If your code version is using an earlier compatibility mode you will need to use release 19.1.1 of this cartridge, which was tested against Compatibility Mode 18.10.

### Cartridge Setup ###

* Please see the [ Setup Instructions ](documentation/AmplienceIntegrationSetupInstructions.pdf)

### Storefront Implementation ###

Please see:

*  [ SFRA Implementation Guide ](documentation/SFRAIntegrationGuide.pdf) and [ cartridges/int_amplience_sfra ](cartridges/int_amplience_sfra/README.md)
*  [ Example Site Genesis Implementation Instructions ](documentation/SiteGenesisStorefrontIntegration.pdf)

### Test Cases ###

* For full manual regression tests see [ Test Cases ](documentation/AmplienceSFCCTestCases.pdf)
* For critcal path tests see [ Critical Path Test Steps ](documentation/CriticalPathTestSteps.pdf)

### User Documentation ###

* Please see [ Content Authoring user documentation ](documentation/ContentAuthoringSFCCUserGuide.pdf)

* See also the [ Amplience Customer Hub ](http://hub.amplience.com/display/DEV/Salesforce+Commerce+Cloud+Cartridges)

## Changelog ##

* For version history see the [Changelog](CHANGELOG.md)

## Contribution guidelines ##
There are two ways you can contribute to this project:

1. File an `Issue` using the GitHub `Issues` facility.  There are no guarantees that issues that are filed will be addressed or fixed within a certain time frame, but logging issues is an important step in improving the quality of these integrations.

2. If you have a suggested code fix, please fork this repository and issue a 'pull request' against it.  The LINK partner will evaluate the pull request, test it, and possibly integrate it back into the main code branch.  Even if the LINK partner does not choose to adopt your pull request, you've still helped the community because the pull request is now logged with this repository where other customers, clients, and integrators can see it and any of them can choose to adopt your suggested changes.

Thank you for helping improve the quality of this cartridge!

## Who do I talk to? ##

* [Oliver Secluna](mailto:osecluna@amplience.com) - Product Owner, Integrations @ Amplience

## Copyright ##

Copyright 2018 Amplience

