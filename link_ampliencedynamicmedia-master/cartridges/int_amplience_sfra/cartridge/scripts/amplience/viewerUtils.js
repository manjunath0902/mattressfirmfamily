'use strict';

/**
 *
 * @param {dw.catalog.Product} product - product for viewer
 * @returns {Object} viewerData - data to populate viewer
 */
function getviewerData(product) {
    // API Includes
    var Site = require('dw/system/Site');

    var DiViewerID = Site.getCurrent().getCustomPreferenceValue('ampDM_Viewer');
    var DiCDNhostname = Site.getCurrent().getCustomPreferenceValue('ampDM_CdnHostname');
    var DiCustomer = Site.getCurrent().getCustomPreferenceValue('ampDM_CustomerPath');
    var mediaSetRegEx = /[^\/]*$/; // eslint-disable-line no-useless-escape
    var imgMediaFile = product.images.set[0];
    var mediaSet = imgMediaFile ? imgMediaFile.url.match(mediaSetRegEx)[0] : '';
    var viewerData = {
        diViewerID: DiViewerID,
        diCDNhostname: DiCDNhostname,
        diCustomer: DiCustomer,
        mediaSet: mediaSet
    };
    return viewerData;
}

module.exports = getviewerData;
