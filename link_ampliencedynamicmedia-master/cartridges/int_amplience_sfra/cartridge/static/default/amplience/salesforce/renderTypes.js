var renderTypes = {
    "blog": "6f7558c5-21d1-4137-8bef-2a7999a525a0",
    "image": "3d4c2aa6-be72-4f31-9912-77348bdd78c2",
    "text": "bca5b4c4-7ae7-416e-a929-e30977a80d07",
    "banner": "57d16e46-8c8f-4d01-9a27-f47127a6c362",
    "video": "b8662e4c-edc5-40aa-9f8a-143aeafade9a",
    "slider": "a4a694c7-b1b4-4449-92e6-f3ba03ea0e26",
    "snippet": "ddfed3f5-333f-4d48-90e7-b52c06107244",
    "card": "0722118f-311f-4150-b4e7-e8a6868f9387",
    "splitBlock": "9c5762ca-18f3-4a37-891b-15f9d4ea7699",
    "promoBanner": "03a8a1dd-fb8e-44c1-a69a-65bde742765b",
    "externalBlock": "b7659f15-dd18-480d-95f4-60835e87bbef",
    "cardList": "361481c6-2842-4763-aa8f-1ea2605da063",
    "homepage": "b482bcaa-db1c-49ec-a360-b40593bf6fe5"
};

if(typeof module !== 'undefined'){
    module.exports = renderTypes;
}