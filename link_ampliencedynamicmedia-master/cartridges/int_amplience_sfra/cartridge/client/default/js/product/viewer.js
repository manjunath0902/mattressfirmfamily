'use strict';

var amp = window.amp;

/**
* @description Gets data from components/amplience/amplience_productimages.isml
* Then instantiates Amplience Viewer-Kit (loaded in components/footer/footer_UI.isml)
*/
function initAmplienceViewer() {
    var div = document.getElementById('ampviewerkitdata');
    var ampClient = div.getAttribute('data-amp-client');
    var ampBasepath = div.getAttribute('data-amp-basepath');
    var ampSet = div.getAttribute('data-amp-set');
    var ampSecure = 1;
    if (location.protocol !== 'https:') {
        ampSecure = 0;
    }
    var viewer = new amp.Viewer({ // eslint-disable-line no-unused-vars
        target: '#amp-pdp-container',
        client: ampClient,
        imageBasePath: ampBasepath,
        secure: ampSecure,
        set: ampSet
    });
}
module.exports = initAmplienceViewer;
