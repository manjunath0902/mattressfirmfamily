# Getting Started

1. Open package.json and update the paths object with the correct path to your local copy of app_storefront_base
'''json
{
	"paths": {
    "base": "../storefront-reference-architecture/cartridges/app_storefront_base/"
  }
}
'''

2. Run 'npm install' in the root of this repository to install all of the local dependancies

3. Run 'npm run compile:js' in the root of this repository to compile all client-side JS files

4. Create `dw.json` file in the root of this repository:
'''json
{
    "hostname": "your-sandbox-hostname.demandware.net",
    "username": "yourlogin",
    "password": "yourpwd",
    "code-version": "version_to_upload_to"
}
'''

6. Run 'npm run uploadCartridge' in the root of this repository to upload 'int_amplience_sfra' cartridge to the sandbox you specified in dw.json file.

7. Update the cartridge path of your Site in Business Manager:
 - Administration >  Sites >  Manage Site: Settings
 - Add cartridge path int_amplience_sfra to the left of the cartridges field

# NPM scripts
Use the provided NPM scripts to compile and upload changes to your Sandbox.

## Compiling your application

* 'npm run compile:js' - Compiles all .js files and aggregates them.

## Watching for changes and uploading

* 'npm run watch' - Watches everything and recompiles (if necessary) and uploads to the sandbox. Requires a valid dw.json file at the root that is configured for the sandbox to upload.

## Uploading

'npm run uploadCartridge' - Will upload 'int_amplience_sfra' to the server. Requires a valid dw.json file at the root that is configured for the sandbox to upload.

'npm run upload <filepath>' - Will upload a given file to the server. Requires a valid dw.json file.



