# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to Salesforce Commerce Cloud's integration versioning ('the version number of all integrations will be the year in which the certification was completed and the number of versions released that year')

## [19.1.3]
### Fixed
 - Increased script timeout to 30 minutes for very large manifest files

## [19.1.2]
### Fixed
 - Update ServiceRegistry objects to LocalServiceRegistry to support Compatibility mode 19.10

## [19.1.1]
### Changed
 - Documentation changed to mark Content Authoring as legacy and simplify Dynamic Media setup

### Added
 - Dynamic Media FAQ document
 
### Fixed
 - URL encoding of image path incorrectly removing url parameters


## [19.1.0]
### Changed
 - Image job now assigns first variant to master product where no separate image exists for the master

### Added
 - Example Storefront Reference Architecture cartridge and associated documentation

### Fixed
 - Fixed image display in business manager 'edit image' modal window. Caused by unescaped special characters

## [17.2.2] 

### Fixed
 - Move template updated check to frontend (broken by moving up-to-date check in 17.2.1)

### Added
 - Copied util modules from SiteGenesis to avoid hardcoded referemnce to Storefront modules


## [17.2.1] - 2018-01-22
### Changed
 - Moved up-to-date check (hash check) of imported items to the front end for improved performance

### Fixed
 - Updated DM manifest import job schedule metadata with correct manifest URL for turnkey account

### Removed
- Removed default value from Accelerators Path site preference, to avoid errors when updating existing implementations

## [17.2.0] - 2017-11-22
### Changed
- Documentation changes to support Amplience CA Accelerators
- Documentation improvements for Dynamic Media AssignImages job
- New test cases

### Fixed
- Fixed issue with Available Content search - Keyword search was case-sensitive; Now case-neutral.
- Missing fields in job import metadata (caused error on manifest import)

### Added
 - Amplience CA Accelerators support:
  - New custom site preference and associated javascript to allow handelbars helpers to be sourced from a location in the storefront javascript.
  - New metadata file to import Accelertors handlebars templates
 - Select All / Clear All button added to imported content screen to ease bulk update / delete process

### Removed
- Legacy rendering kits resources (superceded by new Accelerators)

## [17.1.1] - 2017-10-24
### Changed
- Minor documentation improvements

### Fixed
- Limit of number of content items changed from 100,000 ro 20,000 to avoid quota violations

### Added
- Full test cases document added


## [17.1.0] - 2017-09-28

### Changed
- Converted cartridges from pipelines to javascript controllers
- Converted manifest import job to use job framework instead of integration framework

### Fixed
- Timeout error on Imported Items tab for Content Authoring
- Limit of 500 content items for Content Authoring increased to arbitrary large number

### Added
- Added support for Visualisations
- Added Apache 2.0 license
- Added metadata for manifest import job step configuration
- Added Content Authoring User Guide

### Removed
- Removed support for User Generated Content (UGC)
- Removed support for IM (Interactive Media)

## [16.2.0] - 2016-06-26
### Changed
- Updated installation guide to include Content Authoring

### Fixed
- Workaround for BLOCK_PUT on staging
- move xmlstream reader use to amplienceParser.init() function to put one asset element in memory at a time (instead of the whole manifest node)
- fix for PSWATCHES when a prefix and/orsuffix was added

### Added
- Added Content Authoring cartridge
- Added metadata to support Content Authoring cartridge

### Removed
- Removed invalid parameter bindings from Amplience pipeline
- Removed loadManifestXmlObject() function

## [16.1.0] - 2016-03-18

### Added
- Added User Manual for UGC and IM
- Added service framework


## [15.2.0] - 2015-10-02

### Added
- Added support for interactive Merchandising and User Generated Content


## [15.1.0] - 2015-07-07
### Changed
- Updated documentation

### Added
- Added ampHTTPClient Timeout to system-objecttype-extension metadata
- Added comments to script functins and pipelets
- Added support for responsive/adaptive/localized to Amplience Content Modules


## [14.1.0] - 2014-07-25

### Added
- Initial Amplience Cartridge version