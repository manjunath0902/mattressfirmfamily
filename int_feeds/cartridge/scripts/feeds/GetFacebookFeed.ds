/**
*	FACEBOOK Product Feed Script
*	It is used to generate a text file containing a list of products and its attributes
*
*	@input LocalFilePath : String
*	@input LocalFileName : String
*	@input ErrorFile : String
*   @output OutFile : dw.io.File 
*	@input HostFileName : String
*	@input HostName : String
*	@input HostPassword : String
*   @input HostUserName : String 
*/
importPackage( dw.system );
importPackage( dw.util );
importPackage( dw.catalog );
importPackage( dw.io );
importPackage( dw.object );
importPackage( dw.web );

function execute( pdict : PipelineDictionary ) : Number {

	var ProductUtils = require('app_storefront_core/cartridge/scripts/product/ProductUtils.js');
	var SiteURL:String=dw.system.Site.current.preferences.custom.googleMerchantSiteURL;
	
	var calendar : Calendar = new Calendar();
	calendar.timeZone = "America/New_York";
	var DefaultBrandName:String=dw.system.Site.current.preferences.custom.brandNameSitePreferenceAttribute; 
	
	var filePath:String = pdict.LocalFilePath;
	var fileName:String = pdict.LocalFileName;
	(new File(File.IMPEX + File.SEPARATOR + filePath)).mkdirs();
    // the feed file is created under the IMPEX WebDab directory
	var file : File = new File(File.IMPEX + File.SEPARATOR + filePath + File.SEPARATOR + fileName);
	var exportFileWriter : FileWriter = new FileWriter(file);
	
	var errorFilePath:String = pdict.LocalFilePath;
	var errorFileName:String = pdict.ErrorFile;
	(new File(File.IMPEX + File.SEPARATOR + errorFilePath)).mkdirs();
	var errorFile : File =new File(File.IMPEX + File.SEPARATOR + errorFilePath + File.SEPARATOR + errorFileName);
	var errorFileWriter : FileWriter = new FileWriter(errorFile);
 	var errorCount : Number = 0;
	
	var prods : SeekableIterator = ProductMgr.queryAllSiteProducts();
	// header defines the attributes of the feed	
	exportFileWriter.writeLine("\"id\",\"availability\",\"condition\",\"description\",\"image_link\",\"link\",\"title\",\"price\",\"sale_price\",\"sale_price_effective_date\",\"brand\",\"size\",\"google_product_category\"");
	//exportFileWriter.writeLine("\"id\",\"availability\",\"condition\",\"description\",\"image_link\",\"link\",\"title\",\"price\",\"brand\",\"google_product_category\"");

	//Get week num to append to image url and force fb business manager to reload image cache on a weekly basis
	//*****************************************************************************************************************
	var date = new Date();
	date.setHours(0, 0, 0, 0);
	// Thursday in current week decides the year.
	date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
	// January 4 is always in week 1.
	var week1 = new Date(date.getFullYear(), 0, 4);
	// Adjust to Thursday in week 1 and count number of weeks from date to week1.
	var weekNum = 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000 - 3 + (week1.getDay() + 6) % 7) / 7);
	//*****************************************************************************************************************
		
	// the product attributes are being retrieved here
	while(prods.hasNext()) {
		try {
			var errorLine : String = "";
			var product : Product = prods.next();
			
			//Product ID
			var productID : String = null;
			if( product.getID() !=null){
				productID = product.getID();
			}
			else{
				productID = "";
			}
	
	        if(product.getOnlineFlag() && product.getSearchableFlag() && product.getAvailableFlag() && !product.custom.isDiscontinued) {
				
				var checkOnlineStatus : String = null;
		 		var masterProduct : Product = product.variationModel.getMaster();
				if(masterProduct != null){
					if(masterProduct.getOnlineFlag()){ 
						checkOnlineStatus = "true";
					}else{
						checkOnlineStatus = "false";	
					}
				}else{
					checkOnlineStatus = "true";		
				}
				
				//if product and it's master is online include it in the feed				
				if(checkOnlineStatus != "true"){
					continue;
				}
				
				// Product Name
				var productName : String = null;
				var name : String = null;
				if(product.getName() != null){
					name = product.getName();
					productName = name.split("\"").join("\"\"");
				}
				else{
					errorLine = errorLine + ",product name blank";
					productName = "";	
				}
							
				// Product Description
				var productDescription : String = null;
				var description : String = null;
				var grossDescription : String = null;
	
				if(product.custom.tabDetails != null) {
					grossDescription = product.custom.tabDetails.toString();
					description = StringUtils.trim(grossDescription.toString().replace(/[\r\n\t]/g," ").replace(/<\/?iframe[^>]*\>.*<\/iframe>/g,"").replace(/<[^>]*>/g,"").replace("&nbsp;"," ").replace(/\s\s+/g, ' '));				
				}else{
					if(product.getLongDescription() != null) {
						grossDescription = product.getLongDescription().toString();
						description = StringUtils.trim(grossDescription.toString().replace(/[\r\n\t]/g," ").replace(/<\/?iframe[^>]*\>.*<\/iframe>/g,"").replace(/<[^>]*>/g,"").replace("&nbsp;"," ").replace(/\s\s+/g, ' '));										
					}else{
						if(product.getShortDescription() != null) {
							grossDescription = product.getShortDescription().toString();
							description = StringUtils.trim(grossDescription.toString().replace(/[\r\n\t]/g," ").replace(/<\/?iframe[^>]*\>.*<\/iframe>/g,"").replace(/<[^>]*>/g,"").replace("&nbsp;"," ").replace(/\s\s+/g, ' '));										
						}else{
							errorLine = errorLine + ",product description blank";
							productDescription = "";
						}
					}
				}
				productDescription = description.split("\"").join("\"\"");
				
				// Link 
				var link : String = null;
				if(!empty(productID) && !empty(URLUtils.http('Product-Show', 'pid', productID))) {
					link = 	URLUtils.http('Product-Show', 'pid', productID);
					var lastSlash : Number = String(link).lastIndexOf("/")
					link = SiteURL+String(link).substring(lastSlash);
				}
				else {
					link = "";
				}
				
				var imageLink : String = null;
				if (product.getImage('large',0)!= null) {
					imageLink = SiteURL + product.getImage('large',0).URL + '?wk=' + weekNum + '&cv=' + dw.system.Site.current.preferences.custom.fbCacheParameter; 
				}else{
					if(product.variationModel != null){
						if(product.custom.external_id != null){
                            imageLink = "http://i1.adis.ws/i/hmk/" + product.custom.external_id + ".jpg" + '?wk=' + weekNum + '&cv=' + dw.system.Site.current.preferences.custom.fbCacheParameter; 
						}else{
							imageLink = "";
						}
					}else{
						imageLink = "";
					}				
				}	
				
				// Price
				if (product.master) {
					var standardPrice = 999999999;
					var salePrice = 999999999;
					var salePriceEffectiveDateTo : String = null;
					var salePriceEffectiveDateFrom : String = null;
					
					for each (var varProduct in product.getVariants()) {
						var varPriceData = ProductUtils.getPricing(varProduct);
						
						var varStandardPrice = varPriceData.standard;
						if (varStandardPrice < standardPrice) {
							standardPrice = varStandardPrice;
						}						

						var varSalePrice = varPriceData.sale;
						if (varSalePrice < salePrice) {
							salePrice = varSalePrice;
							if (varProduct.getPriceModel().getPriceInfo()) {
								salePriceEffectiveDateFrom = varProduct.getPriceModel().getPriceInfo().getOnlineFrom();
								salePriceEffectiveDateTo = varProduct.getPriceModel().getPriceInfo().getOnlineTo();
							}
						}						
					}
					
					if (product.variants.empty) {
						standardPrice = "";
					}
					
					if (salePrice >= standardPrice) { 
						salePrice = "";
						salePriceEffectiveDateFrom = null;
						salePriceEffectiveDateTo = null;
					}
					
				} else {
					var priceData = ProductUtils.getPricing(product);
					var standardPrice = priceData.standard;
					var salePrice = priceData.sale;
					var salePriceEffectiveDateFrom : Date = null;
					var salePriceEffectiveDateTo : Date = null;
					
					if (salePrice >= standardPrice) {
						salePrice="";
					} else {
						if (product.getPriceModel().getPriceInfo()) {
							salePriceEffectiveDateFrom = product.getPriceModel().getPriceInfo().getOnlineFrom()
							salePriceEffectiveDateTo = product.getPriceModel().getPriceInfo().getOnlineTo();
						}
					}
				}
								
				if(standardPrice == "" || standardPrice == 0){
					continue;
				}
				standardPrice = standardPrice + " USD";
				if (salePrice) {
					salePrice = salePrice + " USD";
				}
				//Brand
				var brand : String = null;
				if(product.brand != null) {
	     			brand = product.brand;
	     		}
				else{
					var brand : String = DefaultBrandName;
				}
				
				// Size Variation Attribute
				var size : String = "";
				var variantAtributeSize : ProductVariationAttribute = null;
				var attributeSizeValue : ProductVariationAttributeValue = null;
				
				try{
					if(product.variationModel.getProductVariationAttribute("size") != null) {
						variantAtributeSize = product.variationModel.getProductVariationAttribute("size");
						attributeSizeValue = product.variationModel.getVariationValue(product,variantAtributeSize);
				
						size = attributeSizeValue.getDisplayValue();
	
					}
				}
				catch(e) {
					size = "";
				}

				// Google Taxonomy
				var googleTaxonomy : String = null;
				
				if(!empty(product.custom.google_taxonomy)) {
					googleTaxonomy = product.custom.google_taxonomy;
				}
	
				var productCategory : String = null;
				if(product.variationModel != null){
					if(product.variationModel.getMaster().getPrimaryCategory() != null){
						productCategory = product.variationModel.getMaster().getPrimaryCategory().displayName;
					}else if(product.variationModel.getMaster().getClassificationCategory() != null){
						if(product.variationModel.getMaster().getClassificationCategory().displayName != null){
							productCategory = product.variationModel.getMaster().getClassificationCategory().displayName;
						}else{
							productCategory = "";
						}					
					}else{
						productCategory = "";
					}
				}else{
					productCategory = "";
				}
					
				if(productCategory != "mattress" || productCategory != "Adjustable-Beds" || productCategory != "Pillows"){
					if(isCategoryTest('Pillows', product) == true){
						productCategory = "Pillows";					
					}else if(isCategoryTest('Bunk Beds', product) == true){
						productCategory = "Bunk-Beds";	
					}else if(isCategoryTest('Mattress-Protectors', product) == true){
						productCategory = "Mattress Protectors";	
					}else if(isCategoryTest('Adjustable Beds', product) == true){
						productCategory = "Adjustable-Beds";
					}else if(isCategoryTest('Dream Bed', product) == true){
						productCategory = "Bed-In-A-Box";
					}else if(isCategoryTest('Crib Mattresses', product) == true){
						productCategory = "Crib-Mattresses";
					}else if(isCategoryTest('Bed Sets & Headboards', product) == true){
						productCategory = "Headboards-and-Footboards";		
					}else if(isCategoryTest('Massage Chairs', product) == true){
						productCategory = "Massage-Chairs";	
					}else if(isCategoryTest('Mattresses', product) == true){
						productCategory = "Mattress";
					}else if(isCategoryTest('Leather Chairs & Ottomans', product) == true){
						productCategory = "Upholstered-Furniture";
					}else if(isCategoryTest('Sofas & Loveseats', product) == true){
						productCategory = "Upholstered-Furniture";
					}else if(isCategoryTest('Bed Frames', product) == true){
						productCategory = "Frames";
					}else if(isCategoryTest('Mattress Toppers', product) == true){
						productCategory = "Mattress-Toppers";
					}else if(isCategoryTest('Beds & Furniture', product) == true){
						productCategory = "Furniture";
					}else if(isCategoryTest('Holiday Gift Guide', product) == true){
						productCategory = "Furniture";
					}else if(isCategoryTest('Bed & Mattress Accessories', product) == true){
						productCategory = "Accessories";
					}else{
						
					}
	
				}
	
				var google_product_category : String = null;
		
				if(productCategory == "Mattress"){
					google_product_category = "Furniture > Beds & Accessories > Mattresses";
				}else if(productCategory == "Crib-Mattresses"){
					google_product_category = "Furniture > Beds & Accessories > Mattresses";
				}else if(productCategory == "Bed-In-A-Box"){
					if(product.getName().toLocaleLowerCase().search("foundation") > -1){					
						google_product_category = "Furniture > Beds & Accessories > Mattress Foundations";
					}else{
						google_product_category = "Furniture > Beds & Accessories > Mattresses";
					}
				}else if(productCategory == "Adjustable-Beds"){
					google_product_category = "Furniture > Beds & Accessories > Mattress Foundations";
				}else if(productCategory == "Frames"){
					google_product_category = "Furniture > Beds & Accessories > Beds & Bed Frames";
				}else if(productCategory == "Bunk-Beds"){
					google_product_category = "Furniture > Beds & Accessories > Bed & Bed Frame Accessories";
				}else if(productCategory == "Massage-Chairs"){
					google_product_category = "Health & Beauty > Personal Care > Massage & Relaxation > Massage Chairs";
				}else if(productCategory == "hillsdale-furniture"){
					google_product_category = "Furniture > Beds & Accessories > Headboards & Footboards";
				}else if(productCategory == "Headboards-and-Footboards"){
					google_product_category = "Furniture > Beds & Accessories > Headboards & Footboards";
				}else if(productCategory == "Pillows"){
					google_product_category = "Home & Garden > Linens & Bedding > Bedding > Pillows";
				}else if(productCategory == "outlast-bedding"){
					google_product_category = "Home & Garden > Linens & Bedding > Bedding > Bed Sheets";
				}else if(productCategory == "Mattress-Protectors"){
					google_product_category = "Home & Garden > Linens & Bedding > Bedding > Mattress Protectors > Mattress Encasements";
				}else if(productCategory == "Mattress-Toppers"){
					google_product_category = "Home & Garden > Linens & Bedding > Bedding > Mattress Protectors > Mattress Pads";
				}else if(productCategory == "Accessories"){
					if(product.getName().toLocaleLowerCase().search("pillowcase") > -1){					
						google_product_category = "Home & Garden > Linens & Bedding > Bedding > Pillowcases & Shams";
					}else if(product.getName().toLocaleLowerCase().search("sheet") > -1){					
						google_product_category = "Home & Garden > Linens & Bedding > Bedding > Bed Sheets";
					}else if(product.getName().toLocaleLowerCase().search("foundation") > -1){					
						google_product_category = "Furniture > Beds & Accessories > Mattress Foundations";
					}else if(product.getName().toLocaleLowerCase().search("protector") > -1){					
						google_product_category = "Home & Garden > Linens & Bedding > Bedding > Mattress Protectors > Mattress Encasements";
					}else if(product.getName().toLocaleLowerCase().search("topper") > -1){					
						google_product_category = "Home & Garden > Linens & Bedding > Bedding > Mattress Protectors > Mattress Pads";
					}else if(product.getName().toLocaleLowerCase().search("mattress") > -1){					
						google_product_category = "Furniture > Beds & Accessories > Mattresses";
					}else if(product.getName().toLocaleLowerCase().search("dog bed") > -1){					
						google_product_category = "Animals & Pet Supplies > Pet Supplies > Dog Supplies > Dog Beds";
					}else if(product.getName().toLocaleLowerCase().search("bunkie board") > -1){									
						google_product_category = "Furniture > Beds & Accessories > Bed & Bed Frame Accessories";
					}else{														
						google_product_category = "Furniture > Beds & Accessories";			
					}
				}else if(productCategory == "Furniture" || productCategory == "Upholstered-Furniture" || productCategory == "lux-living-only-at-mattress-firm"){
					if(product.getName().toLocaleLowerCase().search("headboard") > -1){					
						google_product_category = "Furniture > Beds & Accessories > Headboards & Footboards";
					}else if(product.getName().toLocaleLowerCase().search("bed set") > -1){					
						google_product_category = "Furniture > Beds & Accessories > Headboards & Footboards";
					}else if(product.getName().toLocaleLowerCase().search("sheet") > -1){									
						google_product_category = "Furniture > Beds & Accessories > Mattresses";
					}else if(product.getName().toLocaleLowerCase().search("office chair") > -1){									
						google_product_category = "Furniture > Office Furniture > Office Chairs";
					}else if(product.getName().toLocaleLowerCase().search("sofa") > -1){									
						google_product_category = "Furniture > Sofas";
					}else if(product.getName().toLocaleLowerCase().search("chair") > -1){									
						google_product_category = "Furniture > Chairs > Arm Chairs, Recliners & Sleeper Chairs";
					}else if(product.getName().toLocaleLowerCase().search("ottoman") > -1){									
						google_product_category = "Furniture > Ottomans";
					}else if(product.getName().toLocaleLowerCase().search("loveseat") > -1){									
						google_product_category = "Furniture > Sofas";
					}else if(product.getName().toLocaleLowerCase().search("recliner") > -1){									
						google_product_category = "Furniture > Chairs > Arm Chairs, Recliners & Sleeper Chairs";
					}else if(product.getName().toLocaleLowerCase().search("bunkie board") > -1){									
						google_product_category = "Furniture > Beds & Accessories > Bed & Bed Frame Accessories";
					}else if(product.getName().toLocaleLowerCase().search("platform bed") > -1){									
						google_product_category = "Furniture > Beds & Accessories > Beds & Bed Frames";
					}else{															
						google_product_category = "Furniture";			
					}
				}else{
					google_product_category = "";
				}
	
				var condition : String = "new";
				var availability : String = "in stock";
	
				try {					  
					var list : ArrayList = new dw.util.ArrayList();
					list.add1("\"" + productID + "\"");				//*ID
					list.add1("\"" + availability + "\"");			//*AVAILABILITY
					list.add1("\"" + condition + "\"");				//*CONDITION
					list.add1("\"" + productDescription + "\"");	//DESCRIPTION
					list.add1("\"" + imageLink + "\"");				//*IMAGE LINK
					list.add1("\"" + link + "\"");					//*LINK
					list.add1("\"" + productName + "\"");	    	//TITLE
					list.add1("\"" + standardPrice + "\"");			//*PRICE			
					list.add1("\"" + salePrice + "\"");				//*SALE PRICE
					if (salePriceEffectiveDateTo) {
						try {
						var calFrom : Calendar =  new Calendar(salePriceEffectiveDateFrom);
						var calTo : Calendar =  new Calendar(salePriceEffectiveDateTo);
						list.add1("\"" + StringUtils.formatCalendar(calFrom, "yyyy-MM-dd'T'HH:mmZZ") + "/" + StringUtils.formatCalendar(calTo, "yyyy-MM-dd'T'HH:mmZZ") + "\"");//*SALE PRICE EFFECTIVE DATE
						} catch (e) {
							list.add1("\"" + "" + "\"");//*SALE PRICE EFFECTIVE DATE
						}
					} else {
						list.add1("\"" + "" + "\"");//*SALE PRICE EFFECTIVE DATE
					}
					list.add1("\"" + brand + "\"");					//MANUFACTURER
					list.add1("\"" + size + "\"");					//SIZE
					list.add1("\"" + google_product_category + "\"");		//*google_product_category
				} catch(e) {
					errorFileWriter.writeLine(productID + errorLine);
				}
				// creates a single feed line separating the attributes with commas
				var delimiter : string = ",";
				var singleProductFeed : String = getArrayListInStringFormatByUsingDelimiters(list, delimiter);
				
				var netSingleProductFeed : String;
				var lastTabIndex : Number;
				// remove last comma
				lastTabIndex = singleProductFeed.lastIndexOf(",");
				netSingleProductFeed = singleProductFeed.substring(0 , lastTabIndex);
				// every feed line is being written in the defined file in the IMPEX Web Dab directory			
				exportFileWriter.writeLine(netSingleProductFeed);
				
				if (!empty(errorLine)) {
					errorFileWriter.writeLine(productID + errorLine);
					errorCount++;
				}
			}
		} catch(e) {
			errorFileWriter.writeLine(productID);
			errorCount++;
		}
	}
		
   	exportFileWriter.flush();
   	exportFileWriter.close();

   	errorFileWriter.flush();
   	errorFileWriter.close();
   	if (errorCount==0) {
   		errorFile.remove();
   	}
   
	//Write duplicate file that is ftped to other sites and overwitten every day.
	try{

		var collectiveFile : File = new File(File.IMPEX + File.SEPARATOR + filePath + File.SEPARATOR + "collective_" + fileName);
		var collectiveFileWriter : FileWriter = new FileWriter(collectiveFile);
	    var collectiveFileReader : FileReader = new FileReader(file);
       
	    if (collectiveFileReader.ready() ) {
	    	var line : String = collectiveFileReader.readLine();
	    	while (line != null ) {
	    		collectiveFileWriter.writeLine(line);
	    		line = collectiveFileReader.readLine();
	    	}
	    	collectiveFileReader.close();
	    	collectiveFileWriter.flush();
	    	collectiveFileWriter.close();
	    	
	    }
	    else {
			Logger.error("Collective csv file not ready to be read.");
	    }
	    	
	}
	catch(e) {
		Logger.error("Collective csv file Exception: ", e);
	}

   pdict.OutFile = file;


   		
   return PIPELET_NEXT;
}



function getArrayListInStringFormatByUsingDelimiters(list : ArrayList , delimiter : String) : String {
	
	var stringThing : String = "";
	var iterator : Iterator =list.iterator();
			
	while(iterator.hasNext()) {
		var aString : String =iterator.next();
		if(aString != null) {
			stringThing = stringThing + aString + delimiter;
		} else {
			stringThing = stringThing + "" + delimiter;	
		}
	}	
	return stringThing;
}


function getCategoryPath(product : Product) : String {
	var categoryPath : String ="";	
	var topProduct : Product = product;
		if( topProduct.isVariant() ) {
			topProduct = product.masterProduct;	
		}
		var theCategory : Category = topProduct.getPrimaryCategory();
		if( empty(theCategory) ) {
			var categories : Collection = topProduct.categories;
			if( !empty(	categories ) ) {
				theCategory = categories[0];	
			}
		}

	var cat : Category = theCategory;
	var path : dw.util.ArrayList = new dw.util.ArrayList();
			while( cat.parent != null)
			{
				if(cat.online){
					path.addAt( 0, cat );
				}	
				cat = cat.parent; 
			}

	var index : Number;	
	for(index=0; index<path.length ; index++){
		if(index==0) {
			categoryPath = categoryPath + path[index].getDisplayName();
		} else {
			categoryPath = categoryPath + ">" + path[index].getDisplayName();
		}
	}	
	return categoryPath;
}

function isCategoryTest(keyword,product){
	var catArray : Array = product.variationModel.getMaster().allCategoryAssignments; 
	var catStr : String;
	var isTest : Bolean = false;

	for each (var prodCategory in  catArray ){
		catStr = prodCategory.getCategory().displayName;
		if(catStr == keyword){
			isTest = true;
		}
	}
	
	return isTest;
}				
