'use strict';    
/**
     * @function
     * @description To check calander date is today's date or not.
     * @Params Expect calander day @Number(date, year, month)
     * @return @Boolean today date or not
     */
var CustomObjectMgr = require('dw/object/CustomObjectMgr');  
var arrayList = require('dw/util/ArrayList');
var today = new Date();
 function isCurrentDate(lastModified){
	 var isCurrentDate =false;
	 if (lastModified.getDate() === today.getDate() && lastModified.getFullYear() === today.getFullYear()  && lastModified.getMonth() === today.getMonth()){
		 isCurrentDate = true;
	 }
	 return isCurrentDate;
 }
 
function MakeAPaymentBatchEmail (args) {
	var customObjectsIterator = CustomObjectMgr.queryCustomObjects('MakeaPaymentOrders',"", 'lastModified DESC');
	var emailData = new arrayList();
	while(customObjectsIterator.hasNext()){
		var customObject = customObjectsIterator.next();
		var isCurrentDay = isCurrentDate(customObject.lastModified);
		if (isCurrentDay) {
			var dataObject = new Object();
			var jsonOrderObj = JSON.parse(customObject.custom.orderObject);
			dataObject.customerName = (jsonOrderObj && jsonOrderObj.customerName) ? jsonOrderObj.customerName:'';
			dataObject.customerPhone = (jsonOrderObj && jsonOrderObj.customerPhone) ? jsonOrderObj.customerPhone:'';
			dataObject.customerEmail = (jsonOrderObj && jsonOrderObj.customerEmail) ? jsonOrderObj.customerEmail:'';
			dataObject.cardHolderName = (jsonOrderObj && jsonOrderObj.cardHolderName) ? jsonOrderObj.cardHolderName:'';
			dataObject.orderNumber = (jsonOrderObj && jsonOrderObj.orderNumber) ? jsonOrderObj.orderNumber:'';
			dataObject.paymentAmount = (jsonOrderObj && jsonOrderObj.paymentAmount) ? jsonOrderObj.paymentAmount:'';
			dataObject.maxStoreNumber = (jsonOrderObj && jsonOrderObj.maxStoreNumber) ? jsonOrderObj.maxStoreNumber:'';
			dataObject.currencySymbol = (session && session.getCurrency() && session.getCurrency().getSymbol()) ? session.getCurrency().getSymbol():'';
			dataObject.retryCount = (customObject && customObject.custom) ? customObject.custom.retrycount:'';
			emailData.push(dataObject);
		} else {
			break;  // Break imediately as we do not recieve today's date custom object
		}
	}
	var emailTemplate: Template = new dw.util.Template("mail/wmap_failure_batch_email");
	var pdictParams : Map = new dw.util.HashMap();
	pdictParams.put('emailData',emailData);
	
	var content: MimeEncodedText = emailTemplate.render(pdictParams);
	var mail: Mail = new dw.net.Mail();
	
	if (args && args.emailFrom && args.emailSubject && args.emailTo){
		mail.addTo(args.emailTo);
		mail.setFrom(args.emailFrom);
		mail.setSubject(args.emailSubject);
		mail.setContent(content); 
		mail.send();
	}

}	
 
 exports.MakeAPaymentBatchEmail = MakeAPaymentBatchEmail;
     