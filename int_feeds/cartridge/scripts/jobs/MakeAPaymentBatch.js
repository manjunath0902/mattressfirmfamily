'use strict';
/**
 * Retrying Make a Payment Create Order Balance Again
 *
 */
 
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');
var Logger = require('dw/system/Logger');
var wmapHelpers = require("int_makepayment/cartridge/scripts/util/wmapHelpers").UtilHelpers;
var MakeAPayemntServices = require("bc_sleepysc/cartridge/scripts/init/service-MakeAPaymentServices");

/* Constants */
const DEFAULT_RETRY_COUNT = 3;

function createOrderBalance(args) {		
	
	var mapBatchMaxRetries = dw.system.Site.getCurrent().getCustomPreferenceValue("mapBatchMaxRetries") ? dw.system.Site.getCurrent().getCustomPreferenceValue("mapBatchMaxRetries") : DEFAULT_RETRY_COUNT;  
	var balanceIterator = CustomObjectMgr.queryCustomObjects("MakeaPaymentOrders", "custom.retrycount = NULL or custom.retrycount < {0}", "custom.ID desc", mapBatchMaxRetries);
	var dueBalance, service, result, requestPacket, requestPacketObject;
	while(balanceIterator.hasNext()) {
		dueBalance = balanceIterator.next();
		var retrycount = !empty(dueBalance.custom.retrycount) ? dueBalance.custom.retrycount : 0;
		try {
			var service = MakeAPayemntServices.OrderPaymentMFRM;
		
			requestPacket = dueBalance.custom.data;
			
			//Parsing JSON for un-escaping Characters
			requestPacketObject = JSON.parse(requestPacket);
			
			requestPacket = JSON.stringify(requestPacketObject);
			
			//Calling Service
			result = service.call(requestPacket);  		    			    		    			
			
			if (!(wmapHelpers.isServiceError(result))) {
				Transaction.wrap(function () {
					CustomObjectMgr.remove(dueBalance);
	        	});
			} 
			else if(wmapHelpers.isOrderPaidOrCancelled(JSON.parse(result.errorMessage).RefCode)){
				Transaction.wrap(function () {
					CustomObjectMgr.remove(dueBalance);
	        	});
			} 
			else {
				Transaction.wrap(function () {
					dueBalance.custom.retrycount = retrycount + 1;
	        	});
				Logger.error('Failed to Create Order Balance');
			}
			
		} catch (e) {
			Transaction.wrap(function () {
				dueBalance.custom.retrycount = retrycount + 1;
	        });
			var msg = e;
			Logger.error('Failed to Create Order Balance, Following Error Occured: ' + e.toString());
		}
	}
}

exports.CreateOrderBalance = createOrderBalance;