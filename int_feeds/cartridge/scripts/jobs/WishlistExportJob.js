'use strict';
/**
 * Export wishlists of customers from last one day based on last modified date.
 *
 */
 
var ProductListMgr = require('dw/customer/ProductListMgr');
var ProductList = require('dw/customer/ProductList');
var ServiceRegistry = require('dw/svc/ServiceRegistry');
var ProductUtils = require('app_storefront_core/cartridge/scripts/product/ProductUtils.js');


function exportProductIDs(args) {
	var CSVStreamWriter = require('dw/io/CSVStreamWriter');
	var FileWriter = require('dw/io/FileWriter');
	var Logger = require('dw/system/Logger');
	var File = require('dw/io/File');

	(new dw.io.File(dw.io.File.IMPEX +'/src/bv/')).mkdirs();
	var out;
	var file;

	try
	{
		file = new dw.io.File(dw.io.File.IMPEX + '/src/bv/products.csv');
		
		if(!file.exists()) 
		{
			if(!file.createNewFile())
			{
				Logger.error("File "+file.name+" could not be created!");
				return PIPELET_ERROR;
			}
		} 
		else
		{
			Logger.debug("File "+file.name+" exits and gets overwritten!");
		}
		
		out = new CSVStreamWriter(new FileWriter(file), ",", "\"");	
	}
	catch(e)
	{
		Logger.error("An error occured while exporting products data {0}.", e);
		return PIPELET_ERROR;
	}

	var header = new Array();
	
	header.push("Product IDs With Brand Pefix");
	header.push("Product IDs Without Brand Prefix");
	out.writeNext(header);	
	
	var prods = dw.catalog.ProductMgr.queryAllSiteProducts();
	while (prods.hasNext()) {
		var product = prods.next();
		var isMaster = product.isMaster();
		var productIDs = new Array();
		if(isMaster) {
			productIDs.push(product.ID);
			productIDs.push((!empty(product.custom.external_id) ? product.custom.external_id : ''));
			out.writeNext(productIDs);
		}
	}
	out.close();
}

exports.ExportProductIDs = exportProductIDs;