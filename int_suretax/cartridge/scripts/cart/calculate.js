'use strict';

/**
 * @module calculate.js
 *
 * This javascript file implements methods (via Common.js exports) that are needed by
 * the new (smaller) CalculateCart.ds script file.  This allows OCAPI calls to reference
 * these tools via the OCAPI 'hook' mechanism
 *
 */
var HashMap = require('dw/util/HashMap');
var PromotionMgr = require('dw/campaign/PromotionMgr');
var ShippingMgr = require('dw/order/ShippingMgr');
var ShippingLocation = require('dw/order/ShippingLocation');
var TaxMgr = require('dw/order/TaxMgr');
var Logger = require('dw/system/Logger');
var Status = require('dw/system/Status');
var Money = require('dw/value/Money');
var Order = require('dw/order');
var dwLogger = require("dw/system").Logger;
var TaxRequest = require("int_suretax/cartridge/scripts/suretax/TaxRequest");
var ConfigUtils = new ( require("int_suretax/cartridge/scripts/util/ConfigUtils") );
var ProductUtils = require('app_storefront_core/cartridge/scripts/product/ProductUtils.js');
var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;
var UtilFunctions = require('app_mattressfirm_storefront/cartridge/scripts/util/UtilFunctions').UtilFunctions;
var Site = require('dw/system/Site');

/**
 * @function calculate
 *
 * calculate is the arching logic for computing the value of a basket.  It makes
 * calls into cart/calculate.js and enables both SG and OCAPI applications to share
 * the same cart calculation logic.
 *
 * @param {Object} args - Arguments object
 */
exports.calculate = function (args) {
    var basket = args.Basket;
    var order = args.Order ? args.Order : null;
    var finalizeTaxRequest = args.FinalizeTaxRequest;
    var customerNo = (args.CustomerNo && args.CustomerNo.length > 0) ? args.CustomerNo : null;
    var invoiceNo = (args.InvoiceNo && args.InvoiceNo.length > 0) ? args.InvoiceNo : null;

	// =========================================================================
	// =====   Manage Quotation Items In Every PRODUCT LINE ITEM PRICES    =====
	// =========================================================================
	
	manageQuotationLineItems(basket);
	
    // ===================================================
    // =====   CALCULATE PRODUCT LINE ITEM PRICES    =====
    // ===================================================

    calculateProductPrices(basket);

    // ===================================================
    // =====    CALCULATE GIFT CERTIFICATE PRICES    =====
    // ===================================================

    calculateGiftCertificatePrices(basket);

	// Removing recycling fees before any promotions are applied.
	mattressPipeletHelper.removeRecyclingFees(basket);
    // ===================================================
    // =====   Note: Promotions must be applied      =====
    // =====   after the tax calculation for         =====
    // =====   storefronts based on GROSS prices     =====
    // ===================================================

    // ===================================================
    // =====   APPLY PROMOTION DISCOUNTS			 =====
    // =====   Apply product and order promotions.   =====
    // =====   Must be done before shipping 		 =====
    // =====   calculation. 					     =====
    // ===================================================

    PromotionMgr.applyDiscounts(basket);

    // ===================================================
    // =====        CALCULATE SHIPPING COSTS         =====
    // ===================================================

    // apply product specific shipping costs
    // and calculate total shipping costs
    ShippingMgr.applyShippingCost(basket);
    
    // Update the shipping costs of In-Market and Parcel shipments based on Custom Preferences
	mattressPipeletHelper.addshippingLineItemCharge(basket);
	
	//remove tier shipping cost from Silver/Gold/Platinum shipping method
    mattressPipeletHelper.removeSilverGoldPlatinumShippingCost(basket);
	
	// ===================================================
	// =====   APPLY PROMOTION DISCOUNTS			 =====
	// =====   Apply product and order and 			 =====
	// =====   shipping promotions.                  =====
	// ===================================================
	
	//Excludes Option Products in Discount Calculation for BTS/Cyber Monday Promo Products Only.
	if(Site.getCurrent().ID === "Mattress-Firm") {
		applyDiscounts(basket);
	}
	else {
		PromotionMgr.applyDiscounts(basket);
	}
	
    // since we might have bonus product line items, we need to
    // reset product prices
    calculateProductPrices(basket);

	// Now we are done with discounts, re-apply any recycling fees
	mattressPipeletHelper.updateRecyclingFees(basket);
	
    // ===================================================
    // =====         CALCULATE TAX                   =====
    // ===================================================

    calculateTax(basket, finalizeTaxRequest, customerNo, invoiceNo, order);

    // ===================================================
    // =====         CALCULATE BASKET TOTALS         =====
    // ===================================================

    basket.updateTotals();

    // ===================================================
    // =====            DONE                         =====
    // ===================================================

    return new Status(Status.OK);
};
/**
 * @function manageQuotationLineItems
 *
 * finds if any quotation line item exists if yes then remove all the quotation line item
 *
 * @param {object} basket The basket containing the elements to be computed
 */
function manageQuotationLineItems (basket) {

	var shipments = basket.getShipments().iterator();
	var havingQuotationLineItems = false;
	var havingRegularLineItems = false;
	//just making sure any line item exists
	while (shipments.hasNext()) {
		var shipment = shipments.next();
		// first we will find quotation and regular items of all the line items
		// of the shipment
		var shipmentLineItems = shipment.getAllLineItems().iterator();
		while (shipmentLineItems.hasNext()) {
			var lineItem = shipmentLineItems.next();
			if(lineItem instanceof dw.order.ProductLineItem ){
				if(lineItem.custom.isQuoteItem) {
					havingQuotationLineItems = true; 
				}
				else{
					havingRegularLineItems = true;
				}
			}
			
		}
	}

	//executing if it is required otherwise lets save resources and processing time
	if(havingQuotationLineItems && havingRegularLineItems){ //saving a loop if not needed
		
		
		//removing cart updates related to quotation
    	basket.custom.mfiQuotationId = null;
    	basket.custom.SalesPersonId = null;
    	basket.custom.CustomerId = null;
		
		var shipments = basket.getShipments().iterator();
		//lets manage the quotation items
		while (shipments.hasNext()) {
			var shipment = shipments.next();
			var shipmentLineItems = shipment.getAllLineItems().iterator();
			while (shipmentLineItems.hasNext()) {
				var lineItem = shipmentLineItems.next();
				if( lineItem instanceof dw.order.ProductLineItem && lineItem.custom.isQuoteItem && havingRegularLineItems ) {


					var createdPriceAdjustment = lineItem.getPriceAdjustmentByPromotionID(lineItem.productID + "-quote");
					if(createdPriceAdjustment != null){
						lineItem.removePriceAdjustment(createdPriceAdjustment);
					}
					lineItem.custom.isQuoteItem = null;
					lineItem.custom.quotePrice = null;
					lineItem.custom.quotediscount = null;
					if(('quoteQuantity' in lineItem.custom)){ //just make sure this custom attribute exists[bcz its only for tulo for now]
						lineItem.custom.quoteQuantity = null;
					}
				}

				
			}
		}
	}
}
/**
 * @function calculateProductPrices
 *
 * Calculates product prices based on line item quantities. Set calculates prices
 * on the product line items.  This updates the basket and returns nothing
 *
 * @param {Object} basket - The basket containing the elements to be computed
 */
function calculateProductPrices (basket) {
    // get total quantities for all products contained in the basket
    var productQuantities = basket.getProductQuantities();
    var productQuantitiesIt = productQuantities.keySet().iterator();

    // get product prices for the accumulated product quantities
    var productPrices = new HashMap();

    while (productQuantitiesIt.hasNext()) {
        var prod = productQuantitiesIt.next();
        var quantity = productQuantities.get(prod);
        productPrices.put(prod, prod.priceModel.getPrice(quantity));
    }

    // iterate all product line items of the basket and set prices
    var productLineItems = basket.getAllProductLineItems().iterator();
    while (productLineItems.hasNext()) {
        var productLineItem = productLineItems.next();

        // handle non-catalog products
        if (!productLineItem.catalogProduct) {
            productLineItem.setPriceValue(productLineItem.basePrice.valueOrNull);
            continue;
        }
        
        //set Temp Pedic Line Item price
        if (productLineItem.bonusProductLineItem && !empty(productLineItem.custom.isTempPedicBonusItem) && productLineItem.custom.isTempPedicBonusItem && !empty(productLineItem.custom.calculatedPrice)) {
        	productLineItem.setPriceValue(productLineItem.custom.calculatedPrice);
            continue;
        }
        
         //set tier price as shipping price
        if (productLineItem.custom.isDeliveryTier) { 
        	productLineItem.setPriceValue(productLineItem.basePrice.valueOrNull);
            continue;
        }

        var product = productLineItem.product;

        // handle option line items
        if (productLineItem.optionProductLineItem) {
            // for bonus option line items, we do not update the price
            // the price is set to 0.0 by the promotion engine
            if (!productLineItem.bonusProductLineItem) {
                productLineItem.updateOptionPrice();
            }
        // handle bundle line items, but only if they're not a bonus
        } else if (productLineItem.bundledProductLineItem) {
            // no price is set for bundled product line items
        // handle bonus line items
        // the promotion engine set the price of a bonus product to 0.0
        // we update this price here to the actual product price just to
        // provide the total customer savings in the storefront
        // we have to update the product price as well as the bonus adjustment
        } else if (productLineItem.bonusProductLineItem && product !== null) {
            var price = product.priceModel.price;
            var adjustedPrice = productLineItem.adjustedPrice;
            productLineItem.setPriceValue(price.valueOrNull);
            // get the product quantity
            var quantity2 = productLineItem.quantity;
            // we assume that a bonus line item has only one price adjustment
            var adjustments = productLineItem.priceAdjustments;
            if (!adjustments.isEmpty()) {
                var adjustment = adjustments.iterator().next();
                var adjustmentPrice = price.multiply(quantity2.value).multiply(-1.0).add(adjustedPrice);
                adjustment.setPriceValue(adjustmentPrice.valueOrNull);
            }


        // set the product price. Updates the 'basePrice' of the product line item,
        // and either the 'netPrice' or the 'grossPrice' based on the current taxation
        // policy

        // handle product line items unrelated to product
        } else if (product === null) {
            productLineItem.setPriceValue(null);
        // handle normal product line items
        } else {
           if(productLineItem.custom.isQuoteItem) {
				//quantity = productQuantities.get(productLineItem.product);
				//var quoteProductPrice = quantity*productLineItem.custom.quotePrice;
				//productLineItem.setPriceValue(quoteProductPrice);
				//productLineItem.setPriceValue(productPrices.get(product).valueOrNull);
				var itemPricingModel = ProductUtils.getPricing(productLineItem.product);
				productLineItem.setPriceValue(itemPricingModel.standardPriceMoney.valueOrNull);

				if(productLineItem.custom.quotediscount > 0) {
					if(productLineItem.custom.isAdjustmentCreated) {
						var createdPriceAdjustment = productLineItem.getPriceAdjustmentByPromotionID(productLineItem.productID + "-quote");
						productLineItem.removePriceAdjustment(createdPriceAdjustment);
						var adjustment = productLineItem.createPriceAdjustment(productLineItem.productID + "-quote");
						var adjustmentPrice = new dw.value.Money(productLineItem.custom.quotediscount, basket.getCurrencyCode());
						var adjustedAdjustmentPrice = adjustmentPrice.multiply((-1.0)*(productLineItem.quantityValue));
						adjustment.setPriceValue(adjustedAdjustmentPrice.valueOrNull);
						adjustment.setReasonCode("QUOTATION");
						adjustment.setManual(true);
						productLineItem.custom.isAdjustmentCreated = true;
					}
					else {
						var adjustment = productLineItem.createPriceAdjustment(productLineItem.productID + "-quote");
						var adjustmentPrice = new dw.value.Money(productLineItem.custom.quotediscount, basket.getCurrencyCode());
						var adjustedAdjustmentPrice = adjustmentPrice.multiply(-1.0);
						adjustment.setPriceValue(adjustedAdjustmentPrice.valueOrNull);
						adjustment.setReasonCode("QUOTATION");
						adjustment.setManual(true);
						productLineItem.custom.isAdjustmentCreated = true;
					}

				}
			}
			else {
				productLineItem.setPriceValue(productPrices.get(product).valueOrNull);
			}
        }
    }
}

/**
 * @function calculateGiftCertificates
 *
 * Function sets either the net or gross price attribute of all gift certificate
 * line items of the basket by using the gift certificate base price. It updates the basket in place.
 *
 * @param {Object} basket - The basket containing the gift certificates
 */
function calculateGiftCertificatePrices (basket) {
    var giftCertificates = basket.getGiftCertificateLineItems().iterator();
    while (giftCertificates.hasNext()) {
        var giftCertificate = giftCertificates.next();
        giftCertificate.setPriceValue(giftCertificate.basePrice.valueOrNull);
    }
}

/**
 * @function calculateTax
 *
 * Determines tax rates for all product line items of the basket
 *
 * @param {Object} basket - The basket containing the elements for which taxes need to be calculated
 * @param {Boolean} finalizeTaxRequest - Boolean value to trigger finalization when order is placed
 * @param {String} customerNo - Customer number that serves as an identifier of the customer.
 * @param {String} invoiceNo - The order number or basket Id if order is not yet created
 * @param {Object} customer - object storing all customer data
 * @param {Object} order - The order containing the elements for which taxes need to be calculated and update
 */
function calculateTax (basket, finalizeTaxRequest, customerNo, invoiceNo, order) {
    try {
    	//
    	// besides shipment line items, we need to calculate tax for possible order-level price adjustments
    	// this includes order-level shipping price adjustments
    	if (!basket.getPriceAdjustments().empty || !basket.getShippingPriceAdjustments().empty) {
    		// calculate a mix tax rate from
    		var basketPriceAdjustmentsTaxRate = (basket.getMerchandizeTotalGrossPrice().value / basket.getMerchandizeTotalNetPrice().value) - 1;

    		var basketPriceAdjustments = basket.getPriceAdjustments().iterator();
    		while (basketPriceAdjustments.hasNext()) {
    			var basketPriceAdjustment = basketPriceAdjustments.next();
    			basketPriceAdjustment.updateTax(basketPriceAdjustmentsTaxRate);
    		}

    		var basketShippingPriceAdjustments = basket.getShippingPriceAdjustments().iterator();
    		while (basketShippingPriceAdjustments.hasNext()) {
    			var basketShippingPriceAdjustment = basketShippingPriceAdjustments.next();
    			basketShippingPriceAdjustment.updateTax(basketPriceAdjustmentsTaxRate);
    		}
    	}
    	//
        var invoice = order ? order : basket,
            data = TaxRequest.calculate(invoice, finalizeTaxRequest, customerNo, invoiceNo),
            taxResponse = data.response;

        //Zero out all line items before individual update to be sure each line item includes taxes
        if(taxResponse !== 304) {
            zeroOutAllItemTax(basket);
        }

        // Update taxes if there is not error, the request is not 304, and the response is in the form of an object
        if(!data.error && taxResponse !== 304 && typeof taxResponse === 'object') {
            basket.custom.totalTax = taxResponse.TotalTax ? taxResponse.TotalTax : "0";
            Logger.getLogger("Suretax", "service").debug("Tax caclulated to - {0}", basket.custom.totalTax);

            //update each basket lineItem present in the tax response (identified by UUID) with corresponding tax amount
            updateAllItemTax(basket, taxResponse.GroupList);
        }

        //update order with CCH SureTax transaction information regardless of status
        if(order) {
            ConfigUtils.updateOrder(data, order);
        }
    }catch(e) {
        dwLogger.getLogger("Suretax", "service").error("[calculate.js] Exception Error in taxation request - {0}", e.message + " " + e.stack);
        return false;
    }
}

/**
 * @function zeroOutAllItemTax
 *
 * Set Tax for all Line items to 0
 *
 * @param {Object} basket - The basket containing the elements for which taxes need to be calculated
 */
function zeroOutAllItemTax(basket) {
    // Set Tax for all priceAdjustment Line item to 0
    var itemsCollection = basket.getAllLineItems();
    var items = itemsCollection.iterator();
    while (items.hasNext()) {
        item = items.next();
            item.updateTax(0);
    }
}

/**
 * @function updateLineItemTax
 *
 * Update each basket lineItem present in the tax response (identified by UUID) with corresponding tax amount
 *
 * @param {Object} basket - The basket containing the elements for which taxes need to be calculated
 * @param {Object} taxItems - Tax Item List from the response.
 */
function updateAllItemTax(basket, taxItems) {
    if(taxItems) {
        //update each basket lineItem present in the tax response (identified by UUID) with corresponding tax amount
        for (var i = 0; i < taxItems.length; i += 1) {
            var taxItem = taxItems[i],
                lineItem = getLineItemByUUID(basket, taxItem.LineNumber);

            if(lineItem) {
                updateLineItemTax(lineItem, taxItem, basket)
            }
        }
    }
}

/**
 * @function updateLineItemTax
 *
 * Update tax data of the lineItem provided
 *
 * @param {Object} li - Individual LineItem
 * @param {Object} taxItem - Individual tax item from response.
 * @param {Object} basket - The basket containing the elements for which taxes need to be calculated
 */
function updateLineItemTax(li, taxItem, basket) {
    var totalItemTax = 0;

    for (var k = 0; k < taxItem.TaxList.length; k += 1) {
        totalItemTax += Number(taxItem.TaxList[k].TaxAmount);
    }

    //Add total tax to the line item
    var totalItemTaxMoney = new Money(totalItemTax, basket.currencyCode);
    li.updateTaxAmount(totalItemTaxMoney);

    //Add total tax rate and tax basis to the line item
    if( li instanceof Order.ProductLineItem) {
    	/// strat 
    	//var billingAddress = basket.getBillingAddress();
    	var priceAdjustment: PriceAdjustment = li.getPriceAdjustmentByPromotionID("RECYCLING_FEE");
        var currentState = session.custom.addressState;
    	if(!empty(priceAdjustment) && currentState == 'CA') {//and condition add for CA
    		var eliminateRecycleFee = priceAdjustment.basePrice.value ? priceAdjustment.basePrice.value : 0;
    		var lineItemRevenue = li.proratedPrice.value - eliminateRecycleFee; 
    		var rate = getTaxRate(totalItemTax, new Money(lineItemRevenue, basket.currencyCode));
    		if (rate){
                li.updateTax(rate, new Money(lineItemRevenue, basket.currencyCode));
            }
    	}else {
    		var rate = getTaxRate(totalItemTax, li.getProratedPrice());
    		if (rate){
                li.updateTax(rate, li.getProratedPrice());
            }
    	}
    	/// end
    	//var rate = getTaxRate(totalItemTax, li.getProratedPrice());

    	//if (rate){
            //li.updateTax(rate, li.getProratedPrice());
        //}
    } else if (li instanceof Order.ShippingLineItem || li instanceof Order.ProductShippingLineItem) {
        var rate = getTaxRate(totalItemTax, li.getAdjustedPrice());

        if (rate){
            li.updateTax(rate, li.getAdjustedPrice());
        }
    }

}

/**
 * @function getTaxRate
 *
 * Calculate tax rate if provided price is greater than 0
 *
 * @param {Object} totalItemTax - The total line item tax returned by CCH
 * @param {Object} price - Money object to calculate tax rate
 */
function getTaxRate(totalItemTax, price){
    if (price > 0){
        return Number(totalItemTax / price)
    }
}

/**
* @function getLineItemByUUID
*
* Get line item from collection of items by the unique universal identifier
*
* @param {Object} basket - The basket containing the elements for which taxes need to be calculated
* @param {String} UUID - The unique universal identifier of the line item
* @return {Object} Lineitem object identified by UUID
*/
function getLineItemByUUID(basket, UUID) {
	var itemsCollection = basket.getAllLineItems();
    var items = itemsCollection.iterator();
   
	while (items.hasNext()) {
	    var lineItem = items.next();
	
	    if(lineItem.getUUID() == UUID ) {
	    	return lineItem;
	    }
	}
   return null;
}



function applyDiscounts(basket: LineItemCtnr) {
    let optionPrices: Array = new Array();
    let zeroPrice: Number = 0;
	var btsPromoStatus = false;
	var cyberPromoStatus = false;
    // Copy all product option prices to a temporary array and nullify them
    for each(let pli: ProductLineItem in basket.getAllProductLineItems()) {
        if (pli.optionProductLineItem && pli.parent) {
        	btsPromoStatus = !empty(pli.parent.product.custom.pdpEmailSigupPromoStatus) ? pli.parent.product.custom.pdpEmailSigupPromoStatus : false;
        	cyberPromoStatus = UtilFunctions.isCyberMondayPromotionProduct(pli.parent.product.ID) ? true : false;
            if (btsPromoStatus || cyberPromoStatus ) {
	        	optionPrices.push(pli.getPriceValue()/pli.getQuantityValue());
	            pli.setPriceValue(zeroPrice);
            }
        }
    }

    // Apply discounts
    PromotionMgr.applyDiscounts(basket);

    // Set all product option prices back to their original values
    let counter: Number = 0;
    for each(let pli: ProductLineItem in basket.getAllProductLineItems()) {

        if (pli.optionProductLineItem&& pli.parent) {
        	btsPromoStatus = !empty(pli.parent.product.custom.pdpEmailSigupPromoStatus) ? pli.parent.product.custom.pdpEmailSigupPromoStatus : false;
        	cyberPromoStatus = UtilFunctions.isCyberMondayPromotionProduct(pli.parent.product.ID) ? true : false;
            if (btsPromoStatus || cyberPromoStatus) {
	            pli.setPriceValue(optionPrices[counter]);
	            counter++;
            }
        }
    }
    basket.updateTotals();
}