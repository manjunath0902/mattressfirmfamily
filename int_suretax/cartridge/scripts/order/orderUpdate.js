"use strict";

/**
 * CCH Order Update Script to parse orders with SureTax update flag set to
 * either cancel or finalize the existing transaction
 *
 * @input Orders: dw.util.Iterator Orders with SureTax update flag set
 * @input OrderCount: Number of Orders with SureTax update flag set
 *
 */

importPackage(dw.util);

/* API CALLS */
var logger = require('dw/system/Logger').getRootLogger(),
    System = require('dw/system/System'),
    Site = require('dw/system/Site'),
    Order = require('dw/order/Order'),
    dwsvc = require("dw/svc"),
    RequestUtils = require("int_suretax/cartridge/scripts/util/RequestUtils"),
    ConfigUtils = new ( require("int_suretax/cartridge/scripts/util/ConfigUtils") );

/**
 * @function execute
 *
 * Function to be executed by script node of the SureTaxUpdate Pipeline
 *
 * @param {Object} args - Iterator of orders that match the jobs criteria
 */
function execute(args) {
    var orders = args.Orders,
        orderCount = args.OrderCount,
        successCount = 0;

    while (orders.hasNext()) {
        var order = orders.next(),
            transaction = triggerTransaction(order);

        if(transaction == true) {
            successCount += 1;
        }
    }
    logger.info(successCount + ' of ' + orderCount + ' orders updated successfully');
    return (successCount == orderCount) ? PIPELET_NEXT : PIPELET_ERROR;
}

/**
 * @function triggerTransaction
 *
 * Function triggers the transaction update based on the order data
 *
 * @param {Object} order - Iterator of orders that match the jobs criteria
 * @return {Boolean} True = Successful transactions; False = Failure
 */
function triggerTransaction(order) {
	//cancel tax transaction if order cancelled
	if(order.status == Order.ORDER_STATUS_CANCELLED){
		var updateType = 'CANCEL';
	}else{
		var updateType = order.custom.SureTaxUpdateFlag.value;
	}
	
	var taxStatus = order.custom.SureTaxStatus
	if(updateType == 'CANCEL' && taxStatus == ConfigUtils.constants.TAX_STATUS_QUOTE) {
		order.custom.SureTaxUpdateFlag = null;
		logger.error('Error updating transaction for Order # ' + order.orderNo + ': Quote transactions cannot be cancelled');
        return false;
	}else if(updateType == 'FINALIZE' && (taxStatus == ConfigUtils.constants.TAX_STATUS_FINALIZED || taxStatus == ConfigUtils.constants.TAX_STATUS_UPDATED)){
		order.custom.SureTaxUpdateFlag = null;
		logger.error('Error updating transaction for Order # ' + order.orderNo + ': This order is already finalized');
		return false;
	}
    

    try{
    	    	
        var service = getService(order, updateType);

        if(!service.error) {
            var responseJson = ConfigUtils.parseResponse(service),
                data = ConfigUtils.processResponse(responseJson);

            //update order status and log with data from response
            ConfigUtils.updateOrder(data, order, updateType);

            if (data.error) {
                logger.error('Error updating transaction for Order # ' + order.orderNo + ': '+ data.response.HeaderMessage +" - "+ data.response.ResponseCode);
                return false;
            } else {
                logger.info('Update transaction successful for Order # ' + order.orderNo);
                return true;
            }
        } else {
            logger.error('Error updating transaction for Order # ' + order.orderNo + ': ' + service.msg + " - " + service.errorMessage);
            return false;
        }
    }catch (err) {
        logger.error('Error updating transaction: '+ err.name + ': ' + err.message + ' ' + err.stack );
        return PIPELET_ERROR;
    }
}

/**
 * @function getService
 *
 * Function to return the service call
 * GetTax will be used if one of the following
 * -Update type is Finalized,
 * -If there is no Transaction Id and update type is Cancel. Service will complete a negative transaction
 * -If the order is older than 90 days and update type is Cancel. Service will complete a negative transaction
 *
 * If not of the above is true then a CancelTransaction call will be made.
 *
 * @param {Object} order - Iterator of orders that match the jobs criteria
 * @param {String} updateType - Update transaction type. CANCEL or FINALIZE
 * @return {Boolean} True = Successful transactions; False = Failure
 */
function getService(order, updateType) {
    var transId = order.custom.SureTaxTransactionID;

    if(updateType == "FINALIZE" || !transId || isOrderOlderThan(order, 90)) {

        // Load up service Utils to make request
        var service = dwsvc.ServiceRegistry.get("suretax.http.PostRequest"),
            serviceCreds = service.getConfiguration().getCredential();

        var requestBody = new RequestUtils(
            order,
            serviceCreds.getUser(),
            serviceCreds.getPassword(),
            true,
            order.getCustomerNo(),
            order.getOrderNo(),
            updateType
        ).buildRequest();

        return service.call({'request': requestBody});

    } else {
        return dwsvc.ServiceRegistry.get("suretax.http.CancelPostRequest").call({
            "transactionId": transId,
            "tracking": updateType
        });
    }
}

/**
 * @function isOrderOlderThan
 *
 * Function to check if Order passed is older than the number of days passed.
 *
 * @param {Object} order - Iterator of orders that match the jobs criteria
 * @param {Number} days - Number of days to check if order is older than
 * @return {Boolean} True = order is older than the days provided; False = Order is within days provided
 */
function isOrderOlderThan(order, days) {
    var currentDate = new Date().getTime(),
        orderDate = order.getCreationDate().getTime(),
        diff = Math.abs(currentDate - orderDate) /(1000 * 60 * 60 * 24);

    return (diff >= days);
}