'use strict';

/**
 * Class used to facilitate the service framework transactions for GetTax and CancelTransaction.
 * Includes service definitions and their instances.
 */

/* API utilties */
importPackage(dw.svc);
importPackage(dw.net);
importPackage(dw.io);

/* Script utilities */
var RequestUtils = require("int_suretax/cartridge/scripts/util/RequestUtils");

ServiceRegistry.configure("suretax.http.PostRequest", {

    createRequest: function(svc : HTTPService, params) {

        // Setup the basic POST HEADERS needed
        svc.setRequestMethod("POST");
        svc.addHeader("Content-Type", "application/json");

        var request = "{'request':'" + JSON.stringify(params.request) + "'}";
        return request;
    },

    parseResponse : function(svc : HTTPService, response : Object) {
        return response;
    },

    getRequestLogMessage: function (request : Object) {
        return JSON.stringify(request);
    },

    getResponseLogMessage: function (response : Object) {
        return JSON.stringify(response);
    },

    mockCall: function(svc : HTTPService, client : HTTPClient) {
        return {
            "Successful" : "Y",
            "ResponseCode" : 9999,
            "TotalTax": "0.50"
        };
    }
});

ServiceRegistry.configure("suretax.http.CancelPostRequest", {

    createRequest: function(svc : HTTPService, params) {
        // Setup the basic POST HEADERS needed
        svc.setRequestMethod("POST");
        svc.addHeader("Content-Type", "application/json");

        var requestBody = {
           ClientNumber: svc.getConfiguration().getCredential().getUser(),
           ClientTracking: params.tracking,
           TransId: params.transactionId,
           ValidationKey:svc.getConfiguration().getCredential().getPassword()
        };

        return "{'requestCancel':'" + JSON.stringify(requestBody) + "'}";
    },

    parseResponse : function(svc : HTTPService, response : Object) {
        return response;
    },

    getRequestLogMessage: function (request : Object) {
        return JSON.stringify(request);
    },

    getResponseLogMessage: function (response : Object) {
        return JSON.stringify(response);
    },

    mockCall: function(svc : HTTPService, client : HTTPClient) {
        return {
            "Successful" : "Y",
            "ResponseCode" : 9999,
        };
    }
});