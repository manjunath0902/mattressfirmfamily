/**
 * Class to handle the actual request build and call to CCH SureTax in order to determine tax
 * rates for all product and shipping line items of the given basket
 *
 *	@input Basket : dw.order.Basket
 *	@output ErrorMsg : String
 */

var dwsvc = require("dw/svc");
var dworder = require("dw/order");
var dwLogger = require("dw/system").Logger;
var RequestUtils = require("int_suretax/cartridge/scripts/util/RequestUtils");
var ConfigUtils = new ( require("int_suretax/cartridge/scripts/util/ConfigUtils") );
var MattressViewHelper = require("int_mattressc/cartridge/scripts/mattress/util/MattressViewHelper.ds");
/**
 * @function calculate
 *
 * Builds and makes request to CCH SureTax in order to determine tax rates for all product and
 * shipping line items of the basket
 *
 * @param {Object} basket - The basket containing the elements for which taxes need to be calculated
 * @param {Boolean} finalizeTaxRequest - Boolean value to trigger finalization when order is placed
 * @param {String} customerNo - Customer number that serves as an identifier of the customer.
 * @param {String} invoiceNo - The order number or basket Id if order is not yet created
 * @param {Object} customer - object storing all customer data
 */
exports.calculate = function (basket, finalizeTaxRequest, customerNo, invoiceNo) {

    try {
        // Can continue if basket and shipping address exists
        if(!canContinue(basket)) {
            return false;
        }

        var service = dwsvc.ServiceRegistry.get("suretax.http.PostRequest"),
            credentials = service.getConfiguration().getCredential(),
            requestObject = new RequestUtils(basket, credentials.getUser(), credentials.getPassword(), finalizeTaxRequest, customerNo, invoiceNo);

        // If not finalizing and is repeat request prevent call to service
        if(!finalizeTaxRequest && isRepeatRequest(basket, requestObject)) {
        	return ConfigUtils.processResponse(session.custom.sureTaxResponse);
        }else {
        	session.custom.sureTaxResponse = "";
            var requestBody = requestObject.buildRequest(),
               serviceCall = service.call({'request': requestBody});
               dwLogger.getLogger("Suretax", "service").debug("suretax request - {0}", JSON.stringify(requestBody));
            if(!empty(serviceCall) && !serviceCall.error) {
                var responseJson = ConfigUtils.parseResponse(serviceCall);
                session.custom.sureTaxResponse = responseJson;
                dwLogger.getLogger("Suretax", "service").debug("suretax response - {0}", JSON.stringify(responseJson));
                return ConfigUtils.processResponse(responseJson);
            } else {
                var message = serviceCall.msg + " - " + serviceCall.errorMessage;
                dwLogger.getLogger("Suretax", "service").error("[TaxRequest.js] Error in taxation request - {0}", message);
                return {
                    error: true,
                    response: {HeaderMessage: message, ResponseCode: ConfigUtils.constants.NO_RESPONSE}
                };
            }
           }

        
    }
    catch(e) {
		dwLogger.getLogger("Suretax", "service").error("[TaxRequest.js] Exception Error in taxation request - {0}", e.message + " " + e.stack);
        return {
            error: true,
            response:{HeaderMessage:e.message + " " + e.stack, ResponseCode: ConfigUtils.constants.NO_RESPONSE }
        };
	}
};

/**
 * @function canContinue
 *
 * Can continue with calculation if the there is a basket and there is a shipping method provided.
 *
 * @param {Object} basket - The basket containing the elements for which taxes need to be calculated
 */
function canContinue(basket) {
    // Don't calculate tax if no basket is present
    if (empty(basket) || (!(basket instanceof dworder.Basket) && !(basket instanceof dworder.Order))) {
        dwLogger.getLogger("Suretax", "service").error("[TaxRequest.js] No basket provided");
        return false;
    }else if (!basket.getDefaultShipment().getShippingAddress()) {
        return false;
    }
    return true;
}

/**
 * @function isRepeatRequest
 *
 * Check to see if the current request is just a re-calculation of the same cart/basket.
 * This is a re-calculation of the same cart if the line items and zip doesn't change.
 *
 * @param {Object} basket - The basket containing the elements for which taxes need to be calculated
 * @param {Object} requestObject - Instance of the RequestUtils class
 * @return {Boolean}
 */
function isRepeatRequest(basket, requestObject) {
	//do not do anything else if the basket is instace of order
	if(basket instanceof dworder.Order) {
		return false;
	}
	
    // pull in the item request to match up
    var requestString = JSON.stringify(requestObject.buildItemListJson());
    var requestZipcode = parseInt(requestObject.buildShipToAddressJson().PostalCode);
    var shippingAddress = basket.getDefaultShipment().getShippingAddress();
    //var multiShipValue = session.forms.multishipping.entered.value;
    //var pickupInStoreId = session.custom.storeId;
 // get the hash for transactionModel
	var stateStr =  getCartStateStr(basket);
	// Check if the cartStateString in session is the same as the newly calculated cartStateString.
    // If the strings are the same, then the cart has not changed and tax calculation will be skipped
   
    if(basket.custom.suretaxRequest === stateStr && requestZipcode === parseInt(shippingAddress.getPostalCode())) {
    	dwLogger.getLogger("Suretax", "service").info("CartStateStrings (Match): ( Last Req: {0}----Current Request: {1} )",basket.custom.suretaxRequest, stateStr);
        return true;
    }else {
    	dwLogger.getLogger("Suretax", "service").info("CartStateStrings (UnMatch): ( Last Req: {0}----Current Request: {1} )",basket.custom.suretaxRequest, stateStr);
        basket.custom.suretaxRequest = stateStr;
        return false;
    }
}

function getCartStateStr(basket) {
    var cartStateString = "";
    //Surcharge Means our Recycle fee & shipping method.
    var recyleFees = false;

    var productLineItems = basket.getAllProductLineItems().iterator();
    while(productLineItems.hasNext()) {
        var productLineItem = productLineItems.next();
        if(MattressViewHelper.hasRecycleFees(productLineItem)) {
        	recyleFees = true;
        }
        cartStateString += productLineItem.productID +";"+ productLineItem.quantityValue +";"+ productLineItem.adjustedPrice + "|";
    }

    // Append shipping totals and basket totals to string (adjustedMerchandizeTotalPrice includes order level price adjustments). Basket Net total checked as catch all for both taxation policies not including taxe.

    cartStateString += basket.adjustedShippingTotalPrice.valueOrNull + "|" 
    				 + basket.adjustedMerchandizeTotalPrice.valueOrNull + "|" 
    				 + basket.totalNetPrice.valueOrNull + "|" 
    				 + basket.defaultShipment.shippingAddress.stateCode + "|" 
    				 + (!empty(basket.defaultShipment.shippingAddress.city)? basket.defaultShipment.shippingAddress.city.toLowerCase() : "") + "|" 
    				 + basket.defaultShipment.shippingAddress.countryCode + "|" 
    				 + basket.defaultShipment.shippingAddress.postalCode + "|"
    				 + recyleFees;

    return cartStateString;
}