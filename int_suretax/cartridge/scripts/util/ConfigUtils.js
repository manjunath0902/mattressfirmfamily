"use strict";

/**
 * Lightweight helper class where all settings and constants can are defined and grouped
 * together so that they can be used within other classes. Class also contain helper methods
 * that can be used within other classes to access and manipulate data
 */

/* Script Modules */
var site = require('dw/system/Site');
var dwLogger = require("dw/system").Logger;

/**
 * Lightweight helper class where all settings can be defined and
 * grouped together so that they can be pulled in anywhere and used.
 * Class also contain helper methods that can be used to access data
 *
 * @returns {Object} Settings object with all current configuration values and helper methods
 */
var ConfigUtils = function () {

    this.currentSite = site.getCurrent();
    this.constants = {
        TAX_STATUS_QUOTE: "QUOTE",
        TAX_STATUS_FINALIZED: "FINALIZED",
        TAX_STATUS_FAILED: "FAILED",
        TAX_STATUS_CANCELLED: "UPDATED-CANCELLED",
        TAX_STATUS_UPDATED: "UPDATED-FINALIZED",
        NO_EXEMPTION_CODE: "00",
        SUCCESSFUL_RESPONSE_CODE: '9999',
        NO_RESPONSE: 'No Response Available'
    };

    this.settings = {
        isEnabled: false,
    };

    if (this.currentSite != undefined) {
        this.settings = {
            isEnabled: this.getConfig('STEnable'),
            constants: this.constants,
            GlobalBusinessUnit: this.getConfig('GlobalBusinessUnit'),
            ProviderType: this.getConfig('ProviderType'),
            Finalize: this.getConfig('Finalize'),
            TaxCalculationOption: this.getConfig('TaxCalculationOption').value,
            TransTypeCode: this.getConfig('TransTypeCode'),
            ShippingTransTypeCode: this.getConfig('ShippingTransTypeCode'),
            SalesTypeCode: this.getConfig('SalesTypeCode').value,
            GlobalExemptionCode: this.getConfig('GlobalExemptionCode').value,
            GlobalExemptReasonCode: this.getConfig('GobalExemptReasonCode').value,
            ShipFromAddress: this.getConfig('SureTaxShipFromAddress'),
            ShipFromAddress2: this.getConfig('SureTaxShipFromAddress2'),
            ShipFromCity: this.getConfig('SureTaxShipFromCity'),
            ShipFromCountry: this.getConfig('SureTaxShipFromCountryCode'),
            ShipFromState: this.getConfig('SureTaxShipFromStateCode'),
            ShipFromZip: this.getConfig('SureTaxShipFromZipCode').substring(0,5),
            noExemption: "00"
        };
    }

    return this;
};

/**
 * @function getConfig
 * Helper method to return custom preference value.
 *
 * @param {String} value - Config value to retrieve
 * @returns {String|Object} - preference value returned.
 */
ConfigUtils.prototype.getConfig = function(value) {
    return this.currentSite.getCustomPreferenceValue(value);
};

/**
 * @function getTaxStatus
 *
 * Helper method to return the order tax status of the recent transaction.
 *
 * @param {String} responseCode - Response code returned from CCH SureTax
 * @returns {String} - The order's tax status.
 * @param {String} updateType - String to identify update type if it exists
 */
ConfigUtils.prototype.getTaxStatus = function(responseCode, updateType) {
    if(responseCode == this.constants.SUCCESSFUL_RESPONSE_CODE) {
        if(updateType) {
            return (updateType == 'CANCEL') ? this.constants.TAX_STATUS_CANCELLED : this.constants.TAX_STATUS_UPDATED;
        }else {
            return this.settings.Finalize ? this.constants.TAX_STATUS_FINALIZED : this.constants.TAX_STATUS_QUOTE;
        }

    } else {
        //if update type exist then do not update the order status if the transaction failed. Order should keep current status
        return updateType ? null : this.constants.TAX_STATUS_FAILED;
    }
};

/**
 * @function parseResponse
 *
 * Return parsed Json data from the response.
 *
 * @param {Object} service - The service transaction data post making the request
 */
ConfigUtils.prototype.parseResponse = function(service) {
    var response = JSON.parse(service.object.text);
    return JSON.parse(response.d);
};

/**
 * @function processResponse
 *
 * Process response and handle logging
 *
 * @param {Object} responseJson - Response Json from CCH
 */
ConfigUtils.prototype.processResponse = function(responseJson) {
    if(responseJson.ResponseCode === this.constants.SUCCESSFUL_RESPONSE_CODE) {
        //dwLogger.getLogger("Suretax", "service").debug("[ConfigUtils.js] Successful taxation request - {0}", JSON.stringify(responseJson));
        return {error: false, response:responseJson };
    } else {
        dwLogger.getLogger("Suretax", "service").error("[ConfigUtils.js] Error in taxation request - {0}", responseJson.HeaderMessage);
        dwLogger.getLogger("Suretax", "service").debug("[ConfigUtils.js] API Request response data - {0}", JSON.stringify(responseJson));
        return {error: true, response:responseJson };
    }
};

/**
 * @function updateOrder
 *
 * Update order data with information form the CCH response
 *
 * @param {Object} data - Data object containing error status and response info
 * @param {Object} order - The order containing the elements for which taxes where calculated
 * @param {String} updateType - String to identify update type if it exists
 */
ConfigUtils.prototype.updateOrder = function(data, order, updateType) {
    var transId = data.response.TransId ? data.response.TransId : null,
        status = this.getTaxStatus(data.response.ResponseCode, updateType);

    //only update the order status if it exist. Instance when status doesn't exist is if a secondary transaction fails. In that case order status remains the same.
    if(status) {
        order.custom.SureTaxStatus = status;
        order.custom.SureTaxTransactionID = transId;

        //set update flag to updated to indicate update transaction already completed
        if(updateType) {
            order.custom.SureTaxUpdateFlag = null;
        }
    } else if (updateType && (order.custom.SureTaxStatus == this.constants.TAX_STATUS_CANCELLED || order.custom.SureTaxStatus == this.constants.TAX_STATUS_UPDATED)){
        order.custom.SureTaxUpdateFlag = null;
    }

    order.custom.SureTaxTransactionNotes = this.buildMessage(data, updateType)
};

/**
 * @function buildMessage
 *
 * Build message string to be stored on order for CCH SureTax Transaction Notes
 *
 * @param {Object} data - Data object containing error status and response info
 * @param {String} updateType - String to identify update type if it exists
 */
ConfigUtils.prototype.buildMessage = function(data, updateType) {
    var responseCode = data.response.ResponseCode,
        headerMessage = data.response.HeaderMessage +" - "+ responseCode;

    if(updateType) {
        headerMessage = updateType +' Transaction Update ' +headerMessage
    }

    if(updateType == 'CANCEL' && data.response.TransId) {
        return headerMessage + ' (Completed via negative transaction)'
    }else if(data.error || updateType == 'CANCEL') {
        return headerMessage;
    } else {
        return headerMessage + " Total Calculated tax: " + data.response.TotalTax;
    }
};

module.exports = ConfigUtils;