/**
 * Request utilities class used to manipulate data provide into a JSON Request
 * ready to be ingested by CCH SureTax.
 */

importPackage(dw.system);
importPackage(dw.util);

var Order = require('dw/order');
var CustomObjectManager = require('dw/object/CustomObjectMgr');
var ConfigUtils = new ( require("int_suretax/cartridge/scripts/util/ConfigUtils") );
var adjustmentPromotionId : String = "RECYCLING_FEE";
var RequestUtils = function(basket, APIuser, APIpassword, finalizeTransaction, customerNo, orderNo, updateType) {
    this.init(basket, APIuser, APIpassword, finalizeTransaction, customerNo, orderNo, updateType);

    return this;
};

/**
 * @function init
 *
 * Function to initialize the RequestUtils prototype
 *
 * @param {Object} basket - The basket or order containing the elements for which taxes need to be calculated
 * @param {String} APIuser - Client Number returned as the api user from the service framework
 * @param {String} APIpassword - Validation Key returned as the api password from the service framework
 * @param {Boolean} finalizeTransaction - Boolean value to trigger finalization when order is placed
 * @param {String} customerNo - Customer number that serves as an identifier of the customer.
 * @param {String} orderNo - The order number or basket Id if order is not yet created
 * @param {String} updateType - String value only passed when doing a transaction update via a job
 */
RequestUtils.prototype.init = function(basket, APIuser, APIpassword, finalizeTransaction, customerNo, orderNo, updateType) {
    this.basket = basket; //Note: this basket value can be either an instance of an Order or a Basket
    this.APIuser = APIuser;
    this.APIpassword = APIpassword;
    this.finalizeTransaction = finalizeTransaction;
    this.returnFileCode = finalizeTransaction ? "0" : "Q";
    this.customer = this.basket.getCustomer();
    this.customerProfile = this.customer ? this.customer.getProfile() : null;
    this.customerGroups =  this.customer ? this.customer.getCustomerGroups() : null;
    this.customerNo = (customerNo != null && customerNo.length > 0) ? customerNo : this.customer.getID();
    this.orderNo = (orderNo != null && orderNo.length > 0) ? orderNo : this.customerNo;
    this.exemptionData = this.getExemptionData();
    this.exemptionCode = this.exemptionData['code'] ? [this.exemptionData['code']] : '';
    this.exemptionReason = (this.exemptionData['code'] && this.exemptionData['reason']) ? this.exemptionData['reason'] : '';
    this.updateType = updateType ? updateType : null;
    this.request;
};

/**
 * @function buildRequest
 *
 * Builds out the Json request
 *
 * @return {Object} request - Json object to be sent to CCH SureTax for tax calculation
 */
RequestUtils.prototype.buildRequest = function() {
    var self = this;
    self.request = {};

    self.buildClientAutoJson();
    self.buildItemListJson();

    //If order exists then update order with exemption data
    self.updateExemptionData();

    return self.request;
};

/**
 * @function getClientAuthNodes
 *
 * Builds out the Client Authorization object to be used in building the request
 *
 * @return {Object} clientAuthNodes - Json object containing authorization information to be used within the overall request
 */
RequestUtils.prototype.getClientAuthNodes = function() {
    var self = this,
        totalRevenue = self.basket.getAdjustedMerchandizeTotalPrice().value + self.basket.getAdjustedShippingTotalPrice().value,
        tracking = self.finalizeTransaction ? self.orderNo : "Quote - " + self.orderNo,
        clientAuthNodes = {
            "ClientNumber": self.APIuser,
            "BusinessUnit": ConfigUtils.settings.GlobalBusinessUnit,
            "ValidationKey": self.APIpassword,
            "DataYear": self.getFullYear().toString(),
            "DataMonth": self.getMonth(),
            "CmplDataYear": self.getFullYear().toString(),
            "CmplDataMonth": self.getMonth(),
            "TotalRevenue": self.updatedRevenue(totalRevenue),
            "ReturnFileCode": self.returnFileCode,
            "ClientTracking": tracking,
            "ResponseType": "D2",
            "ResponseGroup": "00",
            "STAN": ""
        };

    return clientAuthNodes;
};

/**
 * @function buildClientAutoJson
 *
 * Adds Client Authorization node to the request JSON
 */
RequestUtils.prototype.buildClientAutoJson = function() {
    var self = this;
    var clientAuthNodes = self.getClientAuthNodes();

    Object.keys(clientAuthNodes).forEach(function (key) {
        self.addJsonNode(self.request, key, clientAuthNodes[key]);
    });
};

/**
 * @function getTrimmedZipCode
 *
 * Trim zipcode in case a 10 digit zipcode is provided for US addresses. Allow SureAddress to provide exact plus 4
 * @param {Order.OrderAddress} address
 */
RequestUtils.prototype.getTrimmedZipCode = function(address) {
    var countryCode = address.getCountryCode().getValue().toUpperCase(),
        zipCode = address.getPostalCode();

    if(countryCode == 'US' && !empty(zipCode) && zipCode.length > 5) {
        return zipCode.substring(0,5);
    } else {
        return zipCode;
    }
};

/**
 * @function buildBillingAddressJson
 *
 * Builds out and adds the BillingAddress node to the request JSON
 *
 * @return {Object} request - Json object to be sent to CCH SureTax for tax calculation
 */
RequestUtils.prototype.buildBillingAddressJson = function() {
    var self = this,
        billing = self.basket.getBillingAddress();

    if(billing) {
        var addressNode = {
            "PrimaryAddressLine": billing.getAddress1(),
            "SecondaryAddressLine": billing.getAddress2() ? billing.getAddress2() : "",
            "County": "",
            "City": billing.getCity(),
            "State": billing.getStateCode(),
            "PostalCode": self.getTrimmedZipCode(billing),
            "Plus4": "",
            "Country": billing.getCountryCode().toUpperCase(),
            "VerifyAddress": "true"
        };

        self.request["BillingAddress"] = addressNode;
    }
    return self.request;
};

/**
 * @function buildShipToAddressJson
 *
 * Builds out the ShipToAddress node to be added into the request JSON. Data comes from the shipping address on the basket
 *
 * @param {Order.OrderAddress} shipto - Shipping Address form shipment assigned to productLineItems
 * @return {Object} addressNode - Json object to be used within the overall request
 */
RequestUtils.prototype.buildShipToAddressJson = function(shipto) {
    var self = this,
        shipTo = (typeof shipto !== 'undefined') ?  shipto : self.basket.getDefaultShipment().getShippingAddress();

    if(shipTo) {
        var addressNode = {
            "PrimaryAddressLine": shipTo.getAddress1(),
            "SecondaryAddressLine": shipTo.getAddress2() ? shipTo.getAddress2() : "",
            "County": "",
            "City": shipTo.getCity(),
            "State": shipTo.getStateCode(),
            "PostalCode": self.getTrimmedZipCode(shipTo),
            "Plus4": "",
            "Country": shipTo.getCountryCode().getValue().toUpperCase(),
            "VerifyAddress": "true"
        };

        return addressNode
    }
};

/**
 * @function buildShipFromAddressJson
 *
 * Builds out and adds the ShipFromAddress node to the request JSON. Data comes from configuration
 *
 * @return {Object} addressNode - Json object to be used within the overall request
 */
RequestUtils.prototype.buildShipFromAddressJson = function() {
    var address = ConfigUtils.settings;
    var addressNode = {
        "PrimaryAddressLine": address.ShipFromAddress,
        "SecondaryAddressLine": address.ShipFromAddress2 ? address.ShipFromAddress2: "",
        "County": "",
        "City": address.ShipFromCity,
        "State": address.ShipFromState,
        "PostalCode": address.ShipFromZip,
        "Plus4": "",
        "Country": address.ShipFromCountry.toUpperCase(),
        "VerifyAddress": "true"
    };

    return addressNode;
};

/**
 * @function getWarehouseId
 *
 * Builds out ship form request JSON node
 *
 * @return {Object} ware house address - 
 */

RequestUtils.prototype.getWarehouseId = function(address) {
    var warehouseId = null;
    var zipInfoWarehouseIds = new Array();

    //get customer zip code from customer address
    var shipToPostalCode = (!empty(address) && !empty(address.PostalCode)) ? address.PostalCode : session.custom.customerZip;

    //get warehouse Id by zipcode from zipInfo custom object
    var queryString = 'custom.zip_code = {0}';
    var zipInfoObj = CustomObjectManager.queryCustomObject('ZipInfo', queryString, shipToPostalCode.toString());
    if (!empty(zipInfoObj) && !empty(zipInfoObj.custom) && 'warehouseId' in zipInfoObj.custom) {
        zipInfoWarehouseIds = zipInfoObj.custom.warehouseId;
    }

    if (empty(zipInfoWarehouseIds) || zipInfoWarehouseIds.length < 1) {
        return null;
    } else {
        var sureTaxWareHouseAddresses = dw.system.Site.getCurrent().getCustomPreferenceValue('SureTaxWareHouseAddresses');
        var sureTaxWareHouseAddressesList = JSON.parse(sureTaxWareHouseAddresses);
        var warehouseAddress = null;
        
        if (empty(sureTaxWareHouseAddressesList) || sureTaxWareHouseAddressesList.length < 1) {
            return null;
        }else {
        	for each(var zipInfoWarehouseId in zipInfoWarehouseIds) {
                for each(var item in sureTaxWareHouseAddressesList) {
                    if (item.WarehouseID == zipInfoWarehouseId) {
                        var warehouseAddress = new Object();

                        warehouseAddress.PrimaryAddressLine = item.PrimaryAddressLine;
                        warehouseAddress.SecondaryAddressLine = "";
                        warehouseAddress.County = "";
                        warehouseAddress.City = item.City;
                        warehouseAddress.State = item.State;
                        warehouseAddress.PostalCode = item.PostalCode;
                        warehouseAddress.Plus4 = "";
                        warehouseAddress.Country = "US";
                        warehouseAddress.VerifyAddress = "true";
                        return warehouseAddress;
                    }
                }
            }
        }        
        return warehouseAddress;
    }
}



/**
 * @function buildItemListJson
 *
 * Builds out and adds the ItemList node to the request JSON
 *
 * @return {Object} request - Json object to be sent to CCH SureTax for tax calculation
 */
RequestUtils.prototype.buildItemListJson = function() {
    //get all of the shipments from the basket
    var self = this,
        shipments = self.basket.getShipments().iterator(),
        shipFrom = self.buildShipFromAddressJson();

    // Check the request object
    if(typeof self.request !== "object") {
        self.request = {};
    }

    // Set the item list JSON node
    self.request["ItemList"] = [];

    //loop through shipments to properly match up the shipping address with the correct products and shipping line items
    while (shipments.hasNext()) {
        var shipment = shipments.next(),
            shipmentType = shipment.custom.shipmentType;

        //skip if the current shipment do not have any products line items.
        if (empty(shipment.getProductLineItems())){
            continue;
        }

        var lis = shipment.getAllLineItems().iterator(),
            address = self.buildShipToAddressJson(shipment.getShippingAddress());

        //loop through line items of current shipment
        while (lis.hasNext()) {
            var li = lis.next();

            //Create item for everything except Gift Certificates
            if (this.isTaxableLineItem(li)) {
                var item = {
                    "LineNumber": li.getUUID(),
                    "InvoiceNumber": self.orderNo,
                    "CustomerNumber": self.customerNo,
                    "BillToNumber": "",
                    "OrigNumber": "",
                    "TermNumber": "",
                    "TransDate": self.buildDate(),
                    "UnitType": "00",
                    "Seconds": 1,
                    "TaxIncludedCode": "0",
                    "TaxSitusRule": "22",
                    "TransTypeCode": self.getTransTypeCode(li),
                    "SalesTypeCode": self.getSalesTypeCode(),
                    "RegulatoryCode": ConfigUtils.settings.ProviderType,
                    "TaxExemptionCodeList": self.exemptionCode,
                    "ExemptReasonCode": self.exemptionReason,
                    "UDF2": "17.3.0",
                    "FreightOnBoard": "",
                    "ShipFromPOB": "1",
                    "MailOrder": "1",
                    "CommonCarrier": "1",
                    "AuxRevenue": "0",
                    "AuxRevenueType": "01",
                    "P2PZipcode": "",
                    "Zipcode": ""
                };

                if(this.isShippingLineItemType(li)) {
                    item["Revenue"] = self.updatedRevenue(li.adjustedPrice.value);
                    item["Units"] = 1;
                    item['UDF'] = 'Freight';
                    //TODO CommonCarrier and FreightOnBoard needs to be added here
                }else {
                	
                   var priceAdjustment: PriceAdjustment = li.getPriceAdjustmentByPromotionID("RECYCLING_FEE");
                   session.custom.addressState = address.State;
                   if(!empty(priceAdjustment) && (address.State == 'CA')) {                	  
                      var eliminateRecycleFee = priceAdjustment.basePrice.value ? priceAdjustment.basePrice.value : 0;
                      var lineItemRevenue = li.proratedPrice.value - eliminateRecycleFee;
                      item["Revenue"] = self.updatedRevenue(lineItemRevenue);
                      item["Units"] = li.quantity.value;
                      item['UDF'] = li.getProductID();
                    }else {
                    	item["Revenue"] = self.updatedRevenue(li.proratedPrice.value);
                        item["Units"] = li.quantity.value;
                        item['UDF'] = li.getProductID();
                    }

                	
                    
                    
                    
                    
                }
                
                //add in ShipToAddress before we push the item into the item list object
                item["ShipToAddress"] = address;
                
                //get shipfrom address from warehouse preferences
                var shipFromBM = self.getWarehouseId(address);                

                //If item is Pick Up In Store item then ShipFromAddress ia the same address as ShipToAddress else it is configurations
                if(!empty(shipFromBM)) {
                	item["ShipFromAddress"] = shipFromBM;
                }else {
                	item["ShipFromAddress"] = (shipmentType && shipmentType == 'instore') ? address : shipFrom;
                }
                

                self.request["ItemList"].push(item);
            }
        }
    }

    return self.request;
};

/**
 *@function  updatedRevenue
 *
 * Returns negative version of the revenue if the updateType is cancel else return normal value
 *
 * @param {String} value Revenue value of li item
 * @return {String} value Revenue value of li item
 */
RequestUtils.prototype.updatedRevenue = function(value) {
    return (this.updateType == 'CANCEL' && value > 0) ? '-'+ value : value;
};

/**
 *@function  getTransTypeCode
 *
 * Returns the value to use as TransTypeCode. Value returned based on TaxCalculationOption value
 * - If ID is the TaxCalculationOption, return the Product ID for product line items and the shipping TransTypeCode from
 * Configurations
 * - If tax taxClassID is the TaxCalculationOption, return the taxClassID of the Product (for product line items)
 * and taxClassID of Shipping method (Shipping Line Item). If none set fall back to TransTypeCode in configuration
 * - Default to configuration for anything else
 *
 * @param {Object} li Order or basket line item
 * @return {String} TransTypeCode, SKU/ID, or Tax Class ID
 */
RequestUtils.prototype.getTransTypeCode = function(li) {
    var self = this,
        option = ConfigUtils.settings.TaxCalculationOption,
        isShipping =self.isShippingLineItemType(li);

    if (option == 'ID' && !isShipping) {
        return li.getProductID();
    }else if (option == 'taxClassID') {
        // If the tax class is set for the item
        if(li.getTaxClassID()) {
            //if item is taxable then return tax class. if the item is a product option then return the optionValueID
            if (isShipping) {
                return li.getTaxClassID();
            } else if (self.isTaxableLineItem(li)) {
                return li.optionID ? li.getProductID() : li.getTaxClassID();
            }
        } else {
            // if no Tax Class is set for this line return productID or shipping TransTypeCode (tax class can be set to undefined)
            return isShipping ? ConfigUtils.settings.ShippingTransTypeCode : li.getProductID();
        }
    }

    //if shipping line item return shipping TransTypeCode from config
    if (isShipping) {
        return ConfigUtils.settings.ShippingTransTypeCode;
    }
    //all else fails return TransTypeCode from configuration
    return ConfigUtils.settings.TransTypeCode;
};

/**
 * @function getSalesTypeCode
 *
 * Returns the SalesTypeCode based on the following order of precedent
 * - Return value if it exist on the Customer
 * - Return value of the first Customer Group that has the value set and is not the same as global configuration.
 * - Default to configuration
 *
 * @return {String} salesType
 */
RequestUtils.prototype.getSalesTypeCode = function() {
    var self = this,
        customerSalesTypeCode= (self.customerProfile && self.customerProfile.custom.SalesTypeCode) ? self.customerProfile.custom.SalesTypeCode.value : null,
        groups = self.customerGroups,
        configuration = ConfigUtils.settings.SalesTypeCode;

    if (customerSalesTypeCode) {
        return customerSalesTypeCode;
    }else if (groups) {
        groups = groups.iterator();
        while (groups.hasNext()) {
            var group = groups.next(),
                salesType= group.custom.SalesTypeCode;

            if (salesType && salesType.value && salesType.value != configuration) {
                return salesType.value;
            }
        }
        return configuration;
   }
   return configuration;
};

/**
 * @function getExemptionData
 *
 * Returns the ExemptionCode and ExemptionReasonCode based on the following order of precedent
 * - Return value if it exist on the Customer regardless of exemption status
 * - Return value of the first Customer Group that is exempted.
 * - Default to configuration
 *
 * @return {Object} exemptionData
 */
RequestUtils.prototype.getExemptionData = function() {
    var self = this,
        customerExemptCode = (self.customerProfile && self.customerProfile.custom.CustomerExemptionCode) ? self.customerProfile.custom.CustomerExemptionCode.value : null,
        groups = self.customerGroups;

    if(this.updateType) {
        var reason = self.basket.custom.SureTaxExemptionReasonCode.value;
       return {
           'code': self.basket.custom.SureTaxExemptionCode.value,
           'reason': reason ? reason : ''
       };
    } else if (customerExemptCode) {
        return self.getCustomerExemptionData(customerExemptCode);
    } else if (groups) {
        return self.getGroupExemptionData(groups);
    }
    return self.getGlobalExemptionData();
};

/**
 * @function getGlobalExemptionData
 *
 * Returns the ExemptionCode and ExemptionReasonCode from configuration
 *
 * @return {Object} exemptionData
 */
RequestUtils.prototype.getGlobalExemptionData = function() {
    var exemptionData = {},
        exemptionCode = ConfigUtils.settings.GlobalExemptionCode,
        exemptionReason = ConfigUtils.settings.GobalExemptReasonCode,
        noExemption = ConfigUtils.settings.constants.NO_EXEMPTION_CODE;

    exemptionData['code'] = exemptionCode;
    exemptionData['reason'] = ((exemptionCode && exemptionCode != noExemption) && exemptionReason !='none') ? exemptionReason.value : "";

    return exemptionData;
};

/**
 * @function getCustomerExemptionData
 *
 * Returns the ExemptionCode and ExemptionReasonCode from the customer
 *
 * @param {String} customerExemptCode Exemption code value from customer
 * @return {Object} exemptionData
 */
RequestUtils.prototype.getCustomerExemptionData = function(customerExemptCode) {
    var self = this,
        exemptionData = {},
        noExemption = ConfigUtils.settings.constants.NO_EXEMPTION_CODE,
        exemptionReason = self.customerProfile.custom.CustomerExemptReasonCode;

    exemptionData['code'] = customerExemptCode;
    exemptionData['reason'] = (customerExemptCode != noExemption && exemptionReason && exemptionReason.value != 'none') ? exemptionReason.value : "";

    return exemptionData;
};

/**
 * @function getGroupExemptionData
 *
 * Returns the ExemptionCode and ExemptionReasonCode from the customer group
 *
 * @param {Object} groups Customer groups associated to the customer
 * @return {Object} exemptionData
 */
RequestUtils.prototype.getGroupExemptionData = function(groups) {
    var self = this,
        exemptionData = {};

    groups = groups.iterator();
    while (groups.hasNext()) {
        var group = groups.next(),
            noExemption = ConfigUtils.settings.constants.NO_EXEMPTION_CODE,
            exemptionCode = group.custom.GroupExemptionCode,
            exemptionReason = group.custom.GroupExemptReasonCode;

        if (exemptionCode && exemptionCode.value && exemptionCode.value != noExemption) {
            exemptionData['code'] = exemptionCode.value;
            exemptionData['reason'] = (exemptionReason && exemptionReason.value != 'none') ? exemptionReason.value : "";
            return exemptionData;
        }
    }

    return self.getGlobalExemptionData();
};

/**
 * @function isTaxableLineItem
 *
 * Check if line item is either ProductLineItem, ShippingLineItem, or a ProductShippingLineItem
 *
 * @param {Object} li
 * @return {Boolean}
 */
RequestUtils.prototype.isTaxableLineItem = function(li) {
    return (li instanceof Order.ProductLineItem || li instanceof Order.ShippingLineItem || li instanceof Order.ProductShippingLineItem);
};

/**
 * @function isShippingLineItemType
 *
 * Check if line item is either a ShippingLineItem or a ProductShippingLineItem
 *
 * @param {Object} li
 * @return {Boolean}
 */
RequestUtils.prototype.isShippingLineItemType = function(li) {
    return (li instanceof Order.ShippingLineItem || li instanceof Order.ProductShippingLineItem);
}

/**
 * @function updateExemptionData
 *
 * Updates exemption data on the order if it exists. Only to be used when updating previously created transaction
 * Fail safe to maintain order exemption data in case the configurations change.
 *
 */
RequestUtils.prototype.updateExemptionData = function() {
    var self = this;

    if(self.basket instanceof Order.Order && self.updateType == null) {
        self.basket.custom.SureTaxExemptionCode = self.exemptionCode;
        self.basket.custom.SureTaxExemptionReasonCode = self.exemptionReason;
    }
};

/**
 * @function addJsonNode
 *
 * Helper function add json nodes
 *
 * @param {Object} parentNode - Json object
 * @param {String} nodeName - Name to identify node to be added to Json object
 * @parap nodeValue - Value of node to be add to Json Object. Value can be String or Object
 */
RequestUtils.prototype.addJsonNode = function(parentNode, nodeName, nodeValue) {
    parentNode[nodeName] = nodeValue;
};

/**
 * @function padNumber
 *
 * Helper function to add in leading zeros to numbers less than ten
 *
 * @param {String} number
 * @return {String} String value with or without leading zero
 */
RequestUtils.prototype.padNumber = function(number) {
    return (number < 10 ? '0' : '') + number
};

/**
 * @function getMonth
 *
 * Helper function to get basket creation month while adding in leading zero.
 * Note: months in SFCC returns 0-11
 *
 * @return {String} Adjusted month value
 */
RequestUtils.prototype.getMonth = function() {
    return this.padNumber(this.basket.getCreationDate().getMonth() + 1);
};

/**
 * @function getFullYear
 *
 * Helper function to get basket creation year (YYYY)
 *
 * @return {String} Full year value
 */
RequestUtils.prototype.getFullYear = function() {
    return this.basket.getCreationDate().getFullYear();
};

/**
 * @function buildDate
 *
 * Helper function to get full date (MM-DD-YYYY)
 *
 * @return {String} Full date value
 */
RequestUtils.prototype.buildDate = function() {
    var self = this,
        currentDate = this.basket.getCreationDate();

    return  self.getMonth() + '-' +
        self.padNumber(currentDate.getDate()) + '-' +
        self.getFullYear()
            .toString();
};

module.exports = RequestUtils;