# SureTax CCH Integration - Salesforce Commerce Cloud
SFCC/DWRE Link Cartridge for CCH SureTax from Wolters Kluwer

#### Setup

1. Import the "int_suretax" cartridge into the Salesforce Commerce Cloud Studio Workspace.
    1. Open Salesforce Commerce Cloud Studio
    2. Click File -> Import -> General -> Existing Projects Into Workspace
    3. Browse to the directory where you saved the "int_suretax" cartridge.
    4. Click Finish.
    5. Click OK when prompted to link the cartridge to the sandbox and upload.
2. Import Site Preferences and other metadata.
    1. Log into the Salesforce Commerce Cloud Business Manager.
    2. Go to Administration >  Site Development >  Import & Export and upload the meta data file metadata/suretax.xml
    3. Import the uploaded file   
3. Import CCH SureTax Services
    1. Go to Administration  ->  Operations  ->  Import & Export in BM and upload the service import file metadata/suretax_services.xml
    2. Import the uploaded file
4. Import CCH SureTax Job
    1. Go to Administration  ->  Operations  ->  Import & Export in BM and upload the job import file metadata/suretax_jobs.xml
    2. Import the uploaded file

#### Configuration 
1. Add “int_suretax” to the effective cartridge path
    1. Log into the Salesforce Commerce Cloud Business Manager.
    2. Click Administration -> Sites -> Manage Sites
    3. Select the desired site
    4. Click on the Settings tab.
    5. Add "int_suretax:" to the start of the "Cartridges" field.
    6. Click Apply
2. Add “int_suretax” to the effective Business Manager cartridge path
    1. Log into the Salesforce Commerce Cloud Business Manager.
    2. Click Administration -> Sites -> Manage Sites
    3. Select Business Manager
    4. Add "int_suretax:" to the start of the "Cartridges" field.
    5. Click Apply
3. Update suretax.http.PostRequest service with correct credentials
    1. Log into the Salesforce Commerce Cloud Business Manager.
    2. Navigate to Administration -> Operations -> Services
    3. Click on the Credentials tab.
    4. Select CCH SureTax API Credentials (imported in the Setup step)
    5. Remove the dummy values and update the Credentials. The URL should point to either the development or production tax API Endpoint for Cancellations provided by CCH SureTax.  The user name is the Client Number provided by CCH SureTax.  Password is the Validation Key provided by CCH SureTax.
    6. Hit “Apply” when finished.
4. Update suretax.http.CancelPostRequest service with correct credentials
    1. Log into the Salesforce Commerce Cloud Business Manager.
    2. Navigate to Administration -> Operations -> Services
    3. Click on the Credentials tab.
    4. Select CCH SureTax API Credentials (imported in the Setup step)
    5. Remove the dummy values and update the Credentials. The URL should point to either the development or production tax API Endpoint provided by CCH SureTax.  The user name is the Client Number provided by CCH SureTax.  Password is the Validation Key provided by CCH SureTax.
    6. Hit “Apply” when finished.
5. Configure CCH SureTax Configurations using the Salesforce Commerce Cloud Business Manager
    1. Log into the Salesforce Commerce Cloud Business Manager
    2. Select the desired site from the tabs across the top of the page.
    3. Click Site Preferences -> Custom Preferences
    4. Click on CCH SureTax Configurations
    5. Update configurations as directed by CCH SureTax
6. Configure CCH SureTax Job using the Salesforce Commerce Cloud Business Manager
    1. Log into the Salesforce Commerce Cloud Business Manager
    2. Navigate to Adminstration -> Operation -> Job Schedules
    3. Select the "CCHSuretax" job
    4. On the General tab, specify the priority. Normal and High are the options, with Normal being configured as default.
    5. On the Schedule and History tab, adjust the setting to match the desired schedule. The default setting is to run once per day at 3:00 AM.
    6. On the Resources tab, click Assign. Assign following to the job: Resource Type - System Resource, Value - order
    7. On the Step Configurator tab, select the desired scope. Every site in which CCH SureTax is enabled on should be selected within the scope of this job.
    8. On the Notification tab, specify the desired settings for notification

#### Core Code Changes

1. Locate the file **CartModel.js** located in {{siteRoot}}/app_storefront_controllers/cartridge/scripts/models
2. Replace the entire calculate function with the snippet given below. Code update will use CCH SureTax to calculate taxes if enabled, else default to native behavior.


```javascript
    /**
    * START OF CCH SURETAX UPDATES
    */
    calculate: function (finalizeTaxRequest, order) {        
        if(dw.system.Site.getCurrent().getCustomPreferenceValue('STEnable')){
            var finalizeTaxRequest = finalizeTaxRequest ? finalizeTaxRequest : false,
                calculateArgs,
                basket = this.object;
        
            if(order){
                calculateArgs = {
                    Basket: basket,
                    Order: order,
                    FinalizeTaxRequest: finalizeTaxRequest,
                    InvoiceNo: order.getOrderNo(),
                    CustomerNo: order.getCustomer().getID(),
                    customer: order.getCustomer()
                };
            } else {
                calculateArgs = {
                    Basket: basket,
                    FinalizeTaxRequest: false
                };
            }
        
            dw.system.HookMgr.callHook('dw.ocapi.suretax.basket.calculate', 'calculate', calculateArgs);
        } else {
            dw.system.HookMgr.callHook('dw.ocapi.shop.basket.calculate', 'calculate', this.object);
        }
    },
    /**
    * END OF CCH SURETAX UPDATES
    */
```
3. Go to line 178 and copy the entire snippet below and paste it in the COPlaceOrder.js controller in {{siteRoot}}/app_storefront_controllers/COPlaceOrder.js above the function call clearForms(); 

```javascript
    var orderPlacementStatus = Order.submit(order);
    if (!orderPlacementStatus.error) {

        // CCH SURETAX UPDATES: Calls CCH SureTax calculation again to associate transaction with order and possibly finalize the request
        var ConfigUtils = new ( require("int_suretax/cartridge/scripts/util/ConfigUtils") );
        if(ConfigUtils.settings.isEnabled){
            Transaction.wrap(function () {
                cart.calculate(ConfigUtils.settings.Finalize, order);
            });
        }
        // END OF CCH SURETAX UPDATES

        clearForms();
    }
    return orderPlacementStatus;
```
