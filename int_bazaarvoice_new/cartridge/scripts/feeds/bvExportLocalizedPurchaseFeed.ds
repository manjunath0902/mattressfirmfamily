/**
* bvExportLocalizedPurchaseFeed.ds
* export localizedOrders
*

*
* @input OrderList : XML
* @input OrderCount : Number
* 
* @output Message : String
*
*/

var File = require('dw/io/File');
var FileWriter = require('dw/io/FileWriter');
var Site = require('dw/system/Site');
var StringUtils = require('dw/util/StringUtils');
var XMLStreamWriter = require('dw/io/XMLStreamWriter');
var Logger = require('dw/system/Logger').getLogger('Bazaarvoice', 'bvExportLocalizedPurchaseFeed.ds');

var BV_Constants = require('int_bazaarvoice_new/cartridge/scripts/lib/libConstants').getConstants();
var BVHelper = require('int_bazaarvoice_new/cartridge/scripts/lib/libBazaarvoice').getBazaarVoiceHelper();

function execute(pdict) {
    var orderList = pdict.OrderList;
    var orderCount = pdict.OrderCount;
    
    var purchaseFeedEnabled = Site.getCurrent().getCustomPreferenceValue('bvEnablePurchaseFeed_C2013');
    if (!purchaseFeedEnabled) {
       //If the feed isn't enabled, just return.
       pdict.Message = 'Purchase Feed is not enabled!';
       Logger.info('Purchase Feed is not enabled!');
       return PIPELET_NEXT;
    }
    
	// Don't create feed if no orders are in the feed
	if (orderCount == 0) {
		pdict.Message = 'No order to export!';
		Logger.info('No order to export!');
		return PIPELET_NEXT;
	}
    
    //Establish whom to notify if this export fails
    //pdict.NotifyToEmailId = Site.getCurrent().getCustomPreferenceValue('bvAdminEmail_C2013');
    
    var date = new Date();
    var filename = 'PurchaseFeed-' 
		+ date.getFullYear() 
		+ BVHelper.insertLeadingZero(date.getMonth() + 1) 
		+ BVHelper.insertLeadingZero(date.getDate() + 1) 
		+ BVHelper.insertLeadingZero(date.getHours() + 1) 
		+ BVHelper.insertLeadingZero(date.getMinutes() + 1) 
		+ BVHelper.insertLeadingZero(date.getMilliseconds()) + '.xml'; 
		
    var file = new File(File.TEMP + '/' + filename);
    
    try {
        /* Create an output stream */
        var xsw = writeFeed(file, orderList);
            
        var destinationPath = BV_Constants.PurchaseFeedPath;
        var uploadFilename = BV_Constants.PurchaseFeedFilename;
        var destinationFilename = uploadFilename.substr(0, uploadFilename.length - 4) + '-' + StringUtils.formatCalendar(Site.getCalendar(), 'yyyy-MM-dd') + '.xml';
        
        

        var ret = BVHelper.uploadFile(destinationPath, destinationFilename, file, pdict);
        pdict.Message = ret;

    } catch(ex) {
        Logger.error('Exception caught: ' + ex.message);
        return PIPELET_ERROR;    
    } finally {
        if (file.exists()) {
            //file.remove();
        }
    }        

    return PIPELET_NEXT;
}

function writeFeed(file, orderlist) {
    var fw = new FileWriter(file, 'UTF-8');
    var xsw = new XMLStreamWriter(fw);
    var xmlns = BV_Constants.XML_NAMESPACE_PURCHASE;
    
    xsw.writeStartDocument('UTF-8', '1.0');
    var feed = <Feed xmlns={xmlns}>{orderlist.children()}</Feed>;
    xsw.writeRaw(feed);
	xsw.writeEndDocument();
   
    xsw.flush();
    xsw.close();
       
    return xsw;
}

