(function($) {
	
	
	/**
	  *
	  */
	var _self = this;
	
	
	/**
	  *
	  */
	_self.card_types = [
			{
				name: 'American Express',
				code: 'ax',
				pattern: /^3[47]/,
				valid_length: [15],
				formats : [
					{
						length: 15,
						format: 'xxxx xxxxxxx xxxx'//xxxx xxxxxxx xxxx
					}
				]
			}, {
				name: 'Diners Club',
				code: 'dc',
				pattern: /^3[68]/,
				valid_length: [14],
				formats : [
					{
						length: 14,
						format: 'xxxx xxxx xxxx xx'//xxxx xxxx xxxx xx
					}
				]
			}, {
				name: 'Discover',
				code: 'ds',
				pattern: /^6/,
				valid_length: [16],
				formats : [
					{
						length: 16,
						format: 'xxxx xxxx xxxx xxxx'//xxxx xxxx xxxx xxxx
					}
				]
			}, {
				name: 'JCB',
				code: 'jc',
				pattern: /^35/,
				valid_length: [16],
				formats : [
					{
						length: 16,
						format: 'xxxx xxxx xxxx xxxx'//xxxx xxxx xxxx xxxx
					}
				]
			}, {
				name: 'Visa',
				code: 'vs',
				pattern: /^4/,
				valid_length: [16],
				formats : [
					{
						length: 16,
						format: 'xxxx xxxx xxxx xxxx'//xxxx xxxx xxxx xxxx
					}
				]
			}, {
				name: 'Mastercard',
				code: 'mc',
				pattern: /^5[1-5]/,
				valid_length: [16],
				formats : [
					{
						length: 16,
						format: 'xxxx xxxx xxxx xxxx'//xxxx xxxx xxxx xxxx
					}
				]
			}
		];
	
	/**
	  *
	  */
	this.isValidLength = function(cc_num, card_type) {
		for(var i in card_type.valid_length) {
			if (cc_num.length <= card_type.valid_length[i]) {
				return true;
			}
		}
		return false;
	};
	
	/**
	  *
	  */
	this.getCardType = function(cc_num) {
		for(var i in _self.card_types) {
			var card_type = _self.card_types[i];
			if (cc_num.match(card_type.pattern) && _self.isValidLength(cc_num, card_type)) {
				return card_type;
			}
		}
	};
	
	/**
	  *
	  */
	this.getCardFormatString = function(cc_num, card_type) {
		for(var i in card_type.formats) {
			var format = card_type.formats[i];
			if (cc_num.length <= format.length) {
				return format.format;
			}
		}
	};
	
	/**
	  *
	  */
	this.formatCardNumber = function(cc_num, card_type) {
		var numAppendedChars = 0;
		var formattedNumber = '';
		
		if (!card_type) {
			return cc_num;
		}
		
		var cardFormatString = _self.getCardFormatString(cc_num, card_type);
		for(var i = 0; i < cc_num.length; i++) {
			cardFormatIndex = i + numAppendedChars;
			if (!cardFormatString || cardFormatIndex >= cardFormatString.length) {
				return cc_num;
			}
			
			if (cardFormatString.charAt(cardFormatIndex) !== 'x') {
				numAppendedChars++;
				formattedNumber += cardFormatString.charAt(cardFormatIndex) + cc_num.charAt(i);
			} else {
				formattedNumber += cc_num.charAt(i);
			}
		}
		
		return formattedNumber;
	};
	
	this.validateNumber = function(value) {
		isnum = /^\d+$/.test(value);
		return isnum;		
	};
	/**
	  *
	  */
	this.monitorCcFormat = function($elem) {
		var cc_num = $elem.val().replace(/\D/g,'');
		var card_type = _self.getCardType(cc_num);
		$elem.val(_self.formatCardNumber(cc_num, card_type));
		_self.addCardClassIdentifier($elem, card_type);
	};
	
	/**
	  *
	  */
	this.addCardClassIdentifier = function($elem, card_type) {
		var classIdentifier = 'cc_type_unknown';
		if (card_type) {
			classIdentifier = 'cc_type_' + card_type.code;
		}
		
		if (!$elem.hasClass(classIdentifier)) {
			var classes = '';
			for(var i in _self.card_types) {
				classes += 'cc_type_' + _self.card_types[i].code + ' ';
			}
			$elem.removeClass(classes + 'cc_type_unknown');
			$elem.addClass(classIdentifier);
		}
	};
	this.validateEmailAddress = function(email){
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(String(email).toLowerCase());
	}
	this.validateBillingForm = function(){

		$('.btnOrderSubmit').attr('disabled','disabled');
		var form_status = 1;
		var cc_type_vs = 3;
		var cc_type_ds = 0;
		var cc_type_ax = 0;
		var cc_type_mc = 0;
		
		
		if($('#dwfrm_billingform_paymentMethods_creditCard_number').val() == ''){
			form_status = 0;	
		}
		var cc_number = $('#dwfrm_billingform_paymentMethods_creditCard_number').val();
		var cc_number = cc_number.replace(/\s+/g, '');
		if(cc_number.length > 16){
			form_status = 0;
		}
		var card_type = '';
		if($( "#dwfrm_billingform_paymentMethods_creditCard_number" ).hasClass("cc_type_vs")){
			$('#dwfrm_billingform_paymentMethods_creditCard_number').removeClass('error');
			$('#dwfrm_billingform_paymentMethods_creditCard_number-error').remove();
			card_type = 'Visa';
			$( "#dwfrm_billingform_paymentMethods_creditCard_type" ).val(card_type);
			if(cc_number.length != 16 ){
				form_status = 0;				
				$('#dwfrm_billingform_paymentMethods_creditCard_number').removeClass('valid');
				$('#dwfrm_billingform_paymentMethods_creditCard_number').addClass('error');
				$("#dwfrm_billingform_paymentMethods_creditCard_number").after('<span id="dwfrm_billingform_paymentMethods_creditCard_number-error" class="error">Add complete card number.</span>');
			}
			
		}
		
		if($( "#dwfrm_billingform_paymentMethods_creditCard_number" ).hasClass("cc_type_ds")){
			$('#dwfrm_billingform_paymentMethods_creditCard_number').removeClass('error');
			$('#dwfrm_billingform_paymentMethods_creditCard_number-error').remove();
			card_type = 'Discover';
			$( "#dwfrm_billingform_paymentMethods_creditCard_type" ).val(card_type);
			if(cc_number.length != 16 ){
				form_status = 0;				
				$('#dwfrm_billingform_paymentMethods_creditCard_number').removeClass('valid');
				$('#dwfrm_billingform_paymentMethods_creditCard_number').addClass('error');
				$("#dwfrm_billingform_paymentMethods_creditCard_number").after('<span id="dwfrm_billingform_paymentMethods_creditCard_number-error" class="error">Add complete card number.</span>');
			}
		}
		
		if($( "#dwfrm_billingform_paymentMethods_creditCard_number" ).hasClass("cc_type_mc")){
			$('#dwfrm_billingform_paymentMethods_creditCard_number').removeClass('error');
			$('#dwfrm_billingform_paymentMethods_creditCard_number-error').remove();
			card_type = 'Mastercard';
			$( "#dwfrm_billingform_paymentMethods_creditCard_type" ).val(card_type);
			if(cc_number.length != 16 ){
				form_status = 0;		
				$('#dwfrm_billingform_paymentMethods_creditCard_number').removeClass('valid');
				$('#dwfrm_billingform_paymentMethods_creditCard_number').addClass('error');
				$("#dwfrm_billingform_paymentMethods_creditCard_number").after('<span id="dwfrm_billingform_paymentMethods_creditCard_number-error" class="error">Add complete card number.</span>');				
			}
			else{
				
			}
		}
		if($( "#dwfrm_billingform_paymentMethods_creditCard_number" ).hasClass("cc_type_ax")){
			$('#dwfrm_billingform_paymentMethods_creditCard_number').removeClass('error');
			$('#dwfrm_billingform_paymentMethods_creditCard_number-error').remove();
			card_type = 'American Express';
			$( "#dwfrm_billingform_paymentMethods_creditCard_type" ).val(card_type);
			if(cc_number.length != 15 ){
				form_status = 0;		
				$('#dwfrm_billingform_paymentMethods_creditCard_number').removeClass('valid');
				$('#dwfrm_billingform_paymentMethods_creditCard_number').addClass('error');
				$("#dwfrm_billingform_paymentMethods_creditCard_number").after('<span id="dwfrm_billingform_paymentMethods_creditCard_number-error" class="error">Add complete card number.</span>');
			}
		}
		
		if($( "#dwfrm_billingform_paymentMethods_creditCard_number" ).hasClass("cc_type_unknown")){
			$('#dwfrm_billingform_paymentMethods_creditCard_number').removeClass('error');
			$('#dwfrm_billingform_paymentMethods_creditCard_number-error').remove();
			form_status = 0;
			card_type = '';
			$( "#dwfrm_billingform_paymentMethods_creditCard_type" ).val(card_type);
			$('#dwfrm_billingform_paymentMethods_creditCard_number').removeClass('valid');
			$('#dwfrm_billingform_paymentMethods_creditCard_number').addClass('error');
			$("#dwfrm_billingform_paymentMethods_creditCard_number").after('<span id="dwfrm_billingform_paymentMethods_creditCard_number-error" class="error">Credit card number is not correct.</span>');
		}
		
		if($( "#dwfrm_billingform_paymentMethods_creditCard_number" ).hasClass("valid")){
			$( "#dwfrm_billingform_paymentMethods_creditCard_number" ).parent().addClass("valid-card");
		}
		else{
			$( "#dwfrm_billingform_paymentMethods_creditCard_number" ).parent().removeClass("valid-card");
		}
		if($('#dwfrm_billingform_paymentMethods_creditCard_expirydate').val() == ''){
			form_status = 0;
		}
		else{
			$('#dwfrm_billingform_paymentMethods_creditCard_expirydate').removeClass('error');
			$('#dwfrm_billingform_paymentMethods_creditCard_expirydate-error').remove();
			var exp_dt = $('#dwfrm_billingform_paymentMethods_creditCard_expirydate').val();
			var exp_dt_length = exp_dt.length;
			if( exp_dt_length == 5){
				$("#dwfrm_billingform_paymentMethods_creditCard_expirydate-error").remove();
				var current_date = new Date();
				var current_month = current_date.getMonth()+1;				
				var current_year = current_date.getFullYear();
			    //pull the last two digits of the year
				current_year = current_year.toString().substr(-2);
			    				
				var exp_temp = exp_dt.split('/');
				var exp_mn = exp_temp[0];
				var exp_yr = exp_temp[1];
				if(exp_yr < current_year){
					$('#dwfrm_billingform_paymentMethods_creditCard_expirydate').addClass('error');
					form_status = 0;					
					$("#dwfrm_billingform_paymentMethods_creditCard_expirydate").after('<span id="dwfrm_billingform_paymentMethods_creditCard_expirydate-error" class="error">Wrong expiry date</span>');
				}
				else if(exp_yr == current_year){
					if(exp_mn < current_month){
						$('#dwfrm_billingform_paymentMethods_creditCard_expirydate').addClass('error');
						form_status = 0;
						$("#dwfrm_billingform_paymentMethods_creditCard_expirydate").after('<span id="dwfrm_billingform_paymentMethods_creditCard_expirydate-error" class="error">Wrong expiry date</span>');
					}
					else{
						$('#dwfrm_billingform_paymentMethods_creditCard_expirydate').removeClass('error');
						$("#dwfrm_billingform_paymentMethods_creditCard_expirydate-error").remove();
					}
				}
				else if(exp_yr > current_year){
					if(exp_mn < 1 || exp_mn > 12 ){
						form_status = 0;
						$('#dwfrm_billingform_paymentMethods_creditCard_expirydate').addClass('error');											
						$("#dwfrm_billingform_paymentMethods_creditCard_expirydate").after('<span id="dwfrm_billingform_paymentMethods_creditCard_expirydate-error" class="error">Wrong expiry date</span>');
					}
				}
			}
			else{
				$("#dwfrm_billingform_paymentMethods_creditCard_expirydate-error").remove();
				$('#dwfrm_billingform_paymentMethods_creditCard_expirydate').addClass('error');
				form_status = 0;
				$("#dwfrm_billingform_paymentMethods_creditCard_expirydate").after('<span id="dwfrm_billingform_paymentMethods_creditCard_expirydate-error" class="error">Wrong expiry date</span>');
			}
		}
		
		if($('#dwfrm_billingform_paymentMethods_creditCard_cvn').val() == ''){
			form_status = 0;			
		}
		else{
			$("#dwfrm_billingform_paymentMethods_creditCard_cvn-error").remove();
			var cvv_val = $('#dwfrm_billingform_paymentMethods_creditCard_cvn').val();
						  			
			var cvv_length = cvv_val.length;
			var valid_cvv_length = 0;
			if(cvv_length >= 3){
				var card_type = '';
				if($( "#dwfrm_billingform_paymentMethods_creditCard_number" ).hasClass("cc_type_vs")){
					card_type = 'Visa';				
				}			
				if($( "#dwfrm_billingform_paymentMethods_creditCard_number" ).hasClass("cc_type_ds")){
					card_type = 'Discover';
				}			
				if($( "#dwfrm_billingform_paymentMethods_creditCard_number" ).hasClass("cc_type_mc")){
					card_type = 'Mastercard';
				}
				if($( "#dwfrm_billingform_paymentMethods_creditCard_number" ).hasClass("cc_type_ax")){
					card_type = 'AmericanExpress';
				}
				
				if(card_type != ''){
					if(card_type == 'Visa' || card_type == 'Discover' || card_type == 'Mastercard'){
						valid_cvv_length = 3;
					}
					else if(card_type == 'AmericanExpress'){
						valid_cvv_length = 4;
					}
					if(cvv_length == valid_cvv_length){
						$('#dwfrm_billingform_paymentMethods_creditCard_cvn').removeClass('error');	
						$("#dwfrm_billingform_paymentMethods_creditCard_cvn-error").remove();
					}
					else{
						$('#dwfrm_billingform_paymentMethods_creditCard_cvn').addClass('error');
						form_status = 0;
						$("#dwfrm_billingform_paymentMethods_creditCard_cvn").after('<span id="dwfrm_billingform_paymentMethods_creditCard_cvn-error" class="error">Wrong cvv number</span>');
					}
				}
				else{
					$('#dwfrm_billingform_paymentMethods_creditCard_cvn').addClass('error');
					form_status = 0;
					$("#dwfrm_billingform_paymentMethods_creditCard_cvn").after('<span id="dwfrm_billingform_paymentMethods_creditCard_cvn-error" class="error">Wrong cvv number</span>');
				}								
			}
			else{
				$('#dwfrm_billingform_paymentMethods_creditCard_cvn').addClass('error');
				form_status = 0;
				$("#dwfrm_billingform_paymentMethods_creditCard_cvn").after('<span id="dwfrm_billingform_paymentMethods_creditCard_cvn-error" class="error">Wrong cvv number</span>');
			}
			if(!validateNumber(cvv_val)){
				$('#dwfrm_billingform_paymentMethods_creditCard_cvn').addClass('error');
				form_status = 0;
				$("#dwfrm_billingform_paymentMethods_creditCard_cvn").after('<span id="dwfrm_billingform_paymentMethods_creditCard_cvn-error" class="error">Numeric value only.</span>');				
			}
		}
		
		
		if($('#dwfrm_billingform_fullname').val() == ''){
			form_status = 0;
		}
		else{
			$('#dwfrm_billingform_paymentMethods_creditCard_owner').val($('#dwfrm_billingform_fullname').val());
			$('#dwfrm_billingform_fullname').removeClass('error');
			$('#dwfrm_billingform_fullname-error').remove();
			
			var name = $('#dwfrm_billingform_fullname').val();
			var $regexname=/^([a-zA-Z .]{3,50})$/;
			//if (!name.match($regexname)) {
			if (!(name.length >=3 && name.length <= 50)) {
				form_status = 0;
				$('#dwfrm_billingform_fullname').addClass('error');
				$('#dwfrm_billingform_fullname-error').remove();
				$("#dwfrm_billingform_fullname").after('<span id="dwfrm_billingform_fullname-error" class="error">Wrong name.</span>');
             }
		}
		
		if($('#dwfrm_billingform_address1').val() == ''){
			form_status = 0;
		}
		if($('#dwfrm_billingform_city').val() == ''){
			form_status = 0;
		}
		else{			
			$('#dwfrm_billingform_city').removeClass('error');
			$('#dwfrm_billingform_city-error').remove();
			
			var city = $('#dwfrm_billingform_city').val();
			var $regexname=/^([a-zA-Z ]{3,50})$/;
			//if (!city.match($regexname)) {
			if (!(city.length >=3 && city.length <= 50)) {
				form_status = 0;
				$('#dwfrm_billingform_city').addClass('error');
				$('#dwfrm_billingform_city-error').remove();
				$("#dwfrm_billingform_city").after('<span id="dwfrm_billingform_city-error" class="error">Wrong City name.</span>');
             }
		}
		if($('#dwfrm_billingform_postal').val() == ''){
			form_status = 0;
		}
		else{
			$('#dwfrm_billingform_postal').removeClass('error');
			$('#dwfrm_billingform_postal-error').remove();
			var postal = $("#dwfrm_billingform_postal").val();
			var postal_length = postal.length;
			if( postal_length < 5 ){				
				$('#dwfrm_billingform_postal').addClass('error');
				$('#dwfrm_billingform_postal-error').remove();
				$("#dwfrm_billingform_postal").after('<span id="dwfrm_billingform_postal-error" class="error">Wrong Zip code.</span>');				
			}
			
		}
		if($('#dwfrm_billingform_phone').val() == ''){
			form_status = 0;
		}
		else{
			$('#dwfrm_billingform_phone').removeClass('error');
			$('#dwfrm_billingform_phone-error').remove();
			var phone = $("#dwfrm_billingform_phone").val();
			phone = phone.replace(/\s+/g, '');
			phone = phone.replace("(", '');
			phone = phone.replace(")", '');
			phone = phone.replace("-", '');
			var phone_length = phone.length;
			if( phone_length < 10 ){
				form_status = 0;
				$('#dwfrm_billingform_phone').addClass('error');
				$('#dwfrm_billingform_phone-error').remove();
				$("#dwfrm_billingform_phone").after('<span id="dwfrm_billingform_phone-error" class="error">Wrong phone number.</span>');				
			}
			
		}
		if($('#dwfrm_billingform_email_emailAddress').val() == ''){
			form_status = 0;
		}
		else{
			var email = $('#dwfrm_billingform_email_emailAddress').val();
			if ( validateEmailAddress(email) ) {
				$('#dwfrm_billingform_email_emailAddress').removeClass('error');
				$('#dwfrm_billingform_email_emailAddress-error').remove();
			}
			else{
				$('#dwfrm_billingform_email_emailAddress-error').remove();
				$('#dwfrm_billingform_email_emailAddress').addClass('error');
				form_status = 0;
				if($('#dwfrm_billingform_email_emailAddress-error').length == 0){
					$("#dwfrm_billingform_email_emailAddress").after('<span id="dwfrm_billingform_email_emailAddress-error" class="error">Wrong email format.</span>');
				}
				else{
					$("#dwfrm_billingform_email_emailAddress-error").text('Wrong email format.');
					
				}
				
			}
		}
		if($('#dwfrm_billingform_states_state').val() == ''){
			form_status = 0;
		}
		if($('#dwfrm_billingform_country').val() == ''){
			form_status = 0;
		}			
		
		if($('#grecaptcha_status').val() == '0'){
			form_status = 0;
		}
		
		if(form_status == 1){
			$('.btnOrderSubmit').removeAttr('disabled');
		}
	}
	/**
	  *
	  */
	$(function() {
		$(document).find('#dwfrm_billingform_paymentMethods_creditCard_number').each(function() {
			var $elem = $(this);
			if ($elem.is('input')) {
				$elem.on('input', function () {
					_self.monitorCcFormat($elem);
				});
			}
		});
	});
	
	$(function() {
		if ( $('.payment-billing-page').length > 0 ) {
			
			//$('#dwfrm_billingform').each(function() { this.reset() });
			$('#dwfrm_billingform_phone').mask('(000) 000-0000');
			$('#dwfrm_billingform_paymentMethods_creditCard_expirydate').mask('00/00');
			$('#dwfrm_billingform_paymentMethods_creditCard_cvn').mask('0000');
			$('#dwfrm_billingform_postal').mask('00000');
			$('#dwfrm_billingform_paymentMethods_creditCard_number').val("");
			$('#dwfrm_billingform_paymentMethods_creditCard_expirydate').val("");
			$('#dwfrm_billingform_paymentMethods_creditCard_cvn').val("");
			
			/*
			$('#dwfrm_billingform_fullname').val("");				
			$('#dwfrm_billingform_address1').val("");
			$('#dwfrm_billingform_address2').val("");
			$('#dwfrm_billingform_city').val("");
			$('#dwfrm_billingform_postal').val("");
			$('#dwfrm_billingform_phone').val("");
			$('#dwfrm_billingform_email_emailAddress').val("");
			$('#dwfrm_billingform_states_state').val("");
			$('#dwfrm_billingform_country').val("");
			*/
			
			$('.btnOrderSubmit').attr('disabled','disabled');
			
			
			$(document).on("focusout","#dwfrm_billingform_paymentMethods_creditCard_number",function() {				
				validateBillingForm();
			});
			$(document).on("focusout","#dwfrm_billingform_paymentMethods_creditCard_expirydate",function() {
				validateBillingForm();
			});
			$(document).on("focusout","#dwfrm_billingform_paymentMethods_creditCard_cvn",function() {
				validateBillingForm();
			});
			$(document).on("focusout","#dwfrm_billingform_fullname",function() {
				validateBillingForm();
			});
			$(document).on("focusout","#dwfrm_billingform_address1",function() {
				validateBillingForm();
			});
			$(document).on("focusout","#dwfrm_billingform_city",function() {
				validateBillingForm();
			});
			$(document).on("focusout","#dwfrm_billingform_postal",function() {
				validateBillingForm();
			});
			$(document).on("focusout","#dwfrm_billingform_postal",function() {
				validateBillingForm();
			});
			$(document).on("focusout","#dwfrm_billingform_phone",function() {
				validateBillingForm();
			});
			$(document).on("focusout","#dwfrm_billingform_email_emailAddress",function() {
				validateBillingForm();
			});
			$(document).on("focusout","#dwfrm_billingform_states_state",function() {
				validateBillingForm();
			});
			$(document).on("change","#dwfrm_billingform_country",function() {
				validateBillingForm();
			});									
		}
	});
	
	
}(jQuery));