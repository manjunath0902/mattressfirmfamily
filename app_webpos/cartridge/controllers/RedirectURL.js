'use strict';

/**
 * Controller that handles URL redirects.
 *
 * It is called by the system to handle URL mappings (static mappings and mapping rules).
 * The mappings are configured in Business Manager. This controller is highly performance critical,
 * because it is frequently called in case of exploit scans.
 *
 * Please follow these rules:
 * - no or only a few database calls
 * - simple (static) template response
 * - caching the result page is a must
 *
 * @module controllers/RedirectURL
 */

/* API Includes */
var URLRedirectMgr = require('dw/web/URLRedirectMgr');
var URLUtils = require('dw/web/URLUtils');
var Site = require('dw/system/Site');

/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');

/**
 * Gets the redirect. Renders the template for a redirect (util/redirectpermanent template). If no redirect can be found,
 * renders an error page (util/redirecterrorutil/redirecterror template).
 * NOTE: These controllers will prefer the templates located within this cartridge over those specified by the cartridge path.
 */
function start(args) {
    var redirect = URLRedirectMgr.getRedirect(),
        location = redirect ? redirect.getLocation() : null,
        errorBehavior = Site.getCurrent().getCustomPreferenceValue('unresolvedParameterBehavior');
    var origin = URLRedirectMgr.redirectOrigin;
    var req = request;
    if (origin.match(/service-worker([a-z.])*\.js/)) {
        // add globals for service-worker context
        //var publicJS = new Template((origin.match(/service-worker([a-z.])*\.js/)[0])).render().text;
        response.setContentType('text/javascript');
        response.writer.print(dw.system.Site.current.preferences.custom.pwaServiceWorker);
        return;
    } else if (origin.indexOf('manifest.json') > -1) {
        response.setContentType('application/manifest+json');
        response.writer.print(JSON.stringify(require('*/cartridge/scripts/pwa-manifest.js')));
        return;
    }
    else if (!location) {
		// Biggest problem with this port to controllers is missing pdict variables.
		// This tempalte uses pdict nodecorator, ErrorBehavior, Redirecting, PipelineName, CurrentStartNodeName, ErrorText
        app.getView({
        PipelineName: args.ControllerName,
        CurrentStartNodeName: args.CurrentStartNodeName,
        ErrorText: args.ErrorText,
        ErrorBehavior: errorBehavior
        }).render('error/generalerror');
    } else {
        // this one just uses pdict.Location
        app.getView({
            Location: location
        }).render('util/redirectpermanent');
    }
}

/**
 * Hostname-only URLs (http://sitegenesis.com/) cannot be redirected using the URL mapping framework.
 * Instead, specify this controller in your site's hostname alias in Business Manager.
 *
 * However, a redirect to the homepage is performed by the
 * Default controller Start function.
 * The hostname in the URL is the site's HTTP Hostname, if one is configured in Business Manager.
 * Also, you can provide a URL to redirect to an optional parameter, Location.
 *
 * @example
 * Redirect http[s]://sitegenesis.com/ to http://www.sitegenesis.com/:
 * sitegenesis.com,,RedirectURL-Hostname,Location,http://www.sitegenesis.com/
 */
function hostName() {
    var Redirect = require('app_storefront_core/cartridge/scripts/util/Redirect');
    var location = !empty(request.httpParameterMap.Location.stringValue) ? request.httpParameterMap.Location.stringValue : URLUtils.httpHome();
    app.getView({
        Location: location
    }).render('util/redirectpermanent');
}


/*
 * Module exports
 */

/*
 * Web exposed methods
 */
/** Gets a redirect and renders it.
 * @see module:controllers/RedirectURL~start */
exports.Start = guard.ensure([], start);
/** Used by the platform for URL redirects.
 * @see module:controllers/RedirectURL~hostName */
exports.Hostname = guard.ensure([], hostName);
