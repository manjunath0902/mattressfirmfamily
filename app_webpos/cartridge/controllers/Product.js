
'use strict';

/**
 * Controller that renders product detail pages and snippets or includes used on product detail pages.
 * Also renders product tiles for product listings.
 *
 * @module controllers/Product
 */

var params = request.httpParameterMap;

/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app.js');
var guard = require('app_storefront_controllers/cartridge/scripts/guard.js');
var SleepysPipeletHelper = require('bc_sleepysc/cartridge/scripts/sleepys/util/SleepysPipeletsHelper').SleepysPipeletHelper;
var StoreMgr = require('dw/catalog/StoreMgr');
var Site = require('dw/system/Site');
var JsonSchemaDataMF = require('app_mattressfirm_storefront/cartridge/scripts/util/CanonicalURLUtils');
var JsonSchemaDataOneMF = require('app_1800mattressrv_storefront/cartridge/scripts/util/CanonicalURLUtils');
var currentSiteId = dw.system.Site.getCurrent().ID;
var CustomObjectManager = require('dw/object/CustomObjectMgr');
var dwLogger = require("dw/system").Logger;
var ProductUtils = require('app_webpos/cartridge/scripts/product/ProductUtils.js');
/**
 * Renders the product page.
 *
 * If the product is online, gets a ProductView and updates the product data from the httpParameterMap.
 * Renders the product page (product/product template). If the product is not online, sets the response status to 401,
 * and renders an error page (error/notfound template).
 */

/**
 * Renders the product detail page.
 *
 * If the product is online, gets a ProductView and updates the product data from the httpParameterMap.
 * Renders the product detail page (product/productdetail template). If the product is not online, sets the response status to 401,
 * and renders an error page (error/notfound template).
 */
function detail() {     
    var Product = app.getModel('Product');
    var product = Product.get(params.pid.stringValue);

    if (product.isVisible()) {
        var currentVariationModel = product.updateVariationSelection(params);
        var getSelectedAttr = ProductUtils.getSelectedAttributes(currentVariationModel);
        if(empty(getSelectedAttr.size.value)){
        	ProductUtils.setVariationAttributeValue(currentVariationModel,'size', 'QUEEN');
        	if(!empty(currentVariationModel.selectedVariant) && !empty(currentVariationModel.selectedVariant.ID)){
        		product = Product.get(currentVariationModel.selectedVariant.ID);
        	}
        }
        var geoAvailability = ProductUtils.checkGeoAvailability(product.object);
        var productView;
		//check geo based available or not 
		var geoAvailability = ProductUtils.checkGeoAvailability(product.object);
    	productView = app.getView('Product', {
	        product: product,
	        ldSchema:JsonSchemaDataMF.CreateProductDetailPageLDJsonSchema(product),
	        DefaultVariant: product.getVariationModel().getDefaultVariant(),
	        CurrentOptionModel: product.updateOptionSelection(params),
	        CurrentVariationModel: currentVariationModel,
	        geoAvailability : geoAvailability
        });
        
        productView.render(product.getTemplate() || 'product/productdetail');
        
    } else {
        // @FIXME Correct would be to set a 404 status code but that breaks the page as it utilizes
        // remote includes which the WA won't resolve
        response.setStatus(410);
        app.getView().render('error/notfound');
    }

}




function show() {
	 var Product = app.getModel('Product');
	    var product = Product.get(params.pid.stringValue);	    
	    product = getSelectedProduct(product);
	    var manufactureName = product.getBrand();
	    var productOnlineStatus = product.isOnline();
	    /*if(!productOnlineStatus){
	    	var brandCat = dw.catalog.CatalogMgr.getCategory("5637148326");
	    	if(!empty(brandCat)  && brandCat != undefined) {
	    		var rootCat = brandCat.getOnlineSubCategories();
	    	}
	    }*/
	    var categories = product.getAllCategories(); 
	    var check = false;
	    if (product.isVisible()) {
   	        require('app_storefront_controllers/cartridge/scripts/meta').update(product);
   	        var productView = app.getView('Product', {
   	            product: product,
   	            DefaultVariant: product.getVariationModel().getDefaultVariant(),
   	            CurrentOptionModel: product.updateOptionSelection(params),
   	            CurrentVariationModel: product.updateVariationSelection(params)
   	            //AvailInZone: AvailInZone
   	        });
   	        productView.render(product.getTemplate() || 'product/product');
   	    }else {
   	        // @FIXME Correct would be to set a 404 status code but that breaks the page as it utilizes
   	        // remote includes which the WA won't resolve.
   	        response.setStatus(410);
   	        app.getView().render('error/notfound');
   	    }
}

/**
 * Returns product availability data as a JSON object.
 *
 * Gets a ProductModel and gets the product ID from the httpParameterMap. If the product is online,
 * renders product availability data as a JSON object.
 * If the product is not online, sets the response status to 401,and renders an error page (error/notfound template).
 */
function getAvailability() {

    var Product = app.getModel('Product');
    var product = Product.get(params.pid.stringValue);

    if (product.isVisible()) {
        let r = require('~/cartridge/scripts/util/Response');

        r.renderJSON(product.getAvailability(params.Quantity.stringValue));
    } else {
        // @FIXME Correct would be to set a 404 status code but that breaks the page as it utilizes
        // remote includes which the WA won't resolve
        response.setStatus(410);
        app.getView().render('error/notfound');
    }

}

/**
 * Renders a product tile. This is used within recommendation and search grid result pages.
 *
 * Gets a ProductModel and gets a product using the product ID in the httpParameterMap.
 * If the product is online, renders a product tile (product/producttile template), used within family and search result pages.
 */
function hitTile() {

    var Product = app.getModel('Product');
    var product = Product.get(params.pid.stringValue);

    if (product.isVisible()) {
        var productView = app.getView('Product', {
            product: product,
            showswatches: true,
            showpricing: true,
            showpromotion: true,
            showrating: true,
            showcompare: true
        });

        productView.product = product.object;

        var producttileTemplate = 'product/producttile';
        	
    	if(!empty(params.plp_tiles) && params.plp_tiles.stringValue == 'v1') {
    		//new template
    		var shippingInformation = 'shippingInformation' in product.object.custom ? product.object.custom.shippingInformation : null;
            if (!empty(session.custom.customerZip) && product.object.custom.isMFICore && !empty(shippingInformation) && shippingInformation.toLowerCase() == 'core') {        		  
				productView.showAvailabilityMessage = showAvailabilityMessage();
			}else {
				productView.showAvailabilityMessage = false;
			}
    		producttileTemplate = 'product/producttile_mattressfirmsubcategory_ab';
    	} else {
    		//old template
    		producttileTemplate = 'product/producttile_mattressfirmsubcategory';
    	}
            
        productView.render(product.getTemplate() || producttileTemplate);
    }

}

// plp tile availability message
function showAvailabilityMessage(){
	var isAvailable = false;
	var zipCode = session.custom.customerZip;
	var custObjType = 'ZipInfo';
	var custObjKeyVal = zipCode;
	try {
		    var zipInfoObj = CustomObjectManager.getCustomObject(custObjType, custObjKeyVal);
			if (!empty(zipInfoObj) && !empty(zipInfoObj.custom) && 'inMarket' in zipInfoObj.custom) {
				isAvailable = zipInfoObj.custom.inMarket;				
			}		
		} catch (e) {			
			var plpTileAvailabilityMessage = dwLogger.getLogger('PLPTileAvailabilityMsg','PLPTileAvailabilityMsg');
            plpTileAvailabilityMessage.error("PLP tile availability message issue");
        }		
	return isAvailable;
}	

/**
 * Renders a navigation include on product detail pages.
 *
 * Gets a ProductModel and gets a product using the product ID in the httpParameterMap.
 * If the product is online, constructs a search and paging model, executes the search,
 * and renders a navigation include on product detail pages (search/productnav template).
 * Also provides next/back links for customers to traverse a product
 * list, such as a search result list.
 */
function productNavigation() {

    var Product = app.getModel('Product');
    var product = Product.get(params.pid.stringValue);

    if (product.isVisible()) {
        var PagingModel;
        var productPagingModel;

        // Construct the search based on the HTTP params and set the categoryID.
        var Search = app.getModel('Search');
        var productSearchModel = Search.initializeProductSearchModel(params);

        // Reset pid in search.
        productSearchModel.setProductID(null);

        // Special handling if no category ID is given in URL.
        if (!params.cgid.value) {
            var category = null;

            if (product.getPrimaryCategory()) {
                category = product.getPrimaryCategory();
            } else if (product.getVariationModel().getMaster()) {
                category = product.getVariationModel().getMaster().getPrimaryCategory();
            }

            if (category && category.isOnline()) {
                productSearchModel.setCategoryID(category.getID());
            }
        }

        // Execute the product searchs
        productSearchModel.search();

        // construct the paging model
        PagingModel = require('dw/web/PagingModel');
        productPagingModel = new PagingModel(productSearchModel.productSearchHits, productSearchModel.count);
        productPagingModel.setPageSize(3);
        productPagingModel.setStart(params.start.intValue - 2);

        app.getView({
            ProductPagingModel: productPagingModel,
            ProductSearchResult: productSearchModel
        }).render('search/productnav');

    } else {
        // @FIXME Correct would be to set a 404 status code but that breaks the page as it utilizes
        // remote includes which the WA won't resolve
        response.setStatus(410);
        app.getView().render('error/notfound');
    }

}

/**
 * Renders variation selection controls for a given product ID, taken from the httpParameterMap.
 *
 * If the product is online, updates variation information and gets the selected variant. If it is an ajax request, renders the
 * product content page (product/productcontent template), otherwise renders the product page (product/product template).
 * If it is a bonus product, gets information about the bonus discount line item and renders the bonus product include page
 * (pageproduct/components/bonusproduct template). If the product is offline, sets the request status to 401 and renders an
 * error page (error/notfound template).
 */
function variation() {

    var currentVariationModel;
    var Product = app.getModel('Product');
    var product = Product.get(params.pid.stringValue);
    var resetAttributes = false;    
    //var AvailInZone = SleepysPipeletHelper.pdpZoneCheck(params.pid.stringValue);

    product = getSelectedProduct(product);
    currentVariationModel = product.updateVariationSelection(params);

    if (product.isVisible()) {
        if (params.source.stringValue === 'bonus') {
            var Cart = app.getModel('Cart');
            var bonusDiscountLineItems = Cart.get().getBonusDiscountLineItems();
            var bonusDiscountLineItem = null;

            for (var i = 0; i < bonusDiscountLineItems.length; i++) {
                if (bonusDiscountLineItems[i].UUID === params.bonusDiscountLineItemUUID.stringValue) {
                    bonusDiscountLineItem = bonusDiscountLineItems[i];
                    break;
                }
            }

            app.getView('Product', {
                product: product,
                CurrentVariationModel: currentVariationModel,
                BonusDiscountLineItem: bonusDiscountLineItem
            }).render('product/components/bonusproduct');
        }
        else if (params.source.stringValue === 'recommendation') {
            var Cart = app.getModel('Cart');

            app.getView('Product', {
                product: product,
                CurrentVariationModel: currentVariationModel
            }).render('product/components/recommendations-item');
        } else if (params.format.stringValue) {
            if(ProductUtils.isMattressProductChecker(product.object)) {
            	var geoAvailability = ProductUtils.checkGeoAvailability(product.object);
            	app.getView('Product', {
                	product: product,
                	GetImages: true,
                	resetAttributes: resetAttributes,
                	CurrentVariationModel: currentVariationModel,
                	geoAvailability : geoAvailability
                	//AvailInZone: AvailInZone
           	 	}).render('product/productcontent');
            }
            else
            {
            	app.getView('Product', {
                	product: product,
                	GetImages: true,
                	resetAttributes: resetAttributes,
                	CurrentVariationModel: currentVariationModel
                	//AvailInZone: AvailInZone
            	}).render('product/productcontent');
            }
        } else {
            app.getView('Product', {
                product: product,
                CurrentVariationModel: currentVariationModel
                //AvailInZone: AvailInZone
            }).render('product/product');
        }
    } else {
        // @FIXME Correct would be to set a 404 status code but that breaks the page as it utilizes
        // remote includes which the WA won't resolve
        response.setStatus(410);
        app.getView().render('error/notfound');
    }

}

/**
 * Renders variation selection controls for the product set item identified by a given product ID, taken from the httpParameterMap.
 *
 * If the product is online, updates variation information and gets the selected variant. If it is an ajax request, renders the
 * product set page (product/components/productsetproduct template), otherwise renders the product page (product/product template).
 *  If the product is offline, sets the request status to 401 and renders an error page (error/notfound template).
 *
 */
function variationPS() {

    var Product = app.getModel('Product');
    var product = Product.get(params.pid.stringValue);

    if (product.isVisible()) {

        var productView = app.getView('Product', {
            product: product
        });

        var productVariationSelections = productView.getProductVariationSelections(params);
        product = Product.get(productVariationSelections.SelectedProduct);

        if (product.isMaster()) {
            product = Product.get(product.getVariationModel().getDefaultVariant());
        }

        if (params.format.stringValue) {
            app.getView('Product', {product: product}).render('product/components/productsetproduct');
        } else {
            app.getView('Product', {product: product}).render('product/product');
        }
    } else {
        // @FIXME Correct would be to set a 404 status code but that breaks the page as it utilizes
        // remote includes which the WA won't resolve
        response.setStatus(410);
        app.getView().render('error/notfound');
    }

}

/**
 * Renders the last visited products based on the session information (product/lastvisited template).
 */
function includeLastVisited() {
    app.getView({
        LastVisitedProducts: app.getModel('RecentlyViewedItems').getRecentlyViewedProducts(3)
    }).render('product/lastvisited');
}

/**
 * Renders a list of bonus products for a bonus discount line item (product/bonusproductgrid template).
 */
function getBonusProducts() {
    var Cart = app.getModel('Cart');
    var getBonusDiscountLineItemDS = require('app_storefront_core/cartridge/scripts/cart/GetBonusDiscountLineItem');
    var currentHttpParameterMap = request.httpParameterMap;
    var bonusDiscountLineItems = Cart.get().getBonusDiscountLineItems();
    var bonusDiscountLineItem;

    bonusDiscountLineItem = getBonusDiscountLineItemDS.getBonusDiscountLineItem(bonusDiscountLineItems, currentHttpParameterMap.bonusDiscountLineItemUUID);
    var bpCount = bonusDiscountLineItem.bonusProducts.length;
    var bpTotal;
    var maxBonusItemsCount = 0;
    var bonusDiscountProducts;
    if (currentHttpParameterMap.pageSize && !bpCount) {

        var BPLIObj = getBonusDiscountLineItemDS.getBonusPLIs(currentHttpParameterMap.pageSize, currentHttpParameterMap.pageStart, bonusDiscountLineItem);

        bpTotal = BPLIObj.bpTotal;
        bonusDiscountProducts = BPLIObj.bonusDiscountProducts;
    } else {
        bpTotal = -1;
    }
    //bDLineItem => bonusDiscountLineItem
    for each (var bDLineItem in bonusDiscountLineItems) {
    	if (bDLineItem.UUID == bonusDiscountLineItem.UUID) {
    		maxBonusItemsCount = bDLineItem.maxBonusItems;
    		break;
    	}
    }
    app.getView({
        BonusDiscountLineItem: bonusDiscountLineItem,
        BPTotal: bpTotal,
        MaxBonusItemsCount : maxBonusItemsCount,
        BonusDiscountProducts: bonusDiscountProducts,
        CurrentItemSize :currentHttpParameterMap.currentItemSize
    }).render('product/bonusproductgrid');

}

/**
 * Renders a set item view for a given product ID, taken from the httpParameterMap pid parameter.
 * If the product is online, get a ProductView and renders the product set page (product/components/productsetproduct template).
*  If the product is offline, sets the request status to 401 and renders an error page (error/notfound template).
*/
function getSetItem() {
    var currentVariationModel;
    var Product = app.getModel('Product');
    var product = Product.get(params.pid.stringValue);
    product = getSelectedProduct(product);
    currentVariationModel = product.updateVariationSelection(params);

    if (product.isVisible()) {
        app.getView('Product', {
            product: product,
            CurrentVariationModel: currentVariationModel,
            isSet: true
        }).render('product/components/productsetproduct');
    } else {
        // @FIXME Correct would be to set a 404 status code but that breaks the page as it utilizes
        // remote includes which the WA won't resolve
        response.setStatus(410);
        app.getView().render('error/notfound');
    }

}

/**
 * Checks whether a given product has all required attributes selected, and returns the selected variant if true
 *
 * @param {dw.catalog.Product} product
 * @returns {dw.catalog.Product} - Either input product or selected product variant if all attributes selected
 */
function getSelectedProduct (product) {
    const currentVariationModel = product.updateVariationSelection(params);
    let selectedVariant;

    if (currentVariationModel) {
        selectedVariant = currentVariationModel.getSelectedVariant();
        if (selectedVariant) {
            product = app.getModel('Product').get(selectedVariant);
        }
    }
    return product;
}

/**
 * Renders the product detail page within the context of a category.
 * Calls the {@link module:controllers/Product~show|show} function.
 * __Important:__ this function is not obsolete and must remain as it is used by hardcoded platform rewrite rules.
 */
function showInCategory() {
    show();
}

function getIsPickup() {
    var Cart = app.getModel('Cart');
    product = getSelectedProduct(product);
    var isPickupInStore = false;
    var productLineItems : Iterator = Cart.getAllProductLineItems().iterator();
    while(productLineItems.hasNext()){
        var productLineItem : ProductLineItem = productLineItems.next();
        if (productLineItem.productID  === product.getID()) {
            isPickupInStore = true;
        }
    }
    return isPickupInStore;
}

function getFinancingCost() {
	var params = request.httpParameterMap,
		salePrice = params.saleP.stringValue,
		stdPrice = 	params.stdP.stringValue;
	var symbol = session.getCurrency().getSymbol();
		
	salePrice = salePrice.replace(/,/g,'').replace(symbol,'');
	stdPrice = stdPrice.replace(/,/g,'').replace(symbol,'');
	var monthlyPayment = false;
	var financingPromos = dw.system.Site.current.preferences.custom.financingPromo;
	var financingPromoItems = new dw.util.ArrayList();
	var productMinPrice = false;
	productMinPrice = ( Number ( salePrice ) == Number( stdPrice )) ? Number( stdPrice ) : Number( salePrice );
	if (productMinPrice) {
		for each (var financingPromo in financingPromos) {
			var financingPromoData = financingPromo.split('|');
			if (financingPromoData.length > 1 && productMinPrice) {
				var minPurchaseAmount = Number(financingPromoData[2]);
				if (productMinPrice && productMinPrice >= minPurchaseAmount) {
					var maxTerm = Number(financingPromoData[0] * 12).toFixed();
					if (financingPromoData[1] > 0) {
						var interestRate = (financingPromoData[1] / 100) / 12;
						var partD = Math.pow((1 + interestRate), maxTerm);
						var D = Number( (partD - 1) / (interestRate * partD) ).toFixed(2);
						var P = Number(productMinPrice / D);
					} else {
						var P = Number( productMinPrice / maxTerm );
					}
										
					financingPromoItems.push({
						"minPurchaseAmount" : minPurchaseAmount,
						"monthlyPayments" : P,
						"term" : maxTerm
					});
				}
			}
		}
		var financeComparator = new dw.util.PropertyComparator ("monthlyPayments", true);
		financingPromoItems.sort(financeComparator);
		var currencyCode = session.getCurrency().getCurrencyCode();
		monthlyPayment = new dw.value.Money(financingPromoItems[0]["monthlyPayments"], currencyCode);
		var financeTerm = financingPromoItems[0]["term"];
		var minPurchaseAmount = new dw.value.Money(financingPromoItems[0]['minPurchaseAmount'], currencyCode);
	}
	var responseHtml = '<span id="tooltip">';
	responseHtml += symbol + monthlyPayment.value.toFixed(2) + '&#47;mo';
	responseHtml += '</span>';
	responseHtml += '<span class="tooltip-box" data-layout="small">';
	responseHtml += dw.web.Resource.msgf('product.financing.option.tooltip.box','product',null, financeTerm, minPurchaseAmount.toFormattedString());
	responseHtml += '&nbsp;<a href="' + dw.web.URLUtils.url('Page-Show', 'cid', 'financing-mattress-firm-credit-card') + '" target="_blank">Learn more.</a>';
	responseHtml += '</span>';
	response.setContentType('text/html');
	response.getWriter().print(responseHtml);
	
}

function imageZoomOverlay() {
    var Product = app.getModel('Product');
    let product = Product.get(params.pid.stringValue);
    var currentVariationModel = product.updateVariationSelection(params);
    product = product.isVariationGroup() ? product : getSelectedProduct(product);
    
    app.getView({
        Product: product.object,
        defaultVariant: product.getVariationModel().getDefaultVariant(),
        CurrentVariationModel: currentVariationModel
    }).render('product/productimageszoom');
}
/*
 * Web exposed methods
 */
/**
 * Renders the product template.
 * @see module:controllers/Product~show
 */
exports.Show = guard.ensure(['get', 'loggedIn'], show);

/**
 * Renders the product detail page within the context of a category.
 * @see module:controllers/Product~showInCategory
 */
exports.ShowInCategory = guard.ensure(['get'], showInCategory);

/**
 * Renders the productdetail template.
 * @see module:controllers/Product~detail
 */
exports.Detail = guard.ensure(['get'], detail);

/**
 * Returns product availability data as a JSON object.
 * @see module:controllers/Product~getAvailability
 */
exports.GetAvailability = guard.ensure(['get'], getAvailability);

/**
 * Renders a product tile, used within family and search result pages.
 * @see module:controllers/Product~hitTile
 */
exports.HitTile = guard.ensure(['get'], hitTile);

/**
 * Renders a navigation include on product detail pages.
 * @see module:controllers/Product~productNavigation
 */
exports.Productnav = guard.ensure(['get'], productNavigation);

/**
 * Renders variation selection controls for a given product ID.
 * @see module:controllers/Product~variation
 */
exports.Variation = guard.ensure(['get'], variation);

/**
 * Renders variation selection controls for the product set item identified by the given product ID.
 * @see module:controllers/Product~variationPS
 */
exports.VariationPS = guard.ensure(['get'], variationPS);

/**
 * Renders the last visited products based on the session information.
 * @see module:controllers/Product~includeLastVisited
 */
exports.IncludeLastVisited = guard.ensure(['get'], includeLastVisited);

/**
 * Renders a list of bonus products for a bonus discount line item.
 * @see module:controllers/Product~getBonusProducts
 */
exports.GetBonusProducts = guard.ensure(['get'], getBonusProducts);

/**
 * Renders a set item view for the given product ID.
 * @see module:controllers/Product~getSetItem
 */
exports.GetSetItem = guard.ensure(['get'], getSetItem);

/**
 * Renders financing cost for a particular product.
 * @see module:controllers/Product~getFinancingCost
 */
exports.GetFinancingCost = guard.ensure(['get'], getFinancingCost);

/**
 * Renders the images overlay on PDP
 * @see module:controllers/Product~imageZoomOverlay
 */
exports.ImageZoomOverlay = guard.ensure(['get'], imageZoomOverlay);
