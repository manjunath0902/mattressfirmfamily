'use strict';

/**
 * Controller for the default single shipping scenario.
 * Single shipping allows only one shipment, shipping address, and shipping method per order.
 *
 * @module controllers/COShippingMethod
 */

/* API Includes */
var CustomerMgr = require('dw/customer/CustomerMgr');
var HashMap = require('dw/util/HashMap');
var ShippingMgr = require('dw/order/ShippingMgr');
var URLUtils = require('dw/web/URLUtils');
var Resource = require('dw/web/Resource');
var Site = require('dw/system/Site');
var Transaction = require('dw/system/Transaction');

/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var pipeletHelper = require('bc_sleepysc/cartridge/scripts/sleepys/util/SleepysPipeletsHelper').SleepysPipeletHelper;
var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;
var storePicker = require('~/cartridge/controllers/StorePicker');

/**
 * Prepares shipments. Theis function separates gift certificate line items from product
 * line items. It creates one shipment per gift certificate purchase
 * and removes empty shipments. If in-store pickup is enabled, it combines the
 * items for in-store pickup and removes them.
 * This function can be called by any checkout step to prepare shipments.
 *
 * @transactional
 * @return {Boolean} true if shipments are successfully prepared, false if they are not.
 */
function prepareShipments() {
    var cart, homeDeliveries;
    cart = app.getModel('Cart').get();

    homeDeliveries = Transaction.wrap(function () {

        homeDeliveries = false;

        cart.updateGiftCertificateShipments();
        cart.removeEmptyShipments();

        if (Site.getCurrent().getCustomPreferenceValue('enableStorePickUp')) {
            homeDeliveries = cart.consolidateInStoreShipments();

            session.forms.singleshipping.inStoreShipments.shipments.clearFormElement();
            app.getForm(session.forms.singleshipping.inStoreShipments.shipments).copyFrom(cart.getShipments());
        } else {
            homeDeliveries = true;
        }

        return homeDeliveries;
    });

    return homeDeliveries;
}

//calculate tax and shipping for given zip code

function calculateShippingAndTaxForZip(zipCode)
{
	var cart = app.getModel('Cart').get();
	
	var test = session.custom.customerZoneZip;
	if(!empty(cart) && cart != null){
		Transaction.wrap(function () {
	        var defaultShipment, shippingAddress;
	        defaultShipment = cart.getDefaultShipment();
	      	shippingAddress = cart.createShipmentShippingAddress(defaultShipment.getID());
	      	var shippingMethodID = defaultShipment.getShippingMethodID();	       
	        var citiesAndStateJSON = mattressPipeletHelper.getCitiesAndStatesForZipCode(zipCode);   
	        var city = "";
	        var state = "";
	    	if (!empty(citiesAndStateJSON)) {
	    		var cityData = JSON.parse(citiesAndStateJSON);   
	            for (var i = 0; i < cityData.cities.length; i++){
	                var data = cityData.cities[i];
	                if (!empty(data.state)) {
	                	state = data.state.toUpperCase();
	                	city =  data.city;
	                	break;
	        		}
	            }
	            
	            if(!empty(state))
		        {
		        	shippingAddress.setCity(city);
		 	        shippingAddress.setPostalCode(zipCode);
		 	        shippingAddress.setStateCode(state);
		 	        shippingAddress.setCountryCode('US');				       
		 	        cart.updateShipmentShippingMethod(defaultShipment.getID(), shippingMethodID, null, null);
		 	        var forceServiceCall = true;
		            mattressPipeletHelper.splitShipmentsByType(cart.object, forceServiceCall);
		        }
			}
	    	else {
	    		shippingAddress.setCity(city);
	 	        shippingAddress.setPostalCode(zipCode);
	 	        shippingAddress.setStateCode(state);
	 	        shippingAddress.setCountryCode('US');				       
	 	        cart.updateShipmentShippingMethod(defaultShipment.getID(), shippingMethodID, null, null);
	 	        var forceServiceCall = true;
	            mattressPipeletHelper.splitShipmentsByType(cart.object, forceServiceCall);
	    	}        
			
			cart.removeEmptyShipments();
    		cart.calculate();
	 
	    });
	}	
	 return;
}
/**
 * Starting point for the single shipping scenario. Prepares a shipment by removing gift certificate and in-store pickup line items from the shipment.
 * Redirects to multishipping scenario if more than one physical shipment is required and redirects to billing if all line items do not require
 * shipping.
 *
 * @transactional
 */
function start() {
    var cart, pageMeta, homeDeliveries, dateListObject;
    cart = app.getModel('Cart').get();
    if (cart) {
	    //session.forms.cart.coupons.copyFrom(cart.object.couponLineItems);
	    
	    var pageMetaData = request.pageMetaData;
	    pageMetaData.title = Resource.msg('singleshipping.deliveryschedule.meta.pagetitle', 'checkout', null);
	    
	    //Check if the basket contains only dropship products then skip delivery selection page 
	    //and land the user on shipping address page.
	    var IsOnlyRedCarpetItemsInCart = mattressPipeletHelper.getOrderIsRedCarpetStatus(cart);
	    if(!IsOnlyRedCarpetItemsInCart){
        	var redirectURL = dw.web.URLUtils.https('COBilling-Start');
        	response.redirect(redirectURL);
	    }
	    
	    var geolocationZip = request.getGeolocation().getPostalCode();
	    var defaultZipCode ='77025';
	    var postalCode = empty(session.custom.customerZip) ? (empty(geolocationZip) ? defaultZipCode : geolocationZip) : session.custom.customerZip;
	    
		session.custom.customerZip = postalCode;
	    if (empty(session.custom.deliveryOptionChoice)) {
	        session.custom.deliveryOptionChoice = 'shipped';
	    }
	    var deliveryScheduleChange = session.forms.singleshipping.shippingAddress.deliveryScheduleChanged.value;
	    
		// If deliver schedule changed on shipping screen. Go directly to billing
	    if (!(deliveryScheduleChange && deliveryScheduleChange === true) ) {
	    	
	    	calculateShippingAndTaxForZip(postalCode);
	    }

    
    	 
        Transaction.wrap(function () {
            cart.calculate();
        });
            
        // Initializes the  form and prepopulates it with the shipping address of the default
        if (cart.getDefaultShipment().getShippingAddress()) {
            app.getForm(session.forms.singleshipping.shippingAddress.addressFields).copyFrom(cart.getDefaultShipment().getShippingAddress());
            app.getForm(session.forms.singleshipping.shippingAddress.addressFields.states).copyFrom(cart.getDefaultShipment().getShippingAddress());
            app.getForm(session.forms.singleshipping.shippingAddress).copyFrom(cart.getDefaultShipment());
        }
        session.forms.singleshipping.shippingAddress.shippingMethodID.value = cart.getDefaultShipment().getShippingMethodID();
        

        // Prepares shipments.
        homeDeliveries = prepareShipments();

        Transaction.wrap(function () {
            cart.calculate();
        });
        
        if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule')) {
        	var postalCode = session.forms.singleshipping.shippingAddress.addressFields.postal.value;
            var deliveryDatesArr = storePicker.GetATPDeliveryDates(cart, postalCode);
        } else {
			var deliveryDatesArr = mattressPipeletHelper.getDeliveryDates(cart); 
		}
		
        // Go to billing step, if we have no product line items, but only gift certificates in the basket, shipping is not required.
        if (cart.getProductLineItems().size() === 0) {
        	var COBillingController = require('~/cartridge/controllers/COBilling');
            COBillingController.Start();
        } else {
            pageMeta = require('app_storefront_controllers/cartridge/scripts/meta');
            pageMeta.update({
                pageTitle: Resource.msg('singleshipping.meta.pagetitle', 'checkout', 'SiteGenesis Checkout')
            });
            
            var isRestrictedCheckout = mattressPipeletHelper.getIsRestrictedCheckout(session.custom.customerZip);
            
            /*var geolocationZip = request.getGeolocation().getPostalCode();
			var postalCode = (empty(session.forms.storepicker.postalCode.value)) ? geolocationZip : session.forms.storepicker.postalCode.value;
			var distance = (empty(session.forms.storepicker.maxdistance.value)) ? 100 : session.forms.storepicker.maxdistance.value;
			var productID = (empty(session.forms.storepicker.productID.value)) ? null : session.forms.storepicker.productID.value;
			var isBopis = (empty(session.forms.storepicker.isBopis.value)) ? true : session.forms.storepicker.isBopis.value;
			session.custom.isBopis = isBopis;
			var findInStoreResponse = app.getController('StorePicker').FindInStore(postalCode, distance, productID, isBopis);
            
            app.getView({
                ContinueURL: URLUtils.https('COShippingMethod-SingleShipping'),
                Basket: cart.object,
                HomeDeliveries: homeDeliveries,
                DeliveryDatesArr: deliveryDatesArr,
                stores:findInStoreResponse.storeArray,
                pid: findInStoreResponse.productID,
                zip: findInStoreResponse.customerZip,
                isBopis : findInStoreResponse.isBopis,
                isCart : findInStoreResponse.session.custom.isCart,
                totalStores : findInStoreResponse.storeMap.length 					
            }).render('checkout/shipping/shippingmethodstep');*/
			app.getView({
                ContinueURL: URLUtils.https('COShippingMethod-SingleShipping'),
                Basket: cart.object,
                HomeDeliveries: homeDeliveries,
                DeliveryDatesArr: deliveryDatesArr,
                isRestrictedCheckout: isRestrictedCheckout.status
            }).render('checkout/shipping/shippingmethodstep');
        }
    } else {
        app.getController('Cart').Show();
        return;
    }

}

function deliveryDatesCalender() {
	var cart;
    cart = app.getModel('Cart').get();
    
	if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule')) {
    	var postalCode = session.custom.customerZip;
    	var deliveryDatesArr = storePicker.GetATPDeliveryDates(cart, postalCode);
    } else {
		var deliveryDatesArr = mattressPipeletHelper.getDeliveryDates(cart); 
	}
	
	app.getView({
        Basket: cart.object,
        DeliveryDatesArr: deliveryDatesArr
    }).render('checkout/components/delivery-dates-checkout-popup');
}
/**
 * Update customer zipcode
 */
function updateCustomerZipcode() {
	var cart;
	var zipCode = request.httpParameterMap.Zipcode.stringValue;
    cart = app.getModel('Cart').get();
	if(zipCode) {
		if (!empty(cart)) {
				Transaction.wrap(function() {
					try {
						var Basket = cart ? cart.object : null;
						session.custom.customerZip != zipCode ?  MattressViewHelper.removeManualShippingDiscount(Basket) : null;				
					} catch (e) {
						
					}
					cart.calculate();
				});
		}
		session.custom.customerZip = zipCode;
		session.custom.customerZipUpdated = true;
	}
	if (request.httpParameterMap.format.stringValue === 'ajax') {
        let r = require('app_storefront_controllers/cartridge/scripts/util/Response');
        r.renderJSON({
            success: true
        });
    }
}
/**
 * Handles the selected shipping address and shipping method. Copies the
 * address details and gift options to the basket's default shipment. Sets the
 * selected shipping method to the default shipment.
 *
 * @transactional
 * @param {module:models/CartModel~CartModel} cart - A CartModel wrapping the current Basket.
 */
function handleShippingSettings(cart) {

    Transaction.wrap(function () {
        var defaultShipment, shippingAddress, validationResult, BasketStatus, EnableCheckout;
        defaultShipment = cart.getDefaultShipment();
        shippingAddress = cart.createShipmentShippingAddress(defaultShipment.getID());

        shippingAddress.setFirstName(session.forms.singleshipping.shippingAddress.addressFields.firstName.value);
        shippingAddress.setLastName(session.forms.singleshipping.shippingAddress.addressFields.lastName.value);
        shippingAddress.setAddress1(session.forms.singleshipping.shippingAddress.addressFields.address1.value);
        shippingAddress.setAddress2(session.forms.singleshipping.shippingAddress.addressFields.address2.value);
        shippingAddress.setCity(session.forms.singleshipping.shippingAddress.addressFields.cities.city.value);
        shippingAddress.setPostalCode(session.forms.singleshipping.shippingAddress.addressFields.postal.value);
        shippingAddress.setStateCode(session.forms.singleshipping.shippingAddress.addressFields.states.state.value);
        shippingAddress.setCountryCode(session.forms.singleshipping.shippingAddress.addressFields.country.value);
        shippingAddress.setPhone(session.forms.singleshipping.shippingAddress.addressFields.phone.value);
        defaultShipment.setGift(session.forms.singleshipping.shippingAddress.isGift.value);
        defaultShipment.setGiftMessage(session.forms.singleshipping.shippingAddress.giftMessage.value);

        cart.updateShipmentShippingMethod(cart.getDefaultShipment().getID(), session.forms.singleshipping.shippingAddress.shippingMethodID.value, null, null);

        pipeletHelper.updateBasketWithSelectedDate(session.forms.singleshipping.shippingAddress.addressFields.deliveryDate.htmlValue, session.forms.singleshipping.shippingAddress.addressFields.fleetwiseToken.htmlValue, cart.object);
        
        //TODO
        //Call splitShipments() should be reviewed after getting clarifications
        //on Shipping Service return parameters to assign shipping method to shipment
        //Comment out till clarification
        //pipeletHelper.splitShipments(cart.object);
        
        
        cart.calculate();

        validationResult = cart.validateForCheckout();

        // TODO - what are those variables used for, do they need to be returned ?
        BasketStatus = validationResult.BasketStatus;
        EnableCheckout = validationResult.EnableCheckout;

    });

    return;
}

/**
 * Form handler for the singleshipping form. Handles the following actions:
 * - __save__ - saves the shipping address from the form to the customer address book. If in-store
 * shipments are enabled, saves information from the form about in-store shipments to the order shipment.
 * Flags the save action as done and calls the {@link module:controllers/Cart~Show|Cart controller Show function}.
 * If it is not able to save the information, calls the {@link module:controllers/Cart~Show|Cart controller Show function}.
 * - __selectAddress__ - updates the address details and page metadata, sets the ContinueURL property to COShipping-SingleShipping,  and renders the singleshipping template.
 * - __shipToMultiple__ - calls the {@link module:controllers/COShippingMultiple~Start|COShippingMutliple controller Start function}.
 * - __error__ - calls the {@link module:controllers/COShippingMethod~Start|COShipping controller Start function}.
 */
function singleShipping() {
    var singleShippingForm = app.getForm('singleshipping');
    singleShippingForm.handleAction({
        save: function () {
            var cart = app.getModel('Cart').get();

            if (cart) {

                handleShippingSettings(cart); 

                // Attempts to save the used shipping address in the customer address book.
                if (customer.authenticated && session.forms.singleshipping.shippingAddress.addToAddressBook.value) {
                    app.getModel('Profile').get(customer.profile).addAddressToAddressBook(cart.getDefaultShipment().getShippingAddress());
                }
                // Binds the store message from the user to the shipment.
                if (Site.getCurrent().getCustomPreferenceValue('enableStorePickUp')) {

                    if (!app.getForm(session.forms.singleshipping.inStoreShipments.shipments).copyTo(cart.getShipments())) {
                        require('./Cart').Show();
                        return;
                    }
                }
                var COShippingController = require('~/cartridge/controllers/COShipping');
                COShippingController.PrepareShipments();
                COShippingController.UpdateDeliveryDateSelection(cart);
                response.redirect(URLUtils.https('COBilling-Start'));
                
            } else {
                // @FIXME redirect
                app.getController('Cart').Show();
            }
        },
        savemobile: function () {
            var cart = app.getModel('Cart').get();

            if (cart) {

                //handleShippingSettings(cart); 

                // Attempts to save the used shipping address in the customer address book.
                if (customer.authenticated && session.forms.singleshipping.shippingAddress.addToAddressBook.value) {
                    app.getModel('Profile').get(customer.profile).addAddressToAddressBook(cart.getDefaultShipment().getShippingAddress());
                }
                // Binds the store message from the user to the shipment.
                if (Site.getCurrent().getCustomPreferenceValue('enableStorePickUp')) {

                    if (!app.getForm(session.forms.singleshipping.inStoreShipments.shipments).copyTo(cart.getShipments())) {
                        require('./Cart').Show();
                        return;
                    }
                }
                var COShippingController = require('~/cartridge/controllers/COShipping');
                COShippingController.PrepareShipments();
               //COShippingController.UpdateDeliveryDateSelection(cart);
                
                // Start shipping address process
                response.redirect(URLUtils.https('COShipping-Start'));
                                
            } else {
                // @FIXME redirect
                app.getController('Cart').Show();
            }
        },
        saveDeliveryDate: function () {
        	var cart = app.getModel('Cart').get();
            var homeDeliveries = prepareShipments();
            var isRestrictedCheckout = mattressPipeletHelper.getIsRestrictedCheckout(session.custom.customerZip);
            var deliveryDatesArr;

            if (cart) {

                //handleShippingSettings(cart); 

                // Attempts to save the used shipping address in the customer address book.
                if (customer.authenticated && session.forms.singleshipping.shippingAddress.addToAddressBook.value) {
                    app.getModel('Profile').get(customer.profile).addAddressToAddressBook(cart.getDefaultShipment().getShippingAddress());
                }
                // Binds the store message from the user to the shipment.
                if (Site.getCurrent().getCustomPreferenceValue('enableStorePickUp')) {

                    if (!app.getForm(session.forms.singleshipping.inStoreShipments.shipments).copyTo(cart.getShipments())) {
                        require('./Cart').Show();
                        return;
                    }
                }
              
                
                if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule')) {
                    var postalCode = session.forms.singleshipping.shippingAddress.addressFields.postal.value;
                    deliveryDatesArr = storePicker.GetATPDeliveryDates(cart, postalCode);
                } else {
                    deliveryDatesArr = mattressPipeletHelper.getDeliveryDates(cart); 
                }
                                
                if (empty(deliveryDatesArr) ){
                	session.forms.singleshipping.shippingAddress.addressFields.deliveryDate.value = null;
                	session.forms.singleshipping.shippingAddress.addressFields.deliveryTime.value = null;
                }
                
                var COShippingController = require('~/cartridge/controllers/COShipping'); 
                COShippingController.PrepareShipments();
                COShippingController.UpdateDeliveryDateSelection(cart);
                
                var deliveryScheduleChange = session.forms.singleshipping.shippingAddress.deliveryScheduleChanged.value;
                
            	// If deliver schedule changed on shipping screen. Go directly to billing
                if (deliveryScheduleChange && deliveryScheduleChange === true) {
                	
                	 response.redirect(URLUtils.https('COBilling-Start'));
                	 
                } else {
	                // Render second ship method page
	                app.getView({
	                    ContinueURL: URLUtils.https('COShippingMethod-SingleShipping'),
	                    Basket: cart.object,
	                    HomeDeliveries: homeDeliveries,
	                    DeliveryDatesArr: deliveryDatesArr,
	                    isRestrictedCheckout: isRestrictedCheckout.status
	                }).render('checkout/shipping/mobile_shippingmethodstep2');
                }
            }
        },
        selectAddress: function () {
            updateAddressDetails(app.getModel('Cart').get());

            var pageMeta = require('app_storefront_controllers/cartridge/scripts/meta');
            pageMeta.update({
                pageTitle: Resource.msg('singleshipping.meta.pagetitle', 'checkout', 'SiteGenesis Checkout')
            });
            app.getView({
                ContinueURL: URLUtils.https('COShippingMethod-SingleShipping')
            }).render('checkout/shipping/singleshipping');

            return;
        },
        error: function () {
            response.redirect(URLUtils.https('COShippingMethod-Start'));
            return;
        }
    });
}
/**
 * Handle coupon code deletion from shippingMethod page
 */
function handleCouponCheckout() {
    var cartForm = app.getForm('cart');
    var cart = app.getModel('Cart').get();
    cartForm.handleAction({
        error: function () {
            response.redirect(URLUtils.https('COShippingMethod-Start'));
            return;
        },
        deleteCoupon: function (formgroup) {
        	var abc = 0;
            Transaction.wrap(function () {
                cart.removeCouponLineItem(formgroup.getTriggeredAction().object);
            });
            response.redirect(URLUtils.https('COShippingMethod-Start'));
            return ;
        }
    });
}
/**
 * Selects a shipping method for the default shipment. Creates a transient address object, sets the shipping
 * method, and returns the result as JSON response.
 *
 * @transaction
 */
function selectShippingMethod() {
    var cart, address, applicableShippingMethods, TransientAddress;
    TransientAddress = app.getModel('TransientAddress');
    cart = app.getModel('Cart').get();

    if (cart) {

        address = new TransientAddress();
        address.countryCode = session.forms.singleshipping.shippingAddress.addressFields.country.value;
        address.stateCode = session.forms.singleshipping.shippingAddress.addressFields.states.state.value;
        address.postalCode = session.forms.singleshipping.shippingAddress.addressFields.postal.value;
        address.city = session.forms.singleshipping.shippingAddress.addressFields.cities.city.value;
        address.address1 = session.forms.singleshipping.shippingAddress.addressFields.address1.value;
        address.address2 = session.forms.singleshipping.shippingAddress.addressFields.address2.value;

        applicableShippingMethods = cart.getApplicableShippingMethods(address);

        Transaction.wrap(function () {
            cart.updateShipmentShippingMethod(cart.getDefaultShipment().getID(), request.httpParameterMap.shippingMethodID.stringValue, null, applicableShippingMethods);
            cart.calculate();
        });

        app.getView({
            Basket: cart.object
        }).render('checkout/shipping/selectshippingmethodjson');
    } else {
        app.getView.render('checkout/shipping/selectshippingmethodjson');
    }
}

/**
 * Determines the list of applicable shipping methods for the default shipment of
 * the current basket. The applicable shipping methods are based on the
 * merchandise in the cart and any address parameters included in the request.
 * Changes the shipping method of this shipment if the current method
 * is no longer applicable. Precalculates the shipping cost for each applicable
 * shipping method by simulating the shipping selection i.e. explicitly adds each
 * shipping method and then calculates the cart.
 * The simulation is done so that shipping cost along
 * with discounts and promotions can be shown to the user before making a
 * selection.
 * @transaction
 */
function updateShippingMethodList() {
    var i, cart, address, applicableShippingMethods, shippingCosts, currentShippingMethod, method, TransientAddress;
    TransientAddress = app.getModel('TransientAddress');
    cart = app.getModel('Cart').get();

    if (cart) {

        address = new TransientAddress();
        address.countryCode = session.forms.singleshipping.shippingAddress.addressFields.country.value;
        address.stateCode = session.forms.singleshipping.shippingAddress.addressFields.states.state.value;
        address.postalCode = session.forms.singleshipping.shippingAddress.addressFields.postal.value;
        address.city = session.forms.singleshipping.shippingAddress.addressFields.cities.city.value;
        address.address1 = session.forms.singleshipping.shippingAddress.addressFields.address1.value;
        address.address2 = session.forms.singleshipping.shippingAddress.addressFields.address2.value;

        applicableShippingMethods = cart.getApplicableShippingMethods(address);
        shippingCosts = new HashMap();
        currentShippingMethod = cart.getDefaultShipment().getShippingMethod() || ShippingMgr.getDefaultShippingMethod();

        // Transaction controls are for fine tuning the performance of the data base interactions when calculating shipping methods
        Transaction.begin();

        for (i = 0; i < applicableShippingMethods.length; i += 1) {
            method = applicableShippingMethods[i];

            cart.updateShipmentShippingMethod(cart.getDefaultShipment().getID(), method.getID(), method, applicableShippingMethods);
            cart.calculate();
            shippingCosts.put(method.getID(), cart.preCalculateShipping(method));
        }

        Transaction.rollback();

        Transaction.wrap(function () {
            cart.updateShipmentShippingMethod(cart.getDefaultShipment().getID(), currentShippingMethod.getID(), currentShippingMethod, applicableShippingMethods);
            cart.calculate();
        });

        session.forms.singleshipping.shippingAddress.shippingMethodID.value = cart.getDefaultShipment().getShippingMethodID();

        app.getView({
            Basket: cart.object,
            ApplicableShippingMethods: applicableShippingMethods,
            ShippingCosts: shippingCosts
        }).render('checkout/shipping/shippingmethods');
    } else {
        app.getController('Cart').Show();
    }
}

/**
 * Determines the list of applicable shipping methods for the default shipment of
 * the current customer's basket and returns the response as a JSON array. The
 * applicable shipping methods are based on the merchandise in the cart and any
 * address parameters are included in the request parameters.
 */
function getApplicableShippingMethodsJSON() {
    var cart, address, applicableShippingMethods, TransientAddress;
    TransientAddress = app.getModel('TransientAddress');
    cart = app.getModel('Cart').get();

    address = new TransientAddress();
    address.countryCode = session.forms.singleshipping.shippingAddress.addressFields.country.value;
    address.stateCode = session.forms.singleshipping.shippingAddress.addressFields.states.state.value;
    address.postalCode = session.forms.singleshipping.shippingAddress.addressFields.postal.value;
    address.city = session.forms.singleshipping.shippingAddress.addressFields.cities.city.value;
    address.address1 = session.forms.singleshipping.shippingAddress.addressFields.address1.value;
    address.address2 = session.forms.singleshipping.shippingAddress.addressFields.address2.value;

    applicableShippingMethods = cart.getApplicableShippingMethods(address);

    app.getView({
        ApplicableShippingMethods: applicableShippingMethods
    }).render('checkout/shipping/shippingmethodsjson');
}

function initBillingAddressForm(cart) {

    if (app.getForm('singleshipping').object.shippingAddress.useAsBillingAddress.value === true) {
        app.getForm('billing').object.billingAddress.addressFields.firstName.value = app.getForm('singleshipping').object.shippingAddress.addressFields.firstName.value;
        app.getForm('billing').object.billingAddress.addressFields.lastName.value = app.getForm('singleshipping').object.shippingAddress.addressFields.lastName.value;
        app.getForm('billing').object.billingAddress.addressFields.address1.value = app.getForm('singleshipping').object.shippingAddress.addressFields.address1.value;
        app.getForm('billing').object.billingAddress.addressFields.address2.value = app.getForm('singleshipping').object.shippingAddress.addressFields.address2.value;
        app.getForm('billing').object.billingAddress.addressFields.city.value = app.getForm('singleshipping').object.shippingAddress.addressFields.cities.city.value;
        app.getForm('billing').object.billingAddress.addressFields.postal.value = app.getForm('singleshipping').object.shippingAddress.addressFields.postal.value;
        app.getForm('billing').object.billingAddress.addressFields.phone.value = app.getForm('singleshipping').object.shippingAddress.addressFields.phone.value;
        app.getForm('billing').object.billingAddress.addressFields.states.state.value = app.getForm('singleshipping').object.shippingAddress.addressFields.states.state.value;
        app.getForm('billing').object.billingAddress.addressFields.country.value = app.getForm('singleshipping').object.shippingAddress.addressFields.country.value;
        app.getForm('billing').object.billingAddress.addressFields.phone.value = app.getForm('singleshipping').object.shippingAddress.addressFields.phone.value;
    } else if (cart.getBillingAddress() !== null) {
        app.getForm('billing.billingAddress.addressFields').copyFrom(cart.getBillingAddress());
        app.getForm('billing.billingAddress.addressFields.states').copyFrom(cart.getBillingAddress());
    } else if (customer.authenticated && customer.profile.addressBook.preferredAddress !== null) {

        app.getForm('billing.billingAddress.addressFields').copyFrom(customer.profile.addressBook.preferredAddress);
        app.getForm('billing.billingAddress.addressFields.states').copyFrom(customer.profile.addressBook.preferredAddress);
    }
}
/*
* Module exports
*/

/*
* Web exposed methods
*/
/** Starting point for the single shipping scenario.
 * @see module:controllers/COShippingMethod~start */
exports.Start = guard.ensure(['https', 'loggedIn'], start);
/** Selects a shipping method for the default shipment.
 * @see module:controllers/COShippingMethod~selectShippingMethod */
exports.SelectShippingMethod = guard.ensure(['https', 'get'], selectShippingMethod);
/** Determines the list of applicable shipping methods for the default shipment of the current basket.
 * @see module:controllers/COShippingMethod~updateShippingMethodList */
exports.UpdateShippingMethodList = guard.ensure(['https', 'get'], updateShippingMethodList);
/** Determines the list of applicable shipping methods for the default shipment of the current customer's basket and returns the response as a JSON array.
 * @see module:controllers/COShippingMethod~getApplicableShippingMethodsJSON */
exports.GetApplicableShippingMethodsJSON = guard.ensure(['https', 'get'], getApplicableShippingMethodsJSON);
/** Form handler for the singleshipping form.
 * @see module:controllers/COShippingMethod~singleShipping */
exports.SingleShipping = guard.ensure(['https'], singleShipping);
/** Form handler for the singleshipping form.
 * @see module:controllers/COShippingMethod~handleCouponCheckout */
exports.HandleCouponCheckout = guard.ensure(['https', 'post'], handleCouponCheckout);
/** Update customer zipcode.
 * @see module:controllers/COShippingMethod~updateCustomerZipcode */
exports.UpdateCustomerZipcode = guard.ensure(['https', 'get'], updateCustomerZipcode);
/** delivery Dates Calender.
 * @see module:controllers/COShippingMethod~deliveryDatesCalender */
exports.DeliveryDatesCalender = guard.ensure(['https', 'get'], deliveryDatesCalender);

/*
 * Local methods
 */
exports.PrepareShipments = prepareShipments;
exports.CalculateShippingAndTaxForZip = calculateShippingAndTaxForZip;