'use strict';
/**
 * Controller for retrieving a list of available stores with inventory for a specified product.
 *
 * @module controllers/StorePicker
 */

/* API Includes */
var HashMap = require('dw/util/HashMap');
var Countries = require('app_storefront_core/cartridge/scripts/util/Countries');
var StoreMgr = require('dw/catalog/StoreMgr');
var Site = require('dw/system/Site');
var URLUtils = require('dw/web/URLUtils');
var Transaction = require('dw/system/Transaction');
var CustomObjectManager = require('dw/object/CustomObjectMgr');
var Logger = require('dw/system/Logger');

/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var ATPInventoryServices = require("bc_sleepysc/cartridge/scripts/init/services-ATPInventory");
var Basket = dw.order.BasketMgr.getCurrentBasket();

function start() {
	if (request.httpParameterMap.isCart.value == 'true') {
		var prodID = '';
	} else {
		var prodID = request.httpParameterMap.pid.value;
	}
	if(request.httpParameterMap.v1.value == 'pdp_redesign') {
		app.getView({
		  ContinueURL: URLUtils.http('StorePicker-HandleForm','v1','pdp_redesign'),
		  prodID: request.httpParameterMap.pid.value,
		  isBopis: request.httpParameterMap.isBopis.value
	    }).render('components/storepickup/storepicker_ab');
		
	} else {
		app.getView({
		  ContinueURL: URLUtils.http('StorePicker-HandleForm'),
		  prodID: request.httpParameterMap.pid.value,
		  isBopis: request.httpParameterMap.isBopis.value
	    }).render('components/storepickup/storepicker');
	} 
	
}

function handleForm() {
	var pdp_param_ab = request.httpParameterMap.v1.value;
	var geolocationZip = empty(session.custom.customerZip) ? request.getGeolocation().getPostalCode() : session.custom.customerZip;	
	var postalCode = (empty(session.forms.storepicker.postalCode.value)) ? geolocationZip : session.forms.storepicker.postalCode.value;
	var distance = session.forms.storepicker.maxdistance.value;
	var productID = session.forms.storepicker.productID.value;
	var isBopis = session.forms.storepicker.isBopis.value;
	session.custom.isBopis = isBopis;
	if(!empty(pdp_param_ab) && pdp_param_ab == 'pdp_redesign') {
		findInStore_ab(postalCode, distance, productID, isBopis);
	}else {
		findInStore(postalCode, distance, productID, isBopis);
	}
	
}

function findInStore(customerZip, maxDistance, productID, isBopis) {
	session.custom.customerZip = customerZip;
	session.custom.maxDistance = maxDistance;
	session.custom.isCart = false;
	var storeArray = [];
	var storeMap = getNearbyStores(maxDistance);
	if (!empty(productID) && isBopis == 'false') {		
		// convert map to array of stores
		for each( var store in storeMap.entrySet()) {
			storeArray.push(store);
		}
	} else if (!empty(productID) && isBopis == 'true') {		
		if (request.httpParameterMap.requestAgain.booleanValue == true) {
			var params=request.httpParameterMap;
			var stores= params.getParameterNames()[1];
			var storeList = JSON.parse(stores);
			storeList = storeList.stores;
			for each (var store in storeMap.entrySet()) {
				if ((storeList.indexOf(store.key.ID) > -1)) {
					continue;
				}
				var requestedDate = new Date();
				requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
				try {
					var requestPacket = '{"InventoryType": "Store","ZipCode": "' + customerZip + '","RequestedDate": "' + requestedDate + '","StoreId": "' + store.key.ID + '","Products": [{"ProductId": "' + productID + '","Quantity": 1}]}';
					var service = ATPInventoryServices.ATPInventoryMonthMSOS;
					var result = service.call(requestPacket);
					if (result.error != 0 || result.errorMessage != null || result.mockResult) {
							//return null;
					} else {
						var jsonResponse = JSON.parse(result.object.text);
						session.custom[productID + "-" + store.key.ID] = jsonResponse[0];
					}
				} catch (e) {
					var msg = e;
				}
				storeArray.push(store);
				if (storeArray.length == 4) {
					break;
				}
			}
		} else {
			for each (var store in storeMap.entrySet()) {
				var requestedDate = new Date();
				requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
				try {
					var requestPacket = '{"InventoryType": "Store","ZipCode": "' + customerZip + '","RequestedDate": "' + requestedDate + '","StoreId": "' + store.key.ID + '","Products": [{"ProductId": "' + productID + '","Quantity": 1}]}';
					var service = ATPInventoryServices.ATPInventoryMSOS;
					var result = service.call(requestPacket);
					if (result.error != 0 || result.errorMessage != null || result.mockResult) {
						//return null;
					} else {
						var jsonResponse = JSON.parse(result.object.text);
						session.custom[productID + "-" + store.key.ID] = jsonResponse[0];
					}
				} catch (e) {
					var msg = e;
				}
				
				storeArray.push(store);
				if (storeArray.length == 5) {
					break;
				}
			}
		}
	} else {
		session.custom.isCart = true;
		storeArray = evaluateCartForBopis(maxDistance);
	}
	
	
	
	app.getView({
		stores:storeArray,
		pid: productID,
		zip: customerZip,
		isBopis : isBopis,
		isCart : session.custom.isCart,
		totalStores : storeMap.length 
	}).render('components/storepickup/storepicker');
}

function findInStore_ab(customerZip, maxDistance, productID, isBopis) {
	session.custom.customerZip = customerZip;
	session.custom.maxDistance = maxDistance;
	session.custom.isCart = false;
	var storeArray = [];
	var storeMap = getNearbyStores(maxDistance);
	if (!empty(productID) && isBopis == 'false') {		
		// convert map to array of stores
		for each( var store in storeMap.entrySet()) {
			storeArray.push(store);
		}
	} else if (!empty(productID) && isBopis == 'true') {		
		if (request.httpParameterMap.requestAgain.booleanValue == true) {
			var params=request.httpParameterMap;
			var stores= params.getParameterNames()[1];
			var storeList = JSON.parse(stores);
			storeList = storeList.stores;
			for each (var store in storeMap.entrySet()) {
				if ((storeList.indexOf(store.key.ID) > -1)) {
					continue;
				}
				var requestedDate = new Date();
				requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
				try {
					var requestPacket = '{"InventoryType": "Store","ZipCode": "' + customerZip + '","RequestedDate": "' + requestedDate + '","StoreId": "' + store.key.ID + '","Products": [{"ProductId": "' + productID + '","Quantity": 1}]}';
					var service = ATPInventoryServices.ATPInventoryMSOS;
					var result = service.call(requestPacket);
					if (result.error != 0 || result.errorMessage != null || result.mockResult) {
							//return null;
					} else {
						var jsonResponse = JSON.parse(result.object.text);
						session.custom[productID + "-" + store.key.ID] = jsonResponse[0];
					}
				} catch (e) {
					var msg = e;
				}
				storeArray.push(store);
				if (storeArray.length == 4) {
					break;
				}
			}
		} else {
			for each (var store in storeMap.entrySet()) {
				var requestedDate = new Date();
				requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
				try {
					var requestPacket = '{"InventoryType": "Store","ZipCode": "' + customerZip + '","RequestedDate": "' + requestedDate + '","StoreId": "' + store.key.ID + '","Products": [{"ProductId": "' + productID + '","Quantity": 1}]}';
					var service = ATPInventoryServices.ATPInventoryMSOS;
					var result = service.call(requestPacket);
					if (result.error != 0 || result.errorMessage != null || result.mockResult) {
						//return null;
					} else {
						var jsonResponse = JSON.parse(result.object.text);
						session.custom[productID + "-" + store.key.ID] = jsonResponse[0];
					}
				} catch (e) {
					var msg = e;
				}
				
				storeArray.push(store);
				if (storeArray.length == 5) {
					break;
				}
			}
		}
	} else {
		session.custom.isCart = true;
		storeArray = evaluateCartForBopis(maxDistance);
	}
	
	
	
	app.getView({
		stores:storeArray,
		pid: productID,
		zip: customerZip,
		isBopis : isBopis,
		isCart : session.custom.isCart,
		totalStores : storeMap.length 
	}).render('components/storepickup/storepicker_ab');
}

function getNearbyStores(maxDistance) {
	var customerZip = session.custom.customerZip;
	var countryCode = Countries.getCurrent({
		CurrentRequest: {
				locale: request.locale
			}
		}).countryCode;
	var distanceUnit = (countryCode == 'US') ? 'mi' : 'km';
	var storeMap = StoreMgr.searchStoresByPostalCode(countryCode, customerZip, distanceUnit, maxDistance);	
	return storeMap;
}

function setPreferredStore() {
//	var form = session.forms;
	var customer = session.customer; 
	if (customer.registered && !empty(request.httpParameterMap.storeId.value)) {
		Transaction.wrap(function () {
			customer.profile.custom.preferredStore = request.httpParameterMap.storeId.value;
		});
	} else if (!empty(request.httpParameterMap.storeId.value)) {
		session.custom.preferredStore = request.httpParameterMap.storeId.value;
	}
	
}

function setPreferredStoreAB() {	
	var responseUtils = require('app_storefront_controllers/cartridge/scripts/util/Response');
//	var form = session.forms;
	var customer = session.customer; 
	if (customer.registered && !empty(request.httpParameterMap.storeId.value)) {
		Transaction.wrap(function () {
			customer.profile.custom.preferredStore = request.httpParameterMap.storeId.value;
		});
	} else if (!empty(request.httpParameterMap.storeId.value)) {
		session.custom.preferredStore = request.httpParameterMap.storeId.value;
	}
	
	responseUtils.renderJSON({        
        SetCustomerPreferredStore: true         
    });
	
}

function setPreferredStoreFromGeoLocation() {
	var storeMap = getNearbyStores(100);
	for each( var store in storeMap.entrySet()) {
		session.custom.preferredStore = store.key.ID;
		break;
	}
}

function getPreferredStoreAddress() {
	var store = getPreferredStore();
	var prodId = request.httpParameterMap.pid ? request.httpParameterMap.pid : false;
	var ProductMgr = require('dw/catalog/ProductMgr');
	var product = ProductMgr.getProduct(prodId.value);
	var inventoryParam = product.isVariant() ? product.masterProduct.ID : product.ID;
	
	var isBopis = request.httpParameterMap.isBopis ? request.httpParameterMap.isBopis.booleanValue : false;
	var isInventoryList = false;
	if (store != null && isBopis == false) {
		var inventory = store.inventoryList;
		if (!empty(inventory)) {
			try {
				var record = inventory.getRecord(inventoryParam.replace('mfi',''));
			} catch (e) {
				var error = e;
			}
			if (!empty(record)) {
				isInventoryList = true;
			}
		}
	} else if (store != null && isBopis == true) {
		var requestedDate = new Date();
		requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
		var products = new Array();
		products.push({
			"ProductId" : prodId.value,
			"Quantity" : 1
		});
		try {
			var requestPacket = '{"InventoryType": "Store","ZipCode": "' + store.postalCode + '","RequestedDate": "' + requestedDate + '","StoreId": "' + store.ID + '","Products": ' + JSON.stringify(products) + '}';
			var service = ATPInventoryServices.ATPInventoryMSOS;
			var result = service.call(requestPacket);
			if (result.error != 0 || result.errorMessage != null || result.mockResult) {
				
			} else {
				var jsonResponse = JSON.parse(result.object.text);
				var response = jsonResponse[0];
				if (response.Available.toLowerCase() == 'yes' && response.ATPQuantity > 0) {
					session.custom[prodId.value + '-' + store.ID] = response;
					isInventoryList = true;
				}
			}
		} catch (e) {
			var error = e;
		}
	}
	
	if(request.httpParameterMap.v1.value == 'pdp_redesign') {
		app.getView({
		 preferredStore: store,
		 pid: prodId,
		 isInventoryList: isInventoryList
	    }).render('product/preferredStore_ab');
	} else {
		app.getView({
		 preferredStore: store,
		 pid: prodId,
		 isInventoryList: isInventoryList
	    }).render('product/preferredStore');
	}	
}

function getPreferredStore() {
	var store = null;
	var storeIdValue =  request.httpParameterMap.storeId;
	if (storeIdValue != null && storeIdValue != "") {
		if(storeIdValue.toString().length < 6) {
			var missingLeadingZero = Number(6 - storeIdValue.toString().length);
			for(var leadingZero = 0; leadingZero < missingLeadingZero; leadingZero++) {
				storeIdValue = '0'+storeIdValue.toString();
			}
			
		}
		if (session.customer.registered) {
			store = StoreMgr.getStore(storeIdValue);			
		} else if (StoreMgr.getStore(storeIdValue)) {
			store = StoreMgr.getStore(storeIdValue);
		}
		
		if(store == null || store == "") {
			if (session.customer.registered && !empty(session.customer.profile.custom.preferredStore)) {
				store = StoreMgr.getStore(session.customer.profile.custom.preferredStore);
			} else if (StoreMgr.getStore(session.custom.preferredStore)) {
				store = StoreMgr.getStore(session.custom.preferredStore);
			}
		}
	}
	else {
		if (session.customer.registered && !empty(session.customer.profile.custom.preferredStore)) {
			store = StoreMgr.getStore(session.customer.profile.custom.preferredStore);
		} else if (StoreMgr.getStore(session.custom.preferredStore)) {
			store = StoreMgr.getStore(session.custom.preferredStore);
		}
	}
	
	
	
	return store;
}

function updateBopisOption(data) {
	var params = request.httpParameterMap;
	var deliveryoption = params.deliveryoption.stringValue;
	session.custom.deliveryOptionChoice = deliveryoption;
}

function evaluateCartForBopis(maxDistance) {
	if (typeof maxDistance == 'undefined') {
		maxDistance = 100;
	}
	var storeMap = getNearbyStores(maxDistance);
	var storeArray = [];
	for (var i = 0; i < storeMap.entrySet().length; i++) {
		var store = storeMap.entrySet()[i];
		var hasProducts = storeHasProducts(store, true);
		storeArray.push(store);
		if (storeArray.length == 5) {
			break;
		}
	}
	return storeArray;
}
function getAvailablePickupStore() {
	var preferredStore = getPreferredStore();	
	// IF a preferred store is already selected, check if products are all available in preferred store
	if (preferredStore != null) {
		var hasProducts = storeHasProducts(preferredStore);
		if (hasProducts) {
			return preferredStore;
		} else {
			return null;
		}
	}
	return null;
}

/** This function accepts a store and the line item container
 * it checks availability of each product line item in the store and returns
 * true only if all products are available.
 * @param store
 * @param isCart
 * @returns boolean
 */
function storeHasProducts(store, isCart) {
	if (empty(Basket)) {
		return false;
	}
	var plis = Basket.productLineItems;
	var storeID = (store instanceof dw.util.MapEntry) ? store.key.ID : store.ID;
	var storePostalCode = (store instanceof dw.util.MapEntry) ? store.key.postalCode : store.postalCode;
	var products = new Array();
	session.custom.wasbopis = (session.custom.wasbopis != null) ? session.custom.wasbopis : 0;
	for (var i = 0; i < plis.length; i++ ) {
		var pli = plis[i];
		/* if (empty(pli.custom.storePickupStoreID) && pli.custom.storePickupStoreID != storeID) {
			Transaction.wrap( function() {
				pli.custom.storePickupStoreID = storeID;
			});
		} */
		products.push({
			'ProductId' : pli.productID,
			'Quantity' : pli.quantityValue
		});
		var optionLineItems = pli.optionProductLineItems;
		for (var j = 0; j < optionLineItems.length; j++) {
			var oli = optionLineItems[j];
			if (oli.productID != 'none') {
				products.push({
					'ProductId' : oli.productID,
					'Quantity' : oli.quantityValue
				});
			}
		}
	}
	
	if (products.length > 0) {
		var requestedDate = new Date();
		requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
		try {
			var requestPacket = '{"InventoryType": "Store","ZipCode": "' + storePostalCode + '","RequestedDate": "' + requestedDate + '","StoreId": "' + storeID + '","Products": ' + JSON.stringify(products) + '}';
			var service = ATPInventoryServices.ATPInventoryMSOS;
			var result = service.call(requestPacket);
			if (result.error != 0 || result.errorMessage != null || result.mockResult) {
				return false;
			} else {
				var jsonResponse = JSON.parse(result.object.text);
				var response = jsonResponse[0];
				for (var i = 0; i < plis.length; i++) {
					var pli = plis[i];
					session.custom[pli.productID + '-' + storeID] = response;
					/* if (pli.custom.storePickupStoreID != storeID) {
						Transaction.wrap( function() {
							pli.custom.storePickupStoreID = response.Location1;
							pli.custom.deliveryDate = response.SlotDate;
						});
					} */
				}
				if (isCart == true) {
					session.custom['storeId' + storeID] = response;
				}
			}
			return true;
		} catch (e) {
			var error = e;
		}
	}
	return false;
}
function updatePreferredStoreLineItems() {
	if (!empty(Basket)) {
		var plis = Basket.productLineItems;
	}
	
	
}

/**
 * resets the shipping options to Ship To Address and calls {@link module:controllers/COShipping~start|start} function.
 *
 * @transactional
 */
function changeDeliveryToShipAddress() {
	session.forms.singleshipping.clearFormElement();
	app.getController('Cart').ChangeDeliveryOption('shipped');
	session.custom.deliveryOptionChoice = 'shipped';
	app.getController('COShipping').Start();
}

function getATPDeliveryDatesForNextWeek() {
	var params = request.httpParameterMap;
	var cart = app.getModel('Cart').get();
	var postalCode = session.forms.singleshipping.shippingAddress.addressFields.postal.value;
	var format = params.format.stringValue;
	var date = params.currentWeekDate.stringValue;
	
	// make changes to this function call for webpos ATPInventoryMonth call 
	var deliveryDatesArr = getATPDeliveryDates(cart, postalCode, date);
	
	app.getView({
		DeliveryDatesArr: deliveryDatesArr
	}).render('checkout/components/delivery-weekly-schedule-mocked');
}

/** This function accepts products array and the postal code
 * it makes an array of core products along with postalCode at checkout to trigger
 * utag link whenever atp fails
 * @param products (shipment type 'core' only)
 * @param postcalCode
 * @returns array
 */
function makeATPFailedCoreProductsArray(products, postalCode) {
	var atpFailedCoreProductsArray = new Array();
	for (var i = 0; i < products.length; i++) {
		var coreProduct = products[i];
		atpFailedCoreProductsArray.push("Checkout_" + coreProduct.ProductId +"_" + postalCode);
	}
	return atpFailedCoreProductsArray;
}

function getATPDeliveryDates(cart, postalCode, date) {
	session.custom.atpFailedCoreProductsArray = null;
	var products = new Array();
	var deliveryDatesArr = new dw.util.ArrayList();
	if (empty(Basket)) {
		return deliveryDatesArr;
	}
	var plis = Basket.productLineItems;
	for (var i = 0; i < plis.length; i++ ) {
		var pli = plis[i];
		if (!empty(pli.product.custom.shippingInformation) && pli.product.custom.shippingInformation.toLowerCase() == 'core') {
			products.push({
				'ProductId' : pli.productID,
				'Quantity' : pli.quantityValue
			});
			var optionLineItems = pli.optionProductLineItems;
			for (var j = 0; j < optionLineItems.length; j++) {
				var oli = optionLineItems[j];
				if (oli.productID != 'none') {
					products.push({
						'ProductId' : oli.productID,
						'Quantity' : oli.quantityValue
					});
				}
			}
		}
	}
	try {
		if (products.length > 0)
		{
			var requestedDate = new Date();
			requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
			
			var requestAtpAgain = false;
			var requestPacket = '{"InventoryType": "Delivery","ZipCode": "' + postalCode + '","RequestedDate": "' + requestedDate + '","StoreId": "","Products": ' + JSON.stringify(products) + '}';
			var service = ATPInventoryServices.ATPInventoryMonthMSOS;			
			var result = service.call(requestPacket);
						
			if (result.error != 0 || result.errorMessage != null || result.mockResult) {
				session.custom.atpFailedCoreProductsArray = makeATPFailedCoreProductsArray(products, postalCode);
			} else {
				var jsonResponse = JSON.parse(result.object.text);
				if (arguments.length > 2 && !empty(arguments[2]) && !empty(session.custom.deliveryDates)) {
					var deliveryDateSession = session.custom.deliveryDates;
				} else {
					var deliveryDateSession = new Object();
				}
				
				var checkForDuplicates = new dw.util.ArrayList();
				for (var i = 0; i < jsonResponse.length; i++ ) {
					var response = jsonResponse[i];
					var deliveryDate = new Date(response.SlotDate);
					deliveryDate.setHours(10);
											
					var key = deliveryDate.toDateString().replace(' ', '', 'g');
					if (!deliveryDateSession[key]) {
						deliveryDateSession[key] = new Object();
						deliveryDateSession[key]['timeslots'] = new dw.util.ArrayList();
						deliveryDatesArr.add(deliveryDate);
					}
											
					var startTime = new Date(deliveryDate.toDateString() + ' ' + response.StartTime);
					var endTime = new Date(deliveryDate.toDateString() + ' ' + response.EndTime);
					if (checkForDuplicates.indexOf(key + startTime + endTime) == -1 && response.Available.toLowerCase() == 'yes' && response.Slots > 0) {
						checkForDuplicates.add(key + startTime + endTime);
						deliveryDateSession[key]['timeslots'].add({
							'location1' : response.Location1,
							'location2' : response.location2,
							'mfiDSZoneLineId' : response.Zone,
							'mfiDSZipCode' : postalCode,
							'available' : response.Available.toLowerCase(),
							'startTime' : startTime,
							'endTime' : endTime,
							'slots' : Number(response.Slots)
						});
						
						var timeslotsComparator = new dw.util.PropertyComparator ('startTime', 'endTime');
						deliveryDateSession[key]['timeslots'].sort(timeslotsComparator);
						
						if ((requestAtpAgain === true && response.Available.toLowerCase() == 'yes' && response.SlotDate !== null) || (arguments.length == 4 && !empty(arguments[3]) && arguments[3] == true)) {
							requestAtpAgain = false;
						}
					}
				}
				session.custom.deliveryDates = deliveryDateSession;
			}
		}
	} catch (e) {
		var error = e;
	}
	return deliveryDatesArr;
}


//get delivery dates 
function getDeliveryZone(cart, postalCode, date) {
 //session.custom.customerZip = postalCode;
 var products = new Array();
 var deliveryDatesArr = new dw.util.ArrayList();
 var plis = Basket.productLineItems;
	for (var i = 0; i < plis.length; i++ ) {
		var pli = plis[i];
		if (!empty(pli.product.custom.shippingInformation) && pli.product.custom.shippingInformation.toLowerCase() == 'core') {
			products.push({
				'ProductId' : pli.productID,
				'Quantity' : pli.quantityValue
			});
			var optionLineItems = pli.optionProductLineItems;
			for (var j = 0; j < optionLineItems.length; j++) {
				var oli = optionLineItems[j];
				if (oli.productID != 'none') {
					products.push({
						'ProductId' : oli.productID,
						'Quantity' : oli.quantityValue
					});
				}
			}
		}
	}
 try {
     
     var requestedDate = new Date(date);
     requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
     var requestAtpAgain = true,
         requestCount = 0;
     do {
         var requestPacket = '{"InventoryType": "Delivery","ZipCode": "' + postalCode + '","RequestedDate": "' + requestedDate + '","StoreId": "","Products": ' + JSON.stringify(products) + '}';
         var service = ATPInventoryServices.ATPInventoryMSOS;
         var result = service.call(requestPacket);
         requestCount++;
         if (result.error != 0 || result.errorMessage != null || result.mockResult) {
             
         } else {
             var jsonResponse = JSON.parse(result.object.text);
             var deliveryDateSession = new Object();
             
             var checkForDuplicates = new dw.util.ArrayList();
             for (var i = 0; i < jsonResponse.length; i++ ) {
                 var response = jsonResponse[i];
                 var deliveryDate = new Date(response.SlotDate);
                 deliveryDate.setHours(10);
                 
                 var key = deliveryDate.toDateString().replace(' ', '', 'g');
                 if (!deliveryDateSession[key]) {
                     deliveryDateSession[key] = new Object();
                     deliveryDateSession[key]['timeslots'] = new dw.util.ArrayList();
                     deliveryDatesArr.add(deliveryDate);
                 }
                 
                 var startTime = new Date(deliveryDate.toDateString() + ' ' + response.StartTime);
                 var endTime = new Date(deliveryDate.toDateString() + ' ' + response.EndTime);
                 if (checkForDuplicates.indexOf(key + startTime + endTime) == -1) {
                     checkForDuplicates.add(key + startTime + endTime);
                     deliveryDateSession[key]['timeslots'].add({
                         'location1' : response.Location1,
                         'location2' : response.location2,
                         'mfiDSZoneLineId' : response.Zone,
                         'mfiDSZipCode' : postalCode,
                         'available' : response.Available.toLowerCase(),
                         'startTime' : startTime,
                         'endTime' : endTime,
                         'slots' : Number(response.Slots)
                     });
                     
                     var timeslotsComparator = new dw.util.PropertyComparator ('startTime', 'endTime');
                     deliveryDateSession[key]['timeslots'].sort(timeslotsComparator);
                     
                     if (requestAtpAgain === true && response.Available.toLowerCase() == 'yes' && response.SlotDate !== null) {
                         requestAtpAgain = false;
                     }
                 }
             }
             session.custom.deliveryDates = deliveryDateSession;
         }
         
     } while (requestAtpAgain === true && requestCount < 1); 
     
 } catch (e) {
     var error = e;
 }
 return deliveryDatesArr;
}


/*
	Takes as Input a Date Object and a Time string in format 'hh:mm PM' e.g. '01:25 PM'
	Returns as Output a Date Object with the Hours and Minutes set according to the Time string in Date object
*/
function getDateFromDateAndTimeString(dateObj, timeStr) {
	var test, parts, hours, minutes, date,
	d = dateObj.getTime(),
	timeReg = /(\d+)\:(\d+) (\w+)/;
	test = timeStr;
	parts = test.match(timeReg);
	hours = /am/i.test(parts[3]) ?
		function(am) {return am < 12 ? am : 0}(parseInt(parts[1], 10)) :
		function(pm) {return pm < 12 ? pm + 12 : 12}(parseInt(parts[1], 10));
	minutes = parseInt(parts[2], 10);
	date = new Date(d);
	date.setHours(hours);
	date.setMinutes(minutes);
	return date;
}

function calendarToUTCDate(calendar) {
	return Date.UTC(calendar.get(calendar.YEAR), 
					calendar.get(calendar.MONTH), 
					calendar.get(calendar.DAY_OF_MONTH), 
					calendar.get(calendar.HOUR_OF_DAY), 
					calendar.get(calendar.MINUTE), 
					calendar.get(calendar.SECOND));
}

function calendarToUTCDateWithoutTime(calendar) {
	return Date.UTC(calendar.get(calendar.YEAR), 
					calendar.get(calendar.MONTH), 
					calendar.get(calendar.DAY_OF_MONTH));
}

function dateToUTCDate(date) {
	return Date.UTC(date.getFullYear(), 
					date.getMonth(), 
					date.getDate(), 
					date.getHours(), 
					date.getMinutes(), 
					date.getSeconds());	
}

function dateToUTCDateWithoutTime(date) {
	return Date.UTC(date.getFullYear(), 
					date.getMonth(), 
					date.getDate());	
}

/*
	Input
	    Month:        Number,    Range 0-11,    e.g. 2 = March
	    Instance:    Number,    Range 1-4,    e.g. 2 = 2nd Sunday 
	Output:
	    Date:        Date,    Return the date object as "Sun Mar dd 02:00:00 GMT yyyy"
*/

function getDateOfAnInstanceOfSundayOfSPecificMonth(month, instance) {
	var date = new Date();
	date.setMonth(month);
	date.setDate(7);
	var requiredSunday = 7*(instance-1) + (7 - date.getDay());
	date.setDate(requiredSunday);
	date.setHours(2,0,0);    // 02:00 AM
	return date; 
}

function isDaylightSavingONfromDate(date) {
	var secondSundayOfMarch = dateToUTCDate(getDateOfAnInstanceOfSundayOfSPecificMonth(2,2));
	var firstSundayOfNovember = dateToUTCDate(getDateOfAnInstanceOfSundayOfSPecificMonth(10,1));
	var todayUTC = dateToUTCDate(date);
	if (secondSundayOfMarch < todayUTC && todayUTC < firstSundayOfNovember)
	    return true;
	return false;    
}

function isDaylightSavingONfromCalendar(calendar) {
	var secondSundayOfMarch = dateToUTCDate(getDateOfAnInstanceOfSundayOfSPecificMonth(2,2));
	var firstSundayOfNovember = dateToUTCDate(getDateOfAnInstanceOfSundayOfSPecificMonth(10,1));
	var todayUTC = calendarToUTCDate(calendar);
	if (secondSundayOfMarch < todayUTC && todayUTC < firstSundayOfNovember)
	    return true;
	return false;    
}

function getATPAvailabilityMessage() {
	var params = request.httpParameterMap,
		productId = params.productId.stringValue,
		qty : Number = params.qty.intValue,
		postalCode = session.custom.customerZip,
		optionId = params.optionId.stringValue ? params.optionId.stringValue : 'none',
		requestedDate = new Date(),
		todaysDate = new Date(),
		products = new Array(),
		availabilityDate = null,
		availabilityClass = 'in-stock-msg',
		dateDifferenceString = '',
		isInStock = false,
		product = app.getModel('Product').get(productId),
		isATPSuccess = true;
	
	
	if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpAvailabilityMessage') && postalCode) {
		
		if (!session.custom.ATPAvailability) {
			session.custom.ATPAvailability = new Object();
		}
		
		if (!empty(Basket)) {
			var plis = Basket.productLineItems;
			for (var i = 0; i < plis.length; i++ ) {
				var pli = plis[i];
				if (productId == pli.productID) {
					qty = qty + pli.quantityValue;
				}
			}
		}
		
		products.push({
			'ProductId' : productId,
			'Quantity' : qty
		});
		
		if (optionId && optionId != 'none') {
			products.push({
				'ProductId' : optionId,
				'Quantity' : qty
			});
		}
		
		requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
		var requestPacket = '{"InventoryType": "Delivery","ZipCode": "' + postalCode + '","RequestedDate": "' + requestedDate + '","StoreId": "","Products": ' + JSON.stringify(products) + '}';
		var service = ATPInventoryServices.ATPInventoryMSOS;
		var result = service.call(requestPacket);
		if (result.error != 0 || result.errorMessage != null || result.mockResult) {
			isATPSuccess = false;
		} else {
			var jsonResponse = JSON.parse(result.object.text);
			for (var i = 0; i < jsonResponse.length; i++ ) {
				var response = jsonResponse[i];
				var deliveryDate = new Date(response.SlotDate);
				
				var storeOpenForDelivery : Boolean = true; // True for all other sites. Set accordingly for MattressFirm
				
				var queryString = 'custom.zip_code = {0}';
				var zipcodeObj = CustomObjectManager.queryCustomObject('ZipInfo', queryString, (session.custom.customerZip).toString());
				if(!empty(zipcodeObj) && !empty(zipcodeObj.custom) && 'timezone' in zipcodeObj.custom) {
					var timezone = zipcodeObj.custom.timezone;
				}
				var currentDateTimeCalendar_withTimezone : dw.util.Calendar = new dw.util.Calendar();
				if (!empty(timezone)) {
					/*Timezone is offset*/
					currentDateTimeCalendar_withTimezone.add(currentDateTimeCalendar_withTimezone.HOUR_OF_DAY, Number(timezone));
				} else {
					// Render template: availability.isml if timezone was not found
					availabilityDate = null;
					break;
				}
				/* EndTime for Stores = 02:00 pm. Instead of jsonResponse[i].EndTime as time is needed for processing */
				var ATPDateTimeCalendar  : dw.util.Calendar = new dw.util.Calendar(getDateFromDateAndTimeString(deliveryDate, '02:00 pm'));
				storeOpenForDelivery = ATPDateTimeCalendar.compareTo(currentDateTimeCalendar_withTimezone) < 0 ? false : true;
	
				if (Number(response.Slots) > 0 && response.Available.toLowerCase() === 'yes' && storeOpenForDelivery) {
					isInStock = true;
					availabilityDate = deliveryDate;
					
					
					var ATPDateTimeCalendarUTC = calendarToUTCDateWithoutTime(ATPDateTimeCalendar);
					var currentDateTimeCalendar_withTimezoneUTC = calendarToUTCDateWithoutTime(currentDateTimeCalendar_withTimezone);
					var dayDifferenceMF = Math.floor((ATPDateTimeCalendarUTC - currentDateTimeCalendar_withTimezoneUTC) / (1000 * 60 * 60 * 24));
					

					var todaysUTCDate = Date.UTC(todaysDate.getFullYear(), todaysDate.getMonth(), todaysDate.getDate());
					var availabilityUTCDate = Date.UTC(availabilityDate.getFullYear(), availabilityDate.getMonth(), availabilityDate.getDate());
					var dayDifference = Math.floor((availabilityUTCDate - todaysUTCDate) / (1000 * 60 * 60 * 24));
					
					
					dayDifference = dayDifferenceMF;
					
					if (dayDifference === 0) {
						dateDifferenceString = 'today,';
					} else if (dayDifference === 1) {
						dateDifferenceString = 'tomorrow,';
					} else {
						dateDifferenceString = '';
					}
					session.custom.ATPAvailability[productId + optionId + postalCode + qty] = new Object();
					session.custom.ATPAvailability[productId + optionId + postalCode + qty]['availabilityDate'] = availabilityDate;
					session.custom.ATPAvailability[productId + optionId + postalCode + qty]['dateDifferenceString'] = dateDifferenceString;
					session.custom.ATPAvailability[productId + optionId + postalCode + qty]['availabilityClass'] = availabilityClass;
					session.custom.ATPAvailability[productId + optionId + postalCode + qty]['qty'] = qty;
					
					break;
				}
			}
		}
	}
	if (!isInStock) {
		availabilityClass = 'not-available-msg';
	}
					
	app.getView({
		availabilityDate: availabilityDate,
		dateDifferenceString: dateDifferenceString,
		availabilityClass: availabilityClass,
		isInStock: isInStock,
		Product: product.object,
		IsATPSuccess: isATPSuccess
	}).render('product/components/atp-availability');
}


function getDefaultAvailabilityMessage() {
	var params = request.httpParameterMap,
	productId = params.productId.stringValue,
	product = app.getModel('Product').get(productId);
	
	app.getView({
		availabilityDate: null,
		dateDifferenceString: '',
		availabilityClass: 'not-available-msg',
		isInStock: false,
		Product: product.object	
	}).render('product/components/atp-availability');
}


function getATPAvailabilityMessageAjax() {
	var params = request.httpParameterMap,
		productId = params.productId.stringValue,
		qty : Number = params.qty.intValue,
		postalCode = session.custom.customerZip,
		optionId = params.optionId.stringValue ? params.optionId.stringValue : 'none',
		requestedDate = new Date(),
		todaysDate = new Date(),
		products = new Array(),
		availabilityDate = null,
		availabilityClass = 'in-stock-msg',
		dateDifferenceString = '',
		isInStock = false,
		product = app.getModel('Product').get(productId),
		isATPSuccess = true;
	
	
	if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpAvailabilityMessage') && postalCode) { //add check for core .. implementation should be same
		
		if (!session.custom.ATPAvailability) {
			session.custom.ATPAvailability = new Object();
		}
		
		if (!empty(Basket)) {
			var plis = Basket.productLineItems;
			for (var i = 0; i < plis.length; i++ ) {
				var pli = plis[i];
				if (productId == pli.productID) {
					qty = qty + pli.quantityValue;
				}
			}
		}
		
		products.push({
			'ProductId' : productId,
			'Quantity' : qty
		});
		
		if (optionId && optionId != 'none') {
			products.push({
				'ProductId' : optionId,
				'Quantity' : qty
			});
		}
		
		requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
		var requestPacket = '{"InventoryType": "Delivery","ZipCode": "' + postalCode + '","RequestedDate": "' + requestedDate + '","StoreId": "","Products": ' + JSON.stringify(products) + '}';
		var service = ATPInventoryServices.ATPInventoryMSOS;
		var result = service.call(requestPacket);
		if (result.error != 0 || result.errorMessage != null || result.mockResult) {
			isATPSuccess = false;
		} else {
			var jsonResponse = JSON.parse(result.object.text);
			for (var i = 0; i < jsonResponse.length; i++ ) {
				var response = jsonResponse[i];
				var deliveryDate = new Date(response.SlotDate);
				
				var storeOpenForDelivery : Boolean = true; // True for all other sites. Set accordingly for MattressFirm
				
				var queryString = 'custom.zip_code = {0}';
				var zipcodeObj = CustomObjectManager.queryCustomObject('ZipInfo', queryString, (session.custom.customerZip).toString());
				if(!empty(zipcodeObj)) {
					var timezone = zipcodeObj.custom.timezone;
				}
				var currentDateTimeCalendar_withTimezone : dw.util.Calendar = new dw.util.Calendar();
				if (!empty(timezone)) {
					/*Timezone is offset*/
					currentDateTimeCalendar_withTimezone.add(currentDateTimeCalendar_withTimezone.HOUR_OF_DAY, Number(timezone));
				} else {
					// Render template: availability.isml if timezone was not found
					availabilityDate = null;
					break;
				}
				
				/* EndTime for Stores = 02:00 pm. Instead of jsonResponse[i].EndTime as time is needed for processing */
				var ATPDateTimeCalendar  : dw.util.Calendar = new dw.util.Calendar(getDateFromDateAndTimeString(deliveryDate, '02:00 pm'));
                
				/* Daylight Saving Time */
				var DST : Number = 0;
				if(!empty(zipcodeObj) && !empty(zipcodeObj.custom) && 'dst' in zipcodeObj.custom) {
					DST = zipcodeObj.custom.dst;
				}
				if (DST != 0 &&	isDaylightSavingONfromCalendar(currentDateTimeCalendar_withTimezone)) {
					currentDateTimeCalendar_withTimezone.add(currentDateTimeCalendar_withTimezone.HOUR_OF_DAY, DST);
				}
                storeOpenForDelivery = ATPDateTimeCalendar.compareTo(currentDateTimeCalendar_withTimezone) < 0 ? false : true;	
				
				if (Number(response.Slots) > 0 && response.Available.toLowerCase() === 'yes' && storeOpenForDelivery) {
					isInStock = true;
					availabilityDate = deliveryDate;
					
					var ATPDateTimeCalendarUTC = calendarToUTCDateWithoutTime(ATPDateTimeCalendar);
					var currentDateTimeCalendar_withTimezoneUTC = calendarToUTCDateWithoutTime(currentDateTimeCalendar_withTimezone);
					var dayDifferenceMF = Math.floor((ATPDateTimeCalendarUTC - currentDateTimeCalendar_withTimezoneUTC) / (1000 * 60 * 60 * 24));

					var todaysUTCDate = Date.UTC(todaysDate.getFullYear(), todaysDate.getMonth(), todaysDate.getDate());
					var availabilityUTCDate = Date.UTC(availabilityDate.getFullYear(), availabilityDate.getMonth(), availabilityDate.getDate());
					var dayDifference = Math.floor((availabilityUTCDate - todaysUTCDate) / (1000 * 60 * 60 * 24));

					dayDifference = dayDifferenceMF;
					
					if (dayDifference === 0) {
						dateDifferenceString = 'today,';
					} else if (dayDifference === 1) {
						dateDifferenceString = 'tomorrow,';
					} else {
						dateDifferenceString = '';
					}
					session.custom.ATPAvailability[productId + optionId + postalCode + qty] = new Object();
					session.custom.ATPAvailability[productId + optionId + postalCode + qty]['availabilityDate'] = availabilityDate;
					session.custom.ATPAvailability[productId + optionId + postalCode + qty]['dateDifferenceString'] = dateDifferenceString;
					session.custom.ATPAvailability[productId + optionId + postalCode + qty]['availabilityClass'] = availabilityClass;
					session.custom.ATPAvailability[productId + optionId + postalCode + qty]['qty'] = qty;
					
					break;
				}
			}
		}
	}

	if (!isInStock) {
		availabilityClass = 'not-available-msg';
	}
	
	// Whether to show ATP delivery message or message from Custom Objects (AvailabilityMessage & AvailabilityMappingRules)
	var showATPMessage = false;
	if (!product.object.master 
			&& !product.object.variationGroup
			&& product.object.custom.shippingInformation.toLowerCase() == 'core' 
			&& dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpAvailabilityMessage') 
			&& session.custom.customerZip) {
		showATPMessage = true;
	}		

	var detailsContentId : String = "";
	if (availabilityDate != null && isInStock === true) {
		var productInventoryRecord = product.object.availabilityModel.inventoryRecord;
		var messageId = productInventoryRecord.custom.messageType;
		if (dw.content.ContentMgr.getContent("deliver-options-" + messageId) != null) {
			detailsContentId = "deliver-options-" + messageId;
		}
	}
	if (request.httpParameterMap.format.stringValue === 'ajax') {
        let r = require('app_storefront_controllers/cartridge/scripts/util/Response');
        r.renderJSON({
            success: isATPSuccess,
            availabilityDate: availabilityDate,
    		dayDifference: dayDifference,
    		dateDifferenceString: dateDifferenceString,
    		availabilityClass: availabilityClass,
    		isInStock: isInStock,
    		detailsContentId: detailsContentId,
    		showATPMessage: showATPMessage
        });
    }
}

/**
 * Only for MattressFirm site. Chicago DC Delivery for small, core, parcelable products only
 */
function getATPAvailabilityForChicagoDCAjax(productIDs) {
	var params = request.httpParameterMap,
		postalCode = "chicagoDCZipCode" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("chicagoDCZipCode") : '60446',
		responseJSONObject = {
	        	atpSuccess			: false,
	        	productsAvailable	: false,
	    		availabilityClass	: 'not-available-msg',
	    		availabilityMessage	: 'Out of Stock'
	        }
	var enableChicagoDCDelivery = "enableChicagoDCDelivery" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("enableChicagoDCDelivery") : false;
	if (enableChicagoDCDelivery && !empty(Basket)) {
		var parcelableProductIDs = '';
		parcelableProductIDs = dw.system.Site.getCurrent().getCustomPreferenceValue('parcelableProductIDs');
		
		var products = new Array();
		var deliveryDatesArr = new dw.util.ArrayList();
		
		var plis = Basket.productLineItems;
		for (var i = 0; i < plis.length; i++ ) {
			var pli = plis[i];
			if (!empty(pli.product.manufacturerSKU) &&  parcelableProductIDs.indexOf(pli.product.manufacturerSKU) > -1) {
				products.push({
					'ProductId'	: pli.productID,
					'Quantity'	: pli.quantityValue
				});
			}
		}
		try {
			if (products.length > 0) {
				var requestedDate = new Date();
				requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
				
				var requestAtpAgain = true,
					requestCount = 0;
				do {
					/* 
					 * We do not have to pass the Warehouse ID explicitly.
					 * ATP service will automatically get the products availability from the nearest warehouse on the basis of Zipcode
					 */
					var requestPacket = '{"InventoryType": "Delivery","ZipCode": "' + postalCode + '","RequestedDate": "' + requestedDate + '","StoreId": "","Products": ' + JSON.stringify(products) + '}';
					var service = ATPInventoryServices.ATPInventoryMonthMSOS;
					var result = service.call(requestPacket);
					Logger.error('ATP Service call::\nURL: '+service.getURL()+'\nRequest Packet: '+requestPacket);
					requestCount++;
					if (result.error != 0 || result.errorMessage != null || result.mockResult) {
						Logger.error('Service Response: '+result.errorMessage);
					} else {
						responseJSONObject.atpSuccess = true;
						Logger.error('Service Response: '+result.object.text);
						var jsonResponse = JSON.parse(result.object.text);
						var deliveryDateSession = new Object();
						
						var checkForDuplicates = new dw.util.ArrayList();
						for (var i = 0; i < jsonResponse.length; i++ ) {
							var response = jsonResponse[i];
							var deliveryDate = new Date(response.SlotDate);
							deliveryDate.setHours(10);
							
							var key = deliveryDate.toDateString().replace(' ', '', 'g');
							if (!deliveryDateSession[key]) {
								deliveryDateSession[key] = new Object();
								deliveryDateSession[key]['timeslots'] = new dw.util.ArrayList();
								deliveryDatesArr.add(deliveryDate);
							}
							
							var startTime = new Date(deliveryDate.toDateString() + ' ' + response.StartTime);
							var endTime = new Date(deliveryDate.toDateString() + ' ' + response.EndTime);
							if (checkForDuplicates.indexOf(key + startTime + endTime) == -1 && response.Available.toLowerCase() == 'yes' /*&& response.Slots > 0*/) {
								checkForDuplicates.add(key + startTime + endTime);
								deliveryDateSession[key]['timeslots'].add({
									'location1' : response.Location1,
									'location2' : response.location2,
									'mfiDSZoneLineId' : response.Zone,
									'mfiDSZipCode' : postalCode,
									'available' : response.Available.toLowerCase(),
									'startTime' : startTime,
									'endTime' : endTime,
									'slots' : Number(response.Slots)
								});
								
								var timeslotsComparator = new dw.util.PropertyComparator ('startTime', 'endTime');
								deliveryDateSession[key]['timeslots'].sort(timeslotsComparator);
								
								if (requestAtpAgain === true && response.Available.toLowerCase() == 'yes' && response.SlotDate !== null) {
									requestAtpAgain = false;
								}
							}
						}
						session.custom.deliveryDates = deliveryDateSession;
					}
					if (requestAtpAgain === true) {
						deliveryDatesArr = new dw.util.ArrayList();
						requestedDate = new Date(requestedDate);
						requestedDate.setDate(requestedDate.getDate() + 7);
						requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
					}
				} while (requestAtpAgain === true && requestCount < 2);
			}
			if (deliveryDatesArr.length > 0) {
				responseJSONObject.productsAvailable = true;
				responseJSONObject.availabilityClass = 'in-stock-msg';
				responseJSONObject.availabilityMessage = 'Arrives in 3-5 business days';
			}
			
		} catch (e) {
			var error = e.message;
		}
		if (request.httpParameterMap.format.stringValue === 'ajax') {
	        let r = require('app_storefront_controllers/cartridge/scripts/util/Response');
	        r.renderJSON(responseJSONObject);
	    } else {
	    	return responseJSONObject;
	    }
	}
}

/*
* Web exposed methods
*/
/** Starting point for the single shipping scenario.
 * @see module:controllers/COShipping~start */
exports.Start = guard.ensure(['get'], start);
exports.FindInStore = guard.ensure(['get'], findInStore);
exports.HandleForm = guard.ensure(['post'], handleForm);
exports.GetPreferredStore = getPreferredStore;
exports.SetPreferredStore = guard.ensure(['post'], setPreferredStore);
exports.SetPreferredStoreAB = guard.ensure(['post'], setPreferredStoreAB);
exports.GetPreferredStoreAddress = guard.ensure(['get'], getPreferredStoreAddress);
exports.UpdateBopisOption = guard.ensure(['post'], updateBopisOption);
exports.GetNearbyStores = guard.ensure(['get'], getNearbyStores);
exports.GetAvailablePickupStore = guard.ensure(['get'], getAvailablePickupStore);
exports.GetAvailablePickupStoreLocal = getAvailablePickupStore;
exports.EvaluateCartForBopis = evaluateCartForBopis;
exports.ChangeDeliveryToShipAddress = guard.ensure(['https', 'post'], changeDeliveryToShipAddress);
exports.GetATPDeliveryDates = getATPDeliveryDates;
exports.GetATPDeliveryDatesForNextWeek = guard.ensure(['get'], getATPDeliveryDatesForNextWeek);
exports.GetATPAvailabilityMessage = guard.ensure(['get'], getATPAvailabilityMessage);
exports.GetATPAvailabilityMessageAjax = guard.ensure(['get'], getATPAvailabilityMessageAjax);
exports.GetATPAvailabilityForChicagoDCAjax = guard.ensure(['get'], getATPAvailabilityForChicagoDCAjax);
exports.GetDefaultAvailabilityMessage = guard.ensure(['get'], getDefaultAvailabilityMessage);
exports.SetPreferredStoreFromGeoLocation = setPreferredStoreFromGeoLocation;
exports.GetDeliveryZone = getDeliveryZone;