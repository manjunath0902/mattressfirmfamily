'use strict';

/**
 * This controller implements the last step of the checkout. A successful handling
 * of billing address and payment method selection leads to this controller. It
 * provides the customer with a last overview of the basket prior to confirm the
 * final order creation.
 *
 * @module controllers/COSummary
 */

/* API Includes */
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');
var PriceAdjustment = require('dw/order/PriceAdjustment');
var Money = require('dw/value/Money');
var OrderMgr = require('dw/order/OrderMgr');

/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');

//Include Commerce Link Module
var CommerceLinkFactory = require('int_commercelink/cartridge/scripts/utils/CommerceLinkFactory');
var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;
var Cart = app.getModel('Cart');
var Email = app.getModel('Email');

/**
 * Renders the summary page prior to order creation.
 */
function start() {
    var cart = Cart.get();
    if(cart){
    // Checks whether all payment methods are still applicable. Recalculates all existing non-gift certificate payment
    // instrument totals according to redeemed gift certificates or additional discounts granted through coupon
    // redemptions on this page.
    var COBilling = require('~/cartridge/controllers/COBilling');
    if (!COBilling.ValidatePayment(cart)) {
        COBilling.Start();
        return;
    } else {

        Transaction.wrap(function () {
            cart.calculate();
        });

        Transaction.wrap(function () {
            if (!cart.calculatePaymentTransactionTotal()) {
                COBilling.Start();
            }
        });

        var pageMeta = require('app_storefront_controllers/cartridge/scripts/meta');
        pageMeta.update({pageTitle: Resource.msg('summary.meta.pagetitle', 'checkout', 'SiteGenesis Checkout')});
        app.getView({
            Basket: cart.object
        }).render('checkout/summary/summary');
    }
    } else {
        app.getController('Cart').Show();
        return;
    }
}

/**
 * This function is called when the "Place Order" action is triggered by the
 * customer.
 */
function submit() {
	//Convert Bundle Items into Product Line Items
	var cart = Cart.get();
	if(cart){
	//var isBundledOrder = createBundleLineItems(cart);
	
	// Calls the COPlaceOrder controller that does the place order action and any payment authorization.
    // COPlaceOrder returns a JSON object with an order_created key and a boolean value if the order was created successfully.
    // If the order creation failed, it returns a JSON object with an error key and a boolean value.
	var COPlaceOrder = require('~/cartridge/controllers/COPlaceOrder');
    var placeOrderResult = COPlaceOrder.Start();
    var order = placeOrderResult.Order;
    if (placeOrderResult.error) {
        cart = Cart.get();
        
        //Re-engineer what have done in createBundleLineItems(cart); for bundle items
        //var isReEngineered = revertBundleLineItems(cart);
        
        var COBilling = require('~/cartridge/controllers/COBilling');

        if (!COBilling.ValidatePayment(cart)) {
            COBilling.Start();
            return;
        } else {

            Transaction.wrap(function () {
                cart.calculate();
            });

            app.getView({
                Basket: cart.object,
                PlaceOrderError: placeOrderResult.PlaceOrderError
            }).render('checkout/billing/billing');
        }
    } else if (placeOrderResult.order_created) {
    	//Real time Order Creation
    	var clOrderCreationStatus = dw.system.HookMgr.callHook('commercelink.api.order.createpos', 'CreatePOSOrder', placeOrderResult.Order);
    	if(clOrderCreationStatus.error){
    		Transaction.wrap(function () {
                OrderMgr.failOrder(order);       		
            });
    		var redirectURL = dw.web.URLUtils.https('COBilling-Start','error','true');
        	response.redirect(redirectURL);
        	return;
    	}
        var createOrderResponseDetails = clOrderCreationStatus.getDetail(CommerceLinkFactory.STATUS_CODES.RESPONSE_OBJECT);
        
        if('enableStoreAssociatesEmail' in dw.system.Site.current.preferences.custom ? dw.system.Site.getCurrent().getCustomPreferenceValue('enableStoreAssociatesEmail') : false){
        	//Send order confirmation to store 
            var storeEmailID = session.custom.storeID;
            storeEmailID = storeEmailID + '@mfrm.com';
            var fullName =  order.billingAddress.fullName;
            Email.get('mail/orderconfirmation', storeEmailID)
            	.setSubject((Resource.msgf('order.orderconfirmation-email.001', 'order', null, order.getOrderNo() , fullName)).toString())
            	.send({
            		Order: order
        	});
        }
        
        showConfirmation(placeOrderResult.Order);
    }
    } else {
        app.getController('Cart').Show();
        return;
    }
}

/**
 * This function is called when the "Place Order" action is triggered by the
 * customer.
 */
function submitMobileOrder() {	
	var cart = Cart.get();
	if(cart){
      submit();
    } else {
        app.getController('Cart').Show();
        return;
    }
}

/**
 * Renders the order confirmation page after successful order
 * creation. If a nonregistered customer has checked out, the confirmation page
 * provides a "Create Account" form. This function handles the
 * account creation.
 */
function showConfirmation(order) {
    if (!customer.authenticated) {
        // Initializes the account creation form for guest checkouts by populating the first and last name with the
        // used billing address.
        var customerForm = app.getForm('profile.customer');
        customerForm.setValue('firstname', order.billingAddress.firstName);
        customerForm.setValue('lastname', order.billingAddress.lastName);
        customerForm.setValue('email', order.customerEmail);
    }

    app.getForm('profile.login.passwordconfirm').clear();
    app.getForm('profile.login.password').clear();

    var pageMetaData = request.pageMetaData;
    pageMetaData.title = Resource.msg('confirmation.meta.pagetitle', 'checkout', 'SiteGenesis Checkout Confirmation');
    app.getView({
        Order: order,
        ContinueURL: URLUtils.https('Account-RegistrationForm') // needed by registration form after anonymous checkouts
    }).render('checkout/confirmation/confirmation');
}

/*
 * Re-Create Bundle Products Line Items
 */
function createBundleLineItems(cart) {
    var order = cart.object;
    var bundledOrder = false;
    var shipmentsIterator = order.getShipments().iterator();
    var bundlePromotionsCollection = new Array();    
    Transaction.wrap(function () {
        try{
            while (shipmentsIterator.hasNext()) {
                shipIt = shipmentsIterator.next();
                var productLineItems = shipIt.getProductLineItems();
                //collect promotions from bundle item
                for each(var productLineItem in productLineItems) { 
                 if(productLineItem.product.bundle) {
                    if (!productLineItem.getPriceAdjustments().empty) {
                        var bundlePriceAdjustments = productLineItem.getPriceAdjustments().iterator();                        
                        while (bundlePriceAdjustments.hasNext()) {
                          var bundlePriceAdjustment = bundlePriceAdjustments.next();                        
                          var jsonData = new Object();
                          jsonData.bundleId = productLineItem.product.ID;
                          jsonData.promoId = bundlePriceAdjustment.promotionID;
                          jsonData.priceAdjustment = bundlePriceAdjustment.basePrice.value;
                          jsonData.taxClassInfo = productLineItem.taxClassID;
                          bundlePromotionsCollection.push(jsonData);                              
                        }                    
                    }
                 }
                }                
                for each(var productLineItem in productLineItems) {
                    //Convert Bundle LineItems into Product Line Items
                    if(productLineItem.product.bundle) {
                        var bundleItems = productLineItem.product.bundledProducts;
                        var singleBundleItem, createProductLineItem, product, quantity, price, optionModel = null;
                        for (var i=0; i<bundleItems.length; i++) {
                            singleBundleItem = bundleItems[i];
                            product = dw.catalog.ProductMgr.getProduct(singleBundleItem.ID);
                            optionModel = singleBundleItem.optionModel
                            //createProductLineItem = order.createProductLineItem(product, optionModel, shipIt);
                            quantity = productLineItem.getQuantityValue();
                            price = product.priceModel.price;                            
                            var AddProductToBasketResult = new dw.system.Pipelet('AddProductToBasket').execute({
                                Basket: order,
                                Product: product,
                                ProductOptionModel: optionModel,
                                Quantity: quantity,
                                Category: product.categorized ? product.categories[0] : null
                            });

                            if (AddProductToBasketResult.result === PIPELET_ERROR) {
                                return;
                            }
                            AddProductToBasketResult.ProductLineItem.setPriceValue(price.valueOrNull);
                            //Set Bundle Item Attributes                            
                            AddProductToBasketResult.ProductLineItem.custom.isBundledItem = true;
                            var mainBundleID = productLineItem.product.ID;                            
                            AddProductToBasketResult.ProductLineItem.custom.mainBundleID = mainBundleID;                            
                        }
                        order.removeProductLineItem(productLineItem);                        
                        bundledOrder = true;
                    }
                }
            }
            if(bundledOrder) {
            	cart.calculate();
            	if(bundlePromotionsCollection != null && bundlePromotionsCollection.length > 0) {
            	   applyBundlePromo(cart,bundlePromotionsCollection);
            	}else{
            		mattressPipeletHelper.splitShipmentsByType(cart);
            	}
            }
        } catch (e) {
             var error = e;       
        }
    });           
    return bundledOrder;
}

/*
 * Apply Bundle Promotions
 */
function applyBundlePromo(cart,bundlePromoCollection) {	
    	var bundlePromotionsCollection = bundlePromoCollection;
    	var order = cart.object;
    	var bundledOrder = false;
    	var shipmentsIterator = order.getShipments().iterator();    
    	Transaction.wrap(function () {
        try{
            while (shipmentsIterator.hasNext()) {
                shipIt = shipmentsIterator.next();
                var productLineItems = shipIt.getProductLineItems();                                
                for each(var item in  bundlePromotionsCollection) {
                    for each (var productLineItem in productLineItems) {
                         if(item.bundleId == productLineItem.custom.mainBundleID) {
                             var newAdjustment = productLineItem.createPriceAdjustment(item.promoId+"-BundlePromo");                              
                             newAdjustment.setTaxClassID(item.taxClassInfo);                             
                             var lineItemTax = new Money(0, empty(session.getCurrency().getCurrencyCode()) ? "USD" : session.getCurrency().getCurrencyCode());
                             newAdjustment.setTax(lineItemTax);
                             newAdjustment.updateTaxAmount(lineItemTax);                         
                             newAdjustment.setPriceValue(item.priceAdjustment);                                 
                             break;
                         }                            
                    }                 
                }
            }
            mattressPipeletHelper.splitShipmentsByType(cart);            
        } catch (e) {
             var error = e;             
        }
    });
}
/*
 * Re-Create Bundle Products Line Items
 */
function revertBundleLineItems(cart) {
    var order = cart.object;
    var shipmentsIterator = order.getShipments().iterator();
    var cartItemsHashMap = new dw.util.HashMap();
    
    Transaction.wrap(function () {
        try{
            while (shipmentsIterator.hasNext()) {
                shipIt = shipmentsIterator.next();
                var productLineItems = shipIt.getProductLineItems();
                var isBundleCreated = false;
                var createProductLineItem, product, quantity, price, optionModel = null;
                for each(var productLineItem in productLineItems) {                	
                	//Convert Product Line Items into Bundle Product
                    if(productLineItem.custom.isBundledItem) {    					
    					var bundleKey = productLineItem.custom.mainBundleID;
    					var bundleValue = productLineItem.getQuantityValue();
    					if (!cartItemsHashMap.containsKey(bundleKey)) {
    						cartItemsHashMap.put(bundleKey,bundleValue);
    					}                   	
                    	order.removeProductLineItem(productLineItem);
                    }                	
                }
                
            }
            
        	for each(var cartItem in cartItemsHashMap.entrySet().iterator()) {            	
            	var mainBundleId = cartItem.key;            	
                product = dw.catalog.ProductMgr.getProduct(mainBundleId);
                optionModel = product.getOptionModel();
                quantity = cartItem.value;                    
                price = product.priceModel.price;
                //createProductLineItem = order.createProductLineItem(product, optionModel, shipIt);
                var addProductToBasketResult = new dw.system.Pipelet('AddProductToBasket').execute({
                    Basket: order,
                    Product: product,
                    ProductOptionModel: optionModel,
                    Quantity: quantity,
                    Category: product.categorized ? product.categories[0] : null
                });
                if (addProductToBasketResult.result === PIPELET_ERROR) {
                    return false;
                }
                
                //set Quantity, Price and Category
                //createProductLineItem.setQuantityValue(quantityValue.valueOf());
                addProductToBasketResult.ProductLineItem.setPriceValue(price.valueOrNull);
                
                isBundleCreated = true;
            }
                
            //Calculate Again After Re-Creation of Bundle Items
            cart.calculate();
            
            return true;            
            
        } catch (e) {
           var error = e;
           return false             
        }
    }); 
}

/*
 * Module exports
 */

/*
 * Web exposed methods
 */
/** @see module:controllers/COSummary~Start */
exports.Start = guard.ensure(['https'], start);
/** @see module:controllers/COSummary~Submit */
exports.SubmitMobileOrder = guard.ensure(['https'], submitMobileOrder);
exports.Submit = guard.ensure(['https', 'post'], submit);

/*
 * Local method
 */
exports.ShowConfirmation = showConfirmation;
