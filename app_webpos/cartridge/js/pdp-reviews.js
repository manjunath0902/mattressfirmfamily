(function ($) {
	var pdpReviews = {
    tabs: $('#pdp-tabs'),
    stars: $('#pdp-reviews'),
    reviews: $('#BVRRContainer'),
    
		init: function () {
			pdpReviews.setupListeners();
		},

		setupListeners: function () {
	      pdpReviews.stars.click(function(e) {
					e.preventDefault();
					pdpReviews.openTab();
	        $('html, body').animate({	          
	          scrollTop: (pdpReviews.reviews.length < 1) ? ($("#BVRRContainer").offset().top - 125) : (pdpReviews.reviews.offset().top - 125)
	        }, 500);
	      });
		},

		openTab: function() {
			var all = $('.pdp-tabs li, .pdp-tabs-content li'),
				tab = $('.pdp-tabs li:nth-child(3)'),
				content = $('.pdp-tabs-content-reviews'),
				reviewcontent = $('.pdp-tab-content.expand-content.expand-mobile.pdp-reviews-content');

			if(tab && tab.text() && tab.text() == 'Reviews'){
				
				all.removeClass('is-active');

				tab.addClass('is-active');
				content.addClass('is-active').show();
				reviewcontent.show();
			}
		}
	};

	$(document).ready(function () {
		pdpReviews.init();
	});
})(jQuery);