(function ($) {
	var sliders = {

		init: function () {
			sliders.hero();
			sliders.product();
			sliders.saleproduct();
			sliders.sale4jmattressessize();
			sliders.sale4jbrands();
			sliders.whyinfocards();
		},

		hero: function () {
			$('.hero-slider').slick({
				dots: true,
				infinite: true,
				autoplay: true,
				autoplaySpeed: 8000,
				speed: 300,
				slidesToShow: 1
			});
		},

		product: function () {
			$('.product-slider').slick({
				infinite: false,
				dots: true,
				speed: 300,
				slidesToShow: 3,
				slidesToScroll: 3,
				autoplay: false,
				autoplaySpeed: 7000,
				responsive: [{
					breakpoint: 768,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						arrows: false
					}
				}]
			});
		},
		
		saleproduct: function () {
			$('.saleproduct-slider').slick({
				infinite: false,
				dots: true,
				speed: 300,
				slidesToShow: 3,
				slidesToScroll: 3,
				autoplay: false,
				autoplaySpeed: 7000,
				centerMode: false,
				responsive: [
					{
				      breakpoint: 1199,
				      settings: {
				    	slidesToShow: 2,
						slidesToScroll: 2,
						arrows: true,
						centerMode: false,
						centerPadding: '25px'
				      }
				    },
					{
					breakpoint: 768,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						arrows: false,
						centerMode: false,
						}
					}
					]
			});
		},
		sale4jmattressessize: function () {
			if($(window).width() < 768){
				$('.sale-4j-mattresses-size').slick({
					infinite: false,
					dots: true,
					speed: 300,
					slidesToShow: 3,
					slidesToScroll: 3,
					autoplay: false,
					autoplaySpeed: 7000,
					centerMode: true,
					centerPadding: '150px',
					responsive: [{
						breakpoint: 768,
						settings: {
							dots: true,
							slidesToShow: 1,
							slidesToScroll: 1,
							arrows: false,
							centerMode: true,
							centerPadding: '70px'
						}
					}]
				});
				//$('.sale-4j-mattresses-size').slick('slickGoTo', 0);
			}			
		},
		sale4jbrands: function () {
			if($(window).width() < 768){
				$('.salebrands-list').slick({
					infinite: false,
					dots: true,
					speed: 300,
					slidesToShow: 3,
					slidesToScroll: 3,
					autoplay: false,
					autoplaySpeed: 7000,
					centerMode: true,
					centerPadding: '150px',
					variableWidth: true,
					responsive: [{
						breakpoint: 768,
						settings: {
							dots: true,
							slidesToShow: 1,
							slidesToScroll: 1,
							arrows: false,
							centerMode: true,
							centerPadding: '70px'
						}
					}]
				});
				//$('.salebrands-list').slick('slickGoTo', 0);
			}			
		},
		whyinfocards: function () {
			if($(window).width() < 768){
				$('.pdp-info-cards').slick({
					infinite: false,
					dots: true,
					speed: 300,
					slidesToShow: 3,
					slidesToScroll: 3,
					slide: '.info-card',
					autoplay: false,
					autoplaySpeed: 7000,
					centerMode: true,
					centerPadding: '150px',
					responsive: [{
						breakpoint: 768,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
							arrows: false,
							centerMode: false,
							centerPadding: '25px',
						}
					}]
				});
				//$('.pdp-info-cards').slick('slickGoTo', 0);
			}
		}
	};

	$(document).ready(function () {
		sliders.init();
	});
})(jQuery);
