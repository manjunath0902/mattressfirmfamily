/**
 *    (c) 2009-2014 Demandware Inc.
 *    Subject to standard usage terms and conditions
 *    For all details and documentation:
 *    https://bitbucket.com/demandware/sitegenesis
 */

'use strict';

var countries = require("./countries"),
	dialog = require("./dialog"),
	expand = require("./expand"),
	flipclock = require("./flipclock"),
	headerMenu = require("./header-menu"),
	pdpTabs = require("./pdp-tabs"),
	deliveryCountdown = require("./delivery-countdown"),
	countdown = require("./countdown"),
	pdpReveiws = require("./pdp-reviews"),
	sliders = require("./sliders"),
	smoothscroll = require("./smoothscroll"),
	minicart = require("./minicart"),
	mobileMenu = require("./mobile-menu"),
	headerBanner = require("./header-banner"),
	stickyNav = require("./sticky-nav"),
	page = require("./page"),
	rating = require("./rating"),
	searchplaceholder = require("./searchplaceholder"),
	searchsuggest = require("./searchsuggest"),
	searchsuggestbeta = require("./searchsuggest-beta"),
	tooltip = require("./tooltip"),
	scrollTop = require("./scroll-top"),
	hoverIntent = require("./hoverintent"),
	util = require("./util"),
	validator = require("./validator"),
	googleAnalytics = require("./google-analytics"),
	content = require("./content"),
	responsiveSlots = require("./responsiveslots/responsiveSlots"),
	amplience = require("./amplience"),
	zipzone = require("./zipzone"),
	sfemaildialog = require("./sfemaildialog"),
	mobilegeolocator = require("./mobilegeolocator"),
	exacttargetnew = require("./exacttargetnew"),
	progress = require("./progress"),
	addresslookup = require("./addresslookup"),
	addToCart = require("./pages/product/addToCart"),
	tealium = require("./tealium"),
	userCurrentGeoLocation = require("./userCurrentGeoLocation"),
	browserGeoLocation = require("./browserGeoLocation");

// if jQuery has not been loaded, load from google cdn
if (!window.jQuery) {
	var s = document.createElement('script');
	s.setAttribute('src', 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js');
	s.setAttribute('type', 'text/javascript');
	document.getElementsByTagName('head')[0].appendChild(s);
}

require('./jquery-ext')();
require('./cookieprivacy')();
//require('./captcha')();

function setRenderedStoresSlickHeight() {
	var max = -1;
	$('#try-in-store-results li').each(function() {
		var h =$(this).prop('scrollHeight') - $(this).position().top;
		max = h > max ? h : max;
	});
	$('#try-in-store-results li').each(function() {
		$(this).css('height', max);
	});
}

function initializeEvents() {
	$('.fc-products__product').on('click', '.add-to-cart', addToCart.addToCart);
	var controlKeys = ['8', '13', '46', '45', '36', '35', '38', '37', '40', '39'];
	$('body')
		.on('keydown', 'textarea[data-character-limit]', function (e) {
			var text = $.trim($(this).val()),
			charsLimit = $(this).data('character-limit'),
			charsUsed = text.length;

			if ((charsUsed >= charsLimit) && (controlKeys.indexOf(e.which.toString()) < 0)) {
				e.preventDefault();
			}
		}).on('change keyup mouseup', 'textarea[data-character-limit]', function () {
			var text = $.trim($(this).val()),
				charsLimit = $(this).data('character-limit'),
				charsUsed = text.length,
				charsRemain = charsLimit - charsUsed;

			if (charsRemain < 0) {
				$(this).val(text.slice(0, charsRemain));
				charsRemain = 0;
			}

			$(this).next('div.char-count').find('.char-remain-count').html(charsRemain);
		});

	/**
	 * initialize search suggestions, pending the value of the site preference(enhancedSearchSuggestions)
	 * this will either init the legacy(false) or the beta versions(true) of the the search suggest feature.
	 * */
	var $searchContainer = $('.header-search');

	// add support for new HP home search and menu search forms
	//if (parseInt($(window).width()) < 767) {
		//$searchContainer = $('.header-mobile-search.home, .header-search.home');
	//}
	if (SitePreferences.LISTING_SEARCHSUGGEST_LEGACY) {
		searchsuggest.init($searchContainer, Resources.SIMPLE_SEARCH);
	} else {
		searchsuggestbeta.init($searchContainer, Resources.SIMPLE_SEARCH);
	}

	$('.secondary-navigation .toggle').click(function () {
		$(this).toggleClass('expanded').next('ul').toggle();
	});
	// add generic toggle functionality
	$('.toggle').next('.toggle-content').hide();
	$('.toggle').click(function () {
		$(this).toggleClass('expanded').next('.toggle-content').toggle();
	});

	// Refinements Truncate functionality
	$(".hideRefinement").hide();
	$(".refinements-see-less").hide();
	$(".refinements-see-more").on("click", function() {
		$(this).parent().find(".hideRefinement").show();
		$(this).parent().find(".refinements-see-less").show();
		$(this).hide();
	});
	$(".refinements-see-less").on("click", function() {
		$(this).parent().find(".hideRefinement").hide();
		$(this).parent().find(".refinements-see-more").show();
		$(this).hide();

	});

	if($('.pdp-info-cards').length > 0) {
		$( ".pdp-info-cards .info-card .image-holder img.white-icon").css("display" , "none");
		$( ".pdp-info-cards .info-card" ).hover(function() {
			$(this).find(".charcoal-icon").css("display" , "none");
			$(this).find(".white-icon").css({"display": "block", "margin": "0 auto"});
		},function(){
			$(this).find(".charcoal-icon").css({"display": "block", "margin": "0 auto"});
			$(this).find(".white-icon").css("display" , "none");
		});
	}
	var $deliveryDates = $('#delivery-dates-container');
	var deliveryConfig = function () {
		//Select Delivery Date Checking
		if ($deliveryDates.find("input[type='radio']").length > 0)	{
			if (!$deliveryDates.find("input[type='radio']").is(":checked")) {
				$('form#dwfrm_singleshipping_shippingAddress').find('button').addClass('disabled');
			} else {
				if ($deliveryDates.find("input[value='scheduledelivery']").is(":checked")) {
					if ($('.delivery-dates-wrapper').find('.time-slot').hasClass('selected')) {
						$('form#dwfrm_singleshipping_shippingAddress').find('button').removeClass('disabled');
						$deliveryDates.find('.error').hide();
					} else {
						$('form#dwfrm_singleshipping_shippingAddress').find('button').addClass('disabled');
					}
				} else {
					$('form#dwfrm_singleshipping_shippingAddress').find('button').removeClass('disabled');
					$deliveryDates.find('.error').hide();
				}
			}
		}
	}
	deliveryConfig();
	$deliveryDates.find("input[type='radio']").change(function () {
		deliveryConfig();
	});
	$('.delivery-dates').on('click focusin', '.time-slot', function () {
		setTimeout(function () {
			deliveryConfig();
		}, 500);
	});
	$('form#dwfrm_singleshipping_shippingAddress').submit(function () {
		if ($(this).find('button').hasClass('disabled'))
			$deliveryDates.find('.error').show();
	});
	$('form#dwfrm_singleshipping_shippingAddress button').click(function (e) {
		if ($(this).hasClass('disabled')) {
			e.preventDefault();
			$deliveryDates.find('.error').show();
		}
	});
	// subscribe email box
	var $subscribeEmail = $('.subscribe-email');
	if ($subscribeEmail.length > 0)	{
		$subscribeEmail.focus(function () {
			var val = $(this.val());
			if (val.length > 0 && val !== Resources.SUBSCRIBE_EMAIL_DEFAULT) {
				return; // do not animate when contains non-default value
			}

			$(this).animate({color: '#999999'}, 500, 'linear', function () {
				$(this).val('').css('color', '#333333');
			});
		}).blur(function () {
			var val = $.trim($(this.val()));
			if (val.length > 0) {
				return; // do not animate when contains value
			}
			$(this).val(Resources.SUBSCRIBE_EMAIL_DEFAULT)
				.css('color', '#999999')
				.animate({color: '#333333'}, 500, 'linear');
		});
	}
	$('#zipfield').on('focus', function () {
		$(this).select();
		this.setSelectionRange(0,5);
	});
	$('#main').on('focus', '#pdp-delivery-zip', function () {
		$(this).select();
		this.setSelectionRange(0,5);
		setTimeout(function () {
			$('#pdp-delivery-zip').get(0).setSelectionRange(0, 5);
		}, 1);
	});

	$('#main').on('click', '.details', function (e) {
		e.preventDefault();
		var params = {
				dClass: 'deliver-option-class',
				dUrl: $(e.target).attr('href')
			};
		dialogify(params);
	});
	$('.privacylink').on('click', function (e) {
		e.preventDefault();
		var params = {
				dUrl: $(e.target).attr('href'),
				dheight: 600,
				dTitle: $(e.target).attr('title')
			};
		dialogify(params);
	});
	$('.privacy-policy').on('click', function (e) {
		e.preventDefault();
		var params = {
				dUrl: $(e.target).attr('href'),
				dheight: 600,
				dTitle: $(e.target).attr('title'),
				dClass: 'privacy-policy-class'
		};
		dialogify(params);
	});
	$('.dialogify').on('click', function (e) {
		e.preventDefault();
		var params = {
						dUrl: $(e.target).attr('href'),
						dClass: $(e.target).attr('data-class'),
						dheight: $(e.target).attr('data-height'),
						dTitle: $(e.target).attr('title')
					};
		dialogify(params);
	});
	var dialogify = function (params) {
		dialog.open({
			url: params.dUrl,
			options: {
				dialogClass: params.dClass,
				height: params.dheight,
				title: params.dTitle,
				open: function () {
					var dialog = this;
					$('.ui-widget-overlay').on('click', function () {
						$(dialog).dialog('close');
					});
					if (params.dHideTitle) {
						$(".ui-dialog-title").hide();
						$(".ui-dialog-titlebar").css('height', 'auto');
					}
				}
			}
		});
	};

	// DROPDOWN FUNCTIONALITY
	if (parseInt($(window).width()) < 768) {
		$('.dropdown-trigger').click(function () {
			$(this).next('.dropdown-list').slideToggle(150);
			$(this).toggleClass('opened');
		});

		$('.header-top-nav li.header-tooltip').click(function () {
			if ($(this).find('span.arrow').length > 0) {
				$(this).find('span.arrow').remove();
			} else {
				$(this).prepend('<span class="arrow"></span>');
			}
			$(this).find('ul').slideToggle(200);
		});
	}
	// END DROPDOWN FUNCTIONALITY

	// Top Header Promo Bar

	
  var promoContainer = $('.promo-container');
  // if container has fading class, fade promo elements
  if (promoContainer.hasClass('fading')) {
    function initPromoBanner() {
      var items = $('.promo-container ul li');

      // basic function to set next element active, or set first element as active if next doesn't exist
      var timer = function() {
        var first = $('.promo-container ul li:first-child');
        var current = $('.promo-container ul li.active');
        var next = current.next();
        current.removeClass('active');
        if (next.length === 0) {
          first.addClass('active');
        } else {
          next.addClass('active');
        }
      }

      var setTimer = null;
      // run initially
      setTimer = setInterval(timer, 5000);

      items.hover(function() {
        // pause promobar timer
        clearInterval(setTimer);
      },function() {
        // resume promobar timer
        setTimer = setInterval(timer, 5000);
      });
    }

    // init function on page load
    initPromoBanner();
  }

	// Footer chat button
	$('#contentChatWithUs').on('click', function (e) {
		//$('.header-chat a[id^="liveagent_button_online"]').trigger('click');
		/* Trigering New Chat from Footer Link*/
		e.preventDefault();
		if($('#helpButtonSpan').length > 0) {
			$('#helpButtonSpan').click();
		}
	});

	// main menu toggle
	$('.menu-toggle').on('click', function () {
		$('#wrapper').toggleClass('menu-active');
		$('footer').toggleClass('footer-active');
	});
	$('.menu-category li .menu-item-toggle').on('click touchstart', function (e) {
		if (util.isMobileSize()) {
			var $parentLi = $(e.target).closest('li');
			e.preventDefault();
			$parentLi.find('.menu-item-toggle i').toggleClass('fa-angle-right fa-angle-down active');
			$parentLi.find('.level-2').toggle();
			if ($parentLi.hasClass('active')) {
				if ($(e.target).hasClass('arrow')) {
					$parentLi.toggleClass('active');
					return false;
				}
				window.location.href = $parentLi.find('a').attr('href');
			} else {
				$parentLi.siblings('li').removeClass('active').find('.menu-item-toggle i').removeClass('fa-angle-down active').addClass('fa-angle-right');
				$parentLi.siblings('li').find('li.active').removeClass('active').find('.menu-item-toggle i').removeClass('fa-angle-down active').addClass('fa-angle-right');
				$parentLi.toggleClass('active');
				$parentLi.siblings('li').find('.level-2').hide();
				$(e.target).find('i').toggleClass('fa-angle-right fa-angle-down active');
			}
		}
	});
    //top navigation pause
    var hoverconfig = {
        over: function () {
            $(this).find('div.level-2').css('display', 'block');
            $(this).children('a.has-sub-menu').addClass('open');
            $(this).addClass('open');
			var divwidth = $(this).find('div.level-2').width();
			var offset = $(this).find('div.level-2').offset();
			var positionleft = offset.left;
			var screenwidth = $(window).width();
			$('#grid-sort-header').blur();
			$('#grid-paging-header').blur();
			var positionright = screenwidth - (positionleft + divwidth);
			if (screenwidth > 767 && screenwidth < 1025) {
				if (positionright < 50) {
					$(this).find('div.level-2').css('right', 0);
				}
				return false;
			}
        },
        out: function () {
            $(this).find('div.level-2').css('display', 'none');
            $(this).children('a.has-sub-menu').removeClass('open');
        },
        timeout: 100,
        sensitivity: 3,
        interval: 100
    };

	var touchConfig = function () {
		$(document).off('touchend', '.menu-category > li > a');
		// fix for touch tablet devices: prevent first click
		if (util.isMobile() == true && screen.width > util.mobileWidth) {
			$(document).on('touchend', '.menu-category > li > a', function (e) {
				if ($(this).siblings('.level-2').is(':hidden') == true) {
					e.preventDefault();
					// hide all opened menus
					$('.menu-category > li').find('.level-2').css('display', 'none');
					$('.menu-category > li').children('a.has-sub-menu').removeClass('open');
					//open current submenu
					$(this).siblings('.level-2').css('display', 'block');
					$(this).addClass('open');
					var divwidth = $(this).siblings('.level-2').width();
					var offset = $(this).siblings('.level-2').offset();
					var positionleft = offset.left;
					var screenwidth = screen.width;
					var positionright = screenwidth - (positionleft + divwidth);
					if (screenwidth > 767 && screenwidth < 1025) {
						if (positionright < 50) {
							$(this).siblings('.level-2').css('right', 0);
						}
						return false;
					}
					//console.log('ddd' + positionright + 'ddd' + screenwidth);
				}
			})
		}
	};

	touchConfig();

    if (screen.width > util.mobileWidth) {
        $('.level-1 li').hoverIntent(hoverconfig);
    }
    $(window).resize(function () {
        if (screen.width > util.mobileWidth) {
            $('.level-1 li').removeClass('active').hoverIntent(hoverconfig);
        } else {
            $('.level-1 li').unbind("mouseenter").unbind("mouseleave");
            $('.level-1 li').removeProp('hoverIntentT');
            $('.level-1 li').removeProp('hoverIntentS');
		}
        setRenderedStoresSlickHeight();
		touchConfig();
	});
    $('.print-page').on('click', function () {
		window.print();
		return false;
	});
    function validatesignupEmail(email) {
		var $continue = $('.home-email');
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		if (!emailReg.test(email)) {
			//$continue.attr('disabled', 'disabled');
			if ($(".emailsignup #email-alert-address-error").length < 1) {
				if ($(".emailsignup .error2").length > 0) {
					$(".emailsignup .error2").remove();
				}
				//$(".emailsignup form").append("<span id='email-alert-address-error' class='error'>" + Resources.VALIDATE_EMAIL + "</span>");
				$(".emailsignup form").find("#email-alert-address").val("");
				$(".emailsignup form").find("#email-alert-address").attr("placeholder", Resources.VALIDATE_EMAIL);
			} else {
				if ($(".emailsignup .error2").length > 0) {
					$(".emailsignup .error2").remove();
				}
				if ($(".emailsignup #email-alert-address-error").text().length == 0) {
					$(".emailsignup #email-alert-address-error").html(Resources.VALIDATE_EMAIL);
					//$(".emailsignup form").append("<span id='email-alert-address-error' class='error2'>" + Resources.VALIDATE_EMAIL + "</span>");
					$(".emailsignup form").find("#email-alert-address").val("");
					$(".emailsignup form").find("#email-alert-address").attr("placeholder", Resources.VALIDATE_EMAIL);
				} else {
					//console.log('22' + $(".emailsignup #email-alert-address-error").length);
				}
			}
			$("#email-alert-address-error").css('display','block');
			return false;
		} else {
			//$continue.removeAttr('disabled');
			if ($(".emailsignup #email-alert-address-errorr").length > 0) {
				$(".emailsignup form #email-alert-address-error").remove();
			}
			if ($(".emailsignup .error2").length > 0) {
				$(".emailsignup .error2").remove();
			}
			return true;
		}
	}

	/*
	* Extend email signup validation
	*/
	$('#email-alert-signup').validate({
		focusInvalid: false,
		errorPlacement: function(error, element) {
			element.attr("placeholder", "Please enter your email address");
		}
	});

	var emailSignUpFlag = false;
	/*email signup Ajax*/
	$('#email-alert-signup').on('submit', function (e) {
		e.preventDefault();
		var $this = $(this),
			emailSignupInput = $(this).find('#email-alert-address'),
			validationErrorBlock = $('#email-alert-address-error');
		var signupInputValue = $(this).find(emailSignupInput).val();
		var validatetest = validatesignupEmail(signupInputValue);
		if (validatetest && signupInputValue.length > 1 && !emailSignUpFlag) {
			emailSignUpFlag = true;
			var _zipCode = $('#footer-social-zipCode').val();
			var _siteId = $('#footer-social-siteId').val();
			var _leadSource = $('#footer-social-leadSource').val();
			var _optOutFlag = $('#footer-social-optOutFlag').val();

			if ($.cookie) {
				var _gclid = $.cookie('_ga');
				console.log(_gclid);
			}
			var params = {emailAddress: signupInputValue, zipCode: _zipCode, leadSource: _leadSource, siteId: _siteId, optOutFlag: _optOutFlag, gclid: _gclid};

			$.ajax({
				url: Urls.sfEmailSubscription,
				data: params,
				type: 'post',
				success: function (data) {
					$this.hide().parent().append($(data));
				},
				error: function (errorThrown) {
					console.log(errorThrown);
				}
			});
		}
	});
	// Execute a function when the user releases a enter key on the keyboard
	$(document).on("keyup",'#signUpPDP', function(event) {
	  // Number 13 is the "Enter" key on the keyboard
	  if (event.keyCode === 13) {
	    // Cancel the default action, if needed
	    event.preventDefault();
	    // Trigger the button element with a click
	    $(this).click();
	  }
	});
	//MAT-1594 BTS Promo Customer can enter email address on PDP (Promo Live 7/17/2019)
	$(document).on('click','#signUpPDP', function (e) {
		e.preventDefault();
		var $this = $(this),
			validationErrorBlock = $('#email-alert-address-error');
		var signupInputValue = $('#pdp-email-alert-address').val();
		var validatetest = validatesignupEmail(signupInputValue);
		if (validatetest && signupInputValue.length > 1) {
			emailSignUpFlag = true;
			var _zipCode = $('#footer-social-zipCode').val();
			var _siteId = $('#footer-social-siteId').val();
			var _leadSource = $('#bts-pdp-promo').val();
			var _optOutFlag = $('#footer-social-optOutFlag').val();
			var pid = $('#pid').val();
			
			if ($.cookie) {
				var _gclid = $.cookie('_ga');
				console.log(_gclid);
			}
			var params = {emailAddress: signupInputValue, zipCode: _zipCode, leadSource: _leadSource, siteId: _siteId, optOutFlag: _optOutFlag, gclid: _gclid, pid: pid};

			$.ajax({
				url: Urls.sfEmailSubscription,
				data: params,
				type: 'post',
				success: function (data) {
					window.location.reload();
				},
				error: function (errorThrown) {
					if($('#pdp-email-alert-address').next('span.error').length) {
						$('#pdp-email-alert-address').next().show().html("Email already exist")
					} else {
						$('#pdp-email-alert-address').after("<span id='pdp-email-alert-exist' class='error' style='display:block;'>Email already exists</span>" );
					}
				}
			});
		} else {
			//highlighting email input field and adding error class
			$('#pdp-email-alert-address').addClass('error');
			if($('#pdp-email-alert-address').next('span.error').length) {
				$('#pdp-email-alert-address').next().show().html("Please enter a valid email address.")
			} else {
				$('#pdp-email-alert-address').after("<span id='pdp-email-alert-exist' class='error' style='display:block;'>Please enter a valid email address.</span>" );
			}
		}
	});
	
	$(document).on('input propertychange','#pdp-email-alert-address', function() {
		$('#pdp-email-alert-exist').remove();
		if (/^[\w.%+-]+@[\w.-]+\.[\w]{2,6}$/.test($('#pdp-email-alert-address').val())) {
			$('#signUpPDP').removeClass('btn-disabled');
			$('#pdp-email-alert-address').removeClass('error');
			if($('#pdp-email-alert-address').next('span.error').length) {
				$('#pdp-email-alert-address').next().hide();
			}
		} else {
			$('#signUpPDP').addClass('btn-disabled');
		}
	});
	/* Chat link click tracking */
	$("a[id^='liveagent_button_']").each(function () {
		$(this).on('click', function (e) {
			utag.link({event_name: "chat_link"});
		});
	});
	$('.primary-logo').focus();

	/**
	 *  Buy online pick up in store (BOPIS)  and Try in Store(TIS) functionality
	 */

	// BOPIS / Try In Store init zip form
	/* $('.delivery-options-container').on('click', '#instorepickup', function (e) {
		e.preventDefault();
		if ($(this).closest('label').children(".details-container").length == 0) {
			$('.delivery-options-container .store-picker > a').click();
		}
	}); */
	function initStorePicker(e) {
		// tealium.trigger("Try-in-store", "Initiate try in store", "", $(e.toElement).attr('data-tealium'));
		dialog.open({
		url: $(e.target).attr('href'),
			options:{
				open: function () {
					var $storePickerForm = $('#dialog-container').find('form');
					var $spSubmit = $($storePickerForm.find('[name="submit"]'));
					var $spZipCodeInput = $($storePickerForm.find('[name="dwfrm_storepicker_postalCode"]'));
					$spZipCodeInput.select();
					$($spSubmit).on('click', function (e) {
						e.preventDefault();
						progress.show($('#dialog-container'));
						getStores($storePickerForm);
					});
				},
				width: '820',
				title:	$(e.target).attr('title'),
				draggable: false
			}
		});
	}
	// Triggers
	$('#main').on('click', '.store-picker a:not(".get-directions")', function (e) {
		e.preventDefault();
		initStorePicker(e);
	});
	// Helper functions
	function setPreferredStore(storeId) {
		User.storeId = storeId;
		$.ajax({
			url: Urls.setPreferredStore2,
			type: 'POST',
			data: {storeId: storeId}
		});
	}
	
	// Helper functions
	function setPreferredStoreAB(storeId) {
		User.storeId = storeId;
		$.ajax({
			url: Urls.setPreferredStoreAB,
			type: 'POST',
			data: {storeId: storeId},
			success: function (html) {
				window.location.reload();
			}
		});
	}
	
	function getStores(storePickerForm) {
		// tealium.trigger("Try-in-store", "Enter zip code", "", storePickerForm.find('input[id*=postalCode]').val());
		$.ajax({
			type: 'POST',
			url: storePickerForm.attr("action"),
			data: storePickerForm.serialize(),
			dataType: 'html',
			success: function (html) {
				renderStores(html);
			}.bind(this),
			failure: function () {
				window.alert(Resources.SERVER_ERROR);
			}
		});
	}
	function renderStores(html) {
		// replace dialog with updated content
		$('#dialog-container').html(html);
		// init all our variables after content is replaced
		var $dialog				= $('#dialog-container');
		var $changeLink			= $dialog.find('#change-location');
		var $storeZipForm		= $dialog.find('form#StorePickerZip');
		var $preferredStoreForm	= $dialog.find('#SetPreferredStore');
		var $psSubmit			= $preferredStoreForm.find('button[type="submit"]');
		var $spSubmit			= $storeZipForm.find('[name="submit"]');
		var $nextSlick			= $dialog.find('.slick-next');

		$storeZipForm.hide();
		// tealium.init();
		// disable selectability of unavailable stores
		$('.store-result.unavailable').find('input[type="radio"]').on('click', function () {
			return false;
		});
		// if no preferred store is picked, disable the continue button
		if ($preferredStoreForm.find('.preferred-store').length == 0) {
			$psSubmit.attr('disabled', true);
		}
		$('ul.try-in-store-storelist').slick({
            infinite: false,
            slidesToScroll: 1,
            slidesToShow: 4,
            accessibility: false,
            draggable: false,
            responsive: [{
				breakpoint: 1024,
				settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: false,
                    accessibility: false,
                    draggable: false,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: false,
                    accessibility: false,
                    draggable: false,
                }
            },
            {
				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
                    infinite: false,
                    accessibility: false,
                    draggable: false,
				}
            },
            {
                breakpoint: 320,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
                    infinite: false,
                    accessibility: false,
                    draggable: false,

				}
            }]
        });
		// return to zip form when change location link is clicked
		$changeLink.on('click', function (e) {
			e.preventDefault();
			$preferredStoreForm.hide();
			$storeZipForm.show();
			$("form#StorePickerZip :input:visible:enabled:first").focus();
			$("form#StorePickerZip :input:visible:enabled:first").select();
			$($spSubmit).on('click', function (e) {
				e.preventDefault();
				progress.show($dialog);
				getStores($storeZipForm);
			});
		});
		var requestAgain = false;
		$('ul.try-in-store-storelist').on('click','.slick-next', function (e) {
			requestAgain = true;
			if (requestAgain === true) {
				var stores = $('#total-stores').val();
				var slickLength = $('ul.try-in-store-storelist').find('.slick-slide').length;
				if (slickLength < stores) {
					var storesArr = [];
					$('ul.try-in-store-storelist label').each(function() {
						var $li = $(this);
						storesArr.push($li.attr("for"));
					});
					var params = {
						requestAgain: requestAgain
		            };
					var jsonObj = new Object();
					jsonObj.stores = storesArr;
					$.ajax({
						url: util.appendParamsToUrl(Urls.getMoreStoresForBopis, params),
						type: 'POST',
						data: JSON.stringify(jsonObj),
						success:  function (response) {
							$(response).find('ul.try-in-store-storelist li').each(function() {
								$('ul.try-in-store-storelist').slick('slickAdd', $(this))
							});
							setRenderedStoresSlickHeight();
						}.bind(this),
						async: false
					});
				}
			}
		});

		// update classes for styling when stores are selected
		$($preferredStoreForm.find('.select-preferred-store input')).change(function () {
			// tealium.trigger("Try-in-store", "Select Preferred Store", "", $(this).attr('data-tealium'));
			var $buttonContainer = $(this).closest('div');
			var $buttonText = $buttonContainer.find('.select-preferred-store-text');
			var $storeContainer = $(this).closest('li');
			setPreferredStore(this.value);
			// reset other store containers
			$preferredStoreForm.find('.select-preferred-store-text').text(Resources.SELECT_STORE);
			$preferredStoreForm.find('li').removeClass('preferred-store');
			// update the current
			$buttonText.text(Resources.PREFERRED_STORE);
			$storeContainer.addClass('preferred-store');
			$psSubmit.removeAttr('disabled');
		});
		
		// update classes for styling when stores are selected
		$($preferredStoreForm.find('.select-preferred-store-ab input')).change(function () {
			// tealium.trigger("Try-in-store", "Select Preferred Store", "", $(this).attr('data-tealium'));
			var $buttonContainer = $(this).closest('div');
			var $buttonText = $buttonContainer.find('.select-preferred-store-text');
			var $storeContainer = $(this).closest('li');
			
			// reset other store containers
			$preferredStoreForm.find('.select-preferred-store-text').text(Resources.SELECT_STORE);
			$preferredStoreForm.find('li').removeClass('preferred-store');
			// update the current
			$buttonText.text(Resources.PREFERRED_STORE);
			$storeContainer.addClass('preferred-store');
			$psSubmit.removeAttr('disabled');
			setPreferredStoreAB(this.value);
		});

		$($psSubmit).on('click', function (e) {
			var action = $(this).attr("name");
			e.preventDefault();
			$.ajax({
				type: 'POST',
				url: $preferredStoreForm.attr("action"),
				data: $preferredStoreForm.serialize(),
				dataType: 'html',
				success: function (html) {
					var selectedStore = $('#dialog-container').find('.preferred-store input').val();
					setPreferredStore(selectedStore);
					$.ajax({
						url: Urls.getAvailablePickupStore,
						type: 'GET'
					});
					dialog.close();
					if ($('#data-variation-url') && $('#data-variation-url').length) {
						window.location.reload();//window.location = $('#data-variation-url').html();
					} else {
						window.location.reload();
					}
				}.bind(this),
				failure: function () {
					window.alert(Resources.SERVER_ERROR);
				}
			});
		});
		setRenderedStoresSlickHeight();
	}

	// This function is for the event handling on the cart page for when user swaps between shipping and bopis
	$('.delivery-options-container').find('.input-radio').on("change", function (e) {
		var deliveryoption = $(this).val();
		var storeId = $(this).attr('data-store-id');
		var selectedStoreDiv = $(this).closest('label').children(".details-container").length;
		$.ajax({
			url: Urls.changeDeliveryOption,
			type: 'POST',
			data: {deliveryoption: deliveryoption, storeId: storeId},
			success: function (data) {
				if (deliveryoption == 'shipped') {
					$('.storeaddress').addClass('hide');
				} else if ((deliveryoption == 'instorepickup') && selectedStoreDiv == 0) {
					$('.store-picker a').click();
				}
			},
			async: false
		});

	});
	$("button[name='dwfrm_login_register']").on("click", function() {
		window.location.href = Urls.accountRegister;
	});
	
}
/**
 * @private
 * @function
 * @description Adds class ('js') to html for css targeting and loads js specific styles.
 */
function initializeDom() {
	// add class to html for css targeting
	$('html').addClass('js');
	if (SitePreferences.LISTING_INFINITE_SCROLL) {
		$('html').addClass('infinite-scroll');
	}
	// load js specific styles
	util.limitCharacters();
}

/**
 * @private
 * @function
 * @description Prevents opening chat links in new tab.
 */
function removeAnchorTarget() {
	MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

	var observer = new MutationObserver(function(mutations, observer) {
		var searchString=Urls.HTTPHOST;

		var links = $(".sidebarBody").find('a');

		for(var i = 0; i < links.length; i++) {

			var thisLink = links[i];

			if (thisLink.href.toLowerCase().indexOf(searchString) > -1) {
				if(thisLink.hasAttribute("target")){
					thisLink.removeAttribute("target");
				}

			}

		}
	});

	observer.observe(document, {
	  subtree: true,
	  attributes: true
	});
}

function openHawaiiPopup() {
    var hawaiDialog = require("./dialog");
	var hawaiiPopupHtml = $('.hawaii_popup_desktop').html();
    if(hawaiiPopupHtml) {
    	if(hawaiiPopupHtml.length > 0) {
        	$(document).on('click','.hawaii-intercept-close', function () {
        		$('.hawaii-popup-dialog').hide();
        		$('.ui-widget-overlay').hide();
        		hawaiDialog.close();
        		sfemaildialog.init(10);
        	});
        	hawaiDialog.open({
        		html: hawaiiPopupHtml,
        		options: {
        			autoOpen: true,
        			dialogClass: 'hawaii-popup-dialog',
        			close: function () {

        			}
        		}
        	});
        }
    }
}



function updateCustomerZipCode (url,data) {
	$.ajax({
		url: url,
		type: 'get',
		data: data,							
		success: function (data) {           
		   if(data){			   
			   window.location.reload();
		   }           
		},
		error: function (error) {
			var msg = error;
			//initDeliveryLocationEvents();
		}
	});
}

function initDeliveryLocationEvents() {
	if(SitePreferences.isGeoLocationSearchEnabled) {
		//get brand content asset delivery
		$.ajax({
			url: Urls.getDeliveryLocationShopByBrand,
			type: 'get',									
			success: function (data) {           
			   if(data){			   
				   $('.geo-content-selector').html(data);
			   }           
			},
			error: function (error) {
				var msg = error;			
			}		
	     });
		
		//get home content asset delivery
		$.ajax({
			url: Urls.getDeliveryLocationHome,
			type: 'get',									
			success: function (data) {           
			   if(data){			   
				   $('.geo-home-selector').html(data);
			   }           
			},
			error: function (error) {
				var msg = error;			
			}		
	     });
	}else{
		$('.geo-content-selector').css('display','none');
	}
   
	//brand content asset zip change
	$(document).on('click','.btn-change-zip-asset', function (e) {
		e.preventDefault();
		$(".delivery-shipping-asset").hide();
		$(".previous-location-asset").css('display','flex');
	});
	// zipcode selection on focus
	$(document).on('focus','#home-customer-zip', function (e) {	
		$(this).select();
		this.setSelectionRange(0,5);
	});
	$(document).on('focus','#store-customer-zip', function (e) {	
		$(this).select();
		this.setSelectionRange(0,5);
	});
	$("#store-customer-zip").on('keydown', function (e) {
	   e.stopPropagation();
	   if (e.which === 13) {
			e.preventDefault();
			$(".btn-update-store").click();
		}
	});
	$(document).on('focus','#store-customer-zip-mobile', function (e) {	
		$(this).select();
		this.setSelectionRange(0,5);
		$(window).scrollTop(0);
	});
	$(document).on('focus','#header-customer-zip-delivery', function (e) {
		$(this).select();
		this.setSelectionRange(0,5);
	});
	$(document).on('focus','#asset-customer-zip', function (e) {
		$(this).select();
		this.setSelectionRange(0,5);
	});
	$(document).on('focus','#plp-customer-zip', function (e) {
		$(this).select();
		this.setSelectionRange(0,5);
	});
	$(document).on('focus','#header-customer-zip-delivery-mobile', function (e) {
		$(this).select();
		this.setSelectionRange(0,5);
	});
	$(document).on('focus','#plp-customer-zip-mobile', function (e) {
		$(this).select();
		this.setSelectionRange(0,5);
	});
	//close buttons on tooltips
	$(document).on('click','.geo-close-btn', function (e) {
		e.preventDefault();
		$('.store-details.header-tooltip ul').hide();
	});
	$(document).on('touchstart','.geo-close-btn', function (e) {
		e.preventDefault();
		$('.store-details.header-tooltip ul').hide();
	});
	
	$(document).on('click','.geo-shipping-close-btn', function () {
		$('.shipping-details.header-tooltip ul').hide();
	});
	$(document).on('touchstart','.geo-shipping-close-btn', function (e) {
		e.preventDefault();
		$('.shipping-details.header-tooltip ul').hide();
	});
	$(document).on('click','.store-details .btn.primary', function () {
		$('.store-details.header-tooltip ul').show();
	});
	$(document).on('click','.shipping-details .btn.primary', function () {
		$('.header-tooltip ul').show();
	});
	
	$(document).on('touchstart','.mobile-find-store .store-details .btn', function (e) {
		e.preventDefault();
		if(utag) {
			utag.link({
				"eventCategory" : ["Mobile Find a Store"],
				"eventLabel" : ["Mobile Find a Store Clicked"],
				"eventAction" : ["click"]
			});
		}
	});
	
	//image hover
	$( ".deliver-ship" ).hover(function() {
	  $( ".deliver-ship__icon-active" ).css("display" , "none");
	  $( ".deliver-ship__icon" ).css("display" , "block");
	}, function(){
		$( ".deliver-ship__icon-active" ).css("display" , "block");
		$( ".deliver-ship__icon" ).css("display" , "none");
	});
	
	$( ".find-store" ).hover(function() {
		  $( ".find-store__icon" ).css("display" , "none");
		  $( ".find-store__icon-active" ).css("display" , "block");
	}, function(){
		$( ".find-store__icon" ).css("display" , "block");
		$( ".find-store__icon-active" ).css("display" , "none");
		});
	//home content asset zip change
	$(document).on('click','.btn-change-zip-home', function (e) {
		e.preventDefault();
		$(".delivery-shipping-home").hide();
		$(".previous-location-home").show();
	});
	
	//zip change pdp
	$(document).on('click','.btn-change-zip-pdp', function (e) {
		e.preventDefault();
		$(".delivery-shipping-pdp").hide();
		$(".previous-location-pdp").show();
		$(".in-stock-msg").hide();
		$("#pdp-customer-zip").select();
		$(".details-link").hide();
		$(".availability-web").hide();
	});
	//try in store pdp
	$(document).on('click','.storename-wrap', function (e) {
		e.preventDefault();
		$(".store-info").toggle();
		$(".change-tryinstore").toggle();
		$("a.regular").toggle();
		$("i.fa-angle-up").toggleClass('flip');
	});
	
	//brand content asset zip update
	$(document).on('click','.btn-update-asset', function (e) {
		e.preventDefault();
		var zipCodeVal = $('#asset-customer-zip').val();
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) { 
			var url = Urls.setCustomerZipCode;	
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location-asset .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location-asset .btn-update-asset");
	        }
		}	
	});
	
	//home content asset zip update
	$(document).on('click','.btn-update-home', function (e) {
		e.preventDefault();
		var zipCodeVal = $('#home-customer-zip').val();	
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) { 
			var url = Urls.setCustomerZipCode;	
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location-home .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location-home .btn-update-home");
	        }
		}	
	});	
	
	//pdp zip update
	$(document).on('click','.btn-update-pdp', function (e) {
		e.preventDefault();
		var zipCodeVal = $('#pdp-customer-zip').val();	
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) { 
			var url = Urls.setCustomerZipCode;	
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location-pdp .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location-pdp .btn-update-pdp");
	        }
		}	
	});	
	
	//plp desktop zip update	
	$(document).on('click','.btn-update-plp', function (e) {	
		e.preventDefault();
		var zipCodeVal = $('#plp-customer-zip').val();
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) { 
			var url = Urls.setCustomerZipCode;	
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location .btn-update-plp");
	        }
		}		
	});
	
	//plp desktop zip change
	$(document).on('click','.btn-change-zip-plp', function (e) {	
		e.preventDefault();
		$(".delivery-shipping").hide();
		$(".geo-main-wrapper.desktop-plp").hide();
		$(".previous-location").show(100);
	});
	
	//plp mobile zip update
	$(document).on('click','.btn-update-plp-mobile', function (e) {	
		e.preventDefault();
		var zipCodeVal = $('#plp-customer-zip-mobile').val();
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) {
			var url = Urls.setCustomerZipCode;	
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location .btn-update-plp-mobile");
	        }
		}
	});
	
	//plp mobile zip change
	$(document).on('click','.btn-change-zip-plp-mobile', function (e) {	
		e.preventDefault();
		$(".delivery-shipping-mobile").hide();
		$(".geo-main-wrapper.desktop-plp").hide();
		$(".previous-location-mobile").show(100);
	});
	
	//header store zip change
	$(document).on('click','.btn-change-zip-header-store', function (e) {
		e.preventDefault();
		$(".store-delivery-shipping").hide();
		$(".store-previous-location").show(100);
	});
	
	//header store desktop zip update
	$(document).on('click','.btn-update-store', function (e) {	
		e.preventDefault();
		var zipCodeVal = $('#store-customer-zip').val();
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) {
			var url = Urls.setCustomerZipCode;	
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location .btn-update-store");
	        }
		}
	});
	
	//header store mobile zip change
	$(document).on('click','.btn-change-zip-header-store-mobile', function (e) {
		e.preventDefault();
		$(".store-mobile-delivery-shipping").hide();
		$(".store-mobile-previous-location").show(100);
	});
	
	//header store mobile zip update
	$(document).on('click','.btn-update-store-mobile', function (e) {	
		e.preventDefault();
		var zipCodeVal = $('#store-customer-zip-mobile').val();	
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) {
			var url = Urls.setCustomerZipCode;	
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".store-mobile-previous-location .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".store-mobile-previous-location .btn-update-store-mobile");
	        }
		}
	});	
	
	//header delivery desktop zip update
	$(document).on('click','.btn-update-header', function (e) {	
		e.preventDefault();
		var zipCodeVal = $('#header-customer-zip-delivery').val();	
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) { 
			var url = Urls.setCustomerZipCode;	
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location .btn-update-header");
	        }
		}
		
	});
	
	//header delivery mobile zip update
	$(document).on('click','.btn-update-header-mobile', function (e) {	
		e.preventDefault();
		var zipCodeVal = $('#header-customer-zip-delivery-mobile').val();	
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCodeVal)) { 
			var url = Urls.setCustomerZipCode;	
			var data = {zipCode: zipCodeVal};
			updateCustomerZipCode(url,data);
		}else{
			//display error for zip
	    	if ($(".previous-location .error-message").length < 1) {
	            $("<div class='error-message'>Please enter a valid ZIP Code</div>").insertAfter(".previous-location .btn-update-header-mobile");
	        }
		}
		
	});
	
	//user current location 
	$(document).on('click touchstart','.current-location-selector', function (e) {	
        e.preventDefault();
        var zipCodeVal = userCurrentGeoLocation.init();        
    });	
	
	//cookies locations 
	$(document).on('click','.last-visited-zip-selector', function (e) {	
		e.preventDefault();
		var zipCodeVal = $(this).attr('data-zipcode');		
		var url = Urls.setCustomerZipCode;	
		var data = {zipCode: zipCodeVal};
		updateCustomerZipCode(url,data);
		
	});    
}

var pages = {
	account: require('./pages/account'),
	cart: require('./pages/cart'),
	checkout: require('./pages/checkout'),
	compare: require('./pages/compare'),
	product: require('./pages/product'),
	registry: require('./pages/registry'),
	search: require('./pages/search'),
	storefront: require('./pages/storefront'),
	wishlist: require('./pages/wishlist'),
	storelocator: require('./pages/storelocator'),
	makepayment: require('./pages/makepayment')
};

var app = {
	init: function () {
		if (document.cookie.length === 0) {
			$('<div/>').addClass('browser-compatibility-alert').append($('<p/>').addClass('browser-error').html(Resources.COOKIES_DISABLED)).appendTo('#browser-check');
		}
		if(SitePreferences.isBrowserGeoEnabled) {
			browserGeoLocation.init();
		}
        hoverIntent.init();
        initializeDom();
		initializeEvents();
		initDeliveryLocationEvents();

		// init specific global components
        countries.init();
        tooltip.init();
				scrollTop.init();
		minicart.init();
		validator.init();
		rating.init();
		searchplaceholder.init();
		util.uniform();
		content.init();
		amplience.initZoomViewer();
		zipzone.init();
		//sfemaildialog.init();
		mobilegeolocator.initModal();
		exacttargetnew.init();
		
		// execute page specific initializations
		$.extend(page, window.pageContext);
		var ns = page.ns;
		if (ns && pages[ns] && pages[ns].init) {
			pages[ns].init();
		}
		// Initialize the Google Analytics library
		if (typeof window.DW.googleAnalytics === 'object') {
			googleAnalytics.init(ns, window.DW.googleAnalytics.config);
		}
		responsiveSlots.init(ns || '');

		//Open Chat Widet Upon clicking header and footer links
		$( "#contentChatWithUsNew, .contentChatWithUs" ).click(function(e) {
			e.preventDefault();
		  if($('#helpButtonSpan').length > 0) {
				$('#helpButtonSpan').click();
			}
		});
		if ($('.checkout-mini-cart-wrapper').length > 0) {
			//$('.checkout-mini-cart-wrapper').mCustomScrollbar();
		}
		//Prevents opening chat links in new window.
		removeAnchorTarget();
		if (SitePreferences.isGoogleAddressLookUpEnabled && window.pageContext && window.pageContext.ns && window.pageContext.ns == 'checkout'){
			addresslookup.initialize();
		}

		//open hawaii popup
		if(Globals.isHawaiCustomer && SitePreferences.enableHawaiiModal) {
			openHawaiiPopup();
		}
		else {
			sfemaildialog.init();
		}
	}
};

// general extension functions
(function () {
	String.format = function () {
		var s = arguments[0];
		var i, len = arguments.length - 1;
		for (i = 0; i < len; i++) {
			var reg = new RegExp('\\{' + i + '\\}', 'gm');
			s = s.replace(reg, arguments[i + 1]);
		}
		return s;
	};
})();

// initialize app
$(document).ready(function () {
	app.init();
	if ($('.pt_cart').length > 0 || $('.pt_checkout').length > 0) {
        $('.scrollToTop').remove();
    }
	$('.primary-logo').focus();
	$(document).on('click','.ui-widget-overlay', function () {
		$('.ui-dialog').hide();
		$('.ui-widget-overlay').hide();
	});
	checkoutFormIds();
	$("#add-to-cart").removeClass('disabled-btn');
	$("#checkout-button-ID").removeClass('disabled-btn');
	$("#shippingMethodContinueCheckout").removeClass('disabled-btn');
});
function checkoutFormIds(){
	$(document).on('keyup', '#dwfrm_billing_billingAddress_addressFields_city,#dwfrm_billing_billingAddress_addressFields_address2,#dwfrm_billing_billingAddress_addressFields_address1,#dwfrm_billing_billingAddress_addressFields_lastName,#dwfrm_billing_billingAddress_addressFields_firstName,#dwfrm_singleshipping_shippingAddress_addressFields_address2,#dwfrm_singleshipping_shippingAddress_addressFields_city,#dwfrm_singleshipping_shippingAddress_addressFields_address1,#dwfrm_singleshipping_shippingAddress_email_emailAddress,#dwfrm_singleshipping_shippingAddress_addressFields_firstName,#dwfrm_singleshipping_shippingAddress_addressFields_lastName', function () {
		var variableName = $(this).attr('id');
		var shortVariableName = variableName.split('_');
		var type = shortVariableName[1];
		shortVariableName = shortVariableName[shortVariableName.length - 1];
		validateSting("#"+variableName,shortVariableName,type);
	})


}
function validateSting(val, name, type ) {
    if (validateByRegex($(val).val())) {
	    if($('val,.reg-error-'+type+name).length > 0 || $(val).val() == ""){
			  $(val).parent().find('span').remove();
		}

    } else {
		  if($(val).val() != "" && $('val,.reg-error-'+type+name).length < 1){
			  $(val).parent().append("<span class='error count-reg reg-error-"+type+name+"'>"+'Please remove invalid characters'+"</span>");
		  }
		  if($(val).val() == ""){
			  $(val).parent().find('span').remove();
		  }
    }

}
function validateByRegex(val) {
  var re = /^[\w\. \[\]!&()+,\-?=%@_{}#_{},#$ÀÁÂÄàáâäÈÉÊèéêëÌÍÎìíîïÒÓÔÖòóôöÙÚÛÜùúûüÇçÑñ¿¡]+$/;
  return re.test(val);
}

if($(".product-financing-custom").length == 2) {
   $(".product-financing-custom.no-credit").addClass('allignTop');
}
//Add Promo Code on Order Summary
$(document).on('click', 'button#add-promo-code-label-id', function (e) {
    e.preventDefault();
    $('#add-promo-code-label-id').addClass('visually-hidden');
    $('#add-promo-code-input-id').removeClass('visually-hidden');
});
if($('.header-countdown').length == 0) {
    $('#main').css('padding-top','130px');
}
if(screen.width > 767){
	if($('.header-top-new div').length > 0) {
		$('.pt_cart #main').css('padding-top','60px');
		$('.pt_cart #main .cart-empty').css('padding-top','60px');
	}
	if($('.header-top-new div').length > 0 && $('.header-countdown').length == 0) {
		$('.pt_product-search-result #main').css('padding-top','214px');
	}
}
$(document).on('click', '#header-mobile-toggle', function (e) {
	if($(this).hasClass('is-open')){
		$('header.header-main').css('position','initial');
	}else{
		$('header.header-main').css('position','fixed');
	}
});

$(window).scroll(function () {
	var currentScroll = $(document).scrollTop(); // get current position
	if (currentScroll > 100) {
		$('.checkout-mini-cart-wrapper').addClass("order-summary-fixed");
	} else {
		$('.checkout-mini-cart-wrapper').removeClass("order-summary-fixed");
	}
});
$(".geolocation-dialog > .close").click(function() {
	$(".geolocation-dialog").css( "display", "none");
	$('header.header-main').css('top','0px');
});
var windowOffset = $(document).scrollTop();
if($('.geolocation-dialog').css('display') != 'none' && $('.geolocation-dialog').length > 0)
{
	$('header.header-main').css('top','190px');
}
if($('.header-top-new div').length == 0)
{
	$('.header-top-promo').css('top' , '152px');
}

$('#checkout-button-ID').on('click tap', function (e) { 
	$(this).addClass('disabled-btn');
});

$('#shippingMethodContinueCheckout').on('click tap', function (e) { 
	$(this).addClass('disabled-btn');
});
$('.quote').slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	slide: '.inner-main',
	focusOnSelect: true,
	infinite: true,
	dots:true,
	arrows:false,
	autoplay: true,
	autoplaySpeed: 3000,
	adaptiveHeight: true,
});
$('.variation-carousal.AB-s').on('click tap', function (e) { 
	e.preventDefault();
	$('.variations-container.BF').hide();
	$('.variations-container.BS').hide();
	$('.variations-container.BSH').hide();
	$('.variations-container.AB').show();
	$('.Q-section-1').show();
	$('.Q-section-2').hide();
	$('.Q-section-3').hide();
	$('.Q-section-4').hide();
});
$('.variation-carousal.BF-s').on('click tap', function (e) { 
	e.preventDefault();
	$('.variations-container.AB').hide();
	$('.variations-container.BS').hide();
	$('.variations-container.BSH').hide();
	$('.variations-container.BF').show();
	$('.Q-section-1').hide();
	$('.Q-section-2').show();
	$('.Q-section-3').hide();
	$('.Q-section-4').hide();
	$(".variation-carousal.AB-s").removeClass("selected");
});
$('.variation-carousal.BS-s').on('click tap', function (e) {
	e.preventDefault();
	$('.variations-container.AB').hide();
	$('.variations-container.BF').hide();
	$('.variations-container.BSH').hide();
	$('.variations-container.BS').show();
	$('.Q-section-1').hide();
	$('.Q-section-2').hide();
	$('.Q-section-3').show();
	$('.Q-section-4').hide();
	$(".variation-carousal.AB-s").removeClass("selected");
});
$('.variation-carousal.BSH-s').on('click tap', function (e) { 
	e.preventDefault();
	$('.variations-container.AB').hide();
	$('.variations-container.BF').hide();
	$('.variations-container.BS').hide();
	$('.variations-container.BSH').show();
	$('.Q-section-1').hide();
	$('.Q-section-2').hide();
	$('.Q-section-3').hide();
	$('.Q-section-4').show();
	$(".variation-carousal.AB-s").removeClass("selected");
});
$( ".variation-carousal.AB-s .inner" ).hover(function() {
	  $( ".adjustable-base-head-move" ).css("display" , "none");
	  $( ".adjustable-base-head-move-current" ).css("display" , "block");
	}, function(){
		if (!$(this).is(":focus")) {
			$( ".adjustable-base-head-move" ).css("display" , "block");
			$( ".adjustable-base-head-move-current" ).css("display" , "none");
		}
});
$( ".variation-carousal.AB-s .inner" ).focus(function() {
	  $( ".adjustable-base-head-move" ).css("display" , "none");
	  $( ".adjustable-base-head-move-current" ).css("display" , "block");
});
$( ".variation-carousal.AB-s .inner" ).blur(function() {
	  $( ".adjustable-base-head-move" ).css("display" , "block");
	  $( ".adjustable-base-head-move-current" ).css("display" , "none");
});
$( ".variation-carousal.BF-s .inner" ).hover(function() {
	  $( ".bed-sets" ).css("display" , "none");
	  $( ".bed-sets-current" ).css("display" , "block");
	}, function(){
		if (!$(this).is(":focus")) {
			$( ".bed-sets" ).css("display" , "block");
			$( ".bed-sets-current" ).css("display" , "none");
		}
});
$( ".variation-carousal.BF-s .inner" ).focus(function() {
	  $( ".bed-sets" ).css("display" , "none");
	  $( ".bed-sets-current" ).css("display" , "block");
});
$( ".variation-carousal.BF-s .inner" ).blur(function() {
	  $( ".bed-sets" ).css("display" , "block");
	  $( ".bed-sets-current" ).css("display" , "none");
});
$( ".variation-carousal.BS-s .inner" ).hover(function() { 
	  $( ".box-spring" ).css("display" , "none"); 
	  $( ".box-spring-current" ).css("display" , "block");
	}, function(){
		if (!$(this).is(":focus")) {
			$( ".box-spring" ).css("display" , "block");
			$( ".box-spring-current" ).css("display" , "none");
		}
});
$( ".variation-carousal.BS-s .inner" ).focus(function() {
	  $( ".box-spring" ).css("display" , "none");
	  $( ".box-spring-current" ).css("display" , "block");
});
$( ".variation-carousal.BS-s .inner" ).blur(function() {
	  $( ".box-spring" ).css("display" , "block");
	  $( ".box-spring-current" ).css("display" , "none");
});
$( ".variation-carousal.BSH-s .inner" ).hover(function() {
	  $( ".headboard" ).css("display" , "none");
	  $( ".headboard-current" ).css("display" , "block");
	}, function(){
		if (!$(this).is(":focus")) {
			$( ".headboard" ).css("display" , "block");
			$( ".headboard-current" ).css("display" , "none");
		}
});
$( ".variation-carousal.BSH-s .inner" ).focus(function() {
	  $( ".headboard" ).css("display" , "none");
	  $( ".headboard-current" ).css("display" , "block");
});
$( ".variation-carousal.BSH-s .inner" ).blur(function() {
	  $( ".headboard" ).css("display" , "block");
	  $( ".headboard-current" ).css("display" , "none");
});
