	// Sticky Nav
	$(window).bind("scroll", window_scrolled);

	// Hide mobile header on on scroll down
	var didScroll;
	var lastScrollTop = 0;
	var delta = 5;
	var navbarHeight = $('header.header-main').outerHeight();

	var position = $(window).scrollTop();

	var navbarHeightDesktop = $('header.header-main').outerHeight();
	var stickyScrollPoint = 106;

	function window_scrolled() {
		var windowOffset = $(document).scrollTop();
		didScroll = true;
		if (parseInt($(window).width()) < 767) {
			// mobile scroll
			setInterval(function () {
				if (didScroll) {
					hasScrolled();
					didScroll = false;
				}
			}, 250);
			if(parseInt(windowOffset) > 66) {
				$('header.header-main').css('top','-200px');
			}
			 if(parseInt(windowOffset) < 66) {
				$('header.header-main').css('top','0px');
			}
			 if(parseInt(windowOffset) < 66 && $('.geolocation-dialog').css('display') != 'none' && $('.geolocation-dialog').length > 0) {
				$("header.header-main").css("top" , "190px");
			}
		} else {
			// desktop scroll
			if (parseInt(windowOffset) < stickyScrollPoint) {
				$('#wrapper').removeClass('sticky');
			} else {
				if (lastScrollTop < stickyScrollPoint && (($(document).height() - (stickyScrollPoint + navbarHeightDesktop)) > $(window).height())) {
					//$('#wrapper').addClass('sticky');
					if ($('.pt_checkout').length > 0) {
						$('.pt_checkout').removeClass('sticky');
						$(window).scroll(function () {
							var currentScroll = $(document).scrollTop(); // get current position	 
							if (currentScroll > 100) {
								// apply position: fixed if you
								$('.checkout-mini-cart-wrapper').addClass("order-summary-fixed");
								//$('.checkout-mini-cart-wrapper').mCustomScrollbar();
							} else {
								$('.checkout-mini-cart-wrapper').removeClass("order-summary-fixed");
							}
						});
					}
				}
			}
			// desktop filters scroll
			if ($('#secondary.refinements').length > 0 && parseInt($(window).width()) > 767) {
				var contentHeight = parseInt($('.content-slot.slot-grid-header').height() + 200);
				if (parseInt(windowOffset) > contentHeight) {
					$('#secondary.refinements').addClass('sticky');
				} else {
					$('#secondary.refinements').removeClass('sticky');
				}

				// when scrolling up, pin refinement window to top
				var scroll = $(window).scrollTop();
				if (scroll < position) {
					$('#secondary.refinements').scrollTop(0);
				}
				position = scroll;
			}

			lastScrollTop = windowOffset;
		}
	}

	// Fix for unwanted submenu showing when in sticky nav
	$('ul.header-nav li.first-level').hover(function (e) {
		var mY = $(e.target).position().top;
		var navPos = $(this).find('.header-nav-item a').position().top;
		var distance = Math.floor(mY - navPos);
		// show subnav when less than 50px away
		if (distance < 50 && distance > -1) {
			$(this).find('.header-nav-submenu').addClass('active');
		} else {
			$(this).find('.header-nav-submenu').removeClass('active');
		}
	});

	function hasScrolled() {
		var st = $(document).scrollTop();
		// Make sure they scroll more than delta
		if (Math.abs(lastScrollTop - st) <= delta) {
			return;
		}

		if (parseInt(st) < navbarHeight || (st > lastScrollTop && st > navbarHeight)) {
			// Scroll Down
			$('#wrapper').removeClass('sticky');
		} else {
			// Scroll Up
			if (st + $(window).height() < $(document).height()) {
				$('#wrapper').addClass('sticky');
			}
		}

		lastScrollTop = st;
	}