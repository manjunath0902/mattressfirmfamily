'use strict';

var imagesLoaded = require('imagesloaded'),
	quickview = require('./quickview'),
	ResponsiveSlots = require('./responsiveslots/responsiveSlots');

function initQuickViewButtons() {
	$('.tiles-container .product-image').on('mouseenter', function () {
		var $qvButton = $('#quickviewbutton');
		if ($qvButton.length === 0) {
			$qvButton = $('<a id="quickviewbutton" class="quickview">' + Resources.QUICK_VIEW + '</a>');
		}
		var $link = $(this).find('.thumb-link');
		$qvButton.attr({
			'href': $link.attr('href'),
			'title': $link.attr('title')
		}).appendTo(this);
		$qvButton.on('click', function (e) {
			e.preventDefault();
			quickview.show({
				url: $(this).attr('href'),
				source: 'quickview'
			});
		});
	});
}

function gridViewToggle() {
	$('.toggle-grid').on('click', function () {
		$('.search-result-content').toggleClass('wide-tiles');
		$(this).toggleClass('wide');
		if ($('.search-result-content').hasClass('wide-tiles')) {
			$(".product-key-features-plp").show();
			
		} else {
			$(".product-key-features-plp").hide();
		}
		if ( SitePreferences.CURRENT_SITE == "1800Mattress-RV" ) {
			if ($('.search-result-content').hasClass('wide-tiles')) {
				$('.trimmed-name').show();
				$('.product-text-hover').hide();
				var uptoPercent = $('.search-result-content').find('.product-standard-price');
				if(uptoPercent) {
					uptoPercent.css( "padding-left", "0px" );
					uptoPercent.siblings( ".percentageDiscount" ).css( "margin-left", "-70px" );
				}
				if ($('.product-standard-price').hasClass('has-sale-price')) {
					$('.has-sale-price').siblings( ".percentageDiscount" ).css( "margin-left", "0px" );
				}
			} else {
				$('.trimmed-name').hide();
				$('.product-text-hover').show();
			}
			
		}
	});
	$('.toggle-grid').on('keypress', function (ev) {
		if (ev.keyCode == 13 || ev.which == 13) {
			$('.search-result-content').toggleClass('wide-tiles');
			$(this).toggleClass('wide');
			if ($('.search-result-content').hasClass('wide-tiles')) {
				$(".product-key-features-plp").show();
				
			} else {
				$(".product-key-features-plp").hide();
			}
		}
	});
}
function compareToolPadding() {
	var $compareTool = $('.tiles-container .product-tile .compare-check'); //compareTool if on then its padding will be handled by compareToolPadding class
	if ($compareTool.length > 0) {
		$(".search-result-content .product-tile").addClass('compareToolPadding');
	}
}

function getDateSuffix(day) {
	if (day > 3 && day < 21) return 'th'; 
	switch (day % 10) {
		case 1:  return "st";
		break;
	 		case 2:  return "nd";
		break;
			case 3:  return "rd";
			break;
		default: return "th";
	}	
}

/**
 * @private
 * @function
 * @description Initializes events on the product-tile for the following elements:
 * - swatches
 * - thumbnails
 */
function initializeEvents() {
	if (SitePreferences.isQVenabled) {
		initQuickViewButtons();
	}
	gridViewToggle();
	compareToolPadding();
	$(document).on('mouseleave','.swatch-list', function () {
		// Restore current thumb image
		var $tile = $(this).closest('.product-tile'),
			$thumb = $tile.find('.product-image .thumb-link img').eq(0),
			data = $thumb.data('current');

		$thumb.attr({
			src: data.src,
			alt: data.alt,
			title: data.title
		});
	});
	$(document).on('click','.swatch-list .swatch', function (e) {
		if ($(this).hasClass('selected')) { return; }
		if (!$(this).hasClass('selected')) {
			e.preventDefault();
		}
		var $tile = $(this).closest('.product-tile');
		$(this).closest('.swatch-list').find('.swatch.selected').removeClass('selected');
		$(this).addClass('selected');
		$tile.find('.thumb-link').attr('href', $(this).attr('href'));
		$tile.find('name-link').attr('href', $(this).attr('href'));

		var data = $(this).children('img').filter(':first').data('thumb');
		var $thumb = $tile.find('.product-image .thumb-link img').eq(0);
		var currentAttrs = {
			src: data.src,
			alt: data.alt,
			title: data.title
		};
		$thumb.attr(currentAttrs);
		$thumb.data('current', currentAttrs);
	}).on('mouseenter','.swatch-list .swatch', function () {
		// get current thumb details
		var $tile = $(this).closest('.product-tile'),
			$thumb = $tile.find('.product-image .thumb-link img').eq(0),
			data = $(this).children('img').filter(':first').data('thumb'),
			current = $thumb.data('current');

		// If this is the first time, then record the current img
		if (!current) {
			$thumb.data('current', {
				src: $thumb[0].src,
				alt: $thumb[0].alt,
				title: $thumb[0].title
			});
		}

		// Set the tile image to the values provided on the swatch data attributes
		$thumb.attr({
			src: data.src,
			alt: data.alt,
			title: data.title
		});
	});
}

function initializeTileEvents(){	
    $('.btn-more-features').unbind("click");	
	$('.btn-more-features').on('click', function(){
		$(this).closest('.comfort-and-features').toggleClass('more-features-visible');
	});
	$( ".features-list .feature" ).on( "mouseenter touchstart", function( event ) {
		$('.feature-details').hide();
		$(this).find('.feature-details').show();
	});
	$( ".features-list .feature" ).on( "mouseleave touchend", function( event ) {
		$('.feature-details').hide();
		$(this).find('.feature-details').hide();
	});
	$('.btn-more-features-mobile').on('click', function(){
		$(this).siblings('.features-list').show();
	});
	$('.close-features').on('click', function(){
		$(this).closest('.features-list').hide();
	});		



	$(".ab-plp-tiles-v3").find(".plp-tile-availability").each(function(){
		var currentDate = new Date();
		var arrivalDate = new Date();
		var months = ["January","February","March", "April", "May", "June","July", "August", "September", "October","November", "December"];
		var days = ["Sunday","Monday","Tuesday","Wednesday", "Thursday", "Friday", "Saturday"];
		var availibityMsg = '';
		var finalMessage = '';
		var n = currentDate.getHours();
		  if(n < 14) {			    
			  arrivalDate.setDate(arrivalDate.getDate() + 1);
	          availibityMsg = 'As soon as Tomorrow, ';	          
	          var fullMonthName = months[arrivalDate.getMonth()];
	          var date = arrivalDate.getDate();
	          var arrivalDateSuffix = getDateSuffix(date);		     
		      finalMessage = availibityMsg + fullMonthName + ' ' + date + arrivalDateSuffix;
		  } else {			    
			  arrivalDate.setDate(arrivalDate.getDate() + 2);
		      availibityMsg = 'As soon as ';
		      var day = arrivalDate.getDay();
		      var fullDayName = days[arrivalDate.getDay()];		      
		      var fullMonthName = months[arrivalDate.getMonth()];		      
		      var date = arrivalDate.getDate();		      
		      var arrivalDateSuffix = getDateSuffix(date);		      
		      finalMessage = availibityMsg + fullDayName + ', ' + fullMonthName + ' ' + date + arrivalDateSuffix;
		  }	      
	      
		  $(this).html(finalMessage);
	});	
}

exports.init = function () {
	var $tiles = $('.tiles-container .product-tile');
	if ($tiles.length === 0) { return; }
	imagesLoaded('.product-image').on('done', function () {
		/*$tiles.syncHeight()
			.each(function (idx) {
				idx = idx;
				$(this).data('idx', idx);
			});*/
	});
	ResponsiveSlots.smartResize(function () {
		$tiles
		.each(function () {
			$(this).removeAttr('style');
		});
	});
	initializeEvents();
	initializeTileEvents();
};

/* 
*  This is needed in order re-init the layout of the product tile
*  with compare enabled after inifinte scroll loads.
*/
exports.initCompare = function () {
	compareToolPadding();
	var util = require('./util');
	util.uniform();
};

/* 
*  Initialize new product tile events after each infinite scroll load
*/
exports.initTileEvents = function () {
	initializeTileEvents();
};

/* 
*  This initializes the comfort level indicators for the MFRM
*  sub-category landing pages. Needs to be initialized after
*  each infinite scroll load.
*/
exports.initComfortLevelTagging = function () {
	$('.comfort-level.mattressfirm').find('span').each(function() {
		var _this = $(this);
		var comfortLevelClass = _this.attr('class');
		if (comfortLevelClass == _this.data('comfortlevel')) {
			_this.addClass('active');
			_this.prev('span').css('border', 'none');
			_this.on('click', function() {
				window.location = _this.data('pdplink');
			});
		}
	});
};

$('.comfort-level.1800mattressrv').find('span').each(function() {
	 if ($(this).hasClass('active')){
		 $(this).prev('span').css('border', 'none');
	 }
});


if ($('.pdp-comfort-scale-container').hasClass('scale-comfort-level-1')) {
	$('.comfort-scale-items > .comfort-level-1').css('margin-left', '4px');
	$('.comfort-scale-items > .comfort-level-1').prev('span').css('border', 'none');
}
if ($('.pdp-comfort-scale-container').hasClass('scale-comfort-level-2')) {
	$('.comfort-scale-items > .comfort-level-2').css('margin-left', '4px');
	$('.comfort-scale-items > .comfort-level-2').prev('span').css('border', 'none');
}
if ($('.pdp-comfort-scale-container').hasClass('scale-comfort-level-3')) {
	$('.comfort-scale-items > .comfort-level-3').css('margin-left', '4px');
	$('.comfort-scale-items > .comfort-level-3').prev('span').css('border', 'none');
}
if ($('.pdp-comfort-scale-container').hasClass('scale-comfort-level-4')) {
	$('.comfort-scale-items > .comfort-level-4').css('margin-left', '4px');
	$('.comfort-scale-items > .comfort-level-4').prev('span').css('border', 'none');
}
if ($('.pdp-comfort-scale-container').hasClass('scale-comfort-level-5')) {
	$('.comfort-scale-items > .comfort-level-5').css('margin-left', '4px');
	$('.comfort-scale-items > .comfort-level-5').prev('span').css('border', 'none');
}


// add carousel init to the global window object so that the asyncronous carousel can access it
window.productRecs = function() {

	var container = {
		initialize: function($el) {
			this.$el = $el;
		},
		showCarousel: function() {
			var dfd = new $.Deferred();

			var recs = this.$el.find('li');
			var cqSlot = this.$el.closest('div[id^="cq_recomm_slot"]');
			if (window.CQuotient && recs.length > 0 && cqSlot.length > 0) {
				dfd.resolve();
			}

			return dfd.promise();
		}
	};

	function slider(slider) {
		if(slider.hasClass('slick-slider')) {
			return false;
		} else {
			slider.slick({
				infinite: false,
				speed: 300,
				slide: '.grid-tile',
				slidesToShow: 4,
				slidesToScroll: 4,
				responsive: [{
						breakpoint: 1025,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 3
						}
					},
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
							dots:true,
						}
					}
				]
			});
		}
	}

	container.initialize($('.recs-slider'));
	var promise = container.showCarousel();
	promise.done(function() {
		$('.recs-slider').each(function() {
			slider($(this));
		});
	});
};