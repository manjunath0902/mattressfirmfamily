(function ($) {
  var pdpTabs = {
    btn: $('.pdp-tabs li'),
    expand: $('.pdp-tab-title'),
    all: $('.pdp-tabs-content li, .pdp-tabs li'),
    active: 'is-active',

    init: function () {
      pdpTabs.setupListeners();
    },

    setupListeners: function() {
      pdpTabs.btn.on('click', function() {
        var data = $(this).data('pdp-tab'),
            content = $('[data-pdp-tab-content="' + data + '"]');

        pdpTabs.all.removeClass(pdpTabs.active);

        $(this).addClass(pdpTabs.active);
        content.addClass(pdpTabs.active).slideDown();
      });
    }
  };

  $(document).ready(function () {
    pdpTabs.init();
  });
})(jQuery);