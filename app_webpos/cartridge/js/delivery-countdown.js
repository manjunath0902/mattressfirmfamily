(function ($) {
	var deliveryCountdown = {
		d: new Date(),

		init: function () {
			var checkIfTimerExists = setInterval(function () {
				if ($('#delivery-countdown').length) {
					deliveryCountdown.checkTime();
					clearInterval(checkIfTimerExists);
				}
			}, 1000);
		},

		currentDate: function () {
			var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
				month = months[this.d.getMonth()],
				day = this.d.getDate(),
				year = this.d.getFullYear(),
				currentDate = month + ' ' + day + ', ' + year;

			return currentDate;
		},

		currentTime: function (date) {
			var hour = date.getHours(),
				mins = date.getMinutes(),
				secs = date.getSeconds(),
				time = hour + ':' + mins + ':' + secs;

			return [hour, time];
		},

		checkTime: function () {
			var date = new Date(),
				time = deliveryCountdown.currentTime(date)[1],
				hour = deliveryCountdown.currentTime(date)[0],
				timeArr = [14, 18, 24];

			for (i = 0; i < timeArr.length; i++) {
				if (hour < timeArr[i]) {
					deliveryCountdown.countdown(deliveryCountdown.currentDate(), timeArr[i]);
					return false;
				}
			}
		},


		countdown: function (date, time) {
			var countDownDate = new Date();
			countDownDate.setHours(time, 0, 0, 0);

			var counter = setInterval(function () { 
				var now = new Date().getTime(),
					distance = countDownDate - now,
					hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)),
					minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)),
					seconds = Math.floor((distance % (1000 * 60)) / 1000);

				if (60000 > distance) {
					document.getElementById("delivery-countdown").innerHTML = seconds + "secs";
					if (2000 >= distance) {
						clearInterval(counter);
						setTimeout(function () {
							deliveryCountdown.checkTime();
						}, 2500);
					}
				} else {
					document.getElementById("delivery-countdown").innerHTML = hours + "hrs " + minutes + "mins";
				}

			}, 1000);
		}
	};
	// we dont need any timer in web pos. it creates error on pdp thats why commented.

	/*$(document).ready(function () {
		deliveryCountdown.init();
	});*/
})(jQuery);