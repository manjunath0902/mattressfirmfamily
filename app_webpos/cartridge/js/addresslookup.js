'use strict';

var autocompleteShip, 
autocompleteBill;

var componentForm = {
		street_number: 'short_name',
		route: 'long_name',
		locality: 'long_name',
		administrative_area_level_1: 'short_name',
		country: 'long_name',
		postal_code: 'short_name'
};

exports.initialize = function () {

	var options = {
			componentRestrictions: { country: "us" }
	};

	var inputShip = document.getElementById('dwfrm_singleshipping_shippingAddress_addressFields_address1'); //autocomplete input
	var inputBill = document.getElementById('dwfrm_billing_billingAddress_addressFields_address1'); //autocomplete input

	autocompleteShip = new google.maps.places.Autocomplete(inputShip, options);
	autocompleteBill = new google.maps.places.Autocomplete(inputBill, options);

	// Setup the listeners for change
	autocompleteShip.addListener('place_changed', placeChangedHandlerShip);
	autocompleteBill.addListener('place_changed', placeChangedHandlerBill);

}

function placeChangedHandlerShip() {

	var place = autocompleteShip.getPlace();
	var streetNum = "";

	for (var i = 0; i < place.address_components.length; i++) {
		var addressType = place.address_components[i].types[0];
		if (componentForm[addressType]) {
			var val = place.address_components[i][componentForm[addressType]];

			if (addressType == 'street_number'){
				streetNum = val;

			}else if (addressType == 'route'){

				var field = $("#dwfrm_singleshipping_shippingAddress_addressFields_address1");
				if (streetNum && streetNum.trim() != "") {
					field.val(streetNum + ' ' + val);
				}else {
					field.val(val);
				}
				
				// set focus to address 2 field
				$("#dwfrm_singleshipping_shippingAddress_addressFields_address2").focus();

			}else if (addressType == 'locality'){
				var field = $("#dwfrm_singleshipping_shippingAddress_addressFields_city");
				field.val(val);
				field.change(); // Mark change for form validation

			}else if (addressType == 'administrative_area_level_1'){
				var field = $("#dwfrm_singleshipping_shippingAddress_addressFields_states_state");
				field.val(val);
				field.change(); // Mark change for form validation

			}else if (addressType == 'country'){
				//$("#dwfrm_singleshipping_shippingAddress_addressFields_country").val(val);

			} else if (addressType == 'postal_code'){
				var field = $("#dwfrm_singleshipping_shippingAddress_addressFields_postal");
				field.val(val);
				field.change(); // Mark change for form validation
			}                
		}
	}        
}

function placeChangedHandlerBill() {
	// Get the place details from the autocomplete object.
	var place = autocompleteBill.getPlace();
	var streetNum = "";

	for (var i = 0; i < place.address_components.length; i++) {

		var addressType = place.address_components[i].types[0];

		if (componentForm[addressType]) {
			var val = place.address_components[i][componentForm[addressType]];

			if (addressType == 'street_number'){
				streetNum = val;

			}else if (addressType == 'route'){

				var field = $("#dwfrm_billing_billingAddress_addressFields_address1");
				if (streetNum && streetNum.trim() != "") {
					field.val(streetNum + ' ' + val);
				}else {
					field.val(val);
				}
				// set focus to address 2 field
				$("#dwfrm_billing_billingAddress_addressFields_address2").focus();
				
			}else if (addressType == 'locality'){
				var field = $("#dwfrm_billing_billingAddress_addressFields_city");
				field.val(val);
				field.change(); // Mark change for form validation

			}else if (addressType == 'administrative_area_level_1'){
				var field = $("#dwfrm_billing_billingAddress_addressFields_states_state");
				field.val(val);
				field.change(); // Mark change for form validation

			}else if (addressType == 'country'){
				//$("#dwfrm_billing_billingAddress_addressFields_addressFields_country").val(val);

			} else if (addressType == 'postal_code'){
				var field = $("#dwfrm_billing_billingAddress_addressFields_postal");
				field.val(val);
				field.change(); // Mark change for form validation         	  
			}             
		}
	}

}