'use strict';

var util = require('./util'),
	dialog = require('./dialog');


var ampHeight = 500,
	ampWidth  = 598,
	spin360ImageIndex;

var amplience = {
	initZoomViewer: function (ampset) {
		var spin360ID;
		// start fresh
		this.destroy();
		var jsonSet = ampset != null && ampset != '' ? ampset : $('.product-meta').data('jsonset'),
			sash = $('.product-meta').data('sash'),
			ampLoading = true;
		// setup amplience instance
		amp.init({
			client_id: SitePreferences.amplienceClientId,
			di_basepath: 'https://' + SitePreferences.amplienceHost + '/',
			errImg: '../shared/404image.jpg'
		});
		// create html from image set json
		amp.get([{'name': jsonSet, 'type': 's'}], function (data) {
			var dataNav = jQuery.extend(true, {}, data[jsonSet]); // Deep copy
			for (var i = 0; i < dataNav.items.length; i++) {
				var item = dataNav.items[i];
				if (item.set || item.setType) {
					item.type = 'img'; // 360 image (type: set) is not required for Navigation
				}
			}
			//start fresh
			amplience.destroy();
			var dis = amp.di.set(data[jsonSet], {h: ampHeight, w: ampWidth, sm: 'CM'});
			if (sash) {
				dis['items'][0]['src'] += sash;
			}
			amp.genHTML(dis, $('.amplience-main')[0], true);
			//the below check is for image zoom pop-up on PDP
			if($('.amplience-main').length > 1){
				amp.genHTML(dis, $('.amplience-main')[1], true);
			}
			var ampCach = {
				ul: $('.amplience-main').find('ul'),
				img: $('.amplience-main').find('img')
			};
			if (data[jsonSet].items.length > 1) {
				for (var i = 0; i < data[jsonSet].items.length; i++) {
					var item = data[jsonSet].items[i];
					if(item.src != null) {
						item.src = item.src.replace("http://", "https://");
					}					
					if (item.set || item.setType) {
						spin360ID = '#' + item.name;
					}
				}
				amp.genHTML(amp.di.set(dataNav, {h: 135, w: 150, sm: 'CM'}), $('.amplience-carousel')[0], true);
				//the below check is for image zoom pop-up on PDP
				if($('.amplience-carousel').length > 1){
					amp.genHTML(amp.di.set(dataNav, {h: 135, w: 150, sm: 'CM'}), $('.amplience-carousel')[1], true);
				}
				var ampNav = {
					img: $('.amplience-nav').find('img'),
					ul: $('.amplience-nav').find('ul')
				};
				ampNav.img.ampImage({preload: 'visible'});
			}

			// set zoombox fosset from top
			$('#zoom-box').css({top: ($('.amplience-viewer').offset().top - 50) + 'px'});
			ampCach.img.ampZoom({
				translations: 'sm=CM',
				preload: {
					image: 'created',
					zoomed: 'none'
				},
				zoom: 2,
				cursor: {
					active: 'zoom-out',
					inactive: 'zoom-in'
				}
			});
			$('.amplience-main').on('mouseenter', function () {
				ampLoading = true;
			}).on('mouseleave', function () {
				ampLoading = false;
			});
			$('.amplience-viewer ul img').each(function () {
				if($(this).attr('data-amp-src') != null) {
					var str = $(this).attr('data-amp-src').replace("http://","https://");
					$(this).attr('src', str);
					$(this).attr('data-amp-src', str);
					var imgalt = $('.amplience-viewer').attr('data-product-name');
					$(this).attr('alt', imgalt);
					$('.amp-zoom-img').attr('alt', imgalt);
				}else{
					$(this).attr('src', $(this).attr('data-amp-src'));
				}				
			});
			if ($(".product-video-overlay-container").length > 0 && !util.isMobileSize()) {
				var video = $(".product-video-overlay-container").html();
				$(".product-video-overlay-container").remove();
				$(".amplience-nav ul").append("<li>" + video + "</li>");
			}
			if ($(".product-video-amplience-overlay-container").length > 0 && !util.isMobileSize()) {
				var video = $(".product-video-amplience-overlay-container").html();
				$(".product-video-amplience-overlay-container").remove();
				$(".amplience-nav ul").append("<li>" + video + "</li>");
			}
			$('.amplience-nav, .product-video-overlay-container').on('click', '.product-video-overlay', function (e) {
				var src = $(this).attr('data-video-src'),
				$iframe = $('<div class="product-video-wrapper"><iframe src="//www.youtube.com/embed/' + src + '?enablejsapi=1&hl=en&fs=1&showinfo=0&autoplay=1&wmode=transparent" frameborder="0" width="100%" height="600" hei allowfullscreen></iframe></div>');
				dialog.open({
					html: $iframe,
					options: {
						autoOpen: true,
						dialogClass: 'product-video-dialog',
						title: Resources.VIDEO_TITLE,
						close: function () {
							$(this).find('iframe').get(0).contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
						}
					}
				});
			});
			if(spin360ID){
				$(spin360ID).ampSpin({
			        preload:true,
			        cursor: {
			            active: 'default',
			            inactive: 'ew-resize'
			        }
			  });
				$('.amplience-nav, ul').find(spin360ID).after( "<span class='spin-overlay'>360&deg;</span>" );
			}
			$(document).on('click' , '.ui-widget-overlay' , function (e){
				dialog.close();
			});
			function parseVideoSrc(src) {
				var $iframe = '<div class="product-video-wrapper-ampilence"><video controls="true" autoplay="true" id="ampilence-video" width="100%">';
				$('.product-video-amplience').each(function () {
					var videoSrc = $(this).attr('amplience-video-src'),
						videoExt = videoSrc.split('/')[videoSrc.split('/').length - 1],
						videoType = $(videoExt).size() > 0 ? videoExt.split('_')[0] : 'mp4';
					$iframe += '<source src="https://' + SitePreferences.amplienceHost + '/v/' + SitePreferences.amplienceClientId + '/' + $(this).attr('amplience-video-src') + '" type="video/' + videoType + '"></source>';
				});
				$iframe += '</video></div>';

				return $iframe;
			};

			$('.amplience-nav, .product-video-amplience-overlay-container').on('click', '.product-video-amplience-overlay', function (e) {
				var $iframe = parseVideoSrc($(this).attr('amplience-video-src'));
				dialog.open({
					html: $iframe,
					options: {
						autoOpen: true,
						dialogClass: 'product-video-dialog',
						title: Resources.VIDEO_TITLE,
						close: function () {
							var vid = document.getElementById("ampilence-video");
							vid.pause();
						}
					}
				});
			});
			$('.amplience-main > ul').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: true,
				accessibility: true,
				fade: true,
				slide: 'li',
				draggable: false,
				lazyLoad: 'ondemand',
				asNavFor: '.amplience-nav ul',
				infinite: false,
				responsive: [{
						breakpoint: util.mobileWidth,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
							accessibility: true,
							centerMode: true,
							arrows: true,
							fade:false,
							dots: true,
							swipe: true
						}
					},{
						breakpoint: 1026,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
							accessibility: true,
							dots: true,
						}
					}
				]
			});
			var url = window.location.href;

			if (window.isMattressProductPage){			
				$('.amplience-nav ul').slick({
					slidesToShow: 5,
					slidesToScroll: 1,
					asNavFor: '.amplience-main > ul',
					dots: false,
					slide: 'li',
					focusOnSelect: true,
					lazyLoad: 'ondemand',
					infinite: false,
					vertical: false,
						
				});
				$( ".feature" ).hover(function() {
					$(this).find(".red-svg").css("display" , "none");
					$(this).find(".white-svg").css("display" , "block");
				}, function(){
					$(this).find(".red-svg").css("display" , "block");
					$(this).find(".white-svg").css("display" , "none");
				});
				
				$('.features-wrap').slick({
					slidesToShow: 4,
					slidesToScroll: 4,
					slide: '.feature',
					focusOnSelect: true,
					lazyLoad: 'ondemand',
					infinite: false,
					dots:true,
					responsive: [
						{
							breakpoint: 1199,
							settings: {
							slidesToShow: 3,
							  slidesToScroll: 3,
							  arrows:false,
							 dots:true,					        
							 }
						},
						{
							breakpoint: 767,
						      settings: {
						        arrows: false,
						        slidesToShow: 1,
						        accessibility: true,
						        slide: '.feature',
						        slidesToScroll: 1,
						        centerMode: true,
						        centerPadding: '80px',
						      }
						},{
							breakpoint: 380,
								settings: {
									centerMode: false,
									arrows: false,
							        slidesToShow: 1,
							        slidesToScroll: 1,
							        accessibility: true,
							        slide: '.feature',
							        
								}
							}
						]
				});
				
				if ($(window).width() < 767) {
					$('.pdp-info-cards').slick({
						slidesToShow: 1,
						slidesToScroll: 1,
						slide: '.info-card',
						focusOnSelect: true,
						infinite: false,
						dots:true,
						arrows:false,
						adaptiveHeight: true,
					});
				}
				
				//hover replace image 
				$( ".pdp-info-cards .info-card .image-holder img.white-icon").css("display" , "none")
				$( ".pdp-info-cards .info-card" ).hover(function() {
					$(this).find(".charcoal-icon").css("display" , "none");
					$(this).find(".white-icon").css({"display": "block", "margin": "0 auto"});
				},function(){
					$(this).find(".charcoal-icon").css({"display": "block", "margin": "0 auto"});
					$(this).find(".white-icon").css("display" , "none");
				});
				
				
			}
			if ( SitePreferences.CURRENT_SITE == "1800Mattress-RV" ) {
				$('.amplience-nav ul').slick({
					slidesToShow: 5,
					slidesToScroll: 1,
					asNavFor: '.amplience-main ul',
					dots: false,
					slide: 'li',
					focusOnSelect: true,
					lazyLoad: 'ondemand',
					infinite: false,
					vertical: false,
						
				});
			} else {
				$('.amplience-nav ul').slick({
					slidesToShow: 5,
					slidesToScroll: 1,
					asNavFor: '.amplience-main > ul',
					dots: false,
					slide: 'li',
					focusOnSelect: true,
					lazyLoad: 'ondemand',
					infinite: false,
					vertical: false,
						
				});
			}
        },
        function callback () {
        	if (ampset!='') {
        		amplience.initZoomViewer('');
        	}
		});
	},
	initManyProductImages: function (ampset, target) {
		target.each(function (e) {
			var container = $(this);
			var jsonSet = ampset != null && ampset != '' ? ampset : $(this).find('.product-meta').data('jsonset'),
			sash = $(this).find('.product-meta').data('sash'),
			ampLoading = true;
			// setup amplience instance
			amp.init({
				client_id: SitePreferences.amplienceClientId,
				di_basepath: 'https://' + SitePreferences.amplienceHost + '/',
				errImg: '../shared/404image.jpg'
			});
			amp.get([{'name': jsonSet, 'type': 's'}], function (data) {
				amplience.destroyBonus(container);
				var dis = amp.di.set(data[jsonSet], {h: ampHeight, w: ampWidth, sm: 'CM'});
				if (sash) {
					dis['items'][0]['src'] += sash;
				}
				amp.genHTML(dis, container.find('.amplience-main')[0], true);
				var ampCach = {
					ul: container.find('.amplience-main').find('ul'),
					img: container.find('.amplience-main').find('img')
				};
				if (data[jsonSet].items.length > 1) {
					for (var i = 0; i < data[jsonSet].items.length; i++) {
						var item = data[jsonSet].items[i];
						if (item.src != null) {
							item.src = item.src.replace("http://", "https://");
						}
					}
				}
				container.find('.amplience-viewer ul img').each(function () {
					if($(this).attr('data-amp-src') != null) {
						var str = $(this).attr('data-amp-src').replace("http://","https://");
						$(this).attr('src', str);
						$(this).attr('data-amp-src', str);
					}else{
						$(this).attr('src', $(this).attr('data-amp-src'));
					}
					
				});

			});
		});
	},
	// Destroy current PDP viewer
	destroy: function () {
		$('#amplience-main').html('');
		$('#amplience-carousel').html('');
	},
	destroyBonus: function (container) {
		var containerThis = container;
		containerThis.find('.amplience-main').html('');
	}
};
module.exports = amplience;