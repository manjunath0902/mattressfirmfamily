'use strict';

var compareWidget = require('../compare-widget'),
    dialog = require('../dialog'),
	productTile = require('../product-tile'),
	progress = require('../progress'),
	util = require('../util'),
	truncate = require('../curtail'),
	ResponsiveSlots = require('../responsiveslots/responsiveSlots');

function infiniteScroll() {
	// getting the hidden div, which is the placeholder for the next page
	var loadingPlaceHolder = $('.infinite-scroll-placeholder[data-loading-state="unloaded"]');
	// get url hidden in DOM
	var gridUrl = loadingPlaceHolder.attr('data-grid-url');

	if (loadingPlaceHolder.length === 1 && util.elementInViewport(loadingPlaceHolder.get(0), 250)) {
		// switch state to 'loading'
		// - switches state, so the above selector is only matching once
		// - shows loading indicator
		loadingPlaceHolder.attr('data-loading-state', 'loading');
		loadingPlaceHolder.addClass('infinite-scroll-loading');


		// named wrapper function, which can either be called, if cache is hit, or ajax repsonse is received
		var fillEndlessScrollChunk = function (html) {
			loadingPlaceHolder.removeClass('infinite-scroll-loading');
			loadingPlaceHolder.attr('data-loading-state', 'loaded');
			$('div.search-result-content').append(html);
		};

		// old condition for caching was `'sessionStorage' in window && sessionStorage["scroll-cache_" + gridUrl]`
		// it was removed to temporarily address RAP-2649
		if (false) {
			// if we hit the cache
			fillEndlessScrollChunk(sessionStorage['scroll-cache_' + gridUrl]);
		} else {
			// else do query via ajax
			$.ajax({
				type: 'GET',
				dataType: 'html',
				url: gridUrl,
				success: function (response) {
					// put response into cache
					try {
						sessionStorage['scroll-cache_' + gridUrl] = response;
					} catch (e) {
						// nothing to catch in case of out of memory of session storage
						// it will fall back to load via ajax
					}
					// update UI
					fillEndlessScrollChunk(response);
					//productTile.init();
					productTile.initCompare();
					productTile.initComfortLevelTagging();
					productTile.initTileEvents();
				}
			});
		}
	}
}
/**
 * @private
 * @function
 * @description replaces breadcrumbs, lefthand nav and product listing with ajax and puts a loading indicator over the product listing
 */
function updateProductListing(url, isStopEventTriggered) {
	if (!url || url === window.location.href) {
		return;
	}
	progress.show($('.search-result-content'));
	var listView = '';
	if ($('.search-result-content').hasClass('wide-tiles')) {
		var listView = true;
	}

	var filterOpen = false;
	if ($('#secondary.refinements').find('#nav_label').prop('checked')) {
		filterOpen = true;
	}
	
	var currentURL = window.location.href;
	
	$('#main').load(util.appendParamToURL(url, 'format', 'ajax'), function () {
		//get site name
		var siteNameVal = $("input[name='siteName']").val();
		if (siteNameVal == 'Mattress-Firm' || siteNameVal == 'POS') {
		if (filterOpen) {
			$('#secondary.refinements').find('#nav_label').click();
			var _closeButton = $('.refinement-big-container').find('div.close-reminement');
			if (_closeButton.hasClass('sticky')) {
				_closeButton.fadeTo(300, 0, function() {
					$(this).removeClass('sticky');
				});
			} else {
				_closeButton.addClass('sticky');
			}
		}
		/*
		$('.no-results__button').on("click", function(e) {
			e.preventDefault();
			if( currentURL ){
				updateProductListing(currentURL);
				//window.location = currentURL;
			}
			$(".no-results").css("opacity" , "0.5");
			$(".no-results-filters").css("opacity" , "0.5");
		});
		*/
		}
		initContainerHeight();
		initRefinementButtons(isStopEventTriggered);
		initSubcategoryContent();
		compareWidget.init();
		productTile.init();
		productTile.initComfortLevelTagging();
		truncate.init();
		progress.hide();
		util.uniform();
		initPlaceHolder();
		history.pushState(undefined, '', url);
		updatePaginationPosition();
		updateSortPosition();
		updateRefinedByPosition();
		mobileCloseRefinement();
		positionGridList();
		posRefEventInitializer();
		if (listView) {
			$('.search-result-content').addClass('wide-tiles');
			$('.toggle-grid').addClass('wide');
			$(".product-key-features-plp").show();
		}
		if (util.isMobileSize() && siteNameVal != 'Mattress-Firm' && siteNameVal != 'POS') {
			$('html,body').animate({
				scrollTop: $('.refinement-big-container').offset().top},
			'slow');
		}
		var queryString = window.location.search;
		$('body').removeClass('panel-open');
		var n = queryString.indexOf("q=");
		if ((siteNameVal == 'POS' || siteNameVal == 'Mattress-Firm' || siteNameVal == '1800Mattress-RV') && queryString.indexOf("q=") < 0) {
			//get category name
			var defaultTitle = $("input[name='catName']").val();
			//get selected refinement from hidden field on click checkbox
			var ajaxTitle = $("input[name='selectedRefinements']").val();
			if (ajaxTitle && ajaxTitle.length > 0) {
				document.title = ajaxTitle;
			}else if (defaultTitle && defaultTitle.length > 0) {
				//if uncheck all refinment on click
				document.title = defaultTitle;
			}
			var singleFacet = $("input[name='hfCanonicalIdSingleFacet']").val();
			var multiFacet = $("input[name='hfCanonicalIdMultipleFacet']").val();
			var prevPage = $("input[name='prevPageInfo']").val();
			var nextPage = $("input[name='nextPageInfo']").val();
			var priceRefinementCheck = $("input[name='PriceRefinementCheck']").val();
			var isRobot = $("input[name='isRobotAppliesForSingleCanonical']").val();
			if (singleFacet && singleFacet.length > 0 && singleFacet != "null") {
				$('link[name=singleCanonical]').remove();
				$('link[name=multiCanonical]').remove();
				$('meta[name=ROBOTS]').remove();
				$('head').append('<link name="singleCanonical" rel="canonical" href=' + singleFacet + '>');
				if (priceRefinementCheck && priceRefinementCheck.length > 0) {
					$('head').append('<meta name="ROBOTS" content="NOINDEX, FOLLOW">');
				} else if(isRobot == "true") {
					$('head').append('<meta name="ROBOTS" content="NOINDEX, FOLLOW">');
				}
			}
			if (multiFacet && multiFacet.length > 0 && multiFacet != "null") {
				$('link[name=singleCanonical]').remove();
				$('link[name=multiCanonical]').remove();
				$('meta[name=ROBOTS]').remove();
				$('head').append('<meta name="ROBOTS" content="NOINDEX, FOLLOW">');
				$('head').append('<link name="multiCanonical" rel="canonical" href=' + multiFacet + '>');
			}
			$('link[name=prevPageInfo]').remove();
			$('link[name=nextPageInfo]').remove();
			if (prevPage && prevPage.length > 0) {
				$('head').append('<link name="prevPageInfo" rel="prev" href=' + prevPage + '>');
			}
			if (nextPage && nextPage.length > 0) {
				$('head').append('<link name="nextPageInfo" rel="next" href=' + nextPage + '>');
			}

			/*ZipCode Update Event for Clearance Page*/
			var zipCode = User.customerZip;
			var currentLocationZipCode = User.currentLocationZipCode;
			if(zipCode) {
				$('#customer-zip').val(zipCode);
			}
			else if(currentLocationZipCode) {
				$('#customer-zip').val(currentLocationZipCode);
			}

			document.title = document.title.replace("Shop Shop","Shop");
		}
				
	});
	
}

/**
 * @private
 * @function
 * @description initialize refinement: Price Slider
 */
function initPriceSliderRefinement() {
	
	var priceLabel = $('#pricing-labels');
	
	if (priceLabel.length > 0 ){
		var amountsStr = priceLabel.data('amounts').split(',');
		var amounts = [];
		for (var i=0; i < amountsStr.length; i++) {
			amounts.push(Number(amountsStr[i]));
		}
		var maxAvailableAmount = Number(priceLabel.data('max-available-amount'));
		var indexMin = 0;
		var indexMax = amounts.length -1;
	
		var initialIndexes = getInitialPositionOfPricingSlider(amounts);
		if (initialIndexes.indexMin != -1) {
			indexMin = initialIndexes.indexMin;
		}
		if (initialIndexes.indexMax != -1) {
			indexMax = initialIndexes.indexMax;
		}
		// If indexMin & indexMax are both 0 then update indexMax to have a window of 1 step on slider
		if (indexMax == 0 && amounts.length > 1) {
			indexMax++;
		}
		var initSteps = [indexMin, indexMax];
	
		$("#plp-filter-slider-range").slider({
			range: true,
			animate: true,
			orientation: "horizontal",
			min: 0,
			max: amounts.length - 1,
			values: initSteps,
			step: 1,
			create: function(event, ui) {
				var minIndex = $("#plp-filter-slider-range").slider("values", 0)+1;
				var maxIndex = $("#plp-filter-slider-range").slider("values", 1)+1
				showSelectedPrices(minIndex, maxIndex);
			},
			slide: function(event, ui) {
					if(ui.values[0] == ui.values[1]) // min/max equal values not allowed
						return false;
					// If min value selected was the least value from available amounts then take it as 0 (i.e. <100)
					$("#min-selected-price").val(ui.values[0] > 0 ? amounts[ui.values[0]] : 0);
					// If max value selected was the maximum value from available amounts then take it as maxAvailableAmount (i.e. 500+/4000+)
					$("#max-selected-price").val(ui.values[1] < amounts.length-1 ? amounts[ui.values[1]] : maxAvailableAmount);
					showSelectedPrices(ui.values[0]+1, ui.values[1]+1);
			},
			stop: function( event, ui ) {
				var pmax = $("#max-selected-price").val();
				var pmin = $("#min-selected-price").val();
				// Get Search Results on the basis of Price Range selected and render products on PLP
				updateProductListing(updateURLforPricingPLPFilter(pmax, pmin));
				if (util.isMobileSize() || $(window).width() < 768) {
					$('html,body').animate({
				        scrollTop: $(".no-result").offset().top - 100
				    }, 'slow');
				}
				$('html,body').animate({
			        scrollTop: $("#primary").offset().top - 100
			    }, 'slow');
			}
		});
		var amountMinSelected = $("#plp-filter-slider-range").slider("values", 0)>0?amounts[$("#plp-filter-slider-range").slider("values", 0)]:0;
		var amountMaxSelected = $("#plp-filter-slider-range").slider("values", 1)<amounts.length-1?amounts[$("#plp-filter-slider-range").slider("values", 1)]:maxAvailableAmount;
		$("#min-selected-price").val(amountMinSelected);
		$("#max-selected-price").val(amountMaxSelected);
	
		$("#plp-filter-slider-range").draggable();
	}
}

/**
 * @private
 * @function
 * @description find out the current position of Pricing Slider positions
 * @returns {Object} the minIndex and maxIndex of the slider positions
 */
function getInitialPositionOfPricingSlider(amounts) {
	/*
	 * Setting the Min & Max values of the Slider.
	 * If pmax & pmin parameters exist in the Product Search Result then set those values, else take 0 & last index as default values
	 * */
	var indexMin = -1,
		indexMax = -1;
	if ($('#selected-prices').data('pmin') == null || $('#selected-prices').data('pmax') == null) {
		return {
			indexMin : indexMin,
			indexMax : indexMax
		}
	}
	var pMinFromPSR = Number($('#selected-prices').data('pmin'));
	var pMaxFromPSR = Number($('#selected-prices').data('pmax'));

	/*
	 * The below logic will find out the indices for Pricing Slider as such to have the pMinFromPSR & pMaxFromPSR inclusive
	 * */
	// finding out indexMin
	for (var i=0; i<amounts.length-1; i++) {
		if (pMinFromPSR >= amounts[i]) {
			indexMin = i;
		}
	}
	// Select closest value for indexMin from amounts array
	if (indexMin != -1 && (pMinFromPSR - amounts[indexMin]) > (amounts[indexMin+1] - pMinFromPSR)) {
		indexMin++;
	}
	// finding out indexMax
	for (var i=amounts.length-1; i>=0; i--) {
		if (pMaxFromPSR <= amounts[i]) {
			indexMax = i;
		}
	}
	// Select closest value for indexMax from amounts array
	if (indexMax != -1 && (amounts[indexMax] - pMaxFromPSR) > (pMaxFromPSR - amounts[indexMax-1])) {
		indexMax--;
	}
	// non-equal values for indexMin & indexMax
	if (indexMin == indexMax) {
		if (indexMax < amounts.length-1)
			indexMax++;
		else if (indexMin > 0)
			indexMin--;
	}
/*	indexMin = amounts.indexOf(pMinFromPSR)>-1 ? amounts.indexOf(pMinFromPSR) : 0;
	// If pmax was not found in URL or it's value was greater than max value in amounts then last index from amounts array, else have the respective index selected
	indexMax = (amounts.indexOf(pMaxFromPSR) == -1 || pMaxFromPSR > amounts[amounts.length-1]) ? amounts.length-1 : amounts.indexOf(pMaxFromPSR);
*/
	return {
		indexMin : indexMin,
		indexMax : indexMax
	}
}

/**
 * @private
 * @function
 * @description display only selected min/max prices on the Pricing Slider
 * @param {String} min the index of min price label
 * @param {String} max the index of max price label
 */
function showSelectedPrices(min, max) {
	// Hide all price labels
	$('#selected-prices-labels .price-steps *').addClass('hide-me');
	// Display the selected min/max price labels
	$('#amount-selected-label-'+min).removeClass('hide-me');
	$('#amount-selected-label-'+max).removeClass('hide-me');
}

/**
 * @private
 * @function
 * @description initialize refinement: Thickness Slider
 */
function initThicknessSliderRefinement(isStopEventTriggered) {
	var heightIndex = $(".thickness-height-val").length-1;
	var minVal = 0;
	var maxVal = heightIndex;

	var initialIndexes = getInitialPositionOfMattressHeightSlider(isStopEventTriggered);
	if (initialIndexes.minVal != -1) {
		minVal = initialIndexes.minVal;
	}
	if (initialIndexes.maxVal != -1) {
		maxVal = initialIndexes.maxVal;
	}
	var initSteps = [minVal, maxVal];

	$('#mattress-thickness-slider').slider({
        orientation: "vertical",
        animate: true,
        range: true,
        min: 0,
        max: heightIndex,
        step: 1,
        values: initSteps, // Initial state
        stop: function( event, ui ) {
        	sessionStorage['updatedURLForMattressHeight'] = null;
        	var updatedURLForMattressHeight = updateURLforMattressHeightPLPFilter(ui.values[0], ui.values[1]);
        	sessionStorage['updatedURLForMattressHeight'] = updatedURLForMattressHeight;
            updateProductListing(updatedURLForMattressHeight, true);
            $('html,body').animate({
		        scrollTop: $("#primary").offset().top - 100
		    }, 'slow');
        }
    });
	$('#mattress-thickness-slider').draggable();
}

/**
 * @private
 * @function
 * @description find out the current position of Mattress Height Slider positions
 * @returns {Object} the minIndex and maxIndex of the slider positions
 */
function getInitialPositionOfMattressHeightSlider(isStopEventTriggered) {
	var minVal = -1,
		maxVal = -1;

	var prefn = '';
	var currentURL = '';
	if (isStopEventTriggered) {
		currentURL = (sessionStorage['updatedURLForMattressHeight'] != null && sessionStorage['updatedURLForMattressHeight'] != '') ? sessionStorage['updatedURLForMattressHeight'] : window.location.href;
	} else {
		currentURL = window.location.href;
	}
	if(currentURL != null && currentURL != '') {
		prefn = util.getParamNameFromURL(currentURL, 'mattressHeight');
	}

	var paramsVal = "";
	var prefv = 'prefv';
	if (prefn != '') {
		prefv += prefn[prefn.length-1];
		paramsVal = util.getParamValueFromURL(currentURL, prefv);
	}

	if(paramsVal != null && paramsVal != "") {
		var mattressHeightParam = paramsVal.substring(paramsVal.indexOf('=')+1,paramsVal.length);
		var decodedURL = decodeURIComponent(mattressHeightParam);
		var mattressHeightParams = decodedURL.split('|');

		$(".thickness-height-val").each(function (index) {
			if(($("#thickness_"+index).text().toString().replace('"','').replace(/\n/g,'')) == (mattressHeightParams[0].toString().replace('"','').replace(/\n/g,''))){
				minVal = index;
			}
			if(($("#thickness_"+index).text().toString().replace('"','').replace(/\n/g,'')) == (mattressHeightParams[mattressHeightParams.length-1].toString().replace('"','').replace(/\n/g,''))){
				maxVal = index;
			}
		});
	}
	return {
		minVal : minVal,
		maxVal : maxVal
		};
}
function updateURLforMattressHeightPLPFilter(from, to) {
	var selectedHeights = '';
	for (var i=from; i<=to; i++) {
		selectedHeights += $('#thickness_'+i).text().replace(/\n/g,'') + '|';
	}
	selectedHeights = selectedHeights.substring(0, selectedHeights.length-1);
	var currentURL = window.location.href;
	var prefn = util.getParamNameFromURL(currentURL, 'mattressHeight');
	var prefv = 'prefv';
	if (prefn != '') {
		prefv += prefn[prefn.length-1];
		currentURL = util.removeParamFromURL(currentURL, prefv);
	} else {
		var j=1;
		prefn = 'prefn';
		while (util.getParamValueFromURL(currentURL, prefn+j) != '') {
			j++;
		}
		prefn += j;
		prefv += j;
		currentURL = util.appendParamToURL(currentURL, prefn, 'mattressHeight');
	}
	currentURL = util.appendParamToURL(currentURL, prefv, selectedHeights);
	return currentURL;
}

/**
 * @private
 * @function
 * @description initialize refinement buttons for mobile
 */
function initRefinementButtons(isStopEventTriggered) {
	var siteNameVal = $("input[name='siteName']").val();
	if (siteNameVal == 'Mattress-Firm' || siteNameVal == 'POS') {
	var _closeButton = $('.refinement-big-container').find('div.close-reminement');
	$('.refinement-big-container label.mobile-only').on('click', function() {
		if (_closeButton.hasClass('sticky')) {
			_closeButton.fadeTo(300, 0, function() {
				$(this).removeClass('sticky');
			});
		} else {
			_closeButton.addClass('sticky');
		}
	});

	$(window).on('scroll', function() {
		if ($('.refinement-big-container nav').height() > 0) {
			var containerHeight = parseInt($('.refinement-big-container nav')[0].scrollHeight);
			var position = parseInt(containerHeight);
			if (parseInt($(document).scrollTop()) >= position) {
				_closeButton.addClass('relative');
			} else {
				_closeButton.removeClass('relative');
			}
		}
	});

	// If AB testing params were found in the URL
	if ($('.plp-filter').length) {
		initPriceSliderRefinement();
		initThicknessSliderRefinement(isStopEventTriggered);
		$('#clearAllFilters').on("click", function(e) {
			e.preventDefault();
			window.location = $(this).attr('href');
		});			
		
	}
	}
	// Refinements Truncate functionality
	$(".hideRefinement").hide();
	$(".refinements-see-less").hide();
	$(".refinements-see-more").on("click", function() {
		$(this).parent().find(".hideRefinement").show();
		$(this).parent().find(".refinements-see-less").show();
		$(this).hide();
	});
	$(".refinements-see-less").on("click", function() {
		$(this).parent().find(".hideRefinement").hide();
		$(this).parent().find(".refinements-see-more").show();
		$(this).hide();

	});
	if($('.plp-filter').length > 0){
		$(".close-filter-mobile").click(function(){
	        $("#nav_label").prop("checked", false);
	        //$('#secondary.refinements').find('#nav_label').click();
			var _closeButton = $('.refinement-big-container').find('div.close-reminement');
			if (_closeButton.hasClass('sticky')) {
				_closeButton.fadeTo(300, 0, function() {
					$(this).removeClass('sticky');
				});
			} else {
				_closeButton.addClass('sticky');
			}
	    });
	}
}

/**
 * @private
 * @function
 * @description returns new URL with updated parameters values of pmax & pmin
 * @param {String} pmax the value of max price
 * @param {String} pmin the value of min price
 */
function updateURLforPricingPLPFilter(pmax, pmin) {
	var currentURL = $('#price-url').val();
	if (currentURL.indexOf('pmax')>-1 && currentURL.indexOf('pmin')>-1) {
		currentURL = util.removeParamFromURL(currentURL, 'pmax');
		currentURL = util.removeParamFromURL(currentURL, 'pmin');
	}
	return util.appendParamsToUrl(currentURL, {'pmax':pmax, 'pmin':pmin});
}

/**
 * @private
 * @function
 * @description Set min-height of sub-category container based on height of filters
 */
function initContainerHeight() {
	if ($('#secondary.refinements').length > 0 && parseInt($(window).width()) > 767) {
		var filterNavHeight = $('#secondary.refinements').height();
		$('#primary').css('min-height', filterNavHeight + 'px');
	}
}

/**
 * @private
 * @function
 * @description initialize sub-category header content
 */
function initSubcategoryContent() {
	// sub-category tab content
	var _tabContainer = $('.subcategory-content-tabs');
	_tabContainer.fadeIn();
	var _tabNav = _tabContainer.find('ul.tab-nav li');

	_tabNav.find('a').bind('click', function(e) {
		e.preventDefault();
		_tabNav.removeClass('tab-active');
		var target = $(this);
		var tabClass = target.data('tabclass');
		_tabContainer.find('.cat-content-block-text').each(function() {
			if ($(this).hasClass(tabClass)) {
				$(this).show();
				if ($(this).hasClass('related')) {
					$(this).css('display', 'inline-flex');
				}
				target.parent().addClass('tab-active');
			} else {
				$(this).hide();
			}
		});
	});
}

/**
 * @private
 * @function
 * @description updates pagination position for mobile
 */
function updatePaginationPosition() {
	if (util.isMobileSize() && $('.refinements .pagination').length < 1) {
		$('.search-result-options.top .pagination').prependTo('.refinements');
	}
	ResponsiveSlots.smartResize(function () {
		if (util.isMobileSize() && $('.refinements .pagination').length < 1) {
			$('.search-result-options.top .pagination').prependTo('.refinements');
		} else {
			if ($('.search-result-options.top .pagination').length < 1) {
				$('.refinements .pagination').prependTo('.search-result-options.top');
			}
		}
	});
}
/**
 * @private
 * @function
 * @description updates sort by position for mobile
 */
var siteNameVal = $("input[name='siteName']").val();
if (siteNameVal == '1800Mattress-RV') {
	if (util.isMobileSize() || $(window).width() < 1025) {
		if ($('.search-result-options.top .sort-by').length > 0) {
			$('.search-result-options.top .sort-by').hide();
			$('.search-result-options.top .sort-by').insertBefore('#primary').css("display", "block");
		}
		if ($('.refinement-big-container').length > 0) {
			$('.refinement-big-container').hide();
			$('.refinement-big-container').prependTo('#primary').css("display", "block");
		}
	}
}
function updateSortPosition() {
	if (util.isMobileSize() && siteNameVal != '1800Mattress-RV') {
	if ($('.refinement-big-container .sort-by').length < 1) {
		$('.search-result-options.top .sort-by').prependTo('.refinement-big-container');
	}
	}
	ResponsiveSlots.smartResize(function () {
		if (util.isMobileSize() && siteNameVal != '1800Mattress-RV') {
		if ($('.refinement-big-container .sort-by').length < 1) {
			$('.search-result-options.top .sort-by').prependTo('.refinement-big-container');
		}
		} else {
			if ($('.search-result-options.top .sort-by').length < 1) {
				$('.refinement-big-container .sort-by').insertAfter('.search-result-options.top .items-per-page-inner');
			}
		}
	});
	$('.refinement-big-container nav .refinement').last().addClass('refinement-last');
}
/**
 * @private
 * @function
 * @description updates refined by position for mobile
 */
function updateRefinedByPosition() {
	if (util.isMobileSize()) {
		if ($('.refinements .breadcrumb-refinement-mobile').length < 1) {
			$('.breadcrumb-refinement-mobile').insertAfter('.refinement-big-container');
		}
	}
	if ($(window).width() < 1025 && siteNameVal == '1800Mattress-RV') {
		if ($('.refinement-big-container')) {
			$('.refinement-big-container').hide();
			$('.refinement-big-container').insertAfter('.search-result-options.top').css("display", "block");
		}
		if ($(window).width() >767 && $(window).width() < 1025) {
			$('.search-result-options.top').css("bottom", "28px");
		}
	}
}
function initPlaceHolder() {
	$('[placeholder]').focus(function () {
		var input = $(this);
		if (input.attr('id') == 'email-alert-address') {
			return false;
		}
		if (input.val() == input.attr('placeholder')) {
			input.val('');
			input.removeClass('placeholder');
			}
		}).blur(function () {
			var input = $(this);
			if (input.attr('id') == 'email-alert-address') {
				return false;
			}
			if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.addClass('placeholder');
				input.val(input.attr('placeholder'));
				}
			}).blur().parents('form').submit(function () {
				$(this).find('[placeholder]').each(function () {
					var input = $(this);
					if (input.val() == input.attr('placeholder')) {
						input.val('');
					}
				})
			});
}

function mobileCloseRefinement() {
	$('.close-reminement').on('click', function (e) {
		$('#nav_label').click();
		var container = $('.refinement-big-container').offset().top;
		$('html,body').animate({
			scrollTop: container},
		'slow');
		var closeButton = $('.refinement-big-container').find('div.close-reminement.sticky');
		if (closeButton.length > 0) {
			closeButton.removeClass('sticky');
		}
	});
}
function positionGridList() {
	if (!util.isMobileSize()) {
		if ($('.items-per-page').length == 0) {
			$('.toggle-grid').addClass('one-page-items');
		}
		if ($('.items-per-page select option:selected').attr('data-count')) {
			var widthSelect = $('.items-per-page select').width();
			$('.toggle-grid').css('right', widthSelect + 'px');
		}
		$('.toggle-grid').css('display', 'block');
	}
}
function ellipsizeTextBox(el) {
	//var el = document.getElementById(id);
	var wordArray = el.innerHTML.split(' ');
	while (el.scrollHeight > el.offsetHeight) {
		wordArray.pop();
		el.innerHTML = wordArray.join(' ') + '<span class="product-text-hover">...</span>';
		}
	}


function ellipsizeTextBoxFor1800(el) {
	var wordArray = el.innerHTML.split(' ');
	var trimmedName = "";
	while (el.scrollHeight > el.offsetHeight) {
		trimmedName = trimmedName + wordArray.pop();
		el.innerHTML = wordArray.join(' ') + '<span class="product-text-hover">...</span> ' + ' <span class="trimmed-name"> ' + trimmedName + ' </span>';
		}
	}

/**
 * @private
 * @function
 * @description updates clearance zipcode on clearance PLP
 */
function updateClearanceZipCode() {
	var _zip = $.trim($('#customer-zip').val());
	// 1. Verify a valid US 5-digit ZIP first, and that the same ZIP was not entered again
	if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(_zip)) {
		var data = {zip: _zip};
		var url = Urls.pdpZoneCheck;
		$.ajax({
			url: url,
			type: 'post',
			data: data,
			beforeSend: function () {
				//progress.show('#product-content');
			},
			dataType: 'html',
			success: function () {
				window.location = window.location.pathname;
			},
			error: function (request, status, error) {
				/* service is down or had an error so enable the add to cart button */
				//$('#add-to-cart').removeAttr('disabled');
			},
			complete: function () {
				//progress.hide('#product-content');
			}
		});
	}
}


/**
 * @private
 * @function
 * @description Initializes events for the following elements:<br/>
 * <p>refinement blocks</p>
 * <p>updating grid: refinements, pagination, breadcrumb</p>
 * <p>item click</p>
 * <p>sorting changes</p>
 */
function initializeEvents() {
	var $main = $('#main');
	// compare checked
	$main.on('click', 'input[type="checkbox"].compare-check', function () {
		var cb = $(this);
		var tile = cb.closest('.product-tile');

		var func = this.checked ? compareWidget.addProduct : compareWidget.removeProduct;
		var itemImg = tile.find('.product-image a img').first();
		func({
			itemid: tile.data('itemid'),
			uuid: tile[0].id,
			img: itemImg,
			cb: cb
		});

	});

	// handle toggle refinement blocks
	$main.on('click', '.refinement h3', function () {
		$(this).toggleClass('expanded-title')
		.siblings('ul').toggle()
		.siblings('.refinement-top-level').toggle();
	});
	// handle toggle refinement blocks on enter key press
	$main.on('keypress', '.refinement h3', function (ev) {
		ev.preventDefault();
		if (ev.keyCode == 13 || ev.which == 13) {
			$(this).toggleClass('expanded-title')
			.siblings('ul').toggle()
			.siblings('.refinement-top-level').toggle();
			}
		});

	var siteNameVal = $("input[name='siteName']").val();
	if (siteNameVal == 'Mattress-Firm' || siteNameVal == 'POS') {
	$main.on('click', '.refinements a', function () {
		if (util.isMobileSize()) {
			$('html,body').animate({
		        scrollTop: $("#main").offset().top
		    }, 'slow');
		} else {
			$('html,body').animate({
		        scrollTop: $("#primary").offset().top - 100
		    }, 'slow');
		}
	});
	}
	if (siteNameVal == '1800Mattress-RV') {
		$main.on('click', '.refinements a', function () {
			$('html,body').animate({
		        scrollTop: $("#primary").offset().top
		    }, 'slow');
		});
	}
	
	// handle events for updating grid
	$main.on('click', '.refinements a, .pagination a, .breadcrumb-refinement-value a', function (e) {
		// dont't run for closing the refinement window
		if ($(e.target).hasClass('breadcrumb-refinement-clear-all')) {
			return;
		}
		// don't intercept for category and folder refinements, as well as unselectable
		if ($(this).parents('.category-refinement').length > 0 || $(this).parents('.folder-refinement').length > 0 || $(this).parent().hasClass('unselectable')) {
			return;
		}
		e.preventDefault();

		/*
		* This fixes the issue of the main site loading into the #main div when no filters are selected (MAT-518)
		*/
		var selectedBreadcrumbs = $('span.breadcrumb-refinement-value').length;
		var selectedRefinements = $('div.refinement ul li.selected').length;
		// handle breadcrumbs
		if ($(e.target).hasClass('breadcrumb-relax')) {
			if (selectedBreadcrumbs === 1) {
				var url = $('span.breadcrumb-refinement-value a').attr('href');
				window.location = url;
				return;
			}
		}
		// handle refinements
		var target = $(e.target).closest('li');
		if (target.hasClass('selected') && selectedRefinements === 1) {
			window.location = this.href;
			return;
		}
		/* End MAT-518 fix */

		updateProductListing(this.href);
		if ($(this).closest('.search-result-options.bottom').length > 0) {
			window.location.hash = '#primary';
		}
		//window.location.href = this.href;
	});
	$main.on('click', '.product-swatches-all', function (e) {
		if (util.isMobileSize()) {
			var parentHeight = $(this).parents('.product-tile').height();
			if (!$(this).next().is(':visible')) {
				$(this).parents('.product-tile').removeAttr('style').css('min-height', parentHeight + 'px');
			}
			$(this).next().toggle('show');
		}
	});
	// handle events item click. append params.
	$main.on('click', '.product-tile a:not("#quickviewbutton")', function () {
		var a = $(this);
		// get current page refinement values
		var wl = window.location;

		var qsParams = (wl.search.length > 1) ? util.getQueryStringParams(wl.search.substr(1)) : {};
		var hashParams = (wl.hash.length > 1) ? util.getQueryStringParams(wl.hash.substr(1)) : {};

		// merge hash params with querystring params
		var params = $.extend(hashParams, qsParams);
		if (!params.start) {
			params.start = 0;
		}
		// get the index of the selected item and save as start parameter
		var tile = a.closest('.product-tile');
		var idx = tile.data('idx') ? + tile.data('idx') : 0;

		// convert params.start to integer and add index
		params.start = (+params.start) + (idx + 1);
		// set the hash and allow normal action to continue
		a[0].hash = $.param(params);
	});

	// handle sorting change
	$main.on('change', '.sort-by select', function (e) {
		e.preventDefault();
		updateProductListing($(this).find('option:selected').val());
	})
	.on('change', '.items-per-page select', function () {
		var refineUrl = $(this).find('option:selected').val();
		$('.toggle-grid').css('display', 'none');
		if (refineUrl === 'INFINITE_SCROLL') {
			$('html').addClass('infinite-scroll').removeClass('disable-infinite-scroll');
		} else {
			$('html').addClass('disable-infinite-scroll').removeClass('infinite-scroll');
			updateProductListing(refineUrl);
		}
	});
	var windowW = $(window).width();
	if (windowW >= 768 && SitePreferences.CURRENT_SITE != "1800Mattress-RV") {
		$('.product-name .name-link .name-link-text').each(function () {
			ellipsizeTextBox(this);
		});
	}else if (windowW >= 768 && SitePreferences.CURRENT_SITE == "1800Mattress-RV") {
		$('.product-name .name-link .name-link-text').each(function () {
			ellipsizeTextBoxFor1800(this);
		});
	}

	$('.refinement-big-container nav .refinement').last().addClass('refinement-last');
	updatePaginationPosition();
	updateSortPosition();
	updateRefinedByPosition();
	initPlaceHolder();
	mobileCloseRefinement();
	positionGridList();
	$('.primary-logo').focus();

	/*Submit Event for Clearance Page ZipCode Change*/
	$(document).on("submit","#customer-zip-form",function (e) {
		e.preventDefault();
		updateClearanceZipCode();
	});

	/*ZipCode Update Event for Clearance Page*/
	var zipCode = User.customerZip;
	var currentLocationZipCode = User.currentLocationZipCode;
	if(zipCode) {
		$('#customer-zip').val(zipCode);
	}
	else if(currentLocationZipCode) {
		$('#customer-zip').val(currentLocationZipCode);
	}
}

function zipValidationAlert() {
	$('<div id="wrongzipdialog"></div>').appendTo('body')
    .html('<p>Please enter the right delivery<br>zip code and search again.</p>')
    .dialog({
      modal: true,
      zIndex: 10000,
      autoOpen: true,
      width: 'auto',
      modal: true,
      resizable: false,
      closeOnEscape: false,
      open: function(event, ui) {
          $(".ui-dialog-titlebar-close").hide();
          $(".ui-dialog-titlebar.ui-widget-header.ui-corner-all.ui-helper-clearfix").hide();
      },
      buttons: {
        OK: function() {
          $(this).dialog("close");
          $(this).remove();
        }
      },
      close: function(event, ui) {
        $(this).remove();
      }
    });
}

function initializeRefinements() {
	$(document).on('click','#refinements-btn', function () {
		$('body').addClass('panel-open');
		$("#refinements-panel").show();
	
	});
	
	$(document).on('click', function (e) {
	    if ($(e.target).closest(".mobile-refinements").length === 0) {
	        $("#refinements-panel").hide();
	        $('body').removeClass('panel-open');
	    }
	});
	
	$(document).on('click','.overSelect, .drop-down', function () {
		if($(this).hasClass('size')) {
			var element = $('.radios-list');
		}
		else {
			var element = $('.checkboxes-list');
		}
		
		if($(element).hasClass('visible')) {
			$(element).hide();
			$(element).removeClass("visible");
		}
		else {
			$(element).show();
			$(element).addClass("visible");
		}
	});
	
	$(document).on('click', '#applyrefinements, #apply-filters', function (e) {
		e.preventDefault();
		
		var zipCode = $('input[name="customerZip"]').val();
	    if(IsValidZipCode(zipCode)) {
	        $.ajax({
	            type: 'GET',
	            url: Urls.validateZipCode,
	            data: {zipCode:zipCode},
	            dataType: 'json',
	            success: function (data) {
	                if(data.success) {
	                	var refUrl = formFullRefUrl();
	            		//window.location.href = refFormUrl;
	                	$('body').attr('data-httpurl', window.location.href);
	            		updateProductListing(refUrl);
	            		closeOpenPanels();
	                } else if (!data.success) {
	                	zipValidationAlert();
	                }
	            },
	            failure: function () {
	                // do something when ajax fails
	            	zipValidationAlert();
	            }
	        });
	    } else {
	    	zipValidationAlert();
	    }

	});
	
	posRefEventInitializer();

	//Beautify Size & Sort Dropdown using plugin
	//$('select[name="size"]').selectBox();
	//$('#grid-sort-header').selectBox();
	
	//Remove Last Filter
	$(document).on('click', 'button[name="remove-last-filter"]', function (e) {
		e.preventDefault();
		var lastUrl = $('body').attr('data-httpurl');
		updateProductListing(lastUrl);
	});
	
	//Filter Min & Max Fields
	$('input[name="pmin"], input[name="pmax"]').on("keypress keyup blur",function (event) {    
        $(this).val($(this).val().replace(/[^\d].+/, ""));
         if ((event.which < 48 || event.which > 57) || $(this).val().length > 3) {
             event.preventDefault();
         }
     });
}

function formFullRefUrl() {
	var refForm = $('form[name="applyrefinements"]');
	var refFormUrl = $(refForm).attr('action');
	
	refFormUrl = util.appendParamToURL(refFormUrl, "cgid", "root");
	
	//Add customer zip-code to URL as parameter
	var customerZip = $("input[name='customerZip']").val();
	if(customerZip.length > 0) {
		refFormUrl = util.appendParamToURL(refFormUrl, "customerZip", customerZip);
	}
	
	//Retain Q parameter
	if($('.prdct-search').length > 0) {
		var query = $('.prdct-search').attr('data-q');
		if(query.length > 0) {
			refFormUrl = util.appendParamToURL(refFormUrl, "q", query);
		}
	}
	
	var prefn, prefv, counter=1;
	
	//Setting Up Selected Size
	prefn = 'size';//$('.selected-size option:selected').parent().attr('name');
	prefv = $('input[name="size-radio"]:checked').attr('id');
	if (typeof prefn != typeof undefined && typeof prefv != typeof undefined &&  prefn.length > 0 && prefv.length > 0) {
		refFormUrl = util.appendParamToURL(refFormUrl, "prefn" + counter, prefn);
		refFormUrl = util.appendParamToURL(refFormUrl, "prefv" + counter, prefv);
		counter++;
	}
	
	////Setting Up Selected Brands
	var selectedBrands = new Array();
	$(".multiselect #checkboxes input[type='checkbox']:checked").each(function(e){
		selectedBrands.push($(this).val());
	});
	prefn, prefv = null;
	prefn = $('#selected-brands').attr('name');
	prefv = selectedBrands.join('|');
	if(typeof prefn != typeof undefined && typeof prefv != typeof undefined && prefn.length > 0 && prefv.length > 0) {
		refFormUrl = util.appendParamToURL(refFormUrl, "prefn" + counter, prefn);
		refFormUrl = util.appendParamToURL(refFormUrl, "prefv" + counter, prefv);
		counter++;
	}
	
	//Setting Up Secondary Refinements
	$('div.refinement').each(function(){
		if($(this).hasClass('refinement-price')) {
			var pmin, pmax;
			if(!isNaN($("input[name='pmin']").val()) && !isNaN($("input[name='pmax']").val())) {
				pmin = $("input[name='pmin']").val();
				pmax = $("input[name='pmax']").val();
				if(parseInt(pmin) < parseInt(pmax)) {
					refFormUrl = util.appendParamToURL(refFormUrl, 'pmin', pmin);
					refFormUrl = util.appendParamToURL(refFormUrl, 'pmax', pmax);
				}
				else {
					//alert('Please Enter Valid Range');
					return false;
				}
			}
			else {
				var selectedPriceRange = $(this).find("ul li input[type='radio']:checked");
				if(selectedPriceRange.length > 0) {
					var range = $(selectedPriceRange).attr('value');
					if(range.length > 0) {
						var pmin = range.split('-')[0];
						var pmax = range.split('-')[1];
						refFormUrl = util.appendParamToURL(refFormUrl, 'pmin', pmin);
						refFormUrl = util.appendParamToURL(refFormUrl, 'pmax', pmax);
					}
				}
			}
		}
		else {
			var checkedRefs = $(this).find("ul li input[type='checkbox']:checked");
			var refName;
			selectedBrands = new Array();
			$(checkedRefs).each(function() {
				selectedBrands.push($(this).val());
				refName = $(checkedRefs).attr('name');
			});
			prefn, prefv = null;
			prefn = refName;
			prefv = selectedBrands.join('|');
			if(prefn && prefv) {
				refFormUrl = util.appendParamToURL(refFormUrl, "prefn" + counter, prefn);
				refFormUrl = util.appendParamToURL(refFormUrl, "prefv" + counter, prefv);
				counter++;
			}
		}
	});
	
	//add page type refinement for search page only.
	//refFormUrl = util.appendParamToURL(refFormUrl, "ns", "refinement");
	return refFormUrl;
}

function posRefEventInitializer() {
	//Bind Brands Count Event for Multiple Selects
	$('.size-options input[type="checkbox"]').each(function(e){
		$(this).on('change', function(){
			var selectedOptionsCount = $('.size-options input[type="checkbox"]:checked').length;
			$('select[name="manufacturerName"] > option:first-child').text("Brands (" + selectedOptionsCount + ")");
			var ischecked= $(this).is(':checked');
			if(ischecked) {
				$(this).parent().addClass('selected');
			}
			else {
				$(this).parent().removeClass('selected');
			}
	    });
	});
	
	//Bind Size Change
	$('input[name="size-radio"]').each(function(e){
		$(this).on('change', function(){
			$('select[name="size"] > option:first-child').text($(this).val());
			var ischecked = $(this).is(':checked');
			if(ischecked) {
				$('input[name="size-radio"]').parent().removeClass('rselected');
				$(this).parent().addClass('rselected');
			}
			else {
				$(this).parent().removeClass('rselected');
			}
	    });
	});
	
	//Clear Zip Code
	$(document).on('click', '#clear-zipcode', function (e) {
		$('input[name="customerZip"]').val('');
		var attr = $("input[name='customerZip']").attr('id');
    	if (typeof attr == typeof undefined || attr == false) {
    		$("input[name='customerZip']").attr("id", "zipcode-error");
    	}
	});
	
	//Close Refinements
	$(document).on('click', '#close-filter', function (e) {
		$('body').toggleClass('panel-open');
		var rPanel = $('#refinements-panel');
		$(rPanel).hide();
		$(rPanel).removeClass("visible");
	});
	
	//Clear All Secondary Filters
	$('a[name="reset-main-filters"]').click(function(e) {
		e.preventDefault();
		var resetUrl = $(this).attr('href');
		$('body').attr('data-httpurl', window.location.href);
		updateProductListing(resetUrl);
		closeOpenPanels();
	});
	
	//Clear All Secondary Filters
	$(document).on('click', '#remove-desktop-filters', function (e) {
		e.preventDefault();
		var refFormUrl = Urls.quickSearch;
		refFormUrl = util.appendParamToURL(refFormUrl, "cgid", "root");
		
		//Add customer zip-code to URL as parameter
		var customerZip = $("input[name='customerZip']").val();
		if(customerZip.length > 0) {
			refFormUrl = util.appendParamToURL(refFormUrl, "customerZip", customerZip);
		}
		
		var prefn, prefv, counter=1;
		
		//Setting Up Selected Size
		prefn = 'size';//$('.selected-size option:selected').parent().attr('name');
		prefv = $('input[name="size-radio"]:checked').attr('id');
		if(typeof prefn != typeof undefined && typeof prefv != typeof undefined && prefn.length > 0 && prefv.length > 0) {
			refFormUrl = util.appendParamToURL(refFormUrl, "prefn" + counter, prefn);
			refFormUrl = util.appendParamToURL(refFormUrl, "prefv" + counter, prefv);
			counter++;
		}
		
		////Setting Up Selected Brands
		var selectedBrands = new Array();
		$(".multiselect #checkboxes input[type='checkbox']:checked").each(function(e){
			selectedBrands.push($(this).val());
		});
		prefn, prefv = null;
		prefn = $('#selected-brands').attr('name');
		prefv = selectedBrands.join('|');
		if(typeof prefn != typeof undefined && typeof prefv != typeof undefined && prefn.length > 0 && prefv.length > 0) {
			refFormUrl = util.appendParamToURL(refFormUrl, "prefn" + counter, prefn);
			refFormUrl = util.appendParamToURL(refFormUrl, "prefv" + counter, prefv);
			counter++;
		}
		
		if(IsValidZipCode(customerZip)) {
	        $.ajax({
	            type: 'GET',
	            url: Urls.validateZipCode,
	            data: {zipCode:customerZip},
	            dataType: 'json',
	            success: function (data) {
	                if(data.success) {
	                	//var refUrl = formFullRefUrl();
	            		//window.location.href = refFormUrl;
	                	$('body').attr('data-httpurl', window.location.href);
	                	updateProductListing(refFormUrl);
	            		
	                	closeOpenPanels();
	                } else if (!data.success) {
	                	zipValidationAlert();
	                }
	            },
	            failure: function () {
	                // do something when ajax fails
	            	zipValidationAlert();
	            }
	        });
	    } else {
	    	zipValidationAlert();
	    }
		
	});
	
	//Custom Checkboxes Select Behaviour for li & spans
	$('#refinements-panel input[type="checkbox"]').each(function(e){
		$(this).on('change', function(){
			var ischecked= $(this).is(':checked');
			if(ischecked) {
				$(this).parent().addClass('selected');
			}
			else {
				$(this).parent().removeClass('selected');
			}
	    });
	});
	
	//Custom Radio (Price) Select Behaviour for li & spans
	$('#refinements-panel input[type="radio"]').each(function(e){
		$(this).on('change', function() {
			var ischecked = $(this).is(':checked');
			if(ischecked) {
				$('.refinement-price ul li').removeClass('selected');
				$(this).parent().addClass('selected');
			}
			else {
				$(this).parent().removeClass('selected');
			}
	    });
	});
}

function closeOpenPanels() {
	//Close Main Refinement Panel
	var rPanel = $('#refinements-panel');
	$(rPanel).hide();
	$(rPanel).removeClass("visible");
	
	//Close Size Drop down
	var sizeElement = $('.radios-list');
	if($(sizeElement).hasClass('visible')) {
		$(sizeElement).hide();
		$(sizeElement).removeClass("visible");
	}
	
	//Close Brand Drop down
	var brandElement = $('.checkboxes-list');
	if($(brandElement).hasClass('visible')) {
		$(brandElement).hide();
		$(brandElement).removeClass("visible");
	}
}

exports.init = function () {
	initializeRefinements();
	compareWidget.init();
	if (SitePreferences.LISTING_INFINITE_SCROLL) {
		$(window).on('scroll', infiniteScroll);
	}
	productTile.init();
	productTile.initComfortLevelTagging();
	truncate.init();
	initializeEvents();
	initContainerHeight();
	initRefinementButtons();
	initSubcategoryContent();
	var prevPageLoad = $("input[name='prevPageInfo']").val();
	var nextPageLoad = $("input[name='nextPageInfo']").val();
	$('link[name=prevPageInfo]').remove();
	$('link[name=nextPageInfo]').remove();
	if (prevPageLoad && prevPageLoad.length > 0) {
		$('head').append('<link name="prevPageInfo" rel="prev" href=' + prevPageLoad + '>');
	}
	if (nextPageLoad && nextPageLoad.length > 0) {
		$('head').append('<link name="nextPageInfo" rel="next" href=' + nextPageLoad + '>');
	}
};
jQuery(function () {
	initTabNav();
});

function IsValidZipCode(zip) {
    var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
    if (!isValid) {
        return false;
    } else {
        return true;
    }
}

// key handling
function initTabNav() {
	jQuery ('.menu-category').tabNav({
		items: 'li'
	});
}
/*
 * Accessible TAB navigation
 */
;(function ($) {
	var isWindowsPhone = /Windows Phone/.test(navigator.userAgent);
	var isTouchDevice = ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;

	$.fn.tabNav = function (opt) {
		var options = $.extend({
			hoverClass: 'hover',
			items: 'li',
			opener: '>a',
			delay: 10
		},opt);

		if (isWindowsPhone || isTouchDevice) {
			return this;
		}

		return this.each(function () {
			var nav = $(this), items = nav.find(options.items);

			items.each(function (index, navItem) {
				var item = $(this), navActive, touchNavActive;
				var anchor = $(this).children('a.has-sub-menu , a.no-sub-menu-items');
				var anchorSiblingDiv = anchor.next();
				var link = item.find(options.opener), timer;
				link.bind('focus', function () {
					navActive = nav.hasClass('js-nav-active');
					touchNavActive = window.TouchNav && TouchNav.isActiveOn(navItem);
					if (!navActive || touchNavActive) {
						initSimpleNav();
					}
					item.trigger(navActive && touchNavActive ? 'itemhover' : 'mouseenter');
				}).bind('blur', function () {
					item.trigger(navActive && touchNavActive ? 'itemleave' : 'mouseleave');
				});

				var initSimpleNav = function () {
					if (!initSimpleNav.done) {
						initSimpleNav.done = true;
						item.hover(function () {
							clearTimeout(timer);
							timer = setTimeout(function () {
							item.addClass('open');
							item.addClass(options.hoverClass);
							anchor.addClass('open');
							anchorSiblingDiv.css("display","block");
							}, options.delay);
						}, function () {
							clearTimeout(timer);
							timer = setTimeout(function () {
								item.removeClass('open');
								item.removeClass(options.hoverClass);
								anchor.removeClass('open');
								anchorSiblingDiv.css("display","none");
							}, options.delay);
						});
					}
				};
			});
		});
	};
}(jQuery));
/**
 * @private
 * @function
 * @description build quick search url
 */
function buildQuickSearchUrl(size, brand) {
	var actionURL = '';
    var baseUrl = document.location.origin + Urls.quickSearch;
    // append params to url on the base of size and brnds selection
    if (size && brand) {
    	actionURL = util.appendParamsToUrl(baseUrl, { 'cgid':'root', 'prefn1':'size', 'prefv1':size, 'prefn2':'manufacturerName', 'prefv2':brand});
    } else if(size) {
    	actionURL = util.appendParamsToUrl(baseUrl, { 'cgid':'root', 'prefn1':'size', 'prefv1':size});
    }
    return actionURL;
}
/**
 * @private
 * @function
 * @description Validate zip code from custom preferences for Alaska restrictiona and service as well.
 * @param {String} zipCode
 */
function validateZipCodeForQuickSearch(zipCode) {
	var elementToAppend = '<span id="zipcode-error" class="error"></span>';
    var successelementToAppend = '<span id="zipcode-success" class="success"></span>';
    var errorElement = $('#zipcode-error');
    var successElement = $('#zipcode-success');
    if(typeof zipCode!='undefined' && zipCode.length==5) {
    	if (SitePreferences.RestrictedZipCodes && SitePreferences.RestrictedZipCodes.indexOf(zipCode)!=-1) {
    		successElement.length > 0 ? successElement.remove() : null;
            if(!errorElement.length) {
                $('#quick-zip-code').after(elementToAppend);
                $('#quick-zip-code').addClass('error');
                $('#zipcode-error').text(Resources.QUICKSEARCHALASKAZIP_ERROR);
            } else {
            	$('#zipcode-error').text(Resources.QUICKSEARCHALASKAZIP_ERROR);
            }
    		return;
    	}
        $.ajax({
            type: 'GET',
            url: Urls.validateZipCode,
            data: {zipCode:zipCode},
            dataType: 'json',
            async: false,
            success: function (data) {
                if(data.success) {
	                    if (errorElement.length > 0) {
	                    	errorElement.remove();
		                    $('#quick-zip-code').removeClass('error');
	                    }
	                    if (!successElement.length) {
	                        $('#quick-zip-code').after(successelementToAppend);
                        }
                } else if (!data.success) {
                		successElement.length > 0 ? successElement.remove() : null;
                        if(!errorElement.length) {
                            $('#quick-zip-code').after(elementToAppend);
                            $('#quick-zip-code').addClass('error');
                            $('#zipcode-error').text(Resources.QUICKSEARCHZIP_ERROR);
                        } else {
                        	$('#zipcode-error').text(Resources.QUICKSEARCHZIP_ERROR);
                        }

                }
            },
            failure: function () {
                // do something when ajax fails
            }
        });
    }

}
$(window).load(function(){
	
	if ($('#quick-zip-code').length > 0) {
		$('#quick-zip-code').focus();
	}
	$('#quick-zip-code').focusout(function(){
	    var elementToAppend = '<span id="zipcode-error" class="error"></span>';
	    var successelementToAppend = '<span id="zipcode-success" class="success"></span>';
	    var errorElement = $('#zipcode-error');
	    var successElement = $('#zipcode-success');
	    var zipCode = $(this).val();
	    if(!IsValidZipCode(zipCode)) {
	    	successElement.length > 0 ? successElement.remove() : null;
	    	// hide suggestions if zip code is wrong
	    	$('.search-suggestion-wrapper').length > 0 ? $('.search-suggestion-wrapper').css('display','none') : null;
	        if (!errorElement.length) {
	            $('#quick-zip-code').after(elementToAppend);
	            $('#quick-zip-code').addClass('error');
	            $('#zipcode-error').text('Enter Valid Zip Code');
	        } else {
	        	$('#zipcode-error').text('Enter Valid Zip Code');
	        }

	    } else {
	        	if (SitePreferences.RestrictedZipCodes && SitePreferences.RestrictedZipCodes.indexOf(zipCode)!=-1) {
	        		successElement.length > 0 ? successElement.remove() : null;
	    	    	// hide suggestions if zip code is wrong
	    	    	$('.search-suggestion-wrapper').length > 0 ? $('.search-suggestion-wrapper').css('display','none') : null;
	                if(!errorElement.length) {
	                    $('#quick-zip-code').after(elementToAppend);
	                    $('#quick-zip-code').addClass('error');
	                    $('#zipcode-error').text(Resources.QUICKSEARCHALASKAZIP_ERROR);
	                } else {
	                	$('#zipcode-error').text(Resources.QUICKSEARCHALASKAZIP_ERROR);
	                }
	        		return;
	        	}
	            $.ajax({
	                type: 'GET',
	                url: Urls.validateZipCode,
	                data: {zipCode:zipCode},
	                dataType: 'json',
	                async: false,
	                success: function (data) {
	                    if(data.success) {
	    	                    if (errorElement.length > 0) {
	    	                    	errorElement.remove();
	    		                    $('#quick-zip-code').removeClass('error');
	    	                    }
	    	                    if (!successElement.length) {
	    	                        $('#quick-zip-code').after(successelementToAppend);
	                            }
	                    } else if (!data.success) {
	                    		successElement.length > 0 ? successElement.remove() : null;
	                	    	// hide suggestions if zip code is wrong
	                	    	$('.search-suggestion-wrapper').length > 0 ? $('.search-suggestion-wrapper').css('display','none') : null;
	                            if(!errorElement.length) {
	                                $('#quick-zip-code').after(elementToAppend);
	                                $('#quick-zip-code').addClass('error');
	                                $('#zipcode-error').text(Resources.QUICKSEARCHZIP_ERROR);
	                            } else {
	                            	$('#zipcode-error').text(Resources.QUICKSEARCHZIP_ERROR);
	                            }

	                    }
	                },
	                failure: function () {
	                    // do something when ajax fails
	                }
	            });
	    }
	});
});
$(document).ready(function(){
	
	// Zip code front end validation on page load
	if ($('#quick-zip-code').length > 0) {
		$('#quick-zip-code').focus();
		//validateZipCodeForQuickSearch($('#quick-zip-code').val());
	}
	
	// on page load check if queen exist in sizes then select it default
	if (!$('#selectedSize').val() && $('[data-value=Queen]')) {
		$('[data-value=Queen]').addClass('selected');
		$('#selectedSize').val($('[data-value=Queen]').data('value'));
	    var size = $('#selectedSize').val();
    	var actionURL = buildQuickSearchUrl(size);
	    // set url in form action not in use but may be we use action for form submit
	    $("#quick-search-form").attr('action', actionURL);
	}
	// if no result found after search then show popup
	if ($('.no-result-found').length > 0) {
		var uri = window.location.toString();
		if (uri.indexOf("?") > 0) {
		    var clean_uri = uri.substring(0, uri.indexOf("?"));
		    window.history.replaceState({}, document.title, clean_uri);
		}
		var block = $('.no-result-found');	
		if (block.size() > 0) {
			block.removeClass('do-not-show');
			dialog.open({
				html: block,
				options: {
					dialogClass: 'no-result-found-dialog no-close',
					buttons: [{
						text: 'OK',
						click: function () {
							$(this).dialog('close');
						}
					}]
				}
			});
			$('.ui-dialog-title').remove();
		}
	}
	
	
	// Zip code front end validation for Quick Inventory search
	$('#quick-zip-code').on("keypress keyup",function(event){
		var allowedKeyCodes = [8,48,49,50,51,52,53,54,55,56,57];
		// check if text is selected then allow the input and restrict others. else check other critera and prevent default
		if (window.getSelection().toString() && allowedKeyCodes.indexOf(event.which)!=-1) {
			return true;
		} else if (allowedKeyCodes.indexOf(event.which)==-1 || $(this).val().length >= 5) {
		event.preventDefault();
		return false;
		}
	});
	// Zip Back end validation for PLP
	$("input[name='customerZip']").on("keypress keyup",function(){
		var allowedKeyCodes = [8,48,49,50,51,52,53,54,55,56,57];
		// check if text is selected then allow the input and restrict others. else check other critera and prevent default
		if (window.getSelection().toString() && allowedKeyCodes.indexOf(event.which)!=-1) {
			return true;
		} else if (allowedKeyCodes.indexOf(event.which)==-1 || $(this).val().length >= 5) {
		event.preventDefault();
		return false;
		}
	});
	// Handle zip code client side validation on Product Landing Page
	$("input[name='customerZip']").focusout(function(){
	    var zipCode = $(this).val();
    	var attr = $("input[name='customerZip']").attr('id');
	    if(!IsValidZipCode(zipCode)) {
        	if (typeof attr == typeof undefined || attr == false) {
        		$("input[name='customerZip']").attr("id", "zipcode-error");
    	    	// hide suggestions if zip code is wrong
    	    	$('.search-suggestion-wrapper').length > 0 ? $('.search-suggestion-wrapper').css('display','none') : null;
        	}
	    } else {
	        	if (SitePreferences.RestrictedZipCodes && SitePreferences.RestrictedZipCodes.indexOf(zipCode)!=-1) {
	    	    	// hide suggestions if zip code is wrong
	    	    	$('.search-suggestion-wrapper').length > 0 ? $('.search-suggestion-wrapper').css('display','none') : null;
	        		if (typeof attr == typeof undefined || attr == false) {
	            		$("input[name='customerZip']").attr("id", "zipcode-error");
	            	}
		    		return;
	        	}
	            $.ajax({
	                type: 'GET',
	                url: Urls.validateZipCode,
	                data: {zipCode:zipCode},
	                dataType: 'json',
	                async: false,
	                success: function (data) {
	                    if(data.success) {
	                    	var attr = $("input[name='customerZip']").attr('id');
		                	if (typeof attr !== typeof undefined && attr !== false) {
		                		$("input[name='customerZip']").removeAttr("id");
		                		//$("input[name='customerZip']").hasClass('restricted-zip') ? $("input[name='customerZip']").removeClass('restricted-zip'): null;
		                	}
	                    } else if (!data.success) {
	            	    	// hide suggestions if zip code is wrong
	            	    	$('.search-suggestion-wrapper').length > 0 ? $('.search-suggestion-wrapper').css('display','none') : null;
	                    	var attr = $("input[name='customerZip']").attr('id');
		                	if (typeof attr == typeof undefined || attr == false) {
		                		$("input[name='customerZip']").attr("id", "zipcode-error");
		        	    		// put class restricted-zip if service return false
		                		//!$("input[name='customerZip']").hasClass('restricted-zip') ? $("input[name='customerZip']").addClass('restricted-zip') : null;
		                	}
	                    }
	                },
	                failure: function () {
	                    // do something when ajax fails
	                }
	            });
	    }
	});
	// Add and Remove class on size and brand selection to gray out them when keyword search get focusin and out.
	$(".inventory-search-result input[name='q']").focus(function(){
		$('.brands-sizes').addClass('disable-brand-size');
	});
	$(".inventory-search-result input[name='q']").focusout(function(){
		$('.brands-sizes').removeClass('disable-brand-size');
	});
	// get brands and sizes values on selection and build url for search here.
	$(".quick-brands label,.quick-sizes label").on('click',function() {
	    var brands, size = '';
	    var searchUrl = $('#quick-inventry-search').val();
	    var decide = this.className;
	    if(decide=='sizes') {
	    	$(".quick-sizes label").removeClass("selected");
	    	$(this).addClass('selected');
	        size = $(this).data('value');
	        $('#selectedSize').val(size);
	    } else if (decide.indexOf('search-brand')!=-1) {
	        brands = $('#selectedBrands').val();
	        var brand = $(this).data('value');
            if (brands.indexOf(brand)==-1) {
            	$(this).addClass('selected');
            	brands+= brand + '|';
            } else {
                brands = brands.replace($(this).data('value') + '|', "");
                $(this).removeClass('selected');
            }
            $('#selectedBrands').val(brands);
	    }

	    var actionURL = buildQuickSearchUrl($('#selectedSize').val(),$('#selectedBrands').val());
	    // set url in form action not in use but may be we use action for form submit
	    $("#quick-search-form").attr('action', actionURL);
	  });
	// Handle brands and size selection search submit here.
	$("#quick-inventry-search").on('click',function(e) {
	    if (!$('#selectedSize').val() || $('#zipcode-error').length > 0 || !$('#quick-zip-code').val()) {
	    	e.preventDefault();
	    	$('html,body').scrollTop(0);
	    	$('#quick-zip-code').focus();
	    	return;
	    }
	    window.location.href =  $("#quick-search-form").attr('action');
	    //$("#quick-search-form").submit();
	  });
	// Handle Search Pharase Submission
	$('#btn-quick-search-suggestions').click(function(e){
		e.preventDefault();
		// handle suggestion submit button from quick inventory
		if($('#quick-zip-code').length > 0 && ($('#zipcode-error').length > 0 || !$('#quick-zip-code').val() ||
			($("#quick-search-suggestions-form input[name='q']").val()==='Search by product name' ||
			$("#quick-search-suggestions-form input[name='q']").val()==='Search'))) {
			$('#zipcode-error').length > 0 ? $('#quick-zip-code').focus() : null;
			return;
		}
		// handle suggestions from Product Landing Page.
		// "Search by product name" and "Search" are same as null these are search field default values. Placeholder becomes default value.
		if($("input[name='customerZip']").length > 0 && ($('#zipcode-error').length > 0 || !$("input[name='customerZip']").val() ||
				($("#quick-search-suggestions-form input[name='q']").val()==='Search by product name' ||
				$("#quick-search-suggestions-form input[name='q']").val()==='Search'))) {
				$('#zipcode-error').length > 0  ? zipValidationAlert() : null;
				return;
			}
		$("#quick-search-suggestions-form").submit();
	});
});


$('.multiselect input:checkbox').change(function(){
   if($(this).is(':checked')) 
       $(this).parent().addClass('selected'); 
  else 
      $(this).parent().removeClass('selected')
 });


$('.mattress-select input:radio').click(function(){
    $(this).parent().addClass('rselected').siblings().removeClass('rselected');
 });


$('.refinement input:checkbox').change(function(){
   if($(this).is(':checked')) 
       $(this).parent().addClass('selected'); 
  else 
      $(this).parent().removeClass('selected')
 });


$('.refinement input:radio').click(function(){
    $(this).parent().addClass('rselected').siblings().removeClass('rselected');
 });

// sticky filters
$(window).scroll(function() {    
    var scroll = $(window).scrollTop();
    if (scroll >= 100) {
        $(".sticky-head").addClass("sticky");
    } else {
        $(".sticky-head").removeClass("sticky");
    }
});