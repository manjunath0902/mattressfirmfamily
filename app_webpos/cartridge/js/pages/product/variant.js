'use strict';

var amplience = require('../../amplience'),
	ajax = require('../../ajax'),
	image = require('./image'),
	progress = require('../../progress'),
	productStoreInventory = require('../../storeinventory/product'),
	tooltip = require('../../tooltip'),
	util = require('../../util'),
	updatePDPPrice = require('./updatePDPPrice'),
	dialog = require('../../dialog');

var selectedQty = 1;

var isParamsValid = function (href) {
    var vars = [], hash;
    var hashes = href.slice(href.indexOf('?') + 1).split('&');
    var isValid = true;
    if(hashes != null && hashes.length > 0) {
	    for(var i = 0; i < hashes.length; i++) {
	        hash = hashes[i].split('=');
	        if(hash != null && hash[1] == "") {             
	            isValid = false;
	            break;
	        }
	    }
    }
    return isValid;
}

/**
 * @description update product content with new variant from href, load new content to #product-content panel
 * @param {String} href - url of the new product variant
 **/
var updateContent = function (href, ampset) {
	
    var isParamValid = isParamsValid(href);

	if (ampset == null) {
		var ampset = '';
		}
	var $pdpForm = $('.pdpForm');
	//var qty = $pdpForm.find('input[name="Quantity"]').first().val();
	// Preserve selected quantity MAT-1430	PDP - Quantity reverts back to '1' if Size is changed.
	var qtyAvailable = $pdpForm.find('#Quantity');
	if (qtyAvailable.length > 0 ) { 
	    var qty = (qtyAvailable).first().val(); //$pdpForm.find('#Quantity').first().val();
		selectedQty = isNaN(qty) ? '1' : qty;
	} 
	var params = {
		Quantity: isNaN(qty) ? '1' : qty,
		format: 'ajax',
		productlistid: $pdpForm.find('input[name="productlistid"]').first().val()
	}; 

	
    ajax.load({
        url: util.appendParamsToUrl(href, params),
        target: $('#product-content'),
        callback: function () {
            if (SitePreferences.STORE_PICKUP) {
                productStoreInventory.init();
			}
            if (ampset != '') {
            	image.replaceImages();
            	amplience.initZoomViewer(ampset);
            }
            tooltip.init();
            util.uniform();
            updateLabel();
            $(document).trigger("pdpContentUpdateEvent");
            focusOnPDPdropDown();
            updatePDPPrice();
            progress.hide($('#pdpMain'));                
            loadATPAvailabilityMessage();
            $(".select-variation-dropdown").prop('disabled', false);
            $("#lockDropdwon").val("false");
            $("#add-to-cart").removeClass('disabled-btn');
            selectSize();
            
            if($(".product-financing-custom").length == 2) {
            	   $(".product-financing-custom.no-credit").addClass('allignTop');
            }
        }
    });
    if(isParamValid != true){
        $("#add-to-cart").prop('disabled', true);
        if ($('.ui-tooltip').length > 1) {
    		$('.ui-tooltip').hide();
    	}
	}        	
};
if(SitePreferences.CURRENT_SITE === "1800Mattress-RV"){
	if (util.isMobileSize()) {
		if ($('.product-key-features').length > 0) {
			$('.product-key-features').hide();
			$('.product-key-features').insertBefore('.pdp-comfort-scale-container').css("display", "block");
		}
		if ($('.uptopercent').length > 0) {
			$('.uptopercent').hide();
			$('.uptopercent').insertAfter('.top-sec').css("display", "block");
		}
	}
}
function loadATPAvailabilityMessage() {
	$(".availability-web").append('<div class="loader-indicator-atp"></div>');
	var pid = $('#pid').val();
	var url = util.appendParamsToUrl(Urls.getATPAvailabilityMessageAjax, {productId: pid,qty: 1, format: 'ajax'});
	var productShippingInformation = $("#product-content").attr("data-product-shipping-information");
	var customerZipCode = $("#product-content").attr("data-customer-zipcode");
	var hasDisabledClass = $("#add-to-cart").hasClass("add-to-cart-disabled");
	if (productShippingInformation && productShippingInformation == "core" && hasDisabledClass == false){
		$.getJSON(url, function (data) {
			var fail = false;
			var msg = '';
			if (!data) {
				msg = Resources.BAD_RESPONSE;
				fail = true;
			} else if (!data.success) {
				fail = true;
			}
			if (fail) {
				// trigger tealium tag when ATP fails
				/*if(productShippingInformation && customerZipCode && productShippingInformation == "core") {
					utag.link({ "eventCategory" : ["ATP"], "eventLabel" : ["PDP_" + pid + "_" + customerZipCode], "eventAction" : ["fail"] });
				}*/
				
				//show default ATP message
				if ($('#replaceMeID') != null && $('#replaceMeID').length) {
					$("#replaceMeID").parent().remove();
					getDefaultAvailabilityMessageAjax(pid);
				}
				//show default ATP message AB
				if ($('#replaceMeID_ab') != null && $('#replaceMeID_ab').length) {
					$("#replaceMeID_ab").parent().remove();
					getDefaultAvailabilityMessageAjax(pid);
				}
				
				$(".availability-web .loader-indicator-atp").remove();
				//$error.html(msg);
				return;
			}
			if (data.success) {
				if ($('#replaceMeID') != null && $('#replaceMeID').length) {
					if (data.availabilityClass) {
						$("#replaceMeID").removeClass();
						$('#replaceMeID').addClass(data.availabilityClass);
					}
				}
				
				if ($('#replaceMeID_ab') != null && $('#replaceMeID_ab').length) {
					if (data.availabilityClass) {
						$("#replaceMeID_ab").removeClass();
						$('#replaceMeID_ab').addClass(data.availabilityClass);
					}
				}
				if (data.showATPMessage) { /* !pdict.Product.master 
											&& !pdict.Product.variationGroup
											&& pdict.Product.custom.shippingInformation.toLowerCase() == 'core' 
											&& dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpAvailabilityMessage') 
											&& session.custom.customerZip} */
					if (data.availabilityDate != null && data.isInStock === true) {
						var availabilitySuccess = Resources.ATP_AVAILABILITY_SUCCESS;
						var availabilitySuccess_ab = Resources.ATP_AVAILABILITY_SUCCESS_AB;
						var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
						
						//2018-04-05T00:00:00.000Z
						var dateTime = data.availabilityDate;
						var dateOnly = dateTime.split('T');
						var ymd = dateOnly[0].split('-');
						var date = ymd[2];
						var month = months[Number(ymd[1])-1];
						var availabilityDateString = month + ' ' + date;
						if (data.dateDifferenceString != '') {
							availabilitySuccess = availabilitySuccess.replace("tempDate", data.dateDifferenceString + ' ' + availabilityDateString);
							availabilitySuccess_ab = availabilitySuccess_ab.replace("tempDate", data.dateDifferenceString + ' ' + availabilityDateString);
						} else {
							availabilitySuccess = availabilitySuccess.replace("tempDate", availabilityDateString);
							availabilitySuccess_ab = availabilitySuccess_ab.replace("tempDate", availabilityDateString);
						}
						if($('#replaceMeID') != null){
							$('#replaceMeID').find('p').text(availabilitySuccess);							
						}
						if($('#replaceMeID_ab') != null) {							
							$('#replaceMeID_ab').find('p').text(availabilitySuccess_ab);
						}
						
						
						if (data.detailsContentId != '') {
							var anchorDetails;
							if($('#replaceMeID') != null) {
								anchorDetails = $('#replaceMeID').find('a');
							}
							
							if($('#replaceMeID_ab') != null) {
								anchorDetails = $('#replaceMeID_ab').find('a');
							}
							
							if ($('#replaceMeID') != null && anchorDetails.length == 0) {
								$("#replaceMeID").find('p').after("<a></a>");
							}
							
							if ($('#replaceMeID_ab') != null && anchorDetails.length == 0) {
								$("#replaceMeID_ab").find('p').after("<a></a>");
							}
							if ($('#replaceMeID') != null) {
								anchorDetails = $('#replaceMeID').find('a');
							}
							if ($('#replaceMeID_ab') != null) {
								anchorDetails = $('#replaceMeID_ab').find('a');
							}
							
							var hrefDetails = util.appendParamsToUrl(Urls.pageShow, {cid: data.detailsContentId});
							anchorDetails.attr("href", hrefDetails);
							anchorDetails.addClass('details');
							anchorDetails.prop('title', 'details');
							anchorDetails.text('Details');
						}
					} else if ($('#replaceMeID') != null && $('#replaceMeID').length) {
						$("#replaceMeID").parent().remove();
						getDefaultAvailabilityMessageAjax(pid);
					} else if ($('#replaceMeID_ab') != null && $('#replaceMeID_ab').length) {
						$("#replaceMeID_ab").parent().remove();
						getDefaultAvailabilityMessageAjax(pid);
					}
				} else if ($('#replaceMeID') != null && $('#replaceMeID').length) {
					$("#replaceMeID").parent().remove();
					getDefaultAvailabilityMessageAjax(pid);
				}
				else if ($('#replaceMeID_ab') != null && $('#replaceMeID_ab').length) {
					$("#replaceMeID_ab").parent().remove();
					getDefaultAvailabilityMessageAjax(pid);
				}
				$(".availability-web .loader-indicator-atp").remove();
			}
		});
	} else {		
		$(".availability-web .loader-indicator-atp").remove();
	}
	//Replace with the selected quantity after size change MAT-1430	PDP - Quantity reverts back to '1' if Size is changed.
	   $('#Quantity').val(selectedQty);
}

function getDefaultAvailabilityMessageAjax(pid) {
	var availabilityMessageSection = $('.availability-web');
	var url = util.appendParamsToUrl(Urls.getDefaultAvailabilityMessage, {productId: pid});
	availabilityMessageSection.load(url, function () {
	});
}

function focusOnPDPdropDown() {
	$(".select-variation-dropdown").focus();
}

function constructAmpset () {
    var color_masterid = $('.swatches.color').find(':selected').data('masterid');
    var size_masterid = $('.swatches.size').find(':selected').data('masterid');
    var team_masterid = $('.swatches.variation-select').find(':selected').data('masterid');

    var color_ampset = $('.swatches.color').find(':selected').data('ampset');
    var size_ampset = $('.swatches.size').find(':selected').data('ampset');
    var team_ampset = $('.swatches.variation-select').find(':selected').data('ampset');
    
    var ampset = "";
    var masterid = (size_masterid != undefined) ? size_masterid : ((color_masterid != undefined) ? color_masterid : ((team_masterid != undefined) ? team_masterid : ''));

    ampset = ((size_ampset != undefined) ? size_ampset : '') + ((color_ampset != undefined) ? color_ampset : '') + ((team_ampset != undefined) ? team_ampset : '');
    
    if (ampset != ""){
        ampset = masterid + '_' + ampset + '_MSET';
    } else {
        ampset = "";
    }
    return ampset;
}

function constructAmpsetForCustomDD () {
    var color_masterid = $('.swatches.color').find(':selected').data('masterid');
    var size_masterid = $('.select-item.selected').attr("data-masterid");
    var team_masterid = $('.swatches.variation-select').find(':selected').data('masterid');

    var color_ampset = $('.swatches.color').find(':selected').data('ampset');
    var size_ampset = $('.select-item.selected').attr("data-ampset");
    var team_ampset = $('.swatches.variation-select').find(':selected').data('ampset');
    
    var ampset = "";
    var masterid = (size_masterid != undefined) ? size_masterid : ((color_masterid != undefined) ? color_masterid : ((team_masterid != undefined) ? team_masterid : ''));

    ampset = ((size_ampset != undefined) ? size_ampset : '') + ((color_ampset != undefined) ? color_ampset : '') + ((team_ampset != undefined) ? team_ampset : '');
    
    if (ampset != ""){
        ampset = masterid + '_' + ampset + '_MSET';
    } else {
        ampset = "";
    }
    return ampset;
}

var updateLabel = function () {
	$('.product-variations .attribute').each(function (e) {
		var attributeTitle = $(this).find('.swatches').find(':selected').attr('title');
		if ($(this).find('.selected-attr-value').length == 0 && attributeTitle != '') {
			var attributeValue = '<span class="selected-attr-value">' + attributeTitle + "</span>";
			$(attributeValue).appendTo($(this).find('.label'));
		}
	});
}

module.exports = function () {
	var $pdpMain = $('#pdpMain');
	
	// hover on swatch - should update main image with swatch image
	$pdpMain.on('mouseenter mouseleave', '.swatchanchor', function () {
		var largeImg = $(this).data('lgimg'),
			$imgZoom = $pdpMain.find('.main-image'),
			$mainImage = $pdpMain.find('.primary-image');

		if (!largeImg) { return; }
		// store the old data from main image for mouseleave handler
		$(this).data('lgimg', {
			hires: $imgZoom.attr('href'),
			url: $mainImage.attr('src'),
			alt: $mainImage.attr('alt'),
			title: $mainImage.attr('title')
		});
		// set the main image
		image.setMainImage(largeImg);
	});

	
	// click on swatch - should replace product content with new variant
	$pdpMain.on('change', '.swatches', function (e) {            
		$(".select-variation-dropdown").prop('disabled', true);       
		//Amplience image logic - if size is clicked, do nothing.  If color is clicked and "selected," Look for color.
		//Note - Amplience breaks if the image doesn't exist.  They have no "size" based images, it would seem.  So, if size is clicked, then do nothing.
		if ($(this).hasClass("color") || $(this).hasClass("size") || $(this).hasClass("variation-select")) {
			//If the swatch that was clicked has the class "selected" at this point, the swatch is in process of being deselected.
			if ($(this).hasClass("selected")) {
				var ampset = "";
			} else {
                ampset = constructAmpset();
			}
		}
		if($(this).hasClass("variation-select")){
			if(ampset != undefined && ampset != null) {
				updateContent($(this).val(), ampset);
			}
		}
		else{
			updateContent($(this).val(), ampset);
		}
		
		 if ( $(this).hasClass("variation-select") && (ampset == undefined || ampset == null || ampset == "")) {
             $("#add-to-cart").prop('disabled', true);
         }
	});
	
	$pdpMain.on('click', '.li-select', function (e) {				
		$('.fake-input').html('');
		if($("#lockDropdwon").val() === "true") {
			return;
		}
		
		$("#lockDropdwon").val("true");
		
		if( (!$(this).hasClass('select-item') && $(this).hasClass('selected')) || $(this).hasClass('out-of-stock') || $(this).data('url').length === 0)
		{
			$(".tooltip-part").removeClass('active-tooltip');	
			e.stopPropagation();
			return;
		}
		    
		//Amplience image logic - if size is clicked, do nothing.  If color is clicked and "selected," Look for color.
		//Note - Amplience breaks if the image doesn't exist.  They have no "size" based images, it would seem.  So, if size is clicked, then do nothing.
		if ($(this).hasClass("select-item li-select")) {
			//If the swatch that was clicked has the class "selected" at this point, the swatch is in process of being deselected.
			/*if ($(this).hasClass("selected")) {
				var ampset = "";
			} else {
               ampset = constructAmpsetForCustomDD();
			}*/
			
			var ampset = constructAmpsetForCustomDD();
		}
		if($(this).hasClass("variation-select")){
			if(ampset != undefined && ampset != null) {
				updateContent($(this).data('url'), ampset);
			}
		}
		else{
			updateContent($(this).data('url'), ampset);
		}
		
		 if ( $(this).hasClass("variation-select") && (ampset == undefined || ampset == null || ampset == "")) {
            $("#add-to-cart").prop('disabled', true);
        }
	});
	
	$(document).ready(function() {	
		var variantDropDowns= $('.select-variation-dropdown option:selected');
		if(variantDropDowns != null && variantDropDowns.length > 0) {
	        for (var i = 0; i < variantDropDowns.length; i++) {                  
	           if ($(variantDropDowns[i]).text().indexOf("Select") > -1) {
	               $("#add-to-cart").prop('disabled', true);
	           }
	        }
		}
	
        //
		const urlParams = new URLSearchParams(window.location.search);
		const myParam = urlParams.get('v1');
		//		
     
        if ($('.swatches.color').hasClass("selected") || $('.swatches.size').hasClass("selected") || $('.swatches.variation-select').hasClass("selected")) {
			var ampset = "";
		} else {
			//The swatch is being selected at this point, so look for that color-based image.
            var ampset = constructAmpset();
            if (window.isMattressProductPage) { 
                var ampset = constructAmpsetForCustomDD();
            } else {
              var ampset = ""; //constructAmpset();
            }                                        
		}
        if (ampset != '') {
			image.replaceImages();
			amplience.initZoomViewer(ampset);
		}
	});

	if ($('.defaultselect').length  > 0) {          
        $(".select-variation-dropdown").prop('disabled', true);          
        var selectOption = $('.defaultselect');
        selectOption.addClass('selected');
        selectOption.attr('selected','selected');          
        updateContent(selectOption.val(), "");
    }
	
	// change drop down variation attribute - should replace product content with new variant
	//$pdpMain.on('change', '.variation-select', function () {
		//if ($(this).val().length === 0) { return; }
		//updateContent($(this).val(),"");
	//});
	util.uniform();
	updateLabel();
	loadATPAvailabilityMessage();
	selectSize();
};

var selectSize = function () {
	  // show selected values
	  $( ".selectedValue" ).each(function() {
		  var $value =  $(this).closest('.size-select-area').find('li.selected').attr('title');
		  var $dimension = $(this).closest('.size-select-area').find('li.selected').children('.size-dimension').val();
		  if($value != null && $value != undefined)
	       {
			  $(this).val($value);  
	       }
		  if($dimension != null && $dimension != undefined)
	       {
			  $(this).next('.selected-size-dimension').html($dimension);
	       }	
		   	 
	  });	
	$('.size-select-area .fake-input').unbind("click");
	$('.size-select-area .fake-input').click(function(e){
		if($("#lockDropdwon").val() === "true") {
			return;
		}
		var $target =  $(this).closest('.size-select-area');
		var hasClass =  $target.hasClass('active-drop');
	    $( ".size-select-area").removeClass('active-drop');
	    if(hasClass)
	    {
	    	$target.removeClass('active-drop');
	    }
	    else
	    {
	    	$target.addClass('active-drop');
	    }
	    e.stopPropagation();
	});
	
	$(document).on("click", function(e) {
		// hide size/comfort tool tip on click outside
	    if ($(e.target).is(".size-select-area") === false) {
	    	$(".size-select-area").removeClass('active-drop');
	    }
	 // hide comfort tool tip on click outside of button
	  //  if ($(e.target).is(".add-to-cart-container") === false) {
	   // 	$(".add-to-cart-area").removeClass('comfort-not-select');
	  //  }
	 });
	$(document).mouseup(function(e) {
		    var hidedrop = $(".add-to-cart-container");
		    // if the target of the click isn't the container nor a descendant of the container
		    if (!hidedrop.is(e.target) && hidedrop.has(e.target).length === 0) 
		    {
		        $(".add-to-cart-area").removeClass('comfort-not-select');
		    }
	});
	$(".variation-select").off("click", "li.select-item");
	$(".variation-select").on("click", "li.select-item", function(e){
	    e.preventDefault();
	    if($(this).hasClass('selected') || $(this).hasClass('out-of-stock')){ $(".size-select-area").removeClass('active-drop');e.stopPropagation();return;}	
	    $(this).addClass("selected").siblings().removeClass("selected");
	    $(this).closest('.size-select-area').find(".selectedValue").val($(this).attr('title'));
	    $(this).closest('.size-select-area').removeClass('active-drop');
	    $(".tooltip-part").removeClass('active-tooltip');
	});
	
	 $('#btn-minus-qty').click(function(e){
	        // Stop acting like a button
	        e.preventDefault();        
	        var currentVal=parseInt($('#Quantity').val());
	        if(currentVal==1)
	        	return;
	        if (!isNaN(currentVal)) {
	            // Increment
	            $('#Quantity').val(currentVal - 1);
	        } else {
	            // Otherwise put a 0 there
	            $('#Quantity').val(0);
	        }
	    });
	    
	    $('#btn-plus-qty').click(function(e){
	        // Stop acting like a button
	        e.preventDefault();        
	        var currentVal=parseInt($('#Quantity').val());
	        if(currentVal==20)
	        	return;
	        if (!isNaN(currentVal)) {
	            // Increment
	            $('#Quantity').val(currentVal + 1);
	        } else {
	            // Otherwise put a 0 there
	            $('#Quantity').val(0);
	        }
	    });
	    
	    $('.primary-content').on('click', '.pdp-section .amp-zoom-overflow',function(e){
	    	 e.preventDefault(); 
	         var pid = $('#pdp-zoom-product').val();
	         var url = Urls.pdpImageZoom;
	         url = util.appendParamToURL(url, 'pid', pid);
	         dialog.open({
	             url: url,
	             options: {
	                 width: '100%',
	                 height: 'auto',
	                 dialogClass: 'PDP-product-zoom'
	                 
	             },
	             callback: function () {
	            	 amplience.initZoomViewer('');
	             }
	         });
	    });
};
