'use strict';

var dialog = require('../../dialog'),
	minicart = require('../../minicart'),
	page = require('../../page'),
	util = require('../../util'),
	TPromise = require('promise'),
	_ = require('lodash'),
	addToCartIntercept = require('./addToCartIntercept');

/**
 * @description Make the AJAX request to add an item to cart
 * @param {Element} form The form element that contains the item quantity and ID data
 * @returns {Promise}
 */
var addItemToCart = function (form) {
	var $form = $(form),
		$qty = $form.find('input[name="Quantity"]');
	if ($qty.length === 0 || isNaN($qty.val()) || parseInt($qty.val(), 10) === 0) {
		$qty.val('1');
	}
	return TPromise.resolve($.ajax({
		type: 'POST',
		url: util.ajaxUrl(Urls.addProduct),
		data: $form.serialize()
	}));
};

/**
 * @description Handler to handle the add to cart event
 */
var addToCart = function (e) {
	e.preventDefault();
	var $form = $(this).closest('form');
	var _zip = $.trim($('#pdp-delivery-zip').val());
	var _optionname ="";
	var _optionid ="";
	var _optionPrice="";
	if($form.find( ':input[name*="_boxSpring"]' ).length > 0)
	{
		 _optionname = $form.find( ':input[name*="_boxSpring"] option:selected' ).data('option-product-name');
		if(_optionname != "none"){
			 _optionid = $form.find( ':input[name*="_boxSpring"] option:selected' ).val();
			 _optionPrice = $form.find( ':input[name*="_boxSpring"] option:selected' ).data('option-price');
		}
		else{
			 _optionname = "";
			}
		if(_optionname != "none" && _optionid !="" && _optionPrice !=""){
		
/*			utag.link({
			    "tealium_event": "cart_add",
			    "option_product_name": _optionname,
			    "option_product_value_id": _optionid,
			    "option_product_price": _optionPrice
			});	*/
		}
	}
	
	
	// 1. Verify a valid US 5-digit ZIP first.
//	if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(_zip)) { // MAT-137 Remove Zip Code constraint on Add to Cart
		addItemToCart($form).then(function (response) {
			var $uuid = $form.find('input[name="uuid"]');
			if ($uuid.length > 0 && $uuid.val().length > 0) {
				page.refresh();
			} else {
				// do not close quickview if adding individual item that is part of product set
				// @TODO should notify the user some other way that the add action has completed successfully
				if (!$(this).hasClass('sub-product-item')) {
					dialog.close();
				}

				minicart.show(response);
				addToCartIntercept.show();
				if($('.cart-total').length == 1) {
					$('.cart-total').text($($('.mini-cart-subtotals').children('.value')[0]).text());
				}
			}
		}.bind(this));
//	} else { // MAT-137 Remove Zip Code constraint on Add to Cart
	// Show message to set ZIP Code
//		$('.notification-wrapper.empty-zip-code').addClass('active');
//		$('html, body').animate({
//			scrollTop: $('.pdp-delivery-wrapper').offset().top - 50
//		});

//		return false;
//	}
};

/**
 * @description Make the AJAX request to add an Personali item to cart
 * @param {Element} form The form element that contains the item quantity and ID data
 * @returns {Promise}
 */

var addPersonaliItemToCart = function (form, netotiate_offer_id) {
	var $form = $(form),
		$qty = $form.find('input[name="Quantity"]'),
		$addProductURL = util.ajaxUrl(Urls.addProduct);
	if ($qty.length === 0 || isNaN($qty.val()) || parseInt($qty.val(), 10) === 0) {
		$qty.val('1');
	}
	if (netotiate_offer_id != null && netotiate_offer_id != "") {
		$addProductURL = $addProductURL + "&netotiate_offer_id=" + netotiate_offer_id;
	}
	return TPromise.resolve($.ajax({
		type: 'POST',
		url: $addProductURL,
		data: $form.serialize()
	}));
};

/**
 * @description Handler to handle Personali Integration add to cart event
 */
var addPersonaliToCart = function (netotiate_offer_id) {
	var $form = $('.add-to-cart').closest('form');	//	'.add-to-cart'
	addPersonaliItemToCart($form, netotiate_offer_id).then(function (response) {
		var $uuid = $form.find('input[name="uuid"]');
		if ($uuid.length > 0 && $uuid.val().length > 0) {
			page.refresh();
		} else {
			// do not close quickview if adding individual item that is part of product set
			// @TODO should notify the user some other way that the add action has completed successfully
			if (!$(this).hasClass('sub-product-item')) {
				dialog.close();
			}

			minicart.show(response);
			addToCartIntercept.show();
		}
	}.bind(this));
};

/**
 * @description Handler to handle the add all items to cart event
 */
var addAllToCart = function (e) {
	e.preventDefault();
	var $productForms = $('#product-set-list').find('form').toArray();
	TPromise.all(_.map($productForms, addItemToCart))
		.then(function (responses) {
			dialog.close();
			// show the final response only, which would include all the other items
			minicart.show(responses[responses.length - 1]);
		});
};

/**
 * @function
 * @description Binds the click event to a given target for the add-to-cart handling
 */
module.exports = function () {
	$('.add-to-cart[disabled]').attr('title', $('.availability-msg').text());
	$('.product-detail').on('click', '.add-to-cart', addToCart);
	$('#add-all-to-cart').on('click', addAllToCart);
	global.addPersonaliToCart = addPersonaliToCart;
};

module.exports.addToCart = addToCart;
