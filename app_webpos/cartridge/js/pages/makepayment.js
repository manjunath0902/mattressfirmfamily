'use strict';

exports.init = function () {
    $("#dwfrm_billingform_address1,#dwfrm_billingform_address2,#dwfrm_billingform_city,#dwfrm_billingform_fullname").each(function(){
    	$(this).keypress(function(event) {
    		var key = event.key;
    		var pattren = /^[\w\. \[\]!&()+,\-?=%@_{}#_{},#$ÀÁÂÄàáâäÈÉÊèéêëÌÍÎìíîïÒÓÔÖòóôöÙÚÛÜùúûüÇçÑñ¿¡]+$/;
    		return pattren.test(key);
        });
    });
};
