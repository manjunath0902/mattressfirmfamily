'use strict';

var ajax = require('../../ajax'),
    formPrepare = require('./formPrepare'),
    progress = require('../../progress'),
    util = require('../../util'),
    page = require('../../page'),
    dialog = require('../../dialog');

/**
 * @function
 * @description updates the order summary based on a possibly recalculated basket after a shipping promotion has been applied
 */
function updateSummary() {
    var $summary = $('#secondary.summary');
    // indicate progress
    progress.show($summary);

    // load the updated summary area
    $summary.load(Urls.summaryRefreshURL, function () {
        // hide edit shipping method link
        $summary.fadeIn('fast');
        $summary.find('.checkout-mini-cart .minishipment .header a').hide();
        $summary.find('.order-totals-table .order-shipping .label a').hide();
        $('#total_amount').text($summary.find('.order-value').text());
        var totalAmount = $summary.find('.order-value').text();
        $("#transAmount1").attr("value", totalAmount.replace(/[$,]/g,''));
        $('#total_amount_monthly').text($summary.find('.order-value').text());
        //$('.checkout-mini-cart-wrapper').mCustomScrollbar();
        $('.checkout-mini-cart').mCustomScrollbar();
        util.uniform();
    });
}

function updateOPCdeliverySummarySection(zipcode) {
	 var deliverySummarySection = $('#deliverysummaryblock');
	    // indicate progress
	 $('.loader-checkout-delivery-date-section').show();
	 var url = util.appendParamsToUrl(Urls.updateDeliverySummary, {zipCode: zipcode});
     deliverySummarySection.load(url, function (responseTxt, statusTxt, xhr) {
         $('.loader-checkout-delivery-date-section').hide();
         updateSummary();
         util.uniform();
         if(statusTxt == "success") {
        	 	
        	 	if ($("#deliverysummaryblock").find("div.restricted-state").length > 0) {
        	 		$('#restrict_submit_order').show();
        	 		$('#submit_order').hide();
        	 		$('#synchrony_submit_order').hide();
        	 		$('#paypal_submit_order').hide();
        	 	} else {
        	 		var selectedPaymentMethod = $('.payment-method-options').find(':checked').val();
        	 		$('#restrict_submit_order').hide();
        	 		if (selectedPaymentMethod=='CREDIT_CARD') {
        	 			$('#submit_order').show();
        	 			$('#synchrony_submit_order').hide();
        	 		} else if (selectedPaymentMethod=='SYNCHRONY_FINANCIAL') {
        	 			$('#submit_order').hide();
        	 			$('#synchrony_submit_order').show();
        	 		} else {
        	 			$('#paypal_submit_order').show();
        	 		}
        	 	}
        	 
                if ($('#dwfrm_billing_billingAddress_scheduleLater').length > 0) {
                	isValidBillingForm();
                }
             }
         });
         $('#submit_order').show();
}


$(document).on('change', '#dwfrm_billing_billingAddress_scheduleLater , #dwfrm_singleshipping_shippingAddress_addressFields_states_state', function() {
/*	if($('#dwfrm_billing_billingAddress_scheduleLater:checkbox:checked').length > 0){
		utag.link({ "eventCategory" : ["ATP"], "eventLabel" : ["Schedule_Later"], "eventAction" : ["click"] });
	}*/
	isValidBillingForm();
});

$("form#dwfrm_billing input[type=text]").each(function(){
	 var input = $(this);
	 $(this).on('focusout', function() {
			isValidBillingForm();
		});
	});

$("form#dwfrm_billing input[type=tel]").each(function(){
	 var input = $(this);
	 $(this).on('focusout', function() {
			isValidBillingForm();
		});
	});


function LoadShippingCityStateDropdowns(data) {
    var checkoutForm = $("form.address");
    var postalCode = checkoutForm.find("input[name$='_shippingAddress_addressFields_postal']");
    var city = checkoutForm.find('input[name$="_shippingAddress_addressFields_city"]');
    var state = checkoutForm.find('select[name$="_shippingAddress_addressFields_states_state"]');
    var cityJSON = checkoutForm.find("input.cityJSON");
    if (data !== null) {
        city.empty();
        for (var __i = 0; __i < data.cities.length; __i++) {
            
            city.val(data.cities[__i].city);

            if (__i === 0) {
                state.val(data.cities[__i].state);

                // Reset any error conditions on the state field
                state.closest(".form-row").removeClass("error").find(".error-message").hide();

                // State selected so hide drop down
                $(".state-col .dk_options_inner").css("display","none");
            }
        }
        // Enable the city/state select elements so they are submitted with form.
        city.removeAttr("disabled");
        state.removeAttr("disabled");
    }
}

function GetShippingCityStateFromZip(zipCode, updateSummaryFlag) {
    var checkoutForm = $("form.address");
    var postalCode = checkoutForm.find("input[name$='_shippingAddress_addressFields_postal']");
    var city = checkoutForm.find('input[name$="_shippingAddress_addressFields_city"]');
    var state = checkoutForm.find('select[name$="_shippingAddress_addressFields_states_state"]');
    var country = checkoutForm.find('select[name$="_shippingAddress_addressFields_country"]');
    var cityJSON = checkoutForm.find("input.cityJSON");
    // 1. Verify a valid US 5-digit ZIP first, and that the same ZIP was not entered again
    if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCode)) { //&& zipCode != app.clientcache.SESSION_ZIP){

        postalCode.removeClass('error');
        postalCode.next('span.error').remove();
        $.ajax({
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: Urls.cityAndStateForZip,
            async: false,
            cache: false,
            data: {"zipcode": zipCode},

            beforeSend: function () {
                progress.show(".zip-city-state");
            },
            success: function (data) {
                if (data != null && data.cities.length > 0) {

                    LoadShippingCityStateDropdowns(data);

                    $.uniform.update(state);
                    $.uniform.update(city);

                    $('#dwfrm_singleshipping_shippingAddress_addressFields_city-error').remove();

                }
                if (updateSummaryFlag) {
                	updateSummary();
				}                
            },
            error: function (data) {
                //CityFailOver("");
            },
            complete: function () {
                progress.hide(".zip-city-state");
            }
        });
    }
}

function LoadBillingCityStateDropdowns(data) {
    var checkoutForm = $("form.address");
    var postalCode = checkoutForm.find("input[name$='_billingAddress_addressFields_postal']");
    var city = checkoutForm.find('input[name$="_billingAddress_addressFields_city"]');
    var state = checkoutForm.find('select[name$="_billingAddress_addressFields_states_state"]');
    var cityJSON = checkoutForm.find("input.cityJSON");
    if (data !== null) {
        city.empty();
        for (var __i = 0; __i < data.cities.length; __i++) {
            
            city.val(data.cities[__i].city);

            if (__i === 0) {
                state.val(data.cities[__i].state);

                // Reset any error conditions on the state field
                state.closest(".form-row").removeClass("error").find(".error-message").hide();

                // State selected so hide drop down
                $(".state-col .dk_options_inner").css("display","none");
            }
        }
        // Enable the city/state select elements so they are submitted with form.
        city.removeAttr("disabled");
        state.removeAttr("disabled");
    }
}

function GetBillingCityStateFromZip(zipCode) {
    var checkoutForm = $("form.address");
    var postalCode = checkoutForm.find("input[name$='_billingAddress_addressFields_postal']");
    var city = checkoutForm.find('input[name$="_billingAddress_addressFields_city"]');
    var state = checkoutForm.find('select[name$="_billingAddress_addressFields_states_state"]');
    var country = checkoutForm.find('select[name$="_billingAddress_addressFields_country"]');
    var cityJSON = checkoutForm.find("input.cityJSON");
    // 1. Verify a valid US 5-digit ZIP first, and that the same ZIP was not entered again
    if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCode)) { //&& zipCode != app.clientcache.SESSION_ZIP){

        postalCode.removeClass('error');
        postalCode.next('span.error').remove();
        $.ajax({
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: Urls.cityAndStateForZip,
            async: true,
            cache: false,
            data: {"zipcode": zipCode},

            beforeSend: function () {
                progress.show(".zip-city-state");
            },
            success: function (data) {
                if (data != null && data.cities.length > 0) {

                    LoadBillingCityStateDropdowns(data);

                    $.uniform.update(state);
                    $.uniform.update(city);

                    $('#dwfrm_billing_billingAddress_addressFields_city-error').remove();

                } 
            },
            error: function (data) {
                //CityFailOver("");
            },
            complete: function () {
                progress.hide(".zip-city-state");
            }
        });
    }
}

//credit card validations
function initCreditCardValidation() {
	
	var $inputCC = $('#CreditCardBlock input[name*="_creditCard_number"]');
	$('#dwfrm_billing_paymentMethods_creditCard_owner').attr("placeholder", "Cardholder name");
	$('#dwfrm_billing_paymentMethods_creditCard_number').attr("placeholder", "Card number");
	$('#dwfrm_billing_paymentMethods_creditCard_expirydate').attr("placeholder", "MM/YY");
	$('#dwfrm_billing_paymentMethods_creditCard_cvn').attr("placeholder", "CVC");	
	
	$inputCC.on('focusout', function (event) {
		var type= GetCardType($inputCC.val().replace(/\s+/g, ''));
		if(type=="Visa")
		{
			$(this).addClass('Visa');
			$(this).removeClass('Discover');
			$(this).removeClass('MasterCard');
			$(this).removeClass('Amex');
			$(this).parent().addClass('Visa-wrapper');
			$(this).parent().removeClass('Discover-wrapper');
			$(this).parent().removeClass('MasterCard-wrapper');
			$(this).parent().removeClass('Amex-wrapper');
			
			$( "input[name^='dwfrm_billing_paymentMethods_creditCard_cvn']" ).attr("maxlength", "3");
		}
		else if(type=="MasterCard")
		{
			$(this).addClass('MasterCard');
			$(this).removeClass('Visa');
			$(this).removeClass('Discover');
			$(this).removeClass('Amex');
			$(this).parent().addClass('MasterCard-wrapper');
			$(this).parent().removeClass('Visa-wrapper');
			$(this).parent().removeClass('Discover-wrapper');
			$(this).parent().removeClass('Amex-wrapper');
			$( "input[name^='dwfrm_billing_paymentMethods_creditCard_cvn']" ).attr("maxlength", "3");
		}
		else if(type=="Amex")
		{
			$(this).addClass('Amex');
			$(this).removeClass('Visa');
			$(this).removeClass('MasterCard');
			$(this).removeClass('Discover');
			$(this).parent().addClass('Amex-wrapper');
			$(this).parent().removeClass('Visa-wrapper');
			$(this).parent().removeClass('MasterCard-wrapper');
			$(this).parent().removeClass('Discover-wrapper');
			
			$( "input[name^='dwfrm_billing_paymentMethods_creditCard_cvn']" ).attr("maxlength", "4");
		}
		else if(type=="Discover")
		{
			$(this).addClass('Discover');
			$(this).removeClass('Visa');
			$(this).removeClass('MasterCard');
			$(this).removeClass('Amex');
			$(this).parent().addClass('Discover-wrapper');
			$(this).parent().removeClass('Visa-wrapper');
			$(this).parent().removeClass('MasterCard-wrapper');
			$(this).parent().removeClass('Amex-wrapper');
			$( "input[name^='dwfrm_billing_paymentMethods_creditCard_cvn']" ).attr("maxlength", "3");
		}
		else
		{
			$(this).removeClass('Visa');
			$(this).removeClass('MasterCard');
			$(this).removeClass('Amex');
			$(this).removeClass('Discover');
			$(this).parent().removeClass('Visa-wrapper');
			$(this).parent().removeClass('MasterCard-wrapper');
			$(this).parent().removeClass('Amex-wrapper');
			$(this).parent().removeClass('Discover-wrapper');
			$( "input[name^='dwfrm_billing_paymentMethods_creditCard_cvn']" ).attr("maxlength", "3");
		}
		if(type.length > 1){				
			$("#dwfrm_billing_paymentMethods_creditCard_type").val(type);
			if ($(".ccnumber .errorccnumber").length > 0) {
				$(".ccnumber span.errorccnumber").remove();
			}
		}
		else
		{
			$("#dwfrm_billing_paymentMethods_creditCard_type").val('Visa'); // default value
			if ($inputCC.val().length > 0) {
				if ($(".ccnumber .errorccnumber").length < 1) {
					$(".ccnumber .field-wrapper").append("<span class='error errorccnumber'>" + Resources.VALIDATE_CREDITCARD + "</span>");
				}
			}
			else if ($(".ccnumber .errorccnumber").length > 0) {
				$(".ccnumber span.errorccnumber").remove();
			}
			$('#billingSubmitButton').attr("disabled", "disabled");
		}
	});
	$inputCC.on('keypress keydown focusout blur',  function(e){
		
		if ($(".ccnumber .error-message").length > 0) {
			$(".ccnumber div.error-message").remove();
		}
		
	});
	
	 // Default credit card mask
	  var creditCardNumberMask='XXXX XXXX XXXX XXXX';

	  $inputCC.attr("maxlength", creditCardNumberMask.length);
	  $inputCC.attr("x-autocompletetype", "cc-number");
	  $inputCC.attr("autocompletetype", "cc-number");
	  $inputCC.attr("autocorrect", "off");
	  $inputCC.attr("spellcheck", "off");
	  $inputCC.attr("autocapitalize", "off");
	  //
	  // Events
	  //
	  $inputCC.keydown(function(e) {
	    handleMaskedNumberInputKey(e, creditCardNumberMask);
	  });
	  $inputCC.keyup(function(e) {    
		if($inputCC.val().length > 2)
		{
			var card = cardFromNumber($inputCC.val());	
			creditCardNumberMask = card.format;
			$inputCC.attr("maxlength", creditCardNumberMask.length);		
		}
	  });
	  
	  $inputCC.on('paste blur', function(e) {
	    setTimeout(function() {
			
			if($inputCC.val().length > 1)
			{
				var card = cardFromNumber($inputCC.val());	
				if (typeof card != 'undefined') {
					creditCardNumberMask = card.format;
				}
				$inputCC.attr("maxlength", creditCardNumberMask.length);			
			}
			
			var numbersOnly = numbersOnlyString($inputCC.val());
			var formattedNumber = applyFormatMask(numbersOnly, creditCardNumberMask);
			$inputCC.val(formattedNumber);
	      
	    }, 1);
	  });
	
	// expiry date mask and input validation
	$("#dwfrm_billing_paymentMethods_creditCard_expirydate")
    .attr("maxlength", "5")
    .on('keydown',  function(e){
        var val = this.value;
        var temp_val;
        if(!isNaN(val)) {
            if(val > 1 && val < 10 && val.length == 1) {
                temp_val = "0" + val + "/";
                $(this).val(temp_val);
            }
            else if (val >= 1 && val < 10 && val.length == 2 && e.keyCode != 8) {
                temp_val = val + "/";
                $(this).val(temp_val);                        
            }
            else if(val > 9 && val.length == 2 && e.keyCode != 8) {
                temp_val = val + "/";
                $(this).val(temp_val);
            }
        }

        -1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault();

    });
	    
    $("#dwfrm_billing_paymentMethods_creditCard_expirydate").on('paste', function(e) {
	    setTimeout(function() {				
	    	var date= $("#dwfrm_billing_paymentMethods_creditCard_expirydate").val();
			var numbersOnly = numbersOnlyString(date);				
			var mask = "XX/XX"
			var formattedNumber = applyFormatMask(numbersOnly, mask);
			$("#dwfrm_billing_paymentMethods_creditCard_expirydate").val(formattedNumber);
	      
	    }, 1);
	  });


	$( "input[name^='dwfrm_billing_paymentMethods_creditCard_cvn']" )	
	.on('keypress keydown focusout blur',  function(e){
		if ($(".cvn .error-message").length > 0) {
			$(".cvn div.error-message").remove();
		}
        isValidBillingForm();
        /*if((e.type == "focusout") && ($( "input[name^='dwfrm_billing_paymentMethods_creditCard_cvn']" ).val().trim() != "")) {
          utag.link({eventCategory: 'Checkout',eventLabel: 'Creditcard_Code', eventAction: 'Form' });
        }*/
	});
	
	$( "input[name^='dwfrm_billing_paymentMethods_creditCard_expirydate']" )	
	.on('keypress keydown focusout blur',  function(e){
		if ($(".ccdate .error-message").length > 0) {
			$(".ccdate div.error-message").remove();
		}
		if ($("#dwfrm_billing").validate().checkForm()) {
	    	$('#billingSubmitButton').removeAttr('disabled');
	        $('#synchronyDigitalBuyButton').removeAttr('disabled');
	   } else {
	        $('#billingSubmitButton').attr('disabled', 'disabled');
	        $('#synchronyDigitalBuyButton').attr('disabled', 'disabled');
   		}
		/*if((e.type == "focusout") && ($("input[name^='dwfrm_billing_paymentMethods_creditCard_expirydate']").val().trim() != "")) {
		  utag.link({eventCategory: 'Checkout',eventLabel: 'Creditcard_ExpiryDate', eventAction: 'Form' });
		}*/
	});
	
	$( "input[name^='dwfrm_billing_paymentMethods_creditCard_cvn']" )
	.attr("maxlength", "4")
	.on('keydown',  function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
        isValidBillingForm();
   });


	$('#dwfrm_billing_paymentMethods_creditCard_expirydate').on('focusout', function (event) {
		var date= $(this).val();
		if (IsValidExpiryDate(date))
		{
			var datepart = date.split('/');
			$("#dwfrm_billing_paymentMethods_creditCard_expiration_month").val(datepart[0]);
			$("#dwfrm_billing_paymentMethods_creditCard_expiration_year").val('20' + datepart[1]);

			if ($(".expirydate .errordate").length > 0) {
				$(".expirydate span.errordate").remove();
			}
		}
		else
		{    	    		
			if (date.length > 0 ) {
				if ($(".expirydate .errordate").length < 1) {
					$(".expirydate .field-wrapper").append("<span class='error errordate'>" + Resources.VALIDATE_DATE + "</span>");	
				}				
			}
			else if ($(".expirydate .errordate").length > 0) {
				$(".expirydate span.errordate").remove();
			}
			$('#billingSubmitButton').attr("disabled", "disabled");
		}
	});

	var $selectPaymentMethod = $('.payment-method-options');
	var selectedPaymentMethod = $selectPaymentMethod.find(':checked').val();
	$selectPaymentMethod.on('click', 'input[type="radio"]', function () {
		if($(this).val()=='CREDIT_CARD'){				
			$('#submit_order').show();
			$('#synchrony_submit_order').hide();			
		}
		else
		{
			$('#submit_order').hide();
			$('#synchrony_submit_order').show();
			updatePaymentMethod('SYNCHRONY_FINANCIAL');			
		}
		isValidBillingForm();
	});
	
	if(selectedPaymentMethod =='CREDIT_CARD'){				
		$('#submit_order').show();
		$('#synchrony_submit_order').hide();
	}
	else
	{
		$('#submit_order').hide();
		$('#synchrony_submit_order').show();
		isValidBillingForm();
	}

	
}

function isValidBillingForm () {
	if ($('#dwfrm_billing').length > 0) {
		if ($('#dwfrm_billing').validate().checkForm() && $('.count-reg').length < 1) {
	        $('#billingSubmitButton').removeAttr('disabled');
	            $('#synchronyDigitalBuyButton').removeAttr('disabled');
	       } else {
	            $('#billingSubmitButton').attr('disabled', 'disabled');
	            $('#synchronyDigitalBuyButton').attr('disabled', 'disabled');
	     }
	}
}

$(document).on('focusout','#dwfrm_synchrony_billToAccountNumber', function() {
	isValidBillingForm();
	if ($(this).val() == '') {
	    var $continue = $('#synchronyDigitalBuyButton');
	    $continue.attr('disabled', 'disabled'); 
	}
	/*if($("#dwfrm_synchrony_billToAccountNumber").val().trim() != "") {
	  utag.link({eventCategory: 'Checkout',eventLabel: 'Account_Number', eventAction: 'Form' });
	}*/
});
//Checkout flow coupon code apply and error handling
$("body").on('click',"#add-coupon-code",  function (e) {
    e.preventDefault();    
    var $couponCode = $('input[name$="_couponCode"]');
    var code = $couponCode.val();
    if (code.length === 0) {
    	$('#dwfrm_cart_couponCode').val('');
    	$('#dwfrm_cart_couponCode').addClass('error');
    	$('#dwfrm_cart_couponCode').nextAll('.error:first').text(Resources.COUPON_CODE_MISSING);
        return;
    }

    var url = util.appendParamsToUrl(Urls.addCouponCheckout, {couponCode: code, format: 'ajax'});
        $.getJSON(url, function (data) {
            var fail = false;
            var msg = "";
            if (!data) {
                msg = Resources.BAD_RESPONSE;
                fail = true;
            }
            if (data.CouponError == 'NO_ACTIVE_PROMOTION') {
            	msg = Resources.NO_ACTIVE_PROMOTION;
            	var msg = msg.replace("temp", data.CouponCode);
            } else if (data.CouponError == 'COUPON_CODE_MISSING') {
            	msg = Resources.COUPON_CODE_MISSING;
            } else if (data.couponStatus== 'COUPON_CODE_ALREADY_IN_BASKET') {
            	msg = Resources.COUPON_CODE_ALREADY_IN_BASKET;
            	var msg = msg.replace("temp", data.CouponCode);
            } else if (data.couponStatus== 'COUPON_ALREADY_IN_BASKET') {
            	msg = Resources.COUPON_ALREADY_IN_BASKET;
            	var msg = msg.replace("temp", data.CouponCode);
            } else if (data.couponStatus== 'COUPON_CODE_ALREADY_REDEEMED') {
            	msg = Resources.COUPON_CODE_ALREADY_REDEEMED;
            	var msg = msg.replace("temp", data.CouponCode);
            } else if (data.couponStatus== 'COUPON_CODE_UNKNOWN') {
            	msg = Resources.COUPON_CODE_UNKNOWN;
            	var msg = msg.replace("temp", data.CouponCode);
            } else if (data.couponStatus== 'COUPON_DISABLED') {
            	msg = Resources.COUPON_DISABLED;
            	var msg = msg.replace("temp", data.CouponCode);
            } else if (data.couponStatus== 'REDEMPTION_LIMIT_EXCEEDED') {
            	msg = Resources.REDEMPTION_LIMIT_EXCEEDED;
            	var msg = msg.replace("temp", data.CouponCode);
            } else if (data.couponStatus== 'CUSTOMER_REDEMPTION_LIMIT_EXCEEDED') {
            	msg = Resources.CUSTOMER_REDEMPTION_LIMIT_EXCEEDED;
            	var msg = msg.replace("temp", data.CouponCode);
            } else if (data.couponStatus== 'TIMEFRAME_REDEMPTION_LIMIT_EXCEEDED') {
            	msg = Resources.TIMEFRAME_REDEMPTION_LIMIT_EXCEEDED;
            	var msg = msg.replace("temp", data.CouponCode);
            } else if (data.couponStatus== 'NO_ACTIVE_PROMOTION') {
            	msg = Resources.NO_ACTIVE_PROMOTION;
            	var msg = msg.replace("temp", data.CouponCode);
            }
            //Scenarios, whether a promo percentage or bonus product added or whether a promo is applied/not applied
            if(data.CouponApplied == false){
            	page.refresh();
            } else if (data.success && data.CouponApplied && data.CouponType == "BONUS" && msg == "") {
                $(function(){
                	page.refresh();
                	// #coupon-success-message-checkout is the id of overlay which comes after bonus product addition in basket via coupon code.
                	/*$('#coupon-success-message-checkout').show();
				    setTimeout(function() {
				    	$('#coupon-success-message-checkout').fadeOut('fast');
					    page.refresh();
					}, 2000);*/
				});
            }else if (data.success && data.CouponApplied && data.CouponType != "BONUS" && msg == "") {
            	$(function(){
            		page.refresh();
            		//#promo-success-message-checkout is the id of overlay which comes after discount applied on basket via coupon code.
                   /* $('#promo-success-message-checkout').show();
				    setTimeout(function() {
				    	$('#promo-success-message-checkout').fadeOut('fast');
					    page.refresh();
					}, 2000);*/
				});
            } else if (msg != "") {
            	$('#dwfrm_cart_couponCode').val('');
            	$('#dwfrm_cart_couponCode').addClass('error');
            	$('#dwfrm_cart_couponCode').nextAll('.error:first').text(msg);
            }
        });
});

//MAT-1468	Issues with hitting enter for promo codes
//trigger events on enter
$(document.body).on('keydown',$('input[name$="_couponCode"]'), function (e) {
   e.stopPropagation();
   if (e.which === 13) {
		e.preventDefault();
		$("#add-coupon-code").click();
	}
});
// Verify ATP delivery zone
function verifyDeliveryZone(zipCode){
	
	var zoneChanged = false;	
	var params = {
            format: 'ajax',
            zipCode: zipCode
    };
	progress.show();
	$.ajax({
		url: util.appendParamsToUrl(Urls.verifyATPDeliveryZone, params),
		type: 'get',
		async: false,
		success: function (response) {
	    	if (response) {
	    		zoneChanged = true;
			}		    	
		},
		failure: function () {
			window.alert(Resources.SERVER_ERROR);
			
		}
	});
		
	return zoneChanged;
	
}


function deliverySelection() {
	if ($('#zipcodeUpdate').hasClass( "disabled" )) {
		$('#zipcodeUpdate').removeClass("disabled");
	}
    $('.preferred-contact-wrapper').hide();
    $('.delivery-dates-wrapper').show();

    if (SitePreferences.isAtpDeliveryScheduleEnabled) {
        if ($('.schedule').hasClass('slick-initialized')) {
            $('.schedule').slick('unslick');
        }
        $('.schedule').slick({
            infinite: false,
            adaptiveHeight: true,
            slidesToScroll: 1,
            slidesToShow: 1,
            swipe: false,
            prevArrow: $('.slick-prev-arrow'),
            nextArrow: $('.slick-next-arrow'),
            accessibility: false,
            draggable: false
        });
        $('.slick-prev-arrow').attr('tabindex',0);
        $('.slick-next-arrow').attr('tabindex',0);
        $('.slick-prev-arrow').click(function(e) {
        	e.preventDefault();
            $('.schedule').slick('slickGoTo', parseInt($('.schedule').slick('slickCurrentSlide')) - 1);
         });
        $('.slick-next-arrow').on('click', function (e) {
        	e.preventDefault();
            var currentIndex = $('.schedule').slick('slickCurrentSlide');
            var slickSliderLength = $('.sched-date-row.slick-slide').length;
            if (currentIndex == (slickSliderLength - 1) && $(this).data('current-week-date') != false) {
                var params = {
                        format: 'ajax',
                        currentWeekDate: $(this).data('current-week-date')
                };
                progress.show($('#main'));
                ajax.load({
                    url: util.appendParamsToUrl(Urls.getATPDeliveryDatesForNextWeek, params),
                    callback: function (response) {
                        if (response != '') {
                            $('.schedule').slick('slickAdd', response, currentIndex, true);
                            if ($('.slick-next-arrow').data('current-week-date') == false) {
                                $('.schedule').slick('slickRemove', (currentIndex + 1));
                            }
                        }
                        progress.hide();
                        $('.slick-prev-arrow').attr('tabindex',0);
                        $('.slick-next-arrow').attr('tabindex',0);
                                                
                    }
                });
            }
            $('.slick-prev-arrow').attr('tabindex',0);
			$('.slick-next-arrow').attr('tabindex',0);
        });
        var slickPreviousDiv = null;
        var slickNextDiv = null;
        var currentSlide = 0;
        $('.slick-next-arrow').keypress(function (ev) {
			if (ev.keyCode == 13 || ev.which == 13) {
				$('.sched-date-row').find('.time-slot').removeAttr("tabindex");
				if (slickNextDiv != null) {
					slickNextDiv.attr("tabindex", 0);
				}
				currentSlide = $('.schedule').slick('slickCurrentSlide');
				var $this = $('.schedule').find("[data-slick-index=" + currentSlide + "]").find('.time-slot');
				$this.removeAttr("tabindex");
				slickPreviousDiv = $this;
				$(this).trigger('click');
			}
		});
        $('.slick-prev-arrow').keypress(function (ev) {
			if (ev.keyCode == 13 || ev.which == 13) {
				$('.sched-date-row').find('.time-slot').removeAttr("tabindex");
				currentSlide = $('.schedule').slick('slickCurrentSlide');
				var $this = $('.schedule').find("[data-slick-index=" + currentSlide + "]").find('.time-slot');
				$this.removeAttr("tabindex");
				slickNextDiv = $this;
				slickPreviousDiv.attr("tabindex",0);
				$('.slick-prev-arrow').attr('tabindex',0);
				$('.slick-next-arrow').attr('tabindex',0);
				$('.slick-prev-arrow')[0].click();
			}
		});
	} else {
        if ($('.sched-delivery').hasClass('slick-initialized')) {
            $('.sched-delivery').slick('unslick');
        }
        $('.sched-delivery').slick({
            infinite: false,
            adaptiveHeight: true
        });
    }
    
}

function initialDeliveryDates() {
	deliverySelection();

    var addressForm = $('.delivery-dates');
    function changeDeliveryDate(obj) {
        if (!($(obj).hasClass("full"))) {
            /** grab time slot clicked and mark correct day and time slot as selected **/
            $(".time-slot").removeClass("selected");
            $(".header").removeClass("selected");
            $(obj).addClass("selected");
            $(obj).parent().children(".header").addClass("selected");

            /** build text block at bottom stating what date and time was chosen **/
            if (SitePreferences.isAtpDeliveryScheduleEnabled) {
                $(".currently-chosen").removeClass("hide");
                var textdate = $(obj).parent().find(".weekday").data('date');
                //textdate += ", " + $(this).parent().find(".month-day").text();
                textdate += '<span> from </span>' + $(obj).text().replace('-', 'to').trim();
                $(".chosen-time").html(textdate);

                /** set pdict fleetwiseToken to currently selected value **/
                $('#dwfrm_singleshipping_shippingAddress_addressFields_fleetwiseToken').val($(obj).data("fleetwisetoken"));
                var deliveryTime = $(obj).text().replace(RegExp(' ', 'g'), '');
                $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryDate').val($(obj).parent().find(".weekday-timeval").text() + ', ' + deliveryTime);
                $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryTime').val(deliveryTime);
            } else {
            	$(".currently-chosen").removeClass("hide");
                $(".currently-chosen").addClass("visible");
                var textdate = $(obj).parent().find(".weekday").text();
                //textdate += ", " + $(this).parent().find(".month-day").text();
                textdate += ", between " + $(obj).text().replace("-","and").replace(RegExp(" PM", "g"), "pm").replace(RegExp(" AM", "g"), "am").trim();
                $(".chosen-time").text(textdate);

                /** set pdict fleetwiseToken to currently selected value **/
                $('#dwfrm_singleshipping_shippingAddress_addressFields_fleetwiseToken').val($(obj).data("fleetwisetoken"));
                $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryDate').val(textdate);
            }
        }
    }
    
    
    addressForm.on("click", ".time-slot", function () {
		changeDeliveryDate(this);
	});
	addressForm.on("keypress", ".time-slot", function (ev) {
		if (ev.keyCode == 13 || ev.which == 13) {
			changeDeliveryDate(this);
		}
	});
    addressForm.on("focus", ".time-slot", function () {
        var dateValue = $($(this).siblings()[0]).attr('data-date');
        dateValue = dateValue.trim();
        var timeValue = $(this).text();
        timeValue = timeValue.trim();
        var timeSlotAriaLabel = dateValue + " " + timeValue;
        $(this).attr("aria-label", timeSlotAriaLabel);
        $(this).parents('.sched-date-row').attr("aria-hidden", false);
    });
    
    //default select first slot
    $(".sched-date").find(".time-slot").each(function(){
    	$(this).click();
    	return false; // to exit each
	});
    
}

$(document).on('click', '#chooseMyDelivery',function (e) {
    e.preventDefault();
    var url = Urls.deliveryDatesCalender;

	$.ajax({
		url: url,
		type: 'GET'
	})
	// success
	.done(function (response) {
		dialog.open({
			html: $(response),
			options: {
				autoOpen: true,
				dialogClass: 'redCarpet-Delivery-Section-Popup'
			}
		});
		initialDeliveryDates();
		
		var atpFailedCoreProductsArray = $("#atpFailedCoreProductsArrayAdresses").attr("data-atp-failedcoreproductsarray");
		if (atpFailedCoreProductsArray && atpFailedCoreProductsArray != "null") {
			var sliceArray = atpFailedCoreProductsArray.slice(1,-1);
/*			utag.link({ "eventCategory" : ["ATP"], "eventLabel" : sliceArray.split(","), "eventAction" : ["fail"]});*/
		}
		
	})
	.error(function (xhr, status, error) {
    });
});

$(document).on('click', '#schduleMyDelivery',function (e) {
    e.preventDefault();
    var deliverySummarySection = $('#deliverysummaryblock');
    // indicate progress
    //progress.show(deliverySummarySection);
    var deliveryDate = $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryDate').val();
    var deliveryTime = $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryTime').val();
	var url = util.appendParamsToUrl(Urls.updateDeliverySummary, {deliveryDateSelected: true,DeliveryDate: deliveryDate, DeliveryTime: deliveryTime});
	deliverySummarySection.load(url, function (responseTxt, statusTxt, xhr) {
			// update order summary section 
			updateSummary();
			util.uniform();
			 if(statusTxt == "success") {
				 isValidBillingForm();
	          }
	    });
    dialog.close();
});
$(document).on('click', '#noDeliverySlots',function (e) {
    e.preventDefault();
    dialog.close();
});
//#promo-success-message-checkout is the id of overlay which comes after discount applied on basket via coupon code.
$('body').on('click tap', '#promo-success-message-checkout', function (e) {
       e.preventDefault();
       $('#promo-success-message-checkout').hide();
});

// #coupon-success-message-checkout is the id of overlay which comes after bonus product addition in basket via coupon code.
$('body').on('click tap', '#coupon-success-message-checkout', function (e) {
    e.preventDefault();
    $('#coupon-success-message-checkout').hide();
});

// update payment method selection
function updatePaymentMethod (paymentMethodID) {
    var $paymentMethods = $('.payment-method');
    $paymentMethods.removeClass('payment-method-expanded');

    var $selectedPaymentMethod = $paymentMethods.filter('[data-method="' + paymentMethodID + '"]');
    if ($selectedPaymentMethod.length === 0) {
        $selectedPaymentMethod = $('[data-method="Custom"]');
    }
    $selectedPaymentMethod.addClass('payment-method-expanded');

    // ensure checkbox of payment method is checked
    $('input[name$="_selectedPaymentMethodID"]').removeAttr('checked');
    $('input[value=' + paymentMethodID + ']').prop('checked', 'checked');
    if (paymentMethodID === 'SYNCHRONY_FINANCIAL') {
        // get token
        ajax.getJson({
            url: Urls.getToken,
            callback: function (data) {
                $('input[name="clientToken"]').val(data.token);
            }
        });
    }
}


function IsValidExpiryDate(date) {	
	var re = new RegExp("^(0[1-9]|1[0-2]|[1-9])\/(1[7-9]|[2-9][0-9])$");
	if (date.match(re) != null) {
		var datepart = date.split('/');
		var month = parseInt(datepart[0], 10),
		year = 2000 + parseInt(datepart[1], 10),
		currentMonth = new Date().getMonth() + 1,
		currentYear  = new Date().getFullYear();
		 if (year < currentYear || (year == currentYear && month < currentMonth)) {
			 return false;
		 }
		 return true;	
	}
    return false;
}

function GetCardType(number)
{
	// visa
	var re = new RegExp("^4[0-9]{12}(?:[0-9]{3})?$");
	if (number.match(re) != null)
		return "Visa";

	// Master card 
	var re = new RegExp("^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$");
	if (number.match(re) != null) 
		return "MasterCard";

	// AMEX
	re = new RegExp("^3[47][0-9]{13}$");
	if (number.match(re) != null)
		return "Amex";

	// Discover
	re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
	if (number.match(re) != null)
		return "Discover";

	return "";
}

// clear billing form
function clearBillingForm(){	
    
    $("input[name$='billing_billingAddress_addressFields_firstName']").val('');
    $("input[name$='billing_billingAddress_addressFields_lastName']").val('');
    $("input[name$='billing_billingAddress_addressFields_address1']").val('');
    $("input[name$='billing_billingAddress_addressFields_address2']").val('');
    $("input[name$='billing_billingAddress_addressFields_phone']").val('');    
    $("input[name$='billing_billingAddress_addressFields_postal']").val('');
    $('input[name$="billing_billingAddress_addressFields_city"]').val('');
    $('select[name$="billing_billingAddress_addressFields_states_state"]').val('');
}

// copy shipping address into billing address 
function useShippingAsBilling(checkoutForm){
	
	var firstName = checkoutForm.find("input[name$='singleshipping_shippingAddress_addressFields_firstName']").val();
	var lastName = checkoutForm.find("input[name$='singleshipping_shippingAddress_addressFields_lastName']").val();
	var address1 = checkoutForm.find("input[name$='singleshipping_shippingAddress_addressFields_address1']").val();
	var address2 = checkoutForm.find("input[name$='singleshipping_shippingAddress_addressFields_address2']").val();
	var phone = checkoutForm.find("input[name$='singleshipping_shippingAddress_addressFields_phone']").val();
    var postalCode = checkoutForm.find("input[name$='singleshipping_shippingAddress_addressFields_postal']").val();
    var city = checkoutForm.find('input[name$="singleshipping_shippingAddress_addressFields_city"]').val();
    var state = checkoutForm.find('select[name$="singleshipping_shippingAddress_addressFields_states_state"]').val();
    
    $("input[name$='billing_billingAddress_addressFields_firstName']").val(firstName);
    $("input[name$='billing_billingAddress_addressFields_lastName']").val(lastName);
    $("input[name$='billing_billingAddress_addressFields_address1']").val(address1);
    $("input[name$='billing_billingAddress_addressFields_address2']").val(address2);
    $("input[name$='billing_billingAddress_addressFields_phone']").val(phone);    
    $("input[name$='billing_billingAddress_addressFields_postal']").val(postalCode);
    $('input[name$="billing_billingAddress_addressFields_city"]').val(city);
    $('select[name$="billing_billingAddress_addressFields_states_state"]').val(state);
}

function initAddressValidation(){
	
	formPrepare.init({
        continueSelector: '[name$="dwfrm_billing_save"]',
        formSelector:'[id$="dwfrm_billing"]'
    });
	
	if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test($('.opcpostal input').val())) {
        updateOPCdeliverySummarySection($('.opcpostal input').val());
	}
	
	$('.opcpostal input').on("change",function(){
		var zipCode = $(this).val();
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCode)) {
	        updateOPCdeliverySummarySection(zipCode);
		} else {
			var $continue = $('.form-row-button button');
			$continue.attr('disabled', 'disabled'); 
		}
    });
    
/*    $("#dwfrm_singleshipping_shippingAddress_email_emailAddress").on("blur",function(){    	
	        if($("#dwfrm_singleshipping_shippingAddress_email_emailAddress").val().trim() != "") {
	         utag.link({eventCategory: 'Checkout',eventLabel: 'Email_Address', eventAction: 'Form' });
	        }
    });
    
    $("#dwfrm_singleshipping_shippingAddress_addressFields_firstName").on("blur",function(){
    	if($("#dwfrm_singleshipping_shippingAddress_addressFields_firstName").val().trim() != "") {
    	utag.link({eventCategory: 'Checkout',eventLabel: 'First_Name', eventAction: 'Form' });
    	}
    });
    $("#dwfrm_singleshipping_shippingAddress_addressFields_lastName").on("blur",function(){
    	if($("#dwfrm_singleshipping_shippingAddress_addressFields_lastName").val().trim() != "") {
    	utag.link({eventCategory: 'Checkout',eventLabel: 'Last_Name', eventAction: 'Form' });
    	}
    });
    $("#dwfrm_singleshipping_shippingAddress_addressFields_address1").on("blur",function(){
    	if($("#dwfrm_singleshipping_shippingAddress_addressFields_address1").val().trim() != "") {
    	utag.link({eventCategory: 'Checkout',eventLabel: 'Address_Address1', eventAction: 'Form' });
    	}
    });
    $("#dwfrm_singleshipping_shippingAddress_addressFields_city").on("blur",function(){
    	if($("#dwfrm_singleshipping_shippingAddress_addressFields_city").val().trim() != "") {
    	utag.link({eventCategory: 'Checkout',eventLabel: 'Address_City', eventAction: 'Form' });
    	}
    });
    $("#dwfrm_singleshipping_shippingAddress_addressFields_states_state").on("blur",function(){
    	if($("#dwfrm_singleshipping_shippingAddress_addressFields_states_state").val().trim() != "") {
    	utag.link({eventCategory: 'Checkout',eventLabel: 'Address_State', eventAction: 'Form' });
    	}
    }); 
    $("#dwfrm_singleshipping_shippingAddress_addressFields_postal").on("blur",function(){
    	if($("#dwfrm_singleshipping_shippingAddress_addressFields_postal").val().trim() != "") {
    	utag.link({eventCategory: 'Checkout',eventLabel: 'Address_Postal', eventAction: 'Form' });
    	}
    });
    $("#dwfrm_singleshipping_shippingAddress_addressFields_phone").on("blur",function(){ 
    	if($("#dwfrm_singleshipping_shippingAddress_addressFields_phone").val().trim() != "") {
    	utag.link({eventCategory: 'Checkout',eventLabel: 'Address_Phone', eventAction: 'Form' });
    	}
    });
    $("#dwfrm_singleshipping_shippingAddress_useAsBillingAddress").on("blur",function(){
    	if($("#dwfrm_singleshipping_shippingAddress_useAsBillingAddress").val().trim() != "") {
    	utag.link({eventCategory: 'Checkout',eventLabel: 'UseAs_BillingAddress', eventAction: 'Check Box' });
    	}
    });
    $("#is-CREDIT_CARD").on("blur",function(){
    	if($("#is-CREDIT_CARD").val().trim() != "") {
    	utag.link({eventCategory: 'Checkout',eventLabel: 'Creditcard', eventAction: 'Radio Button' });
    	}
    });
    $("#dwfrm_billing_paymentMethods_creditCard_number").on("blur",function(){
    	if($("#dwfrm_billing_paymentMethods_creditCard_number").val().trim() != "") {
    	utag.link({eventCategory: 'Checkout',eventLabel: 'Creditcard_Number', eventAction: 'Form' });
    	}
    });*/
    //$("#dwfrm_billing_paymentMethods_creditCard_expirydate") yes
    //$("#dwfrm_billing_paymentMethods_creditCard_cvn") yes
    /*$("#is-SYNCHRONY_FINANCIAL").on("blur",function(){
    	if($("#is-SYNCHRONY_FINANCIAL").val().trim() != "") {
    	utag.link({eventCategory: 'Checkout',eventLabel: 'SynchronyFinancial', eventAction: 'Radio Button' });
    	}
    });*/
    //$("#dwfrm_synchrony_billToAccountNumber") yes
    
    $(".link-login-mob-show a").on("click",function(){
    	$(".mobile-top-tabs-holder li").removeClass("active");
    	$(this).parent().addClass("active");
    	$(".mobile-checkout-process").fadeOut();
    	if($(this).hasClass("customerportal")) {
    		window.location.href = $(this).attr('data-url');
    		return false;
    	}
    	else {
        	$(".mobile-login-template").fadeIn();
    	}
    });
    
    $(".link-checkout-mob-show a").on("click",function(){
    	$(".mobile-top-tabs-holder li").removeClass("active");
    	$(this).parent().addClass("active");
    	$(".mobile-login-template").fadeOut();
    	$(".mobile-checkout-process").fadeIn();
    });
    /*$(".btn-mobile-next-step button").on("click",function(){
    	$(".page-title").fadeOut();
    	$(".mobile-top-tabs-holder").fadeOut();
    	$(".mobile-shipping-address").fadeOut();
    	$(".mobile-payment-and-billing").fadeIn();
    	$(".btn-mobile-next-step").hide();
    	window.scrollTo(0, 0);
    });*/
    
    var heightTooltipCVN = $(".checkout-security-code-tooltip .tooltip-holder").height();
    var heightTooltipHELP = $(".checkout-phone-help-tooltip .tooltip-holder").height();
    $(".checkout-security-code-tooltip .tooltip-holder").css("marginTop",-( heightTooltipCVN / 2));
    $(".checkout-phone-help-tooltip .tooltip-holder").css("marginTop", -(heightTooltipHELP / 2));
    if($(window).width() < 768) {
    	$(".cvn .icon").on({
    	    mouseover: function() {
    	    	$(".cvn").find('.tooltip-content').addClass('show');
    	    },
    	    mouseout: function() {
    	    	$(".cvn").find('.tooltip-content').removeClass('show');
    	    }
    	});
    }
    $(".mobile-pt-checkout-container .login-box.login-account .remind-me").find("button").wrap("<div class='mobile-signin-button'></div>");
    $(".mobile-payment-method-tabs .CREDIT_CARD").addClass("active");
    $(".mobile-payment-method-tabs .CREDIT_CARD,.mobile-payment-method-tabs .SYNCHRONY_FINANCIAL").addClass("mobile-payment-tabs");
    $(".mobile-payment-tabs").on("click",function(){
    	$(".mobile-payment-tabs").removeClass("active");
    	$(this).addClass("active");
    });
    
    if ($('#useasbillingaddress .input-checkbox').is(":checked")) {
        $('#billingaddress').fadeOut('slow');
        $('.mobile-billing-save-address').fadeIn('slow');
        var checkoutForm = $("form.address");        	
    	useShippingAsBilling(checkoutForm);
    	displayBillingAddress();
        isValidBillingForm();
    }
	// use shipping address as billing address
	$('#useasbillingaddress .input-checkbox').change(function(){
        if (this.checked) {
        	var checkoutForm = $("form.address");        	
        	useShippingAsBilling(checkoutForm);
            $('#billingaddress').fadeOut('slow');
            $('.mobile-billing-save-address').fadeIn('slow');
            isValidBillingForm();
        }
        else {
        	clearBillingForm();
            $('#billingaddress').fadeIn('slow');
            $('.mobile-billing-save-address').fadeOut('slow');
            isValidBillingForm();
        }                   
    });
	
	$('.opcphones input').blur(function (e) {
		var rgx = /^\(?([2-9][0-8][0-9])\)?[\-\. ]?([2-9][0-9]{2})[\-\. ]?([0-9]{4})(\s*x[0-9]+)?$/;
		var value= $(this).val();		
		var isValid = rgx.test($.trim(value));
		
		if (!isValid) {
			if ($(".phone").parent().find(".error").length < 1) {
				$(".phone").parent().append("<span class='error'>" + Resources.INVALID_PHONE + "</span>");
				$(".phone").addClass("error");
			}
			
		} else {
			if ($(".phone").parent().find(".error").length > 0) {
				$(".phone").parent().find("span.error").remove();
				$(".phone").removeClass("error");
			}
		}
	});
	// add phone mask to phone
	$('.opcphones input').inputmask("(999) 999-9999", { showMaskOnHover: false, showMaskOnFocus: false });

	//billing submit button for one page checkout 
    $('#billingSubmitButton').on('click tap', function (e) {    	
    	if ($('#useasbillingaddress .input-checkbox').is(":checked")) {
    		var checkoutForm = $("form.address");        	
    		useShippingAsBilling(checkoutForm);
        }
    	
    	var $inputCC = $('#CreditCardBlock input[name*="_creditCard_number"]');
    	$inputCC.val($inputCC.val().replace(/\s+/g, ''));
    	
    	//set credit card holder name from billing first name and last name
        var firstName = $("#dwfrm_billing_billingAddress_addressFields_firstName").val();
        var lastName = $("#dwfrm_billing_billingAddress_addressFields_lastName").val();
        $('#dwfrm_billing_paymentMethods_creditCard_owner').val(firstName + ' ' + lastName);
        
        var isMobileDeviceType = $("#isMobileDeviceType").val();
        if(isMobileDeviceType == 'false') {
            var emailAddress =$("#dwfrm_singleshipping_shippingAddress_email_emailAddress").val();
        	var emailRgx = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    	        if (emailRgx.test(emailAddress)) {
    		    	var params = {
    		                format: 'ajax',
    		                emailAddress: emailAddress,
    		                optFlag: $('#uniform-dwfrm_singleshipping_shippingAddress_addToSubscription').children().hasClass("checked")//$('#dwfrm_singleshipping_shippingAddress_addToSubscription').val()
    		        };
    		    	
    		    	$.ajax({
    		            url: util.appendParamsToUrl(Urls.captureCheckoutEmail, params),
    		            type: 'POST'
    		        });
    	        }
        }        
        
        var valid = jQuery('#dwfrm_billing').validate().checkForm();  
	    if (valid) {
	    	$(this).addClass('disabled-btn');
	    }
    });

//    $('#mobileShippingSubmitButton').on('click tap', function (e) {    	
//    	
//    	var zipCode = $("#dwfrm_singleshipping_shippingAddress_addressFields_postal").val();
//    	progress.show();
//    	var zoneChanged = verifyDeliveryZone(zipCode);
//    	if (zoneChanged) {    		
//    		e.preventDefault();
//    		window.location.href = util.appendParamsToUrl(Urls.COShippingMethodStart, {deliveryScheduleChanged : 'true'});
//    	}
//    });
    
    //saved address values 
    
    function displayBillingAddress(){
    	var savedFirstName = $("#dwfrm_singleshipping_shippingAddress_addressFields_firstName").val();
        var savedLastName = $("#dwfrm_singleshipping_shippingAddress_addressFields_lastName").val();
        var savefullName = savedFirstName + " " + savedLastName;
        var saveAddressOne = $("#dwfrm_singleshipping_shippingAddress_addressFields_address1").val();
        var saveAddressTwo = $("#dwfrm_singleshipping_shippingAddress_addressFields_address2").val();
        var saveCityName = $("#dwfrm_singleshipping_shippingAddress_addressFields_city").val();
        var saveStateName = $("#dwfrm_singleshipping_shippingAddress_addressFields_states_state").val();
        var savedZippedCode = $("#dwfrm_singleshipping_shippingAddress_addressFields_postal").val();
        var savedPhoneNumber = $("#dwfrm_singleshipping_shippingAddress_addressFields_phone").val();  
        var cityStateZip = saveCityName + ", " + saveStateName + " " + savedZippedCode;
        $(".mobile-saved-name").html("<strong>" + savefullName + "</strong>");
        $(".mobile-save-address").html("<span>" + saveAddressOne + "</span>");
        if(saveAddressTwo){
        	$(".mobile-save-address1").html("<span>" + saveAddressTwo + "</span>");
        }
        $(".mobile-save-city-name").html("<span>" + cityStateZip + "</span>");
        //$(".mobile-save-state-name").html("<span>" + saveStateName + "</span>");
        //$(".mobile-save-zipped-code").html("<span>" + savedZippedCode + "</span>");
        $(".mobile-save-phone-number").html("<span>" + savedPhoneNumber + "</span>");
    }
    
    
}
	var KEYS = {
		  "0" : 48,
		  "9" : 57,
		  "NUMPAD_0" : 96,
		  "NUMPAD_9" : 105,
		  "DELETE" : 46,
		  "BACKSPACE" : 8,
		  "ARROW_LEFT" : 37,
		  "ARROW_RIGHT" : 39,
		  "ARROW_UP" : 38,
		  "ARROW_DOWN" : 40,
		  "HOME" : 36,
		  "END" : 35,
		  "TAB" : 9,
		  "A" : 65,
		  "X" : 88,
		  "C" : 67,
		  "V" : 86
		};



	var defaultFormat = "XXXX XXXX XXXX XXXX";

	var cards = [
     {
       type: 'amex',
       pattern: /^3[47]/,
       format: 'XXXX XXXXXX XXXXX',
       length: [15],
       cvcLength: [4],
       luhn: true
     }, {
       type: 'discover',
       pattern: /^(6011|65|64[4-9]|622)/,
       format: defaultFormat,
       length: [16],
       cvcLength: [3],
       luhn: true
     },{
       type: 'maestro',
       pattern: /^(5018|5020|5038|6304|6703|6708|6759|676[1-3])/,
       format: defaultFormat,
       length: [12, 13, 14, 15, 16, 17, 18, 19],
       cvcLength: [3],
       luhn: true
     }, {
       type: 'mastercard',
       pattern: /^(5[1-5]|677189)|^(222[1-9]|2[3-6]\d{2}|27[0-1]\d|2720)/,
       format: defaultFormat,
       length: [16],
       cvcLength: [3],
       luhn: true
     },{
       type: 'visaelectron',
       pattern: /^4(026|17500|405|508|844|91[37])/,
       format: defaultFormat,
       length: [16],
       cvcLength: [3],
       luhn: true
     }, {
       type: 'visa',
       pattern: /^4/,
       format: defaultFormat,
       length: [13, 16, 19],
       cvcLength: [3],
       luhn: true
     }
   ];


	/**
	 *
	 *
	 * @param e
	 * @param mask
	 */
	function handleMaskedNumberInputKey(e, mask) {
	 
	  var keyCode = e.which || e.keyCode;

	  var element = e.target;

	  var caretStart = caretStartPosition(element);
	  var caretEnd = caretEndPosition(element);


	  // Calculate normalised caret position
	  var normalisedStartCaretPosition = normaliseCaretPosition(mask, caretStart);
	  var normalisedEndCaretPosition = normaliseCaretPosition(mask, caretEnd);


	  var newCaretPosition = caretStart;

	  var isNumber = keyIsNumber(e);
	  var isDelete = keyIsDelete(e);
	  var isBackspace = keyIsBackspace(e);

	  if (isNumber || isDelete || isBackspace) {
	    e.preventDefault();
	    var rawText = $(element).val();
	    var numbersOnly = numbersOnlyString(rawText);

	    var digit = digitFromKeyCode(keyCode);

	    var rangeHighlighted = normalisedEndCaretPosition > normalisedStartCaretPosition;

	    // Remove values highlighted (if highlighted)
	    if (rangeHighlighted) {
	      numbersOnly = (numbersOnly.slice(0, normalisedStartCaretPosition) + numbersOnly.slice(normalisedEndCaretPosition));
	    }

	    // Forward Action
	    if (caretStart != mask.length) {

	      // Insert number digit
	      if (isNumber && rawText.length <= mask.length) {
	        numbersOnly = (numbersOnly.slice(0, normalisedStartCaretPosition) + digit + numbersOnly.slice(normalisedStartCaretPosition));
	        newCaretPosition = Math.max(
	          denormaliseCaretPosition(mask, normalisedStartCaretPosition + 1),
	          denormaliseCaretPosition(mask, normalisedStartCaretPosition + 2) - 1
	        );
	      }

	      // Delete
	      if (isDelete) {
	        numbersOnly = (numbersOnly.slice(0, normalisedStartCaretPosition) + numbersOnly.slice(normalisedStartCaretPosition + 1));
	      }

	    }

	    // Backward Action
	    if (caretStart != 0) {

	      // Backspace
	      if(isBackspace && !rangeHighlighted) {
	        numbersOnly = (numbersOnly.slice(0, normalisedStartCaretPosition - 1) + numbersOnly.slice(normalisedStartCaretPosition));
	        newCaretPosition = denormaliseCaretPosition(mask, normalisedStartCaretPosition - 1);
	      }
	    }


	    $(element).val(applyFormatMask(numbersOnly, mask));

	    setCaretPosition(element, newCaretPosition);
	  }
	};



	/**
	 * Get the caret start position of the given element.
	 *
	 * @param element
	 * @returns {*}
	 */
	function caretStartPosition(element) {
	  if(typeof element.selectionStart == "number") {
	    return element.selectionStart;
	  }
	  return false;
	};


	/**
	 * Gte the caret end position of the given element.
	 *
	 * @param element
	 * @returns {*}
	 */
	function caretEndPosition(element) {
	  if(typeof element.selectionEnd == "number") {
	    return element.selectionEnd;
	  }
	  return false;
	};


	/**
	 * Set the caret position of the given element.
	 *
	 * @param element
	 * @param caretPos
	 */
	function setCaretPosition(element, caretPos) {
	  if(element != null) {
	    if(element.createTextRange) {
	      var range = element.createTextRange();
	      range.move('character', caretPos);
	      range.select();
	    } else {
	      if(element.selectionStart) {
	        element.focus();
	        element.setSelectionRange(caretPos, caretPos);
	      } else {
	        element.focus();
	      }
	    }
	  }
	};


	/**
	 * Normalise the caret position for the given mask.
	 *
	 * @param mask
	 * @param caretPosition
	 * @returns {number}
	 */
	function normaliseCaretPosition(mask, caretPosition) {
	  var numberPos = 0;
	  if(caretPosition < 0 || caretPosition > mask.length) { return 0; }
	  for(var i = 0; i < mask.length; i++) {
	    if(i == caretPosition) { return numberPos; }
	    if(mask[i] == "X") { numberPos++; }
	  }
	  return numberPos;
	};


	/**
	 * Denormalise the caret position for the given mask.
	 *
	 * @param mask
	 * @param caretPosition
	 * @returns {*}
	 */
	function denormaliseCaretPosition(mask, caretPosition) {
	  var numberPos = 0;
	  if(caretPosition < 0 || caretPosition > mask.length) { return 0; }
	  for(var i = 0; i < mask.length; i++) {
	    if(numberPos == caretPosition) { return i; }
	    if(mask[i] == "X") { numberPos++; }
	  }
	  return mask.length;
	};



	/**
	 *
	 *
	 * @param e
	 */
	function filterNumberOnlyKey(e) {
	  var isNumber = keyIsNumber(e);
	  var isDeletion = keyIsDeletion(e);
	  var isArrow = keyIsArrow(e);
	  var isNavigation = keyIsNavigation(e);
	  var isKeyboardCommand = keyIsKeyboardCommand(e);
	  var isTab = keyIsTab(e);

	  if(!isNumber && !isDeletion && !isArrow && !isNavigation && !isKeyboardCommand && !isTab) {
	    e.preventDefault();
	  }
	};


	/**
	 *
	 *
	 * @param keyCode
	 * @returns {*}
	 */
	function digitFromKeyCode(keyCode) {

	  if(keyCode >= KEYS["0"] && keyCode <= KEYS["9"]) {
	    return keyCode - KEYS["0"];
	  }

	  if(keyCode >= KEYS["NUMPAD_0"] && keyCode <= KEYS["NUMPAD_9"]) {
	    return keyCode - KEYS["NUMPAD_0"];
	  }

	  return null;
	};



	/**
	 * Get the key code from the given event.
	 *
	 * @param e
	 * @returns {which|*|Object|which|which|string}
	 */
	function keyCodeFromEvent(e) {
	  return e.which || e.keyCode;
	};


	/**
	 * Get whether a command key (ctrl of mac cmd) is held down.
	 *
	 * @param e
	 * @returns {boolean|metaKey|*|metaKey}
	 */
	function keyIsCommandFromEvent(e) {
	  return e.ctrlKey || e.metaKey;
	};


	/**
	 * Is the event a number key.
	 *
	 * @param e
	 * @returns {boolean}
	 */
	function keyIsNumber(e) {
	  return keyIsTopNumber(e) || keyIsKeypadNumber(e);
	};


	/**
	 * Is the event a top keyboard number key.
	 *
	 * @param e
	 * @returns {boolean}
	 */
	function keyIsTopNumber(e) {
	  var keyCode = keyCodeFromEvent(e);
	  return keyCode >= KEYS["0"] && keyCode <= KEYS["9"];
	};


	/**
	 * Is the event a keypad number key.
	 *
	 * @param e
	 * @returns {boolean}
	 */
	function keyIsKeypadNumber(e) {
	  var keyCode = keyCodeFromEvent(e);
	  return keyCode >= KEYS["NUMPAD_0"] && keyCode <= KEYS["NUMPAD_9"];
	};


	/**
	 * Is the event a delete key.
	 *
	 * @param e
	 * @returns {boolean}
	 */
	function keyIsDelete(e) {
	  return keyCodeFromEvent(e) == KEYS["DELETE"];
	};


	/**
	 * Is the event a backspace key.
	 *
	 * @param e
	 * @returns {boolean}
	 */
	 function keyIsBackspace(e) {
	  return keyCodeFromEvent(e) == KEYS["BACKSPACE"];
	};


	/**
	 * Is the event a deletion key (delete or backspace)
	 *
	 * @param e
	 * @returns {boolean}
	 */
	function keyIsDeletion(e) {
	  return keyIsDelete(e) || keyIsBackspace(e);
	};


	/**
	 * Is the event an arrow key.
	 *
	 * @param e
	 * @returns {boolean}
	 */
	function keyIsArrow(e) {
	  var keyCode = keyCodeFromEvent(e);
	  return keyCode >= KEYS["ARROW_LEFT"] && keyCode <= KEYS["ARROW_DOWN"];
	};


	/**
	 * Is the event a navigation key.
	 *
	 * @param e
	 * @returns {boolean}
	 */
	function keyIsNavigation(e) {
	  var keyCode = keyCodeFromEvent(e);
	  return keyCode == KEYS["HOME"] || keyCode == KEYS["END"];
	};


	/**
	 * Is the event a keyboard command (copy, paste, cut, highlight all)
	 *
	 * @param e
	 * @returns {boolean|metaKey|*|metaKey|boolean}
	 */
	function keyIsKeyboardCommand(e) {
	  var keyCode = keyCodeFromEvent(e);
	  return keyIsCommandFromEvent(e) &&
	    (
	      keyCode == KEYS["A"] ||
	      keyCode == KEYS["X"] ||
	      keyCode == KEYS["C"] ||
	      keyCode == KEYS["V"]
	    );
	};


	/**
	 * Is the event the tab key?
	 *
	 * @param e
	 * @returns {boolean}
	 */
	function keyIsTab(e) {
	  return keyCodeFromEvent(e) == KEYS["TAB"];
	};


	/**
	 * Strip all characters that are not in the range 0-9
	 *
	 * @param string
	 * @returns {string}
	 */
	function numbersOnlyString(string) {
	  var numbersOnlyString = "";
	  for(var i = 0; i < string.length; i++) {
	    var currentChar = string.charAt(i);
	    var isValid = !isNaN(parseInt(currentChar));
	    if(isValid) { numbersOnlyString += currentChar; }
	  }
	  return numbersOnlyString;
	};


	/**
	 * Apply a format mask to the given string
	 *
	 * @param string
	 * @param mask
	 * @returns {string}
	 */
	function applyFormatMask(string, mask) {
	  var formattedString = "";
	  var numberPos = 0;
	  for(var j = 0; j < mask.length; j++) {
	    var currentMaskChar = mask[j];
	    if(currentMaskChar == "X") {
	      var digit = string.charAt(numberPos);
	      if(!digit) {
	        break;
	      }
	      formattedString += string.charAt(numberPos);
	      numberPos++;
	    } else {
	      formattedString += currentMaskChar;
	    }
	  }
	  return formattedString;
	};
	function cardFromNumber(num) {
		var card, j, len;
		num = (num + '').replace(/\D/g, '');
		for (j = 0, len = cards.length; j < len; j++) {
		  card = cards[j];
		  if (card.pattern.test(num)) {
		    return card;
		  }
		}
	};

exports.init = function () {
	var billtoAccountVal = $('#dwfrm_synchrony_billToAccountNumber').val();
	if(billtoAccountVal == 'null'){
		$('#dwfrm_synchrony_billToAccountNumber').val('');
	}
	
	initCreditCardValidation();
	initAddressValidation();
	/*if($('.error-form-inside').attr("data-attribute") == 'checkout_fail') {		
		utag.link({eventCategory: 'Checkout',eventLabel: 'Checkout_Fail', eventAction : 'submit'});
	}*/
	
}

$('#customerinfo-next').click(function(){
	if($('#dwfrm_billing').length > 0 && !$('#dwfrm_billing').validate().checkForm()){
		$('#dwfrm_billing').valid();
		return;
	}
	$(this).parents('#primary').addClass("active");
	$('.nav-tabs li').removeClass('active').siblings().next().addClass('active');
	$('.nav-tabs li:first-child').click(function(){
		$(this).addClass('active').siblings().removeClass('active');
		$(this).parents('#primary').removeClass('active');
	})
});
// Gray out everything and show the loader bar on clicking submit order.
$('.submitorder-for-payment').click(function(){
	var firstID = $('input[id$=_salesAssociateName1]').val();
	var secondID = $('input[id$=_salesAssociateName2]').val();
	if(firstID != '' && secondID != ''){
		if(firstID.length < 6 || secondID.length < 6){
			$('.submitorder-for-payment').attr('disabled', true);
			return;
		}
		if(firstID == secondID){
			$('#operatorid-invalid').text(Resources.SAME_OPERATOR_ERROR);
			$('.submitorder-for-payment').attr('disabled', true);
			return;
		}
	}else if(firstID != ''){
		if(firstID.length < 6){
			$('.submitorder-for-payment').attr('disabled', true);
			return;
		}
	}else if(secondID != ''){
		if(firstID.length < 6){
			$('.submitorder-for-payment').attr('disabled', true);
			return;
		}
	}
	if(firstID != '' && secondID != ''){
		var url = util.appendParamsToUrl(Urls.validateOperatorID, {firstID: firstID, secondID: secondID , format: 'ajax'});
	}else if(firstID != ''){
		var url = util.appendParamsToUrl(Urls.validateOperatorID, {firstID: firstID, format: 'ajax'});
	}else if(secondID != ''){
		var url = util.appendParamsToUrl(Urls.validateOperatorID, {secondID: secondID , format: 'ajax'});
	}
	$.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        async: false,
        success: function (data) {
            if(data.success) {
            	//page.refresh();
				progress.show('.main');
            } else if (!data.success) {
            	$('#operatorid-invalid').text(data.message);
                $('.submitorder-for-payment').attr('disabled', true);
            }
        }
    });
});

// show sales associate suggestions
$(document).on('keyup', '#dwfrm_singleshipping_salesAssociateFields_salesAssociateName1, #dwfrm_singleshipping_salesAssociateFields_salesAssociateName2', function() {
	var salesAssociateField = $(this);
	var suggestionsElementParent = salesAssociateField.parent().parent().next('.suggestion-div');
	var suggestionsElement = $(suggestionsElementParent).find('.sales-associate-suggestions');
	if (this.value.length === 0) {
		$(suggestionsElementParent).hide();
		$(suggestionsElement).html('');
	} else {
		var url = util.appendParamsToUrl(Urls.getProfileSuggestions, {searchValue: this.value, format: 'ajax'});
		var suggestions = '';
	    $.getJSON(url, function (data) {
	        var fail = false;
	        var msg = "";
	        if (!data || data === null) {
	            msg = Resources.BAD_RESPONSE;
	            fail = true;
	            console.log('Error');
	            console.log(msg);
	        } else {
	        	if (data.length > 0) {
	        		for (var __i = 0; __i < data.length; __i++) {
		        		suggestions += '<div data-value="' + data[__i].username + '">' + data[__i].name + " - " + data[__i].username + '</div>';
		        		if (__i >= 10) {
		        			break;
		        		}
		        	}
		        	$(suggestionsElement).html(suggestions);
		        	$(suggestionsElementParent).show();
		        	
		        	$(suggestionsElement).children('div').each(function() {
		        		$(this).on('mouseover', function() {
			        		salesAssociateField.val($(this).data('value'));
			        	});
		        	});
		        	
	        	} else {
	        		$(suggestionsElementParent).hide();
	        		$(suggestionsElement).html('');
	        	}
	        }
	    });
	}
});

$(document).on('focus', '#dwfrm_singleshipping_salesAssociateFields_salesAssociateName1, #dwfrm_singleshipping_salesAssociateFields_salesAssociateName2', function() {
	this.value = '';
});

$(document).on('blur', '#dwfrm_singleshipping_salesAssociateFields_salesAssociateName1, #dwfrm_singleshipping_salesAssociateFields_salesAssociateName2', function() {
	var suggestionsElementParent = $(this).parent().parent().next('.suggestion-div');
	var suggestionsElement = $(suggestionsElementParent).find('.sales-associate-suggestions');
	$(suggestionsElementParent).hide();
	$(suggestionsElement).html('');
});