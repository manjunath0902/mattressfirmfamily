'use strict';

var giftcert = require('./../giftcert'),
	tooltip = require('./../tooltip'),
	util = require('./../util'),
	dialog = require('./../dialog'),
	page = require('./../page'),
	validator = require('../validator'),
	formPrepare = require('./checkout/formPrepare');
	//Rules = require('./../validation-rules');

/**
 * @function
 * @description Initializes the events on the address form (apply, cancel, delete)
 * @param {Element} form The form which will be initialized
 */
function initializeAddressForm() {
	var $form = $('#edit-address-form');

	$form.find('input[name="format"]').remove();
	tooltip.init();
	//$("<input/>").attr({type:"hidden", name:"format", value:"ajax"}).appendTo(form);

	$("#dwfrm_profile_address_states_state").change(function () {
		console.log($("#dwfrm_profile_address_states_state :selected").text());
		$("#dwfrm_profile_address_states_state-span").text($("#dwfrm_profile_address_states_state :selected").text());
	});

	$("#dwfrm_profile_address_country").on('click', function (e) {
		$("#dwfrm_profile_address_country-span").text($("#dwfrm_profile_address_country :selected").text());
	});

	$form.on('click', '.apply-button', function (e) {
		e.preventDefault();
		if (!$form.valid()) {
			return false;
		}
		var url = util.appendParamToURL($form.attr('action'), 'format', 'ajax');
		var applyName = $form.find('.apply-button').attr('name');
		var options = {
			url: url,
			data: $form.serialize() + '&' + applyName + '=x',
			type: 'POST'
		};
		$.ajax(options).done(function (data) {
			if (typeof(data) !== 'string') {
				if (data.success) {
					dialog.close();
					page.refresh();
				} else {
					window.alert(data.message);
					return false;
				}
			} else {
				$('#dialog-container').html(data);
				account.init();
				tooltip.init();
				util.uniform();
			}
		});
	})
	.on('click', '.cancel-button, .close-button', function (e) {
		e.preventDefault();
		dialog.close();
	})
	.on('click', '.delete-button', function (e) {
		e.preventDefault();
		if (window.confirm(String.format(Resources.CONFIRM_DELETE, Resources.TITLE_ADDRESS))) {
			var url = util.appendParamsToUrl(Urls.deleteAddress, {
				AddressID: $form.find('#addressid').val(),
				format: 'ajax'
			});
			$.ajax({
				url: url,
				method: 'POST',
				dataType: 'json'
			}).done(function (data) {
				if (data.status.toLowerCase() === 'ok') {
					dialog.close();
					page.refresh();
				} else if (data.message.length > 0) {
					window.alert(data.message);
					return false;
				} else {
					dialog.close();
					page.refresh();
				}
			});
		}
	});
	//postal code check. -- dwfrm_singleshipping_shippingAddress_addressFields_postal
	$('input.postal').first().keyup(function () {
		var $postalcodetmp = this.value;
		IsValidZipCode($postalcodetmp);
		//console.log('test' + IsValidZipCode($postalcodetmp));
	});
	validator.init();
}
/**
 * @private
 * @function
 * @description Toggles the list of Orders
 */
function toggleFullOrder () {
	$('.order-items')
		.find('li.hidden:first')
		.prev('li')
		.append('<a class="toggle">View All</a>')
		.children('.toggle')
		.click(function () {
			$(this).parent().siblings('li.hidden').show();
			$(this).remove();
		});
}
/**
 * @private
 * @function
 * @description Binds the events on the address form (edit, create, delete)
 */
function initAddressEvents() {
	var addresses = $('#addresses');
	if (addresses.length === 0) { return; }

	addresses.on('click', '.address-edit, .address-create', function (e) {
		var titleclass = $(this).attr('class');
		var titleclasscreate = titleclass.indexOf("address-create");
		var titleclassedit = titleclass.indexOf("address-edit");
		if (titleclassedit == true) {
			var addresstitle =  Resources.ADDRESSEDIT_TITLE;
		} else {
			var addresstitle =  Resources.ADRESSCREATE_TITLE;
		}
		e.preventDefault();
		dialog.open({
			url: this.href,
			options: {
				title: addresstitle,
				open: initializeAddressForm,
				width: 630
			},
			callback: function () {
				util.uniform();
			}
		});
	}).on('click', '.delete', function (e) {
		e.preventDefault();
		if (window.confirm(String.format(Resources.CONFIRM_DELETE, Resources.TITLE_ADDRESS))) {
			$.ajax({
				url: util.appendParamToURL($(this).attr('href'), 'format', 'ajax'),
				dataType: 'json'
			}).done(function (data) {
				if (data.status.toLowerCase() === 'ok') {
					page.redirect(Urls.addressesList);
				} else if (data.message.length > 0) {
					window.alert(data.message);
				} else {
					page.refresh();
				}
			});
		}
	});
}

function IsValidZipCode(zip) {
	var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
	if (!isValid) {
		if ($(".postal").parent().find(".error").length < 1) {
			//$(".postal .field-wrapper").append("<span class='error'>" + Resources.INVALID_ZIP + "</span>");
			$(".postal").parent().append("<span class='error'>" + Resources.INVALID_ZIP + "</span>");
			$(".postal").addClass("error");
			$("#edit-address-form").find('button').attr("disabled", "disabled");
		}
		return false;
	} else {
		if ($(".postal").parent().find(".error").length > 0) {
			$(".postal").parent().find("span.error").remove();
			$(".postal").removeClass("error");
			$("#edit-address-form").find('button').removeAttr("disabled", "disabled");
		}
		return true;
	}
}
/**
 * @private
 * @function
 * @description Binds the events of the payment methods list (delete card)
 */
function initPaymentEvents() {
	$('.add-card').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href')
		});
	});

	var paymentList = $('.payment-list');
	if (paymentList.length === 0) { return; }

	util.setDeleteConfirmation(paymentList, String.format(Resources.CONFIRM_DELETE, Resources.TITLE_CREDITCARD));

	$('form[name="payment-remove"]').on('submit', function (e) {
		e.preventDefault();
		// override form submission in order to prevent refresh issues
		var button = $(this).find('.delete');
		$('<input/>').attr({
			type: 'hidden',
			name: button.attr('name'),
			value: button.attr('value') || 'delete card'
		}).appendTo($(this));
		var data = $(this).serialize();
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: data
		})
		.done(function () {
			page.redirect(Urls.paymentsList);
		});
	});
}
/**
 * @private
 * @function
 * @description init events for the loginPage
 */
function initLoginPage() {
	//o-auth binding for which icon is clicked
	$('.oAuthIcon').bind('click', function () {
		$('#OAuthProvider').val(this.id);
	});

	//toggle the value of the rememberme checkbox
	$('#dwfrm_login_rememberme').bind('change', function () {
		if ($('#dwfrm_login_rememberme').attr('checked')) {
			$('#rememberme').val('true');
		} else {
			$('#rememberme').val('false');
		}
	});
	$('#password-reset').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href'),
			options: {
				title: Resources.FORGOTPASSWORD_TITLE,
				open: function () {
					validator.init();
					var $requestPasswordForm = $('[name$="_requestpassword"]'),
						$submit = $requestPasswordForm.find('[name$="_requestpassword_send"]');
						$($submit).on('click', function (e) {
							if (!$requestPasswordForm.valid()) {
								return;
							}
							e.preventDefault();
							dialog.submit($submit.attr('name'));
						});
				}
			}
		});
	});
}

/**
 * @private
 * @function
 * @description Binds the events of the order, address and payment pages
 */
function initializeEvents() {
	toggleFullOrder();
	initAddressEvents();
	initPaymentEvents();
	initLoginPage();
}

var account = {
	init: function () {
		initializeEvents();
		giftcert.init();
	},
	initCartLogin: function () {
		initLoginPage();
	}
};

module.exports = account;

//Login Form Popup
$(document).on('click', 'a#alreadycustomer',function (e) {
    e.preventDefault();
    var login_url = encodeURI($(this).attr('href')+'?format=ajax');

	$.ajax({
		url: login_url,
		type: 'GET'
	})
	// success
	.done(function (response) {
		dialog.open({
			html: $(response),
			options: {
				autoOpen: true,
				dialogClass: 'data-class signin-popup',
				title: 'Sign in to your Mattress Firm Account'
			}
		});
	})
	.error(function (xhr, status, error) {
    });
});

$(document).on('click', '#loginPopupSubmitButton', function (e) {
	e.preventDefault();
	var $signinFormLogin = $('.login-popup-email-class').find(':input').val();
	var $signinFormPassword = $('.login-popup-password-class').find(':password').val();

	var url = util.appendParamsToUrl(Urls.loginFromPopupOnCheckout, {username: $signinFormLogin, password: $signinFormPassword, format: 'ajax'});
	$.getJSON(url, function (data) {
		var fail = false;
		var msg = '';
		if (!data) {
			msg = Resources.BAD_RESPONSE;
			fail = true;
		} else if (!data.success) {
			fail = true;
		}
		if (fail) {
			var $signinForm = $('#dwfrm_login');
			if ($signinForm.find(".error").length < 1) {
				$signinForm.append("<span class='error' tabindex='0'>" + Resources.INVALID_CREDENTIALS + "</span>");
			}
			return;
		}
		if (data.success) {
			var $signinForm = $('#dwfrm_login');
			if ($signinForm.find(".error").length > 0) {
				$signinForm.find("span.error").remove();
			}
			page.redirect(Urls.billing);
		}
	});
});

function IsValidEmail(email) {
	var isValid = /^[\w.%+-]+@[\w.-]+\.[\w]{2,6}$/.test(email);
	//var $signinForm = $('#dwfrm_login').children().find(':input');
	var $signinFormLogin = $('.login-popup-email-class');
	var $signinFormButton = $('#dwfrm_login').find(':button[type=submit]');
	if (!isValid) {
		if ($signinFormLogin.find(".error").length < 1) {
			$signinFormLogin.append("<span class='error' tabindex='0'>" + Resources.INVALID_EMAIL + "</span>");
			$signinFormButton.attr("disabled", "disabled");
		}
		return false;
	} else {
		if ($signinFormLogin.find(".error").length > 0) {
			$signinFormLogin.find("span.error").remove();
			if($('.login-popup-password-class').find(':password').val().length != 0) { /*Check if Password is not empty*/
				$signinFormButton.removeAttr("disabled", "disabled");
			}
		}
		return true;
	}
}

$(document).on('keyup input change paste', '.login-popup-email-class', function() {
	var $popupEmail = $(this).children().find(':input').val();
	IsValidEmail($popupEmail);
});

$(document).on('keyup input change paste', '.login-popup-password-class', function() {
	var $popupPassword = $(this);
	var $signinFormButton = $('#dwfrm_login').find(':button[type=submit]');
	if ($popupPassword.find(':password').val().length == 0) {
		if ($popupPassword.find(".error").length < 1) {
			$popupPassword.append("<span class='error' tabindex='0'>" + Resources.EMPTY_PASSWORD + ".</span>");
			$signinFormButton.attr("disabled", "disabled");
		}
	} else {
		if ($popupPassword.find(".error").length > 0) {
			$popupPassword.find("span.error").remove();
		}
	}
	if($popupPassword.find(".error").length == 0) {
		if (IsValidEmail($('.login-popup-email-class').find(':input').val())) { /*Check if email is also valid*/
			$signinFormButton.removeAttr("disabled", "disabled");
		}
	}
});

//Reset Password Popup
$(document).on('click', 'a#password-reset-popup',function (e) {
	e.preventDefault();
	var new_url = $(e.target).attr('href')+'&format=ajax';
	dialog.open({
		url: new_url,
		options: {
			dialogClass: 'forgotpassword-popup',
			title: Resources.FORGOTPASSWORD_TITLE,
			open: function () {
				validator.init();
				var $requestPasswordForm = $('[name$="_requestpassword"]'),
					$submit = $requestPasswordForm.find('[name$="_requestpassword_send"]');
					$($submit).on('click', function (e) {
						if (!$requestPasswordForm.valid()) {
							return;
						}
						e.preventDefault();
						dialog.submit($submit.attr('name'));
					});
			}
		}
	});
	$(document).on('click', '#closethiswindow', function(){
	    dialog.close();
	});
});
$(document).ready(function(){
	// If we have backend validation error messages then remove them on focus out.
	$('#dwfrm_login_username,#dwfrm_login_storeid').on('focusout',function(){
	    	if ( $(this).parent().next('.error-message').length > 0 ) {
	    		$('.error-message').remove();
	    		$(this).parent().parent().hasClass('error') ? $(this).parent().parent().removeClass('error') : null;
	    	}
	});
	// Restricting user to not enter more than 6 digit
	$('#dwfrm_login_username,#dwfrm_login_storeid').on("keypress keyup",function (event) {
		var allowedKeyCodes = [8,48,49,50,51,52,53,54,55,56,57];
		// check if text is selected then allow the input and restrict others. else check other critera and prevent default
		if (window.getSelection().toString() && allowedKeyCodes.indexOf(event.which)!=-1) {
			return true;
		} else if (allowedKeyCodes.indexOf(event.which)==-1 || $(this).val().length >= 6) {
			event.preventDefault();
			return false;
		}
	});
});