'use strict';

var account = require('./account'),
    bonusProductsView = require('../bonus-products-view'),
    quickview = require('../quickview'),
    cartStoreInventory = require('../storeinventory/cart'),
    util = require('../util'),
    page = require('../page'),
    dialog = require('../dialog');	

/**
 * @private
 * @function
 * @description Binds events to the cart page (edit item's details, bonus item's actions, coupon code entry)
 */
function initializeEvents() {
	
    $('#cart-table').on('click', '.item-edit-details a', function (e) {
        e.preventDefault();
        quickview.show({
            url: e.target.href,
            source: 'cart'
        });
    })
    .on('click',  '.item-details .bonusproducts a', function (e) {
        e.preventDefault();
        bonusProductsView.show(this.href);
    });
    $('#cart-items-form').on('click', '.bonus-item-actions a', function (e) {
        e.preventDefault();
        bonusProductsView.show({
        	url: this.href
        });
    });
    
    // override enter key for coupon code entry
    $('form input[name$="_couponCode"]').on('keydown', function (e) {
        if (e.which === 13 && $(this).val().length === 0) { return false; }
    });

    //to prevent multiple submissions of the form when removing a product from the cart
    var removeItemEvent = false;
    $('button[name$="deleteProduct"]').on('click', function (e) {
        if (removeItemEvent) {
            e.preventDefault();
        } else {
            removeItemEvent = true;
        }
    });
    if (!util.isMobileSize()) {
        $('.cart-footer-slot').appendTo('.cart-footer');
    }
    /*$('.quantity-dropdown').on('change', function () {
        $('#update-cart').trigger('click');
    });*/
    //MAT-557 binding event when coupon code response renders on cart
    // if this code uncommented then comment50 to 52 line number
    /*$('.primary-content').on('change','.quantity-dropdown', function () {
        $('#update-cart').trigger('click');
    });*/
    // 
	 $('.btn-minus-qty').click(function(e){
	        // Stop acting like a button
	        e.preventDefault();        
	        var currentVal=parseInt($(this).parent().find('#Quantity').val());
	        if(currentVal==1)
	        	return;
	        if (!isNaN(currentVal)) {
	            // Increment
	        	$(this).parent().find('#Quantity').val(currentVal - 1);
	            $('#update-cart').trigger('click');
	        } else {
	            // Otherwise put a 0 there
	        	$(this).parent().find('#Quantity').val(0);
	        }
	    });
	    
	    $('.btn-plus-qty').click(function(e){
	        // Stop acting like a button
	        e.preventDefault();        
	        var currentVal=parseInt($(this).parent().find('#Quantity').val());
	        if(currentVal==20)
	        	return;
	        if (!isNaN(currentVal)) {
	            // Increment
	        	$(this).parent().find('#Quantity').val(currentVal + 1);
	            $('#update-cart').trigger('click');
	        } else {
	            // Otherwise put a 0 there
	        	$(this).parent().find('#Quantity').val(0);
	        }
	    });
    $('.primary-content').on('click', '.bonus-item-actions a', function (e) {
        e.preventDefault();
        var $bonusProduct = $('#bonus-product-dialog');
        //bonusProductsView.show(this.href);
        var dialogWidth = $(this).parent().prev().val() || 650;
        // 'dialogueWidth' is passed as a paramater in the href of anchor tag (Choice of Bonus Product button) from cart.isml
        var dwidth = util.getParamValueFromURL($(this).attr('href'), 'dialogueWidth');
		if (dwidth != '') {
			dialogWidth = Number(dwidth);
		}
		// create the dialog
		dialog.open({
			target: $bonusProduct,
			url: this.href,
			options: {
				width: dialogWidth,
				title: Resources.BONUS_PRODUCTS
			},
			callback: function () {
				initializeGrid();
				hideSwatches();
				util.uniform();
				$(".ui-dialog-title").show();
				$(".ui-dialog").addClass('bonus-product-popup');
				amplience.initManyProductImages("", $('.col-1'));
				$(".ui-dialog").find('a.continue').on('click', function (e) {
					e.preventDefault();
					$('.ui-dialog-titlebar-close').trigger('click');				
				});
				$(".ui-dialog").find('.ui-icon-closethick').on('click', function (e) {
					e.preventDefault();					
					//page.refresh();
				});
			}
		});
    });
    //for bonus product button
    if($('.cart-btns-holder .bonus-item-actions').length == 0) {
		$('tr.cart-row-btns').css('display' , 'none');
	}
    $(document).on('keypress keyup','.discount-price-inputfield input',function(event){
		/*var allowedKeyCodes = [8,46,48,49,50,51,52,53,54,55,56,57,190];
		if (allowedKeyCodes.indexOf(event.which)==-1 || $(this).val().length >= 7) {
		event.preventDefault();
		return false;
		}*/
		var allowedKeyCodes = [8,46,48,49,50,51,52,53,54,55,56,57];
		// check if text is selected then allow the input and restrict others. else check other critera and prevent default
		if (window.getSelection().toString() && allowedKeyCodes.indexOf(event.which)!=-1) {
			return true;
		} else if (allowedKeyCodes.indexOf(event.which)==-1 || $(this).val().length >= 8) {
		event.preventDefault();
		return false;
		}
		});
    $(document).on('focusout','.discount-price-inputfield input',function(){
		var salePrice = Number($(this).attr('max'));
		var leastDiscountPrice = Number($(this).attr('min'));
		var discountedPrice = Number($(this).val());
		var selectedInput = $(this).attr('name');
		
		var elementToAppend = '<span id="discount-error" class="error"></span>';
		var errorElement = $('#discount-error');
		if (discountedPrice > salePrice || discountedPrice < 0) {
			// change button type to restrict for submission.
			$("button[name='dwfrm_cart_applyDiscounts']").attr('type','button');
			if (!$("input[name='"+selectedInput+"']").next('#discount-error').length) {
	            $("input[name='"+selectedInput+"']").after(elementToAppend);
	            $("input[name='"+selectedInput+"']").addClass('error');
	            $("input[name='"+selectedInput+"']").next('#discount-error').text(Resources.DISCOUNT_RANGE_MESSAGE+salePrice);
			} else {
				$("input[name='"+selectedInput+"']").next('#discount-error').text(Resources.DISCOUNT_RANGE_MESSAGE+salePrice);
			}
//            $("input[name='"+selectedInput+"']").val($("input[name='"+selectedInput+"']").next().val());

				//wrongdiscount-dialog
				/*$('<div id="wrongzipdialog"></div>')
				.appendTo('body')
			    .html('<p>'+ Resources.DISCOUNT_RANGE_MESSAGE + ' ' + leastDiscountPrice + ' and ' + salePrice +'.</p>')
			    .dialog({
			    	dialogClass: "wrong-discount",
			    	modal: true,
			        zIndex: 10000,
			        autoOpen: true,
			        width: 'auto',
			        modal: true,
			        resizable: false,
			        closeOnEscape: false,
			        open: function(event, ui) {
			            $(".wrong-discount .ui-dialog-titlebar-close").hide();
			            $(".wrong-discount .ui-dialog-titlebar.ui-widget-header.ui-corner-all.ui-helper-clearfix").hide();
			        },
			        buttons: {
			          OK: function() {
			            $(this).dialog("close");
			            $(this).remove();
			            $("input[name='"+selectedInput+"']").val($("input[name='"+selectedInput+"']").next().val());
			          }
			        },
			        close: function(event, ui) {
			          $(this).remove();
			        }
			      });*/
		} else {
			if ($("input[name='"+selectedInput+"']").next('#discount-error').length) {
				$("input[name='"+selectedInput+"']").next('#discount-error').remove();
	            $("input[name='"+selectedInput+"']").removeClass('error');
			}
			!$('#discount-error').length ? $("button[name='dwfrm_cart_applyDiscounts']").attr('type','submit') : null;
		} 
    });
    // Add Discount on Cart Page
    $(document).on('click', '#cart-add-discount', function(e){
    	e.preventDefault(); 
        var url = Urls.addDiscount;
        url = util.appendParamToURL(url, 'format', 'ajax');
        dialog.open({
            url: url,
            options: {
                width: '80%',
                height: 'auto',
                closeOnEscape: false,
                dialogClass: 'car-add-discount'
            },
            callback: function () {
            }
        });
    });
}

var addPersonaliCartRescue = function (netotiate_offer_id) {
	var options = {
		url: Urls.addCartRescuePriceAdjustments,
		data: {netotiateOfferId: netotiate_offer_id},
		type: 'POST'
	};
    $.ajax(options).done(function (data) {
        if (typeof(data) !== 'string') {
            if (data.success) {
                //dialog.close();
                //page.refresh();
            } else {
                window.alert(data.message);
                return false;
            }
        } else {
            console.log(data);
            //dialog.close();
            page.refresh();
        }
    });
}
var cart = {
	init : function () {
    initializeEvents();
    var $addCoupon = $('#add-coupon');
	var $couponCode = $('input[name$="_couponCode"]');
	var $checkoutForm = $('.checkout-billing');
    if (SitePreferences.STORE_PICKUP) {
        cartStoreInventory.init();
    }
    account.initCartLogin();
    $("body").on('click', '.change-store', function (e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href'),
            title: $(e.target).attr('title'),
            options: {
                height: 600
            }
        });
    });
    $("body").on('click', '.select-store', function (e) {
        var selectedStore = $(e.target).val();
        setPreferredStore(selectedStore);
        $("div[class*='store']").removeClass('selected');
        $('.store_' + selectedStore).addClass('selected');
        $(".select-store:contains('Preferred Store')").closest('button').first().text('Select Store');
        $(e.target).text('Preferred Store');
    });

    $('body').on('submit', '#dwfrm_pickupinstore', function (e) {
        e.preventDefault();
        // serialize the form and get the post url
        var buttonName = $('#dwfrm_pickupinstore').find('.pickupinstore').attr('name');
        var options = {
            url: Urls.pickupInStore,
            data: $('#dwfrm_pickupinstore').serialize() + '&' + buttonName + '=x',
            type: 'POST'
        };
        $.ajax(options).done(function (data) {
            if (typeof(data) !== 'string') {
                if (data.success) {
                    //dialog.close();
                    //page.refresh();
                } else {
                    window.alert(data.message);
                    return false;
                }
            } else {
                $('#dialog-container').html(data);
                //dialog.close();
                //page.refresh();
            }
        });

    });   
    
    $(document).on("click","#showCouponDisclaimerMsgMobile", function(e){
    	e.preventDefault();
    	/*$(this).next().addClass('active');*/
    	 var $html = $(this).next().find('.couponDisclaimerMsg').children('.promo-modal').html();
    	 dialog.open({
				html: $html,
				options: {
					autoOpen: false,
					dialogClass: 'promo-modal-class'
					
				}
			});
    });
    
    /*$(document).on("click","#hideCouponDisclaimerMsg-mob", function(e){
    	e.preventDefault();
    	$(this).closest('.mobileCouponDisclaimerMsg-popup').removeClass('active');
    });*/
    
    $(document).on("click","#showCouponDisclaimerMsg", function(e){
    	e.preventDefault();
        $('.couponDisclaimerMsg').slideDown("slow");
    });
     
    $(document).on("click","#hideCouponDisclaimerMsg", function(e){
    	e.preventDefault();
    	$('.couponDisclaimerMsg').slideUp("slow");
    });
    
    //MAT-557 coupon code default behavior to ajax
  $(".primary-content").on('click',"#add-coupon",  function (e) {
		e.preventDefault();	
		 addHiddenInput( this.name );
    	 submitCartAction(e);
	});

	// trigger events on enter
   $(".primary-content").on('keydown',$couponCode, function (e) {
	   e.stopPropagation();
	   if (e.which === 13) {
			e.preventDefault();
			$("#add-coupon").click();
		}
	});
   
   // open First Selection Program Modal
	$('.select-fsp-items').unbind('click'); 
	$('.select-fsp-items').on('click', function (e) {
		e.preventDefault();
		cart.openFSPDialog({
		    	url: this.href
		});
	});
	
	// Remove price adjustment from cart
	$(document).on('click',".remove-fsp-discount",function(e){
		e.preventDefault();
		var productID = $(this).attr('data-pid');
		if(productID.length > 0) {
			var url = util.appendParamsToUrl(Urls.removeFSPPriceAdjustments, {productID: productID});
	 		
			// make the server call
	 		$.ajax({
	 			type: 'POST',
	 			dataType: 'json',
	 			cache: false,
	 			contentType: 'application/json',
	 			url: url
	 		})
	 		.done(function () {
	 			page.refresh();
	 		})
	 		.fail(function (xhr, textStatus) {
	 			// failed
	 			if (textStatus === 'parsererror') {
	 				window.alert(Resources.BAD_RESPONSE);
	 			} else {
	 				window.alert(Resources.SERVER_CONNECTION_ERROR);
	 			}
	 		});
		}
	});
	
/* 
 // This code has been written in app.js because it was needed on multiple pages
 //Add Promo Code on Order Summary
   $('.primary-content').on('click', '#add-promo-code-label-id', function (e) {
   	e.preventDefault();
   	$('#add-promo-code-label-id').addClass('visually-hidden');
   	$('#add-promo-code-input-id').removeClass('visually-hidden');
   });
*/   
   
// #coupon-success-message is the id of overlay which comes after bonus product addition in basket via coupon code.
   $('#coupon-success-message').on('click tap', function (e) {
       e.preventDefault();
       $('#coupon-success-message').hide();
   });
	//#promo-success-message is the id of overlay which comes after discount applied on basket via coupon code.
   $('#promo-success-message').on('click tap', function (e) {
	   	e.preventDefault();
	   	$('#promo-success-message').hide();
   });

	var submitCartAction  = function (e){
		e.preventDefault();
		var $form =  $('#cart-items-form');
		var ajaxdest = $form.attr('action');
		var url = util.appendParamsToUrl(ajaxdest, {format: 'ajax'});
		var formData = $(":input").serialize(); //$form.serialize();
		$.post( url, formData )
		.done(function( text ){
			var $response = $( text );
			var divcontent = $('.item-quantity.item-quantity-details');
			$(".primary-content").html($response);
			if($('#couponError').val() == "false") {
				page.refresh();
			}
			/*if($('#couponApplied').length > 0 && $('#couponApplied').val() == "true" && $('#couponError').val() == "false") {
				if($('#couponType').val() == "BONUS") {
					$(function(){
						page.refresh();
						// #coupon-success-message is the id of overlay which comes after bonus product addition in basket via coupon code.
						$('#coupon-success-message').show();
					    setTimeout(function() {
						    $('#coupon-success-message').fadeOut('fast');
						    page.refresh();
						}, 2000);
					});
				} else {
					$(function(){
						page.refresh();
						//#promo-success-message is the id of overlay which comes after discount applied on basket via coupon code.
						$('#promo-success-message').show();
						setTimeout(function() {
						    $('#promo-success-message').fadeOut('fast');
						    page.refresh();
						}, 2000);
					});
				}
			}*/
			$(".primary-content").find('.item-quantity.item-quantity-details').each(function(e) {
				$(this).html($(divcontent[e]).html());
			});			
			$("#checkout-button-ID").removeClass('disabled-btn');
		});	
	};
	function addHiddenInput( name ){
		var $form =  $('#cart-items-form');
	    $form.append( "<input type='hidden' class='form_submittype' name='" + name + "' value='true'>" );
	  }
	

    $("body").on('click', '.continue-with-select-store', function (e) {
        var selectedStore = $('.selected').attr('data-storeid');
        var pid = $('input[name="pid"]').val();
        var format = 'ajax';
        addProductSelectStore(selectedStore, pid, format);
    });
    global.addPersonaliCartRescue = addPersonaliCartRescue;
},

openFSPDialog: function (params) {
	
	var $fspProduct = $('#fsp-product-dialog');
	// create the dialog
	dialog.open({
		target: $fspProduct,
		url: params.url,
		options: {
			width: 'auto', 
			maxWidth: 920,
			height: 'auto',				
			modal: true,
			fluid: true, 
			resizable: false,
			draggable: true,
			responsive: true,
			autoResize:true,
			open: function () {
				
				$(".fsp-checkboxes").each(function() {
					if($(this).attr("data-optFirstSelectionProgram").length > 0) {
						var optFirstSelectionProgram = $(this).attr("data-optFirstSelectionProgram");
						if(optFirstSelectionProgram == "true") { 
							$(this).prop('checked', 'checked');
			            } else if(optFirstSelectionProgram == "false"){
			            	$(this).removeAttr('checked');
			            }
					}
		        });
			}
			
		},
		callback: function () {
			cart.initFSPModalEvents();
			util.uniform();
			$(".ui-dialog").addClass('fsp-discount-popup');
		}
	});			
},

checkStatusOfCheckboxes: function() {
	var isCheckBoxesChecked = false;
	$(".fsp-checkboxes").each(function() {
		if($(this).is(':checked')) {
			isCheckBoxesChecked = true;
			return false;
        }
    });
	return isCheckBoxesChecked;
},

checkFSPValidations: function(origin) {
	   var isCheckBoxesChecked = cart.checkStatusOfCheckboxes();
	   var isTermsConditionsChecked = $('#termsandconditions').is(':checked');
	   if(isTermsConditionsChecked && isCheckBoxesChecked){
		   $("#fsp-modal-button").removeClass('btn-fsp-disabled');
		   $("#fsp-modal-button").addClass('btn-default');
		   $(".checkbox-terms-conditions").removeClass('error');
		   $(".fsp-submit-button").removeClass('error');
		   return true;
	   }else if(isTermsConditionsChecked && !isCheckBoxesChecked){
		   $("#fsp-modal-button").removeClass('btn-default');
		   $("#fsp-modal-button").addClass('btn-fsp-disabled');
		   $(".checkbox-terms-conditions").removeClass('error');
		   $(".fsp-submit-button").removeClass('error');
		   return false;
	   } 
	   else {
		   $("#fsp-modal-button").removeClass('btn-default');
		   $("#fsp-modal-button").addClass('btn-fsp-disabled');
		   if(origin == 'terms' || origin == 'button') {
			   $(".checkbox-terms-conditions").addClass('error');
			   $(".fsp-submit-button").addClass('error');
		   }
		   return false;
	   }
	   return;
},

initFSPModalEvents: function () {

		$('#fsp-modal-button').on('click', function (e) {
	    	e.preventDefault();
	    
	    	var validationStatus = cart.checkFSPValidations('button');
	    	
	    	if(validationStatus){
	    		
	    	    $("#fsp-modal-button").removeClass('btn-default');
	  		    $("#fsp-modal-button").addClass('btn-fsp-disabled');
	  		    
		    	var fspProdctsArray = [];
				$(".fsp-checkboxes").each(function() {
					if($(this).is(':checked')) {
						var productId = $(this).attr("data-pid");
						var fspdiscount = $(this).attr("data-fspdiscount");	
						
						var fspObject = new Object();
						fspObject.productId = productId;
						fspObject.fspdiscount = fspdiscount;
						fspProdctsArray.push(fspObject);
						
		            }
		        });
				
		 		// make the server call
		 		$.ajax({
		 			type: 'POST',
		 			dataType: 'json',
		 			cache: false,
		 			contentType: 'application/json',
		 			url: Urls.addFSPPriceAdjustments,
		 			data: JSON.stringify(fspProdctsArray)
		 		})
		 		.done(function () {
		 			dialog.close();
		 			page.refresh();
		 		})
		 		.fail(function (xhr, textStatus) {
		 			// failed
		 			if (textStatus === 'parsererror') {
		 				window.alert(Resources.BAD_RESPONSE);
		 			} else {
		 				window.alert(Resources.SERVER_CONNECTION_ERROR);
		 			}
		 		});
	    	}
	    });
		
		$('#termsandconditions').click(function () {
			cart.checkFSPValidations('terms');
		});
		
		$('#no-thanks').click(function () {
			dialog.close();
		});
		
		$(document).on('change', '[type=checkbox]', function(event) {
			cart.checkFSPValidations('product');
		});

}

};
module.exports = cart;