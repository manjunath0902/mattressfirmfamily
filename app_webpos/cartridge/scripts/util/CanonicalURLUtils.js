'use strict';
importScript( "int_amplience:common/AmplienceSash.ds");
importScript( "int_amplience:common/AmplienceUtils.ds");
var ProductUtils = require('app_storefront_core/cartridge/scripts/product/ProductUtils.js');
var StoreMgr = require('dw/catalog/StoreMgr');

function createCanonical(productSearchResultParam) {
    var productSearchModel = productSearchResultParam;
    var selectedRefinementsMapMeta = new dw.util.HashMap();    
    var parentCategoryName = productSearchModel.category.getParent().displayName;
    var selectedCategoryName = productSearchModel.category.displayName;
    var selectedCategoryId = productSearchModel.category.ID;
    var categoryName = "";
    if(selectedCategoryId == '5637156576') { //sale
    	categoryName = "mattress-sale";
	}else if(selectedCategoryId == '5637146830') { // shop by brand
        categoryName = "mattresses";
    } else if(selectedCategoryId == 'mattress-sizes') {
        categoryName = "Mattresses";
	}else {
		categoryName = selectedCategoryName;
	}
    var canonicalURL = "/" + categoryName + "/";
    if (productSearchModel != null && productSearchModel.refinements != null && productSearchModel.refinements.refinementDefinitions != null) {
        for (var i = 0; i < productSearchModel.refinements.refinementDefinitions.length; i++) {
            var selectedRefinement = productSearchModel.refinements.refinementDefinitions[i];
            if (selectedRefinement.isAttributeRefinement() && productSearchModel.isRefinedByAttribute(selectedRefinement.attributeID)) {
                var selectedRefinementArray = new Array();
                selectedRefinementArray = productSearchModel.refinements.getRefinementValues(selectedRefinement);
                for (var j = 0; j < selectedRefinementArray.length; j++) {
                    var refinementValue = selectedRefinementArray[j];
                    if (productSearchModel.isRefinedByAttributeValue(selectedRefinement.attributeID, refinementValue.value)) {
                        selectedRefinementsMapMeta.put(selectedRefinement.displayName, selectedRefinementArray[j].value);
                    }
                }
            }
        }
    }

    //get keys as per canonical rule
    //for comfort    
    if (selectedRefinementsMapMeta.containsKey("Comfort")) {        
        var uri = selectedRefinementsMapMeta.get("Comfort").replace(/ /g,"-");
        canonicalURL = canonicalURL + encodeURIComponent(uri) + "/";
    } else if (parentCategoryName == 'Comfort') {
        canonicalURL = canonicalURL + categoryName + "/";
    }
    
    // for Brand
    if (selectedRefinementsMapMeta.containsKey("Brands")) {        
        var uri = selectedRefinementsMapMeta.get("Brands").replace(/ /g,"-");
        canonicalURL = canonicalURL + encodeURIComponent(uri) + "/";
    } else if (parentCategoryName == 'Brand') {
        canonicalURL = canonicalURL + categoryName + "/";
    }  

    //for size
    if (selectedRefinementsMapMeta.containsKey("Size")) {
    	var uri = selectedRefinementsMapMeta.get("Size").replace(/ /g,"-");
        canonicalURL = canonicalURL + encodeURIComponent(uri) + "/";
    } else if (parentCategoryName == 'Size') {
        canonicalURL = canonicalURL + categoryName + "/";
    }

    //for type
    if (selectedRefinementsMapMeta.containsKey("Mattress Type")) {        
        var uri = selectedRefinementsMapMeta.get("Mattress Type").replace(/ /g,"-");
        canonicalURL = canonicalURL + encodeURIComponent(uri) + "/";
    } else if (parentCategoryName == 'Type') {
        canonicalURL = canonicalURL + categoryName + "/";
    }
    
    
    var canonicalURLStructure = canonicalURL.replace(/\s+/g, "-").toLowerCase();
    var result = canonicalURLStructure.split("/");
    var singleFinalCanonical = "/"+result[1]+'/'+result[2]+'/';
    var finalSingleCanonical = "";        
	if(selectedCategoryId == 'web-specials' || 
		 selectedCategoryId == 'New-At-Mattress-Firm' || 
		 selectedCategoryId == 'same-day-delivery' || 
		 selectedCategoryId == 'financing-sale' || 
		 selectedCategoryId == '5637146830') {
             	if(result.length == 7 || result.length == 6 || result.length == 5) {
                	finalSingleCanonical =  "/"+result[1]+'/'+result[2]+'/';
                	return finalSingleCanonical.replace(/\/+/g,"/").replace("'","");
            	}else {        		
            		for(var refinementsValue=0; refinementsValue < result.length; refinementsValue ++) {
            			finalSingleCanonical = finalSingleCanonical+ result[refinementsValue]+"/";
            		}
            		return finalSingleCanonical.replace(/\/+/g,"/").replace("'","");
            	}        		 
		 } else {
			 for(var refinementsValue=0; refinementsValue < result.length; refinementsValue ++) {
					finalSingleCanonical = finalSingleCanonical+ result[refinementsValue]+"/";
				}
				return finalSingleCanonical.replace(/\/+/g,"/").replace("'","");        		 
		 }        
    return "";
}


function createContentAssetId(productSearchResultParam) {

    var productSearchModel = productSearchResultParam;
    var selectedRefinementsMapMeta = new dw.util.HashMap();
    var categoryName = productSearchModel.category.displayName;
    if(categoryName == 'Mattress Sizes') {
        categoryName = "Mattresses";
    }
    var contentAssetId = categoryName+"_";
    var parentCategoryName = productSearchModel.category.getParent().displayName;
    
    var refinementBrandArray = new Array();
    var refinementSizeArray = new Array();
    var refinementTypeArray = new Array();
    var refinementComfortArray = new Array();



    if (productSearchModel != null && productSearchModel.refinements != null && productSearchModel.refinements.refinementDefinitions != null) {
        for (var i = 0; i < productSearchModel.refinements.refinementDefinitions.length; i++) {
            var selectedRefinement = productSearchModel.refinements.refinementDefinitions[i];
            if (selectedRefinement.isAttributeRefinement() && productSearchModel.isRefinedByAttribute(selectedRefinement.attributeID)) {
                var selectedRefinementArray = new Array();
                selectedRefinementArray = productSearchModel.refinements.getRefinementValues(selectedRefinement);
                for (var j = 0; j < selectedRefinementArray.length; j++) {
                    var refinementsValues = selectedRefinementArray[j];
                    if (productSearchModel.isRefinedByAttributeValue(selectedRefinement.attributeID, refinementsValues.value)) {
                        var jsonData = {};
                        jsonData.name = selectedRefinement.displayName;
                        jsonData.value = selectedRefinementArray[j].value

                        if (selectedRefinement.displayName == 'Brands') {
                            refinementBrandArray.push(jsonData);
                        }
                        if (selectedRefinement.displayName == 'Size') {
                            refinementSizeArray.push(jsonData);
                        }
                        if (selectedRefinement.displayName == 'Mattress Type') {
                            refinementTypeArray.push(jsonData);
                        }
                        if (selectedRefinement.displayName == 'Comfort') {
                            refinementComfortArray.push(jsonData);
                        }
                    }
                }
            }
        }
    }

    if (refinementBrandArray && refinementBrandArray.length > 0) {
        for (var i = 0; i < refinementBrandArray.length; i++) {
            contentAssetId = contentAssetId + refinementBrandArray[i].value + "_";
        }
    } 

    if (refinementSizeArray && refinementSizeArray.length > 0) {
        for (var i = 0; i < refinementSizeArray.length; i++) {
            contentAssetId = contentAssetId + refinementSizeArray[i].value + "_";
        }
    } 

    if (refinementTypeArray && refinementTypeArray.length > 0) {
        for (var i = 0; i < refinementTypeArray.length; i++) {
            contentAssetId = contentAssetId + refinementTypeArray[i].value + "_";
        }
    } 

    if (refinementComfortArray && refinementComfortArray.length > 0) {
        for (var i = 0; i < refinementComfortArray.length; i++) {
            contentAssetId = contentAssetId + refinementComfortArray[i].value + "_";
        }
    } 


    //remove spaces and replace wih -
    return contentAssetId.substring(0, contentAssetId.length - 1).replace(/\s+/g, "-").replace(/\&+/g, "and").toLowerCase();
    
}

function createMultipleCanonical(productSearchResultParam) {

    var productSearchModel = productSearchResultParam;
    var selectedRefinementsMapMeta = new dw.util.HashMap();
    var contentAssetId = "";
    
    var parentCategoryName = productSearchModel.category.getParent().displayName;    
    var selectedCategoryName = productSearchModel.category.displayName;
    var selectedCategoryId = productSearchModel.category.ID;
    var categoryName = "";
    if(selectedCategoryId == '5637156576') { //Sale Category
    	categoryName = "mattress-sale";
	}else if(selectedCategoryId == '5637146830') {  //Shop by brand
        categoryName = "mattresses";
    } else if(selectedCategoryId == 'mattress-sizes') {
        categoryName = "Mattresses";
	}else {
		categoryName = selectedCategoryName;
	}
    var multipleCanonicalURL = "/"+categoryName+"/";
    var refinementBrandArray = new Array();
    var refinementSizeArray = new Array();
    var refinementTypeArray = new Array();
    var refinementComfortArray = new Array();



    if (productSearchModel != null && productSearchModel.refinements != null && productSearchModel.refinements.refinementDefinitions != null) {
        for (var i = 0; i < productSearchModel.refinements.refinementDefinitions.length; i++) {
            var selectedRefinement = productSearchModel.refinements.refinementDefinitions[i];
            if (selectedRefinement.isAttributeRefinement() && productSearchModel.isRefinedByAttribute(selectedRefinement.attributeID)) {
                var selectedRefinementArray = new Array();
                selectedRefinementArray = productSearchModel.refinements.getRefinementValues(selectedRefinement);
                for (var j = 0; j < selectedRefinementArray.length; j++) {
                    var refinementsValues = selectedRefinementArray[j];
                    if (productSearchModel.isRefinedByAttributeValue(selectedRefinement.attributeID, refinementsValues.value)) {
                        var jsonData = {};
                        jsonData.name = selectedRefinement.displayName;
                        jsonData.value = selectedRefinementArray[j].value

                        if (selectedRefinement.displayName == 'Brands') {
                            refinementBrandArray.push(jsonData);
                        }
                        if (selectedRefinement.displayName == 'Size') {
                            refinementSizeArray.push(jsonData);
                        }
                        if (selectedRefinement.displayName == 'Mattress Type') {
                            refinementTypeArray.push(jsonData);
                        }
                        if (selectedRefinement.displayName == 'Comfort') {
                            refinementComfortArray.push(jsonData);
                        }
                    }
                }
            }
        }
    }

    var brandValue = "";
    var sizeValue = "";
    var typeValue = "";
    var comfortValue = "";
    if (refinementBrandArray && refinementBrandArray.length == 1) {
    	var uri = refinementBrandArray[0].value.replace(/ /g,"-");
    	brandValue = encodeURIComponent(uri);
    }else {
    	brandValue = "";
    }
    if (refinementSizeArray && refinementSizeArray.length == 1) {
    	var uri = refinementSizeArray[0].value.replace(/ /g,"-");        
    	sizeValue = encodeURIComponent(uri);
    }else {
    	sizeValue = "";
    }
    if (refinementTypeArray && refinementTypeArray.length == 1) {
    	var uri = refinementTypeArray[0].value.replace(/ /g,"-");
    	typeValue = encodeURIComponent(uri);
    }else {
    	typeValue = "";
    }
    if (refinementComfortArray && refinementComfortArray.length == 1) {
    	var uri = refinementComfortArray[0].value.replace(/ /g,"-");
    	comfortValue = encodeURIComponent(uri);
    }else {
    	comfortValue = "";
    }
    multipleCanonicalURL = multipleCanonicalURL+"/"+comfortValue+"/"+brandValue+"/"+sizeValue+"/"+typeValue+"/";
    return multipleCanonicalURL.replace(/\s+/g, "-").toLowerCase().replace(/\/+/g,"/").replace("'","");   
}

function createProductDetailPageLDJsonSchema(productInfo) {	
	var product = productInfo.object;
	if('AmplienceHost' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.AmplienceHost!='') {
		   var AmplienceHost = dw.system.Site.current.preferences.custom.AmplienceHost;
	}
	if('AmplienceClientId' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.AmplienceClientId!='') {
		   var AmplienceId = dw.system.Site.current.preferences.custom.AmplienceClientId;
	}
	var store = getPreferredStore();
	
	var inventoryParam = product.isVariant() ? product.masterProduct.ID : product.ID;	
	
	var isInventoryList = false;
	if (store != null) {
		var inventory = store.inventoryList;
		if (!empty(inventory)) {
			try {
				var record = inventory.getRecord(inventoryParam.replace('mfi',''));
			} catch (e) {
				var error = e;
			}
			if (!empty(record)) {
				isInventoryList = true;
			}
		}
	}
	
	if(product != null && product.isVariant())	{ // for variants
		var pdpJsonSchema = new Object();
		   pdpJsonSchema["@context"] ="https://schema.org";
		   pdpJsonSchema["@type"] = "Product";
		   pdpJsonSchema.name = product.name;
		   
		   //brand node start
		   pdpJsonSchema.brand = new Object(); 
		   pdpJsonSchema.brand["@type"] = "Brand";
		   pdpJsonSchema.brand.name = product.brand;
		   //brand node end
		   
		   pdpJsonSchema.description = product.getShortDescription().toString();
		   pdpJsonSchema.image = request.httpProtocol+'://'+AmplienceHost+'/i/'+AmplienceId+'/'+product.custom.external_id+'.jpg';	   
		   pdpJsonSchema.url = dw.web.URLUtils.abs('Product-Show','pid', product.masterProduct.ID).toString();
		   pdpJsonSchema.sku = product.masterProduct.ID;
		   if(product.UPC){ pdpJsonSchema.gtin12 = product.UPC; }
		   
		   //aggregate rating node start
		   var bvProd = product;
		   if(product.isMaster()) {
			   bvProd = product;
		   }
		   else if(!empty(product.variationModel.master)) {
			   bvProd = product.getMasterProduct();
		   }
		   //var bvProd = product.isMaster() ? product : product.getMasterProduct();
		   
		   if(bvProd.custom.bvAverageRating != null && bvProd.custom.bvAverageRating > 0 && bvProd.custom.bvAverageRating < 6 && bvProd.custom.bvReviewCount != null && bvProd.custom.bvReviewCount > 0) {
			   pdpJsonSchema.aggregateRating = new Object();  
			   pdpJsonSchema.aggregateRating["@type"]="AggregateRating";
			   pdpJsonSchema.aggregateRating.ratingValue = bvProd.custom.bvAverageRating;
			   pdpJsonSchema.aggregateRating.reviewCount = bvProd.custom.bvReviewCount;
		   }
		   //aggregate rating node end
		   //offer node start	   
		   if(product.variationModel != null && product.variationModel.variants !=null && product.variationModel.variants.length > 0) {
			  
			  variationCount = product.variationModel.variants.length;		  
			  var priceArray = [];
			  for(var item=0; item < variationCount; item++) {
					var variantObject = product.variationModel.variants[item];
				    var variant = new Object();	     
			    	
					if(variantObject.priceModel.price.value != '0') {
						variant.price = variantObject.priceModel.price.value.toFixed(2);
					} else if (variantObject.priceModel.maxPrice.value != '0') {
						variant.price = variantObject.priceModel.maxPrice.value.toFixed(2);
					} else {
						variant.price = variantObject.priceModel.minPrice.value.toFixed(2);
					}
					priceArray.push(variant.price);
			  }		  
			  
			  
			  pdpJsonSchema.offers = new Object();		  
			  pdpJsonSchema.offers["@type"]="AggregateOffer";
			  pdpJsonSchema.offers.highPrice=getMaxPrice(priceArray).toString();
			  pdpJsonSchema.offers.lowPrice = getMinPrice(priceArray).toString();
			  pdpJsonSchema.offers.offerCount= variationCount;
			  pdpJsonSchema.offers.priceCurrency = session.getCurrency().getCurrencyCode();
		   }  
		   //offer node end
		   //graph node start		  
		      pdpJsonSchema["@graph"] = [];
		      if(product.variationModel != null && product.variationModel.variants !=null && product.variationModel.variants.length > 0) {
		    	  for(var item=0; item < variationCount; item++) {
					  var variantObject = product.variationModel.variants[item];
					    var variant = new Object();	 
					    variant["@context"]="http://schema.org";
				    	variant["@type"]="Product";
				    	variant.name = variantObject.name;
				    	variant.url = dw.web.URLUtils.abs('Product-Show','pid', variantObject.ID).toString();
				    	variant.description = variantObject.getShortDescription().toString();
				    	if(!empty(AmplienceHost) && !empty(AmplienceId)) {
				    		variant.image = request.httpProtocol+'://'+AmplienceHost+'/i/'+AmplienceId+'/'+product.custom.external_id+'.jpg';
				    	}
				    	variant.brand = new Object(); 
				    	variant.brand["@type"] = "Brand";
				    	variant.brand.name = variantObject.brand;
				    	variant.sku = variantObject.ID;
						if(variantObject.UPC){ variant.gtin12 = variantObject.UPC; }
						//aggregate rating node
						if(bvProd.custom.bvAverageRating != null && bvProd.custom.bvAverageRating > 0 && bvProd.custom.bvAverageRating < 6 && bvProd.custom.bvReviewCount != null && bvProd.custom.bvReviewCount > 0) {
							variant.aggregateRating = new Object();  
							variant.aggregateRating["@type"]="AggregateRating";
							variant.aggregateRating.ratingValue = bvProd.custom.bvAverageRating;
							variant.aggregateRating.reviewCount = bvProd.custom.bvReviewCount;
						}
						//aggregate rating node end
				    	//offer node start
				    	variant.offers = new Object();
				    	variant.offers["@type"]="Offer";
				    	variant.offers.url = dw.web.URLUtils.abs('Product-Show','pid', variantObject.ID).toString();
				    	if(variantObject.priceModel.price.value != '0') {
							variant.offers.price = variantObject.priceModel.price.value.toFixed(2);
						} else if (variantObject.priceModel.maxPrice.value != '0') {
							variant.offers.price = variantObject.priceModel.maxPrice.value.toFixed(2);
						} else {
							variant.offers.price = variantObject.priceModel.minPrice.value.toFixed(2);
						}
				    	variant.offers.priceCurrency = session.getCurrency().getCurrencyCode();
						variant.offers.availability="https://schema.org/InStock";
					    variant.offers.itemCondition="https://schema.org/NewCondition";
					    variant.offers.seller = new Object();
					    variant.offers.seller.name = "Mattress Firm";
					    variant.offers.seller.type = "Organization";
					    if(isInventoryList) {
					    	variant.offers.availableAtOrFrom = new Object();
						    variant.offers.availableAtOrFrom["@type"] = "Place";
						    variant.offers.availableAtOrFrom.branchCode = store.ID; 
						    variant.offers.availableAtOrFrom.name = store.name;
					    }
					    
					    pdpJsonSchema["@graph"].push(variant);
				  }	
		      }
	}else if(product != null) {// for master product 
		   var pdpJsonSchema = new Object();
		   pdpJsonSchema["@context"] ="https://schema.org";
		   pdpJsonSchema["@type"] = "Product";
		   pdpJsonSchema.name = product.name;
		   pdpJsonSchema.url = dw.web.URLUtils.abs('Product-Show','pid', product.ID).toString();
		   pdpJsonSchema.description = product.getShortDescription().toString();	
		   pdpJsonSchema.image = request.httpProtocol+'://'+AmplienceHost+'/i/'+AmplienceId+'/'+product.custom.external_id+'.jpg';	   
		   pdpJsonSchema.sku = product.ID;
		   if(product.UPC){ pdpJsonSchema.gtin12 = product.UPC; }
		   
		   pdpJsonSchema.brand = new Object(); 
		   pdpJsonSchema.brand["@type"] = "Brand";
		   pdpJsonSchema.brand.name = product.brand;
		   
		   //aggregate rating node start
		   var bvProd = product;
		   if(product.isMaster()) {
			   bvProd = product;
		   }
		   else if(!empty(product.variationModel.master)) {
			   bvProd = product.getMasterProduct();
		   }
		   //var bvProd = product.isMaster() ? product : product.getMasterProduct();
		   
		   if(bvProd.custom.bvAverageRating != null && bvProd.custom.bvAverageRating > 0 && bvProd.custom.bvAverageRating < 6 && bvProd.custom.bvReviewCount != null && bvProd.custom.bvReviewCount > 0) {
			   pdpJsonSchema.aggregateRating = new Object();  
			   pdpJsonSchema.aggregateRating["@type"]="AggregateRating";
			   pdpJsonSchema.aggregateRating.ratingValue = bvProd.custom.bvAverageRating;
			   pdpJsonSchema.aggregateRating.reviewCount = bvProd.custom.bvReviewCount;
		   }
		   //aggregate rating node end
		   
		   
		   if('AmplienceHost' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.AmplienceHost!='') {
			   var AmplienceHost = dw.system.Site.current.preferences.custom.AmplienceHost;
		   }
		   if('AmplienceClientId' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.AmplienceClientId!='') {
			   var AmplienceId = dw.system.Site.current.preferences.custom.AmplienceClientId;
		   }
		   if(product.variationModel != null && product.variationModel.variants !=null && product.variationModel.variants.length > 0) {
				  variationCount = product.variationModel.variants.length;		  
				  var priceArray = [];
				  for(var item=0; item < variationCount; item++) {
						var variantObject = product.variationModel.variants[item];
					    var variant = new Object();	     
				    	
						if(variantObject.priceModel.price.value != '0') {
							variant.price = variantObject.priceModel.price.value.toFixed(2);
						} else if (variantObject.priceModel.maxPrice.value != '0') {
							variant.price = variantObject.priceModel.maxPrice.value.toFixed(2);
						} else {
							variant.price = variantObject.priceModel.minPrice.value.toFixed(2);
						}
						priceArray.push(variant.price);
				  }			  pdpJsonSchema.offers = [];
				  var aggregateOffer = new Object();		  
				  aggregateOffer["@type"]="AggregateOffer";
				  aggregateOffer.highPrice=getMaxPrice(priceArray).toString();
				  aggregateOffer.lowPrice = getMinPrice(priceArray).toString();
				  aggregateOffer.offerCount= variationCount;
				  aggregateOffer.priceCurrency = session.getCurrency().getCurrencyCode();
				  pdpJsonSchema.offers.push(aggregateOffer);		  
				  
				  for(var item=0; item < variationCount; item++) {
					var variantObject = product.variationModel.variants[item];
				    var variant = new Object();	     
			    	variant["@type"]="Offer";
			    	variant.url = dw.web.URLUtils.abs('Product-Show','pid', variantObject.ID).toString();
			    	variant.sku = variantObject.ID;
			    	if(variantObject.UPC){ variant.gtin12 = variantObject.UPC; }
			    	if(!empty(AmplienceHost) && !empty(AmplienceId)) {
			    		variant.image = request.httpProtocol+'://'+AmplienceHost+'/i/'+AmplienceId+'/'+product.custom.external_id+'.jpg';
			    	}
					if(variantObject.priceModel.price.value != '0') {
						variant.price = variantObject.priceModel.price.value.toFixed(2);
					} else if (variantObject.priceModel.maxPrice.value != '0') {
						variant.price = variantObject.priceModel.maxPrice.value.toFixed(2);
					} else {
						variant.price = variantObject.priceModel.minPrice.value.toFixed(2);
					}
					variant.priceCurrency = session.getCurrency().getCurrencyCode();
					variant.availability="https://schema.org/InStock";
				    variant.itemCondition="https://schema.org/NewCondition";
				    variant.seller = new Object();
				    variant.seller.name = "Mattress Firm";
				    variant.seller.type = "Organization";            
				    pdpJsonSchema.offers.push(variant);
				  }
		   }else{
			   
		    	//offer node start
			   pdpJsonSchema.offers = new Object();
			   pdpJsonSchema.offers["@type"]="Offer";
			   pdpJsonSchema.offers.url = dw.web.URLUtils.abs('Product-Show','pid', product.ID).toString();
			   pdpJsonSchema.offers.sku = product.ID;
			   if(!empty(AmplienceHost) && !empty(AmplienceId)) {
				   pdpJsonSchema.offers.image = request.httpProtocol+'://'+AmplienceHost+'/i/'+AmplienceId+'/'+product.custom.external_id+'.jpg';
			   }
			   if(product.priceModel.price.value != '0') {
				   pdpJsonSchema.offers.price = product.priceModel.price.value.toFixed(2);
			   } else if (product.priceModel.maxPrice.value != '0') {
				   pdpJsonSchema.offers.price = product.priceModel.maxPrice.value.toFixed(2);
			   } else {
				   pdpJsonSchema.offers.price = product.priceModel.minPrice.value.toFixed(2);
			   }
			   pdpJsonSchema.offers.priceCurrency = session.getCurrency().getCurrencyCode();
			   pdpJsonSchema.offers.availability="https://schema.org/InStock";
			   pdpJsonSchema.offers.itemCondition="https://schema.org/NewCondition";
			   pdpJsonSchema.offers.seller = new Object();
			   pdpJsonSchema.offers.seller.name = "Mattress Firm";
			   pdpJsonSchema.offers.seller.type = "Organization";			     
		   }
		   
	}
	return JSON.stringify(pdpJsonSchema);
}

function createBreadCrumbSchema(breadCrumbArray) {
	var bcJsonSchema = new Object();
	bcJsonSchema["@context"] ="https://schema.org";
	bcJsonSchema["@type"] = "BreadcrumbList";
	bcJsonSchema.itemListElement = new Array();
	
	if(breadCrumbArray != null && breadCrumbArray.length > 0) {
		for(var bd=0; bd < breadCrumbArray.length; bd++) {
			var itemBD = breadCrumbArray[bd];
			
			var element = new Object();
			element["@type"] = "ListItem";
			element.position = bd+1;
			
			element.item = new Object();
			element.item.id = itemBD['id'].replace("'","");
			element.item.name = itemBD.name;
			bcJsonSchema.itemListElement.push(element);
		}	       
	}	
	//return bcJsonSchema.itemListElement[0];
    return bcJsonSchema;
}

function getMinPrice(arr) {
  var len = arr.length, min = Infinity;
  while (len--) {
    if (Number(arr[len]) < min) {
      min = Number(arr[len]);
    }
  }
  return min.toFixed(2);
}

function getMaxPrice(arr) {
  var len = arr.length, max = -Infinity;
  while (len--) {
    if (Number(arr[len]) > max) {
      max = Number(arr[len]);
    }
  }
  return max.toFixed(2);
}

function hasDuplicate(array) {
    var valuesSoFar = Object.create(null);
    for (var i = 0; i < array.length; ++i) {
        var value = array[i];
        if (value in valuesSoFar) {
            return true;
        }
        valuesSoFar[value] = true;
    }
    return false;
}


function getPreferredStore() {
	var store = null;
	var storeIdValue =  request.httpParameterMap.storeId;
	if (storeIdValue != null && storeIdValue != "") {
		if(storeIdValue.toString().length < 6) {
			var missingLeadingZero = Number(6 - storeIdValue.toString().length);
			for(var leadingZero = 0; leadingZero < missingLeadingZero; leadingZero++) {
				storeIdValue = '0'+storeIdValue.toString();
			}
			
		}
		if (session.customer.registered) {
			store = StoreMgr.getStore(storeIdValue);			
		} else if (StoreMgr.getStore(storeIdValue)) {
			store = StoreMgr.getStore(storeIdValue);
		}
		
		if(store == null || store == "") {
			if (session.customer.registered && !empty(session.customer.profile.custom.preferredStore)) {
				store = StoreMgr.getStore(session.customer.profile.custom.preferredStore);
			} else if (StoreMgr.getStore(session.custom.preferredStore)) {
				store = StoreMgr.getStore(session.custom.preferredStore);
			}
		}
	}
	else {
		if (session.customer.registered && !empty(session.customer.profile.custom.preferredStore)) {
			store = StoreMgr.getStore(session.customer.profile.custom.preferredStore);
		} else if (StoreMgr.getStore(session.custom.preferredStore)) {
			store = StoreMgr.getStore(session.custom.preferredStore);
		}
	}
	
	return store;
}

function isRobotAppliesForSingleCanonical(productSearchResultParam) {
    var productSearchModel = productSearchResultParam;
    var selectedRefinementsMapMeta = new dw.util.HashMap();    
    var parentCategoryName = productSearchModel.category.getParent().displayName;    
    var selectedCategoryId = productSearchModel.category.ID;
    var categoryName = productSearchModel.category.displayName;    
	
	
    var canonicalURL = "/" + categoryName + "/";
    if (productSearchModel != null && productSearchModel.refinements != null && productSearchModel.refinements.refinementDefinitions != null) {
        for (var i = 0; i < productSearchModel.refinements.refinementDefinitions.length; i++) {
            var selectedRefinement = productSearchModel.refinements.refinementDefinitions[i];
            if (selectedRefinement.isAttributeRefinement() && productSearchModel.isRefinedByAttribute(selectedRefinement.attributeID)) {
                var selectedRefinementArray = new Array();
                selectedRefinementArray = productSearchModel.refinements.getRefinementValues(selectedRefinement);
                for (var j = 0; j < selectedRefinementArray.length; j++) {
                    var refinementValue = selectedRefinementArray[j];
                    if (productSearchModel.isRefinedByAttributeValue(selectedRefinement.attributeID, refinementValue.value)) {
                        selectedRefinementsMapMeta.put(selectedRefinement.displayName, selectedRefinementArray[j].value);
                    }
                }
            }
        }
    }

    //get keys as per canonical rule
    //for comfort    
    if (selectedRefinementsMapMeta.containsKey("Comfort")) {
        canonicalURL = canonicalURL + selectedRefinementsMapMeta.get("Comfort") + "/";
    } else if (parentCategoryName == 'Comfort') {
        canonicalURL = canonicalURL + categoryName + "/";
    }
    
    // for Brand
    if (selectedRefinementsMapMeta.containsKey("Brands")) {
        canonicalURL = canonicalURL + selectedRefinementsMapMeta.get("Brands") + "/";
    } else if (parentCategoryName == 'Brand') {
        canonicalURL = canonicalURL + categoryName + "/";
    }  

    //for size
    if (selectedRefinementsMapMeta.containsKey("Size")) {
        canonicalURL = canonicalURL + selectedRefinementsMapMeta.get("Size") + "/";
    } else if (parentCategoryName == 'Size') {
        canonicalURL = canonicalURL + categoryName + "/";
    }

    //for type
    if (selectedRefinementsMapMeta.containsKey("Mattress Type")) {
        canonicalURL = canonicalURL + selectedRefinementsMapMeta.get("Mattress Type") + "/";
    } else if (parentCategoryName == 'Type') {
        canonicalURL = canonicalURL + categoryName + "/";
    }
    
    
    var canonicalURLStructure = canonicalURL.replace(/\s+/g, "-").toLowerCase();
    var result = canonicalURLStructure.split("/");
    var singleFinalCanonical = "/"+result[1]+'/'+result[2]+'/';
        var isRobot = false;
        if(selectedCategoryId == 'web-specials' || 
           selectedCategoryId == 'New-At-Mattress-Firm' || 
           selectedCategoryId == 'same-day-delivery' || 
           selectedCategoryId == 'financing-sale' || 
           selectedCategoryId == '5637146830') {
            if(result.length == 7 || result.length == 6 || result.length == 5) {
            	isRobot =  true;            	
        	} else {
        		isRobot = false;
        	}
        }else if(selectedCategoryId == '5637155076' ||
      		  selectedCategoryId == '5637167827' ||
    		  selectedCategoryId == 'sale') {
        		isRobot = true;
		    } else {
		    	isRobot = false;
		    }
		return isRobot;
}

function breadCrumbCreatorRule(productSearchResultParam) {

    var productSearchModel = productSearchResultParam;
    var selectedRefinementsMapMeta = new dw.util.HashMap();
    var contentAssetId = "";
    
    var parentCategoryName = productSearchModel.category.getParent().displayName;    
    var selectedCategoryName = productSearchModel.category.displayName;
    var selectedCategoryId = productSearchModel.category.ID;
    var categoryName = "";
    if(selectedCategoryId == '5637156576') { //Sale Category
    	categoryName = "mattress-sale";
	}else if(selectedCategoryId == '5637150650') {  //Shop by brand
        categoryName = "mattresses";
    } else if(selectedCategoryId == 'mattress-sizes') {
        categoryName = "Mattresses";
	}else {
		categoryName = selectedCategoryName;
	}
    var multipleCanonicalURL = "";//"/"+categoryName+"/";
    var refinementBrandArray = new Array();
    var refinementSizeArray = new Array();
    var refinementTypeArray = new Array();
    var refinementComfortArray = new Array();



    if (productSearchModel != null && productSearchModel.refinements != null && productSearchModel.refinements.refinementDefinitions != null) {
        for (var i = 0; i < productSearchModel.refinements.refinementDefinitions.length; i++) {
            var selectedRefinement = productSearchModel.refinements.refinementDefinitions[i];
            if (selectedRefinement.isAttributeRefinement() && productSearchModel.isRefinedByAttribute(selectedRefinement.attributeID)) {
                var selectedRefinementArray = new Array();
                selectedRefinementArray = productSearchModel.refinements.getRefinementValues(selectedRefinement);
                for (var j = 0; j < selectedRefinementArray.length; j++) {
                    var refinementsValues = selectedRefinementArray[j];
                    if (productSearchModel.isRefinedByAttributeValue(selectedRefinement.attributeID, refinementsValues.value)) {
                        var jsonData = {};
                        jsonData.name = selectedRefinement.displayName;
                        jsonData.value = selectedRefinementArray[j].value

                        if (selectedRefinement.displayName == 'Brands') {
                            refinementBrandArray.push(jsonData);
                        }
                        if (selectedRefinement.displayName == 'Size') {
                            refinementSizeArray.push(jsonData);
                        }
                        if (selectedRefinement.displayName == 'Mattress Type') {
                            refinementTypeArray.push(jsonData);
                        }
                        if (selectedRefinement.displayName == 'Comfort') {
                            refinementComfortArray.push(jsonData);
                        }
                    }
                }
            }
        }
    }

    var brandValue = "";
    var sizeValue = "";
    var typeValue = "";
    var comfortValue = "";
    
    if (refinementComfortArray && refinementComfortArray.length == 2) {    	
    	multipleCanonicalURL = multipleCanonicalURL;
        return multipleCanonicalURL.replace(/\s+/g, "-").toLowerCase().replace(/\/+/g,"/"); 
    }
    if (refinementComfortArray && refinementComfortArray.length == 1 && refinementBrandArray && refinementBrandArray.length == 2) {
    	multipleCanonicalURL = multipleCanonicalURL + '/'+refinementComfortArray[0].value + "/";
        return multipleCanonicalURL.replace(/\/+/g,"/");//multipleCanonicalURL.replace(/\s+/g, "-").toLowerCase().replace(/\/+/g,"/").replace("'","");
    }
    if (refinementComfortArray && refinementComfortArray.length == 1 && refinementBrandArray && refinementBrandArray.length == 1 && refinementSizeArray && refinementSizeArray.length == 2) {
    	multipleCanonicalURL = multipleCanonicalURL + '/'+refinementComfortArray[0].value + '/'+refinementBrandArray[0].value + "/";
        return multipleCanonicalURL.replace(/\/+/g,"/");//multipleCanonicalURL.replace(/\s+/g, "-").toLowerCase().replace(/\/+/g,"/").replace("'","");
    }
    if (refinementComfortArray && refinementComfortArray.length == 1 && refinementBrandArray && refinementBrandArray.length == 1 && refinementSizeArray && refinementSizeArray.length == 1 && refinementTypeArray && refinementTypeArray.length == 2) {
    	multipleCanonicalURL = multipleCanonicalURL + '/'+refinementComfortArray[0].value + '/'+refinementBrandArray[0].value + '/' + refinementSizeArray[0].value + "/";
        return multipleCanonicalURL.replace(/\/+/g,"/");//multipleCanonicalURL.replace(/\s+/g, "-").toLowerCase().replace(/\/+/g,"/").replace("'","");
    }
    if (refinementComfortArray && refinementComfortArray.length == 1 && refinementBrandArray && refinementBrandArray.length == 1 && refinementSizeArray && refinementSizeArray.length == 1 && refinementTypeArray && refinementTypeArray.length == 1) {
    	multipleCanonicalURL = multipleCanonicalURL + '/'+refinementComfortArray[0].value + '/'+refinementBrandArray[0].value + '/' + refinementSizeArray[0].value + '/' + refinementTypeArray[0].value + "/";
        return multipleCanonicalURL.replace(/\/+/g,"/");//multipleCanonicalURL.replace(/\s+/g, "-").toLowerCase().replace(/\/+/g,"/").replace("'","");
    }else {
    	if (refinementBrandArray && refinementBrandArray.length == 1) {
        	brandValue = refinementBrandArray[0].value;
        }else {
        	brandValue = "";
        }
        if (refinementSizeArray && refinementSizeArray.length == 1) {
        	sizeValue = refinementSizeArray[0].value;
        }else {
        	sizeValue = "";
        }
        if (refinementTypeArray && refinementTypeArray.length == 1) {
        	typeValue = refinementTypeArray[0].value;
        }else {
        	typeValue = "";
        }
        if (refinementComfortArray && refinementComfortArray.length == 1) {
        	comfortValue = refinementComfortArray[0].value;
        }else {
        	comfortValue = "";
        }
        multipleCanonicalURL = multipleCanonicalURL+"/"+comfortValue+"/"+brandValue+"/"+sizeValue+"/"+typeValue+"/";
        var finalRule = multipleCanonicalURL.replace(/\/+/g,"/");
        if(finalRule != '/') {
        	return finalRule;
        }else {
        	return "";
        }
    }
    
      
}

function createBC(arr) {	
	var aa = new Array();	
	var length = arr.length;
    for(var i=0 ; i < length; i++) {
    	var str = "";
    	var strurl = "";
    	for(var j=0 ; j <= i; j++) { 
    		str = str +' '+ arr[j]; 
    		strurl = strurl + '/' + arr[j].replace(/\s+/g, "-").toLowerCase();
    	}
    	var obj = {};
		obj.displayName = str;
		obj.url = strurl;
		aa.push(obj);    	
	}    
	return aa;	
}

exports.CreateCanonical = createCanonical;
exports.CreateContentAssetId = createContentAssetId;
exports.CreateMultipleCanonical = createMultipleCanonical;
exports.CreateProductDetailPageLDJsonSchema = createProductDetailPageLDJsonSchema;
exports.CreateBreadCrumbSchema = createBreadCrumbSchema;
exports.HasDuplicate = hasDuplicate;
exports.IsRobotAppliesForSingleCanonical = isRobotAppliesForSingleCanonical;
exports.BreadCrumbCreatorRule = breadCrumbCreatorRule;
exports.CreateBC = createBC;


