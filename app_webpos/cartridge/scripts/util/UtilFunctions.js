var Site = require('dw/system/Site');

var UtilFunctions = {
		isChicagoProduct : function (manufacturerSKU) {
			var enableChicagoDCDelivery = "enableChicagoDCDelivery" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("enableChicagoDCDelivery") : false;
		    var chicagoInventLocationId = '';
		    var parcelableProductIDs = '';
		    if (enableChicagoDCDelivery) {
		    	parcelableProductIDs = "parcelableProductIDs" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("parcelableProductIDs") : '';
		    	if(!empty(manufacturerSKU) && parcelableProductIDs.indexOf(manufacturerSKU) > -1) {
			    	return true;
			    }
		    }
		    return false;
		},
		chicagoInventLocationId : function () {
			var enableChicagoDCDelivery = "enableChicagoDCDelivery" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("enableChicagoDCDelivery") : false;
    	    var chicagoInventLocationId = '';
    	    if (enableChicagoDCDelivery) {
    	    	chicagoInventLocationId = dw.system.Site.getCurrent().getCustomPreferenceValue('chicagoInventLocationId');
    	    }
    	    return chicagoInventLocationId;
		},
		appendParamToURL: function (url, name, value) {
			// quit if the param already exists
			if (url.indexOf(name + '=') !== -1) {
				return url;
			}
			var separator = url.indexOf('?') !== -1 ? '&' : '?';
			var refinedURL = url + separator + name + '=' + encodeURIComponent(value);
			return refinedURL;
		},
		appendParamsToUrl: function (url, params) {
			var _url = url;
			for(var key in params) {
				_url = this.appendParamToURL(_url, key, params[key]);
			};
			return _url;
		}
};

exports.UtilFunctions = UtilFunctions;
