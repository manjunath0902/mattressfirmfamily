var URLUtils = require('dw/web/URLUtils');
var manifest = {
    /*
	short_name: 'Commerce Cloud',
    name: 'Salesforce Storefront Reference Architecture',
    icons: [
        {
            src: URLUtils.staticURL('/salesforce512.png').toString(),
            type: 'image/png',
            sizes: '512x512'
        },
        {
            src: URLUtils.staticURL('/salesforce192.png').toString(),
            type: 'image/png',
            sizes: '192x192'
        }
    ],
    start_url: URLUtils.url('Home-Show').toString(),
    background_color: '#FFFFFF',
    scope : '/',
    display: 'standalone',
    theme_color: '#21A0DF'
    */
	
	"name": "Mobile SOS",
	"short_name": "Mobile SOS",
	"lang": "en-US",
	"theme_color": "#2196f3",
	"background_color": "#2196f3",
	"display": "standalone",
	"Scope": "/",
	"start_url": (request.httpProtocol + "://" + request.httpHost),
	"icons": [
		{
		  "src": URLUtils.staticURL('/pwa/images/icons/icon-72x72.png').toString(),
		  "sizes": "72x72",
		  "type": "image/png"
		},
		{
		  "src": URLUtils.staticURL('/pwa/images/icons/icon-96x96.png').toString(),
		  "sizes": "96x96",
		  "type": "image/png"
		},
		{
		  "src": URLUtils.staticURL('/pwa/images/icons/icon-128x128.png').toString(),
		  "sizes": "128x128",
		  "type": "image/png"
		},
		{
		  "src": URLUtils.staticURL('/pwa/images/icons/icon-144x144.png').toString(),
		  "sizes": "144x144",
		  "type": "image/png"
		},
		{
		  "src": URLUtils.staticURL('/pwa/images/icons/icon-152x152.png').toString(),
		  "sizes": "152x152",
		  "type": "image/png"
		},
		{
		  "src": URLUtils.staticURL('/pwa/images/icons/icon-192x192.png').toString(),
		  "sizes": "192x192",
		  "type": "image/png"
		},
		{
		  "src": URLUtils.staticURL('/pwa/images/icons/icon-384x384.png').toString(),
		  "sizes": "384x384",
		  "type": "image/png"
		},
		{
		  "src": URLUtils.staticURL('/pwa/images/icons/icon-512x512.png').toString(),
		  "sizes": "512x512",
		  "type": "image/png"
		}
	],
	"splash_pages": null
};

module.exports = manifest;
