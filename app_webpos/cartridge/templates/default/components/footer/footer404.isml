<iscontent type="text/html" charset="UTF-8" compact="true"/>
<iscomment>
	This is the footer for all pages. Be careful caching it if it contains
	user dependent information. Cache its elements instead if necessary (do not forget
	to change the isinclude into a pipeline include for that).
</iscomment>
<isinclude template="util/modules"/>

<isscript>
	var ContentMgr = require('dw/content/ContentMgr');
	var homeIconLinks2 = ContentMgr.getContent('home-icon-links-2').custom.body;
	var footerNavSocial = ContentMgr.getContent('footer-nav-social').custom.body;
	var footerNav = ContentMgr.getContent('footer-nav').custom.body;
	var footerDisclaimer = ContentMgr.getContent('footer-disclaimer').custom.body;
	if(dw.system.System.instanceType == dw.system.System.PRODUCTION_SYSTEM) {
		var mfrmChatScript = ContentMgr.getContent('mfrm-chat-script-prod').custom.body;
	}
	else {
		var mfrmChatScript = ContentMgr.getContent('mfrm-chat-script').custom.body;
	}
	var footerSocialEmail = ContentMgr.getContent('footer-social-email').custom.body;
</isscript>

<isprint value="${homeIconLinks2}" encoding="off"/>

<footer class="main-footer">
	<div class="row">
		<div class="column">
			<div class="footer-email-signup">
				<p class="h4 red bold">Sign Up Today for Special Offers and Promotions.</p>

				<input type="hidden" name="footer-social-zipCode" id="footer-social-zipCode" value="${session.custom.customerZip}">
				<input type="hidden" name="footer-social-siteId" id="footer-social-siteId" value="${dw.system.Site.current.ID}">
				<input type="hidden" name="footer-social-leadSource" id="footer-social-leadSource" value="footer">
				<input type="hidden" name="footer-social-optOutFlag" id="footer-social-optOutFlag" value="false">

				<isprint value="${footerSocialEmail}" encoding="off" />
			</div>
		</div>
	</div>

	<div class="row footer-content">
		<div class="column small-12 medium-4 large-3 mobile-text-center">
			<a href="${URLUtils.httpHome()}">
				<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" class="lazyload" data-src="${URLUtils.staticURL('images/mf-logo-red.png')}" alt="Mattress Firm Logo" title="Mattress Firm Logo">
			</a>

			<div class="footer-social-wrap hide-mobile">
				<isprint value="${footerNavSocial}" encoding="off" />
			</div>

			<a href="https://www.mattressfirmfosterkids.org/" class="hide-mobile">
				<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" class="lazyload" data-src="${URLUtils.staticURL('images/mf-fk-logo-white.png')}" alt="Mattress Firm - Foster Kids Logo" title="Mattress Firm - Foster Kids Logo"
				 style="max-width: 180px; margin-top: 15px;">
			</a>
		</div>

		<div class="footer-nav column small-12 medium-8 large-9">
			<isprint value="${footerNav}" encoding="off" />
		</div>

		<div class="column small-12 medium-12 large-12 hide-desktop mobile-text-center">
			<isprint value="${footerNavSocial}" encoding="off" />
		</div>
	</div>

	<div class="row">
		<div class="column">
			<isprint value="${footerDisclaimer}" encoding="off" />
		</div>
	</div>
</footer>

<iscomment>
	Customer registration can happen everywhere in the page flow. As special tag in the pdict
	is indicating it. So we have to check on every page, if we have to report this event for
	the reporting engine.
</iscomment>
<isinclude template="util/reporting/ReportUserRegistration.isml"/>

<isif condition="${dw.system.Site.current.preferences.custom.tealiumController === true}">
	<isinclude url="${URLUtils.url('Tealium_utag-RenderTealium',
	   'title', request.pageMetaData.title,
	   'pid', pdict.CurrentHttpParameterMap.pid.stringValue,
	   'checkoutstep', (!empty(pdict.checkoutStep) ? pdict.checkoutStep : ''),
	   'pagecontexttype', ('pageContext' in this && !empty(pageContext)) ? ''+pageContext.type : null,
	   'pagecontexttitle', ('pageContext' in this && !empty(pageContext)) ? ''+pageContext.title : null,
	   'searchterm', request.httpParameterMap.q.stringValue,
	   'searchresultscount', (!empty(pdict.ProductSearchResult) ? ''+pdict.ProductSearchResult.count : null),
	   'productid', (!empty(pdict.Product) ? pdict.Product.ID : null),
	   'pagecgid',  request.httpParameterMap.cgid.stringValue,
	   'orderno', (!empty(pdict.Order) ? pdict.Order.orderNo : null)
	  )}"/>
  <iselse>
  	<isinclude template="tealium/footer_tealium.isml"/>
</isif>


<isinclude template="components/footer/footer_UI"/>

<isif condition="${!(dw.system.Site.getCurrent().getCustomPreferenceValue('enableChat'))}">
	<isscript>
	    var buttonID : String = dw.system.Site.getCurrent().getCustomPreferenceValue('liveAgentButtonID');
	</isscript>
	<isif condition="${!empty(buttonID) && buttonID}">
		<isinclude template="util/live_agent_chat/init.isml"/>
	</isif>
</isif>
