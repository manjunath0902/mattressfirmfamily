<iscontent type="text/html" charset="UTF-8" compact="true"/>
<iscomment>
	Displays order details, such as order number, creation date, payment information,
	order totals and information for each contained shipment.
	This template module can be used in order confirmation pages as well as in the
	order history to render the details of a given order. Depending on the context
	being used in, one might omit rendering certain information.

	Parameters:

	order		: the order whose details to render
	orderstatus	: if set to true, the order status will be rendered
				  if set to false or not existing, the order status will not be rendered
</iscomment>
<isscript>
    var MattressViewHelper = require("int_mattressc/cartridge/scripts/mattress/util/MattressViewHelper.ds");
</isscript>

<isset name="Order" value="${pdict.order}" scope="page"/>
<isinclude template="util/modules"/>
<p style="padding: 30px 0 0 0; margin: 0; font-size: 14px; line-height: 25px; color: #000000;">
	<span style="font-weight: 700;">${Resource.msg('order.orderdetails.orderplaced','order',null)}</span>
	<isprint value="${Order.creationDate}" style="DATE_LONG"/>
</p>

<p style="padding: 0; margin: 0; font-size: 14px; line-height: 25px; color: #000000;">
	<span style="font-weight: 700;">${Resource.msg('order.orderdetails.ordernumber','order',null)}</span>
	<isprint value="${Order.orderNo}"/>
</p>

<table style="background:#ffffff;width:980px; padding: 25px 0;">
	<tr>
		<td style="font-size:13px;font-family:arial;padding:0 10px 0 0;vertical-align:top; width: 33%; box-sizing: border-box;">
			<b style="color: #cb171e;border-bottom: 1px solid #d9d9d9;font-weight: 700; font-size: 25px; margin-bottom: 4px; display: block;">${Resource.msg('order.orderdetails.billingaddress','order',null)}</b>
			<isminiaddress p_address="${Order.billingAddress}"/>
		</td>
		<td style="font-size:13px;font-family:arial;padding:0 10px 0 0;vertical-align:top;  width: 33%; box-sizing: border-box;">

			<isif condition="${Order.paymentInstruments.length == 1}">
				<b style="color: #cb171e;border-bottom: 1px solid #d9d9d9;font-weight: 700; font-size: 25px; margin-bottom: 4px; display: block;">${Resource.msg('order.orderdetails.paymentmethod','order',null)}</b>
			<iselse/>
				<b style="color: #cb171e;border-bottom: 1px solid #d9d9d9;font-weight: 700; font-size: 25px; margin-bottom: 4px; display: block;">${Resource.msg('order.orderdetails.paymentmethods','order',null)}</b>
			</isif>

			<iscomment>Render All Payment Instruments</iscomment>
			<isloop items="${Order.getPaymentInstruments()}" var="paymentInstr" status="piloopstate">
				<div><isprint value="${dw.order.PaymentMgr.getPaymentMethod(paymentInstr.paymentMethod).name}" /></div>
				<isif condition="${dw.order.PaymentInstrument.METHOD_GIFT_CERTIFICATE.equals(paymentInstr.paymentMethod)}">
					<isprint value="${paymentInstr.maskedGiftCertificateCode}"/><br />
				</isif>
				<isminicreditcard card="${paymentInstr}" show_expiration="${false}"/>
				<div>
					<span class="label">${Resource.msg('global.amount','locale',null)}:</span>
					<span class="value"><isprint value="${paymentInstr.paymentTransaction.amount}"/></span>
				</div><!-- END: payment-amount -->
			</isloop>
		</td>
		<td style="font-size:13px;font-family:arial;font-weight: 700; padding:0; vertical-align:top;  width: 33%; box-sizing: border-box;">
			<b style="color: #cb171e;border-bottom: 1px solid #d9d9d9;font-weight: 700; font-size: 25px; margin-bottom: 4px; display: block;">${Resource.msg('order.orderdetails.ordersummary','order',null)}</b>
			<isif condition="${Order.shipments.length > 1}">
				<isemailordertotals p_lineitemctnr="${Order}" p_showshipmentinfo="${true}" p_shipmenteditable="${false}" p_totallabel="${Resource.msg('global.ordertotal','locale',null)}"/>
			<iselse/>
				<isemailordertotals p_lineitemctnr="${Order}" p_showshipmentinfo="${false}" p_shipmenteditable="${false}" p_totallabel="${Resource.msg('global.ordertotal','locale',null)}"/>
				<br />
			</isif>
		</td>

	</tr>
</table>


<iscomment>render a box for each shipment</iscomment>

	<isloop items="${Order.shipments}" var="shipment" status="shipmentloopstate">

		<p style="font-size: 25px; line-height: 30px; font-weight: 900; color: #cb171e; margin: 0; padding: 0 0 10px 0;"><b>${(Resource.msg('order.orderconfirmation-email.shipmentnumber','order',null))}</b></p>
      	<iscomment>
      	<isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule') && shipment.custom.deliveryDate != null && shipment.custom.deliveryTime != null && session.custom.scheduleInfo == 'scheduledelivery'}">
      	</iscomment>
      	<isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule') && shipment.custom.deliveryDate != null && shipment.custom.deliveryTime != null}">
			<p>
				<b>${Resource.msg('deliveryshedule.deliverydetail','checkout',null)}</b>
				<span>
					<isprint value="${new Date(shipment.custom.deliveryDate)}" formatter="EEEE MMMM d, yyyy" />
					<isprint value="${'<span> from </span>' + shipment.custom.deliveryTime.replace('-',' to ', 'g') + '.'}" encoding="off" />
				</span>
			</p>																		
			<p>${Resource.msg('deliveryshedule.intimation','checkout',null)}</p>
		</isif>
		<isif condition="${shipment.productLineItems.size() > 0}">

			<iscomment>Shipment items table</iscomment>
			<table style="background:#ffffff;width:980px;" cellpadding="0" cellspacing="0">
				<thead>
					<tr>
						<th style="border-left:1px solid #d9d9d9; border-top:1px solid #d9d9d9; border-bottom:1px solid #d9d9d9; background:#f2f2f2; padding: 25px 0px 25px 20px; font-size: 14px; color: #000000; font-weight: 700; text-align: left;" colspan="2">${Resource.msg('global.product','locale',null)}</th>
						<th style="border-top:1px solid #d9d9d9; border-bottom:1px solid #d9d9d9; background:#f2f2f2; padding: 25px 0px 25px 0; font-size: 14px; color: #000000; font-weight: 700; text-align: left;">${Resource.msg('global.price','locale',null)}</th>
						<th style="border-top:1px solid #d9d9d9; border-bottom:1px solid #d9d9d9; background:#f2f2f2; padding: 25px 0px 25px 0; font-size: 14px; color: #000000; font-weight: 700; text-align: left;">${Resource.msg('global.qty','locale',null)}</th>
						<th style="border-top:1px solid #d9d9d9; border-bottom:1px solid #d9d9d9; background:#f2f2f2; padding: 25px 0px 25px 0; font-size: 14px; color: #000000; font-weight: 700; text-align: left;">${Resource.msg('global.totalprice','locale',null)}</th>										
						<isif condition="${shipment.custom.shipmentType == 'instore' || !empty(session.custom.storeIdEmail)}"/>
						    <th style="border-top:1px solid #d9d9d9; border-right:1px solid #d9d9d9; border-bottom:1px solid #d9d9d9; background:#f2f2f2; padding: 25px 30px 25px 30px; font-size: 14px; color: #000000; font-weight: 700; text-align: left;">${Resource.msg('minishipments.name.instorepickup','checkout',null)}</th>							
						<iselse/>
							<th style="border-top:1px solid #d9d9d9; border-right:1px solid #d9d9d9; border-bottom:1px solid #d9d9d9; background:#f2f2f2; padding: 25px 30px 25px 30px; font-size: 14px; color: #000000; font-weight: 700; text-align: left;">${Resource.msg('minishipments.name.shippingaddress.simple','checkout',null)}</th>
						</isif>
					</tr>
				</thead>
				<isset name="bundlesItems" value="${new dw.util.HashMap()}" scope="page" />
				<isset name="isShippingAddressDisplayed" value="${false}" scope="page" />
				<isloop items="${shipment.productLineItems}" var="productLineItem" status="pliloopstate">
					<isif condition="${productLineItem.custom.isBundledItem}">
						<isscript>
							bundlesItems.put(productLineItem.custom.mainBundleID, productLineItem.quantityValue);
						</isscript>
		                <iscontinue>
		            </isif>
					<tr>
						<td style="vertical-align:top; padding: 30px 15px; width: 20.644%; border-left:1px solid #d9d9d9;  border-bottom:1px solid #d9d9d9; box-sizing: border-box;">
								<isif condition="${'AmplienceHost' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.AmplienceHost!=''}">
									<isset name="AmplienceHost" value="${dw.system.Site.current.preferences.custom.AmplienceHost}" scope="page" />
								</isif>
								<isif condition="${'AmplienceClientId' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.AmplienceClientId!=''}">
									<isset name="AmplienceId" value="${dw.system.Site.current.preferences.custom.AmplienceClientId}" scope="page" />
								</isif>
								<isset name="imgUrl" value="${'https://'+AmplienceHost+'/i/'+AmplienceId+'/'+ productLineItem.product.custom.external_id}" scope="page"/>
								<isif condition="${imgUrl}">
									<img src="${imgUrl}" alt="${productLineItem.product.productName}" title="${productLineItem.product.productName}" width="180" style="width: 180px; height: auto; display: block;"/>
								<iselse/>
									<img src="${URLUtils.httpStatic('/images/noimagesmall.png')}" alt="${productLineItem.productName}" title="${productLineItem.productName}" width="180" style="width: 180px; height: auto; display: block;"/>
								</isif>
						</td>
						<td style="font-size:14px;font-family:arial; width: 24.1016%; padding: 30px 25px 30px 0;vertical-align:top; border-bottom:1px solid #d9d9d9; box-sizing: border-box;">
							<iscomment>Display product line and product using module</iscomment>
							<isdisplayliproduct p_productli="${productLineItem}" p_editable="${false}"/>
							<isif condition="${MattressViewHelper.hasRecycleFees(productLineItem)}">
									<isset name="shippingState" value="${shipment.shippingAddress != null ? shipment.shippingAddress.stateCode.toUpperCase() : ""}" scope="page" >
									<p>${Resource.msgf('global.recycling-fee','locale', '', shippingState)}</p>
					    	</isif>
						</td>
						<td style="vertical-align:top; width: 11.0847%; padding: 30px 20px 30px 0; font-size:14px; border-bottom:1px solid #d9d9d9; box-sizing: border-box;">
								<isif condition="${productLineItem.product != null}">


												<iscomment>
													StandardPrice: quantity-one unit price from the configured list price
													book. SalesPrice: product line item base price. If these are
													different, then we display crossed-out StandardPrice and also
													SalesPrice.
												</iscomment>


												<iscomment>Get the price model for this	product.</iscomment>
												<isset name="PriceModel" value="${productLineItem.product.getPriceModel()}" scope="page" />


												<iscomment>Get StandardPrice from list price book.</iscomment>
												<isinclude template="product/components/standardprice" />


												<iscomment>Get SalesPrice from line item itself.</iscomment>
												<isset name="SalesPrice" value="${productLineItem.basePrice}" scope="page" />
												<isif condition="${StandardPrice.available && StandardPrice > SalesPrice}">
													<iscomment>StandardPrice and SalesPrice are different, show standard</iscomment>
													<div class="price-promotion">
														<span class="price-sales" style="color: #cb171e; font-weight: 900;"><isprint value="${SalesPrice}" /></span>
														<span class="price-standard" style="text-decoration: line-through;"><isprint value="${StandardPrice}" /></span>
													</div>
												<iselse/>
													<isif condition="${productLineItem.priceAdjustments.length == 0}">
														<span class="price-sales"><isprint value="${SalesPrice}" /></span>
													</isif>
												</isif>
											</isif>

							</td>
						<td style="font-size:14px;font-family:arial; width: 5%; padding: 30px 25px 30px 0;vertical-align:top; border-bottom:1px solid #d9d9d9; box-sizing: border-box;">
							<isprint value="${productLineItem.quantity}"/>
						</td>

						<td style="font-size:14px;font-family:arial; width: 12.1355%; padding: 30px 25px 30px 0;vertical-align:top; border-bottom:1px solid #d9d9d9; box-sizing: border-box; font-weight: 900;">
							<iscomment>Render quantity. If it is a bonus product render word 'Bonus'</iscomment>
								<isif condition="${productLineItem.bonusProductLineItem}">
									<isif condition="${MattressViewHelper.getNumNonRecycleAdjustments(productLineItem) > 0}">
										<isset name="bonusProductPrice" value="${MattressViewHelper.subtractLIRecycleFees(productLineItem, productLineItem.getAdjustedPrice(), false)}" scope="page"/>
										<isset name="hasRecycleFees" value="${true}" scope="page"/>
									<iselse/>
										<isset name="bonusProductPrice" value="${productLineItem.getAdjustedPrice()}" scope="page"/>
										<isset name="hasRecycleFees" value="${false}" scope="page"/>
									</isif>							
									<isif condition="${bonusProductPrice == 0}">
					                	${Resource.msg('global.free','locale',null)}
					                <iselse/>
					                	<isprint value="${bonusProductPrice}"/>
					                </isif>
					                <isif condition="${hasRecycleFees}">
										<p>+<isprint value="${MattressViewHelper.getTotalRecycleFees(productLineItem)}"></p>
						    		</isif>				                
							<iselse/>
								<isprint value="${MattressViewHelper.subtractLIRecycleFees(productLineItem, productLineItem.adjustedPrice, false)}"/>
								<isif condition="${productLineItem.optionProductLineItems.size() > 0}">
									<isloop items="${productLineItem.optionProductLineItems}" var="optionLI">
										<isif condition="${optionLI.price > 0}">
											<p>+<isprint value="${MattressViewHelper.subtractLIRecycleFees(optionLI, optionLI.adjustedPrice)}"/></p>
										</isif>
									</isloop>
								</isif>
								<isif condition="${MattressViewHelper.hasRecycleFees(productLineItem)}">
									<p>+<isprint value="${MattressViewHelper.getTotalRecycleFees(productLineItem)}"></p>
					    		</isif>

							</isif>
						</td>

						<iscomment>only show shipping address for first pli in shipment</iscomment>
						<isif condition="${pliloopstate.first}">
							<isset name="isShippingAddressDisplayed" value="${true}" scope="page" />
							<isset name="rowSpan" value="${shipment.productLineItems.size()}" scope="page"/>
							<td rowspan="${rowSpan.toFixed()}" style="font-size:14px;font-family:arial;padding:30px 29px;vertical-align:top; border-left: 1px solid #d9d9d9; border-right: 1px solid #d9d9d9; border-bottom:2px solid #d9d9d9; box-sizing: border-box; ">
								<isscript>
		                            var StoreMgr = require('dw/catalog/StoreMgr');
		                            var store = StoreMgr.getStore(session.custom.storeIdEmail);
		                        </isscript>
		                        <isif condition="${!empty(session.custom.storeIdEmail)}"> 
			                        <div><isprint value="${store.name}"/></div>
			                        <div><isprint value="${store.address1}"/></div>
			                        <isif condition="${!empty(store.address2)}">
			                            <div><isprint value="${store.address2}"/></div>
			                        </isif>
			                        <div><isprint value="${store.city}"/>, <isprint value="${store.stateCode}"/> <isprint value="${store.postalCode}"/></div>
			                        <div><isprint value="${store.phone}"/></div>
			                        <isset name="earliestPickupDate" value ="${PickupDate.getDate(storeId, shipment)}" scope="page" />
									<strong>
										${Resource.msg('summary.available','checkout',null)} 
										<isif condition="${!empty(session.custom.earliestPickupDateEmail)}">
											<isprint value="${session.custom.earliestPickupDateEmail}" formatter="MMM. d, yyyy" />
										<iselse>
											${Resource.msg('checkout.storepickup.representative.contact','checkout',null)}
										</isif>
									</strong>
								<iselse/>
									<div>
										<isminishippingaddress p_shipment="${shipment}" p_editable="${false}" p_showmethod="${false}" p_showpromos="${false}"/>
									</div>
									<div>
										${Resource.msg('order.orderdetails.shippingmethod','order',null)}
										<isif condition="${!empty(shipment.shippingMethod)}">
											<isprint value="${shipment.shippingMethod.displayName}"/>
										<iselse/>
											<isprint value="${shipment.shippingMethodID}"/>
										</isif>
									</div>
									<div>
										${Resource.msg('order.orderdetails.shippingstatus','order',null)}
										<isif condition="${shipment.shippingStatus==dw.order.Shipment.SHIPPING_STATUS_NOTSHIPPED}">
											${Resource.msg('order.orderdetails.notshipped','order',null)}
										<iselseif condition="${shipment.shippingStatus==dw.order.Shipment.SHIPPING_STATUS_SHIPPED}">
											${Resource.msg('order.orderdetails.shipped','order',null)}
										<iselse/>
											${Resource.msg('order.orderdetails.notknown','order',null)}
										</isif>
									</div>
									<isif condition="${!empty(shipment.trackingNumber)}">
										<div>
											${Resource.msg('order.orderdetails.tracking','order',null)}
											<!-- Tracking Number --><isprint value="${shipment.trackingNumber}"/>
										</div>
									</isif>
								</isif>
							</td>
						</isif>
					</tr>

				</isloop>
				
				<iscomment>Render Bundle Line Items</iscomment>
				<isloop items="${bundlesItems.keySet().iterator()}" var="bundleKey" status="loopstate">
					<isset name="bundleProduct" value="${dw.catalog.ProductMgr.getProduct(bundleKey)}" scope="page"/>
					<isset name="bundleProductQty" value="${bundlesItems.get(bundleKey)}" scope="page"/>
					<tr>
						<td style="vertical-align:top; padding: 30px 15px; width: 20.644%; border-left:1px solid #d9d9d9;  border-bottom:1px solid #d9d9d9; box-sizing: border-box;">
							<isif condition="${'AmplienceHost' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.AmplienceHost!=''}">
								<isset name="AmplienceHost" value="${dw.system.Site.current.preferences.custom.AmplienceHost}" scope="page" />
							</isif>
							<isif condition="${'AmplienceClientId' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.AmplienceClientId!=''}">
								<isset name="AmplienceId" value="${dw.system.Site.current.preferences.custom.AmplienceClientId}" scope="page" />
							</isif>
							<isset name="imgUrl" value="${'https://'+AmplienceHost+'/i/'+AmplienceId+'/'+ bundleProduct.custom.external_id}" scope="page"/>
							<isif condition="${imgUrl}">
								<img src="${imgUrl}" alt="${bundleProduct.name}" title="${bundleProduct.name}" width="180" style="width: 180px; height: auto; display: block;"/>
							<iselse/>
								<img src="${URLUtils.httpStatic('/images/noimagesmall.png')}" alt="${bundleProduct.name}" title="${bundleProduct.name}" width="180" style="width: 180px; height: auto; display: block;"/>
							</isif>
						</td>
						
						<td style="font-size:14px;font-family:arial; width: 24.1016%; padding: 30px 25px 30px 0;vertical-align:top; border-bottom:1px solid #d9d9d9; box-sizing: border-box;">
							<div class="name">
								<span style="color: #000000; text-decoration: none; font-size: 15px; font-weight: 900;"><isprint value="${bundleProduct.manufacturerName + ' ' + bundleProduct.name}"/></span>
							</div>
						</td>
						
						<td style="vertical-align:top; width: 11.0847%; padding: 30px 20px 30px 0; font-size:14px; border-bottom:1px solid #d9d9d9; box-sizing: border-box;">
							<isif condition="${bundleProduct != null}">
								<iscomment>
									StandardPrice: quantity-one unit price from the configured list price
									book. SalesPrice: product line item base price. If these are
									different, then we display crossed-out StandardPrice and also
									SalesPrice.
								</iscomment>


								<iscomment>Get the price model for this	product.</iscomment>
								<isset name="PriceModel" value="${bundleProduct.getPriceModel()}" scope="page" />


								<iscomment>Get StandardPrice from list price book.</iscomment>
								<isinclude template="product/components/standardprice" />


								<iscomment>Get SalesPrice from line item itself.</iscomment>
								<isset name="SalesPrice" value="${PriceModel.getPrice()}" scope="page" />
								<isif condition="${StandardPrice.available && StandardPrice > SalesPrice}">
									<iscomment>StandardPrice and SalesPrice are different, show standard</iscomment>
									<div class="price-promotion">
										<span class="price-sales" style="color: #cb171e; font-weight: 900;"><isprint value="${SalesPrice}" /></span>
										<span class="price-standard" style="text-decoration: line-through;"><isprint value="${StandardPrice}" /></span>
									</div>
								<iselse/>
									<span class="price-sales"><isprint value="${SalesPrice}" /></span>
								</isif>
							</isif>
						</td>
						
						
						<td style="font-size:14px;font-family:arial; width: 5%; padding: 30px 25px 30px 0;vertical-align:top; border-bottom:1px solid #d9d9d9; box-sizing: border-box;">
							<isprint value="${bundleProductQty}" formatter="#" />
						</td>
						
						<td style="font-size:14px;font-family:arial; width: 12.1355%; padding: 30px 25px 30px 0;vertical-align:top; border-bottom:1px solid #d9d9d9; box-sizing: border-box; font-weight: 900;">
							<isprint value="${PriceModel.getPrice().multiply(bundleProductQty)}"/>
						</td>
						
						<iscomment>only show shipping address for first pli in shipment</iscomment>
						<isif condition="${!isShippingAddressDisplayed}">
							<isset name="isShippingAddressDisplayed" value="${false}" scope="page" />
							<isset name="rowSpan" value="${shipment.productLineItems.size()}" scope="page"/>
							<td rowspan="${rowSpan.toFixed()}" style="font-size:14px;font-family:arial;padding:30px 29px;vertical-align:top; border-left: 1px solid #d9d9d9; border-right: 1px solid #d9d9d9; border-bottom:2px solid #d9d9d9; box-sizing: border-box; ">
								<isscript>
		                            var StoreMgr = require('dw/catalog/StoreMgr');
		                            var store = StoreMgr.getStore(session.custom.storeIdEmail);
		                        </isscript>
		                        <isif condition="${!empty(session.custom.storeIdEmail)}"> 
			                        <div><isprint value="${store.name}"/></div>
			                        <div><isprint value="${store.address1}"/></div>
			                        <isif condition="${!empty(store.address2)}">
			                            <div><isprint value="${store.address2}"/></div>
			                        </isif>
			                        <div><isprint value="${store.city}"/>, <isprint value="${store.stateCode}"/> <isprint value="${store.postalCode}"/></div>
			                        <div><isprint value="${store.phone}"/></div>
			                        <isset name="earliestPickupDate" value ="${PickupDate.getDate(storeId, shipment)}" scope="page" />
									<strong>
										${Resource.msg('summary.available','checkout',null)} 
										<isif condition="${!empty(session.custom.earliestPickupDateEmail)}">
											<isprint value="${session.custom.earliestPickupDateEmail}" formatter="MMM. d, yyyy" />
										<iselse>
											${Resource.msg('checkout.storepickup.representative.contact','checkout',null)}
										</isif>
									</strong>
								<iselse/>
									<div>
										<isminishippingaddress p_shipment="${shipment}" p_editable="${false}" p_showmethod="${false}" p_showpromos="${false}"/>
									</div>
									<div>
										${Resource.msg('order.orderdetails.shippingmethod','order',null)}
										<isif condition="${!empty(shipment.shippingMethod)}">
											<isprint value="${shipment.shippingMethod.displayName}"/>
										<iselse/>
											<isprint value="${shipment.shippingMethodID}"/>
										</isif>
									</div>
									<div>
										${Resource.msg('order.orderdetails.shippingstatus','order',null)}
										<isif condition="${shipment.shippingStatus==dw.order.Shipment.SHIPPING_STATUS_NOTSHIPPED}">
											${Resource.msg('order.orderdetails.notshipped','order',null)}
										<iselseif condition="${shipment.shippingStatus==dw.order.Shipment.SHIPPING_STATUS_SHIPPED}">
											${Resource.msg('order.orderdetails.shipped','order',null)}
										<iselse/>
											${Resource.msg('order.orderdetails.notknown','order',null)}
										</isif>
									</div>
									<isif condition="${!empty(shipment.trackingNumber)}">
										<div>
											${Resource.msg('order.orderdetails.tracking','order',null)}
											<!-- Tracking Number --><isprint value="${shipment.trackingNumber}"/>
										</div>
									</isif>
								</isif>
							</td>
						</isif>
						
					</tr>
				</isloop>
				
			<iscomment>Shipment Gift Message</iscomment>
			<isif condition="${shipment.gift}">
				<tr>
					<td colspan="4" style="font-size:12px;font-family:arial;padding:20px 10px;vertical-align:top;">
						${Resource.msg('order.orderdetails.giftmessage','order',null)}
					</td>
				</tr>
				<tr>
					<td colspan="4" style="font-size:12px;font-family:arial;padding:20px 10px;vertical-align:top;">
						<isif condition="${!empty(shipment.giftMessage)}">
							<isprint value="${shipment.giftMessage}"/>
						<iselse/>
							&nbsp;
						</isif>
					</td>
				</tr>
			</isif>
			</table>

		</isif>

		<isif condition="${shipment.giftCertificateLineItems.size() > 0}">

			<iscomment>Shipment Gift Certificate</iscomment>
			<table  style="background:#ffffff;border:1px solid #999999;width:680px;">
				<thead>
					<tr>
						<th style="background:#cccccc;padding:5px 20px;font-size:12px;font-family:arial;text-align:left;">${Resource.msg('global.item','locale',null)}</th>
						<th style="background:#cccccc;padding:5px 20px;font-size:12px;font-family:arial;text-align:left;">${Resource.msg('global.price','locale',null)}</th>
						<th style="background:#cccccc;padding:5px 20px;font-size:12px;font-family:arial;text-align:left;">${Resource.msg('order.orderdetails.shippingto','order',null)}</th>
					</tr>
				</thead>
				<isloop items="${shipment.giftCertificateLineItems}" var="giftCertificateLineItem" status="gcliloopstate">
					<tr>
						<td style="font-size:12px;font-family:arial;padding:20px 10px;vertical-align:top;">
							${Resource.msg('global.giftcertificate','locale',null)}
							<div>
								${Resource.msg('order.orderdetails.giftcertto','order',null)}
									<isprint value="${giftCertificateLineItem.recipientName}"/><br />
									<isprint value="${giftCertificateLineItem.recipientEmail}"/>
							</div>
							<div>
								${Resource.msg('order.orderdetails.giftcertfrom','order',null)}
									<isprint value="${giftCertificateLineItem.senderName}"/><br />
									<isprint value="${Order.customerEmail}"/>
							</div>
						</td>
						<td style="font-size:12px;font-family:arial;padding:20px 10px;vertical-align:top;">
							<isprint value="${giftCertificateLineItem.price}"/>
						</td>
						<td style="font-size:12px;font-family:arial;padding:20px 10px;vertical-align:top;">
							<div>
								${Resource.msg('order.orderdetails.giftcertshippingaddress','order',null)}
								<div>
									<isprint value="${giftCertificateLineItem.recipientName}"/>
									<isprint value="${giftCertificateLineItem.recipientEmail}"/>
								</div>
							</div>
							<div>
								${Resource.msg('order.orderdetails.shippingmethod','order',null)}
								${Resource.msg('order.orderdetails.giftcertshipping','order',null)}
							</div>
						</td>
					</tr>
				</isloop>

				<iscomment>if shipment is marked as gift</iscomment>
				<tr>
					<td colspan="4" style="font-size:12px;font-family:arial;padding:20px 10px;vertical-align:top;">
						${Resource.msg('order.orderdetails.giftmessage','order',null)}
					</td>
				</tr>

				<isif condition="${shipment.gift}">
					<tr>
						<td colspan="4" style="font-size:12px;font-family:arial;padding:20px 10px;vertical-align:top;">
							<isif condition="${!empty(shipment.giftMessage)}">
								<isprint value="${shipment.giftMessage}"/>
							<iselse/>
								&nbsp;
							</isif>
						</td>
					</tr>
				<iselse/>
					<tr>
						<td colspan="4" style="font-size:12px;font-family:arial;padding:20px 10px;vertical-align:top;">
							<isset name="theGiftCert" value="${shipment.giftCertificateLineItems.iterator().next()}" scope="page"/>
							<isif condition="${!empty(theGiftCert.message)}">
								<isprint value="${theGiftCert.message}"/>
							<iselse/>
								&nbsp;
							</isif>
						</td>
					</tr>
				</isif>
			</table>

		</isif>
	</isloop>
