<iscontent type="text/html" charset="UTF-8" compact="true"/>
<iscomment>
	This template is used to render the order totals for a basket or an order.

    Parameters:
    p_lineitemctnr     : the line item container to render (this could be either an order or a basket as they are both line item containers)
    p_showshipmentinfo : boolean that controls if individual shipment information is rendered or if aggregate totals are rendered
    p_shipmenteditable : boolean that controls if the shipment should have an edit link
    p_totallabel       : label to use for the total at bottom of summary table
</iscomment>

<iscomment>Create page varibale representing the line item container</iscomment>
<isset name="LineItemCtnr" value="${pdict.p_lineitemctnr}" scope="page"/>

<isif condition="${!empty(LineItemCtnr)}">
	<table class="order-totals-table">
		<tbody>
			<iscomment>Order level discount plus all item level sale price discount</iscomment>
			<isscript>
				var MattressViewHelper = require("int_mattressc/cartridge/scripts/mattress/util/MattressViewHelper.ds");
				var merchTotalExclOrderDiscounts : dw.value.Money = MattressViewHelper.getGrossTotalWithOptionsForCart(LineItemCtnr);//LineItemCtnr.getMerchandizeTotalNetPrice();
				var totalRecycleFee = MattressViewHelper.getTotalRecycleFeeForCartWithoutOptionLineItems(LineItemCtnr);
				var merchTotalInclOrderDiscounts : dw.value.Money = LineItemCtnr.getAdjustedMerchandizeTotalPrice(true);
				merchTotalInclOrderDiscounts = merchTotalInclOrderDiscounts.subtract(totalRecycleFee); 
				var orderDiscount : dw.value.Money = merchTotalExclOrderDiscounts.subtract( merchTotalInclOrderDiscounts );
				var Site = require('dw/system/Site');
				importScript("app_storefront_core:cart/CartUtils.ds");
				var enableFSPProgram = "enableFSPProgram" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("enableFSPProgram") : false;				
				if(enableFSPProgram) {
					var fspDiscount = CartUtils.getFSPDiscount(LineItemCtnr);
					if(!empty(fspDiscount.value)) {
						orderDiscount = orderDiscount.add(fspDiscount.value);
					}
				}
				
				var cartTotal : dw.value.Money = MattressViewHelper.getCartTotal(LineItemCtnr);
			</isscript>					
			<tr class="cart-total">
				<td>${Resource.msg('order.ordersummary.carttotal','order',null)}</td>
				<td><isprint value="${cartTotal}"/></td>
			</tr>
						
			<isif condition="${!empty(orderDiscount) && orderDiscount > 0.0}">
				<tr class="discount">
					<td>${Resource.msg('order.ordersummary.orderdiscount','order',null)}</td>
					<td class="value-less">- <isprint value="${orderDiscount}"/></td>
				</tr>
			</isif>			
			<isif condition="${enableFSPProgram && !empty(fspDiscount.value) && fspDiscount.value != 0}">
				<tr class="discount">
					<td>${Resource.msg('cart.fsp.name','checkout',null)}</td>
					<td><isprint value="${fspDiscount.value}"/></td>
				</tr>
			</isif>
			<iscomment>
				render order subtotal if there are both contained in the cart, products and gift certificates
				(product line item prices including product level promotions plus gift certificate line items prices)
			</iscomment>
			<tr class="order-subtotal">
				<td>${Resource.msg('order.ordersummary.subtotal','order',null)}</td>
				<td><isprint value="${LineItemCtnr.getAdjustedMerchandizeTotalPrice(true).add(LineItemCtnr.giftCertificateTotalPrice)}"/></td>
			</tr>

			<iscomment>render each single shipment or shipping total</iscomment>
			<isif condition="${pdict.p_showshipmentinfo}">
				<iscomment>the url to edit shipping depends on the checkout scenario</iscomment>
				<isset name="editUrl" value="${URLUtils.https('COShipping-Start')}" scope="page"/>
				<isif condition="${pdict.CurrentForms.multishipping.entered.value}">
					<isset name="editUrl" value="${URLUtils.https('COShippingMultiple-StartShipments')}" scope="page"/>
				</isif>
				<isloop items="${LineItemCtnr.shipments}" var="Shipment" status="loopstatus">
					<iscomment>show shipping cost per shipment only if it's a physical shipment containing product line items</iscomment>
					<isif condition="${Shipment.productLineItems.size() > 0}">
						<tr class="order-shipping <isif condition="${loopstatus.first}"> first <iselseif condition="${loopstatus.last}"> last</isif>">
							<td>
								<isif condition="${pdict.p_shipmenteditable}">
									<a href="${editUrl}" title="${Resource.msg('order.ordersummary.ordershipping.edit','order',null)}">${Resource.msg('order.ordersummary.ordershipping.edit','order',null)}</a>
								</isif>
								${Resource.msg('order.ordersummary.ordershipping.total','order',null)}
	                        </td>
							<td>
								<isif condition="${LineItemCtnr.shippingTotalPrice.available}">
									<isprint value="${Shipment.shippingTotalPrice}"/>
								</isif>
							</td>
						</tr>
					</isif>
				</isloop>
			<iselse/>
				<tr class="order-shipping">
					<td>
						<isif condition="${!empty(session.custom.customerZip) && !empty(checkoutstep) && (checkoutstep == 3) && (typeof checkoutstep != 'undefined')}">
							${Resource.msgf('order.ordersummary.ordershipping.total','order',null,session.custom.customerZip)}
						<iselseif condition="${!empty(session.custom.customerZip) && !empty(checkoutstep) && (checkoutstep == 4) && (typeof checkoutstep != 'undefined')}">	
							${Resource.msgf('order.ordersummary.ordershipping.total','order',null,session.custom.customerZip)}	
						<iselse/>
							${Resource.msgf('order.ordersummary.ordershipping.total','order',null,session.custom.customerZip)}
						</isif>
					</td>
					<td>
						<isif condition="${LineItemCtnr.shippingTotalPrice.available && !pdict.isRestrictedCheckout}">
							<isif condition="${LineItemCtnr.shippingTotalPrice <= 0}">
								${Resource.msg('global.free','locale',null)}
							<iselse>
								<isprint value="${LineItemCtnr.shippingTotalPrice}"/>
							</isif>
						<iselse/>
							<isprint value="${LineItemCtnr.shippingTotalPrice.newMoney(null)}"/>
						</isif>
					</td>
				</tr>
			</isif>

			<iscomment>calculate shipping discount</iscomment>
			<isscript>
				var shippingExclDiscounts : dw.value.Money = LineItemCtnr.shippingTotalPrice;
				var shippingInclDiscounts : dw.value.Money = LineItemCtnr.getAdjustedShippingTotalPrice();
				var shippingDiscount : dw.value.Money = shippingExclDiscounts.subtract( shippingInclDiscounts );
			</isscript>
			<isif condition="${!empty(shippingDiscount) && shippingDiscount.value > 0.0}">
				<isif condition="${pdict.p_showshipmentinfo}">
					<tr class="order-shipping-discount discount">
						<td>${Resource.msg('order.ordersummary.ordershippingdiscount','order',null)}</td>
						<td class="value-less">- <isprint value="${shippingDiscount}"/></td>
					</tr>
				<iselse/>
					<tr class="order-shipping-discount discount">
						<td>${Resource.msg('order.ordersummary.ordershippingdiscount','order',null)}</td>
						<td class="value-less">- <isprint value="${shippingDiscount}"/></td>
					</tr>
				</isif>
			</isif>

			<iscomment>tax amount - Note: only show this field if taxation policy is net</iscomment>
			<isif condition="${dw.order.TaxMgr.getTaxationPolicy() == dw.order.TaxMgr.TAX_POLICY_NET}">
				<tr class="order-sales-tax">
				<isif condition="${!empty(checkoutstep) && checkoutstep > 3}">
					<td>${Resource.msg('order.ordersummary.ordertax','order',null)}</td>
				<iselseif condition="${!empty(checkoutstep) && checkoutstep == 3}">
					<td>${Resource.msg('order.ordersummary.ordertax','order',null)}</td>
				<iselseif condition="${LineItemCtnr.totalTax.available}">
					<td>${Resource.msg('order.ordersummary.ordertax','order',null)}</td>
				<iselse>
					<td>${Resource.msg('order.ordersummary.ordertaxatcheckout','order',null)}</td>
				</isif>
				<td>
					<isif condition="${LineItemCtnr.totalTax.available}">
						<isprint value="${LineItemCtnr.totalTax}"/>
					</isif>
				</td>
				</tr>
			</isif>

			<iscomment>order total</iscomment>
			<tr class="order-total">
				<isif condition="${LineItemCtnr.totalGrossPrice.available}">
				 	<isset name="orderTotalValue" value="${LineItemCtnr.totalGrossPrice}" scope="page"/>
				<iselse/>
					<isset name="orderTotalValue" value="${LineItemCtnr.getAdjustedMerchandizeTotalPrice(true).add(LineItemCtnr.giftCertificateTotalPrice)}" scope="page"/>
				</isif>

				<td><isprint value="${pdict.p_totallabel}"/></td>
				<td class="order-value"><isprint value="${orderTotalValue}"/></td>
			</tr>
		</tbody>
	</table>
</isif>
