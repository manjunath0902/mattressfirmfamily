<iscontent type="text/html" charset="UTF-8" compact="true"/>

<iscomment>
The <!—BEGIN/END… comments are control statements for the build cartridge which can be found in xChange https://xchange.demandware.com/docs/DOC-5728 or checked out from SVN at https://svn2.hosted-projects.com/cs_europe/DWTechRepository/cartridges/build_cs
If you are not using the build cartridge the comments can be safely removed.
</iscomment>

<meta charset=UTF-8>

<iscomment>See https://github.com/h5bp/html5-boilerplate/blob/5.2.0/dist/doc/html.md#x-ua-compatible</iscomment>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<iscomment>See https://github.com/h5bp/html5-boilerplate/blob/5.2.0/dist/doc/html.md#mobile-viewport</iscomment>
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
<!-- moved script to storefront.isml for -->
<iscomment>the page title calculated by the app </iscomment>

<isif condition="${dw.system.System.getInstanceType() != dw.system.System.PRODUCTION_SYSTEM}">
	<iscomment><title>${pdict.CurrentPageMetaData.title} | ${Resource.msg('global.site.name', 'locale', null)} | ${Resource.msg('revisioninfo.revisionnumber', 'revisioninfo', null)}</title></iscomment>
	<title>${pdict.CurrentPageMetaData.title}</title>
<iselse/>
  <title><isprint value="${pdict.CurrentPageMetaData.title}" encoding="off" /></title>
</isif>

<iscomment>FAVICON ICON: (website icon, a page icon or an urlicon) 16x16 pixel image icon for website</iscomment>
<link href="${URLUtils.staticURL('/images/favicon.ico')}" rel="shortcut icon" />

<iscomment>include all meta tags</iscomment>
<iscomment>
	This Content-Type setting is optional as long as the webserver transfers
	the Content-Type in the http header correctly. But because some browsers or
	proxies might not deal with this setting in the http header correctly, a
	second setting can help to keep everything just fine.
</iscomment>

<iscomment>Automatic generation for meta tags.</iscomment>
<meta name="description" content="<isif condition="${!empty(pdict.CurrentPageMetaData.description)}">${pdict.CurrentPageMetaData.description}</isif>"/>
<meta name="keywords" content="<isif condition="${!empty(pdict.CurrentPageMetaData.keywords)}">${pdict.CurrentPageMetaData.keywords}</isif> ${Resource.msg('global.storename','locale',null)}"/>

<meta property="fb:app_id" content="${dw.system.Site.current.preferences.custom.facebookAppID}">
<meta property="og:site_name" content="${dw.system.Site.current.name}">
<meta property="og:title" content="${pdict.CurrentPageMetaData.title}">
<meta property="og:description" content="${pdict.CurrentPageMetaData.description}">

<isif condition=${pageContext.type == 'product'}>
	<meta property="og:type" content="website">

	<iscomment>
		<meta property="product:retailer_item_id" content="${pdict.Product.ID}" />
		<meta property="product:price:amount"     content="0" />
		<meta property="product:price:currency"     content="USD" />
		<meta property="product:availability"     content="in stock" />
		<meta property="product:condition"        content="new" /> 
	 </iscomment>

	<meta property="og:url" content="${URLUtils.abs('Product-Show', 'pid', pdict.Product.ID)}">
	<meta property="og:image" content="${'//i1.adis.ws/i/hmk/' + pdict.Product.custom.external_id + '.jpg'}">
	
<iselseif condition=${pageContext.type == 'storefront'}>
	<isscript>
		var SiteURL:String=dw.system.Site.current.preferences.custom.googleMerchantSiteURL;
	</isscript>
	<meta property="og:type" content="website">
	<meta property="og:url" content="${SiteURL}">	
	<isset name="rootCatalog" value="${dw.catalog.CatalogMgr.siteCatalog.root}" scope="page" />
	<isif condition=${!empty(rootCatalog.image) && rootCatalog.image.absURL} >
		<meta property="og:image" content="${rootCatalog.image.absURL}">
	</isif>

<iselseif condition=${pageContext.type == 'search'}>
	<meta property="og:type" content="website">
	<isif condition="${(pdict.CurrentHttpParameterMap.cgid.submitted && pdict.CurrentHttpParameterMap.cgid.value != '')}">
		<meta property="og:url" content="${URLUtils.abs('Search-Show','cgid', pdict.CurrentHttpParameterMap.cgid)}">
	<iselse>
	    <meta property="og:url" content="${pdict.CurrentRequest.httpURL}">
	</isif>
	<isset name="categoryImage" value="${category.image}" scope="page" />
	<isif condition=${!empty(categoryImage) && categoryImage.absURL}>
		<meta property="og:image" content="${categoryImage.absURL}">
	</isif>
	
<iselse>
	<meta property="og:type" content="website">
	<isif condition="${(pdict.CurrentHttpParameterMap.cid.submitted && pdict.CurrentHttpParameterMap.cid.value != '')}">
		<meta property="og:url" content="${URLUtils.abs('Page-Show', 'cid', pdict.Content.ID)}">
	<iselse>
	    <meta property="og:url" content="${pdict.CurrentRequest.httpURL}">
	</isif>
	<isset name="categoryImage" value="${category.image}" scope="page" />
	<isif condition=${!empty(categoryImage) && categoryImage.absURL}>
		<meta property="og:image" content="${categoryImage.absURL}">
	</isif>
	
</isif>

<iscomment>
	Add your own meta information here, e.g. Dublin-Core information
</iscomment>
<iscomment>
    Adding this so that we can selectively request pages not be indexed at the advice of Merkle
</iscomment>
<isif condition=${pdict.NoIndexOverride}>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
</isif>


<iscomment>STYLE SHEETS ARE PLACED HERE SO THAT jQuery, Power Review and other RichUI styles do not overwrite the definitions below.</iscomment>

<iscomment>DEFAULT STYLESHEETS INCLUDED ON ALL PAGES</iscomment>

<isinclude template="components/header/htmlhead_UI"/>

<!--  UI -->
<iscomment>Comment out unused css </iscomment>
<iscomment>
<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,900' rel='stylesheet' type='text/css'>
</iscomment>

<!--[if lte IE 8]>
<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js" type="text/javascript"></script>
<script src="https://cdn.rawgit.com/chuckcarpenter/REM-unit-polyfill/master/js/rem.min.js" type="text/javascript"></script>
<![endif]-->


<!--  UI -->
<!--[if gt IE 9]><!-->
<iscomment>STYLE.CSS - THE BIG ONE</iscomment>
<isif condition="${'useMinifiedFiles' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.useMinifiedFiles === true}">
<link rel="stylesheet" href="${URLUtils.staticURL('/css/style.min.css')}" />
<iselse>
<link rel="stylesheet" href="${URLUtils.staticURL('/css/style.css')}" />
</isif>
<!--<![endif]-->
<!--[if lte IE 9]>
<link rel="stylesheet" href="${URLUtils.staticURL('/ie-css/style.min.css')}" />
<![endif]-->


<iscomment>Insert meta tag for the "Google-Verification" feature to verify that you are the owner of this site.</iscomment>
<isif condition="${'GoogleVerificationTag' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.GoogleVerificationTag!=''}">
    <meta name="google-site-verification" content="<isprint value="${dw.system.Site.current.preferences.custom.GoogleVerificationTag}"/>" />
</isif>

<iscomment>Gather device-aware scripts</iscomment>
<isinclude url="${URLUtils.url('Home-SetLayout')}"/>

<iscomment>If is Storefront - print canonical</iscomment>
<isif condition=${typeof pageContext !== 'undefined'}>
	<isif condition=${typeof pageContext.type !== 'undefined'}>
		<isif condition=${pageContext.type == "storefront"}>
			<link rel="canonical" href="${URLUtils.https('Page-Show','cid', pdict.CurrentHttpParameterMap.cid.value)}"/>
		</isif>
	</isif>
</isif>
<iscomment> This is optimize code </iscomment>
<isif condition="${dw.system.System.getInstanceType() == dw.system.System.PRODUCTION_SYSTEM}">
	<iscomment>head tag scripts for production </iscomment>
	<iscontentasset aid="head-tag-scripts-prod"/>
<iselse/>
	<iscomment>head tag scripts qa </iscomment>
	<iscontentasset aid="head-tag-scripts-qa"/>
</isif>

<iscomment>PWA Initialization</iscomment>
<isinclude template="components/header/pwaheaderhtml"/>