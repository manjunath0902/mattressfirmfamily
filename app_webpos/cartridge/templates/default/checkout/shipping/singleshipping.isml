<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isinclude template="util/modules"/>
<isscript>
    var StringHelpers = require('app_storefront_core/cartridge/scripts/util/StringHelpers.js');
    var result = StringHelpers.isMobileDevice(pdict.CurrentRequest.httpUserAgent);
</isscript>
<isset name="isMobileDevice" value="${result}" scope="page"/>

<isif condition="${isMobileDevice}">
	<isinclude template="checkout/shipping/mobile_singleshipping"/>	
<iselse>

<isdecorate template="checkout/pt_checkout">


<iscomment>
    This template visualizes the first step of the single shipping checkout
    scenario. It renders a form for the shipping address and shipping method
    selection. Both are stored at a single shipment only.
</iscomment>

<iscomment>Report this checkout step (we need to report two steps)</iscomment>

<isset name="isBopisShipping" value="${session.custom.deliveryOptionChoice == 'instorepickup'}" scope="page" />
<isset name="isBopisAvailable" value="${dw.system.Site.getCurrent().getCustomPreferenceValue('enableBOPIS')}" scope="page"/>

<isreportcheckout checkoutstep="${2}" checkoutname="${'ShippingAddress'}"/>
<isreportcheckout checkoutstep="${3}" checkoutname="${'ShippingMethod'}"/>
<isscript>
    importScript("app_storefront_core:cart/CartUtils.ds");
    var productListAddresses = CartUtils.getAddressList(pdict.Basket, pdict.CurrentCustomer, true);
</isscript>

    <iscomment>checkout progress indicator</iscomment>
    
    <ischeckoutprogressindicator step="1" rendershipping="${pdict.Basket.productLineItems.size() == 0 ? 'false' : 'true'}"/>
    <div class="pickup-location">
		<isif condition="${isBopisAvailable}">
	        <div class="ship-to-you-details">
	        	<div class="ship-to-you-title hide">
	       			${Resource.msg('cart.store.deliveryoptions', 'checkout', null)}
	       		</div>
	       		<div class="ship-to-you-message-block">
					<div id="currentDeliveryChoice" class="hide">${session.custom.deliveryOptionChoice}</div>
	       			<div class="ship-to-you-icon"></div>
	       			<span class="selected_pickup">
		   				<isif condition="${session.custom.deliveryOptionChoice != 'instorepickup'}"> 
		    				${Resource.msg('cart.shiptoyou.message', 'checkout', null)}
		       			<iselse/>
			       			${Resource.msg('cart.storepickup.message', 'checkout', null)}
		       			</isif>
	       			</span>
	       			<a class="ship-to-you-chg-delivery">${Resource.msg('cart.shiptoyou.change', 'checkout', null)}</a>
	       			
	       			<div class="ship-to-you-change-block hide">
	       				<div class="form-row">
							<div class="field-wrapper">
			  					<label for="shipped">
									<input type="radio" id="shipped" class="input-radio uniform" name="ShipToDeliveryOption" value="shipped" <isif condition="${session.custom.deliveryOptionChoice != 'instorepickup' }">checked="checked"</isif>/>
									<span>${Resource.msg('cart.shiptoyou.address', 'checkout', null)}</span>
								</label>
							</div>
						
							<div class="field-wrapper">
								<label for="instorepickup">
									<input type="radio" id="instorepickup" class="input-radio uniform"  name="ShipToDeliveryOption" value="instorepickup" <isif condition="${session.custom.deliveryOptionChoice == 'instorepickup'}">checked="checked"</isif>/>
									<span>${Resource.msg('product.pickupinstore', 'product', null)}</span>
								</label>
							</div>
	
							<div class="ship-to-you-modify">
								<span class="ship-to-you-modify-button"><button>${Resource.msg('cart.shiptoyou.modify', 'checkout', null)}</button></span>
								<span class="ship-to-you-modify-cancel">Cancel</span>	
							</div>
	       			
	       				</div>
	       			</div>
				</div>
	       	</div>   
	   	</isif>
        <isif condition="${session.custom.deliveryOptionChoice == 'instorepickup'}">
            <ispickuplocation p_lineitemctnr="${pdict.Basket}" />
        <iselse>
        	<div class="store-picker hide">
	        	<a href="${URLUtils.url('StorePicker-Start', 'isBopis', 'true', 'isCart', 'true')}" title="${Resource.msg('product.pickup.dialogtitle','product',null)}">${Resource.msg('cart.store.changelocation', 'checkout', null)}</a>
	        </div>
        </isif>
    </div>
    
    <form action="${URLUtils.continueURL()}" method="post" id="${pdict.CurrentForms.singleshipping.shippingAddress.htmlName}" class="checkout-shipping address form-horizontal <isif condition="${isBopisShipping}">bopis</isif>">
        <fieldset>
        <isif condition="${pdict.HomeDeliveries}">
                <iscomment>shipping address area</iscomment>

                    <h2 class="address-legend">
                    	<isif condition="${isBopisShipping}">
                    		${Resource.msg('singleshipping.contactinfo','checkout',null)}
                    	<iselse>
                    		${Resource.msg('singleshipping.enteraddress','checkout',null)}
                    	</isif>
                        <div class="dialog-required"> <span class="required-indicator">* <em>${Resource.msg('global.requiredfield','locale',null)}</em></span></div>
                    </h2>

                    <iscomment>Entry point for Multi-Shipping (disabled on purpose)</iscomment>
                    <isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('enableMultiShipping')}">
                        <isscript>
                            var plicount = 0;
                            for each (var pli : ProductLineItem in pdict.Basket.allProductLineItems){
                                if(pli.custom.fromStoreId == null ){
                                    plicount += pli.quantityValue;
                                }
                            }
                        </isscript>
                        <isif condition="${plicount > 1 }">
                            <div class="ship-to-multiple">
                                ${Resource.msg('singleshipping.multiple','checkout',null)}
                                <button class="shiptomultiplebutton button-fancy-medium cancel" type="submit" name="${pdict.CurrentForms.singleshipping.shipToMultiple.htmlName}" value="${Resource.msg('global.yes','locale',null)}">
                                    ${Resource.msg('global.yes','locale',null)}
                                </button>
                            </div>
                        </isif>
                    </isif>


                    <iscomment>display select box with stored addresses if customer is authenticated and there are saved addresses</iscomment>
                    <isif condition="${!dw.system.Site.getCurrent().getCustomPreferenceValue('sfLoginEnabled')}">
	                    <isif condition="${pdict.CurrentCustomer.authenticated && pdict.CurrentCustomer.profile.addressBook.addresses.size() > 0}">
	                        <div class="select-address form-row">
	                            <label for="${pdict.CurrentForms.singleshipping.addressList.htmlName}">
	                                ${Resource.msg('global.selectaddressmessage','locale',null)}
	                            </label>
	                            <div class="field-wrapper">
	                                <isaddressselectlist p_listId="${pdict.CurrentForms.singleshipping.addressList.htmlName}" p_listaddresses="${productListAddresses}" />
	                            </div>
	                        </div>
	                    </isif>
					</isif>
					
                    <isscript>
                        var currentCountry = require('app_storefront_core/cartridge/scripts/util/Countries').getCurrent(pdict);
                        var disabledAttrs = {"disabled": "disabled"};
                    </isscript>
                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.firstName}" type="input"/>
                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.lastName}" type="input"/>
                    
                    <isif condition="${isBopisShipping}">
	                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.address1}" type="hidden" />
	                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.address2}" type="hidden"/>
	                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.aptNumber}" type="hidden"/>
	                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.crossStreet}" type="hidden" rowclass="cross-street" />
	                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.phone}" rowclass="phones"  type="input"/>
	                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.mobilePhone}" rowclass="phones" type="input"/>
	                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.postal}" maxlength="5" rowclass="postal"  type="hidden"/>
	                    <input id="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.cityJSON.htmlName}" rowclass="city"  name="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.cityJSON.htmlName}" type="hidden" />
	                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.country}" type="hidden" rowclass="ct"   attributes = "${disabledAttrs}"/>
	                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.cities.city}" rowclass="one city-col" type="hidden" xhtmlclass="city"  attributes = "${disabledAttrs}" />
	                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.states.state}"  rowclass="one state-col" type="hidden" xhtmlclass="state" attributes = "${disabledAttrs}" />
	                <iselse>
	                    <isscript>
	                        var help = {
	                        label: Resource.msg('singleshipping.apofpo','checkout',null),
	                        cid: 'apo-fpo'
	                        };
	                    </isscript>
	
	                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.address1}" type="input" help="${help}"/>
	                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.address2}" type="input"/>
	                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.aptNumber}" type="input"/>
	                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.crossStreet}" type="input" rowclass="cross-street" />
	                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.phone}" rowclass="phones"  type="input"/>
	                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.mobilePhone}" rowclass="phones" type="input"/>
	                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.postal}" maxlength="5" rowclass="postal"  type="input"/>
	                    <input id="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.cityJSON.htmlName}" rowclass="city"  name="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.cityJSON.htmlName}" type="hidden" />
	                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.country}" type="select" rowclass="ct"   attributes = "${disabledAttrs}"/>
	                    
	                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.cities.city}" rowclass="one city-col" type="select" xhtmlclass="city"  attributes = "${disabledAttrs}" />
	                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.states.state}"  rowclass="one state-col" type="select" xhtmlclass="state" attributes = "${disabledAttrs}" />
	                    <input type="checkbox" id="${pdict.CurrentForms.singleshipping.shippingAddress.keepAddress.htmlName}" name="${pdict.CurrentForms.singleshipping.shippingAddress.keepAddress.htmlName}" <isif condition="${pdict.CurrentForms.singleshipping.shippingAddress.keepAddress.htmlValue}" >checked="checked"</isif> style="display:none;" />
	
	                    <iscomment>Use address for Billing Address</iscomment>
	                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.useAsBillingAddress}" type="checkbox"/>
	                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addToAddressBook}" type="checkbox"/>
	                    <isinclude template="checkout/components/addresssuggestion" />
                    
	                    <iscomment><isdynamicform formobject="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields}" formdata="${currentCountry.dynamicForms.addressDetails}"/></iscomment>
	                    <h2 class="email-legend">
	                        ${Resource.msg('singleshipping.contactinfo','checkout',null)}
	                        <div class="dialog-required"> <span class="required-indicator">* <em>${Resource.msg('global.requiredfield','locale',null)}</em></span></div>
	                    </h2>
					</isif>
                   
                    <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.email.emailAddress}" type="input" rowclass="emailaddr"/>
                    <isif condition="${dw.content.ContentMgr.getContent('zipcode-change') && dw.content.ContentMgr.getContent('zipcode-change').isOnline()}">
                        <a title="Zip code change" href="${URLUtils.url('Page-Show','cid','zipcode-change')}" class="zipcodechange">zipcode change</a>
                    </isif>
                    <iscomment>Add address to Address Book</iscomment>
                        <isif condition="${pdict.CurrentCustomer.authenticated}"></isif>
                        <div class="addtoaddressbook-wrapper">
                            <div class="form-row label-inline form-indent form-checkbox">
                                <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addToSubscription}" type="checkbox" rowclass="addprivacy"/>
                                <iscomment><a href="#" class="privacy-link">${Resource.msg('singleshipping.privacylink','checkout',null)}</a></iscomment>
                                 <a title="${Resource.msg('global.privacypolicy','locale',null)}" href="${URLUtils.url('Page-Show','cid','privacy-policy')}" class="privacy-policy" >${Resource.msg('global.privacypolicy','locale',null)}</a>
                            </div>
                        </div>
                    
                    <input id="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.fleetwiseToken.htmlName}" name="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.fleetwiseToken.htmlName}" type="hidden" />
                    <input id="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.deliveryDate.htmlName}" name="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.deliveryDate.htmlName}" type="hidden" />


                    <iscomment>Is this a gift</iscomment>
                    <iscomment>
                        <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.isGift}" type="radio"/>

                        <isscript>
                            var attributes = {
                                rows: 4,
                                cols: 10,
                                'data-character-limit': 250
                            };
                        </isscript>
                        <isinputfield rowclass="gift-message-text" formfield="${pdict.CurrentForms.singleshipping.shippingAddress.giftMessage}" type="textarea" attributes="${attributes}"/>
                    </iscomment>
                </fieldset>
<iscomment>
                <div id="shipping-method-list">
                    <isinclude url="${URLUtils.https('COShipping-UpdateShippingMethodList')}"/>
                </div>
</iscomment>
        </isif>
        <fieldset>

            <isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('enableStorePickUp') && isBopisAvailable}">
                <isset name="instoreShipmentsExists" value="${false}" scope="page"/>
                <isinclude template="checkout/shipping/storepickup/instoremessages"/>
            <iselse/>
                <isset name="instoreShipmentsExists" value="${false}" scope="page"/>
            </isif>
            <isif condition="${pdict.ShowMattressRemovalOption && !pdict.MattressRemovalInBasket && !dw.system.Site.getCurrent().getCustomPreferenceValue('hideHaulAwayOptions')}">
                <div class="haulaway">
                <h2 class="email-legend">
                    ${Resource.msg('singleshipping.haulaway','checkout','Haulaway Service')}
                     <div class="dialog-required"> <span class="required-indicator">* <em>${Resource.msg('global.requiredfield','locale',null)}</em></span></div>
                </h2>
                  <isslot id="mattress-removal" description="mattress removal on the cart page" context="global"/>
                  <div class="items-to-remove">
                    <iscomment><iscontentasset aid="haulaway-service"/></iscomment>
                    <div class="mattress-first">
                        <input type="radio" name="mattressremoval" value="yes" class="uniform"/>
                        <label for="mattressremoval" >Yes, I would like to add Haulaway Service to my order. </label>
                        <div class="header">
                               ${Resource.msg('cart.itemstoremove','checkout',' Number of items')}
                        </div>
                        <div class="pickup">
                            <input maxlength="1" type="text" name="${pdict.CurrentForms.singleshipping.pickups.htmlName}" id="${pdict.CurrentForms.singleshipping.pickups.htmlName}" value="${!empty(pdict.CurrentForms.singleshipping.pickups.value) ? StringUtils.formatNumber(pdict.CurrentForms.singleshipping.pickups.value,"#",locale) : ""}" disabled="disabled"/>
                            <div class="error vh" id="errmsg">
                                  ${Resource.msg('cart.numberofmattresses','checkout',null)}
                            </div>
                            
                        </div>
                        <span class="tooltip">
                            <div class="tooltip-content" data-layout="small">
                                <iscontentasset aid="mattress-first-yes"/>
                            </div>
                        </span>
                        <div class="clear"></div>
                    </div>
                    <div class="mattress-second">
                        <input type="radio" name="mattressremoval" value="no" checked="true" class="uniform"/>
                        <label for="mattressremoval" >No, I don't want to request this service.</label>
                            <span class="tooltip">
                                <div class="tooltip-content" data-layout="small">
                                    <iscontentasset aid="mattress-first-no"/>
                                </div>
                            </span>
                        <div class="clear"></div>
                    </div>
                  </div>
            <iselse>
                <div class="cart-advert">
                    <iscontentasset aid="cart-advert"/>
                </div>
            </isif>

			<div id="hawaii-message">
				<iscontentasset aid="hawaii-message"/>
			</div>
			
            <isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('enableStorePickUp') && instoreShipmentsExists}">
                <div class="form-row form-row-button instore-continue-button">
            <iselse/>
                <div class="form-row form-row-button">
            </isif>
                <button class="button-fancy-large" type="submit" name="${pdict.CurrentForms.singleshipping.shippingAddress.save.htmlName}" value="${Resource.msg('global.continuebilling','locale',null)}"><span>${Resource.msg('global.continuebilling','locale',null)}</span></button>
                <a href="${URLUtils.https('Cart-Show')}" class="back" title="${Resource.msg('global.back','locale',null)}">${Resource.msg('global.back','locale',null)}</a>
            </div>

            <iscomment>Entry point for Multi-Shipping (disabled on purpose)</iscomment>
            <isif condition="${pdict.Basket.productLineItems.size() > 1 && false}">
                <div class="ship-to-multiple">
                    ${Resource.msg('singleshipping.multiple','checkout',null)} <a href="${URLUtils.https('COShippingMultiple-Start')}">${Resource.msg('global.yes','locale',null)}</a>
                </div>
            </isif>

            <input type="hidden" name="${pdict.CurrentForms.singleshipping.secureKeyHtmlName}" value="${pdict.CurrentForms.singleshipping.secureKeyValue}"/>

        </fieldset>
    </form>
<isscript>
    importScript("app_storefront_core:util/ViewHelpers.ds");
    var addressForm = pdict.CurrentForms.singleshipping.shippingAddress.addressFields;
    var countries = ViewHelpers.getCountriesAndRegions(addressForm);
    var json = JSON.stringify(countries);
</isscript>
<script>
window.Countries = <isprint value="${json}" encoding="off"/>;
window.initialCity = <isprint value="${JSON.stringify(pdict.CurrentForms.singleshipping.shippingAddress.addressFields.cities.city.value)}" encoding="off"/>;
</script>
</isdecorate>
</isif>