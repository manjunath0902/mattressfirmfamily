<iscontent type="text/html" charset="UTF-8" compact="true"/>
<iscomment>
	Desc:
		Creates the variation section if product is a variation or a master,
		otherwise

	Parameter:
		Product - the product to use 

	Options:
		none
	Available if empty: no
	Full-Block-Element: yes
</iscomment>
<isscript>
	importPackage(dw.system);
	importPackage(dw.util);
	var ProductUtils = require('app_storefront_core/cartridge/scripts/product/ProductUtils.js');
	var qs = ProductUtils.getQueryString(pdict.CurrentHttpParameterMap, ['source', 'uuid', 'v1']),
		qsAppend = qs.length == 0 ? '' : ('&' + qs),
		pUtil = new ProductUtils(pdict),
		PVM = pdict.CurrentVariationModel != null ? pdict.CurrentVariationModel : pdict.Product.variationModel,
		selectedAtts = (pdict.Product.isVariant() || pdict.Product.isVariationGroup()) ? ProductUtils.getSelectedAttributes(PVM) : {},
		sizeChartShown = false,
		swatchAttributes = ['color', 'size', 'width', 'waist', 'length', 'foundation'],
		selectedVariants = [],
		selectedVariant = !empty(pdict.CurrentVariationModel.selectedVariant) ? pdict.CurrentVariationModel.selectedVariant : ProductUtils.getDefaultVariant(PVM);
	if (pdict.Product.variationGroup) {
		PVM = pdict.Product.variationGroup.getVariationModel();
	}
</isscript>
<isif condition="${pdict.Product.variant || pdict.Product.variationGroup || pdict.Product.master}">
	<div class="product-variations" data-attributes="${JSON.stringify(selectedAtts)}">
		<h2 class="visually-hidden">Variations</h2>
		<iscomment>
			Filter out variation attribute values with no orderable variants.
			The "clean" ProductVariationModel of the master without any selected attribute values is used to filter the variants.
			Otherwise hasOrderableVariants() would use currently selected values resulting in a too narrow selection of variants.
		</iscomment>
		<isset name="cleanPVM" value="${(pdict.Product.variant ? pdict.Product.masterProduct.variationModel : pdict.Product.variationModel)}" scope="page"/>
		<ul>
		<isif condition="${typeof(cartInterceptRendering) == 'undefined'}">
			<isset name="PV_Attributes" value="${PVM.productVariationAttributes.toArray()}" scope="page"/>
		<iselse/>
			<isset name="PV_Attributes" value="${PVM.productVariationAttributes.toArray().reverse()}" scope="page"/>
		</isif>
		<isloop items="${PV_Attributes}" var="VA">
			<isset name="VAVALS" value="${PVM.getAllValues(VA)}" scope="page"/>
			<isset name="Valength" value="${VAVALS.length}" scope="page"/>
			<isset name="vaId" value="${VA.getAttributeID()}" scope="page"/>
			<isif condition="${swatchAttributes.indexOf(vaId) >= 0}">
				<li class="attribute">
					<div class="label ${VA.displayName}">
						<label for="size"><isprint value="${VA.displayName}"/> </label><isif condition="${!empty(pdict.Product.custom[vaId]) && !empty(PVM.getSelectedValue(VA).displayValue)}"><span class="selected-attr-value" >${PVM.getSelectedValue(VA).displayValue}</span></isif>
						<span class="tooltip" data-layout="small" tabindex='0' role="tooltip" aria-label="size help icon">
							<div class="tooltip-content">
								<iscontentasset aid="${'pdp-information-' + VA.ID}" />
							</div>
						</span>
					</div>
					<div class="value" id="select-container-${vaId.toLowerCase()}">
					<isif condition="${VA.displayName.toLowerCase()=='size'}">
					 <input type="hidden" id="lockDropdwon" value="false">
					<div class="size-select">	
						<div class="title">							
							<span class="tooltip-part">								
								<div class="tooltip-holder">									
									<isset name="selectedDisplayValue" value="" scope="page"/>	
									<isset name="selectedCustomDescription" value="" scope="page"/>							
										<isloop items="${VAVALS}" var="VV">											
												<isset name="lgImage" value="${PVM.getImage('large', VA, VV)}" scope="page"/>
												<isscript>
													var selected = ""; 								
													var productVariationAttribHashMap : dw.util.HashMap = new dw.util.HashMap();
													productVariationAttribHashMap.clear();
													productVariationAttribHashMap.put(VA.ID, VV.ID);
													var sizeVariants = PVM.getVariants(productVariationAttribHashMap);
													var sizeVariant = sizeVariants[0];													
													
													if(PVM.isSelectedAttributeValue(VA, VV))
													{
														selected = 'selected';
														selectedDisplayValue =  (VV.displayValue||VV.value);
														if(!empty(sizeVariant.custom.sizeDimmension))
														{
															selectedCustomDescription = selectedDisplayValue + ' ' + sizeVariant.custom.sizeDimmension;
														}
														else
														{
															selectedCustomDescription = selectedDisplayValue;
														}
													}																									
												</isscript>												
										</isloop>									
								</div>
							</span>
						</div>						
						<span class="size-select-area">
							<span class="fake-input">
								<isset name="selectedSizeIcon" value="${selectedDisplayValue.replace(/ /g,'-').replace(/"|'|&/g,'').toLowerCase()}" scope="page"/>
								<span>
									<isif condition="${!empty(selectedDisplayValue)}">
										<img src="${URLUtils.staticURL(dw.web.URLUtils.CONTEXT_LIBRARY, null,'/category-landing/plp-assets-v1/icon-black-'+selectedSizeIcon+'.svg')}"  />
									<iselse>										
										<span class="no-variation"><isprint value="${'Select Size'}" /></span>
									</isif>
								</span>						  
								<span class='selected-size-value'><span id="selectedValue" class="selectedValue" onfocus="this.blur()" readonly="readonly">${selectedDisplayValue}</span> <span class='selected-size-dimension'></span> </span>
							</span>
							<ul class="variation-select uniform" id="va-${vaId}" name="${'dwvar_' + pdict.Product.variationModel.master.ID + '_' + vaId}">
								<isloop items="${VAVALS}" var="VV">									
										<isscript>
											var selected = PVM.isSelectedAttributeValue(VA, VV) ? 'selected' : '',
												linkURL = selected ? PVM.urlUnselectVariationValue('Product-Variation', VA) : PVM.urlSelectVariationValue('Product-Variation', VA, VV),
												//isAvailable = selectedVariants.length > 0 ? pUtil.isVariantAvailable(VA.ID + '-' + VV.value, selectedVariants) : true,
												displayValue = (VV.displayValue||VV.value);
												isSelectedNew = PVM.isSelectedAttributeValue(VA, VV);
											if (selected.length > 0) {
												selectedVariants.push(VA.ID + '-' + VV.value);
											}
											
											linkURL = linkURL + qsAppend;

											if (pdict.Product.variationGroup) {
												variationGroup = pdict.Product;
											} else if (pdict.CurrentHttpParameterMap.vgid) {
												variationGroup = dw.catalog.ProductMgr.getProduct(pdict.CurrentHttpParameterMap.vgid);
											}
											if (variationGroup) {
												linkURL += '&vgid=' + variationGroup.ID;												
											}
											
											var productVariationAttribHashMap : dw.util.HashMap = new dw.util.HashMap();
											productVariationAttribHashMap.clear();
											productVariationAttribHashMap.put(VA.ID, VV.ID);
											var sizeVariants = PVM.getVariants(productVariationAttribHashMap);
											var sizeVariant = sizeVariants[0];																						
											
										</isscript>
										<isset name="sizeIcon" value="${displayValue.replace(/ /g,'-').replace(/"|'|&/g,'').toLowerCase()}" scope="page"/>
											<li class="select-item li-select ${selected}${(!cleanPVM.hasOrderableVariants(VA, VV))?' out-of-stock':''}"  data-value="${VV.value}" data-url="${linkURL}"  title="<isprint value="${displayValue}"/>" data-masterid="${pdict.Product.variationModel.getMaster().custom.external_id.toUpperCase()}" data-ampset="${VV.getID().replace(/\s|\//g, '').toUpperCase()}" >												
												 <input type="hidden" class="size-dimension" value="${' ' + (sizeVariant.custom.sizeDimmension != null ? sizeVariant.custom.sizeDimmension : '')}">
												<img src="${URLUtils.staticURL(dw.web.URLUtils.CONTEXT_LIBRARY, null,'/category-landing/plp-assets-v1/icon-black-'+sizeIcon+'.svg')}"  />
												<isprint value="${displayValue.toUpperCase()}"/> 
												<span class="price">${(sizeVariant.custom.sizeDimmension != null ? sizeVariant.custom.sizeDimmension : '')}</span>
											</li>										
								</isloop>
							</ul>
						</span>
					</div>  
					<iselse>
						<select class="swatches ${vaId.toLowerCase()} uniform select-variation-dropdown" id="size" role='Combo box'>
							<isset name="selectedSwatch" value="${''}" scope="page"/>
							<option value="${PVM.urlUnselectVariationValue('Product-Variation', VA)}+${qsAppend}" title="">${Resource.msg('product.variations.labelselect','product',null)} <isprint value="${VA.displayName}"/></option>
							<isloop items="${VAVALS}" var="VV">
								<isif condition="${cleanPVM.hasOrderableVariants(VA, VV)}">
									<isscript>
										var lgImage = PVM.getImage('large', VA, VV);
										var swImage = VV.getImage('swatch');
										var selectable = PVM.hasOrderableVariants(VA, VV);
										var isSelected = PVM.isSelectedAttributeValue(VA, VV);
										var displayValue = VV.displayValue ? VV.displayValue : VV.value;
										var linkURL = PVM.urlSelectVariationValue('Product-Variation', VA, VV);
										var hiresURL = VV.getImage('hi-res') ? VV.getImage('hi-res').getURL() : '';
										var swatchClass = selectable ? 'selectable' : 'unselectable';
										var variationGroup;

										if (isSelected) {
											swatchClass += ' selected';
											selectedSwatch = displayValue;
											linkURL = PVM.urlUnselectVariationValue('Product-Variation', VA);
											selectedVariants.push(VA.ID + '-' + VV.value);
										}
										linkURL = linkURL + qsAppend;

										if (pdict.Product.variationGroup) {
											variationGroup = pdict.Product;
										} else if (pdict.CurrentHttpParameterMap.vgid) {
											variationGroup = dw.catalog.ProductMgr.getProduct(pdict.CurrentHttpParameterMap.vgid);
										}
										if (variationGroup) {
											linkURL += '&vgid=' + variationGroup.ID;
											// PVM.getVariationValue returns `null` for attribute that
											// is not assigned to the variation group
											if (PVM.getVariationValue(variationGroup, VA) === null) {
												swatchClass += ' variation-group-value';
											}
										}
										//RA-5/8/19-MAT-1446									
										var displaySizeDimmension= VA.displayName=='Size' ? true: false;
										var productVariationAttribHashMap : dw.util.HashMap = new dw.util.HashMap();
										productVariationAttribHashMap.clear();
										productVariationAttribHashMap.put(VA.ID, VV.ID);
										var sizeVariants = PVM.getVariants(productVariationAttribHashMap);
										var sizeVariant = sizeVariants[0];																																			
										
									</isscript>
									<isset name="sizeIcon" value="${displayValue.replace(/ /g,'-').replace(/"|'|&/g,'').toLowerCase()}" scope="page"/>									
									<isif condition="${selectable}">
                                        <option style="background:url('${URLUtils.staticURL(dw.web.URLUtils.CONTEXT_LIBRARY, null,'/category-landing/plp-assets-v1/icon-black-'+sizeIcon+'.svg')}')" class="${isSelected ? 'selected' : ''}${(Valength == 1 && pdict.Product.isVariant() == false) ? ' defaultselect' : ''}  ${sizeIcon}" <isif condition="${isSelected}">selected="selected"</isif> data-masterid="${pdict.Product.variationModel.getMaster().custom.external_id.toUpperCase()}" data-ampset="${VV.getID().replace(/\s|\//g, '').toUpperCase()}"  value="${linkURL}" title="${displayValue}" data-lgimg='<isif condition="${!empty(lgImage)}">{"url":"${lgImage.getURL()}", "title":"${lgImage.title}", "alt":"${lgImage.alt}", "hires":"${hiresURL}"}</isif>'>                                                
                                               <isprint value="${displayValue.toUpperCase()}"/>
                                               <iscomment>RA-5/8/19-MAT-1446- Append Size Dimension</iscomment>                                               
                                               <isif condition="${displaySizeDimmension}"> 
                                               		<span class="price">${(sizeVariant.custom.sizeDimmension != null ? sizeVariant.custom.sizeDimmension : '')}</span>
                                               </isif>                                                                                                                                     
                                          </option>
									<iscomment>
									<iselse/>
										<option class="unselectable" disable="disable"><isprint value="${displayValue}"/></option>
									</iscomment>
									</isif>
								</isif>
							</isloop>
							</select>
					</isif>	
						



						<iscomment>Size Chart link</iscomment>
						<isif condition="${vaId != 'color' && !sizeChartShown}">
							<isscript>
								// get category from products primary category
								var category = pdict.Product.primaryCategory,
									sizeChartID;

								// get category from product master if not set at variant
								if (!category && pdict.Product.variant) {
									category = pdict.Product.masterProduct.primaryCategory;
								}

								while (category && !sizeChartID) {
									if ('sizeChartID' in category.custom && !empty(category.custom.sizeChartID)) {
										sizeChartID = category.custom.sizeChartID;
									} else {
										category = category.parent;
									}
								}
							</isscript>
							<isif condition="${!empty(sizeChartID)}">
								<div class="size-chart-link">
									<a href="${URLUtils.url('Page-Show','cid', sizeChartID)}" target="_blank" title="${Resource.msg('product.variations.sizechart.label', 'product', null)}">${Resource.msg('product.variations.sizechart', 'product', null)}</a>
								</div>
								<isscript>
									sizeChartShown = true;
								</isscript>
							</isif>
						</isif>
					</div>
				</li>
			<iselse/>
				<iscomment>Drop down list</iscomment>
				<li class="attribute variant-dropdown">
					<div class="label">
						<span><isprint value="${VA.displayName}"/>: </span><isif condition="${!empty(pdict.Product.custom[vaId])}"><span class="selected-attr-value"><isprint value="${pdict.Product.custom[vaId]}"></span></isif>
						<span class="tooltip" tabindex='0' role="tooltip" aria-label="size help icon">
							<div class="tooltip-content" data-layout="small">
								<iscontentasset aid="${'pdp-information-' + VA.ID}" />
							</div>
						</span>
					</div>
					<div class="value">
						<select class="variation-select swatches uniform" id="va-${vaId}" name="${'dwvar_' + pdict.Product.variationModel.master.ID + '_' + vaId}">
							<option value="${PVM.urlUnselectVariationValue('Product-Variation', VA)}" class="emptytext" title="" >${Resource.msg('global.select','locale',null)} <isprint value="${VA.displayName}"/></option>
							<isloop items="${VAVALS}" var="VV">
								<iscomment>filter out unorderable variation attribute values</iscomment>
								<isif condition="${cleanPVM.hasOrderableVariants(VA, VV)}">
									<isset name="lgImage" value="${PVM.getImage('large', VA, VV)}" scope="page"/>
									<isscript>
										var selected = PVM.isSelectedAttributeValue(VA, VV) ? 'selected="selected"' : '',
											linkURL = selected ? PVM.urlUnselectVariationValue('Product-Variation', VA) : PVM.urlSelectVariationValue('Product-Variation', VA, VV),
											isAvailable = selectedVariants.length > 0 ? pUtil.isVariantAvailable(VA.ID + '-' + VV.value, selectedVariants) : true,
											displayValue = (VV.displayValue||VV.value);
											isSelectedNew = PVM.isSelectedAttributeValue(VA, VV);
										if (selected.length > 0) {
											selectedVariants.push(VA.ID + '-' + VV.value);
										}
									</isscript>
									<isif condition="${isAvailable}">
       									<option <isif condition="${isSelectedNew}">selected="selected" class="selected"</isif> data-masterid="${pdict.Product.variationModel.getMaster().custom.external_id.toUpperCase()}" data-ampset="${VV.getID().replace(/\s|\//g, '').toUpperCase()}"  value="${PVM.urlSelectVariationValue('Product-Variation', VA, VV)}&source=${pdict.CurrentHttpParameterMap.source.stringValue||'detail'}<isif condition="${empty(pdict.CurrentHttpParameterMap.uuid)}">&uuid=${pdict.CurrentHttpParameterMap.uuid.stringValue}</isif>" data-lgimg='<isif condition="${!empty(lgImage)}">{"url":"${lgImage.getURL()}", "title":"${lgImage.title}", "alt":"${lgImage.alt}", "hires":"${hiresURL}"}</isif>' title="<isprint value="${(VV.displayValue||VV.value)}"/>">
											<isprint value="${(VV.displayValue||VV.value)}"/>
										</option>
									</isif>
								</isif>
							</isloop>
						</select>
					</div>
				</li>
			</isif>
		</isloop>
		</ul>
	</div>
</isif>
<isif condition="${pdict.Product.custom.isClearanceItem}">
	<div class="product-variations">
		<ul>
			<isif condition="${!empty(pdict.Product.custom.size)}">
				<isset name="sizes" value="${JSON.parse(dw.system.Site.current.preferences.custom['sizes'])}" scope="page" />
				<li class="attribute">
					<div class="label"><label for="size">Size: </label></div>
					<div class="value" id="select-container-size">
					   <div class="selector" id="uniform-size" style="width: 593px;">
					      <span style="width: 593px; user-select: none;"><isprint value="${sizes[pdict.Product.custom.size]}" /></span>
					      <select class="swatches size uniform select-variation-dropdown" id="size" role="Combo box">
					         <option selected="selected" class="selected" data-ampset="120716_FULL_MSET" value="" title="Full" data-lgimg="">
					         	<isprint value="${sizes[pdict.Product.custom.size]}" />
					         </option>
					      </select>
					   </div>
					</div>
				</li>
			</isif>
			
			<isif condition="${!empty(pdict.Product.custom.color)}">
				<isset name="colors" value="${JSON.parse(dw.system.Site.current.preferences.custom['colors'])}" scope="page" />
				<li class="attribute">
					<div class="label"><label for="size">Color: </label></div>
					<div class="value" id="select-container-color">
					   <div class="selector" id="uniform-size" style="width: 593px;">
					      <span style="width: 593px; user-select: none;"><isprint value="${colors[pdict.Product.custom.color]}" /></span>
					      <select class="swatches size uniform select-variation-dropdown" id="size" role="Combo box">
					         <option selected="selected" class="selected" data-ampset="120716_FULL_MSET" value="" title="Full" data-lgimg="">
					         	<isprint value="${colors[pdict.Product.custom.color]}" />
					         </option>
					      </select>
					   </div>
					</div>
				</li>
			</isif>
			
			<isif condition="${!empty(pdict.Product.custom.foundation)}">
				<isset name="foundations" value="${JSON.parse(dw.system.Site.current.preferences.custom['foundation'])}" scope="page" />
				<li class="attribute">
					<div class="label"><label for="size">Foundation: </label></div>
					<div class="value" id="select-container-color">
					   <div class="selector" id="uniform-size" style="width: 593px;">
					      <span style="width: 593px; user-select: none;"><isprint value="${foundations[pdict.Product.custom.foundation]}" /></span>
					      <select class="swatches size uniform select-variation-dropdown" id="size" role="Combo box">
					         <option selected="selected" class="selected" data-ampset="120716_FULL_MSET" value="" title="Full" data-lgimg="">
					         	<isprint value="${foundations[pdict.Product.custom.foundation]}" />
					         </option>
					      </select>
					   </div>
					</div>
				</li>
			</isif>
		</ul>
	</div>
</isif>