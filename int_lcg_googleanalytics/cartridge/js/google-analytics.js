/*global ga */
'use strict';
// Extend the DW.googleAnalytics object, currently only containing the configuration, with the rest of the functionality
var GoogleAnalytics = {
	/**
	 * Initializes the Google Analytics library with the given namespace and configuration
	 *
	 * @param namespace Page the user is currently on
	 * @param config Google Analytics configuration data
	 */
	init: function (namespace, config) { // fires on every page load
		// Add the given configuration to the object
		this.config = config;
		// Only continue if we have a configuration and an account has been supplied
		if (this.config && this.config.accountID) {
			// set account ID
			ga('create', this.config.accountID, 'auto');
			ga('require', 'linkid', 'linkid.js');
			ga('require', 'displayfeatures');
			ga('require', 'ec');
			ga('set', '&cu', this.config.currency);
			ga('send', 'pageview');

			// execute page specific GA initializations
			if (this[namespace] && this[namespace].init) {
				this[namespace].init(this.config);
			}
		}
	},

	search: { // search & category pages
		init: function (config) {
			if (config.searchResultsCount) {
				ga('set', {'dimension3': 'Search Results Count', 'metric3': config.searchResultsCount});
			}

			if (config.categoryID) {
				ga('set', 'dimension2', config.categoryID);
			}

			$('#main').on('click', '.refinements a', function (e) {
				ga('set', 'dimension1', e.target.title);
			});

			// compare
			$('#main').on('click', '#compare-items-button', function () {
				ga('send', 'event', 'button', 'click', 'Compare Items');
			});

			// add to cart
			$('body').on('click', '.add-to-cart', function () {
				ga('ec:addProduct', {
					'id': $('#pid').val(),
					'name': $('[itemprop=name]').text()
				});
				ga('ec:setAction', 'add');
				ga('send', 'event', 'QuickView', 'click', 'Add To Shopping Cart');
			});
		}
	},

	categorylanding: { //category landing pages
		init: function (config) {
			if (config.categoryID) {
				ga('set', 'dimension2', config.categoryID);
			}
		}
	},

	product: { //PDP
		init: function () {
			var $pdpMain = $('#pdpMain'),
				$recommendations = $('#carousel-recomendations');
			// Fire the product detail page view event if the product number is in the correct location
			if ($('.product-number [itemprop=productID]').length > 0) {
				var addOptions = {
					id: $('[itemprop=productID]').text(),
					name: $('[itemprop=name]').text()
				};

				// Add the top level category if its available in the breadcrumbs
				if ($('.breadcrumb .breadcrumb-element:eq(1)').length) {
					addOptions.category = $('.breadcrumb .breadcrumb-element:eq(1)').text().replace(/\n/g, '');
				}

				ga('ec:addProduct', addOptions);
				ga('ec:setAction', 'detail');
			}
			// Add to cart event
			$pdpMain.on('click', '.add-to-cart', function () {
				var self = this,
					addOptions = {
						id: $(self).siblings('input[name=pid]').val()
					};

				// Retrieve the product name from a different location depending on the PDP type
				if ($('.product-set-item').length) {
					addOptions.name = $(self).parents('.product-set-item').find('.item-name').text().replace(/\n/g, '');
				} else {
					addOptions.name = $('[itemprop=name]').text().replace(/\n/g, '');
				}

				// Add the top level category if its available in the breadcrumbs
				if ($('.breadcrumb .breadcrumb-element:eq(1)').length) {
					addOptions.category = $('.breadcrumb .breadcrumb-element:eq(1)').text().replace(/\n/g, '');
				}

				ga('ec:addProduct', addOptions);
				ga('ec:setAction', 'add');
				ga('send', 'event', 'PDP', 'click', 'Add To Shopping Cart');
			});

			//recommendations
			$recommendations.on('click', '.product-image.recommendation_image a', function (e) {
				ga('send', 'event', 'Related Product', 'click', $('.analytics.capture-product-id').html(), {'page': e.target.src});
			});
			$recommendations.on('click', '.product-name a', function (e) {
				ga('send', 'event', 'Related Product', 'click', $('.analytics.capture-product-id').html(), {'page': e.target.href});
			});
		}
	},

	orderconfirmation: {
		init: function (config) { //order confirmation page initialize

			var order = config.order;

			for (var i = 0; i < order.products.length; i++) { // all products in order
				var product = order.products[i];
				ga('ec:addProduct', {							// Provide product details in an productFieldObject.
					'id': product.ID,							// Product ID (string).
					'name': '\'' + product.name + '\'',			// Product name (string).
					'category': '\'' + product.category + '\'',	// Product category (string).
					'price': product.price,						// Product price (currency).
					'quantity': product.quantity				// Product quantity (number).
				});
				ga('ec:setAction', 'checkout', {
					'step': 4
				});
			}

			ga('ec:setAction', 'purchase', {						// Transaction details are provided in an actionFieldObject
				'id': order.orderNo,								// (Required) Transaction id (string)
				'affiliation': order.storeName,						// Affiliation (string)
				'revenue': order.adjustedMerchandizeTotalNetPrice,	// Revenue (currency)
				'tax': order.totalTax,								// Tax (currency)
				'shipping': order.adjustedShippingTotalNetPrice		// Shipping (currency)
			});

			ga('send', 'event', 'ReportingEvent-Start', '5', 'OrderSummary');
		}
	},

	checkout: { // checkout pages
		init: function (config) {
			if (config.checkoutStep) {
				switch (config.checkoutStep) {
					case 'cart':
					this.cartSubmit();
					break;
					case 'shipping':
					this.shippingSubmit();
					break;
					case 'billing':
					this.billingSubmit();
					break;
				}
			}
		},

		cartSubmit: function () { // cart page submitted
			ga('ec:setAction', 'checkout', {
				'step': 1
			});
			ga('send', 'event', 'ReportingEvent-Start', '1', 'CheckoutMethod');
		},

		shippingSubmit: function () { // shipping page submitted
			ga('ec:setAction', 'checkout', {
				'step': 2
			});
			ga('send', 'event', 'ReportingEvent-Start', '2', 'ShippingAddress');
			ga('send', 'event', 'ReportingEvent-Start', '3', 'ShippingMethod');
		},

		billingSubmit: function () { // billing page submitted
			ga('ec:setAction', 'checkout', {
				'step': 3
			});
			ga('send', 'event', 'ReportingEvent-Start', '4', 'Billing');
		}
	}
};

module.exports = GoogleAnalytics;
