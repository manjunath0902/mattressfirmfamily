(function (app){
    app.elements = {};
    app.listners = {
        closeModale : function (event) {
            var clickedEl = event.target;
            if(clickedEl.id == 'modale_wrapper' || clickedEl.id == 'modale_title'){
                app.elements.modaleWrapper.style.display = 'none';
                app.elements.modaleTitle.innerHTML = '';
                app.elements.modaleContent.innerHTML = '';
            }
        },
        closeBillingAgreement : function (event) {
            var clickedEl = event.target;
            if(clickedEl.tagName === 'BUTTON' && clickedEl.className.indexOf('amazon_order_billing_agreement_action_close') > -1) {
                var orderNo = clickedEl.parentElement.parentElement.getAttribute('data-id');
                var url = app.urls.closeBillingAggrement;
                url += '?orderno='+orderNo;
                        
                var xhr = new XMLHttpRequest();
                xhr.open('GET', url);
                xhr.onload = function() {
                    if (xhr.status === 200) {
                        var responceJSON = JSON.parse(xhr.responseText);
                        if(responceJSON.success){
                            app.showMessage('Success', 'Billing Agreement Closed');
                        } else {
                            app.showMessage('Error', responceJSON.error);
                        }
                    }
                    else {
                        app.showMessage('Error', xhr.status);
                    }
                };
                xhr.send();
            }
        },
        search : function (event) {
            var clickedEl = event.target;
            if(clickedEl.tagName === 'BUTTON'){
                var agreementId = clickedEl.parentElement.children.namedItem('billingagreementid').value.trim();
                var url = app.urls.showBillingAggrements;
                
                if(agreementId.length > 0) {
                    url += '?id='+encodeURIComponent(agreementId) + '&ajax=true';
                } else {
                    url += '?ajax=true';
                }
                
                var xhr = new XMLHttpRequest();
                xhr.open('GET', url);
                xhr.onload = function() {
                    if (xhr.status === 200) {
                        var responceJSON = JSON.parse(xhr.responseText);
                        updatePage(responceJSON);
                    }
                };
                xhr.send();
            }
        },
        changePage : function (event){
            event.preventDefault();
            var clickedEl = event.target;
            if(clickedEl.tagName === 'A' && clickedEl.className.indexOf('current') == -1){
                var page = clickedEl.getAttribute('data-page');
                var url = app.urls.showBillingAggrements;
                url += '?page='+ page + '&ajax=true';
                
                var xhr = new XMLHttpRequest();
                xhr.open('GET', url);
                xhr.onload = function() {
                    if (xhr.status === 200) {
                        var responceJSON = JSON.parse(xhr.responseText);
                        updatePage(responceJSON);
                    }
                };
                xhr.send();
            }
        }
    };
    
    app.init = function (){
        initDOM();
        initEvents();
    };
    
    app.showMessage = function (title, content) {
        app.elements.modaleTitle.innerHTML = title;
        app.elements.modaleContent.innerHTML = content;
        app.elements.modaleWrapper.style.display = 'block';
    };
    
    function initDOM (){
        app.elements.amzonOrders = document.getElementById('amzon_orders');
        app.elements.modaleWrapper = document.getElementById('modale_wrapper');
        app.elements.modaleTitle = document.getElementById('modale_title');
        app.elements.modaleContent = document.getElementById('modale_content');
        app.elements.searchWrapper = document.getElementById('amazon_order_billing_agreements_search');
        app.elements.paginationWrapper = document.getElementById('amazon_order_billing_agreements_pagination');
    }
    
    function initEvents (){
        app.elements.amzonOrders.addEventListener('click', app.listners.closeBillingAgreement);
        app.elements.modaleWrapper.addEventListener('click', app.listners.closeModale);
        app.elements.searchWrapper.addEventListener('click', app.listners.search);
        app.elements.paginationWrapper.addEventListener('click', app.listners.changePage);
        
    }
    
    function updatePage(responceJSON) {
        var tbody = '';
        for (var order of responceJSON.orders){
            var row = '<tr class="amazon_order" data-id="'+order.orderNo+'">';
                row += '<td class="amazon_order_no">'+order.orderNo+'</td>';
                row += '<td class="amazon_order_billing_agreement_active">'+order.amazonBillingAgreementActive+'</td>';
                row += '<td class="amazon_order_billing_agreement_status">'+order.amazonBillingAgreementStatus+'</td>';
                row += '<td class="amazon_order_billing_agreement_id">'+order.amazonBillingAgreementID+'</td>';
                row += '<td class="amazon_order_billing_agreement_action"><button class="amazon_order_billing_agreement_action_close">Close Billing Agreement</button></td>';
                row += '</tr>';
            tbody += row;
        }
        for (var element of app.elements.amzonOrders.children){
            if(element.tagName == 'TBODY'){
                element.innerHTML = tbody;
            }
        }
        var pagination = '';
        for (var page of responceJSON.pagination.pages){
            var pagelink = '<a data-page="'+page.no+'" '+(parseInt(page.no) == responceJSON.pagination.currPage ? 'class="current"' : '')+'>'+page.text+'</a>';
            pagination += pagelink;
        }
        app.elements.paginationWrapper.innerHTML = pagination;
    }
    
})(window.app = window.app || {});

document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        app.init();
    }
}