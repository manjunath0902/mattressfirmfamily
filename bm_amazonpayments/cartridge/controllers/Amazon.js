'use strict';

/**
 * Controller that renders the home page.
 *
 * @module controllers/Amazon
 */


/* API Includes */
var OrderMgr = require('dw/order/OrderMgr');

/* Script Modules */
var app          = require('~/cartridge/scripts/app'),
    guard        = require('~/cartridge/scripts/guard'),
    apController = require('int_amazonpayments/cartridge/controllers/AmazonPayments');



/**
 * Show orders with billing agreements
 */
function showBillingAgreement() {
    var params   = request.httpParameterMap,
        template = 'billing_agreements';

    if (!params.ajax.empty) {
        template = 'billing_agreements_json';
    }

    if (!params.id.empty) {
        var agreementID = decodeURIComponent(params.id.value),
            orders      = OrderMgr.searchOrders('custom.amazonBillingAgreementID = {0}', 'creationDate DESC', agreementID);
    } else {
        var orders = OrderMgr.searchOrders('custom.amazonBillingAgreementID != null', 'creationDate DESC');
    }

    var pagination = {
        prev : false,
        next : false,
        pageSize : 15,
        currPage : 1,
        start : 0,
        pages : []
    }

    pagination.noPages = Math.ceil(orders.count / pagination.pageSize);

    if (!params.page.empty) {
        pagination.currPage = params.page.intValue;
        pagination.start    = (pagination.currPage * pagination.pageSize) - pagination.pageSize;
    }

    if (pagination.noPages > 1) {
        orders = orders.asList(pagination.start, pagination.pageSize);

        if (pagination.currPage > 1) {
            pagination.prev = true;
            pagination.pages.push({
                text : '>>',//'&laquo;',
                no : (pagination.currPage-1).toFixed(0)
            });
        }

        for (var i = 1; i <= pagination.noPages; i++) {
            pagination.pages.push({
                text : i.toFixed(0),
                no : i.toFixed(0)
            });
        }

        if (pagination.currPage < pagination.noPages) {
            pagination.next = true;
            pagination.pages.push({
                text : '>>',//'&raquo;',
                no : (pagination.currPage+1).toFixed(0)
            });
        }
    }
    
    app.getView({
        orders : orders,
        pagination : pagination
    }).render(template);

}

function closeBillingAgreement() {
    var params = request.httpParameterMap;

    if (!params.orderno.empty) {
        var order                       = OrderMgr.getOrder(params.orderno.value);
            closeBillingAgreementResult = apController.CloseBillingAgreement({searchVal : order.custom.amazonBillingAgreementID});
        
        if (closeBillingAgreementResult.error) {
            app.getView({
                ErrorCode : dw.util.StringUtils.encodeString(closeBillingAgreementResult.response, dw.util.StringUtils.ENCODE_TYPE_HTML)
            }).render('util/errorjson');
        } else {
            app.getView().render('util/successjson');
        }
    }
}
/*
 * Export the publicly available controller methods
 */
/** Renders the Amazon billing agreements.
 * @see module:controllers/Amazon~showBillingAgreement */
exports.ShowBillingAgreement = guard.ensure(['get'], showBillingAgreement);
/** Close Amazon Billing Agreement
 * @see module:controllers/Amazon~closeBillingAgreement */
exports.CloseBillingAgreement = guard.ensure(['get'], closeBillingAgreement);