'use strict';

/**
 * Controller that adds and removes products and coupons in the cart.
 * Also provides functions for the continue shopping button and minicart.
 *
 * @module controllers/Cart
 */

/* API Includes */
var ArrayList = require('dw/util/ArrayList');
var ISML = require('dw/template/ISML');
var ProductListMgr = require('dw/customer/ProductListMgr');
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');
var Logger = require('dw/system/Logger');
var Site = require('dw/system/Site');
var Money = require('dw/value/Money');
importScript("app_storefront_core:cart/CartUtils.ds");

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var pipeletHelper = require('bc_sleepysc/cartridge/scripts/sleepys/util/SleepysPipeletsHelper').SleepysPipeletHelper;
var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;
var UtilFunctionsMF = require('app_mattressfirm_storefront/cartridge/scripts/util/UtilFunctions').UtilFunctions; 
var enableFSPProgram = "enableFSPProgram" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("enableFSPProgram") : false;
var ProductUtils = require('app_storefront_core/cartridge/scripts/product/ProductUtils.js');
var StringHelpers = require('app_storefront_core/cartridge/scripts/util/StringHelpers');

/**
 * Redirects the user to the last visited catalog URL if known, otherwise redirects to
 * a hostname-only URL if an alias is set, or to the Home-Show controller function in the default
 * format using the HTTP protocol.
 */
function continueShopping() {

    var location = require('~/cartridge/scripts/util/Browsing').lastCatalogURL();

    if (location) {
    	// MAT-911 - API converts the space into '+' - search ignores the + sign, converting to space for search 
    	if (location.toString().indexOf("%2B") !== -1 ){
    		
    		location = location.toString().replace(new RegExp("%2B", 'g'), "%20");
    		
    	}
    	// MAT-911 - API converts the apos into '2725' - search.Show() find zero results
    	if (location.toString().indexOf("%2527") !== -1){
    		
    		location = location.toString().replace(new RegExp("%2527", 'g'), "%27");
    	}
    	
        response.redirect(location);
    } else {
        response.redirect(URLUtils.httpHome());
    }
}

function backToShopping() {
 
	var location=request.httpParameterMap.backurl.stringValue;
    
    if (location) {
    	// MAT-911 - API converts the space into '+' - search ignores the + sign, converting to space for search 
    	if (location.toString().indexOf("%2B") !== -1 ){
    		
    		location = location.toString().replace(new RegExp("%2B", 'g'), "%20");
    		
    	}
    	// MAT-911 - API converts the apos into '2725' - search.Show() find zero results
    	if (location.toString().indexOf("%2527") !== -1){
    		
    		location = location.toString().replace(new RegExp("%2527", 'g'), "%27");
    	}
    	
        response.redirect(location);
    } else {
        response.redirect(URLUtils.httpHome());
    }
}

/**
 * Invalidates the login and shipment forms. Renders the checkout/cart/cart template.
 */
function show() {
	var cartForm = app.getForm('cart');
    app.getForm('login').invalidate();

    cartForm.get('shipments').invalidate();
    var cart = app.getModel('Cart').get();
    
    //Resetting delivery tracking attributes
	if (!empty(cart)) {
		Transaction.wrap(function () {
		    cart.object.custom.defaultDeliveryDateTimeZoneZip = "";
		  	cart.object.custom.selectedDeliveryDateTimeZoneZip = "";
		    cart.object.custom.addressesDeliveryDateTimeZoneZip = "";
			cart.object.custom.confirmationDeliveryDateTimeZoneZip = "";
		});
		
		if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableDeliveryTiers')) {
			var productLineItems = cart.object.productLineItems;
			Transaction.wrap(function () {
				//Reset delivery tier
				for each(var pli in productLineItems) {
					if (pli.custom.isDeliveryTier) {
						cart.object.removeProductLineItem(pli);
					}
				}
			});
		}
	    var productsGeoAvailabilityMap = UtilFunctionsMF.buildProductsGeoAvailabilityMap(cart.object);
    }
    var cartAsset = app.getModel('Content').get('cart');
    
    pageMeta = require('~/cartridge/scripts/meta');
    pageMeta.update(cartAsset);
    
    // calculate shipping and tax on cart page for mattressFirm
    if (dw.system.Site.getCurrent().getID() == 'Mattress-Firm') {
	    var geolocationZip = request.getGeolocation().getPostalCode();
	    var postalCode = empty(session.custom.customerZip) ? geolocationZip : session.custom.customerZip;
	    if(!empty(postalCode)){
			session.custom.customerZip = postalCode;  
			var COShippingMethodController = require('app_mattressfirm_storefront/cartridge/controllers/COShippingMethod');
			COShippingMethodController.CalculateShippingAndTaxForZip(postalCode);
	    }
	    
    } else if (dw.system.Site.getCurrent().getID() == '1800Mattress-RV') { // calculate shipping and tax on cart page 1800MattressRV
    	    var geolocationZip = request.getGeolocation().getPostalCode();  
    	    var postalCode = empty(session.custom.customerZip) ? geolocationZip : session.custom.customerZip;
    	    if(!empty(postalCode)){
    			session.custom.customerZip = postalCode;  
    			var COShippingMethodController = require('app_1800mattressrv_storefront/cartridge/controllers/COShippingMethod');
    			COShippingMethodController.CalculateShippingAndTaxForZip(postalCode);
    	    }
        }
    var chicagoDCDelivery = {
    		chicagoProductsPresent	: false,
    		chicagoProductsAvailable: false,
    		chicagoAvailabilityClass: '',
    		chicagoAvailabilityMsg	: '',
    		chicagoProductIDs		: [],
    		chicagoOOSSkus			: [],
    		enableCheckoutChicago	: true
    }
    var enableCheckoutChicago = true;
    if (cart) {
        Transaction.wrap(function () {
            pipeletHelper.clearAllShipmentSchedules(cart);
            if (session.custom.deliveryOptionChoice == 'instorepickup') {
            	mattressPipeletHelper.splitShipmentsByType(cart);
            }
            // ATP service call to check availability of products in Chicago DC
            var chicagoATPObj = isATPCallRequired(cart);
            if (chicagoATPObj.atpCallRequired) {
        		var ATPResponseObj = app.getController('StorePicker').GetATPAvailabilityForIndividualChicagoDCAjax(chicagoATPObj.chicagoProductIDs);
        		chicagoDCDelivery.chicagoProductsPresent	= chicagoATPObj.chicagoProductIDs.length > 0 ? true : false;
        		chicagoDCDelivery.chicagoProductsAvailable	= ATPResponseObj.productsAvailable;
        		chicagoDCDelivery.dcParcleProducts			= ATPResponseObj.products;
        		chicagoDCDelivery.chicagoProductIDs			= chicagoATPObj.chicagoProductIDs;
        		chicagoDCDelivery.lastAvailableDate         = ATPResponseObj.lastAvailableDate;
        		chicagoDCDelivery.chicagoOOSSkus			= ATPResponseObj.chicagoOOSSkus;
        	    if (chicagoDCDelivery.chicagoProductsPresent && !chicagoDCDelivery.chicagoProductsAvailable) {
        	    	chicagoDCDelivery.enableCheckoutChicago = false;
        	    }
        	}
            cart.calculate();
        });
    }
    // Add object in array to prevent session string exceed quote violation.
    var chicagoDCDeliveryArray = new dw.util.ArrayList();
    chicagoDCDeliveryArray.add(JSON.stringify(chicagoDCDelivery));
    session.custom.chicagoDCDeliveryStr = chicagoDCDeliveryArray;
    if(cart){
    	var minimumCartValuePref = "minimumCartValue" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("minimumCartValue") : 0;
    	var fivePercentCartValue = minimumCartValuePref * 5 / 100;
    	var minimumCartValue = minimumCartValuePref - fivePercentCartValue;
    				
    	// if First selection program is enabled then check the minimum available cart value
    	// add site check

    	if(enableFSPProgram && dw.system.Site.getCurrent().getID() == 'Mattress-Firm') {
    	    var cart = app.getModel('Cart').get();	    	
    	    var isLineItemQualifiedForFSP = false;
    	    for each(var lineItem in cart.object.productLineItems) {
    	           if (!empty(lineItem) && !empty(lineItem.product) && !empty(lineItem.product.custom.mattressConstruction)) {
    	                isLineItemQualifiedForFSP = ((lineItem.getAdjustedPrice()/lineItem.quantity) >= minimumCartValue) ? true : false;
    	                if(!isLineItemQualifiedForFSP) {	    		    	
    	                	Transaction.wrap(function () {	                    	
    	                    		lineItem.custom.optFirstSelectionProgram = false;
    	                    		lineItem.custom.fspQuantity = 0;	                    		
    	                    		// Remove current price adjustment
    	                    		var currentPriceAdjustment = lineItem.getPriceAdjustmentByPromotionID("firstSelectionProgram");
    	                    		if(currentPriceAdjustment) {
    	                    			lineItem.removePriceAdjustment(currentPriceAdjustment);
    	                    		}	                        
    	                    });
    	    		    	Transaction.wrap(function () {
    	    			  		cart.calculate();
    	    		    	});
    	    	        }
    	            }
    	        }    	
    	}
   	
    }
    //get userGent Mobile|Desktop
    var isMobileDevice = StringHelpers.isMobileDevice(request.httpUserAgent);
    var deviceType = "";
    if (isMobileDevice) {
    	session.custom.checkoutStep = "cart";
		deviceType = "Mobile";
	    if (!empty(cart)) {
		    Transaction.wrap(function () {
			    cart.object.custom.checkoutInfo = deviceType + " Flow";
			});
		}
		var cartTemplate = 'checkout/cart/cart_redesign';
	} else {
		deviceType = "Desktop";
		if (!empty(cart)) {
		    Transaction.wrap(function () {
			    cart.object.custom.checkoutInfo= deviceType + " Flow";
			});
		}
		var cartTemplate = 'checkout/cart/cart';
	}
  
    var allProductsGeoAvailable = true;
    if(cart) {
	     for each(var pli in cart.object.productLineItems) {
	     	if(productsGeoAvailabilityMap[pli.productID] == false) {
	     		allProductsGeoAvailable = false;
	     		break;
	     	}
	     }
	 }
	app.getView('Cart', {
        cart				: app.getModel('Cart').get(),
        RegistrationStatus	: false,
        chicagoDCDelivery	: chicagoDCDelivery,
        ProductsGeoAvailabilityMap : productsGeoAvailabilityMap,
        AllProductsGeoAvailable : allProductsGeoAvailable
    }).render(cartTemplate);

}

/**
 * Invalidates the login and shipment forms. Renders the checkout/cart/cart template.
 */
function showATC() {
	
    app.getView('Cart').render('checkout/cart/cartATC');

}


/**
 * 
 * @param cart
 * @returns
 * Description: ATP service call to check availability of products in Chicago DC
 */
function isATPCallRequired(cart) {
	var Product = app.getModel('Product');
	var chicagoProductIDs = new Array();
	var atpCallRequired = false;
	if (dw.system.Site.getCurrent().getID() == 'Mattress-Firm') {
		var enableChicagoDCDelivery = "enableChicagoDCDelivery" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("enableChicagoDCDelivery") : false;
		
		if (enableChicagoDCDelivery) {
	    	for each (var pli : ProductLineItem in cart.object.getProductLineItems()) {
		    		if (!empty(pli.product) && UtilFunctionsMF.isParcelableAndCore(pli.product.manufacturerSKU)) {
		    			chicagoProductIDs.push(pli.product.manufacturerSKU);
		    			atpCallRequired = true;
		    		}
		    	}
	    }
	}
	return {
		atpCallRequired		: atpCallRequired,
		chicagoProductIDs	: chicagoProductIDs
	}
}

/**
 *
 * @returns true 
 * Description: Reset all price adjustments added to products for FSP 
 */
function resetFSPPriceAdjustments () {
	var cart = app.getModel('Cart').get();
    var basket = cart.object;
    
    Transaction.wrap(function () {
    	for each(var lineItem in basket.productLineItems) {
    		lineItem.custom.optFirstSelectionProgram = false;
    		lineItem.custom.fspQuantity = 0;    		
    		// Remove current price adjustment
    		var currentPriceAdjustment = lineItem.getPriceAdjustmentByPromotionID("firstSelectionProgram");
    		if(currentPriceAdjustment) {
    			lineItem.removePriceAdjustment(currentPriceAdjustment);
    		}
        }
    });
    return true;
}

/**
*
* Description: Opens First Selection Modal
*/
function getFSPDiscountModal () {    
    var cart = app.getModel('Cart').get();
    var basket = cart.object;

    app.getView({
        LineItems: basket.productLineItems
    }).render('checkout/cart/fspdiscountmodal');
}

/**
*
* @returns true or false in json format in response of ajax call
* Description: Adds price adjustments according to the user selection received from front end 
*/
function addFSPPriceAdjustments () {
	
	var cart = app.getModel('Cart').get();
    var basket = cart.object;
    
    var isResetFSPPriceAdjustments =  resetFSPPriceAdjustments();
    
    if(isResetFSPPriceAdjustments) {
    	var data = JSON.parse(request.httpParameterMap.getRequestBodyAsString());

        if(!empty(data)) {
        	if(data.length != 0) {
        		Transaction.wrap(function () {
            		for(var i = 0; i < data.length; i++){
            			//var fspQuantityCounter = 0;            			
            		    	for each(var lineItem in basket.productLineItems) {
            		    		if(data[i].productId == lineItem.productID) {            		    			               		    			
            		    				var fspDiscount = data[i].fspdiscount*-1;            		    				
                		    			                		    			                		    			
                		    			
                		    			//check if this line item has already fsp promo
                		    			var currentPriceAdjustment = lineItem.getPriceAdjustmentByPromotionID("firstSelectionProgram");
                			    		
                		    			//if yes then get the previous amount
                		    			if(!empty(currentPriceAdjustment)){
                		        			
                		        			//add previous amount and new amount
                		    				var prevAmount = currentPriceAdjustment.netPrice.value;
                		        			fspDiscount = Number(fspDiscount)+Number(prevAmount);
                    		    			//remove prev adjustment
                    		    			lineItem.removePriceAdjustment(currentPriceAdjustment);                    		    			
                		        		}
                		    			
                		    			//add new adjustment with updated amount
    									var newAdjustment = lineItem.createPriceAdjustment("firstSelectionProgram");                             
    		                             var fiveP = fspDiscount;
    		                             newAdjustment.setPriceValue(fiveP); 
    		                             newAdjustment.setLineItemText(Resource.msg('cart.fsp.name','checkout',null));
    		                             lineItem.custom.fspQuantity = lineItem.custom.fspQuantity+1; 
    		                             lineItem.custom.optFirstSelectionProgram = true;
            		    		}
            		        }
                	}
            		cart.calculate();
        		});
        	} else {
        		Transaction.wrap(function () {
        			cart.calculate();
        		});
        	}
        }

        let r = require('app_storefront_controllers/cartridge/scripts/util/Response');
        r.renderJSON({
            success: true
        });

    } else {
    	let r = require('app_storefront_controllers/cartridge/scripts/util/Response');
        r.renderJSON({
            success: false
        });
        return;
    }
}

/**
*
* @returns true 
* Description: Remove price adjustments for selected product 
*/
function removeFSPPriceAdjustments() {    
   
   let r = require('app_storefront_controllers/cartridge/scripts/util/Response');
   var cart = app.getModel('Cart').get();
   var basket = cart.object;
   var productID = request.httpParameterMap.productID.stringValue;
   
   if(!empty(productID)){
	    Transaction.wrap(function () {
	    	for each(var lineItem in basket.productLineItems) {
	    		if(lineItem.productID === productID) {
	    			lineItem.custom.optFirstSelectionProgram = false;
	    			lineItem.custom.fspQuantity = 0;	    			
		    		// Remove current price adjustment
		    		var currentPriceAdjustment = lineItem.getPriceAdjustmentByPromotionID("firstSelectionProgram");
		    		if(currentPriceAdjustment) {
		    			lineItem.removePriceAdjustment(currentPriceAdjustment);
		    		}
		    		
	    		}
	        }
	    });
   }
   
   r.renderJSON({
       success: true
   });
}

/**
 * Handles the form actions for the cart.
 * - __addCoupon(formgroup)__ - adds a coupon to the basket in a transaction. Returns a JSON object with parameters for the template.
 * - __calculateTotal__ - returns the cart object.
 * - __checkoutCart__ - validates the cart for checkout. If valid, redirect to the COCustomer-Start controller function to start the checkout. If invalid returns the cart and the results of the validation.
 * - __continueShopping__ - calls the {@link module:controllers/Cart~continueShopping|continueShopping} function and returns null.
 * - __deleteCoupon(formgroup)__ - removes a coupon from the basket in a transaction. Returns a JSON object with parameters for the template
 * - __deleteGiftCertificate(formgroup)__ - removes a gift certificate from the basket in a transaction. Returns a JSON object with parameters for the template.
 * - __deleteProduct(formgroup)__ -  removes a product from the basket in a transaction. Returns a JSON object with parameters for the template.
 * - __editLineItem(formgroup)__ - gets a ProductModel that wraps the pid (product ID) in the httpParameterMap and updates the options to select for the product. Updates the product in a transaction.
 * Renders the checkout/cart/refreshcart template. Returns null.
 * - __login__ - calls the Login controller and returns a JSON object with parameters for the template.
 * - __logout__ - logs the customer out and returns a JSON object with parameters for the template.
 * - __register__ - calls the Account controller StartRegister function. Updates the cart calculation in a transaction and returns null.
 * - __unregistered__ - calls the COShipping controller Start function and returns null.
 * - __updateCart__ - In a transaction, removes zero quantity line items, removes line items for in-store pickup, and copies data to system objects based on the form bindings.
 * Returns a JSON object with parameters for the template.
 * - __error__ - returns null.
 *
 * __Note:__ The CartView sets the ContinueURL to this function, so that any time URLUtils.continueURL() is used in the cart.isml, this function is called.
 * Several actions have <b>formgroup</b> as an input parameter. The formgroup is supplied by the {@link module:models/FormModel~FormModel/handleAction|FormModel handleAction} function in the FormModel module.
 * The formgroup is session.forms.cart object of the triggered action in the form definition. Any object returned by the function for an action is passed in the parameters to the cart template
 * and is accessible using the $pdict.property syntax. For example, if a function returns {CouponStatus: status} is accessible via ${pdict.CouponStatus}
 * Most member functions return a JSON object that contains {cart: cart}. The cart property is used by the CartView to determine the value of
 * $pdict.Basket in the cart.isml template.
 *
 * For any member function that returns an object, the page metadata is updated, the function gets a ContentModel that wraps the cart content asset,
 * and the checkout/cart/cart template is rendered.
 *
 */
function submitForm() {
    // There is no existing state, so resolve the basket again.
    var cart, formResult, cartForm, cartAsset, pageMeta;
    cartForm = app.getForm('cart');
    cart = app.getModel('Cart').goc();
    var action = cartForm.getTriggeredAction();
    var customerSteps = "Landed : "+action.formId+" | ";
    formResult = cartForm.handleAction({
        //Add a coupon if a coupon was entered correctly and is active.
        'addCoupon': function (formgroup) {        	
    		
        	if(enableFSPProgram && dw.system.Site.getCurrent().getID() == 'Mattress-Firm') {
                resetFSPPriceAdjustments();
            }
            var status, result, couponType;
            var basket = cart.object;
            if (formgroup.couponCode.htmlValue) {
            	
            	formgroup.couponCode.htmlValue = formgroup.couponCode.htmlValue.toUpperCase();
                status = Transaction.wrap(function () {
                    return cart.addCoupon(formgroup.couponCode.htmlValue);
                });

                if (status) {
                	Transaction.wrap(function () {
                		cart.removePersonaliCartRescue();
                		cart.removePersonaliFromLineItems();
                	});
                	var couponApplied = false;                	
                	for each(var couponLineItem in basket.couponLineItems) {
                    	if (couponLineItem.couponCode.toUpperCase() == formgroup.couponCode.htmlValue.toUpperCase()) {
                    		couponApplied = couponLineItem.applied;
                    		if(couponApplied) {
                			  var priceAdjustment1 = couponLineItem.getPriceAdjustments().iterator();
                			  while (priceAdjustment1.hasNext()) {
                			   var priceAdjustment2 = priceAdjustment1.next();
                			   couponType = priceAdjustment2.appliedDiscount.type;
                      		  }
                    		}	
                    	}
                    }          
           			var currentCouponBonusLineItems = cart.getBonusLineItemsByCouponCode(formgroup.couponCode.htmlValue.toUpperCase());
        			var currProdQualifiedBonusProducts=cart.getJsonByLineItems(currentCouponBonusLineItems);
                	if (couponApplied) {
	                    result = {
	                        cart: cart,
	                        CouponStatus: status.CouponStatus,
	                        CouponApplied: couponApplied,
	                        CouponType: couponType,
	                        qualifiedBonusProducts : currProdQualifiedBonusProducts,
                        	action : 'addcoupon'
	                    };
                	} else {
                		result = {
    	                        cart: cart,
    	                        CouponStatus: status.CouponStatus,
    	                        CouponApplied: 'none',
    	                        CouponType: 'none'
    	                    };
                	}
                } else {
                    result = {
                        cart: cart,
                        CouponError: 'NO_ACTIVE_PROMOTION',
                        CouponApplied: 'none',
                        CouponType: 'none'
                    };
                }
            } else {
            	var couponStatus = {};
            	couponStatus.error = "true";
                result = {
                    cart: cart,
                    CouponError: 'COUPON_CODE_MISSING',
                    CouponApplied: 'none',
                    CouponType: 'none',
                    CouponStatus: couponStatus
                };
            }
            var showMattressRemovalOption = pipeletHelper.showMatressRemovalOptionCheck(cart);
            result.RegistrationStatus = false;
            result.ShowMattressRemovalOption = showMattressRemovalOption;
            
            var productsGeoAvailabilityMap = UtilFunctionsMF.buildProductsGeoAvailabilityMap(cart.object);
            var allProductsGeoAvailable = true;
            if(cart) {
        	     for each(var pli in cart.object.productLineItems) {
        	     	if(productsGeoAvailabilityMap[pli.productID] == false) {
        	     		allProductsGeoAvailable = false;
        	     		break;
        	     	}
        	     }
        	}

            result.ProductsGeoAvailabilityMap = productsGeoAvailabilityMap;
            result.AllProductsGeoAvailable = allProductsGeoAvailable;
            //get userGent Mobile|Desktop
            var isMobileDevice = StringHelpers.isMobileDevice(request.httpUserAgent);
            if (isMobileDevice) {
            	var cartTemplate = 'checkout/cart/cart_redesign';
        	}else{
        		var cartTemplate = 'checkout/cart/cart';
        	}
            
            app.getView('Cart', result).render(cartTemplate);
            return;

        },
        'calculateTotal': function () {
            // Nothing to do here as re-calculation happens during view anyways
            return {
                cart: cart
            };
        },
        'checkoutCartRedesign': function () {
            var validationResult;
            session.custom.amazonGuestCheckout = false;
            session.custom.amazonDeliveryPage = null;
            session.custom.AmazonAuthErrorMessage=null;
            session.custom.AmazonResultErrorMessage = null;
            session.custom.amazonCustomer = null;

			validationResult = cart.validateForCheckout();

			if (validationResult.EnableCheckout) {
            	response.redirect(URLUtils.https('COShipping-CustomerInfo'));
				return;
			}
        },
        'checkoutCart': function () {
        	var mFinderEmptyCartLogger = Logger.getLogger('MattressFinder','Cart');
            mFinderEmptyCartLogger.info("Cart Checkout Cart Starts.");
            var validationResult, result;
            session.custom.amazonGuestCheckout = false;
            session.custom.amazonDeliveryPage = null;
            //reseting AmazonAuthErrorMessage and AmazonResultErrorMessage sessions
            session.custom.AmazonAuthErrorMessage=null;
            session.custom.AmazonResultErrorMessage = null;
            
            session.custom.amazonCustomer = null;

            validationResult = cart.validateForCheckout();
            mFinderEmptyCartLogger.info("Cart Validation Result : " + validationResult.EnableCheckout + " .");
            mFinderEmptyCartLogger.info("Basket Status : " + validationResult.BasketStatus.message + " .");
            if (validationResult.EnableCheckout) {
                            	
            	if (dw.system.Site.getCurrent().getID() == 'Mattress-Firm' || dw.system.Site.getCurrent().getID() == '1800Mattress-RV') {
            	response.redirect(URLUtils.https('COShippingMethod-Start'));
            	}
            	else
        		{
        		mFinderEmptyCartLogger.info("Jump to COCustomer-Start.");
        		response.redirect(URLUtils.https('COCustomer-Start'));
        		}

            } else {
            	mFinderEmptyCartLogger.info("Invalid Jump from cart.");
                result = {
                    cart: cart,
                    BasketStatus: validationResult.BasketStatus,
                    EnableCheckout: validationResult.EnableCheckout
                };
            }
            return result;
        },
        'continueShopping': function () {
            continueShopping();
            return null;
        },
        'deleteCoupon': function (formgroup) {
        	var CouponBonusLineItems = cart.getBonusLineItemsByCouponCode(formgroup.getTriggeredAction().object.couponCode);
        	var lineItemsHavingTempPedicItems = getLineItemsReferingTempPedicItem(CouponBonusLineItems);
        	if(enableFSPProgram && dw.system.Site.getCurrent().getID() == 'Mattress-Firm') {
                resetFSPPriceAdjustments();
            }
            Transaction.wrap(function () {
                cart.removeCouponLineItem(formgroup.getTriggeredAction().object);
            });
            
            if(lineItemsHavingTempPedicItems.size() > 0) {        		
        		initializeTempPedicLineItemsBalance(lineItemsHavingTempPedicItems);
        		resetRemainingBalance(lineItemsHavingTempPedicItems);
        	}
            
            var currProdQualifiedBonusProducts=cart.getJsonByLineItems(CouponBonusLineItems);

            var result = {
                cart: cart,
                qualifiedBonusProducts : currProdQualifiedBonusProducts,
                action : 'deleteCoupon'
            };
            
            app.getView('Cart', result).render('checkout/cart/cart');
            return;
        },
        'deleteGiftCertificate': function (formgroup) {
            Transaction.wrap(function () {
                cart.removeGiftCertificateLineItem(formgroup.getTriggeredAction().object);
            });

            return {
                cart: cart
            };
        },
        'deleteProduct': function (formgroup) {
        	try{
        		var BonusProductLineItems = formgroup.getTriggeredAction().object.relatedBonusProductLineItems;
	        	customerSteps = customerSteps + "BonusProductLineItems Count: "+BonusProductLineItems.length+" | ";
	        	Transaction.wrap(function () {
	            	var triggeredProductLineItem = formgroup.getTriggeredAction().object;
	            	customerSteps = customerSteps + "Product Id : "+triggeredProductLineItem.product.ID+" | " + "Product Qty : "+triggeredProductLineItem.quantity.value+" | ";
	            	var ProductLineItems = new ArrayList();
	            	ProductLineItems.add(triggeredProductLineItem);
	            	var lineItemsHavingTempPedicItems = getLineItemsReferingTempPedicItem(ProductLineItems);
	            	customerSteps = customerSteps + "LineItemsHavingTempPedicItems Count : "+ lineItemsHavingTempPedicItems.length + " | ";
	            	if(triggeredProductLineItem.custom.isQuoteItem) {
	            		customerSteps = customerSteps + "QuoteItem : Yes | ";
	            		//removing triggered line item
	            		cart.removeProductLineItem(triggeredProductLineItem);
	            		
	            		//removing cart updates related to quotation
	            		cart.object.custom.mfiQuotationId = null;
						cart.object.custom.SalesPersonId = null;
						cart.object.custom.CustomerId = null;
						
						//adjusting existing product line items
						var allLineItems = cart.object.allProductLineItems;
						for (var i=0; i<allLineItems.length; i++) {
							var lineItem = allLineItems[i];
							if( lineItem instanceof dw.order.ProductLineItem && lineItem.custom.isQuoteItem) {
								var createdPriceAdjustment = lineItem.getPriceAdjustmentByPromotionID(lineItem.productID + "-quote");
								lineItem.removePriceAdjustment(createdPriceAdjustment);
								lineItem.custom.isQuoteItem = null;
								lineItem.custom.quotePrice = null;
								lineItem.custom.quotediscount = null;
							}
						}
						cart.calculate();
						customerSteps = customerSteps + "QuoteItem Removed : Yes | ";
	            	}
	            	else {
	            		customerSteps = customerSteps + "Regular Product : Yes | ";
						cart.removePersonaliCartRescue();
	            		customerSteps = customerSteps + "RemovePersonaliCartRescue : Yes | ";
						//Delete the Temp Pedic Bonus Items 
						if(hasTempPedicBonusLineItems(triggeredProductLineItem)){
							customerSteps = customerSteps + "HasTempPedicBonusLineItems : Yes | ";
							removeTempPedicBonusLineItemsIfExist(triggeredProductLineItem);
							customerSteps = customerSteps + "RemoveTempPedicBonusLineItemsIfExists : Yes | ";
						}
						cart.removeProductLineItem(formgroup.getTriggeredAction().object); 
						customerSteps = customerSteps + "removeProductLineItem : Yes | ";
	            	}
	            	if(lineItemsHavingTempPedicItems.size() > 0) {
	            		RefreshTempPedicLineItemBalance(lineItemsHavingTempPedicItems);
	            		customerSteps = customerSteps + "RefreshTempPedicLineItemBalance : Yes | ";
	            	}
	            });
	        	
	            
	            var currProdQualifiedBonusProducts=cart.getJsonByLineItems(BonusProductLineItems);

	            if(!empty(currProdQualifiedBonusProducts) && !empty(currProdQualifiedBonusProducts[0]) && !empty(currProdQualifiedBonusProducts[0].productid)){
	            	customerSteps = customerSteps + "currProdQualifiedBonusProducts Exist : "+currProdQualifiedBonusProducts[0].productid+ " | ";
	            }else{
	            	customerSteps = customerSteps + "currProdQualifiedBonusProducts Not Exist : |";
	            }
	            
	            var result = {
	                cart: cart,
	                qualifiedBonusProducts : currProdQualifiedBonusProducts,
	                action : 'deleteProduct'
	            };
	            
	            app.getView('Cart', result).render('checkout/cart/cart');
	            return;
        	}catch(e){
        	    var message = e.message;
        	    var stackFlow = e.stack;
        	    var isRemoveItemLogEnabled = 'enableRemoveItemLog' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.enableRemoveItemLog ? true : false;
        	    if(isRemoveItemLogEnabled){
        	    	var deleteProductLogInfo = Logger.getLogger('deleteProductLogInfo','Cart');
        	    	deleteProductLogInfo.info("Issue while delete product ==> error message: "+message + " ==> CustomerSteps: "+customerSteps+" ==> StackFlow: "+stackFlow);
        	    }
                var result = {
                    action : 'deleteProduct'
                };
                app.getView('Cart', result).render('checkout/cart/cart');
                return;
           }
        },       
        'editLineItem': function (formgroup) {
            var product, productOptionModel;
            product = app.getModel('Product').get(request.httpParameterMap.pid.stringValue).object;
            productOptionModel = product.updateOptionSelection(request.httpParameterMap);

            Transaction.wrap(function () {
            	cart.removePersonaliCartRescue();
                cart.updateLineItem(formgroup.getTriggeredAction().object, product, request.httpParameterMap.Quantity.doubleValue, productOptionModel);
                cart.calculate();
            });

            ISML.renderTemplate('checkout/cart/refreshcart');
            return null;
        },
        'login': function () {
            // TODO should not be processed here at all
            var success, result;
            success = app.getController('Login').Process();

            if (success) {
                response.redirect(URLUtils.https('COCustomer-Start'));
            } else if (!success) {
                result = {
                    cart: cart
                };
            }
            return result;
        },
        'logout': function () {
            var CustomerMgr = require('dw/customer/CustomerMgr');
            CustomerMgr.logoutCustomer();
            return {
                cart: cart
            };
        },
        'register': function () {
            app.getController('Account').StartRegister();
            Transaction.wrap(function () {
                cart.calculate();
            });

            return null;
        },
        'unregistered': function () {
            app.getController('COShipping').Start();
            return null;
        },
        'updateCart': function () {

            Transaction.wrap(function () {
                var shipmentItem, item;

                // remove zero quantity line items
                for (var i = 0; i < session.forms.cart.shipments.childCount; i++) {
                    shipmentItem = session.forms.cart.shipments[i];

                    for (var j = 0; j < shipmentItem.items.childCount; j++) {
                        item = shipmentItem.items[j];

                        if (item.quantity.value === 0) {
                            cart.removeProductLineItem(item.object);
                        }
                    }
                }

                session.forms.cart.shipments.accept();
                cart.removePersonaliCartRescue();
                cart.updatePersonaliProducts();
                cart.checkInStoreProducts();
            });
            if(enableFSPProgram && dw.system.Site.getCurrent().getID() == 'Mattress-Firm') {
                resetFSPPriceAdjustments();
            }            
            RefreshTempPedicLineItemBalance(cart.object.productLineItems);
            return {
                cart: cart
            };
        },
        'error': function () {
            return null;
        }
    });

    if (formResult) {
    	//Cart page title not working here so thats why moved to Cart-Show method
        /*cartAsset = app.getModel('Content').get('cart');

        pageMeta = require('~/cartridge/scripts/meta');
        pageMeta.update(cartAsset);*/
        
    	response.redirect(URLUtils.https('Cart-Show'));
		return;
	
   }
}

/**
 * Removes multiple products from the cart based on the product IDs received in httpParameterMap.
 * For now this is only used for removing Chicago DC products which were unavailable for delivery.
 * Redirects to Cart-Show after removing all specified products.
 */
function removeMultipleProductsFromCart() {
	var productIDsStr = request.httpParameterMap.chicagoProductIDs.stringValue;
	var productIDsArr = '';
	if (!empty(productIDsStr)) {
		productIDsArr = productIDsStr.split(',');
	}
	var cart = app.getModel('Cart').get();
    var Basket = cart ? cart.object : null;
    if (!empty(Basket)) {
    	var pliCollection = Basket.getProductLineItems();
    	for each (var pli in pliCollection) {
    		if (!empty(productIDsArr) && productIDsArr.indexOf(pli.product.manufacturerSKU) > -1) {
    			Transaction.wrap(function () {
                	if(pli.custom.isQuoteItem) {
                		//removing triggered line item
                		cart.removeProductLineItem(pli);
                		
                		//removing cart updates related to quotation
                		cart.object.custom.mfiQuotationId = null;
    					cart.object.custom.SalesPersonId = null;
    					cart.object.custom.CustomerId = null;
    					
    					//adjusting existing product line items
    					var allLineItems = cart.object.allProductLineItems;
    					for (var i=0; i<allLineItems.length; i++) {
    						var lineItem = allLineItems[i];
    						if( lineItem instanceof dw.order.ProductLineItem && lineItem.custom.isQuoteItem) {
    							var createdPriceAdjustment = lineItem.getPriceAdjustmentByPromotionID(lineItem.productID + "-quote");
    							lineItem.removePriceAdjustment(createdPriceAdjustment);
    							lineItem.custom.isQuoteItem = null;
    							lineItem.custom.quotePrice = null;
    							lineItem.custom.quotediscount = null;
    						}
    					}
    					cart.calculate();
                	}
                	else {
    					cart.removePersonaliCartRescue();
                		cart.removeProductLineItem(pli);
                	}
                });
    		}
    	}
    	
    	response.redirect(URLUtils.https('Cart-Show'));
    }
}
/**
 * It will return the Option product Model of line item
 * @param lineItem
 * @param productModel
 * @returns optionModel
 */
function getPreviousOptionProductModel(lineItem,productModel){	
	var optionModel = productModel.object.getOptionModel();
	if(lineItem.optionProductLineItems.size() > 0){
		var selectedOptionProductLineItem = lineItem.optionProductLineItems.toArray()[0];
		var optionID    = selectedOptionProductLineItem.optionID;
    	var optionValueID = selectedOptionProductLineItem.optionValueID;

     	if (optionValueID) {
        	var option = optionModel.getOption(optionID);
        	if (option && optionValueID) {
        		var optionValue = optionModel.getOptionValue(option, optionValueID);
            	if (optionValue) {
            		optionModel.setSelectedOptionValue(option, optionValue);
            	}
        	}
     	}
	}
    return optionModel;
}

/**
 * This method loop through all the product line items and figure out which lineitem needs update
 * and also check which line item requires removal from cart
 * @return It returns the UUID after updating the lineitem  
 */
function updateExistingLineItem(cart, recentlyAddedLineItem, params, product, productOptionModel, productModel) {
	var count = 0;
	var updatedQuantity = parseInt(params.Quantity.value);
	var lineItemToUpdate;
	var lineItemToUpdateUUID;
	var lineItemToDeleteOrUpdate;
	
	for each(var lineItem in cart.getProductLineItems()) {
		  if(lineItem.optionProductLineItems.size() > 0){
          	var currentOptionProductLineItem = lineItem.optionProductLineItems.toArray()[0];
				if(currentOptionProductLineItem.optionValueID == params.optionValue.stringValue && lineItem.productID == params.pid.stringValue){						
					updatedQuantity = updatedQuantity + lineItem.quantity.value; 	
					lineItemToUpdate = lineItem;
					count++;
				} else if(lineItem.productID == params.pid.stringValue && currentOptionProductLineItem.optionValueID == params.previousOptionValue.stringValue) {
					lineItemToDeleteOrUpdate = lineItem;
				}
         	}
	}
	
	if(count > 0) {
		 Transaction.wrap(function () {
			cart.updateLineItem(lineItemToUpdate, product, updatedQuantity, productOptionModel);
       });
	   lineItemToUpdateUUID = lineItemToUpdate.UUID;
	}
	if(!empty(lineItemToDeleteOrUpdate)) {
		Transaction.wrap(function () {
			var newUpdatedQuantity = lineItemToDeleteOrUpdate.quantity.value - parseInt(params.Quantity.value);
			if(newUpdatedQuantity == 0 && count > 0){
			    cart.removeProductLineItem(lineItemToDeleteOrUpdate);
			} else if (newUpdatedQuantity >= 0) {
				var previousProductOptionModel = getPreviousOptionProductModel(lineItemToDeleteOrUpdate, productModel);
                var updatedOptionProductSelection = newUpdatedQuantity == 0 ? productOptionModel : previousProductOptionModel;
                var finalUpdatedQuantity = newUpdatedQuantity == 0 ? parseInt(params.Quantity.value) : newUpdatedQuantity;

                cart.updateLineItem(lineItemToDeleteOrUpdate, product, finalUpdatedQuantity, updatedOptionProductSelection);
			}
			if(newUpdatedQuantity != 0 && count == 0) {
             	cart.addProductItem(product, parseInt(params.Quantity.value), params.cgid.value, productOptionModel);
             	lineItemToUpdateUUID = true;
         	}
      });
	}

	return lineItemToUpdateUUID;
}
    		
/**
 * Adds or replaces a product in the cart, gift registry, or wishlist.
 * If the function is being called as a gift registry update, calls the
 * {@link module:controllers/GiftRegistry~replaceProductListItem|GiftRegistry controller ReplaceProductListItem function}.
 * The httpParameterMap source and cartAction parameters indicate how the function is called.
 * If the function is being called as a wishlist update, calls the
 * {@link module:controllers/Wishlist~replaceProductListItem|Wishlist controller ReplaceProductListItem function}.
 * If the product line item for the product to add has a:
 * - __uuid__ - gets a ProductModel that wraps the product and determines the product quantity and options.
 * In a transaction, calls the {@link module:models/CartModel~CartModel/updateLineItem|CartModel updateLineItem} function to replace the current product in the line
 * item with the new product.
 * - __plid__ - gets the product list and adds a product list item.
 * Otherwise, adds the product and checks if a new discount line item is triggered.
 * Renders the checkout/cart/refreshcart template if the httpParameterMap format parameter is set to ajax,
 * otherwise renders the checkout/cart/cart template.
 */
function addProduct() {
    var cart = app.getModel('Cart').goc();
    var params = request.httpParameterMap;
    var format = empty(params.format.stringValue) ? '' : params.format.stringValue.toLowerCase();
    var addToCartRedesign = empty(params.AddtoCart_Redesign.stringValue) ? '' : params.AddtoCart_Redesign.stringValue.toLowerCase();
    var isATCPanelEnabled = 'enableATCPanel' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.enableATCPanel ? true : false;
    var Product = app.getModel('Product');
    var productOptionModel;
    var product;
    var template = 'checkout/cart/minicart';
    var newBonusDiscountLineItem;
    var lastItemAdded = false;
    var lastItemAddedQty = false;
	var ATCUtils = require('app_mattressfirm_storefront/cartridge/scripts/cart/ATCUtils').ATCUtils;
    var currentSiteID = dw.system.Site.getCurrent().getID();
    //preserve the last index of Bonus Item
	var bonusProductStartIndex=0;
    if(cart.object.bonusLineItems.length>0){
    	bonusProductStartIndex = cart.object.bonusLineItems.length;
    }
    
    var splitLineItem = false;    
    if (params.source && params.source.stringValue === 'giftregistry' && params.cartAction && params.cartAction.stringValue === 'update') {
        app.getController('GiftRegistry').ReplaceProductListItem();
        return;
    }

    if (params.source && params.source.stringValue === 'wishlist' && params.cartAction && params.cartAction.stringValue === 'update') {
        app.getController('Wishlist').ReplaceProductListItem();
        return;
    }

    // Updates a product line item.
    if (params.uuid.stringValue) {
        var lineItem = cart.getProductLineItemByUUID(params.uuid.stringValue);
        var currentLineItemUUID = params.uuid.stringValue;
        if (lineItem) {
            var productModel = Product.get(request.httpParameterMap.pid.stringValue);
            product = productModel.object;
            var quantity = parseInt(params.Quantity.value);                        
            productOptionModel = productModel.updateOptionSelection(request.httpParameterMap);
            
            if(isATCPanelEnabled && addToCartRedesign === 'v1' && currentSiteID == 'Mattress-Firm'){
	        	var updateExistingLineItemUUID = updateExistingLineItem(cart, lineItem, params, product, productOptionModel, productModel);
	        	
	        	if(updateExistingLineItemUUID == true){
	        		splitLineItem = true;
	        	} else if(!empty(updateExistingLineItemUUID)) {
	        		currentLineItemUUID = updateExistingLineItemUUID;
	        	}
	    	} else {
	    		Transaction.wrap(function () {
	                cart.updateLineItem(lineItem, product, quantity, productOptionModel);
	            });
	    	}
	            if (params.netotiate_offer_id.stringValue && !empty(params.netotiate_offer_id.stringValue)) {
	        		var personaliResponse = cart.getPersonaliProductResponse(params.netotiate_offer_id.stringValue);
	        		if (personaliResponse !== false) {
	        			var adjustedPrice : Number = personaliResponse["adjusted_price"] / 100,
		    				originalPrice : Number = personaliResponse["original_price"] / 100,
		    				offerId : String = params.netotiate_offer_id.stringValue;
		    			if (adjustedPrice && originalPrice && adjustedPrice < originalPrice) {
		    				Transaction.wrap(function () {
		    					cart.removePersonaliCartRescue();
		        				personaliPriceAdjustment(lastItemAdded, offerId, params.Quantity.doubleValue, adjustedPrice, originalPrice);
		    	            });
		    			}
	        		}
	        		
	        	} else if (lineItem.custom['personaliOfferID'] || lineItem.custom['personaliDiscount']) {
	        		Transaction.wrap(function () {
	        			delete lineItem.custom['personaliOfferID'];
	            		delete lineItem.custom['personaliDiscount'];
	        		});
	        	}
	
	            // If this product is to be picked up in a store -- update store id
	            if (!empty(params.storeId.stringValue)) {
	                // Determine if products in cart are eligible for in store pickup
	                var allProductsAvailableForInStorePickup = product.custom.availableForInStorePickup;
	                if (allProductsAvailableForInStorePickup) {
	                    var plisIterator = cart.getAllProductLineItems().iterator();
	                    while (plisIterator.hasNext()) {
	                        var pli = plisIterator.next();
	                        if (!(pli.product.custom.availableForInStorePickup)) {
	                            allProductsAvailableForInStorePickup = false;
	                        }
	                    }
	                    // all products are eligible
	                    if (allProductsAvailableForInStorePickup) {
	                        Transaction.wrap(function () {
	                            var plisIterator = cart.getAllProductLineItems().iterator();
	                            while (plisIterator.hasNext()) {
	                                var pli = plisIterator.next();
	                                pli.custom.storePickupStoreID = params.storeId.stringValue;
	                            }
	                         });
	                    }
	                }
	            }
	            if(isATCPanelEnabled && addToCartRedesign === 'v1' && currentSiteID == 'Mattress-Firm'){
	            	if(splitLineItem){
	            		var lastItemAddedCollection = cart.getProductLineItems(product.ID);
	            		lastItemAdded = lastItemAddedCollection[lastItemAddedCollection.size() -1];
	            	}
	            	else{
	            		lastItemAdded = cart.getProductLineItemByUUID(currentLineItemUUID);
	            	}
	            	
	                lastItemAddedQty = params.Quantity.value;
	            }
	            
	            if (format === 'ajax') {
	                template = 'checkout/cart/refreshcart';
	            }
        } else {
            app.getView('Cart', {Basket: cart}).render('checkout/cart/cart');
        }
    } else if (params.plid.stringValue) {
        // Adds a product to a product list.
        var productList = ProductListMgr.getProductList(params.plid.stringValue);
        cart.addProductListItem(productList && productList.getItem(params.itemid.stringValue), params.Quantity.doubleValue, params.cgid.value);
    } else {
    	// This session variable is used to print the count of newly added item in cart
    	if(empty(session.custom.recentlyAddedItemCount)) {
    		session.custom.recentlyAddedItemCount = parseInt(0);
    	}
    	
        // Adds a product.
        product = Product.get(params.pid.stringValue);
        var previousBonusDiscountLineItems = cart.getBonusDiscountLineItems();

        if (product.object.isProductSet()) {
            var childPids = params.childPids.stringValue.split(',');
            var childQtys = params.childQtys.stringValue.split(',');
            var counter = 0;

            for (var i = 0; i < childPids.length; i++) {
                var childProduct = Product.get(childPids[i]);

                if (childProduct.object && !childProduct.isProductSet()) {
                    var childProductOptionModel = childProduct.updateOptionSelection(request.httpParameterMap);
                    cart.addProductItem(childProduct.object, parseInt(childQtys[counter], 10), params.cgid.value, childProductOptionModel);
                }
                counter++;
                session.custom.recentlyAddedItemCount = parseInt(session.custom.recentlyAddedItemCount) + 1;
            }
        } else {
            var productId = product.object.ID;
        	var availabilityOptionId = params.optionValue.stringValue;	
        	var customerZip = session.custom.customerZip;
        	var availabilityQty = params.Quantity.intValue; 
            
            //MAT-356            
            var lineItemsOfPid = cart.getProductLineItemByID(params.pid.stringValue);
            var productFoundInCart = false;
            if (lineItemsOfPid.length > 0) {
            	var productModel = Product.get(params.pid.stringValue);
            	var lineItemQuantity = params.Quantity.value;
            	var productToAdd = productModel.object;
	        	productOptionModel = productModel.updateOptionSelection(request.httpParameterMap);
	        	for each(var lineItem in lineItemsOfPid) {
        			var productOptions = productOptionModel.getOptions();
	        		if(productOptions.length >= 0){
	        			var lineItemOptions = lineItem.getOptionProductLineItems();
	        			// productOptions are of that product which is going into cart which is productToAdd
	        			//lineItemOptions, extracting all lineItem iterator's all option products into lineItemOptions
	        			productFoundInCart = compareProductOptionProducts_With_ProductLineItemOptionProducts (productOptions, lineItemOptions, productOptionModel)
		        		if (productFoundInCart) {
		        			lineItemQuantity = parseInt(params.Quantity.value) + lineItem.quantity;
		        			availabilityQty = lineItemQuantity;
		        			session.custom.recentlyAddedItemCount = session.custom.recentlyAddedItemCount + parseInt(params.Quantity.value); 
		        	    	
				            Transaction.wrap(function () {
				            cart.updateLineItem(lineItem, productToAdd, lineItemQuantity, productOptionModel);
				            });
				            //Quantity is updated we need to recalculate the Temp Pedic Line item Balance
				            if(hasTempPedicBonusLineItems(lineItem)) {
				            	var ProductLineItems = new ArrayList();
				            	ProductLineItems.add(lineItem);
				            	RefreshTempPedicLineItemBalance(ProductLineItems);
				            }
				            break;
		        		}		        			
	        		}
	        	}
	        }
            
            if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpAvailabilityMessage') && !empty(session.custom.ATPAvailability) && !empty(session.custom.ATPAvailability[productId + availabilityOptionId + customerZip + availabilityQty])) {
            	if (!session.custom.ATPCartAvailability) {
            		session.custom.ATPCartAvailability = new Object(); 
            	} 
            	session.custom.ATPCartAvailability[productId + availabilityOptionId + customerZip + availabilityQty] = new Object();
            	session.custom.ATPCartAvailability[productId + availabilityOptionId + customerZip + availabilityQty]['availabilityDate'] = session.custom.ATPAvailability[productId + availabilityOptionId + customerZip + availabilityQty]['availabilityDate'];
            	session.custom.ATPCartAvailability[productId + availabilityOptionId + customerZip + availabilityQty]['dateDifferenceString'] = session.custom.ATPAvailability[productId + availabilityOptionId + customerZip + availabilityQty]['dateDifferenceString'];
            	session.custom.ATPCartAvailability[productId + availabilityOptionId + customerZip + availabilityQty]['availabilityClass'] = session.custom.ATPAvailability[productId + availabilityOptionId + customerZip + availabilityQty]['availabilityClass'];
            	session.custom.ATPCartAvailability[productId + availabilityOptionId + customerZip + availabilityQty]['qty'] = session.custom.ATPAvailability[productId + availabilityOptionId + customerZip + availabilityQty]['qty'];
    		}
            
            if (!productFoundInCart){
	        	productOptionModel = product.updateOptionSelection(request.httpParameterMap);
	        	cart.addProductItem(product.object, params.Quantity.doubleValue, params.cgid.value, productOptionModel);
    	    	session.custom.recentlyAddedItemCount = session.custom.recentlyAddedItemCount + parseInt(params.Quantity.value);
    	    	
    	    	if(enableFSPProgram && dw.system.Site.getCurrent().getID() == 'Mattress-Firm') {
    	    		var fspLineItem = cart.getProductLineItems(product.object.ID);
    			    if(fspLineItem.length > 0){
    			    	Transaction.wrap(function () {
        			    	fspLineItem[0].custom.optFirstSelectionProgram = false;
        	 		    });
    			    }
    	    	}
            }
            if (params.source && params.source.stringValue !== 'recommendation') {
                var lastItemAddedCollection = cart.getProductLineItems(product.object.ID);
                if (availabilityOptionId) {
	                for each (var li in lastItemAddedCollection) {
	                	if (li.optionProductLineItems.size() > 0) {
		                	if (li.optionProductLineItems[0].optionValueID == availabilityOptionId) {
		                		lastItemAdded = li;
		                		break;
		                	} 
	                	}
	                }
                } 
                if (!lastItemAdded) {
	                lastItemAdded = lastItemAddedCollection[lastItemAddedCollection.size() -1];
                }
                lastItemAddedQty = params.Quantity.value;
                if (params.netotiate_offer_id.stringValue && !empty(params.netotiate_offer_id.stringValue)) {
            		var personaliResponse = cart.getPersonaliProductResponse(params.netotiate_offer_id.stringValue);
            		if (personaliResponse !== false) {
            			var adjustedPrice : Number = personaliResponse["adjusted_price"] / 100,
            				originalPrice : Number = personaliResponse["original_price"] / 100,
            				offerId : String = params.netotiate_offer_id.stringValue;
            			if (adjustedPrice && originalPrice && adjustedPrice < originalPrice) {
            				Transaction.wrap(function () {
    	        				personaliPriceAdjustment(lastItemAdded, offerId, params.Quantity.doubleValue, adjustedPrice, originalPrice);
    	    	            });
            			}
            		}
            		
            	}
            }
            // If this product is to be picked up in a store -- update store id
            if (!empty(params.storeId.stringValue)) {
                // Determine if products in cart are eligible for in store pickup
                var allProductsAvailableForInStorePickup = product.object.custom.availableForInStorePickup;
                var plisIterator = cart.getAllProductLineItems().iterator();
                while (plisIterator.hasNext()) {
                    var pli = plisIterator.next();
                    if (!(pli.product.custom.availableForInStorePickup)) {
                        allProductsAvailableForInStorePickup = false;
                    }
                }
                // all products are eligible
                if (allProductsAvailableForInStorePickup) {
                    Transaction.wrap(function () {
                        var plisIterator = cart.getAllProductLineItems().iterator();
                        while (plisIterator.hasNext()) {
                            var pli = plisIterator.next();
                            pli.custom.storePickupStoreID = params.storeId.stringValue;
                        }
                     });
                }
            }
            if (!empty(session.custom.customerZone)) {
//            	lastItemAdded.custom.zoneCodeAddedWith = session.custom.customerZone;
            }
        }

        // When adding a new product to the cart, check to see if it has triggered a new bonus discount line item.
        newBonusDiscountLineItem = cart.getNewBonusDiscountLineItem(previousBonusDiscountLineItems);
    }
    var newlyAddedBonusItems = cart.object.bonusLineItems.length > 0 ? cart.object.bonusLineItems.toArray(bonusProductStartIndex, cart.object.bonusLineItems.length) : [];
    var currProdQualifiedBonusProducts = cart.getJsonByLineItems(newlyAddedBonusItems);
    if (format === 'ajax') {
    	if(empty(session.custom.recentlyAddedItemCount)) {
    		session.custom.recentlyAddedItemCount = parseInt(0);
    		session.custom.recentlyAddedItemCount = session.custom.recentlyAddedItemCount + parseInt(params.Quantity.value);
        }
    	//MAT-1568 - Render ATC side panel if site preference is on and AB test enabled
    	if(isATCPanelEnabled && addToCartRedesign === 'v1' && currentSiteID == 'Mattress-Firm'){
    		var optionProductStatus = ATCUtils.hasOptionProduct(lastItemAdded);
    		var isLastAddedMattress = !empty(lastItemAdded.product.custom.finish) && lastItemAdded.product.custom.finish == 'Mattress' ? true : false; 
    		var atcPanelState = ATCUtils.setATCPanelState(cart.object.productLineItems);
    		var cartData = ATCUtils.getCartValueAndMessage(cart);
    		var lineItemJson = cart.getJsonByLineItem(lastItemAdded);
        	template='checkout/cart/cartATC';        	
        	app.getView('Cart', {
	            cart: cart,	            
	            LastItem: lastItemAdded,
	            Product: lastItemAdded.product,
	            OptionValue: params.optionValue.stringValue,
	            LastItemAddedQty: lastItemAddedQty,
	            OptionProductStatus: optionProductStatus,
	            AtcPanelState: atcPanelState,
	            BonusDiscountLineItem: newBonusDiscountLineItem,
	            CartData: cartData,
	            IsLastAddedMattress: isLastAddedMattress,
	            qualifiedBonusProducts : currProdQualifiedBonusProducts,
	            lineItemJson : lineItemJson
	        }).render(template);
    	}
    	else {
    		app.getView('Cart', {
	            cart: cart,
	            BonusDiscountLineItem: newBonusDiscountLineItem,
	            LastItem: lastItemAdded,
	            LastItemAddedQty: lastItemAddedQty,
	            qualifiedBonusProducts: currProdQualifiedBonusProducts
	        }).render(template);
    	}
    		
    } else {
        response.redirect(URLUtils.url('Cart-Show'));
    }
}

function compareProductOptionProducts_With_ProductLineItemOptionProducts (productOptions, lineItemOptions, productOptionModel) {
	var optionProductFound = false;
	if (lineItemOptions.length == 0) {
		optionProductFound = true;
	} else {
		for each(var i = 0 ; i < productOptions.length ; i++) {
			optionProductFound = false;
			var selectedOption = productOptionModel.getSelectedOptionValue(productOptions[i]);
			for each(var j = 0 ; j < lineItemOptions.length ; j++) {
				if(lineItemOptions[j].optionValueID == selectedOption.ID) {
					optionProductFound = true;
					break;
				}
			}
			if (!optionProductFound){
				return optionProductFound;
			}
		}
	}
	return optionProductFound;
}

/**
 * Adds multiple products to cart via a JSON string supplied in the parameters.
 * parameter name should be cartPayload and JSON format is as follows:
 * [{"pid":"mfiV000088629","qty":1},{"pid":"mfiV000088630","qty":4},{"pid":"mfiV000088631","qty":5}]
 */
function addMultipleProducts() {
    var Product = app.getModel('Product');
    var cart = app.getModel('Cart').goc();
    var cartPayload = request.httpParameterMap.cartPayload;
    try {
        var jsonPayload = JSON.parse(cartPayload);
        for (var i = 0; i < jsonPayload.length; i++){
            var jsonProduct = jsonPayload[i];
            //var product = Product.get(params.pid.stringValue);
            try {
                var product = Product.get(jsonProduct.pid);
                var productOptionModel = product.updateOptionSelection(request.httpParameterMap);
                cart.addProductItem(product.object, jsonProduct.qty, (!empty(product.object.primaryCategory) ? product.object.primaryCategory.ID : null), productOptionModel);
            } catch (e) {
                Logger.error("Error processing Cart-AddMultipleProducts: Invalid product ID: " + jsonProduct.pid);
            }
        }
    } catch (e) {
        Logger.error("Error processing Cart-AddMultipleProducts: " + e.message);
        show();
        return;
    }
    show();

}
/**
 * Displays the current items in the cart in the minicart panel.
 */
function miniCart() {

    var cart = app.getModel('Cart').get();
    app.getView({
        Basket: cart ? cart.object : null
    }).render('checkout/cart/minicart');

}

/**
 * Adds the product with the given ID to the wish list.
 *
 * Gets a ProductModel that wraps the product in the httpParameterMap. Uses
 * {@link module:models/ProductModel~ProductModel/updateOptionSelection|ProductModel updateOptionSelection}
 * to get the product options selected for the product.
 * Gets a ProductListModel and adds the product to the product list. Renders the checkout/cart/cart template.
 */
function addToWishlist() {
    var productID, product, productOptionModel, productList, Product;
    Product = app.getModel('Product');

    productID = request.httpParameterMap.pid.stringValue;
    product = Product.get(productID);
    productOptionModel = product.updateOptionSelection(request.httpParameterMap);

    productList = app.getModel('ProductList').get();
    productList.addProduct(product.object, request.httpParameterMap.Quantity.doubleValue, productOptionModel);

    app.getView('Cart', {
        cart: app.getModel('Cart').get(),
        ProductAddedToWishlist: productID
    }).render('checkout/cart/cart');

}

/**
 * Adds a bonus product to the cart.
 *
 * Parses the httpParameterMap and adds the bonus products in it to an array.
 *
 * Gets the bonus discount line item. In a transaction, removes the bonus discount line item. For each bonus product in the array,
 * gets the product based on the product ID and adds the product as a bonus product to the cart.
 *
 * If the product is a bundle, updates the product option selections for each child product, finds the line item,
 * and replaces it with the current child product and selections.
 *
 * If the product and line item can be retrieved, recalculates the cart, commits the transaction, and renders a JSON object indicating success.
 * If the transaction fails, rolls back the transaction and renders a JSON object indicating failure.
 */
function addBonusProductJson() {
    var h, i, j, cart, data, productsJSON, bonusDiscountLineItem, product, lineItem, childPids, childProduct, ScriptResult, foundLineItem, Product;
    cart = app.getModel('Cart').goc();
    Product = app.getModel('Product');

    // parse bonus product JSON
    data = JSON.parse(request.httpParameterMap.getRequestBodyAsString());
    productsJSON = new ArrayList();

    for (h = 0; h < data.bonusproducts.length; h += 1) {
        // add bonus product at index zero (front of the array) each time
        productsJSON.addAt(0, data.bonusproducts[h].product);
    }

    bonusDiscountLineItem = cart.getBonusDiscountLineItemByUUID(request.httpParameterMap.bonusDiscountLineItemUUID.stringValue);

    Transaction.begin();
    cart.removeBonusDiscountLineItemProducts(bonusDiscountLineItem);

    for (i = 0; i < productsJSON.length; i += 1) {

        product = Product.get(productsJSON[i].pid).object;
        var quantity = Number(productsJSON[i].qty);
        lineItem = cart.addBonusProduct(bonusDiscountLineItem, product, new ArrayList(productsJSON[i].options), quantity);

        if (lineItem && product) {
            if (product.isBundle()) {

                childPids = productsJSON[i].childPids.split(',');

                for (j = 0; j < childPids.length; j += 1) {
                    childProduct = Product.get(childPids[j]).object;

                    if (childProduct) {

                        // TODO: CommonJSify cart/UpdateProductOptionSelections.ds and import here

                        var UpdateProductOptionSelections = require('app_storefront_core/cartridge/scripts/cart/UpdateProductOptionSelections');
                        ScriptResult = UpdateProductOptionSelections.update({
                            SelectedOptions:  new ArrayList(productsJSON[i].options),
                            Product: childProduct
                        });

                        foundLineItem = cart.getBundledProductLineItemByPID(lineItem.getBundledProductLineItems(),
                            (childProduct.isVariant() ? childProduct.masterProduct.ID : childProduct.ID));

                        if (foundLineItem) {
                            foundLineItem.replaceProduct(childProduct);
                        }
                    }
                }
            }
        } else {
            Transaction.rollback();

            let r = require('~/cartridge/scripts/util/Response');
            r.renderJSON({
                success: false
            });
            return;
        }
    }

    cart.calculate();
    Transaction.commit();

    let r = require('~/cartridge/scripts/util/Response');
    r.renderJSON({
        success: true
    });
}


function addTempPedicBonusProductJson() {
    var h, i, j, cart, data, productsJSON, bonusDiscountLineItem, product, lineItem,blineItem, childPids, childProduct, ScriptResult, foundLineItem, Product;
    cart = app.getModel('Cart').goc();
    Product = app.getModel('Product');

    bonusDiscountLineItem = cart.getBonusDiscountLineItemByUUID(request.httpParameterMap.bonusDiscountLineItemUUID.stringValue);
    // parse bonus product JSON
    data = JSON.parse(request.httpParameterMap.getRequestBodyAsString());   
    productsJSON = new ArrayList();
    var addedQuantity = 0;     
    
    var variantID = data.bonusproducts.length > 0 ? data.bonusproducts[0].product.pid: 0;
    
    for (h = 0; h < data.bonusproducts.length; h += 1) {
        // add bonus product at index zero (front of the array) each time
    	productsJSON.addAt(0, data.bonusproducts[h].product);
    }

    try {
	    //cart.removeBonusDiscountLineItemProducts(bonusDiscountLineItem);
	
	    for (i = 0; i < productsJSON.length; i += 1) {
	        for(index=0; index < productsJSON[i].qty;  index += 1){
		        Transaction.begin();
		        product = Product.get(productsJSON[i].pid).object;
		        //var quantity = Number(productsJSON[i].qty);
		        var quantity = 1;
		       
		        blineItem = cart.addBonusProduct(bonusDiscountLineItem, product, new ArrayList(productsJSON[i].options), quantity);
		        
		        //set the bonus item custom attributes for use in calculation
		        if(request.httpParameterMap.qualifiedlineItemUUID.submitted) {
		        	blineItem.custom.parentLineItem = request.httpParameterMap.qualifiedlineItemUUID;       	
		        }       
		
		        if (blineItem && product) {
		            if (product.isBundle()) {
		
		                childPids = productsJSON[i].childPids.split(',');
		
		                for (j = 0; j < childPids.length; j += 1) {
		                    childProduct = Product.get(childPids[j]).object;
		
		                    if (childProduct) {
		
		                        // TODO: CommonJSify cart/UpdateProductOptionSelections.ds and import here
		
		                        var UpdateProductOptionSelections = require('app_storefront_core/cartridge/scripts/cart/UpdateProductOptionSelections');
		                        ScriptResult = UpdateProductOptionSelections.update({
		                            SelectedOptions:  new ArrayList(productsJSON[i].options),
		                            Product: childProduct
		                        });
		
		                        foundLineItem = cart.getBundledProductLineItemByPID(lineItem.getBundledProductLineItems(),
		                            (childProduct.isVariant() ? childProduct.masterProduct.ID : childProduct.ID));
		
		                        if (foundLineItem) {
		                            foundLineItem.replaceProduct(childProduct);
		                        }
		                    }
		                }
		            }
		        } else {
		            Transaction.rollback();
		
		            let r = require('~/cartridge/scripts/util/Response');
		            r.renderJSON({
		                success: false
		            });
		            return;
		        }
		    	cart.calculate();
		    	Transaction.commit();
		    	addedQuantity =addedQuantity+1;
		    	if(request.httpParameterMap.qualifiedlineItemUUID.submitted) {
					lineItem = cart.getProductLineItemByUUID(request.httpParameterMap.qualifiedlineItemUUID.stringValue);
		    		var calculatedprice = 0;
		    		var adjustedBalance = lineItem.custom.remainingBalance - blineItem.adjustedPrice.value; 
		    		if(adjustedBalance >= 0) {
		    			calculatedprice = 0;
		    		}
		    		else {
		    			if(lineItem.custom.remainingBalance>0){
		    				calculatedprice = adjustedBalance * -1;
		    			}
		    			else {
		    				calculatedprice = blineItem.adjustedPrice.value;
		    			}	
		    		}
		    		if (blineItem) {
				    	Transaction.wrap(function () {
				    		blineItem.custom.isTempPedicBonusItem = true; 
				    		blineItem.custom.orgPrice = blineItem.adjustedPrice.value;
				    		blineItem.custom.calculatedPrice = calculatedprice;	  				
				    	});
		    		}
		    		calculateTempPedicLineItemBalance(lineItem);
				}
			}
	    }	
		
	    // format the Remaining Balance in site currency
	    var currencyCode = dw.system.Site.getCurrent().currencyCode;
	    var remainingBalance =dw.util.StringUtils.formatMoney(dw.value.Money(lineItem.custom.remainingBalance, currencyCode));
	
	    let r = require('~/cartridge/scripts/util/Response');
	    r.renderJSON({
	        success: true,
	        productID: variantID,
	        remainingBalance: lineItem.custom.remainingBalance,
	        formatedRemainingBalance: remainingBalance,
	        addQuantity: addedQuantity  
	    });
    } catch (e) {
    	var errormsg = e.message;
		Logger.error("Error While adding Temp Pedic Bonus Products" + errormsg);
    }
}

/***
 * Calculate Temp Pedic Remaining/consumed Balance
 * @param lineItem
 * @returns
 */
function calculateTempPedicLineItemBalance(lineItem) {
	try {
			var consumedBalance = 0;		
			var CashBackBonusPromoValues = dw.system.Site.getCurrent().getCustomPreferenceValue('CashBackBonusPromotions');			
			var BonusAmount = ProductUtils.getCashBackBonusAmount(lineItem)*lineItem.quantityValue;			
			var cart = app.getModel('Cart').get();
			for each(var bli in cart.object.bonusLineItems) {
				if(!empty(bli.custom.parentLineItem) && bli.custom.parentLineItem === lineItem.getUUID() 
						&& !empty(bli.custom.isTempPedicBonusItem) && bli.custom.isTempPedicBonusItem && !empty(bli.custom.orgPrice)) {
						consumedBalance = consumedBalance + bli.custom.orgPrice;
				}
			}
			
			Transaction.wrap(function () {
				lineItem.custom.remainingBalance = BonusAmount - consumedBalance;
				lineItem.custom.consumedBalance = consumedBalance;
    		});  		
			
        } catch (e) {
        		var errormsg = e.message;
        		Logger.error("Error calculating Balance" + errormsg);
        }
}

/***
 * Calculate the Remaining/Consumed Balance of passed Items 
 * @param pLineItems
 * @returns
 */
function RefreshTempPedicLineItemBalance(pLineItems) {
	cart = app.getModel('Cart').goc();
	for each(var lineItem in pLineItems) {		
		calculateTempPedicLineItemBalance(lineItem);
		splitBalanceinTempPedicBonusItems(lineItem);
	}
	Transaction.wrap(function () {
  		cart.calculate();
   	});
}
/***
 * initalize the Remaining/consumed Balance of pass line items
 * @param pLineItems
 * @returns
 */
function initializeTempPedicLineItemsBalance(pLineItems) {
	for each(var lineItem in pLineItems) {
		var cashBackAmount = ProductUtils.getCashBackBonusAmount(lineItem);
		Transaction.wrap(function () {
			lineItem.custom.remainingBalance = cashBackAmount*lineItem.quantityValue;
			lineItem.custom.consumedBalance = 0;
		});
	}
}
/***
 * Return parent line items of Temp Pedic Bonus Products
 * @param bLineItems
 * @returns
 */
function getLineItemsReferingTempPedicItem(bLineItems) {
	var lineItems = new ArrayList();
	var cart = app.getModel('Cart').get();
	for each(var bli in bLineItems) {
		if(!empty(bli.custom.isTempPedicBonusItem) && bli.custom.isTempPedicBonusItem && bli.custom.parentLineItem != null) {			
			var lineItem = cart.getProductLineItemByUUID(bli.custom.parentLineItem);
			if (!empty(lineItem) && !lineItems.contains(lineItem)) {
				lineItems.add(lineItem);
			}
		} 	
	}
	return lineItems;
}

/***
 * Return Related Temp Pedic Bonus Line items
 * @param bLineItems
 * @returns
 */
function getRelatedTempPedicBonusLineItems(lineItem) {
	var blineItems = new ArrayList();
	var cart = app.getModel('Cart').get();	
	for each(var bli in cart.object.bonusLineItems) {
		if(!empty(bli.custom.isTempPedicBonusItem) && bli.custom.isTempPedicBonusItem && !empty(bli.custom.parentLineItem) && bli.custom.parentLineItem === lineItem.getUUID()) {			
				if (!blineItems.contains(bli)) {
					blineItems.add(bli);
				}
		}
	}
	return blineItems;
}
/***
 * Remove Bonus line items related to Temp Pedic line item
 * @param lineItem
 * @returns
 */
function removeTempPedicBonusLineItemsIfExist(lineItem) {
	var cart = app.getModel('Cart').get();	
	var tempPedicBonusLineItems=getRelatedTempPedicBonusLineItems(lineItem);
	for each(var bli in tempPedicBonusLineItems) {
		cart.removeProductLineItem(bli);
	}
}
/***
 * Determine if the line item has Temp Pedic Bonus line Items
 * @param lineItem
 * @returns
 */
function hasTempPedicBonusLineItems(lineItem) {
	var tempPedicBonusLineItems=getRelatedTempPedicBonusLineItems(lineItem);
	if(tempPedicBonusLineItems.size() >0) {
		return true;
	}
	else {
		return false;
	}
}
/***
 * Split the Cash Back Bonus amount in Temp Pedic Bonus items 
 * @param lineItem
 * @returns
 */
function splitBalanceinTempPedicBonusItems(lineItem) {
	var BonusAmount = ProductUtils.getCashBackBonusAmount(lineItem)*lineItem.quantityValue;
	var tempPedicBonusLineItems=getRelatedTempPedicBonusLineItems(lineItem);
	for each(var bli in tempPedicBonusLineItems) {		
		var adjustedBalance = BonusAmount - bli.custom.orgPrice;
		if(adjustedBalance >= 0) {
    		calculatedprice = 0;    			
    	}
    	else {
    		if(BonusAmount>0){
    			calculatedprice = adjustedBalance * -1;
    		}
    		else {
    				calculatedprice = bli.custom.orgPrice;
    			}	
    	}			
    	Transaction.wrap(function () {
    		bli.custom.calculatedPrice = calculatedprice;	  				
    	});
    	BonusAmount = BonusAmount - bli.custom.orgPrice;		
	}
}


function resetRemainingBalance(lineitems) {	
	for each(lineItem in  lineitems) {
		Transaction.wrap(function () {
			lineItem.custom.remainingBalance = null;
		});
	}
}

/**
 * Adds a coupon to the cart using JSON.
 *
 * Gets the CartModel. Gets the coupon code from the httpParameterMap couponCode parameter.
 * In a transaction, adds the coupon to the cart and renders a JSON object that includes the coupon code
 * and the status of the transaction.
 *
 */
function addCouponJson() {
    var couponCode, cart, couponStatus;

    couponCode = request.httpParameterMap.couponCode.stringValue;
    cart = app.getModel('Cart').goc();

    Transaction.wrap(function () {
        couponStatus = cart.addCoupon(couponCode);
    });

    if (request.httpParameterMap.format.stringValue === 'ajax') {
        let r = require('~/cartridge/scripts/util/Response');
        r.renderJSON({
            status: couponStatus.code,
            message: Resource.msgf('cart.' + couponStatus.code, 'checkout', null, couponCode),
            success: !couponStatus.error,
            baskettotal: cart.object.adjustedMerchandizeTotalGrossPrice.value,
            CouponCode: couponCode
        });
    }
}

/**
 * Adds a coupon to the Checkout using JSON.
 *
 * Gets the CartModel. Gets the coupon code from the httpParameterMap couponCode parameter.
 * In a transaction, adds the coupon to the cart and renders a JSON object that includes the coupon code
 * and the status of the transaction.
 *
 */
function addCouponCheckoutJson() {
    var couponStatus, result, code;
    var CouponError = '';
    var couponType='';
    var couponApplied = true;
    cart = app.getModel('Cart').goc();
    var basket = cart.object;
    var couponCode = request.httpParameterMap.couponCode.stringValue;
    if (couponCode) {
        if(enableFSPProgram && dw.system.Site.getCurrent().getID() == 'Mattress-Firm') {
        	resetFSPPriceAdjustments();
        }
        couponCode = couponCode.toUpperCase();
       Transaction.wrap(function () {
           couponStatus =  cart.addCoupon(couponCode);
        });

        if (couponStatus) {
            Transaction.wrap(function () {
                cart.removePersonaliCartRescue();
                cart.removePersonaliFromLineItems();
            });
        } else {
                CouponError = 'NO_ACTIVE_PROMOTION';
        }
    } else {
            CouponError = 'COUPON_CODE_MISSING';
    }
    for each(var couponLineItem in basket.couponLineItems) {
    	if (couponLineItem.couponCode.toUpperCase() == couponCode) {
    		couponApplied = couponLineItem.applied;
    		if(couponApplied) {
			  var priceAdjustment1 = couponLineItem.getPriceAdjustments().iterator();
			  while (priceAdjustment1.hasNext()) {
			   var priceAdjustment2 = priceAdjustment1.next();
			   couponType = priceAdjustment2.appliedDiscount.type;
      		  }
    		}
    			
    	}
    }
    if (request.httpParameterMap.format.stringValue === 'ajax') {
        let r = require('app_storefront_controllers/cartridge/scripts/util/Response');
        var currentCouponBonusLineItems = cart.getBonusLineItemsByCouponCode(couponCode.toUpperCase());
        var currProdQualifiedBonusProducts=cart.getJsonByLineItems(currentCouponBonusLineItems);
        r.renderJSON({
            couponStatus: couponStatus.CouponStatus.code,
            success: !couponStatus.CouponStatus.error,
            CouponCode: couponCode,
            CouponError: CouponError,
            CouponApplied: couponApplied,
            CouponType: couponType,
            qualifiedBonusProducts : currProdQualifiedBonusProducts,
            action : 'addcoupon'
        });
    }
}

function changePickupLocation() {
    var cart = app.getModel('Cart').goc();
    var params = request.httpParameterMap;
    var format = empty(params.format.stringValue) ? '' : params.format.stringValue.toLowerCase();
    var pid = empty(params.pid.stringValue) ? '' : params.pid.stringValue;
    var preferredStore = app.getController('StorePicker').GetPreferredStore();
   // If this product is to be picked up in a store -- update store id
   if (!empty(params.storeId.stringValue)) {
        Transaction.wrap(function () {
            // update the product line item with the store id
            var plisIterator = cart.getAllProductLineItems().iterator();
            while (plisIterator.hasNext()) {
                var pli = plisIterator.next();
                if (session.custom.isCart){
                    // update all products in the cart
                	pli.custom.storePickupStoreID = preferredStore.ID;
                } else {
                    pli.custom.storePickupStoreID = params.storeId.stringValue;
                }
            }
        });
    }
}

function changeDeliveryOption(option) {
    // change from pickup in store to shipped to you option
    var store = "";
    var cart = app.getModel('Cart').goc();
    var params = request.httpParameterMap;
    var deliveryoption = (!empty(params.deliveryoption.value)) ? params.deliveryoption.stringValue : option;
    session.custom.deliveryOptionChoice = deliveryoption;
    var storeId = (deliveryoption == 'shipped') ? null : params.storeId.stringValue;
    if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule') && storeId !== null) {
    	try {
    		var chicagoInventLocationId = UtilFunctionsMF.chicagoInventLocationId();
        	Transaction.wrap(function () {
                var plisIterator = cart.getAllProductLineItems().iterator();
                while (plisIterator.hasNext()) {
                    var pli = plisIterator.next();
                    var key = pli.productID + "-" + storeId;
                    var storeResponse = session.custom[key];
                    if (storeResponse && !empty(storeResponse)) {
                    	pli.custom.InventLocationId = storeResponse.Location1;
                    	pli.custom.secondaryWareHouse = storeResponse.location2;
        	            pli.custom.deliveryDate = storeResponse.SlotDate;
        	            pli.custom.mfiATPLeadDate = storeResponse.MFIATPLeadDate;
                    }
                    if (!empty(pli.product) && UtilFunctionsMF.isParcelableAndCore(pli.product.manufacturerSKU)) {
                    	pli.custom.InventLocationId = chicagoInventLocationId;
                    }
                }
            });
        } catch (e) {
        	var error = e;
        }
    } else {
    	Transaction.wrap(function () {
            var plisIterator = cart.getAllProductLineItems().iterator();
            while (plisIterator.hasNext()) {
                var pli = plisIterator.next();
                pli.custom.storePickupStoreID = storeId;
                pli.custom.secondaryWareHouse = storeId;
            }
        });
         if (request.httpParameterMap.format.stringValue === 'ajax') {
        	if (session.customer.registered && !empty(session.customer.profile.custom.preferredStore)) {
				store = session.customer.profile.custom.preferredStore;
			} else {
				store = (session.custom.preferredStore == null) ? "" : session.custom.preferredStore;
			}
			let r = require('~/cartridge/scripts/util/Response');
			if(empty(store)) {
				r.renderJSON({
       		    	success: false
            	});
			} else {
				r.renderJSON({
       				success: true
    			});
			}
         }
    }
}

function personaliPriceAdjustment(productLineItem, offerId, quantity, adjustedPrice, originalPrice) {
	try {
		productLineItem.custom['personaliDiscount'] = (adjustedPrice - originalPrice);	//	pli.product.priceModel.price.value;
		productLineItem.custom['personaliOfferID'] = offerId;
		// Remove current price adjustment
		var currentPriceAdjustment : PriceAdjustment = productLineItem.getPriceAdjustmentByPromotionID(offerId);
		if(currentPriceAdjustment) {
			productLineItem.removePriceAdjustment(currentPriceAdjustment);
		}
		
		// Create price adjustment
		var priceAdjust = productLineItem.createPriceAdjustment(offerId);
		// Other product-level price adjustments will be removed in CalculateCart.ds
		// priceAdjust.setPriceValue((discountPrice - pli.product.priceModel.price.decimalValue) * params.Quantity.doubleValue);
		priceAdjust.setPriceValue( ( adjustedPrice - originalPrice ) * quantity );
		priceAdjust.setLineItemText(Resource.msg('personali.lineItemText', 'personali', null));
		
		return true;
	} catch (e) {
		var error = e;
		return false;
	}
}

function addCartRescuePriceAdjustments() {
	var cart = app.getModel('Cart').goc();
    var netotiateOfferId = request.httpParameterMap.netotiateOfferId.stringValue;
    if (!empty(netotiateOfferId)) {
    	Transaction.wrap(function () {
    		cart.getPersonaliCartRescueResponse(netotiateOfferId);
    	});
    	return netotiateOfferId;
    }
}

/**
 * Renders Product recommendations for product id
 *
 * Gets the productId (Variant/Master) httpParameterMap
 * 
 */
function addToCartRecommendations(){
	
	var cart = app.getModel('Cart').get();
	var params = request.httpParameterMap;

	if ( !empty(params.productId) ){
		
		var productId =  params.productId.stringValue;
		var lastItem = app.getModel('Product').get(productId);
		
		if ( !empty(lastItem) ){ 
			session.custom.lastItemAddedSize = !empty(lastItem.object.custom.size) ? lastItem.object.custom.size : null;
			app.getView({
					Basket: cart ? cart.object : null,
					LastItem: lastItem
				}).render('product/components/recommendations-intercept');
		}
	}
}

function removeItemFromCart() {
	var Product = app.getModel('Product');
	var cart = app.getModel('Cart').get();
	var params = request.httpParameterMap;
	var lastProductUUID = params.uuid.stringValue;
    var quantity = parseInt(params.quantity.value);

	if (!empty(lastProductUUID)) {       		
		var lineItem = cart.getProductLineItemByUUID(lastProductUUID);
			Transaction.wrap(function () {
				if(lineItem.quantity.value == quantity){
					cart.removeProductLineItem(lineItem);
				} else {
					var productModel = Product.get(lineItem.product.ID);
					var product = productModel.object;
					var optionModel = product.getOptionModel();
					var newQuantity = parseInt(lineItem.quantity.value) - quantity;
					cart.updateLineItem(lineItem, product, newQuantity, optionModel);
				}           		            	      		
			});
	}
    //Redirect to the cart page
	session.custom.recentlyAddedItemCount = parseInt(0);
	response.redirect(URLUtils.https('Cart-Show'));
}

/*
* Module exports
*/

/*
* Exposed methods.
*/
/** Adds a product to the cart.
 * @see {@link module:controllers/Cart~addProduct} */
exports.AddProduct = guard.ensure(['post'], addProduct);
/** Adds multiple products to the cart.
 * @see {@link module:controllers/Cart~addMultipleProducts} */
exports.AddMultipleProducts = guard.ensure(['https'], addMultipleProducts);
/** Invalidates the login and shipment forms. Renders the basket content.
 * @see {@link module:controllers/Cart~show} */
exports.Show = guard.ensure(['https'], show);

exports.ShowATC = guard.ensure(['https'], showATC);
/** Form handler for the cart form.
 * @see {@link module:controllers/Cart~submitForm} */
exports.SubmitForm = guard.ensure(['post', 'https'], submitForm);
/** Redirects the user to the last visited catalog URL.
 * @see {@link module:controllers/Cart~continueShopping} */
exports.ContinueShopping = guard.ensure(['https'], continueShopping);
/** Adds a coupon to the cart using JSON. Called during checkout.
 * @see {@link module:controllers/Cart~addCouponJson} */
exports.AddCouponJson = guard.ensure(['get', 'https'], addCouponJson);
/** Adds a coupon to the cart using JSON. Called during checkout.
 * @see {@link module:controllers/Cart~addCouponCheckoutJson} */
exports.AddCouponCheckoutJson = guard.ensure(['get', 'https'], addCouponCheckoutJson);
/** Displays the current items in the cart in the minicart panel.
 * @see {@link module:controllers/Cart~miniCart} */
exports.MiniCart = guard.ensure(['get'], miniCart);
/** Adds the product with the given ID to the wish list.
 * @see {@link module:controllers/Cart~addToWishlist} */
exports.AddToWishlist = guard.ensure(['get', 'https', 'loggedIn'], addToWishlist, {
    scope: 'wishlist'
});
/** Adds bonus product to cart.
 * @see {@link module:controllers/Cart~addBonusProductJson} */
exports.AddBonusProduct = guard.ensure(['post'], addBonusProductJson);
/** Changes pickup location on products  in cart.
 * @see {@link module:controllers/Cart~changePickupLocation} */
exports.ChangePickupLocation = guard.ensure(['post'], changePickupLocation);
/** Changes pickup in store to shipped to you
 * @see {@link module:controllers/Cart~changeDeliveryOption} */
exports.ChangeDeliveryOption = guard.ensure(['post'], changeDeliveryOption);
exports.ChangeDeliveryOptionInternal = changeDeliveryOption;
/** Adds Cart Rescue price adjustments from Personali
 * @see {@link module:controllers/Cart~addCartRescuePriceAdjustments} */
exports.AddCartRescuePriceAdjustments = guard.ensure(['post'], addCartRescuePriceAdjustments);
/** Add to Cart Recommendations
 * @see {@link module:controllers/Cart~addToCartRecommendations} */
exports.AddToCartRecommendations = guard.ensure(['get'], addToCartRecommendations);
/** Remove multiple ProductLineItems from Cart
 * @see {@link module:controllers/Cart~removeMultipleProductsFromCart} */
exports.RemoveMultipleProductsFromCart = guard.ensure(['get'], removeMultipleProductsFromCart);
exports.BackToShopping = guard.ensure(['https'], backToShopping);
/**
 * Renders a list of lineitems eligible for FSP.
 * @see module:controllers/Cart~getFSPDiscountModal
 */
exports.GetFSPDiscountModal = guard.ensure(['get'], getFSPDiscountModal);

/**
 * Apply PA to list of lineitems eligible for FSP.
 * @see module:controllers/Cart~addFSPPriceAdjustments
 */
exports.AddFSPPriceAdjustments = guard.ensure(['post'], addFSPPriceAdjustments);

/**
 * Delete PA for the given Product ID.
 * @see module:controllers/Cart~deleteFSPDiscount
 */
exports.RemoveFSPPriceAdjustments = guard.ensure(['post'], removeFSPPriceAdjustments);

exports.RemoveItemFromCart = guard.ensure(['get'], removeItemFromCart);

/** Adds bonus product to cart.
 * @see {@link module:controllers/Cart~addTempPedicBonusProductJson} */
exports.AddTempPedicBonusProduct = guard.ensure(['post'], addTempPedicBonusProductJson);

