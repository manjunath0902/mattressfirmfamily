'use strict';

/**
 * Controller that renders the home page.
 *
 * @module controllers/Home
 */

var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var dwLogger = require("dw/system").Logger;
var responseUtils = require('app_storefront_controllers/cartridge/scripts/util/Response');
var Site = require('dw/system/Site');

/**
 * Renders the home page.
 */
function show() {
    var rootFolder = require('dw/content/ContentMgr').getSiteLibrary().root;
    require('~/cartridge/scripts/meta').update(rootFolder);
    //resetting amazon checkout related session variables
    session.custom.amazonDeliveryPage = null;
    session.custom.amazonGuestCheckout = false;
    
    //reseting AmazonAuthErrorMessage and AmazonResultErrorMessage sessions
    session.custom.AmazonAuthErrorMessage=null;
    session.custom.AmazonResultErrorMessage = null;
    if(Site.getCurrent().getCustomPreferenceValue("enableBrightspotHomePage")) {
        app.getView().render('content/home/brightspothomepage');
    }
    else {
        app.getView().render('content/home/homepage');
    }
}

/**
 * Remote include for the header.
 * This is designed as a remote include to achieve optimal caching results for the header.
 */
function includeHeader() {
    app.getView().render('components/header/header');
}

/**
 * Remote include for the header banner only.
 * This is designed as a remote include to achieve optimal caching results for the header.
 */
function includeHeaderBanner() {
    app.getView().render('components/header/header-banner');
}

/**
 * Renders the category navigation and the menu to use as a remote include.
 * It is cached.
 *
 * @deprecated Converted into a template include.
 */
function includeHeaderMenu() {
    app.getView().render('components/header/headermenu');
}

/**
 * Renders the category navigation from slots
 * It is cached.
 */
function includeCategoryNavigation() {
    app.getView().render('components/header/header-bottom-include');
}

/**
 * Renders customer information.
 *
 * This is designed as a remote include as it represents dynamic session information and must not be
 * cached.
 */
function includeHeaderCustomerInfo() {
    app.getView().render('components/header/headercustomerinfo');
}


/**
 * Renders customer information.
 *
 * This is designed as a remote include as it represents dynamic session information and must not be
 * cached.
 */
function includeHeaderTop() {
    app.getView().render('components/header/header-top');
}

/**
 * Sets a 410 HTTP response code for the response and renders an error page (error/notfound template).
 */
function errorNotFound() {
    // @FIXME Correct would be to set a 404 status code but that breaks the page as it utilizes
    // remote includes which the WA won't resolve
    response.setStatus(410);
    app.getView().render('error/notfound');
}

/**
 * Used in the setlayout.isml and htmlhead.isml templates to control device-aware display.
 * Sets the session custom property 'device' to mobile. Renders the changelayout.isml template.
 * TODO As we want to have a responsive layout, do we really need the below?
 */
function mobileSite() {
    session.custom.device = 'mobile';
    app.getView().render('components/changelayout');
}

/**
 * Sets the session custom property 'device' to mobile.  Renders the setlayout.isml template.
 * @FIXME remove - not responsive - maybe replace with a CSS class forcing the layout.
 */
function fullSite() {
    session.custom.device = 'fullsite';
    app.getView().render('components/changelayout');
}

/**
 * Renders the setlayout.isml template.
 * @FIXME remove - not responsive
 */
function setLayout() {
    app.getView().render('components/setlayout');
}

/**
 * Renders the devicelayouts.isml template.
 * @FIXME remove - not responsive
 */
function deviceLayouts() {
    app.getView().render('util/devicelayouts');
}

/**
* Get the customer Zip info.
* @FIXME remove - not responsive
*/
function getLocationByIP() { 
    
    if (empty(session.custom.customerZip)) {
         var geolocationZipCode = request.getGeolocation().getPostalCode();         
         session.custom.customerZip = geolocationZipCode;
    }    
    var geoLogger = dwLogger.getLogger('GeoBrowser','GeoLocation');
    geoLogger.info("Location By IP : " + session.custom.customerZip);    
    responseUtils.renderJSON({        
            CustomerZipCode: session.custom.customerZip         
    });    
}

/**
* Set the customer zip info.
* @FIXME remove - not responsive
*/
function setGeoBrowserZipCode() {
	var zip = request.httpParameterMap.zip.value;
    session.custom.customerZip = zip;
    responseUtils.renderJSON({        
        IsZipCodeSet: true         
    });
}

/**
 * Renders Mobile Geo Locator Drawer on top of the mobile home page
 * 
 */
function getMobileGeoLocator() {
	
	if(dw.system.Site.getCurrent().getCustomPreferenceValue('enableMobileGeolocator')) {		
	    app.getView().render('mobile_geolocator');	
	}else{
		response.writer.print('');
	}
}

/**
 * Renders cart module data on homepage
 * 
 */
function getCartModuleData() {	
    var cart = app.getModel('Cart').get();
    app.getView({
        Basket: cart ? cart.object : null
    }).render('components/home/homepage-cart-module');	
}

/**
 * Renders Server Side Rendered Brightspot Slots
 * 
 */
function brightspotSlot () {
    app.getView().render('brightspot/renderslot');
}

/**
 * Renders Server Side Rendered Brightspot Assets
 * 
 */
function brightspotAsset () {
    app.getView().render('brightspot/renderasset');
}

/*
 * Export the publicly available controller methods
 */
/** Renders the home page.
 * @see module:controllers/Home~show */
exports.Show = guard.ensure(['get'], show);
/** Renders the home page.
 * @see module:controllers/Home~showProd */
exports.ShowProd = guard.ensure(['get'], show);
/** Remote include for the header.
 * @see module:controllers/Home~includeHeader */
exports.IncludeHeader = guard.ensure(['include'], includeHeader);
/** Remote include for the header-banner.
 * @see module:controllers/Home~includeHeaderBanner */
exports.IncludeHeaderBanner = guard.ensure(['include'], includeHeaderBanner);
/** Renders the category navigation and the menu to use as a remote include.
 * @see module:controllers/Home~includeHeaderMenu */
exports.IncludeHeaderMenu = guard.ensure(['include'],includeHeaderMenu);
/** This is designed as a remote include as it represents dynamic session information and must not be cached.
 * @see module:controllers/Home~includeHeaderCustomerInfo */
exports.IncludeHeaderCustomerInfo = guard.ensure(['include'], includeHeaderCustomerInfo);
/** This is designed as a remote include as it represents dynamic session information and must not be cached.
 * @see module:controllers/Home~includeHeaderTop */
exports.IncludeHeaderTop = guard.ensure(['include'], includeHeaderTop);
/** Sets a 410 HTTP response code for the response and renders an error page
 * @see module:controllers/Home~errorNotFound */
exports.ErrorNotFound = guard.ensure(['get'], errorNotFound);
/** Used to control device-aware display.
 * @see module:controllers/Home~mobileSite */
exports.MobileSite = guard.ensure(['get'], mobileSite);
/** Sets the session custom property 'device' to mobile. Renders the setlayout.isml template.
 * @see module:controllers/Home~fullSite */
exports.FullSite = guard.ensure(['get'], fullSite);
/** Renders the setlayout.isml template.
 * @see module:controllers/Home~setLayout */
exports.SetLayout = guard.ensure(['get'], setLayout);
/** Renders the devicelayouts.isml template.
 * @see module:controllers/Home~deviceLayouts */
exports.DeviceLayouts = guard.ensure(['get'], deviceLayouts);
/** Get IP Address for zipcode.
 * @see module:controllers/Home~getIPAddress */
exports.GetLocationByIP = guard.ensure(['get'], getLocationByIP);
/** Set Geo Zip Code.
 * @see module:controllers/Home~setGeoZipCode */
exports.SetGeoBrowserZipCode = guard.ensure(['get'], setGeoBrowserZipCode);
/** Get Mobile Geo Locator
 * @see module:controllers/Home~getMobileGeoLocator */
exports.GetMobileGeoLocator = guard.ensure(['get'], getMobileGeoLocator);
/** Get Cart Module Data
 * @see module:controllers/Home~getCartModuleData */
exports.GetCartModuleData = guard.ensure(['get'], getCartModuleData);

exports.IncludeCategoryNavigation = guard.ensure(['get'],includeCategoryNavigation);
/** Remote include for the Brightspot Slot.
 * @see module:controllers/Home~brightspotSlot */
 exports.BrightspotSlotInclude = guard.ensure(['include'], brightspotSlot);
 /** Remote include for the Brightspot Asset.
 * @see module:controllers/Home~brightspotAsset */
 exports.BrightspotAssetInclude = guard.ensure(['include'], brightspotAsset);
