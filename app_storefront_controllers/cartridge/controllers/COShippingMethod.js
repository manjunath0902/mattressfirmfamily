'use strict';

/**
 * Controller for the default single shipping scenario.
 * Single shipping allows only one shipment, shipping address, and shipping method per order.
 *
 * @module controllers/COShippingMethod
 */

/* API Includes */
var CustomerMgr = require('dw/customer/CustomerMgr');
var HashMap = require('dw/util/HashMap');
var Resource = require('dw/web/Resource');
var ShippingMgr = require('dw/order/ShippingMgr');
var Site = require('dw/system/Site');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var pipeletHelper = require('bc_sleepysc/cartridge/scripts/sleepys/util/SleepysPipeletsHelper').SleepysPipeletHelper;
var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;
var storePicker = require('~/cartridge/controllers/StorePicker');

/**
 * Prepares shipments. Theis function separates gift certificate line items from product
 * line items. It creates one shipment per gift certificate purchase
 * and removes empty shipments. If in-store pickup is enabled, it combines the
 * items for in-store pickup and removes them.
 * This function can be called by any checkout step to prepare shipments.
 *
 * @transactional
 * @return {Boolean} true if shipments are successfully prepared, false if they are not.
 */
function prepareShipments() {
    var cart, homeDeliveries;
    cart = app.getModel('Cart').get();

    homeDeliveries = Transaction.wrap(function () {

        homeDeliveries = false;

        cart.updateGiftCertificateShipments();
        cart.removeEmptyShipments();

        if (Site.getCurrent().getCustomPreferenceValue('enableStorePickUp')) {
            homeDeliveries = cart.consolidateInStoreShipments();

            session.forms.singleshipping.inStoreShipments.shipments.clearFormElement();
            app.getForm(session.forms.singleshipping.inStoreShipments.shipments).copyFrom(cart.getShipments());
        } else {
            homeDeliveries = true;
        }

        return homeDeliveries;
    });

    return homeDeliveries;
}

/**
 * Starting point for the single shipping scenario. Prepares a shipment by removing gift certificate and in-store pickup line items from the shipment.
 * Redirects to multishipping scenario if more than one physical shipment is required and redirects to billing if all line items do not require
 * shipping.
 *
 * @transactional
 */
function start() {
    var cart, pageMeta, homeDeliveries, dateListObject;
    cart = app.getModel('Cart').get();

    if (cart) {
        // Redirects to multishipping scenario if more than one physical shipment is contained in the basket.
        //physicalShipments = cart.getPhysicalShipments();
       // if (!(Site.getCurrent().getCustomPreferenceValue('enableMultiShipping') && physicalShipments && physicalShipments.size() > 1 )) {
       // Will use multi-ship for all orders as long as the site preference is set.
            
            // Initializes the  form and prepopulates it with the shipping address of the default
            // shipment if the address exists, otherwise it preselects the default shipping method in the form.
            if (cart.getDefaultShipment().getShippingAddress()) {
                app.getForm(session.forms.singleshipping.shippingAddress.addressFields).copyFrom(cart.getDefaultShipment().getShippingAddress());
                app.getForm(session.forms.singleshipping.shippingAddress.addressFields.states).copyFrom(cart.getDefaultShipment().getShippingAddress());
                app.getForm(session.forms.singleshipping.shippingAddress).copyFrom(cart.getDefaultShipment());
            } else {
                if (customer.authenticated && customer.registered && customer.addressBook.preferredAddress) {
                    app.getForm(session.forms.singleshipping.shippingAddress.addressFields).copyFrom(customer.addressBook.preferredAddress);
                    app.getForm(session.forms.singleshipping.shippingAddress.addressFields.states).copyFrom(customer.addressBook.preferredAddress);
                }
            }
            session.forms.singleshipping.shippingAddress.shippingMethodID.value = cart.getDefaultShipment().getShippingMethodID();
            

            // Prepares shipments.
            homeDeliveries = prepareShipments();

            Transaction.wrap(function () {
                cart.calculate();
            });
            
            if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule')) {
            	var postalCode = session.forms.singleshipping.shippingAddress.addressFields.postal.value;
            	var deliveryDatesArr = storePicker.GetATPDeliveryDates(cart, postalCode);
            } else {
				var deliveryDatesArr = mattressPipeletHelper.getDeliveryDates(cart); 
			}
			
            // Go to billing step, if we have no product line items, but only gift certificates in the basket, shipping is not required.
            if (cart.getProductLineItems().size() === 0) {
                app.getController('COBilling').Start();
            } else {
                pageMeta = require('~/cartridge/scripts/meta');
                pageMeta.update({
                    pageTitle: Resource.msg('singleshipping.meta.pagetitle', 'checkout', 'SiteGenesis Checkout')
                });
                app.getView({
                    ContinueURL: URLUtils.https('COShippingMethod-SingleShipping'),
                    Basket: cart.object,
                    HomeDeliveries: homeDeliveries,
                    DeliveryDatesArr: deliveryDatesArr
                }).render('checkout/shipping/shippingmethodstep');
            }
    } else {
        app.getController('Cart').Show();
        return;
    }

}

/**
 * Handles the selected shipping address and shipping method. Copies the
 * address details and gift options to the basket's default shipment. Sets the
 * selected shipping method to the default shipment.
 *
 * @transactional
 * @param {module:models/CartModel~CartModel} cart - A CartModel wrapping the current Basket.
 */
function handleShippingSettings(cart) {

    Transaction.wrap(function () {
        var defaultShipment, shippingAddress, validationResult, BasketStatus, EnableCheckout;
        defaultShipment = cart.getDefaultShipment();
        shippingAddress = cart.createShipmentShippingAddress(defaultShipment.getID());

        shippingAddress.setFirstName(session.forms.singleshipping.shippingAddress.addressFields.firstName.value);
        shippingAddress.setLastName(session.forms.singleshipping.shippingAddress.addressFields.lastName.value);
        shippingAddress.setAddress1(session.forms.singleshipping.shippingAddress.addressFields.address1.value);
        shippingAddress.setAddress2(session.forms.singleshipping.shippingAddress.addressFields.address2.value);
        shippingAddress.setCity(session.forms.singleshipping.shippingAddress.addressFields.cities.city.value);
        shippingAddress.setPostalCode(session.forms.singleshipping.shippingAddress.addressFields.postal.value);
        shippingAddress.setStateCode(session.forms.singleshipping.shippingAddress.addressFields.states.state.value);
        shippingAddress.setCountryCode(session.forms.singleshipping.shippingAddress.addressFields.country.value);
        shippingAddress.setPhone(session.forms.singleshipping.shippingAddress.addressFields.phone.value);
        defaultShipment.setGift(session.forms.singleshipping.shippingAddress.isGift.value);
        defaultShipment.setGiftMessage(session.forms.singleshipping.shippingAddress.giftMessage.value);

        cart.updateShipmentShippingMethod(cart.getDefaultShipment().getID(), session.forms.singleshipping.shippingAddress.shippingMethodID.value, null, null);

        pipeletHelper.updateBasketWithSelectedDate(session.forms.singleshipping.shippingAddress.addressFields.deliveryDate.htmlValue, session.forms.singleshipping.shippingAddress.addressFields.fleetwiseToken.htmlValue, cart.object);
        
        //TODO
        //Call splitShipments() should be reviewed after getting clarifications
        //on Shipping Service return parameters to assign shipping method to shipment
        //Comment out till clarification
        //pipeletHelper.splitShipments(cart.object);
        
        
        cart.calculate();

        validationResult = cart.validateForCheckout();

        // TODO - what are those variables used for, do they need to be returned ?
        BasketStatus = validationResult.BasketStatus;
        EnableCheckout = validationResult.EnableCheckout;

    });

    return;
}

/**
 * Form handler for the singleshipping form. Handles the following actions:
 * - __save__ - saves the shipping address from the form to the customer address book. If in-store
 * shipments are enabled, saves information from the form about in-store shipments to the order shipment.
 * Flags the save action as done and calls the {@link module:controllers/Cart~Show|Cart controller Show function}.
 * If it is not able to save the information, calls the {@link module:controllers/Cart~Show|Cart controller Show function}.
 * - __selectAddress__ - updates the address details and page metadata, sets the ContinueURL property to COShipping-SingleShipping,  and renders the singleshipping template.
 * - __shipToMultiple__ - calls the {@link module:controllers/COShippingMultiple~Start|COShippingMutliple controller Start function}.
 * - __error__ - calls the {@link module:controllers/COShippingMethod~Start|COShipping controller Start function}.
 */
function singleShipping() {
    var singleShippingForm = app.getForm('singleshipping');
    singleShippingForm.handleAction({
        save: function () {
            var cart = app.getModel('Cart').get();

            if (cart) {

                handleShippingSettings(cart);

                // Attempts to save the used shipping address in the customer address book.
                if (customer.authenticated && session.forms.singleshipping.shippingAddress.addToAddressBook.value) {
                    app.getModel('Profile').get(customer.profile).addAddressToAddressBook(cart.getDefaultShipment().getShippingAddress());
                }
                // Binds the store message from the user to the shipment.
                if (Site.getCurrent().getCustomPreferenceValue('enableStorePickUp')) {

                    if (!app.getForm(session.forms.singleshipping.inStoreShipments.shipments).copyTo(cart.getShipments())) {
                        require('./Cart').Show();
                        return;
                    }
                }
                
                var COShippingController = app.getController('COShipping');
                COShippingController.PrepareShipments();
                COShippingController.UpdateDeliveryDateSelection(cart);

                // Mark step as fulfilled.
                session.forms.singleshipping.fulfilled.value = true;
                if (session.forms.billing.fulfilled.value === true) {
                    initBillingAddressForm(cart);
                    // @FIXME redirect
                    app.getController('COSummary').Start();
                } else {
                    // @FIXME redirect
                    app.getController('COBilling').Start();
                }
            } else {
                // @FIXME redirect
                app.getController('Cart').Show();
            }
        },
        selectAddress: function () {
            updateAddressDetails(app.getModel('Cart').get());

            var pageMeta = require('~/cartridge/scripts/meta');
            pageMeta.update({
                pageTitle: Resource.msg('singleshipping.meta.pagetitle', 'checkout', 'SiteGenesis Checkout')
            });
            app.getView({
                ContinueURL: URLUtils.https('COShippingMethod-SingleShipping')
            }).render('checkout/shipping/singleshipping');

            return;
        },
        error: function () {
            response.redirect(URLUtils.https('COShippingMethod-Start'));
            return;
        }
    });
}

/**
 * Selects a shipping method for the default shipment. Creates a transient address object, sets the shipping
 * method, and returns the result as JSON response.
 *
 * @transaction
 */
function selectShippingMethod() {
    var cart, address, applicableShippingMethods, TransientAddress;
    TransientAddress = app.getModel('TransientAddress');
    cart = app.getModel('Cart').get();

    if (cart) {

        address = new TransientAddress();
        address.countryCode = session.forms.singleshipping.shippingAddress.addressFields.country.value;
        address.stateCode = session.forms.singleshipping.shippingAddress.addressFields.states.state.value;
        address.postalCode = session.forms.singleshipping.shippingAddress.addressFields.postal.value;
        address.city = session.forms.singleshipping.shippingAddress.addressFields.cities.city.value;
        address.address1 = session.forms.singleshipping.shippingAddress.addressFields.address1.value;
        address.address2 = session.forms.singleshipping.shippingAddress.addressFields.address2.value;

        applicableShippingMethods = cart.getApplicableShippingMethods(address);

        Transaction.wrap(function () {
            cart.updateShipmentShippingMethod(cart.getDefaultShipment().getID(), request.httpParameterMap.shippingMethodID.stringValue, null, applicableShippingMethods);
            cart.calculate();
        });

        app.getView({
            Basket: cart.object
        }).render('checkout/shipping/selectshippingmethodjson');
    } else {
        app.getView.render('checkout/shipping/selectshippingmethodjson');
    }
}

/**
 * Determines the list of applicable shipping methods for the default shipment of
 * the current basket. The applicable shipping methods are based on the
 * merchandise in the cart and any address parameters included in the request.
 * Changes the shipping method of this shipment if the current method
 * is no longer applicable. Precalculates the shipping cost for each applicable
 * shipping method by simulating the shipping selection i.e. explicitly adds each
 * shipping method and then calculates the cart.
 * The simulation is done so that shipping cost along
 * with discounts and promotions can be shown to the user before making a
 * selection.
 * @transaction
 */
function updateShippingMethodList() {
    var i, cart, address, applicableShippingMethods, shippingCosts, currentShippingMethod, method, TransientAddress;
    TransientAddress = app.getModel('TransientAddress');
    cart = app.getModel('Cart').get();

    if (cart) {

        address = new TransientAddress();
        address.countryCode = session.forms.singleshipping.shippingAddress.addressFields.country.value;
        address.stateCode = session.forms.singleshipping.shippingAddress.addressFields.states.state.value;
        address.postalCode = session.forms.singleshipping.shippingAddress.addressFields.postal.value;
        address.city = session.forms.singleshipping.shippingAddress.addressFields.cities.city.value;
        address.address1 = session.forms.singleshipping.shippingAddress.addressFields.address1.value;
        address.address2 = session.forms.singleshipping.shippingAddress.addressFields.address2.value;

        applicableShippingMethods = cart.getApplicableShippingMethods(address);
        shippingCosts = new HashMap();
        currentShippingMethod = cart.getDefaultShipment().getShippingMethod() || ShippingMgr.getDefaultShippingMethod();

        // Transaction controls are for fine tuning the performance of the data base interactions when calculating shipping methods
        Transaction.begin();

        for (i = 0; i < applicableShippingMethods.length; i += 1) {
            method = applicableShippingMethods[i];

            cart.updateShipmentShippingMethod(cart.getDefaultShipment().getID(), method.getID(), method, applicableShippingMethods);
            cart.calculate();
            shippingCosts.put(method.getID(), cart.preCalculateShipping(method));
        }

        Transaction.rollback();

        Transaction.wrap(function () {
            cart.updateShipmentShippingMethod(cart.getDefaultShipment().getID(), currentShippingMethod.getID(), currentShippingMethod, applicableShippingMethods);
            cart.calculate();
        });

        session.forms.singleshipping.shippingAddress.shippingMethodID.value = cart.getDefaultShipment().getShippingMethodID();

        app.getView({
            Basket: cart.object,
            ApplicableShippingMethods: applicableShippingMethods,
            ShippingCosts: shippingCosts
        }).render('checkout/shipping/shippingmethods');
    } else {
        app.getController('Cart').Show();
    }
}

/**
 * Determines the list of applicable shipping methods for the default shipment of
 * the current customer's basket and returns the response as a JSON array. The
 * applicable shipping methods are based on the merchandise in the cart and any
 * address parameters are included in the request parameters.
 */
function getApplicableShippingMethodsJSON() {
    var cart, address, applicableShippingMethods, TransientAddress;
    TransientAddress = app.getModel('TransientAddress');
    cart = app.getModel('Cart').get();

    address = new TransientAddress();
    address.countryCode = session.forms.singleshipping.shippingAddress.addressFields.country.value;
    address.stateCode = session.forms.singleshipping.shippingAddress.addressFields.states.state.value;
    address.postalCode = session.forms.singleshipping.shippingAddress.addressFields.postal.value;
    address.city = session.forms.singleshipping.shippingAddress.addressFields.cities.city.value;
    address.address1 = session.forms.singleshipping.shippingAddress.addressFields.address1.value;
    address.address2 = session.forms.singleshipping.shippingAddress.addressFields.address2.value;

    applicableShippingMethods = cart.getApplicableShippingMethods(address);

    app.getView({
        ApplicableShippingMethods: applicableShippingMethods
    }).render('checkout/shipping/shippingmethodsjson');
}

function initBillingAddressForm(cart) {

    if (app.getForm('singleshipping').object.shippingAddress.useAsBillingAddress.value === true) {
        app.getForm('billing').object.billingAddress.addressFields.firstName.value = app.getForm('singleshipping').object.shippingAddress.addressFields.firstName.value;
        app.getForm('billing').object.billingAddress.addressFields.lastName.value = app.getForm('singleshipping').object.shippingAddress.addressFields.lastName.value;
        app.getForm('billing').object.billingAddress.addressFields.address1.value = app.getForm('singleshipping').object.shippingAddress.addressFields.address1.value;
        app.getForm('billing').object.billingAddress.addressFields.address2.value = app.getForm('singleshipping').object.shippingAddress.addressFields.address2.value;
        app.getForm('billing').object.billingAddress.addressFields.city.value = app.getForm('singleshipping').object.shippingAddress.addressFields.cities.city.value;
        app.getForm('billing').object.billingAddress.addressFields.postal.value = app.getForm('singleshipping').object.shippingAddress.addressFields.postal.value;
        app.getForm('billing').object.billingAddress.addressFields.phone.value = app.getForm('singleshipping').object.shippingAddress.addressFields.phone.value;
        app.getForm('billing').object.billingAddress.addressFields.states.state.value = app.getForm('singleshipping').object.shippingAddress.addressFields.states.state.value;
        app.getForm('billing').object.billingAddress.addressFields.country.value = app.getForm('singleshipping').object.shippingAddress.addressFields.country.value;
        app.getForm('billing').object.billingAddress.addressFields.phone.value = app.getForm('singleshipping').object.shippingAddress.addressFields.phone.value;
    } else if (cart.getBillingAddress() !== null) {
        app.getForm('billing.billingAddress.addressFields').copyFrom(cart.getBillingAddress());
        app.getForm('billing.billingAddress.addressFields.states').copyFrom(cart.getBillingAddress());
    } else if (customer.authenticated && customer.profile.addressBook.preferredAddress !== null) {

        app.getForm('billing.billingAddress.addressFields').copyFrom(customer.profile.addressBook.preferredAddress);
        app.getForm('billing.billingAddress.addressFields.states').copyFrom(customer.profile.addressBook.preferredAddress);
    }
}
/*
* Module exports
*/

/*
* Web exposed methods
*/
/** Starting point for the single shipping scenario.
 * @see module:controllers/COShippingMethod~start */
exports.Start = guard.ensure(['https'], start);
/** Selects a shipping method for the default shipment.
 * @see module:controllers/COShippingMethod~selectShippingMethod */
exports.SelectShippingMethod = guard.ensure(['https', 'get'], selectShippingMethod);
/** Determines the list of applicable shipping methods for the default shipment of the current basket.
 * @see module:controllers/COShippingMethod~updateShippingMethodList */
exports.UpdateShippingMethodList = guard.ensure(['https', 'get'], updateShippingMethodList);
/** Determines the list of applicable shipping methods for the default shipment of the current customer's basket and returns the response as a JSON array.
 * @see module:controllers/COShippingMethod~getApplicableShippingMethodsJSON */
exports.GetApplicableShippingMethodsJSON = guard.ensure(['https', 'get'], getApplicableShippingMethodsJSON);
/** Form handler for the singleshipping form.
 * @see module:controllers/COShippingMethod~singleShipping */
exports.SingleShipping = guard.ensure(['https', 'post'], singleShipping);

/*
 * Local methods
 */
exports.PrepareShipments = prepareShipments;
