importPackage( dw.system );
importPackage( dw.util );
importPackage( dw.web ); 
importPackage( dw.io );
importPackage( dw.object );
importPackage( dw.net );
importPackage( dw.catalog );

var UtilFunctionsMF = require('app_mattressfirm_storefront/cartridge/scripts/util/UtilFunctions').UtilFunctions;

'use strict';

/**
 * Controller 
 *
 * @module controllers/DispatchTracker
 */

/* API Includes */
var Logger = require('dw/system/Logger');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');


// Receives the AJAX submission of dispatch tracking and responds
function submitOrderNumber() {
	var params = request.httpParameterMap;
	var orderNumber : String = params.orderNumber.stringValue;
	var securityOption : String = params.securityOption.stringValue;
	var securityValue : String = params.securityValue.stringValue;
	//var CSRFTokenName : String = params.CSRFTokenName.stringValue;
	//var CSRFToken : String = params.CSRFToken.stringValue;
	var CSRFValidate : Bolean = dw.web.CSRFProtection.validateRequest();
	var dispatchTrackAPICSRFEnable = Site.current.preferences.custom.dispatchTrackAPICSRFEnable;
	
	if(CSRFValidate || dispatchTrackAPICSRFEnable == false){

		var httpClient : HTTPClient = new HTTPClient();
		var dispatchTrackAPIUrl = Site.current.preferences.custom.dispatchTrackAPIUrl;
		var dispatchTrackAPIUser = Site.current.preferences.custom.dispatchTrackAPIUser;
		var dispatchTrackAPIPassword = Site.current.preferences.custom.dispatchTrackAPIPassword;
		var jsonObj : String;
		var apiUrl : String =  dispatchTrackAPIUrl + orderNumber + "?" + securityOption + "=" + securityValue;
		
		httpClient.open('GET', apiUrl ,dispatchTrackAPIUser, dispatchTrackAPIPassword);
		httpClient.setTimeout(3000000);
		httpClient.send();
		 
		if (httpClient.statusCode == 200)
		{
			 jsonObj = httpClient.text;
			 
			 var validJson = true;
			 var trackJsonString = "";
			try {
				var trackingJson = JSON.parse(jsonObj);
				Logger.info("Dispatch tracking Request URL: " + apiUrl)
				Logger.info("Dispatch tracking Response: " + JSON.stringify(trackingJson));
				// Check if valid keys are  part of json
				if(Object.keys(trackingJson).length > 0) {
					
					if(!empty(trackingJson[0]) && !empty(trackingJson[0].line_items) ) {
						
						for (var i = 0; i < trackingJson[0].line_items.length; i++){							
							var lineItem = trackingJson[0].line_items[i];
							var info = UtilFunctionsMF.getCarrierDetails(lineItem.carrierName, lineItem.trackingNumber);
							lineItem.trackingURL = info.url;
							lineItem.carrierName = !empty(info.name) ? info.name : lineItem.carrierName ;
						}
					}
					trackJsonString = JSON.stringify(trackingJson);			
				}else{
					validJson = false;
				}
			} catch(e) {
				validJson = false;
			}
			
			if(validJson) {
				 app.getView({
					 JSONData: trackJsonString,
				 }).render('util/output');
			}else {
				
				 app.getView({
					 JSONData: jsonObj,
				 }).render('util/output');
			}
		}
		else
		{
		     // error handling
			 //var errorObj = JSON.parse(httpClient.errorText);
			 //var errorObjMessage = errorObj.error;
			 jsonObj = '{"error":"An error occurred with status code '+httpClient.statusCode+'"}';
			 app.getView({
				 JSONData: jsonObj,
			 }).render('util/output');	 
		 }
		
	}else{
	     // error handling
		 jsonObj = '{"error":"CSRF expired"}';
		 app.getView({
			 JSONData: jsonObj,
		 }).render('util/output');			
	}
		
}

//Gets product image id and size
function getProductInfo() {
	var params = request.httpParameterMap;
	var product : Product = ProductMgr.getProduct(params.pid.stringValue);

	var jsonObj : String;

	var imageID : String = null;
	if(product.custom.external_id != null){
		imageID = product.custom.external_id;
	}

	var size : String = null;		
	var sizeAttribute : String = null;
	var isParcelable : String = "false";
	var shippingInfo : String = null

	if(product.attributeModel != null)
	{
		isParcelable = product.attributeModel.getValue(product.getAttributeModel().getAttributeDefinition('isParcelable'));
		shippingInfo = product.attributeModel.getValue(product.getAttributeModel().getAttributeDefinition('shippingInformation'));
	}

	if(product.variationModel != null){	
		try {
			sizeAttribute = product.variationModel.getSelectedValue(product.variationModel.getProductVariationAttribute("size"));
		} catch (e) {
			sizeAttribute = null;
		}

		if(sizeAttribute != null){
			size = sizeAttribute.displayValue;
		}
	}
	
	var title : String = null;
	if (product.name != null) {
		title = product.name.replace('&ldquo;',' Inch').replace('"',' Inch');
	}else{
		title = "";
	}

	if(product){
		 jsonObj = '{"ID":"' + params.pid.stringValue + '","imageID":"' + imageID + '","size":"' + size + '","name":"' + title + '","isParcelable":"' + isParcelable + '","shippingInfo":"' + shippingInfo + '"}';
		 app.getView({
			 JSONData: jsonObj,
		 }).render('util/output');		
	}else{
		 jsonObj = '{"error":"Product not found"}';
		 app.getView({
			 JSONData: jsonObj,
		 }).render('util/output');		
	}
}



//FedEx Tracking
function fedexTracking() {
	var params = request.httpParameterMap;
	var fedexTrackingNumber : String = params.fedexTrackingNumber.stringValue;
	//var CSRFTokenName : String = params.CSRFTokenName.stringValue;
	//var CSRFToken : String = params.CSRFToken.stringValue;
	var CSRFValidate : Bolean = dw.web.CSRFProtection.validateRequest();
	
	if(CSRFValidate){

		var httpClient : HTTPClient = new HTTPClient();
		var fedexTrackAPIUrl = Site.current.preferences.custom.fedexTrackAPIUrl;
		var fedexTrackKey = Site.current.preferences.custom.fedexTrackKey;
		var fedexTrackPassword = Site.current.preferences.custom.fedexTrackPassword;
		var fedexTrackUserKey = Site.current.preferences.custom.fedexTrackKey;
		var fedexTrackUserPassword = Site.current.preferences.custom.fedexTrackPassword;
		var fedexTrackAccountNumber = Site.current.preferences.custom.fedexTrackAccountNumber;
		var fedexTrackMeterNumber = Site.current.preferences.custom.fedexTrackMeterNumber;

	    var email = "";

	    var cDataString : String;
	    var theXML : XML = 
	    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v14="http://fedex.com/ws/track/v14">
	    <soapenv:Header/>
	    <soapenv:Body>
	        <v14:TrackRequest>
        <v14:WebAuthenticationDetail>
           <v14:ParentCredential>
              <v14:Key>{fedexTrackKey}</v14:Key>
              <v14:Password>{fedexTrackPassword}</v14:Password>
           </v14:ParentCredential>
           <v14:UserCredential>
              <v14:Key>{fedexTrackUserKey}</v14:Key>
              <v14:Password>{fedexTrackUserPassword}</v14:Password>
           </v14:UserCredential>
        </v14:WebAuthenticationDetail>
        <v14:ClientDetail>
           <v14:AccountNumber>{fedexTrackAccountNumber}</v14:AccountNumber>
           <v14:MeterNumber>{fedexTrackMeterNumber}</v14:MeterNumber>
        </v14:ClientDetail>
        <v14:TransactionDetail>
           <v14:CustomerTransactionId>Track By Number_v14</v14:CustomerTransactionId>
           <v14:Localization>
              <v14:LanguageCode>EN</v14:LanguageCode>
              <v14:LocaleCode>US</v14:LocaleCode>
           </v14:Localization>
        </v14:TransactionDetail>
        <v14:Version>
           <v14:ServiceId>trck</v14:ServiceId>
           <v14:Major>14</v14:Major>
           <v14:Intermediate>0</v14:Intermediate>
           <v14:Minor>0</v14:Minor>
        </v14:Version>
        <v14:SelectionDetails>
           <v14:CarrierCode>FDXE</v14:CarrierCode>
           <v14:PackageIdentifier>
              <v14:Type>TRACKING_NUMBER_OR_DOORTAG</v14:Type>
              <v14:Value>{fedexTrackingNumber}</v14:Value>
           </v14:PackageIdentifier>        
           <v14:ShipmentAccountNumber/>
           <v14:SecureSpodAccount/>
             <v14:Destination>
              <v14:GeographicCoordinates>rates evertitque aequora</v14:GeographicCoordinates>
           </v14:Destination>
        </v14:SelectionDetails>
        </v14:TrackRequest>
        </soapenv:Body>
        </soapenv:Envelope>;
		
		var jsonObj : String;
		
		var httpClient : HTTPClient = new dw.net.HTTPClient();
		var message : XML;
		    
		httpClient.setRequestHeader("Content-Type","text/xml");
		httpClient.setTimeout(30000);
		httpClient.open('POST', fedexTrackAPIUrl);		
		httpClient.send( theXML );
		 
		if (httpClient.statusCode == 200)
		{
			 message = new XML(httpClient.text);
			 app.getView({
				 JSONData: message,
			 }).render('util/output');	 
		}
		else
		{
		     // error handling
			 jsonObj = '{"error":"An error occurred with status code '+httpClient.statusCode+'"}'+'<br>'+httpClient.errorText+'';
			 app.getView({
				 JSONData: jsonObj,
			 }).render('util/output');	 
		 }
		
	}else{
	     //error handling
		 jsonObj = '{"error":"CSRF expired"}';
		 app.getView({
			 JSONData: jsonObj,
		 }).render('util/output');			
	}
		
}


/* Exports of the controller */

/** @see {@link module:controllers/DispatchTracking~submitOrderNumber} */
exports.SubmitOrderNumber = guard.ensure(['get'], submitOrderNumber);

/** @see {@link module:controllers/DispatchTracking~getProductInfo} */
exports.GetProductInfo = guard.ensure(['get'], getProductInfo);

/** @see {@link module:controllers/DispatchTracking~fedexTracking} */
exports.FedexTracking = guard.ensure(['get'], fedexTracking);
