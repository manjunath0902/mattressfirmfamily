'use strict';

/**
 * Controller handling search, category, and suggestion pages.
 *
 * @module controllers/Search
 */

/* API Includes */
var ISML = require('dw/template/ISML');
var PagingModel = require('dw/web/PagingModel');
var URLUtils = require('dw/web/URLUtils');
var ContentMgr = require('dw/content/ContentMgr');
var Site = require('dw/system/Site');
var Store = require('app_storefront_controllers/cartridge/controllers/Stores');
var dwLogger = require("dw/system/Logger");
var HashMap = require('dw/util/HashMap');
var ProductUtils = require('app_storefront_core/cartridge/scripts/product/ProductUtils.js');
var productsCountWithOutZipCode = 0;
/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var SleepysPipeletHelper = require('bc_sleepysc/cartridge/scripts/sleepys/util/SleepysPipeletsHelper').SleepysPipeletHelper;
var currentSiteId = Site.getCurrent().ID;
var ProductMgr = require('dw/catalog/ProductMgr');


/**
 * Renders a full-featured product search result page.
 * If the httpParameterMao format parameter is set to "ajax" only the product grid is rendered instead of the full page.
 *
 * Checks for search redirects configured in Business Manager based on the query parameters in the
 * httpParameterMap. If a search redirect is found, renders the redirect (util/redirect template).
 * Constructs the search based on the HTTP params and sets the categoryID. Executes the product search and then the
 * content asset search.
 *
 * If no search term, search parameter or refinement was specified for the search and redirects
 * to the Home controller Show function. If there are any product search results
 * for a simple category search, it dynamically renders the category page for the category searched.
 *
 * If the search query included category refinements, or is a keyword search it renders a product hits page for the category
 * (rendering/category/categoryproducthits template).
 * If only one product is found, renders the product detail page for that product.
 * If there are no product results found, renders the nohits page (search/nohits template).
 * @see {@link module:controllers/Search~showProductGrid|showProductGrid} function}.
 * 
 * 
 */

function show() {

    var params = request.httpParameterMap;
    var isUserSearch = false;

    if (params.format.stringValue === 'ajax' || params.format.stringValue === 'page-element') {
        // TODO refactor and merge showProductGrid() code into here
        showProductGrid();
        return;
    }

    // TODO - replace with script API equivalent once available
    var SearchRedirectURLResult = new dw.system.Pipelet('SearchRedirectURL').execute({
        SearchPhrase: params.q.value
    });

    if (SearchRedirectURLResult.result === PIPELET_NEXT) {
        ISML.renderTemplate('util/redirect', {
            Location: SearchRedirectURLResult.Location,
            CacheTag: true
        });
        return;
    }

    // Constructs the search based on the HTTP params and sets the categoryID.
    var Search = app.getModel('Search');
    var productSearchModel = Search.initializeProductSearchModel(params);
    var contentSearchModel = Search.initializeContentSearchModel(params);
    var Content = app.getModel('Content');
    var searchAsset = Content.get('search-metadata');
    var noSearchAsset = Content.get('search-metadata-noresult');
    var pageMeta = require('~/cartridge/scripts/meta');
    
    // execute the product search
    productSearchModel.search();
    contentSearchModel.search();
    
    var abTestMastervsVariantActive = false;
    if(currentSiteId == 'Mattress-Firm' && dw.campaign.ABTestMgr.isParticipant('PLP_Master_vs_Variation_tiles', 'SegmentA') ){    	   
	    if(!empty(params) && !empty(params.cgid) && params.cgid == '5637146827'){    	
	    	if(productSearchModel.category && !empty(productSearchModel.category.ID) && productSearchModel.category.ID == 'mattress-sizes'){
	    		abTestMastervsVariantActive = true;
	    	}    	
	    }
    }
    productsCountWithOutZipCode = productSearchModel.count;
    if (productSearchModel.emptyQuery && contentSearchModel.emptyQuery) {
        response.redirect(URLUtils.abs('Home-Show'));
    } else if (productSearchModel.count > 0) {    	
    	if(Site.getCurrent().getCustomPreferenceValue('enableGeoLocationSearch')) {
        	applyGeoLocationRefinement(productSearchModel);
        }  	
        if ((productSearchModel.count > 1) || productSearchModel.refinedSearch || (contentSearchModel.count > 0)) {
            //if No Result after applying Refinement, redirect user to no Result page
        	if(productSearchModel.count == 0){
            	redirectNoHitPage(productSearchModel,contentSearchModel);            	
            }
            else{
	        	var productPagingModel = new PagingModel(productSearchModel.productSearchHits, productSearchModel.count);
	            if (params.start.submitted) {
	                productPagingModel.setStart(params.start.intValue);
	            }
	
	            if(currentSiteId == 'Mattress-Firm' || currentSiteId == '1800Mattress-RV')
	        	{
	            	if (params.sz.submitted && request.httpParameterMap.sz.intValue <= 90) {
	                    productPagingModel.setPageSize(params.sz.intValue);
	                }else {                	
	                	productPagingModel.setPageSize(Site.getCurrent().getCustomPreferenceValue('plpPageSize'));                    
	                }
	        	}else
	    		{
	        		if (params.sz.submitted && request.httpParameterMap.sz.intValue <= 60) {
	                    productPagingModel.setPageSize(params.sz.intValue);
	                } else {                	
	            		    productPagingModel.setPageSize(Site.getCurrent().getCustomPreferenceValue('plpPageSize'));                    
	                }
	    		}
	           
	            
	            if (productSearchModel.category) {
	                require('~/cartridge/scripts/meta').update(productSearchModel.category,productSearchModel);
	            }
	
	            if (productSearchModel.categorySearch && !productSearchModel.refinedCategorySearch && productSearchModel.category.template) {
	                // Renders a dynamic template.
	                app.getView({
	                    ProductSearchResult: productSearchModel,
	                    ContentSearchResult: contentSearchModel,
	                    ProductPagingModel: productPagingModel
	                }).render(productSearchModel.category.template);
	            } else {
	
	                //SearchPromo - for displaying search driven banners above the product grid, provided there is a q parameter in the httpParameterMap
	                var searchPromo;
	                if (params.q.value) {
	                    searchPromo = ContentMgr.getContent('keyword_' + params.q.value.toLowerCase());
	                    // In this case mapping to NoIndexOverride because that is what should trigger if it's a user search. Recommended by Merkel SEO
	                    isUserSearch = true;
	                    pageMeta.update(searchAsset);
	                }
	                app.getView({
	                    ProductSearchResult: productSearchModel,
	                    ContentSearchResult: contentSearchModel,
	                    ProductPagingModel: productPagingModel,
	                    SearchPromo: searchPromo,
	                    NoIndexOverride: isUserSearch,
	                    MasterToVariantSwitch: abTestMastervsVariantActive
	                }).render('rendering/category/categoryproducthits');
	            }
            }
        } else {
            var targetProduct = productSearchModel.getProductSearchHits().next();
            var productID = targetProduct.productID;
            // If the target was not a master, simply use the product ID.
            if (targetProduct.product.isMaster()) {
                // In the case of a variation master, the master is the representative for
                // all its variants. If there is only one variant, return the variant's
                // product ID.
                var iter = productSearchModel.getProductSearchHits();
                if (iter.hasNext()) {
                    var productSearchHit = iter.next();
                    if (productSearchHit.getRepresentedProducts().size() === 1) {
                        productID = productSearchHit.getFirstRepresentedProductID();
                    }
                }
            }

            ISML.renderTemplate('util/redirect', {
                Location: URLUtils.http('Product-Show', 'pid', productID)
            });
        }
    } else {
    	redirectNoHitPage(productSearchModel,contentSearchModel);
    }

}
/**
 * Return the requested number of products based on passed variation, also apply GEO Location
 * if applyGeoLocation Flag is on 
 * @param vName
 * @param vValue
 * @param applyGeoLocation
 * @param returnCount
 * @returns
 */
function getProductSearchResultByVariation(vName,vValue,applyGeoLocation,returnCount)
{
	var params = request.httpParameterMap;
		
	var Search = app.getModel('Search');
    var productSearchModel = Search.initializeProductSearchModel(params);
    	
	//Search only Orderable products
	productSearchModel.setOrderableProductsOnly(true);
	
	if(vName != null && vValue != null){
		productSearchModel.addRefinementValues(vName, vValue);
	}
	
	// execute the product search
    productSearchModel.search();
    
    // Apply GEO Location
    if(applyGeoLocation && Site.getCurrent().getCustomPreferenceValue('enableGeoLocationSearch')) {
    	applyGeoLocationRefinement(productSearchModel);
    }
    
    if(returnCount>0)
    {
    	var index=0;
    	var productArray : Array = [];
    	var productSearchHitIt = productSearchModel.productSearchHits;
		while(productSearchHitIt.hasNext()){
			var productSearchHit = productSearchHitIt.next();    	
    		var product = productSearchHit.getProduct();
    		
    		if(product.isMaster){
    			for each(var variant in product.variants) {
    				if(!empty(variant.custom.size) && variant.custom.size.toLowerCase() == vValue.toLowerCase()){
    	    			if(ProductUtils.hasSalePrice(variant)){
    	    				productArray.push(variant);
    	    				index=index+1;
    	    				break;
    	    			}
    	    		}
    			}
    		} 
    		else
    		{
	    		if(!empty(product.custom.size)){
	    			productArray.push(product);
	    			index=index+1;    		
	    		}
    		}
    		if(returnCount==index){
				break;
			}
    	}
    	return productArray;
    }
    
    return productSearchModel;
}
/**
 * Render the Top selling king and Queen Mattress based on User GEO Location
 * @returns
 */
function showTopSellingMattresses(){
	var NumOfRecProducts = 'NumOfRecProducts' in dw.system.Site.current.preferences.custom ? dw.system.Site.current.preferences.custom.NumOfRecProducts : 10;
	var kProducts = getProductSearchResultByVariation('size','King',true,NumOfRecProducts);
	var qProducts = getProductSearchResultByVariation('size','Queen',true,NumOfRecProducts);	
	app.getView({
		kProducts : kProducts,
		qProducts : qProducts
    }).render('search/topsellingproducts');
}

/**
 * Redirect user to No Result page.
 * @param productSearchModel
 * @param contentSearchModel
 * @returns
 */
function redirectNoHitPage(productSearchModel,contentSearchModel){
	var Content = app.getModel('Content');
	var noSearchAsset = Content.get('search-metadata-noresult');
	var pageMeta = require('~/cartridge/scripts/meta');
	var params = request.httpParameterMap;
	if (params.q.submitted) {
		pageMeta.update(noSearchAsset);		
		//do it only for mattressfirm
		if (currentSiteId == 'Mattress-Firm' ) {
			var zipCode = getValidZipCode();
			var deliveryAddressInfo = getZipCodeInfo(zipCode);
			var noResultTemplate = 'search/nohitsnewtemplate';
			var productsCount = productsCountWithOutZipCode;
			if(productsCount == 0){
				noResultTemplate = 'search/nohits';
			}
		    app.getView({
		    	deliveryAddressInfo: deliveryAddressInfo,		    	
		    	ProductSearchResult: productSearchModel,
		        ContentSearchResult: contentSearchModel,
		        ProductsCount : productsCount
		    }).render(noResultTemplate);
		} else {
			//search query no result page for other sites except mattress-firm
			app.getView({
		        ProductSearchResult: productSearchModel,
		        ContentSearchResult: contentSearchModel
		    }).render('search/nohits');
		}
	} else {
		//PLP no results case
	    app.getView({
	        ProductSearchResult: productSearchModel,
	        ContentSearchResult: contentSearchModel
	    }).render('search/nohits'); 
	}
    return;
}


/**
 * Renders a full-featured content search result page.
 *
 * Constructs the search based on the httpParameterMap params and executes the product search and then the
 * content asset search.
 *
 * If no search term, search parameter or refinement was specified for the search, it redirects
 * to the Home controller Show function. If there are any content search results
 * for a simple folder search, it dynamically renders the content asset page for the folder searched.
 * If the search included folder refinements, it renders a folder hits page for the folder
 * (rendering/folder/foldercontenthits template).
 *
 * If there are no product results found, renders the nohits page (search/nohits template).
 */
function showContent() {

    var params = request.httpParameterMap;

    var Search = app.getModel('Search');
    var productSearchModel = Search.initializeProductSearchModel(params);
    var contentSearchModel = Search.initializeContentSearchModel(params);

    // Executes the product search.
    productSearchModel.search();
    contentSearchModel.search();

    if (productSearchModel.emptyQuery && contentSearchModel.emptyQuery) {
        response.redirect(URLUtils.abs('Home-Show'));
    } else if (contentSearchModel.count > 0) {

        var contentPagingModel = new PagingModel(contentSearchModel.content, contentSearchModel.count);
        contentPagingModel.setPageSize(Site.getCurrent().getCustomPreferenceValue('plpPageSize'));
        if (params.start.submitted) {
            contentPagingModel.setStart(params.start.intValue);
        }

        if (contentSearchModel.folderSearch && !contentSearchModel.refinedFolderSearch && contentSearchModel.folder.template) {
            // Renders a dynamic template
            app.getView({
                ProductSearchResult: productSearchModel,
                ContentSearchResult: contentSearchModel,
                ContentPagingModel: contentPagingModel
            }).render(contentSearchModel.folder.template);
        } else {
            app.getView({
                ProductSearchResult: productSearchModel,
                ContentSearchResult: contentSearchModel,
                ContentPagingModel: contentPagingModel
            }).render('rendering/folder/foldercontenthits');
        }
    } else {
        app.getView({
            ProductSearchResult: productSearchModel,
            ContentSearchResult: contentSearchModel
        }).render('search/nohits');
    }

}

/**
 * Determines search suggestions based on httpParameterMap query information and renders the JSON response for the list of suggestions.
 * Renders the search suggestion page (search/suggestions template).
 */
function getSuggestions() {
    /*
     * Switches between legacy and beta versions of the search suggest feature based on the enhancedSearchSuggestions site preference.
     */
    if (!(request.httpParameterMap.legacy && request.httpParameterMap.legacy == 'true')) {
        app.getView().render('search/suggestionsbeta');
    } else {
        // TODO - refactor once search suggestion can be retrieved via the script API.
        var GetSearchSuggestionsResult = new dw.system.Pipelet('GetSearchSuggestions').execute({
            MaxSuggestions: 10,
            SearchPhrase: request.httpParameterMap.q.value
        });

        app.getView({
            Suggestions: GetSearchSuggestionsResult.Suggestions
        }).render('search/suggestions');

    }

}


/**
 * Renders the partial content of the product grid of a search result as rich HTML.
 *
 * Constructs the search based on the httpParameterMap parameters and executes the product search and then the
 * content asset search. Constructs a paging model and determines whether the infinite scrolling feature is enabled.
 *
 * If there are any product search results for a simple category search, it dynamically renders the category page
 * for the category searched.
 *
 * If the search query included category refinements or is a keyword search, it renders a product hits page for the category
 * (rendering/category/categoryproducthits template).
 */
function showProductGrid() {

    var params = request.httpParameterMap;

    // Constructs the search based on the HTTP params and sets the categoryID.
    var Search = app.getModel('Search');
    var productSearchModel = Search.initializeProductSearchModel(params);
    var contentSearchModel = Search.initializeContentSearchModel(params);

    // Executes the product search.
    productSearchModel.search();
    contentSearchModel.search();
    
    var abTestMastervsVariantActive = false;
    if(currentSiteId == 'Mattress-Firm' && dw.campaign.ABTestMgr.isParticipant('PLP_Master_vs_Variation_tiles', 'SegmentA') ){    	   
	    if(!empty(params) && !empty(params.cgid) && params.cgid == '5637146827'){    	
	    	if(productSearchModel.category && !empty(productSearchModel.category.ID) && productSearchModel.category.ID == 'mattress-sizes'){
	    		abTestMastervsVariantActive = true;
	    	}    	
	    }
    }
    
    if(Site.getCurrent().getCustomPreferenceValue('enableGeoLocationSearch')) {
    	applyGeoLocationRefinement(productSearchModel);
    }    
    var productPagingModel = new PagingModel(productSearchModel.productSearchHits, productSearchModel.count);
    if (params.start.submitted) {
        productPagingModel.setStart(params.start.intValue);
    }
    
    
    if(currentSiteId == 'Mattress-Firm' || currentSiteId == '1800Mattress-RV')
	{
    	if (params.sz.submitted && params.sz.intValue <= 90) {
            productPagingModel.setPageSize(params.sz.intValue);
        }else {                	
        	productPagingModel.setPageSize(Site.getCurrent().getCustomPreferenceValue('plpPageSize'));                    
        }
	}else
	{
		if (params.sz.submitted && params.sz.intValue <= 60) {
	        productPagingModel.setPageSize(params.sz.intValue);
	    } else {                	
    		    productPagingModel.setPageSize(Site.getCurrent().getCustomPreferenceValue('plpPageSize'));                    
        }
	}

    if (Site.getCurrent().getCustomPreferenceValue('enableInfiniteScroll') && params.format.stringValue === 'page-element') {
        app.getView({
            ProductSearchResult: productSearchModel,
            ProductPagingModel: productPagingModel
        }).render('search/productgridwrapper');
    } else {
        if (productSearchModel.categorySearch && !productSearchModel.refinedCategorySearch && productSearchModel.category.template) {
            // Renders a dynamic template.
            app.getView({
                ProductSearchResult: productSearchModel,
                ContentSearchResult: contentSearchModel,
                ProductPagingModel: productPagingModel
            }).render(productSearchModel.category.template);
        } else {
            app.getView({
                ProductSearchResult: productSearchModel,
                ContentSearchResult: contentSearchModel,
                ProductPagingModel: productPagingModel,
                MasterToVariantSwitch: abTestMastervsVariantActive
            }).render('rendering/category/categoryproducthits');
        }
    }

}
/**
 * Commented out as not used

function getZoneForMF() {
	var zip = request.httpParameterMap.zip.value;
	SleepysPipeletHelper.setCustomerZone(zip);
	var obj = new Object();
    obj.zonecode = session.custom.customerZone;
    app.getView({
        obj: obj
    }).render('util/json');
}
*/

function findMatress() {
	var zip = request.httpParameterMap.zip.value;
	SleepysPipeletHelper.setCustomerZone(zip);
    if (productSearchModel.emptyQuery && contentSearchModel.emptyQuery) {
        response.redirect(URLUtils.abs('Home-Show'));
    } else {
    	app.getView({
            psm: productSearchModel,
            step: request.httpParameterMap.step.value
        }).render("components/mattressfinderendpoint");
    }
}

function pdpZoneZip() {
	/*
	var pid = request.httpParameterMap.pid.value;
	var AvailInZone = SleepysPipeletHelper.pdpZoneCheck(pid);
	 */

	var zip = request.httpParameterMap.zip.value;
	SleepysPipeletHelper.setCustomerZone(zip);

	if (request.httpParameterMap.format.stringValue == 'json') {
		app.getView({
			//AvailInZone: AvailInZone,
			step: request.httpParameterMap.step.value
		}).render("product/brand-x/json/availability");
	} else {
		app.getView({
			//AvailInZone: AvailInZone,
			step: request.httpParameterMap.step.value
		}).render("product/components/availability");
	}
}

function initZone() {
	var zip = request.httpParameterMap.zip.value;

	SleepysPipeletHelper.setCustomerZone(zip);
	/*	set Preferred store when updating zip from header	*/
	if (!session.customer.registered && !empty(session.custom.customerZip)) {
		var app = require('app_storefront_controllers/cartridge/scripts/app');
		app.getController('StorePicker').SetPreferredStoreFromGeoLocation();
		session.custom.deliveryOptionChoice = 'shipped';
	}

    app.getView().render("components/header/zonefield");
}
function einsteinCarousel() {
	 app.getView().render("components/einsteincarousel");
	}
	
function getValidZipCode() {
	if(empty(request.getGeolocation()) && empty(session.custom.customerZip)) {
		return null;
	}
	var geolocationZip = request.getGeolocation() ? request.getGeolocation().getPostalCode() : null;
	var existingZipCode = session.custom.customerZip;
	var zipCode = geolocationZip;
	if(existingZipCode && existingZipCode != '00000') {
		zipCode = existingZipCode; 
	}
	return zipCode;
}

function getStoresByGeoLocation(zipCode) {
	if(empty(request.getGeolocation()) && empty(session.custom.customerZip)) {
		return null;
	}        
	var nearestStores = Store.GetStoresByGeoLocation(zipCode);
    return nearestStores;   
}

/**
 * zip info object by zipcode
 */
function getZipCodeInfo(zipCode) {
	var deliveryAddressInfo =  Store.GetWareHouseInfo(zipCode); 
	return deliveryAddressInfo;      
}

/**
 * get stored zipcode from cookies and send to template
 */
function getLastVisitedZipcodes() {
	var lastVisitedZipCodes = new Array();           
	var zipCodeValues;	
	//get cookies for stored zipcode
	var lastThreeZipCode = request.httpCookies['lastThreeZipCode'];
    if(!empty(lastThreeZipCode)) {
    	zipCodeValues = lastThreeZipCode.getValue().split('-');
    	for(var i=0; i < zipCodeValues.length; i++) {    		
    		lastVisitedZipCodes.push(getZipCodeInfo(zipCodeValues[i]));
    	}	    	
    }    
    return lastVisitedZipCodes;  
}

/**
 * plp delivery zipcode
 * get stored zipcode from cookies and send to template
 */
function getDeliveryLocationPLP() {	
	var zipCode = getValidZipCode();
	var deliveryAddressInfo = getZipCodeInfo(zipCode);
	//get cookies for stored zipcode
    var lastVisitedZipCodes = getLastVisitedZipcodes();    
    app.getView({
    	deliveryAddressInfo: deliveryAddressInfo,
    	lastVisitedZipCodes: lastVisitedZipCodes
    }).render('search/components/plp_geo_delivery');    
}

/**
 * plp mobile delivery zipcode
 * get stored zipcode from cookies and send to template
 */
function getDeliveryLocationMobilePLP() {	
	var zipCode = getValidZipCode();
	var deliveryAddressInfo = getZipCodeInfo(zipCode);
	//get cookies for stored zipcode
    var lastVisitedZipCodes = getLastVisitedZipcodes();    
    app.getView({
    	deliveryAddressInfo: deliveryAddressInfo,
    	lastVisitedZipCodes: lastVisitedZipCodes
    }).render('search/components/plp_geo_delivery_mobile');    
}

/**
 * plp delivery zipcode ab
 * get stored zipcode ab from cookies and send to template
 */
function getDeliveryLocationPLPAB() {    
    var zipCode = getValidZipCode();
    var deliveryAddressInfo = getZipCodeInfo(zipCode);
    //get cookies for stored zipcode
    var lastVisitedZipCodes = getLastVisitedZipcodes();    
    app.getView({
        deliveryAddressInfo: deliveryAddressInfo,
        lastVisitedZipCodes: lastVisitedZipCodes
    }).render('search/components/plp_geo_delivery_ab');    
}

/**
 * brand content asset delivery zipcode * 
 */
function getDeliveryLocationShopByBrand() {	
	var zipCode = getValidZipCode();
	var deliveryAddressInfo = getZipCodeInfo(zipCode);   
    app.getView({
    	deliveryAddressInfo: deliveryAddressInfo    	
    }).render('search/components/shop_by_brand_geo_delivery');    
}

/**
 * home content asset delivery zipcode * 
 */
function getDeliveryLocationHome() {	
	var zipCode = getValidZipCode();
	var deliveryAddressInfo = getZipCodeInfo(zipCode);   
    app.getView({
    	deliveryAddressInfo: deliveryAddressInfo    	
    }).render('search/components/home_geo_delivery');    
}
/**
 * Render the GEO Location on Marketing Page
 * @returns
 */
function getDeliveryLocationMarketLanding() {	
	var zipCode = getValidZipCode();
	var deliveryAddressInfo = getZipCodeInfo(zipCode);   
    app.getView({
    	deliveryAddressInfo: deliveryAddressInfo    	
    }).render('search/components/marketing_geo_delivery');    
}

/**
 * Get Customer Delivery Location for header section
 * @returns
 */
function getDeliveryLocationHeader() {
	var zipCode = getValidZipCode();	
	var deliveryAddressInfo = getZipCodeInfo(zipCode);
	//get cookies for stored zipcode
    var lastVisitedZipCodes = getLastVisitedZipcodes();  
    app.getView({
    	deliveryAddressInfo: deliveryAddressInfo,
    	lastVisitedZipCodes: lastVisitedZipCodes
    }).render('search/components/header_view_geo_delivery');    
}

/**
 * Get Customer Delivery Location for mobile header section
 * @returns
 */
function getDeliveryLocationMobileHeader() {
	var zipCode = getValidZipCode();	
	var deliveryAddressInfo = getZipCodeInfo(zipCode);
	//get cookies for stored zipcode
    var lastVisitedZipCodes = getLastVisitedZipcodes();  
    app.getView({
    	deliveryAddressInfo: deliveryAddressInfo,
    	lastVisitedZipCodes: lastVisitedZipCodes
    }).render('search/components/header_view_geo_delivery_mobile');    
}

/**
 * Get Customer Delivery Location for ab PDP
 * @returns
 */
function getDeliveryLocationPDP() {
	var zipCode = getValidZipCode();	
	var deliveryAddressInfo = getZipCodeInfo(zipCode);      
    app.getView({
    	deliveryAddressInfo: deliveryAddressInfo    	
    }).render('product/pdp_delivery_ab');    
}

/**
 * Get Product For Purple Carousel Brand Page
 * @returns
 */
function getProductForBrandCarousel() {
	var paramNames = request.httpParameterMap.parameterNames;
	var productIDList = new Array();
	if(!empty(paramNames) && paramNames.length > 0 ){
		for(i=0; i < paramNames.length ; i++) {
			var pid = request.httpParameterMap.get(paramNames[i]);
			if(!empty(pid)) {
				productIDList.push(pid);
			}
		}	 
	}
	    
	app.getView({    	
		productIDList: productIDList    	
	}).render('components/brandproductcarousel');    
}

function getProductForBrandCarouselTile(){
	var productID = request.httpParameterMap.pid.stringValue;
	var Product;
	if(!empty(productID)){
		Product = ProductMgr.getProduct(productID);
	}
	app.getView({    	
		Product: Product    	
	}).render('components/brandproductcarousel_tile');
}

/**
 * set customer zipcode and set in cookies
 */
function setCustomerZipCode() {
	var responseUtils = require('app_storefront_controllers/cartridge/scripts/util/Response');
    var zip = request.httpParameterMap.zipCode.value;
    //get zipinfo
    var isValidZipCode = getZipCodeInfo(zip);
    if(!empty(isValidZipCode) && !empty(isValidZipCode.state) && !empty(isValidZipCode.city)) {
    	session.custom.customerZip = zip;
    }
    
    //set in cookies
    var lastThreeZipCode = request.httpCookies['lastThreeZipCode'];    
    if(!empty(lastThreeZipCode)) {
    	    var zipCodeCollection = lastThreeZipCode.getValue().split('-');
    	    var zipCodeCookiesValue = lastThreeZipCode.getValue();
    	    if(zipCodeCollection.length < 3) {    	    	
    	    	if(zipCodeCookiesValue.indexOf(session.custom.customerZip) == -1){
    	    		zipCodeCookiesValue = zipCodeCookiesValue + '-' + session.custom.customerZip;
    	    	}    	    	
    	    }else {
    	    	if(zipCodeCookiesValue.indexOf(session.custom.customerZip) == -1){
    	    	zipCodeCookiesValue = zipCodeCookiesValue.substring(zipCodeCookiesValue.indexOf('-')+1, zipCodeCookiesValue.length);    	    	
    	    		zipCodeCookiesValue = zipCodeCookiesValue + '-' + session.custom.customerZip;
    	    	}
    	    }	    	
	    	lastThreeZipCode.setMaxAge(7*24*60*60); 
	    	lastThreeZipCode.setSecure(true);
	    	lastThreeZipCode.setValue(zipCodeCookiesValue);	    	
	    	lastThreeZipCode.setPath("/");
    } else {
	    	var lastThreeZipCode = dw.web.Cookie("lastThreeZipCode",session.custom.customerZip); 				
	    	lastThreeZipCode.setMaxAge(7*24*60*60);
	    	lastThreeZipCode.setSecure(true);
	    	lastThreeZipCode.setValue(session.custom.customerZip);	    	
	    	lastThreeZipCode.setPath("/");
    }
    request.addHttpCookie(lastThreeZipCode);
	response.addHttpCookie(lastThreeZipCode);     
    responseUtils.renderJSON({        
        SetCustomerZipCode: true         
    });     
}


/**
 * Get nearest geo store 
 */
function getNearestGeoStoreMobileHeader() {
	var zipCode = getValidZipCode();
	var nearestGeoStores = getStoresByGeoLocation(zipCode);	
	var deliveryAddressInfo = getZipCodeInfo(zipCode);
	//get cookies for stored zipcode
    var lastVisitedZipCodes = getLastVisitedZipcodes();
    //set customer default store
    if(!empty(nearestGeoStores) && nearestGeoStores.length > 0) {
       if (customer.registered && !empty(request.httpParameterMap.storeId.value)) {
		    Transaction.wrap(function () {
			    //customer.profile.custom.preferredStore = nearestGeoStores[0].ID;
		    });
		} else {
			//session.custom.preferredStore = nearestGeoStores[0].ID
		}     
    }
	
    app.getView({    	
    	nearestGeoStores: nearestGeoStores,
    	deliveryAddressInfo: deliveryAddressInfo,
    	lastVisitedZipCodes:lastVisitedZipCodes    	
    }).render('search/components/header_view_geo_storeInfo_mobile');	    
}

/**
 * Get nearest geo store 
 */
function getNearestGeoStoreHeader() {
	var zipCode = getValidZipCode();
	var nearestGeoStores = getStoresByGeoLocation(zipCode);	
	var deliveryAddressInfo = getZipCodeInfo(zipCode);
	//get cookies for stored zipcode
    var lastVisitedZipCodes = getLastVisitedZipcodes();
    //set customer default store
    if(!empty(nearestGeoStores) && nearestGeoStores.length > 0) {
       if (customer.registered && !empty(request.httpParameterMap.storeId.value)) {
		    Transaction.wrap(function () {
			    //customer.profile.custom.preferredStore = nearestGeoStores[0].ID;
		    });
		} else {
			//session.custom.preferredStore = nearestGeoStores[0].ID
		}     
    }
	
    app.getView({    	
    	nearestGeoStores: nearestGeoStores,
    	deliveryAddressInfo: deliveryAddressInfo,
    	lastVisitedZipCodes:lastVisitedZipCodes    	
    }).render('search/components/header_view_geo_storeInfo');	    
}

/**
 * Applies geo location based refinement using warehouse IDs based on zip code
 * it takes product search model add refinement and returns back same search model.
 */
function applyGeoLocationRefinement (productSearchModel) {
	//custom category
	var isGeoLocationSAYTEnabled =  false;
	if(!empty(dw.system.Site.current.preferences.custom.enableGeoLocationSAYT)) {
		isGeoLocationSAYTEnabled = dw.system.Site.current.preferences.custom.enableGeoLocationSAYT && request.httpParameterMap.q.submitted;
	}
	
	if((productSearchModel.category && productSearchModel.category.custom.isGeoSearchEnabled) ||  isGeoLocationSAYTEnabled) {
		try {
			if(empty(request.getGeolocation()) && empty(session.custom.customerZip)) {
				return productSearchModel;
			}
			var geolocationZip = request.getGeolocation() ? request.getGeolocation().getPostalCode() : null;
			var existingZipCode = session.custom.customerZip;
			var zipCode = geolocationZip;
			if(existingZipCode) {
				zipCode = existingZipCode; 
			}

			var matchedZipObj = getZipCodeInfo(zipCode);        
	        productSearchModel.setOrderableProductsOnly(true);
	        if(!empty(matchedZipObj)) {
	        	productSearchModel.setRefinementValues("warehouseIds", matchedZipObj.warehouseId);
	        }
	        productSearchModel.search();  
	        
	        var wareHouseIdsLogger = dwLogger.getLogger('WareHouseIds','WareHouseIds');
	        wareHouseIdsLogger.info("WareHouseIds : " + matchedZipObj.warehouseId);   
	    
	    } catch (e) {
	        var msg = e;
	    }
	}	
	return productSearchModel;
}

/**
 * Renders Purple accessories products.
**/
function includeBrandAccessories() {
	var productIDList = new Array();
	var paramNames = request.httpParameterMap.parameterNames;
	var i;
	for(i=0; i < paramNames.length ; i++) {
		var pid = request.httpParameterMap.get(paramNames[i]);
		if(!empty(pid)) {
			productIDList.push(pid);
		}
	}
	
    app.getView({
    	productIDList :productIDList 
    }).render('components/brandaccessories'); 
}

function includeBrandAccessoriesProducts() {
	var productID = request.httpParameterMap.pid.stringValue;
	var loopStateIndex = request.httpParameterMap.loopstateindex.stringValue;
	var Product;
	if(!empty(productID)){
		Product = ProductMgr.getProduct(productID);
	}
    app.getView({
    	Product: Product,
    	loopStateIndex: loopStateIndex
    }).render('components/brandaccessories_products'); 
}

/**
 * Renders mobile header geo information for both
 *   - Find a Store
 *   - Ship to {zip} 
 *
 * This is designed as a remote include as it represents dynamic session information and must not be
 * cached.
 */
function includeGeoLocationHeaderMobile() {
    var zipCode = getValidZipCode();
    var nearestGeoStores = getStoresByGeoLocation(zipCode); 
    var deliveryAddressInfo = getZipCodeInfo(zipCode);
    //get cookies for stored zipcode
    var lastVisitedZipCodes = getLastVisitedZipcodes();

    /*
    //set customer default store
    if(!empty(nearestGeoStores) && nearestGeoStores.length > 0) {
       if (customer.registered && !empty(request.httpParameterMap.storeId.value)) {
            Transaction.wrap(function () {
                //customer.profile.custom.preferredStore = nearestGeoStores[0].ID;
            });
        } else {
            //session.custom.preferredStore = nearestGeoStores[0].ID
        }     
    }
    */
      
    app.getView({
        nearestGeoStores: nearestGeoStores,
        deliveryAddressInfo: deliveryAddressInfo,
        lastVisitedZipCodes: lastVisitedZipCodes
    }).render('search/components/geolocation_header_mobile');    
}


/**
 * Renders desktop header geo information for both
 *   - Find a Store
 *   - Delivery & Shipping 
 *
 * This is designed as a remote include as it represents dynamic session information and must not be
 * cached.
 */
function includeGeoLocationHeaderDesktop() {
    var zipCode = getValidZipCode();    
    var deliveryAddressInfo = getZipCodeInfo(zipCode);
    var nearestGeoStores = getStoresByGeoLocation(zipCode);
    //get cookies for stored zipcode
    var lastVisitedZipCodes = getLastVisitedZipcodes();  
    
    /*
    //set customer default store
    if(!empty(nearestGeoStores) && nearestGeoStores.length > 0) {
       if (customer.registered && !empty(request.httpParameterMap.storeId.value)) {
            Transaction.wrap(function () {
                //customer.profile.custom.preferredStore = nearestGeoStores[0].ID;
            });
        } else {
            //session.custom.preferredStore = nearestGeoStores[0].ID
        }     
    }
    */
    
    app.getView({       
        nearestGeoStores: nearestGeoStores,
        deliveryAddressInfo: deliveryAddressInfo,
        lastVisitedZipCodes:lastVisitedZipCodes     
    }).render('search/components/geolocation_header');    

}

/**
 * Render the passed products in passed template
 * @returns
 */
function getProductCarouselByProductIDs() {
	var paramNames = request.httpParameterMap.parameterNames;
	var carouseltemplate = '';
	var templateToInclude = '';
	var showTryInStore = false;
	
	if(request.httpParameterMap.carouseltemplate.submitted) {
		carouseltemplate = request.httpParameterMap.carouseltemplate.stringValue;
	}
	else {
		carouseltemplate = 'components/brandproductcarousel';
	}
	if(!empty(carouseltemplate)){
		if(carouseltemplate == 'components/productcarouselsealy'){
			templateToInclude = 'components/productcarouselsealy_tile';
		}
	}
	if(request.httpParameterMap.showTryInStore.submitted) {
		showTryInStore = request.httpParameterMap.showTryInStore.stringValue.toLowerCase() === 'true'?true:false;
	}
	paramNames.remove('carouseltemplate');
	var productList = new Array();
	var productIDList = new Array();
	if(!empty(paramNames) && paramNames.length > 0 ){
		for(i=0; i < paramNames.length ; i++) {
			//iterate only product parameters, add parameters which you want to skip
			if(paramNames[i] === 'tabname' || paramNames[i] === 'showTryInStore') continue; 
			
			var pid = request.httpParameterMap.get(paramNames[i]);
			if(!empty(pid)){
				productIDList.push(pid);
			}
			var product = ProductMgr.getProduct(pid);
			if(!empty(product)) {
				productList.push(product);
			}
		}	 
	}
	    
    app.getView({    	
    	productList: productList,
    	showTryInStore:  showTryInStore,
    	templateToInclude: templateToInclude,
    	productIDList: productIDList
    }).render(carouseltemplate);    
}	

function getProductInfoByID() {
	var showTryInStore = request.httpParameterMap.showTryInStore.stringValue;
	var templateToInclude = request.httpParameterMap.templateToInclude.stringValue;
	var productID = request.httpParameterMap.pid.stringValue;
	var product;
	if(!empty(productID)){
		product = ProductMgr.getProduct(productID);
	}
	    
    app.getView({
    	product: product,
    	Product: product,
    	showTryInStore: showTryInStore
    }).render(templateToInclude);    
}

function showStoreItems() {

    var params = request.httpParameterMap;
    var isUserSearch = false;

    if (params.format.stringValue === 'ajax' || params.format.stringValue === 'page-element') {
        // TODO refactor and merge showProductGrid() code into here
    	showStoreProductGrid();
        return;
    }

    // TODO - replace with script API equivalent once available
    var SearchRedirectURLResult = new dw.system.Pipelet('SearchRedirectURL').execute({
        SearchPhrase: params.q.value
    });

    if (SearchRedirectURLResult.result === PIPELET_NEXT) {
        ISML.renderTemplate('util/redirect', {
            Location: SearchRedirectURLResult.Location,
            CacheTag: true
        });
        return;
    }

    // Constructs the search based on the HTTP params and sets the categoryID.
    var Search = app.getModel('Search');
    var productSearchModel = Search.initializeProductSearchModel(params);
    var contentSearchModel = Search.initializeContentSearchModel(params);
    var Content = app.getModel('Content');
    var searchAsset = Content.get('search-metadata');
    var noSearchAsset = Content.get('search-metadata-noresult');
    var pageMeta = require('~/cartridge/scripts/meta');

    if (params.store.submitted) {
        productSearchModel.setRefinementValues("storeIds", params.store.value);
    }
    
    //productSearchModel.setRefinementValues("mattress_type", "001005");
    // execute the product search
    productSearchModel.search();
    contentSearchModel.search();
    productsCountWithOutZipCode = productSearchModel.count;
    if (productSearchModel.emptyQuery && contentSearchModel.emptyQuery) {
        response.redirect(URLUtils.abs('Home-Show'));
    } else if (productSearchModel.count > 0) {    	
    	if(Site.getCurrent().getCustomPreferenceValue('enableGeoLocationSearch')) {
        	applyGeoLocationRefinement(productSearchModel);
        }  	
        if ((productSearchModel.count > 0) || productSearchModel.refinedSearch || (contentSearchModel.count > 0)) {
            //if No Result after applying Refinement, redirect user to no Result page
        	if(productSearchModel.count == 0){
            	redirectNoHitPage(productSearchModel,contentSearchModel);            	
            }
            else{
	        	var productPagingModel = new PagingModel(productSearchModel.productSearchHits, productSearchModel.count);
	            if (params.start.submitted) {
	                productPagingModel.setStart(params.start.intValue);
	            }
	
	            if(currentSiteId == 'Mattress-Firm' || currentSiteId == '1800Mattress-RV')
	        	{
	            	if (params.sz.submitted && request.httpParameterMap.sz.intValue <= 90) {
	                    productPagingModel.setPageSize(params.sz.intValue);
	                }else {                	
	                	productPagingModel.setPageSize(Site.getCurrent().getCustomPreferenceValue('plpPageSize'));                    
	                }
	        	}else
	    		{
	        		if (params.sz.submitted && request.httpParameterMap.sz.intValue <= 60) {
	                    productPagingModel.setPageSize(params.sz.intValue);
	                } else {                	
	            		    productPagingModel.setPageSize(Site.getCurrent().getCustomPreferenceValue('plpPageSize'));                    
	                }
	    		}
	           
	            
	            if (productSearchModel.category) {
	                require('~/cartridge/scripts/meta').update(productSearchModel.category,productSearchModel);
	            }
	
	            if (productSearchModel.categorySearch && !productSearchModel.refinedCategorySearch && productSearchModel.category.template) {
	                // Renders a dynamic template.
	                app.getView({
	                    ProductSearchResult: productSearchModel,
	                    ContentSearchResult: contentSearchModel,
	                    ProductPagingModel: productPagingModel
	                }).render(productSearchModel.category.template);
	            } else {
	
	                //SearchPromo - for displaying search driven banners above the product grid, provided there is a q parameter in the httpParameterMap
	                var searchPromo;
	                if (params.q.value) {
	                    searchPromo = ContentMgr.getContent('keyword_' + params.q.value.toLowerCase());
	                    // In this case mapping to NoIndexOverride because that is what should trigger if it's a user search. Recommended by Merkel SEO
	                    isUserSearch = true;
	                    pageMeta.update(searchAsset);
	                }
	                app.getView({
	                    ProductSearchResult: productSearchModel,
	                    ContentSearchResult: contentSearchModel,
	                    ProductPagingModel: productPagingModel,
	                    SearchPromo: searchPromo,
	                    NoIndexOverride: isUserSearch
	                }).render('rendering/category/categoryproducthits');
	            }
            }
        }
    } else {
    	redirectNoHitPage(productSearchModel,contentSearchModel);
    }


}

function showStoreProductGrid() {

    var params = request.httpParameterMap;

    // Constructs the search based on the HTTP params and sets the categoryID.
    var Search = app.getModel('Search');
    var productSearchModel = Search.initializeProductSearchModel(params);
    var contentSearchModel = Search.initializeContentSearchModel(params);
    if (params.store.submitted) {
        productSearchModel.setRefinementValues("storeIds", params.store.value);
    }
    // Executes the product search.
    productSearchModel.search();
    contentSearchModel.search();
    if(Site.getCurrent().getCustomPreferenceValue('enableGeoLocationSearch')) {
    	applyGeoLocationRefinement(productSearchModel);
    }    
    var productPagingModel = new PagingModel(productSearchModel.productSearchHits, productSearchModel.count);
    if (params.start.submitted) {
        productPagingModel.setStart(params.start.intValue);
    }
    
    
    if(currentSiteId == 'Mattress-Firm' || currentSiteId == '1800Mattress-RV')
	{
    	if (params.sz.submitted && params.sz.intValue <= 90) {
            productPagingModel.setPageSize(params.sz.intValue);
        }else {                	
        	productPagingModel.setPageSize(Site.getCurrent().getCustomPreferenceValue('plpPageSize'));                    
        }
	}else
	{
		if (params.sz.submitted && params.sz.intValue <= 60) {
	        productPagingModel.setPageSize(params.sz.intValue);
	    } else {                	
    		    productPagingModel.setPageSize(Site.getCurrent().getCustomPreferenceValue('plpPageSize'));                    
        }
	}

    if (Site.getCurrent().getCustomPreferenceValue('enableInfiniteScroll') && params.format.stringValue === 'page-element') {
        app.getView({
            ProductSearchResult: productSearchModel,
            ProductPagingModel: productPagingModel
        }).render('search/productgridwrapper');
    } else {
        if (productSearchModel.categorySearch && !productSearchModel.refinedCategorySearch && productSearchModel.category.template) {
            // Renders a dynamic template.
            app.getView({
                ProductSearchResult: productSearchModel,
                ContentSearchResult: contentSearchModel,
                ProductPagingModel: productPagingModel
            }).render(productSearchModel.category.template);
        } else {
            app.getView({
                ProductSearchResult: productSearchModel,
                ContentSearchResult: contentSearchModel,
                ProductPagingModel: productPagingModel
            }).render('rendering/category/categoryproducthits');
        }
    }

}

/*
 * Web exposed methods
 */
/** Renders a full featured product search result page.
 * @see module:controllers/Search~show */
exports.Show            = guard.ensure(['get'], show);
/** Renders a full featured content search result page.
 * @see module:controllers/Search~showContent */
exports.ShowContent     = guard.ensure(['get'], showContent);
/** Determines search suggestions based on a given input and renders the JSON response for the list of suggestions.
 * @see module:controllers/Search~getSuggestions */
exports.GetSuggestions  = guard.ensure(['get'], getSuggestions);
//exports.GetZoneForMF  = guard.ensure(['get'], getZoneForMF);
exports.InitZone  = guard.ensure(['post'], initZone);
exports.PDPZipZone = guard.ensure(['post'], pdpZoneZip);
exports.EinsteinCarousel = guard.ensure(['get'], einsteinCarousel);
exports.GetDeliveryLocationPLP =  guard.ensure(['include'],getDeliveryLocationPLP);
exports.GetDeliveryLocationPLPAB =  guard.ensure(['include'],getDeliveryLocationPLPAB);
exports.SetCustomerZipCode =  guard.ensure(['get'],setCustomerZipCode);
exports.GetDeliveryLocationHeader =  guard.ensure(['include'],getDeliveryLocationHeader);
exports.GetNearestGeoStoreHeader =  guard.ensure(['include'],getNearestGeoStoreHeader);
exports.GetDeliveryLocationShopByBrand = guard.ensure(['get'],getDeliveryLocationShopByBrand);
exports.GetDeliveryLocationHome = guard.ensure(['get'],getDeliveryLocationHome);
exports.GetDeliveryLocationMobilePLP =  guard.ensure(['include'],getDeliveryLocationMobilePLP);
exports.GetNearestGeoStoreMobileHeader = guard.ensure(['include'],getNearestGeoStoreMobileHeader);
exports.GetDeliveryLocationMobileHeader = guard.ensure(['include'],getDeliveryLocationMobileHeader);
exports.GetDeliveryLocationPDP = guard.ensure(['include'],getDeliveryLocationPDP);
exports.GetProductForBrandCarousel = guard.ensure(['include'],getProductForBrandCarousel);
exports.IncludeBrandAccessories = guard.ensure(['include'],includeBrandAccessories);
exports.ShowTopSellingMattresses = guard.ensure(['get'], showTopSellingMattresses);
exports.GetDeliveryLocationMarketLanding = guard.ensure(['get'],getDeliveryLocationMarketLanding);
exports.IncludeGeoLocationHeaderDesktop = guard.ensure(['get'],includeGeoLocationHeaderDesktop);
exports.IncludeGeoLocationHeaderMobile = guard.ensure(['get'],includeGeoLocationHeaderMobile);
exports.GetProductCarouselByProductIDs = guard.ensure(['include'],getProductCarouselByProductIDs);
exports.GetProductForBrandCarouselTile = guard.ensure(['get'], getProductForBrandCarouselTile);
exports.IncludeBrandAccessoriesProducts = guard.ensure(['get'], includeBrandAccessoriesProducts);
exports.GetProductInfoByID = guard.ensure(['get'], getProductInfoByID);
exports.ShowStoreItems = guard.ensure(['get'], showStoreItems);


