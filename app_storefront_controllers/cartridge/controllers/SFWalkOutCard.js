'use strict';

/**
 * Controller 
 *
 * @module controllers/SFWalkOutCard
 */

/* API includes */
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var StringUtils = require('dw/util/StringUtils');
var Transaction = require('dw/system/Transaction');
var Request = require('dw/system/Request');

/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var walkOutCardHelper = require("~/cartridge/scripts/util/SFWalkOutCardHelper").SFWalkOutCardHelper; 

function showForm() 
{
	var params = request.httpParameterMap;
	var StoreNumber : String = params.StoreNumber.stringValue;
	var password : String = params.password.stringValue;
	if(StoreNumber.length > 3){
		app.getView({
			password: password,
			StoreNumber: StoreNumber
		}).render('walkoutcard/SFWalkoutCardForm');
	}
}

function showFormBeta() 
{
	var params = request.httpParameterMap;
	var StoreNumber : String = params.StoreNumber.stringValue;
	var password : String = params.password.stringValue;
	if(StoreNumber.length > 3){
		app.getView({
			dwcBeta: true,
			password: password,
			StoreNumber: StoreNumber
		}).render('walkoutcard/SFWalkoutCardForm');
	}
}

// Recieves the AJAX submission of email address and responds
function submitMessage() {
	var params = request.httpParameterMap;
	var storeID : String = params.storeID.stringValue;
	var associateADID : String = params.associateADID.stringValue;
	var associateName : String = params.associateName.stringValue;
	var associateEmailAddress : String = params.associateEmailAddress.stringValue;
	var guestFirstName : String = params.guestFirstName.stringValue;
	var guestLastName : String = params.guestLastName.stringValue;
	var emailAddress : String = params.emailAddress.stringValue;
	var preferredMethodEmail : String = params.preferredMethodEmail.stringValue;
	var phone : String = params.phone.stringValue;
	var preferredMethodPhone : String = params.preferredMethodPhone.stringValue;
	var shoppingFor : String = params.shoppingFor.stringValue;
	var detailsOffer : String = params.detailsOffer.stringValue;
	var leadSource : String = params.leadSource.stringValue;
	var guestZipCode : String = params.guestZipCode.stringValue;
	var zipCodeModal : String = params.zipCodeModal.stringValue;
	var siteId : String = params.siteId.stringValue;
	var optOutFlag = params.optOutFlag.booleanValue;
	var gclid = params.gclid.stringValue;

	//save e-mail address using lower case
	emailAddress = emailAddress.toLowerCase();
	// REGEX to check email
	if (!/^[a-zA-Z0-9]+(?:(\.|_)[A-Za-z0-9!#$%&'*+/=?^`{|}~-]+)*@(?!([a-zA-Z0-9]*\.[a-zA-Z0-9]*\.[a-zA-Z0-9]*\.))(?:[A-Za-z0-9](?:[a-zA-Z0-9-]*[A-Za-z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/.test(emailAddress)) {
	// email falied validation
	}
	var messageParams = {
			storeID: storeID,
			associateADID: associateADID,
			associateName: associateName,
			associateEmailAddress: associateEmailAddress,
			guestFirstName: guestFirstName,
			guestLastName: guestLastName,
			emailAddress: emailAddress,  
			preferredMethodEmail: preferredMethodEmail,
			phone: phone,
			preferredMethodPhone: preferredMethodPhone,
			shoppingFor: shoppingFor,
			detailsOffer: detailsOffer,
			leadSource: leadSource,
			guestZipCode: guestZipCode, 
			zipCodeModal: zipCodeModal, 
			siteId: siteId, 
			optOutFlag: optOutFlag,
			gclid: gclid,
			dwsid: session.sessionID
			};
	
	var returnResult = walkOutCardHelper.sendSFMessageInfo(messageParams);
	
	
	if (returnResult.Status == 'SERVICE_ERROR'){
		walkOutCardHelper.sendFailSafe(messageParams, returnResult.ErrorCode);
	}
	app.getView({
		JSONResponse: returnResult
	}).render('util/responsejson');
	
}


function processFailSafe() {
	var returnResult = emailHelper.processFailSafe();
	return returnResult;
}



exports.Submit = guard.ensure(['post'], submitMessage);
exports.ProcessFailSafe = processFailSafe;
exports.Form = guard.ensure(['get'], showForm);
exports.FormBeta = guard.ensure(['get'], showFormBeta);

