'use strict';
/**
 * Controller for retrieving a list of available stores with inventory for a specified product.
 *
 * @module controllers/StorePicker
 */

/* API Includes */
var HashMap = require('dw/util/HashMap');
var Countries = require('app_storefront_core/cartridge/scripts/util/Countries');
var StoreMgr = require('dw/catalog/StoreMgr');
var Site = require('dw/system/Site');
var URLUtils = require('dw/web/URLUtils');
var StringUtils = require('dw/util/StringUtils');
var Transaction = require('dw/system/Transaction');
var CustomObjectManager = require('dw/object/CustomObjectMgr');
var Logger = require('dw/system/Logger');
var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var Basket = dw.order.BasketMgr.getCurrentBasket();
var UtilFunctionsMF = require('app_mattressfirm_storefront/cartridge/scripts/util/UtilFunctions').UtilFunctions;
var ATPCacheServices = require("bc_sleepysc/cartridge/scripts/init/service-ATPCacheInventory");
var ATPInventoryServices = require("bc_sleepysc/cartridge/scripts/init/services-ATPInventory");
var ProductUtils = require('app_storefront_core/cartridge/scripts/product/ProductUtils.js');

var ISMattressFirm = dw.system.Site.getCurrent().getID().toLowerCase() === 'mattress-firm' ? true: false;
function start() {
	if (request.httpParameterMap.isCart.value == 'true') {
		var prodID = '';
	} else {
		var prodID = request.httpParameterMap.pid.value;
	}
	if(request.httpParameterMap.v1.value == 'pdp_redesign') {
		app.getView({
		  ContinueURL: URLUtils.http('StorePicker-HandleForm','v1','pdp_redesign'),
		  prodID: request.httpParameterMap.pid.value,
		  isBopis: request.httpParameterMap.isBopis.value
	    }).render('components/storepickup/storepicker_ab');
		
	} else {
		app.getView({
		  ContinueURL: URLUtils.http('StorePicker-HandleForm'),
		  prodID: request.httpParameterMap.pid.value,
		  isBopis: request.httpParameterMap.isBopis.value
	    }).render('components/storepickup/storepicker');
	} 
	
}

function handleForm() {
	var pdp_param_ab = request.httpParameterMap.v1.value;
	var geolocationZip = empty(session.custom.customerZip) ? request.getGeolocation().getPostalCode() : session.custom.customerZip;	
	var postalCode = (empty(session.forms.storepicker.postalCode.value)) ? geolocationZip : session.forms.storepicker.postalCode.value;
	var distance = session.forms.storepicker.maxdistance.value;
	var productID = session.forms.storepicker.productID.value;
	var isBopis = session.forms.storepicker.isBopis.value;
	session.custom.isBopis = isBopis;
	if(!empty(pdp_param_ab) && pdp_param_ab == 'pdp_redesign') {
		findInStore_ab(postalCode, distance, productID, isBopis);
	}else {
		findInStore(postalCode, distance, productID, isBopis);
	}
	
}

function findInStore(customerZip, maxDistance, productID, isBopis) {
	session.custom.customerZip = customerZip;
	session.custom.maxDistance = maxDistance;
	session.custom.isCart = false;
	var storeArray = [];
	var enableBOPIS = ('enableBOPIS' in Site.current.preferences.custom && Site.getCurrent().getCustomPreferenceValue('enableBOPIS')) ? true : false;
	var enableStorePickUp = ('enableStorePickUp' in Site.current.preferences.custom && Site.getCurrent().getCustomPreferenceValue('enableStorePickUp')) ? true : false;
	var storeMap = getNearbyStores(maxDistance);
	if (!empty(productID) && isBopis == 'false') {		
		// convert map to array of stores
		for each( var store in storeMap.entrySet()) {
			storeArray.push(store);
		}
	} 
	/* else if (!empty(productID) && isBopis == 'true') {		
		if (request.httpParameterMap.requestAgain.booleanValue == true) {
			var params=request.httpParameterMap;
			var stores= params.getParameterNames()[1];
			var storeList = JSON.parse(stores);
			storeList = storeList.stores;
			for each (var store in storeMap.entrySet()) {
				if ((storeList.indexOf(store.key.ID) > -1)) {
					continue;
				}
				var requestedDate = new Date();
				requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
				try {
					var requestPacket = '{"InventoryType": "Store","ZipCode": "' + customerZip + '","RequestedDate": "' + requestedDate + '","StoreId": "' + store.key.ID + '","Products": [{"ProductId": "' + productID + '","Quantity": 1}]}';
					var service = ATPInventoryServices.ATPInventoryMonth;
					service.setURL(dw.system.Site.getCurrent().getCustomPreferenceValue('AtpServiceDomain')  + service.URL);
					var result = service.call(requestPacket);
					if (result.error != 0 || result.errorMessage != null || result.mockResult) {
							//return null;
					} else {
						var jsonResponse = JSON.parse(result.object.text);
						session.custom[productID + "-" + store.key.ID] = jsonResponse[0];
					}
				} catch (e) {
					var msg = e;
				}
				storeArray.push(store);
				if (storeArray.length == 4) {
					break;
				}
			}
		} else {
			for each (var store in storeMap.entrySet()) {
				var requestedDate = new Date();
				requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
				try {
					var requestPacket = '{"InventoryType": "Store","ZipCode": "' + customerZip + '","RequestedDate": "' + requestedDate + '","StoreId": "' + store.key.ID + '","Products": [{"ProductId": "' + productID + '","Quantity": 1}]}';
					if (ISMattressFirm) {
						var service =  ATPCacheServices.ATPInventoryMFRM;
					} else {
						var service = ATPInventoryServices.ATPInventory1800;
					}
					var result = service.call(requestPacket);
					if (result.error != 0 || result.errorMessage != null || result.mockResult) {
						//return null;
					} else {
						var jsonResponse = JSON.parse(result.object.text);
						session.custom[productID + "-" + store.key.ID] = jsonResponse[0];
					}
				} catch (e) {
					var msg = e;
				}
				
				storeArray.push(store);
				if (storeArray.length == 5) {
					break;
				}
			}
		}
	} */
	else if (enableBOPIS && enableStorePickUp) { // SHOP-2824 Check Added: to not Call ATp Store Availability For MattressFirm Try in Store
		session.custom.isCart = true;
		storeArray = evaluateCartForBopis(maxDistance);
	}
	
	
	
	app.getView({
		stores:storeArray,
		pid: productID,
		zip: customerZip,
		isBopis : isBopis,
		isCart : session.custom.isCart,
		totalStores : storeMap.length 
	}).render('components/storepickup/storepicker');
}

function findInStore_ab(customerZip, maxDistance, productID, isBopis) {
	session.custom.customerZip = customerZip;
	session.custom.maxDistance = maxDistance;
	session.custom.isCart = false;
	var storeArray = [];
	var storeMap = getNearbyStores(maxDistance);
	if (!empty(productID) && isBopis == 'false') {		
		// convert map to array of stores
		for each( var store in storeMap.entrySet()) {
			storeArray.push(store);
		}
	} else if (!empty(productID) && isBopis == 'true') {		
		if (request.httpParameterMap.requestAgain.booleanValue == true) {
			var params=request.httpParameterMap;
			var stores= params.getParameterNames()[1];
			var storeList = JSON.parse(stores);
			storeList = storeList.stores;
			for each (var store in storeMap.entrySet()) {
				if ((storeList.indexOf(store.key.ID) > -1)) {
					continue;
				}
				var requestedDate = new Date();
				requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
				try {
					var requestPacket = '{"InventoryType": "Store","ZipCode": "' + customerZip + '","RequestedDate": "' + requestedDate + '","StoreId": "' + store.key.ID + '","Products": [{"ProductId": "' + productID + '","Quantity": 1}]}';
					if (ISMattressFirm) {
						var service =  ATPCacheServices.ATPInventoryMFRM;
					} else {
						var service =  ATPInventoryServices.ATPInventory1800;
					}
					var result = service.call(requestPacket);
					if (result.error != 0 || result.errorMessage != null || result.mockResult) {
							//return null;
					} else {
						var jsonResponse = JSON.parse(result.object.text);
						session.custom[productID + "-" + store.key.ID] = jsonResponse[0];
					}
				} catch (e) {
					var msg = e;
				}
				storeArray.push(store);
				if (storeArray.length == 4) {
					break;
				}
			}
		} else {
			for each (var store in storeMap.entrySet()) {
				var requestedDate = new Date();
				requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
				try {
					var requestPacket = '{"InventoryType": "Store","ZipCode": "' + customerZip + '","RequestedDate": "' + requestedDate + '","StoreId": "' + store.key.ID + '","Products": [{"ProductId": "' + productID + '","Quantity": 1}]}';
					if (ISMattressFirm) {
						var service = ATPCacheServices.ATPInventoryMFRM;
					} else {
						var service = ATPInventoryServices.ATPInventory1800;
					}
					var result = service.call(requestPacket);
					if (result.error != 0 || result.errorMessage != null || result.mockResult) {
						//return null;
					} else {
						var jsonResponse = JSON.parse(result.object.text);
						session.custom[productID + "-" + store.key.ID] = jsonResponse[0];
					}
				} catch (e) {
					var msg = e;
				}
				
				storeArray.push(store);
				if (storeArray.length == 5) {
					break;
				}
			}
		}
	} else {
		session.custom.isCart = true;
		storeArray = evaluateCartForBopis(maxDistance);
	}
	
	
	
	app.getView({
		stores:storeArray,
		pid: productID,
		zip: customerZip,
		isBopis : isBopis,
		isCart : session.custom.isCart,
		totalStores : storeMap.length 
	}).render('components/storepickup/storepicker_ab');
}

function getNearbyStores(maxDistance) {
	var customerZip = session.custom.customerZip;
	var countryCode = Countries.getCurrent({
		CurrentRequest: {
				locale: request.locale
			}
		}).countryCode;
	var distanceUnit = (countryCode == 'US') ? 'mi' : 'km';
	var storeMap = StoreMgr.searchStoresByPostalCode(countryCode, customerZip, distanceUnit, maxDistance);	
	return storeMap;
}

function setPreferredStore() {
//	var form = session.forms;
	var customer = session.customer; 
	if (customer.registered && !empty(request.httpParameterMap.storeId.value)) {
		Transaction.wrap(function () {
			customer.profile.custom.preferredStore = request.httpParameterMap.storeId.value;
		});
	} else if (!empty(request.httpParameterMap.storeId.value)) {
		session.custom.preferredStore = request.httpParameterMap.storeId.value;
	}
	
}

function setPreferredStoreAB() {	
	var responseUtils = require('app_storefront_controllers/cartridge/scripts/util/Response');
//	var form = session.forms;
	var customer = session.customer; 
	if (customer.registered && !empty(request.httpParameterMap.storeId.value)) {
		Transaction.wrap(function () {
			customer.profile.custom.preferredStore = request.httpParameterMap.storeId.value;
		});
	} else if (!empty(request.httpParameterMap.storeId.value)) {
		session.custom.preferredStore = request.httpParameterMap.storeId.value;
	}
	
	responseUtils.renderJSON({        
        SetCustomerPreferredStore: true         
    });
	
}

function setPreferredStoreFromGeoLocation() {
	var storeMap = getNearbyStores(100);
	for each( var store in storeMap.entrySet()) {
		session.custom.preferredStore = store.key.ID;
		break;
	}
}

function getPreferredStoreAddress() {
	var store = getPreferredStore();
	var prodId = request.httpParameterMap.pid ? request.httpParameterMap.pid : false;
	var ProductMgr = require('dw/catalog/ProductMgr');
	var product = ProductMgr.getProduct(prodId.value);
	var inventoryParam = product.isVariant() ? product.masterProduct.ID : product.ID;
	
	var isBopis = request.httpParameterMap.isBopis ? request.httpParameterMap.isBopis.booleanValue : false;
	var isInventoryList = false;
	if (store != null && isBopis == false) {
		var inventory = store.inventoryList;
		if (!empty(inventory)) {
			try {
				var record = inventory.getRecord(inventoryParam.replace('mfi',''));
			} catch (e) {
				var error = e;
			}
			if (!empty(record)) {
				isInventoryList = true;
			}
		}
	} else if (store != null && isBopis == true) {
		var requestedDate = new Date();
		requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
		var products = new Array();
		products.push({
			"ProductId" : prodId.value,
			"Quantity" : 1
		});
		try {
			var requestPacket = '{"InventoryType": "Store","ZipCode": "' + store.postalCode + '","RequestedDate": "' + requestedDate + '","StoreId": "' + store.ID + '","Products": ' + JSON.stringify(products) + '}';
			if (ISMattressFirm) {
				var service = ATPCacheServices.ATPInventoryMFRM;
			} else {
				var service = ATPInventoryServices.ATPInventory1800;
			}
			var result = service.call(requestPacket);
			if (result.error != 0 || result.errorMessage != null || result.mockResult) {
				
			} else {
				var jsonResponse = JSON.parse(result.object.text);
				var response = jsonResponse[0];
				if (response.Available.toLowerCase() == 'yes' && response.ATPQuantity > 0) {
					session.custom[prodId.value + '-' + store.ID] = response;
					isInventoryList = true;
				}
			}
		} catch (e) {
			var error = e;
		}
	}
	
	if(request.httpParameterMap.v1.value == 'pdp_redesign') {
		app.getView({
		 preferredStore: store,
		 pid: prodId,
		 isInventoryList: isInventoryList
	    }).render('product/preferredStore_ab');
	} else {
		app.getView({
		 preferredStore: store,
		 pid: prodId,
		 isInventoryList: isInventoryList
	    }).render('product/preferredStore');
	}	
}

function getPreferredStore() {
	var store = null;
	var storeIdValue =  request.httpParameterMap.storeId;
	if (storeIdValue != null && storeIdValue != "") {
		if(storeIdValue.toString().length < 6) {
			var missingLeadingZero = Number(6 - storeIdValue.toString().length);
			for(var leadingZero = 0; leadingZero < missingLeadingZero; leadingZero++) {
				storeIdValue = '0'+storeIdValue.toString();
			}
			
		}
		if (session.customer.registered) {
			store = StoreMgr.getStore(storeIdValue);			
		} else if (StoreMgr.getStore(storeIdValue)) {
			store = StoreMgr.getStore(storeIdValue);
		}
		
		if(store == null || store == "") {
			if (session.customer.registered && !empty(session.customer.profile.custom.preferredStore)) {
				store = StoreMgr.getStore(session.customer.profile.custom.preferredStore);
			} else if (StoreMgr.getStore(session.custom.preferredStore)) {
				store = StoreMgr.getStore(session.custom.preferredStore);
			}
		}
	}
	else {
		if (session.customer.registered && !empty(session.customer.profile.custom.preferredStore)) {
			store = StoreMgr.getStore(session.customer.profile.custom.preferredStore);
		} else if (StoreMgr.getStore(session.custom.preferredStore)) {
			store = StoreMgr.getStore(session.custom.preferredStore);
		}
	}
	
	
	
	return store;
}

function updateBopisOption(data) {
	var params = request.httpParameterMap;
	var deliveryoption = params.deliveryoption.stringValue;
	session.custom.deliveryOptionChoice = deliveryoption;
}

function evaluateCartForBopis(maxDistance) {
	if (typeof maxDistance == 'undefined') {
		maxDistance = 100;
	}
	var storeMap = getNearbyStores(maxDistance);
	var storeArray = [];
	for (var i = 0; i < storeMap.entrySet().length; i++) {
		var store = storeMap.entrySet()[i];
		var hasProducts = storeHasProducts(store, true);
		storeArray.push(store);
		if (storeArray.length == 5) {
			break;
		}
	}
	return storeArray;
}
function getAvailablePickupStore() {
	var preferredStore = getPreferredStore();	
	// IF a preferred store is already selected, check if products are all available in preferred store
	if (preferredStore != null) {
		var hasProducts = storeHasProducts(preferredStore);
		if (hasProducts) {
			return preferredStore;
		} else {
			return null;
		}
	}
	return null;
}

/** This function accepts a store and the line item container
 * it checks availability of each product line item in the store and returns
 * true only if all products are available.
 * @param store
 * @param isCart
 * @returns boolean
 */
function storeHasProducts(store, isCart) {
	if (empty(Basket)) {
		return false;
	}
	var plis = Basket.productLineItems;
	var storeID = (store instanceof dw.util.MapEntry) ? store.key.ID : store.ID;
	var storePostalCode = (store instanceof dw.util.MapEntry) ? store.key.postalCode : store.postalCode;
	var products = new Array();
	session.custom.wasbopis = (session.custom.wasbopis != null) ? session.custom.wasbopis : 0;
	for (var i = 0; i < plis.length; i++ ) {
		var pli = plis[i];
		/* if (empty(pli.custom.storePickupStoreID) && pli.custom.storePickupStoreID != storeID) {
			Transaction.wrap( function() {
				pli.custom.storePickupStoreID = storeID;
			});
		} */
		products.push({
			'ProductId' : pli.productID,
			'Quantity' : pli.quantityValue
		});
		var optionLineItems = pli.optionProductLineItems;
		for (var j = 0; j < optionLineItems.length; j++) {
			var oli = optionLineItems[j];
			if (oli.productID != 'none') {
				products.push({
					'ProductId' : oli.productID,
					'Quantity' : oli.quantityValue
				});
			}
		}
	}
	
	if (products.length > 0) {
		var requestedDate = new Date();
		requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
		try {
			var requestPacket = '{"InventoryType": "Store","ZipCode": "' + storePostalCode + '","RequestedDate": "' + requestedDate + '","StoreId": "' + storeID + '","Products": ' + JSON.stringify(products) + '}';
            if (ISMattressFirm) {
            	// SHOP-2824 In case of MattressFirm we dont make an ATP call for ATP Store.
            	return false;
            } else {
            	var service = ATPInventoryServices.ATPInventory1800;
            }
			var result = service.call(requestPacket);
			if (result.error != 0 || result.errorMessage != null || result.mockResult) {
				return false;
			} else {
				var jsonResponse = JSON.parse(result.object.text);
				var response = jsonResponse[0];
				for (var i = 0; i < plis.length; i++) {
					var pli = plis[i];
					session.custom[pli.productID + '-' + storeID] = response;
					/* if (pli.custom.storePickupStoreID != storeID) {
						Transaction.wrap( function() {
							pli.custom.storePickupStoreID = response.Location1;
							pli.custom.deliveryDate = response.SlotDate;
						});
					} */
				}
				if (isCart == true) {
					session.custom['storeId' + storeID] = response;
				}
			}
			return true;
		} catch (e) {
			var error = e;
		}
	}
	return false;
}
function updatePreferredStoreLineItems() {
	if (!empty(Basket)) {
		var plis = Basket.productLineItems;
	}
	
	
}

/**
 * resets the shipping options to Ship To Address and calls {@link module:controllers/COShipping~start|start} function.
 *
 * @transactional
 */
function changeDeliveryToShipAddress() {
	session.forms.singleshipping.clearFormElement();
	app.getController('Cart').ChangeDeliveryOption('shipped');
	session.custom.deliveryOptionChoice = 'shipped';
	app.getController('COShipping').Start();
}

function confirmATPAvailablity() {
	var params = request.httpParameterMap;
	var deliveryDateExists = true;
	var enableAtpDeliverySchedule =  dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule');
    var successFlag = true;
    
    if(enableAtpDeliverySchedule){
    	
    	try{
	    	var cart = app.getModel('Cart').get();
	    	var hasRedCarpetItemsInCart = !empty(cart.object) ? mattressPipeletHelper.getOrderIsRedCarpetStatus(cart.object) : false;
	   
	    	if(hasRedCarpetItemsInCart){
	    		var postalCode = session.forms.singleshipping.shippingAddress.addressFields.postal.value;
	        	
		    	var deliveryDatesArr = getATPDeliveryDates(cart, postalCode);
				var inMarketShipment = cart.getShipment('In-Market');
		    	
				if (inMarketShipment && !empty(deliveryDatesArr)) {
		    		var deliveryDate = inMarketShipment.custom.deliveryDate;		    		
		    		
		    		if(!empty(deliveryDate)){
		    			var dtdeliveryDate = new Date(deliveryDate);
		    			
		    			for(var i = 0; i < deliveryDatesArr.length; i++ ){		    				
		    				var slotDate = deliveryDatesArr.get(i);
		    			
		    				if(!empty(slotDate) && !empty(slotDate.toDateString()) ){
		    					
		    					var selectedDeliveryDateKey = dtdeliveryDate.toDateString().replace(' ', '', 'g');
	    						var slotDateKey = slotDate.toDateString().replace(' ', '', 'g');
	    						var deliveryTimeExists = false;
	    						// Compare dates first
		    					if(selectedDeliveryDateKey.equals(slotDateKey) ){
		    						var deliveryTime = inMarketShipment.custom.deliveryTime;
		    						
			    					if (!empty(session.custom.deliveryDates) && !empty(deliveryTime)) {
			    						// Compare Slot Time with current select Time
		    							var deliveryTimeSlots = session.custom.deliveryDates[slotDateKey]['timeslots'];		    								    						
		    							if (!empty(deliveryTimeSlots)) {
				    						for (var j = 0; j < deliveryTimeSlots.length; j++) {  						
				    							if (deliveryTimeSlots[j]['available'] == 'yes' && deliveryTimeSlots[j]['slots'] > 0) {
				    								var startTime = deliveryTimeSlots[j]['startTime'];
				    								var endTime = deliveryTimeSlots[j]['endTime'];
				    								var slotTimeKey = StringUtils.formatDate(startTime, "h:mma").toString() +'-' + StringUtils.formatDate(endTime, "h:mma").toString();
				    								if (deliveryTime.equalsIgnoreCase(slotTimeKey)) {						
				    									deliveryTimeExists = true;
				    									break;
				    								}			
				    							}
				    						}
		    							}
			    					}
			    					// Both Date and Time keys matched
		    						if(deliveryTimeExists){
		    							deliveryDateExists = true;
		    							break;
		    						}else{
		    							deliveryTimeExists = false;
		    						}
		    						
		    					}else {    						
		    						deliveryDateExists = false;
		    					}
		    				}
		    			}
		    		}
		    	}
	    	}
    	}catch(e){
    		var errMsg = e.message;
    		deliveryDateExists = true; // error during ATP confirmation but order Creation must go through
    		successFlag = false;
    		Logger.error('Error in ATP Slots Availablity confirmation:  '+ errMsg);
    	}
    }	
	
	if (request.httpParameterMap.format.stringValue === 'ajax') {
		let r = require('app_storefront_controllers/cartridge/scripts/util/Response');
        r.renderJSON({           
    		showATPCalendar: !deliveryDateExists,
    		success: successFlag
        });
    }
}


function confirmATPAvailablityAmazon() {
	var params = request.httpParameterMap;
	var paramsDeliveryDate = params.deliveryDateData.value.replace(' ', '', 'g');
	var paramsDeliveryTimeData = params.deliveryTimeData.value.replace(' ', '', 'g');
	
    var deliveryDateExists = true;
	var enableAtpDeliverySchedule =  dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule');
    var successFlag = true;
    
    if(enableAtpDeliverySchedule){
    	
    	try{
	    	var cart = app.getModel('Cart').get();
	    	var hasRedCarpetItemsInCart = !empty(cart.object) ? mattressPipeletHelper.getOrderIsRedCarpetStatus(cart.object) : false;
	   
	    	if(hasRedCarpetItemsInCart){
	    		var postalCode = session.forms.singleshipping.shippingAddress.addressFields.postal.value;
	        	
		    	var deliveryDatesArr = getATPDeliveryDates(cart, postalCode);
				var inMarketShipment = cart.getShipment('In-Market');
		    	
				if (inMarketShipment && !empty(deliveryDatesArr)) {
		    		for(var i = 0; i < deliveryDatesArr.length; i++ ){		    				
		    			var slotDate = deliveryDatesArr.get(i);
		    			if(!empty(slotDate) && !empty(slotDate.toDateString()) ){
		    				var selectedDeliveryDateKey = paramsDeliveryDate;//dtdeliveryDate.toDateString().replace(' ', '', 'g');
	    					var slotDateKey = slotDate.toDateString().replace(' ', '', 'g');
	    					var deliveryTimeExists = false;
	    					// Compare dates first
		    				if(selectedDeliveryDateKey.equals(slotDateKey) ){
		    						var deliveryTime = paramsDeliveryTimeData;//inMarketShipment.custom.deliveryTime;
		    						
			    					if (!empty(session.custom.deliveryDates) && !empty(deliveryTime)) {
			    						// Compare Slot Time with current select Time
		    							var deliveryTimeSlots = session.custom.deliveryDates[slotDateKey]['timeslots'];		    								    						
		    							if (!empty(deliveryTimeSlots)) {
				    						for (var j = 0; j < deliveryTimeSlots.length; j++) {  						
				    							if (deliveryTimeSlots[j]['available'] == 'yes' && deliveryTimeSlots[j]['slots'] > 0) {
				    								var startTime = deliveryTimeSlots[j]['startTime'];
				    								var endTime = deliveryTimeSlots[j]['endTime'];
				    								var slotTimeKey = StringUtils.formatDate(startTime, "h:mma").toString() +'-' + StringUtils.formatDate(endTime, "h:mma").toString();
				    								if (deliveryTime.equalsIgnoreCase(slotTimeKey)) {						
				    									deliveryTimeExists = true;
				    									break;
				    								}			
				    							}
				    						}
		    							}
			    					}
			    					// Both Date and Time keys matched
		    						if(deliveryTimeExists){
		    							deliveryDateExists = true;
		    							break;
		    						}else{
		    							deliveryTimeExists = false;
		    						}
		    						
		    				}else {    						
		    						deliveryDateExists = false;
		    				}
		    			}
		    		}
		    	}
	    	}
    	}catch(e){
    		var errMsg = e.message;
    		deliveryDateExists = true; // error during ATP confirmation but order Creation must go through
    		successFlag = false;
    		Logger.error('Error in ATP Slots Availablity confirmation:  '+ errMsg);
    	}
    }	
	
	if (request.httpParameterMap.format.stringValue === 'ajax') {
		let r = require('app_storefront_controllers/cartridge/scripts/util/Response');
        r.renderJSON({           
    		showATPCalendar: !deliveryDateExists,
    		success: successFlag
        });
    }
}

function getATPDeliveryDatesForNextWeek() {
	var params = request.httpParameterMap;
	var cart = app.getModel('Cart').get();
	var postalCode = session.forms.singleshipping.shippingAddress.addressFields.postal.value;
	var format = params.format.stringValue;
	var date = params.currentWeekDate.stringValue;
	var deliveryDatesArr = getATPDeliveryDates(cart, postalCode, date);
	
	app.getView({
		DeliveryDatesArr: deliveryDatesArr
	}).render('checkout/components/delivery-weekly-schedule-mocked');
}

/** This function accepts products array and the postal code
 * it makes an array of core products along with postalCode at checkout to trigger
 * utag link whenever atp fails
 * @param products (shipment type 'core' only)
 * @param postcalCode
 * @returns array
 */
function makeATPFailedCoreProductsArray(products, postalCode) {
	var atpFailedCoreProductsArray = new Array();
	for (var i = 0; i < products.length; i++) {
		var coreProduct = products[i];
		atpFailedCoreProductsArray.push("Checkout_" + coreProduct.ProductId +"_" + postalCode);
	}
	return atpFailedCoreProductsArray;
}

function getATPDeliveryDates(cart, postalCode, date) {
	session.custom.atpFailedCoreProductsArray = null;
	var products = new Array();
	var deliveryDatesArr = new dw.util.ArrayList();
	if (empty(Basket)) {
		return deliveryDatesArr;
	}
	var plis = Basket.productLineItems;
	for (var i = 0; i < plis.length; i++ ) {
		var pli = plis[i];
		if (!empty(pli.product.custom.shippingInformation) && pli.product.custom.shippingInformation.toLowerCase() == 'core' && !UtilFunctionsMF.isParcelableAndCore(pli.product.manufacturerSKU)) {
			products.push({
				'ProductId' : pli.productID,
				'Quantity' : pli.quantityValue
			});
			var optionLineItems = pli.optionProductLineItems;
			for (var j = 0; j < optionLineItems.length; j++) {
				var oli = optionLineItems[j];
				if (oli.productID != 'none') {
					products.push({
						'ProductId' : oli.productID,
						'Quantity' : oli.quantityValue
					});
				}
			}
		}
	}
	var enableATPDeliveryCalendar = ('enableATPDeliveryCalendar' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.enableATPDeliveryCalendar);
	var enableCheckoutCache 	  = ('enableATPCacheOnCheckout' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.enableATPCacheOnCheckout);
	
	try {
			if (products.length > 0)
			{
				if (arguments.length > 2 && !empty(arguments[2])) {
					var requestedDate = new Date(arguments[2]);
					requestedDate.setDate(requestedDate.getDate() + 7);
					requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
				} else {
					var requestedDate = new Date();
					requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
				}
				var requestAtpAgain = true,
					requestCount = 0;
					do {
					
						if ('enableFutureAtpDeliverySchedule' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.enableFutureAtpDeliverySchedule) {			
							var noOfWeeksData = 'numberOfWeeksDeliveryScheduler' in dw.system.Site.current.preferences.custom ? dw.system.Site.current.preferences.custom.numberOfWeeksDeliveryScheduler : 6;
							var requestPacket = '{"InventoryType": "Delivery","ZipCode": "' + postalCode + '","RequestedDate": "' + requestedDate + '","StoreId": "","Products": ' + JSON.stringify(products) + ', "Weeks":' + noOfWeeksData + '}';
	
						} else {
							var requestPacket = '{"InventoryType": "Delivery","ZipCode": "' + postalCode + '","RequestedDate": "' + requestedDate + '","StoreId": "","Products": ' + JSON.stringify(products) + '}';	
						}
						var service = null;

						if (enableCheckoutCache){
							service = ATPCacheServices.ATPInventoryDynamic;
						} else {
							if (ISMattressFirm) {
								service = ATPCacheServices.ATPInventoryMFRM;
							} else {
								service = ATPInventoryServices.ATPInventory1800;
							}
						}
						var result = service.call(requestPacket);
						requestCount++;
						if (result.error != 0 || result.errorMessage != null || result.mockResult) {
							session.custom.atpFailedCoreProductsArray = makeATPFailedCoreProductsArray(products, postalCode);
						} else {
							var jsonResponse = JSON.parse(result.object.text);
							if (arguments.length > 2 && !empty(arguments[2])) {
								var deliveryDateSession = session.custom.deliveryDates;
							} else {
								var deliveryDateSession = new Object();
							}
							if (enableATPDeliveryCalendar) {
								var todaysDate = new Date();								
								for (var i = 0; i < jsonResponse.length; i++ ) {
					                 var response = jsonResponse[i];
					                 var deliveryDate = new Date(response.Date);
					                 deliveryDate.setHours(10);		                 
					                 var key = deliveryDate.toDateString().replace(' ', '', 'g');
					                 if (!deliveryDateSession[key]) {
					                     deliveryDateSession[key] = new Object();
					                     deliveryDateSession[key]['Available'] = !empty(response.AvailableSlots) ? response.AvailableSlots : 'NO';
					                     var ATPSlots = [];
					                     for each(var slot in response.ATPSlots) {
					                    	 if (!empty(slot)) {
					 							var startTime = new Date(deliveryDate.toDateString() + ' ' + slot.StartTime);
												var endTime = new Date(deliveryDate.toDateString() + ' ' + slot.EndTime);
					                    		 ATPSlots.push({
						                    			'SlotDate'  : slot.SlotDate,
						                    			'location1' : slot.Location1,
														'location2' : slot.location2,
														'mfiDSZoneLineId' : slot.Zone,
														'mfiDSZipCode' : postalCode,
														'available' : slot.Available.toLowerCase(),
														'startTime' : startTime,
														'endTime' : endTime,
														'slots' : Number(slot.Slots)
					                    		 });
					                    	 }
					                     }
					                     deliveryDateSession[key]['timeslots'] = ATPSlots;
					                     // in case we dont have any slots from atp for every date then array should be empty
					                     if (!empty(response.ATPSlots) && response.ATPSlots.length > 0 && deliveryDate.toDateString() != todaysDate.toDateString()) {
						                     deliveryDatesArr.add(deliveryDate); 
					                     }
					                 }
					                     if (requestAtpAgain === true) {
					                         requestAtpAgain = false;
					                     }
					             }
							} else {
								var checkForDuplicates = new dw.util.ArrayList();
								for (var i = 0; i < jsonResponse.length; i++ ) {
									var response = jsonResponse[i];
									var deliveryDate = new Date(response.SlotDate);
									deliveryDate.setHours(10);
									
									var key = deliveryDate.toDateString().replace(' ', '', 'g');
									if (!deliveryDateSession[key]) {
										deliveryDateSession[key] = new Object();
										deliveryDateSession[key]['timeslots'] = new dw.util.ArrayList();
										deliveryDatesArr.add(deliveryDate);
									}
									
									var startTime = new Date(deliveryDate.toDateString() + ' ' + response.StartTime);
									var endTime = new Date(deliveryDate.toDateString() + ' ' + response.EndTime);
									if (checkForDuplicates.indexOf(key + startTime + endTime) == -1 && response.Available.toLowerCase() == 'yes' && response.Slots > 0) {
										checkForDuplicates.add(key + startTime + endTime);
										deliveryDateSession[key]['timeslots'].add({
											'location1' : response.Location1,
											'location2' : response.location2,
											'mfiDSZoneLineId' : response.Zone,
											'mfiDSZipCode' : postalCode,
											'available' : response.Available.toLowerCase(),
											'startTime' : startTime,
											'endTime' : endTime,
											'slots' : Number(response.Slots)
										});
										
										var timeslotsComparator = new dw.util.PropertyComparator ('startTime', 'endTime');
										deliveryDateSession[key]['timeslots'].sort(timeslotsComparator);
										
										if (requestAtpAgain === true && response.Available.toLowerCase() == 'yes' && response.SlotDate !== null) {
											requestAtpAgain = false;
										}
									}
								}
							}
							session.custom.deliveryDates = deliveryDateSession;
							Logger.error('DeliverSchedulaer Object: '+ JSON.stringify(deliveryDateSession) + Object.keys(deliveryDateSession).length);
						}
					if (requestAtpAgain === true) {
						deliveryDatesArr = new dw.util.ArrayList();
						requestedDate = new Date(requestedDate);
						requestedDate.setDate(requestedDate.getDate() + 7);
						requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
					}
				} while (requestAtpAgain === true && requestCount < 2);
			}
			
		} catch (e) {
			var error = e;
		}			
	return deliveryDatesArr;
}
/** This function return a template containing selected slots for desktop delivery info page
 * @param slot key query params to get selected slot data.
 * @returns template
 */
function getSelectedATPSlots() {
	var cart = app.getModel('Cart').get();
	var params = request.httpParameterMap;
	if(params.slotKey.submitted) {
		var key = params.slotKey.stringValue;
		var selectedATPDate = !empty(session.custom.deliveryDates)?session.custom.deliveryDates[key] : null;
		var selectedDateAvailableSlots = new dw.util.ArrayList();
		if(!empty(selectedATPDate)) {
			for (var i = 0; i < selectedATPDate.timeslots.length; i++ ) {
				if(selectedATPDate.timeslots[i].available.toUpperCase() == "YES") {
					selectedDateAvailableSlots.add(selectedATPDate.timeslots[i]);
				}	
			}
		}
		if(!selectedDateAvailableSlots.empty && selectedDateAvailableSlots[0].available.toLowerCase() == 'yes') {
			session.custom.deliveryZone = selectedDateAvailableSlots[0].mfiDSZoneLineId;
		}
		
		app.getView({
			selectedDateAvailableSlots: selectedDateAvailableSlots,
			slotkey: key
		}).render('checkout/components/delivery-calendar-selected-date');
	}
}
/** This function return a template for mobile delivery selection calander.
 * @param slot key query params to set selected slot.
 * @returns template
 */
function getMobileCalander() {
	var params = request.httpParameterMap;
	var key = params.slotKey.submitted ? params.slotKey.stringValue : "";
	var cart = app.getModel('Cart').get();
	var postalCode = session.custom.customerZip;
	var deliveryDatesArr = getATPDeliveryDates(cart, postalCode);
	session.custom.deliveryDatesArr = deliveryDatesArr;
	
	app.getView({
		selectedSlotkey: key,
		DeliveryDatesArr : deliveryDatesArr
	}).render('checkout/components/mob-delivery-calendar');
}


//get delivery dates 
function getDeliveryZone(cart, postalCode, date) {
 //session.custom.customerZip = postalCode;
 var products = new Array();
 var deliveryDatesArr = new dw.util.ArrayList();
 var plis = Basket.productLineItems;
	for (var i = 0; i < plis.length; i++ ) {
		var pli = plis[i];
		if (!empty(pli.product.custom.shippingInformation) && pli.product.custom.shippingInformation.toLowerCase() == 'core') {
			products.push({
				'ProductId' : pli.productID,
				'Quantity' : pli.quantityValue
			});
			var optionLineItems = pli.optionProductLineItems;
			for (var j = 0; j < optionLineItems.length; j++) {
				var oli = optionLineItems[j];
				if (oli.productID != 'none') {
					products.push({
						'ProductId' : oli.productID,
						'Quantity' : oli.quantityValue
					});
				}
			}
		}
	}
 try {
     
     var requestedDate = new Date(date);
     requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
     var requestAtpAgain = true,
         requestCount = 0;
     do {
         var requestPacket = '{"InventoryType": "Delivery","ZipCode": "' + postalCode + '","RequestedDate": "' + requestedDate + '","StoreId": "","Products": ' + JSON.stringify(products) + '}';
         if (ISMattressFirm) {
			var service = ATPCacheServices.ATPInventoryMFRM;
		} else {
			var service = ATPInventoryServices.ATPInventory1800;
		}
         var result = service.call(requestPacket);
         requestCount++;
         if (result.error != 0 || result.errorMessage != null || result.mockResult) {
             
         } else {
             var jsonResponse = JSON.parse(result.object.text);
             var deliveryDateSession = new Object();
             
             var checkForDuplicates = new dw.util.ArrayList();
             for (var i = 0; i < jsonResponse.length; i++ ) {
                 var response = jsonResponse[i];
                 var deliveryDate = new Date(response.SlotDate);
                 deliveryDate.setHours(10);
                 
                 var key = deliveryDate.toDateString().replace(' ', '', 'g');
                 if (!deliveryDateSession[key]) {
                     deliveryDateSession[key] = new Object();
                     deliveryDateSession[key]['timeslots'] = new dw.util.ArrayList();
                     deliveryDatesArr.add(deliveryDate);
                 }
                 
                 var startTime = new Date(deliveryDate.toDateString() + ' ' + response.StartTime);
                 var endTime = new Date(deliveryDate.toDateString() + ' ' + response.EndTime);
                 if (checkForDuplicates.indexOf(key + startTime + endTime) == -1) {
                     checkForDuplicates.add(key + startTime + endTime);
                     deliveryDateSession[key]['timeslots'].add({
                         'location1' : response.Location1,
                         'location2' : response.location2,
                         'mfiDSZoneLineId' : response.Zone,
                         'mfiDSZipCode' : postalCode,
                         'available' : response.Available.toLowerCase(),
                         'startTime' : startTime,
                         'endTime' : endTime,
                         'slots' : Number(response.Slots)
                     });
                     
                     var timeslotsComparator = new dw.util.PropertyComparator ('startTime', 'endTime');
                     deliveryDateSession[key]['timeslots'].sort(timeslotsComparator);
                     
                     if (requestAtpAgain === true && response.Available.toLowerCase() == 'yes' && response.SlotDate !== null) {
                         requestAtpAgain = false;
                     }
                 }
             }
             session.custom.deliveryDates = deliveryDateSession;
         }
         
     } while (requestAtpAgain === true && requestCount < 1); 
     
 } catch (e) {
     var error = e;
 }
 return deliveryDatesArr;
}


/*
	Takes as Input a Date Object and a Time string in format 'hh:mm PM' e.g. '01:25 PM'
	Returns as Output a Date Object with the Hours and Minutes set according to the Time string in Date object
*/
function getDateFromDateAndTimeString(dateObj, timeStr) {
	var test, parts, hours, minutes, date,
	d = dateObj.getTime(),
	timeReg = /(\d+)\:(\d+) (\w+)/;
	test = timeStr;
	parts = test.match(timeReg);
	hours = /am/i.test(parts[3]) ?
		function(am) {return am < 12 ? am : 0}(parseInt(parts[1], 10)) :
		function(pm) {return pm < 12 ? pm + 12 : 12}(parseInt(parts[1], 10));
	minutes = parseInt(parts[2], 10);
	date = new Date(d);
	date.setHours(hours);
	date.setMinutes(minutes);
	return date;
}

function calendarToUTCDate(calendar) {
	return Date.UTC(calendar.get(calendar.YEAR), 
					calendar.get(calendar.MONTH), 
					calendar.get(calendar.DAY_OF_MONTH), 
					calendar.get(calendar.HOUR_OF_DAY), 
					calendar.get(calendar.MINUTE), 
					calendar.get(calendar.SECOND));
}

function calendarToUTCDateWithoutTime(calendar) {
	return Date.UTC(calendar.get(calendar.YEAR), 
					calendar.get(calendar.MONTH), 
					calendar.get(calendar.DAY_OF_MONTH));
}

function dateToUTCDate(date) {
	return Date.UTC(date.getFullYear(), 
					date.getMonth(), 
					date.getDate(), 
					date.getHours(), 
					date.getMinutes(), 
					date.getSeconds());	
}

function dateToUTCDateWithoutTime(date) {
	return Date.UTC(date.getFullYear(), 
					date.getMonth(), 
					date.getDate());	
}

function getMaxAvailabilityMessage() {
	var params = request.httpParameterMap;
	var productIds = params.productIds.stringValue.split(',');
	var type = params.shippingType.stringValue;
	var maxAvailabilityMessage;
	var prevStartDate, prevEndDate;

	for(let i=0; i< productIds.length; i++) {
		if(productIds[i] === "") continue;

		var prodObj = app.getModel('Product').get("mfi" + productIds[i]).object;

		var skip = true;
		if(prodObj !== null) {			
			switch(type) {
				//all shipping types
				case "0":
					skip = false;
					break;
				//shipping type parcel and drop ship
				case "1":
					skip = !(UtilFunctionsMF.isDropShip(productIds[i]) || UtilFunctionsMF.isParcelableAndCore(productIds[i]));
					break;
				//shipping type drop ship
				case "2":
					skip = !UtilFunctionsMF.isDropShip(productIds[i]);
					break;
				//shipping parcel
				case "3":
					skip = !UtilFunctionsMF.isParcelableAndCore(productIds[i]);
					break;
				default:
					break;
			}
		}

		if(skip) continue;

		var product = prodObj.isVariant() ? prodObj.masterProduct : prodObj;
		var messageType = product.availabilityModel.inventoryRecord.custom.messageType;
		var availabilityMsgObj = CustomObjectManager.getCustomObject('AvailabilityMessage', messageType);
		var availabilityMsg = availabilityMsgObj.custom.cartMessage;
		var curDays = availabilityMsg.match(/\d+/g).map(function(x) {
			return parseInt(x);
		});

		if(typeof prevStartDate == "undefined" || typeof prevEndDate == "undefined")
		{
			prevStartDate = curDays[0];
			prevEndDate = curDays[1];
			maxAvailabilityMessage = availabilityMsg;
		} 
		
		if(prevStartDate < curDays[0] || prevEndDate < curDays[1]) {
			prevStartDate = curDays[0];
			prevEndDate = curDays[1];
			maxAvailabilityMessage = availabilityMsg;
		}
	}

	if (typeof maxAvailabilityMessage !== "undefined") {
		jsonObj = JSON.stringify({ availabilityMessage: maxAvailabilityMessage });
		app.getView({
			JSONData: jsonObj,
		}).render('util/output');
	} else {
		jsonObj = '{"error":"Availability message not found. "}';
			app.getView({
				JSONData: jsonObj,
			}).render('util/output');	
	}
}


/*
	Input
	    Month:        Number,    Range 0-11,    e.g. 2 = March
	    Instance:    Number,    Range 1-4,    e.g. 2 = 2nd Sunday 
	Output:
	    Date:        Date,    Return the date object as "Sun Mar dd 02:00:00 GMT yyyy"
*/

function getDateOfAnInstanceOfSundayOfSPecificMonth(month, instance) {
	var date = new Date();
	date.setMonth(month);
	date.setDate(7);
	var requiredSunday = 7*(instance-1) + (7 - date.getDay());
	date.setDate(requiredSunday);
	date.setHours(2,0,0);    // 02:00 AM
	return date; 
}

function isDaylightSavingONfromDate(date) {
	var secondSundayOfMarch = dateToUTCDate(getDateOfAnInstanceOfSundayOfSPecificMonth(2,2));
	var firstSundayOfNovember = dateToUTCDate(getDateOfAnInstanceOfSundayOfSPecificMonth(10,1));
	var todayUTC = dateToUTCDate(date);
	if (secondSundayOfMarch < todayUTC && todayUTC < firstSundayOfNovember)
	    return true;
	return false;    
}

function isDaylightSavingONfromCalendar(calendar) {
	var secondSundayOfMarch = dateToUTCDate(getDateOfAnInstanceOfSundayOfSPecificMonth(2,2));
	var firstSundayOfNovember = dateToUTCDate(getDateOfAnInstanceOfSundayOfSPecificMonth(10,1));
	var todayUTC = calendarToUTCDate(calendar);
	if (secondSundayOfMarch < todayUTC && todayUTC < firstSundayOfNovember)
	    return true;
	return false;    
}

function getATPAvailabilityMessage() {
	var params = request.httpParameterMap,
		productId = params.productId.stringValue,
		qty : Number = params.qty.intValue,
		postalCode = session.custom.customerZip,
		optionId = params.optionId.stringValue ? params.optionId.stringValue : 'none',
		requestedDate = new Date(),
		todaysDate = new Date(),
		products = new Array(),
		availabilityDate = null,
		availabilityClass = 'in-stock-msg',
		dateDifferenceString = '',
		isInStock = false,
		product = app.getModel('Product').get(productId),
		isATPSuccess = true;
	
	var context = (!empty(params.atpContext) &&  !empty(params.atpContext.stringValue) ) ? params.atpContext.stringValue : "" ;
		
	if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpAvailabilityMessage') && postalCode) {
		
		if (!session.custom.ATPAvailability) {
			session.custom.ATPAvailability = new Object();
		}
		
		if (!empty(Basket)) {
			var plis = Basket.productLineItems;
			for (var i = 0; i < plis.length; i++ ) {
				var pli = plis[i];
				if (productId == pli.productID) {
					qty = qty + pli.quantityValue;
				}
			}
		}
		
		products.push({
			'ProductId' : productId,
			'Quantity' : qty
		});
		
		if (optionId && optionId != 'none') {
			products.push({
				'ProductId' : optionId,
				'Quantity' : qty
			});
		}
		
		var enablePDPCache = dw.system.Site.getCurrent().getCustomPreferenceValue('enableATPCacheOnPDP');
		var service = null;
		if (ISMattressFirm) {
			if (enablePDPCache && context == "product"){
				service = ATPCacheServices.ATPInventoryMFRM;
			}
		}
		else {
			service = ATPInventoryServices.ATPInventory1800;
		}
		
		requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
		var requestPacket = '{"InventoryType": "Delivery","ZipCode": "' + postalCode + '","RequestedDate": "' + requestedDate + '","StoreId": "","Products": ' + JSON.stringify(products) + '}';
		
		var result = service.call(requestPacket);
		if (result.error != 0 || result.errorMessage != null || result.mockResult) {
			isATPSuccess = false;
		} else {
			var jsonResponse = JSON.parse(result.object.text);
			for (var i = 0; i < jsonResponse.length; i++ ) {
				var response = jsonResponse[i];
				var deliveryDate = new Date(response.SlotDate);
				
				var storeOpenForDelivery : Boolean = true; // True for all other sites. Set accordingly for MattressFirm
				if (dw.system.Site.getCurrent().getID() == 'Mattress-Firm') {
					var queryString = 'custom.zip_code = {0}';
					var zipcodeObj = CustomObjectManager.queryCustomObject('ZipInfo', queryString, (session.custom.customerZip).toString());
					if(!empty(zipcodeObj) && !empty(zipcodeObj.custom) && 'timezone' in zipcodeObj.custom) {
						var timezone = zipcodeObj.custom.timezone;
					}
					var currentDateTimeCalendar_withTimezone : dw.util.Calendar = new dw.util.Calendar();
					if (!empty(timezone)) {
						/*Timezone is offset*/
						currentDateTimeCalendar_withTimezone.add(currentDateTimeCalendar_withTimezone.HOUR_OF_DAY, Number(timezone));
					} else {
						// Render template: availability.isml if timezone was not found
						availabilityDate = null;
						break;
					}
					/* EndTime for Stores = 02:00 pm. Instead of jsonResponse[i].EndTime as time is needed for processing */
					var ATPDateTimeCalendar  : dw.util.Calendar = new dw.util.Calendar(getDateFromDateAndTimeString(deliveryDate, '02:00 pm'));
					storeOpenForDelivery = ATPDateTimeCalendar.compareTo(currentDateTimeCalendar_withTimezone) < 0 ? false : true;
				}	
				if (Number(response.Slots) > 0 && response.Available.toLowerCase() === 'yes' && storeOpenForDelivery) {
					isInStock = true;
					availabilityDate = deliveryDate;
					
					if (dw.system.Site.getCurrent().getID() == 'Mattress-Firm') {
						var ATPDateTimeCalendarUTC = calendarToUTCDateWithoutTime(ATPDateTimeCalendar);
						var currentDateTimeCalendar_withTimezoneUTC = calendarToUTCDateWithoutTime(currentDateTimeCalendar_withTimezone);
						var dayDifferenceMF = Math.floor((ATPDateTimeCalendarUTC - currentDateTimeCalendar_withTimezoneUTC) / (1000 * 60 * 60 * 24));
					}

					var todaysUTCDate = Date.UTC(todaysDate.getFullYear(), todaysDate.getMonth(), todaysDate.getDate());
					var availabilityUTCDate = Date.UTC(availabilityDate.getFullYear(), availabilityDate.getMonth(), availabilityDate.getDate());
					var dayDifference = Math.floor((availabilityUTCDate - todaysUTCDate) / (1000 * 60 * 60 * 24));
					
					if (dw.system.Site.getCurrent().getID() == 'Mattress-Firm') {
						dayDifference = dayDifferenceMF;
					}
					
					if (dayDifference === 0) {
						dateDifferenceString = 'today,';
					} else if (dayDifference === 1) {
						dateDifferenceString = 'tomorrow,';
					} else {
						dateDifferenceString = '';
					}
					session.custom.ATPAvailability[productId + optionId + postalCode + qty] = new Object();
					session.custom.ATPAvailability[productId + optionId + postalCode + qty]['availabilityDate'] = availabilityDate;
					session.custom.ATPAvailability[productId + optionId + postalCode + qty]['dateDifferenceString'] = dateDifferenceString;
					session.custom.ATPAvailability[productId + optionId + postalCode + qty]['availabilityClass'] = availabilityClass;
					session.custom.ATPAvailability[productId + optionId + postalCode + qty]['qty'] = qty;
					
					break;
				}
			}
		}
	}
	if (!isInStock) {
		availabilityClass = 'not-available-msg';
	}
					
	app.getView({
		availabilityDate: availabilityDate,
		dateDifferenceString: dateDifferenceString,
		availabilityClass: availabilityClass,
		isInStock: isInStock,
		Product: product.object,
		IsATPSuccess: isATPSuccess
	}).render('product/components/atp-availability');
}

/* For Mattress-Firm only */
function getDefaultAvailabilityMessage() {
	var params = request.httpParameterMap,
	productId = params.productId.stringValue,
	product = app.getModel('Product').get(productId);
	
	app.getView({
		availabilityDate: null,
		dateDifferenceString: '',
		availabilityClass: 'not-available-msg',
		isInStock: false,
		Product: product.object	
	}).render('product/components/atp-availability');
}

/**
 * Get First available variant (Queen, King, Full, Twin)
 *
 * @param {product} master product
 * @returns string variant product id
 */
function getFirstAvailableVariant(product){
	var availableVariantId = "";
	var variantNames = ["QUEEN", "KING", "FULL", "TWIN"];
	
	if(product != null && !product.isVariant()){
		var model = app.getModel('Product');
		
		for each (var varName in variantNames) {			
			var variantID = ProductUtils.getVariantIDForVariantSize(product, varName);
			
			if(typeof variantID === 'string' && !empty(variantID)){
				var variantProduct = model.get(variantID);

    			if (variantProduct.isVisible()) {
        			var available = variantProduct.getAvailability(1);
        			
        			if(available && available.isAvailable && available.inStock){
        				availableVariantId = variantID;
        				break;
        			}
    			}
			}		
		}
	}
	return availableVariantId;
}
/**
 * PLP ATP Availability messaging
 */
function getPLPATPAvailabilityMessage() {
	var params = request.httpParameterMap,
		productId = params.productId.stringValue,
		sizeCount : Number = params.sizeCount.intValue,
		qty : Number = params.qty.intValue,
		postalCode = session.custom.customerZip,
		optionId = params.optionId.stringValue ? params.optionId.stringValue : 'none',
		requestedDate = new Date(),
		todaysDate = new Date(),
		products = new Array(),
		availabilityDate = null,
		availabilityClass = 'in-stock-msg',
		dateDifferenceString = '',
		isInStock = false,
		product = app.getModel('Product').get(productId),
		isATPSuccess = true,
		variantExists = true;
	try{
		// Convert master SKU to Queen variant
		if(product && !product.isVariant()){
			// Default Queen variant
			var variantID = ProductUtils.getVariantIDForVariantSize(product, "QUEEN");
			if(!empty(variantID)){
				product = app.getModel('Product').get(variantID);
				var available = product.getAvailability(1); 
				// Check for Queen availablity
				if (product.isVisible() && available && available.isAvailable && available.inStock) {        			       			
	        		productId = variantID;
	        	}else{
	        		variantExists = false;
				}			
			}else {
				variantExists = false;
			}
		}else {
			// For a variant where multiple sizes are selected on PLP
			if(sizeCount > 1){
				var masterProduct = (product.object && !product.object.master) ? product.object.masterProduct : null;
				var variantID = getFirstAvailableVariant(masterProduct);
				if(!empty(variantID)){
					product = app.getModel('Product').get(variantID);
					productId = variantID;
				}else {
					variantExists = false;
				}	
			}
		}
	}catch(e){
		var errMsg = e.message;
		variantExists = false;
		Logger.error('Product variant level setup Error:  '+ errMsg);
	}

	if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableATPCacheOnPLP') && postalCode && variantExists) {
		
		products.push({
			'ProductId' : productId,
			'Quantity' : qty
		});		
		
		var service = ATPCacheServices.ATPInventoryMFRM;

		requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
		var requestPacket = '{"InventoryType": "Delivery","ZipCode": "' + postalCode + '","RequestedDate": "' + requestedDate + '","StoreId": "","Products": ' + JSON.stringify(products) + '}';

		var result = service.call(requestPacket);
		if (result.error != 0 || result.errorMessage != null || result.mockResult) {
			isATPSuccess = false;
		} else {
			var jsonResponse = JSON.parse(result.object.text);
			
			for (var i = 0; i < jsonResponse.length; i++ ) {
				var response = jsonResponse[i];
				var deliveryDate = new Date(response.SlotDate);
				
				var storeOpenForDelivery : Boolean = true; // True for all other sites. Set accordingly for MattressFirm
				if (dw.system.Site.getCurrent().getID() == 'Mattress-Firm') {
					var queryString = 'custom.zip_code = {0}';
					var zipcodeObj = CustomObjectManager.queryCustomObject('ZipInfo', queryString, (session.custom.customerZip).toString());
					if(!empty(zipcodeObj)) {
						var timezone = zipcodeObj.custom.timezone;
					}
					var currentDateTimeCalendar_withTimezone : dw.util.Calendar = new dw.util.Calendar();
					if (!empty(timezone)) {
						/*Timezone is offset*/
						currentDateTimeCalendar_withTimezone.add(currentDateTimeCalendar_withTimezone.HOUR_OF_DAY, Number(timezone));
					} else {
						// Render template: availability.isml if timezone was not found
						availabilityDate = null;
						break;
					}
					
					/* EndTime for Stores = 02:00 pm. Instead of jsonResponse[i].EndTime as time is needed for processing */
					var ATPDateTimeCalendar  : dw.util.Calendar = new dw.util.Calendar(getDateFromDateAndTimeString(deliveryDate, '02:00 pm'));
                    
					/* Daylight Saving Time */
					var DST : Number = 0;
					if(!empty(zipcodeObj) && !empty(zipcodeObj.custom) && 'dst' in zipcodeObj.custom) {
						DST = zipcodeObj.custom.dst;
					}
					if (DST != 0 &&	isDaylightSavingONfromCalendar(currentDateTimeCalendar_withTimezone)) {
						currentDateTimeCalendar_withTimezone.add(currentDateTimeCalendar_withTimezone.HOUR_OF_DAY, DST);
					}
                    storeOpenForDelivery = ATPDateTimeCalendar.compareTo(currentDateTimeCalendar_withTimezone) < 0 ? false : true;	
				}
				if (Number(response.Slots) > 0 && response.Available.toLowerCase() === 'yes' && storeOpenForDelivery) {
					isInStock = true;
					availabilityDate = deliveryDate;
					
					if (dw.system.Site.getCurrent().getID() == 'Mattress-Firm') {
						var ATPDateTimeCalendarUTC = calendarToUTCDateWithoutTime(ATPDateTimeCalendar);
						var currentDateTimeCalendar_withTimezoneUTC = calendarToUTCDateWithoutTime(currentDateTimeCalendar_withTimezone);
						var dayDifferenceMF = Math.floor((ATPDateTimeCalendarUTC - currentDateTimeCalendar_withTimezoneUTC) / (1000 * 60 * 60 * 24));
					}

					var todaysUTCDate = Date.UTC(todaysDate.getFullYear(), todaysDate.getMonth(), todaysDate.getDate());
					var availabilityUTCDate = Date.UTC(availabilityDate.getFullYear(), availabilityDate.getMonth(), availabilityDate.getDate());
					var dayDifference = Math.floor((availabilityUTCDate - todaysUTCDate) / (1000 * 60 * 60 * 24));
					
					if (dw.system.Site.getCurrent().getID() == 'Mattress-Firm') {
						dayDifference = dayDifferenceMF;
					}
					if (dayDifference === 0) {
						dateDifferenceString = 'Today,';
					} else if (dayDifference === 1) {
						dateDifferenceString = 'Tomorrow,'; 
					} else {
						dateDifferenceString = '';
					}
					break;
				}
			}
		}
	}

	if (!isInStock) {
		availabilityClass = 'not-available-msg';
	}
	
	// Whether to show ATP delivery message or message from Custom Objects (AvailabilityMessage & AvailabilityMappingRules)
	var showATPMessage = false;
	if (!product.object.master 
			&& !product.object.variationGroup
			&& product.object.custom.shippingInformation.toLowerCase() == 'core' 
			&& dw.system.Site.getCurrent().getCustomPreferenceValue('enableATPCacheOnPLP')  
			&& session.custom.customerZip) {
		showATPMessage = true;
	}		

	var r = require('app_storefront_controllers/cartridge/scripts/util/Response');
    r.renderJSON({
            success: isATPSuccess,
            availabilityDate: availabilityDate,
    		dayDifference: dayDifference,
    		dateDifferenceString: dateDifferenceString,
    		availabilityClass: availabilityClass,
    		isInStock: isInStock,
    		showATPMessage: showATPMessage
        });
}


/* For Mattress-Firm only */
function getATPAvailabilityMessageAjax() {
	var params = request.httpParameterMap,
		productId = params.productId.stringValue,
		qty : Number = params.qty.intValue,
		postalCode = session.custom.customerZip,
		optionId = params.optionId.stringValue ? params.optionId.stringValue : 'none',
		requestedDate = new Date(),
		todaysDate = new Date(),
		products = new Array(),
		availabilityDate = null,
		availabilityClass = 'in-stock-msg',
		dateDifferenceString = '',
		isInStock = false,
		product = app.getModel('Product').get(productId),
		isATPSuccess = true;
	
	var context = (!empty(params.atpContext) &&  !empty(params.atpContext.stringValue) ) ? params.atpContext.stringValue : "" ;
		
	if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpAvailabilityMessage') && postalCode) { //add check for core .. implementation should be same
		
		if (!session.custom.ATPAvailability) {
			session.custom.ATPAvailability = new Object();
		}
		
		if (!empty(Basket)) {
			var plis = Basket.productLineItems;
			for (var i = 0; i < plis.length; i++ ) {
				var pli = plis[i];
				if (productId == pli.productID) {
					qty = qty + pli.quantityValue;
				}
			}
		}
		
		products.push({
			'ProductId' : productId,
			'Quantity' : qty
		});
		
		if (optionId && optionId != 'none') {
			products.push({
				'ProductId' : optionId,
				'Quantity' : qty
			});
		}
		
		var enablePDPCache = dw.system.Site.getCurrent().getCustomPreferenceValue('enableATPCacheOnPDP');
		var service = null;
		if (ISMattressFirm) {
			if (enablePDPCache && (context == "product" || context == "finder") ){
				service = ATPCacheServices.ATPInventoryMFRM;
			}
		} else {
			service = ATPInventoryServices.ATPInventory1800;
		}
		requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
		var requestPacket = '{"InventoryType": "Delivery","ZipCode": "' + postalCode + '","RequestedDate": "' + requestedDate + '","StoreId": "","Products": ' + JSON.stringify(products) + '}';

		var result = service.call(requestPacket);
		if (result.error != 0 || result.errorMessage != null || result.mockResult) {
			isATPSuccess = false;
		} else {
			
			var jsonResponse = JSON.parse(result.object.text);
			
			for (var i = 0; i < jsonResponse.length; i++ ) {
				var response = jsonResponse[i];
				var deliveryDate = new Date(response.SlotDate);
				
				var storeOpenForDelivery : Boolean = true; // True for all other sites. Set accordingly for MattressFirm
				if (dw.system.Site.getCurrent().getID() == 'Mattress-Firm') {
					var queryString = 'custom.zip_code = {0}';
					var zipcodeObj = CustomObjectManager.queryCustomObject('ZipInfo', queryString, (session.custom.customerZip).toString());
					if(!empty(zipcodeObj)) {
						var timezone = zipcodeObj.custom.timezone;
					}
					var currentDateTimeCalendar_withTimezone : dw.util.Calendar = new dw.util.Calendar();
					if (!empty(timezone)) {
						/*Timezone is offset*/
						currentDateTimeCalendar_withTimezone.add(currentDateTimeCalendar_withTimezone.HOUR_OF_DAY, Number(timezone));
					} else {
						// Render template: availability.isml if timezone was not found
						availabilityDate = null;
						break;
					}
					
					/* EndTime for Stores = 02:00 pm. Instead of jsonResponse[i].EndTime as time is needed for processing */
					var ATPDateTimeCalendar  : dw.util.Calendar = new dw.util.Calendar(getDateFromDateAndTimeString(deliveryDate, '02:00 pm'));
                    
					/* Daylight Saving Time */
					var DST : Number = 0;
					if(!empty(zipcodeObj) && !empty(zipcodeObj.custom) && 'dst' in zipcodeObj.custom) {
						DST = zipcodeObj.custom.dst;
					}
					if (DST != 0 &&	isDaylightSavingONfromCalendar(currentDateTimeCalendar_withTimezone)) {
						currentDateTimeCalendar_withTimezone.add(currentDateTimeCalendar_withTimezone.HOUR_OF_DAY, DST);
					}
                    storeOpenForDelivery = ATPDateTimeCalendar.compareTo(currentDateTimeCalendar_withTimezone) < 0 ? false : true;	
				}
				if (Number(response.Slots) > 0 && response.Available.toLowerCase() === 'yes' && storeOpenForDelivery) {
					isInStock = true;
					availabilityDate = deliveryDate;
					
					if (dw.system.Site.getCurrent().getID() == 'Mattress-Firm') {
						var ATPDateTimeCalendarUTC = calendarToUTCDateWithoutTime(ATPDateTimeCalendar);
						var currentDateTimeCalendar_withTimezoneUTC = calendarToUTCDateWithoutTime(currentDateTimeCalendar_withTimezone);
						var dayDifferenceMF = Math.floor((ATPDateTimeCalendarUTC - currentDateTimeCalendar_withTimezoneUTC) / (1000 * 60 * 60 * 24));
					}

					var todaysUTCDate = Date.UTC(todaysDate.getFullYear(), todaysDate.getMonth(), todaysDate.getDate());
					var availabilityUTCDate = Date.UTC(availabilityDate.getFullYear(), availabilityDate.getMonth(), availabilityDate.getDate());
					var dayDifference = Math.floor((availabilityUTCDate - todaysUTCDate) / (1000 * 60 * 60 * 24));
					
					if (dw.system.Site.getCurrent().getID() == 'Mattress-Firm') {
						dayDifference = dayDifferenceMF;
					}
					if (dayDifference === 0) {
						dateDifferenceString = 'today,';
					} else if (dayDifference === 1) {
						dateDifferenceString = 'tomorrow,';
					} else {
						dateDifferenceString = '';
					}
					session.custom.ATPAvailability[productId + optionId + postalCode + qty] = new Object();
					session.custom.ATPAvailability[productId + optionId + postalCode + qty]['availabilityDate'] = availabilityDate;
					session.custom.ATPAvailability[productId + optionId + postalCode + qty]['dateDifferenceString'] = dateDifferenceString;
					session.custom.ATPAvailability[productId + optionId + postalCode + qty]['availabilityClass'] = availabilityClass;
					session.custom.ATPAvailability[productId + optionId + postalCode + qty]['qty'] = qty;
					
					break;
				}
			}
		}
	}

	if (!isInStock) {
		availabilityClass = 'not-available-msg';
	}
	
	// Whether to show ATP delivery message or message from Custom Objects (AvailabilityMessage & AvailabilityMappingRules)
	var showATPMessage = false;
	if (!product.object.master 
			&& !product.object.variationGroup
			&& product.object.custom.shippingInformation.toLowerCase() == 'core' 
			&& dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpAvailabilityMessage') 
			&& session.custom.customerZip) {
		showATPMessage = true;
	}		

	var detailsContentId : String = "";
	if (availabilityDate != null && isInStock === true) {
		var productInventoryRecord = product.object.availabilityModel.inventoryRecord;
		var messageId = productInventoryRecord.custom.messageType;
		if (dw.content.ContentMgr.getContent("deliver-options-" + messageId) != null) {
			detailsContentId = "deliver-options-" + messageId;
		}
	}
	if (request.httpParameterMap.format.stringValue === 'ajax') {
        let r = require('app_storefront_controllers/cartridge/scripts/util/Response');
        r.renderJSON({
            success: isATPSuccess,
            availabilityDate: availabilityDate,
    		dayDifference: dayDifference,
    		dateDifferenceString: dateDifferenceString,
    		availabilityClass: availabilityClass,
    		isInStock: isInStock,
    		detailsContentId: detailsContentId,
    		showATPMessage: showATPMessage
        });
    }
}

/**
 * Only for MattressFirm site. Chicago DC Delivery for small, core, parcelable products only for individual
 * prododuct response.
 * MAT-2044 Handle out of stock seprately for each chicago item.
 */
function getATPAvailabilityForIndividualChicagoDCAjax(productIDs) {
	var cart = app.getModel('Cart').get();
    var enableCartATPCache = 'enableATPCacheOnCart' in Site.current.preferences.custom ? Site.getCurrent().getCustomPreferenceValue('enableATPCacheOnCart') : false;
    var service = ATPCacheServices.ATPInventoryStatus;
	var params = request.httpParameterMap,
		isInStock = true,
		postalCode = "chicagoDCZipCode" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("chicagoDCZipCode") : '60446',
		responseJSONObject = {
	        	atpSuccess			: false,
	        	productsAvailable	: true,
	        	products            : new Object(),
	        	lastAvailableDate   : null,
	        	chicagoOOSSkus		: []
	        }
	var enableChicagoDCDelivery = "enableChicagoDCDelivery" in Site.current.preferences.custom ? Site.current.getCustomPreferenceValue("enableChicagoDCDelivery") : false;
	if (Site.getCurrent().ID == 'Mattress-Firm' && enableChicagoDCDelivery && !empty(Basket)) {		
		var allProducts = new Array();
		
		var bundleProductsTracker = new Array();
		var accomulativeResults = new Array();
		var nonBundleProductIDs = new Array();
		var deliveryDatesArr = new dw.util.ArrayList();
		var plis = Basket.productLineItems;
		//collect products from cart for atp request
		for (var i = 0; i < plis.length; i++ ) {
			var pli = plis[i];
			if (!empty(pli.product.manufacturerSKU) &&  UtilFunctionsMF.isParcelableAndCore(pli.product.manufacturerSKU)) {
				var isbundle = pli.product.bundle;
				  if(isbundle) {
				  	    
                        var bundleItems = pli.product.bundledProducts;
                        var singleBundleItem, product, quantity = null;
                        
                        for (var x=0; x < bundleItems.length; x++) {
                            singleBundleItem = bundleItems[x];
                            var bundleProductId = singleBundleItem.ID;
                            var mainBundleID = pli.product.ID;
                            var bundleAvailablity = '';
                            product = dw.catalog.ProductMgr.getProduct(bundleProductId);
                            quantity = pli.getQuantityValue() * pli.product.getBundledProductQuantity(product).value;
                            pli.custom.mainBundleID = mainBundleID;
                            allProducts.push({
								'ProductId'	: bundleProductId,
								'Quantity'	: quantity
							});
							
							bundleProductsTracker.push({
								'mainBundleID' : mainBundleID,
								'bundleProductId' : bundleProductId,
								'Availability' : '',
								'bundleSKU' : pli.product.manufacturerSKU
							});
                                                       
                        }
                        
                    }else{
                    	allProducts.push({
							'ProductId'	: pli.productID,
							'Quantity'	: pli.quantityValue
						});
                    	nonBundleProductIDs.push(pli.productID);
					}
			}
			
		}
		//collect products from cart for atp request END
		
		//check if duplicate then merge product with quantity
		var refinedProductHashMap = new dw.util.HashMap();  
		if(!empty(allProducts) && allProducts.length > 0){
			for(var r=0; r < allProducts.length; r++){
				if (refinedProductHashMap.containsKey(allProducts[r].ProductId)) {        
	        		var existingProductValue = refinedProductHashMap.get(allProducts[r].ProductId);
	        		var updatedQty = Number(existingProductValue) + Number(allProducts[r].Quantity);
	        		refinedProductHashMap.put(allProducts[r].ProductId, updatedQty);
	        		
	    		} else  {
	    			refinedProductHashMap.put(allProducts[r].ProductId, allProducts[r].Quantity);
	    		}
				
			}
		}
		
		var products = new Array();
		for each(var prodItem in refinedProductHashMap.entrySet().iterator()) {            	
            	products.push({
								'ProductId'	: prodItem.key,
								'Quantity'	: prodItem.value
							});                   
          }
		//check if duplicate then merge product with quantity END
		
		try {
			if (products.length > 0) {
				var requestedDate = new Date();
				requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
				
				var requestAtpAgain = true,
					requestCount = 0;
				do {
					/* 
					 * We do not have to pass the Warehouse ID explicitly.
					 * ATP service will automatically get the products availability from the nearest warehouse on the basis of Zipcode
					 */
					var requestPacket = '{"InventoryType": "Delivery","ZipCode": "' + postalCode + '","RequestedDate": "' + requestedDate + '","StoreId": "","Products": ' + JSON.stringify(products) + '}';
					var result = service.call(requestPacket);
					Logger.error('ATP Service call::\nURL: '+service.getURL()+'\nRequest Packet: '+requestPacket);
					requestCount++;
					if (result.error != 0 || result.errorMessage != null || result.mockResult) {
						Logger.error('Service Response: '+result.errorMessage);
					} else {
						responseJSONObject.atpSuccess = true;
						// get next week date to compare chicago product date to show out of stock if date is after seven days.
						var nextWeekDate = new Date();
						nextWeekDate.setDate(nextWeekDate.getDate() + 7);
						Logger.error('Service Response: '+result.object.text);
						var jsonResponse = JSON.parse(result.object.text);
						for (var i = 0; i < jsonResponse.length; i++ ) {
								var response = jsonResponse[i];
								var productDate = !empty(response.Date) ? new Date(response.Date) : null;								
								var atpQuantity = new Number(response.Quantity);
								if (atpQuantity == 0 && responseJSONObject.productsAvailable) {
									responseJSONObject.productsAvailable = false;
								}
								if (!empty(response)) {
									if (atpQuantity == 0 || nextWeekDate < productDate) {
										for(var bt=0; bt < bundleProductsTracker.length; bt++){
											if(response.ProductId == bundleProductsTracker[bt].bundleProductId) {
													bundleProductsTracker[bt].Availability = 'NO';
											}
										}
										accomulativeResults.push({
												'ProductId' : response.ProductId,
												'Availability' : 'NO',
												'ProductDate' : response.Date
										});
									} else {
										for(var bta=0; bta < bundleProductsTracker.length; bta++){
											if(response.ProductId == bundleProductsTracker[bta].bundleProductId) {
												bundleProductsTracker[bta].Availability = 'YES';
											}
										}
										accomulativeResults.push({
											'ProductId' : response.ProductId,
											'Availability' : 'YES',
											'ProductDate' : productDate
										});
									}
								}
								if (requestAtpAgain === true && !empty(response) && !empty(response.ProductId) && !empty(response.Date)) {
									requestAtpAgain = false;									
								}
							}
							//session.custom.dcParcleProducts = responseJSONObject.products;
							Logger.error('Service Response: '+result.errorMessage);
					}
					if (requestAtpAgain === true) {
						responseJSONObject.products = new Object();
						responseJSONObject.productsAvailable = true;
						requestedDate = new Date(requestedDate);
						requestedDate.setDate(requestedDate.getDate() + 7);
						requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
					}else {
						
						var mainBundleId;
						var isBundleAvailable = false;
						
						if(!empty(accomulativeResults) && accomulativeResults.length > 0) {
							for(var t=0; t < accomulativeResults.length; t++) {
								var matchedBundleArray = getmatchedBundleFromTracker(accomulativeResults[t].ProductId, bundleProductsTracker);
								var productDate = new Date(accomulativeResults[t].ProductDate);
								if (bundleProductsTracker.length > 0 && matchedBundleArray.length > 0) {
									for each(var bundle in matchedBundleArray) {
										mainBundleId = bundle.mainBundleID;
										if (!responseJSONObject.products.hasOwnProperty(mainBundleId)) {
											responseJSONObject.products[mainBundleId] = new Object();
										}
										isBundleAvailable = checkBundleAvailability(mainBundleId,bundleProductsTracker);
										if (empty(responseJSONObject.products[mainBundleId]['available']) || responseJSONObject.products[mainBundleId]['available'] === true) {
											responseJSONObject.products[mainBundleId]['Date'] = accomulativeResults[t].ProductDate;	
											if (isBundleAvailable) {
												// Get Last Available Date in chicago DC Products.
												if ( !empty(productDate) && (empty(responseJSONObject.lastAvailableDate) || responseJSONObject.lastAvailableDate < productDate) ) {
													responseJSONObject.lastAvailableDate = productDate;
												}
												responseJSONObject.products[mainBundleId]['available'] = true;
											} else {
												responseJSONObject.products[mainBundleId]['available'] = false;
												// Expect mfi prefix to get manufacturer id. exclude first 3 chars
												responseJSONObject.chicagoOOSSkus.push(bundle.bundleSKU);
											}
										}
									}
									//in case we have any common non-bundle product.
									if (nonBundleProductIDs.indexOf(accomulativeResults[t].ProductId) > -1) {
										responseJSONObject.products[accomulativeResults[t].ProductId] = new Object();
										responseJSONObject.products[accomulativeResults[t].ProductId]['Date'] = accomulativeResults[t].ProductDate;
										if(accomulativeResults[t].Availability == 'YES') {
											// Get Last Available Date in chicago DC Products.
											if ( !empty(productDate) && (empty(responseJSONObject.lastAvailableDate) || responseJSONObject.lastAvailableDate < productDate) ) {
												responseJSONObject.lastAvailableDate = accomulativeResults[t].ProductDate;
											}
												responseJSONObject.products[accomulativeResults[t].ProductId]['available'] = true;
										}else{
											responseJSONObject.products[accomulativeResults[t].ProductId]['available'] = false;
											// Expect mfi prefix to get manufacturer id. exclude first 3 chars
											responseJSONObject.chicagoOOSSkus.push(accomulativeResults[t].ProductId.substring(3));
										}
									}
								} else {
									responseJSONObject.products[accomulativeResults[t].ProductId] = new Object();
									responseJSONObject.products[accomulativeResults[t].ProductId]['Date'] = accomulativeResults[t].ProductDate;
									if(accomulativeResults[t].Availability == 'YES') {
										// Get Last Available Date in chicago DC Products.
										if ( !empty(productDate) && (empty(responseJSONObject.lastAvailableDate) || responseJSONObject.lastAvailableDate < productDate) ) {
											responseJSONObject.lastAvailableDate = accomulativeResults[t].ProductDate;
										}
										responseJSONObject.products[accomulativeResults[t].ProductId]['available'] = true;
									} else {
										responseJSONObject.products[accomulativeResults[t].ProductId]['available'] = false;
										// Expect mfi prefix to get manufacturer id. exclude first 3 chars
										responseJSONObject.chicagoOOSSkus.push(accomulativeResults[t].ProductId.substring(3));
									}
								}							
							}
							Logger.error('Chicago Individual Products Packet: '+ JSON.stringify(responseJSONObject) + Object.keys(responseJSONObject).length);
						}
					}
				} while (requestAtpAgain === true && requestCount < 2);
			}
			
		} catch (e) {
			var error = e.message;
		}
		// in case if service not respond or we dont get products object. use for disabling cart button.
		if (!responseJSONObject.atpSuccess || empty(responseJSONObject.products)) {
			responseJSONObject.productsAvailable = false;
		}
		if (request.httpParameterMap.format.stringValue === 'ajax') {
	        let r = require('app_storefront_controllers/cartridge/scripts/util/Response');
	        r.renderJSON(responseJSONObject);
	    } else {
	    	return responseJSONObject;
	    }
	}
}


function checkBundleAvailability(mainBundleId, bundleProductsTracker){
	var availabilitySring="";
	for(var i=0; i < bundleProductsTracker.length; i++){
		if(bundleProductsTracker[i].mainBundleID.toString().toLowerCase() == mainBundleId.toString().toLowerCase()){
			availabilitySring = availabilitySring+bundleProductsTracker[i].Availability;
		}
	}
	
	if(availabilitySring.indexOf('NO') > -1){
		return false;
	}else{
		return true;
	}
}
function getmatchedBundleFromTracker (productId, bundleProductsTracker) {
	var matchedBundles = [];
	for(var i=0; i < bundleProductsTracker.length; i++) {
		if(bundleProductsTracker[i].bundleProductId.toString().toLowerCase() == productId.toString().toLowerCase()){
			matchedBundles.push(bundleProductsTracker[i]);
		}
	}
	return matchedBundles;
}
function getPreferredStoreAddressMattressFinder() {
	var store = getPreferredStore();
	var prodId = request.httpParameterMap.pid ? request.httpParameterMap.pid : false;
	var ProductMgr = require('dw/catalog/ProductMgr');
	var product = ProductMgr.getProduct(prodId.value);
	var inventoryParam = product.isVariant() ? product.masterProduct.ID : product.ID;
	
	var isInventoryList = false;
	var record;
	if (store != null) {
		var inventory = store.inventoryList;
		if (!empty(inventory)) {
			try {
				record = inventory.getRecord(inventoryParam.replace('mfi',''));
			} catch (e) {
				var error = e;
			}
			if (record) {
				isInventoryList = true;
			}
		}
	} 
	
	app.getView({
	   preferredStore: store,
	   pid: prodId,
	   isInventoryList: isInventoryList
	}).render('product/preferredStore_mattressfinder');
	 
}

function getPreferredStoreAddressMattressMatcher() {
	var store = getPreferredStore();
	var prodId = request.httpParameterMap.pid ? request.httpParameterMap.pid : false;
	var ProductMgr = require('dw/catalog/ProductMgr');
	var product = ProductMgr.getProduct(prodId.value);
	var inventoryParam = product.isVariant() ? product.masterProduct.ID : product.ID;
	
	var isInventoryList = false;
	var record;
	if (store != null) {
		var inventory = store.inventoryList;
		if (!empty(inventory)) {
			try {
				record = inventory.getRecord(inventoryParam.replace('mfi',''));
			} catch (e) {
				var error = e;
			}
			if (record) {
				isInventoryList = true;
			}
		}
	} 
	
	app.getView({
	   preferredStore: store,
	   pid: prodId,
	   isInventoryList: isInventoryList
	}).render('product/preferredStore_mattressmatcher');
	 
}
/*
* Web exposed methods
*/
/** Starting point for the single shipping scenario.
 * @see module:controllers/COShipping~start */
exports.Start = guard.ensure(['get'], start);
exports.FindInStore = guard.ensure(['get'], findInStore);
exports.HandleForm = guard.ensure(['post'], handleForm);
exports.GetPreferredStore = getPreferredStore;
exports.SetPreferredStore = guard.ensure(['post'], setPreferredStore);
exports.SetPreferredStoreAB = guard.ensure(['post'], setPreferredStoreAB);
exports.GetPreferredStoreAddress = guard.ensure(['get'], getPreferredStoreAddress);
exports.UpdateBopisOption = guard.ensure(['post'], updateBopisOption);
exports.GetNearbyStores = guard.ensure(['get'], getNearbyStores);
exports.GetAvailablePickupStore = guard.ensure(['get'], getAvailablePickupStore);
exports.GetAvailablePickupStoreLocal = getAvailablePickupStore;
exports.EvaluateCartForBopis = evaluateCartForBopis;
exports.ChangeDeliveryToShipAddress = guard.ensure(['https', 'post'], changeDeliveryToShipAddress);
exports.GetATPDeliveryDates = getATPDeliveryDates;
exports.GetSelectedATPSlots = guard.ensure(['get'], getSelectedATPSlots);
exports.GetMobileCalander = guard.ensure(['get'], getMobileCalander);
exports.GetATPDeliveryDatesForNextWeek = guard.ensure(['get'], getATPDeliveryDatesForNextWeek);
exports.GetATPAvailabilityMessage = guard.ensure(['get'], getATPAvailabilityMessage);
exports.GetATPAvailabilityMessageAjax = guard.ensure(['get'], getATPAvailabilityMessageAjax);
exports.GetATPAvailabilityForIndividualChicagoDCAjax = guard.ensure(['get'], getATPAvailabilityForIndividualChicagoDCAjax);
exports.GetDefaultAvailabilityMessage = guard.ensure(['get'], getDefaultAvailabilityMessage);
exports.GetMaxAvailabilityMessage = guard.ensure(['get'], getMaxAvailabilityMessage);
exports.SetPreferredStoreFromGeoLocation = setPreferredStoreFromGeoLocation;
exports.GetDeliveryZone = getDeliveryZone;
exports.GetPreferredStoreAddressMattressFinder = guard.ensure(['get'], getPreferredStoreAddressMattressFinder);
exports.GetPLPATPAvailabilityMessage = guard.ensure(['get'], getPLPATPAvailabilityMessage);
exports.GetPreferredStoreAddressMattressMatcher = guard.ensure(['get'], getPreferredStoreAddressMattressMatcher);
exports.ConfirmATPAvailablity = guard.ensure(['get'], confirmATPAvailablity);
exports.ConfirmATPAvailablityAmazon = guard.ensure(['get'], confirmATPAvailablityAmazon);

