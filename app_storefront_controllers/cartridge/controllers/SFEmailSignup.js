'use strict';

/**
 * Controller 
 *
 * @module controllers/SFEmailSignup
 */

/* API includes */
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var StringUtils = require('dw/util/StringUtils');
var Transaction = require('dw/system/Transaction');

/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var emailHelper = require("~/cartridge/scripts/util/SFEmailSubscriptionHelper").SFEmailSubscriptionHelper; 
var UtilFunctionsMF = require('app_mattressfirm_storefront/cartridge/scripts/util/UtilFunctions').UtilFunctions;

function showForm() 
{
	app.getView('EmailSignup').render('emailsubscriptions/SFEmailModalForm');
}

// Recieves the AJAX submission of email address and responds
function submitEmail() {
	var params = request.httpParameterMap;
	var emailAddress : String = params.emailAddress.stringValue;
	var firstName : String = params.firstName.stringValue;
	var leadSource : String = params.leadSource.stringValue;
	var leadCreative : String = params.leadCreative.stringValue;
	var leadPage : String = params.leadPage.stringValue;
	var zipCode : String = params.zipCode.stringValue;
	var zipCodeModal : String = params.zipCodeModal.stringValue;
	var siteId : String = params.siteId.stringValue;
	var optOutFlag = params.optOutFlag.booleanValue;
	var gclid = params.gclid.stringValue;
	var signupVersion = params.signupVersion.submitted? params.signupVersion.stringValue : '';
	var successAsset = params.successAsset.submitted? params.successAsset.stringValue : '';
	var emailErrorAsset = params.emailErrorAsset.submitted? params.emailErrorAsset.stringValue : '';

	//save e-mail address using lower case
	emailAddress = emailAddress.toLowerCase();
	// REGEX to check email
	if (!/^[a-zA-Z0-9]+(?:(\.|_)[A-Za-z0-9!#$%&'*+/=?^`{|}~-]+)*@(?!([a-zA-Z0-9]*\.[a-zA-Z0-9]*\.[a-zA-Z0-9]*\.))(?:[A-Za-z0-9](?:[a-zA-Z0-9-]*[A-Za-z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/.test(emailAddress)) {
	// email falied validation
	}
	var emailParams = {
			emailAddress: emailAddress, 
			firstName: firstName, 
			zipCode: zipCode, 
			zipCodeModal: zipCodeModal, 
			leadSource: leadSource, 
			leadCreative: leadCreative,
			leadPage: leadPage,
			siteId: siteId, 
			optOutFlag: optOutFlag,
			gclid: gclid,
			dwsid: session.sessionID
			};
	
	var returnResult = emailHelper.sendSFEmailInfo(emailParams);
	
	if (leadSource == 'modal') {
		var responseTemplate = 'SFEmailResponseModal';
	} else if(leadSource == 'bts_2019' && params.pid.stringValue && returnResult.Status != 'SERVICE_ERROR' && returnResult.Status != 'ERROR') { 
		//MAT-1594 BTS Promo Customer can enter email address on PDP (Promo Live 7/17/2019)
		var ProductMgr = require('dw/catalog/ProductMgr');
		var product = ProductMgr.getProduct(params.pid.stringValue);
	    if (!empty(product.custom.pdpEmailSigupPromoStatus && dw.system.Site.current.preferences.custom.pdpEmailSignPromoSourceCodeID) && product.custom.pdpEmailSigupPromoStatus == true) {
    		//setting source code of BTS promotion from custom preference
	    	session.setSourceCode(dw.system.Site.current.preferences.custom.pdpEmailSignPromoSourceCodeID);
    	}
		var responseTemplate = 'SFEmailResponseFooter';

	} else if(leadSource == dw.system.Site.current.preferences.custom.cmEmailSignupLeadSource 
					&& !empty(params.pid.stringValue) && UtilFunctionsMF.isCyberMondayPromotionProduct(params.pid.stringValue)) { 
		
		// Check for StrikeIron Service Error
		if(empty(returnResult.ErrorCode) || returnResult.ErrorCode != "SI_SERVICE_ERROR")	{	
    	
			var ProductMgr = require('dw/catalog/ProductMgr');
			var product = ProductMgr.getProduct(params.pid.stringValue);
		    
		    if (!empty(product)  && !empty(dw.system.Site.current.preferences.custom.cmEmailSignupPromoSourceCodeID)) {
		    		
		    	var sourceCode = dw.system.Site.current.preferences.custom.cmEmailSignupPromoSourceCodeID;
	    		var retResult = session.setSourceCode(sourceCode);
	    	}
			var responseTemplate = 'SFEmailResponseFooter';
		}

	} else if(signupVersion.toLowerCase() == 'generic' || signupVersion.toLowerCase() == 'bf2019') {
		var responseTemplate = 'SFEmailGenResponse';
	}
	else if (returnResult.Status == 'SERVICE_ERROR' || returnResult.Status == 'ERROR'){
		if (leadSource == 'footer' && (returnResult.Status == 'ERROR' || returnResult.Status == 'SERVICE_ERROR')) {
			var responseTemplate = 'SFEmailResponseFooter'
		}
	} else if (leadSource == 'bts_2019' && (returnResult.Status == 'SERVICE_ERROR' || returnResult.Status == 'ERROR')){
		var responseTemplate = 'pdpemailsignuperror';
		var returnResult = emailHelper.sendFailSafe(emailParams, returnResult.ErrorCode);
		app.getView('EmailSignup', {SFEmailStatus : returnResult.Status, SFEmailMsg : '', SFEmailErrorCode: '', contactID: ''}).render('emailsubscriptions/' + responseTemplate);
	} else {
		var responseTemplate = 'SFEmailResponseFooter';
	}
	if (leadSource == 'mattress_finder') {
		if (returnResult.Status == 'SERVICE_ERROR'){
			emailHelper.sendFailSafe(emailParams, returnResult.ErrorCode);
		}
		app.getView({
			JSONResponse: returnResult
		}).render('util/responsejson');
	} 
	else if (signupVersion == 'generic' || signupVersion.toLowerCase() == 'bf2019') {
		var contentAsset='';
		if (returnResult.Status == 'SERVICE_ERROR' || returnResult.Status == 'ERROR'){
			var returnResult1 = emailHelper.sendFailSafe(emailParams, returnResult.ErrorCode);
			if(dw.content.ContentMgr.getContent(emailErrorAsset+ '-' + returnResult.ErrorCode)){
				contentAsset = emailErrorAsset+ '-' + returnResult.ErrorCode;
			}
			else {
				contentAsset = emailErrorAsset;
			}
		}
		else {
			contentAsset = successAsset;
		}
		
		app.getView('EmailSignup', {SFEmailStatus : returnResult.Status, SFEmailMsg : returnResult.Msg, SFEmailErrorCode: returnResult.ErrorCode, contactID: returnResult.ContactID, contentAsset: contentAsset}).render('emailsubscriptions/' + responseTemplate);
	}
	else {
		if (returnResult.Status == 'SERVICE_ERROR') {
			var returnResult = emailHelper.sendFailSafe(emailParams, returnResult.ErrorCode);
			app.getView('EmailSignup', {SFEmailStatus : returnResult.Status, SFEmailMsg : '', SFEmailErrorCode: '', contactID: ''}).render('emailsubscriptions/' + responseTemplate);
		} else {
			app.getView('EmailSignup', {SFEmailStatus : returnResult.Status, SFEmailMsg : returnResult.Msg, SFEmailErrorCode: returnResult.ErrorCode, contactID: returnResult.ContactID}).render('emailsubscriptions/' + responseTemplate);
		}
	}
	
}

function submitQuestions() {
	var params = request.httpParameterMap;
	var questionNum : Number = params.questionNum.value;
	var numQuestions : Number = params.numQuestions.value;
	var contactID : String = params.contactID.stringValue;
	
	if (questionNum == numQuestions) {
		var returnResult = emailHelper.sendSFQuestions(params);
		var responseTemplate = 'SFQuestionsResponseModal';
		
		if (returnResult.Status == 'SERVICE_ERROR'){
			var returnResult = emailHelper.sendQuestionsFailSafe(params, 'modal', returnResult.ErrorCode);
			app.getView('Questions', {SFEmailStatus : returnResult.Status, SFEmailMsg : '', SFEmailErrorCode: '', ContactID: returnResult.ContactID}).render('emailsubscriptions/' + responseTemplate);
		} else {
			app.getView('Questions', {SFEmailStatus : returnResult.Status, SFEmailMsg : '', SFEmailErrorCode: '', ContactID: returnResult.ContactID}).render('emailsubscriptions/' + responseTemplate);
		}
	} else {
		var responseTemplate = 'SFQuestions';
		app.getView('Questions',  params).render('emailsubscriptions/' + responseTemplate);
	}		
}

function submitQuestionsLanding() {
	var params = request.httpParameterMap;
	var questionNum : Number = params.questionNum.value ? params.questionNum.value : 0;
	var numQuestions : Number = params.numQuestions.value ? params.numQuestions.value : 1;
	
	if (questionNum == numQuestions) {
		var returnResult = emailHelper.sendSFQuestions(params);
		var responseTemplate = 'SFQuestionsResponseLanding';
		
		if (returnResult.Status == 'SERVICE_ERROR'){
			var returnResult = emailHelper.sendQuestionsFailSafe(params, 'landing', returnResult.ErrorCode);
			app.getView('Questions', {SFEmailStatus : returnResult.Status, SFEmailMsg : '', SFEmailErrorCode: '', ContactID: returnResult.ContactID}).render('emailsubscriptions/' + responseTemplate);
		} else {
			app.getView('Questions', {SFEmailStatus : returnResult.Status, SFEmailMsg : '', SFEmailErrorCode: '', ContactID: returnResult.ContactID}).render('emailsubscriptions/' + responseTemplate);
		}
	} else {
		var responseTemplate = 'SFQuestionsLanding';
		app.getView('Questions',  params).render('emailsubscriptions/' + responseTemplate);
	}		
}

function processFailSafe() {
	var returnResult = emailHelper.processFailSafe();
	return returnResult;
}

function processQuestionsFailSafe() {
	var returnResult = emailHelper.processQuestionsFailSafe();
	return returnResult;
}

exports.Submit = guard.ensure(['post'], submitEmail);
exports.SubmitQuestions = guard.ensure(['post'], submitQuestions);
exports.SubmitQuestionsLanding = guard.ensure(['https', 'get'], submitQuestionsLanding);
exports.ProcessFailSafe = processFailSafe;
exports.ProcessQuestionsFailSafe = processQuestionsFailSafe;
exports.Form = guard.ensure(['get'], showForm);

