'use strict';

/**
 * Controller 
 *
 * @module controllers/WebToCase
 */

/* API includes */
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var guard = require('app_storefront_controllers/cartridge/scripts/guard');

/** Global Variables */
var TIMEOUT_MSG = "Read timed out";
var TIMEOUT_ERROR = "Service Timed Out...";
var params = request.httpParameterMap;

function submitWebToCaseForm() {
	try {
		var result = webToCaseService.call();
		if (result.error != 0 || result.errorMessage != null || result.mockResult) {
			response.redirect(URLUtils.https('Page-Show', 'cid', 'customer-service-form'));
		} else {
			response.redirect(URLUtils.https('Page-Show', 'cid', 'customer-service-form') + "#thankYou");
		}
	} catch (e) {
		response.redirect(URLUtils.https('Page-Show', 'cid', 'customer-service-form'));
	}
}

/** salesforce.contact.webToCase */
var webToCaseService = LocalServiceRegistry.createService("salesforce.contact.webToCase", {
    createRequest: function(service, args) {
        service.addHeader("Content-Type", "application/x-www-form-urlencoded");
        service.setRequestMethod("POST");
		var reqObject = new Object();
		for (var param in params) {
			if (params[param] && params[param] instanceof dw.web.HttpParameter) {
				var key = param;
				var value = encodeURIComponent(params[param]["stringValue"]);
				var encodedValue = encodeURIComponent(params[param]["stringValue"]);
				service.addHeader(key, encodedValue);
				reqObject[key] = value;
			}
		}
       return reqObject;
    },
    parseResponse : function(svc, listOutput) {
        return listOutput.text;
    },
    getRequestLogMessage : function(requestObj) {
       try {
           return requestObj;
       } catch(e) {}
       return requestObj;
   },
   getResponseLogMessage : function(responseObj) {
       if (responseObj instanceof dw.net.HTTPClient) {
           try {
               return responseObj.text;
           } catch(e) {}
       }
       return responseObj;
   },
   filterLogMessage: function(msg) {
	   if(msg === TIMEOUT_MSG) {
			return TIMEOUT_ERROR;
	   }else {
			return msg;
	   }
	}
});

/** Form handler for the customer service form.
 * @see {@link module:controllers/WebToCase~submitWebToCaseForm} */
exports.SubmitWebToCaseForm = guard.ensure(['post'], submitWebToCaseForm);

