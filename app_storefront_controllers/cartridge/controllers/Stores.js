'use strict';

/**
 * Controller that renders the store finder and store detail pages.
 *
 * @module controllers/Stores
 */

/* API Includes */
var StoreMgr = require('dw/catalog/StoreMgr');
var SystemObjectMgr = require('dw/object/SystemObjectMgr');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var CustomObjectManager = require('dw/object/CustomObjectMgr');


/**
 * Provides a form to locate stores by geographical information.
 *
 * Clears the storelocator form. Gets a ContentModel that wraps the store-locator content asset.
 * Updates the page metadata and renders the store locator page (storelocator/storelocator template).
 */
function find() {
    var storeLocatorForm = app.getForm('storelocator');
    storeLocatorForm.clear();

    var Content = app.getModel('Content');
    var storeLocatorAsset = Content.get('store-locator');

    var pageMeta = require('~/cartridge/scripts/meta');
    pageMeta.update(storeLocatorAsset);

    app.getView('StoreLocator').render('storelocator/storelocator');
}

/**
 * The storelocator form handler. This form is submitted with GET.
 * Handles the following actions:
 * - findbycountry
 * - findbystate
 * - findbyzip
 * In all cases, gets the search criteria from the form (formgroup) passed in by
 * the handleAction method and queries the platform for stores matching that criteria. Returns null if no stores are found,
 * otherwise returns a JSON object store, search key, and search criteria information. If there are search results, renders
 * the store results page (storelocator/storelocatorresults template), otherwise renders the store locator page
 * (storelocator/storelocator template).
 */
function findStores() {
    var Content = app.getModel('Content');
    var storeLocatorAsset = Content.get('store-locator');

    var pageMeta = require('~/cartridge/scripts/meta');
    pageMeta.update(storeLocatorAsset);

    var storeLocatorForm = app.getForm('storelocator');
    var searchResult = storeLocatorForm.handleAction({
        findbycountry: function (formgroup) {
            var searchKey = formgroup.country.htmlValue;
            var stores = SystemObjectMgr.querySystemObjects('Store', 'countryCode = {0}', 'countryCode desc', searchKey);
            if (empty(stores)) {
                return null;
            } else {
                return {'stores': stores, 'searchKey': searchKey, 'type': 'findbycountry'};
            }
        },
        findbystate: function (formgroup) {
            var searchKey = formgroup.state.htmlValue;
            var stores = null;

            if (!empty(searchKey)) {
                stores = SystemObjectMgr.querySystemObjects('Store', 'stateCode = {0}', 'stateCode desc', searchKey);
            }

            if (empty(stores)) {
                return null;
            } else {
                return {'stores': stores, 'searchKey': searchKey, 'type': 'findbystate'};
            }
        },
        findbyzip: function (formgroup) {
            var searchKey = formgroup.postalCode.value;
            var storesMgrResult = StoreMgr.searchStoresByPostalCode(formgroup.countryCode.value, searchKey, formgroup.distanceUnit.value, formgroup.maxdistance.value);
            var stores = storesMgrResult.keySet();
            if (empty(stores)) {
                return null;
            } else {
                return {'stores': stores, 'searchKey': searchKey, 'type': 'findbyzip'};
            }
        }
    });

    if (searchResult) {
        app.getView('StoreLocator', searchResult)
            .render('storelocator/storelocatorresults');
    } else {
        app.getView('StoreLocator')
            .render('storelocator/storelocator');
    }

}

/**
 * Renders the details of a store.
 *
 * Gets the store ID from the httpParameterMap. Updates the page metadata.
 * Renders the store details page (storelocator/storedetails template).
 */
function details() {

    var storeID = request.httpParameterMap.StoreID.value;
    var store = dw.catalog.StoreMgr.getStore(storeID);

    var pageMeta = require('~/cartridge/scripts/meta');
    pageMeta.update(store);

    app.getView({Store: store})
        .render('storelocator/storedetails');

}

/*
 * Office Hours Parser for [listing, details]
 */
function simplifyWorkingHours(storeHours, timezoneOffset){
    var storeHours = storeHours.toString().trim().replace(/\s+/g, '-');
    var rawData = storeHours.trim().split("-");
    var days = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
    var daySlots = [];
    var tmpDaySlot = {};
    var dayIndex = 0;
    var storeDay = getStoreDay(timezoneOffset);
    for(x = 0; x < rawData.length; x++){
        let chunk = rawData[x];
        if((days.indexOf(chunk.toLowerCase())) > -1){
            dayIndex = days.indexOf(chunk.toLowerCase());
            var tmpDaySlot = {
                day: chunk,
                open: null,
                openMeridiem: null,
                close: null,
                closeMeridiem: null,
                today: false
            };
            tmpDaySlot.today = dayIndex == storeDay;
            tmpDaySlot.day = days[dayIndex];
        }
        else{
            if(chunk.toLowerCase().indexOf('am') > -1 || chunk.toLowerCase().indexOf('pm') > -1) {
                if(tmpDaySlot.openMeridiem == null) {
                    tmpDaySlot.openMeridiem = chunk;        
                } 
                else {
                    tmpDaySlot.closeMeridiem = chunk;        
                }
            }
            else {
                if(tmpDaySlot.open == null) {
                    tmpDaySlot.open = chunk;        
                }
                else if(tmpDaySlot.close == null) {
                    tmpDaySlot.close = chunk;        
                }
            }
            daySlots[dayIndex] = tmpDaySlot;
        }
        
    }
    return daySlots;
}
/*
 * Returns day according to store location
 */
function getStoreDay(timezoneOffset) {
    var storeDay;
    if(timezoneOffset == null || empty(timezoneOffset)) {
        storeDay = new Date().getDay();
    }
    else {
        var targetDate = new Date();
        var timestamp = targetDate.getTime()/1000 + targetDate.getTimezoneOffset() * 60;
        timezoneOffset = parseFloat(timezoneOffset);
        var localdate = new Date(timestamp * 1000 + timezoneOffset);
        storeDay = localdate.getDay();
    }
    return storeDay;
}

/*
 * prepare listing hours
 */
function prepareListingHours(storeHours, timezoneOffset){
    var daySlots = simplifyWorkingHours(storeHours, timezoneOffset);
    var day = getStoreDay(timezoneOffset); 
    var today = day;
    var tomorrow = (today == 6) ? 0 : today + 1; //assuming day can't be higher than 6
    today = (typeof daySlots[today] != 'undefined') ? daySlots[today] : null;
    tomorrow = (typeof daySlots[tomorrow] != 'undefined') ? daySlots[tomorrow] : null;
    
    return {today: today, tomorrow: tomorrow};
}
    
/*
* prepare details hours
*/
function prepareDetailsHours(storeHours, timezoneOffset){
    var daySlots = simplifyWorkingHours(storeHours, timezoneOffset);
    var date = new Date(); 
    var today = date.getDay();
    today = (typeof daySlots[today] != 'undefined') ? daySlots[today] : null;
    var tomorrow = (today == 6) ? 0 : today + 1; //assuming day can't be higher than 6
    tomorrow = (typeof daySlots[tomorrow] != 'undefined') ? daySlots[tomorrow] : null;
    let daysOrder = [1, 2, 3, 4, 5, 6, 0];
    var officeHours = [];
    for(x = 0; x < daysOrder.length; x++){
        officeHours[x] = (typeof daySlots[daysOrder[x]] != 'undefined') ? daySlots[daysOrder[x]] : null;
    }
    return officeHours;
}
    
function getStoreStructre(item, storesMgrResult){
    storesMgrResult = storesMgrResult || null;
    var hours = item.storeHours.toString();//false ? prepareDetailsHours(item.storeHours, item.custom.timezoneOffset) : prepareListingHours(item.storeHours, item.custom.timezoneOffset);    
    var distance = (storesMgrResult == null) ?  null :  storesMgrResult.get(item);
    
    var qAttr = "";
    if (item.address1 != null) qAttr += item.address1 + ", ";
    if (item.address2 != null) qAttr += item.address2 + ", ";
    if (item.city != null) qAttr += item.city + ", ";
    if (item.postalCode != null) qAttr += item.postalCode + ", ";
    if (item.stateCode != null) qAttr += item.stateCode + ", ";
    if (item.countryCode != null) qAttr += item.countryCode;
    var directionsLink = "//maps.google.com/maps?hl=en&f=q&q=" + encodeURI(qAttr);    
    var distance = (distance == null) ? distance : distance.toFixed(1);
    var warehouseId = !empty(item.custom.warehouseId) ? item.custom.warehouseId : ''
    return { 
             'ID' : item.ID,
             'name' : item.name,
             'address1' : item.address1,
             'address2' : item.address2,
             'city' : item.city,
             'stateCode' : item.stateCode,
             'postalCode' : item.postalCode,
             'countryCode' : item.countryCode,
             'phone' : item.phone,
             'email' : item.email,
             'lat' : item.latitude,
             'lng' : item.longitude,
             'distance' : distance,
             'storeHours' : hours,
             'directionsLink': directionsLink,
             'warehouseId': warehouseId,
         };
        
}

/**
 * Get zipcode info based on zip code
 */
function getWareHouseInfo (zipCode) {
    var custObjType = 'ZipInfo';
    var custObjKeyVal = zipCode;
    var matchedZipObj = new Object();
    var wareHouseInfo = null;
    try {        
        if(!empty(session.custom["zipCode_" + zipCode])) {
    		matchedZipObj = session.custom["zipCode_" + zipCode];
    	}else {
    		matchedZipObj = CustomObjectManager.getCustomObject(custObjType, custObjKeyVal);    		
    		session.custom["zipCode_" + zipCode] =  matchedZipObj;
    	}
        
        if (!empty(matchedZipObj)) {
        	var warehouseIds = matchedZipObj.custom.warehouseId; 
        	var isInMarket = matchedZipObj.custom.inMarket;
            var warehouseAssociationID = warehouseIds.join('|');
            if(warehouseAssociationID.length == 0 ){
		    	warehouseAssociationID = warehouseAssociationID + 'WEBONLY'
		    }else{
		    	warehouseAssociationID = warehouseAssociationID + '|WEBONLY'
		    }
            if(isInMarket) {
	    		warehouseAssociationID = warehouseAssociationID + '|EXCLUDEGEO'
	    	}
            wareHouseInfo = new Object();            
    		wareHouseInfo.city = matchedZipObj.custom.cities[0];
    		wareHouseInfo.state = matchedZipObj.custom.state;
    		wareHouseInfo.warehouseId = warehouseAssociationID;
    		wareHouseInfo.zipCode = matchedZipObj.custom.zip_code;            
        }            
    } catch (e) {
        var msg = e;
    }
    return wareHouseInfo;
}

/**
 * Get nearest stores based on zip code
 */
function getStoresByGeoLocation(zipCode) {		
	var StoreMgr = require('dw/catalog/StoreMgr');
	var nearestStores = new Array();
    var Countries = require('app_storefront_core/cartridge/scripts/util/Countries');
    var Store = require('app_storefront_controllers/cartridge/controllers/Stores');	
    var countryCode = Countries.getCurrent({
        CurrentRequest: {
                locale: request.locale
            }
        }).countryCode;
    var distanceUnit = (countryCode == 'US') ? 'mi' : 'km';
    if(!empty(zipCode) && !empty(countryCode)){
	    var storeMap = StoreMgr.searchStoresByPostalCode(countryCode, zipCode, distanceUnit, 30);
	    var storesSet = storeMap.keySet();    
	    for(i=0; i < storesSet.length; i++) {
	        let tmpStore = getStoreStructre(storesSet[i], storeMap);
	        nearestStores.push(tmpStore);
	        if(nearestStores.length == 3 ){
	            break;
	        }        
	     }
    }
    return nearestStores;
}

/**
 * Get nearest store (single) based on zip code in 30mile radius
 */
function getNerestStoreByZipCode(zipCode) {		
	var nearestStore = null;
	var StoreMgr = require('dw/catalog/StoreMgr');
	var Countries = require('app_storefront_core/cartridge/scripts/util/Countries');
    var countryCode = Countries.getCurrent({
        CurrentRequest: {
                locale: request.locale
            }
        }).countryCode;
    var distanceUnit = (countryCode == 'US') ? 'mi' : 'km';
    if(!empty(zipCode) && !empty(countryCode)){
	    var storeMap = StoreMgr.searchStoresByPostalCode(countryCode, zipCode, distanceUnit, 30);
	    var storesSet = storeMap.keySet();    
	    for(i = 0; i < storesSet.length; i++) {
	    	nearestStore = getStoreStructre(storesSet[i], storeMap);
	        break;     
	     }
    }
    return nearestStore;
}
/*
 * Exposed web methods
 */
/** Renders form to locate stores by geographical information.
 * @see module:controllers/Stores~find */
exports.Find = guard.ensure(['get'], find);
/** The storelocator form handler.
 * @see module:controllers/Stores~findStores */
exports.FindStores = guard.ensure(['post'], findStores);
/** Renders the details of a store.
 * @see module:controllers/Stores~details */
exports.Details = guard.ensure(['get'], details);
exports.GetStoresByGeoLocation = getStoresByGeoLocation;
exports.GetWareHouseInfo = getWareHouseInfo;
exports.GetNerestStoreByZipCode = getNerestStoreByZipCode;


