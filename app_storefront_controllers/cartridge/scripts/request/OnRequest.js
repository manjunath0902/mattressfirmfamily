'use strict';

/**
 * The onRequest hook is called with every top-level request in a site. This happens both for requests to cached and non-cached pages.
 * For performance reasons the hook function should be kept short.
 *
 * @module  request/OnRequest
 */

var Status = require('dw/system/Status');
var BasketMgr = require('dw/order/BasketMgr');
var Store = require('app_storefront_controllers/cartridge/controllers/Stores');
var Site = require('dw/system/Site');

/**
 * The onRequest hook function.
 */
exports.onRequest = function () {
	try {
		//Request Level IP Filtering for POS Site Only
		if(Site.getCurrent().ID === 'POS') {
			require('~/cartridge/scripts/util/firewall/Firewall').init();
			return new Status(Status.OK);
		}
		
		if (request.httpParameterMap.zip && request.httpParameterMap.zip.submitted){
	    	
	    	var urlZip = request.httpParameterMap.zip.value;				
		    if (!empty(urlZip)){
		    	var isValidZip = /(^\d{5}$)/.test(urlZip);
		    	if (isValidZip) {
		    		var warehouse = Store.GetWareHouseInfo(urlZip);
		    		
		    		if( !empty(warehouse) && !empty(warehouse.state) && !empty(warehouse.city) ){    		
		    		
		    			session.custom.customerZip = urlZip;
		    		}
		    	}
		    }
		    
		}
		if(dw.system.Site.getCurrent().getCustomPreferenceValue('sfLoginEnabled') || dw.system.Site.getCurrent().getCustomPreferenceValue('enableRioCookies')) {
			//Set Customer Cookie for RIO
			
			if(customer.authenticated) {
				var customerCookie = dw.web.Cookie("rio_customer_name", customer.profile.firstName);//customer.profile.firstName
				customerCookie.setComment("Cookie for RIO Store Page: Logged In Customer First Name");
				//cookie.setDomain(request.httpHost);
				customerCookie.setMaxAge(7*24*60*60);
				customerCookie.setSecure(true);
				customerCookie.setValue(customer.profile.firstName);
				customerCookie.setPath("/");
			}
			else {
				var customerCookie = dw.web.Cookie("rio_customer_name", "");
				customerCookie.setMaxAge(0);
				customerCookie.setValue(null);
				customerCookie.setPath("/");
			}
			
			request.addHttpCookie(customerCookie);
			response.addHttpCookie(customerCookie);
			
			//Set Cart Total Cookie for RIO
			var basket = BasketMgr.getCurrentBasket();
			if(basket) {
				var cartQty = 0;
				var pliIt = basket.productLineItems.iterator();
				while (pliIt.hasNext()) {
					var pli = pliIt.next();
					cartQty += pli.quantity;
				}
				
				var cartCookie = dw.web.Cookie("rio_cart_total", cartQty+"");//customer.profile.firstName
				cartCookie.setComment("Cookie for RIO Stor Page: Cart Total");
				//cookie.setDomain(request.httpHost);
				cartCookie.setMaxAge(7*24*60*60);
				cartCookie.setSecure(true);
				cartCookie.setValue(cartQty+"");
				cartCookie.setPath("/");
			}
		}
	}
	catch (e) {
		// TODO: handle exception
		 var msg = e;
	}
    return new Status(Status.OK);
};
