'use strict';
/**
 * @module meta
 */
var HOME_BREADCRUMB = {
	name: dw.web.Resource.msg('global.home', 'locale', null),
	url: dw.web.URLUtils.httpHome()
};
/**
 * Constructor for metadata singleton
 *
 * This should be initialized via the current context object (product, category, asset or folder) and can
 * be used to retrieve the page metadata, breadcrumbs and to render the accumulated information to the client
 *
 * @class
 */
var Meta = function() {
	this.data = {
		page: {
			title: '',
			description: '',
			keywords: ''
		},
		// supports elements with properties name and url
		breadcrumbs: [HOME_BREADCRUMB],
		resources: {}
	};
};
Meta.prototype = {
	/**
	 * The core method of this class which updates the internal data represenation with the given information
	 *
	 * @param  {Object|dw.catalog.Product|dw.catalog.Category|dw.content.Content|dw.content.Folder} object The object to update with
	 *
	 * @example
	 * // using a product object
	 * meta.update(product);
	 * // using a plain object
	 * meta.update({resources: {
	 *     'MY_RESOURCE': dw.web.Resource.msg('my.resource', 'mybundle', null)
	 * }});
	 * // using a string
	 * meta.update('account.landing')
	 */
	update: function(object) {
		// check if object wrapped in AbstractModel and get system object if so get the system object
		if ('object' in object) {
			object = object.object;
		}
		// check if it is a system object
		if (object.class) {
			// update metadata
			var title = null;
			var multiCanononicalTitle = null;
			var MultipleCanonical = 'N';
			if (!title && 'pageTitle' in object) {
				title = object.pageTitle;
			}
			if (!title && dw.system.Site.getCurrent().name == 'Mattress Firm') {
				if ('manufacturerName' in object && 'name' in object) { // && 'mattress_type' in object.custom
					var size: String = '';
					var variantAtributeSize: ProductVariationAttribute = null;
					var atributeSizeValue: ProductVariationAttributeValue = null;
					try {
						if (object.variationModel.getProductVariationAttribute('size') != null) {
							variantAtributeSize = object.variationModel.getProductVariationAttribute('size');
							atributeSizeValue = object.variationModel.getVariationValue(object, variantAtributeSize);
							size = ' ' + atributeSizeValue.getDisplayValue();
						} else size = '';
					} catch (e) {
						size = '';
					}
					title = object.manufacturerName + ' ' + object.name + size + ' - Mattress Firm';
				}
			}
			if (!title && dw.system.Site.getCurrent().name == 'Dream Bed') {
				if ('manufacturerName' in object && 'name' in object) { // && 'mattress_type' in object.custom
					var size: String = '';
					var variantAtributeSize: ProductVariationAttribute = null;
					var atributeSizeValue: ProductVariationAttributeValue = null;
					try {
						if (object.variationModel.getProductVariationAttribute('size') != null) {
							variantAtributeSize = object.variationModel.getProductVariationAttribute('size');
							atributeSizeValue = object.variationModel.getVariationValue(object, variantAtributeSize);
							size = ' ' + atributeSizeValue.getDisplayValue();
						} else size = '';
					} catch (e) {
						size = '';
					}
					title = object.manufacturerName + ' ' + object.name + size + ' - Mattress Firm';
				}
			}
			if (!title && 'name' in object) {
				title = object.name;
			} else if (!title && 'displayName' in object) {
				title = object.displayName;
			}
			var currentSiteId = dw.system.Site.getCurrent().ID;
			if (arguments.length > 1) {
				var isRefinementChecked = false;
				var selectedRefinementsMapMeta = new dw.util.HashMap();
				var refinementBrandArray = new Array();
				var refinementSizeArray = new Array();
				var refinementTypeArray = new Array();
				var refinementComfortArray = new Array();
				var productSearchModel = arguments[1];
				var PriceRefinementOnLoad = 'N';
				if (productSearchModel.refinements.priceRefinementDefinition && productSearchModel.refinedByPrice) {
					PriceRefinementOnLoad = 'Y';
				}
				//get all selected refinements on page load for title rendering 
				if (productSearchModel != null && productSearchModel.refinements != null && productSearchModel.refinements.refinementDefinitions != null) {
					for (var i = 0; i < productSearchModel.refinements.refinementDefinitions.length; i++) {
						var selectedRefinement = productSearchModel.refinements.refinementDefinitions[i];
						if (selectedRefinement.isAttributeRefinement() && productSearchModel.isRefinedByAttribute(selectedRefinement.attributeID)) {
							var arrayMy = new Array();
							arrayMy = productSearchModel.refinements.getRefinementValues(selectedRefinement);
							for (var ii = 0; ii < arrayMy.length; ii++) {
								var ojb = arrayMy[ii];
								if (productSearchModel.isRefinedByAttributeValue(selectedRefinement.attributeID, ojb.value)) {
									isRefinementChecked = true;
									selectedRefinementsMapMeta.put(selectedRefinement.displayName, arrayMy[ii].value);
									var jsonData = {};
									jsonData.name = selectedRefinement.displayName;
									jsonData.value = arrayMy[ii].value
									if (selectedRefinement.displayName == 'Brands') {
										refinementBrandArray.push(jsonData);
									}
									if (selectedRefinement.displayName == 'Size') {
										refinementSizeArray.push(jsonData);
									}
									if (selectedRefinement.displayName == 'Mattress Type') {
										refinementTypeArray.push(jsonData);
									}
									if (selectedRefinement.displayName == 'Comfort') {
										refinementComfortArray.push(jsonData);
									}
								}
							}
						} else {
							if (!isRefinementChecked == true) {
								isRefinementChecked = false;
							}
						}
					}
				}
				//set title on page load according to site id
				var siteId = dw.system.Site.getCurrent().ID;
				var brand = "";
				var size = "";
				var type = "";
				var comfort = "";
				var brandMultipleCanonical = "";
				var sizeMultipleCanonical = "";
				var typeMultipleCanonical = "";
				var comfortMultipleCanonical = "";
				var categoryName = productSearchModel.category.displayName;
				var parentCategoryName = !empty(productSearchModel.category.getParent()) ? productSearchModel.category.getParent().displayName : '';
				if (isRefinementChecked == true) {
					//// for mattress firm
					if (siteId == 'Mattress-Firm') {
						if (selectedRefinementsMapMeta.containsKey("Brands")) {
							brand = selectedRefinementsMapMeta.get("Brands");
						}
						if (selectedRefinementsMapMeta.containsKey("Size")) {
							size = selectedRefinementsMapMeta.get("Size");
						}
						if (selectedRefinementsMapMeta.containsKey("Mattress Type")) {
							type = selectedRefinementsMapMeta.get("Mattress Type");
						}
						if (selectedRefinementsMapMeta.containsKey("Comfort")) {
							comfort = selectedRefinementsMapMeta.get("Comfort");
						}
						if (categoryName == 'Mattresses' || categoryName =='Mattress Sizes' || categoryName =='Shop by Brand' || categoryName =='Fan Shop' || categoryName =='Pillows' || categoryName =='Futons' || categoryName =='Shop Clearance and Overstock') {
							
							var seoCategoryName = "";
							if(categoryName == 'Mattresses' || categoryName =='Mattress Sizes') {
								seoCategoryName = 'Mattresses';
							} else if(categoryName =='Shop Clearance and Overstock'){
								seoCategoryName = 'Clearance and Overstock';
							} else if(categoryName =='Shop by Brand'){
								seoCategoryName = 'Shop by Brand';
							} else{
								seoCategoryName = categoryName;
							}
							
							//Size
							if (size != '' && brand == '' && type == '' && comfort == '') {
								title = size + '-Size '+ seoCategoryName + ' | Mattress Firm';
							}
							//Type
							else if (size == '' && brand == '' && type != '' && comfort == '') {
								title = type + ' '+ seoCategoryName +' | Mattress Firm';
							}
							//Size + Type
							else if (size != '' && brand == '' && type != '' && comfort == '') {
								title = size + '-Size ' + type + ' ' + seoCategoryName +' | Mattress Firm';
							}
							//Brand + Size
							else if (size != '' && brand != '' && type == '' && comfort == '') {
								title = brand + ' ' + size + '-Size ' + seoCategoryName+' | Mattress Firm';
							}
							//Size + Comfort
							else if (size != '' && brand == '' && type == '' && comfort != '') {
								title = comfort + ' ' + size + '-Size '+ seoCategoryName +' | Mattress Firm';
							}
							//Size + Type + Comfort
							else if (size != '' && brand == '' && type != '' && comfort != '') {
								title = comfort + ' ' + size + '-Size ' + type + ' '+ seoCategoryName;
							}
							//Brand + Size + Type
							else if (size != '' && brand != '' && type != '' && comfort == '') {
								title = brand + ' ' + size + '-Size ' + type + ' ' + seoCategoryName;
							}
							//Brand + Size + Comfort
							else if (brand != '' && size != '' && type == '' && comfort != '') {
								title = comfort + ' ' + brand + ' ' + size + '-Size ' + seoCategoryName;
							}
							//Brand + Size + Type + Comfort
							else if (brand != '' && size != '' && type != '' && comfort != '') {
								title = comfort + ' ' + brand + ' ' + size + '-Size ' + type + ' ' + seoCategoryName;
							}
							//Brand + Type
							else if (brand != '' && size == '' && type != '' && comfort == '') {
								title = brand + ' ' + type + ' '+ seoCategoryName+' | Mattress Firm';
							}
							//Type + Comfort
							else if (brand == '' && size == '' && type != '' && comfort != '') {
								title = type + ' ' + comfort + ' '+ seoCategoryName +' | Mattress Firm';
							}
							//Brand + Type + Comfort
							else if (brand != '' && size == '' && type != '' && comfort != '') {
								title = comfort + ' ' + brand + ' ' + type + ' '+seoCategoryName;
							}
							//Brand
							else if (brand != '' && size == '' && type == '' && comfort == '') {
								title = brand + ' ' + seoCategoryName + ' | Mattress Firm';
							}
							//Brand + Comfort
							else if (brand != '' && size == '' && type == '' && comfort != '') {
								title = brand + ' ' + comfort + ' ' + seoCategoryName + ' | Mattress Firm';
							}
							//Comfort
							else if (brand == '' && size == '' && type == '' && comfort != '') {
								title = comfort + ' ' + seoCategoryName + ' | Mattress Firm';
							} else {
								title = 'Shop ' + seoCategoryName + ' | Mattress Firm';
							}
						} else if (categoryName == 'Sale') {
							//Size
							if (size != '' && brand == '' && type == '' && comfort == '') {
								title = 'Sale ' + size + '-Size Mattresses | Mattress Firm';
							}
							//Type
							else if (size == '' && brand == '' && type != '' && comfort == '') {
								title = 'Sale ' + type + ' Mattresses | Mattress Firm';
							}
							//Size + Type
							else if (size != '' && brand == '' && type != '' && comfort == '') {
								title = 'Sale ' + size + '-Size ' + type + ' Mattresses | Mattress Firm';
							}
							//Brand + Size
							else if (size != '' && brand != '' && type == '' && comfort == '') {
								title = 'Sale ' + brand + ' ' + size + '-Size Mattresses | Mattress Firm';
							}
							//Size + Comfort
							else if (size != '' && brand == '' && type == '' && comfort != '') {
								title = 'Sale ' + comfort + ' ' + size + '-Size Mattresses | Mattress Firm';
							}
							//Size + Type + Comfort
							else if (size != '' && brand == '' && type != '' && comfort != '') {
								title = 'Sale ' + comfort + ' ' + size + '-Size ' + type + ' Mattresses';
							}
							//Brand + Size + Type
							else if (size != '' && brand != '' && type != '' && comfort == '') {
								title = 'Sale ' + brand + ' ' + size + '-Size ' + type + ' Mattresses';
							}
							//Brand + Size + Comfort
							else if (brand != '' && size != '' && type == '' && comfort != '') {
								title = 'Sale ' + comfort + ' ' + brand + ' ' + size + '-Size Mattresses';
							}
							//Brand + Size + Type + Comfort
							else if (brand != '' && size != '' && type != '' && comfort != '') {
								title = 'Sale ' + comfort + ' ' + brand + ' ' + size + '-Size ' + type + ' Mattresses';
							}
							//Brand + Type
							else if (brand != '' && size == '' && type != '' && comfort == '') {
								title = 'Sale ' + brand + ' ' + type + ' Mattresses | Mattress Firm';
							}
							//Type + Comfort
							else if (brand == '' && size == '' && type != '' && comfort != '') {
								title = 'Sale ' + type + ' ' + comfort + ' Mattresses | Mattress Firm';
							}
							//Brand + Type + Comfort
							else if (brand != '' && size == '' && type != '' && comfort != '') {
								title = 'Sale ' + comfort + ' ' + brand + ' ' + type + ' Mattresses';
							}
							//Brand
							else if (brand != '' && size == '' && type == '' && comfort == '') {
								title = 'Sale ' + brand + ' Mattresses | Mattress Firm';
							}
							//Brand + Comfort
							else if (brand != '' && size == '' && type == '' && comfort != '') {
								title = 'Sale ' + brand + ' ' + comfort + ' Mattresses | Mattress Firm';
							}
							//Comfort
							else if (brand == '' && size == '' && type == '' && comfort != '') {
								title = 'Sale ' + comfort + ' Mattresses | Mattress Firm';
							} else {
								title = 'Shop ' + categoryName + ' | Mattress Firm';
							}
						} else {
							//Brand + Category
							if (brand != '' && size == '' && type == '' && comfort == '') {
								title = brand + ' ' + categoryName;
							}
							//Category + Size
							else if (brand == '' && size != '' && type == '' && comfort == '') {
								title = size + '-Size ' + categoryName + ' | Mattress Firm';
							}
							//Category + Type
							else if (brand == '' && size == '' && type != '' && comfort == '') {
								title = type + ' ' + categoryName + ' | Mattress Firm';
							}
							//Brand + Category + Type
							else if (brand != '' && size == '' && type != '' && comfort == '') {
								title = brand + ' ' + type + ' ' + categoryName;
							}
							//Brand + Category + Size
							else if (brand != '' && size != '' && type == '' && comfort == '') {
								title = brand + ' ' + size + '-Size ' + categoryName;
							}
							//Brand + Category + Type + Size
							else if (brand != '' && size != '' && type != '' && comfort == '') {
								title = brand + ' ' + type + ' ' + size + '-Size ' + categoryName;
							}
							//Brand
							else if (brand != '' && size == '' && type == '' && comfort == '') {
								title = 'Shop ' + categoryName + ' by ' + brand;
							} else {
								title = 'Shop ' + categoryName + ' | Mattress Firm';
							}
						}
					}
					//// for 1800 RV
					if (siteId == '1800Mattress-RV') {
						if (selectedRefinementsMapMeta.containsKey("Brands")) {
							brand = selectedRefinementsMapMeta.get("Brands");
						}
						if (selectedRefinementsMapMeta.containsKey("Size")) {
							size = selectedRefinementsMapMeta.get("Size");
						}
						if (selectedRefinementsMapMeta.containsKey("Mattress Type")) {
							type = selectedRefinementsMapMeta.get("Mattress Type");
						}
						if (selectedRefinementsMapMeta.containsKey("Comfort")) {
							comfort = selectedRefinementsMapMeta.get("Comfort");
						}
						if (categoryName == 'Mattresses') {
							var categoryNameMattress = categoryName;//'Mattress';
							//Size
							if (size != '' && brand == '' && type == '' && comfort == '') {
								title = size + '-Size '+ categoryNameMattress +' Deals | 1800mattress';															
							}
							//Type
							else if (size == '' && brand == '' && type != '' && comfort == '') {
								title = type + ' '+ categoryNameMattress +' Deals | 1800mattress';								
							}
							//Size + Type
							else if (size != '' && brand == '' && type != '' && comfort == '') {
								title = size + '-Size ' + type +' '+ categoryNameMattress +' Deals | 1800mattress';								
							}
							//Brand + Size
							else if (size != '' && brand != '' && type == '' && comfort == '') {
								title = size +'-Size '+ brand+' '+ categoryNameMattress + ' Deals | 1800mattress';								
							}
							//Size + Comfort
							else if (size != '' && brand == '' && type == '' && comfort != '') {
								title = size +'-Size '+ comfort + ' ' + categoryNameMattress + ' Deals | 1800mattress';							
							}
							//Size + Type + Comfort
							else if (size != '' && brand == '' && type != '' && comfort != '') {
								title = size+'-Size'+' '+comfort+ ' '+type+' '+ categoryNameMattress+' Deals';						
							}
							//Brand + Size + Type
							else if (size != '' && brand != '' && type != '' && comfort == '') {
								title = size+'-Size' +' '+brand+' '+type+' '+categoryNameMattress+' Deals';								
							}
							//Brand + Size + Comfort
							else if (brand != '' && size != '' && type == '' && comfort != '') {
								title = comfort + ' ' + brand + ' ' + size + '-Size '+ categoryNameMattress+' Deals';								
							}
							//Brand + Size + Type + Comfort
							else if (brand != '' && size != '' && type != '' && comfort != '') {
								title = comfort + ' ' + brand + ' ' + size + '-Size ' + type +' '+ categoryNameMattress+' Deals';							
							}
							//Brand + Type
							else if (brand != '' && size == '' && type != '' && comfort == '') {
								title = brand + ' ' + type + ' '+ categoryNameMattress+' Deals | 1800mattress';								
							}
							//Type + Comfort
							else if (brand == '' && size == '' && type != '' && comfort != '') {
								title = comfort+' '+type+' '+categoryNameMattress+' Deals | 1800mattress';								
							}
							//Brand + Type + Comfort
							else if (brand != '' && size == '' && type != '' && comfort != '') {
								title = comfort + ' ' + brand + ' ' + type + ' '+categoryNameMattress+ ' Deals | 1800mattress';							
							}
							//Brand
							else if (brand != '' && size == '' && type == '' && comfort == '') {
								title = brand + ' ' +categoryNameMattress + ' Deals | 1800mattress';							
							}
							//Brand + Comfort
							else if (brand != '' && size == '' && type == '' && comfort != '') {
								title = comfort+' '+brand+ ' '+categoryNameMattress+' Deals | 1800mattress';								
							}
							//Comfort
							else if (brand == '' && size == '' && type == '' && comfort != '') {
								title = comfort+' '+categoryNameMattress+' Deals | 1800mattress';								
							} else {
								title = 'Shop ' + categoryNameMattress + ' | 1800mattress';
							}
						} else if (categoryName == 'Sale') {
						   	var categoryNameMattress = categoryName;
							//Size
							if (size != '' && brand == '' && type == '' && comfort == '') {
								title = size + '-Size '+ categoryNameMattress +' | 1800mattress';															
							}
							//Type
							else if (size == '' && brand == '' && type != '' && comfort == '') {
								title = type + ' '+ categoryNameMattress +' | 1800mattress';								
							}
							//Size + Type
							else if (size != '' && brand == '' && type != '' && comfort == '') {
								title = size + '-Size ' + type +' '+ categoryNameMattress +' | 1800mattress';								
							}
							//Brand + Size
							else if (size != '' && brand != '' && type == '' && comfort == '') {
								title = size +'-Size '+ brand+' '+ categoryNameMattress + ' | 1800mattress';								
							}
							//Size + Comfort
							else if (size != '' && brand == '' && type == '' && comfort != '') {
								title = size +'-Size '+ comfort + ' ' + categoryNameMattress + ' | 1800mattress';							
							}
							//Size + Type + Comfort
							else if (size != '' && brand == '' && type != '' && comfort != '') {
								title = size+'-Size'+' '+comfort+ ' '+type+' '+ categoryNameMattress;						
							}
							//Brand + Size + Type
							else if (size != '' && brand != '' && type != '' && comfort == '') {
								title = size+'-Size' +' '+brand+' '+type+' '+categoryNameMattress;								
							}
							//Brand + Size + Comfort
							else if (brand != '' && size != '' && type == '' && comfort != '') {
								title = comfort + ' ' + brand + ' ' + size + '-Size '+ categoryNameMattress;								
							}
							//Brand + Size + Type + Comfort
							else if (brand != '' && size != '' && type != '' && comfort != '') {
								title = comfort + ' ' + brand + ' ' + size + '-Size ' + type +' '+ categoryNameMattress;							
							}
							//Brand + Type
							else if (brand != '' && size == '' && type != '' && comfort == '') {
								title = brand + ' ' + type + ' '+ categoryNameMattress+' | 1800mattress';								
							}
							//Type + Comfort
							else if (brand == '' && size == '' && type != '' && comfort != '') {
								title = comfort+' '+type+' '+categoryNameMattress+' | 1800mattress';								
							}
							//Brand + Type + Comfort
							else if (brand != '' && size == '' && type != '' && comfort != '') {
								title = comfort + ' ' + brand + ' ' + type + ' '+categoryNameMattress+ ' | 1800mattress';							
							}
							//Brand
							else if (brand != '' && size == '' && type == '' && comfort == '') {
								title = brand + ' ' +categoryNameMattress + ' | 1800mattress';							
							}
							//Brand + Comfort
							else if (brand != '' && size == '' && type == '' && comfort != '') {
								title = comfort+' '+brand+ ' '+categoryNameMattress+' | 1800mattress';								
							}
							//Comfort
							else if (brand == '' && size == '' && type == '' && comfort != '') {
								title = comfort+' '+categoryNameMattress+' | 1800mattress';								
							} else {
								title = 'Shop ' + categoryNameMattress + ' | 1800mattress';
							}
						} else {
							var categoryNameMattress = categoryName;
							//Size
							if (size != '' && brand == '' && type == '' && comfort == '') {
								title = size + '-Size '+ categoryNameMattress +' | 1800mattress';															
							}
							//Type
							else if (size == '' && brand == '' && type != '' && comfort == '') {
								title = type + ' '+ categoryNameMattress +' | 1800mattress';								
							}
							//Size + Type
							else if (size != '' && brand == '' && type != '' && comfort == '') {
								title = size + '-Size ' + type +' '+ categoryNameMattress +' | 1800mattress';								
							}
							//Brand + Size
							else if (size != '' && brand != '' && type == '' && comfort == '') {
								title = size +'-Size '+ brand+' '+ categoryNameMattress + ' | 1800mattress';								
							}
							//Size + Comfort
							else if (size != '' && brand == '' && type == '' && comfort != '') {
								title = size +'-Size '+ comfort + ' ' + categoryNameMattress + ' | 1800mattress';							
							}
							//Size + Type + Comfort
							else if (size != '' && brand == '' && type != '' && comfort != '') {
								title = size+'-Size'+' '+comfort+ ' '+type+' '+ categoryNameMattress;						
							}
							//Brand + Size + Type
							else if (size != '' && brand != '' && type != '' && comfort == '') {
								title = size+'-Size' +' '+brand+' '+type+' '+categoryNameMattress;								
							}
							//Brand + Size + Comfort
							else if (brand != '' && size != '' && type == '' && comfort != '') {
								title = comfort + ' ' + brand + ' ' + size + '-Size '+ categoryNameMattress;								
							}
							//Brand + Size + Type + Comfort
							else if (brand != '' && size != '' && type != '' && comfort != '') {
								title = comfort + ' ' + brand + ' ' + size + '-Size ' + type +' '+ categoryNameMattress;							
							}
							//Brand + Type
							else if (brand != '' && size == '' && type != '' && comfort == '') {
								title = brand + ' ' + type + ' '+ categoryNameMattress+' | 1800mattress';								
							}
							//Type + Comfort
							else if (brand == '' && size == '' && type != '' && comfort != '') {
								title = comfort+' '+type+' '+categoryNameMattress+' | 1800mattress';								
							}
							//Brand + Type + Comfort
							else if (brand != '' && size == '' && type != '' && comfort != '') {
								title = comfort + ' ' + brand + ' ' + type + ' '+categoryNameMattress+ ' | 1800mattress';							
							}
							//Brand
							else if (brand != '' && size == '' && type == '' && comfort == '') {
								title = brand + ' ' +categoryNameMattress + ' | 1800mattress';							
							}
							//Brand + Comfort
							else if (brand != '' && size == '' && type == '' && comfort != '') {
								title = comfort+' '+brand+ ' '+categoryNameMattress+' | 1800mattress';								
							}
							//Comfort
							else if (brand == '' && size == '' && type == '' && comfort != '') {
								title = comfort+' '+categoryNameMattress+' | 1800mattress';								
							} else {
								title = 'Shop ' + categoryNameMattress + ' | 1800mattress';
							}
						}  
					}
				} else {
					//if no any refinment selected on page load (category click from menu title generation logic)                    
					if (siteId == 'Mattress-Firm') {
						if (categoryName == 'Mattresses' || categoryName =='Mattress Sizes' || categoryName =='Shop by Brand' || categoryName =='Fan Shop' || categoryName =='Pillows' || categoryName =='Futons' || categoryName =='Shop Clearance and Overstock') {
							
							var seoCategoryName = "";
							if(categoryName == 'Mattresses' || categoryName =='Mattress Sizes') {
								seoCategoryName = 'Mattresses';
							} else if(categoryName =='Shop Clearance and Overstock'){
								seoCategoryName = 'Clearance and Overstock';
							} else if(categoryName =='Shop by Brand'){
								seoCategoryName = 'Shop by Brand';
							} else{
								seoCategoryName = categoryName;
							}
							
							title = 'Shop ' + seoCategoryName + ' | Mattress Firm';
						} if (categoryName == 'Black Friday Sale') {
							title = categoryName + ' - Best Mattress Deals | Mattress Firm';
						} else {
							if(categoryName == 'Sale') {
								title = 'Shop ' + categoryName + ' Mattresses | Mattress Firm';
							}else {
								title = 'Shop ' + categoryName + ' | Mattress Firm';
							}
							
						}
					}
					if (siteId == '1800Mattress-RV') {
						if (categoryName == 'Mattresses') {
							title = 'Shop Mattresses | 1800Mattress';
						} else {
							title = 'Shop ' + categoryName + ' | 1800Mattress';
						}
					}
				}
				//canonical counter start that is refinement have multiple facet or single to set canonical tag and robot tag on page load
				if (productSearchModel != null && productSearchModel.refinements != null && productSearchModel.refinements.refinementDefinitions != null) {
					var canonicalCounter = 0;
					for (var i = 0; i < productSearchModel.refinements.refinementDefinitions.length; i++) {
						var canonicalCounter = 0;
						var selectedRefinement = productSearchModel.refinements.refinementDefinitions[i];
						if (selectedRefinement.isAttributeRefinement() && productSearchModel.isRefinedByAttribute(selectedRefinement.attributeID)) {
							var arrayMy = new Array();
							arrayMy = productSearchModel.refinements.getRefinementValues(selectedRefinement);
							for (var ii = 0; ii < arrayMy.length; ii++) {
								var ojb = arrayMy[ii];
								if (productSearchModel.isRefinedByAttributeValue(selectedRefinement.attributeID, ojb.value)) {
									canonicalCounter = canonicalCounter + 1;
								}
							}
							if (canonicalCounter > 1) {
								MultipleCanonical = 'Y';
								break;
							} else {
								MultipleCanonical = 'N';
							}
						}
					}
				}
				session.custom.canonical = MultipleCanonical;
				session.custom.priceRefinement = PriceRefinementOnLoad;
				if (MultipleCanonical == 'N') {
					//if there is single value per facet then title as per rules
					this.data.page.title = title.replace("Shop Shop","Shop").replace('Size-Size','Size').replace('Extra Long-Size','Extra Long');
				} else {
					//////////// for mattress firm
					if (siteId == 'Mattress-Firm') {
						categoryName = productSearchModel.category.displayName;
						if (refinementBrandArray && refinementBrandArray.length == 1) {
							brandMultipleCanonical = refinementBrandArray[0].value;
						} else {
							brandMultipleCanonical = "";
						}
						if (refinementSizeArray && refinementSizeArray.length == 1) {
							sizeMultipleCanonical = refinementSizeArray[0].value;
						} else {
							sizeMultipleCanonical = "";
						}
						if (refinementTypeArray && refinementTypeArray.length == 1) {
							typeMultipleCanonical = refinementTypeArray[0].value;
						} else {
							typeMultipleCanonical = "";
						}
						if (refinementComfortArray && refinementComfortArray.length == 1) {
							comfortMultipleCanonical = refinementComfortArray[0].value;
						} else {
							comfortMultipleCanonical = "";
						}
						////////////////////////////////////////////////////////////////////////////////////////////////////
						if (categoryName == 'Mattresses' || categoryName =='Mattress Sizes' || categoryName =='Shop by Brand' || categoryName =='Fan Shop' || categoryName =='Pillows' || categoryName =='Futons' || categoryName =='Shop Clearance and Overstock') {
							
							var seoCategoryName = "";
							if(categoryName == 'Mattresses' || categoryName =='Mattress Sizes') {
								seoCategoryName = 'Mattresses';
							} else if(categoryName =='Shop Clearance and Overstock'){
								seoCategoryName = 'Clearance and Overstock';
							} else if(categoryName =='Shop by Brand'){
								seoCategoryName = 'Shop by Brand';
							} else{
								seoCategoryName = categoryName;
							}
							
							//Size
							if (sizeMultipleCanonical != '' && brandMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = sizeMultipleCanonical + '-Size' + seoCategoryName + ' | Mattress Firm me title load';
							}
							//Type
							else if (sizeMultipleCanonical == '' && brandMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = typeMultipleCanonical + ' ' + seoCategoryName +' | Mattress Firm';
							}
							//Size + Type
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = sizeMultipleCanonical + '-Size ' + typeMultipleCanonical + ' ' + seoCategoryName  + ' | Mattress Firm';
							}
							//Brand + Size
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical != '' && typeMultipleCanonical == '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = brandMultipleCanonical + ' ' + sizeMultipleCanonical + '-Size'+ seoCategoryName+' | Mattress Firm';
							}
							//Size + Comfort
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical + ' ' + sizeMultipleCanonical + '-Size' + seoCategoryName + ' | Mattress Firm';
							}
							//Size + Type + Comfort
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical + ' ' + sizeMultipleCanonical + '-Size ' + typeMultipleCanonical + ' '+ seoCategoryName;
							}
							//Brand + Size + Type
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical != '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = brandMultipleCanonical + ' ' + sizeMultipleCanonical + '-Size ' + typeMultipleCanonical + ' '+ seoCategoryName;
							}
							//Brand + Size + Comfort
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical != '' && typeMultipleCanonical == '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical + ' ' + brandMultipleCanonical + ' ' + sizeMultipleCanonical + '-Size '+ seoCategoryName;
							}
							//Brand + Size + Type + Comfort
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical != '' && typeMultipleCanonical != '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical + ' ' + brandMultipleCanonical + ' ' + sizeMultipleCanonical + '-Size ' + typeMultipleCanonical + ' '+ seoCategoryName;
							}
							//Brand + Type
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = brandMultipleCanonical + ' ' + typeMultipleCanonical + ' Mattresses | Mattress Firm';
							}
							//Type + Comfort
							else if (brandMultipleCanonical == '' && sizeMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = typeMultipleCanonical + ' ' + comfortMultipleCanonical + ' ' + seoCategoryName + ' | Mattress Firm';
							}
							//Brand + Type + Comfort
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical + ' ' + brandMultipleCanonical + ' ' + typeMultipleCanonical + ' '+ seoCategoryName;
							}
							//Brand
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = brandMultipleCanonical + ' ' + seoCategoryName +' | Mattress Firm';
							}
							//Brand + Comfort
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = brandMultipleCanonical + ' ' + comfortMultipleCanonical + ' '  + seoCategoryName + ' | Mattress Firm';
							}
							//Comfort
							else if (brandMultipleCanonical == '' && sizeMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical + ' ' + seoCategoryName +  ' | Mattress Firm';
							} else {
								multiCanononicalTitle = 'Shop ' + seoCategoryName + ' | Mattress Firm';
							}
						} else if (categoryName == 'Sale') {
							//Size
							if (sizeMultipleCanonical != '' && brandMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = 'Sale ' + sizeMultipleCanonical + '-Size Mattresses | Mattress Firm';
							}
							//Type
							else if (sizeMultipleCanonical == '' && brandMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = 'Sale ' + typeMultipleCanonical + ' Mattresses | Mattress Firm';
							}
							//Size + Type
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = 'Sale ' + sizeMultipleCanonical + '-Size ' + typeMultipleCanonical + ' Mattresses | Mattress Firm';
							}
							//Brand + Size
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical != '' && typeMultipleCanonical == '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = 'Sale ' + brandMultipleCanonical + ' ' + sizeMultipleCanonical + '-Size Mattresses | Mattress Firm';
							}
							//Size + Comfort
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = 'Sale ' + comfortMultipleCanonical + ' ' + sizeMultipleCanonical + '-Size Mattresses | Mattress Firm';
							}
							//Size + Type + Comfort
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = 'Sale ' + comfortMultipleCanonical + ' ' + sizeMultipleCanonical + '-Size ' + typeMultipleCanonical + ' Mattresses';
							}
							//Brand + Size + Type
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical != '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = 'Sale ' + brandMultipleCanonical + ' ' + sizeMultipleCanonical + '-Size ' + typeMultipleCanonical + ' Mattresses';
							}
							//Brand + Size + Comfort
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical != '' && typeMultipleCanonical == '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = 'Sale ' + comfortMultipleCanonical + ' ' + brandMultipleCanonical + ' ' + sizeMultipleCanonical + '-Size Mattresses';
							}
							//Brand + Size + Type + Comfort
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical != '' && typeMultipleCanonical != '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = 'Sale ' + comfortMultipleCanonical + ' ' + brandMultipleCanonical + ' ' + sizeMultipleCanonical + '-Size ' + typeMultipleCanonical + ' Mattresses';
							}
							//Brand + Type
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = 'Sale ' + brandMultipleCanonical + ' ' + typeMultipleCanonical + ' Mattresses | Mattress Firm';
							}
							//Type + Comfort
							else if (brandMultipleCanonical == '' && sizeMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = 'Sale ' + typeMultipleCanonical + ' ' + comfortMultipleCanonical + ' Mattresses | Mattress Firm';
							}
							//Brand + Type + Comfort
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = 'Sale ' + comfortMultipleCanonical + ' ' + brandMultipleCanonical + ' ' + typeMultipleCanonical + ' Mattresses';
							}
							//Brand
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = 'Sale ' + brandMultipleCanonical + ' Mattresses | Mattress Firm';
							}
							//Brand + Comfort
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = 'Sale ' + brandMultipleCanonical + ' ' + comfortMultipleCanonical + ' Mattresses | Mattress Firm';
							}
							//Comfort
							else if (brandMultipleCanonical == '' && sizeMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = 'Sale ' + comfortMultipleCanonical + ' Mattresses | Mattress Firm';
							} else {								
								multiCanononicalTitle = 'Shop ' + categoryName + ' Mattresses | Mattress Firm';															
							}
						} else {
							//Brand + Category
							if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = brandMultipleCanonical + ' ' + categoryName;
							}
							//Category + Size
							else if (brandMultipleCanonical == '' && sizeMultipleCanonical != '' && typeMultipleCanonical == '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = sizeMultipleCanonical + '-Size ' + ' ' + categoryName + ' | Mattress Firm';
							}
							//Category + Type
							else if (brandMultipleCanonical == '' && sizeMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = typeMultipleCanonical + ' ' + categoryName + ' | Mattress Firm';
							}
							//Brand + Category + Type
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = brandMultipleCanonical + ' ' + typeMultipleCanonical + ' ' + categoryName;
							}
							//Brand + Category + Size
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical != '' && typeMultipleCanonical == '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = brandMultipleCanonical + sizeMultipleCanonical + '-Size ' + ' ' + categoryName;
							}
							//Brand + Category + Type + Size
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical != '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = brandMultipleCanonical + ' ' + typeMultipleCanonical + ' ' + sizeMultipleCanonical + '-Size ' + ' ' + categoryName;
							}
							//Brand
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = 'Shop ' + categoryName + ' by ' + brandMultipleCanonical;
							} else {
								var trimTitle = 'Shop ' + categoryName + ' | Mattress Firm';
								multiCanononicalTitle = trimTitle.replace('Shop Shop','Shop');
							}
						}
						////////////////////////////////////////////////////////////////////////////////////////////////////                      
					}
					//////////// for 1800
					if (siteId == '1800Mattress-RV') {
						categoryName = productSearchModel.category.displayName;
						if (refinementBrandArray && refinementBrandArray.length == 1) {
							brandMultipleCanonical = refinementBrandArray[0].value;
						} else {
							brandMultipleCanonical = "";
						}
						if (refinementSizeArray && refinementSizeArray.length == 1) {
							sizeMultipleCanonical = refinementSizeArray[0].value;
						} else {
							sizeMultipleCanonical = "";
						}
						if (refinementTypeArray && refinementTypeArray.length == 1) {
							typeMultipleCanonical = refinementTypeArray[0].value;
						} else {
							typeMultipleCanonical = "";
						}
						if (refinementComfortArray && refinementComfortArray.length == 1) {
							comfortMultipleCanonical = refinementComfortArray[0].value;
						} else {
							comfortMultipleCanonical = "";
						}
						////////////////////////////////////////////////////////////////////////////////////////////////////
						if (categoryName == 'Mattresses') {
							var categoryNameMattress = categoryName;//'Mattress';
							//Size
							if (sizeMultipleCanonical != '' && brandMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = sizeMultipleCanonical + '-Size '+ categoryNameMattress +' Deals | 1800mattress';
							}
							//Type
							else if (sizeMultipleCanonical == '' && brandMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = typeMultipleCanonical + ' '+ categoryNameMattress +' Deals | 1800mattress';	
							}
							//Size + Type
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = sizeMultipleCanonical + '-Size ' + typeMultipleCanonical +' '+ categoryNameMattress +' Deals | 1800mattress';
							}
							//Brand + Size
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical != '' && typeMultipleCanonical == '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = sizeMultipleCanonical +'-Size '+ brandMultipleCanonical+' '+ categoryNameMattress + ' Deals | 1800mattress';
							}
							//Size + Comfort
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = sizeMultipleCanonical +'-Size '+ comfortMultipleCanonical + ' ' + categoryNameMattress + ' Deals | 1800mattress';
							}
							//Size + Type + Comfort
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = sizeMultipleCanonical+'-Size'+' '+comfortMultipleCanonical+ ' '+typeMultipleCanonical+' '+ categoryNameMattress+' Deals';
							}
							//Brand + Size + Type
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical != '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = sizeMultipleCanonical+'-Size' +' '+brandMultipleCanonical+' '+typeMultipleCanonical+' '+categoryNameMattress+' Deals';
							}
							//Brand + Size + Comfort
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical != '' && typeMultipleCanonical == '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical + ' ' + brandMultipleCanonical + ' ' + sizeMultipleCanonical + '-Size '+ categoryNameMattress+' Deals';
							}
							//Brand + Size + Type + Comfort
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical != '' && typeMultipleCanonical != '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical + ' ' + brandMultipleCanonical + ' ' + sizeMultipleCanonical + '-Size ' + typeMultipleCanonical +' '+ categoryNameMattress+' Deals';
							}
							//Brand + Type
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = brandMultipleCanonical + ' ' + typeMultipleCanonical + ' '+ categoryNameMattress+' Deals | 1800mattress';
							}
							//Type + Comfort
							else if (brandMultipleCanonical == '' && sizeMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical+' '+typeMultipleCanonical+' '+categoryNameMattress+' Deals | 1800mattress';
							}
							//Brand + Type + Comfort
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical + ' ' + brandMultipleCanonical + ' ' + typeMultipleCanonical + ' '+categoryNameMattress+ ' Deals | 1800mattress';	
							}
							//Brand
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = brandMultipleCanonical + ' ' +categoryNameMattress + ' Deals | 1800mattress';	
							}
							//Brand + Comfort
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical+' '+brandMultipleCanonical+ ' '+categoryNameMattress+' Deals | 1800mattress';
							}
							//Comfort
							else if (brandMultipleCanonical == '' && sizeMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical+' '+categoryNameMattress+' Deals | 1800mattress';
							} else {
								multiCanononicalTitle = 'Shop ' + categoryNameMattress + ' | 1800mattress';
							}
						}else if (categoryName == 'Sale') {
							var categoryNameMattress = categoryName;
							//Size
							if (sizeMultipleCanonical != '' && brandMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = sizeMultipleCanonical + '-Size '+ categoryNameMattress +' | 1800mattress';
							}
							//Type
							else if (sizeMultipleCanonical == '' && brandMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = typeMultipleCanonical + ' '+ categoryNameMattress +' | 1800mattress';	
							}
							//Size + Type
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = sizeMultipleCanonical + '-Size ' + typeMultipleCanonical +' '+ categoryNameMattress +' | 1800mattress';
							}
							//Brand + Size
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical != '' && typeMultipleCanonical == '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = sizeMultipleCanonical +'-Size '+ brandMultipleCanonical+' '+ categoryNameMattress + ' | 1800mattress';
							}
							//Size + Comfort
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = sizeMultipleCanonical +'-Size '+ comfortMultipleCanonical + ' ' + categoryNameMattress + ' | 1800mattress';
							}
							//Size + Type + Comfort
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = sizeMultipleCanonical+'-Size'+' '+comfortMultipleCanonical+ ' '+typeMultipleCanonical+' '+ categoryNameMattress;
							}
							//Brand + Size + Type
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical != '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = sizeMultipleCanonical+'-Size' +' '+brandMultipleCanonical+' '+typeMultipleCanonical+' '+categoryNameMattress;
							}
							//Brand + Size + Comfort
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical != '' && typeMultipleCanonical == '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical + ' ' + brandMultipleCanonical + ' ' + sizeMultipleCanonical + '-Size '+ categoryNameMattress;
							}
							//Brand + Size + Type + Comfort
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical != '' && typeMultipleCanonical != '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical + ' ' + brandMultipleCanonical + ' ' + sizeMultipleCanonical + '-Size ' + typeMultipleCanonical +' '+ categoryNameMattress;
							}
							//Brand + Type
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = brandMultipleCanonical + ' ' + typeMultipleCanonical + ' '+ categoryNameMattress+' | 1800mattress';
							}
							//Type + Comfort
							else if (brandMultipleCanonical == '' && sizeMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical+' '+typeMultipleCanonical+' '+categoryNameMattress+' | 1800mattress';
							}
							//Brand + Type + Comfort
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical + ' ' + brandMultipleCanonical + ' ' + typeMultipleCanonical + ' '+categoryNameMattress+ ' | 1800mattress';	
							}
							//Brand
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = brandMultipleCanonical + ' ' +categoryNameMattress + ' | 1800mattress';	
							}
							//Brand + Comfort
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical+' '+brandMultipleCanonical+ ' '+categoryNameMattress+' | 1800mattress';
							}
							//Comfort
							else if (brandMultipleCanonical == '' && sizeMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical+' '+categoryNameMattress+' | 1800mattress';
							} else {
								multiCanononicalTitle = 'Shop ' + categoryNameMattress + ' | 1800mattress';
							}
						} else {
							var categoryNameMattress = categoryName;
							//Size
							if (sizeMultipleCanonical != '' && brandMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = sizeMultipleCanonical + '-Size '+ categoryNameMattress +' | 1800mattress';
							}
							//Type
							else if (sizeMultipleCanonical == '' && brandMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = typeMultipleCanonical + ' '+ categoryNameMattress +' | 1800mattress';	
							}
							//Size + Type
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = sizeMultipleCanonical + '-Size ' + typeMultipleCanonical +' '+ categoryNameMattress +' Deals | 1800mattress';
							}
							//Brand + Size
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical != '' && typeMultipleCanonical == '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = sizeMultipleCanonical +'-Size '+ brandMultipleCanonical+' '+ categoryNameMattress + ' | 1800mattress';
							}
							//Size + Comfort
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = sizeMultipleCanonical +'-Size '+ comfortMultipleCanonical + ' ' + categoryNameMattress + ' | 1800mattress';
							}
							//Size + Type + Comfort
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = sizeMultipleCanonical+'-Size'+' '+comfortMultipleCanonical+ ' '+typeMultipleCanonical+' '+ categoryNameMattress;
							}
							//Brand + Size + Type
							else if (sizeMultipleCanonical != '' && brandMultipleCanonical != '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = sizeMultipleCanonical+'-Size' +' '+brandMultipleCanonical+' '+typeMultipleCanonical+' '+categoryNameMattress;
							}
							//Brand + Size + Comfort
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical != '' && typeMultipleCanonical == '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical + ' ' + brandMultipleCanonical + ' ' + sizeMultipleCanonical + '-Size '+ categoryNameMattress;
							}
							//Brand + Size + Type + Comfort
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical != '' && typeMultipleCanonical != '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical + ' ' + brandMultipleCanonical + ' ' + sizeMultipleCanonical + '-Size ' + typeMultipleCanonical +' '+ categoryNameMattress;
							}
							//Brand + Type
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = brandMultipleCanonical + ' ' + typeMultipleCanonical + ' '+ categoryNameMattress+' | 1800mattress';
							}
							//Type + Comfort
							else if (brandMultipleCanonical == '' && sizeMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical+' '+typeMultipleCanonical+' '+categoryNameMattress+' | 1800mattress';
							}
							//Brand + Type + Comfort
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical != '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical + ' ' + brandMultipleCanonical + ' ' + typeMultipleCanonical + ' '+categoryNameMattress+ ' | 1800mattress';	
							}
							//Brand
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical == '') {
								multiCanononicalTitle = brandMultipleCanonical + ' ' +categoryNameMattress + ' | 1800mattress';	
							}
							//Brand + Comfort
							else if (brandMultipleCanonical != '' && sizeMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical+' '+brandMultipleCanonical+ ' '+categoryNameMattress+' | 1800mattress';
							}
							//Comfort
							else if (brandMultipleCanonical == '' && sizeMultipleCanonical == '' && typeMultipleCanonical == '' && comfortMultipleCanonical != '') {
								multiCanononicalTitle = comfortMultipleCanonical+' '+categoryNameMattress+' | 1800mattress';
							} else {
								multiCanononicalTitle = 'Shop ' + categoryNameMattress + ' | 1800mattress';
							}
						} 
						////////////////////////////////////////////////////////////////////////////////////////////////////                      
					}
					//here finla multiple canonical rule based title
					this.data.page.title = multiCanononicalTitle.replace('Shop Shop','Shop').replace('Size-Size','Size').replace('Extra Long-Size','Extra Long');
				}
			} else {
				this.data.page.title = title;
			}
			if ('pageKeywords' in object && object.pageKeywords) {
				this.data.page.keywords = object.pageKeywords;
			}
			if (dw.system.Site.getCurrent().name == 'Mattress Firm' && 'product' in object && object.product) {
				if (jcCategoryTest('Mattresses', object) == true) {
					if (!empty(object.custom.mattress_type)) {
						this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Mattress Firm. This ' + object.custom.mattress_type[0].toLowerCase() + ' mattress equals a great nights sleep.';
					} else {
						this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Mattress Firm. This ' + '' + ' mattress equals a great nights sleep.';
					}
				} else if (jcCategoryTest('Pillows', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Mattress Firm. This pillow will keep you cool & comfortable for an amazing sleep.';
				} else if (jcCategoryTest('Pillow Cases', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Mattress Firm. This breathable pillowcase comes in a variety of colors to match your bedroom.';
				} else if (jcCategoryTest('Bed Frames', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Mattress Firm. It\'s easy-to-assemble & sturdy for maximum durability.';
				} else if (jcCategoryTest('Comforters', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Mattress Firm. This cozy comforter will keep you wrapped in warmth.';
				} else if (jcCategoryTest('Bed Sheets', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Mattress Firm. These soft sheets will keep you wrapped in comfort.';
				} else if (jcCategoryTest('Bunk Beds', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Mattress Firm. Save some space without sacrificing style or durability.';
				} else if (jcCategoryTest('Adjustable Beds', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Mattress Firm. Adjust your sleeping or reading position for maximum comfort.';
				} else if (jcCategoryTest('Mattress Toppers', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Mattress Firm. Add a quality layer for maximum sleep comfort.';
				} else if (jcCategoryTest('Mattress Protectors', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Mattress Firm. Protect your mattress from liquids, dust mites, & allergens.';
				} else if (jcCategoryTest('Dog Beds', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Mattress Firm. Give your best friend the best comfort that will help them sleep soundly & keep them happy.';
				} else if (jcCategoryTest('Massage Chairs', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Mattress Firm. Come home after a long day & unwind in this relaxing chair.';
				} else if (jcCategoryTest('Sofas & Loveseats', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Mattress Firm. It\'s classic, homey style will perfectly match your home decor.';
				} else if (jcCategoryTest('Leather Chairs & Ottomans', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Mattress Firm. It\'s classic, homey style will perfectly match your home decor.';
				} else if (jcCategoryTest('Office Chairs', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Mattress Firm. It\'s classic, homey style will perfectly match your home decor.';
				} else if (jcCategoryTest('Bed Sets & Headboards', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Mattress Firm. It\'s stylish & sturdy for maximum durability.';
				} else if ('pageDescription' in object && object.pageDescription) {
					this.data.page.description = object.pageDescription;
				}
			} else if (dw.system.Site.getCurrent().name == 'Dream Bed' && 'product' in object && object.product) {
				if (jcCategoryTest('Mattresses', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Dream Bed. This ' + object.custom.mattress_type[0].toLowerCase() + ' mattress equals a great nights sleep.';
				} else if (jcCategoryTest('Pillows', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Dream Bed. This pillow will keep you cool & comfortable for an amazing sleep.';
				} else if (jcCategoryTest('Pillow Cases', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Dream Bed. This breathable pillowcase comes in a variety of colors to match your bedroom.';
				} else if (jcCategoryTest('Bed Frames', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Dream Bed. It\'s easy-to-assemble & sturdy for maximum durability.';
				} else if (jcCategoryTest('Comforters', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Dream Bed. This cozy comforter will keep you wrapped in warmth.';
				} else if (jcCategoryTest('Bed Sheets', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Dream Bed. These soft sheets will keep you wrapped in comfort.';
				} else if (jcCategoryTest('Bunk Beds', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Dream Bed. Save some space without sacrificing style or durability.';
				} else if (jcCategoryTest('Adjustable Beds', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Dream Bed. Adjust your sleeping or reading position for maximum comfort.';
				} else if (jcCategoryTest('Mattress Toppers', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Dream Bed. Add a quality layer for maximum sleep comfort.';
				} else if (jcCategoryTest('Mattress Protectors', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Dream Bed. Protect your mattress from liquids, dust mites, & allergens.';
				} else if (jcCategoryTest('Dog Beds', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Dream Bed. Give your best friend the best comfort that will help them sleep soundly & keep them happy.';
				} else if (jcCategoryTest('Massage Chairs', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Dream Bed. Come home after a long day & unwind in this relaxing chair.';
				} else if (jcCategoryTest('Sofas & Loveseats', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Dream Bed. It\'s classic, homey style will perfectly match your home decor.';
				} else if (jcCategoryTest('Leather Chairs & Ottomans', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Dream Bed. It\'s classic, homey style will perfectly match your home decor.';
				} else if (jcCategoryTest('Office Chairs', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Dream Bed. It\'s classic, homey style will perfectly match your home decor.';
				} else if (jcCategoryTest('Bed Sets & Headboards', object) == true) {
					this.data.page.description = 'Shop for your ' + object.manufacturerName + ' ' + object.name + ' at Dream Bed. It\'s stylish & sturdy for maximum durability.';
				} else if ('pageDescription' in object && object.pageDescription) {
					this.data.page.description = object.pageDescription;
				}
			} else if ('pageDescription' in object && object.pageDescription) {
				this.data.page.description = object.pageDescription;
			}
			this.updatePageMetaData();
			// Update breadcrumbs for content
			if (object.class === dw.content.Content) {
				var path = require('~/cartridge/scripts/models/ContentModel').get(object).getFolderPath();
				this.data.breadcrumbs = path.map(function(folder) {
					return {
						name: folder.displayName
					};
				});
				this.data.breadcrumbs.unshift(HOME_BREADCRUMB);
				this.data.breadcrumbs.push({
					name: object.name,
					url: dw.web.URLUtils.url('Page-Show', 'cid', object.ID)
				});
				dw.system.Logger.debug('Content breadcrumbs calculated: ' + JSON.stringify(this.data.breadcrumbs));
			}
		} else if (typeof object === 'string') {
			// @TODO Should ideally allow to pass something like account.overview, account.wishlist etc.
			// and at least generate the breadcrumbs & page title
		} else {
			if (object.pageTitle) {
				this.data.page.title = object.pageTitle;
			}
			if (object.pageKeywords) {
				this.data.page.keywords = object.pageKeywords;
			}
			if (object.pageDescription) {
				this.data.page.description = object.pageDescription;
			}
			// @TODO do an _.extend(this.data, object) of the passed object
		}
	},
	/**
	 * Update the Page Metadata with the current internal data
	 */
	updatePageMetaData: function() {
		var pageMetaData = request.pageMetaData;
		pageMetaData.title = this.data.page.title;
		pageMetaData.keywords = this.data.page.keywords;
		pageMetaData.description = this.data.page.description;
	},
	/**
	 * Get the breadcrumbs for the current page
	 *
	 * @return {Array} an array containing the breadcrumb items
	 */
	getBreadcrumbs: function() {
		return this.data.breadcrumbs || [];
	},
	/**
	 * Adds a resource key to meta, the key of the given bundle is simply dumped to a data
	 * attribute and can be consumed by the client.
	 *
	 * @example
	 * // on the server
	 * require('meta').addResource('some.message.key', 'bundlename');
	 * // on the client
	 * console.log(app.resources['some.message.key']);
	 *
	 * @param {string} key          The resource key
	 * @param {string} bundle       The bundle name
	 * @param {string} defaultValue Optional default value, empty string otherwise
	 */
	addResource: function(key, bundle, defaultValue) {
		this.data.resources[key] = dw.web.Resource.msg(key, bundle, defaultValue || '');
	},
	/**
	 * Dumps the internally held data into teh DOM
	 *
	 * @return {String} A div with a data attribute containing all data as JSON
	 */
	renderClientData: function() {
		return '<div class="page-context" data-dw-context="' + JSON.stringify(this.data) + '" />';
	}
};

function jcCategoryTest(keyword, product) {
		var masterProduct = product.variationModel.getMaster();
		var jcTest: Bolean = false;
		if(!empty(masterProduct)) {
			var jcArray: Array = product.variationModel.getMaster().allCategoryAssignments;
			var jcStr: String;
			for each(var jcCategory in jcArray) {
				jcStr = jcCategory.getCategory().displayName;
				if (jcStr == keyword) {
					jcTest = true;
					break;
				}
			}
		}
		return jcTest;
	}
	/**
	 * Singleton instance for meta data handling
	 * @type {module:meta~Meta}
	 */
module.exports = new Meta();