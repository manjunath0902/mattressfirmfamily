'use strict';

/**
 * The init Firewall function.
 *
 * @module  firewall/init
 */
//var ipRangeCheck = require('~/cartridge/scripts/util/firewall/Firewall');
var Firewall = require('./FirewallInit');
var Site = require('dw/system/Site');
var Status = require('dw/system/Status');
var IsIPFromRange = false;
var IsIPFromList = false;

/**
 * The init Firewall function.
 */
exports.init = function () {
	var preferences = Site.getCurrent().getPreferences().getCustom();
	if(!request.includeRequest) {
		if('enableMsosBlockingMode' in preferences && preferences['enableMsosBlockingMode']) {
			var remoteIpAddress = request.httpRemoteAddress;
			var ipRanges = (preferences['whitelistedIPRangesMsos'].length > 0) ? preferences['whitelistedIPRangesMsos'] : null;
			var individualIPs = (preferences['whitelistedIndividualIPsMsos'] && preferences['whitelistedIndividualIPsMsos'].length > 0) ? preferences['whitelistedIndividualIPsMsos'].split(',') : null;
			
			if(remoteIpAddress) {
				//Validate IP from Given Ranges
				IsIPFromRange = ipRanges ? Firewall(remoteIpAddress, ipRanges) : false;
				
				//Validate IP from Given List
				IsIPFromList = (individualIPs && individualIPs.indexOf(remoteIpAddress) !== -1) ? true : false;
			}
		}
		else {
			IsIPFromRange = true;
			IsIPFromList = true;
		}
	}
	
	if(!(IsIPFromRange || IsIPFromList)) {
		response.setStatus(403);
		return;
	}
    return new Status(Status.OK);
};
