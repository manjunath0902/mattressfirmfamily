importPackage( dw.svc );
importPackage( dw.system );
importPackage( dw.object );
importPackage( dw.util );
importPackage( dw.crypto );

var SFEmailSubscriptionHelper = {
	sendSFEmailInfo : function(obj) : Object {
		//importScript("app_storefront_core:util/ViewHelpers.ds");
		//debugHelper();
				
		var emailAddress = obj.emailAddress;
		var firstName = obj.firstName;
		var zipCode = obj.zipCode;
		var zipCodeModal = obj.zipCodeModal;
		var leadSource = obj.leadSource;
		var leadCreative = obj.leadCreative;
		var leadPage = obj.leadPage;
		var siteId = obj.siteId;
		var optOutFlag = obj.optOutFlag;
		var leadDate = obj.leadDate;
		var gclid = obj.gclid;
		var dwsid = obj.dwsid;
		
		var service : Service;
		var result  :  Result;
		var recordTypeIdCustomer = Site.current.preferences.custom.sfCustomerRecordTypeID; //'012G00000017CrRIAU';
		var recordTypeIdLead = Site.current.preferences.custom.sfLeadRecordTypeID; //'012G0000001QVPXIA4';
		var returnObject : Object = {};		
		var sitePrefix : String;
		
		//not currently used, but here in case other sites are addded in the future
		//prefix would be used for the custom fields eg. MFRM_Lead_Source__c
		if (siteId == 'Mattress-Firm') {
			sitePrefix = 'MFRM';
		} else if (siteId == 'Tulo') {
			sitePrefix = 'tulo';
		} else {
	    	returnObject.Status = "ERROR";		
			returnObject.ErrorCode = "NOT_MFRM_TULO";
	    	return returnObject;
		}
		
		if (empty(optOutFlag)) {
			optOutFlag = false;
		}
		
		if (empty(obj.leadDate)) {
			leadDate = dw.system.Site.calendar;
			leadDate.setTimeZone("UTC");
		}
				
		if (empty(emailAddress)) {
			returnObject.Status = "ERROR";
			returnObject.ErrorCode = "EMPTY_EMAIL";
			return returnObject;
		}
		emailAddress = emailAddress.toLowerCase().trim();


				
		var responseOAuth = SFEmailSubscriptionHelper.getToken();
		
		if (responseOAuth.Status != "OK"){
			returnObject.Status = responseOAuth.Status;
			returnObject.Msg = "Error retrieving token";
			returnObject.ErrorCode = responseOAuth.ErrorCode;
			returnObject.ContactID = "";
			return returnObject;			
		}

		var sfServiceName = responseOAuth.sfServiceName;
		var token : String = responseOAuth.access_token;
		var url   : String = responseOAuth.instance_url; 		 
		//
		//	SF Contacts
		//
		
		var queryContact = "select Id, OtherPostalCode, RecordTypeId, email, FirstName, LastName, MFRM_Lead_Source__c, MFRM_Lead_Creative__c, MFRM_Lead_Page__c, MFRM_Lead_Date__c, tulo_Lead_Source__c, tulo_Lead_Date__c, EmailVerificationCode__c, Persona_Sleep_Style__c, Persona_Mattress_Preference__c, HasOptedOutOfEmail from contact where email = '" + emailAddress + "'";
		result = SFEmailSubscriptionHelper.getData(sfServiceName, url, token, queryContact);		  		 
	    var responseContacts : object = JSON.parse(result.object);
		Logger.info("query contact results: " + JSON.stringify(JSON.parse(result.object)));
	    
		if (responseContacts) {
			
			var updValsObj = {		
				_leadDate : leadDate,
				_leadSource : leadSource,
				_leadCreative : leadCreative,
				_leadPage : leadPage,
				_optOutFlag : optOutFlag,
				_gclid : gclid,
				_dwsid : dwsid,
				_zipCode : !zipCode ? undefined : zipCode,
				_zipCodeModal : !zipCodeModal ? undefined : zipCodeModal,
				_sitePrefix : sitePrefix,
				_firstName : !firstName ? undefined : firstName
			};
		
		    if (responseContacts.totalSize == 0) {
				if (sitePrefix.toUpperCase() == 'MFRM') {	    	
			    	var restArgs = {
			    		 	method: 'POST',
			    		 	type: 'CREATE',
			    		 	body : {     	
			    		        RecordTypeId: recordTypeIdLead,
			    		        Email: emailAddress,
			    		        FirstName: firstName,
			    		        LastName: 'Email Lead',
			    		        OtherPostalCode: !zipCodeModal ? zipCode : zipCodeModal,
			    		        MFRM_Lead_Source__c: leadSource,
			    		        MFRM_Lead_Creative__c: leadCreative,
			    		        MFRM_Lead_Page__c: leadPage,
			    		        MFRM_Lead_Date__c: leadDate.getTime(),
			    		        HasOptedOutOfEmail: optOutFlag
			    		        //Google_Client_ID__c: gclid,
			    		        //DW_Session_ID__c: dwsid
			    		 	}
			    	 };
				} else if (sitePrefix.toUpperCase() == 'TULO') {	    	
			    	var restArgs = {
			    		 	method: 'POST',
			    		 	type: 'CREATE',
			    		 	body : {     	
			    		        RecordTypeId: recordTypeIdLead,
			    		        Email: emailAddress,
			    		        //FirstName: firstName,
			    		        LastName: 'Email Lead',
			    		        OtherPostalCode: zipCode,
			    		        tulo_Lead_Source__c: leadSource,
			    		        tulo_Lead_Date__c: leadDate.getTime(),
			    		        HasOptedOutOfEmail: optOutFlag
			    		        //Google_Client_ID__c: gclid,
			    		        //DW_Session_ID__c: dwsid
			    		 	}
			    	 };
				}
				
	    	    //
	    	    // Create Contact
	    	    //
	    		service = ServiceRegistry.get(sfServiceName);	// reset the service to remove oauth service params
	    		service.URL = url + "/services/data/v40.0/sobjects/Contact/";
	    		service.addHeader("Authorization", "Bearer " + token);       
	    		service.addHeader("Content-Type", "application/json");
	    		
	    		result = service.call(restArgs);
	    		
	    		var contactId = JSON.parse(result.object).id;		 
	        						
		    	returnObject.Status = "OK";		
		    	returnObject.Msg = "";
				returnObject.ErrorCode = "";
				returnObject.ContactID = contactId;
		    	
		    } else if (responseContacts.totalSize == 1) {	
		    	//if record is customer then don't update zip
		
		    	if (responseContacts.records[0].RecordTypeId == recordTypeIdCustomer) {
		    		updValsObj._zipCode = undefined;
		    		updValsObj._firstName = undefined;
		    		var result = SFEmailSubscriptionHelper.updateContacts(sfServiceName, url, token, responseContacts, updValsObj);
		    	} else {
		    		var result = SFEmailSubscriptionHelper.updateContacts(sfServiceName, url, token, responseContacts, updValsObj);
		    	}
				
		    	var siteExists = (sitePrefix.toUpperCase() == 'MFRM' &&  responseContacts.records[0].MFRM_Lead_Source__c != null) || (sitePrefix.toUpperCase() == 'TULO' &&  responseContacts.records[0].tulo_Lead_Source__c != null);
				
		    	if (siteExists) {
			    	returnObject.Status = "ERROR";		   
			    	returnObject.Msg = "EXISTS: " + responseContacts.records[0].Id + ' ' + siteId;	
					returnObject.ErrorCode = "exists";
					returnObject.ContactID = result.ContactID;
		    	} else {
			    	returnObject.Status = "OK";		
					returnObject.Msg = "";
					returnObject.ErrorCode = "";
					returnObject.ContactID = result.ContactID;
		    	}
		    } else {
	
				result = SFEmailSubscriptionHelper.getData(sfServiceName, url, token, queryContact + " and RecordTypeId = '" + recordTypeIdCustomer + "' order by LastModifiedDate desc, " + sitePrefix + "_Lead_Date__c desc limit 1");
				var responseCustomerContacts : object = JSON.parse(result.object);
					    
				
				if (responseCustomerContacts.totalSize == 0) {
					//Its all Lead records, Update them all
		    		var result = SFEmailSubscriptionHelper.updateContacts(sfServiceName, url, token, responseContacts, updValsObj);
				} else {
					//Update only the customer records, Don't Update zip and leadsource on customer records
					updValsObj._zipCode = undefined;
					updValsObj._firstName = undefined;
					var result = SFEmailSubscriptionHelper.updateContacts(sfServiceName, url, token, responseCustomerContacts, updValsObj);
				}
	
		    	var siteExists = (sitePrefix.toUpperCase() == 'MFRM' &&  responseContacts.records[0].MFRM_Lead_Source__c != null) || (sitePrefix.toUpperCase() == 'TULO' &&  responseContacts.records[0].tulo_Lead_Source__c != null);
		    	if (siteExists) {
			    	returnObject.Status = "ERROR";		
			    	returnObject.Msg = "MANY EXISTS" + ' ' + siteId;
					returnObject.ErrorCode = "exists";
					returnObject.ContactID = result.ContactID;
		    	} else {
			    	returnObject.Status = "OK";		
			    	returnObject.Msg = "";
					returnObject.ErrorCode = "";
					returnObject.ContactID = result.ContactID;	
		    	}
		    }  
		}
	    return returnObject;
	},

	//**************************************************************************
	getToken : function () : Object {
		var service : Service;
		var result  : Result;
		var sfServiceName = Site.current.preferences.custom.sfAPIServiceName;
		var returnObject : Object = {};
	
    	/*
    	returnObject.Status = "OK";		
    	returnObject.ErrorCode = "";
    	returnObject.sfServiceName = sfServiceName;
    	returnObject.access_token = "00DM0000001f8dK!AQgAQPRPqdvxkljpyhmmQOSFxuBrTGV51ECopYMVWY4HPX2HGhi74aQBixTTZpRVu4cRrY7Hg6_CWFKoZz.Crbfe.Dz6I4PA";
    	returnObject.instance_url = "https://mattress--DEV012.cs7.my.salesforce.com";
   		return returnObject;
		*/
		
		if (Site.current.preferences.custom.sfAPIInstance.toLowerCase() == 'dev') {
			var sfClientId = Site.current.preferences.custom.sfAPIDevClientID;
			var sfSecret = Site.current.preferences.custom.sfAPIDevSecret;
			var sfUsername = Site.current.preferences.custom.sfAPIDevUsername;
			var sfPassword = Site.current.preferences.custom.sfAPIDevPassword;
		} else {
			var sfClientId = Site.current.preferences.custom.sfAPIProdClientID;
			var sfSecret = Site.current.preferences.custom.sfAPIProdSecret;
			var sfUsername = Site.current.preferences.custom.sfAPIProdUsername;
			var sfPassword = Site.current.preferences.custom.sfAPIProdPassword;			
		}
		//
		//	SF OAuth Authentication
		//
		try {
			service = ServiceRegistry.get(sfServiceName);
			service.addParam('grant_type', 'password')
				.addParam('client_id', sfClientId)
				.addParam('client_secret', sfSecret)
				.addParam('username', sfUsername)
				.addParam('password', sfPassword);
			
			var restArgs = {
			 	method: 'POST',
			 	type: 'OAUTH',
			 	body: ''
			}
        } catch (e) {
            returnObject.Status = "SERVICE_ERROR";        
            returnObject.ErrorCode = "SERVICE_ERROR";                
            return returnObject;
        }
			
		try {
			result = service.call(restArgs);
		} catch (e) {
	    	returnObject.Status = "SERVICE_ERROR";		
	    	returnObject.ErrorCode = "SERVICE_ERROR";			
            return returnObject;
		}
		
		if (!result.ok){
			var access_token : String = '';
			var instance_url   : String = '';
		
	    	returnObject.Status = "SERVICE_ERROR";		
	    	returnObject.ErrorCode = "SERVICE_ERROR";	    	
	    	return returnObject;
		}
		
	 	var responseOAuth : object = JSON.parse(result.object);
		var access_token : String = responseOAuth.access_token;
		var instance_url   : String = responseOAuth.instance_url; 
		
    	returnObject.Status = "OK";		
    	returnObject.ErrorCode = "";
    	returnObject.sfServiceName = sfServiceName;
    	returnObject.access_token = access_token;
    	returnObject.instance_url = instance_url;
    	
		return returnObject;
		
	},
	//**************************************************************************

	sendSFQuestions : function(obj) : Object {
		//importScript("app_storefront_core:util/ViewHelpers.ds");
		//debugHelper();
		var returnObject : Object = {};
		var contactID = obj.contactID;
		if (typeof contactID == "string") {
			if (contactID == '') {
				returnObject.Status = 'SERVICE_ERROR';
				returnObject.Msg = "NO CONTACT ID";
				returnObject.ErrorCode = 'NO_CONTACT_ID';
				returnObject.ContactID = null;
				return returnObject;						
			}
		} else {
			if (contactID.value == '') {
				returnObject.Status = 'SERVICE_ERROR';
				returnObject.Msg = "NO CONTACT ID";
				returnObject.ErrorCode = 'NO_CONTACT_ID';
				returnObject.ContactID = null;
				return returnObject;						
			}
		}
		var datetime : Calendar = dw.system.Site.calendar;
		datetime.setTimeZone("UTC");
		var updValsObj = {};
		if (typeof obj.questions == "string") {
			var questions : [] = JSON.parse(obj.questions);
		} else {
			var questions : [] = JSON.parse(obj.questions.value);
		}
		var isComplete = true;
		for (var i=0; i<questions.length; i++) {
			updValsObj[questions[i].field] = questions[i].answer;
			isComplete = (isComplete && (questions[i].answer!=null && questions[i].answer!=""));
		}
		
		if (isComplete) {
			updValsObj['MFRM_Lead_Modal_2_Date__c'] = datetime.getTime();
		} else {
			updValsObj['MFRM_Lead_Modal_2_Date__c'] = null;
		}
		updValsObj['MFRM_Lead_Modal_2_Complete__c'] = isComplete;

		var responseOAuth = SFEmailSubscriptionHelper.getToken();
		
		if (responseOAuth.Status != "OK"){
			returnObject.Status = responseOAuth.Status;
			returnObject.Msg = "Error retrieving token";
			returnObject.ErrorCode = responseOAuth.ErrorCode;
			returnObject.ContactID = contactID;
			return returnObject;			
		}
		
		var sfServiceName = responseOAuth.sfServiceName;
		var token : String = responseOAuth.access_token;
		var url   : String = responseOAuth.instance_url; 		 

		var result = SFEmailSubscriptionHelper.updateQuestions(sfServiceName, url, token, contactID, updValsObj);
		
		returnObject.Status = "OK";
		returnObject.Msg = "";
		returnObject.ErrorCode = "";
		returnObject.ContactID = "";
		return returnObject;
	},
	//**************************************************************************

	getData : function(_sfServiceName, _url, _token, _q) : Result {
		var service = ServiceRegistry.get(_sfServiceName);	// reset the service to remove oauth service params
		var query : String = Encoding.toURI(_q);
		service.URL = _url + "/services/data/v40.0/query?q=" + query;
		service.addHeader("Authorization", "Bearer " + _token);
		var restArgs = {
		 	method: 'GET',
		 	type: 'QUERY',
		 	body: ''
		}
		var result = service.call(restArgs);
		return result;
	},

	//**************************************************************************
	updateQuestions : function (_sfServiceName, _url, _token, _contactID, _updValsObj) : Object {
		//importScript("app_storefront_core:util/ViewHelpers.ds");
		//debugHelper();
		service = ServiceRegistry.get(_sfServiceName);	// reset the service to remove oauth service params
		service.URL = _url + "/services/data/v40.0/composite/batch";
		service.addHeader("Authorization", "Bearer " + _token);       
		service.addHeader("Content-Type", "application/json");
		
		var restArgs = {
			 	method: 'PATCH',
			 	type: 'BATCH_UPDATE',
			 	body : {
			 		batchRequests : []
			 	}
			};
		var j = { 
				method: 'PATCH',
				url: "v40.0/sobjects/Contact/" + _contactID,
		        richInput : _updValsObj
			};
		restArgs.body.batchRequests[0]= j;
		try {
			var newResult = service.call(restArgs);
			// Add ContactID to the result object to pass to SFMC
			newResult = JSON.parse(newResult.object);
			newResult.ContactID = _contactID;
		} catch (e) {
			var newResult = {Status: 'ERROR', Msg: e, ErrorCode: '', object: {}};
		}
		return newResult;
	},

	//**************************************************************************
	updateContacts : function (_sfServiceName, _url, _token, _responseContacts, _updValsObj) : Object {
		
		service = ServiceRegistry.get(_sfServiceName);	// reset the service to remove oauth service params
		service.URL = _url + "/services/data/v40.0/composite/batch";
		service.addHeader("Authorization", "Bearer " + _token);       
		service.addHeader("Content-Type", "application/json");
		
		var _leadDate = _updValsObj._leadDate;
		var _leadSource = _updValsObj._leadSource;
		var _leadCreative = _updValsObj._leadCreative;
		var _leadPage = _updValsObj._leadPage;
		var _optOutFlag = _updValsObj._optOutFlag;
		var _gclid = _updValsObj._gclid;
		var _dwsid = _updValsObj._dwsid;
		var _zipCode = _updValsObj._zipCode;
		var _zipCodeModal = _updValsObj._zipCodeModal;
		var _sitePrefix = _updValsObj._sitePrefix;
		var _firstName = _updValsObj._firstName;
		
		var restArgs = {
		 	method: 'PATCH',
		 	type: 'BATCH_UPDATE',
		 	body : {
		 		batchRequests : []
		 	}
		};
		
		var batchIndex = 0;
		for (var i in _responseContacts.records) {
			var contactId = _responseContacts.records[i].Id;
			
			var recCalendar : Calendar = dw.system.Site.calendar;
			recCalendar.setTimeZone("UTC");
			try {
				if (_sitePrefix.toUpperCase() == 'MFRM') {
					recCalendar.parseByFormat(_responseContacts.records[i].MFRM_Lead_Date__c,  "yyyy-MM-dd'T'HH:mm:ss");
				} else if (_sitePrefix.toUpperCase() == 'TULO') {
					recCalendar.parseByFormat(_responseContacts.records[i].tulo_Lead_Date__c,  "yyyy-MM-dd'T'HH:mm:ss");
				}
			} catch (e) {
				recCalendar = _leadDate;				
			}

			if (_leadDate.after(recCalendar) || _leadDate.equals(recCalendar) ) {
	    		if (_sitePrefix.toUpperCase() == 'MFRM') {
					var j = { 
							method: 'PATCH',
							url: "v40.0/sobjects/Contact/" + contactId,
					        richInput : {
					        	OtherPostalCode: (_zipCodeModal == undefined && _responseContacts.records[i].OtherPostalCode ? _responseContacts.records[i].OtherPostalCode : (_zipCodeModal == undefined ? _zipCode : _zipCodeModal)),
					        	MFRM_Lead_Source__c: _leadSource,
					        	MFRM_Lead_Creative__c: _leadCreative,
					        	MFRM_Lead_Page__c: _leadPage,
					        	MFRM_Lead_Date__c: _leadDate.getTime(),
					        	HasOptedOutOfEmail: _optOutFlag,
					        	FirstName : _firstName
					        	//Google_Client_ID__c: _gclid,
					        	//DW_Session_ID__c: _dwsid
					        }
						};
	    		} else if (_sitePrefix.toUpperCase() == 'TULO') {
					var j = { 
							method: 'PATCH',
							url: "v40.0/sobjects/Contact/" + contactId,
					        richInput : {
					        	OtherPostalCode: _zipCode,
					        	tulo_Lead_Source__c: _leadSource,
					        	tulo_Lead_Date__c: _leadDate.getTime(),
					        	HasOptedOutOfEmail: _optOutFlag
					        	//Google_Client_ID__c: _gclid,
					        	//DW_Session_ID__c: _dwsid
					        }
						};
				}
				restArgs.body.batchRequests[batchIndex++]= j;
				
	    	}

		}
		
		try {
			var newResult = service.call(restArgs);
			// Add ContactID to the result object to pass to SFMC
			newResult = JSON.parse(newResult.object);
			newResult.ContactID = contactId;
		} catch (e) {
			var newResult = {Status: 'ERROR', Msg: e, ErrorCode: '', object: {}};
		}
		return newResult;
	},

	//**************************************************************************
	

	sendQuestionsFailSafe : function(obj, source, errorMsg) : Object {
		//importScript("app_storefront_core:util/ViewHelpers.ds");
		//debugHelper();
		var returnObject : Object = {};
		var contactID = obj.contactID ? obj.contactID : null;

		var yyyyMMdd : String = StringUtils.formatCalendar(new Calendar(),'yyyyMMdd');
		var sysTime : String = StringUtils.formatCalendar(new Calendar(),'yyyyMMdd-HHmmss');
		try{
			var leadNurtureObject : Object = CustomObjectMgr.getCustomObject('SFQuestions', yyyyMMdd + '-0');
			var i = 0;
			while(leadNurtureObject !== null && leadNurtureObject.custom.data.length === 200){
				i++;
				leadNurtureObject = CustomObjectMgr.getCustomObject('SFQuestions', yyyyMMdd + '-' + i);
			}
	
			Transaction.wrap(function () {
				if(leadNurtureObject === null){
					leadNurtureObject = CustomObjectMgr.createCustomObject('SFQuestions', yyyyMMdd + '-' + i);
				}
	
				var leadNurtureQuestions : Array = new Array();
	
				for each(var questionRecords : String in leadNurtureObject.custom.data) {
					leadNurtureQuestions.push(questionRecords);
				}
				
				
				leadNurtureQuestions.push(obj.contactID + '|' + session.sessionID + '|' + obj.questions.value + '|' + source + '|' + sysTime + '|' + errorMsg);
	
				leadNurtureObject.custom.data = leadNurtureQuestions;
	
				returnObject.Status = 'OK';
				returnObject.ContactID = null;
				
			});
		} catch(e){
			Logger.error('sendQuestionsFailSafe error:' + e);
			returnObject.Status = "ERROR";
			returnObject.ContactID = null;
			return returnObject;
		}
		return returnObject;
	},
	
	//**************************************************************************

	sendFailSafe : function(obj, errorMsg) : Object {
		var emailAddress = obj.emailAddress;
		var zipCode = obj.zipCode;
		var leadSource = obj.leadSource;
		var leadCreative = obj.leadCreative;
		var leadPage = obj.leadPage;
		var optOutFlag = obj.optOutFlag;
		var gclid = obj.gclid;
		var dwsid = obj.dwsid;
		var firstName = obj.firstName;

		var returnObject : Object = {};
		if (empty(emailAddress)) {
			returnObject.Status = "ERROR";
			return returnObject;
		}
		if (empty(optOutFlag)) {
			optOutFlag = false;
		}
		var yyyyMMdd : String = StringUtils.formatCalendar(new Calendar(),'yyyyMMdd');
		var sysTime : String = StringUtils.formatCalendar(new Calendar(),'yyyyMMdd-HHmmss');
		try{
			var emailSubscriptions : Object = CustomObjectMgr.getCustomObject('SFEmailSubscriptions', yyyyMMdd + '-0');
			var i = 0;
			while(emailSubscriptions !== null && emailSubscriptions.custom.data.length === 200){
				i++;
				emailSubscriptions = CustomObjectMgr.getCustomObject('SFEmailSubscriptions', yyyyMMdd + '-' + i);
			}

			Transaction.wrap(function () {
				if(emailSubscriptions === null){
					emailSubscriptions = CustomObjectMgr.createCustomObject('SFEmailSubscriptions', yyyyMMdd + '-' + i);
				}

				var emailAddresses : Array = new Array();

				for each(var email : String in emailSubscriptions.custom.data) {
					emailAddresses.push(email);
				}
				
				emailAddresses.push(emailAddress + '|' + zipCode + '|' + leadSource + '|' + optOutFlag + '|' + sysTime + '|' + gclid + '|' + dwsid + '|' + firstName + '|' + leadCreative + '|' + leadPage + '|' + errorMsg);

				emailSubscriptions.custom.data = emailAddresses;

				returnObject.Status = 'OK';
			});
		} catch(e){
			Logger.error('sendFailSafe error:' + e);
			returnObject.Status = "ERROR";
			return returnObject;
		}
		return returnObject;
	},
	
	//**************************************************************************

	processQuestionsFailSafe : function () : Object {
		//importScript("app_storefront_core:util/ViewHelpers.ds");
		//debugHelper();
		var returnObject : Object = {};

		var sysTime : String = StringUtils.formatCalendar(new Calendar(),'yyyyMMdd-HHmmss');
		try {
			var leadNurtureCustomObject : Object = CustomObjectMgr.getAllCustomObjects('SFQuestions');
		} catch (e) {
			returnObject.Status = 'ERROR';
			returnObject.Msg = e;
			return returnObject;
		}
		var cnt = 0;
		
		if (leadNurtureCustomObject !== null) {
			var responseOAuth = SFEmailSubscriptionHelper.getToken();
			if (responseOAuth.Status != "OK"){
				returnObject.Status = responseOAuth.Status;
				returnObject.Msg = "Error retrieving token";
				returnObject.ErrorCode = responseOAuth.ErrorCode;
				returnObject.ContactID = "";
				return returnObject;			
			}

			var sfServiceName = responseOAuth.sfServiceName;
			var token : String = responseOAuth.access_token;
			var url   : String = responseOAuth.instance_url; 		 

			while(leadNurtureCustomObject.hasNext()) {
	    		var leadNurtureObject = leadNurtureCustomObject.next();	 
	    		var leadNurtureRecords : Array = new Array();
				for each (var edata : String in leadNurtureObject.custom.data) {
					cnt = cnt+1;
					
					var dataarr : [] = edata.split('|');
					var recCalendar : Calendar = dw.system.Site.calendar;
					recCalendar.setTimeZone("UTC");
					try {
						recCalendar.parseByFormat(dataarr[2],  "yyyyMMdd-HHmmss");
					} catch (e) {
										
					}

					var contactID = dataarr[0];
					
					var questionsParams = {
            				contactID: contactID, 
							sessionID: dataarr[1],
							sysTime: dataarr[4],
            				questions: dataarr[2],
            				source: dataarr[3]
            		};
            		
					Transaction.wrap(function () {
						if (cnt <= 2) {
							var result = SFEmailSubscriptionHelper.sendSFQuestions(questionsParams);
							if (result.Status != 'OK' && result.ErrorCode != 'exists') {
								leadNurtureRecords.push(contactID + '|' + questionsParams.sessionID + '|' + questionsParams.questions + '|' + dataarr[3] + '|' + dataarr[4] + '|' + result.ErrorCode + '|' + sysTime + '|' + cnt);				
							}
						} else {
							leadNurtureRecords.push(edata);
							cnt = cnt-1;
						}
					});
				}
				Transaction.wrap(function () {
					try {
						if (leadNurtureRecords.length == 0) {
							CustomObjectMgr.remove(leadNurtureObject);
						} else {
							leadNurtureObject.custom.data = leadNurtureRecords;
						}
					} catch (e) {
						Logger.error('sendFailSafe error:' + e);
						returnObject.Status = 'ERROR';
						returnObject.Msg = leadNurtureRecords + e;
						return returnObject;
					}
				});				
			}
		}
		returnObject.Status = 'OK';
		returnObject.Msg = '';
		return returnObject;
	},	

	//**************************************************************************

	processFailSafe : function () : Object {

		var returnObject : Object = {};

		var sysTime : String = StringUtils.formatCalendar(new Calendar(),'yyyyMMdd-HHmmss');
		try {
			var emailSubscriptions : Object = CustomObjectMgr.getAllCustomObjects('SFEmailSubscriptions');
		} catch (e) {
			returnObject.Status = 'ERROR';
			returnObject.Msg = e;
			return returnObject;
		}
		var cnt = 0;
		
		if (emailSubscriptions !== null) {
	    	while(emailSubscriptions.hasNext()) {
	    		var emailSubscriptionObject = emailSubscriptions.next();	 
	    		var emailAddresses : Array = new Array();
				for each (var edata : String in emailSubscriptionObject.custom.data) {
					cnt = cnt+1;
					
					var dataarr = edata.split('|');
					var recCalendar : Calendar = dw.system.Site.calendar;
					recCalendar.setTimeZone("UTC");
					try {
						recCalendar.parseByFormat(dataarr[4],  "yyyyMMdd-HHmmss");
					} catch (e) {
										
					}
					var emailParams = {
            				emailAddress: dataarr[0], 
            				zipCode: dataarr[1], 
            				leadSource: dataarr[2], 
            				siteId: Site.current.ID, 
            				optOutFlag: dataarr[3],
            				leadDate: recCalendar,
            				gclid: dataarr[5],
            				dwsid: dataarr[6],
            				firstName: dataarr[7],
            				leadCreative: dataarr[8],
							leadPage: dataarr[9]
            		};
            		
					Transaction.wrap(function () {
						if (cnt <= 2) {
							var result = SFEmailSubscriptionHelper.sendSFEmailInfo(emailParams);
							if (result.Status != 'OK' && result.ErrorCode != 'exists') {
								emailAddresses.push(emailParams.emailAddress + '|' + emailParams.zipCode + '|' + emailParams.leadSource + '|' + emailParams.optOutFlag + '|' + dataarr[4] + '|' + emailParams.gclid + '|' + emailParams.dwsid + '|' + result.firstName + '|' + result.leadCreative + '|' + result.leadPage + '|' + result.ErrorCode + '|' + sysTime);
							}
						} else {
							emailAddresses.push(edata);
						}
					});
				}
				Transaction.wrap(function () {
					try {
						if (emailAddresses.length == 0) {
							CustomObjectMgr.remove(emailSubscriptionObject);
						} else {
							emailSubscriptionObject.custom.data = emailAddresses;
						}
					} catch (e) {
						Logger.error('sendFailSafe error:' + e);
						returnObject.Status = 'ERROR';
						returnObject.Msg = emailAddresses + e;
						return returnObject;
					}
				});				
			}
		}
		returnObject.Status = 'OK';
		returnObject.Msg = '';
		return returnObject;
	}	
};

exports.SFEmailSubscriptionHelper = SFEmailSubscriptionHelper;
