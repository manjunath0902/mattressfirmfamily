importPackage( dw.svc );
importPackage( dw.system );
importPackage( dw.object );
importPackage( dw.util );
importPackage( dw.crypto );

var SFWalkOutCardHelper = {
	sendSFMessageInfo : function(obj) : Object {
		//importScript("app_storefront_core:util/ViewHelpers.ds");
		//debugHelper();
		
		var storeID = obj.storeID;
		var associateADID = obj.associateADID;
		var associateName = obj.associateName;
		var associateEmailAddress = obj.associateEmailAddress;
		var guestFirstName = obj.guestFirstName;
		var guestLastName = obj.guestLastName;
		var guestZipCode = obj.guestZipCode;
		var dwcDate = dw.system.Site.calendar;
		dwcDate.setTimeZone("UTC");


		var emailAddress = obj.emailAddress;
		var preferredMethodEmail = obj.preferredMethodEmail;
		if( preferredMethodEmail == "on" ){ preferredMethodEmail = true }else{ preferredMethodEmail = false }
		var phone = obj.phone;
		var preferredMethodPhone = obj.preferredMethodPhone;
		if( preferredMethodPhone == "on" ){ preferredMethodPhone = true }else{ preferredMethodPhone = false }
		var shoppingFor = obj.shoppingFor;
		var detailsOffer = obj.detailsOffer;
		
		var leadSource = obj.leadSource;
		var siteId = obj.siteId;
		var optOutFlag = obj.optOutFlag;
		var leadDate = obj.leadDate;
		var gclid = obj.gclid;
		var dwsid = obj.dwsid;
		
		var service : Service;
		var result  :  Result;
		var recordTypeIdCustomer = Site.current.preferences.custom.sfCustomerRecordTypeID; //'012G00000017CrRIAU';
		var recordTypeIdLead = Site.current.preferences.custom.sfLeadRecordTypeID; //'012G0000001QVPXIA4';
		var returnObject : Object = {};		
		var sitePrefix : String;
		
		//not currently used, but here in case other sites are addded in the future
		//prefix would be used for the custom fields eg. MFRM_Lead_Source__c
		if (siteId == 'Mattress-Firm') {
			sitePrefix = 'MFRM';
		} else {
	    	returnObject.Status = "ERROR";		
			returnObject.ErrorCode = "NOT_MFRM";
	    	return returnObject;
		}
		
		if (empty(optOutFlag)) {
			optOutFlag = false;
		}
		
		if (empty(obj.leadDate)) {
			leadDate = dw.system.Site.calendar;
			leadDate.setTimeZone("UTC");
		}
				
		if (empty(emailAddress)) {
			returnObject.Status = "ERROR";
			returnObject.ErrorCode = "EMPTY_EMAIL";
			return returnObject;
		}
		emailAddress = emailAddress.toLowerCase().trim();
		
		var strikeIronCode = 0;		// no longer used
//		var strikeIronCode = SFWalkOutCardHelper.getStrikeIronCode(emailAddress);
//		if (strikeIronCode == 0) {
//	    	returnObject.Status = "SERVICE_ERROR";		
//	    	returnObject.ErrorCode = "SI_SERVICE_ERROR";
//	    	return returnObject;
//		}
				
		var responseOAuth = SFWalkOutCardHelper.getToken();
		
		if (responseOAuth.Status != "OK"){
			returnObject.Status = responseOAuth.Status;
			returnObject.Msg = "Error retrieving token";
			returnObject.ErrorCode = responseOAuth.ErrorCode;
			returnObject.ContactID = "";
			return returnObject;			
		}

		var sfServiceName = responseOAuth.sfServiceName;
		var token : String = responseOAuth.access_token;
		var url   : String = responseOAuth.instance_url; 		 
		//
		//	SF Contacts
		//
		
		var queryContact = "select Id, RecordTypeId, email, FirstName, LastName, OtherPostalCode, DWC_Store_ID__c, DWC_Associate_ADID__c, DWC_Associate_Name__c, DWC_Associate_Email__c, DWC_Guest_First_Name__c, DWC_Guest_Last_Name__c, DWC_Guest_Email__c, DWC_Preferred_Method_Email__c, DWC_Guest_Phone__c, DWC_Preferred_Method_Phone__c, DWC_Shopping_For__c,  DWC_Details_Offer__c, MFRM_Lead_Source__c, MFRM_Lead_Date__c, EmailVerificationCode__c, HasOptedOutOfEmail from contact where email = '" + emailAddress + "'"
		result = SFWalkOutCardHelper.getData(sfServiceName, url, token, queryContact);		  		 
	    var responseContacts : object = JSON.parse(result.object);
	    
		if (responseContacts) {
			var updValsObj = {		
				_leadDate : leadDate,
				_dwcDate : dwcDate,
				_leadSource : leadSource,
				_optOutFlag : optOutFlag,
				_gclid : gclid,
				_dwsid : dwsid,
				_sitePrefix : sitePrefix,
				_storeID : !storeID ? undefined : storeID,
				_associateADID : !associateADID ? undefined : associateADID,
				_associateName : !associateName ? undefined : associateName,
				_associateEmailAddress : !associateEmailAddress ? undefined : associateEmailAddress,
				_guestFirstName : !guestFirstName ? undefined : guestFirstName,
				_guestLastName : !guestLastName ? undefined : guestLastName,
				_guestZipCode : !guestZipCode ? undefined : guestZipCode,
				_emailAddress : emailAddress,
				_preferredMethodEmail : preferredMethodEmail,
				_phone : !phone ? undefined : phone,
				_preferredMethodPhone : preferredMethodPhone,
				_shoppingFor : !shoppingFor ? undefined : shoppingFor,
				_detailsOffer : !detailsOffer ? undefined : detailsOffer
			};
		
		    if (responseContacts.totalSize == 0) {
				if (sitePrefix.toUpperCase() == 'MFRM') {	    	
			    	var restArgs = {
			    		 	method: 'POST',
			    		 	type: 'CREATE',
			    		 	body : {     	
			    		        RecordTypeId: recordTypeIdLead,
			    		        Email: emailAddress,
			    		        FirstName: guestFirstName,
			    		        LastName: !guestLastName ? 'Email Lead' : guestLastName,
			    		        DWC_Eligible_for_Email__c: "true",
			    		        DWC_RSA_Eligible_for_Email__c: "true",
			    		        DWC_Store_ID__c: storeID,
			    		        DWC_Associate_ADID__c: associateADID,
			    		        DWC_Associate_Name__c: associateName,
			    		        DWC_Associate_Email__c: associateEmailAddress,
			    		        DWC_Guest_First_Name__c: guestFirstName,
			    		        DWC_Guest_Last_Name__c: guestLastName,
			    		        DWC_Guest_Email__c: emailAddress,
			    		        DWC_Preferred_Method_Email__c: preferredMethodEmail,
			    		        DWC_Guest_Phone__c: phone,
			    		        OtherPostalCode: guestZipCode,
					    		DWC_Preferred_Method_Phone__c: preferredMethodPhone,
					    		DWC_Shopping_For__c: shoppingFor,
					    		DWC_Details_Offer__c: detailsOffer,
			    		        //MFRM_Lead_Source__c: leadSource,
					    		DWC_Timestamp__c: dwcDate.getTime(),
			    		        //EmailVerificationCode__c: strikeIronCode,
			    		        HasOptedOutOfEmail: optOutFlag
			    		        //Google_Client_ID__c: gclid,
			    		        //DW_Session_ID__c: dwsid
			    		 	}
			    	 };
				} 
				
	    	    //
	    	    // Create Contact
	    	    //
	    		service = ServiceRegistry.get(sfServiceName);	// reset the service to remove oauth service params
	    		service.URL = url + "/services/data/v40.0/sobjects/Contact/";
	    		service.addHeader("Authorization", "Bearer " + token);       
	    		service.addHeader("Content-Type", "application/json");
	    		
	    		result = service.call(restArgs);
	    		
	    		var contactId = JSON.parse(result.object).id;
	    		
	    		//service.URL = url + "/services/data/v40.0/sobjects/Contact_Web_History__c/";
		    	//var restArgs = {
		    	//	 	method: 'POST',
		    	//	 	type: 'CREATE',
		    	//	 	body : {
		    	//	 		Contact__c: contactId,
		    	//	 		Google_Client_ID__c: gclid,
		    	//	 		DW_Session_ID__c: dwsid
		    	//	 	}
		    	//};
		    	//result = service.call(restArgs); 		 
	        						
		    	returnObject.Status = "OK";		
		    	returnObject.Msg = '(' + strikeIronCode + ')';
				returnObject.ErrorCode = "";
				returnObject.ContactID = contactId;
		    	
		    } else if (responseContacts.totalSize == 1) {	
		    	//if record is customer
		
		    	var result = SFWalkOutCardHelper.updateContacts(sfServiceName, url, token, responseContacts, strikeIronCode, updValsObj);
							
			    returnObject.Status = "OK";		   
			    returnObject.Msg = "EXISTS: " + responseContacts.records[0].Id + ' (' + strikeIronCode + ')' + ' ' + siteId;	
				returnObject.Code = "updated";
				returnObject.ContactID = result.ContactID;
	
		    } else {
	
				result = SFWalkOutCardHelper.getData(sfServiceName, url, token, queryContact + " and RecordTypeId = '" + recordTypeIdCustomer + "' order by LastModifiedDate desc, " + sitePrefix + "_Lead_Date__c desc limit 1");
				var responseCustomerContacts : object = JSON.parse(result.object);
					    
				
				if (responseCustomerContacts.totalSize == 0) {
					//Its all Lead records, Update them all
		    		var result = SFWalkOutCardHelper.updateContacts(sfServiceName, url, token, responseContacts, strikeIronCode, updValsObj);
				} else {
					//Update only the customer records, Don't Update zip and leadsource on customer records
					var result = SFWalkOutCardHelper.updateContacts(sfServiceName, url, token, responseCustomerContacts, strikeIronCode, updValsObj);
				}
	
	
			    returnObject.Status = "OK";		
			    returnObject.Msg = "MANY EXISTS" + ' (' + strikeIronCode + ')' + ' ' + siteId;
				returnObject.Code = "updated";
				returnObject.ContactID = result.ContactID;
	
		    }  
		}
	    return returnObject;
	},

	
	//**************************************************************************
	
	
	getToken : function () : Object {
		var service : Service;
		var result  : Result;
		var sfServiceName = Site.current.preferences.custom.sfAPIServiceName;
		var returnObject : Object = {};
	
    	/*
    	returnObject.Status = "OK";		
    	returnObject.ErrorCode = "";
    	returnObject.sfServiceName = sfServiceName;
    	returnObject.access_token = "00DM0000001f8dK!AQgAQPRPqdvxkljpyhmmQOSFxuBrTGV51ECopYMVWY4HPX2HGhi74aQBixTTZpRVu4cRrY7Hg6_CWFKoZz.Crbfe.Dz6I4PA";
    	returnObject.instance_url = "https://mattress--DEV012.cs7.my.salesforce.com";
   		return returnObject;
		*/
		
		if (Site.current.preferences.custom.sfAPIInstance.toLowerCase() == 'dev') {
			var sfClientId = Site.current.preferences.custom.sfAPIDevClientID;
			var sfSecret = Site.current.preferences.custom.sfAPIDevSecret;
			var sfUsername = Site.current.preferences.custom.sfAPIDevUsername;
			var sfPassword = Site.current.preferences.custom.sfAPIDevPassword;
		} else {
			var sfClientId = Site.current.preferences.custom.sfAPIProdClientID;
			var sfSecret = Site.current.preferences.custom.sfAPIProdSecret;
			var sfUsername = Site.current.preferences.custom.sfAPIProdUsername;
			var sfPassword = Site.current.preferences.custom.sfAPIProdPassword;			
		}
		//
		//	SF OAuth Authentication
		//
		try {
			service = ServiceRegistry.get(sfServiceName);
			service.addParam('grant_type', 'password')
				.addParam('client_id', sfClientId)
				.addParam('client_secret', sfSecret)
				.addParam('username', sfUsername)
				.addParam('password', sfPassword);
			
			var restArgs = {
			 	method: 'POST',
			 	type: 'OAUTH',
			 	body: ''
			}
        } catch (e) {
            returnObject.Status = "SERVICE_ERROR";        
            returnObject.ErrorCode = "SERVICE_ERROR";                
            return returnObject;
        }
			
		try {
			result = service.call(restArgs);
		} catch (e) {
	    	returnObject.Status = "SERVICE_ERROR";		
	    	returnObject.ErrorCode = "SERVICE_ERROR";			
            return returnObject;
		}
		
		if (!result.ok){
			var access_token : String = '';
			var instance_url   : String = '';
		
	    	returnObject.Status = "SERVICE_ERROR";		
	    	returnObject.ErrorCode = "SERVICE_ERROR";	    	
	    	return returnObject;
		}
		
	 	var responseOAuth : object = JSON.parse(result.object);
		var access_token : String = responseOAuth.access_token;
		var instance_url   : String = responseOAuth.instance_url; 
		
    	returnObject.Status = "OK";		
    	returnObject.ErrorCode = "";
    	returnObject.sfServiceName = sfServiceName;
    	returnObject.access_token = access_token;
    	returnObject.instance_url = instance_url;
    	
		return returnObject;
		
	},
	
	
	//**************************************************************************


	getData : function(_sfServiceName, _url, _token, _q) : Result {
		var service = ServiceRegistry.get(_sfServiceName);	// reset the service to remove oauth service params
		var query : String = Encoding.toURI(_q);
		service.URL = _url + "/services/data/v40.0/query?q=" + query;
		service.addHeader("Authorization", "Bearer " + _token);
		var restArgs = {
		 	method: 'GET',
		 	type: 'QUERY',
		 	body: ''
		}
		var result = service.call(restArgs);
		return result;
	},



	//**************************************************************************
	
	
	updateContacts : function (_sfServiceName, _url, _token, _responseContacts, _strikeIronCode, _updValsObj) : Object {
		
		service = ServiceRegistry.get(_sfServiceName);	// reset the service to remove oauth service params
		service.URL = _url + "/services/data/v40.0/composite/batch";
		service.addHeader("Authorization", "Bearer " + _token);       
		service.addHeader("Content-Type", "application/json");
		
		var _leadDate = _updValsObj._leadDate;
		var _dwcDate = _updValsObj._dwcDate;
		var _leadSource = _updValsObj._leadSource;
		var _optOutFlag = _updValsObj._optOutFlag;
		var _gclid = _updValsObj._gclid;
		var _dwsid = _updValsObj._dwsid;
		var _sitePrefix = _updValsObj._sitePrefix;
		var _storeID = _updValsObj._storeID;
		var _associateADID = _updValsObj._associateADID;
		var _associateName = _updValsObj._associateName;
		var _associateEmailAddress = _updValsObj._associateEmailAddress;
		var _guestFirstName = _updValsObj._guestFirstName;
		var _guestLastName = _updValsObj._guestLastName;
		var _emailAddress = _updValsObj._emailAddress;
		var _preferredMethodEmail = _updValsObj._preferredMethodEmail;
		var _phone = _updValsObj._phone;
		var _preferredMethodPhone = _updValsObj._preferredMethodPhone;
		var _shoppingFor = _updValsObj._shoppingFor;
		var _detailsOffer = _updValsObj._detailsOffer;
		
		var restArgs = {
		 	method: 'PATCH',
		 	type: 'BATCH_UPDATE',
		 	body : {
		 		batchRequests : []
		 	}
		};
		
		var batchIndex = 0;
		for (var i in _responseContacts.records) {
			var contactId = _responseContacts.records[i].Id;
			var recCalendar : Calendar = dw.system.Site.calendar;
			recCalendar.setTimeZone("UTC");
			try {
				if (_sitePrefix.toUpperCase() == 'MFRM') {
					recCalendar.parseByFormat(_responseContacts.records[i].MFRM_Lead_Date__c,  "yyyy-MM-dd'T'HH:mm:ss");
				} else if (_sitePrefix.toUpperCase() == 'TULO') {
					recCalendar.parseByFormat(_responseContacts.records[i].tulo_Lead_Date__c,  "yyyy-MM-dd'T'HH:mm:ss");
				}
			} catch (e) {
				recCalendar = _leadDate;				
			}

			if (_leadDate.after(recCalendar) || _leadDate.equals(recCalendar) ) {
	    		if (_sitePrefix.toUpperCase() == 'MFRM') {
					var j = { 
							method: 'PATCH',
							url: "v40.0/sobjects/Contact/" + contactId,
					        richInput : {
					        	//MFRM_Lead_Source__c: _leadSource,
					        	//MFRM_Lead_Date__c: _leadDate.getTime(),
					        	//EmailVerificationCode__c: _strikeIronCode,
					        	HasOptedOutOfEmail: _optOutFlag,
					        	FirstName : _guestFirstName,
			    		        DWC_Eligible_for_Email__c: "true",
					        	DWC_RSA_Eligible_for_Email__c: "true",
					        	DWC_Timestamp__c: _dwcDate.getTime(),
			    		        DWC_Store_ID__c: _storeID,
			    		        DWC_Associate_ADID__c: _associateADID,
			    		        DWC_Associate_Name__c: _associateName,
			    		        DWC_Associate_Email__c: _associateEmailAddress,
			    		        DWC_Guest_First_Name__c: _guestFirstName,
			    		        DWC_Guest_Last_Name__c: _guestLastName,
			    		        DWC_Guest_Email__c: _emailAddress,
			    		        DWC_Preferred_Method_Email__c: _preferredMethodEmail,
			    		        DWC_Guest_Phone__c: _phone,
					    		DWC_Preferred_Method_Phone__c: _preferredMethodPhone,
					    		DWC_Shopping_For__c: _shoppingFor,
					    		DWC_Details_Offer__c: _detailsOffer
					        }
						};
					
					// update zip code if previously null and now is not null
					var contactZip = _responseContacts.records[i].OtherPostalCode;
					var updatedZip = _updValsObj._guestZipCode;			
					// Logger.info(i + ": contactZip=" + contactZip + ", updatedZip=" + updatedZip);
					
					if (!contactZip && updatedZip) {
						j.richInput.OtherPostalCode = updatedZip;
					}
					
					
	    		}
				restArgs.body.batchRequests[batchIndex++]= j;
				
		    	//var webHistoryArgs = {
		    	//	 	method: 'POST',
				//		url: "v40.0/sobjects/Contact_Web_History__c/",
				//		richInput : {
		    	//	 		Contact__c: contactId,
		    	//	 		Google_Client_ID__c: _gclid,
		    	//	 		DW_Session_ID__c: _dwsid
		    	//	 	}
		    	//};
		    	//restArgs.body.batchRequests[batchIndex++]= webHistoryArgs;
	    	}

		}
		
		try {
			var newResult = service.call(restArgs);
			// Add ContactID to the result object to pass to SFMC
			newResult = JSON.parse(newResult.object);
			newResult.ContactID = contactId;
		} catch (e) {
			var newResult = {Status: 'ERROR', Msg: e, ErrorCode: '', object: {}};
		}
		return newResult;
	},

	//**************************************************************************
	
	getStrikeIronCode : function(_emailAddress) {
		var service = ServiceRegistry.get("strikeiron.soap");
		var params = {"email": _emailAddress,
					  "userid": Site.current.preferences.custom.siUserID,
					  "password": Site.current.preferences.custom.siPassword,
					  "timeout": Site.current.preferences.custom.siTimeOut
					  };
		
		var soapResult = service.call(params);		
		if (soapResult.status == 'OK') { //if strikeiron service failed then just move on, if not then check verification
			return soapResult.object.Status
		} else {
			return 0;
		}
		
	},
	
	
	//**************************************************************************

	sendFailSafe : function(obj, errorMsg) : Object {
		var storeID = obj.storeID;
		var associateADID = obj.associateADID;
		var associateName = obj.associateName;
		var associateEmailAddress = obj.associateEmailAddress;
		var guestFirstName = obj.guestFirstName;
		var guestLastName = obj.guestLastName;
		var emailAddress = obj.emailAddress;
		var preferredMethodEmail = obj.preferredMethodEmail;
		if( preferredMethodEmail == "on" ){ preferredMethodEmail = true }else{ preferredMethodEmail = false }
		var phone = obj.phone;
		var preferredMethodPhone = obj.preferredMethodPhone;
		if( preferredMethodPhone == "on" ){ preferredMethodPhone = true }else{ preferredMethodPhone = false }
		var shoppingFor = obj.shoppingFor;
		var detailsOffer = obj.detailsOffer;
		
		var leadSource = obj.leadSource;
		var optOutFlag = obj.optOutFlag;
		var gclid = obj.gclid;
		var dwsid = obj.dwsid;
		var firstName = obj.guestFirstName;
		

		var returnObject : Object = {};
		if (empty(emailAddress)) {
			returnObject.Status = "ERROR";
			return returnObject;
		}
		if (empty(optOutFlag)) {
			optOutFlag = false;
		}
		var yyyyMMdd : String = StringUtils.formatCalendar(new Calendar(),'yyyyMMdd');
		var sysTime : String = StringUtils.formatCalendar(new Calendar(),'yyyyMMdd-HHmmss');
		try{
			var emailSubscriptions : Object = CustomObjectMgr.getCustomObject('SFWalkOutCard', yyyyMMdd + '-0');
			var i = 0;
			while(emailSubscriptions !== null && emailSubscriptions.custom.data.length === 200){
				i++;
				emailSubscriptions = CustomObjectMgr.getCustomObject('SFWalkOutCard', yyyyMMdd + '-' + i);
			}

			Transaction.wrap(function () {
				if(emailSubscriptions === null){
					emailSubscriptions = CustomObjectMgr.createCustomObject('SFWalkOutCard', yyyyMMdd + '-' + i);
				}

				var emailAddresses : Array = new Array();

				for each(var email : String in emailSubscriptions.custom.data) {
					emailAddresses.push(email);
				}
				
				emailAddresses.push(emailAddress + '|' + leadSource + '|' + optOutFlag + '|' + sysTime + '|' + gclid + '|' + dwsid + '|' + firstName + '|' + storeID + '|' + associateADID + '|' + associateName + '|' + associateEmailAddress + '|' + guestFirstName + '|' + guestLastName + '|' + preferredMethodEmail + '|' + phone + '|' + preferredMethodPhone + '|' + shoppingFor + '|' + detailsOffer + '|' + errorMsg);

				emailSubscriptions.custom.data = emailAddresses;

				returnObject.Status = 'OK';
			});
		} catch(e){
			Logger.error('sendFailSafe error:' + e);
			returnObject.Status = "ERROR";
			return returnObject;
		}
		return returnObject;
	},
	
	//**************************************************************************


	processFailSafe : function () : Object {

		var returnObject : Object = {};

		var sysTime : String = StringUtils.formatCalendar(new Calendar(),'yyyyMMdd-HHmmss');
		try {
			var emailSubscriptions : Object = CustomObjectMgr.getAllCustomObjects('SFWalkOutCard');
		} catch (e) {
			returnObject.Status = 'ERROR';
			returnObject.Msg = e;
			return returnObject;
		}
		var cnt = 0;
		
		if (emailSubscriptions !== null) {
	    	while(emailSubscriptions.hasNext()) {
	    		var emailSubscriptionObject = emailSubscriptions.next();	 
	    		var emailAddresses : Array = new Array();
				for each (var edata : String in emailSubscriptionObject.custom.data) {
					cnt = cnt+1;
					
					var dataarr = edata.split('|');
					var recCalendar : Calendar = dw.system.Site.calendar;
					recCalendar.setTimeZone("UTC");
					try {
						recCalendar.parseByFormat(dataarr[4],  "yyyyMMdd-HHmmss");
					} catch (e) {
										
					}
					var emailParams = {
            				emailAddress: dataarr[0],  
            				leadSource: dataarr[1], 
            				siteId: Site.current.ID, 
            				optOutFlag: dataarr[2],
            				leadDate: recCalendar,
            				gclid: dataarr[4],
            				dwsid: dataarr[5],
            				firstName: dataarr[6],
            				storeID: dataarr[7],
            				associateADID: dataarr[8],
            				associateName: dataarr[9],
            				associateEmailAddress: dataarr[10],
            				guestFirstName: dataarr[11],
            				guestLastName: dataarr[12],
            				preferredMethodEmail: dataarr[13],
            				phone: dataarr[14],
            				preferredMethodPhone: dataarr[15],
            				shoppingFor: dataarr[16],
            				detailsOffer: dataarr[17]
            		};
            		
					Transaction.wrap(function () {
						if (cnt <= 2) {
							var result = SFWalkOutCardHelper.sendSFEmailInfo(emailParams);
							if (result.Status != 'OK' && result.ErrorCode != 'exists') {
								emailAddresses.push(emailParams.emailAddress + '|' + emailParams.leadSource + '|' + emailParams.optOutFlag + '|' + dataarr[3] + '|' + emailParams.gclid + '|' + emailParams.dwsid + '|' + result.firstName + '|' + emailParams.storeID + '|' + emailParams.associateADID + '|' + emailParams.associateName + '|' + emailParams.associateEmailAddress + '|' + emailParams.guestFirstName + '|' + emailParams.guestLastName + '|' + emailParams.preferredMethodEmail + '|' + emailParams.phone + '|' + emailParams.preferredMethodPhone + '|' + emailParams.shoppingFor + '|' + emailParams.detailsOffer + '|' + result.ErrorCode + '|' + sysTime);
							}
						} else {
							emailAddresses.push(edata);
						}
					});
				}
				Transaction.wrap(function () {
					try {
						if (emailAddresses.length == 0) {
							CustomObjectMgr.remove(emailSubscriptionObject);
						} else {
							emailSubscriptionObject.custom.data = emailAddresses;
						}
					} catch (e) {
						Logger.error('sendFailSafe error:' + e);
						returnObject.Status = 'ERROR';
						returnObject.Msg = emailAddresses + e;
						return returnObject;
					}
				});				
			}
		}
		returnObject.Status = 'OK';
		returnObject.Msg = '';
		return returnObject;
	}	
};

exports.SFWalkOutCardHelper = SFWalkOutCardHelper;
