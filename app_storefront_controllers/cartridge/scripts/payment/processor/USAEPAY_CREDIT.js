'use strict';

var Pipeline = require('dw/system/Pipeline');
var PaymentMgr = require('dw/order/PaymentMgr');
var Transaction = require('dw/system/Transaction');
var OrderMgr = require('dw/order/OrderMgr');

/**
 * Verifies a credit card against a valid card number and expiration date and
 * possibly invalidates invalid form fields. If the verification was successful
 * a credit card payment instrument is created. The pipeline just reuses the
 * basic credit card validation pipeline from processor BASIC_CREDIT.
 */
function Handle(args) {
    return require('./BASIC_CREDIT').Handle(args);
}

/**
 * Authorizes a payment using a credit card. A real integration is not
 * supported, that's why the pipeline returns this state back to the calling
 * checkout pipeline.
 */
function Authorize(args) {
    var usaepayResult = "";
    var orderNo = args.OrderNo;
    var orderAmount;
  try{
	    var order = OrderMgr.getOrder(orderNo);
	    if(!empty(order)){
	    	orderAmount = order.getTotalGrossPrice().value;​
	    }
	    
	    //Auth call
	    var resultAuth = doAuth(order);
	    if (!empty(resultAuth) && resultAuth != null && empty(resultAuth.errorMessage) && !empty(resultAuth.result)) {
	        if (resultAuth.result == "Approved") {​
	            //Forter call
	            var forterDecision = doForterValidateOrder(resultAuth, order);            
	            if(!empty(forterDecision.JsonResponseOutput)) {
	            	if (forterDecision.JsonResponseOutput.processorAction === 'skipCapture' || forterDecision.JsonResponseOutput.processorAction === 'notReviewed' || forterDecision.JsonResponseOutput.processorAction === 'void') {
	            		//void call 
	            		resultCapture = doVoid({refnum: resultAuth.refnum, amount: orderAmount});
	                    var voidMessage = "";
	                	if (resultCapture.result == "Approved") {
	                		voidMessage = "Void Approved"
	                    } else {
	                    	voidMessage = "Void Error"
	                    }                	
	                	usaepayResult = 'Decline';
	                } else if (forterDecision.JsonResponseOutput.processorAction === 'disabled' || forterDecision.JsonResponseOutput.processorAction === 'internalError' || forterDecision.JsonResponseOutput.processorAction === 'capture') {​
	                    //Capture call
	                    resultCapture = doCapture(order);
	                    if (resultCapture.result == "Approved") {
	                        usaepayResult = resultCapture.result;
	                    } else {
	                        usaepayResult = "Error";
	                        //void call 
	                        var resultVoid = doVoid({refnum: resultAuth.refnum, amount: orderAmount});

	                    }
	                }	
	            } else if (forterDecision.result == false) {
	            	//void call 
	            	resultCapture = doVoid({refnum: resultAuth.refnum, amount: orderAmount});
	                var voidMessage = "";
	            	if (resultCapture.result == "Approved") {
	            		voidMessage = "Void Approved"
	                } else {
	                	voidMessage = "Void Error"
	                }   
	            	usaepayResult = "Error";
	            }            
	        }
	    } else {
	        usaepayResult = "Error";
	    }  
  }catch(e){
	  usaepayResult = "Error";
  }
  
  if(!empty(forterDecision) ){	  
	  var forterErrorCodeStr = forterDecision.PlaceOrderError ? forterDecision.PlaceOrderError.code : "";
	  
	  if(!empty(forterDecision.JsonResponseOutput) ){
		  var forterResultStr = forterDecision.JsonResponseOutput.actionEnum;
	  }
  }
  
  switch (usaepayResult) {
       case 'Approved':
            return {authorized: true};
       case 'Error':
            return {error: true,reasonCode: "",forterErrorCode: ""};
       case 'review':
            return {review: true};
       case 'Decline':
            return {authorized: false, reasonCode: "", forterResult: forterResultStr,  forterErrorCode: forterErrorCodeStr};            
       default:
            return {not_supported: true,reasonCode: ""};
    }
}

function doAuth(order) {
	try{
		var USAePayAuthtorize = dw.system.HookMgr.callHook("app.payment.processor.USAePay", 'Authtorize', order);
	    var resultAuth = JSON.parse(USAePayAuthtorize);
	    return resultAuth;	    
	}catch(e) {
		return USAePayAuthtorize;
	}
    
}

function doCapture(order) {
    try{
    	var USAePayCapture = dw.system.HookMgr.callHook("app.payment.processor.USAePay", 'Capture', order);
        var resultCapture = JSON.parse(USAePayCapture);
        return resultCapture;	    
	}catch(e) {
		return USAePayCapture;
	}
}

function doForterValidateOrder(resultAuth, order) {
    var argOrderValidate = {
            Order: order,
            CCAuthResponse: resultAuth,
            orderValidateAttemptInput: 1
        },
        forterCall = require('int_forter/cartridge/controllers/ForterValidate.js'),
        forterDecision = forterCall.ValidateOrder(argOrderValidate);
    return forterDecision;
}

function doVoid(args) {
    var USAePayCapture = dw.system.HookMgr.callHook("app.payment.processor.USAePay", 'Void', args);    
    return USAePayCapture;
}
/*
 * Module exports
 */

/*
 * Local methods
 */
exports.Handle = Handle;
exports.Authorize = Authorize;