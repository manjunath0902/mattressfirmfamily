'use strict';

var Pipeline = require('dw/system/Pipeline');
var PaymentMgr = require('dw/order/PaymentMgr');
var Transaction = require('dw/system/Transaction');
var OrderMgr = require('dw/order/OrderMgr');

/**
 * Verifies a credit card against a valid card number and expiration date and
 * possibly invalidates invalid form fields. If the verification was successful
 * a credit card payment instrument is created. The pipeline just reuses the
 * basic credit card validation pipeline from processor BASIC_CREDIT.
 */
function Handle(args) {
    return require('./BASIC_CREDIT').Handle(args);
}

/**
 * Authorizes a payment using a credit card. A real integration is not
 * supported, that's why the pipeline returns this state back to the calling
 * checkout pipeline.
 */
function Authorize(args) {
	var orderNo = args.OrderNo;
	var order = OrderMgr.getOrder(orderNo);
	var pdict = Pipeline.execute('Cybersource_Authorize-Start',{
		PaymentInstrument: args.PaymentInstrument,
		Basket: order,
		Order: order,
		OrderNo: orderNo
	});
	var forterError = null;
	if (!empty(pdict.ForterResponse)) {
		forterError = pdict.ForterResponse.PlaceOrderError;
    } 

	switch (pdict.CybersourceEndState) {
		case 'review':
			return {review: true};
		case 'error':
			return {error: true, reasonCode: pdict.ReasonCode, forterErrorCode : forterError};
		case 'authorized':
			return {authorized: true};
		case 'decline':
			return {authorized: false ,reasonCode: pdict.ReasonCode};
		default: return {not_supported: true, reasonCode: pdict.ReasonCode};
	}
}

/*
 * Module exports
 */

/*
 * Local methods
 */
exports.Handle = Handle;
exports.Authorize = Authorize;
