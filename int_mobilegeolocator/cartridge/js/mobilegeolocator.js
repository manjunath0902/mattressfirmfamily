'use strict';

/*
*   Initializes the Google Maps Distance API and returns a value
*/
function initDistance(origin, destination) {
    var service = new google.maps.DistanceMatrixService;
    var d = $.Deferred();
    service.getDistanceMatrix({
        origins: [origin],
        destinations: [destination],
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.IMPERIAL
    }, function (response, status) {
        var distance = 100;
        if (status === google.maps.DistanceMatrixStatus.OK) {
            $('div.geolocation-dialog').find('span.distance').html(response.rows[0].elements[0].distance.text);
            distance = parseInt(response.rows[0].elements[0].distance.text.replace(' mi', ''));
            d.resolve(distance);
        }
    });
    return d.promise();
}

/*
*   Sets the appropriate map link per mobile device
*/
function getMobileMapLink(link) {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    if (/windows phone/i.test(userAgent)) {
        return "maps:" + link;
    }
    if (/android/i.test(userAgent)) {
    	//url encode link
        return "https://www.google.com/maps/dir/?api=1&destination=" + encodeURI(link);
    }
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        return "maps://maps.apple.com/?q=" + link;
    }
    return "maps://maps.apple.com/?q=" + link;
}

/*
*   Sets a cookie when the dialog is closed so it will not nag
*/
function setCookie() {
    var exdays = 14;
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = "supressMobileGeolocator=true;" + expires + ";domain=" + window.location.hostname + ";path=/";
}

/*
*   Gets the content of a cookie
*/
function getCookie(cname) {
    var name = "supressMobileGeolocator";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

/*
*   Checks if a cookie exists
*/
function checkCookie() {
    var cookie = getCookie();
    var cookieExists = false;
    if (cookie != "") {
        cookieExists = true;
    }
    return cookieExists;
}

var MobileGeolocator = {
    initModal: function () {
        var _dialog = $('div.geolocation-dialog');
        var origin = _dialog.data('origin');
        var destination = _dialog.data('destination');
        var link = _dialog.data('link');

        if (parseInt($(window).width()) < 425 && !checkCookie()) {
            initDistance(origin, destination).done(function (response) {
                _dialog.slideDown();
                utag.link({
                    eventCategory: 'Mobile Geolocator',
                    eventAction: 'Mobile Geolocator Drawer Opened',
                    eventLabel: 'MG Drawer Opened',
                    nonInteraction: true
                });
                $('a.directions').on('click', function () {
                    utag.link({
                        eventCategory: 'Mobile Geolocator',
                        eventAction: 'Mobile Geolocator Directions Clicked',
                        eventLabel: 'MG Directions Clicked'
                    });
                    window.open(getMobileMapLink(link));
                });
                $('span.close').on('click', function () {
                    utag.link({
                        eventCategory: 'Mobile Geolocator',
                        eventAction: 'Mobile Geolocator Closed',
                        eventLabel: 'MG Directions Closed'
                    });
                    _dialog.slideUp();
                    setCookie();
                });
                $('p.store-location a').on('click', function () {
                    utag.link({
                        eventCategory: 'Mobile Geolocator',
                        eventAction: 'Mobile Geolocator Find Another Location Clicked',
                        eventLabel: 'MG Find Another Location Clicked'
                    });
                    setCookie();
                });
            });
        }
    }
};

module.exports = MobileGeolocator;