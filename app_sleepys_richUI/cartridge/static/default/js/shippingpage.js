/**
@class app.enhancedShipping
*/
(function (app, $) {
	var $cache;
	
	/**
	 * @private
	 * @function
	 * @description Initializes the cache for the Enhanced Shipping page features.
	 */
	function initializeCache() {
		$cache = {
			addressForm:		$('#dwfrm_singleshipping_shippingAddress'),
			firstName:			$('#dwfrm_singleshipping_shippingAddress_addressFields_firstName'),
			lastName:			$('#dwfrm_singleshipping_shippingAddress_addressFields_lastName'),
			crossStreets:	 	$('#dwfrm_singleshipping_shippingAddress_addressFields_crossStreets'),
			apartment: 			$('#dwfrm_singleshipping_shippingAddress_addressFields_apartment'),
			companyName: 		$('#dwfrm_singleshipping_shippingAddress_addressFields_companyName'),
			address1:			$('#dwfrm_singleshipping_shippingAddress_addressFields_address1'),
			address2:			$('#dwfrm_singleshipping_shippingAddress_addressFields_address2'),
			phone:				$('#dwfrm_singleshipping_shippingAddress_addressFields_phone'),
			mobilePhone:	 	$('#dwfrm_singleshipping_shippingAddress_addressFields_mobilePhone'),
			postalCode: 		$('#dwfrm_singleshipping_shippingAddress_addressFields_zip'),
			city: 				$('#dwfrm_singleshipping_shippingAddress_addressFields_cities_city'),
			stateFallback: 		$('#dwfrm_singleshipping_shippingAddress_addressFields_states_stateUSNOAPO'),
			stateCode: 			$('#dwfrm_singleshipping_shippingAddress_addressFields_state'),
			countryCode: 		$('#dwfrm_singleshipping_shippingAddress_addressFields_countryCode'),
			fleetwiseToken:     $('#dwfrm_singleshipping_shippingAddress_addressFields_fleetwiseToken'),
			storedAddressForm:  $('#dwfrm_singleshipping_addressList'),
			cityName:			'',
			stateCodeStr:			''
		};
	}
		
	function initializeDom(){
		//var storedAddresses = $('#dwfrm_singleshipping_addressList').html();
		//checkRequiredComplete();
		/*$('input.required').each(function() {
			$(this).on('focus', function(e) {
				checkRequiredComplete();
			});	
		});*/
		
		deliveryWindowInit();
		
		//task #5126 Make Shipping & Delivery Address City Value error more prominent when you come back to shipping 
		//so user knows to select city to trigger windows
		/*if( $cache.postalCode.val() && !($cache.city.val()) ) {
			//figure out the city offset $cache.city.offset().top returns 0
			var dest = $('#dk_container_dwfrm_singleshipping_shippingAddress_addressFields_cities_city');
			var scrollDistance = Math.floor(dest.offset().top - dest.outerHeight());
			$('html, body').animate({
				scrollTop: scrollDistance,
			}, 800);
			dest.find('.dk_label').addClass('error');
		}*/
	}
	
	/*function checkRequiredComplete(){
		if($.trim($cache.firstName.val()) == '' || $.trim($cache.lastName.val()) == '' || $.trim($cache.address1.val()) == '' || $.trim($cache.phone.val()) == ''){
			$cache.postalCode.attr('readonly', true); 
			return false
		}else{
			$cache.postalCode.attr('readonly', false); 
			return true;
		}
	}*/
	
	function deliveryWindowInit(){
		if($('#withRep:checked').length) {
			$('.delivery-dates').addClass('vh');
		} else {
			$('.delivery-dates').removeClass('vh');
		}
	}
	
	/**
	 * @private
	 * @function
	 * @description CheckATP (also verify Address, get dates, and calc taxes) 
	 */
	function checkAtp(data){
		app.progress.show($cache.addressForm);
		setTimeout(function() {
			var newData = data+"&format=ajax";
			$(".checkout-button").attr("disabled","disabled");
			//console.log('checkatp: ');
			$.ajax({
				url: app.urls.checkATP,
				type: "POST",
				data: newData,
				async: false,
				timeout: 5000,
				dataType: "HTML",
				success: function(data){
					$cache.addressForm.html(data);
					$cache.addressForm.find('select').dropkick();
					$cache.addressForm.find('#dwfrm_singleshipping_shippingAddress_addressFields_state').attr('readonly', true);
					deliveryWindowInit();
					app.progress.hide();
					$(".checkout-button").removeAttr("disabled");
					
					if($('#addressSuggestion, .flash-message').length){
						var scrollDistance = Math.floor($('#addressSuggestion, .flash-message').offset().top - $('#addressSuggestion').outerHeight() );
						$('html, body').animate({
							scrollTop: scrollDistance
						}, 300);
					}
				}
			});
		});
	}
	
	/**
	 * @private
	 * @function
	 * @description Initializes events.
	 */
	function initializeEvents() {

		$cache.addressForm.on("click", ".time-slot", function () {
			if(!($(this).hasClass("full"))) {
				/** grab time slot clicked and mark correct day and time slot as selected **/
				$(".time-slot").removeClass("selected");
				$(".header").removeClass("selected");
				$(this).addClass("selected");
				$(this).parent().children(".header").addClass("selected");
				
				/** build text block at bottom stating what date and time was chosen **/
				$(".currently-chosen").addClass("visible");
				var textdate = $(this).parent().find(".weekday").text();
				textdate += ", " + $(this).parent().find(".month-day").text();
				textdate += ", between " + $(this).text().replace("-","and").replace(RegExp(" PM", "g"), "pm").replace(RegExp(" AM", "g"), "am").trim();
				$(".chosen-time").text(textdate);
				
				/** set pdict fleetwiseToken to currently selected value **/ 
				$('#dwfrm_singleshipping_shippingAddress_addressFields_fleetwiseToken').val($(this).data("fleetwisetoken"));
				$('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryDate').val(textdate);
			}
		});
		
		$cache.addressForm.on("click", ".right-left", function () {
			/** grab the 2nd set of time slots **/
			$(".chosen-week").addClass("left-right").removeClass("right-left");
			$(".schedule:last").addClass("selected");
			$(".schedule:first").removeClass("selected");
		});

		$cache.addressForm.on("click", ".left-right", function () {
			/** grab the 1st set of time slots **/
			$(".chosen-week").addClass("right-left").removeClass("left-right");
			$(".schedule:last").removeClass("selected");
			$(".schedule:first").addClass("selected");
		});

		$cache.addressForm.on('change', '#dwfrm_singleshipping_shippingAddress_addressFields_cities_city', function(e){

				if($(this).val().length > 0 && $cache.cityName != $(this).val()){
					$cache.cityName = $(this).val();
					
					// ONLY do this next bit and call CheckATP from here if STATE_VALS has been defined
					// why.. b/c if STATE_VALS is undefined, it means the CITSTATEFROMZIP service is down
					// which means this change event gets fired on an element that technically no longer exists in the UI
					if(typeof STATE_VALS != 'undefined') {
						var stateArray = STATE_VALS.split('|'), selectedIndex = parseInt($(this)[0].selectedIndex - 1), stateVal = stateArray[selectedIndex];
						$('#dwfrm_singleshipping_shippingAddress_addressFields_state, #dwfrm_singleshipping_shippingAddress_addressFields_states_stateUSNOAPO').val(stateVal);
						$cache.stateCodeStr = stateVal;
						
						var data = $(this).parents('form').serialize();
						checkAtp(data);
					}
				}
		});
		
		/*$cache.addressForm.on('change', '#dwfrm_singleshipping_shippingAddress_addressFields_states_stateUSNOAPO', function(e){
			
			if($(this).val().length > 0 && $cache.cityName != $(this).val()){
				var data = $(this).parents('form').serialize();
				checkAtp(data);
			}
			 
		});*/
		
		$cache.addressForm.on('click', '#addressSuggestion .address-suggestion', function(e){
			if ($(this).attr("id") == "use-suggested") {
				if (!($(this).data('addressline1') === undefined)) $('#dwfrm_singleshipping_shippingAddress_addressFields_address1').val($(this).data('addressline1'));
				if (!($(this).data('city') === undefined)) {
					$('#dwfrm_singleshipping_shippingAddress_addressFields_cities_city').val($(this).data('city').toUpperCase());
				}
				if (!($(this).data('statecode') === undefined)) $('#dwfrm_singleshipping_shippingAddress_addressFields_state, #dwfrm_singleshipping_shippingAddress_addressFields_states_stateUSNOAPO').val($(this).data('statecode'));
				if (!($(this).data('zipcode') === undefined)) $('#dwfrm_singleshipping_shippingAddress_addressFields_zip').val($(this).data('zipcode'));
				var data = $(this).parents('form').serialize() + '&keep=false'; 
			} else {
				var data = $(this).parents('form').serialize() + '&keep=true'; 
			}			 	
			checkAtp(data);
			e.preventDefault();

		});
		
		$cache.addressForm.on('click', '#window-target a', function(e){
			var li = $('#window-target li.active'),
				thisLi = $(this).parent('li'),
				index = $(this).attr('id');
				id = index.split('-')[1];
			
			li.removeClass('active');
			thisLi.addClass('active');
			$('.date-windows').removeClass('active');
			$('#dateWindow-'+id).addClass('active');
			
			e.preventDefault();
		});
		
		$cache.addressForm.on('click', '#withRep', function(e){
			deliveryWindowInit();
		});
		
		$cache.addressForm.on("keyup", '#dwfrm_singleshipping_shippingAddress_addressFields_zip', function(e){

			var _zip = $.trim($(this).val());
 
				// 1. Verify a valid US 5-digit ZIP first, and that the same ZIP was not entered again
			if( /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(_zip) && _zip != app.clientcache.SESSION_ZIP){

				$cache.cityName = $cache.stateCodeStr = null;
				app.clientcache.SESSION_ZIP = _zip;
				$cache.postalCode.removeClass('error');
				app.progress.show($cache.addressForm);

				$.ajax({
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					url: app.urls.cityAndStateForZip,
					async: false,
					cache: false,
					data: { "zipcode" : _zip },
					
					success : function (data) {
						// Check what was returned
						var $cityEl = $("select.city");
						var $stateEl = $("select.state");
									
						$cityEl.empty();
						
						if(data.cities.length > 0) {
							
							// Add "Choose a City"
							if(data.cities.length > 1)
								$cityEl.append($("<option />").attr("value", "").text("Choose a City"));
							
							for(var __i=0;__i<data.cities.length;__i++) {
								$opt = $("<option />");			
							    $opt.attr("value", data.cities[__i].city).text(data.cities[__i].city);
							    
							    // If there is only a single city object returned, auto select it.
								if(data.cities.length == 1)
									$opt.attr("selected", "selected");
								
								$cityEl.append($opt);

								if(__i==0) {
									$stateEl.val(data.cities[__i].state);
									$stateEl.dropkick('refresh');
								}
							}
							$cityEl.dropkick('refresh');
						} else {
							// No city objects returned from service, switch over to manual input.
							var _name = $cityEl.attr("name");
							var $formRow = $cityEl.closest(".form-row");
							$formRow.find(".dk_container").remove();
							$formRow.find("select").remove();
							
							$input = $("<input type='text' class='input-text required' id='"+_name+"' name='"+_name+"' max-length='50' />");			
						    
							$formRow.append($input);
						}			
						
						//deliveryWindowInit();
						app.progress.hide();
					},
					error : function(data) {
						app.progress.hide();							
					}
				});
			}
 
		});
		
		
	}
	
	/*************** app.enhancedShipping public object ***************/
	app.enhancedShipping = {
		init : function () {
			initializeCache();
			initializeEvents();
			initializeDom();
		}
	}
	
}(window.app = window.app || {}, jQuery));

$(document).ready( function(){
	app.enhancedShipping.init();
});
