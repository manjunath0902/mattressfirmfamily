(function(){
	$('#dwfrm_checkdeliverystatus_phoneNumber_phoneAreaCode').autotab({ format: 'number', target: '#dwfrm_checkdeliverystatus_phoneNumber_phonePrefix' });
	$('#dwfrm_checkdeliverystatus_phoneNumber_phonePrefix').autotab({ format: 'number', target: '#dwfrm_checkdeliverystatus_phoneNumber_phoneLineNumber', previous: '#dwfrm_checkdeliverystatus_phoneNumber_phoneAreaCode' });
	$('#dwfrm_checkdeliverystatus_phoneNumber_phoneLineNumber').autotab({ format: 'number', previous: '#dwfrm_checkdeliverystatus_phoneNumber_phonePrefix' });
})();

