/**
@class app.refineModule
*/
(function(app, $) {
	var $cache;
	
	/**
	 * @private
	 * @function
	 * @description Initializes the cache for the Refine Module on the Segment pages.
	 */
	function initializeCache() {
		$cache = {
			parent : $('.refine-module')
		};
	}
	
	/**
	 * @private
	 * @function
	 * @description Initializes events for the Refine Module.
	 */
	function initializeEvents() {
		
		$cache.parent.on("change", "select", function(e) {
			var target = $cache.parent.find('#refine-target');
			var url = $(this).val();

			app.ajax.load({
				url: url,
				callback : function (data) {
					target.html(data);
					target.find('select').not('.skip-dk').dropkick();
				}
			});
		});
	}
	
	/*************** app.refineModule public object ***************/
	app.refineModule = {
		init : function() {
			initializeCache();
			initializeEvents();
		}
	}
	
}(window.app = window.app || {}, jQuery));

/**
@class app.mattressFinder
*/
(function(app, $) {
	var $cache;
	
	/**
	 * @private
	 * @function
	 * @description Initializes the cache for the Refine Module on the Segment pages.
	 */
	function initializeCache() {
		$cache = {
			parent	: $('.mattressfinder-module'),
			target	: $('#mf-target'),
			step	: 1,
			zip		: '',
			zone	: '',
			baseUrl	: false
		};
	}
	
	/**
	 * @private
	 * @function
	 * @description Initializes events for the Refine Module.
	 */
	function initializeEvents() { 
		
		$cache.parent.on("click", "a.refLink", function(e) {
			$cache.baseUrl = $(this).attr('href');
			app.progress.show($cache.parent);
			updateUI($cache.baseUrl + '&step=' + $cache.step + '&zip=' + $cache.zip);
			e.preventDefault();
		});
		
		$cache.parent.on('click', 'a.button', function(e) {
			if ($(this).hasClass('prev') || ( $(this).hasClass('next') && $(this).parent().siblings('ul').children('li').hasClass('selected'))) {
				$cache.step = parseInt($(this).data('step'));
				if ($cache.step !== 0) {
					$(this).parents('.mf-step').fadeOut(300, function() {
						$('.step-' + $cache.step).fadeIn(300);
					});
				} else {
					window.location = $(this).attr('href');
				}
			} else {
				$('.errordisplay').show();
			}
			
			e.preventDefault();
		});
		
		$cache.parent.on('keyup', '#mf-zip', function(e) {
			var _zip = $(this).val();
			
			if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(_zip)) { 
				if (!$cache.baseUrl) {
					alert('select a price range first.');
				} else {
					app.progress.show($cache.parent);
					$cache.zip = _zip;
					$.ajax({
						url: app.urls.getZoneForMF,
						data: {zip: _zip},
						type: 'post',
						dataType: 'json',
						success: function(json) {
							var a = json.zonecode.split(',').join('|'); 
							$cache.zone = a;
							updateUI($cache.baseUrl + '&step=1&prefn1=zone_code&prefv1=' + $cache.zone + '&zip=' + $cache.zip); 
						}
					});
				}
			}
			e.preventDefault();
		});
	}
	
	function updateUI(url) {
		app.ajax.load({
			url: url,
			callback : function(data) {
				$cache.target.html(data);
				app.progress.hide();
			}
		});
	}
	
	/*************** app.refineModule public object ***************/
	app.mattressFinder = {
		init : function() {
			initializeCache();
			initializeEvents();
		}
	}
	
}(window.app = window.app || {}, jQuery));

/**
@class app.locationSearch
*/
(function(app, $) {
	var $cache;
	
	/*************** app.locationSearch private vars and functions ***************/
	function checkForLocation() {
		$.ajax({
			url: app.urls.checkSavedLocation,
			dataType: 'html',
			success: function(html) {
				if ($.trim(html).length) {
					showSavedLocation(html);
				} else {
					showZipForms();
				}
			}
		});
	}
	
	function showSavedLocation(html) {
		$.each($cache.parent, function(i,e) {
			var form = $('#findByZipAjax').find('form'), savedLocation = $(this).find('aside');
			//console.log(form + ' ' + savedLocation);
			form.hide();
			savedLocation.html(html).fadeIn(200);		
		});
	}
	
	function showZipForms() {
		$cache.parent.each(function(i,e) {
			var form = $('#findByZipAjax').find('form'), savedLocation = $('#findByZipAjax').find('aside');
			savedLocation.hide().html('');
			form.fadeIn(200);
		});
	}
	
	function hideResults(target) {
		target.fadeOut(200, function(){target.html('');});
	}
	
	/**
	 * @private
	 * @function
	 * @description Initializes the cache for the Ajax Find Loaction By Zip.
	 */
	function initializeCache() {
		$cache = {
			parent : $(".findStoresModule")
		};
	}
	
	/**
	 * @private
	 * @function
	 * @description Initializes events for the Ajax Find Loaction By Zip.
	 */
	function initializeEvents() {
		
		checkForLocation();
		
		$cache.parent.on('submit', 'form', function(e) {
			var data = $(this).serialize(), target = $(this).parent().find('section'), url = $(this).attr('action'), input = $(this).find('input.p-code'), zip = $.trim(input.val()), form = $(this).find('form');
			input.removeClass('error').focus();
			
			if (zip !== '') {
				$.ajax({
					url: url,
					type: 'post',
					data: data,
					success : function(data) {
						target.html(data).fadeIn(200);
					}
				});
			} else {
				input.addClass('error');
			}
			
			e.preventDefault();
			return false;
			
		}).on('click', 'a.set-location', function(e) {
			var url = $(this).attr('href'), input = $cache.parent.find('input.p-code');
			
			$.ajax({
				url: url,
				type: 'post',
				dataType: 'html',
				success : function(html) {
					showSavedLocation(html);
					input.val('');
					hideResults($('section.results'));
				}
			});
			e.preventDefault();
		}).on('click', 'a.change-location', function(e) {
			console.log(this);
			showZipForms();
			e.preventDefault();
		});

	}

	/*************** app.locationSearch public object ***************/
	app.locationSearch = {
		init : function() {
			initializeCache();
			initializeEvents();
		}
	}
	

}(window.app = window.app || {}, jQuery));

/**
@class app.pdpZipZone
* this also services the Customer Zone in the Header
*/
(function(app, $) {
	var $cache;
	
	/**
	 * @private
	 * @function
	 * @description Initializes the cache for the Zip/Zone check on the PDP page.
	 */
	function initializeCache() {
		$cache = {
			parent : $('#primary'),
			locationParent: $('[id="CustomerZone"]')
		};
	}
	
	/**
	 * @private
	 * @function
	 * @description Initializes events for the PDP Zip/Zone check.
	 */
	function initializeEvents() {
		
		$cache.locationParent.on('click', 'a', function(e) {
			if ($(this).siblings('input:visible').length) {
				$(this).siblings('input').hide();
			} else {
				$(this).siblings('input').show().focus();
			}
			e.preventDefault();
		}).on('keydown', 'input', function(e) {
			$(this).removeClass('invalidZip');
		});
		
		$cache.locationParent.on("keyup", 'input', function(e) {
		
			var _zip = $.trim($(this).val());
			// 1. Verify a valid US 5-digit ZIP first, and that the same ZIP was not entered again
			if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(_zip)) {
				var data = {zip: _zip};
				var url = app.urls.initZoneCheck;
				
				$.ajax({
					url: url,
					type: 'post',
					data: data,
					dataType: 'html',
					beforeSend : function() { app.progress.show(); },
					success : function(data) {

						// Check what was returned
						if ($.trim(data) === "invalidZip") {
							$cache.locationParent.find('.p-code').addClass('invalidZip').val("Invalid Zip Code");
						} else {
							$cache.locationParent.html(data);
							if ($('.variation-select').length > 0) {
								$('#va-size').change();
							} else {
								//window.location.reload();
								var zoneCode = $('#CustomerZone input[name=postalCode]').data('zoneCode') != null ? $('#CustomerZone input[name=postalCode]').data('zoneCode').replace(',' , '|') : '';
								
								var newUrl = window.location.href;

								if ((zoneCode != '' && v_category != '') || (newUrl.indexOf('search?q=') > -1 && newUrl.indexOf('zone_code') > 1)) {								
									if (window.location.hash.length > 0) {
										newUrl = newUrl.substring(0, newUrl.indexOf('#') -1);
										newUrl = newUrl + '#prefn1=zone_code&prefv1='+zoneCode;
									} else if (newUrl.indexOf('search?q=') > -1) {
										newUrl = newUrl.substring(0, newUrl.indexOf('prefv1'));
										newUrl = newUrl + 'prefv1='+zoneCode;
									}
								}
								
								var link = document.createElement("a");
								link.setAttribute('href', newUrl);
								document.body.appendChild(link);
								link.click();
							}
						}
					},
					complete : function() { app.progress.hide(); }
				});
			}
		});
		
		$cache.parent.on("keyup", '#zipZone', function(e) {
			var _zip = $.trim($(this).val()); 
			
			// 1. Verify a valid US 5-digit ZIP first, and that the same ZIP was not entered again
			if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(_zip)) {
				var data = {zip: _zip, pid : $('#pid').val()};
				var url = app.urls.pdpZoneCheck;
				$.ajax({
					url: url,
					type: 'post',
					data: data,
					beforeSend: function() {
						app.progress.show("#product-content");
					},
					dataType: 'html',
					success : function (data) {
						// Check what was returned
						$cache.parent.find('.availability .target').html(data);
						var newZip = $('#newZip').val();
						if ($('#AvailInZone').val() === 'true') {
							$('#add-to-cart').removeAttr('disabled');
						} else {
							$('#add-to-cart').attr('disabled', 'true');
						}
						if (newZip) {
							$("#CustomerZone a").html(newZip);
						}
					},
					error : function(request, status, error) {
						/* service is down or had an error so enable the add to cart button */
						$('#add-to-cart').removeAttr('disabled');
					},
					complete : function() {
						app.progress.hide("#product-content");
					}
				});
			}
		});
	}
	
	/*************** app.pdpZipZone public object ***************/
	app.pdpZipZone = {
		init : function() {
			initializeCache();
			initializeEvents();
		}
	}
	
}(window.app = window.app || {}, jQuery));

/**
@class app.emailSignup
*/

(function(app, $) {

	var $cache;

	/**
	 * @private
	 * @function
	 * @description Initializes the cache for the Email Signup in the footer
	 */
	function initializeCache() {
		$cache = {
			parent: $('#emailsignup'),
			target: $('#dwfrm_emailsignup_email'),
			pageSource: $('#pageSourceFooter')
		};
	}

	function initializeEvents() {

		$cache.parent.on('click', "input[type=submit]", function(e) {
			var _email = $($cache.target).val();
			var _pageSource = $($cache.pageSource).val();
			
			if (/^[a-zA-Z0-9]+(?:(\.|_)[A-Za-z0-9!#$%&'*+/=?^`{|}~-]+)*@(?!([a-zA-Z0-9]*\.[a-zA-Z0-9]*\.[a-zA-Z0-9]*\.))(?:[A-Za-z0-9](?:[a-zA-Z0-9-]*[A-Za-z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/.test(_email)) {
				$.ajax({
					url: app.urls.emailSignup,
					data: {
						ajaxEmail : _email, 
						pageSource : _pageSource
					},
					success: function(html){
						$cache.parent.html(html);
					}
				});
			} else {
				$($cache.parent).find('.error').remove().end().append(
						$('<div/>', {"class":"error"}).html('Please enter a valid email address')
				);
			}

			return false;
		});
	}
	/*************** app.emailSignup public object ***************/
	app.emailSignup = {
		init : function () {
			initializeCache();
			initializeEvents();
		}
	}
}(window.app = window.app || {}, jQuery));

/**
@class app.mattressremoval
*/
(function(app, $) {

	var $cache;

	/**
	 * @private
	 * @function
	 * @description Initializes the cache for the mattress removal dropdown in cart
	 */
	function initializeCache() {
		$cache = {
			parent: $('#mattressRemovalSelect'),
			target: $('#mattressRemovalApply')
		};
	}

	function initializeEvents(){
		$cache.parent.on('change',  function(e){
			$cache.target.trigger('click');
			return false;
		});
	}
	/*************** app.mattressremoval public object ***************/
	app.mattressremoval = {
		init : function () {
			initializeCache();
			initializeEvents();
		}
	}
}(window.app = window.app || {}, jQuery));

/**
@class app.amplience
*/
(function(app, $) {
	
	app.amplience = {
		initZoomViewer : function(ampset) {
			
			// start fresh
			app.amplience.destroy();

			var jsonSet = ampset != null && ampset != '' ? ampset : $('.product-meta').data('jsonset'),
				sash = $('.product-meta').data('sash'),
				ampLoading = true;
			var altimg = $('.product-meta').data('alt');
			// setup amplience instance
			amp.init({
				client_id: 'hmk',
				di_basepath: 'https://i1.adis.ws/',
				errImg: '../shared/404image.jpg'
			});

			// create html from image set json
			amp.get([{'name': jsonSet, 'type': 's'}], function(data) {
			
				// start fresh
				app.amplience.destroy();

				var dis = amp.di.set(data[jsonSet], {h: 500, w: 598, sm: 'CM'});

				if (sash) {
					dis['items'][0]['src'] += sash;
				}
				if (data[jsonSet].items.length > 1) {
					for (var i = 0; i < data[jsonSet].items.length; i++) {
						if (altimg) {
							dis['items'][i]['alt'] = altimg;
						}	
					}
				}
				else {
					if (altimg) {
						dis['items'][0]['alt'] = altimg;
					}	
				}

				var contents =  $('#amplience-main').first();
				var template = Handlebars.compile($("#templateimage").html());
				var dom = template(dis);
				contents.append(dom);

				//amp.genHTML(dis, $('#amplience-main')[0], true);

				var ampCach = {
					ul  : $('#amplience-main').find('ul'),
					img : $('#amplience-main').find('img')
				};

				if (data[jsonSet].items.length > 1) {

					for (var i = 0; i < data[jsonSet].items.length; i++) {
						var item = data[jsonSet].items[i];

						if (item.set) {
							item.type = "img";
						}
						if (altimg) {
							dis['items'][i]['alt'] = altimg;
						}	
					}

					var contents =  $('#amplience-carousel').first();
					var template = Handlebars.compile($("#templatecarousel").html());
					var dom = template(amp.di.set(data[jsonSet], {h: 80, w: 150, sm: 'CM'}));
					contents.append(dom);
					//amp.genHTML(amp.di.set(data[jsonSet], {h: 80, w: 150, sm: 'CM'}), $('#amplience-carousel')[0], true);
					
					var ampNav = {
						img : $('#amplience-nav').find('img'),
						ul  : $('#amplience-nav').find('ul')
					};

					ampNav.img.ampImage({preload: 'visible'});

					ampNav.ul.ampCarousel({
						height: 100,
						easing: 'ease-in-out',
						animSpeed: 500,
						loop: false
					});
					
					ampNav.ul.ampNav({
						on: 'select',
						action: 'goTo',
						selector: '#amplience-main ul'
					});
					
					$('#amplience-nav').find('.prev').bind('click', function(e) {
						ampNav.ul.ampCarousel('prev');
					});

					$('#amplience-nav').find('.next').bind('click', function(e) {
						ampNav.ul.ampCarousel('next');
					});
				}

				// Create carousel with html created above
				ampCach.ul.ampCarousel({
					width: 598,
					height: 500,
					start: 0
				});

				// set zoombox fosset from top
				$('#zoom-box').css({top: ($('.amplience-viewer').offset().top - 50) + 'px'});
			
				ampCach.img.ampZoom({
					translations: 'sm=CM',
					preload: {
						image: 'created',
						zoomed: 'none'
					},
					zoom: 2,
					cursor: {
						active: 'zoom-out',
						inactive: 'zoom-in'
					}
				});

				$(document).on('click', function() {
					if (!ampLoading) {
						ampCach.img.ampZoom('zoom', false);
					}
				});

				$('#amplience-main').on('mouseenter', function() {
					ampLoading = true;
				}).on('mouseleave', function() {
					ampLoading = false;
				});
				var zoomImg = $('.amp-zoom-img');
				zoomImg.attr('alt', altimg);
			});
		},

		// Destroy current PDP viewer
		destroy : function() {
			$('#amplience-main').html("");
			$('#amplience-carousel').html("");
		}
	}
	
}(window.app = window.app || {}, jQuery));

$(document).ready(function() {
	$('.datepicker').datepicker();
	$('.sorts, select').not('.skip-dk').dropkick();
	$('.product-tabs').tabs();
	$('.scroll-pane, .scrollable').jScrollPane();
	$('.expandable-adspace').click(function() {
		var icon = $(this).children('.icon');
		var ad = $('a.adspace');
		if (ad.is(':visible')) {
			ad.slideUp('slow'); icon.removeClass('close');
		} else { 
			ad.fadeIn('slow'); icon.addClass('close');
		}	
	});
	
	$('#CustomerZone input')
		.focus(function(e) {
			var cuszone = $(this).val();
			$(this).val('');
		});
	
	if ($('.disable-if-empty')) {
		$('.disable-if-empty').each(function() {
			var disabledButton = $(this).find('.disabled');
			$(this).find('.disable-target').on('change', function() {
				var ok = $(this).val().length ? true : false; 
				if (ok) {
					disabledButton.removeAttr('disabled').removeClass('disabled');
				} else {
					disabledButton.attr('disabled', 'disabled').addClass('disabled');
				}
			});
		});		
	}

	// Refine Module 
	app.refineModule.init();
	
	// Mini Search Location By Zip
	app.locationSearch.init();
	
	// Mattress Finder Init
	app.mattressFinder.init();
	
	//PDP Zip Zone Check
	app.pdpZipZone.init();
	
	// EmailSignup
	app.emailSignup.init();

	//mattress removal
	app.mattressremoval.init();
	
	// Amplience Utils
	app.amplience.initZoomViewer();
	
	//establish uppercasing for values that need it. coupon, etc 
	$('.uppercaseit').on('keyup', function() { 
		this.value = this.value.toUpperCase();
	});
	//carousels and swiping

	 var oneTileOptions = {
			 autoWidth: true,
			 responsive: true,
			 visible: 1,
			 btnGo: $('.jcarousel-control a')
	 };
	 var homepageCarouselOptions = {
			 autoWidth: true,
			 responsive: true,
			 visible: 1,
			 btnGo: $('.jcarousel-control a'),
			 auto: true,
			 speed: 2000,
			 timeout: 4500
	 };
	 var ratingsTileOptions = {
			 autoCSS: false, 
			 circular: false,
			 responsive: true,
			 visible: 1,
			 btnPrev: function() {
				 return $(this).find('.jcarousel-prev-horizontal');
			 },
			 btnNext: function() {
				 return $(this).find('.jcarousel-next-horizontal');
			 }		 
	 };
	 var fourTileOptions = {
				responsive: true,
				autoCSS: true,
				autoWidth: true,
				circular: false,
				btnPrev: function() {
				  return $(this).find('.jcarousel-prev-horizontal');
				},
				btnNext: function() {
				  return $(this).find('.jcarousel-next-horizontal');
				}
			  };
	 var fiveTileOptions = {
			 autoWidth: true,
			 responsive: true,
			 visible: 5,
			 btnGo: $('.jcarousel-control a')
	 };
	 $('.one-tile').each(function() {
		 if ($(this).parent().attr('id') === 'homepage_slider') {
			 $(this).jCarouselLite(homepageCarouselOptions);
		 } else {
			 $(this).jCarouselLite(oneTileOptions);
		 }
	 });
	 $('.product-tile-rating').each(function() { $(this).jCarouselLite(ratingsTileOptions); });
	 $('.four-tiles').each(function() { $(this).jCarouselLite(fourTileOptions); });
	 $('.promo-carousel').each(function() { $(this).jCarouselLite(fourTileOptions); }); 
	 $('.five-tiles').each(function() { $(this).jCarouselLite(fiveTileOptions); });
	//MEDIAHIVE UPDATE custom for amplience, load all the color variations zoom players separately, then show on select
	amplienceClass = $.trim($('#va-color').find('option:selected').text());
	$('.primary-image-amplience li').each(function() {
		$(this).removeClass('active');
		if ($(this).hasClass(amplienceClass)) $(this).addClass('active');
	});
	
});

var zoneCode = $('#CustomerZone input[name=postalCode]').data('zoneCode') != null ? $('#CustomerZone input[name=postalCode]').data('zoneCode').replace(',' , '|') : '';
var currentURL = window.location.href;
/*if (zoneCode != '' && v_category != '' && window.location.hash.length == 0) {
	var newUrl = window.location.href + '#prefn1=zone_code&prefv1='+zoneCode;
	var link = document.createElement("a");
	link.setAttribute('href', newUrl);
	document.body.appendChild(link);
	link.click();
}*/