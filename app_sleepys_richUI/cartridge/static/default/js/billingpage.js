/**
@class app.enhancedBilling
*/
(function (app, $) {
	var $cache;
	
	/**
	 * @private
	 * @function
	 * @description Initializes the cache for the Enhanced Shipping page features.
	 */
	function initializeCache() {
		$cache = {
			billingAddressForm:		$('#dwfrm_billing'),
			storedBillingAddress: 	$('#dwfrm_billing_addressList'),
			firstName:				$('#dwfrm_billing_billingAddress_addressFields_firstName'),
			lastName:				$('#dwfrm_billing_billingAddress_addressFields_lastName'),
			address1:				$('#dwfrm_billing_billingAddress_addressFields_address1'),
			address2:				$('#dwfrm_billing_billingAddress_addressFields_address2'),
			phone:					$('#dwfrm_billing_billingAddress_addressFields_phone'),
			email:					$('#dwfrm_billing_billingAddress_email_emailAddress'),
			postalCode: 			$('#dwfrm_billing_billingAddress_addressFields_zip'),
			city: 					$('#dwfrm_billing_billingAddress_addressFields_cities_city'),
			stateCode: 				$('#dwfrm_billing_billingAddress_addressFields_states_state'),
			countryCode: 			$('#dwfrm_billing_billingAddress_addressFields_countryCode'),
			mobilePhone:	 		$('#dwfrm_billing_billingAddress_addressFields_mobilePhone'),
			crossStreets:	 		$('#dwfrm_billing_billingAddress_addressFields_crossStreets'),
			apartment: 				$('#dwfrm_billing_billingAddress_addressFields_apartment'),
			companyName: 			$('#dwfrm_billing_billingAddress_addressFields_companyName'),
			storedCards: 			$('#PaymentMethod_CREDIT_CARD'),
			ccOwner: 				$('#dwfrm_billing_paymentMethods_creditCard_owner'),
			ccNumber: 				$('#dwfrm_billing_paymentMethods_creditCard_number'),
			ccCVN: 					$('#dwfrm_billing_paymentMethods_creditCard_cvn'),
			ccMonth: 				$('#dwfrm_billing_paymentMethods_creditCard_month'),
			ccYear: 				$('#dwfrm_billing_paymentMethods_creditCard_year'),
			ccType: 				$('#dwfrm_billing_paymentMethods_creditCard_type'),
			cityName:				'',
			stateCodeStr:			''
		};
	}
	

	function initializeDom(){
		//checkRequiredComplete();
		/*$('input.required').each(function() {
			$(this).on('focus', function(e) {
				checkRequiredComplete();
			});	
		});
		
		//task #5126 Make Shipping & Delivery Address City Value error more prominent, apply to billing city as well
		if( $cache.postalCode.val() && !($cache.city.val() ) ) {
			//figure out the city offset $cache.city.offset().top returns 0
			var dest = $('#dk_container_dwfrm_billing_billingAddress_addressFields_cities_city');
			var scrollDistance = Math.floor(dest.offset().top - dest.outerHeight());
			$('html, body').animate({
				scrollTop: scrollDistance,
			}, 800);
			dest.find('.dk_label').addClass('error');
		}*/

	}
	
	/*function checkRequiredComplete(){
		if($.trim($cache.firstName.val()) == '' || $.trim($cache.lastName.val()) == '' || $.trim($cache.address1.val()) == '' || $.trim($cache.email.val()) == '' || $.trim($cache.ccOwner.val()) == '' || $.trim($cache.ccNumber.val()) == '' || $.trim($cache.ccCVN.val()) == ''){
			$cache.postalCode.attr('readonly', true); 
			//console.log('check req false');
			return false
		}else{
			$cache.postalCode.attr('readonly', false); 
			//console.log('check req true');
			return true;
		}
	}*/
	
	/*function checkAddress(data){
		app.progress.show($cache.billingAddressForm);
		$.ajax({
			url: app.urls.checkBillingAddress,
			type: 'post',
			data: data+"&format=ajax",
			dataType: 'html',
			success: function(data){
				$cache.billingAddressForm.html(data);
				initializeCache();
				$cache.stateCode.attr('readonly', true);
				//not in billing deliveryWindowInit();
				initializeCreditCardType();
				app.progress.hide();
				$cache.billingAddressForm.find('select').dropkick();
				if($('#addressSuggestion').length){
					$('html, body').animate({
						scrollTop: $('#addressSuggestion').offset().top
					}, 300);
				}
			}
		});
	}*/

	/**
	 * Returns the type of the card based on the card number.
	 * @param ccNum {String}
	 * @return {String}
	 */
	/*function getCCType(ccNum) {
		var visa = /^4[0-9]+$/,
				masterCard = /^5[1-5][0-9]+$/,
				amex = /^3[47][0-9]+$/,
				discover = /^6(?:011|5[0-9]{2})[0-9]+$/;

		if (ccNum == null || ccNum.length < 2) { return null; }
		if (visa.test(ccNum)) {
			return "Visa";
		} else if (masterCard.test(ccNum)) {
			return "Master";
		} else if (amex.test(ccNum)) {
			return "Amex";
		} else if (discover.test(ccNum)) {
			return "Discover";
		}

		return null;
	}

	function initializeCreditCardType() {
		// 'enhance' the cc type UI
		var $container = $cache.ccType.closest('.form-row');
		
		if (!$container.hasClass('cc-type')) {
			$container.addClass('cc-type').append('<div class="card-indicator"> </div>');

			// attach listener for keyup events to update the selected type
			$cache.ccNumber.on('keyup', function(e){
				var ccNum = $(this).val(),
						ccTypeString = getCCType(ccNum);

				$container.removeClass('cc-visa cc-master cc-amex cc-discover');
				if (ccTypeString != null) {
					$cache.ccType.val(ccTypeString);
					$container.addClass('cc-'+ccTypeString.toLowerCase());
				}
			});
		}

		if ($cache.ccType.val().length > 0 && $cache.ccNumber.val() != '') {
			$container.removeClass('cc-visa cc-master cc-amex cc-discover');
			$container.addClass('cc-'+$cache.ccType.val().toLowerCase());
		}
	}*/
	
	/**
	 * @private
	 * @function
	 * @description Initializes events.
	 */
	function initializeEvents() {

		//initializeCreditCardType();
		
		/*$cache.billingAddressForm.on("change", "#dwfrm_billing_addressList", function () {
			//console.log (' bill stored change');
			var selected = $(this).children(":selected").first();
			var data = $(selected).data("address");
			if (!data) {  
				return; 
			}
			var p;
			
			for (p in data) {
				//console.log(data);
				//if cached value exists and new address values
				if ($cache[p] && data[p]) {
					//update values
					$cache[p].val(data[p].replace("^","'"));
					//console.log( $cache[p] && data[p] );
					if ($cache[p]===$cache.stateCode) {
						app.util.updateStateOptions($cache[p]);
						if($cache.stateFallback) {
							$cache.stateFallback.val(data.stateCode).trigger("change");
						}
						else {
							$cache.stateCode.val(data.stateCode).trigger("change");	
						}
					}
					if ($cache[p]===$cache.postalCode) {
						//triggers get cities
						$cache.postalCode.trigger("change");
					}
					else {
						//updateShippingMethodList();
					}
				}
				else {
					//console.log('null'+ $cache[p] + data[p] );
					//set null values to '' so that they dont populate the field with null
					data[p] = '';
					
				}
			}
			var dataNew = $cache.billingAddressForm.serialize()+"&apartment="+data.apartment+"&crossStreets="+data.crossStreets+"&mobilePhone="+data.mobilePhone+"&phone="+data.phone+"&address2="+data.address2+"&address1="+data.address1+"&zip="+data.postalCode+"&city="+data.city.toUpperCase()+"&dwfrm_billing_billingAddress_addressFields_city="+data.city.toUpperCase()+"&state="+data.stateCode+"&format=ajax";
			app.progress.show($cache.billingAddressForm);
			$.ajax({
				url: app.urls.processStoredBillingAddress,
				type: 'post',
				data: dataNew,
				dataType: 'html',
				success : function(data){
					$cache.billingAddressForm.html(data);
					$cache.billingAddressForm.find('select').dropkick();
					$cache.stateCode.attr('readonly', true);
					//$cache.postalCode.trigger("keyup");
					app.progress.hide();
				}
			});

		});*/
		
		/*$cache.billingAddressForm.on('change', '#dwfrm_billing_billingAddress_addressFields_cities_city', function(e){
				if($(this).val().length > 0 && $cache.cityName != $(this).val()){
					$cache.cityName = $(this).val();
					var stateArray = STATE_VALS.split('|'), selectedIndex = parseInt($(this)[0].selectedIndex -1), stateVal = stateArray[selectedIndex];
					
					$('#dwfrm_billing_billingAddress_addressFields_state').val(stateVal);
					$('#dwfrm_billing_billingAddress_addressFields_states_state').val(stateVal);
					$cache.stateCodeStr = stateVal;
					
					var data = $(this).parents('form').serialize();
					checkAddress(data);
				}
			
		});*/
		
		//check address when getcity fails
		/*$cache.billingAddressForm.on('change', '#dwfrm_billing_billingAddress_addressFields_states_stateUSNOAPO', function(e){
			
			if($(this).val().length > 0 && $cache.cityName != $(this).val()){
				var data = $(this).parents('form').serialize();
				checkAddress(data);
			}
			 
		});*/
		
		/*$cache.billingAddressForm.on('click', '#addressSuggestion table tr.selectors span', function(e){
			var option = $(this).attr('id').toLowerCase(), set = $(this).parents('table').find('a.'+option);
			//if option c, user clicked try again on -1 address, clear city/state/zip so they can start over.
			if(option=='c') {
				$cache.stateCodeStr = $cache.cityName = app.clientcache.SESSION_ZIP = null;
				$('#dwfrm_billing_billingAddress_addressFields_zip').val('');
				$('#dwfrm_billing_billingAddress_addressFields_state, #dwfrm_billing_billingAddress_addressFields_states_stateUSNOAPO').val('');
				$('#dwfrm_billing_billingAddress_addressFields_cities_city').val('');
			}
			$(set).each( function(i,e){ 
				var field = $(this).data('field'), val = $(this).text();
				
				switch(field){
					case 'addressLine1':
						$('#dwfrm_billing_billingAddress_addressFields_address1').val(val);
						break;
					case 'stateCode':
						$('#dwfrm_billing_billingAddress_addressFields_state, #dwfrm_billing_billingAddress_addressFields_states_stateUSNOAPO').val(val);	
						break;
					case 'zipCode':
						$('#dwfrm_billing_billingAddress_addressFields_zip').val(val);	
						break;
				}
				
				
			});
			
			$(set).each( function(i,e){ 
				var field = $(this).data('field'), val = $(this).text();
				if(field == 'city') $('#dwfrm_billing_billingAddress_addressFields_cities_city').val(val.toUpperCase()).change();
			});
			
			$(this).parents('tr').find('span.selected').removeClass('selected');
			$(this).addClass('selected');
			var keep = (option == 'a') ? true : false;
			var data = $(this).parents('form').serialize() + '&keep=' + keep;
			checkAddress(data);
			e.preventDefault();
		});*/
		
		
		//do we do it this
		
		/*$cache.billingAddressForm.on("keyup", '#dwfrm_billing_billingAddress_addressFields_zip', function(e){
			var _zip = $.trim($(this).val());

				// 1. Verify a valid US 5-digit ZIP first, and that the same ZIP was not entered again
				if( /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(_zip) && _zip != app.clientcache.SESSION_ZIP){
					$cache.cityName = $cache.stateCodeStr = null;
					app.clientcache.SESSION_ZIP = _zip;
					$cache.postalCode.removeClass('error');
					var form = $(this).parents('form');
					
					var data = form.serialize();
					var url = app.urls.cityAndStateForZipBilling;
					app.progress.show($cache.billingAddressForm);
					$.ajax({
						url: url,
						type: 'post',
						data: data+'&format=ajax',
						dataType: 'html',
						success : function (data) {
							// Check what was returned
							$cache.billingAddressForm.html(data);
							//console.log($('#dwfrm_billing_billingAddress_addressFields_cities_city'));
							$cache.billingAddressForm.find('select').dropkick();
							initializeCache()
							initializeCreditCardType();
							$cache.stateCode.attr('readonly', true);
							app.progress.hide();
						}
					});
				}

		});*/
	}
	
	/*************** app.enhancedShipping public object ***************/
	app.enhancedBilling = {
		/*init : function () {
			initializeCache();
			initializeEvents();
			initializeDom();
		},
		reInit: function () {
			initializeCache();
			initializeCreditCardType();
		}*/
	}
	
}(window.app = window.app || {}, jQuery));

$(document).ready( function(){
	//app.enhancedBilling.init();
});
