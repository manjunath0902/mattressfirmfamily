(function($,sr){
 
  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  var debounce = function (func, threshold, execAsap) {
      var timeout;
 
      return function debounced () {
          var obj = this, args = arguments;
          function delayed () {
              if (!execAsap)
                  func.apply(obj, args);
              timeout = null; 
          };
 
          if (timeout)
              clearTimeout(timeout);
          else if (execAsap)
              func.apply(obj, args);
 
          timeout = setTimeout(delayed, threshold || 500); 
      };
  }
	// smartresize 
	jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };
 
}(jQuery,'smartresize'));


/*
 * All java script logic for the mobile layout.
 *
 * The code relies on the jQuery JS library to
 * be also loaded.
 *
 * The logic extends the JS namespace app.*
 */
(function(app, $, undefined) {
	
	app.responsive = {
	    /*originally 500*/
		mobileLayoutWidth : 767,
		
		init : function () {

			$cache = {
					wrapper: $('#container'),
					navigation: $('#navigation'),
					homepageSlider: $('#homepage-slider'),
					footerNav: $('#footer-navigation'), 
					segmentHero: $('#segment-hero'),
					menuDropdowns: $('.menu-category'),
					syncheight: $('.syncheight')
				};
			
			// toggle menu element
			$cache.navigation.find('.navigation-header')
				.click(function(){
					jQuery(this).siblings('.menu-category').toggle();
				});

			// check onload to see if mobile enabled
			if( $cache.wrapper.width() <= this.mobileLayoutWidth ) {
				app.responsive.enableMobileNav();
				app.responsive.imageMapResize();
			}
	
		},
		
		// build vertical, collapsable menu
		enableMobileNav : function(){
			$('div.level-2 + span').remove();
			$('<span></span>').insertAfter('.menu-category li div.level-2'); 
			$cache.menuDropdowns.find('li span, li div.level-1').unbind('click');
			$cache.menuDropdowns.find('li span, li div.level-1').click(function(event) {
				if ( $(window).width() <= 767 ) {
					event.preventDefault(); 
					var lvl2 = jQuery(this).parent().children('.level-2');
					if(lvl2.is(':visible')) {
						lvl2.removeClass('open').slideUp()
					} else {
						lvl2.addClass('open').slideDown();
					}
				}
			});
		},

		
		// revert to standard horizontal menu
		disableMobileNav : function(){  
			$('div.level-2 + span').remove();
			$cache.menuDropdowns.find('div.level-2').removeAttr('style'); 
			jQuery('.refinement').removeAttr('style'); 
			$cache.menuDropdowns.removeAttr('style'); 
		},
		
		// pull the slideshow into a variable to re-use
		rebuildHomepageSlides: function() {
			 $('.horizontal-carousel').trigger('go', 0);
			 $('.jcarousel-control a').removeClass('active').eq(0).addClass('active');
		},
		// image map resize
		imageMapResize: function() {
			$('map').imageMapResize();
		},
		
		toggleGridWideTileView : function(){
			
			/*	toggle grid/wide tile	*/
			if(jQuery('.toggle-grid').length == 0 && (jQuery('.pt_order').length == 0) && (jQuery('.pt_content-search-result').length == 0))
			{
				jQuery('.results-hits').prepend('<a class="toggle-grid" href="'+location.href+'">+</a>');
				jQuery('.toggle-grid').click(function(){
					jQuery('.search-result-content').toggleClass('wide-tiles');
					return false;
				});
			}
			
		} 
		

	}
	
	
	
	
	$(document).ready(function(){
		$('.syncheight').syncHeight();
		app.responsive.init();
		
		// set up listener so we can update DOM if page grows/shrinks, only on bigger platforms
		if(screen.width > 767){
			
			$(window).smartresize(function(){
				$('.syncheight').removeAttr('style'); 
				app.responsive.rebuildHomepageSlides();
				 
				if( jQuery('#container').width() <= app.responsive.mobileLayoutWidth   ) {
					app.responsive.enableMobileNav(); 
					app.responsive.imageMapResize();
				}
				else {
					app.responsive.disableMobileNav(); 
				} 
				
			});
		
		}

	});
		
}(window.app = window.app || {}, jQuery));



