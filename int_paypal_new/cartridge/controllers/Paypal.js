'use strict';

/* global request response session */

var URLUtils = require('dw/web/URLUtils');
var Transaction = require('dw/system/Transaction');

var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var Cart = app.getModel('Cart');

/**
 * Render JSON from GET-url parameters
 */
function paramsToJson() {
    var paramNames = request.httpParameterMap.getParameterNames();
    var data = {};
    for (var i = 0; i < paramNames.length; i++) {
        var name = paramNames[i];
        data[name] = request.httpParameterMap.get(name).getStringValue();
    }
    response.setContentType('application/json');
    response.writer.print(JSON.stringify(data, null, 2));
}

/**
 * Make redirect
 *
 * @param {string} url - Redirect End point
 */
function renderRedirect(url) {
    app.getView({
        redirectUrl: url
    }).render('paypal/util/redirect');
}

/**
 * Render JSON from Object
 * @param {Object} data - Data
 */
function renderJson(data) {
    response.setContentType('application/json');
    response.writer.print(JSON.stringify(data, null, 2));
}

/**
 * Entry Point for the Express Checkout from cart. Should be linked from template.
 */
function startCheckoutFromCart() {
    var result = require('~/cartridge/scripts/paypal/controllerBase').startCheckoutFromCart();

    renderJson(result);
}

/**
 * Entry point for the returning back from PayPal. Is passed as a return url in PayPal api calls
 */
function returnFromPaypal() {
    var isFromCart = request.httpParameterMap.isFromCart.booleanValue;

    var result = require('~/cartridge/scripts/paypal/controllerBase').returnFromPaypal(isFromCart);

    if (result.paypalErrorMessage) {
        session.custom.paypalErrorMessage = result.paypalErrorMessage;
    }
    renderRedirect(result.redirectUrl);
}

/**
 * Entry Point for the Express Checkout from cart by using Customer Billing Agreemend ID. Should be linked from template.
 */
function startBillingAgreementCheckout() {
    var result = require('~/cartridge/scripts/paypal/controllerBase').startBillingAgreementCheckout();

    if (result.paypalErrorMessage) {
        session.custom.paypalErrorMessage = result.paypalErrorMessage;
    }
    renderRedirect(result.redirectUrl);
}

/**
 * Shipping Address Form for setting shipping address for Basket
 */
function editDefaultShippinAddress() {
    app.getForm('profile').clear();

    app.getView({
        CurrentForms: session.forms,
        ContinueUrl: URLUtils.https('Paypal-HandleDefaultShippinAddress')
    }).render('paypal/account/shippingAddressEdit');
}

/**
 * Handle Shipping Address Form action
 */
function handleDefaultShippinAddress() {
    app.getForm('profile').handleAction({
        edit: function () {
            if (!session.forms.profile.address.valid) {
                return editDefaultShippinAddress();
            }
            var cart = Cart.get();
            if (!cart) {
                return editDefaultShippinAddress();
            }

            var formFields = session.forms.profile.address;

            Transaction.wrap(function () {
                var shippingAddress = cart.object.defaultShipment.createShippingAddress();
                shippingAddress.setFirstName(formFields.firstname.value);
                shippingAddress.setLastName(formFields.lastname.value);
                shippingAddress.setAddress1(formFields.address1.value);
                shippingAddress.setAddress2(formFields.address2.value);
                shippingAddress.setCity(formFields.city.value);
                shippingAddress.setPostalCode(formFields.postal.value);
                shippingAddress.setStateCode(formFields.states.state.value);
                shippingAddress.setCountryCode(formFields.country.value);
                shippingAddress.setPhone(formFields.phone.value);
            });

            if (request.httpParameterMap.format.stringValue === 'ajax') {
                renderJson({ success: true });
            } else {
                startBillingAgreementCheckout();
            }
            return true;
        },
        cancel: function () {
            editDefaultShippinAddress();
        }
    });
}

exports.StartCheckoutFromCart = guard.ensure(['https'], startCheckoutFromCart);

exports.ReturnFromPaypal = guard.ensure(['https'], returnFromPaypal);

exports.StartBillingAgreementCheckout = guard.ensure(['https'], startBillingAgreementCheckout);

exports.EditDefaultShippinAddress = guard.ensure(['https'], editDefaultShippinAddress);
exports.HandleDefaultShippinAddress = guard.ensure(['https'], handleDefaultShippinAddress);

exports.ParamsToJson = guard.ensure(['https'], paramsToJson);

