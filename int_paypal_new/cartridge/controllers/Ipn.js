'use strict';

var guard = require('*/cartridge/scripts/guard');
var ipn = require('~/cartridge/scripts/paypal/ipn');

exports.Listener = guard.ensure(['https', 'post'], ipn.listen);
