'use strict';

var URLUtils = require('dw/web/URLUtils');

var app = require('*/cartridge/scripts/app');

/**
 * Make redirect
 *
 * @param {string} url - Redirect End point
 */
function renderRedirect(url) {
    app.getView({
        redirectUrl: url
    }).render('paypal/util/redirect');
}

/**
 * Create PayPal payment instrument and run Paypal-Handle function
 *
 * @param {Object} args - Arguments
 * @returns {Object} Handle call result
 */
function Handle(args) {
    var result = require('~/cartridge/scripts/paypal/processor').handle(args.Basket);
    if (result.redirectUrl) {
        renderRedirect(result.redirectUrl);
        return result;
    }
    if (result.error) {
        renderRedirect(URLUtils.https('Paypal-ParamsToJson', 'error', result.paypalErrorMessage, 'paypalBillingAgreementNotActaual', result.paypalBillingAgreementNotActaual));
    } else {
        renderRedirect(URLUtils.https('Paypal-ParamsToJson', 'token', result.paypalToken, 'paypalBillingAgreementIsActaual', result.paypalBillingAgreementIsActaual));
    }
    return result;
}

/**
 * Authorizes a payment using PayPal. The payment is authorized by using the PAYPAL processor
 * only and setting the order no as the transaction ID.
 *
 * @param {Object} args - Arguments
 * @returns {Object} Authorize call result
 */
function Authorize(args) {
    var result = require('~/cartridge/scripts/paypal/processor').authorize(args.Order, args.OrderNo, args.PaymentInstrument);
    if (result.redirectUrl) {
        renderRedirect(result.redirectUrl);
    }
    return result;
}

exports.Handle = Handle;
exports.Authorize = Authorize;
