'use strict';

/* global dw empty */

var SGHelper = {};

/**
 * Returns array with field options
 *
 * @param {dw.web.FormField} formField - Form field
 * @param {string} resourceFile - Resource file path
 * @returns {Array} Array of options
 */
SGHelper.getFieldOptions = function (formField, resourceFile) {
    if (empty(formField.options)) {
        return {};
    }
    var fields = {};

    var opts = formField.options;
    for (var o in opts) { // eslint-disable-line guard-for-in, no-restricted-syntax
        try {
            if (opts[o] && opts[o].value && opts[o].value.length > 0) {
                var option = opts[o];
                fields[option.value] = dw.web.Resource.msg(option.label, resourceFile, option.label);
            }
        } catch (error) {
            if (!fields.error) {
                fields.error = [];
            }
            fields.error.push('Error: ' + error);
        }
    }

    return fields;
};


/**
 * Returns object with countries and regions
 *
 * @param {dw.web.FormGroup} addressForm - Address Form Group
 * @param {string} resource - Resource id
 * @returns {Object} Array of options
 */
SGHelper.getCountriesAndRegions = function (addressForm, resource) {
    var list = {};
    var countryField = addressForm.country;
    var stateForm = addressForm.states;
    var resourceName = resource || 'forms';

    if (empty(countryField.options)) {
        return list;
    }

    var countryOptions = countryField.options;
    for (var o in countryOptions) { // eslint-disable-line guard-for-in, no-restricted-syntax
        try {
            if (countryOptions[o] && countryOptions[o].value && countryOptions[o].value.length > 0) {
                var option = countryOptions[o];
                var stateField = stateForm['state' + option.value];
                var postalField = addressForm['postal' + option.value];
                list[option.value] = {
                    regionLabel: dw.web.Resource.msg(stateField.label, resourceName, stateField.label),
                    regions: SGHelper.getFieldOptions(stateField, resourceName),
                    postalLabel: dw.web.Resource.msg(postalField.label, resourceName, postalField.label)
                };
            }
        } catch (error) {
            if (!list.error) {
                list.error = [];
            }
            list.error.push('Error: ' + error);
        }
    }
    return list;
};

module.exports = SGHelper;
