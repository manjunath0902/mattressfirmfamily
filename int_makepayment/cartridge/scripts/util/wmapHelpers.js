var UtilHelpers = {
    
    isServiceError : function(result){
    	if (result.error != 0 || result.errorMessage != null || result.mockResult) {
    		return true;
    	}
    	return false;
    },
    
    isOrderPaidOrCancelled : function(clCode){
    	if (clCode == "4002"){
			return true;
		}
		else if (clCode == "4004"){
			return true;
		}
    	return false;
    }
	
	
};

exports.UtilHelpers = UtilHelpers;
