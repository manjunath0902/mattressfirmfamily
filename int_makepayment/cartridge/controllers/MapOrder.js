'use strict';

/**
 * Controller for the billing logic.
 * functionality and is responsible for payment method selection and entering a billing address.
 *
 * @module controllers/MapOrder
 */

var Logger = require('dw/system/Logger');
/* Script Modules */
var URLUtils = require('dw/web/URLUtils');
var Resource = require('dw/web/Resource');
var StringUtils = require('dw/util/StringUtils');
var LogUtils=  require("app_mattressfirm_storefront/cartridge/scripts/util/LogUtilFunctions").LogUtilFunctions;
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var StringHelpers = require('app_storefront_core/cartridge/scripts/util/StringHelpers');
var emailHelper = require("app_storefront_controllers/cartridge/scripts/util/SFEmailSubscriptionHelper").SFEmailSubscriptionHelper;
var ISML = require('dw/template/ISML');
var PaymentInstrument = require('dw/order/PaymentInstrument');
var PaymentMgr = require('dw/order/PaymentMgr');
var Transaction = require('dw/system/Transaction');
var Countries = require('app_storefront_core/cartridge/scripts/util/Countries');
var ServiceRegistry = require('dw/svc/ServiceRegistry');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var URLUtils = require('dw/web/URLUtils');
var Pipeline = require('dw/system/Pipeline');
var OrderMgr = require('dw/order/OrderMgr');
var usaEpayService = require("int_usa_e_pay/cartridge/scripts/usaEpayService");
var MakeAPayemntServices = require("bc_sleepysc/cartridge/scripts/init/service-MakeAPaymentServices");
var PaymentError, FraudError, DeclinedReasonCode, isTransactionDeclinedByForter = false;
var params = request.httpParameterMap;
var isPartialPaymentsEnabled = ('enablePartialPayments' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.enablePartialPayments) ? true : false;
var isPartiallyPaid=false;
var displayBillingForm = false;

const SALES_ORDER_BALANCE= {"ID":"orderBalance", "description": "Sales Order Balance" } ;
const CREATE_ORDER_PAYMENT = {"ID":"createOrderPayment" , "description": "Create Order Payment"};
const RECAPTCHA = {"ID":"recaptcha" , "description": "Recaptcha"};
const USAEPAY_AUTHORIZE = {"ID":"usaePayAuthorize" , "description": "USAePay Authorize"};
const USAEPAY_CAPTURE = {"ID":"usaePayCapture" , "description": "USAePay Capture"};
const USAEPAY_VOID = {"ID":"usaePayVoid" , "description": "USAePay Void"};
const OTHER = {"ID":"other" , "description": "General"};


function addEmptyCCTypeByDefault(applicablePaymentCards) {
    var defaultCC = new Object();
    defaultCC.active = true;
    defaultCC.cardType = '';
    defaultCC.name = 'Select';
    applicablePaymentCards.addAt(0, defaultCC);
}

function billing() {
	updateMeadataMakePayment("makeapayment");
	app.getForm('billingform').clear();
	var requestOrderId = extractCaseSensitiveParam(request.httpParameterMap);//request.httpParameterMap.orderId.stringValue;
	var is_error 	= false;	
	var page_error 	= "";
	var isPaidOrder = false;
	var ApiSuccess 	= "";
	var clServiceError = false;
    var ApiResponse = new Object();    
	var result 		= new Object();
	var orderObject = new Object();
    if (requestOrderId != null) {
    	/*
    	 * HTTP-Service: SIT API[GetSalesOrderBalance]
    	 * Request Param: orderId
    	 * Response: JSON object with Order detail for partial payment.
    	*/    	
    	var svc = MakeAPayemntServices.SalesOrderBalanceMFRM;
    	svc.addParam("orderId", requestOrderId);
    	svc.setRequestMethod('GET');
    	result = svc.call();    	    	
        
        if (result.status == 'OK') {
        	ApiSuccess  	= "Yes";
        	ApiResponse 	= result.object.text;   
        	var countryCode = 'US';
        	var jsonOrderObj = JSON.parse(ApiResponse); 
			orderObject = jsonOrderObj.Data;
        	        	
            if (orderObject.AmountDue <= 0) {
        		page_error 	= Resource.msg('confirm.paid.full','makeapayment',null);
        		ApiSuccess  = "No";  
        		isPaidOrder = true;
        		
        		LogUtils.logAndEmailError(requestOrderId, SALES_ORDER_BALANCE, StringUtils.format("Amount Due {0} is less than equal to zero", orderObject.AmountDue));
        		
        		app.getView({   
        			is_error:is_error,
        			page_error:page_error,        		        	
        			requestOrderId: requestOrderId,
        			isPaidOrder: isPaidOrder,
        			displayBillingForm: displayBillingForm
        		}).render('billing');
            } else { // some Amount is due
            	//loading payment methods.
            	var applicablePaymentMethods = PaymentMgr.getApplicablePaymentMethods(customer, 'US', orderObject.AmountDue);
                var applicablePaymentCards = PaymentMgr.getPaymentMethod(PaymentInstrument.METHOD_CREDIT_CARD).getApplicablePaymentCards(customer, 'US', orderObject.AmountDue);
                
                var billingForm = app.getForm('billingform').object;
                
                var paymentMethods = billingForm.paymentMethods;
                if (paymentMethods.valid) {
                    paymentMethods.selectedPaymentMethodID.setOptions(applicablePaymentMethods.iterator());
                }
                var isBillingAddressAvailable = false;
                /*
                if(orderObject.Customer.Address) {
                	session.forms.billingform.fullname.value = orderObject.Customer.name;
                    session.forms.billingform.address1.value = orderObject.Customer.Address.Street;
                    if(orderObject.Customer.Address.StreetNumber.length > 0)
                    	session.forms.billingform.address2.value = orderObject.Customer.Address.StreetNumber;
                    session.forms.billingform.city.value = orderObject.Customer.Address.City;
                    session.forms.billingform.states.state.value = orderObject.Customer.Address.State;
                    session.forms.billingform.country.value = "US";
                    session.forms.billingform.phone.value = orderObject.Customer.Address.Phone;
                    session.forms.billingform.postal.value = orderObject.Customer.Address.ZipCode;
                    isBillingAddressAvailable = true;
                }
                */
                //Setting API Response In Session
                session.custom.orderObject = orderObject;
                displayBillingForm = true;                	
            	app.getView({
        			ContinueURL: URLUtils.https('MapOrder-HandleBilling'),		
        			is_error:is_error,        		
        			orderObj: orderObject,
        			requestOrderId: requestOrderId,
        			isBillingAddressAvailable: isBillingAddressAvailable,
        			isPartialPaymentsEnabled: isPartialPaymentsEnabled,
        			displayBillingForm: displayBillingForm
        		}).render('billing');
            } // some Amount is due
        } else { // Service result was not "OK"
        	var clErrorCode;
        	if(!empty(result.error) && (result.error == '400') && !empty(result.errorMessage)) {
				clErrorCode = JSON.parse(result.errorMessage).RefCode;
			} else {
				clErrorCode = !empty(result.error) ? result.error.toString() : '';
			}
			if(clErrorCode != undefined && clErrorCode == '4002') {
				is_error 	= true;	
				var contactSupportURL = URLUtils.url('Page-Show', 'cid', 'faq-customer-service');
				page_error = Resource.msgf('wmap.error.ordercancelled', 'makeapayment', null, requestOrderId, clErrorCode, contactSupportURL);
			} else if (clErrorCode != undefined && (clErrorCode == '5001' || clErrorCode == '0' || clErrorCode == '500')){
				is_error 	= true;	
				var contactSupportURL = URLUtils.url('Page-Show', 'cid', 'faq-customer-service');
				page_error = Resource.msgf('wmap.error.technicalerror', 'makeapayment', null, contactSupportURL, clErrorCode);
			} else if (clErrorCode != undefined && clErrorCode == '4001'){
				is_error 	= true;	
				page_error = Resource.msgf('wmap.error.ordernotexist', 'makeapayment', null, requestOrderId, clErrorCode);
			} else {
				is_error 	= true;	
	        	page_error 	= Resource.msg('confirm.error.technical','makeapayment',null);  	
			} 
			ApiSuccess  = "No";      	
        	LogUtils.logAndEmailError(requestOrderId, SALES_ORDER_BALANCE, result.errorMessage);
        	app.getView({
        		is_error: is_error,
        		clErrorCode: clErrorCode,
        		page_error: page_error,
                requestOrderId: requestOrderId,
                isPartialPaymentsEnabled: isPartialPaymentsEnabled,
                displayBillingForm: displayBillingForm
        	}).render('billing');
        }
    } else { // OrderId was empty
    	is_error 	= true;	
    	page_error 	= "Error: please make sure selected order is correct.";    	
    	app.getView({
    		is_error: is_error,
    		page_error: page_error,    		
    		requestOrderId: requestOrderId,
    		isPartialPaymentsEnabled: isPartialPaymentsEnabled
    	}).render('billing');
    }
}

function handleBilling(){
    app.getForm('billingform').handleAction({
    	ordersubmit: function () {
    		var form 			= app.getForm('billingform');
    		var responseError 	= "";
    		var is_error 		= false;
    		var page_error 		= "";	
    		var orderObj 		= new Object();    		
    		var partialPayAmount = "0.00";    		
    		var ResponseObj 	= new Object();
    		var fraudErrorMessage = "";
    		/*
        	 * HTTP[Get]-Service: Google reCaptcha
        	 * Request Param: g-recaptcha-response {auto generated random string from google recaptcha plugin}
        	 * Response: 'true' [in case captcha response is valid]  / 'false' [in case captcha reponse invalide]
        	*/    		
    		if (params.isParameterSubmitted("g-recaptcha-response") && params['g-recaptcha-response'] != "") {
    			var recaptcha_param = request.httpParameterMap['g-recaptcha-response'];
    			var svc = ServiceRegistry.get("sleepys.recaptcha.rest");
    			var reCaptcha_Site_URL   = dw.system.Site.getCurrent().getCustomPreferenceValue("reCaptcha_Site_URL");    			
    			var reCaptcha_Secret_Key = dw.system.Site.getCurrent().getCustomPreferenceValue("reCaptcha_Secret_Key");    					                         
    			svc.setURL(reCaptcha_Site_URL + "?secret=" + reCaptcha_Secret_Key + "&response=" + recaptcha_param);
    			var gc_result = svc.call();
    			if(gc_result.msg == "OK") {
    				var gc_response = JSON.parse(gc_result.object);
    				if (gc_response.success) {
    					//once captcha validated and response is true, 
    					//load all form values in order object for making payment against partial due amount.
    		    		//order details
    		    		orderObj.SalesId 									= params.SalesId.stringValue;
    		    		orderObj.OrderNo 									= params.SalesId.stringValue;
    		    		orderObj.OrderDate 									= params.OrderDate.stringValue;
    		    		orderObj.GrossAmount 								= params.GrossAmount.stringValue;
    		    		orderObj.AmountDue 									= params.AmountDue.stringValue;    		    		
    		    		//Allow partial payment only if it is enabled in Site Preference
    		    		partialPayAmount=orderObj.AmountDue;
    		    		if(isPartialPaymentsEnabled){
	    		    		if(params.PaymentOptions.submitted && params.PaymentOptions=='OtherAmountOption'){
	    		    			partialPayAmount 								= app.getForm('billingform.PartialPayment.otherAmount').value();
	    		    			isPartiallyPaid									= true;
	    		    		}
	    		    		else{
	    		    			partialPayAmount 								= params.AmountDue.stringValue;
	    		    			isPartiallyPaid									= false;
	    		    		}
    		    		}
    		    		orderObj.AmountPaid 								= params.AmountPaid.stringValue;
    		    		//customer details
    		    		orderObj.Customer = new Object();
    		    		orderObj.Customer.name 								= app.getForm('billingform.fullname').value();
    		    		orderObj.Customer.AccountNumber 					= params.CustomerAccountNumber.stringValue;
    		    		orderObj.Customer.Email 						    = app.getForm('billingform.email.emailAddress').value();
    		    		//paid amount / discounted amount.
    		    		orderObj.InventoryLocationId 						= params.InventoryLocationId.stringValue;
    		    		orderObj.PrepaymentAmountPaid 						= params.PrepaymentAmountPaid.stringValue;
    		    		orderObj.TotalAmount 								= params.TotalAmount.stringValue;
    		    		orderObj.TotalDiscount 								= params.TotalDiscount.stringValue;
    		    		//payment history from online portal.
    		    		orderObj.PaymentHisotry = new Object();
    		    		orderObj.PaymentHisotry.Amount 						= params.PaymentHisotryAmount.stringValue;
    		    		orderObj.PaymentHisotry.AmountInTenderedCurrency 	= params.PaymentHisotryAmountInTenderedCurrency.stringValue;
    		    		orderObj.PaymentHisotry.AmountInCompanyCurrency 	= params.PaymentHisotryAmountInCompanyCurrency.stringValue;
    		    		orderObj.PaymentHisotry.Currency 					= params.PaymentHisotryCurrency.stringValue;
    		    		//payment details // available payment method is CC.
    		    		orderObj.paymentInstrument = new Object();
    		    		orderObj.paymentInstrument.creditCardType			= app.getForm('billingform.paymentMethods.creditCard.type').value();
    		    		orderObj.paymentInstrument.creditCardHolder			= app.getForm('billingform.fullname').value();
						var card_number										= app.getForm('billingform.paymentMethods.creditCard.number').value().replace(/\s+/g, '');//(params.dwfrm_billingform_paymentMethods_creditCard_number.stringValue).replace(/\s+/g, '');																									   	
						
						var card_last4gidit = 0;
						orderObj.paymentInstrument.creditCardNumber4Digits = "";
						if(card_number != null){
							orderObj.paymentInstrument.creditCardNumber			= card_number;							
							card_last4gidit								        = card_number.substr(-4);
							orderObj.paymentInstrument.creditCardNumber4Digits  = card_last4gidit;
						}						
    		    		
    		    		var expiry_date = request.httpParameterMap.dwfrm_billingform_paymentMethods_creditCard_expirydate.stringValue;    		    		
    		    		orderObj.paymentInstrument.creditCardExpirationMonth = "";
    		    		orderObj.paymentInstrument.creditCardExpirationYear  = "";
    		    		if(expiry_date != null){
    		    			var temp_expiry = expiry_date.split('/');
        		    		orderObj.paymentInstrument.creditCardExpirationMonth= temp_expiry[0];
        		    		orderObj.paymentInstrument.creditCardExpirationYear = temp_expiry[1];
    		    		}    		    			
    		    		orderObj.paymentInstrument.cvn						= app.getForm('billingform.paymentMethods.creditCard.cvn').value();		
    		    		orderObj.paymentInstrument.creditCardHolder 		= orderObj.Customer.name;
    		    			
    		    		//customer address details
    		    		orderObj.shippingAddress = new Object();    		    		
    		    		orderObj.shippingAddress.address1					= params.shippingaddress1.stringValue;
    		    		orderObj.shippingAddress.address2					= params.shippingaddress2.stringValue;
    		    		orderObj.shippingAddress.city   					= params.shippingcity.stringValue;
    		    		orderObj.shippingAddress.stateCode					= params.shippingstate.stringValue;
    		    		orderObj.shippingAddress.countryCode				= params.shippingcountry.stringValue;
    		    		orderObj.shippingAddress.zipCode    				= params.shippingpostal.stringValue;
    		    		orderObj.shippingAddress.phone    				    = params.shippingphone.stringValue;
    		    		
    		    		//customer billing address details
    		    		orderObj.billingAddress = new Object();
    		    		orderObj.billingAddress.fullName					= app.getForm('billingform.fullname').value();
    		    		orderObj.billingAddress.address1					= app.getForm('billingform.address1').value();
    		    		orderObj.billingAddress.address2					= app.getForm('billingform.address2').value();
    		    		orderObj.billingAddress.city   						= app.getForm('billingform.city').value();
    		    		orderObj.billingAddress.stateCode					= app.getForm('billingform.states.state').value();
    		    		orderObj.billingAddress.countryCode					= "US";
    		    		orderObj.billingAddress.zipCode    					= app.getForm('billingform.postal').value();
    		    		orderObj.billingAddress.phone    				    = app.getForm('billingform.phone').value();
    		    		orderObj.salesOrderBalanceObject                    = session.custom.orderObject;
    		    		/*
    		    		* Calling to USAePay Service
    		    		* Request custom order details with orderObj
    		    		* Response Transaction Detail in case of success / Error in case of any issue.
    		    		*/
    		    		orderObj.paymentResponse = new Object();
    		    		var processPaymentResponse = processPayment(orderObj);
    		    
    		    		if (processPaymentResponse.action) {
    		    			/*
    		    			 * Payment successfully made
    		    			 * Send Email to customer for order confirmation
    		    			 * Keep storeid@mfrm.com in Bcc
    		    			*/
    		    			var Email = app.getModel('Email');
    						
    						/*Email.get('mail/confirmation', orderObj.Customer.Email).setFrom(dw.system.Site.getCurrent().getCustomPreferenceValue('customerServiceEmail')).setSubject("Mattress Firm - Payment Confirmation - " + orderObj.OrderNo).send({
    							orderObj: orderObj
    						}); */
    						       					
    						var emailTemplate: Template = new dw.util.Template("mail/confirmation");
    						var pdictParams : Map = new dw.util.HashMap();
    						pdictParams.put("orderObj", orderObj);
    						pdictParams.put("isPartiallyPaid", isPartiallyPaid);
    						pdictParams.put("partialPayAmount", partialPayAmount);
    						
    						var emailContent : MimeEncodedText = emailTemplate.render(pdictParams);
    						var mail : Mail = new dw.net.Mail();
    						mail.addTo(orderObj.Customer.Email); // Address the email to Customer
    						mail.setFrom(dw.system.Site.getCurrent().getCustomPreferenceValue('customerServiceEmail'));
    						//MAT-1852 - Send email to store Associates if custom preference is enabled
    						if ('enableStoreAssociatesEmail' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.enableStoreAssociatesEmail == true) {
    							if (!empty(orderObj.InventoryLocationId)) {
        							mail.addBcc(orderObj.InventoryLocationId + '@mfrm.com'); // Keep storeid@mfrm.com in Bcc
    							}
    						}
    						// Add Email IDs in Bcc from the Custom Preference
    						if ('mapPaymentConfirmationEmailIds' in dw.system.Site.current.preferences.custom && !empty(dw.system.Site.current.preferences.custom.mapPaymentConfirmationEmailIds)) {
    					    	var emailsToAddInBcc = dw.system.Site.current.preferences.custom.mapPaymentConfirmationEmailIds;
    					    	if (!empty(emailsToAddInBcc) && emailsToAddInBcc.length > 0) {
    					    		for (var i=0; i<emailsToAddInBcc.length; i++) {
    					    			mail.addBcc(emailsToAddInBcc[i]);
    					        	}
    							}
    					    }
    						mail.setSubject("Mattress Firm - Payment Confirmation - " + orderObj.OrderNo);
    						mail.setContent(emailContent);
    						mail.send();

    						/*
    			        	 * HTTP[Post]-Service: SIT - FulfillOrderPayment
    			        	 * Request Param: JSON object with order detail + Payment transaction details
    			        	 * Response: 'same order id' [in case all good]  / 'error' [error message]
    			        	*/
    		    			var paymentResponse= processPaymentResponse.authResponse;
    		    			var fraudResponse= processPaymentResponse.fraudResponse;
    		    			
    		    			//var requestPacket = '{"SalesId": "'+orderObj.SalesId+'","CustomerAttributes":{},"Payment": {"CreditCard": {"cardType": "'+orderObj.paymentInstrument.creditCardType+'","cardNumber": "************'+card_last4gidit+'","cardHolder": "'+orderObj.paymentInstrument.creditCardHolder+'","expirationMonth": '+temp_expiry[0]+',"expirationYear": '+temp_expiry[0]+'},"amount": 22.11,"processorId": "BASIC_CREDIT","transactionId": "'+paymentResponse.refnum+'","CustomAttributes":{}}}';//"'+orderObj.AmountDue+'"
    		    			var requestPacket = new Object();
    		    			requestPacket.SalesId = orderObj.SalesId;
    		    			requestPacket.CustomerAttributes = new Object();
    		    			
    		    			//Setting Payment Data
    		    			requestPacket.Payment = new Object();
    		    			
    		    			//Setting Up Credit Card Masked Data
    		    			requestPacket.Payment.CreditCard 					= new Object();
    		    			requestPacket.Payment.CreditCard.cardType 			= orderObj.paymentInstrument.creditCardType;
    		    			requestPacket.Payment.CreditCard.cardNumber 		= "************"+orderObj.paymentInstrument.creditCardNumber4Digits
    		    			requestPacket.Payment.CreditCard.cardHolder 		= orderObj.paymentInstrument.creditCardHolder;
    		    			requestPacket.Payment.CreditCard.expirationMonth 	= orderObj.paymentInstrument.creditCardExpirationMonth;
    		    			requestPacket.Payment.CreditCard.expirationYear	 	= orderObj.paymentInstrument.creditCardExpirationYear;
    		    			
    		    			if(isPartialPaymentsEnabled){
    		    				requestPacket.Payment.amount 			= new Number(partialPayAmount);
    		    			}
    		    			else{
    		    				requestPacket.Payment.amount 			= new Number(orderObj.AmountDue);
    		    			}
    		    			requestPacket.Payment.processorId 		= "USAEPAY_CREDIT";
    		    			requestPacket.Payment.transactionId 	= paymentResponse.refnum;
    		    			requestPacket.Payment.CustomAttributes 	= new Object();
    		    			
    		    			//Set Payment Response in Custom Attributes for Record
    		    			requestPacket.Payment.CustomAttributes.epay_type = paymentResponse.type;
    		    			requestPacket.Payment.CustomAttributes.epay_key = paymentResponse.key;
    		    			requestPacket.Payment.CustomAttributes.epay_refnum = paymentResponse.refnum;
    		    			requestPacket.Payment.CustomAttributes.epay_is_duplicate = paymentResponse.is_duplicate;
    		    			requestPacket.Payment.CustomAttributes.epay_result_code = paymentResponse.result_code;
    		    			requestPacket.Payment.CustomAttributes.epay_result = paymentResponse.result;
    		    			requestPacket.Payment.CustomAttributes.epay_authcode = paymentResponse.authcode;
    		    			requestPacket.Payment.CustomAttributes.epay_auth_amount = paymentResponse.auth_amount;
    		    			requestPacket.Payment.CustomAttributes.epay_trantype = paymentResponse.trantype;
    		    			requestPacket.Payment.CustomAttributes.epay_batch_key = paymentResponse.batch.key;
    		    			requestPacket.Payment.CustomAttributes.epay_batch_sequence = paymentResponse.batch.sequence;
    		    			requestPacket.Payment.CustomAttributes.epay_creditcard_category_code = paymentResponse.creditcard.category_code;
    		    			requestPacket.Payment.CustomAttributes.epay_creditcard_entry_mode = paymentResponse.creditcard.entry_mode;
    		    			/*
    		    			//Set Fraud Response in Custom Attributes for Record
    		    			requestPacket.Payment.CustomAttributes.fraud_status = fraudResponse.status;
    		    			requestPacket.Payment.CustomAttributes.fraud_transaction = fraudResponse.transaction;
    		    			requestPacket.Payment.CustomAttributes.fraud_action = fraudResponse.action;
    		    			requestPacket.Payment.CustomAttributes.fraud_message = fraudResponse.message;
    		    			requestPacket.Payment.CustomAttributes.fraud_reasonCode = fraudResponse.reasonCode;
    		    			requestPacket.Payment.CustomAttributes.fraud_actionEnum = fraudResponse.actionEnum;
    		    			requestPacket.Payment.CustomAttributes.fraud_orderLink = fraudResponse.orderLink;
    		    			requestPacket.Payment.CustomAttributes.fraud_processorAction = fraudResponse.processorAction;
    		    			*/
    		    			//Adding Request ID for edgeX Purpose
    		    			requestPacket.Payment.CustomAttributes.RequestId = orderObj.SalesId;
    		    			
    		    			
    		    			var service = MakeAPayemntServices.OrderPaymentMFRM;
    		    			
    		    			//var result = new Object();
    		    			var result = service.call(JSON.stringify(requestPacket)); // Update payment in AX   		    			    		    			
        					
    		    			if (result.error != 0 || result.errorMessage != null || result.mockResult) {
	    		    			/*
	    			        	 * Save Order object along with payment response in Custom object called MakeaPaymentOrders.
	    			        	*/
	    		    			//orderObj.paymentResponse.usaepayResponse =   response.authResponse;
    		    				LogUtils.logAndEmailError(orderObj.SalesId, CREATE_ORDER_PAYMENT, result.errorMessage);
	    		    			ResponseObj = 	orderObj;
	    		        		/*
	    		        		 * Saving order details which is required for Daily Batch Failure email to Field Support & Banking ASST-226
	    		        		 * */
	    		    			var emailObject  = new Object();
	    		    			emailObject.customerName = !empty(orderObj.billingAddress) ? orderObj.billingAddress.fullName:'';
	    		    			emailObject.customerPhone = !empty(orderObj.billingAddress) ? orderObj.billingAddress.phone:'';
	    		    			emailObject.customerEmail = !empty(orderObj.Customer) ? orderObj.Customer.Email:'';
	    		    			emailObject.cardHolderName = !empty(orderObj.paymentInstrument) ? orderObj.paymentInstrument.creditCardHolder:'';
	    		    			emailObject.orderNumber = !empty(orderObj.OrderNo)? orderObj.OrderNo:'';
	    		    			emailObject.paymentAmount = !empty(requestPacket.Payment) ? requestPacket.Payment.amount:'';
	    		    			emailObject.maxStoreNumber = !empty(orderObj.InventoryLocationId) ? orderObj.InventoryLocationId:'';
	    		    			
	    		    			Transaction.begin();
	    		    				var coResponse = CustomObjectMgr.getCustomObject('MakeaPaymentOrders', orderObj.SalesId);
	    		    				if(!empty(coResponse)) {
	    		    					coResponse.custom.data = JSON.stringify(requestPacket);
	    		    					coResponse.custom.orderObject = JSON.stringify(emailObject);
	    		    				}
	    		    				else {
	    		    					coResponse = CustomObjectMgr.createCustomObject('MakeaPaymentOrders', orderObj.SalesId);
		    		        			coResponse.custom.data = JSON.stringify(requestPacket);
		    		        			coResponse.custom.orderObject = JSON.stringify(emailObject);
	    		    				}
	    		    				coResponse.custom.retrycount = 0;
	    		    				coResponse.custom.status = true;
	    		        		Transaction.commit();																
	    		        		
	    		        		// Sending email on initial failure
	    		        		if (!empty(orderObj)){
	    		        			var emailTemplate: Template = new dw.util.Template("mail/wmap_failure");
	        						var pdictParams : Map = new dw.util.HashMap();
	        						pdictParams.put("customerName", (!empty(orderObj.billingAddress)) ? orderObj.billingAddress.fullName:'');
	        						pdictParams.put("customerPhone", (!empty(orderObj.billingAddress)) ? orderObj.billingAddress.phone:'');
	        						pdictParams.put("customerEmail", (!empty(orderObj.Customer)) ? orderObj.Customer.Email:'');
	        						pdictParams.put("cardHolderName", (!empty(orderObj.paymentInstrument)) ? orderObj.paymentInstrument.creditCardHolder:'');
	        						pdictParams.put("orderNumber", (!empty(orderObj.OrderNo))? orderObj.OrderNo:'');
	        						pdictParams.put("paymentAmount", (!empty(requestPacket.Payment)) ? requestPacket.Payment.amount:'');
	        						pdictParams.put("currencySymbol", (!empty(session.getCurrency())) ? session.getCurrency().getSymbol():'');
	        						pdictParams.put("maxStoreNumber", (!empty(orderObj.InventoryLocationId)) ? orderObj.InventoryLocationId:'');
	        						
	        						var content: MimeEncodedText = emailTemplate.render(pdictParams);
	        						var mail: Mail = new dw.net.Mail();
	        						
	        						if ('wmapFailureStoreEmailRecipients' in dw.system.Site.current.preferences.custom && 'customerServiceEmail' in dw.system.Site.current.preferences.custom){
	        							mail.addTo(dw.system.Site.getCurrent().getCustomPreferenceValue('wmapFailureStoreEmailRecipients'));
		        						mail.setFrom(dw.system.Site.getCurrent().getCustomPreferenceValue('customerServiceEmail'));
		        						if ('enableStoreAssociatesEmail' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.enableStoreAssociatesEmail == true) {
		        							if (!empty(orderObj.InventoryLocationId)) {
		            							mail.addBcc(orderObj.InventoryLocationId + '@mfrm.com'); // Keep storeid@mfrm.com in Bcc
		        							}
		        						}
		        						if ('wmapFailureStoreEmailSubject' in dw.system.Site.current.preferences.custom){
		        							var emaiSubject = StringUtils.format(dw.system.Site.getCurrent().getCustomPreferenceValue('wmapFailureStoreEmailSubject'),orderObj.OrderNo);
		        						} else {
		        							var emaiSubject = 'WMAP - Order Payment Failed - ' + orderObj.OrderNo;
		        						}
		        						mail.setSubject(emaiSubject);
		        						mail.setContent(content); 
		        						mail.send();
	        						}
	    		        		}	
	    		    		}
    		    			is_error = false;		    		    		        		    		
    		    		} else { //processPaymentResponse.action is FALSE
    		    			is_error = true;
        					if(!empty(processPaymentResponse.errorCode)){
        						displayBillingForm = true;
        						page_error = Resource.msgf('wmap.error.declined','makeapayment',null,processPaymentResponse.errorCode);
        					}else if(isTransactionDeclinedByForter == true){
        						displayBillingForm = true;
        						page_error = Resource.msgf('wmap.forterdeclined.resoncode','makeapayment', null, DeclinedReasonCode);
        					}else{
        						page_error = Resource.msg('wmap.error.frauderror','makeapayment',null);
        					}
    		    		}
    				} else { // gc_response.success is FALSE
    					is_error = true;
    					page_error = Resource.msg('wmap.recaptcha.failure','makeapayment',null);
    					LogUtils.logAndEmailError(params.SalesId.stringValue, RECAPTCHA, gc_result.object);
    				}
    			} // gc_result.msg == "OK" END
    			else {
    				LogUtils.logAndEmailError(params.SalesId.stringValue, RECAPTCHA, gc_result.errorMessage);
    			}
    		} else {
    			is_error = true;
    			page_error = Resource.msg('wmap.recaptcha.sessiontimeout','makeapayment',null);
    		}
    		
    		
    		if (is_error) {
    			app.getView({
    				is_error:is_error,
    				page_error:page_error,
    				FraudError: FraudError,
    				PaymentError: PaymentError,
    				orderObj: orderObj,
    				isPartialPaymentsEnabled: isPartialPaymentsEnabled,
        			isPartiallyPaid: isPartiallyPaid,
        			fraudErrorMessage: fraudErrorMessage,
        			displayBillingForm: displayBillingForm
    			}).render('billing');
    		} else {
    			updateMeadataMakePayment("makeapayment-confirmation");
    			app.getView({
    				is_error:is_error,
    				orderObj:orderObj,
    				partialPayAmount:partialPayAmount,
    				isPartialPaymentsEnabled:isPartialPaymentsEnabled,
    				isPartiallyPaid: isPartiallyPaid,
    				isConfimation: true,
    				FraudError: FraudError,
    				PaymentError: PaymentError
               }).render('Confirmation');
    		}
    		Logger.error('MapOrder OrderObj: '+JSON.stringify(orderObj));
        },
        error: function () {
        	is_error = true;
			gc_success = false;
			page_error = "Error, All fields are required, please fill all fields properly and try again.";
           app.getView({
        	   is_error:is_error,
        	   gc_success:gc_success,
        	   page_error:page_error,
        	   isPartialPaymentsEnabled:isPartialPaymentsEnabled,
			   isPartiallyPaid: isPartiallyPaid
           }).render('billing');
        }
    });	
}

function processPayment(order) {
    var paymentStatus = {
    		action: 				false,
    		authStatus: 			null,
    		authResponse: 			null,
    		fraudStatus: 			null,
    		fraudResponse: 			null,
    		captureStatus: 			null,
    		captureResponse: 		null,
    		voidStatus: 			null,
    		voidResponse: 			null,
    		exceptionStatus:		null,
    		exceptionResponse: 		null
    };
	try {
        var USAePayAuthtorize = usaEpayService.AuthorizeMakePayment(false,'Authtorize', {order:order});//dw.system.HookMgr.callHook("app.payment.processor.USAePay", 'AuthtorizePayment', order);
        try{
        	var resultAuth = JSON.parse(USAePayAuthtorize);
        }
        catch (e) {
        	//Set Auth Status in Return Response
        	paymentStatus.authStatus = "AUTH_DECLINED";
        	paymentStatus.authResponse = resultAuth;
        	//Add Decision in Response
    		paymentStatus.action = false;
    		PaymentError = true;
        }
        
        if (USAePayAuthtorize.status == "ERROR") {
    		LogUtils.logAndEmailError(order.OrderNo, USAEPAY_AUTHORIZE, USAePayAuthtorize.errorMessage);
    		paymentStatus.errorCode = USAePayAuthtorize.error;
    	} else if (USAePayAuthtorize.status == "OK" && JSON.parse(USAePayAuthtorize.object).error_code != null) {
    		LogUtils.logAndEmailError(order.OrderNo, USAEPAY_AUTHORIZE, USAePayAuthtorize.object);
    		paymentStatus.errorCode = JSON.parse(USAePayAuthtorize.object).error_code;
    	}
        
        var captureResult, voidResult;
        if (PaymentError != true && resultAuth.result == "Approved") {
        	//Set Auth Status in Return Response
        	paymentStatus.authStatus = "AUTH_APPROVED";
        	paymentStatus.authResponse = resultAuth;
        	
        	var refnum = resultAuth.refnum;
        	var isForterEnabledForMakePayment = dw.system.Site.getCurrent().getCustomPreferenceValue("enableForterForMakePayment"); 
        	if(isForterEnabledForMakePayment) {
        		var forterCall = require('int_forter/cartridge/controllers/ForterValidate.js');
                var forterDecision = forterCall.ValidateMakePaymentOrder({
                	Order: session.custom.orderObject,
                	orderObj: order,
                	CCAuthResponse: resultAuth,
                    orderValidateAttemptInput: 1
                });
                
                if(!empty(forterDecision.JsonResponseOutput)) {
                	//Set Fraud Response in Return Response
                	paymentStatus.fraudResponse = forterDecision.JsonResponseOutput;
                	
                	if (forterDecision.JsonResponseOutput.processorAction === 'skipCapture' || forterDecision.JsonResponseOutput.processorAction === 'notReviewed' || forterDecision.JsonResponseOutput.processorAction === 'void') {
                		//Set Fraud Status in Return Response
                    	if(forterDecision.JsonResponseOutput.processorAction === 'skipCapture') {
                    		paymentStatus.fraudStatus = "FRAUD_SKIP_CAPTURE";
                    	}
                    	else if(forterDecision.JsonResponseOutput.processorAction === 'notReviewed') {
                    		paymentStatus.fraudStatus = "FRAUD_NOT_REVIEWED";
                    	}
                    	else {
                    		paymentStatus.fraudStatus = "FRAUD_VOID";
                    	}
                    	//Void Call
                    	var amountToVoid=GetAmountToVoid(order);
                		var USAePayVoid = usaEpayService.AuthorizeMakePayment(false,'Void', {order:order, refnum: refnum, amount: amountToVoid});
                		if (USAePayVoid.status == "ERROR") {
                    		LogUtils.logAndEmailError(order.OrderNo, USAEPAY_VOID, USAePayVoid.errorMessage);
                    		paymentStatus.errorCode = USAePayVoid.error;
                		} else if (USAePayVoid.status == "OK" && JSON.parse(USAePayVoid.object).error_code != null  ) {
                    		LogUtils.logAndEmailError(order.OrderNo, USAEPAY_VOID, USAePayVoid.object);
                    		paymentStatus.errorCode = JSON.parse(USAePayVoid.object).error_code;
                    	}
                		captureResult = USAePayVoid;//JSON.parse(USAePayVoid);
                		//Set Capture Response in Return
                		paymentStatus.voidResponse = captureResult;
                		
                		if ("result" in captureResult && captureResult.result == "Approved") {
                			//set capture status in response
                			paymentStatus.voidStatus = "VOID_APPROVED";
                        } else {
                        	//set capture status in response
                			paymentStatus.voidStatus = "VOID_ERROR";
                        }
                		//Add Decision in Response
                		paymentStatus.action = false;
                		//paymentStatus.action = true;
                		FraudError = true;
                		DeclinedReasonCode = forterDecision.JsonResponseOutput.declinedReasonCode;
                		isTransactionDeclinedByForter = forterDecision.JsonResponseOutput.transactionDeclined;
                	}
                	else if (forterDecision.JsonResponseOutput.processorAction === 'disabled' || forterDecision.JsonResponseOutput.processorAction === 'internalError' || forterDecision.JsonResponseOutput.processorAction === 'capture') {
                		//Set Fraud Status in Return Response
                		if(forterDecision.JsonResponseOutput.processorAction === 'disabled') {
                    		paymentStatus.fraudStatus = "FRAUD_DISABLED";
                    	}
                    	else if(forterDecision.JsonResponseOutput.processorAction === 'internalError') {
                    		paymentStatus.fraudStatus = "FRAUD_NOT_INTERNAL_ERROR";
                    	}
                    	else {
                    		paymentStatus.fraudStatus = "FRAUD_CAPTURE";
                    	}
                		
                		//Capture call
                		var USAePayCapture = usaEpayService.AuthorizeMakePayment(false,'Capture', {order:order, refnum: refnum });
                		if (USAePayCapture.status == "ERROR") {
                    		LogUtils.logAndEmailError(order.OrderNo, USAEPAY_CAPTURE, USAePayCapture.errorMessage);
                		} else if (USAePayCapture.status == "OK" && JSON.parse(USAePayCapture.object).error_code != null  ) {
                    		LogUtils.logAndEmailError(order.OrderNo, USAEPAY_CAPTURE, USAePayCapture.object);
                    	}
                		captureResult = JSON.parse(USAePayCapture);
                		//Set Capture Response in Return
                		paymentStatus.captureResponse = captureResult;
                		
                		if (captureResult.result == "Approved") {
                			//set capture status in response
                			paymentStatus.captureStatus = "CAPTURE_APPROVED";
                        } else {
                        	//set capture status in response
                			paymentStatus.captureStatus = "CAPTURE_ERROR";
                        }
                		
                		//Add Decision in Response
                		paymentStatus.action = true;
                	}
                }
                else {
                	//Set Fraud Response in Return Response
                	paymentStatus.fraudResponse = forterDecision;
                	//Set Fraud Status
                	paymentStatus.fraudStatus = "FRAUD_ERROR";
                	
                	//Void Call
                	var amountToVoid=GetAmountToVoid(order);
            		var USAePayVoid = usaEpayService.AuthorizeMakePayment(false,'Void', {order:order, refnum: refnum, amount: amountToVoid});
            		
            		if (USAePayVoid.status == "ERROR") {
                		LogUtils.logAndEmailError(order.OrderNo, USAEPAY_VOID, USAePayVoid.errorMessage);
                		paymentStatus.errorCode = USAePayVoid.error;
            		} else if (USAePayVoid.status == "OK" && JSON.parse(USAePayVoid.object).error_code != null ) {
                		LogUtils.logAndEmailError(order.OrderNo, USAEPAY_VOID, USAePayVoid.object);
                		paymentStatus.errorCode = JSON.parse(USAePayVoid.object).error_code;
                	}
            		
            		voidResult = USAePayVoid;//JSON.parse(USAePayVoid);
            		//Set Capture Response in Return
            		paymentStatus.voidResponse = voidResult;
            		
            		if ("result" in voidResult && voidResult.result == "Approved") {
            			//set capture status in response
            			paymentStatus.voidStatus = "VOID_APPROVED";
                    } else {
                    	//set capture status in response
            			paymentStatus.voidStatus = "VOID_ERROR";
                    }
            		
            		//Add Decision in Response
            		paymentStatus.action = false;
            		FraudError = true;
                }
        	}
        	else {
        		//Capture call
        		var USAePayCapture = usaEpayService.AuthorizeMakePayment(false,'Capture', {order:order, refnum: refnum });
        		if (USAePayCapture.status == "ERROR") {
            		LogUtils.logAndEmailError(order.OrderNo, USAEPAY_CAPTURE, USAePayCapture.errorMessage);
        		} else if (USAePayCapture.status == "OK" && JSON.parse(USAePayCapture.object).error_code != null  ) {
            		LogUtils.logAndEmailError(order.OrderNo, USAEPAY_CAPTURE, USAePayCapture.object);
            	}
        		captureResult = JSON.parse(USAePayCapture);
        		//Set Capture Response in Return
        		paymentStatus.captureResponse = captureResult;
        		
        		if (captureResult.result == "Approved") {
        			//set capture status in response
        			paymentStatus.captureStatus = "CAPTURE_APPROVED";
                } else {
                	//set capture status in response
        			paymentStatus.captureStatus = "CAPTURE_ERROR";
                }
        		
        		//Add Decision in Response
        		paymentStatus.action = true;
        	}
        	
        }
        else {
        	//Set Auth Status in Return Response
        	paymentStatus.authStatus = "AUTH_DECLINED";
        	paymentStatus.authResponse = resultAuth;
        	//Add Decision in Response
    		paymentStatus.action = false;
    		PaymentError = true;
        }
        
        //Return Payment Status
        return paymentStatus;
    } catch(e) {
    	LogUtils.logAndEmailError(order.OrderNo, OTHER, e.message);
    	//Add Exception Detail & Response in Return
    	paymentStatus.exceptionStatus = "EXCEPTION";
    	paymentStatus.exceptionResponse = e;
    	//Add Decision in Response
		paymentStatus.action = false;
		
        return paymentStatus;
    }
}

function GetAmountToVoid(order) {
	  //MAT-1541 - Return the Amount to void 
	var isPartialPaymentsEnabled = ('enablePartialPayments' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.enablePartialPayments) ? true : false;
	var params = request.httpParameterMap;
	var amountToPay;
	if(isPartialPaymentsEnabled){
		if(params.PaymentOptions.submitted && params.PaymentOptions=='OtherAmountOption' && params.dwfrm_billingform_PartialPayment_otherAmount.submitted){
			amountToPay= params.dwfrm_billingform_PartialPayment_otherAmount.stringValue;
		}
		else {
		    amountToPay = order.AmountDue;		    			
		}
	}
	else {
			amountToPay = order.AmountDue;	
	}
	
	return amountToPay;
}

function extractCaseSensitiveParam(params) {
	//var params = request.httpParameterMap;
	var hash = "orderId";
	for(var param in params) {
		var key = param;
		var value = params[param];
		if(hash.toUpperCase() === key.toUpperCase()) {
			return value;
		}
	}
	return;
}

function searchOrders() {
	var orderId = extractCaseSensitiveParam(request.httpParameterMap);//request.httpParameterMap.orderId.stringValue;
	updateMeadataMakePayment("makeapayment-search");
	try {
		if(orderId) {
			
			var svc = MakeAPayemntServices.SalesOrderBalanceMFRM;		
			svc.addParam("orderId", orderId);
			svc.setRequestMethod('GET');
			var result = svc.call();
			
			if (result.error != 0 || result.errorMessage != null || result.mockResult) {   
				var clErrorCode, errorMessageToUser, isError = false;
				if(!empty(result.error) && (result.error == '400') && !empty(result.errorMessage)){
					clErrorCode = JSON.parse(result.errorMessage).RefCode;
				} else {
					clErrorCode = !empty(result.error) ? result.error.toString() : '';
				}
				if(clErrorCode != undefined && clErrorCode == '4002'){
					isError = true;
					var contactSupportURL = URLUtils.url('Page-Show', 'cid', 'faq-customer-service');
					errorMessageToUser = Resource.msgf('wmap.error.ordercancelled', 'makeapayment', null, orderId, clErrorCode, contactSupportURL);
				} else if (clErrorCode != undefined && (clErrorCode == '5001' || clErrorCode == '0' || clErrorCode == '500')) {
					isError = true;
					var contactSupportURL = URLUtils.url('Page-Show', 'cid', 'faq-customer-service');
					errorMessageToUser = Resource.msgf('wmap.error.technicalerror', 'makeapayment', null, contactSupportURL, clErrorCode);
				} else if (clErrorCode != undefined && clErrorCode == '4001') {
					isError = true;
					errorMessageToUser = Resource.msgf('wmap.error.ordernotexist', 'makeapayment', null, orderId, clErrorCode);
				} else {
					isError = true;
					errorMessageToUser = Resource.msgf('wmap.error.issearchresultfound', 'makeapayment', null);
				}
				app.getView({
					errorMessageToUser: errorMessageToUser,
					isError: isError
				}).render('SearchOrders');
			}
			else {
				var jsonOrderObj = JSON.parse(result.object.text); 
				var orderObject = jsonOrderObj.Data;       	
	            var isAmountDue = false;
	        	if(orderObject.AmountDue > 0){
	        		isAmountDue = true;
	            }
	        	var orderData = orderObject.OrderDate;
	        	var splitedOrderData = orderData.split('T');
	        	if(splitedOrderData.length > 0) {
	        		var orderObj = splitedOrderData[0].split('-');
	        		var year = orderObj[0];
	        		var month = orderObj[1];
	        		var day = orderObj[2];
	        	}
	        	app.getView({
	        		isAmountDue: isAmountDue,
	        		orderId: orderId,
	        		isSearchResultFound: true,
	        		orderDate: (month + "/" + day + "/" + year)
	        	}).render('SearchOrders'); 
			}
		}
		else {
			app.getView().render('SearchOrders');
		}
	} catch (e) {
		var msg = e.message;
		Logger.error('sitgetsalesorderbalance Error: '+msg);
		app.getView({isSearchResultFound: false}).render('SearchOrders');
	}
}

function updateMeadataMakePayment(assetID) {
	try {
		var pageAsset, pageMeta, Content;
	    Content = app.getModel('Content');
	    pageAsset = Content.get(assetID);

	    pageMeta = require('app_storefront_controllers/cartridge/scripts/meta');
	    pageMeta.update(pageAsset);
	}
	catch(e) {
		
	}
}


/*
* Web exposed methods
*/
exports.Billing = guard.ensure(['https'], billing);
exports.HandleBilling =  guard.ensure(['https'], handleBilling);

//exports.Confirmation = guard.ensure(['https', 'get'], Confirmation);
exports.SearchOrders = guard.ensure(['https', 'get'], searchOrders);
//exports.HandleSearchOrders = guard.ensure(['https', 'post'], handleSearchOrders);

