<iscontent type="text/html" charset="UTF-8" compact="true"/>
	<isset name="current_page" value="map-confirmation" scope="pdict" />
	<isscript>
		var globalHelper = require("GlobalUtilities");
    	var sfCustomerPortalLandingURL = globalHelper.getSfCustomerPortalLandingURL();
	</isscript>
<isdecorate template="pt_order">
	<isinclude template="util/modules"/>	

	<div id="primary" class="primary-content">		
   		<isset name="is_error" value="${pdict.is_error}" scope="page" />    			
   		<isif condition="${(is_error == true)}">
   			<isset name="page_error" value="${pdict.page_error}" scope="page" />
			<div class="error_message">
				<isprint value="${page_error}" />
			</div>    			    			
   		<iselse/>
   			<isset name="orderObj" value="${pdict.orderObj}" scope="page" />   			
   			<pre><isprint value="${orderObj}" /></pre>
   		   <div class="mapo-pg-header">
				<h1 class="primary-heading">
					${Resource.msg('makepayment.orderconfirmation','locale',null)}
					<a href="#" onclick="PrintPage()" class="print-order"><img class="print-icon" src="${URLUtils.staticURL('images/print-icon.png')}" alt="" title="" /><span class="print-label">PRINT</span></a>
				</h1>
				<p>Thank you for your payment.</p>
				<p>${Resource.msg('makepayment.confirmation.paymenthoursalert','locale',null)}</p>
			</div>
			<div class="mapo-pg-body confirmation-page">
				<div class="notification-cntr payment-info-section">
					<p class="notification-text">
						<img src="${URLUtils.staticURL('images/truck@2x.png')}" alt="delivery icon" title="delivery icon" />
						<isif condition="${(pdict.isPartiallyPaid == true)}">
							<span class="notification-link">
								Your outstanding balance is: <span class="bold"> $<isprint value="${new Number(orderObj.AmountDue - pdict.partialPayAmount)}" /></span><br> 
								Order must be paid in full to schedule delivery. <a href="https://www.mattressfirm.com/stores/">Contact your store</a> for more details.
							</span>
						<iselse>
							<span class="notification-link full">You can now schedule your delivery by <a href="https://www.mattressfirm.com/stores/">contacting the store</a> where you made your purchase.</span>
						</isif>
					</p>			
				</div>
				<div class="section-to-print">
					<div class="print-only">
						<div class="logo">
							<img src="${URLUtils.staticURL('images/header/mattressfirm-logo.svg')}" alt="${Resource.msg('global.storelogotext', 'locale', null)}" title="${Resource.msg('global.storelogotext', 'locale', null)}" />
						</div>
					</div>
					<div class="payment-details">
						<h2 class="secondary-heading bb-heading hide-print">Payment Details</h2>
						<div class="print-only">
							<h2 class="secondary-heading bb-heading">Payment Confirmation</h2>
							<p>Thank you for your Payment</p>
						</div>
						<div class="section-container">
							<h2 class="small-heading">Payment Information</h2>						
							<p class="payment-info card-number"><span class="label"><isprint value="${orderObj.paymentInstrument.creditCardType}" /> ending in</span> <span class="value"><isprint value="${orderObj.paymentInstrument.creditCardNumber4Digits}" /> </p>					
							<p class="payment-info card-expiry"><span class="label">Expiration:</span> <span class="value"><isprint value="${orderObj.paymentInstrument.creditCardExpirationMonth}" />/<isprint value="${orderObj.paymentInstrument.creditCardExpirationYear}" /></p>
							<p class="payment-info order-amount"><span class="label">Amount:</span> <span class="value">$<isprint value="${pdict.isPartiallyPaid ? pdict.partialPayAmount : orderObj.AmountDue}"/></p>					 					
						</div>
						<div class="section-container">
							<h2 class="small-heading">Billing Address</h2>
							<p class="billingaddress-info name"><isprint value="${session.forms.billingform.fullname.value}" /> </p>					
							<p class="billingaddress-info address"><isprint value="${session.forms.billingform.address1.value}" /> <isprint value="${session.forms.billingform.address2.value}" /></p>
							<p class="billingaddress-info"><span class="city"><isprint value="${session.forms.billingform.city.value}" /></span>, <span class="state-code"><isprint value="${session.forms.billingform.states.state.value}" /></span> <span class="zipcode"><isprint value="${session.forms.billingform.postal.value}" /></span></p>
							<p class="billingaddress-info country">United States</p>
							<p class="billingaddress-info phone"><isprint value="${session.forms.billingform.phone.value}" /></p>
							<p class="billingaddress-info phone"><isprint value="${orderObj.Customer.Email}" /></p>							 					
						</div>
					</div>
					<div class="summary-details mt-40 mb-30">
						<h2 class="secondary-heading bb-heading">Order Summary</h2>
						<div class="payment-billing-page summary-header row">
							<div class="order-summary-section-top">
								<div class="section">
									<span class="mf-col-8">Order Number:&nbsp;<isprint value="${orderObj.SalesId}" /></span>
								</div>
								<div class="section billing-order-total">
									<span class="mf-col-8 print-bold">ORDER TOTAL</span>
									<span class="mf-col-4 text-right">$<isprint value="${new Number(orderObj.TotalAmount)}" /></span>
								</div>
								<iscomment>MAT-1541-Render Partial payment section only if it is enabled in site preference </iscomment>
								<isif condition="${(pdict.isPartiallyPaid == true)}">
									<div class="section">
										<span class="mf-col-8 print-bold">${Resource.msg('orderConfirmation.previousBalance','checkout',null)}</span>
										<span class="mf-col-4 text-right">$<isprint value="${new Number(orderObj.AmountDue)}" /></span>
									</div>
									<div class="section">
										<span class="mf-col-8 print-bold">${Resource.msg('orderConfirmation.amountPaidToday','checkout',null)}</span>
										<span class="mf-col-4 text-right">-$<isprint value="${new Number(pdict.partialPayAmount)}" /></span>
									</div>
									<div class="section">
										<span class="mf-col-8 print-bold text-pale-red">${Resource.msg('orderConfirmation.outstandingBalance','checkout',null)}</span>
										<span class="mf-col-4 text-right text-pale-red">$<isprint value="${new Number(orderObj.AmountDue - pdict.partialPayAmount)}" /></span>
									</div>
								<iselse>
									<div class="section">
										<span class="mf-col-8 print-bold">Paid</span>
										<span class="mf-col-4 text-right">-$<isprint value="${new Number(orderObj.AmountDue)}" /></span>
									</div>
									<div class="section">
										<span class="mf-col-8 print-bold" >Amount Due</span>
										<span class="mf-col-4 text-right" >$0.00</span>
									</div>
								</isif>
							</div>
						</div>				
					</div>
				</div>
				<div id="action-container">
					<a href="${sfCustomerPortalLandingURL}" class="btn primary back-portal">Back to my Account</a>		
		     		<p class="disclaimer-text mb-50">Note: your final payment may not be reflected in the Customer Portal for up to four hours. </p>	        		        		        					        			       
		     	</div>
			</div>				
   		</isif>	
	</div>
	<script>
		function PrintPage(){
			window.print();
		}
		window.onload = function () {
            if(utag) {
                var orderTotalForUtag = new Number("${orderObj.GrossAmount}");
                if(orderTotalForUtag > 0) {
                    var roundedOffTotal = Math.round(orderTotalForUtag);
                    <isset name="store" value="${orderObj.InventoryLocationId}" scope="page" />
                   	<isif condition="${store === '095073' || store === '095074' || store === '095076' || store === '095099'}">
	                  	//Chat Purchase
	                    utag.link({
	                        "eventCategory" : ["Call & Chat"],
	                        "eventLabel" : ["Chat Purchase"],
	                        "eventAction" : ["Offline Purchase"],
	                        "eventValue" : ["" + roundedOffTotal]
	                    }); 
                   	<iselseif condition="${store === '095066' || store === '095068' || store === '095098'}" >
	                  //Call Purchase
	                    utag.link({
	                        "eventCategory" : ["Call & Chat"], 
	                        "eventLabel" : ["Call Purchase"], 
	                        "eventAction" : ["Offline Purchase"], 
	                        "eventValue" : ["" + roundedOffTotal] 
	                    });
                   	<iselse>
	                  //Store Purchase:
	                    utag.link({
	                        "eventCategory" : ["Store"], 
	                        "eventLabel"    : ["${'Store Purchase - ' + store}"],
	                        "eventAction"   : ["Offline Purchase"],
	                        "eventValue"    : [roundedOffTotal]
	                    }); 
                   	</isif> 
                }
            }
        };
	</script>
</isdecorate>
