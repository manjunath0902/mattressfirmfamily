'use strict';

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

/**
*
* General Service Object
*
*/
var serviceObject = {
    createRequest: function(service, args) {
        var apiKey = dw.system.Site.getCurrent().getCustomPreferenceValue("sit_x_api_key");
        service.addHeader("Content-Type", "application/json");
        service.addHeader("Accept", "application/json");
		service.addHeader("x-api-key", apiKey);
        return args;
    },
    parseResponse : function(svc, listOutput) {
        return listOutput;
    },
	filterLogMessage: function(msg : String) {
		if(msg==="Read timed out") {
			return "Oops Service Timed Out...";
		}
		else {
			return msg;
		}
	},
    getRequestLogMessage : function(requestObj : Object) : String {
       try {
           return requestObj;
       } catch(e) {}
       return requestObj;
   },
   getResponseLogMessage : function(responseObj : Object) : String {
	   try {
		    if (responseObj instanceof dw.net.HTTPClient) {
		    	if(responseObj.statusCode == 200 || responseObj.statusMessage === 'OK'){
		    		return responseObj.text;
		       	}
		        else {
		        	return responseObj.errorText
		        }
		    }
		} catch(e) {}
		return responseObj;
   }
};


/**
*
* Global Exports
*
*/

//MFRM Site Specific orderpayment
exports.OrderPaymentMFRM = LocalServiceRegistry.createService("commercelink.map.createOrderPayment", serviceObject);

//MFRM Site Specific SalesOrderBalance
exports.SalesOrderBalanceMFRM = LocalServiceRegistry.createService("commercelink.map.getSalesOrderBalance", serviceObject);
