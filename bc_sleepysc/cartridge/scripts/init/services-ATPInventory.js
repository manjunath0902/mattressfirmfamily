'use strict';

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

/**
*
* General Service Object
*
*/
var serviceObject = {
    createRequest: function(service, args) {
        var apiKey = dw.system.Site.getCurrent().getCustomPreferenceValue("x-api-key");
        service.addHeader("Content-Type", "application/json");
        service.addHeader("Accept", "application/json");
		service.addHeader("x-api-key", apiKey);
		service.setRequestMethod("POST");
        return args;
    },
    parseResponse : function(svc, listOutput) {
        return listOutput;
    },
	filterLogMessage: function(msg : String) {
		if(msg==="Read timed out") {
			return "Oops Service Timed Out...";
		}
		else {
			return msg;
		}
	},
    getRequestLogMessage : function(requestObj : Object) : String {
       try {
           return requestObj;
       } catch(e) {}
       return requestObj;
   },
   getResponseLogMessage : function(responseObj : Object) : String {
	   try {
		    if (responseObj instanceof dw.net.HTTPClient) {
		    	if(responseObj.statusCode == 200 || responseObj.statusMessage === 'OK'){
		    		return responseObj.text;
		       	}
		        else {
		        	return responseObj.errorText
		        }
		    }
		} catch(e) {}
		return responseObj;
   }
};


/**
*
* Global Exports
*
*/

//MSOS Site Specific Returns one month slots
exports.ATPInventoryMonthMSOS = LocalServiceRegistry.createService("MSOS.commercelink.atp.inventoryMonth", serviceObject);

//FP Site Specific Returns one month slots
exports.ATPInventoryMonthFP = LocalServiceRegistry.createService("FP.commercelink.atp.inventoryMonth", serviceObject);

//1800 Site Specific Inventory Returns One Week Data
exports.ATPInventory1800 = LocalServiceRegistry.createService("1800.commercelink.atp.inventory", serviceObject);

//TULO Site Specific Inventory Returns One Week Data
exports.ATPInventoryTULO = LocalServiceRegistry.createService("TULO.commercelink.atp.inventory", serviceObject);

//FP Site Specific Inventory Returns One Week Data
exports.ATPInventoryFP = LocalServiceRegistry.createService("FP.commercelink.atp.inventory", serviceObject);

//MSOS Site Specific Inventory Returns One Week Data
exports.ATPInventoryMSOS = LocalServiceRegistry.createService("MSOS.commercelink.atp.inventory", serviceObject);

//FP site specific service for UpdateSalesOrder
exports.UpdateSalesOrderFP = LocalServiceRegistry.createService("FP.commercelink.updateSalesOrder", serviceObject);