'use strict';

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

/**
*
* General Service Object
*
*/
var serviceObject = {
		createRequest: function(service, args) {
	        
	        var apiKey = dw.system.Site.getCurrent().getCustomPreferenceValue("x-api-key");
	        service.addHeader("Content-Type", "application/json");
	        service.addHeader("Accept", "application/json");
			service.addHeader("x-api-key", apiKey);
			service.setRequestMethod("POST");
	        	
	        return args;
	    },
	    parseResponse : function(svc, listOutput) {
	        return listOutput;
	    },
	    /**
		* filterLogMessage()
		*	This function filters logs messages if required.
		*/
		filterLogMessage: function(msg : String) {
			if(msg==="Read timed out") {
				return "Oops Service Timed Out...";
			}
			else {
				return msg;
			}
		},
	    getRequestLogMessage : function(requestObj : Object) : String {
	       try {
	           return requestObj;
	       } catch(e) {}
	   },
	   getResponseLogMessage : function(responseObj : Object) : String {
		   try {
			    if (responseObj instanceof dw.net.HTTPClient) {
			    	if(responseObj.statusCode == 200 || responseObj.statusMessage === 'OK'){
			    		return responseObj.text;
			       	}
			        else {   
			        	return responseObj.errorText
			        }       
			    }
			} catch(e) {}
			return responseObj;
	   }
};

/**
*
* Inventory Service with ATP Cache Implementation
*
*/

//Returns One Week Slots
exports.ATPInventoryMFRM = LocalServiceRegistry.createService("MFRM.commercelink.atp.inventory", serviceObject);

//Returns Slots for Chicago DC
exports.ATPInventoryStatus = LocalServiceRegistry.createService("commercelink.atp.inventoryStatus", serviceObject);

//Returns Slots for Delivery Info
exports.ATPInventoryDynamic = LocalServiceRegistry.createService("commercelink.atp.inventoryDynamic", serviceObject);
