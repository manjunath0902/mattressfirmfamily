/**
* 	Name: 			CreateAddressObject.ds
* 	Description: 	Takes address fields and converts it into a utility address object.
*
* 	@author Carl Napoli (Media Hive)
*
*	@input AddressLine1 : String Address Line 1
*	@input AddressLine2 : String Address Line 2
*	@input ApartmentNumber : String Apartment Number
*	@input City : String City
*	@input StateCode : String State Code
*	@input ZipCode : String Zip Code
*
*   @output Address : Object
*
*/
importPackage( dw.system );
importPackage( dw.util );

function execute( args : PipelineDictionary ) : Number
{
	var addressLine1 = (!empty(args.AddressLine1)) ? StringUtils.trim(args.AddressLine1) : "";
	var addressLine2 = (!empty(args.AddressLine2)) ? StringUtils.trim(args.AddressLine2) : "";
	var apartmentNumber = (!empty(args.ApartmentNumber)) ? StringUtils.trim(args.ApartmentNumber) : "";
	var city = (!empty(args.City)) ? StringUtils.trim(args.City) : "";
	var stateCode = (!empty(args.StateCode)) ? StringUtils.trim(args.StateCode) : "";
	var zipCode = (!empty(args.ZipCode)) ? StringUtils.trim(args.ZipCode) : "";

	var address = {
		addressLine1: addressLine1,
		addressLine2: addressLine2,
		apartmentNumber: apartmentNumber,
		city: city,
		stateCode: stateCode,
		zipCode: zipCode
	}

	args.Address = address;	

   return PIPELET_NEXT;
}
