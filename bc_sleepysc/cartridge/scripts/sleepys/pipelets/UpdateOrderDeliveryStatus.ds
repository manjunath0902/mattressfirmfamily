/**
* 	Name: 			UpdateOrderDeliveryStatus.ds
* 	Description: 	Updates the order delivery status from a web service call.	
*
* 	@author Michael Valeiko (Sleepys)
*
*	@input siestaOrders : Object Customer orders from Siesta
*	@input historyOrders : dw.util.Iterator Customer orders from history
*	
*
*/
importPackage( dw.system );

function execute( args : PipelineDictionary ) : Number
{
	var siestaOrders : Object = args.siestaOrders;
	var historyOrders : Object = args.historyOrders;
	var found : Boolean = false;
	var historyOrderList : List = historyOrders.asList();
	
	for(var orderIndex in siestaOrders){
		var orderStatus = siestaOrders[orderIndex].deliveryStatus;
		var webOrderId = siestaOrders[orderIndex].webOrderID;
		found = false;

		for (historyOrderIndex = 0; historyOrderIndex < historyOrderList.size(); historyOrderIndex++) {
			var tempHistoryOrder : Order = historyOrderList[historyOrderIndex];
			var historyOrderId = tempHistoryOrder.orderNo;
			if (webOrderId == historyOrderId) {
				//Set order status.
				switch(orderStatus) {
					case 'CANCELLED' : tempHistoryOrder.setStatus(dw.order.Order.ORDER_STATUS_CANCELLED);
						break;
					case 'CLOSED' : tempHistoryOrder.setStatus(dw.order.Order.ORDER_STATUS_COMPLETED);
						break;
					case 'BOOKED' : tempHistoryOrder.setStatus(dw.order.Order.ORDER_STATUS_OPEN);
						break;
					default :
						break;
				}
				
				var historyShipments = tempHistoryOrder.getShipments();
				var webShipments = siestaOrders[orderIndex].shipments;
				for (var historyShipmentIndex in historyShipments) {
					var tempShipment = historyShipments[historyShipmentIndex];
					for(var webShipmentIndex in webShipments) {
						var tempWebShipment = webShipments[historyShipmentIndex];
						if (tempShipment.shipmentNo == tempWebShipment.shipmentId) {
							//Set shipment status.
							var shipmentStatus = tempWebShipment.shipping_status;
							switch(shipmentStatus) {
							case 'SHIPPED' : tempShipment.setShippingStatus(dw.order.Order.SHIPPING_STATUS_SHIPPED);
								break;
							case 'NOT_SHIPPED' : tempShipment.setShippingStatus(dw.order.Order.SHIPPING_STATUS_NOTSHIPPED);
								break;
							default :
								break;
							}

						}
						//Set tracking number.
						if(!empty(tempShipment.trackingNumber)) {
							tempShipment.trackingNumber = tempWebShipment.trackingNo;
						}
						//Set actural deliery date.
						if (!empty(tempWebShipment.actualShipDate)) {
							var pli = tempShipment.getProductLineItems();
							for (var pliIndex in pli) {
								var tempPli = tempShipment.getProductLineItems()[pliIndex];
								tempPli.custom.deliveryDate = tempWebShipment.actualShipDate; 
							}
						}
					}
				}
				found = true;
			}
		}
	}
    

   return PIPELET_NEXT;
}
