'use strict';
var healthServiceObj = require("~/cartridge/scripts/init/service-HealthService.ds");
function healthCheckerMethod(args) {
	 if (!empty(dw.system.Site.getCurrent().getCustomPreferenceValue('healthUrl'))) {
		 	
		    var service = healthServiceObj.HealthStatus; 
			var sitePort= dw.system.Site.getCurrent().getCustomPreferenceValue('healthUrl');
			var url = service.URL+sitePort;
			service.setURL(url);
			var response = service.call();
		 	
			if (!empty(response) && empty(response.errorMessage))
			{	
			     subject = 'ATP works fine';
			    if(dw.system.Site.getCurrent().ID == 'FulfillmentPortal'){
			    	 message = JSON.stringify(response.object.text);   
			    }
			    else{
			    	var response = JSON.parse(response.object.text);
					message = JSON.stringify(response); 
			    }
			       
			}
			else
			{
			    // error handling
				subject = 'ATP service down';
				
				 var status = "An error occurred with status code " + response.error + ' for ' + dw.system.Site.getCurrent().ID + '\n'	;	
			     var response = JSON.parse(response.errorMessage);
			     message = status + '\n' + JSON.stringify(response);
			}
			sendMail(message,args,subject);
	 }
	
}
function sendMail(message,args,subject) {
    
    var mail: Mail = new dw.net.Mail();
    mail.addTo(args.healthEmailToList);
    mail.setFrom(args.healthEmailSenderId);
    mail.setSubject(dw.system.Site.getCurrent().ID + ' ' + subject);
    // sets the content of the mail as plain string
    mail.setContent(message);
    mail.send();
}
exports.HealthCheckerMethod = healthCheckerMethod;