importPackage( dw.svc );
importPackage( dw.system );
importPackage( dw.order );
importPackage( dw.catalog );
importPackage( dw.util );
importPackage( dw.crypto );
importPackage( dw.campaign );
importPackage( dw.customer );
importPackage( dw.object );
var Transaction = require('dw/system/Transaction');

importScript("app_storefront_core:checkout/Utils.ds");
var sleepysHelper = require("./SleepysUtils").SleepysHelper;
var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;

var SleepysPipeletHelper = {
	/***************************************************************************************
	 *	Name: 	verifyAddress()
	 * 	Description: Calls Sleepy's Address Verification Service to check if an address is valid.
	 ***************************************************************************************/
	/*verifyAddress: function ( address : Object ) : Object
	{
	    var service:Service;
	    var result:Result;
	    var params;
	    var returnObject : Object = {};

		var _verifyAddressJSONArr : Array = new Array();
		var _address = address;
		var _addressScore : Number = -1;
		var _i = 0, _j = 0;

		if (empty(_address)) {
			returnObject.Status = "ERROR";
			return returnObject;
		}

		service = ServiceRegistry.get("sleepys.verifyaddressservice.soap");
		params = {"addressLine1": _address.addressLine1, "addressLine2" : _address.addressLine2, "apartmentNumber" : _address.apartmentNumber, "city" : _address.city, "stateCode" : _address.stateCode, "zipCode" : _address.zipCode};
		result = service.call(params);

		if(result.status == "OK")
		{
			if(!empty(result) && !empty(service) && !empty(result.object.response) && !empty(result.object.response.suggestedAddress)) {
				var _suggestedAddress : Object = result.object.response.suggestedAddress;
				_addressScore = result.object.response.addressScore;

				// Check if address values are the same, if so, change address score to -1. We don't want to recommend an address that is exactly the same.
				if(_addressScore > -1 && !empty(_suggestedAddress))
					_addressScore = SleepysPipeletHelper.compareAddresses(_address, _suggestedAddress);

				_verifyAddressJSONArr[_j++] = "{";
				_verifyAddressJSONArr[_j++] = StringUtils.format("\"addressScore\" : {0}", _addressScore.toFixed());
				_verifyAddressJSONArr[_j++] = ", \"suggestedAddress\" : {";
				_verifyAddressJSONArr[_j++] = StringUtils.format("\"addressLine1\" : \"{0}\", \"addressLine2\" : \"{1}\", \"city\" : \"{2}\" ,\"stateCode\" : \"{3}\", \"zipCode\" : \"{4}\"", _suggestedAddress.addressLine1, (empty(_suggestedAddress.addressLine2) ? '' : _suggestedAddress.addressLine2), _suggestedAddress.city, _suggestedAddress.stateCode, _suggestedAddress.zipCode);
				_verifyAddressJSONArr[_j++] = "} }";
			}
		}

		returnObject.VerifyAddressJSON = _verifyAddressJSONArr.join("");
		returnObject.AddressScore = _addressScore;

	   	return returnObject;
	},*/
	/***************************************************************************************
	 *	Name: 	compareAddresses()
	 * 	Description:
	 ***************************************************************************************/
	compareAddresses : function (CurrentAddress : Object, SuggestedAddress : Object): Number
	{
		if(CurrentAddress.addressLine1.toLowerCase() != StringUtils.trim((empty(SuggestedAddress.addressLine1) ? '' : SuggestedAddress.addressLine1).toLowerCase()))
			return 1;

		if(CurrentAddress.city.toLowerCase() != StringUtils.trim((empty(SuggestedAddress.city) ? '' : SuggestedAddress.city).toLowerCase()))
			return 1;

		if(CurrentAddress.stateCode.toLowerCase() != StringUtils.trim((empty(SuggestedAddress.stateCode) ? '' : SuggestedAddress.stateCode).toLowerCase()))
			return 1;

		if(CurrentAddress.zipCode.toLowerCase() != StringUtils.trim((empty(SuggestedAddress.zipCode) ? '' : SuggestedAddress.zipCode).toLowerCase()))
			return 1;

		return -1; // These are identical addresses, do not suggest.
	},

	/*checkATP: function ( address : Object, basket ) : Object {

	    var service:Service;
	    var result:Result;
		var address = address;
		var returnObject : Object = {};

		var masterStoreCode = sleepysHelper.getMasterStoreCode();

		if (empty(address) || empty(basket) || empty(masterStoreCode)) {
			returnObject.Status = "ERROR";
			return returnObject;
		}

		service = ServiceRegistry.get("sleepys.checkatpservice.soap");
		params = {"address": address, "basket" : basket, "masterStoreCode" : masterStoreCode};
		result = service.call(params);


		if(result.status == "OK")
		{
			if(!empty(result.object) && !empty(result.object.response))
			{
				returnObject.MaxATP = result.object.response.maxAtp; // '2016-06-10';
				returnObject.ATPLineItems = result.object.response.lineItems;
			}
		}

	   	return returnObject;
	},*/

	/*getDeliveryStatus: function ( phonenumber : String ) : Object {

	    var service:Service;
	    var result:Result;
		var phoneNumber = phonenumber;
	    var params;
	    var returnObject : Object = {};

		var masterStoreCode = sleepysHelper.getMasterStoreCode();

	// 	To test use 5553214567 as the phone number and it will return test phone numbers to use."
	//	phoneNumber = "6465126388";

		if (empty(phoneNumber) || empty(masterStoreCode)) {
			returnObject.Status = "ERROR";
			return returnObject;
		}

		service = ServiceRegistry.get("sleepys.deliverystatus.soap");
		params = {"phoneNumber": phoneNumber, "masterStoreCode" : masterStoreCode};
		result = service.call(params);

		returnObject.Result = result.object;
		returnObject.Status = result.status; //result.object.statusCode;

		if ( result == null || service == null ){
			returnObject.Status = "ERROR";
		    return returnObject;
		}

	   	return returnObject;
	},*/

	cityStateForUI: function (response : Object, zipFromClient : String, billingZip : String, isBilling : Boolean) : Object {

	    // read pipeline dictionary input parameter
	    var res : Object = response;
		var cleanResponse : Object = {};
	    var cities : Array = [], state : String = '', states : Array = [], country : String = '', county : String = '', bucket : Object = {};
	    for(var items in res){
	    	var obj = res[items];
	    	for(var prop in obj){
	    		switch(prop){
	    			case 'city':
	    				cities.push( obj[prop] );
	    			break;
	    			case 'state':
	    				states.push(obj[prop]);
	    				if(state != obj[prop]) state= obj[prop];
	    			break;
	    			case 'country':
	    				if(country != obj[prop]) country= obj[prop];
	    			break;
	    			case 'county':
	    				if(county != obj[prop]) county= obj[prop];
	    			break;
	    			default:

	    			break;
	    		}
	    	}
	    }

	    //NOTE: the json.isml does not seem to want to parse this array, so I'm sending as Piped string
	    bucket.cities = cities.join("|");
	    bucket.states = states.join("|");
	    bucket.state = state;
	    bucket.country = country;
	    bucket.county = county;

	    // write pipeline dictionary output parameter

	    cleanResponse = bucket;
	    var s = JSON.stringify(bucket);

	    if(!isBilling){
	    	if(session.custom.citySelector == null || session.custom.citySelector != s){
		    	session.custom.citySelector = bucket.cities;
		    	session.custom.state = state;
		    	session.custom.states = bucket.states;
		    	session.custom.country = country;
		    	session.custom.county = county;
		    }

		    if(session.custom.customerZoneZip == null || session.custom.customerZoneZip != zipFromClient){
		    	session.custom.customerZoneZip = zipFromClient;
		    }
	    }else{
	    	if(session.custom.billingCitySelector == null || session.custom.billingCitySelector != s){
		    	session.custom.billingCitySelector = bucket.cities;
		    	session.custom.billingState = state;
		    	session.custom.billingStates = bucket.states;
		    	session.custom.billingCountry = country;
		    	session.custom.billingCounty = county;
		    }

	    	if(session.custom.billingZip == null || session.custom.billingZip != billingZip){
		    	session.custom.billingZip = billingZip;
		    }
	    }

	   return cleanResponse;
	},

	compareBasketHash: function ( AllPLI : dw.util.Iterator ) : Boolean
	{

	    // read pipeline dictionary input parameter
	    var pli : Iterator = AllPLI;
	    var concatIDs : String = '';
	    var storedHash = session.custom.BasketHash;
	    var BasketHasChanged : Boolean = false;

	    if(storedHash !== null){
	    	while(pli.hasNext()){
	    		var pl = pli.next();
	    		concatIDs += pl.productID + pl.quantityValue;
	    	}

	    	var B : Bytes = new Bytes(concatIDs, "UTF-8");
	    	var newHash = Encoding.toBase64(B);

	    	if(newHash === storedHash){
		    	BasketHasChanged = false;
		    }else{
		    	BasketHasChanged = true;
		    }
	    }else{
	    	BasketHasChanged = false;
	    }

	   return BasketHasChanged;
	},
	/*
	*	@input addressLine1 : String Address Line 1
	*	@input addressLine2 : String Address Line 2
	*	@input apartmentNumber : String Apartment Number
	*	@input city : String City
	*	@input stateCode : String State Code
	*	@input zipCode : String Zip Code
	*
	*   @output address : Object
	 */
	createAddressObject: function ( addressLine1, addressLine2, apartmentNumber, city, stateCode, zipCode) : Object
	{
		var _addressLine1 = (!empty(addressLine1)) ? StringUtils.trim(addressLine1) : "";
		var _addressLine2 = (!empty(addressLine2)) ? StringUtils.trim(addressLine2) : "";
		var _apartmentNumber = (!empty(apartmentNumber)) ? StringUtils.trim(apartmentNumber) : "";
		var _city = (!empty(city)) ? StringUtils.trim(city) : "";
		var _stateCode = (!empty(stateCode)) ? StringUtils.trim(stateCode) : "";
		var _zipCode = (!empty(zipCode)) ? StringUtils.trim(zipCode) : "";

		var address = {
			addressLine1: _addressLine1,
			addressLine2: _addressLine2,
			apartmentNumber: _apartmentNumber,
			city: _city,
			stateCode: _stateCode,
			zipCode: _zipCode
		}

	   return address;
	},
	/*
	*-
	*-	Description: 	Calls Sleepy's GetCitiesAndStatesForZipCode Service to return
	*-					the correct values for both the city and state drop down boxes
	*-					based on the entered zip code
	*
	*	@input ZipCode : String Zip Code
	*   ---------------------------------------
	*	@return CityStateJSON : String;
	*/
	/*getCitiesAndStatesForZipCode : function (zipCode) : String {

	    var service:Service;
	    var result:Result;
		var returnObject : Object = {};

		var _cityStateJSONArr : Array = new Array();
		var _i = 0, _j = 0;

		if (empty(zipCode)) {
			returnObject.Status = "ERROR";
		}


		service = ServiceRegistry.get("sleepys.citiesandstatesforzipcodeservice.soap");
		result = service.call(zipCode);

		if(result.status == "OK")
		{
			if(!empty(result.object) && !empty(result.object.response)) {
				var _itemCount = result.object.response.webCities.length;

				if(_itemCount > 0) {
					var _cities : ArrayList = new ArrayList();

					// Filter out any duplicate cities. (for cities that exist in multiple counties)
					for each(var __obj in result.object.response.webCities) {
						if(!_cities.contains(__obj.city)) {
							_cities.add(__obj.city);
						}
					}
					_itemCount = _cities.length;

					_cityStateJSONArr[_j++] = "{ \"cities\" : [";
					_cities.clear();

					for each(var __obj in result.object.response.webCities) {
						if(!_cities.contains(__obj.city)) {
							_cities.add(__obj.city);

							_cityStateJSONArr[_j++] = "{";
							_cityStateJSONArr[_j++] = StringUtils.format("\"city\" : \"{0}\", \"state\" : \"{1}\", \"county\" : \"{2}\" ,\"country\" : \"{3}\"", __obj.city, __obj.state, __obj.county, __obj.country);
							_cityStateJSONArr[_j++] = "}";
							if(_itemCount != (_i+1))
								_cityStateJSONArr[_j++] = ",";
							_i++;
						}
					}
					_cityStateJSONArr[_j++] = "] }";
				}
			}
		}

		 return _cityStateJSONArr.join("");
	},*/

	getClientCityState : function () : Object
	{
		var returnObject : Object = {};
		if (request.getGeolocation() != null) {
			var city : String = request.getGeolocation().getCity();
			var state: String = request.getGeolocation().getRegionCode();
			var postalCode : String = request.getGeolocation().getPostalCode();

			sleepysHelper.logInfo("Geolocation city:" + city + "\n");
			sleepysHelper.logInfo("Geolocation state:" + state + "\n");
			sleepysHelper.logInfo("Geolocation zipcode:" + postalCode + "\n");

			returnObject.city = city;
			returnObject.state = state;
			returnObject.zipcode = postalCode;
		}

		return returnObject;
	},

	getClientIPAddress : function()
	{

		var ipAddress : String = null;

		var httpHeaders : Map = request.getHttpHeaders();

		if (httpHeaders != null) {

			// Log out all of the headers
			var headers : String = "";

			for each (var entry : MapEntry in httpHeaders.entrySet()) {

			    var key = entry.getKey();
			    var value = entry.getValue();

				headers += "Key: " + key + ", Value: " + value + "\n";

			}

			sleepysHelper.logInfo("HTTP Headers: \n" + headers);

			// Determine if the client ip address is being passed through Akamai and return the correct one.
			if (httpHeaders.get("true-client-ip") == null) {
				ipAddress = request.getHttpRemoteAddress();
				sleepysHelper.logInfo("True Client IP was null.  Passing Remote Address instead: " + ipAddress);
			}
			else {
				ipAddress = httpHeaders.get("true-client-ip");
				sleepysHelper.logInfo("True Client IP is available from Akamai: " + ipAddress);
			}

		}

		return ipAddress;
	},

	/*getDeliveries : function (basket, address, maxATP, page, maxDeliveryWait) : Object {

	    var service:Service;
	    var result:Result;
		var _address = address;
		var _maxATP = maxATP;
		var _basket = basket;
		var _page = (!empty(page)) ? page : 0;
		var _maxDeliveryWait = (!empty(maxDeliveryWait)) ? maxDeliveryWait : 30;
		var returnObject : Object = {};

		var _masterStoreCode = sleepysHelper.getMasterStoreCode();

		if (empty(_address) || empty(_maxATP) || empty(_basket) || empty(_masterStoreCode)) {
			returnObject.Status = "ERROR";
			return returnObject;
		}

		service = ServiceRegistry.get("sleepys.deliveriesservice.soap");
		params = {"address": _address, "basket" : _basket, "maxATP" :  _maxATP, "page" :  _page, "maxDeliveryWait" :  _maxDeliveryWait, "masterStoreCode" :  _masterStoreCode};
		result = service.call(params);

		if(result.status == "OK")
		{
			if(!empty(result.object) && !empty(result.object.response))
			{
				returnObject.Result = result.object.response.windows;
			}
		}

	   	return returnObject;
	},*/

	/*getDeliveryStatusForCustomer : function (customerId) : Number {
	    var service:Service;
	    var result:Result;
	    var params;
	    var returnObject : Object = {};

		if (empty(customerId)) {
			//TODO Probably need to return an error status here.
			returnObject.Status = "ERROR";
			return returnObject;
		}

		try {

		service = ServiceRegistry.get("sleepys.deliverystatusforcustomerservice.soap");

		params = {"customerId": customerId};
		result = service.call(params);

		returnObject.Result = result.object;
		returnObject.Status = result.object.status;
		}
		catch (e) {
			var error = e;
		}

	   	return returnObject;
	},*/
	/*
	 * 	Description: 	Calls Sleepy's GetZoneForIP Service to retrieve a zone for a specified ip address, or zip code
	 *
	 *	@input iPAddress : String IP Address
	 *	@input zipCode : String Zip Code
	 *	@input city : String City
	 *	@input state : String State
	*/
	getZoneForIP : function (ipAddress, zipCode, city, state) : Object {

	    var service:Service;
	    var result:Result;
	    var params;
	    var returnObject : Object = {};

		var masterStoreCode = sleepysHelper.getMasterStoreCode();

		if (empty(masterStoreCode)) {
			//TODO Probably need to return an error status here.
			returnObject.Status = "ERROR";
			return returnObject;
		}

		if (empty(ipAddress) && empty(zipCode) && (empty(city) && empty(state)) ) {
			//TODO Probably need to return an error status here.
			returnObject.Status = "ERROR";
			return returnObject;
		}

		service = ServiceRegistry.get("sleepys.zoneforipservice.soap");
		params = {"masterStoreCode": masterStoreCode, "ipAddress" : ipAddress, "zipCode" : zipCode, "city" : city, "state" : state};
		result = service.call(params);

		returnObject.Result = result.object;
		if (empty(result.object)) {
			returnObject.Status = "ERROR";
		} else returnObject.Status = result.object.status;

	   	return returnObject;
	},
	/*
	 * @input zipCode : String Zip Code
	 *
	 */
	getZoneForPostalCode : function ( zipCode) : Object {

		var returnObject : Object = {};
		if (empty(zipCode)) {
			returnObject.Status = "ERROR";
			returnObject.ErrorMessage = "Empty zipCode";
			return returnObject;
		}

		var _zipCode : Number = new Number(zipCode);

		var queryString = "custom.zipcode_start <= {0} and custom.zipcode_end >= {1}";

		var zipZoneInfoItr : SeekableIterator = CustomObjectMgr.queryCustomObjects("ZipZoneInfo", queryString, "custom.zone_code asc", _zipCode, _zipCode);

		var customerZone;

		while( zipZoneInfoItr.hasNext() ) {
			var zipZoneInfo : CustomObject = zipZoneInfoItr.next();

			if (empty(customerZone)) {
				customerZone = zipZoneInfo.custom.zone_code;
			}
			else {
				customerZone += "," + zipZoneInfo.custom.zone_code;
			}
		}

		zipZoneInfoItr.close();

		if (!empty(customerZone)) {
			returnObject.Status = "OK";
			returnObject.CustomerZone = customerZone;
		}

	   	return returnObject;
	},
	/*
	 *  	@input	emailAddress : String
	 *	@input  pageSource : String
	 */
	sendEmailInfo : function(emailAddress, pageSource) : Object {

		var returnObject : Object = {};
		if (empty(emailAddress)) {
			returnObject.Status = "ERROR";
			return returnObject;
		}
		var yyyyMMdd : String = StringUtils.formatCalendar(new Calendar(),'yyyyMMdd');
		try{
			var newsletterSubscriptions : Object = CustomObjectMgr.getCustomObject('NewsletterSubscriptions', yyyyMMdd + '-0');
			var i = 0;
			while(newsletterSubscriptions !== null && newsletterSubscriptions.custom.emailAddresses.length === 200){
				i++;
				newsletterSubscriptions = CustomObjectMgr.getCustomObject('NewsletterSubscriptions', yyyyMMdd + '-' + i);
			}

			Transaction.wrap(function () {
				if(newsletterSubscriptions === null){
					newsletterSubscriptions = CustomObjectMgr.createCustomObject('NewsletterSubscriptions', yyyyMMdd + '-' + i);

				}

				var emailAddresses : Array = new Array();

				for each(var email : String in newsletterSubscriptions.custom.emailAddresses){
					emailAddresses.push(email);
				}

				emailAddresses.push(emailAddress);

				newsletterSubscriptions.custom.emailAddresses = emailAddresses;

				returnObject.Status = 'OK';
			});
		} catch(e){
			Logger.error('DWNewsletter error:' + e);
			returnObject.Status = "ERROR";
			return returnObject;
		}
		/*
		var zipCode : String = session.custom.customerZoneZip;
		var dateTime : String = StringUtils.formatCalendar(new Calendar(),'MM/dd/yyyy HH:mm:ss');
		var masterStoreCode = sleepysHelper.getMasterStoreCode();

		var service = ServiceRegistry.get("sleepys.emailcapture.soap");

		var params = {"email":emailAddress, "dateTime":dateTime, "zipCode":zipCode, "storeCode":masterStoreCode, "pageSource":pageSource};
		var result = service.call(params);

		if (result == null || service == null) {
			returnObject.Status = "ERROR";
		    return returnObject;
		}

		returnObject.Result = result.object;
		returnObject.Status = result.status;
		*/

	   	return returnObject;

	},
	/*
	 *	@input AllPLI : dw.util.Iterator All Product Line Items from the Basket
	 */
	setBasketHash : function (AllPLI) : Number
	{

	    // read pipeline dictionary input parameter
	    var pli : Iterator = AllPLI;
	    var concatIDs : String = '';

	    while(pli.hasNext()){
	    	var pl = pli.next();
	    	concatIDs += pl.productID + pl.quantityValue;
	    }

	    var B : Bytes = new Bytes(concatIDs, "UTF-8");
	    session.custom.BasketHash = Encoding.toBase64(B);

	   return;
	},
	/*
	 * @input order : dw.order.Order
	 * @input shippingForm : dw.web.FormGroup The address form
	 */
	setMasterStoreCodeToOrder : function (order : Order, shippingForm : FormGroup) : Number
	{
		if (!empty(order)) {
			order.custom.masterStoreCode = sleepysHelper.getMasterStoreCode();

			if (!empty(order.customer.profile)) {
				order.custom.customerExternalId = order.customer.profile.credentials.externalID;
			}

			if (!empty(shippingForm)) {
				var val = shippingForm.shippingAddress.addToEmailList.value;
				order.billingAddress.custom.addToEmailList = val;
			}

		}

		return true;
	},

	setNeedsNewPasswordResetFalse : function ( customer )
	{
		try{
			if(!empty(customer.profile.custom.needsPasswordReset) && customer.profile.custom.needsPasswordReset){
				customer.profile.custom.needsPasswordReset = false;
			}
		} catch(e){
			Logger.getLogger('Sleepys', 'NeedsPasswordReset').error('SetNeedsNewPasswordResetFalse.ds - Failed to set flag to false, error: ' + e);
		}

	    return;
	},

	setOrderLevelDiscountsForProductLineItems : function ( basket )
	{
		var productLineItems = basket.getProductLineItems();

		for each (var productLineItem in productLineItems) {

			if (!productLineItem.bonusProductLineItem) {
				var orderDiscounts : Array = null;

				var proratedPriceAdjustmentPrices = productLineItem.getProratedPriceAdjustmentPrices();

				for each (var entry : MapEntry in proratedPriceAdjustmentPrices.entrySet()) {

				    var priceAdjustmentObject = entry.getKey();
				    var adjustmentPrice = entry.getValue();

				    var campaignID = "";
				    var promotionID = "";
				    var couponCode = "";

				    if (!empty(priceAdjustmentObject) && !empty(adjustmentPrice) && adjustmentPrice.available) {
				    	if (!empty(priceAdjustmentObject.campaign)
				    		&& !empty(priceAdjustmentObject.campaign.ID)) {

				    		campaignID = priceAdjustmentObject.campaign.ID;
				    	}

				    	if (!empty(priceAdjustmentObject.promotion)
				    		&& !empty(priceAdjustmentObject.promotion.ID)
				    		&& !empty(priceAdjustmentObject.promotion.promotionClass)
				    		&& priceAdjustmentObject.promotion.promotionClass == Promotion.PROMOTION_CLASS_ORDER
				    		&& priceAdjustmentObject.promotion.active == true) {

				    		promotionID = priceAdjustmentObject.promotion.ID;
				    	}
				    	else {
				    		continue;
				    	}

				    	if (!empty(priceAdjustmentObject.couponLineItem) && !empty(priceAdjustmentObject.couponLineItem.couponCode)) {

				    		couponCode = priceAdjustmentObject.couponLineItem.couponCode;
				    	}

					    var orderDiscount = campaignID + '|' + promotionID + '|' + couponCode + '|' + adjustmentPrice.value;

						if (orderDiscounts == null) {
							orderDiscounts = new Array();
						}

						orderDiscounts.push(orderDiscount);

				    }

				}

				if (!empty(orderDiscounts)) {
					productLineItem.custom.orderDiscounts = orderDiscounts;
				}
			}
		}


	   return;
	},

	showMatressRemovalOptionCheck : function ( basket ) : Boolean
	{

		var showMattressRemovalOption : Boolean = false;
		var mattressRemovalProductID = sleepysHelper.getMattressPickupProductID();

		//var mattressRemovalProduct = ProductMgr.getProduct(mattressRemovalProductID);

	    var PSM : ProductSearchModel = new ProductSearchModel();
		if (session.custom.customerZone) {
			var zone : String = session.custom.customerZone.split(',').join("|");
			PSM.addRefinementValues("zone_code", zone);
		}

		var containsTruckDeliveryItem : Boolean = false;

		PSM.setSearchPhrase(mattressRemovalProductID);
		PSM.search();

		if (PSM.count > 0) {
			var pli : Iterator = basket.getProductLineItems().iterator();

		    while (pli.hasNext()){
		    	var li : ProductLineItem = pli.next();

		    	if (li.productID != mattressRemovalProductID) {
			    	if (!empty(li.custom.shipMethod)) {
				    	if (li.custom.shipMethod == "SLEEPYS_TRUCK" || li.custom.shipMethod == "DEFAULT_TRUCK" || li.custom.shipType.indexOf('DELIVERY') > -1) {
			    			containsTruckDeliveryItem = true;
				    	}
			    	}
			    	else {
			    		if ( li.product.custom.useBrandXPDP == 'null' || !li.product.custom.useBrandXPDP){
		                    if (li.product.custom.ship_method == "DEFAULT_TRUCK") {
		                        containsTruckDeliveryItem = true;
		                    }
			    		} else {
	                        if (li.custom.shipType.indexOf('DELIVERY') > -1 ) {
	                            containsTruckDeliveryItem = true;
	                        }
			    		}
			    	}
		    	}
		    }

			if (containsTruckDeliveryItem == true) {
				showMattressRemovalOption = true;
			}

		}
		return showMattressRemovalOption;
	},

	splitShipments : function ( basket )
	{
		var defaultShipment : Shipment = null;
		var allShippingMethodsCollection : Collection = null;
		var shipmentsMap : HashMap = null;

		if (empty(basket)) {
			return false;
		}

		defaultShipment = basket.defaultShipment;

		if (empty(defaultShipment)) {
			return false;
		}

		var defaultShippingAddress : OrderAddress = defaultShipment.shippingAddress;

		if( defaultShippingAddress == null )
		{
			return false;
		}

		// Get all the shipping methods and place them in a hashmap to make it easier to retrieve a specific one.
		allShippingMethodsCollection = ShippingMgr.getAllShippingMethods();

		var allShippingMethodsMap = new HashMap();

		for each (var currentShippingMethod : ShippingMethod in allShippingMethodsCollection) {
			allShippingMethodsMap.put(currentShippingMethod.ID, currentShippingMethod);
		}

		for each (var pli : ProductLineItem in basket.getProductLineItems()){

			var shipType = pli.custom.shipType;
			var shipMethod = pli.custom.shipMethod;
			var deliveryDate = pli.custom.deliveryDate;
			var carrierCustomInfo = pli.custom.carrierCustomInfo;
			var shipmentID;
			if (!empty(pli.product.custom.dw_mattress_type) || !empty(pli.product.custom.mattress_type) || pli.product.ID == 'DWREMOVEBED') {
				shipmentID = shipMethod + '_MATTRESS';
			} else {
				shipmentID = shipMethod + '_' + deliveryDate;
			}

			var shipment : Shipment = null;
			var orderAddress : OrderAddress = null;

			// Copy the product's manufacturer SKU into the product line items' custom attribute so it is
			// available when the order is exported.
			pli.custom.manufacturerSKU = pli.product.manufacturerSKU;

			// Check to see if this shipment exists already otherwise create a new one
		    shipment = basket.getShipment(shipmentID);

		    if (shipment == null) {
		    	shipment = basket.createShipment(shipmentID);
		    }

			// Copy the address from the default shipment and set it to this shipment.
			orderAddress = shipment.createShippingAddress();

			var shippingAddress : Object = new ShippingAddress();
			shippingAddress.UUID = UUIDUtils.createUUID();
			shippingAddress.copyFrom(defaultShippingAddress);
			shippingAddress.copyTo(orderAddress);

			// Assign this shipment's common product line item values (ship type,
			// delivery date, carrier custom info) to the shipment.
			shipment.custom.deliveryDate = deliveryDate;
			shipment.custom.carrierCustomInfo = carrierCustomInfo;
			shipment.custom.shipType = shipType;

			// Retrieve the shipping method from the map
			var shippingMethod = allShippingMethodsMap.get(shipMethod);

			// Assign the shipping method to this shipment
			if (shippingMethod != null) {
				shipment.setShippingMethod(shippingMethod);
			}

			// Move the line item to this shipment
			pli.setShipment(shipment);
		}

		return true;
	},

	setCustomerZone : function ( zipCode ) {
//		zipCode = '60631';
		if (empty(zipCode)) {
			if (empty(session.custom.customerZone)) {
				var ipAddress = SleepysPipeletHelper.getClientIPAddress();
				var clientsCityState = SleepysPipeletHelper.getClientCityState();
				if (!empty(ipAddress) && !empty(clientsCityState.city) && !empty(clientsCityState.state)) {
				//	var zoneObject = SleepysPipeletHelper.getZoneForIP(ipAddress, clientsCityState.zipcode, clientsCityState.city, clientsCityState.state);
				//	if (!empty(zoneObject.Status) && zoneObject.Status.code == "OK") {
				//		session.custom.customerZone = zoneObject.Result.response.zoneCode;
						session.custom.customerZip = clientsCityState.zipcode;
						mattressPipeletHelper.getCitiesAndStatesForZipCode(clientsCityState.zipcode, true);
						/*	set Preferred store when identifying IP from geolocation	*/
						if (!session.customer.registered && !empty(session.custom.customerZip)) {
							var app = require('app_storefront_controllers/cartridge/scripts/app');
							app.getController('StorePicker').SetPreferredStoreFromGeoLocation();
						}
				//	}
				}
			}
		} else {
			var customerZoneObject = SleepysPipeletHelper.getZoneForPostalCode(zipCode);
			if (customerZoneObject.Status == "OK") {
				session.custom.customerZone = customerZoneObject.CustomerZone;
				session.custom.customerZip = zipCode;
				mattressPipeletHelper.getCitiesAndStatesForZipCode(zipCode, true);
			}
		}
		return true;
	},

	structureDeliveryDatesForUI : function (dates)
	{

		var returnObject : Object = {};
	    var structured : Array = new Array();
	    var coll : Array = new Array();
	    var currentDate : String = '';
	    var datesLength : Number = parseInt(dates.length);
	    var i : Number = 0;
	    var mNames : Array = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

	    while(i < datesLength+1){
	    	if(i == datesLength){
	    		structured = SleepysPipeletHelper.buildOb(coll, currentDate, mNames, structured);
	    		break;
	    	}

	    	var obj : Object = dates[i];
	    	if (obj.deliveryDate != null) {
		    	var d = obj.deliveryDate.toString();

		    	if(currentDate != d){
		    		structured = SleepysPipeletHelper.buildOb(coll, currentDate, mNames, structured);
		    		currentDate = d;
				}

				coll.push(obj);
	    	}
			i++;
	    }

	    var dString : String = JSON.stringify(structured);

	    returnObject.JSON = dString;
		//session.custom.loadDeliveries = true;
	    returnObject.DateListObject = structured;

	   return returnObject;
	},

	buildOb : function (coll : Array, date : String, mNames : Array, structured : Array){
		if(coll.length > 0){
			var a = date.split('-');

			var ob = {
				date: date,
				month: mNames[parseInt(a[1] -1)],
				day: a[2],
				dates: coll.slice(0)
			}
			structured.push(ob);
		}
		coll.length = 0;
		return structured;
	},

	suggestedAddressLogic : function ( addressScore, suggestedAddress )
	{
		var returnObject : Object = {};
		returnObject.cleanSuggestedAddress = false;
		returnObject.doSuggest = false;
		var obj : ArrayList = new ArrayList();

	    if(addressScore >= 0){
	    	var addy : Object = suggestedAddress;

	    	for(var field in addy){
	    		var  a = args[field],
	    			 b = addy[field];
	    		// If the Suggested Field is not null or blank, it's compared in Uppercase to the entered value (case insensitive)
	    		if((b !== null && b != '') && a.toUpperCase() != b.toUpperCase()){
	    			 obj.add({"a": a, "b": b, "name": field});
	    		}
	    	}
	    	returnObject.CleanSuggestedAddress = obj;
	    }

	    returnObject.DoSuggest = (addressScore < 1) ? true : false;

	   return returnObject;
	},

	updateBasketForGetDeliveriesFallback : function ( basket )
	{
		var _basket = basket;
		var returnObject : Object = {};

		if (empty(_basket)) {
			returnObject.Status = "ERROR";
		    return returnObject;
		}

		var _productLineItems = _basket.getAllProductLineItems();

		for each(var __pli : ProductLineItem in _productLineItems) {

			if (__pli.custom.shipType == "DELIVERY") {
				__pli.custom.deliveryDate = 'NO_TIME_SLOTS_AVAILABLE';
				__pli.custom.carrierCustomInfo = __pli.custom.shipMethod + '___NO_TIME_SLOTS_AVAILABLE';
			}
		}
		returnObject.Status = "OK";
		return returnObject;
	},

	updateBasketFromCheckATPLineItems : function ( ATPLineItems, Basket )
	{
		var _basket : Basket = Basket;
		var _getDeliverySchedule : Boolean = false;
	    var _sleepysTruck : Boolean = false;
	    var _whiteGlove : Boolean = false;
		var _nonFleetwiseTruck : Boolean = false;
		var _atpLineItems : Object = ATPLineItems;
		var returnObject : Object = {};

		if (empty(_basket)) {
			returnObject.Status = "ERROR";
		    return returnObject;
		}

		if(!empty(_atpLineItems) && _atpLineItems.length > 0)
		{
			var _checkATPLineItemsMap : HashMap = new HashMap();

			for each(var __currentCheckATPLineItem : Object in _atpLineItems) {
				_checkATPLineItemsMap.put(__currentCheckATPLineItem.itemCode, __currentCheckATPLineItem);
			}

			var _productLineItems = _basket.getAllProductLineItems();
			var _checkATPLineItem = null;

			for each(var __pli : ProductLineItem in _productLineItems) {

				_checkATPLineItem = _checkATPLineItemsMap.get(__pli.manufacturerSKU);

				/* the following "if block" of code was added to resolve defect SLPS-194 where the _checkATPLineItem.shipMethod returned an empty value from the call to CheckATP */
				/* in this particular case, the checkATPLineItem.atpDate had a value, but the checkATPLineItem.shipMethod did not */
				if (empty(_checkATPLineItem.shipMethod)) {
					_checkATPLineItem.shipMethod = __pli.product.custom.ship_method.value
				}

				if (empty(_checkATPLineItem)) {
					__pli.custom.shipMethod = __pli.product.custom.ship_method.value;
					__pli.custom.shipType = '';
					__pli.custom.deliveryDate = 'NO_TIME_SLOTS_AVAILABLE';
					__pli.custom.carrierCustomInfo = __pli.product.custom.ship_method.value + '___NO_TIME_SLOTS_AVAILABLE';
					break;
				}

				if (_checkATPLineItem.shipType != "DELIVERY") {
					__pli.custom.shipMethod = _checkATPLineItem.shipMethod;
					__pli.custom.shipType = _checkATPLineItem.shipType;
					__pli.custom.deliveryDate = _checkATPLineItem.atpDate;
					__pli.custom.carrierCustomInfo = _checkATPLineItem.shipMethod + '___' + _checkATPLineItem.atpDate;
				}
				else
				{
					__pli.custom.shipMethod = _checkATPLineItem.shipMethod;
					__pli.custom.shipType = _checkATPLineItem.shipType;
					__pli.custom.deliveryDate = '';
					__pli.custom.carrierCustomInfo = '';
					_getDeliverySchedule = true;

					 if(_checkATPLineItem.shipMethod == "SLEEPYS_TRUCK")
					 {
					 	_sleepysTruck = true;
					 }
					 else if (_checkATPLineItem.shipMethod == "NON_FLEETWISE_TRUCK")
					 {
					 	_nonFleetwiseTruck = true;
					 }
				}
			}
		}
		else {
			var _productLineItems = _basket.getAllProductLineItems();

			for each(var __pli : ProductLineItem in _productLineItems) {
				if ('shipMethod' in __pli.custom && __pli.custom.shipMethod.indexOf('USER_') == -1){
				    __pli.custom.shipMethod = __pli.product.custom.ship_method.value;
	            }
				__pli.custom.shipType = '';
				__pli.custom.deliveryDate = 'NO_TIME_SLOTS_AVAILABLE';
				__pli.custom.carrierCustomInfo = __pli.product.custom.ship_method.value + '___NO_TIME_SLOTS_AVAILABLE';
			}

			_getDeliverySchedule = false;
		}

		returnObject.GetDeliverySchedule = _getDeliverySchedule;
		returnObject.SleepysTruck = _sleepysTruck;
		returnObject.NonFleetwiseTruck = _nonFleetwiseTruck;

		return returnObject;
	},

	updateBasketWithSelectedDate : function ( deliveryDate, fleetwiseToken, basket)
	{
		var deliveryDate = mattressPipeletHelper.getISODate(deliveryDate);
		var token = fleetwiseToken;
//		var basket : dw.order.Basket = Basket;
		var returnObject : Object = {};

		if (empty(basket)) {
			returnObject.Status = "ERROR";
		    return returnObject;
		}

		var productLineItems = basket.getAllProductLineItems();

	// Decided not to use this code, instead for ShipMethod of NON_FLEETWISE_TRUCK use the date that is in the token
	/* Convert deliveryDate */
	/* From:  Thursday, Aug 24, between 9am and 5pm */
	/* To:  2014-08-21 */
//		var carrierDeliveryDate = '';
//		var arrSchedDelivery = deliveryDate.split(","); /* separate date from string using comma as separator */
//		if (arrSchedDelivery.length > 1) {
//			var arrSchedDate = arrSchedDelivery[1].split(" "); /* separate month and day */
//			if (arrSchedDate.length > 1) {
//				var schedMonth = arrSchedDate[1]; /* get scheduled month e.g. 'Aug' */
//				var schedDay = arrSchedDate[2].toString(); /* get scheduled day e.g. '24' */
//				var schedMonthNumber = "JanFebMarAprMayJunJulAugSepOctNovDec".indexOf(schedMonth) / 3 + 1; /* get number from month string  e.g. convert 'Aug' to 8 */
//				var strSchedMonthNumber = (schedMonthNumber < 10) ? "0" + schedMonthNumber : schedMonthNumber; /* get two digit month  e.g. convert 8 to 08 */
//				var strSchedDayNumber = (schedDay.length == 1) ? "0" + schedDay : schedDay; /* get two digit day of month  e.g. convert 3 to 03 */
//				var curYear = new Date().getFullYear(); /* get current year */
//				var curMonthNumber = new Date().getMonth() + 1; /* get current numeric month */
//				/* if it is now December, but delivery is scheduled for January of February then increment year */
//				if (curMonthNumber > schedMonthNumber ) {
//					curYear++;
//				}
//				carrierDeliveryDate = curYear.toString() + '-' + strSchedMonthNumber + '-' + strSchedDayNumber;
//			}
//		}

		for each(var productLineItem in productLineItems) {

			if (productLineItem.custom.shipType == "DELIVERY") {

				if (deliveryDate == "USER_UNDECIDED") {
					productLineItem.custom.carrierCustomInfo = productLineItem.custom.shipMethod + '___' + deliveryDate;
				}
				else {
					if (productLineItem.custom.shipMethod == "SLEEPYS_TRUCK") {
						productLineItem.custom.carrierCustomInfo = productLineItem.custom.shipMethod + '___' + token;
					}
					else {
						productLineItem.custom.carrierCustomInfo = token.replace("_20","___20");
					}
				}

				productLineItem.custom.deliveryDate = deliveryDate;
			}
		}
		returnObject.Status = "OK";
		return returnObject;
	},

	updateOrderDeliveryStatus : function ( siestaOrders, historyOrders )
	{

		var found : Boolean = false;
		var historyOrderList : List = historyOrders.asList();

		for(var orderIndex in siestaOrders){
			var orderStatus = siestaOrders[orderIndex].deliveryStatus;
			var webOrderId = siestaOrders[orderIndex].webOrderID;
			found = false;

			for (historyOrderIndex = 0; historyOrderIndex < historyOrderList.size(); historyOrderIndex++) {
				var tempHistoryOrder : Order = historyOrderList[historyOrderIndex];
				var historyOrderId = tempHistoryOrder.orderNo;
				if (webOrderId == historyOrderId) {
					//Set order status.
					switch(orderStatus) {
						case 'CANCELLED' : tempHistoryOrder.setStatus(dw.order.Order.ORDER_STATUS_CANCELLED);
							break;
						case 'CLOSED' : tempHistoryOrder.setStatus(dw.order.Order.ORDER_STATUS_COMPLETED);
							break;
						case 'BOOKED' : tempHistoryOrder.setStatus(dw.order.Order.ORDER_STATUS_OPEN);
							break;
						default :
							break;
					}

					var historyShipments = tempHistoryOrder.getShipments();
					var webShipments = siestaOrders[orderIndex].shipments;
					for (var historyShipmentIndex in historyShipments) {
						var tempShipment = historyShipments[historyShipmentIndex];
						for(var webShipmentIndex in webShipments) {
							var tempWebShipment = webShipments[historyShipmentIndex];
							if (tempShipment.shipmentNo == tempWebShipment.shipmentId) {
								//Set shipment status.
								var shipmentStatus = tempWebShipment.shipping_status;
								switch(shipmentStatus) {
								case 'SHIPPED' : tempShipment.setShippingStatus(dw.order.Order.SHIPPING_STATUS_SHIPPED);
									break;
								case 'NOT_SHIPPED' : tempShipment.setShippingStatus(dw.order.Order.SHIPPING_STATUS_NOTSHIPPED);
									break;
								default :
									break;
								}

							}
							//Set tracking number.
							if(!empty(tempShipment.trackingNumber)) {
								tempShipment.trackingNumber = tempWebShipment.trackingNo;
							}
							//Set actural deliery date.
							if (!empty(tempWebShipment.actualShipDate)) {
								var pli = tempShipment.getProductLineItems();
								for (var pliIndex in pli) {
									var tempPli = tempShipment.getProductLineItems()[pliIndex];
									tempPli.custom.deliveryDate = tempWebShipment.actualShipDate;
								}
							}
						}
					}
					found = true;
				}
			}
		}


	   return true;
	},

	/*getOrders : function (customerId, ordersUnpaged)
	{
		var result;

		result = SleepysPipeletHelper.getDeliveryStatusForCustomer(customerId);
		if (!empty(result.Status) && result.Status.code == "OK") {
			if (SleepysPipeletHelper.updateOrderDeliveryStatus(result.Result.response.orders, ordersUnpaged))
			{

				return true;
			}
		}
	},*/

	createSiestaPaymentObject : function (formGroup)
	{
		var makeAPaymentFormGroup = formGroup;

		var paymentObject = new Object();
		paymentObject.paymentAmount = makeAPaymentFormGroup.paymentAmount.value;
		paymentObject.nameOnCard = makeAPaymentFormGroup.creditcard.owner.value;
		paymentObject.cardNumber = makeAPaymentFormGroup.creditcard.number.value;
		paymentObject.expMonth = makeAPaymentFormGroup.creditcard.month.value;
		paymentObject.expYear = makeAPaymentFormGroup.creditcard.year.value;
		paymentObject.securityCode = makeAPaymentFormGroup.creditcard.cvn.value;
		paymentObject.paymentType = makeAPaymentFormGroup.creditcard.type.value;
		paymentObject.emailAddress = makeAPaymentFormGroup.emailAddress.value;
		paymentObject.clientIP = SleepysPipeletHelper.getClientIPAddress();

		return paymentObject;
	},

	pdpZoneCheck : function (pid)
	{
		var customerZones : String = ('customerZone' in session.custom && !empty(session.custom.customerZone)) ? session.custom.customerZone.split(',') : null;
		var lineProduct : Product = ProductMgr.getProduct(pid);
		var validZones : ArrayList	= new ArrayList(lineProduct.custom.zone_code);
		var availInZone = false;

		for each(var zone in customerZones) {
			if(validZones.contains(zone)) {
				if (lineProduct.custom.useBrandXPDP){
					if (zone != 'US'){
	                    availInZone = true;
	                    break;
					}
				} else {
					availInZone = true;
					break;
				}
			}
		}
		return availInZone;
	},

	clearAllShipmentSchedules : function ( basket )
	{
		var mattressExists = false;

		/* loop through all products in the basket and remove any special delivery options that were created during the checkout process */
		for each(var __lineitem : ProductLineItem in basket.object.getProductLineItems())
		{
			if('carrierCustomInfo' in __lineitem.custom && !empty(__lineitem.custom.carrierCustomInfo)) {__lineitem.custom.carrierCustomInfo = null};
			if('deliveryDate' in __lineitem.custom && !empty(__lineitem.custom.deliveryDate)) {__lineitem.custom.deliveryDate = null};
			if('manufacturerSKU' in __lineitem.custom && !empty(__lineitem.custom.manufacturerSKU)) {__lineitem.custom.manufacturerSKU = null};
			if('shipMethod' in __lineitem.custom && !empty(__lineitem.custom.shipMethod)) {__lineitem.custom.shipMethod = null};
			if('carrierCustomInfo' in __lineitem.custom && !empty(__lineitem.custom.carrierCustomInfo)) {__lineitem.custom.carrierCustomInfo = null};
		}

	    return true;
	}
};

exports.SleepysPipeletHelper = SleepysPipeletHelper;