require('/bc_sleepysc/cartridge/scripts/sleepys/services/AbstractSleepysService');
var sf = require("bc_serviceframework");
var CheckATPResponse = require("../objects/CheckATPResponse").CheckATPResponse; 

/**
 * Will return the CheckATPService var.
 * 
 * @returns Object The created CheckATPService object.
 */
function getCheckATPService() {
	return CheckATPService;
}

var CheckATPService = sf.getService('AbstractSleepysService').extend(
/** @lends CheckATPService.prototype */ 
{
	/**
	 * @constructs 
	 * @augments AbstractSleepysService
	 */
	init: function() {
		this._super("CheckATPService" , new CheckATPResponse());
	},
	
	
	/**
	 * Initialises the webservice object parsed from the given WSDL File
	 */
	initServiceClient: function() {
		this.webReference = webreferences.WebOrderService;			
		this.serviceClient = this.webReference.getDefaultService();	
	},

	createRequest: function (address, basket, masterStoreCode) {

		var webOrder = new this.webReference.WebOrder();
		
		var deliveryAddress = new this.webReference.StreetAddress();
		
		deliveryAddress.setAddressLine1(address.addressLine1);
		deliveryAddress.setAddressLine2(address.addressLine2);
		deliveryAddress.setApartmentNumber(address.apartmentNumber);
		deliveryAddress.setCity(address.city);
		deliveryAddress.setStateCode(address.stateCode);
		deliveryAddress.setZipCode(address.zipCode);
		
		// Set the delivery address
		webOrder.setDeliveryAddress(deliveryAddress);
		
		// Set the store code
		webOrder.setWebStoreId(masterStoreCode);
		
		// Set the order line items
		var webOrderLines : Array = new Array();
		
		var productLineItems = basket.getAllProductLineItems();
		
		for each(var productLineItem in productLineItems) {

			var webOrderLine = new this.webReference.WebOrderLine();
			
			webOrderLine.setItemCode(productLineItem.manufacturerSKU);
			webOrderLine.setQuantity(productLineItem.quantity.value);
			//webOrderLine.setUnitPrice(1000.00);
			
			webOrderLines.push(webOrderLine);
		}
		
		webOrder.setLineItems(webOrderLines);
		
		var requestObject = new this.webReference.CheckAtp(webOrder);

		return requestObject;			
	},
	
	initStatusFromResponse: function () {

		if (!empty(this.response) && !empty(this.response._return)) {
			var statusCode = this.response._return.statusCode;
			var status = this.response._return.status;
		
			if (statusCode == 0 || statusCode == 200) {
				this.status.code = "OK";
			}
			else {
				this.status.code = "ERROR";
			}

			this.status.error = statusCode;
			this.status.msg = status;

		}
		else {
			this.status.code = "ERROR";
		}
		
	},
		
	/** 
	 * Call the service and return the response.
	 * 
	 */ 
	executeServiceCall: function() {	
		return this.serviceClient.checkAtp(this.request);
	},
				
	getDummyResponse: function() : Object {
		return this.object.dummy();
	}
});
sf.registerService('CheckATPService',CheckATPService);