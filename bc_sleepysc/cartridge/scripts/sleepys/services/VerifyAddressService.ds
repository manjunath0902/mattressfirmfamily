require('/bc_sleepysc/cartridge/scripts/sleepys/services/AbstractSleepysService');
var sf = require("bc_serviceframework");
var VerifyAddressResponse = require("../objects/VerifyAddressResponse").VerifyAddressResponse;

/**
 * Will return the VerifyAddressService var.
 * 
 * @returns Object The created VerifyAddressService object.
 */
function getVerifyAddressService() {
	return VerifyAddressService;
}

var VerifyAddressService = sf.getService('AbstractSleepysService').extend(
/** @lends VerifyAddressService.prototype */ 
{
	/**
	 * @constructs 
	 * @augments AbstractSleepysService
	 */
	init: function() {
		this._super("VerifyAddressService" , new VerifyAddressResponse());
	},
	
	
	/**
	 * Initialises the webservice object parsed from the given WSDL File
	 */
	initServiceClient: function() {
		this.webReference = webreferences.AddressVerificationService;			
		this.serviceClient = this.webReference.getDefaultService();	
	},

	createRequest: function (addressLine1, addressLine2, apartmentNumber, city, stateCode, zipCode) {
				
		var requestObject = new this.webReference.VerifyAddress();

		var streetAddress = new this.webReference.StreetAddress();
		
		streetAddress.setAddressLine1( (!empty(addressLine1)) ? addressLine1 : "");
		streetAddress.setAddressLine2((!empty(addressLine2)) ? addressLine2 : "");
		streetAddress.setApartmentNumber((!empty(apartmentNumber)) ? apartmentNumber : "");
		streetAddress.setCity((!empty(city)) ? city : "");
		streetAddress.setStateCode((!empty(stateCode)) ? stateCode : "");
		streetAddress.setZipCode((!empty(zipCode)) ? zipCode : "");
		
		requestObject.setStreetAddress(streetAddress);
		
		return requestObject;			
	},
	
	initStatusFromResponse: function () {
		
		if (!empty(this.response) && !empty(this.response._return)) {
			var statusCode = this.response._return.statusCode;
			var status = this.response._return.status;
		
			if (statusCode == 0 || statusCode == 200) {
				this.status.code = "OK";
			}
			else {
				this.status.code = "ERROR";
			}

			this.status.error = statusCode;
			this.status.msg = status;

		}
		else {
			this.status.code = "ERROR";
		}

	},
		
	/** 
	 * Call the service and return the response.
	 * 
	 */ 
	executeServiceCall: function() {	
		return this.serviceClient.verifyAddress(this.request);
	},
	
	getDummyResponse: function() : Object {
		return this.object.dummy();
	}
});
sf.registerService('VerifyAddressService',VerifyAddressService);
