'use strict';

/**
 * Controller 
 *
 * @module controllers/Sleepys_EmailSubmit
 */

/* API includes */
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var StringUtils = require('dw/util/StringUtils');
var Transaction = require('dw/system/Transaction');

/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var pipeletHelper = require('~/cartridge/scripts/sleepys/util/SleepysPipeletsHelper').SleepysPipeletHelper;

var sleepysHelper = require("~/cartridge/scripts/sleepys/util/SleepysUtils").SleepysHelper; 


// Recieves the AJAX submission of email address and responds
function submitEmail() {
	var params = request.httpParameterMap;
	var zipCode = null;
	var emailAddress : String = params.email.stringValue;
	//save e-mail address using lower case
	emailAddress = emailAddress.toLowerCase();
	// REGEX to check email
	if (!/^[a-zA-Z0-9]+(?:(\.|_)[A-Za-z0-9!#$%&'*+/=?^`{|}~-]+)*@(?!([a-zA-Z0-9]*\.[a-zA-Z0-9]*\.[a-zA-Z0-9]*\.))(?:[A-Za-z0-9](?:[a-zA-Z0-9-]*[A-Za-z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/.test(emailAddress)) {
	// email falied validation
	}
	
	var pageSource : String = params.pageSource.stringValue;

	var returnResult = pipeletHelper.sendEmailInfo(emailAddress, pageSource);

	app.getView('EmailSignup', {SaveEmailAddressStatus : returnResult.Status}).render('sleepys/emailsignup/SaveEmailAddressResponse');
	
}

exports.Submit = guard.ensure(['post'], submitEmail);

