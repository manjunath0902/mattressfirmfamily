// Older "accept" file extension method. Old docs: http://docs.jquery.com/Plugins/Validation/Methods/accept
$.validator.addMethod("extension", function(value, element, param) {
	param = typeof param === "string" ? param.replace(/,/g, "|")
			: "png|jpe?g|gif";
	return this.optional(element)
			|| value.match(new RegExp("\\.(" + param + ")$", "i"));
}, null);

/**
 * Return true if the field value matches the given format RegExp
 * 
 * @example $.validator.methods.pattern("AR1004",element,/^AR\d{4}$/)
 * @result true
 * 
 * @example $.validator.methods.pattern("BR1004",element,/^AR\d{4}$/)
 * @result false
 * 
 * @name $.validator.methods.pattern
 * @type Boolean
 * @cat Plugins/Validate/Methods
 */
$.validator.addMethod("pattern", function(value, element, param) {
	if (this.optional(element)) {
		return true;
	}
	if (typeof param === "string") {
		param = new RegExp("^(?:" + param + ")$");
	}
	return param.test(value);
}, "Invalid format.");

/**
 * 
 * @param value
 * @param element
 * @returns
 */
$.validator.addMethod("noSpace", function(value, element) {
	return value.indexOf(" ") < 0 && value != "";
}, "Contains space");