function Marker(t, e, o, s) {
	this.map_ = t, this.domobj_ = null, this.nb_ = e, this.latlng_ = o, this.id_ = s, this.setMap(t)
}
function lf_loadGAtracking(t, e) {
	checkGA && (window._gaq.push(["lf_widget._setAccount", "UA-36084665-1"]), window._gaq.push(["lf_widget._trackPageview"])), window._gaq.push(["lf_widget._trackEvent", "widget", t, e, 0, !1]), checkGA && !function () {
		var t = document.createElement("script");
		t.type = "text/javascript", t.async = !0, t.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
		var e = document.getElementsByTagName("script")[0];
		e.parentNode.insertBefore(t, e)
	}(), checkGA = !1
}
function lf_fillHour(t) {
	return("00" + t).slice(-2)
}
function lf_displayDate(t, e) {
	t = t.split("-");
	var o = e.split(",");
	return 3 == t.length ? t[2] + " " + o[parseInt(t[1].replace(/^[0]+/g, "")) - 1] + " " + t[0] : ""
}
function timeBeforeToClose(t, e, o, s) {
	var a = e;
	if (void 0 != o && "" != o) {
		if ("-1" == o)return $("#" + t).html(s.hours_tbtc_eoh_closed).addClass("closed"), "";
		if ("00:00" == o)return $("#" + t).html(s.hours_tbtc_eoh_opened).addClass("opened"), "";
		a = o
	}
	if (void 0 != a && "" != a) {
		var i = new Date, r = i.getFullYear() + "/" + ("00" + (i.getMonth() + 1)).slice(-2) + "/" + ("00" + i.getDate()).slice(-2), l = r + " " + a.substring(a.length - 5), n = new Date(l), d = n.getTime() - i.getTime();
		if (0 > d)return $("#" + t).html(s.hours_tbtc_closed).addClass("closed"), "";
		var _ = Math.floor(d / 3600 / 1e3);
		_ = "00" + _, d -= 3600 * _ * 1e3;
		var f = Math.floor(d / 60 / 1e3);
		return f = "00" + f, $("#" + t).html(s.hours_tbtc_opened + _.substring(_.length - 2) + s.hours_tbtc_hour + f.substring(f.length - 2)).addClass("opened").attr("data-translation", "hours_tbtc_opened"), ""
	}
	return $("#" + t).html(s.hours_tbtc_closed).addClass("closed"), ""
}
function parseCities(t, e) {
	var o = [];
	if (e)for (var s = 0; s < t[e].length; s++)o[s] = capitalizeLetter(t[e][s]); else for (var a = Object.keys(t), i = 0, r = 0; r < a.length; r++)for (var l = 0; l < t[a[r]].length; l++)o[i] = capitalizeLetter(t[a[r]][l]), i++;
	$('.lf_storeLocatorWidget input[type="text"]').autoComplete({minChars: 2, source: function (t, e) {
		t = t.toLowerCase();
		var s = o, a = [];
		for (r = 0; r < s.length; r++)~s[r].toLowerCase().indexOf(t) && a.push(s[r]);
		e(a)
	}})
}
function capitalizeLetter(t) {
	return t.replace(/\w\S*/g, function (t) {
		return t.charAt(0).toUpperCase() + t.substr(1).toLowerCase()
	})
}
function clickoncta(t) {
	return window.location.href = $(t).parent().find("a").prop("href"), !1
}
jQuery.support.cors = !0, Marker.prototype = new google.maps.OverlayView, Marker.prototype.onAdd = function () {
	var t = $("<span>", {id: this.id_, "class": "lf_storeLocatorWidget_marker", text: this.nb_});
	this.domobj_ = $(t).get(0);
	var e = this.getPanes();
	e.overlayLayer.appendChild(this.domobj_), this.draw()
}, Marker.prototype.draw = function () {
	var t = this.getProjection(), e = t.fromLatLngToDivPixel(this.latlng_);
	$(this.domobj_).css({left: e.x - .8 * $(this.domobj_).width() + "px", top: e.y - $(this.domobj_).height() + "px", position: "absolute"})
}, Marker.prototype.onRemove = function () {
	this.domobj_.parentNode.removeChild(this.domobj_), this.domobj_ = null
}, Marker.prototype.setCenter = function (t) {
	this.center_ = t
}, function (t, e, o) {
	function s(t) {
		for (var e = {x: t.offsetLeft, y: t.offsetTop}; t = t.offsetParent;)e.x += t.offsetLeft, e.y += t.offsetTop;
		return e
	}

	function a(t) {
		for (var e = 1; e < arguments.length; e++) {
			var s = arguments[e];
			for (var a in s)t[a] === o && (t[a] = s[a])
		}
		return t
	}

	function i(t, e) {
		for (var o in e)t.style[r(t, o) || o] = e[o];
		return t
	}

	function r(t, e) {
		var s, a, i = t.style;
		if (i[e] !== o)return e;
		for (e = e.charAt(0).toUpperCase() + e.slice(1), a = 0; a < f.length; a++)if (s = f[a] + e, i[s] !== o)return s
	}

	function l(t, e, o, s) {
		var a = ["opacity", e, ~~(100 * t), o, s].join("-"), i = .01 + o / s * 100, r = Math.max(1 - (1 - t) / e * (100 - i), t), l = _.substring(0, _.indexOf("Animation")).toLowerCase(), n = l && "-" + l + "-" || "";
		return p[a] || (c.insertRule("@" + n + "keyframes " + a + "{0%{opacity:" + r + "}" + i + "%{opacity:" + t + "}" + (i + .01) + "%{opacity:1}" + (i + e) % 100 + "%{opacity:" + t + "}100%{opacity:" + r + "}}", 0), p[a] = 1), a
	}

	function n(t, e, o) {
		return o && !o.parentNode && n(t, o), t.insertBefore(e, o || null), t
	}

	function d(t, o) {
		var s, a = e.createElement(t || "div");
		for (s in o)a[s] = o[s];
		return a
	}

	var _, f = ["webkit", "Moz", "ms", "O"], p = {}, c = function () {
		var t = d("style");
		return n(e.getElementsByTagName("head")[0], t), t.sheet || t.styleSheet
	}(), u = function m(t) {
		return this.spin ? void(this.opts = a(t || {}, m.defaults, h)) : new m(t)
	}, h = u.defaults = {lines: 12, length: 7, width: 5, radius: 10, color: "#000", speed: 1, trail: 100, opacity: .25, fps: 20}, g = u.prototype = {spin: function (t) {
		this.stop();
		var e, o, a = this, r = a.el = i(d(), {position: "relative"});
		if (t && (o = s(n(t, r, t.firstChild)), e = s(r), i(r, {left: (t.offsetWidth >> 1) - e.x + o.x + "px", top: (t.offsetHeight >> 1) - e.y + o.y + "px"})), r.setAttribute("aria-role", "progressbar"), a.lines(r, a.opts), !_) {
			var l = a.opts, f = 0, p = l.fps, c = p / l.speed, u = (1 - l.opacity) / (c * l.trail / 100), h = c / l.lines;
			!function g() {
				f++;
				for (var t = l.lines; t; t--) {
					var e = Math.max(1 - (f + t * h) % c * u, l.opacity);
					a.opacity(r, l.lines - t, e, l)
				}
				a.timeout = a.el && setTimeout(g, ~~(1e3 / p))
			}()
		}
		return a
	}, stop: function () {
		var t = this.el;
		return t && (clearTimeout(this.timeout), t.parentNode && t.parentNode.removeChild(t), this.el = o), this
	}};
	g.lines = function (t, e) {
		function o(t, o) {
			return i(d(), {position: "absolute", width: e.length + e.width + "px", height: e.width + "px", background: t, boxShadow: o, transformOrigin: "left", transform: "rotate(" + ~~(360 / e.lines * a) + "deg) translate(" + e.radius + "px,0)", borderRadius: (e.width >> 1) + "px"})
		}

		for (var s, a = 0; a < e.lines; a++)s = i(d(), {position: "absolute", top: 1 + ~(e.width / 2) + "px", transform: "translate3d(0,0,0)", opacity: e.opacity, animation: _ && l(e.opacity, e.trail, a, e.lines) + " " + 1 / e.speed + "s linear infinite"}), e.shadow && n(s, i(o("#000", "0 0 4px #000"), {top: "2px"})), n(t, n(s, o(e.color, "0 0 1px rgba(0,0,0,.1)")));
		return t
	}, g.opacity = function (t, e, o) {
		e < t.childNodes.length && (t.childNodes[e].style.opacity = o)
	}, function () {
		var t, e = i(d("group"), {behavior: "url(#default#VML)"});
		if (!r(e, "transform") && e.adj) {
			for (t = 4; t--;)c.addRule(["group", "roundrect", "fill", "stroke"][t], "behavior:url(#default#VML)");
			g.lines = function (t, e) {
				function o(t, o, a) {
					n(_, n(i(s(), {rotation: 360 / e.lines * t + "deg", left: ~~o}), n(i(d("roundrect", {arcsize: 1}), {width: r, height: e.width, left: e.radius, top: -e.width >> 1, filter: a}), d("fill", {color: e.color, opacity: e.opacity}), d("stroke", {opacity: 0}))))
				}

				function s() {
					return i(d("group", {coordsize: l + " " + l, coordorigin: -r + " " + -r}), {width: l, height: l})
				}

				var a, r = e.length + e.width, l = 2 * r, _ = s(), f = ~(e.length + e.radius + e.width) + "px";
				if (e.shadow)for (a = 1; a <= e.lines; a++)o(a, -2, "progid:DXImageTransform.Microsoft.Blur(pixelradius=2,makeshadow=1,shadowopacity=.3)");
				for (a = 1; a <= e.lines; a++)o(a);
				return n(i(t, {margin: f + " 0 0 " + f, zoom: 1}), _)
			}, g.opacity = function (t, e, o, s) {
				var a = t.firstChild;
				s = s.shadow && s.lines || 0, a && e + s < a.childNodes.length && (a = a.childNodes[e + s], a = a && a.firstChild, a = a && a.firstChild, a && (a.opacity = o))
			}
		} else _ = r(e, "animation")
	}(), t.Spinner = u
}(window, document), function (t) {
	t.fn.spin = function (e, o) {
		var s = {tiny: {lines: 8, length: 2, width: 2, radius: 3}, small: {lines: 8, length: 4, width: 3, radius: 5}, large: {lines: 10, length: 8, width: 4, radius: 8}, lf_style: {lines: 12, lenght: 7, width: 3, radius: 8, trail: 20, speed: 1.3}};
		if (Spinner)return this.each(function () {
			var a = t(this), i = a.data();
			i.spinner && (i.spinner.stop(), delete i.spinner), e !== !1 && ("string" == typeof e && (e = e in s ? s[e] : {}, o && (e.color = o)), i.spinner = new Spinner(t.extend({color: a.css("color")}, e)).spin(this))
		});
		throw"Spinner class not available."
	}
}(jQuery), function (t) {
	var e = function (o) {
		function s(e) {
			t.each(e, function (t, e) {
				void 0 == a.opts.lang_override[t] ? a.lan[t] = e : a.lan[t] = a.opts.lang_override[t]
			}), 1 == a.formWdgt.find("label a").length ? a.formWdgt.find("label a").text(a.lan.formLabelFind).attr("data-translation", "formLabelFind") : a.formWdgt.find("label").text(a.lan.formLabelFind).attr("data-translation", "formLabelFind"), a.formWdgt.find(".lf_geoloc").text(a.lan.formLabelGeoloc).attr("data-translation", "formLabelGeoloc"), a.formWdgt_Search.attr("placeholder", "Enter city, state, or zipcode").attr("data-translation", "formPlaceHolder"), a.formWdgt.find(".lf_storeLocatorWidget .submit").text(a.lan.formLabelSearch).attr("data-translation", "formLabelSearch"), a.formWdgt.find(".lf_results-or").text(a.lan.formLabelOr).attr("data-translation", "formLabelOr"), t(".lf_storeLocatorWidget").find(".lf_error").text(a.lan.errorGeoLoc), t(".lf_storeLocatorWidget").find(".lf_storeLocatorWidget_viewMore").text("See More Stores").attr("data-translation", "textViewMore"), t(".lf_storeLocatorWidget").find(".maxmind").html(a.lan.formLabelGeolocIE8).attr("data-translation", "formLabelGeolocIE8")
		}

		var a = this;
		a.lan = new Object;
		var i = new Date, r = t.ajax({url: a.params.languri + o + ".json?_=" + i.getTime(), dataType: "jsonp", jsonp: "callback", jsonpCallback: "initLanguage", success: function (t) {
			s(t)
		}});
		r.error(function (t, s, i) {
			"en" != o ? e.apply(a, ["en"]) : window.console && window.console.log && console.warn("Can't load language file " + t.status + " " + o + " : " + s + " : " + i)
		})
	}, o = function (e, o, i) {
		var r = this, l = [];
		if (t(e).find("ul.lf_results").empty(), this.opts.display_map) {
			if (void 0 == this.map) {
				var n = {disableDefaultUI: !1, panControl: !1, zoomControl: !0, mapTypeControl: !1, scaleControl: !1, streetViewControl: !1, overviewMapControl: !1, center: new google.maps.LatLng(0, 0), mapTypeId: google.maps.MapTypeId.ROADMAP};
				this.map = new google.maps.Map(t(o).get(0), n)
			} else {
				for (var d = 0; d < this.map_markers.length; d++)this.map_markers[d].setVisible(!1), this.map_markers[d].setMap(null), this.map_markers[d].isAdded = !1, this.map_markers[d] = null;
				this.map_markers.length = new Array, this.map.fitBounds(new google.maps.LatLngBounds)
			}
			"undefined" != typeof geo_marker && geo_marker.setMap(null), 0 != user_latitude && 0 != user_longitude && (geo_marker = new google.maps.Marker({map: this.map, icon: "http://maps.gstatic.com/mapfiles/ridefinder-images/mm_20_red.png", position: new google.maps.LatLng(user_latitude, user_longitude)}), user_latitude = 0, user_longitude = 0)
		} else t(".lf_storeLocatorWidget_ggmaps").hide();
		var _ = new Array, f = 0, p = "," + this.opts.pos_infos.join() + ",", c = !0;
		if (-1 != p.toLowerCase().indexOf("," + r.opts.filter_name.toLowerCase() + ","))var c = !1;
		for (var u = r.opts.filter_values, h = 0, g = "", d = 0; d < r.opts.max_results && void 0 != i.results[d]; d++) {
			if (void 0 == r.opts.filter_values || "" == r.opts.filter_values || void 0 == r.opts.filter_name || "" == r.opts.filter_name)var m = !0, c = !1; else {
				var m = !0;
				if (!c) {
					if ("##" != u.substring(0, 2) && (u = "##" + u + "##"), -1 != u.indexOf("##" + i.results[d][r.opts.filter_name] + "##")) {
						h++;
						var m = !1
					}
					r.opts.filter_hide || (m = !m)
				}
			}
			if (r.opts.filter_all || m) {
				var v = new google.maps.LatLng(i.results[d].latitude, i.results[d].longitude);
				if (_.push(v), this.opts.display_map) {
					new Marker(this.map, f + 1, v, "MapMarker_" + i.results[d].id);
					if (this.opts.display_infowindow) {
						var w = new google.maps.InfoWindow({content: ""});
						if (infowindow_content = '<div class="lf_storeLocatorWidget_infowindow">', void 0 != this.opts.fo_uri) {
							var y = "";
							i.results[d].has_page && !this.opts.remove_url ? (this.opts.contact_form && i.results[d].use_form && (y = "&action=lf_contactform"), infowindow_content += '<a class="lf_results_address_a" href="' + this.opts.fo_uri + i.results[d].slug + "?utm_source=widget&utm_medium=desktop&utm_campaign=" + window.location.host + y + '"><span class="lf_results_address_a_span" >' + i.results[d].name + "</span></a><br>") : infowindow_content += '<span class="lf_results_address_a_span" >' + i.results[d].name + "</span><br>"
						} else infowindow_content += '<span class="lf_results_address_a_span" >' + i.results[d].name + "</span><br>";
						if (void 0 != i.results[d].address1 && null != i.results[d].address1 && "" != i.results[d].address1 && (infowindow_content += '<div class="lf_storeLocatorWidget_address_address1" data-translation="address1">' + i.results[d].address1 + "</div>"), void 0 != i.results[d].address2 && null != i.results[d].address2 && "" != i.results[d].address2 && (infowindow_content += '<div class="lf_storeLocatorWidget_address_address2" data-translation="address2">' + i.results[d].address2 + "</div>"), -1 != jQuery.inArray("postal_code", this.opts.pos_infos) && (infowindow_content += '<div class="lf_storeLocatorWidget_address_zipcode_city"><span class="lf_storeLocatorWidget_address_zipcode">' + i.results[d].postal_code + ' </span>- <span class="lf_storeLocatorWidget_address_city">' + i.results[d].city + "</span></div>"), this.opts.display_phone && void 0 != i.results[d].phone && null != i.results[d].phone && "" != i.results[d].phone && (infowindow_content += '<div class="lf_storeLocatorWidget_address_phone" data-translation="phone">' + this.lan.tel + i.results[d].phone + "</div>"), this.opts.display_fax && void 0 != i.results[d].fax && null != i.results[d].fax && "" != i.results[d].fax && (infowindow_content += '<div class="lf_storeLocatorWidget_address_fax" data-translation="fax">' + this.lan.fax + i.results[d].fax + "</div>"), "undefined" != typeof r.opts.custom_values && "" != r.opts.custom_values && "string" == typeof r.opts.custom_values && -1 == r.opts.custom_values.indexOf("##")) {
							var b = r.opts.custom_values;
							-1 != b.indexOf("{") && (b = b.replace(/{{pos.id}}/g, encodeURIComponent(i.results[d].id)), b = b.replace(/{{pos.custom_id}}/g, encodeURIComponent(i.results[d].custom_id)), b = b.replace(/{{pos.inventory_service_id}}/g, encodeURIComponent(i.results[d].inventory_service_id))), b = b.replace(/script/g, ""), infowindow_content += b
						}
						infowindow_content += "</div>", l[d] = infowindow_content;
						var L = new google.maps.Marker({position: {lat: parseFloat(i.results[d].latitude), lng: parseFloat(i.results[d].longitude)}, map: this.map, icon: this.opts.overlay_icon, label: "."});
						L.addListener("click", function (t, e) {
							return function () {
								w.setContent(l[e]), w.open(this.map, t)
							}
						}(L, d))
					}
				}
				var x = "";
				m || (x = "inactif"), "undefined" != typeof i.results[d].point_of_sale_type && "undefined" != typeof i.results[d].point_of_sale_type.label && g != i.results[d].point_of_sale_type.label && (g = i.results[d].point_of_sale_type.label, t("<li class='pos_type'>" + g + "</li>").appendTo(t(e).find("ul.lf_results")));
				var k = t("<li inventory_service_id='" + i.results[d].inventory_service_id + "' custom_id='" + i.results[d].custom_id + "' id='" + i.results[d].id + "' class='pos " + x + "'>").appendTo(t(e).find("ul.lf_results"));
				t("<span>", {"class": "lf_storeLocatorWidget_marker", text: f + 1}).appendTo(k);
				a.apply(r, [k, i.results[d]]), t("<div>", {"class": "clr"}).appendTo(k), f++
			}
		}
		if (this.opts.display_map) {
			var C = new google.maps.OverlayView;
			C.draw = function () {
				this.getPanes().markerLayer.id = "lf_storeLocatorWidget_overlay"
			}, C.setMap(this.map)
		}
		c && t("body").find(".lf_storeLocatorWidget_poslist ul.lf_results li.pos").each(function () {
			var e = t(this).attr("id"), o = t(this);
			t.ajax({url: r.opts.api_uri + "point_of_sales/" + e + ".json", data: "oauth_token=" + r.opts.api_key, cache: !1, type: "GET", dataType: "jsonp", statusCode: {404: function () {
				lf_error.text(r.lan.errorXHR).slideDown()
			}}}).fail(function (t, e) {
				return lf_error.text(r.lan.errorXHR + " : " + e).slideDown(), !0
			}).done(function (a) {
				if (void 0 != a.client.smart_tags)for (var i = 0; i <= a.client.smart_tags.length; i++)if (a.client.smart_tags[i].name.toLowerCase() == r.opts.filter_name.toLowerCase()) {
					for (var l = a.client.smart_tags[i].id, n = 0; n <= a.smart_tag_values.length; n++)if (void 0 != a.smart_tag_values[n]) {
						if (a.smart_tag_values[n].smart_tag_id == l) {
							"##" != u.substring(0, 2) && (u = "##" + u + "##"), -1 != u.indexOf("##" + a.smart_tag_values[n].text + "##") ? r.opts.filter_all ? r.opts.filter_hide && o.addClass("inactif") : r.opts.filter_hide && (o.remove(), t("#MapMarker_" + e).remove(), s(r.opts.filter_all)) : r.opts.filter_all ? r.opts.filter_hide || o.addClass("inactif") : r.opts.filter_hide || (o.remove(), t("#MapMarker_" + e).remove(), s(r.opts.filter_all));
							break
						}
					} else r.opts.filter_all ? r.opts.filter_hide || o.addClass("inactif") : r.opts.filter_hide || (o.remove(), t("#MapMarker_" + e).remove(), s(r.opts.filter_all));
					break
				}
			})
		}), void 0 != r.opts.filter_values && "" != r.opts.filter_values && void 0 != r.opts.filter_name && "" != r.opts.filter_name && (r.opts.filter_hide || 0 != h || c || t("body").find(".lf_storeLocatorWidget .lf_error").last().text(r.lan.errorNoResult).slideDown()), "undefined" != typeof r.opts.custom_values && "" != r.opts.custom_values && "string" == typeof r.opts.custom_values && (-1 == r.opts.custom_values.indexOf("##") ? t("body").find(".lf_storeLocatorWidget_infowindow").each(function () {
			var e = r.opts.custom_values;
			-1 != e.indexOf("{") && (e = e.replace(/{{pos.id}}/g, encodeURIComponent(t(this).attr("id"))), e = e.replace(/{{pos.custom_id}}/g, encodeURIComponent(t(this).attr("custom_id"))), e = e.replace(/{{pos.inventory_service_id}}/g, encodeURIComponent(t(this).attr("inventory_service_id")))), e = e.replace(/script/g, ""), t(this).append(e)
		}) : t("body").find(".lf_storeLocatorWidget_infowindow").each(function () {
			if ("string" == typeof r.opts.filter_name && "" != r.opts.filter_name && "string" == typeof r.opts.filter_values && "" != r.opts.filter_values)for (var e = r.opts.custom_values.split("##"), o = r.opts.filter_values.split("##"), s = 0; s < e.length; s++)if (t(this).attr(r.opts.filter_name) == o[s]) {
				b = e[s], -1 != b.indexOf("{{") && (b = b.replace(/{{pos.id}}/g, encodeURIComponent(t(this).attr("id"))), b = b.replace(/{{pos.custom_id}}/g, encodeURIComponent(t(this).attr("custom_id"))), b = b.replace(/{{pos.inventory_service_id}}/g, encodeURIComponent(t(this).attr("inventory_service_id")))), b = b.replace(/script/g, ""), t(this).append(b);
				break
			}
		}));
		for (var T = new google.maps.LatLngBounds(0, 0), d = 0, W = _.length; W > d; d++)T.extend(_[d]);
		this.opts.display_map && setTimeout(function () {
			r.map.fitBounds(T)
		}, 1)
	}, s = function (e) {
		var o = 1;
		e || t("body").find(".lf_storeLocatorWidget_poslist ul.lf_results li.pos").each(function () {
			t(this).children(".lf_storeLocatorWidget_marker").html(o), t("#MapMarker_" + t(this).attr("id")).html(o), o++
		})
	}, a = function (e, o) {
		for (var s = "", a = "", i = new Date, r = i.getFullYear() + "-" + ("00" + (i.getMonth() + 1)).slice(-2) + "-" + ("00" + i.getDate()).slice(-2), n = i.getDay(), _ = 0; _ <= this.opts.pos_infos.length; _++)switch (this.opts.pos_infos[_]) {
			case"name":
				if (void 0 != this.opts.fo_uri) {
					var f = "";
					o.has_page && !this.opts.remove_url ? (this.opts.contact_form && o.use_form && (f = "&action=lf_contactform"), s += '<a class="lf_results_address_a" href="' + this.opts.fo_uri + o.slug + "?utm_source=widget&utm_medium=desktop&utm_campaign=" + window.location.host + f + '"><span class="lf_results_address_a_span" >' + o.name + "</span></a>") : s += '<span class="lf_results_address_a_span" >' + o.name + "</span>"
				} else s += '<span class="lf_results_address_a_span" >' + o.name + "</span>";
				break;
			case"postal_code":
				break;
			case"city":
				-1 != jQuery.inArray("postal_code", this.opts.pos_infos) && (s += '<div class="lf_storeLocatorWidget_address_zipcode_city"><span class="lf_storeLocatorWidget_address_zipcode">' + o[this.opts.pos_infos[jQuery.inArray("postal_code", this.opts.pos_infos)]] + ' </span>- <span class="lf_storeLocatorWidget_address_city">' + o[this.opts.pos_infos[_]] + "</span></div>");
				break;
			case"id":
			case"custom_id":
			case"inventory_service_id":
				break;
			case"distance":
				"f" == o.exact && (s += '<span class="proximity">' + Math.round(100 * parseFloat(o[this.opts.pos_infos[_]]) * this.opts.distance_ratio) / 100 + " " + this.opts.distance_unit + "</span>"), this.opts.contact_form && o.use_form && (s += '<div class="lf_storeLocatorWidget_use_form" onclick="clickoncta(this)" data-translation="contact">' + this.lan.contact + "</div>");
				break;
			case"opening_hours":
				if (!this.opts.display_hours)continue;
				var p = "", c = new Array;
				if ("object" == typeof o[this.opts.pos_infos[_]])for (d in o[this.opts.pos_infos[_]]) {
					p = "default";
					var u = o[this.opts.pos_infos[_]][d];
					"undefined" != typeof u.opening_hour_label && "undefined" != typeof u.opening_hour_label.label && "null" != u.opening_hour_label.label && (p = u.opening_hour_label.label), "undefined" == typeof c[p] && (c[p] = new Array), "undefined" == typeof c[p][u.day_of_week] && (c[p][u.day_of_week] = new Array, c[p][u.day_of_week] = "");
					var h = u.start.split("T");
					h = h.length > 1 ? h[1].replace(":00Z", "") : "";
					var g = u.end.split("T");
					g = g.length > 1 ? g[1].replace(":00Z", "") : "", c[p][u.day_of_week] += '<li class="lf_normalopeninghours_oh" data-translation="hours_start,hours_end">' + this.lan.hours_start + h + this.lan.hours_end + g + "</li>", n.toString() == u.day_of_week && (c[p].tbtc = g)
				}
				var m = this.lan.hours_day_label.split(","), v = 1;
				for (l in c) {
					c[l].tbtc_index = "tbtc" + o.id + "_" + v, s += '<div class="lf_openinghoursdays"><h4 class="lf_openinghoursdays_title">' + l + '</h4><span class="tbtc" id="' + c[l].tbtc_index + '"></span><ul class="lf_days">';
					for (var w = 1; 7 >= w; w++) {
						var y = "";
						ldow = w, 7 == ldow && (ldow = 0), ldow.toString() == n.toString() && (y = "today"), s += '<li class="day ' + y + '"><strong class="week_day" data-translation="hours_day_label">' + m[w - 1] + "</strong>", s += "undefined" == typeof c[l][ldow] ? '<ul class="lf_normalopeninghours"><li class="lf_normalopeninghours_closed" data-translation="hours_closed" >' + this.lan.hours_closed + "</li></ul>" : '<ul class="lf_normalopeninghours">' + c[l][ldow] + "</ul>", s += "</li>"
					}
					s += "</ul></div>", v++
				}
				break;
			case"specific_opening_hours":
				if (this.opts.display_hours && "object" == typeof o[this.opts.pos_infos[_]] && o[this.opts.pos_infos[_]].length > 0) {
					sHTML_EOH = "";
					for (d in o[this.opts.pos_infos[_]])if ("undefined" != typeof o[this.opts.pos_infos[_]][d]) {
						var u = o[this.opts.pos_infos[_]][d], b = new Date(u.starts_at);
						if (null == u.ends_at)var L = new Date(u.starts_at); else var L = new Date(u.ends_at);
						var x = new Date(r);
						if (b.getTime() <= x.getTime() && L.getTime() >= x.getTime()) {
							if (0 == u.closed)var k = this.lan.hours_eoh_opened; else var k = this.lan.hours_eoh_closed;
							if (null == u.ends_at)var C = lf_displayDate(u.starts_at, this.lan.hours_eoh_month); else var C = this.lan.hours_eoh_start + lf_displayDate(u.starts_at, this.lan.hours_eoh_month) + this.lan.hours_eoh_end + lf_displayDate(u.ends_at, this.lan.hours_eoh_month);
							var T = "";
							null != u.time_starts_at1 && (T += this.lan.hours_start + lf_fillHour(new Date(u.time_starts_at1).getUTCHours()) + ":" + lf_fillHour(new Date(u.time_starts_at1).getUTCMinutes())), null != u.time_ends_at1 && (T += this.lan.hours_end + lf_fillHour(new Date(u.time_ends_at1).getUTCHours()) + ":" + lf_fillHour(new Date(u.time_ends_at1).getUTCMinutes())), null != u.time_starts_at2 && (T += this.lan.hours_eoh_and + this.lan.hours_start + lf_fillHour(new Date(u.time_starts_at2).getUTCHours()) + ":" + lf_fillHour(new Date(u.time_starts_at2).getUTCMinutes())), null != u.time_ends_at2 && (T += this.lan.hours_end + lf_fillHour(new Date(u.time_ends_at2).getUTCHours()) + ":" + lf_fillHour(new Date(u.time_ends_at2).getUTCMinutes()));
							var W = "";
							if ("null" != u.description && (W = '<p class="lf_eoh_description">' + u.description + "</p>"), sHTML_EOH += '<div class="lf_eoh_wrapper">', sHTML_EOH += '<strong class="lf_eoh_detail" data-translation="hours_eoh_opened,hours_eoh_closed,hours_eoh_start,hours_eoh_end,hours_eoh_and">' + C + " " + T + " - " + k + "</strong>", sHTML_EOH += W, sHTML_EOH += "</div>", null == u.ends_at)r == u.starts_at && (1 == u.closed ? a = "-1" : (a = "00:00", null != u.time_ends_at1 && (a = lf_fillHour(new Date(u.time_ends_at1).getUTCHours()) + ":" + lf_fillHour(new Date(u.time_ends_at1).getUTCMinutes())), null != u.time_ends_at2 && (a = lf_fillHour(new Date(u.time_ends_at2).getUTCHours()) + ":" + lf_fillHour(new Date(u.time_ends_at2).getUTCMinutes())))); else {
								var b = new Date(u.starts_at), L = new Date(u.ends_at), x = new Date(r);
								b.getTime() <= x.getTime() && L.getTime() >= x.getTime() && (1 == u.closed ? a = "-1" : (a = "00:00", null != u.time_ends_at1 && (a = lf_fillHour(new Date(u.time_ends_at1).getUTCHours()) + ":" + lf_fillHour(new Date(u.time_ends_at1).getUTCMinutes())), null != u.time_ends_at2 && (a = lf_fillHour(new Date(u.time_ends_at2).getUTCHours()) + ":" + lf_fillHour(new Date(u.time_ends_at2).getUTCMinutes()))))
							}
						}
					}
					"" != sHTML_EOH && (s += '<div class="lf_distantexceptionalopeninghours"><h4 class="lf_distantexceptionalopeninghours_title" data-translation="hours_eoh_label">' + this.lan.hours_eoh_label + "</h4>", s += sHTML_EOH, s += "</div>")
				}
				break;
			case"fax":
				this.opts.display_fax && void 0 != o[this.opts.pos_infos[_]] && null != o[this.opts.pos_infos[_]] && "" != o[this.opts.pos_infos[_]] && (s += '<div class="lf_storeLocatorWidget_address_' + this.opts.pos_infos[_] + '" data-translation="fax">' + this.lan.fax + o[this.opts.pos_infos[_]] + "</div>");
				break;
			case"phone":
				this.opts.display_phone && void 0 != o[this.opts.pos_infos[_]] && null != o[this.opts.pos_infos[_]] && "" != o[this.opts.pos_infos[_]] && (s += '<div class="lf_storeLocatorWidget_address_' + this.opts.pos_infos[_] + '" data-translation="tel">' + this.lan.tel + o[this.opts.pos_infos[_]] + "</div>");
				break;
			default:
				if (void 0 == o[this.opts.pos_infos[_]] || null == o[this.opts.pos_infos[_]] || "" == o[this.opts.pos_infos[_]])continue;
				s += '<div class="lf_storeLocatorWidget_address_' + this.opts.pos_infos[_] + '">' + o[this.opts.pos_infos[_]] + "</div>"
		}
		if (t('<address class="lf_results_address">').html(s).appendTo(e), this.opts.display_hours && "undefined" != typeof c)for (l in c)timeBeforeToClose(c[l].tbtc_index, c[l].tbtc, a, this.lan)
	};
	t.fn.lf_storelocator = function (s) {
		var a = {lang: "en", allow_geolocation: !0, lang_override: {}, max_results: 3, pos_infos: ["id", "custom_id", "name", "address1", "address2", "postal_code", "city", "phone", "fax", "inventory_service_id", "distance", "specific_opening_hours", "opening_hours"], api_uri: "https://api.leadformance.com/", filter_values: "", filter_name: "", filter_hide: !1, filter_all: !1, position_latitude: 0, position_longitude: 0, ga_tracking: !0, display_hours: !1, custom_values: "", contact_form: !1, extra_search: "", display_fax: !1, display_phone: !0, use_autocomplete: !0, remove_url: !1, distance_unit: "km", distance_ratio: 1, display_map: !0, display_infowindow: !1, overlay_icon: "img/marker.png"};
		return void 0 == s.api_key ? void(window.console && window.console.log && console.warn("API Key is missing")) : this.length ? this.each(function () {
			this.opts = t.extend(a, s), this.formWdgt = t(this).find("form"), this.formWdgt_Search = this.formWdgt.find('input[name="query"]'), this.params = {languri: "https://widgets.leadformance.com/storeLocator/v2/lang/"}, void 0 != this.opts.fo_uri && (0 == t(".lf_storeLocatorWidget form fieldset label a").length ? t(".lf_storeLocatorWidget form fieldset label").html("<a class='lf_storeLocatorWidget_search_label_a' href='" + this.opts.fo_uri + "'>" + t(".lf_storeLocatorWidget form fieldset label").text() + "</a>") : t(".lf_storeLocatorWidget form fieldset label a").attr("href") != this.opts.fo_uri && t(".lf_storeLocatorWidget form fieldset label").html("<a class='lf_storeLocatorWidget_search_label_a' href='" + this.opts.fo_uri + "'>" + t(".lf_storeLocatorWidget form fieldset label").text() + "</a>")), this.geoloc = {latitude: 0, longitude: 0}, this.map, this.map_markers = new Array;
			var i = !0, r = this, l = t("<p>", {"class": "lf_error"}).insertBefore(this.formWdgt), n = t("<p>", {"class": "lf_storeLocatorWidget_submit_loader"}).insertBefore(l), d = t("<div>", {"class": "lf_storeLocatorWidget_results"}).insertBefore(n).css({display: "none"}), _ = t("<div>", {"class": "lf_storeLocatorWidget_ggmaps"});
			d.append(_);
			var f = t("<div>", {"class": "lf_storeLocatorWidget_poslist"}).insertAfter(_);
			if (f.append(t('<ul class="lf_results">')), this.link_more, void 0 != r.opts.fo_uri && (this.link_more = t("<a />", {"class": "lf_storeLocatorWidget_viewMore", href: r.opts.fo_uri + "search", text: " "}), f.append(this.link_more)), 0 != r.opts.position_latitude && 0 != r.opts.position_longitude && (r.geoloc.latitude = r.opts.position_latitude, r.geoloc.longitude = r.opts.position_longitude), this.opts.allow_geolocation ? (push_geoloc = t("<a>", {"class": "lf_geoloc", href: "#"}).appendTo(this.formWdgt.find("fieldset")), r.formWdgt.find(".lf_geoloc").click(function (t) {
				t.preventDefault && t.preventDefault(), navigator.geolocation.getCurrentPosition(function (t) {
					r.geoloc.latitude = t.coords.latitude, r.geoloc.longitude = t.coords.longitude, r.formWdgt.trigger("submit")
				}, function (t) {
					l.slideDown()
				})
			})) : t(".lf_results-or").hide(), this.lan = t.extend({}, this.opts.lang_override), e.apply(this, [this.opts.lang]), this.formWdgt.submit(function (e) {
				if (t("body").find(".lf_storeLocatorWidget_marker").remove(), t.fn.lf_storelocator.launch && (r.opts.filter_values = t.fn.lf_storelocator.filter_values, r.opts.filter_name = t.fn.lf_storelocator.filter_name, r.opts.filter_hide = t.fn.lf_storelocator.filter_hide, r.opts.filter_all = t.fn.lf_storelocator.filter_all, 0 != t.fn.lf_storelocator.latitude && 0 != t.fn.lf_storelocator.longitude && (r.geoloc.latitude = t.fn.lf_storelocator.latitude, r.geoloc.longitude = t.fn.lf_storelocator.longitude), r.opts.custom_values = t.fn.lf_storelocator.custom_values, r.opts.extra_search = t.fn.lf_storelocator.extra_search), e.preventDefault(), i) {
					i = !1, l.hide(), d.slideUp();
					var s = jQuery.trim(r.formWdgt_Search.val());
					jQuery('.stores-near').remove();
					var topContent = t("<div>", {"class": "stores-near"}).html("Stores near " + jQuery.trim(r.formWdgt_Search.val())).insertBefore(f);
					"" != s || 0 != r.geoloc.latitude && 0 != r.geoloc.longitude ? (l.hide(), searchURI = "", searchType = "", 0 != r.geoloc.latitude && 0 != r.geoloc.longitude ? (s = "lat=" + r.geoloc.latitude + "&lng=" + r.geoloc.longitude, searchURI = "search/geo.json", searchType = "geoloc") : (s = "query=" + encodeURIComponent(s), searchURI = "search.json", searchType = "keyword"), "string" == typeof s ? (r.opts.ga_tracking && lf_loadGAtracking(searchType, s), "" != r.opts.extra_search && (s = s + "&" + encodeURIComponent(r.opts.extra_search).replace(/%3D/g, "=").replace(/%26/g, "&")), n.animate({height: 75}, 100, function () {
						n.spin({lines: 12, lenght: 7, width: 3, radius: 8, trail: 20, speed: 1.3});
						t.ajax({url: r.opts.api_uri + searchURI, data: s + "&oauth_token=" + r.opts.api_key + "&locale=" + r.opts.lang, cache: !1, type: "GET", dataType: "jsonp", statusCode: {404: function () {
							l.text(r.lan.errorXHR).slideDown()
						}}}).always(function () {
							n.spin(!1), n.animate({height: 0}, 100), i = !0, user_latitude = r.geoloc.latitude, user_longitude = r.geoloc.longitude, r.geoloc.latitude = 0, r.geoloc.longitude = 0
						}).fail(function (e, o) {
							l.text(r.lan.errorXHR + " : " + o).slideDown(), t("body").trigger("lf_widget_end_error")
						}).done(function (e) {
							e.error ? (l.text(r.lan.errorXHR).slideDown(), t("body").trigger("lf_widget_end_error")) : e.results && 0 != e.results.length ? (o.apply(r, [f, _, e]), void 0 != r.link_more && ("geoloc" == searchType ? r.link_more.attr("href", r.opts.fo_uri + "?utm_source=widget&utm_medium=desktop&utm_campaign=" + window.location.host) : r.link_more.attr("href", r.opts.fo_uri + "search?" + s + "&utm_source=widget&utm_medium=desktop&utm_campaign=" + window.location.host)), t(d).slideDown(), t("body").trigger("lf_widget_end_with_results")) : (l.text(r.lan.errorNoResult).slideDown(), t(d).slideUp(), t("body").trigger("lf_widget_end_no_result")), i = !0
						})
					})) : i = !0) : l.text(r.lan.errorNoData).slideDown(function () {
						i = !0
					})
				}
				t.fn.lf_storelocator.launch = void 0
			}), r.opts.use_autocomplete) {
				var p = document.createElement("script");
				p.type = "text/javascript", p.src = "https://widgets.leadformance.com/storeLocator/v2/jquery.auto-complete.min.js", document.body.appendChild(p), t.ajax({url: this.opts.fo_uri + "/cities.json?callback=widget", type: "GET", dataType: "jsonp", success: function (t) {
					parseCities(t)
				}})
			}
		}) : void 0
	}, t.fn.lf_storelocator.launch_lf_storelocator = function (e, o, s, a, i, r, l, n, d) {
		void 0 != e && (t.fn.lf_storelocator.filter_values = e), void 0 != o && (t.fn.lf_storelocator.filter_name = o), void 0 != s && (t.fn.lf_storelocator.filter_hide = s), void 0 != a && (t.fn.lf_storelocator.filter_all = a), void 0 != i && (t.fn.lf_storelocator.custom_values = i), void 0 != r && void 0 != l ? (t.fn.lf_storelocator.latitude = r, t.fn.lf_storelocator.longitude = l) : (t.fn.lf_storelocator.latitude = 0, t.fn.lf_storelocator.longitude = 0), t.fn.lf_storelocator.launch = !0, void 0 != d && (t.fn.lf_storelocator.extra_search = d), void 0 != n && n && t(".lf_storeLocatorWidget form").trigger("submit")
	}
}(jQuery);
var _gaq = _gaq || [], checkGA = !0, user_latitude = 0, user_longitude = 0, geo_marker;
