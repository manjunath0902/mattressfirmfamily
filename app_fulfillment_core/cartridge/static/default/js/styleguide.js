var deliveryPicker = {
	initDeliveryPicker: function () {
		$('.delivery-picker').slick({
			infinite: false,
			slidesToShow: 7,
			slidesToScroll: 7
		});
	},

	onClickOfRadioButtonDiv: function () {
		var $list = $('.delivery-picker');

		$list.find('.delivery-picker__date-time-slot').on('click', function (e) {
			var $self = $(e.target);
			$list.find('.delivery-picker__date-time-slot').removeClass('selected');
			$list.find('.delivery-picker__date').removeClass('selected');
			$self.find('input[type=radio]').prop('checked', 'checked');
			$self.closest('.delivery-picker__date').addClass('selected');
			$self.closest('.delivery-picker__date-time-slot').addClass('selected');
		})
	},

	setDefaultRadioButtonDiv: function() {
		var $list = $('.delivery-picker');

		if ($list.find('input[type=radio]:checked').length === 0) {
			$list.find('input[type=radio]:first').prop('checked', 'checked');
			$list.find('input[type=radio]:first').closest('.delivery-picker__date-time-slot').addClass('selected');
			$list.find('input[type=radio]:first').closest('.delivery-picker__date').addClass('selected');
		}

		this.onClickOfRadioButtonDiv();
	},

	init: function() {
		this.initDeliveryPicker();
		this.setDefaultRadioButtonDiv();
	}
};

var styleGuideDemos = {
	init: function () {
		// Invoke multiple tag picker
		var url = $('#create_event_url').val();
		$('.js-example-basic-multiple-container').show();
		$('.js-example-basic-multiple').select2();

		// date picker, that goes in dialog
		$( "#datepicker" ).datepicker({
			altField: "#alternate",
			altFormat: "DD, MM, d, yy"
		});

		$( "#dialog" ).dialog({
			autoOpen: false,
			width: 889,
			height: 588,
			modal: true
		});

		$( ".opener" ).on( "click", function(e) {
			e.preventDefault();
			$( "#dialog" ).dialog( "open" );
		});
	}
}


$(function () {
	// Scheduler / Delivery Picker Page
	if ($('.styleguide-delivery-picker-page').length > 0) {
		deliveryPicker.init();
	}

	if ($('.styleguide-main-page').length > 0) {
		styleGuideDemos.init();
	}

});
