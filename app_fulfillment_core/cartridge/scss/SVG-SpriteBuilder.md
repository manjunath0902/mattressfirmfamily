## SVG Sprite Builder Setup

1. **Install svg sprite package via CLI:**
	1. navigate to gulp_builder directory
	2. sudo npm install gulp-svg-sprite --save-dev

2. **Update gulpfile.js** - in the gulp_builder directory with the svg config version of the file.

3. **Create svg-icons folder** - In Eclipse create svg directory in the images directory named ‘svg-icons’

4. **svg-helper.isml** - In templates/util, place svg-helper.isml file.

5. **Modify your util/modules.isml file:**

   ``` isml
	<iscomment>
		Render the svg icon HTML based on the given name and other options
	</iscomment>

	<ismodule template="util/svghelper"
		name="svghelper"
		attribute="icon"
		attribute="hideUrl"
	/>
```

6. **Add Images.xml pipeline** - In the pipeline directory and create icons-symbols.isml file in templates/components - this file is the sprite file where the svg’s will be automatically built to.

7. **SVGContent.ds** - Add this file to scripts/utils 

8. **Create SCSS file** - To apply styles for the svg icons, create a dedicated .scss file (Example file: _iconography.scss), which extends the generated styles from _svg-icons.scss: ```@import "../compiled/svg-icons";```.

	Style classes generated are ```.icon.nameofsvg``` and include height, width, stroke, and stroke fill values. Include import path for this file in styles.scss. 

9. **Include in styles.scss** - ```@import "iconography";```

10. **Set up build properties** - Trigger an automatic rebuild after adding svg’s to the svg-icons directory by taking these steps:

	1. Right click on the gulp builder cartridge &gt; properties &gt; Builders &gt; Styles Builder &gt; Edit

	2. **Refresh Tab** &gt; Specify Resources &gt; navigate to the sprite file, icons-symbols.isml, and make sure this is **checked**.  Navigate to the compiled scss folder, and make sure the 'compiled' folder is **unchecked** (or you'll have an infinite build loop).

	3. **Build Options Tab** &gt; Specify Resources &gt; navigate to svg-icons directory in images and make sure this is **checked**.

<a name="steps-for-other-dev-team-members"></a>
## Steps for other dev team members:

1. **Install svg sprite package via CLI:**
	1. navigate to gulp_builder directory
	2. sudo npm install gulp-svg-sprite --save-dev

2. **Update gulpfile.js** (if necessary) - In the gulp_builder directory with the svg config version of the file.

3. **Set up build properties** - Trigger an automatic rebuild after adding svg’s to the svg-icons directory by taking these steps:

	1. Right click on the gulp builder cartridge &gt; properties &gt; Builders &gt; Styles Builder &gt; Edit

	2. **Refresh Tab** &gt; Specify Resources &gt; navigate to the sprite file, icons-symbols.isml (templates\default\components\icons-symbols.isml), and make sure this is **checked**.  Navigate to the compiled scss folder, and make sure the 'compiled' folder is **unchecked** (or you'll have an infinite build loop).

	3. **Build Options Tab** &gt; Specify Resources &gt; navigate to svg-icons directory in images and make sure this is **checked**.

## Adding SVGs
1. **Start with a clean .svg file** - If necessary, you can clean the svg by opening in a text editor, removing styles in between ```<svg>``` tag, and leaving just the ```<path d="...">```, removing any styles in the path tag. If it contains data element it is a bitmap svg, re-request true vector svg file from creative.

2. **Add the .svg file** -  in Eclipse, add the updated svg file to the images/svg-icons directory (this should automatically kick off a gulp build, if you've got the gulp builder configured correctly).

3. **Insert into ISML** - Insert the svghelper tag  where wish to place the icon in an isml file: ```<issvghelper icon=“nameofsvg” />```

4. **Style the icon** - in _iconography.scss, extend styles from svg-icons for each new svg:

	``` scss
	&.nameofsvg {
		@extend .svg-nameofsvg-dims;
	}