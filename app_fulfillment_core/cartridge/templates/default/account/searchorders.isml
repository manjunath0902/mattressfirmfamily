<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isdecorate template="account/pt_account">
<isinclude template="util/modules"/>
<isset name="orderResults" value="${pdict.orderResults}" scope="page" />

<isscript>
  var disabled_attributes = {
  disabled: 'disabled'
  };
  var autocomplete_attributes = {
  autocomplete: "off"
  };
  var placeholder_eventname_attributes = {
  placeholder: "Event Name",
  autocomplete: "off"
  };
  var placeholder_datefrom_attributes = {
  placeholder: "From:"
  };
  var placeholder_dateto_attributes = {
  placeholder: "To:"
  };
  var placeholder_ordernumber_attributes = {
  placeholder: "Order Number",
  autocomplete: "off"
  };
  var placeholder_storeid_attributes = {
  placeholder: "Store ID",
  autocomplete: "off"
  };
</isscript>

<div id="primary" class="primary-content search-pages search-orders">
  <div class="col-12 cta">
    <div class="row row-border-bottom">
      <div class="cta__heading col-12">
        <div class="cta__heading-icon-container">
          <img class="cta__heading-icon" src="${URLUtils.staticURL('/images/svg-icons/entity-icon.svg')}" alt="">
        </div>
        <div class="cta__heading-text">
          <p class="cta__heading-text-info">${Resource.msg('searchorders.orders','search',null)}</p>
          <h3 class="cta__heading-text-h3">${Resource.msg('searchorders.searchordersevents','search',null)}</h3>
        </div>
      </div><!-- /.cta__heading -->
    </div><!-- /.row -->
    <div class="row">
      <div class="col-12 search-pages__form">
        <div class="mb-2 mr-2">Search by:</div>
        <form action="${URLUtils.httpsContinue()}" method="post" id="search-orders">
        <div class="search-order-events-holder">
        <div class="form-horizontal">
          <div class="form-radio-holder">
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="search-orders-by" value="search-orders-event" id="search-orders-event" <isif condition="${empty(pdict.searchedBy) || pdict.searchedBy=='search-orders-event'}">checked="checked"</isif>/>
              <label class="form-check-label" for="search-orders-event">${Resource.msg('searchorders.eventname','search',null)}</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="search-orders-by" value="search-orders-date" id="search-orders-date"  <isif condition="${pdict.searchedBy=='search-orders-date'}">checked="checked"</isif>/>
              <label class="form-check-label" for="search-orders-date">${Resource.msg('searchorders.daterange','search',null)}</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="search-orders-by" value="search-orders-order-number" id="search-orders-order-number"  <isif condition="${pdict.searchedBy=='search-orders-order-number'}">checked="checked"</isif>/>
              <label class="form-check-label" for="search-orders-order-number">${Resource.msg('searchorders.ordernum','search',null)}</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="search-orders-by" value="search-orders-store-id" id="search-orders-store-id"  <isif condition="${pdict.searchedBy=='search-orders-store-id'}">checked="checked"</isif>/>
              <label class="form-check-label" for="search-orders-store-id">${Resource.msg('searchorders.storeid','search',null)}</label>
            </div>
          </div>
        </div>

          <div class="reveal-options">
            <div class="form-group search-orders-event-fields">
              <isinputfield formfield="${pdict.CurrentForms.searchorders.searchvalue.eventname}" type="input" attributes="${placeholder_eventname_attributes}"/>
            </div>

            <div class="form-group search-orders-date-fields">
              <label class="search-orders-date-fields__label" for="dwfrm_searchorders_searchvalue_datefrom">${Resource.msg('searchorders.daterangefrom','search',null)} </label>
              <input class="search-orders-date-fields__input-text search-orders-date-range" autocomplete="off" type="text" name="dwfrm_searchorders_searchvalue_datefrom" id="dwfrm_searchorders_searchvalue_datefrom" />

              <label class="search-orders-date-fields__label" for="dwfrm_searchorders_searchvalue_dateto">${Resource.msg('searchorders.daterangeto','search',null)} </label>
              <input class="search-orders-date-fields__input-text search-orders-date-range" autocomplete="off" type="text" name="dwfrm_searchorders_searchvalue_dateto" id="dwfrm_searchorders_searchvalue_dateto" />
            </div>

            <div class="form-group search-orders-order-number-fields">
              <isinputfield formfield="${pdict.CurrentForms.searchorders.searchvalue.ordernumber}" type="input" attributes="${placeholder_ordernumber_attributes}"/>
            </div>

            <div class="form-group search-orders-store-id-fields">
              <isinputfield formfield="${pdict.CurrentForms.searchorders.searchvalue.storeid}" type="input" attributes="${placeholder_storeid_attributes}"/>
            </div>
          </div><!-- /.reveal-options -->

          <div class="form-btn-group">
            <button class="btn btn-secondary" type="submit" value="${Resource.msg('global.search','locale',null)}" name="${pdict.CurrentForms.searchorders.search.htmlName}">
              ${Resource.msg('global.search','locale',null)}
            </button>
            <button class="btn btn-secondary" id="search-orders-cancel" type="reset" value="${Resource.msg('global.cancel','locale',null)}" name="${pdict.CurrentForms.searchorders.cancel.htmlName}">
              ${Resource.msg('global.cancel','locale',null)}
            </button>
            <input type="hidden" name="${pdict.CurrentForms.searchorders.secureKeyHtmlName}" value="${pdict.CurrentForms.searchorders.secureKeyValue}"/>
          </div>
          </div>
        </form>
      </div><!--/.search-pages__form -->
    </div><!-- /.row -->

   </div>
  </div>
  <div class="col-12 search-results-heading">
    <div class="row">
      <h3>${Resource.msg('searchorders.searchresults','search',null)}</h3>
    </div>
  </div>
  <isif condition="${!empty(orderResults)}">
  	<div class="col-12">
	  <div class="row">
	  	<isif condition="${orderResults.length > 1}">
	      <p class="search-pages__no-results">${Resource.msgf('searchorders.recordsfound','search',null,orderResults.length)}</p>
	    <iselse>
	      <p class="search-pages__no-results">${Resource.msgf('searchorders.recordfound','search',null,orderResults.length)}</p>
	    </isif>
	  </div>
	</div>

	  <div class="search-orders-results">
	    <div class="products__table-holder">
	      <table>
	        <tr>
	          <th>${Resource.msg('searchorders.orderstatus','search',null)} <span class="sort-indicator"></span></th>
	          <th>${Resource.msg('searchorders.ordernum','search',null)} <span class="sort-indicator"></span></th>
	          <th class="text-center">${Resource.msg('searchorders.eventname','search',null)} <span class="sort-indicator"></span></th>
	          <th class="no-sort">${Resource.msg('searchorders.storeid','search',null)}</th>
	          <th class="no-sort">${Resource.msg('searchorders.ordercreationdate','search',null)}</th>
	          <th class="no-sort">${Resource.msg('searchorders.deliverydatetime','search',null)}</th>
	          <th class="no-sort">${Resource.msg('searchorders.delivery','search',null)}</th>
	          <th>${Resource.msg('searchorders.name','search',null)} <span class="sort-indicator"></th>
	          <th class="text-center">${Resource.msg('searchorders.qty','search',null)}</th>
	          <th class="text-center">${Resource.msg('searchorders.ordertotal','search',null)}</th>
	          <th class="no-sort">${Resource.msg('searchorders.orderaction','search',null)}</th>
	        </tr>
	        <iscomment>TODO: Loop through search results</iscomment>
	        <isloop items="${orderResults}" var="order" status="orderloopstate">
	          <tr>
	          	<td>
					<isif condition="${order.exportStatus == dw.order.Order.EXPORT_STATUS_EXPORTED &&
									order.confirmationStatus == dw.order.Order.CONFIRMATION_STATUS_CONFIRMED &&
									order.status == dw.order.Order.ORDER_STATUS_COMPLETED &&
									order.shippingStatus == dw.order.Order.SHIPPING_STATUS_SHIPPED}">
						<span class="status-scheduled">Delivered</span>
					<iselseif condition="${((order.exportStatus == 1 || order.exportStatus == 2) && order.paymentStatus == 2)}">
						<span class="status-paid">Paid</span>
					<iselseif condition="${(order.exportStatus == 1 || order.exportStatus == 2)}">
						<span class="status-scheduled">Submitted</span>
					<iselseif condition="${(order.status == 0 || order.status == 3)  && (empty(order.deliveryDate) || order.deliveryDate == null) && empty(order.deliveryTime)}">
						<span class="status-new">New</span>
					<iselseif condition="${(order.paymentStatus != 2) && (order.status == 0 || order.status == 3) && (!empty(order.deliveryDate) && order.deliveryDate != null) && (!empty(order.deliveryTime) && order.deliveryTime != null )}">
						<isif condition="${order.deliveryType == 'Drop Ship'}">
							<span class="status-new">New</span>
						<iselse>
							<span class="status-scheduled">Scheduled</span>
						</isif>
					<iselseif condition="${(order.paymentStatus != 2) && (order.status == 0 || order.status == 3) && (!empty(order.deliveryDate) && order.deliveryDate != null) && order.shippingMethod == 'storePickup'}">
						<span class="status-scheduled">Scheduled Pickup</span>
					</isif>
				</td>
	            <td><isprint value="${order.orderNo}" /></td>
	            <td><isprint value="${order.eventName}" /></td>
	            <td><isprint value="${order.storeId}" /></td>
	            <td><isprint value="${new Date(order.creationDate).toLocaleDateString()}" /></td>
	            <iscomment>To-do: Handle the case for Drop Ship orders to showing date range as delivery message</iscomment>
	            <isif condition="${!empty(order.deliveryDate)}">
		            <isif condition="${order.deliveryType == 'Drop Ship'}">
		              <td>Delivered by vendor selected carrier</td>
		            <iselse>
		              <td>
		                <isprint value="${new Date(order.deliveryDate).toDateString()}" />
			            <isif condition="${order.shippingMethod != 'storePickup' && (order.deliveryType == 'Mixed' || order.deliveryType == 'Local')}">
			              , <isprint value="${order.deliveryTime}" />
			            </isif>
		              </td>
		            </isif>
		        <iselse>
		        	<td>${Resource.msg('searchorders.notscheduled','search',null)}</td>
	            </isif>
	            <td>
	            	<isif condition="${(order.shippingMethod == 'storePickup' && order.deliveryType != 'Mixed')}">
						<span class="text-center">Warehouse Pickup</span>
					<iselse>
						<isprint value="${order.deliveryType}" />
					</isif>
	            </td>
	            <td><span class="customer-name"><isprint value="${order.customerName}" /></span></td>
	            <td class="text-center"><isprint value="${order.qty}" formatter="#" /></td>
	            <td class="text-center"><isprint value="${session.getCurrency().getSymbol()+order.total}" /></td>
	            <td>
		            <isif condition="${order.exportStatus == dw.order.Order.EXPORT_STATUS_EXPORTED &&
									order.confirmationStatus == dw.order.Order.CONFIRMATION_STATUS_CONFIRMED &&
									order.status == dw.order.Order.ORDER_STATUS_COMPLETED &&
									order.shippingStatus == dw.order.Order.SHIPPING_STATUS_SHIPPED}">
						<span class=""></span>
					<iselseif condition="${(order.exportStatus == 1 || order.exportStatus == 2 || order.paymentStatus == 2)}">
						<a href="https://www.mattressfirm.com/delivery-tracking.html" target="_blank">Track Order</a>
						<isif condition="${order.shippingMethod != 'storePickup' && order.deliveryType != 'Mixed'}">
							<isif condition="${dw.order.Order.EXPORT_STATUS_EXPORTED == order.exportStatus}">
								<isif condition="${(order.deliveryType == 'Mixed' || order.deliveryType == 'Local') && order.reSchedule}">
								|
								</isif>
								
								<isif condition="${order.reSchedule && order.deliveryType != 'Drop Ship'}">
									<a href="${URLUtils.https('Fulfillment-RescheduleOrder', 'eventId', order.eventID, 'orderId', order.orderNo, 'comingFromRescheduleLink', 'true')}">Reschedule</a>
								</isif>
							</isif>
						<iselseif condition="${order.shippingMethod == 'storePickup' && order.deliveryType == 'Mixed'}">
							 | <span class="text-center">Warehouse Pickup</span>
						<iselseif condition="${order.shippingMethod != 'storePickup' && order.deliveryType == 'Mixed'}">
							<isif condition="${dw.order.Order.EXPORT_STATUS_EXPORTED == order.exportStatus && order.reSchedule}"> | <a href="${URLUtils.https('Fulfillment-RescheduleOrder', 'eventId', order.eventID, 'orderId', order.orderNo, 'comingFromRescheduleLink', 'true')}">Reschedule</a> </isif>	
						<iselseif condition="${order.shippingMethod != 'storePickup' && order.deliveryType == 'Local'}">
							<isif condition="${dw.order.Order.EXPORT_STATUS_EXPORTED == order.exportStatus && order.reSchedule}">
								<a href="${URLUtils.https('Fulfillment-RescheduleOrder', 'eventId', order.eventID, 'orderId', order.orderNo, 'comingFromRescheduleLink', 'true')}">Reschedule</a>
							</isif>
						<iselseif condition="${order.shippingMethod == 'storePickup' && order.deliveryType == 'Local'}">
							| <span class="text-center">Warehouse Pickup</span>
						<iselse>
							<span class="text-center">Warehouse Pickup</span>
						</isif>
					<iselseif condition="${(order.status == 0 || order.status == 3)  && (empty(order.deliveryDate) || order.deliveryDate == null) && (empty(order.deliveryTime) || shipment.custom.deliveryTime == null )}">
						<a href="${URLUtils.https('Fulfillment-OrderSummary', 'eventId', order.eventID, 'orderId', order.orderNo)}" class="action-schedule-delivery">Schedule Delivery</a>
					<iselseif condition="${(order.paymentStatus != 2) && (order.status == 0 || order.status == 3) && (!empty(order.deliveryDate) && order.deliveryDate != null) && (!empty(order.deliveryTime) && order.deliveryTime != null ) && order.shippingMethod != 'storePickup' && order.deliveryType != 'Drop Ship'}">
						<a href="${URLUtils.https('Fulfillment-OrderSummary', 'eventId', order.eventID, 'orderId', order.orderNo)}" class="action-complete-payment">Complete Order</a>
					<iselseif condition="${(order.paymentStatus != 2) && (order.status == 0 || order.status == 3) && (!empty(order.deliveryDate) && order.deliveryDate != null) && (order.shippingMethod == 'storePickup' || order.deliveryType == 'Drop Ship')}">
						<a href="${URLUtils.https('Fulfillment-Billing', 'eventId', order.eventID, 'orderId', order.orderNo)}" class="action-complete-payment">Complete Order</a>
					</isif>
				</td>
	          </tr>
	        </isloop>
	      </table>
	    </div>
	  </div>
  <iselse>
    <iscomment>Note: No orders found. When no orders found return the below message.</iscomment>
  	<isif condition="${pdict.searchPerformed}">
  	  <div class="col-12">
	    <div class="row">
	      <p class="search-pages__no-results">${Resource.msg('searchorders.noresults','search',null)}</p>
	    </div>
	  </div>
  	</isif>
  </isif>
</isdecorate>
