<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isdecorate template="account/pt_account">
	<isinclude template="util/modules"/>

	<iscomment>Builds a registration page for the user. It provides all input fields for names and address</iscomment>
	<isscript>
		var disabled_attributes = {
			disabled: 'disabled'
		};
		var autocomplete_attributes = {
			autocomplete: "0"
		};
	</isscript>

	<div id="primary" class="primary-content">
		<div class="col-6 cta p-0">
			<isif condition="${!empty(pdict.newCustomerCreated) && (pdict.newCustomerCreated == true)}">
				<div id="accountCreatedMessage" class="success-message">New Customer Account has been created successfully</div>
			</isif>
			<div class="row">
				<div class="cta__heading col-12">
					<div class="cta__heading-icon-container">
						<img class="cta__heading-icon" src="${URLUtils.staticURL('/images/svg-icons/new-account.svg')}" alt="">
					</div>
					<div class="cta__heading-text">
						<p class="cta__heading-text-info">${Resource.msg('account.user.registration.newuser','account',null)}</p>
						<h3 class="cta__heading-text-h3">${Resource.msg('account.user.registration.createnew','account',null)}</h3>
					</div>
				</div><!-- /.cta__heading -->
			</div><!-- /.row -->
			<div id='registration-form'>
				<form action="${URLUtils.httpsContinue()}" method="post" class="form-horizontal validation-form" id="RegistrationForm">
					<fieldset>
						<div class="cta__row-no-indent">
							<legend>
								${Resource.msg('account.user.registration.contactinformation','account',null)}
							</legend>
							<div class="cta__row-no-indent row justify-content-between">
								<isinputfield formfield="${pdict.CurrentForms.profile.customer.firstname}" type="input" maxlength="50" attributes="${autocomplete_attributes}" rowclass="col-5"/>
								<isinputfield formfield="${pdict.CurrentForms.profile.customer.lastname}" type="input" maxlength="50" attributes="${autocomplete_attributes}" rowclass="col-5"/>
							</div>
						</div>
	
						<isinputfield formfield="${pdict.CurrentForms.profile.customer.phone}" type="input" maxlength="12" attributes="${autocomplete_attributes}" rowclass="col-6 cta__row-no-indent"/>
					</fieldset>
	
					<fieldset>
						<legend>
							${Resource.msg('account.user.registration.companyinformation','account',null)}
						</legend>
						<isinputfield formfield="${pdict.CurrentForms.profile.customer.companyname}" maxlength="50" attributes="${autocomplete_attributes}" type="input"/>
						<div id="form-radio-holder" class="form-radio-holder">
							<isinputfield formfield="${pdict.CurrentForms.profile.login.accounttype}" type="radio"/>
						</div>
						<div class="form-row required form-input" aria-required="true">
							<div class="js-example-basic-multiple-container" style="display:none;">
									<p>Please type in one or more numeric characters to view Store IDs in the system. Then click one or more Store IDs to assign to user.</p>
									<label for="store_ids">Store ID(s)</label>
									<select id="store_ids" class="js-example-basic-multiple required" name="store_ids" multiple="multiple">
										<isif condition="${!empty(pdict.storeIds)}">
											<isloop items="${pdict.storeIds}" var="storeid">										
												<option 
													<isif condition="${!empty(pdict.storeIdsArr) && pdict.storeIdsArr.contains(storeid.custom.storeId)}">
														selected="selected"
													</isif>
													value=${storeid.custom.storeId}>
													${storeid.custom.storeId}
												</option>
											</isloop>
										</isif>
									</select>
								<div class="form-caption"></div>
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend>
							${Resource.msg('account.user.registration.businessaddress','account',null)}
						</legend>
						<div class="cta__row-no-indent row justify-content-between">
							<isinputfield formfield="${pdict.CurrentForms.profile.address.address1}" type="input" maxlength="50" attributes="${autocomplete_attributes}" rowclass="col-7 address1"/>
							<isinputfield formfield="${pdict.CurrentForms.profile.address.suite}" type="input" maxlength="50" attributes="${autocomplete_attributes}" rowclass="col-4 suite"/>
						</div>
	
						<div class="cta__row-no-indent row justify-content-between">
							<isinputfield formfield="${pdict.CurrentForms.profile.address.city}" type="input"  maxlength="50" attributes="${autocomplete_attributes}" rowclass="col-5 city"/>
							<isinputfield formfield="${pdict.CurrentForms.profile.address.states.state}" type="select" xhtmlclass="state" rowclass="col-4 state" />
							<isinputfield formfield="${pdict.CurrentForms.profile.address.postal}" maxlength="5" type="input" attributes="${autocomplete_attributes}" rowclass="col-2 zip" />
						</div>
	
						<div class="cta__row-no-indent">
							<isinputfield formfield="${pdict.CurrentForms.profile.address.country}" type="hidden" attribute1="value" value1="US"/>
						</div>
					</fieldset>
					<fieldset>
						<div class="cta__row-no-indent">
							<legend>
								${Resource.msg('account.user.registration.accountlogin','account',null)}
							</legend>
							<isinputfield formfield="${pdict.CurrentForms.profile.customer.email}" type="input" attributes="${autocomplete_attributes}" maxlength="320"/>
							<isinputfield formfield="${pdict.CurrentForms.profile.login.password}" type="password" maxlength="255" dynamicname="true" attributes="${autocomplete_attributes}"/>
							<isinputfield formfield="${pdict.CurrentForms.profile.login.passwordconfirm}" type="password" maxlength="255" dynamicname="true" attributes="${autocomplete_attributes}"/>
							<isif condition="${customer.registered && customer.authenticated && customer.profile.custom.accountType == 'Admin'}">
								<div class="btn-container-mid-form">								
									<button class="btn btn-secondary btn-bordered" type="button" id="generatePassword">
										${Resource.msg('global.generatepassword','locale',null)}
									</button>
								</div>
							</isif>
							<isinputfield formfield="${pdict.CurrentForms.profile.login.accountenabled}" type="select"/>
							<div class="cta__btn-container">
								<button class="btn btn-primary" type="submit" value="${Resource.msg('global.createaccount','locale',null)}" name="${pdict.CurrentForms.profile.confirm.htmlName}">
									${Resource.msg('global.createaccount','locale',null)}
								</button>
							</div>
							<input type="hidden" name="${pdict.CurrentForms.profile.secureKeyHtmlName}" value="${pdict.CurrentForms.profile.secureKeyValue}"/>
						</div>
					</fieldset>
				</form>
			</div>
		</div><!-- /.cta -->
	</div>
</isdecorate>
