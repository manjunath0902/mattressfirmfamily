<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isdecorate template="fulfillment/order/pt_fulfillment-order"/>
<isinclude template="util/modules"/>

<isset name="order" value="${pdict.orderSummary.orderObj}" scope="page" />
<isset name="event" value="${pdict.orderSummary.eventObj}" scope="page" />
<isset name="shipment" value="${order.defaultShipment}" scope="page" />
<isset name="events" value="${pdict.orderSummary.eventsArray}" scope="page" />
<isset name="totalAdditionalOrderReadyToProcess" value="${pdict.orderSummary.totals.totalOrdersReadyToProcess}" scope="page" />
<isset name="orderStep" value="${pdict.orderSummary.orderStep}" scope="page" />

<div class="inner-wrapper">
	<isif id="orderNotPlaced" condition="${!empty(pdict.orderSummary.ordersNotPlaced)}">
		<div class="error-message-display alert alert-danger">
			Orders Not Placed: <isprint value="${pdict.orderSummary.ordersNotPlaced}" encoding="off">
		</div>
	</isif>
	<isbreadcrumbs
		bcurl1="${URLUtils.url('Fulfillment-Dashboard')}"
		bctext1="${Resource.msg('fulfillment.dashboard','fulfillment',null)}"
		bctext2="${Resource.msg('fulfillment.ordersubmission','fulfillment',null)}"
	/>
		<isif condition="${!empty(shipment.custom.deliveryDate) || shipment.custom.deliveryDate != null}">
			<div class="customer-heading cta">
				<div class="row cta__row-no-indent">
					<div class="cta__heading col-12">
						<div class="cta__heading-icon-container">
							<div class="cta__heading-icon__product-heading"></div>
						</div>
						<div class="cta__heading-text">
							<p class="cta__heading-text-info">Ready to Invoice</p>
							<h3 class="cta__heading-text-h3">
								Order <isprint value="${order.orderNo}" />
							</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="products__table-holder paynow-table">
				<table>
					<tr>
						<th>Event</th>
						<th>Name</th>
						<th>Delivery</th>
						<th>Scheduled Date</th>
						<th class="text-center">Quantity</th>
						<th class="text-center">Total</th>
					</tr>
					<isloop items="${order.shipments}" var="Shipment" status="loopState">
						<isloop items="${Shipment.productLineItems}" var="pli" status="productloopstate">
							<tr>
								<td><isprint value="${event.custom.eventName}" /></td>
								<td><isprint value="${Shipment.getShippingAddress().fullName}" /></td>
								<td>
									<isif condition="${(pdict.orderSummary.orderObjShippingMethod == 'storePickup' && pli.product.custom.shippingInformation != 'Drop Ship')}">
										Warehouse Pickup
									<iselse>
										<isprint value="${pli.product.custom.shippingInformation == 'Core'?'Local':pli.product.custom.shippingInformation}" />
									</isif>
								</td>
								<td>
									<isif condition="${pli.product.custom.shippingInformation == 'Drop Ship'}">
										Delivered by vendor selected carrier
									<iselse>
										<isscript>
											var ymd = Shipment.custom.deliveryDate.split('T')[0];
											var y = parseInt(ymd.split('-')[0], 10),
												m = parseInt(ymd.split('-')[1], 10),
												d = parseInt(ymd.split('-')[2], 10);
											var eventDateStr = m + '/' + d + '/' + y;
										</isscript>
										<isprint value="${eventDateStr}" />
									</isif>
								</td>
								<td class="text-center"><isprint value="${pli.quantityValue}" formatter="#" /></td>
								<td class="text-center">${session.getCurrency().getSymbol()}<isprint value="${pli.priceValue}"  formatter="#0.00#"/></td>
							</tr>
						</isloop>
					</isloop>
					<isloop items="${order.shipments}" var="Shipment" status="shipmentLoopState">
						<isif condition="${Shipment.getAdjustedShippingTotalNetPrice() > 0}">
							<tr>
							<td colspan="4"><isprint value="${Shipment.shippingMethod.ID.toLowerCase()=='nationwide'?'Local Delivery':Shipment.shippingMethod.displayName}" /></td>
							<td class="text-center"><isprint value="${1}" formatter="#" /></td>
							<td class="text-center">${session.getCurrency().getSymbol()}<isprint value="${Shipment.getAdjustedShippingTotalNetPrice()}" formatter="#0.00#" /></td>
						</tr>
						</isif>
					</isloop>
					<tr>
						<td colspan="3"></td>
						<td><b>Order Summary</b></td>
						<td class="text-center"><b><isprint value="${order.productQuantityTotal}" formatter="#" /></b></td>
						<td class="color-dark text-center"><b>${session.getCurrency().getSymbol()}<isprint value="${order.totalGrossPrice.value}"  formatter="#0.00#"/></b></td>
					</tr>
				</table>
			</div>
		</isif>

		<isif condition="${totalAdditionalOrderReadyToProcess > 0}">
			<div class="additional-alert-info">
				You have <isprint value="${totalAdditionalOrderReadyToProcess}" formatter="#" /> additional orders pending submission. Select pending orders if you'd like to complete them now.
			</div>
			<div class="customer-heading cta">
				<div class="row cta__row-no-indent">
					<div class="cta__heading col-12">
						<div class="cta__heading-icon-container">
							<div class="cta__heading-icon__product-heading"></div>
						</div>
						<div class="cta__heading-text">
							<p class="cta__heading-text-info">Ready to Invoice</p>
							<h3 class="cta__heading-text-h3">
								Additional Orders Pending Submission
							</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="products__table-holder paynow-table">
				<table>
					<tr>
						<th><input id="checkAllAdditionalOrders" type="checkbox" name="checkAllAdditionalOrders" /></th>
						<th>Event</th>
						<th>Name</th>
						<th>Delivery</th>
						<th>Scheduled Date</th>
						<th class="text-center">Quantity</th>
						<th class="text-center">Total</th>
					</tr>
					<isloop items="${events}" var="Event" status="eventloopstate">
						<isloop items="${Event.orders}" var="Order" status="orderloopstate">
							<tr>
								<td><input id="${Order.orderNo}" type="checkbox" /></td>
								<td><isprint value="${Event.eventName}" /></td>
								<td><isprint value="${Order.customerName}" /></td>
								<td>
									<isif condition="${(Order.shippingMethod == 'storePickup')}">
										Warehouse Pickup
									<iselse>
										<isprint value="${Order.deliveryType}" />
									</isif>
								</td>
								<td>
									<isif condition="${Order.deliveryType == 'Drop Ship'}">
										Delivered by vendor selected carrier
									<iselse>
										<isscript>
											var ymd = Order.deliveryDate.split('T')[0];
											var y = parseInt(ymd.split('-')[0], 10),
												m = parseInt(ymd.split('-')[1], 10),
												d = parseInt(ymd.split('-')[2], 10);
											var eventDateStr = m + '/' + d + '/' + y;
										</isscript>
										<isprint value="${eventDateStr}" />
									</isif>
								</td>
								<td id="${Order.orderNo}-items" class="text-center" data-items-count="${Order.itemsQuantities}">
									<isprint value="${Order.itemsQuantities}" formatter="#" />
								</td>
								<td id="${Order.orderNo}-total" class="text-center" data-order-total="${Order.orderTotal}">
									${session.getCurrency().getSymbol()}<isprint value="${Order.orderTotal}" formatter="#0.00#" />
								</td>
							</tr>
						</isloop>
					</isloop>
					<tr>
						<td colspan="4"></td>
						<td><b>Additional Orders Selected</b></td>
						<td id="additional-order-item-count" class="text-center" data-additional-items-count="0"><b>0</b></td>
						<td id="additional-order-amount" class="color-dark text-center" data-additional-order-total="0"><b>0</b></td>
					</tr>
				</table>
			</div>
		</isif>

		<iscomment>TODO: Restore pay by credit card in Phase II.</iscomment>
		<iscomment>
			<div class="customer-heading cta">
				<div class="row cta__row-no-indent">
					<div class="cta__heading col-12">
						<div class="cta__heading-icon-container">
							<div class="cta__heading-icon__payment-heading"></div>
						</div>
						<div class="cta__heading-text">
							<p class="cta__heading-text-info">
								Pay by Credit Card
							</p>
							<h3 class="cta__heading-text-h3">
								Payment
							</h3>
						</div>
						<div class="payment-cardstype-holder cta__heading-button-group">
							<a href="javascript:void(0)">
								<img src="${URLUtils.staticURL('/images/svg-icons/credit-cards-visa.svg')}" alt="">
							</a>
							<a href="javascript:void(0)">
								<img src="${URLUtils.staticURL('/images/svg-icons/credit-cards-mastercard.svg')}" alt="">
							</a>
							<a href="javascript:void(0)">
								<img src="${URLUtils.staticURL('/images/svg-icons/credit-cards-amex.svg')}" alt="">
							</a>
							<a href="javascript:void(0)">
								<img src="${URLUtils.staticURL('/images/svg-icons/discover.svg')}" alt="">
							</a>
						</div>
					</div>
				</div>
			</div>
			<isinclude template="fulfillment/order/paymentfulfillment" />
		</iscomment>
		<isif condition="${order != null || totalAdditionalOrderReadyToProcess > 0}">
			<isinclude template="fulfillment/order/paymentinvoice" />

			<div class="notes">
				<p class="notes--note">
					<isprint value="${Resource.msg('order.billing.note.dropship','order',null)}" encoding="off"/>
				</p>
				<p class="notes--note">
					<isprint value="${Resource.msg('order.billing.note.delivery','order',null)}" encoding="off"/>
				</p>
			</div>
		<iselse>
			<br />
			<div class="alert alert-warning">You do not have any more orders to pay.</div>
			<br />
			<button class="btn btn-secondary go-dashboard-order" onClick="javascript: window.location='${URLUtils.url('Fulfillment-Dashboard').toString()}'">Go to Dashboard</button>
		</isif>
	</div>
</isdecorate>
