<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isif condition="${pdict.Product.master && pdict.CurrentVariationModel && pdict.CurrentVariationModel.variants.size() > 0}">
    <isset name="imageProduct" value="${pdict.CurrentVariationModel.variants[0]}" scope="page"/>
<iselse/>
    <isset name="imageProduct" value="${pdict.Product}" scope="page"/>
</isif>
<isif condition="${pdict.BonusDiscountLineItem.bonusProducts.size() > 1}">
    <isif condition="${!empty(imageProduct.getImage('small',0))}">
        <isset name="imageUrl" value="${imageProduct.getImage('medium',0).getURL()}" scope="page"/>
        <isset name="imageAlt" value="${imageProduct.getImage('medium',0).alt}" scope="page"/>
        <isset name="imageTitle" value="${imageProduct.getImage('medium',0).title}" scope="page"/>
    <iselse/>
        <isset name="imageUrl" value="${URLUtils.staticURL('/images/noimagemediuml.png')}" scope="page"/>
        <isset name="imageAlt" value="${pdict.Product.name}" scope="page"/>
        <isset name="imageTitle" value="${pdict.Product.name}" scope="page"/>
    </isif>
<iselse/>
    <isif condition="${!empty(imageProduct.getImage('large',0))}">
        <isset name="imageUrl" value="${imageProduct.getImage('large',0).getURL()}" scope="page"/>
        <isset name="imageAlt" value="${imageProduct.getImage('large',0).alt}" scope="page"/>
        <isset name="imageTitle" value="${imageProduct.getImage('large',0).title}" scope="page"/>
    <iselse/>
        <isset name="imageUrl" value="${URLUtils.staticURL('/images/noimagelarge.png')}" scope="page"/>
        <isset name="imageAlt" value="${pdict.Product.name}" scope="page"/>
        <isset name="imageTitle" value="${pdict.Product.name}" scope="page"/>
    </isif>
</isif>

<div class="product-col-1 product-image-container">
	<isinclude template="product/components/bonusproductimages"/>
</div>

<div class="product-col-2  product-detail">
    <h1 class="product-name" itemprop="name"><isprint value="${pdict.Product.name}"/></h1>
    <iscomment>
    <isif condition="${BonusProductsCollection.length==1}"> 
    	<isset name="bonusProductWidth" value="${BonusProductsCollection.length*335}" scope="session" />
    <iselse>
    	<isset name="bonusProductWidth" value="${BonusProductsCollection.length*200}" scope="session" />
    </isif>
    </iscomment>
    <isscript>
		importPackage(dw.system);
		importPackage(dw.util);
		var ProductUtils = require('~/cartridge/scripts/product/ProductUtils.js');
		var qs = ProductUtils.getQueryString(pdict.CurrentHttpParameterMap, ['source', 'uuid']),
			qsAppend = qs.length == 0 ? '' : ('&' + qs),
			pUtil = new ProductUtils(pdict),
			PVM = pdict.CurrentVariationModel != null ? pdict.CurrentVariationModel : pdict.Product.variationModel,
			selectedAtts = (pdict.Product.isVariant() || pdict.Product.isVariationGroup()) ? ProductUtils.getSelectedAttributes(PVM) : {},
			sizeChartShown = false,
			swatchAttributes = ['color', 'size', 'width', 'waist', 'length', 'foundation'],
			selectedVariants = [],
			selectedVariant = !empty(pdict.CurrentVariationModel.selectedVariant) ? pdict.CurrentVariationModel.selectedVariant : ProductUtils.getDefaultVariant(PVM);
		if (pdict.Product.variationGroup) {
			PVM = pdict.Product.variationGroup.getVariationModel();
		}
	</isscript>
    <isif condition="${pdict.Product.variant || pdict.Product.variationGroup || pdict.Product.master}">
		<div class="product-variations" data-attributes="${JSON.stringify(selectedAtts)}">
			<h2 class="visually-hidden">Variations</h2>
			<iscomment>
				Filter out variation attribute values with no orderable variants.
				The "clean" ProductVariationModel of the master without any selected attribute values is used to filter the variants.
				Otherwise hasOrderableVariants() would use currently selected values resulting in a too narrow selection of variants.
			</iscomment>
			<isset name="cleanPVM" value="${(pdict.Product.variant ? pdict.Product.masterProduct.variationModel : pdict.Product.variationModel)}" scope="page"/>
			<ul>
			<isloop items="${PVM.productVariationAttributes}" var="VA">
				<isset name="VAVALS" value="${PVM.getAllValues(VA)}" scope="page"/>
				<isset name="Valength" value="${VAVALS.length}" scope="page"/>
				<isset name="vaId" value="${VA.getAttributeID()}" scope="page"/>
				<isif condition="${swatchAttributes.indexOf(vaId) >= 0}">
					<li class="attribute">
					<isif condition="${VA.displayName=='Foundation'}"> 
						<div class="label">
							<label for="size"><isprint value="${VA.displayName}"/>: </label><isif condition="${!empty(pdict.Product.custom[vaId]) && !empty(PVM.getSelectedValue(VA).displayValue)}"><span class="selected-attr-value" >${PVM.getSelectedValue(VA).displayValue}</span></isif>
						</div>
					</isif>
						<div class="value" id="select-container-${vaId.toLowerCase()}">
							<iscomment>Size Chart link</iscomment>
							<isif condition="${vaId != 'color' && !sizeChartShown}">
								<isscript>
									// get category from products primary category
									var category = pdict.Product.primaryCategory,
										sizeChartID;
	
									// get category from product master if not set at variant
									if (!category && pdict.Product.variant) {
										category = pdict.Product.masterProduct.primaryCategory;
									}
	
									while (category && !sizeChartID) {
										if ('sizeChartID' in category.custom && !empty(category.custom.sizeChartID)) {
											sizeChartID = category.custom.sizeChartID;
										} else {
											category = category.parent;
										}
									}
								</isscript>
								<isif condition="${!empty(sizeChartID)}">
									<div class="size-chart-link">
										<a href="${URLUtils.url('Page-Show','cid', sizeChartID)}" target="_blank" title="${Resource.msg('product.variations.sizechart.label', 'product', null)}">${Resource.msg('product.variations.sizechart', 'product', null)}</a>
									</div>
									<isscript>
										sizeChartShown = true;
									</isscript>
								</isif>
							</isif>
							<span class="tooltip" data-layout="small" tabindex='0' role="tooltip" aria-label="size help icon">
								<div class="tooltip-content">
									<iscontentasset aid="${'pdp-information-' + VA.ID}"/>
								</div>
							</span>
						</div>
					</li>
				</isif>
			</isloop>
			</ul>
		</div>
	</isif>
    <isif condition="${!empty(pdict.Product.priceModel.price.decimalValue)}">
		<div class="product-price">
		<isscript>
		var bonusProductPrice = dw.value.Money((pdict.MaxBonusItemsCount * pdict.Product.priceModel.price.decimalValue),session.getCurrency().getCurrencyCode());		
		</isscript>
			<isif condition="${!empty(pdict.BonusDiscountLineItem.getBonusProductPrice(pdict.Product))}">
    			<isif condition="${pdict.BonusDiscountLineItem.getBonusProductPrice(pdict.Product) == 0}">
    			<p>Free</p>
    			<iselse/>
    			<p><isprint value="${pdict.BonusDiscountLineItem.getBonusProductPrice(pdict.Product)}" /></p>
    			</isif>
    		</isif>
			<del class="bonus-price-modal"></span><isprint value="${bonusProductPrice}" /></del>
			
		</div>
	</isif>	    



    <div class="product-number">
        ${Resource.msg('product.item','product',null)} <span itemprop="productID"><isprint value="${pdict.Product.ID}"/></span>
    </div>
    
    <div class="product-add-to-cart">
	<form action="${URLUtils.url('Cart-AddProduct')}" method="post" id="${pdict.CurrentForms.product.addtocart.dynamicHtmlName}" name="${pdict.CurrentForms.product.addtocart.htmlName}" class="bonus-product-form">
		<fieldset>
	         <isinclude template="product/components/options"/>
	         <input type="hidden" name="pid" value="${pdict.Product.ID}"/>
	         <input type="hidden" name="productUUID" value="${pdict.Product.UUID}"/>
	         <input type="hidden" name="bonusDiscountLineItemUUID" value="${pdict.BonusDiscountLineItem.UUID}"/>
	         <input type="hidden" id="maxBonusItemsCount" name="maxBonusItemsCount" value="${pdict.MaxBonusItemsCount}">
	         <div class="inventory">
	             <div class="quantity">
	                 <label for="qty-${pdict.Product.UUID}">${Resource.msg('global.qty','locale',null)}:</label>
	                 <input type="text" class="input-text" name="Quantity" id="qty-${pdict.Product.UUID}" size="2" maxlength="3" value="${Number(empty(pdict.CurrentHttpParameterMap.Quantity.stringValue) ? ${pdict.MaxBonusItemsCount} : pdict.CurrentHttpParameterMap.Quantity.stringValue).toFixed()}"/>
	             </div>
	             <label class="quantity-error"/></label>
	         </div>
	         <isscript>
	             var avm = pdict.Product.availabilityModel;
	             var disableAttr = '';
	             if (!(avm.availabilityStatus === dw.catalog.ProductAvailabilityModel.AVAILABILITY_STATUS_IN_STOCK && !pdict.Product.master)) {
	                 disableAttr = 'disabled="disabled"';
	             }
	         </isscript>
			<button type="button" class="add-to-cart-bonus button-fancy-medium" value="${Resource.msg('global.addtocart','locale',null)}">
				${Resource.msg('global.addtocart','locale',null)}
			</button>
	
	         <button type="submit" style="display:none" class="select-bonus-item"><span>${Resource.msg('global.select', 'locale', null)}</span></button>
		</fieldset>
	</form>
</div>
    
	<isif condition="${pdict.Product.bundle && pdict.Product.getBundledProducts().size() > 0}">
        <iscomment>Display bundle Individual Products</iscomment>
        <iscomment>Preserve the current product instance</iscomment>
        <isset name="ProductBundle" value="${pdict.Product}" scope="pdict"/>
        <isloop items="${pdict.Product.getBundledProducts()}" var="BundledProduct" status="bundleLoop">
            <isset name="Product" value="${BundledProduct}" scope="pdict"/>
            <isset name="ProductCount" value="${bundleLoop.count}" scope="pdict"/>
            <isobject object="${pdict.Product}" view="setproduct">
                <isinclude template="product/components/subbonusproduct"/>
            </isobject>
        </isloop>
        <iscomment>restore the current product instance</iscomment>
        <isset name="Product" value="${pdict.ProductBundle}" scope="pdict"/>
    </isif>

    <isset name="pam" value="${pdict.Product.getAttributeModel()}" scope="page"/>
    <isset name="group" value="${pam.getAttributeGroup('mainAttributes')}" scope="page"/>
    <isinclude template="product/components/group"/>

    <iscomment><isinclude template="product/components/variations"/></iscomment>

	<iscomment><isscript>
		importPackage(dw.system);
		importPackage(dw.util);
		var ProductUtils = require('~/cartridge/scripts/product/ProductUtils.js');
		var qs = ProductUtils.getQueryString(pdict.CurrentHttpParameterMap, ['source', 'uuid']),
			qsAppend = qs.length == 0 ? '' : ('&' + qs),
			pUtil = new ProductUtils(pdict),
			PVM = pdict.CurrentVariationModel != null ? pdict.CurrentVariationModel : pdict.Product.variationModel,
			selectedAtts = (pdict.Product.isVariant() || pdict.Product.isVariationGroup()) ? ProductUtils.getSelectedAttributes(PVM) : {},
			sizeChartShown = false,
			swatchAttributes = ['color', 'size', 'width', 'waist', 'length', 'foundation'],
			selectedVariants = [],
			selectedVariant = !empty(pdict.CurrentVariationModel.selectedVariant) ? pdict.CurrentVariationModel.selectedVariant : ProductUtils.getDefaultVariant(PVM);
		if (pdict.Product.variationGroup) {
			PVM = pdict.Product.variationGroup.getVariationModel();
		}
	</isscript></iscomment>
	<isif condition="${pdict.Product.variant || pdict.Product.variationGroup || pdict.Product.master}">
		<div class="product-variations" data-attributes="${JSON.stringify(selectedAtts)}">
			<h2 class="visually-hidden">Variations</h2>
			<iscomment>
				Filter out variation attribute values with no orderable variants.
				The "clean" ProductVariationModel of the master without any selected attribute values is used to filter the variants.
				Otherwise hasOrderableVariants() would use currently selected values resulting in a too narrow selection of variants.
			</iscomment>
			<isset name="cleanPVM" value="${(pdict.Product.variant ? pdict.Product.masterProduct.variationModel : pdict.Product.variationModel)}" scope="page"/>
			<ul>
			<isloop items="${PVM.productVariationAttributes}" var="VA">
				<isset name="VAVALS" value="${PVM.getAllValues(VA)}" scope="page"/>
				<isset name="Valength" value="${VAVALS.length}" scope="page"/>
				<isset name="vaId" value="${VA.getAttributeID()}" scope="page"/>
				<isif condition="${swatchAttributes.indexOf(vaId) >= 0}">
					<li class="attribute">
						<isif condition="${VA.displayName != 'Foundation'}"> 
							<div class="label">
								<label for="size"><isprint value="${VA.displayName}"/>: </label><isif condition="${!empty(pdict.Product.custom[vaId]) && !empty(PVM.getSelectedValue(VA).displayValue)}"><span class="selected-attr-value" >${PVM.getSelectedValue(VA).displayValue}</span></isif>
							</div>
						</isif>
						<div class="value" id="select-container-${vaId.toLowerCase()}">
							<iscomment>Size Chart link</iscomment>
							<isif condition="${vaId != 'color' && !sizeChartShown}">
								<isscript>
									// get category from products primary category
									var category = pdict.Product.primaryCategory,
										sizeChartID;
	
									// get category from product master if not set at variant
									if (!category && pdict.Product.variant) {
										category = pdict.Product.masterProduct.primaryCategory;
									}
	
									while (category && !sizeChartID) {
										if ('sizeChartID' in category.custom && !empty(category.custom.sizeChartID)) {
											sizeChartID = category.custom.sizeChartID;
										} else {
											category = category.parent;
										}
									}
								</isscript>
								<isif condition="${!empty(sizeChartID)}">
									<div class="size-chart-link">
										<a href="${URLUtils.url('Page-Show','cid', sizeChartID)}" target="_blank" title="${Resource.msg('product.variations.sizechart.label', 'product', null)}">${Resource.msg('product.variations.sizechart', 'product', null)}</a>
									</div>
									<isscript>
										sizeChartShown = true;
									</isscript>
								</isif>
							</isif>
							<span class="tooltip" data-layout="small" tabindex='0' role="tooltip" aria-label="size help icon">
								<div class="tooltip-content">
									<iscontentasset aid="${'pdp-information-' + VA.ID}"/>
								</div>
							</span>
						</div>
					</li>
				<iselse/>
					<iscomment>Drop down list</iscomment>
					<li class="attribute variant-dropdown">
						<div class="label">
							<span><isprint value="${VA.displayName}"/>: </span><isif condition="${!empty(pdict.Product.custom[vaId])}"><span class="selected-attr-value"><isprint value="${pdict.Product.custom[vaId]}"></span></isif>
						</div>
						<div class="value">
							<span class="tooltip" tabindex='0' role="tooltip" aria-label="size help icon">
								<div class="tooltip-content" data-layout="small">
									<iscontentasset aid="${'pdp-information-' + VA.ID}" />
								</div>
							</span>
						</div>
					</li>
				</isif>
			</isloop>
			</ul>
		</div>
	</isif>
	<iscomment><isif condition="${!empty(pdict.BonusDiscountLineItem.getBonusProductPrice(pdict.Product))}"> </iscomment>
	<div class="quantity-part">
		<span class="display-name">${Resource.msg('global.qty','locale',null)}</span>: <span class="display-value">${pdict.MaxBonusItemsCount}</span>
		<ul class="bonus-product-features">
		<isloop items="${pdict.Product.getAttributeModel().getAttributeGroup('pdpFeatures').getAttributeDefinitions()}" var="productFeaturesDefinition" status="loopstate1">
			<isset name="currentFeature" value="${pdict.Product.getAttributeModel()}" scope="page"/>
			<isif condition="${!empty(currentFeature.getDisplayValue(productFeaturesDefinition))}" />
				<li><isprint value="${currentFeature.getDisplayValue(productFeaturesDefinition)}" /></li>
			</isif>
			<isif condition="${loopstate1.index >= loopstate.index}"> 
				<isbreak>
			</isif>
		</isloop>
	</ul>	
    </div>
    
			
    <div class="product-add-to-cart product-add-to-cart-mobile">
		<form action="${URLUtils.url('Cart-AddProduct')}" method="post" id="${pdict.CurrentForms.product.addtocart.dynamicHtmlName}" name="${pdict.CurrentForms.product.addtocart.htmlName}" class="bonus-product-form">
			<fieldset>
		         <isinclude template="product/components/options"/>
		         <input type="hidden" name="pid" value="${pdict.Product.ID}"/>
		         <input type="hidden" name="productUUID" value="${pdict.Product.UUID}"/>
		         <input type="hidden" name="bonusDiscountLineItemUUID" value="${pdict.BonusDiscountLineItem.UUID}"/>
		         <input type="hidden" id="maxBonusItemsCount" name="maxBonusItemsCount" value="${pdict.MaxBonusItemsCount}">
		         <div class="inventory">
		             <div class="quantity">
		                 <label for="qty-${pdict.Product.UUID}">${Resource.msg('global.qty','locale',null)}:</label>
		                 <input type="text" class="input-text" name="Quantity" id="qty-${pdict.Product.UUID}" size="2" maxlength="3" value="${Number(empty(pdict.CurrentHttpParameterMap.Quantity.stringValue) ? ${pdict.MaxBonusItemsCount} : pdict.CurrentHttpParameterMap.Quantity.stringValue).toFixed()}"/>
		             </div>
		             <label class="quantity-error"/></label>
		         </div>
		         <isscript>
		             var avm = pdict.Product.availabilityModel;
		             var disableAttr = '';
		             if (!(avm.availabilityStatus === dw.catalog.ProductAvailabilityModel.AVAILABILITY_STATUS_IN_STOCK && !pdict.Product.master)) {
		                 disableAttr = 'disabled="disabled"';
		             }
		         </isscript>
				<button type="button" class="add-to-cart-bonus button-fancy-medium" value="${Resource.msg('global.addtocart','locale',null)}">
					${Resource.msg('global.addtocart','locale',null)}
				</button>
		
		         <button type="submit" style="display:none" class="select-bonus-item"><span>${Resource.msg('global.select', 'locale', null)}</span></button>
			</fieldset>
		</form>
	</div>
	                            
</div>