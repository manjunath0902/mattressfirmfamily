'use strict';

var util = require('./util'),
    dialog = require('./dialog'),
    page = require('./page');


function initializePicker() {
	$(".pickup-stores-slider").slick({
		  infinite: true,
		  slidesToShow: 5,
		  slidesToScroll: 1
		});

		$(".store-pickup-point.pickup label , .store-pickup-point.pickup input").click(function(){
			if($(".store-pickup-point.pickup input[type='radio']:checked")){
				$(".pickup-stores-holder").slideDown();
				$(".delivery-picker-holder").slideUp();
				$(".pickup-stores-slider").slick('setPosition');
			}

		});

		$(".store-pickup-point.delivery-picker-top label,.store-pickup-point.delivery-picker-top input").click(function(){
			if($(".store-pickup-point.delivery-picker-top input[type='radio']:checked")){
				$(".pickup-stores-holder").slideUp();
				$(".delivery-picker-holder").slideDown();
				$(".pickup-stores-slider").slick('setPosition');
			}
		});

		$(".pickup-store-details").click(function(){
			$(this).toggleClass("active");
		});


		$(".show-button-picker").click(function(){
			$(this).hide();
			$(".store-picker-detial-info").slideDown();
			$(".show-button-picker-pay").slideDown();
			$(".process-bar__item.new").removeClass("active").addClass("complete");
			$(".process-bar__item.scheduled").addClass("active");
		});




		var deliveryPicker = {
			initDeliveryPicker: function () {
				$('.delivery-picker').slick({
					infinite: false,
					slidesToShow: 7,
					slidesToScroll: 7
				});
			},

			onClickOfRadioButtonDiv: function () {
				var $list = $('.delivery-picker');

				$list.find('.delivery-picker__date-time-slot').on('click', function (e) {
					var $self = $(e.target);
					$list.find('.delivery-picker__date-time-slot').removeClass('selected');
					$list.find('.delivery-picker__date').removeClass('selected');
					$self.find('input[type=radio]').prop('checked', 'checked');
					$self.closest('.delivery-picker__date').addClass('selected');
					$self.closest('.delivery-picker__date-time-slot').addClass('selected');
				})
			},

			setDefaultRadioButtonDiv: function() {
				var $list = $('.delivery-picker');

				if ($list.find('input[type=radio]:checked').length === 0) {
					$list.find('input[type=radio]:first').prop('checked', 'checked');
					if(!$list.find('input[type=radio]').closest('.delivery-picker__date-time-slot').hasClass('selected')){
						$list.find('input[type=radio]:first').closest('.delivery-picker__date-time-slot').addClass('selected');
						$list.find('input[type=radio]:first').closest('.delivery-picker__date').addClass('selected');
					}
				}

				this.onClickOfRadioButtonDiv();
			},

			init: function() {
				this.initDeliveryPicker();
				this.setDefaultRadioButtonDiv();
			}
		};

		var styleGuideDemos = {
			init: function () {
				// Invoke multiple tag picker
				var url = $('#create_event_url').val();
				$('.js-example-basic-multiple-container').show();
				$('.js-example-basic-multiple').select2();

				// date picker, that goes in dialog
				$( "#datepicker" ).datepicker({
					altField: "#alternate",
					altFormat: "DD, MM, d, yy"
				});

				$( "#dialog" ).dialog({
					autoOpen: false,
					width: 889,
					height: 588,
					modal: true
				});

				$( ".opener" ).on( "click", function(e) {
					e.preventDefault();
					$( "#dialog" ).dialog( "open" );
				});
			}
		}


		$(function () {
			// Scheduler / Delivery Picker Page
			if ($('.styleguide-delivery-picker-page').length > 0) {
				deliveryPicker.init();
			}

			if ($('.styleguide-main-page').length > 0) {
				styleGuideDemos.init();
			}

		});
}

exports.init = function () {
//	$(document).on("click", ".delivery-picker__date-time-label", function () {
//		var obj = $(this);
//		var deliveryTime = $(obj).find('.delivery-picker__date-time-slot.selected').text().replace(RegExp(' ', 'g'), '');
//		$('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryDate').val($(obj).closest('.delivery-picker__date').find(".weekday-timeval").text() + ', ' + deliveryTime);
//	    $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryTime').val(deliveryTime);
//
//	    var textdate = $(obj).closest('.delivery-picker__date').find(".delivery-picker__date-title").find(".weekday-timeval").text();
//        textdate += ", between " + $(obj).text().replace("-","and").replace(RegExp(" PM", "g"), "pm").replace(RegExp(" AM", "g"), "am").trim();
//       // $(".chosen-time").text(textdate);
//	});
	$(document).on("click", "#print-receipt" , function(e) {
		e.preventDefault();
		var orders = $("#confirmed-orders .order-no").map(function () {return $(this).text();}).get().join(',');
		var url = util.appendParamsToUrl(this.href,{"orderIds":orders});
		window.open(url);
	});

	$(document).on("click", "#schedule-delivery" , function(e) {
		e.preventDefault();
		var deliveryTime, deliveryDate, location1, location2, mfiDSZoneLineId, mfiDSZipCode;
		var obj = $(".delivery-picker__date-time-label").closest("div .selected");
		if(obj.length > 1) {
			deliveryTime = $(obj).find('.delivery-picker__date-time-slot.selected label').text().replace(RegExp(' ', 'g'), '').trim();
			deliveryDate = $(obj).closest('.delivery-picker__date').find(".weekday-timeval").text() + ', ' + deliveryTime;

			location1 = $(obj).find('.delivery-picker__date-time-slot.selected .location1').text();
			location2 = $(obj).find('.delivery-picker__date-time-slot.selected .location2').text();
			mfiDSZoneLineId = $(obj).find('.delivery-picker__date-time-slot.selected .mfiDSZoneLineId').text();
			mfiDSZipCode = $(obj).find('.delivery-picker__date-time-slot.selected .mfiDSZipCode').text();
		} else if(obj.length == 1) {
			deliveryTime = obj.find('label').text().replace(RegExp(' ', 'g'), '').trim();
			deliveryDate = $(obj).closest('.delivery-picker__date').find(".weekday-timeval").text();

			location1 = obj.find('.location1').text();
			location2 = obj.find('.location2').text();
			mfiDSZoneLineId = obj.find('.mfiDSZoneLineId').text();
			mfiDSZipCode = obj.find('.mfiDSZipCode').text();
		} else {
			$("#schedule-delivery").attr("disabled", "disabled");
			return;	// In case of "No Slots available" stay on the same page
		}
		var orderId = getUrlParameter('orderId'),
		eventId = getUrlParameter('eventId'),
 		params = {
					"orderId" : orderId,
					"deliveryDate" : deliveryDate,
					"deliveryTime" : deliveryTime,
					"location1" : location1,
					"location2" : location2,
					"mfiDSZoneLineId" : mfiDSZoneLineId,
					"mfiDSZipCode" : mfiDSZipCode
				};

		$.ajax({
			url: Urls.fulfillmentScheduleDelivery,
			data: params,
			type: "POST",
			success: function(data, response) {
				// Order was already exported and now it cannot be (re)scheduled locally
				if (response && data.indexOf('EXPORT_STATUS_EXPORTED') > -1) { // OrderExportStatus == EXPORT_STATUS_EXPORTED
					window.location = util.appendParamsToUrl(Urls.fulfillmentBilling, {"eventId":eventId, "orderId":orderId});
				} else if (response) { // Order scheduled successfully
					window.location = util.appendParamsToUrl(Urls.fulfillmentOrderSummary,{"eventId":eventId,"orderId":orderId});
				}
			},
			failure: function() {
				alert("Fail");
				window.alert(Resources.SERVER_ERROR);
			}
		});

	});

	$(document).on('click','#schedule-delivery-payment',function(e) {
		e.preventDefault();

		var deliveryTime, deliveryDate, location1, location2, mfiDSZoneLineId, mfiDSZipCode;
		var obj = $(".delivery-picker__date-time-label").closest("div .selected");
		if(obj.length > 1) {
			deliveryTime = $(obj).find('.delivery-picker__date-time-slot.selected label').text().replace(RegExp(' ', 'g'), '').trim();
			deliveryDate = $(obj).closest('.delivery-picker__date').find(".weekday-timeval").text() + ', ' + deliveryTime;

			location1 = $(obj).find('.delivery-picker__date-time-slot.selected .location1').text();
			location2 = $(obj).find('.delivery-picker__date-time-slot.selected .location2').text();
			mfiDSZoneLineId = $(obj).find('.delivery-picker__date-time-slot.selected .mfiDSZoneLineId').text();
			mfiDSZipCode = $(obj).find('.delivery-picker__date-time-slot.selected .mfiDSZipCode').text();
		} else {
			deliveryTime = obj.find('label').text().replace(RegExp(' ', 'g'), '').trim();
			deliveryDate = $(obj).closest('.delivery-picker__date').find(".weekday-timeval").text();

			location1 = obj.find('.location1').text();
			location2 = obj.find('.location2').text();
			mfiDSZoneLineId = obj.find('.mfiDSZoneLineId').text();
			mfiDSZipCode = obj.find('.mfiDSZipCode').text();
		}

		var orderId = getUrlParameter('orderId'),
		eventId = getUrlParameter('eventId'),
		params = {
			"orderId" : orderId,
			"deliveryDate" : deliveryDate,
			"deliveryTime" : deliveryTime,
			"location1" : location1,
			"location2" : location2,
			"mfiDSZoneLineId" : mfiDSZoneLineId,
			"mfiDSZipCode" : mfiDSZipCode
		};

		$.ajax({
			url: Urls.fulfillmentScheduleDelivery,
			data: params,
			type: "POST",
			success: function(data, response) {
				if(response) {
					window.location = util.appendParamsToUrl(Urls.fulfillmentBilling, {"eventId":eventId, "orderId":orderId});
				}
			},
			failure: function() {
				alert("Fail");
				window.alert(Resources.SERVER_ERROR);
			}
		});

	});

	$(document).on('click','#dropship-payment',function(e) {
		e.preventDefault();

		var orderId = getUrlParameter('orderId'),
		eventId = getUrlParameter('eventId'),
		params = {
			"orderId" : orderId,
		};

		$.ajax({
			url: Urls.fulfillmentScheduleDelivery,
			data: params,
			type: "POST",
			success: function(data, response) {
				if(response) {
					window.location = util.appendParamsToUrl(Urls.fulfillmentBilling, {"eventId":eventId, "orderId":orderId});
				}
			},
			failure: function() {
				alert("Fail");
				window.alert(Resources.SERVER_ERROR);
			}
		});

	});

	$(document).ready(function() {

		if($('.order-details').length) {

			var orderId = getUrlParameter('orderId'),
				eventId = getUrlParameter('eventId');

			$.ajax({
				url: util.appendParamsToUrl(Urls.getEventOrderSummary, {format: 'ajax'}),
				type	: 'GET',
				data	: {"orderId":orderId, "eventId":eventId},
				success: function (response) {

			    	if (response) {
			    		if ($('.order-details').length) {
			    			$(".order-details").html(response);

			    			initializePicker();
			    		}
					}
				},
				failure: function () {
					alert("Fail");
					window.alert(Resources.SERVER_ERROR);
				}
			});
		}

		initializePicker();

	});

	$(document).on('click','#confirm-new-delivery', function(e) {

		e.preventDefault();
		var deliveryTime, deliveryDate, location1, location2, mfiDSZoneLineId, mfiDSZipCode;
		var obj = $(".delivery-picker__date-time-label").closest("div .selected");
		if(obj.length > 1) {
			deliveryTime = $(obj).find('.delivery-picker__date-time-slot.selected label').text().replace(RegExp(' ', 'g'), '').trim();
			deliveryDate = $(obj).closest('.delivery-picker__date').find(".weekday-timeval").text() + ', ' + deliveryTime;

			location1 = $(obj).find('.delivery-picker__date-time-slot.selected .location1').text();
			location2 = $(obj).find('.delivery-picker__date-time-slot.selected .location2').text();
			mfiDSZoneLineId = $(obj).find('.delivery-picker__date-time-slot.selected .mfiDSZoneLineId').text();
			mfiDSZipCode = $(obj).find('.delivery-picker__date-time-slot.selected .mfiDSZipCode').text();
		} else if(obj.length == 1) {
			deliveryTime = obj.find('label').text().replace(RegExp(' ', 'g'), '').trim();
			deliveryDate = $(obj).closest('.delivery-picker__date').find(".weekday-timeval").text();

			location1 = obj.find('.location1').text();
			location2 = obj.find('.location2').text();
			mfiDSZoneLineId = obj.find('.mfiDSZoneLineId').text();
			mfiDSZipCode = obj.find('.mfiDSZipCode').text();
		} else {
			$("#confirm-new-delivery").attr("disabled", "disabled");
			return;	// In case of "No Slots available" stay on the same page
		}

		var orderId = getUrlParameter('orderId'),
			eventId = getUrlParameter('eventId'),
			params = {
				"orderId" : orderId,
				"eventId" : eventId,
				"deliveryDate" : deliveryDate,
				"deliveryTime" : deliveryTime,
				"location1" : location1,
				"location2" : location2,
				"mfiDSZoneLineId" : mfiDSZoneLineId,
				"mfiDSZipCode" : mfiDSZipCode
			};

		$.ajax({
			url: Urls.updateOrderSchedule,
			data: params,
			type: "POST",
			success: function(data, response) {
				if(response && data == 0 && data.indexOf('failed') == -1) { // OrderExportStatus == EXPORT_STATUS_NOTEXPORTED (in case of local reschedule)
					$.ajax({
						url: util.appendParamsToUrl(Urls.getEventOrderSummary, {format: 'ajax'}),
						type	: 'GET',
						data	: {"orderId":orderId, "eventId":eventId},
						async	: false,
						success: function (response) {

					    	if (response) {
				    			window.location = util.appendParamsToUrl(Urls.fulfillmentBilling, {"eventId":eventId, "orderId":orderId});
							}
						},
						failure: function () {
							//alert("Fail");
							window.alert(Resources.SERVER_ERROR);
						}
					});
				} else if (data.indexOf('failed') > -1) {
					window.location = util.removeParamFromURL(window.location.href, 'reschedulefailed') + "&reschedulefailed=true";
				} else { // OrderExportStatus == EXPORT_STATUS_EXPORTED
					window.location = util.removeParamFromURL(window.location.href, 'reschedulefailed') + "&rescheduled=true";
				}
			},
			failure: function () {
				//alert("Fail");
				window.alert(Resources.SERVER_ERROR);
			}
		});

	});

	var getUrlParameter = function getUrlParameter(sParam) {
	    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName,
	        i;

	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');

	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : sParameterName[1];
	        }
	    }
	};

};
