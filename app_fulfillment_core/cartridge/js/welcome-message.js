'use strict';

var $dashboard = $('.dashboard'),
    $welcomeMessage = $('#welcome-message'),
    $welcomeCloseBtn = $('.welcome-message__close-button');


var welcomeMessage = {
  init: function() {
    this.initWelcomeMessage();
  },

  // Hide message and set cookie to prevent seeing it again.
  dismissWelcomeMessage: function() {
    $welcomeMessage.slideUp(300, function(){
      $.cookie('hide-welcome-on-click', 'yes', { expires:1826 });
    });
  },

  // Bind click event to hide message
  onClickWelcomeCloseBtn: function() {
    var self = this;
    $welcomeCloseBtn.on('click', function(e) {
      e.preventDefault();
      self.dismissWelcomeMessage();
    })
  },

  /**
  * @private
  * @function
  * @description Check cookie to see user dismissed welcome msg.
  */
  initWelcomeMessage: function () {
    // If the user has not clicked to close - setting the cookie to hide the
    // welcome message, then show it.
    var self = this;
    if ($.cookie('hide-welcome-on-click') != 'yes') {
      $welcomeMessage.removeClass('d-none');
      $welcomeMessage.slideDown(300, function(){
        self.onClickWelcomeCloseBtn();
      });
    }
    // If the cookie is true hide the welcome message.
    else {
      $welcomeMessage.addClass('d-none');
    }
  }
}

module.exports = welcomeMessage;
