'use strict';

var util	= require('./util'),
    dialog	= require('./dialog'),
    page	= require('./page');

exports.init = function () {

	$(document).on("click", "#schedule-pickup" , function(e) {
		e.preventDefault();
		var orderId = getUrlParameter('orderId'),
		eventId = getUrlParameter('eventId'),
		warehouseId = $('#warehouseID').val(),
		warehousePickupDate = new Date($('#warehousePickupDate').val()).toISOString(),
		params = {"orderId":orderId, "eventId":eventId, "warehouseId": warehouseId, "warehousePickupDate": warehousePickupDate};

		$.ajax({
			url	: Urls.warehousePickerSchedulePickup,
			data: params,
			type: "POST",
			success: function(data, response) {
				if(response) {
					window.location = util.appendParamsToUrl(Urls.fulfillmentOrderSummary,{"eventId":eventId,"orderId":orderId,"showScheduleThisTime":"true"});
				}
			},
			failure: function() {
				alert("Fail");
				window.alert(Resources.SERVER_ERROR);
			}
		});
	});

	$(document).on('click','#schedule-pickup-payment',function(e) {
		e.preventDefault();
		var orderId = getUrlParameter('orderId'),
		eventId = getUrlParameter('eventId'),
		warehouseId = $('#warehouseID').val(),
		warehousePickupDate = new Date($('#warehousePickupDate').val()).toISOString(),
		params = {"orderId":orderId, "eventId":eventId, "warehouseId": warehouseId, "warehousePickupDate": warehousePickupDate};

		$.ajax({
			url: Urls.warehousePickerSchedulePickup,
			data: params,
			type: "POST",
			success: function(data, response) {
				if(response) {
					window.location = util.appendParamsToUrl(Urls.fulfillmentBilling, {"eventId":eventId, "orderId":orderId});
				}
			},
			failure: function() {
				alert("Fail");
				window.alert(Resources.SERVER_ERROR);
			}
		});
	});

	var getUrlParameter = function getUrlParameter(sParam) {
	    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName,
	        i;

	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');

	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : sParameterName[1];
	        }
	    }
	};

};
