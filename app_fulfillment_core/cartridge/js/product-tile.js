'use strict';

var imagesLoaded = require('imagesloaded'),
	quickview = require('./quickview');
	//ResponsiveSlots = require('./responsiveslots/responsiveSlots');

function initQuickViewButtons() {
	$('.tiles-container .product-image').on('mouseenter', function () {
		var $qvButton = $('#quickviewbutton');
		if ($qvButton.length === 0) {
			$qvButton = $('<a id="quickviewbutton" class="quickview">' + Resources.QUICK_VIEW + '</a>');
		}
		var $link = $(this).find('.thumb-link');
		$qvButton.attr({
			'href': $link.attr('href'),
			'title': $link.attr('title')
		}).appendTo(this);
		$qvButton.on('click', function (e) {
			e.preventDefault();
			quickview.show({
				url: $(this).attr('href'),
				source: 'quickview'
			});
		});
	});
}

function gridViewToggle() {
	$('.toggle-grid').on('click', function () {
		$('.search-result-content').toggleClass('wide-tiles');
		$(this).toggleClass('wide');
		if ($('.search-result-content').hasClass('wide-tiles')) {
			$(".product-key-features-plp").show();
			utag.link({
				eventCategory: 'PLP',
				eventAction: 'Select',
				eventLabel: 'List View'
			});
		} else {
			$(".product-key-features-plp").hide();
		}
	});
	$('.toggle-grid').on('keypress', function (ev) {
		if (ev.keyCode == 13 || ev.which == 13) {
			$('.search-result-content').toggleClass('wide-tiles');
			$(this).toggleClass('wide');
			if ($('.search-result-content').hasClass('wide-tiles')) {
				$(".product-key-features-plp").show();
				utag.link({
					eventCategory: 'PLP',
					eventAction: 'Select',
					eventLabel: 'List View'
				});
			} else {
				$(".product-key-features-plp").hide();
			}
		}
	});
}
function compareToolPadding() {
	var $compareTool = $('.tiles-container .product-tile .compare-check'); //compareTool if on then its padding will be handled by compareToolPadding class
	if ($compareTool.length > 0) {
		$(".search-result-content .product-tile").addClass('compareToolPadding');
	}
}

/**
 * @private
 * @function
 * @description Initializes events on the product-tile for the following elements:
 * - swatches
 * - thumbnails
 */
function initializeEvents() {
	if (SitePreferences.isQVenabled) {
		initQuickViewButtons();
	}
	gridViewToggle();
	compareToolPadding();
	$('.swatch-list').on('mouseleave', function () {
		// Restore current thumb image
		var $tile = $(this).closest('.product-tile'),
			$thumb = $tile.find('.product-image .thumb-link img').eq(0),
			data = $thumb.data('current');

		$thumb.attr({
			src: data.src,
			alt: data.alt,
			title: data.title
		});
	});
	$('.swatch-list .swatch').on('click', function (e) {
		if ($(this).hasClass('selected')) { return; }
		if (!$(this).hasClass('selected')) {
			e.preventDefault();
		}
		var $tile = $(this).closest('.product-tile');
		$(this).closest('.swatch-list').find('.swatch.selected').removeClass('selected');
		$(this).addClass('selected');
		$tile.find('.thumb-link').attr('href', $(this).attr('href'));
		$tile.find('name-link').attr('href', $(this).attr('href'));

		var data = $(this).children('img').filter(':first').data('thumb');
		var $thumb = $tile.find('.product-image .thumb-link img').eq(0);
		var currentAttrs = {
			src: data.src,
			alt: data.alt,
			title: data.title
		};
		$thumb.attr(currentAttrs);
		$thumb.data('current', currentAttrs);
	}).on('mouseenter', function () {
		// get current thumb details
		var $tile = $(this).closest('.product-tile'),
			$thumb = $tile.find('.product-image .thumb-link img').eq(0),
			data = $(this).children('img').filter(':first').data('thumb'),
			current = $thumb.data('current');

		// If this is the first time, then record the current img
		if (!current) {
			$thumb.data('current', {
				src: $thumb[0].src,
				alt: $thumb[0].alt,
				title: $thumb[0].title
			});
		}

		// Set the tile image to the values provided on the swatch data attributes
		$thumb.attr({
			src: data.src,
			alt: data.alt,
			title: data.title
		});
	});
}

exports.init = function () {
	var $tiles = $('.tiles-container .product-tile');
	if ($tiles.length === 0) { return; }
	imagesLoaded('.product-image').on('done', function () {
		/*$tiles.syncHeight()
			.each(function (idx) {
				idx = idx;
				$(this).data('idx', idx);
			});*/
	});
	ResponsiveSlots.smartResize(function () {
		$tiles
		.each(function () {
			$(this).removeAttr('style');
		});
	});
	initializeEvents();
};

/* 
*  This is needed in order re-init the layout of the product tile
*  with compare enabled after inifinte scroll loads.
*/
exports.initCompare = function () {
	compareToolPadding();
	var util = require('./util');
	util.uniform();
};

/* 
*  This initializes the comfort level indicators for the MFRM
*  sub-category landing pages. Needs to be initialized after
*  each infinite scroll load.
*/
exports.initComfortLevelTagging = function () {
	$('.comfort-level.mattressfirm').find('span').each(function() {
		var _this = $(this);
		var comfortLevelClass = _this.attr('class');
		if (comfortLevelClass == _this.data('comfortlevel')) {
			_this.addClass('active');
			_this.prev('span').css('border', 'none');
			_this.on('click', function() {
				window.location = _this.data('pdplink');
			});
		}
	});
};

// add carousel init to the global window object so that the asyncronous carousel can access it
window.productRecs = function() {

	var container = {
		initialize: function($el) {
			this.$el = $el;
		},
		showCarousel: function() {
			var dfd = new $.Deferred();

			var recs = this.$el.find('li');
			var cqSlot = this.$el.closest('div[id^="cq_recomm_slot"]');
			if (window.CQuotient && recs.length > 0 && cqSlot.length > 0) {
				dfd.resolve();
			}

			return dfd.promise();
		}
	};

	container.initialize($('.recs-slider'));

	var promise = container.showCarousel();
	promise.done(function() {
		$('.product-listing-1x4 h2').css('visibility', 'visible');
		$('.recs-slider').slick({
			infinite: false,
			speed: 300,
			slide: '.grid-tile',
			slidesToShow: 4,
			slidesToScroll: 4,
			responsive: [{
				breakpoint: 1025,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}]
		});
	});

};