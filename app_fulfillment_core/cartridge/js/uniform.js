'use strict';

var uniform = {
	init: function () {
		this.uniform();
	},

	update: function () {
		$('.uniform').uniform.update();
	},

	uniform: function () {
		$('.uniform').uniform({
			selectAutoWidth: true
		});
	}
};

module.exports = uniform;
