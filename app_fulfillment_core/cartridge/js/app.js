/**
 *    (c) 2009-2014 Demandware Inc.
 *    Subject to standard usage terms and conditions
 *    For all details and documentation:
 *    https://bitbucket.com/demandware/sitegenesis
 */

'use strict';

var countries = require('./countries'),
	dialog = require('./dialog'),
	minicart = require('./minicart'),
	page = require('./page'),
	rating = require('./rating'),
	searchplaceholder = require('./searchplaceholder'),
	searchsuggest = require('./searchsuggest'),
	searchsuggestbeta = require('./searchsuggest-beta'),
	tooltip = require('./tooltip'),
	scrollTop = require('./scroll-top'),
	hoverIntent = require('./hoverintent'),
	util = require('./util'),
	validator = require('./validator'),
	content = require('./content'),
	scheduleDelivery = require('./schedule-delivery'),
	schedulePickup = require('./schedule-pickup'),
	createeditevents = require('./createeditevents'),
	searchOrders = require('./search-orders'),
	welcomeMessage = require('./welcome-message'),
	dashboard = require('./dashboard'),
	billing = require('./pages/checkout/billing'),
	searchUsers= require('./pages/searchusers');

// if jQuery has not been loaded, load from google cdn
if (!window.jQuery) {
	var s = document.createElement('script');
	s.setAttribute('src', 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js');
	s.setAttribute('type', 'text/javascript');
	document.getElementsByTagName('head')[0].appendChild(s);
}

require('./jquery-ext')();
require('./cookieprivacy')();
require('./captcha')();

function initializeEvents() {
	var controlKeys = ['8', '13', '46', '45', '36', '35', '38', '37', '40', '39'];

	$('body')
		.on('keydown', 'textarea[data-character-limit]', function (e) {
			var text = $.trim($(this).val()),
				charsLimit = $(this).data('character-limit'),
				charsUsed = text.length;

				if ((charsUsed >= charsLimit) && (controlKeys.indexOf(e.which.toString()) < 0)) {
					e.preventDefault();
				}
		})
		.on('change keyup mouseup', 'textarea[data-character-limit]', function () {
			var text = $.trim($(this).val()),
				charsLimit = $(this).data('character-limit'),
				charsUsed = text.length,
				charsRemain = charsLimit - charsUsed;

			if (charsRemain < 0) {
				$(this).val(text.slice(0, charsRemain));
				charsRemain = 0;
			}

			$(this).next('div.char-count').find('.char-remain-count').html(charsRemain);
		});

	/**
	 * initialize search suggestions, pending the value of the site preference(enhancedSearchSuggestions)
	 * this will either init the legacy(false) or the beta versions(true) of the the search suggest feature.
	 * */
	var $searchContainer = $('.header-search');
	if (SitePreferences.LISTING_SEARCHSUGGEST_LEGACY) {
		searchsuggest.init($searchContainer, Resources.SIMPLE_SEARCH);
	} else {
		searchsuggestbeta.init($searchContainer, Resources.SIMPLE_SEARCH);
	}

	$('.secondary-navigation .toggle').click(function () {
		$(this).toggleClass('expanded').next('ul').toggle();
	});
	// add generic toggle functionality
	$('.toggle').next('.toggle-content').hide();
	$('.toggle').click(function () {
		$(this).toggleClass('expanded').next('.toggle-content').toggle();
	});
	// subscribe email box
	var $subscribeEmail = $('.subscribe-email');
	if ($subscribeEmail.length > 0)	{
		$subscribeEmail.focus(function () {
			var val = $(this.val());
			if (val.length > 0 && val !== Resources.SUBSCRIBE_EMAIL_DEFAULT) {
				return; // do not animate when contains non-default value
			}

			$(this).animate({color: '#999999'}, 500, 'linear', function () {
				$(this).val('').css('color', '#333333');
			});
		}).blur(function () {
			var val = $.trim($(this.val()));
			if (val.length > 0) {
				return; // do not animate when contains value
			}
			$(this).val(Resources.SUBSCRIBE_EMAIL_DEFAULT)
				.css('color', '#999999')
				.animate({color: '#333333'}, 500, 'linear');
		});
	}

	// main menu toggle
	$('.menu-toggle').on('click', function () {
		$('#wrapper').toggleClass('menu-active');
		$('footer').toggleClass('footer-active');
	});
	$('.menu-category li .menu-item-toggle').on('click', function (e) {
		e.preventDefault();
		var $parentLi = $(e.target).closest('li');
		$parentLi.siblings('li').removeClass('active').find('.menu-item-toggle').removeClass('fa-chevron-down active').addClass('fa-chevron-right');
		$parentLi.toggleClass('active');
		$(e.target).toggleClass('fa-chevron-right fa-chevron-down active');
	});

    //top navigation pause
    var hoverconfig = {
        over: function () {
            $(this).find('div.level-2').css('display', 'block');
            $(this).children('a.has-sub-menu').addClass('open');
        },
        out: function () {
            $(this).find('div.level-2').css('display', 'none');
            $(this).children('a.has-sub-menu').removeClass('open');
        },
        timeout: 100,
        sensitivity: 3,
        interval: 100
    };

	var touchConfig = function () {
		$(document).off('touchend', '.menu-category > li > a');
		// fix for touch tablet devices: prevent first click
		if (util.isMobile() == true && screen.width > util.mobileWidth) {
			$(document).on('touchend', '.menu-category > li > a', function (e) {
				if ($(this).siblings('.level-2').is(':hidden') == true) {
					e.preventDefault();
					// hide all opened menus
					$('.menu-category > li').find('.level-2').css('display', 'none');
					$('.menu-category > li').children('a.has-sub-menu').removeClass('open');

					//open current submenu
					$(this).siblings('.level-2').css('display', 'block');
					$(this).addClass('open');
					return false;
				}
			})
		}
	};

	touchConfig();

    if (screen.width > util.mobileWidth) {
        $('.level-1 li').hoverIntent(hoverconfig);
    }
    $(window).resize(function () {
        if (screen.width > util.mobileWidth) {
            $('.level-1 li').removeClass('active').hoverIntent(hoverconfig);
        } else {
            $('.level-1 li').unbind("mouseenter").unbind("mouseleave");
            $('.level-1 li').removeProp('hoverIntentT');
            $('.level-1 li').removeProp('hoverIntentS');
		}
		touchConfig();
	});
    $('.print-page').on('click', function () {
		window.print();
		return false;
	});
    function validatesignupEmail(email) {
		var $continue = $('.home-email');
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		if (!emailReg.test(email)) {
			//$continue.attr('disabled', 'disabled');
			if ($(".emailsignup #email-alert-address-error").length < 1) {
				if ($(".emailsignup .error2").length > 0) {
					$(".emailsignup .error2").remove();
				}
				$(".emailsignup form").append("<span id='email-alert-address-error' class='error'>" + Resources.VALIDATE_EMAIL + "</span>");
			} else {
				if ($(".emailsignup .error2").length > 0) {
					$(".emailsignup .error2").remove();
				}
				if ($(".emailsignup #email-alert-address-error").text().length == 0) {
					$(".emailsignup #email-alert-address-error").html(Resources.VALIDATE_EMAIL);
					$(".emailsignup form").append("<span id='email-alert-address-error' class='error2'>" + Resources.VALIDATE_EMAIL + "</span>");
				} else {
					//console.log('22' + $(".emailsignup #email-alert-address-error").length);
				}
			}
			$("#email-alert-address-error").css('display','block');
			return false;
		} else {
			//$continue.removeAttr('disabled');
			if ($(".emailsignup #email-alert-address-errorr").length > 0) {
				$(".emailsignup form #email-alert-address-error").remove();
			}
			if ($(".emailsignup .error2").length > 0) {
				$(".emailsignup .error2").remove();
			}
			return true;
		}
	}
	/*email signup Ajax*/
	$('#email-alert-signup').on('submit', function (e) {
		e.preventDefault();
		var $this = $(this),
			emailSignupInput = $(this).find('#email-alert-address'),
			validationErrorBlock = $('#email-alert-address-error');
		var signupInputValue = $(this).find(emailSignupInput).val();
		var validatetest = validatesignupEmail(signupInputValue);
		if (validatetest && signupInputValue.length > 1) {
			var emailPageSource = $(this).data('pagesource');
			var params = {email: signupInputValue, pageSource: emailPageSource};
				$.ajax({
					url: Urls.emailAjaxSubscription,
					data: params,
					type: 'post',
					success: function (data, url) {
						$this.hide().parent().append($(data));
						//console.log('test2=' + url);
					},
					error: function (errorThrown) {
						//console.log(errorthrown);
					}
				});
		}
	});
}
/**
 * @private
 * @function
 * @description Adds class ('js') to html for css targeting and loads js specific styles.
 */
function initializeDom() {
	// add class to html for css targeting
	$('html').addClass('js');
	if (SitePreferences.LISTING_INFINITE_SCROLL) {
		$('html').addClass('infinite-scroll');
	}
	// load js specific styles
	util.limitCharacters();
}

var pages = {
	account: require('./pages/account'),
	cart: require('./pages/cart'),
	checkout: require('./pages/checkout'),
	compare: require('./pages/compare'),
	product: require('./pages/product'),
	registry: require('./pages/registry'),
	search: require('./pages/search'),
	storefront: require('./pages/storefront')
};

var app = {
	init: function () {
		if (document.cookie.length === 0) {
			$('<div/>').addClass('browser-compatibility-alert').append($('<p/>').addClass('browser-error').html(Resources.COOKIES_DISABLED)).appendTo('#browser-check');
		}
		hoverIntent.init();
		initializeDom();
		initializeEvents();

		// init specific global components
		countries.init();
		tooltip.init();
		scrollTop.init();
		minicart.init();
		validator.init();
		rating.init();
		searchplaceholder.init();
		util.uniform();
		content.init();
		scheduleDelivery.init();
		schedulePickup.init();
		searchOrders.init();
		dashboard.init();
		billing.init();
		searchUsers.init();
		
		// execute page specific initializations
		$.extend(page, window.pageContext);
		var ns = page.ns;
		if (ns && pages[ns] && pages[ns].init) {
			pages[ns].init();
		}
		// Initialize the Google Analytics library
		/*if (typeof window.DW.googleAnalytics === 'object') {
			googleAnalytics.init(ns, window.DW.googleAnalytics.config);
		}*/
		//responsiveSlots.init(ns || '');
	}
};

// general extension functions
(function () {
	String.format = function () {
		var s = arguments[0];
		var i, len = arguments.length - 1;
		for (i = 0; i < len; i++) {
			var reg = new RegExp('\\{' + i + '\\}', 'gm');
			s = s.replace(reg, arguments[i + 1]);
		}
		return s;
	};
})();

// initialize app
$(document).ready(function () {
	app.init();
});
