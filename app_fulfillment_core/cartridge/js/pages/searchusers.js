'use strict';

var util = require('../util');
var Rules = require('../validation-rules');

exports.init = function() {
	// On click: Search
	$("#search-users-btn").click(function(e) {
		e.preventDefault();
		showSearchResults();
	});
	
	// On Enter key press in input field
	$('#search-users-by-term').keypress(function(e) {
	    if(e.which == 13) {
	    	e.preventDefault();
	    	showSearchResults();
	    }
	});
	
	// On click Cancel
	$("#cancel-search-users-btn").click(function(e) {
		e.preventDefault();
		//$('#search-users-name').click(); // Select Search by Name
		$('#search-users-by-term').val(''); // Make the Search term empty
	});
	
	
	// Fetch the customer results from Business Manager and show on Search Users result page
	function showSearchResults() {
		var searchBy;
		if ( $('input[name=search-users-by]:checked').val() == 'search-users-name') {
			searchBy = 'name';
		} else {
			searchBy = 'email';
		}
		
		var value = $('#search-users-by-term').val();
		if(value && value.trim() != '') {
			$('#search-user-results').hide();
			//$('#no-results').hide();
			$('#no-results').addClass('visually-hidden');
			$('#loading-wait').show();
			
			$.ajax({
				url: util.appendParamsToUrl(Urls.fulfillmentSearchUsers, {searchBy: searchBy, searchValue: value, format: 'ajax'}),
				type	: 'GET',
				success: function (response) {
			    	if (response && response.length  > 0) {
			    		var table = $('#search-users-table > tbody');
			    		table.empty();
			    		for(var i=0; i<response.length; i++) {
			    			var customer = response[i];
			    			var editUrl =util.appendParamsToUrl(Urls.EditUserProfile,{"email":customer.email}); 
			    			table.append('<tr><td>' + customer.name + '</td>' + '<td>'  + customer.email + 
			    					'</td><td><a target="_blank" href="' + editUrl +  '">Edit</td></tr>');
			    		}
				    	$('#loading-wait').hide();
			    		$('#search-user-results').show();
						//$('#no-results').hide();
						$('#no-results').addClass('visually-hidden');
					} else {
				    	$('#loading-wait').hide();
						$('#search-user-results').hide();
						//$('#no-results').show();
						$('#no-results').removeClass('visually-hidden');
					}
				},
				error: function(xhr, status, error){
					util.handleAjaxFailure(xhr, status, error, {continueUrl: Urls.fulfillmentSearchUsersShow});
				}
			});
		}
	}
};
