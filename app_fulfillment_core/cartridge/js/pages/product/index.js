'use strict';

var dialog = require('../../dialog'),
	productStoreInventory = require('../../storeinventory/product'),
	tooltip = require('../../tooltip'),
	util = require('../../util'),
	addToCart = require('./addToCart'),
	availability = require('./availability'),
	image = require('./image'),
	productNav = require('./productNav'),
	productSet = require('./productSet'),
	recommendations = require('./recommendations'),
	variant = require('./variant'),
	updatePDPPrice = require('./updatePDPPrice'),
	ajax = require('../../ajax');

/**
 * @description Initialize product detail page with reviews, recommendation and product navigation.
 */
function initializeDom() {
	productNav();
	recommendations();
	tooltip.init();
}

/**
 * @description Initialize event handlers on product detail page
 */
function updatePDPTabs() {
	if (util.isMobileSize()) {
		$('.tabs').find('input:radio').each(function () {
			$("<input type='checkbox' />").addClass('tab-switch').attr({
				name: this.name,
				value: this.value,
				id: this.id
			}).insertBefore(this);
		}).remove();
	}
}

function initializeEvents() {
	var $pdpMain = $('#pdpMain');

	addToCart();
	availability();
	variant();
	image();
	productSet();
	productStoreInventory.init();

	// Add to Wishlist and Add to Gift Registry links behaviors
	$pdpMain.on('click', '[data-action="wishlist"], [data-action="gift-registry"]', function () {
		var data = util.getQueryStringParams($('.pdpForm').serialize());
		if (data.cartAction) {
			delete data.cartAction;
		}
		var url = util.appendParamsToUrl(this.href, data);
		this.setAttribute('href', url);
	});

	// product options
	$pdpMain.on('change', '.product-options select', function () {
		updatePDPPrice();
        if (typeof(personaliDataLayer) != 'undefined' && personaliDataLayer && personaliDataLayer[0]) {
            personaliDataLayer[0].option.price = selectedItem.data('option-price');
            personaliDataLayer[0].option.sku = selectedItem.val();
        }
        var updateATPAvailability = new CustomEvent('updateATPAvailability');
        window.dispatchEvent(updateATPAvailability);
	});

	$pdpMain.on('change', '.quantity select.quantity-dropdown', function () {
		var updateATPAvailability = new CustomEvent('updateATPAvailability');
		window.dispatchEvent(updateATPAvailability);
	});

	window.addEventListener('updateATPAvailability', function (e) {
		var $pdpForm = $('.pdpForm');
		var qty = $pdpForm.find('select[name="Quantity"]').first().val();
		var params = {
			qty: isNaN(qty) ? '1' : qty,
			productId: $pdpForm.find('input[name="pid"]').first().val(),
			optionId: $pdpForm.find('.product-options .product-option').first().val()
		};
		$('#optionValue').val(params.optionId);
		ajax.load({
			url: util.appendParamsToUrl(Urls.getATPAvailabilityMessage, params),
			target: $('.availability-web')
		});
	});

	// prevent default behavior of thumbnail link and add this Button
	$pdpMain.on('click', '.thumbnail-link, .unselectable a', function (e) {
		e.preventDefault();
	});

	$('.size-chart-link a').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href')
		});
	});
	if (($('.all-product-promos').children().length > 0 || $('.single-finance-promo').children().length > 0) && $(window).width() < 1025) {
		$('.see-more-offers').css('display', 'block');
	}
	$('.see-more-offers').on('click', function () {
		$('.more-promos').fadeIn();
		$(this).css('display', 'none');
		$('.see-fewer-offers').css('display', 'block');
	});
	$('.see-fewer-offers').on('click', function () {
		$('.more-promos').hide();
		$(this).css('display', 'none');
		$('.see-more-offers').css('display', 'block');
	});
	$('.tab-label').on('keypress', function (e) {
		if (e.keyCode === 32 || e.keyCode === 13) {
			var tabID = $(this).prop('for');
			$('.tab-switch').prop('checked',false);
			$("#" + tabID).prop('checked',true);
		}
	});
}

var product = {
	initializeEvents: initializeEvents,
	init: function () {
		initializeDom();
		initializeEvents();
		updatePDPTabs();
		updatePDPPrice();
	}
};

module.exports = product;
