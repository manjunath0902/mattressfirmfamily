'use strict';

var dialog = require('./../../dialog'),
	page = require('./../../page'),
	util = require('./../../util'),
	minicart = require('../../minicart');

var hideSwatches = function () {
	$('.recommendation-item:not([data-producttype="master"]) .swatches li').not('.selected').not('.variation-group-value').hide();
	// prevent unselecting the selected variant
	$('.recommendation-item .swatches .selected').on('click', function () {
		return false;
	});
};

function initializeRecommendationGrid () {
	var $recommendationList = $('.add-to-cart-intercept-wrapper .recommendations');
	$recommendationList.on('change', '.swatches', function (e) {
		e.preventDefault();
		var url = $(this).val(),
			$this = $(this);
		url = util.appendParamsToUrl(url, {
			'source': 'recommendation',
			'format': 'ajax'
		});
		$.ajax({
			url: url,
			success: function (response) {
				$this.closest('.recommendation-item').empty().html(response);
				util.uniform();
			}
		});
	})
	.on('click', '.add-recommendations-item', function (e) {
		e.preventDefault();
		var pid = $(this).closest('form').find('input[name="pid"]').val();
		var url = util.appendParamsToUrl(util.ajaxUrl(Urls.addProduct), {pid: pid, source: 'recommendation'});
		var $this = $(this);
		// make the server call
		$.ajax({
			type: 'POST',
			cache: false,
			url: url,
			data: $this.closest('form').serialize()
		})
		.done(function (response) {
			minicart.show(response);
			$this.next('.add-to-cart-resp').text('Added to Cart');
		})
		.fail(function (xhr, textStatus) {
			if (textStatus === 'parsererror') {
				window.alert(Resources.BAD_RESPONSE);
			} else {
				window.alert(Resources.SERVER_CONNECTION_ERROR);
			}
		});
	});
}

var addToCartIntercept = {
	show: function () {
		$('.price-standard').next().css("color", "#d63426");
		$('.Adjusted-Price').prev().css( "margin-left", "5px" );
		var block = $('#mini-cart .add-to-cart-intercept-wrapper');
		initializeRecommendationGrid();

		if (block.size() > 0) {
			dialog.open({
				html: block,
				options: {
					width: 840,
					title: Resources.YOU_JUST_ADDED,
					dialogClass: 'add-to-cart-intercept-dialog',
					close: function () {
						$(this).find('.add-to-cart-intercept-wrapper').remove();
					},
					open: function () {
						var dialog = $(this);
						$('.ui-dialog-buttonpane').remove();
						util.uniform();
						$(this).find('a.continue').on('click', function (e) {
							e.preventDefault();
							location.reload(true);
							dialog.dialog('close');
						});
					}
				}
			});
		}
	}
};

module.exports = addToCartIntercept;