'use strict';

var amplience = require('../../amplience'),
	ajax = require('../../ajax'),
	image = require('./image'),
	progress = require('../../progress'),
	productStoreInventory = require('../../storeinventory/product'),
	tooltip = require('../../tooltip'),
	util = require('../../util'),
	updatePDPPrice = require('./updatePDPPrice');


/**
 * @description update product content with new variant from href, load new content to #product-content panel
 * @param {String} href - url of the new product variant
 **/
var updateContent = function (href, ampset) {
	if (ampset == null) {
		var ampset = '';
		}
	var $pdpForm = $('.pdpForm');
	var qty = $pdpForm.find('input[name="Quantity"]').first().val();
	var params = {
		Quantity: isNaN(qty) ? '1' : qty,
		format: 'ajax',
		productlistid: $pdpForm.find('input[name="productlistid"]').first().val()
	};

	if ( SitePreferences.CURRENT_SITE !== "Mattress-Firm" ) {
		progress.show($('#pdpMain'));
	}
	ajax.load({
		url: util.appendParamsToUrl(href, params),
		target: $('#product-content'),
		callback: function () {
			if (SitePreferences.STORE_PICKUP) {
				productStoreInventory.init();
			}
			if (ampset != '') {
				image.replaceImages();
				amplience.initZoomViewer(ampset);
			}
			tooltip.init();
			util.uniform();
			updateLabel();
			$(document).trigger("pdpContentUpdateEvent");
			focusOnPDPdropDown();
			updatePDPPrice();
            progress.hide($('#pdpMain'));
            if ( SitePreferences.CURRENT_SITE === "Mattress-Firm" ) {
                loadATPAvailabilityMessage();
            }
		}
	});
};
function loadATPAvailabilityMessage() {
	$(".availability-web").append('<div class="loader-indicator-atp"></div>');
	var pid = $('#pid').val();
	var url = util.appendParamsToUrl(Urls.getATPAvailabilityMessageAjax, {productId: pid,qty: 1, format: 'ajax'});
	$.getJSON(url, function (data) {
		var fail = false;
		var msg = '';
		if (!data) {
			msg = Resources.BAD_RESPONSE;
			fail = true;
		} else if (!data.success) {
			fail = true;
		}
		if (fail) {
			//$error.html(msg);
			return;
		}
		if (data.success) {
			if ($('#replaceMeID').length) {
				if (data.availabilityClass) {
					$("#replaceMeID").removeClass();
					$('#replaceMeID').addClass(data.availabilityClass);
				}
			}
			if (data.showATPMessage) { /* !pdict.Product.master 
										&& !pdict.Product.variationGroup
										&& pdict.Product.custom.shippingInformation.toLowerCase() == 'core' 
										&& dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpAvailabilityMessage') 
										&& session.custom.customerZip} */
				if (data.availabilityDate != null && data.isInStock === true) {
					var availabilitySuccess = Resources.ATP_AVAILABILITY_SUCCESS;
					var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
					
					//2018-04-05T00:00:00.000Z
					var dateTime = data.availabilityDate;
					var dateOnly = dateTime.split('T');
					var ymd = dateOnly[0].split('-');
					var date = ymd[2];
					var month = months[Number(ymd[1])-1];
					var availabilityDateString = month + ' ' + date;
					if (data.dateDifferenceString != '') {
						availabilitySuccess = availabilitySuccess.replace("tempDate", data.dateDifferenceString + ' ' + availabilityDateString);
					} else {
						availabilitySuccess = availabilitySuccess.replace("tempDate", availabilityDateString);
					}
					$('#replaceMeID').find('p').text(availabilitySuccess);
					
					if (data.detailsContentId != '') {
						var anchorDetails = $('#replaceMeID').find('a');
						if (anchorDetails.length == 0) {
							$("#replaceMeID").find('p').after("<a></a>");
						}
						anchorDetails = $('#replaceMeID').find('a');
						var hrefDetails = util.appendParamsToUrl(Urls.pageShow, {cid: data.detailsContentId});
						anchorDetails.attr("href", hrefDetails);
						anchorDetails.addClass('details');
						anchorDetails.prop('title', 'details');
						anchorDetails.text('Details');
					}
				} else if ($('#replaceMeID').length) {
					$("#replaceMeID").parent().remove();
					getDefaultAvailabilityMessageAjax(pid);
				}
			} else if ($('#replaceMeID').length) {
				$("#replaceMeID").parent().remove();
				getDefaultAvailabilityMessageAjax(pid);
			}
			$(".availability-web .loader-indicator-atp").remove();
		}
	});
}

function getDefaultAvailabilityMessageAjax(pid) {
	var availabilityMessageSection = $('.availability-web');
	var url = util.appendParamsToUrl(Urls.getDefaultAvailabilityMessage, {productId: pid});
	availabilityMessageSection.load(url, function () {
	});
}

function focusOnPDPdropDown() {
	$(".select-variation-dropdown").focus();
}
var updateLabel = function () {
	$('.product-variations .attribute').each(function (e) {
		var attributeTitle = $(this).find('.swatches').find(':selected').attr('title');
		if ($(this).find('.selected-attr-value').length == 0 && attributeTitle != '') {
			var attributeValue = '<span class="selected-attr-value">' + attributeTitle + "</span>";
			$(attributeValue).appendTo($(this).find('.label'));
		}
	});
}

module.exports = function () {
	var $pdpMain = $('#pdpMain');
	// hover on swatch - should update main image with swatch image
	$pdpMain.on('mouseenter mouseleave', '.swatchanchor', function () {
		var largeImg = $(this).data('lgimg'),
			$imgZoom = $pdpMain.find('.main-image'),
			$mainImage = $pdpMain.find('.primary-image');

		if (!largeImg) { return; }
		// store the old data from main image for mouseleave handler
		$(this).data('lgimg', {
			hires: $imgZoom.attr('href'),
			url: $mainImage.attr('src'),
			alt: $mainImage.attr('alt'),
			title: $mainImage.attr('title')
		});
		// set the main image
		image.setMainImage(largeImg);
	});

	// click on swatch - should replace product content with new variant
	$pdpMain.on('change', '.swatches', function (e) {
		//Amplience image logic - if size is clicked, do nothing.  If color is clicked and "selected," Look for color.
		//Note - Amplience breaks if the image doesn't exist.  They have no "size" based images, it would seem.  So, if size is clicked, then do nothing.
		if ($(this).hasClass("color")) {
			//If the swatch that was clicked has the class "selected" at this point, the swatch is in process of being deselected.
			if ($(this).hasClass("selected")) {
				var ampset = "";
			} else {
				//The swatch is being selected at this point, so look for that color-based image.
				var ampset = $(this).find(':selected').data('ampset');
			}
		}
		updateContent($(this).val(), ampset);
	});

	// change drop down variation attribute - should replace product content with new variant
	$pdpMain.on('change', '.variation-select', function () {
		if ($(this).val().length === 0) { return; }
		updateContent($(this).val(),"");
	});
	util.uniform();
	updateLabel();
	if ( SitePreferences.CURRENT_SITE === "Mattress-Firm" ) {
        loadATPAvailabilityMessage();
    }
};
