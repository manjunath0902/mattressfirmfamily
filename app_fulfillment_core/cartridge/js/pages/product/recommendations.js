'use strict';

/**
 * @description Creates product recommendation carousel using jQuery jcarousel plugin
 **/
module.exports = function () {
	var util = require('../../util');	//ResponsiveSlots = require('../../responsiveslots/responsiveSlots'),
		

	var initThisColection = function () {
		var containerFirst = $('#carousel-recommendations ul');
		containerFirst.slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			infinite: false,
			variableWidth: false,
			responsive: [
				{
					breakpoint: util.tabletWidth,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: util.mobileWidth,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		});
		var	stHeight = $('#carousel-recommendations').find('.this-collection');
				var blockHeight = '380';
				stHeight.css({'height':''});
				stHeight.each(function () {
					var height = $(this).outerHeight();
					if (height > blockHeight && height > 380) {
						blockHeight = height;
					}
				});
				stHeight.each(function () {
					$(this).css({'height': + blockHeight});
				});
	}

	var initProPicks = function () {
		var containerSecond = $('#propicks-recommendations ul');
		containerSecond.slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			infinite: false,
			variableWidth: false,
			responsive: [
				{
					breakpoint: util.tabletWidth,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: util.mobileWidth,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		});
		var	stHeight = $('#propicks-recommendations').find('.propicks-item');
		var blockHeight = '380';
		stHeight.css({'height':''});
		stHeight.each(function () {
			var height = $(this).outerHeight();
			if (height > blockHeight && height > 380) {
				blockHeight = height;
			}
		});
		stHeight.each(function () {
			$(this).css({'height': + blockHeight});
		});
	}
	initThisColection();
	initProPicks();
};
