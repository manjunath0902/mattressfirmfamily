'use strict';

var ajax = require('../../ajax'),
    formPrepare = require('./formPrepare'),
    progress = require('../../progress'),
    tooltip = require('../../tooltip'),
    util = require('../../util'),
    page = require('../../page'),
    dialog = require('../../dialog');

var shippingMethods;
/**
 * @function
 * @description Initializes gift message box, if shipment is gift
 */
function giftMessageBox() {
    // show gift message box, if shipment is gift
    $('.gift-message-text').toggleClass('hidden', $('input[name$="_shippingAddress_isGift"]:checked').val() !== 'true');
}

/**
 * @function
 * @description updates the order summary based on a possibly recalculated basket after a shipping promotion has been applied
 */
function updateSummary() {
    var $summary = $('#secondary.summary');
    // indicate progress
    progress.show($summary);

    // load the updated summary area
    $summary.load(Urls.summaryRefreshURL, function () {
        // hide edit shipping method link
        $summary.fadeIn('fast');
        $summary.find('.checkout-mini-cart .minishipment .header a').hide();
        $summary.find('.order-totals-table .order-shipping .label a').hide();
        $('.checkout-mini-cart').mCustomScrollbar();
//		util.uniform();
//		var checkoutForm = $("form.address"),
//		city = checkoutForm.find('select[name$="_addressFields_cities_city"]'),
//		$formRow = city.closest(".form-row");
//		$formRow.find('#uniform-' + city.attr("name")).show();
//		city.show();
    });
}

/**
 * @function
 * @description Helper method which constructs a URL for an AJAX request using the
 * entered address information as URL request parameters.
 */
function getShippingMethodURL(url, extraParams) {
    var $form = $('.address');
    return util.appendParamsToUrl(url, extraParams);
}

/**
 * @function
 * @description selects a shipping method for the default shipment and updates the summary section on the right hand side
 * @param
 */
function selectShippingMethod(shippingMethodID) {
    // nothing entered
    if (!shippingMethodID) {
        return;
    }
    // attempt to set shipping method
    var url = getShippingMethodURL(Urls.selectShippingMethodsList, {shippingMethodID: shippingMethodID});
    ajax.getJson({
        url: url,
        callback: function (data) {
            updateSummary();
            if (!data || !data.shippingMethodID) {
                window.alert('Couldn\'t select shipping method.');
                return false;
            }
            // display promotion in UI and update the summary section,
            // if some promotions were applied
            $('.shippingpromotions').empty();

            // TODO the for loop below isn't doing anything?
            // if (data.shippingPriceAdjustments && data.shippingPriceAdjustments.length > 0) {
            // 	var len = data.shippingPriceAdjustments.length;
            // 	for (var i=0; i < len; i++) {
            // 		var spa = data.shippingPriceAdjustments[i];
            // 	}
            // }
        }
    });
}

/**
 * @function
 * @description Make an AJAX request to the server to retrieve the list of applicable shipping methods
 * based on the merchandise in the cart and the currently entered shipping address
 * (the address may be only partially entered).  If the list of applicable shipping methods
 * has changed because new address information has been entered, then issue another AJAX
 * request which updates the currently selected shipping method (if needed) and also updates
 * the UI.
 */
function updateShippingMethodList() {
    var $shippingMethodList = $('#shipping-method-list');
    if (!$shippingMethodList || $shippingMethodList.length === 0) { return; }
    var url = getShippingMethodURL(Urls.shippingMethodsJSON);

    ajax.getJson({
        url: url,
        callback: function (data) {
            if (!data) {
                window.alert('Couldn\'t get list of applicable shipping methods.');
                return false;
            }
            if (shippingMethods && shippingMethods.toString() === data.toString()) {
                // No need to update the UI.  The list has not changed.
                return true;
            }

            // We need to update the UI.  The list has changed.
            // Cache the array of returned shipping methods.
            shippingMethods = data;
            // indicate progress
            progress.show($shippingMethodList);

            // load the shipping method form
            var smlUrl = getShippingMethodURL(Urls.shippingMethodsList);
            $shippingMethodList.load(smlUrl, function () {
                $shippingMethodList.fadeIn('fast');
                // rebind the radio buttons onclick function to a handler.
                $shippingMethodList.find('[name$="_shippingMethodID"]').click(function () {
                    selectShippingMethod($(this).val());
                });

                // update the summary
                updateSummary();
                progress.hide();
                tooltip.init();
                util.uniform();
                //if nothing is selected in the shipping methods select the first one
                if ($shippingMethodList.find('.input-radio:checked').length === 0) {
                    $shippingMethodList.find('.input-radio:first').prop('checked', 'checked');
                }
            });
        }
    });
}

function LoadCityStateDropdowns(data) {
    var checkoutForm = $("form.address");
    var postalCode = checkoutForm.find("input[name$='_postal']");
    var city = checkoutForm.find('select[name$="_addressFields_cities_city"]');
    var state = checkoutForm.find('select[name$="_addressFields_states_state"]');
    var cityJSON = checkoutForm.find("input.cityJSON");
    if (data !== null) {
        city.empty();

        // Add "Choose a City"
        if (data.cities.length > 1) {
            city.append($("<option />").attr("value", "").text("Select City"));
        }

        for (var __i = 0; __i < data.cities.length; __i++) {
            var $opt = $("<option />");
            $opt.attr("value", data.cities[__i].city).text(data.cities[__i].city);

            // If there is only a single city object returned, auto select it.
            if (data.cities.length === 1) {
                $opt.attr("selected", "selected");
                city.val(data.cities[__i].city);
            }

            city.append($opt);

            if (__i === 0) {
                state.val(data.cities[__i].state);

                // Reset any error conditions on the state field
                state.closest(".form-row").removeClass("error").find(".error-message").hide();

                // State selected so hide drop down
                $(".state-col .dk_options_inner").css("display","none");
            }
        }
        // Enable the city/state select elements so they are submitted with form.
        city.removeAttr("disabled");
        state.removeAttr("disabled");
    }
}

function GetCityStateFromZip(zipCode) {
    var checkoutForm = $("form.address");
    var postalCode = checkoutForm.find("input[name$='_addressFields_postal']");
    var city = checkoutForm.find('select[name$="_addressFields_cities_city"]');
    var state = checkoutForm.find('select[name$="_addressFields_states_state"]');
    var country = checkoutForm.find('select[name$="_addressFields_country"]');
    var cityJSON = checkoutForm.find("input.cityJSON");
    // 1. Verify a valid US 5-digit ZIP first, and that the same ZIP was not entered again
    if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCode)) { //&& zipCode != app.clientcache.SESSION_ZIP){

        postalCode.removeClass('error');
        postalCode.next('span.error').remove();
        $.ajax({
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: Urls.cityAndStateForZip,
            async: true,
            cache: false,
            data: {"zipcode": zipCode},

            beforeSend: function () {
                progress.show(".zip-city-state");
            },
            success: function (data) {
                if (data != null && data.cities.length > 0) {

                    // RESET : Make sure to enable the select if disabled and remove any fail over input element and reset cache for element.
                    var $formRow = city.closest(".form-row");
                    $formRow.find("input").remove();
                    $formRow.find("select").removeAttr("disabled").addClass('required');
                    $formRow.find('#uniform-' + city.attr("name")).show();
                    country.removeAttr("disabled");
                    LoadCityStateDropdowns(data);

                    $.uniform.update(state);
                    $.uniform.update(city);

                    $('#dwfrm_singleshipping_shippingAddress_addressFields_cities_city-error').remove();
//					util.uniform();

                    // Fade-in City & State
//					$('.cover').remove();

                } else {
                    // No city objects returned from service, switch over to manual input.
                    CityFailOver("");
                }
            },
            error: function (data) {
                CityFailOver("");
            },
            complete: function () {
                progress.hide(".zip-city-state");
                StateIntercept(state.children().filter(':selected').first().val());
            }
        });
    }
}

function StateIntercept(state) {
    if (state == 'HI') {
        var params = {cid: 'hawaii-intercept'};

        $.ajax({
            url: Urls.liveContent,
            data: params,
            dataType: 'json',
            success: function (data) {
                if (data.content && data.content.onlineFlag) {
                    var html = '<div class="live-content-wrapper">' + data.content.body + '</div>';

                    dialog.open({
                        html: html,
                        options: {
                            maxHeight: 635,
                            dialogClass: 'live-content-dialog',
                            title: data.content.name
                        }
                    });

                    if ($(".postal .errorHawaiiValidation").length < 1) {
                        $(".postal .field-wrapper").append("<span class='error errorHawaiiValidation'><a title='Click for more info' class='error hawaii-intercept'>Click for more info</a></span>");
                    }
                    $('form#dwfrm_singleshipping_shippingAddress').find('button').addClass('disabled');
                    $('.postal input').addClass("error");
                    $('#hawaii-message').show();
                    $('#hawaii-small').show();
                }
            },
            error: function (data) {
            }
        });
    } else {
        $(".postal").find("span.errorHawaiiValidation").remove();
        $('form#dwfrm_singleshipping_shippingAddress').find('button').removeClass('disabled');
        $('#hawaii-message').hide();
        $('#hawaii-small').hide();
    }
}

function CityFailOver(cityOverride, stateOverride) {
    var checkoutForm = $("form.address");
    var city = checkoutForm.find('select[name$="_addressFields_cities_city"]');
    var state = checkoutForm.find('select[name$="_addressFields_states_state"]');
    var _name = city.attr("name");
    var $formRow = city.closest(".form-row");
    $formRow.find("select").attr("disabled", "disabled").removeClass('required');
    $formRow.find('#uniform-' + _name).hide();
    city.empty();

    $formRow.find("input.input-text").remove();
    var $input = $("<input type='text' class='input-text required city-txt' id='" + _name + "' name='" + _name + "' value='" + (cityOverride.length > 3 ? cityOverride : '') + "' max-length='50' />");
    var fieldWrapper = $formRow.find('.field-wrapper');
    fieldWrapper.append($input);

    // Switch cache target.
    city = $input;

    state.removeAttr("disabled");
    if (stateOverride) {
        state.val(stateOverride);
    } else {
        state.val("");
    }
    state.trigger('change');
    $.uniform.update(state);
}

function KeepAddress() {
    var checkoutForm = $("form.address");
    var postalCode = checkoutForm.find("input[name$='_postal']");
    var city = checkoutForm.find('select[name$="_addressFields_cities_city"]');
    var state = checkoutForm.find('select[name$="_addressFields_states_state"]');
    var keepAddress = checkoutForm.find("input[name$='_keepAddress']");
    var fleetwiseToken = checkoutForm.find("input[name$='_fleetwiseToken']");
    var addToAddressBook = checkoutForm.find("input[name$='_addToAddressBook']");
    var addressSuggestion = checkoutForm.find("#addressSuggestion");
    // Keep Address
    $.ajax({
        type: "post",
        dataType: "html",
        url: Urls.keepAddress,
        async: true,
        cache: false,
        beforeSend: function () {
//			progress.show(".delivery-schedule-box");
        },
        success: function (data) {
            if (data !== null) {
                if (data.indexOf("<body>") == -1) {
                    // if there are delivery dates, disable continue button and show calendar
                    if (data.indexOf('delivery-dates-container') != -1) {
                        $("#delivery-schedule-button").click(function (e) {
                            e.preventDefault();
                            $('#delivery-schedule-button').hide();
                            $("#delivery-dates-container").show();
                        });
                    } else {
                        // no dates available, possibly bad zip, enable continue button
                        // if indeed it's a bad zip, clicking continue will take the user back to cart
                        $("button.checkout-button").removeClass("disabled");
                    }

                } else {
                    if (data.indexOf("pt_cart") > -1) {
                        window.location = urls.cartShow;
                    } else {
                        // no dates available, possibly bad zip, enable continue button
                        // if indeed it's a bad zip, clicking continue will take the user back to cart
                        $("button.checkout-button").removeClass("disabled");
                    }
                }
            } else {
                // no data available, possibly bad zip, enable continue button
                // if indeed it's a bad zip, clicking continue will take the user back to cart
                $("button.checkout-button").removeClass("disabled");
            }
        },
        error: function (data) {
//			$cache.deliveryScheduleBox.hide();

            // no dates available, possibly bad zip, enable continue button
            // if indeed it's a bad zip, clicking continue will take the user back to cart
            $("button.checkout-button").removeClass("disabled");
        },
        complete: function () {
//			progress.hide(".delivery-schedule-box");

            fleetwiseToken.val("USER_UNDECIDED");
//			deliveryDate.val("USER_UNDECIDED");

            updateSummary();
        }
    });
}

function initialCityStateZipLoad() {
    var checkoutForm = $("form.address");
    var postalCode = checkoutForm.find("input[name$='_postal']");
    var state = checkoutForm.find('select[name$="_addressFields_states_state"]');
    // Fade-in City & State
    if ($.trim(postalCode.val()).length === 5) {
        if (window.initialCity) {
            CityFailOver(window.initialCity, state.val());
            updateSummary();
        } else {
            //hideCityState();
        }
    } else {
        //hideCityState();
    }
}

function deliverySelection(block) {
    if (block.val() == 'scheduledelivery') {    	
        $('.preferred-contact-wrapper').hide();
        $('.delivery-dates-wrapper').show();

        if (SitePreferences.isAtpDeliveryScheduleEnabled) {
            if ($('.schedule').hasClass('slick-initialized')) {
                $('.schedule').slick('unslick');
            }
            $('.schedule').slick({
                infinite: false,
                adaptiveHeight: true,
                slidesToScroll: 1,
                slidesToShow: 1,
                swipe: false,
                prevArrow: $('.slick-prev-arrow'),
                nextArrow: $('.slick-next-arrow'),
                accessibility: false,
                draggable: false
            });
            $('.slick-prev-arrow').attr('tabindex',0);
            $('.slick-next-arrow').attr('tabindex',0);
            $('.slick-next-arrow').on('click', function () {
                var currentIndex = $('.schedule').slick('slickCurrentSlide');
                var slickSliderLength = $('.sched-date-row.slick-slide').length;
                if (currentIndex == (slickSliderLength - 1) && $(this).data('current-week-date') != false) {
                    var params = {
                            format: 'ajax',
                            currentWeekDate: $(this).data('current-week-date')
                    };
                    progress.show($('#main'));
                    ajax.load({
                        url: util.appendParamsToUrl(Urls.getATPDeliveryDatesForNextWeek, params),
                        callback: function (response) {
                            if (response != '') {
                                $('.schedule').slick('slickAdd', response, currentIndex, true);
                                if ($('.slick-next-arrow').data('current-week-date') == false) {
                                    $('.schedule').slick('slickRemove', (currentIndex + 1));
                                }
                            }
                            progress.hide();
                            $('.slick-prev-arrow').attr('tabindex',0);
                            $('.slick-next-arrow').attr('tabindex',0);
                        }
                    });
                }
                $('.slick-prev-arrow').attr('tabindex',0);
				$('.slick-next-arrow').attr('tabindex',0);
            });
            var slickPreviousDiv = null;
            var slickNextDiv = null;
            var currentSlide = 0;
            $('.slick-next-arrow').keypress(function (ev) {
				if (ev.keyCode == 13 || ev.which == 13) {
					$('.sched-date-row').find('.time-slot').removeAttr("tabindex");
					if (slickNextDiv != null) {
						slickNextDiv.attr("tabindex", 0);
					}
					currentSlide = $('.schedule').slick('slickCurrentSlide');
					var $this = $('.schedule').find("[data-slick-index=" + currentSlide + "]").find('.time-slot');
					$this.removeAttr("tabindex");
					slickPreviousDiv = $this;
					$(this).trigger('click');
				}
			});
            $('.slick-prev-arrow').keypress(function (ev) {
				if (ev.keyCode == 13 || ev.which == 13) {
					$('.sched-date-row').find('.time-slot').removeAttr("tabindex");
					currentSlide = $('.schedule').slick('slickCurrentSlide');
					var $this = $('.schedule').find("[data-slick-index=" + currentSlide + "]").find('.time-slot');
					$this.removeAttr("tabindex");
					slickNextDiv = $this;
					slickPreviousDiv.attr("tabindex",0);
					$('.slick-prev-arrow').attr('tabindex',0);
					$('.slick-next-arrow').attr('tabindex',0);
					$('.slick-prev-arrow')[0].click();
				}
			});
		} else {
            if ($('.sched-delivery').hasClass('slick-initialized')) {
                $('.sched-delivery').slick('unslick');
            }
            $('.sched-delivery').slick({
                infinite: false,
                adaptiveHeight: true
            });
        }
    } else {
        $('.delivery-dates-wrapper').hide();
        //$('.preferred-contact-wrapper').show();
        
        var item = $(".preferred-contact-wrapper");
    	item.show();
    	block.parent().parent().first().next().after(item);
    }
    block.closest("span").addClass("checked");
}

function initDeliverySelection() {
    var deliverySelectionBlock = $('#schedule-with-rep input');
    deliverySelectionBlock.on('change', function () {
            deliverySelection($(this));
    });
    
    var deliveryDate = $('[name=dwfrm_singleshipping_shippingAddress_scheduleWithRep]').last();    
    deliveryDate.trigger("click");
}

function initialDeliveryDates() {
    initDeliverySelection();

    var addressForm = $('.delivery-dates');
    function changeDeliveryDate(obj) {
        if (!($(obj).hasClass("full"))) {
            /** grab time slot clicked and mark correct day and time slot as selected **/
            $(".time-slot").removeClass("selected");
            $(".header").removeClass("selected");
            $(obj).addClass("selected");
            $(obj).parent().children(".header").addClass("selected");

            /** build text block at bottom stating what date and time was chosen **/
            if (SitePreferences.isAtpDeliveryScheduleEnabled) {
                $(".currently-chosen").removeClass("hide");
                var textdate = $(obj).parent().find(".weekday").data('date');
                //textdate += ", " + $(this).parent().find(".month-day").text();
                textdate += '<span> from </span>' + $(obj).text().replace('-', 'to').trim();
                $(".chosen-time").html(textdate);

                /** set pdict fleetwiseToken to currently selected value **/
                $('#dwfrm_singleshipping_shippingAddress_addressFields_fleetwiseToken').val($(obj).data("fleetwisetoken"));
                var deliveryTime = $(obj).text().replace(RegExp(' ', 'g'), '');
                $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryDate').val($(obj).parent().find(".weekday-timeval").text() + ', ' + deliveryTime);
                $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryTime').val(deliveryTime);
            } else {
            	$(".currently-chosen").removeClass("hide");
                $(".currently-chosen").addClass("visible");
                var textdate = $(obj).parent().find(".weekday").text();
                //textdate += ", " + $(this).parent().find(".month-day").text();
                textdate += ", between " + $(obj).text().replace("-","and").replace(RegExp(" PM", "g"), "pm").replace(RegExp(" AM", "g"), "am").trim();
                $(".chosen-time").text(textdate);

                /** set pdict fleetwiseToken to currently selected value **/
                $('#dwfrm_singleshipping_shippingAddress_addressFields_fleetwiseToken').val($(obj).data("fleetwisetoken"));
                $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryDate').val(textdate);
            }
        }
    }
	addressForm.on("click", ".time-slot", function () {
		changeDeliveryDate(this);
	});
	addressForm.on("keypress", ".time-slot", function (ev) {
		if (ev.keyCode == 13 || ev.which == 13) {
			changeDeliveryDate(this);
		}
	});
    addressForm.on("focus", ".time-slot", function () {
        var dateValue = $($(this).siblings()[0]).attr('data-date');
        dateValue = dateValue.trim();
        var timeValue = $(this).text();
        timeValue = timeValue.trim();
        var timeSlotAriaLabel = dateValue + " " + timeValue;
        $(this).attr("aria-label", timeSlotAriaLabel);
        $(this).parents('.sched-date-row').attr("aria-hidden", false);
    });
	//Accessibility turned off and this code not perfectly handling acessibility focus in slick slider
	/*var slickPreviousDiv = null;
	var slickNextDiv = null;
	addressForm.on("keydown", ".time-slot", function (e) {
		if (e.keyCode === 39 || e.keyCode === 40) {
			e.preventDefault();
			$(this).parents('.sched-date-row').find('.time-slot').attr("tabindex", 0);
			slickPreviousDiv = $(this).parents('.sched-date-row').find('.time-slot');
			var nextDiv = $(this).parent().next().find('.time-slot');
			$('.time-slot').attr("aria-checked", false);
			nextDiv.attr("tabindex", 0);
			nextDiv.attr("aria-checked", true);
			$('.slick-next-arrow').trigger('click');
			$(this).parents('.sched-date-row').find('.time-slot').removeAttr("tabindex");
			$('.time-slot').attr("tabindex", 0);
			if (slickNextDiv != null) {
				slickNextDiv.attr("tabindex", 0);
				slickNextDiv.focus();
			} else {
				nextDiv.focus();
			}
			$(this).parent().next().find('.time-slot:first').focus;
			nextDiv.focus();
		}else if (e.keyCode === 37 || e.keyCode === 38) {
			e.preventDefault();
			var nextDiv = $(this).parent().prev().find('.time-slot');
			$('.time-slot').attr("tabindex", 0);
			$('.time-slot').attr("aria-checked", false);
			slickPreviousDiv.attr("tabindex", 0);
			slickNextDiv = $(this).parents('.sched-date-row').find('.time-slot');
			nextDiv.attr("aria-checked", true);
			$(this).parents('.sched-date-row').find('.time-slot').removeAttr("tabindex");
			$(this).parent().prev().find('.time-slot:first').focus();
			nextDiv.focus();
			slickPreviousDiv.focus();
		}
		$('.slick-prev-arrow').attr('tabindex',0);
		$('.slick-next-arrow').attr('tabindex',0);
	});*/
}

function validateEmail2(email) {
    var $continue = $('.form-row-button button');
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (!emailReg.test(email)) {
        $continue.attr('disabled', 'disabled');
        if ($(".emailaddr .error").length < 1) {
            $(".emailaddr .field-wrapper").append("<span class='error'>" + Resources.VALIDATE_EMAIL + "</span>");
        }
        return false;
    } else {
        $continue.removeAttr('disabled');
        if ($(".error").length > 0) {
            $(".emailaddr .error").remove();
        }
        return true;
    }
}

// these 2 functions are on product.js (should include js?)
function addProductSelectStore(storeId, pid, format) {
    $.ajax({
        url: Urls.changePickupLocation,
        type: 'POST',
        data: {storeId: storeId, format: format}
    }).done(function () {
        dialog.close();
        page.redirect(Urls.cartShow);
    });
 }

function setPreferredStore(storeId) {
    User.storeId = storeId;
    $.ajax({
        url: Urls.setPreferredStore,
        type: 'POST',
        data: {storeId: storeId}
    });
}

exports.init = function () {
	if ($('#dwfrm_singleshipping_shippingAddress').length > 0) {
		var formFields = $('#dwfrm_singleshipping_shippingAddress');
        if(($(formFields).find('#dwfrm_singleshipping_shippingAddress_addressFields_address1').length > 0) 
        		&& ($('#dwfrm_singleshipping_shippingAddress_addressFields_address1').is(":visible"))) {
            $(formFields).find('#dwfrm_singleshipping_shippingAddress_addressFields_address1').val('');
        }
        if(($(formFields).find('#dwfrm_singleshipping_shippingAddress_addressFields_address2').length > 0) 
        		&& ($('#dwfrm_singleshipping_shippingAddress_addressFields_address2').is(":visible"))) {
            $(formFields).find('#dwfrm_singleshipping_shippingAddress_addressFields_address2').val('');
        }
	}
	$(".currently-chosen").addClass("hide");
    formPrepare.init({
        continueSelector: '[name$="shippingAddress_save"]',
        formSelector:'[id$="singleshipping_shippingAddress"]'
    });
    $('input[name$="_shippingAddress_isGift"]').on('click', giftMessageBox);

    /*
     * Single shipping method would be used at launch
    $('.address').on('change',
        'input[name$="_addressFields_address1"], input[name$="_addressFields_address2"], select[name$="_addressFields_states_state"], input[name$="_addressFields_cities_city"] , input[name$="_email_emailAddress"]', function (e) {
        updateShippingMethodList();
    });
    */

    $('.address').on('click', 'a.hawaii-intercept', function (e) {
        e.preventDefault();
        StateIntercept("HI");
    });
    $('.address').on('change', 'input[name$="_addressFields_postal"]', function () {
        GetCityStateFromZip($(this).val());
    });
    $('form.address').on('click', '#addressSuggestion a.address-suggestion', function (e) {
        e.preventDefault();
        var checkoutForm = $("form.address");
        var address1 = checkoutForm.find("input[name$='_address1']");
        var address2 = checkoutForm.find("input[name$='_address2']");
        var postalCode = checkoutForm.find("input[name$='_postal']");
        var city = checkoutForm.find('select[name$="_addressFields_cities_city"]');
        var state = checkoutForm.find('select[name$="_addressFields_states_state"]');
        var keepAddress = checkoutForm.find("input[name$='_keepAddress']");
        var fleetwiseToken = checkoutForm.find("input[name$='_fleetwiseToken']");
        var addToAddressBook = checkoutForm.find("input[name$='_addToAddressBook']");
        var addressSuggestion = checkoutForm.find("#addressSuggestion");
        addressSuggestion.hide();

        var _data;
        if ($(this).attr("id") === "use-suggested") {
            _data = $.parseJSON(addressSuggestion.data("json"));

            address1.val(_data.suggestedAddress.addressLine1);
            address2.val(_data.suggestedAddress.addressLine2);

            // Check if service Suggested a city that the GetCityStateFrom Zip does not return.
            if (city.find("option[value='" + _data.suggestedAddress.city.toUpperCase() + "']").length > 0) {
                city.val(_data.suggestedAddress.city.toUpperCase());
            } else {
                CityFailOver(_data.suggestedAddress.city.toUpperCase());
            }

            state.val(_data.suggestedAddress.stateCode);
            postalCode.val(_data.suggestedAddress.zipCode);

            if (city[0].nodeName.toLowerCase() === "select") {
                city.uniform();
            }

            // don't show drop down list of states if user selects use suggested address
            state.closest(".state-col").find(".dk_options_inner").hide();
        }

        // Set "Keep" flag for any choice to denote no further verification action needed.
        keepAddress.prop('checked', true);

        //Re run Keep Address
        KeepAddress();
    });
    if ($('input[name$="_addressFields_lastName"]').val() != '' && $('input[name$="_addressFields_lastName"]').val() != undefined) {
        if ($('input[name$="_addressFields_address1"]').val() != '' && $('input[name$="_addressFields_address1"]').val() != undefined) {
            var $phoneval = $('input[name$="_addressFields_phone"]').val();
            if ($('input[name$="_addressFields_phone"]').val() != '' && $('input[name$="_addressFields_phone"]').val() != undefined) {
            } else {
                if ($(".phones.required .field-wrapper .error").length > 0) {
                } else {
                    if ($('input[name$="_addressFields_phone"]').val() != '' && $('input[name$="_addressFields_phone"]').val() != undefined) {
                        if ($(".phones.required #temp-error").length > 0) {
                            $(".phones.required #temp-error").remove();
                        }
                    } else {
                        $(".phones.required .field-wrapper").append("<span class='error' id='temp-error'>" + Resources.INVALID_PHONE + "</span>");
                        $(".phones.required .field-wrapper input.phone").addClass("error");
                    }
                }
            }
        }
    }
    $('input[name="mattressremoval"]').change(function (e) {
        var selected_value = $('input[name="mattressremoval"]:checked').val();
        if (selected_value == 'yes') {
            $('#dwfrm_singleshipping_pickups').removeAttr("disabled");
        } else {
            $('#dwfrm_singleshipping_pickups').val('');
            $('#dwfrm_singleshipping_pickups').attr("disabled", "disabled");
        }
    });
    giftMessageBox();

    //Single shipping method would be used at launch
    //Replaced with summary update
    //updateShippingMethodList();
    updateSummary();

    initialCityStateZipLoad();
    initialDeliveryDates();
    if ($.trim($('input[name$="_addressFields_postal"]').val()).length === 5 && $('input[name$="_addressFields_cities_city"]').length === 0) {
        GetCityStateFromZip($('input[name$="_addressFields_postal"]').val());
    }
    formPrepare.validateForm();
    var checkoutForm = $("form.address");
    var emailaddressinput = checkoutForm.find("input[name$='email_emailAddress']").val();
    if (typeof emailaddressinput != 'undefined') {
        validateEmail2(emailaddressinput);
    }
    $("body").on('click', '.change-store', function (e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href'),
            title: $(e.target).attr('title'),
            options: {
                height: 600
            },
            callback: function () {
                $('#dwfrm_pickupinstore').trigger('submit');
            }
        });
    });
    $("body").on('click', '.select-store', function (e) {
        var selectedStore = $(e.target).val();
        setPreferredStore(selectedStore);
        $("div[class*='store']").removeClass('selected');
        $('.store_' + selectedStore).addClass('selected');
        $(".select-store:contains('Preferred Store')").closest('button').first().text('Select Store');
        $(e.target).text('Preferred Store');
    });
    $('.pickup-in-store').on('click', function (e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href'),
            title: $(e.target).attr('title'),
            options: {
                height: 600
            },
            callback: function () {
                $('#dwfrm_pickupinstore').trigger('submit');
            }
        });
    });
    $('body').on('submit', '#dwfrm_pickupinstore', function (e) {
        e.preventDefault();
        // serialize the form and get the post url
        var buttonName = $('#dwfrm_pickupinstore').find('.pickupinstore').attr('name');
        var options = {
            url: Urls.pickupInStore,
            data: $('#dwfrm_pickupinstore').serialize() + '&' + buttonName + '=x',
            type: 'POST'
        };
        $.ajax(options).done(function (data) {
            if (typeof(data) !== 'string') {
                if (data.success) {
                    //dialog.close();
                    //page.refresh();
                } else {
                    window.alert(data.message);
                    return false;
                }
            } else {
                $('#dialog-container').html(data);
                //dialog.close();
                //page.refresh();
            }
        });

    });
    $("body").on('click', '.continue-with-select-store', function (e) {
        var selectedStore = $('.selected').attr('data-storeid');
        var pid = $('input[name="pid"]').val();
        var format = 'ajax';
        addProductSelectStore(selectedStore, pid, format);
    });
    
    // add listeners for the delivery options notice on the shipping page
    $('body').on('click', '.ship-to-you-chg-delivery', function (e) {
        e.preventDefault();
        // hide the change link and show the selection form
        $(e.target).hide();
        $('.ship-to-you-change-block').show();
    });
    $('body').on('click', '.ship-to-you-modify-cancel', function (e) {
        e.preventDefault();
        // show the change link and hide the selection form
        $('.ship-to-you-change-block').hide()
        $('.ship-to-you-chg-delivery').show();
    });

    $('body').on('click', '.ship-to-you-modify-button', function (e) {
        var newDeliveryChoice = $('input[name="ShipToDeliveryOption"]:checked').val();
        var currentOption = $('#currentDeliveryChoice')[0].innerText;
        if ((currentOption == 'shipped' || currentOption == 'null') && newDeliveryChoice == 'instorepickup') {
            // TODO: it probably makes more sense to change the delivery option on the confirmation
            // of continuing with the preferred store, rather than in the middle of the calls.
        	$.ajax({
                url: Urls.changeDeliveryOption,
                type: 'POST',
                dataType : "json",
                data: {deliveryoption: newDeliveryChoice, format: 'ajax'},
                async: false
            }).done(function (res) {
            	 if (res.success) {
                 	page.redirect(Urls.coShippingStart);
                 } else {
                 	$('.store-picker a').click();
                 }
            });               
        } else if ((currentOption == 'instorepickup' || currentOption == 'null') && newDeliveryChoice == 'shipped') {
            // change to ship to address
            $.ajax({
                url: Urls.changeToShipping,
                type: 'POST',
                data: {deliveryoption: newDeliveryChoice},
                async: false
            }).done(function () {
            	page.redirect(Urls.coShippingStart);
            });
        } else {
            // leave in same state
            $('.ship-to-you-modify-cancel').trigger('click');
        }
    });
};

exports.updateShippingMethodList = updateShippingMethodList;