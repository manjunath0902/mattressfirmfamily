'use strict';

var ajax = require('../../ajax'),
	formPrepare = require('./formPrepare'),
	giftcard = require('../../giftcard'),
	util = require('../../util');

/**
 * @function
 * @description Fills the Credit Card form with the passed data-parameter and clears the former cvn input
 * @param {Object} data The Credit Card data (holder, type, masked number, expiration month/year)
 */
function setCCFields(data) {
	var $creditCard = $('[data-method="CREDIT_CARD"]');
	$creditCard.find('input[name$="creditCard_owner"]').val(data.holder).trigger('change');
	$creditCard.find('select[name$="_type"]').val(data.type).trigger('change');
	$creditCard.find('input[name*="_creditCard_number"]').val(data.maskedNumber).trigger('change');
	$creditCard.find('[name$="_month"]').val(data.expirationMonth).trigger('change');
	$creditCard.find('[name$="_year"]').val(data.expirationYear).trigger('change');
	$creditCard.find('input[name$="_cvn"]').val('').trigger('change');
}

/**
 * @function
 * @description Updates the credit card form with the attributes of a given card
 * @param {String} cardID the credit card ID of a given card
 */
function populateCreditCardForm(cardID) {
	// load card details
	var url = util.appendParamToURL(Urls.billingSelectCC, 'creditCardUUID', cardID);
	ajax.getJson({
		url: url,
		callback: function (data) {
			if (!data) {
				window.alert(Resources.CC_LOAD_ERROR);
				return false;
			}
			setCCFields(data);
		}
	});
}

/**
 * @function
 * @description Changes the payment method form depending on the passed paymentMethodID
 * @param {String} paymentMethodID the ID of the payment method, to which the payment method form should be changed to
 */
function updatePaymentMethod(paymentMethodID) {
	var $paymentMethods = $('.payment-method');
	$paymentMethods.removeClass('payment-method-expanded');

	var $selectedPaymentMethod = $paymentMethods.filter('[data-method="' + paymentMethodID + '"]');
	if ($selectedPaymentMethod.length === 0) {
		$selectedPaymentMethod = $('[data-method="Custom"]');
	}
	$selectedPaymentMethod.addClass('payment-method-expanded');

	// ensure checkbox of payment method is checked
	$('input[name$="_selectedPaymentMethodID"]').removeAttr('checked');
	$('input[value=' + paymentMethodID + ']').prop('checked', 'checked');

	formPrepare.validateForm();
}

/**
 * @function
 * @description loads billing address, Gift Certificates, Coupon and Payment methods
 */
exports.init = function () {
	var $checkoutForm = $('.checkout-billing');
	var $addGiftCert = $('#add-giftcert');
	var $giftCertCode = $('input[name$="_giftCertCode"]');
	var $addCoupon = $('#add-coupon');
	var $couponCode = $('input[name$="_couponCode"]');
	var $selectPaymentMethod = $('.payment-method-options');
	var selectedPaymentMethod = $selectPaymentMethod.find(':checked').val();

	formPrepare.init({
		formSelector: 'form[id$="billing"]',
		continueSelector: '[name$="billing_save"]'
	});

	// default payment method to 'CREDIT_CARD'
	updatePaymentMethod((selectedPaymentMethod) ? selectedPaymentMethod : 'CREDIT_CARD');
	$selectPaymentMethod.on('click', 'input[type="radio"]', function () {
		updatePaymentMethod($(this).val());
	});

	// select credit card from list
	$('#creditCardList').on('change', function () {
		var cardUUID = $(this).val();
		if (!cardUUID) {return;}
		populateCreditCardForm(cardUUID);

		// remove server side error
		$('.required.error').removeClass('error');
		$('.error-message').remove();
	});

	$('#check-giftcert').on('click', function (e) {
		e.preventDefault();
		var $balance = $('.balance');
		if ($giftCertCode.length === 0 || $giftCertCode.val().length === 0) {
			var error = $balance.find('span.error');
			if (error.length === 0) {
				error = $('<span>').addClass('error').appendTo($balance);
			}
			error.html(Resources.GIFT_CERT_MISSING);
			return;
		}

		giftcard.checkBalance($giftCertCode.val(), function (data) {
			if (!data || !data.giftCertificate) {
				$balance.html(Resources.GIFT_CERT_INVALID).removeClass('success').addClass('error');
				return;
			}
			$balance.html(Resources.GIFT_CERT_BALANCE + ' ' + data.giftCertificate.balance).removeClass('error').addClass('success');
		});
	});

	$addGiftCert.on('click', function (e) {
		e.preventDefault();
		var code = $giftCertCode.val(),
			$error = $checkoutForm.find('.giftcert-error');
		if (code.length === 0) {
			$error.html(Resources.GIFT_CERT_MISSING);
			return;
		}

		var url = util.appendParamsToUrl(Urls.redeemGiftCert, {giftCertCode: code, format: 'ajax'});
		$.getJSON(url, function (data) {
			var fail = false;
			var msg = '';
			if (!data) {
				msg = Resources.BAD_RESPONSE;
				fail = true;
			} else if (!data.success) {
				msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
				fail = true;
			}
			if (fail) {
				$error.html(msg);
				return;
			} else {
				window.location.assign(Urls.billing);
			}
		});
	});

	$addCoupon.on('click', function (e) {
		e.preventDefault();
		var $error = $checkoutForm.find('.coupon-error'),
			code = $couponCode.val();
		if (code.length === 0) {
			$error.html(Resources.COUPON_CODE_MISSING);
			return;
		}

		var url = util.appendParamsToUrl(Urls.addCoupon, {couponCode: code, format: 'ajax'});
		$.getJSON(url, function (data) {
			var fail = false;
			var msg = '';
			if (!data) {
				msg = Resources.BAD_RESPONSE;
				fail = true;
			} else if (!data.success) {
				msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
				fail = true;
			}
			if (fail) {
				$error.html(msg);
				return;
			}

			//basket check for displaying the payment section, if the adjusted total of the basket is 0 after applying the coupon
			//this will force a page refresh to display the coupon message based on a parameter message
			if (data.success && data.baskettotal === 0) {
				window.location.assign(Urls.billing);
			}
		});
	});

	// trigger events on enter
	$couponCode.on('keydown', function (e) {
		if (e.which === 13) {
			e.preventDefault();
			$addCoupon.click();
		}
	});
	$giftCertCode.on('keydown', function (e) {
		if (e.which === 13) {
			e.preventDefault();
			$addGiftCert.click();
		}
	});
	if ($('.step.step-3').hasClass('active')) {
		$('.right-summary').css('display', 'block');
	}

	// Bulk Payment - Additional Orders to Pay
	$("input[type='checkbox']").change(function() {
		var idCheckbox = $(this).attr('id');	// ID of checkbox
		if (idCheckbox == 'checkAllAdditionalOrders') {
		    var checkBoxes = $(":checkbox");

	        if (this.checked) {	// check all other checkboxes as well
	        	// Loop on all checkboxes to add all orders to total
	    		for (var i=1; i<checkBoxes.length; i++) {	// Skipping index 0, as it is 'checkAllAdditionalOrders' checkbox
	        		if (!checkBoxes[i].checked) {	// Only add those orders which are not already added
	        			checkBoxes[i].click();		// check to call addThisOrderToTotal(this,idCheckbox);
	        		}
	        	}
	        } else {	// uncheck all other checkboxes as well
	        	for (var i=1; i<checkBoxes.length; i++) {	// Skipping index 0, as it is 'checkAllAdditionalOrders' checkbox
	        		if (checkBoxes[i].checked) {	// Only remove those orders which were added
	        			checkBoxes[i].click();		// uncheck to call addThisOrderToTotal(this,idCheckbox);
	        		}
	        	}
	        }
		} else {
			// Handle single checkbox toggle
			addThisOrderToTotal(this,idCheckbox);
		}
	});

	// Handle single checkbox toggle to Add/Subtract the Order amount to Total
	function addThisOrderToTotal(thisCheckbox, idCheckbox) {
		// To-do: Math.round is needed to handle the infinite decimal values
		var idOrderTotal = '#'+idCheckbox+'-total';		// ID of order total amount
		var iditemsCount = '#'+idCheckbox+'-items';		// ID of order items count
		var orderTotal = Number($(idOrderTotal).data('order-total'));	// Amount of selected order through checkbox
		var itemsCount = Number($(iditemsCount).data('items-count'));	// Number of items of selected order through checkbox

		var $additionalOrderAmount = $('#additional-order-amount');
		var $additionalOrderItemCount = $('#additional-order-item-count');
		var grandTotalAmount = Number($('#grand-total').data('amount'));	// Grand Order Total that will be billed

		// IDs of Orders to be submitted in Controller
		var orderIDsArray = $('#additional-orders-selected-for-payment').val().toString().split(',');

		if(thisCheckbox.checked) {
			// Add the selected Order's Amount to Total Additional Orders selected Amount
			var newTotalAmount = Number($additionalOrderAmount.data('additional-order-total')) + orderTotal;
			// Add the selected Order's Item count to Total Additional Orders selected Items count
			var newTotalItemsCount = Number($additionalOrderItemCount.data('additional-items-count')) + itemsCount;
			// Update Grand Order Total containing amount of Current Order + Additional selected orders
			var newGrandTotalAmount = grandTotalAmount + orderTotal;
			// Add Order Id to array for submitting order in Controller
			if (orderIDsArray.indexOf(idCheckbox) == -1) {	// Only add, if not already present
				orderIDsArray.push(idCheckbox);
			}
	    } else {
			// Subtract the unselected Order's Amount from Total Additional Orders selected Amount
			var newTotalAmount = Number($additionalOrderAmount.data('additional-order-total')) - orderTotal;
			// Subtract the unselected Order's Item count from Total Additional Orders selected Items count
			var newTotalItemsCount = Number($additionalOrderItemCount.data('additional-items-count')) - itemsCount;
			// Update Grand Order Total containing amount of Current Order + Additional selected orders
			var newGrandTotalAmount = grandTotalAmount - orderTotal;
			// Remove Order Id from array
			var indexOfOrderIdToRemove = orderIDsArray.indexOf(idCheckbox);
			if (indexOfOrderIdToRemove > -1) {	// If Order Id to remove was present in array
				orderIDsArray.splice(indexOfOrderIdToRemove, 1);	// remove the order Id
			}
		}

		$additionalOrderAmount.data('additional-order-total', newTotalAmount);
		$additionalOrderAmount.html('<b>$'+(Math.abs(Math.round(newTotalAmount*100)/100)).toFixed(2)+'</b>');

		$additionalOrderItemCount.data('additional-items-count', newTotalItemsCount);
		$additionalOrderItemCount.html('<b>'+newTotalItemsCount+'</b>');

		$('#grand-total').data('amount', newGrandTotalAmount);
		$('#grand-total').html((Math.round(newGrandTotalAmount*100)/100).toFixed(2));

		var selectedOrderIDs = orderIDsArray.join(',');
		$('#additional-orders-selected-for-payment').val(selectedOrderIDs);
	}

	var disableSubmitOrderButton = function() {
		$('#submitOrderCSF').prop("disabled", true);
	}

	var enableSubmitOrderButton = function() {
		$('#submitOrderCSF').prop("disabled", false);
	}

	var showSubmitLoadingSpinner = function() {
		if (!$(".submitting-wait").length) {
			$('<div class="submitting-wait"><div class="loading-message">Submitting Order, Please Wait... <span class="loading-indicator"></span></div></div>').insertBefore('#submitOrderCSF');
		}
		else {
			$(".submitting-wait").show();
		}
	};

	$(document).on("click", "#submitOrderCSF" , function(e) {
		e.preventDefault();
		var orderId = $('#orderId').val(),
			eventId = $('#eventId').val(),
			additionalOrdersSelectedForPayment = $('#additional-orders-selected-for-payment').val(),
			params = {"orderId":orderId, "eventId":eventId, "additionalOrdersSelectedForPayment":additionalOrdersSelectedForPayment, "format":"ajax"};

			disableSubmitOrderButton();
			showSubmitLoadingSpinner();

		$.ajax({
			url	: Urls.submitCSFOrder,
			data: params,
			type: "POST",
			success: function(data, response) {
				if (response == 'success') {
					var paramsObj = JSON.parse(data);
					if (paramsObj.allOrdersPlaced) { // Redirect to Order Confirmation page
			    		window.location = util.appendParamsToUrl(Urls.fulfillmentOrderConfirmation, {'orderIds': paramsObj.orderIds});
					}else if(paramsObj.noOrdersPendingMessage != null) {
						window.location = util.appendParamsToUrl(Urls.fulfillmentOrderConfirmation, {noOrdersPendingMessage:paramsObj.noOrdersPendingMessage});
					} else { // Redirect to Billing page & display error message as well
						window.location = util.appendParamsToUrl(Urls.fulfillmentBilling, {eventId:paramsObj.eventId, orderId: paramsObj.orderId, ordersNotPlaced : paramsObj.ordersNotPlaced});
					}
				}
			},
			failure: function() {
				//window.location = Urls.fulfillmentBilling;

				// If failure, hide the spinner and enable the submit button.
				$('.submitting-wait').hide();
				enableSubmitOrderButton();
			}
		});
	});



	var getUrlParameter = function getUrlParameter(sParam) {
	    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName,
	        i;

	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');

	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : sParameterName[1];
	        }
	    }
	};
};
