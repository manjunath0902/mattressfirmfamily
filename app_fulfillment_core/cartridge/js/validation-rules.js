'use strict';
var EMAIL_REGEX = /^[\w.%+-]+@[\w.-]+\.[\w]{2,6}$/,
	ZIP_CODE_REGEX = /(^\d{5}(-\d{4})?$)|(^[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Za-z]{1} *\d{1}[A-Za-z]{1}\d{1}$)/,
	PHONE_NO_REGEX = /^\(?([2-9][0-8][0-9])\)?[\-\. ]?([2-9][0-9]{2})[\-\. ]?([0-9]{4})(\s*x[0-9]+)?$/;
var rules = {
	AddEventRules : {
		onclick: false,
		rules : {
			ordersfile : {
				extension : 'csv'
			}
		},
		storeId: {
			required: true
		}
	},

	EditCustomerRules : {
		rules : {
			dwfrm_editcustomerinfo_customer_email : {
				pattern : EMAIL_REGEX
			},
			dwfrm_editcustomerinfo_address_city : {
				minlength : 2
			},
			dwfrm_editcustomerinfo_address_postal : {
				minlength : 5,
				pattern : ZIP_CODE_REGEX
			},
			dwfrm_editcustomerinfo_customer_phone : {
				pattern : PHONE_NO_REGEX
			}
		},
		messages : {
			dwfrm_editcustomerinfo_customer_email : {
				pattern : Resources.VALIDATE_EMAIL
			},
			dwfrm_editcustomerinfo_address_postal : {
				minlength : Resources.VALIDATE_ZIPCODE,
				pattern : Resources.VALIDATE_ZIPCODE
			},
			dwfrm_editcustomerinfo_customer_phone : {
				pattern : Resources.VALIDATE_PHONENO
			},
			dwfrm_editcustomerinfo_address_city : {
				minlength : Resources.VALIDATE_CITY
			}
		}
	},
	EditProfile : {
		errorElement: 'span',
		rules : {
			dwfrm_profile_customer_phone : {
				pattern : PHONE_NO_REGEX
			},
			dwfrm_profile_customer_firstname: {
				maxlength : 50
			},
			dwfrm_profile_customer_lastname: {
				maxlength : 50
			},
			dwfrm_profile_address_postal: {
				minlength : 5,
				pattern : ZIP_CODE_REGEX
			},
			dwfrm_profile_address_city : {
				minlength : 2
			}
		},
		messages : {
			dwfrm_profile_customer_phone : {
				pattern : Resources.VALIDATE_PHONENO,
				phone: Resources.VALIDATE_PHONENO
			},
			dwfrm_profile_customer_firstname : {
				pattern : Resources.VALIDATE_EMAIL
			},
			dwfrm_profile_address_postal : {
				minlength : Resources.VALIDATE_ZIPCODE,
				pattern : Resources.VALIDATE_ZIPCODE
			},
			dwfrm_profile_address_city : {
				minlength : Resources.VALIDATE_CITY
			}
		}
	},
	CreateAccount : {
		errorElement: 'span',
		rules : {
			dwfrm_profile_customer_phone : {
				pattern : PHONE_NO_REGEX
			},
			dwfrm_profile_customer_firstname: {
				maxlength : 50
			},
			dwfrm_profile_customer_lastname: {
				maxlength : 50
			},
			dwfrm_profile_address_postal: {
				minlength : 5,
				pattern : ZIP_CODE_REGEX
			},
			dwfrm_profile_address_city : {
				minlength : 2
			},
			dwfrm_profile_customer_email: {
				pattern : EMAIL_REGEX
			}
		},
		messages : {
			dwfrm_profile_customer_phone : {
				pattern : Resources.VALIDATE_PHONENO,
				phone: Resources.VALIDATE_PHONENO
			},
			dwfrm_profile_customer_firstname : {
				pattern : Resources.VALIDATE_EMAIL
			},
			dwfrm_profile_address_postal : {
				minlength : Resources.VALIDATE_ZIPCODE,
				pattern : Resources.VALIDATE_ZIPCODE
			},
			dwfrm_profile_address_city : {
				minlength : Resources.VALIDATE_CITY
			},
			dwfrm_profile_customer_email: {
				pattern : Resources.VALIDATE_EMAIL
			}
		}
	},
	ChangePassword: {
		errorElement: 'span',
		rules : {
			dwfrm_changepassword_login_newpassword : {
				minlength: 8,
				maxlength : 255,
				noSpace: true
			},
			dwfrm_changepassword_login_newpasswordconfirm: {
				minlength: 8,
				maxlength : 255,
				noSpace: true,
				equalTo: '#dwfrm_changepassword_login_newpassword'
			}
		},
		messages : {
			dwfrm_changepassword_login_newpassword : {
				minlength : Resources.VALIDATE_PASSLENGTH,
				noSpace: Resources.VALIDATE_PASSWORD_NO_SPACE
			},
			dwfrm_changepassword_login_newpasswordconfirm : {
				minlength : Resources.VALIDATE_PASSLENGTH,
				noSpace: Resources.VALIDATE_PASSWORD_NO_SPACE,
				equalTo: Resources.VALIDATE_PASSWORD_MATCH
			}
		}
	},
	ResetPassword: {
		errorElement: 'span',
		onkeyup: false,
		rules : {
		},
		messages : {
		}
	},
};

module.exports = rules;