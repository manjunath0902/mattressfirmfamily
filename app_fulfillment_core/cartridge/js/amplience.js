'use strict';

var util = require('./util'),
	dialog = require('./dialog');


var ampHeight = 500,
	ampWidth  = 598;

var amplience = {
	initZoomViewer: function (ampset) {
		// start fresh
		this.destroy();
		var jsonSet = ampset != null && ampset != '' ? ampset : $('.product-meta').data('jsonset'),
			sash = $('.product-meta').data('sash'),
			ampLoading = true;
		// setup amplience instance
		amp.init({
			client_id: SitePreferences.amplienceClientId,
			di_basepath: 'https://' + SitePreferences.amplienceHost + '/',
			errImg: '../shared/404image.jpg'
		});
		// create html from image set json
		amp.get([{'name': jsonSet, 'type': 's'}], function (data) {
			//start fresh
			amplience.destroy();
			var dis = amp.di.set(data[jsonSet], {h: ampHeight, w: ampWidth, sm: 'CM'});
			if (sash) {
				dis['items'][0]['src'] += sash;
			}
			amp.genHTML(dis, $('#amplience-main')[0], true);
			var ampCach = {
				ul: $('#amplience-main').find('ul'),
				img: $('#amplience-main').find('img')
			};
			if (data[jsonSet].items.length > 1) {
				for (var i = 0; i < data[jsonSet].items.length; i++) {
					var item = data[jsonSet].items[i];
					if (item.set) {
						item.type = 'img';
					}
				}
				amp.genHTML(amp.di.set(data[jsonSet], {h: 70, w: 64, sm: 'CM'}), $('#amplience-carousel')[0], true);
				var ampNav = {
					img: $('#amplience-nav').find('img'),
					ul: $('#amplience-nav').find('ul')
				};
				ampNav.img.ampImage({preload: 'visible'});
			}

			// set zoombox fosset from top
			$('#zoom-box').css({top: ($('.amplience-viewer').offset().top - 50) + 'px'});
			ampCach.img.ampZoom({
				translations: 'sm=CM',
				preload: {
					image: 'created',
					zoomed: 'none'
				},
				zoom: 2,
				cursor: {
					active: 'zoom-out',
					inactive: 'zoom-in'
				}
			});
			$('#amplience-main').on('mouseenter', function () {
				ampLoading = true;
			}).on('mouseleave', function () {
				ampLoading = false;
			});
			$('.amplience-viewer ul img').each(function () {
				$(this).attr('src', $(this).attr('data-amp-src'));
			});
			if ($(".product-video-overlay-container").length > 0 && !util.isMobileSize()) {
				var video = $(".product-video-overlay-container").html();
				$(".product-video-overlay-container").remove();
				$("#amplience-nav ul").append("<li>" + video + "</li>");
			}
			if ($(".product-video-amplience-overlay-container").length > 0 && !util.isMobileSize()) {
				var video = $(".product-video-amplience-overlay-container").html();
				$(".product-video-amplience-overlay-container").remove();
				$("#amplience-nav ul").append("<li>" + video + "</li>");
			}
			$('#amplience-nav, .product-video-overlay-container').on('click', '.product-video-overlay', function (e) {
				var src = $(this).attr('data-video-src'),
				$iframe = $('<div class="product-video-wrapper"><iframe src="http://www.youtube.com/embed/' + src + '?enablejsapi=1&hl=en&fs=1&showinfo=0&autoplay=1&wmode=transparent" frameborder="0" width="100%" height="600" hei allowfullscreen></iframe></div>');
				dialog.open({
					html: $iframe,
					options: {
						autoOpen: true,
						dialogClass: 'product-video-dialog',
						title: Resources.VIDEO_TITLE,
						close: function () {
							$(this).find('iframe').get(0).contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
						}
					}
				});
			});

			function parseVideoSrc(src) {
				var $iframe = '<div class="product-video-wrapper-ampilence"><video controls="true" autoplay="true" id="ampilence-video" width="100%">';
				$('.product-video-amplience').each(function () {
					var videoSrc = $(this).attr('amplience-video-src'),
						videoExt = videoSrc.split('/')[videoSrc.split('/').length - 1],
						videoType = $(videoExt).size() > 0 ? videoExt.split('_')[0] : 'mp4';
					$iframe += '<source src="https://' + SitePreferences.amplienceHost + '/v/' + SitePreferences.amplienceClientId + '/' + $(this).attr('amplience-video-src') + '" type="video/' + videoType + '"></source>';
				});
				$iframe += '</video></div>';

				return $iframe;
			};

			$('#amplience-nav, .product-video-amplience-overlay-container').on('click', '.product-video-amplience-overlay', function (e) {
				var $iframe = parseVideoSrc($(this).attr('amplience-video-src'));
				dialog.open({
					html: $iframe,
					options: {
						autoOpen: true,
						dialogClass: 'product-video-dialog',
						title: Resources.VIDEO_TITLE,
						close: function () {
							var vid = document.getElementById("ampilence-video");
							vid.pause();
						}
					}
				});
			});
			$('#amplience-main ul').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: true,
				accessibility: true,
				fade: true,
				slide: 'li',
				lazyLoad: 'ondemand',
				asNavFor: '#amplience-nav ul',
				infinite: false,
				responsive: [{
						breakpoint: util.mobileWidth,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
							accessibility: true,
							centerMode: true,
							arrows: true,
							dots: true
						}
					}
				]
			});
			$('#amplience-nav ul').slick({
				slidesToShow: 4,
				slidesToScroll: 1,
				asNavFor: '#amplience-main ul',
				dots: false,
				slide: 'li',
				focusOnSelect: true,
				lazyLoad: 'ondemand',
				infinite: false,
					responsive: [
						{
							breakpoint: util.desktopXLargeWidth,
							settings: {
								slidesToShow: 4,
								slidesToScroll: 1,
								accessibility: true
							}
						},{
							breakpoint: util.tabletWidth,
							settings: {
								slidesToShow: 5,
								slidesToScroll: 1,
								accessibility: true,
								arrows: false
							}
						}
					]
			});
		});
	},
	initManyProductImages: function (ampset, target) {
		target.each(function (e) {
			var container = $(this);
			var jsonSet = ampset != null && ampset != '' ? ampset : $(this).find('.product-meta').data('jsonset'),
			sash = $(this).find('.product-meta').data('sash'),
			ampLoading = true;
			// setup amplience instance
			amp.init({
				client_id: SitePreferences.amplienceClientId,
				di_basepath: 'https://' + SitePreferences.amplienceHost + '/',
				errImg: '../shared/404image.jpg'
			});
			amp.get([{'name': jsonSet, 'type': 's'}], function (data) {
				amplience.destroyBonus(container);
				var dis = amp.di.set(data[jsonSet], {h: ampHeight, w: ampWidth, sm: 'CM'});
				if (sash) {
					dis['items'][0]['src'] += sash;
				}
				amp.genHTML(dis, container.find('.amplience-main')[0], true);
				var ampCach = {
					ul: container.find('.amplience-main').find('ul'),
					img: container.find('.amplience-main').find('img')
				};
				if (data[jsonSet].items.length > 1) {
					for (var i = 0; i < data[jsonSet].items.length; i++) {
						var item = data[jsonSet].items[i];
						if (item.set) {
							item.type = 'img';
						}
					}
				}
				container.find('.amplience-viewer ul img').each(function () {
					$(this).attr('src', $(this).attr('data-amp-src'));
				});

			});
		});
	},
	// Destroy current PDP viewer
	destroy: function () {
		$('#amplience-main').html('');
		$('#amplience-carousel').html('');
	},
	destroyBonus: function (container) {
		var containerThis = container;
		containerThis.find('.amplience-main').html('');
	}
};
module.exports = amplience;