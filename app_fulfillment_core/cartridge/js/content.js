'use strict';

var dialog = require('./dialog');

var content = {
	init: function () {
		this.initHeader();
	},

	initHeader: function () {
		var $menu = $('.header-top__utility-menu'),
				$toggleMenuBtn = $('.header-top__utility-menu-toggle');

		function observeToggleMenuButton() {
			$toggleMenuBtn.on('click', function (e) {
				e.preventDefault();
				$menu.toggleClass('hidden');
			});
		}

		function hideMenuOnClickAway() {
			$(document).click(function(event) {
				if(!$(event.target).closest('.header-top__utility-menu-toggle').length) {
					if($menu.is(":visible")) {
						$menu.toggleClass('hidden');
					}
				}
			});
		}

		observeToggleMenuButton();
		hideMenuOnClickAway();
	}
};

module.exports = content;
