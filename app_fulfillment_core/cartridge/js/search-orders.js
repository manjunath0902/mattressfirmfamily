'use strict';

var $searchOrders = $('#search-orders');

var searchOrders = {

  init: function() {
    if ($searchOrders.length > 0) {
      this.bindUIActions();
      this.checkInitialSelection();
    }
  },

  clearFields: function () {
    $("#dwfrm_searchorders_searchvalue_eventname").val("");
    $(".search-orders-date-fields__input-text").val("");
    $("#dwfrm_searchorders_searchvalue_ordernumber").val("");
    $("#dwfrm_searchorders_searchvalue_storeid").val("");
  },

  checkInitialSelection: function() {
    if ($("#search-orders-order-number").is(":checked")) {
      searchOrders.revealFields(".search-orders-order-number-fields");
    }
    else if ($("#search-orders-date").is(":checked")) {
      searchOrders.revealFields(".search-orders-date-fields");
    }
    else if ($("#search-orders-event").is(":checked")) {
      searchOrders.revealFields(".search-orders-event-fields");
    }
    else if ($("#search-orders-store-id").is(":checked")) {
      searchOrders.revealFields(".search-orders-store-id-fields");
    }
  },

  onCancelButtonClick: function () {
    $("#search-orders-cancel").off().on("click", function(e) {
      e.preventDefault();
      searchOrders.clearFields();
    })
  },

  bindUIActions: function() {
    $(".form-radio-holder").find(".form-check-input").on("change", this.applyRevealConditional);
    $(".search-orders-date-range").datepicker();
    this.onCancelButtonClick();
  },

  revealFields: function(fieldEl) {
     $(fieldEl).show().siblings().hide();
  },

  applyRevealConditional: function() {
    var el = $(this),
       self = this;

    searchOrders.clearFields();

    if ($(el).is("#search-orders-event") && $(el).is(":checked")) {
     searchOrders.revealFields(".search-orders-event-fields");
    }
    else if ($(el).is("#search-orders-date") && $(el).is(":checked")) {
     searchOrders.revealFields(".search-orders-date-fields");
    }
    else if ($(el).is("#search-orders-order-number") && $(el).is(":checked")) {
     searchOrders.revealFields(".search-orders-order-number-fields");
    }
    else if ($(el).is("#search-orders-store-id") && $(el).is(":checked")) {
     searchOrders.revealFields(".search-orders-store-id-fields");
    }
  }

}

module.exports = searchOrders;
