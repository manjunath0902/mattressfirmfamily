'use strict';

var util = require('./util'),
    dialog = require('./dialog'),
    page = require('./page'),
    welcomeMessage = require('./welcome-message'),
    Rules = require('./validation-rules');

/**
 * @private
 * @function
 * @description Bind DataTable sorting functionality to event tables
 */
function initEventTables() {
  $('table.display').dataTable({
    'paging':   false,
    'bLengthChange': false,
    'searching': false,
    "columns": [
        null,
        null,
        { "orderable": false },
        null,
        { "orderable": false },
        { "orderable": false },
        { "orderable": false },
        { "orderable": false },
        { "orderable": false }
      ]
  });
}

/**
 * @private
 * @function
 * @description Display name of file being uploaded
 */
function initOnUploadFile() {
	var csvRegex = new RegExp("(.*?)\.(csv)$");
	$('.inputfile').each(function () {
    var $input = $(this),
        $label = $input.next('label'),
        $errorLabel = $label.next('label'),
        labelVal = $label.html();

    $input.on('change', function (e) {
      var fileName = '';

      if (this.files && this.files.length > 1) {
        fileName = ( this.getAttribute('data-multiple-caption') || '' ).replace( '{count}', this.files.length );
      }
      else if (e.target.value) {
        fileName = e.target.value.split( '\\' ).pop();
      }

      if (fileName) {
        $label.find('span').html(fileName);
      }
      else {
        $label.html( labelVal );
      }

      var submitBtn = $('form button[type=submit]');

      if (fileName && !(csvRegex.test(fileName))) {
    	  $errorLabel.html(Resources.VALIDATE_FILE_TYPE);
	  } else {
		  $errorLabel.html('');
	  }

      util.validateForm("#events-form", Rules.AddEventRules);

    });

    // Firefox bug fix
    $input.on({
      'focus': function () {
        $input.addClass( 'has-focus' );
      },
      'blur': function () {
        $input.removeClass( 'has-focus' );
      }
    })
  })
}

/**
 * @private
 * @function
 * @description Binds events on the dashboard page
 */
 function initDashboardEvents() {
   $('.dashboard').on('click','.event-edit, .event-create', function(e) {
     e.preventDefault();
     // Set eventdate input as datepicker
     $('#dwfrm_createevent_event_eventdate').datepicker({
       autoOpen: false,
       minHeight: 628,
       minWidth: 889
     });

     dialog.open({
        url: $(e.target).attr('href'),
        modal: true,
        options: {
          open: function (event, ui) {

          var $dialog = $(this);

          $('#btnCancel').on('mousedown', function(e) {
            e.preventDefault();
            $dialog.dialog("close");
          });

          $('#events-form').find('input,select').on('change', function() {
            var empty = false;

            $('#events-form input, #events-form select').each(function () {
              if ($(this).hasClass('error')){
                 empty = true;
                 $(this).trigger('change');
               }
            });

            if (empty){
              $('.save-btn').attr('disabled','disabled');

            }
            else {
              $('.save-btn').removeAttr('disabled').on('click', function () {
                $('#events-form .btn-group-sm').prepend('<span class="loading-indicator"></span>');
                $dialog.dialog("close");
              });
            }
          });


          util.bindEventsToValidateForm('#events-form', Rules.AddEventRules);
            // If we clicked on event-edit, set date and show appropriate title
            if ($(e.target).attr('class').indexOf('event-edit') !== -1) {
              $('#dwfrm_createevent_event_eventdate').val( convert( $("#dwfrm_createevent_event_eventdate").val() ) );
              $('span.ui-dialog-title').html(Resources.EVENTEDIT_TITLE);
            } // Else we have clicked on event-create, gets current date and different header.
            else {
              $('#dwfrm_createevent_event_eventdate').val(moment().local().format('L'));
              $('span.ui-dialog-title').html(Resources.EVENTCREATE_TITLE);
            }
            initOnUploadFile();
            //$('#dwfrm_createevent_event_eventdate').val(moment($("#dwfrm_createevent_event_eventdate").val()).format('L'));
            $( "#dwfrm_createevent_event_eventdate" ).datepicker();
          }
        },
        failure: function (xhr, status, error) {
        	util.handleAjaxFailure(xhr, status, error, {continueUrl: Urls.fulfillmentDashboard});
        }
      })
   });

   // Edit Customer Info on Order Details page
   $(document).on('click', '#edit-customer-info-on-order', function (e) {
	    e.preventDefault();
	    var login_url = encodeURI($(this).attr('href')+'&format=ajax');

		$.ajax({
			url: login_url,
			type: 'GET'
		})
		// success
		.done(function (data, response) {
			if(response && data.indexOf('eventDeleted') > -1) { // Event does not exist
				window.location = util.appendParamsToUrl(Urls.fulfillmentDashboard, {eventDeleted: true});
				return;
			}
			dialog.open({
				html: $(data),
				options: {
					autoOpen: true,
					dialogClass: 'data-class edit-customer-info-dialogue',
					title: 'Edit Customer Information'
				}
			});
			$('#btnCancel').on('mousedown', function(e) {
		       e.preventDefault();
		       dialog.close();
		      });
			var rules = Rules.EditCustomerRules;

			$('#dwfrm_editcustomerinfo_customer_phone').mask('000-000-0000');
    	    util.bindEventsToValidateForm('#dwfrm_editcustomerinfo', rules, ['dwfrm_editcustomerinfo_customer_email', 'dwfrm_editcustomerinfo_customer_phone']);
		})
		.error(function (xhr, status, error) {
			util.handleAjaxFailure(xhr, status, error, {continueUrl: Urls.fulfillmentOrderSummary, params: util.getQueryString(window.location.href)});
	    });
	});
 }

// Pagination of Events on Dashboard
function getEventsForThisPage(pageNo) {
	var success = true;
	if ($('#currentPageEventsData').length) {
		var eventsCompleteData = $("#currentPageEventsData").data('events');
	}

	// Make chunks of the Event Data to show limited data per page
	var eventsArr = eventsCompleteData.eventsObjs; //JSON.parse(eventsCompleteData).eventsObjs;
	var eventsChunkArr = [];
	var pageNumber = pageNo || 1; // Page Number of Event on storefront
	var pageSize = 3;	// Number of Events to show per page

	// Creating Page Indexes (only on page load)
	if (pageNo == null || pageNo == undefined) {
		var pageCount = eventsArr.length / pageSize;
		var currentlyShowingEventNumbers = '';
		if (pageCount > 1) {
			$('#pagination').removeClass('visually-hidden');
		} else {
			$('#pagination').addClass('visually-hidden');
		}
	} // End Creating Page Indexes

	for (var i=(pageNumber-1)*pageSize, si=i+1, ei=i; (i <= pageNumber*pageSize-1) && (i < eventsArr.length); i++, ei++) {
		eventsChunkArr.push(eventsArr[i]);
	}

	var eventsChunkJson = {
		eventsObjs : []
	};
	for (var j=0; j < eventsChunkArr.length; j++) {
		eventsChunkJson.eventsObjs.push(eventsChunkArr[j]);
	}
	var eventsDataStrChunk = JSON.stringify(eventsChunkJson);
	// end making chunks logic

	$.ajax({
		url: util.appendParamsToUrl(Urls.getEventsForThisPage, {format: 'ajax'}),
		type	: 'POST',
		data	: {eventsDataStrChunk:eventsDataStrChunk},
		async	: false,
		success: function (response) {
	    	if (response) {
	    		if ($('#currentPageEventsData').length) {
	    			$("#currentPageEventsData").html(response);
	    		}
			}
		},
		error: function(xhr, status, error){
			success = false;
			util.handleAjaxFailure(xhr, status, error, {continueUrl: Urls.fulfillmentDashboard});
		},
		failure: function () {
			alert("Fail");
			window.alert(Resources.SERVER_ERROR);
		}
	});
	if(success) {
		// Update currently showing event numbers
		if ($("#currentlyShowingEventNumbers").length > 0 && !($('#pagination').hasClass('visually-hidden'))) {
			var updateCurrentlyShowingEventNumbers = '';
			updateCurrentlyShowingEventNumbers += si.toString() + '-' + ei.toString() + ' of ' + (eventsArr.length).toString();
			$('#currentlyShowingEventNumbers').text(updateCurrentlyShowingEventNumbers);
		}
	}
	return success;
}

function rescheduleExpiredOrdersOnLogin(){
	var noError = false;
	$.ajax({
		url: util.appendParamsToUrl(Urls.rescheduledOrdersOnLogin, {format: 'ajax'}),
		type	: 'GET',
		async	: false,
		success: function (response) {
			noError  = true;
		},
		failure: function () {
			alert("Fail");
			window.alert(Resources.SERVER_ERROR);
		}
	});
	return noError;
}
var res = false;
$(document).ready(function() {
  welcomeMessage.init();
	// Get Events of 1st page on page load

	if ($('#currentPageEventsData').length) {
//		res = rescheduleExpiredOrdersOnLogin();
//		if(res) {
			getEventsForThisPage();
		//}

	}


	// On click: Previous
	$("#prevEventsPage").on( "click", function(e) {
		e.preventDefault();
		var targetPageNumber = $("#prevEventsPage").data('pagenumber');
		var successResp = true;
		if (targetPageNumber >= 1) {
			// Get Events of previous page
			successResp = getEventsForThisPage(parseInt(targetPageNumber));
			// Decrement target page numbers
			if (successResp && targetPageNumber > 1) {
				$("#prevEventsPage").data('pagenumber',
						parseInt(targetPageNumber)-1);
				$("#nextEventsPage").data('pagenumber',
						parseInt($("#nextEventsPage").data('pagenumber'))-1);
			}
		}
		if(successResp) {
			// Bind sorting on event tables, post click.
			initEventTables();
		}
		$(window).scrollTop(0);
	});

	// On click: Next
	$("#nextEventsPage").on( "click", function(e) {
    e.preventDefault();
		var eventsCount = parseInt($("#currentPageEventsData").data('eventcount')),
			pageSize = 3,
			targetPageNumber = $("#nextEventsPage").data('pagenumber');
		var successResp = true;
		var pageCount = Math.ceil(eventsCount / pageSize);
		if (targetPageNumber <= pageCount) {
			// Get Events of next page
			successResp = getEventsForThisPage(parseInt(targetPageNumber));
			// Increment target page numbers
			if (successResp && targetPageNumber < pageCount) {
				$("#nextEventsPage").data('pagenumber',
						parseInt(targetPageNumber)+1);
				$("#prevEventsPage").data('pagenumber',
						parseInt($("#prevEventsPage").data('pagenumber'))+1);
			}
		}
		if(successResp) {
			// Bind sorting on event tables, post click.
			initEventTables();
		}
		$(window).scrollTop(0);
	});
});


 function redirectToLogin(continueUrl, params) {
	var url = Urls.loginRedirect;
	if(continueUrl){
		url += "?original=" + encodeURIComponent(continueUrl);
		if(params) {
			url += encodeURIComponent("?" + params);
		}
	}

	window.location.href = url;
}

 function convert(str) {
	 var myDate	= str,
	 	 strMnth = myDate.substring(4,7).toLowerCase(),
	 	 day = myDate.substring(8,10),
	 	 year = myDate.substring(24,28),
	 	 strMonth ;

	 if(strMnth == 'jan')
		 strMonth = '01';
	 else if(strMnth == 'feb')
		 strMonth = '02';
	 else if(strMnth == 'mar')
		 strMonth = '03';
	 else if(strMnth == 'apr')
		 strMonth = '04';
	 else if(strMnth == 'may')
		 strMonth = '05';
	 else if(strMnth == 'jun')
		 strMonth = '06';
	 else if(strMnth == 'jul')
		 strMonth = '07';
	 else if(strMnth == 'aug')
		 strMonth = '08';
	 else if(strMnth == 'sep')
		 strMonth = '09';
	 else if(strMnth == 'oct')
		 strMonth = '10';
	 else if (strMnth == 'nov' )
		 strMonth = '11';
	 else if(strMnth == 'dec')
		 strMonth = '12';

	 return strMonth +'/'+day+'/'+year;
	}
var dashboard = {
   init: function() {
     initDashboardEvents();
     initEventTables();
   }
 };

 module.exports = dashboard;
