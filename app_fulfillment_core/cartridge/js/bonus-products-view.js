'use strict';

var dialog = require('./dialog'),
	page = require('./page'),
	amplience = require('./amplience'),
	util = require('./util');

var selectedList = [];
var maxItems = 1;
var bliUUID = '';

/**
 * @private
 * @function
 * description Gets a list of bonus products related to a promoted product
 */
function getBonusProducts() {
	var o = {};
	o.bonusproducts = [];

	var i, len;
	for (i = 0, len = selectedList.length; i < len; i++) {
		var p = {
			pid: selectedList[i].pid,
			qty: selectedList[i].qty,
			options: {}
		};
		var a, alen, bp = selectedList[i];
		if (bp.options) {
			for (a = 0, alen = bp.options.length; a < alen; a++) {
				var opt = bp.options[a];
				p.options = {optionName:opt.name, optionValue:opt.value};
			}
		}
		o.bonusproducts.push({product:p});
	}
	return o;
}

var selectedItemTemplate = function (data) {
	var attributes = '';
	for (var attrID in data.attributes) {
		var attr = data.attributes[attrID];
		attributes += '<li data-attribute-id="' + attrID + '">\n';
		attributes += '<span class="display-name">' + attr.displayName + '</span>: ';
		attributes += '<span class="display-value">' + attr.displayValue + '</span>\n';
		attributes += '</li>';
	}
	attributes += '<li class="item-qty">\n';
	attributes += '<span class="display-name">Qty</span>: ';
	attributes += '<span class="display-value">' + data.qty + '</span>';
	return [
		'<li class="selected-bonus-item" data-uuid="' + data.uuid + '" data-pid="' + data.pid + '">',
		'<div class="item-name">' + data.name + '<a class="remove-link" title="Remove this product" href="#">(Remove)</a></div>',
		'<ul class="item-attributes">',
		attributes,
		'<ul>',
		'<li>'
	].join('\n');
};

// hide swatches that are not selected or not part of a Product Variation Group
var hideSwatches = function () {
	$('.bonus-product-item:not([data-producttype="master"]) .swatches li').not('.selected').not('.variation-group-value').hide();
	// prevent unselecting the selected variant
	$('.bonus-product-item .swatches .selected').on('click', function () {
		return false;
	});
};

/**
 * @private
 * @function
 * @description Updates the summary page with the selected bonus product
 */
function updateSummary() {
	var $bonusProductList = $('#bonus-product-list');
	var allQty = 0;
	if (selectedList.length === 0) {
		$bonusProductList.find('li.selected-bonus-item').remove();
	} else {
		var ulList = $bonusProductList.find('ul.selected-bonus-items').first();
		ulList.empty();
		var i, len;
		for (i = 0, len = selectedList.length; i < len; i++) {
			var item = selectedList[i];
			allQty +=  parseInt(selectedList[i].qty);
			var li = selectedItemTemplate(item);
			$(li).appendTo(ulList);
		}
	}

	// get remaining item count
	var remain = maxItems - allQty;
	$bonusProductList.find('.bonus-items-available').text(remain);
	if (remain <= 0) {
		$bonusProductList.find('.select-bonus-item').attr('disabled', 'disabled');
		$('.add-to-cart-bonus-container').addClass('active');
	} else {
		$bonusProductList.find('.product-variations').each(function () {
			if (!$.isEmptyObject($(this).data('attributes'))) {
				$(this).next('.product-add-to-cart').find('.select-bonus-item').removeAttr('disabled');
				$('.add-to-cart-bonus-container').removeClass('active');
			}
		});
	}

}

function initializeGrid () {
	var $bonusProduct = $('#bonus-product-dialog'),
		$bonusProductList = $('#bonus-product-list'),
	bliData = $bonusProductList.data('line-item-detail');
	maxItems = bliData.maxItems;
	bliUUID = bliData.uuid;
	if (bliData.itemCount >= maxItems) {
		$bonusProductList.find('.select-bonus-item').attr('disabled', 'disabled');
	}
	if($('.variant-text').length>0) {
		$('.bonus-product-item').css('margin-top', '100px');
	}

	var cartItems = $bonusProductList.find('.selected-bonus-item');
	cartItems.each(function () {
		var ci = $(this);
		var product = {
			uuid: ci.data('uuid'),
			pid: ci.data('pid'),
			qty: ci.find('.item-qty span.display-value').text(),
			name: ci.find('.item-name').html(),
			attributes: {}
		};
		var attributes = ci.find('ul.item-attributes li');
		attributes.each(function () {
			var li = $(this);
			product.attributes[li.data('attributeId')] = {
				displayName:li.children('.display-name').html(),
				displayValue:li.children('.display-value').html()
			};
		});
		selectedList.push(product);
		$('.add-to-cart-bonus-container').addClass('active');
	});

	$bonusProductList.on('change', '.swatches', function (e) {
		e.preventDefault();
		var url = $(this).val(),
			$this = $(this),
			ampset = '';
		if ($(this).hasClass("color")) {
			//If the swatch that was clicked has the class "selected" at this point, the swatch is in process of being deselected.
			if ($(this).hasClass("selected")) {
				ampset = '';
			} else {
				//The swatch is being selected at this point, so look for that color-based image.
				ampset = $(this).find(':selected').data('ampset');
			}
		}
		url = util.appendParamsToUrl(url, {
			'source': 'bonus',
			'format': 'ajax'
		});
		$.ajax({
			url: url,
			success: function (response) {
				var parentContainer = $this.closest('.bonus-product-item');
				if (ampset != '') {
					parentContainer.empty().html(response);
					amplience.initManyProductImages(ampset, parentContainer.find('.col-1'));
				} else {
					var imageContent = parentContainer.find('.col-1').html();
					parentContainer.empty().html(response);
					parentContainer.find('.col-1').html(imageContent);
				}
				hideSwatches();
				util.uniform();
			}
		});
	})
	.on('change', '.input-text', function () {
		$bonusProductList.find('.select-bonus-item').removeAttr('disabled');
		$(this).closest('.bonus-product-form').find('.quantity-error').text('');
	})
	.on('click', '.select-bonus-item', function (e) {
		e.preventDefault();
		var itemsQty = $('#maxBonusItemsCount').val(), n;
		var form = $(this).closest('.bonus-product-form'),
		detail = $(this).closest('.product-detail'),
		uuid = form.find('input[name="productUUID"]').val();
		if (selectedList.length > 0) {
			for (n = 0; n < selectedList.length; n++) {
				if (selectedList[n].uuid === uuid) {
					itemsQty = itemsQty + parseInt(selectedList[n].qty);
				}
			}
		}
		if (selectedList.length >= maxItems) {
			$bonusProductList.find('.select-bonus-item').attr('disabled', 'disabled');
			$bonusProductList.find('.bonus-items-available').text('0');
			return;
		}
		var qtyVal = form.find('input[name="Quantity"]').val(),
			qty = (isNaN(qtyVal)) ? itemsQty : (+qtyVal);
		if (qty > maxItems) {
			$bonusProductList.find('.select-bonus-item').attr('disabled', 'disabled');
			form.find('.quantity-error').text(Resources.BONUS_PRODUCT_TOOMANY);
			return;
		}
		var i, len = selectedList.length;
		if (selectedList.length > 0) {
			for (i = 0; i < len; i++) {
				if (selectedList[i].uuid === uuid) {
					qty = qty + parseInt(selectedList[i].qty);
					selectedList.splice(i, 1);
					break;
				}
			}
		}

		var product = {
			uuid: uuid,
			pid: form.find('input[name="pid"]').val(),
			qty: qty,
			name: detail.find('.product-name').text(),
			attributes: detail.find('.product-variations').data('attributes'),
			options: []
		};

		var optionSelects = form.find('.product-option');

		optionSelects.each(function () {
			product.options.push({
				name: this.name,
				value: $(this).val(),
				display: $(this).children(':selected').first().html()
			});
		});
		selectedList.push(product);
		//commented because we don't need summary when product added
		//updateSummary();
	})
	.on('click', '.remove-link', function (e) {
		e.preventDefault();
		var container = $(this).closest('.selected-bonus-item');
		if (!container.data('uuid')) { return; }

		var uuid = container.data('uuid');
		var i, len = selectedList.length;
		for (i = 0; i < len; i++) {
			if (selectedList[i].uuid === uuid) {
				selectedList.splice(i, 1);
				break;
			}
		}
		//updateSummary();
	})
	.on('click', '.add-to-cart-bonus', function (e) {
		e.preventDefault();
		var selectBonusItem = $(this).next();
		$(selectBonusItem).trigger('click');
		var url = util.appendParamsToUrl(Urls.addBonusProduct, {bonusDiscountLineItemUUID: bliUUID});
		var bonusProducts = getBonusProducts();
		bonusProducts.bonusproducts[0].product.pid = $($(this).parent().children()[0]).val();
		if (bonusProducts.bonusproducts[0].product.qty > maxItems) {
			bonusProducts.bonusproducts[0].product.qty = maxItems;
		}
		$('.add-to-cart-bonus').removeAttr('disabled','disabled');
        $(this).attr('disabled','disabled');
		// make the server call
		$.ajax({
			type: 'POST',
			dataType: 'json',
			cache: false,
			contentType: 'application/json',
			url: url,
			data: JSON.stringify(bonusProducts)
		})
		.done(function () {
			$('.ui-dialog-titlebar-close').trigger('click');
			page.refresh();
			// success
			//page.refresh();
		})
		.fail(function (xhr, textStatus) {
			// failed
			if (textStatus === 'parsererror') {
				window.alert(Resources.BAD_RESPONSE);
			} else {
				window.alert(Resources.SERVER_CONNECTION_ERROR);
			}
		})
		.always(function () {
			//$bonusProduct.dialog('close');
		});
	})
	.on('click', '#more-bonus-products', function (e) {
		e.preventDefault();
		var uuid = $('#bonus-product-list').data().lineItemDetail.uuid;

		//get the next page of choice of bonus products
		var lineItemDetail = JSON.parse($('#bonus-product-list').attr('data-line-item-detail'));
		lineItemDetail.pageStart = lineItemDetail.pageStart + lineItemDetail.pageSize;
		$('#bonus-product-list').attr('data-line-item-detail', JSON.stringify(lineItemDetail));

		var url = util.appendParamsToUrl(Urls.getBonusProducts, {
			bonusDiscountLineItemUUID: uuid,
			format: 'ajax',
			lazyLoad: 'true',
			pageStart: lineItemDetail.pageStart,
			pageSize: $('#bonus-product-list').data().lineItemDetail.pageSize,
			bonusProductsTotal: $('#bonus-product-list').data().lineItemDetail.bpTotal
		});

		$.ajax({
			type: 'GET',
			cache: false,
			contentType: 'application/json',
			url: url
		})
		.done(function (data) {
			//add the new page to DOM and remove 'More' link if it is the last page of results
			$('#more-bonus-products').before(data);
			if ((lineItemDetail.pageStart + lineItemDetail.pageSize) >= $('#bonus-product-list').data().lineItemDetail.bpTotal) {
				$('#more-bonus-products').remove();
			}
		})
		.fail(function (xhr, textStatus) {
			if (textStatus === 'parsererror') {
				window.alert(Resources.BAD_RESPONSE);
			} else {
				window.alert(Resources.SERVER_CONNECTION_ERROR);
			}
		});
	});
	$('.bonus-product-thanks-class').on('click', function (e) {
		e.preventDefault();
		var $bonusProduct = $('#bonus-product-dialog');
		$bonusProduct.dialog('close');
		page.refresh();
	});
}

var bonusProductsView = {
	/**
	 * @function
	 * @description Open the list of bonus products selection dialog
	 */
	show: function (params) {
		var $bonusProduct = $('#bonus-product-dialog');
		var dialogWidth = (SessionAttributes.BONUS_PRODUCT_WIDTH) || 650;
		// create the dialog
		dialog.open({
			target: $bonusProduct,
			url: params.url,
			options: {
				width: dialogWidth,
				title: Resources.BONUS_PRODUCTS
			},
			callback: function () {
				initializeGrid();
				hideSwatches();
				util.uniform();
				$(".ui-dialog-title").show();
				$(".ui-dialog").addClass('bonus-product-popup');
				amplience.initManyProductImages("", $('.col-1'));
				$(".ui-dialog").find('a.continue').on('click', function (e) {
					e.preventDefault();
					$('.ui-dialog-titlebar-close').trigger('click');				
				});
				$(".ui-dialog").find('.ui-icon-closethick').on('click', function (e) {
					e.preventDefault();					
					//page.refresh();
				});
			}
		});
	},
	/**
	 * @function
	 * @description Open bonus product promo prompt dialog
	 */
	loadBonusOption: function () {
		var	self = this,
			bonusDiscountContainer = document.querySelector('.bonus-discount-container');
		if (!bonusDiscountContainer) { return; }

		// get the html from minicart, then trash it
		var bonusDiscountContainerHtml = bonusDiscountContainer.outerHTML;
		bonusDiscountContainer.parentNode.removeChild(bonusDiscountContainer);

		dialog.open({
			html: bonusDiscountContainerHtml,
			options: {
				width: 400,
				title: Resources.BONUS_PRODUCT,
				buttons: [{
					text: Resources.SELECT_BONUS_PRODUCTS,
					click: function () {
						var uuid = $('.bonus-product-promo').data('lineitemid'),
							url = util.appendParamsToUrl(Urls.getBonusProducts, {
								bonusDiscountLineItemUUID: uuid,
								source: 'bonus',
								format: 'ajax',
								lazyLoad: 'false',
								pageStart: 0,
								pageSize: 10,
								bonusProductsTotal: -1
							});
						$(this).dialog('close');
						self.show(url);
					}
				}, {
					text: Resources.NO_THANKS,
					click: function () {
						$(this).dialog('close');
					}
				}]
			},
			callback: function () {
				// show hide promo details
				$('.show-promo-details').on('click', function () {
					$('.promo-details').toggleClass('visible');
				});
			}
		});
	}
};

module.exports = bonusProductsView;
