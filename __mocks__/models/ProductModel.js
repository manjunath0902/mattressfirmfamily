'use strict';

function ProductModel() {
    this.base = {};
}
var ProductMock = require('./../__mocks__/dw/catalog/Product');


ProductModel.prototype = {
    get: function get(id) {
        let object = new ProductMock();
        object.manufacturerSKU = id;
        object.variationModel = "variationModel";

        /** Returning Product Object in Same Way as Product Model Returns */
        let product = new Object();
        product.object = object;
        return product;
    }
};

module.exports = ProductModel;