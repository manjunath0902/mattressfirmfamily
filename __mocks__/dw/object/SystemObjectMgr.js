var Profile = require('../customer/Profile')
var Iterator = require('../util/Iterator')

var SystemObjectMgr = function(){};

var defaultValue = {
    'Profile': new Iterator ([ new Profile() ])
}
var systemObjectsAvailable = defaultValue;

SystemObjectMgr.setTestValuesForObject = function(objName, valuesArr) {
    systemObjectsAvailable[objName] = new Iterator(valuesArr)
}
SystemObjectMgr.restoreDefaultTestValues = function() {
    systemObjectsAvailable = defaultValue;
}
SystemObjectMgr.describe = function(){};
SystemObjectMgr.querySystemObject = function(){};
SystemObjectMgr.getAllSystemObjects = function(systemObjName){
    if (systemObjectsAvailable[systemObjName]) {
        return systemObjectsAvailable[systemObjName]
    }
    throw new Error('No system object with name ' + systemObjName)
};
SystemObjectMgr.querySystemObjects = function(){};
SystemObjectMgr.prototype.allSystemObjects=null;

module.exports = SystemObjectMgr;