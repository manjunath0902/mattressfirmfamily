var Site = function(id){
    this.customPreferences = {};
    this.ID = id || "Mattress-Firm";
};

var currentTestSite = null;

Site.prototype.setTestCusomPreferences = function (preferences) {
    this.customPreferences = preferences;
}
Site.setCurrentTestSite = function(site){ currentTestSite = site }

Site.prototype.getCurrencyCode = function(){};
Site.prototype.getName = function(){};
Site.prototype.getID = function(){};
Site.prototype.setID = function(id){this.ID = id || "Dummy-Site";};

Site.getCurrent = function(){return currentTestSite || new Site()};
Site.prototype.getPreferences = function(){};
Site.prototype.getHttpHostName = function(){};
Site.prototype.getHttpsHostName = function(){};
Site.prototype.getCustomPreferenceValue = function(key){ return this.customPreferences[key] };
Site.prototype.setCustomPreferenceValue = function(key, value){ this.customPreferences[key] = value };
Site.prototype.getDefaultLocale = function(){};
Site.prototype.getAllowedLocales = function(){};
Site.prototype.getAllowedCurrencies = function(){};
Site.prototype.getDefaultCurrency = function(){};
Site.prototype.getTimezone = function(){};
Site.prototype.getTimezoneOffset = function(){};
Site.getCalendar = function(){return new require('../util/Calendar')();};
Site.prototype.isOMSEnabled = function(){};
Site.prototype.currencyCode=null;
Site.prototype.name=null;
Site.prototype.ID=null;
Site.prototype.current=null;
Site.prototype.preferences=null;
Site.prototype.httpHostName=null;
Site.prototype.httpsHostName=null;
Site.prototype.customPreferenceValue=null;
Site.prototype.defaultLocale=null;
Site.prototype.allowedLocales=null;
Site.prototype.allowedCurrencies=null;
Site.prototype.defaultCurrency=null;
Site.prototype.timezone=null;
Site.prototype.timezoneOffset=null;
Site.prototype.calendar=null;

module.exports = Site;