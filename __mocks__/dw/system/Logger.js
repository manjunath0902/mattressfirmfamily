var Logger = function() {};

Logger.prototype.getLogger = function(){return this;};
Logger.prototype.isDebugEnabled = function(){return this.isDebug;};
Logger.prototype.isInfoEnabled = function(){return this.isInfo;};
Logger.prototype.isErrorEnabled = function(){return this.isError;};
Logger.prototype.error = function(msg){this.message = msg;};
Logger.prototype.debug = function(msg){this.message = msg;};
Logger.prototype.info = function(msg){this.message = msg;};

Logger.prototype.isDebug = true;
Logger.prototype.isInfo = true;
Logger.prototype.isError = true;
Logger.prototype.message = '';

module.exports = Logger;