var System = function(){};

System.getPreferences = function(){};

System.getInstanceType = function() {
    return System.TEST_SYSTEM;
};

System.getInstanceTimeZone = function(){};
System.getInstanceHostname = function(){  return 'validhostname'};
System.getCalendar = function(){};
System.prototype.preferences=null;
System.prototype.instanceType=null;
System.prototype.instanceTimeZone=null;
System.prototype.instanceHostname=null;
System.prototype.calendar=null;

System.PRODUCTION_SYSTEM = 0;
System.DEVELOPMENT_SYSTEM = 1;
System.TEST_SYSTEM = 3;
module.exports = System;