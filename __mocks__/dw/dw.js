module.exports = {
    order: {
        ProductShippingLineItem : require('./order/ProductShippingLineItem'),
        ProductLineItem         : require('./order/ProductLineItem'),
        ShippingLineItem        : require('./order/ShippingLineItem')
    }
}