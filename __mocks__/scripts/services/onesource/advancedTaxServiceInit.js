var DocumentAmount = function(amount) {
    this.amount = amount;
}
DocumentAmount.prototype.getDOCUMENT_AMOUNT = function() { return this.amount; }
DocumentAmount.prototype.getAUTHORITY_AMOUNT = function() { return this.amount; }
DocumentAmount.prototype.amount = null;

var TaxLine = function(taxableBasis, crossAmount) {
    this.taxableBasis = taxableBasis;
    this.crossAmount = crossAmount;
}
TaxLine.prototype.getERP_TAX_CODE = function() { return 'USHI'; };
TaxLine.prototype.getTAX_RATE = function() { return 0; };
TaxLine.prototype.getTAX_AMOUNT = function() { return new DocumentAmount(0.00); };
TaxLine.prototype.getTAXABLE_BASIS = function() { return new DocumentAmount(this.taxableBasis); };
TaxLine.prototype.getGROSS_AMOUNT = function() { return new DocumentAmount(this.taxableBasis); };
TaxLine.prototype.getINVOICE_DESCRIPTION = function() { return 'No Liability'};
TaxLine.prototype.getJURISDICTION_TEXT = function() { return 'USSG121: No Tax Applies Because Seller is not Established'};
TaxLine.prototype.getTAX_DIRECTION = function() {return null};
TaxLine.prototype.taxableBasis = null;
TaxLine.prototype.crossAmount = null;

function call(shipFrom, requestId, param) {
            
    var taxLines = {};
    var lines = param.order.lines;

    for (let i = 0; i < lines.length; i++) {
        taxLines[+lines[i].No - 1] = {
            uuid   : 'uuid_product',
            getTAX : function() {
                return [ new TaxLine(lines[i].amount, lines[i].amount) ]
            },
            getTAX_SUMMARY: function() {
                return {
                    getEFFECTIVE_TAX_RATE: function() { return 0; }    
                };
            },
            getTOTAL_TAX_AMOUNT: function() { return 0; },
        };
    }
    
    return {
        ok     : true,
        object : {
            getOUTDATA: function() {
                return {
                    getREQUEST_STATUS: function() {
                        return {
                            isIS_SUCCESS         : function() { return true; },
                            isIS_PARTIAL_SUCCESS : function() { return true; }
                        }
                    },
                    getINVOICE: function() {
                        return [{
                            getREQUEST_STATUS: function() {
                                return {
                                    isIS_SUCCESS: function() { return true; }
                                }
                            },
                            getLINE: function() {
                                return taxLines
                            }
                        }]
                    }
                }
            }
        }
    }
}

module.exports.call = call;