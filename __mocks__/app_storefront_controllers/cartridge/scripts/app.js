'use strict';

function app() {
    this.base = {};
};

app.prototype = {
    getModel: jest.fn().mockImplementation(() => {
        return require(mocksPath + modelName + 'Model');
      }),
    /*
    getModel: function getModel(modelName) {
        let mocksPath = './../../../__mocks__/models/';
        return require(mocksPath + modelName + 'Model');
    },
    */
    getView: function getView() {},
    getForm: function getForm(url) {}
};

module.exports = app;