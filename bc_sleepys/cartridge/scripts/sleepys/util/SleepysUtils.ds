importPackage( dw.system );
importPackage( dw.util );
importPackage( dw.order );
importPackage( dw.crypto );
importPackage( dw.web );

var SleepysHelper = {

	/***************************************************************************************
	 *	Name: 	getMasterStoreCode()
	 * 	Description: Retrieves the master store code for this site from custom site preference.
	 ***************************************************************************************/
	getMasterStoreCode : function() {
		
		var masterStoreCode : String = null;
		
		if(!empty(Site.getCurrent().getCustomPreferenceValue("masterStoreCode"))) {
			masterStoreCode = Site.getCurrent().getCustomPreferenceValue("masterStoreCode").toString();
		}
		
		return masterStoreCode;
		
	},
	
	/***************************************************************************************
	 *	Name: 	getMattressPickupProductID()
	 * 	Description: Retrieves the mattress pickup product id for this site from custom site preference.
	 ***************************************************************************************/
	getMattressPickupProductID : function() {
		
		var mattressPickupProductID : String = null;
		
		if(!empty(Site.getCurrent().getCustomPreferenceValue("mattressPickupID"))) {
			mattressPickupProductID = Site.getCurrent().getCustomPreferenceValue("mattressPickupID").toString();
		}
		
		return mattressPickupProductID;
		
	},	

	/***************************************************************************************
	 *	Name: 	adjustShippingCharges()
	 * 	Description: Resets all other shipments shipping costs to 0 if they are not the 
	 *					highest cost.  In the case where two shipments have the same cost,
	 *					the first one we run into will keep its cost and the other will be 
	 *					reset like the others.
	 ***************************************************************************************/	
	adjustShippingCharges : function(basket) {
		
		if (!empty(basket)) {

			var shipments : Iterator = basket.getShipments().iterator();

			var currentHighestPriceShipmentID = null;
			var currentHighestPriceShipmentPrice = null;
			
			// Loop through all the shipments and keep track of the highest priced shipping cost.
			while(shipments.hasNext()) {
				var shipment : Shipment = shipments.next();
				
				var shippingLineItem : ShippingLineItem = shipment.getStandardShippingLineItem();

				if (!empty(shippingLineItem)) {
					
					if (currentHighestPriceShipmentPrice == null) {
						if (shippingLineItem.getPrice() != null) {
							currentHighestPriceShipmentID = shipment.getID();
							currentHighestPriceShipmentPrice = shippingLineItem.getPrice();
						}
					}
					else {
						if (shippingLineItem.getPrice() != null) {
							if (shippingLineItem.getPrice() > currentHighestPriceShipmentPrice) { 
								currentHighestPriceShipmentID = shipment.getID();
								currentHighestPriceShipmentPrice = shippingLineItem.getPrice();
							}
						}
					}
				}
			}
			
			shipments = basket.getShipments().iterator();
			
			// Now that we have the highest priced shipping cost and its shipment id, loop through all the 
			// other shipments and set their prices to 0
			while(shipments.hasNext()) {
				var shipment : Shipment = shipments.next();
				
				if (shipment.getID() != currentHighestPriceShipmentID) {
					
					var shippingLineItem : ShippingLineItem = shipment.getStandardShippingLineItem();

					if (!empty(shippingLineItem)) {
						shippingLineItem.setPriceValue(0);			
					}
				}
			}
		}	
	},
	
	removeAllProductLineItems : function(basket) {
		
		for each (var pli : ProductLineItem in basket.getProductLineItems()){
			basket.removeProductLineItem(pli);
		}
	},
	
	removeAllShipments : function(basket, keepGiftCertShipments) {

		for each (var shipment : Shipment in basket.getShipments()) {
			if (!shipment.isDefault() && !(shipment.giftCertificateLineItems.length > 0)) {
				basket.removeShipment(shipment);
			}
			else if (!shipment.isDefault() && (shipment.giftCertificateLineItems.length > 0) ) {
				if (empty(keepGiftCertShipments) || (!empty(keepGiftCertShipments) && keepGiftCertShipments == false) ) {
					basket.removeShipment(shipment);
				}
			}
		}
	},
	
	cartChanged : function(basket: dw.order.LineItemCtnr) : Boolean {
		
		var basket : LineItemCtnr = basket;
		var cartStateString = "";
		var surcharge : Boolean = false;
		//var crypto: MessageDigest = new MessageDigest(MessageDigest.DIGEST_SHA_256);
		//var stateCookie: Cookie = request.getHttpCookies()['dw_CartState'];
		var sessionCartStateString = session.custom.cartStateString;
		
		var productLineItems : Iterator = basket.getAllProductLineItems().iterator();
		
		while(productLineItems.hasNext()) {
			
			var productLineItem : ProductLineItem = productLineItems.next();	
			if(null !== productLineItem.shippingLineItem && productLineItem.shippingLineItem.surcharge && basket.adjustedShippingTotalTax.value == 0) {
				surcharge = true;
			}

			cartStateString += productLineItem.productID +";"
								+ productLineItem.getUUID() + ";"
								+ productLineItem.quantityValue +";"
								+ productLineItem.adjustedPrice + "|";
		}	
		
		// Append shipping totals and basket totals to string (adjustedMerchandizeTotalPrice includes order level price adjustments). Basket Net total checked as catch all for both taxation policies not including tax.
		cartStateString += basket.adjustedShippingTotalPrice.valueOrNull + "|" 
							+ basket.adjustedMerchandizeTotalPrice.valueOrNull + "|" 
							// Commenting out the totalNetPrice since after we get the tax values and update the taxes, the cart
							// recalculate changes the totalNetPrice from null to the actual value, and so it appears as if the cart
							// has changed.
//							+ basket.totalNetPrice.valueOrNull + "|" 
							+ basket.defaultShipment.shippingAddress.stateCode + "|" 
							+ basket.defaultShipment.shippingAddress.city.toLowerCase() + "|" 
							+ basket.defaultShipment.shippingAddress.countryCode + "|" 
							+ basket.defaultShipment.shippingAddress.postalCode + "|" 
							+ basket.defaultShipment.shippingAddress.address1 + "|";
		
		//cartStateString = crypto.digest(cartStateString);

		// Check if the cartStateString in session is the same as the newly calculated cartStateString. 
		// If the strings are the same, then the cart has not changed and tax calculation will be skipped
//		if(stateCookie != null && !empty(stateCookie.value) && stateCookie.value == cartStateString && !surcharge) {
		if(sessionCartStateString != null && !empty(sessionCartStateString) && sessionCartStateString == cartStateString && !surcharge) {
		this.logDebug("SleepysUtils.ds - cartChanged() - \n Session Cart State String: " + sessionCartStateString + "\n New Cart State String: " + cartStateString + "\n The cart has not changed."); 

			return false;
		}

		this.logDebug("SleepysUtils.ds - cartChanged() - \n Session Cart State String: " + sessionCartStateString + "\n New Cart State String: " + cartStateString + "\n The cart has changed."); 
		
		//save cart state string here and return true
//		stateCookie = new Cookie('dw_CartState', cartStateString);
//		stateCookie.setMaxAge(-1);
		
//		request.addHttpCookie(stateCookie);
		session.custom.cartStateString = cartStateString;
	
		return true;	
	},
	
	/*****************************************************************************
	 * Name: logInfo
	 * Description: Log info messages to custom log file
	 *****************************************************************************/	
	logInfo : function(message : String) {
		var logger: Log = Logger.getLogger('sleepys-info', 'sleepys');
		logger.info("\n - Info: {0} \n", message);
	},
	
	/*****************************************************************************
	 * Name: logWarn
	 * Description: Log warn messages to custom log file
	 *****************************************************************************/	
	logWarn : function(message : String) {
		var logger: Log = Logger.getLogger('sleepys-warn', 'sleepys');
		logger.warn("\n - Warn: {0} \n", message);
	},
	
	/*****************************************************************************
	 * Name: logError
	 * Description: Log error messages to custom log file
	 *****************************************************************************/	
	logError : function(message : String) {
		var logger: Log = Logger.getLogger('sleepys-error', 'sleepys');
		logger.error("\n - Error: {0} \n", message);
	},
	
	/*****************************************************************************
	 * Name: logDebug
	 * Description: Log debug messages to custom log file
	 *****************************************************************************/		
	logDebug : function(message : String) {
		var logger: Log = Logger.getLogger('sleepys-debug', 'sleepys');
		logger.debug("\n - Debug: {0} \n", message);
	},
	
	/*****************************************************************************
	 * Name: logFatal
	 * Description: Log fatal messages to custom log file
	 *****************************************************************************/	
	logFatal : function(message : String) {
		var logger: Log = Logger.getLogger('sleepys-fatal', 'sleepys');
		logger.fatal("\n - Fatal: {0} \n", message);
	},
	
	/*****************************************************************************
	 * Name: logFatal
	 * Description: Log fatal messages to custom log file
	 *****************************************************************************/	
	
	getSFTokenURL : function() : String {
			return Site.getCurrent().getCustomPreferenceValue("sfCustomerPortalBaseURL") + Site.getCurrent().getCustomPreferenceValue("sfTokenURL");
	},

	getSFClientID : function() : String{
			return Site.getCurrent().getCustomPreferenceValue("sfCustomerConsumerKey");
	},
	
	getSFClientSecret : function() : String {
		return Site.getCurrent().getCustomPreferenceValue("sfCustomerSecretKey");
	}
};

exports.SleepysHelper = SleepysHelper;

