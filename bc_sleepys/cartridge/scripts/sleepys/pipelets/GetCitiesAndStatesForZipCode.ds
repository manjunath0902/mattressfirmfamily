/*
*- 	Name: 			GetCitiesAndStatesForZipCode.ds
*-	Description: 	Calls Sleepy's GetCitiesAndStatesForZipCode Service to return 
*-					the correct values for both the city and state drop down boxes
*-					based on the entered zip code
*
*	@input ZipCode : String Zip Code
*   ---------------------------------------
*	@output CityStateJSON : String;
*/

importPackage( dw.svc );
importPackage( dw.system );
importPackage( dw.util );


function execute( args : PipelineDictionary ) : Number {

    var service:Service;
    var result:Result;    

	var _cityStateJSONArr : Array = new Array();
	var zipCode = args.ZipCode;
	var _i = 0, _j = 0;
	
	if (empty(zipCode)) {
		return PIPELET_ERROR;
	}


	service = ServiceRegistry.get("sleepys.citiesandstatesforzipcodeservice.soap");
	result = service.call(zipCode);

	if(result.status == "OK")
	{
		if(!empty(result.object) && !empty(result.object.response)) {
			var _itemCount = result.object.response.webCities.length;

			if(_itemCount > 0) {
				var _cities : ArrayList = new ArrayList();
				
				// Filter out any duplicate cities. (for cities that exist in multiple counties)
				for each(var __obj in result.object.response.webCities) {
					if(!_cities.contains(__obj.city)) {
						_cities.add(__obj.city);
					}
				}
				_itemCount = _cities.length;
				
				_cityStateJSONArr[_j++] = "{ \"cities\" : [";
				_cities.clear();
				
				for each(var __obj in result.object.response.webCities) {
					if(!_cities.contains(__obj.city)) {
						_cities.add(__obj.city);
						
						_cityStateJSONArr[_j++] = "{";
						_cityStateJSONArr[_j++] = StringUtils.format("\"city\" : \"{0}\", \"state\" : \"{1}\", \"county\" : \"{2}\" ,\"country\" : \"{3}\"", __obj.city, __obj.state, __obj.county, __obj.country);
						_cityStateJSONArr[_j++] = "}";
						if(_itemCount != (_i+1))
							_cityStateJSONArr[_j++] = ",";
						_i++;
					}
				}
				_cityStateJSONArr[_j++] = "] }";
			}
		}
	}
	
	args.CityStateJSON = _cityStateJSONArr.join("");
	
   	return PIPELET_NEXT;
}