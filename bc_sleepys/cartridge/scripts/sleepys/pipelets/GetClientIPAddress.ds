/**
* 	Name: 			GetClientIPAddress.ds
* 	Description: 	Retrieve the client ip address from either the remote address, or 
*					the true client ip being passed from Akamai.
*
* 	@author Carl Napoli (Media Hive)
*
*   @output IPAddress : String
*
*/
importPackage( dw.system );
importPackage( dw.util );

var sleepysHelper = require("../util/SleepysUtils").SleepysHelper; 

function execute( args : PipelineDictionary ) : Number
{

	var ipAddress : String = null;

	var httpHeaders : Map = request.getHttpHeaders();

	if (httpHeaders != null) {

		// Log out all of the headers
		var headers : String = "";
		
		for each (var entry : MapEntry in httpHeaders.entrySet()) {
			
		    var key = entry.getKey();
		    var value = entry.getValue();

			headers += "Key: " + key + ", Value: " + value + "\n";
		    
		}
		
		sleepysHelper.logInfo("HTTP Headers: \n" + headers);

		// Determine if the client ip address is being passed through Akamai and return the correct one.
		if (httpHeaders.get("true-client-ip") == null) {
			ipAddress = request.getHttpRemoteAddress();
			sleepysHelper.logInfo("True Client IP was null.  Passing Remote Address instead: " + ipAddress);
		}
		else {
			ipAddress = httpHeaders.get("true-client-ip"); 	
			sleepysHelper.logInfo("True Client IP is available from Akamai: " + ipAddress);
		}

	}
	
	args.IPAddress = ipAddress;

	return PIPELET_NEXT;
}
