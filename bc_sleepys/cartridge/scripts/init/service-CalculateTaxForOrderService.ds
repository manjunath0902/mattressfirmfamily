/**
 * Initialize SOAP services for a cartridge
 */    
importPackage( dw.svc );
importPackage( dw.net );
importPackage( dw.io );  

ServiceRegistry.configure("sleepys.calculatetaxfororderservice.soap", {

	 initServiceClient : function(svc:SOAPService) {
	    svc.webOrderService = webreferences.WebOrderService;
	    return svc.webOrderService.getDefaultService();
	 },
	  
	  createRequest : function(svc: SOAPService, paramObject) : Object {

	  	var basket = paramObject.basket;
	  	var masterStoreCode = paramObject.masterStoreCode;
	  			
		if(empty(basket)){
			throw new Error("Basket is not available.");
		}

		var defaultShipment = basket.defaultShipment;

		if(empty(defaultShipment.shippingAddress)){
			throw new Error("Basket does not have a default shipping address.");
		}

		if(empty(masterStoreCode)){
			throw new Error("Invalid master store code.");
		}
		
		var webOrder = new svc.webOrderService.WebOrder();
		
		var deliveryAddress = new svc.webOrderService.StreetAddress();

		deliveryAddress.setAddressLine1(defaultShipment.shippingAddress.address1);
		deliveryAddress.setAddressLine2(defaultShipment.shippingAddress.address2);
		deliveryAddress.setCity(defaultShipment.shippingAddress.city);
		deliveryAddress.setStateCode(defaultShipment.shippingAddress.stateCode);
		deliveryAddress.setZipCode(defaultShipment.shippingAddress.postalCode);
		
		webOrder.setDeliveryAddress(deliveryAddress);
		
		webOrder.setWebStoreId(masterStoreCode);
				
		// Set the order line items
		var webOrderLines : Array = new Array();


		var shipments : Iterator = basket.getShipments().iterator();
		while(shipments.hasNext())
		{
			var shipment : Shipment = shipments.next();
	
			var shipmentLineItems : Iterator = shipment.getAllLineItems().iterator();
			while(shipmentLineItems.hasNext())
			{
				var lineItem : LineItem = shipmentLineItems.next();
				
				var webOrderLine = new svc.webOrderService.WebOrderLine();

				if(lineItem instanceof dw.order.ProductLineItem) {

					webOrderLine.setWebLineId(lineItem.getUUID());
					webOrderLine.setItemCode(lineItem.manufacturerSKU);
					webOrderLine.setQuantity(lineItem.quantity.value);
					//webOrderLine.setUnitPrice(lineItem.adjustedPrice.value);
					webOrderLine.setUnitPrice(lineItem.proratedPrice.value / lineItem.quantity.value);
				}
				else if ( lineItem instanceof dw.order.ShippingLineItem) {

					webOrderLine.setWebLineId(lineItem.getUUID());
					webOrderLine.setItemCode("DELIVERY CHARGE");
					webOrderLine.setQuantity(1);
					webOrderLine.setUnitPrice(lineItem.adjustedPrice.value);
				}

				if (webOrderLine.getUnitPrice() != 0) {
					webOrderLines.push(webOrderLine);
				}
				
			}
		}
		
		webOrder.setLineItems(webOrderLines);
				
		var requestObject = new svc.webOrderService.CalculateTaxForOrder(webOrder);
		
		return requestObject;			
	  },
		   
	  execute: function(svc:SOAPService, requestObject) {
		return svc.serviceClient.calculateTaxForOrder(requestObject);
	  },
	  
	  parseResponse : function(svc: SOAPService, responseObject) : Object {
        var responseWrapper = {}; 
        responseWrapper.status = {};
 
 		if (!empty(responseObject) && !empty(responseObject._return)) {
			var statusCode = responseObject._return.statusCode;
			var status = responseObject._return.status;
		
			if (statusCode == 0 || statusCode == 200) {
				responseWrapper.status.code = "OK";
			}
			else {
				responseWrapper.status.code = "ERROR";
				return responseWrapper;
			}

			responseWrapper.status.error = statusCode;
			responseWrapper.status.msg = status;

		}
		else {
			responseWrapper.status.code = "ERROR";
			return responseWrapper;
		}
 
        
		var webOrder = responseObject._return;
 		
 		var deliveryAddress = {
 			addressLine1: webOrder.deliveryAddress.addressLine1,
 			addressLine2: webOrder.deliveryAddress.addressLine2,
 			apartmentNumber: webOrder.deliveryAddress.apartmentNumber,
 			city: webOrder.deliveryAddress.city,
 			stateCode: webOrder.deliveryAddress.stateCode,
 			zipCode: webOrder.deliveryAddress.zipCode
 		}
 		
 		var lineItems = [];
 		
 		if (!empty(webOrder.lineItems)) {
 			for each( var webOrderLineItem in webOrder.lineItems) {
 				
 				var lineItem = {
					atpDate: webOrderLineItem.atpDate,
					itemCode: webOrderLineItem.itemCode,
					quantity: webOrderLineItem.quantity,
					shipMethod: webOrderLineItem.shipMethod,
					statusCode: webOrderLineItem.statusCode,
					taxAmount: webOrderLineItem.taxAmount,
					taxRate: webOrderLineItem.taxRate,
					unitPrice: webOrderLineItem.unitPrice,
					warehouse: webOrderLineItem.warehouse,
					webLineId: webOrderLineItem.webLineId
 				}

 				lineItems.push(lineItem);
 			}
 		}
 		
		responseWrapper.response = {
			deliveryAddress: deliveryAddress,
			deliveryAddressLatitude: webOrder.deliveryAddressLatitude,
			deliveryAddressLongitude: webOrder.deliveryAddressLongitude,
			lineItems: lineItems,
			maxAtp: webOrder.maxAtp,
			siestaOrderID: webOrder.siestaOrderID,
			status: webOrder.status,
			webOrderID: webOrder.webOrderID,
			webStoreID: webOrder.webStoreId
		};
 
        return responseWrapper;

      },
      
	mockCall : function(svc : SOAPService, requestObject : Object) : Object {
		var lineItemsArr = [];
		lineItemsArr[0] = { atpDate : null, itemCode: "KK99604/6MAT", quantity : 1, shipMethod: "SLEEPYS_TRUCK", statusCode : 0, taxAmount: 15.52, taxRate: 0.08625, unitPrice : 179.99, warehouse: null, webLineId: null};
		lineItemsArr[1] = { atpDate : null, itemCode: "DELIVERY_CHARGE", quantity : 1, shipMethod: "SLEEPYS_TRUCK", statusCode : 0, taxAmount: 6.9, taxRate: 0.08625, unitPrice : 79.99, warehouse: null, webLineId: null}
		
		return { _return : { deliveryAddress : {addressLine1 : "99 Nancy Pl", addressLine2 : null, apartmentNumber :null, city: "MASSAPEQUA", stateCode : "NY", zipCode : "11758"}, 
							  deliveryAddressLatitude:0, 
							  deliveryAddressLongitude:0,
							  deliveryStatus : null,
							  lineItems : lineItemsArr,
							  maxATP : null,
							  shipments : null,
							  siestaOrderID : null,
							  status : "Order Update Successfull",
							  statusCode : 200,
							  webOrderID : null,
							  webStoreID : 500
							}
				};
							 
	}
});
