'use strict';

/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');


function emailForm_v3() {
    app.getView().render('exacttargetform_v3');
}

function emailForm_MM() {
    app.getView().render('exacttargetform_MM');
}

function mm_emailForm() {
    app.getView().render('exacttargetform_mattressmatcher');
}

/*
 * Local methods
 */
exports.EmailForm_v3 = guard.ensure(['get'], emailForm_v3);
exports.EmailForm_MM = guard.ensure(['get'], emailForm_MM);
exports.MMEmailForm = guard.ensure(['get'], mm_emailForm);
