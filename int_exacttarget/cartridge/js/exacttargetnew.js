'use strict';

var ExactTarget = {
    init: function () {
        var _exacttargetFormContainer = $('div.exacttarget-form-container');
        var _loader = _exacttargetFormContainer.find('img.ajax-loader');
        var _exactTargetSubmitButton = $('button.exacttarget-email-button');
        
        $(document).on('focus', 'input.exacttarget-email', function() {
            $(this).removeClass('error');
            $(this).attr('placeholder', "Email Address");
        });
        $(document).on('click','.exacttarget-email-button', function(e) {
            e.preventDefault();
            var _exacttargetEmailInput = $(this).form().find('.exacttarget-email');
            var valid = exactTargetValidation(_exacttargetEmailInput);
            if(valid) {
            	 var email = _exacttargetEmailInput.val();
            	 var _gclid = getCookie("_ga");
            	 var _leadSource = $('#footer-social-leadSource').val();
                 var _optOutFlag = ($('#footer-social-optOutFlag').is(':checked')) ? true : false;
                 
                 var _exactTargetForm = _exacttargetFormContainer.find('form.exacttargetform');
                 _exactTargetForm.hide();
                 $.ajax({
                     url: Urls.sendMatressFinderResult_v3, 
                     data: {emailID: email, googleClientID : _gclid, optOut: _optOutFlag, lead : _leadSource},
                     type: 'get',
                     success: function (data) {
                       _exacttargetFormContainer.html(data);
                   },
                   error: function (errorThrown) {
                     console.log(errorThrown);
                   }
                 });
            }
        });
    }
};

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function validateEmail(email) {
    var emailReg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return emailReg.test(email);
}

function exactTargetValidation(emailField) {
    var emailaddress = emailField.val();
    if(!validateEmail(emailaddress) || emailaddress == null || emailaddress == '') {
        emailField.addClass('error');
        emailField.val('');
        emailField.attr('placeholder', "Enter Valid Email");
        
        return false;
    }
    return true;
}

module.exports = ExactTarget;