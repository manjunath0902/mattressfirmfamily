'use strict';

var app                   = require('~/cartridge/scripts/app'),
    guard                 = require('~/cartridge/scripts/guard'),
    Transaction           = require('dw/system/Transaction'),
    ForterOrder           = require("int_forter/cartridge/scripts/lib/forter/dto/ForterOrder.ds"),
    ForterMakePaymentOrder= require("int_forter/cartridge/scripts/lib/forter/dto/ForterMakePaymentOrder.ds"),
    ForterValidateService = require("int_forter/cartridge/scripts/lib/forter/services/ForterValidateService.ds"),
    ForterErrorsService   = require("int_forter/cartridge/scripts/lib/forter/services/ForterErrorsService.ds"),
    ForterLogger          = require("int_forter/cartridge/scripts/lib/forter/ForterLogger.ds"),
    ForterResponse        = require("int_forter/cartridge/scripts/lib/forter/ForterResponse.ds"),
    ForterError           = require("int_forter/cartridge/scripts/lib/forter/dto/ForterError.ds"),
    ForterConfig          = require('int_forter/cartridge/scripts/lib/forter/ForterConfig.ds').ForterConfig;

function validateOrder(args) {
    var log                   = new ForterLogger("ForterValidate.validateOrder.js"),
        fResponse             = new ForterResponse(),
        forterValidateService = new ForterValidateService(),
        forterErrorsService   = new ForterErrorsService(),
        order                 = args.Order,
        orderRequest          = null,
        result                = true,
        orderValidateAttempt  = args.orderValidateAttemptInput ? args.orderValidateAttemptInput : 1,
        callArgs              = {
            siteId    : ForterConfig.forterSiteID,
            secretKey : ForterConfig.forterSecretKey,
            orderId   : order.originalOrderNo
        };

    var resp = {};

    if (ForterConfig.forterEnabled === false) {
        fResponse.processorAction = 'disabled';
        resp.JsonResponseOutput = fResponse;
        resp.result = true;
        return resp;
    }

    try {
        orderRequest      = new ForterOrder(args);
        var requestString = JSON.stringify(orderRequest, undefined, 2);

        log.debug("Forter Order Validate Request ::: \n" + requestString);
        log.debug("Forter Order Validate Attempt ::: " + orderValidateAttempt);

        var forterResponse = forterValidateService.validate(callArgs, orderRequest);

        if (forterResponse.ok === true) {
            log.debug("Forter Order Validate Response ::: \n" + forterResponse.object.text);

            result    = true;
            fResponse = JSON.parse(forterResponse.object.text);

            fResponse.actionEnum =
                (fResponse.action === "approve") ? "APPROVED" :
                    (fResponse.action === "decline") ? "DECLINED" :
                        (fResponse.action === "not reviewed") ? "NOT_REVIEWED" : "FAILED";

            Transaction.wrap(function () {
                order.custom.forterOrderStatus = order.status.value;
            });

            var getLink = new RegExp("(?=http)(.*?)(\\s|$)"),
                match   = getLink.exec(fResponse.message);

            if (match != null) {
                fResponse.orderLink = match[1];
            } else {
                fResponse.orderLink = "";
            }
            
            //if successfully sent first time : have an order with the Retry number: field set as 0 (Order -> Attributes tab)
            Transaction.wrap(function () {
                order.custom.forterRetryNumber = '0';
            });
            
        } else {
            log.error(forterResponse.msg);

            fResponse.actionEnum      = 'ERROR';
            fResponse.orderLink       = '';
            fResponse.processorAction = 'internalError';

            var forterError         = new ForterError(order.getOrderNo(), forterResponse.msg, forterResponse.errorMessage),
                forterErrorResponse = forterErrorsService.call(callArgs, forterError);
            
            //if 1st retry : have an order with the Retry number: field set as 1 (Order -> Attributes tab)
            //if 1st retry failed : have an order with the Retry number: field set as 2 (Order -> Attributes tab)
            Transaction.wrap(function () {
                order.custom.forterRetryNumber = orderValidateAttempt.toString();
            });

            if (orderValidateAttempt == 1) {
                log.error("Forter Order " + order.originalOrderNo + "." + " Validate Retry: " + orderValidateAttempt + "." + " Response status: " + forterResponse.status + "." + " Forter response error message: " + forterResponse.errorMessage);

                resp.orderValidateAttemptInput = 2;
                resp.result = false;
                return resp;
            }

            if (orderValidateAttempt > 1) {
                log.error("Forter Order " + order.originalOrderNo + "." + " Validate Retry: " + orderValidateAttempt + "." + " Response status: " + forterResponse.status + "." + " Forter response error message: " + forterResponse.errorMessage);
            }
        }
    } catch (e) {
        log.error(e);

        fResponse.actionEnum      = 'ERROR';
        fResponse.orderLink       = '';
        fResponse.processorAction = 'internalError';

        var forterError         = new ForterError(order.getOrderNo(), e),
            forterErrorResponse = forterErrorsService.call(callArgs, forterError);
        
        //if 1st retry : have an order with the Retry number: field set as 1 (Order -> Attributes tab)
        //if 1st retry failed : have an order with the Retry number: field set as 2 (Order -> Attributes tab)
        Transaction.wrap(function () {
            order.custom.forterRetryNumber = orderValidateAttempt.toString();
        });
        
        if (orderValidateAttempt == 1) {
            log.error("Forter Order " + order.originalOrderNo + "." + " Validate Retry: " + orderValidateAttempt + "." + " Catched error: " + e);

            resp.orderValidateAttemptInput = 2;
            resp.result = false;
            return resp;
        }

        if (orderValidateAttempt > 1) {
            log.error("Forter Order " + order.originalOrderNo + "." + " Validate Retry: " + orderValidateAttempt + "." + " Catched error: " + e);
        }
    }

    if (fResponse.actionEnum === 'DECLINED') {
        if (ForterConfig.forterCancelOrderOnDecline == true) {
            if (ForterConfig.forterCancelOrderAfter24) {
                fResponse.processorAction = 'skipCapture';
            } else {
                fResponse.processorAction = 'void';
                if (ForterConfig.forterShowDeclinedPage == true && !empty(ForterConfig.forterCustomDeclineMessage)) {
                    resp.PlaceOrderError = {
                        'code': ForterConfig.forterCustomDeclineMessage
                    };
                }
            }
        } else {
            fResponse.processorAction = 'skipCapture';
        }
    } else if (fResponse.actionEnum === 'NOT_REVIEWED') {
    	fResponse.processorAction = 'notReviewed';
    } else if (fResponse.actionEnum === 'APPROVED') {
        fResponse.processorAction = ForterConfig.forterAutoInvoiceOnApprove ? 'capture' : 'skipCapture';
    }

    Transaction.wrap(function () {
        args.Order.custom.forterDecision    = fResponse.actionEnum;
        args.Order.custom.forterOrderLink   = fResponse.orderLink;
        args.Order.custom.forterUserAgent   = !empty(orderRequest) ? orderRequest.connectionInformation.userAgent : null;
        args.Order.custom.forterTokenCookie = !empty(orderRequest) ? orderRequest.connectionInformation.forterTokenCookie : null;
    });

    resp.JsonResponseOutput = fResponse;

    if (result === true) {
        resp.result = true;
        return resp;
    }

    resp.result = false;
    return resp;
}

function validateMakePaymentOrder(args) {
    var log                   = new ForterLogger("ForterValidate.validateOrder.js"),
    	LogUtils			  = require("app_mattressfirm_storefront/cartridge/scripts/util/LogUtilFunctions").LogUtilFunctions;
        fResponse             = new ForterResponse(),
        forterValidateService = new ForterValidateService(),
        forterErrorsService   = new ForterErrorsService(),
        order                 = args.Order,
        orderRequest          = null,
        result                = true,
        orderValidateAttempt  = args.orderValidateAttemptInput ? args.orderValidateAttemptInput : 1,
        callArgs              = {
            siteId    : ForterConfig.forterSiteID,
            secretKey : ForterConfig.forterSecretKey,
            orderId   : order.SalesId
        };

    const FORTER = {"ID":"forter" , "description": "FORTER"};	
    const FORTER_DECLINE = {"ID":"forterDecline" , "description": "FORTER Decline"};
    const FORTER_NOTREVIEWED = {"ID":"forterNotReviewed" , "description": "FORTER Not Reviewed"};

    var resp = {};

    if (ForterConfig.forterEnabled === false) {
        fResponse.processorAction = 'disabled';
        resp.JsonResponseOutput = fResponse;
        resp.result = true;
        return resp;
    }

    try {
        orderRequest      = new ForterMakePaymentOrder(args);
        var requestString = JSON.stringify(orderRequest, undefined, 2);

        log.debug("Forter Order Validate Request ::: \n" + requestString);
        log.debug("Forter Order Validate Attempt ::: " + orderValidateAttempt);

        var forterResponse = forterValidateService.validate(callArgs, orderRequest);

        if (forterResponse.ok === true) {
            log.debug("Forter Order Validate Response ::: \n" + forterResponse.object.text);

            result    = true;
            fResponse = JSON.parse(forterResponse.object.text);

            fResponse.actionEnum =
                (fResponse.action === "approve") ? "APPROVED" :
                    (fResponse.action === "decline") ? "DECLINED" :
                        (fResponse.action === "not reviewed") ? "NOT_REVIEWED" : "FAILED";


            var getLink = new RegExp("(?=http)(.*?)(\\s|$)"),
                match   = getLink.exec(fResponse.message);

            if (match != null) {
                fResponse.orderLink = match[1];
            } else {
                fResponse.orderLink = "";
            }
            
        } else {
        	LogUtils.logAndEmailError(order.SalesId, FORTER, forterResponse.errorMessage);
            log.error(forterResponse.msg);

            fResponse.actionEnum      = 'ERROR';
            fResponse.orderLink       = '';
            fResponse.processorAction = 'internalError';

            var forterError         = new ForterError(order.SalesId, forterResponse.msg, forterResponse.errorMessage),
                forterErrorResponse = forterErrorsService.call(callArgs, forterError);
            
            //if 1st retry : have an order with the Retry number: field set as 1 (Order -> Attributes tab)

            if (orderValidateAttempt == 1) {
                log.error("Forter Order " + order.SalesId + "." + " Validate Retry: " + orderValidateAttempt + "." + " Response status: " + forterResponse.status + "." + " Forter response error message: " + forterResponse.errorMessage);

                resp.orderValidateAttemptInput = 2;
                resp.result = false;
                return resp;
            }

            if (orderValidateAttempt > 1) {
                log.error("Forter Order " + order.SalesId + "." + " Validate Retry: " + orderValidateAttempt + "." + " Response status: " + forterResponse.status + "." + " Forter response error message: " + forterResponse.errorMessage);
            }
        }
    } catch (e) {
    	LogUtils.logAndEmailError(order.SalesId, FORTER, e.message);
        log.error(e);
        var error = e;
        fResponse.actionEnum      = 'ERROR';
        fResponse.orderLink       = '';
        fResponse.processorAction = 'internalError';

        var forterError         = new ForterError(order.SalesId, e),
        forterErrorResponse = forterErrorsService.call(callArgs, forterError);
        
        //if 1st retry : have an order with the Retry number: field set as 1 (Order -> Attributes tab)
        
        if (orderValidateAttempt == 1) {
            log.error("Forter Order " + order.SalesId + "." + " Validate Retry: " + orderValidateAttempt + "." + " Catched error: " + e);

            resp.orderValidateAttemptInput = 2;
            resp.result = false;
            return resp;
        }

        if (orderValidateAttempt > 1) {
            log.error("Forter Order " + order.SalesId + "." + " Validate Retry: " + orderValidateAttempt + "." + " Catched error: " + e);
        }
    }

    if (fResponse.actionEnum === 'DECLINED') {
    	LogUtils.logAndEmailError(order.SalesId, FORTER_DECLINE, forterResponse.object.text);
       	fResponse.transactionDeclined = true;
    	fResponse.declinedReasonCode = 'F-1001';
    	if (ForterConfig.forterCancelOrderOnDecline == true) {
            if (ForterConfig.forterCancelOrderAfter24) {
                fResponse.processorAction = 'skipCapture';
            } else {
                fResponse.processorAction = 'void';
                if (ForterConfig.forterShowDeclinedPage == true && !empty(ForterConfig.forterCustomDeclineMessage)) {
                    resp.PlaceOrderError = {
                        'code': ForterConfig.forterCustomDeclineMessage
                    };
                }
            }
        } else {
            fResponse.processorAction = 'skipCapture';
        }
    } else if (fResponse.actionEnum === 'NOT_REVIEWED') {
    	fResponse.processorAction = 'notReviewed';
    	LogUtils.logAndEmailError(order.SalesId, FORTER_NOTREVIEWED, forterResponse.object.text);
    } else if (fResponse.actionEnum === 'APPROVED') {
        fResponse.processorAction = ForterConfig.forterAutoInvoiceOnApprove ? 'capture' : 'skipCapture';
    }

    resp.JsonResponseOutput = fResponse;

    if (result === true) {
        resp.result = true;
        return resp;
    }

    resp.result = false;
    return resp;
}

function storeResponse(args) {
    var log                   = new ForterLogger("ForterValidate.storeResponse.js"),
        pi                    = args.PaymentInstrument,
        responseDataContainer = args.ResponseDataContainer,
        responseType          = args.ResponseType;

    try {
        var stringifiedResponse = stringifyResponse(responseDataContainer.data);

        if (responseType == "paypal_transaction_details_response") {
            Transaction.wrap(function () {
                pi.custom.paypal_transaction_details_response = stringifiedResponse;
            });

            log.debug("Forter Store Response Details ::: \n" + stringifiedResponse);

        } else if ( responseType == "paypal_capture_response") {
            Transaction.wrap(function () {
                pi.custom.paypal_capture_response = stringifiedResponse;
            });

            log.debug("Forter Store Response Capture ::: \n" + stringifiedResponse);

        } else if ( responseType == "paypal_void_response") {
            Transaction.wrap(function () {
                pi.custom.paypal_void_response = stringifiedResponse;
            });

            log.debug("Forter Store Response Void ::: \n" + stringifiedResponse);

        } else if ( responseType == "paypal_authorization_response") {
            Transaction.wrap(function () {
                pi.custom.paypal_authorization_response = stringifiedResponse;
            });

            log.debug("Forter Store Response Authorization ::: \n" + stringifiedResponse);

        } else if ( responseType == "paypal_expresscheckout_response") {
            Transaction.wrap(function () {
                pi.custom.paypal_expresscheckout_response = stringifiedResponse;
            });

            log.debug("Forter Store Response Express Checkout ::: \n" + stringifiedResponse);

        }

    } catch (e) {
        log.error(e);
        return false;
    }

    return true;
}

function stringifyResponse(obj : HashMap) {
    var parsedObj = {};

    for each (var key in obj.keySet().toArray()) {
        parsedObj[key] = obj.get(key);
    }

    return JSON.stringify(parsedObj);
}

exports.ValidateOrder = guard.ensure(['https'], validateOrder);
exports.ValidateMakePaymentOrder = guard.ensure(['https'], validateMakePaymentOrder);
exports.StoreResponse = guard.ensure(['https'], storeResponse);
