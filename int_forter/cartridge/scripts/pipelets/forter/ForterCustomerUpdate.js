var ForterLogger           = require("int_forter/cartridge/scripts/lib/forter/ForterLogger.ds"),
    ForterResponse         = require("int_forter/cartridge/scripts/lib/forter/ForterResponse.ds"),
    ForterCustomerAccount  = require("int_forter/cartridge/scripts/lib/forter/dto/ForterCustomerAccount.ds"),
    ForterCustomersService = require("int_forter/cartridge/scripts/lib/forter/services/ForterCustomersService.ds"),
    ForterConfig           = require('int_forter/cartridge/scripts/lib/forter/ForterConfig.ds').ForterConfig;

function execute(args) {
    var log                    = new ForterLogger("ForterCustomerUpdate.js"),
        forterCustomersService = new ForterCustomersService(),
        eventType              = args.EventType;

    try {
        var callArgs = {
                siteId           : ForterConfig.forterSiteID,
                secretKey        : ForterConfig.forterSecretKey,
                customerId       : customer.ID
            };
        var forterCustomerAccount = new ForterCustomerAccount(eventType);
        log.debug("Forter Customer Account Update Request ::: \n"
                + JSON.stringify(forterCustomerAccount, undefined, 2));
        var forterResponse = forterCustomersService.send(callArgs, forterCustomerAccount);

        if (forterResponse.ok == true) {
            log.debug("Forter Customer Account Update Response ::: \n"
            + forterResponse.object.text);
        } else {
            log.error(forterResponse.msg);
        }

    } catch (e) {
        log.error(e);

        return false;
    }

    return true;
}

module.exports = {
    execute: execute
};