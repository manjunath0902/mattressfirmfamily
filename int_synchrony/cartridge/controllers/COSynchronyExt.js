/**
* Synchrony controller to get token and status of transactions
*
* @module  controllers/Synchrony
*/

'use strict';

var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
//var ServiceRegistry = require('dw/svc/ServiceRegistry');
var Logger = require('dw/system/Logger');
var setPayLogger = Logger.getLogger('setPayTulo','setPay');
var URLUtils = require('dw/web/URLUtils');
var Money = require('dw/value/Money');
var Transaction = require('dw/system/Transaction');
//var service = ServiceRegistry.get("synchrony.http.PostRequest");
var servicePath = require("/bc_sleepysc/cartridge/scripts/init/service-Synchrony.ds");
var serviceObj = servicePath.SynchronyService;
var merchantID = dw.system.Site.getCurrent().getCustomPreferenceValue('synchronyMerchantId');
var password = dw.system.Site.getCurrent().getCustomPreferenceValue('synchronyMerchPassword');
var synchronyGetTokenURL = dw.system.Site.getCurrent().getCustomPreferenceValue('synchronyGetTokenURL');
var synchronyGetStatusURL = dw.system.Site.getCurrent().getCustomPreferenceValue('synchronyGetStatusURL');
var synchronypcgc = dw.system.Site.getCurrent().getCustomPreferenceValue('synchronypcgc');
var COBilling = require('int_tulo/cartridge/controllers/COBilling');
var OrderMgr = require('dw/order/OrderMgr');

// Creating a new payment instrument for SYNCHRONY_FINANCIAL
function createPaymentInstrument(statusResult) {
    var Cart = app.getModel('Cart');
    var basket :  Basket = Cart.get().object;
    var paymentType = 'SYNCHRONY_FINANCIAL';

    var clientTransactionID : String = 'SynchronyOrderNo' in session.custom && !empty(session.custom.SynchronyOrderNo) ? session.custom.SynchronyOrderNo : "";

    var amountToRedeem : Money = new Money(basket.totalGrossPrice.add(basket.giftCertificateTotalPrice).value, basket.getCurrencyCode());
    Transaction.begin();
    try{
        var paymentInstr : PaymentInstrument = null;
        // try to grab the existing one if its already defined, as there should only be one
        for each (var pi:PaymentInstrument in basket.getPaymentInstruments(paymentType)) {
            if(empty(paymentInstr)) {
                paymentInstr = pi;
            } else {
                basket.removePaymentInstrument(pi);
            }
        }
        if(empty(paymentInstr)) {
            paymentInstr = basket.createPaymentInstrument(paymentType, amountToRedeem);
        } else {
            paymentInstr.paymentTransaction.setAmount(amountToRedeem);
        }

        paymentInstr.paymentTransaction.setTransactionID(statusResult.transactionId);
        paymentInstr.paymentTransaction.custom.authCode = statusResult.AuthCode;
        paymentInstr.paymentTransaction.custom.synchronyClientTransactionID = statusResult.transactionId;//session.custom.SynchronyOrderNo;        
        paymentInstr.paymentTransaction.custom.synchronyClientToken = statusResult.TokenId;
        paymentInstr.paymentTransaction.custom.synchronyFullName = statusResult.FirstName + " " + statusResult.LastName;
        paymentInstr.paymentTransaction.custom.mfiFinAccountNumber = statusResult.AccountNumber;
        paymentInstr.paymentTransaction.custom.synchronyTransactionDate = statusResult.TransactionDate;
        paymentInstr.paymentTransaction.custom.synchronyTransactionDesc = statusResult.TransactionDescription;
        paymentInstr.paymentTransaction.custom.authAmount = amountToRedeem.value;
        paymentInstr.paymentTransaction.custom.approvalStatus = statusResult.StatusCode;
        paymentInstr.paymentTransaction.custom.synchronyCapitalPromoCode = statusResult.PromoCode;
        paymentInstr.paymentTransaction.custom.mfiPlanId = statusResult.PromoCode;
        paymentInstr.paymentTransaction.custom.mfiSynchronyToken = statusResult.TokenId;

        } catch(e) {
        	var transactionId = "";
        	if(!empty(statusResult.transactionId)){
        		transactionId = statusResult.transactionId;
        	}
        	setPayLogger.error("Error in int_synchrony:createPaymentInstrument: Error: " + e.name + " | Message: " + e.message + " | " + e.toString() + " | Transaction Id : "+transactionId);
            return false;
        }
        basket.updateTotals();
        Transaction.commit();
        setPayLogger.info('PaymentInstrument created successfully with transaction id : ' + statusResult.transactionId);
    return true;
}

function getStatusOfClosedModal() {
    var result = {};
    
    // default count for status api is 3 
    var synchronyServiceCount = 3;
  
	if('synchronyServiceCount' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.synchronyServiceCount != null) {
		synchronyServiceCount = dw.system.Site.current.preferences.custom.synchronyServiceCount;
	}

    // calls the status API for multiple times in order to make sure the availability of status API response
    for (var i = 0; i < synchronyServiceCount; i++) {
        try {
            //var service = ServiceRegistry.get("synchrony.http.PostRequest");
        	var serviceObj = servicePath.SynchronyService;
        	var token = session.custom.token;
            if (!empty(token)) {
                var paramsBody = {
                        "merchantNumber": merchantID,
                        "password" : password,
                        "userToken": token
                };
                serviceObj.setURL(synchronyGetStatusURL);
                var getStatusResult = serviceObj.call({params: JSON.stringify(paramsBody), place:"getStatus"});
                result = JSON.parse(getStatusResult.object.text);
                
                if(!empty(result)) {
                	setPayLogger.info('Modal status API got result with transaction Id : ' + result.transactionId + ' and count is :' + i);
                    break;
                }else{
                	setPayLogger.info('Modal status API count with empty result is :' + i );
                }
            }else {
            	setPayLogger.info('Token is empty while calling modal status API :' + token);
            }
        }
        catch(e) {
            var msg = e.message;
            setPayLogger.info('Modal status API got exception :' + msg);
        }
    }
    return result;
}

function saveOrder() {
    var cart = app.getModel('Cart').get();
    // validate payment field
    // Performs payment method specific checks, such as credit card verification.
    if (!COBilling.ResetPaymentForms() || COBilling.HandlePaymentSelection(cart).error) { //call controller
    	setPayLogger.info('Synchrony order not saved successfully');
    	COBilling.ReturnToForm(cart);
    } else {     
        if(customer.authenticated) {
            var defaultShipment = cart.getDefaultShipment();
            if (defaultShipment && defaultShipment != null && defaultShipment.shippingMethodID != 'storePickup') {
                var shippingAddress = defaultShipment.getShippingAddress();
                if(shippingAddress && shippingAddress != null ) {
                    app.getModel('Profile').get(customer.profile).addAddressToAddressBook(shippingAddress);
                }
            }
        }

        // Mark step as fulfilled
        app.getForm('billing').object.fulfilled.value = true;
        setPayLogger.info('Synchrony order saved successfully : ' + session.custom.SynchronyOrderNo);
        // A successful billing page will jump to the next checkout step.
        var COSummaryController = require('int_tulo/cartridge/controllers/COSummary');
        COSummaryController.Submit();       
        return;
    }
}

// create and fails the setPay order for tracking purposes
function failSynchronyOrder() {
	var order;
	var cart = app.getModel('Cart').get();
    Transaction.wrap(function () {
        order = OrderMgr.createOrder(cart.object, session.custom.SynchronyOrderNo);
        OrderMgr.failOrder(order);
        cart.calculate();
    });
    setPayLogger.info('Synchrony Order failed due to status API error | failed order number : ' + session.custom.SynchronyOrderNo);
    session.custom.SynchronyOrderNo = null;
    
}

function placeOrder() {
    
    var statusResult = getStatusOfClosedModal();    
    setPayLogger.info('Synchrony payment response: ' + JSON.stringify(statusResult));
    
    if(!empty(statusResult) && !empty(statusResult.StatusCode)) {
        if (statusResult.StatusCode === '000') {
            try {
                createPaymentInstrument(statusResult);
                saveOrder();
            } catch (e) {
            	var transaction = "";
            	if(!empty(statusResult.transactionId)){
            		transaction = statusResult.transactionId;
            	}
            	
            	var orderNumber = "";
            	if(!empty(session.custom.SynchronyOrderNo)){
            		orderNumber = session.custom.SynchronyOrderNo;
            	}
				
				var statusCode= "";
				if(!empty(statusResult.StatusCode)){
				  statusCode = statusResult.StatusCode;
				}
            	
                setPayLogger.info("Unable to place order with transaction Id : " + transaction + " | synchrony order : " + orderNumber + " | statusCode : " + statusCode);
                failSynchronyOrder();
                response.redirect(URLUtils.https('COBilling-Start', 'er', 1));
                return;
            }
            
        } else if (statusResult.StatusCode === '100') {
        	var transactionId = "";
        	if(!empty(statusResult.transactionId)){
        		transactionId = statusResult.transactionId;
        	}
        	setPayLogger.info("Placing synchrony order with status code : " + statusResult.StatusCode + " | Transaction ID : " + transactionId);
        	response.redirect(URLUtils.https('COBilling-Start'));
        	return;
    	}
    	else if (statusResult.StatusCode === '400' || statusResult.StatusCode === '401' || statusResult.StatusCode === '500') {
    		var transactionId = "";
        	if(!empty(statusResult.transactionId)){
        		transactionId = statusResult.transactionId;
        	}
    		setPayLogger.info("Failed While placing synchrony order with Status Code : " + statusResult.StatusCode + " | Transaction ID : " + transactionId);
    		failSynchronyOrder();
        	response.redirect(URLUtils.https('COBilling-Start', 'er', 1));
        	return;
    	}         
        else {
        	var statusCode= "";
			if(!empty(statusResult.StatusCode)){
			   statusCode = statusResult.StatusCode;
			}
			var transactionId = "";
        	if(!empty(statusResult.transactionId)){
        		transactionId = statusResult.transactionId;
        	}
        	
        	var orderNumber = "";
            if(!empty(session.custom.SynchronyOrderNo)){
            	orderNumber = session.custom.SynchronyOrderNo;
            }
        	setPayLogger.info("Failed While placing synchrony order with Status Code : " + statusCode + " | Transaction ID : " + transactionId + " | Order Number : " + orderNumber);        	
        	failSynchronyOrder();
            response.redirect(URLUtils.https('COBilling-Start', 'er', 1));
        	return;
        }
    } else {
    	var orderNumber = "";
        if(!empty(session.custom.SynchronyOrderNo)){
           	orderNumber = session.custom.SynchronyOrderNo;
        }
    	setPayLogger.info("Got empty modal status result while placing an order number : " + orderNumber);
    	failSynchronyOrder();
        response.redirect(URLUtils.https('COBilling-Start', 'er', 1));
        return;
    }
}


function preFilledForm () {
	var orderNo = OrderMgr.createOrderNo();
	session.custom.SynchronyOrderNo = orderNo;
	session.forms.synchronyext.TokenId.value = session.custom.token;
	session.forms.synchronyext.merchantID.value = merchantID;
	session.forms.synchronyext.mid.value = merchantID;
	session.forms.synchronyext.pcgc.value = synchronypcgc;
	session.forms.synchronyext.CTID.value = orderNo;  
	// copy the address attributes
	session.forms.synchronyext.FN.value = app.getForm('billing').object.billingAddress.addressFields.firstName.value;
	session.forms.synchronyext.LN.value = app.getForm('billing').object.billingAddress.addressFields.lastName.value;
	session.forms.synchronyext.zip.value = app.getForm('billing').object.billingAddress.addressFields.postal.value;
	session.forms.synchronyext.custAddress1Id.value = app.getForm('billing').object.billingAddress.addressFields.address1.value;
  	session.forms.synchronyext.cityId.value = app.getForm('billing').object.billingAddress.addressFields.city.value;
  	session.forms.synchronyext.stateId.value = app.getForm('billing').object.billingAddress.addressFields.states.state.value;
  	session.forms.synchronyext.phoneNumber.value = app.getForm('billing').object.billingAddress.addressFields.phone.value;
  	session.forms.synchronyext.emailAddress.value = app.getForm('billing').object.billingAddress.email.emailAddress.value;
  	return true;
}

function getClientToken() {
	try {
        var params : String;
        var token = "";
        params = "merchantId=" + merchantID + "&password=" + password;
        serviceObj.setURL(synchronyGetTokenURL);
        var tokenResult = serviceObj.call({params: params, place:"getToken"});
        var result = JSON.parse(tokenResult.object.text);
        //log result 
        setPayLogger.info('synchrony modal open successfully with token : ' + result.clientToken);
        if(!empty(result)){
            token = result.clientToken;
            session.custom.token = token;
            session.forms.synchronyext.TokenId.value = session.custom.token;
            session.forms.billing.paymentMethods.selectedPaymentMethodID.value = 'SYNCHRONY_FINANCIAL';
            var obj = new Object();
            obj.token = token;
            app.getView({
                obj: obj
            }).render('util/json');
        }else{
        	setPayLogger.info('synchrony token not recieved while calling the service for opening the modal  : ' + result);
        }       
    } catch(e) {
        var msg = e;
        setPayLogger.info('synchrony token not recieved while opening the modal due to exception : ' + msg);
    }
}


/*
* Web exposed methods
*/
exports.GetClientToken = guard.ensure(['https', 'get'], getClientToken);
exports.PlaceOrder = guard.ensure(['https', 'post'], placeOrder);
/*
 * Local methods
 */
exports.preFilledForm = preFilledForm;

