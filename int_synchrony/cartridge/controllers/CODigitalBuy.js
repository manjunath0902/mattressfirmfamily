/**
* Digital Buy controller for synchrony implementation at Mattress Firm
*
* @module  controllers/Synchrony
*/

'use strict';

var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var Logger = require('dw/system/Logger');
var setPayLogger = Logger.getLogger('setPayTulo','setPay');
var URLUtils = require('dw/web/URLUtils');
var Money = require('dw/value/Money');
var Transaction = require('dw/system/Transaction');
var servicePath = require("/bc_sleepysc/cartridge/scripts/init/service-Synchrony.ds");
var serviceObj = servicePath.SynchronyService;
var merchantID = dw.system.Site.getCurrent().getCustomPreferenceValue('synchronyMerchantId');
var password = dw.system.Site.getCurrent().getCustomPreferenceValue('synchronyMerchPassword');
var synchronyGetTokenURL = dw.system.Site.getCurrent().getCustomPreferenceValue('synchronyGetTokenURL');
var synchronyGetStatusURL = dw.system.Site.getCurrent().getCustomPreferenceValue('synchronyGetStatusURL');
var COBilling = require('app_mattressfirm_storefront/cartridge/controllers/COBilling');
var OrderMgr = require('dw/order/OrderMgr');
var digitalBuyHelper = require('*/cartridge/scripts/digitalbuy/digitalBuyHelper').digitalBuyHelper;
var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;
var UtilFunctions = require('app_mattressfirm_storefront/cartridge/scripts/util/UtilFunctions').UtilFunctions;
var StringHelpers = require('app_storefront_core/cartridge/scripts/util/StringHelpers');

/** 
 * @function
 * @description This function accepts addresses (shipping and billing)
 * it assigns all the value to their respective form by using session
 * true only if all products are available.
 * @param params (shipping and billing addresses)
 * @returns boolean
 */
function setAddresses(params) {
	//set shipping address
	session.forms.singleshipping.shippingAddress.email.emailAddress.value = params.email.value;
	session.forms.singleshipping.shippingAddress.addToSubscription.value = params.emailsubscription;
	session.forms.singleshipping.shippingAddress.addressFields.firstName.value = params.firstname.value;
	session.forms.singleshipping.shippingAddress.addressFields.lastName.value = params.lastname.value;
	session.forms.singleshipping.shippingAddress.addressFields.address1.value = params.address1.value;
	session.forms.singleshipping.shippingAddress.addressFields.address2.value = params.address2.value;
	session.forms.singleshipping.shippingAddress.addressFields.city.value = params.city.value;
	session.forms.singleshipping.shippingAddress.addressFields.states.state.value = params.state.value;
	session.forms.singleshipping.shippingAddress.addressFields.postal.value = params.zipcode.value;
	session.forms.singleshipping.shippingAddress.addressFields.phone.value = params.phone.value;
	session.forms.singleshipping.shippingAddress.addressFields.country.value = params.country.value;
	
	session.forms.billing.billingAddress.addressFields.firstName.value = params.firstnameBilling.value;
	session.forms.billing.billingAddress.addressFields.lastName.value = params.lastnameBilling.value;
	session.forms.billing.billingAddress.addressFields.address1.value = params.address1Billing.value;
	session.forms.billing.billingAddress.addressFields.address2.value = params.address2Billing.value;
	session.forms.billing.billingAddress.addressFields.city.value = params.cityBilling.value;
	session.forms.billing.billingAddress.addressFields.states.state.value = params.stateBilling.value;
	session.forms.billing.billingAddress.addressFields.postal.value = params.zipcodeBilling.value;
	session.forms.billing.billingAddress.addressFields.phone.value = params.phoneBilling.value;
	session.forms.billing.billingAddress.addressFields.country.value = params.countryBilling.value;
	
	return true;
}

/**
 * @function
 * @description This function set addresses in cart because Mattress Firm has one page checkout
 * In order to proceed or fail the order the cart must have addresses
 */
function setAddressesInCart() {
	var cart = app.getModel('Cart').get();
	var addresses = session.custom.addresses;
	var email = addresses.email.value;
	var shipments =  cart.getShipments();
	
	Transaction.wrap(function () {
    	cart.getBillingAddress().setCity(addresses.cityBilling.value);
        cart.getBillingAddress().setFirstName(addresses.firstnameBilling.value);
        cart.getBillingAddress().setLastName(addresses.lastnameBilling.value);
        cart.getBillingAddress().setAddress1(addresses.address1Billing.value);
        cart.getBillingAddress().setAddress2(addresses.address2Billing.value);
       	cart.getBillingAddress().setPostalCode(addresses.zipcodeBilling.value);
        cart.getBillingAddress().setPhone(addresses.phoneBilling.value);	
        cart.getBillingAddress().setStateCode(addresses.stateBilling.value);
        cart.getBillingAddress().setCountryCode(addresses.countryBilling.value);
        
        for(var i = 0; i < shipments.length ; i++){
        	    shipments[i].getShippingAddress().setCity(addresses.city.value);
		        shipments[i].getShippingAddress().setFirstName(addresses.firstname.value);
		        shipments[i].getShippingAddress().setLastName(addresses.lastname.value);
		        shipments[i].getShippingAddress().setAddress1(addresses.address1.value);
		        shipments[i].getShippingAddress().setAddress2(addresses.address2.value);
		       	shipments[i].getShippingAddress().setPostalCode(addresses.zipcode.value);
		        shipments[i].getShippingAddress().setPhone(addresses.phone.value);	
		        shipments[i].getShippingAddress().setStateCode(addresses.state.value);
		        shipments[i].getShippingAddress().setCountryCode(addresses.country.value);
        
        }
        cart.setCustomerEmail(email);
        cart.calculate();
    });
}

/**
 * @function 
 * @description This function sets the synchrony forms values got from the front end
 * it also calls setaddresses function to set shipping and billing addresses in forms
 * true only if all products are available.
 * received one param from request object
 * @returns boolean
 */
function fillSynchronyDigitalBuyFormData() {
	var params = request.httpParameterMap;
	session.forms.synchronydigitalbuy.FN.value              =   params.firstnameBilling.value;
    session.forms.synchronydigitalbuy.LN.value              =   params.lastnameBilling.value;
    session.forms.synchronydigitalbuy.custAddress1Id.value  =   params.address1Billing.value;
    session.forms.synchronydigitalbuy.cityId.value          =   params.cityBilling.value;
    session.forms.synchronydigitalbuy.zip.value             =   params.zipcodeBilling.value;
    session.forms.synchronydigitalbuy.phoneNumber.value     =   params.phoneBilling.value;
    session.forms.synchronydigitalbuy.stateId.value         =   params.stateBilling.value;
    session.forms.synchronydigitalbuy.emailAddress.value    =   params.email.value;
    session.custom.addresses                                =   params;
    session.custom.zipcode = params.zipcodeBilling.value;
    setAddresses(params);
    return true;
}

/** This function creates new order number  
 * COBilling-Start calls this function in order to assign values in synchrony digital buy form
 * @returns boolean
 */
function preFilledSynchronyDigitalBuyForm () {
	var orderNo = OrderMgr.createOrderNo();
	session.custom.synchronyDigitalBuyOrderNo = orderNo;
	session.forms.synchronydigitalbuy.TokenId.value = session.custom.token;
	session.forms.synchronydigitalbuy.merchantID.value = merchantID;
	session.forms.synchronydigitalbuy.CTID.value = orderNo;
  	return true;
}
/**
 * @function 
 * @description This function actually proceeds the placement of order
 */
function saveOrder() {
    var cart = app.getModel('Cart').get();
    if (!COBilling.ResetPaymentForms() || !COBilling.handleBillingAddress(cart) || COBilling.HandlePaymentSelection(cart).error) { //call controller
    	COBilling.ReturnToForm(cart);
    } else {
		// Mark step as fulfilled
		app.getForm('billing').object.fulfilled.value = true;
    		
        if (customer.authenticated && app.getForm('billing').object.billingAddress.addToAddressBook.value) {
            app.getModel('Profile').get(customer.profile).addAddressToAddressBook(cart.getBillingAddress());
        }

        // Mark step as fulfilled
        app.getForm('billing').object.fulfilled.value = true;

        // A successful billing page will jump to the next checkout step.
        //app.getController('COSummary').Submit();
        // Calling the mfrm specific controller
        var COSummary = require('app_mattressfirm_storefront/cartridge/controllers/COSummary');
        COSummary.Submit();
        
        return;
    }
}


/**
 * @function 
 * @description This function is called when digital buy modal closes
 * it calls the getStatusOfClosedModal from digitalbuyhelper and get the status of closed modal
 * Upon receiving the status of closed modal it will further proceed and calls for creating payment instrument
 * It also fails the order according to the status received from Synchrony digital buy
 */
function placeOrder() {
	 var cart = app.getModel('Cart').get();
	 //get userGent Mobile|Desktop
	 var isMobileDevice = StringHelpers.isMobileDevice(request.httpUserAgent);
	    
	 var isOnlyRedCarpetItemsInCart = !empty(cart.object) ? mattressPipeletHelper.getOrderIsRedCarpetStatus(cart.object) : false;
     if (isOnlyRedCarpetItemsInCart) {
		 var addressAtpInfoAttr = UtilFunctions.getAddressAtpInfoAttr_FromShipment(cart);
		 Transaction.wrap(function () {
	      		cart.object.custom.confirmationDeliveryDateTimeZoneZip = addressAtpInfoAttr;
	      		cart.object.custom.addressesDeliveryDateTimeZoneZip = addressAtpInfoAttr;
	       });
	 }
    if(COBilling.handleBillingAddress(cart)){
    	setAddressesInCart();
    }
    var statusResult = digitalBuyHelper.getStatusOfClosedModal();
    
    setPayLogger.info('Synchrony payment response: ' + JSON.stringify(statusResult));
    
    if(!empty(statusResult) && !empty(statusResult.StatusCode)) {
        if (statusResult.StatusCode === '000') {
            try {
            	digitalBuyHelper.createPaymentInstrument(statusResult);
            	saveOrder();
            } catch (e) {
                setPayLogger.error("Unable to create payment instrument");
                digitalBuyHelper.failOrder();
                if(isMobileDevice) {
                	response.redirect(URLUtils.https('COBilling-ContinueToBilling','er', 1));
                    return;
                } else {
	                response.redirect(URLUtils.https('COBilling-Start', 'er', 1));
	                return;
                }
            }
        } else if (statusResult.StatusCode === '100') {
        	if(isMobileDevice) {
            	response.redirect(URLUtils.https('COBilling-ContinueToBilling'));
                return;
            } else {
                response.redirect(URLUtils.https('COBilling-Start'));
                return;
            }
    	}
    	else if (statusResult.StatusCode === '400' || statusResult.StatusCode === '401' || statusResult.StatusCode === '403' || statusResult.StatusCode === '500') {
    		digitalBuyHelper.failOrder();
    		if(isMobileDevice) {
            	response.redirect(URLUtils.https('COBilling-ContinueToBilling','er', 1));
                return;
            } else {
                response.redirect(URLUtils.https('COBilling-Start', 'er', 1));
                return;
            }
    	}         
        else {
        	digitalBuyHelper.failOrder();
        	if(isMobileDevice) {
            	response.redirect(URLUtils.https('COBilling-ContinueToBilling','er', 1));
                return;
            } else {
                response.redirect(URLUtils.https('COBilling-Start', 'er', 1));
                return;
            }
        }
    } else {
    	digitalBuyHelper.failOrder();
    	if(isMobileDevice) {
        	response.redirect(URLUtils.https('COBilling-ContinueToBilling','er', 1));
            return;
        } else {
            response.redirect(URLUtils.https('COBilling-Start', 'er', 1));
            return;
        }
    }
}

/**
 * @function 
 * @description This function is called from front end when synchrony digital buy modal is about to open
 * it calls the authentication api to get a token
 * @returns It returns the token after getting from authentication API
 */
function getDigitalBuyClientToken() {
	try {
        var params : String;
        var token = "";
        params = "merchantId=" + merchantID + "&password=" + password;
        serviceObj.setURL(synchronyGetTokenURL);
        var tokenResult = serviceObj.call({params: params, place:"getToken"});
        var result = JSON.parse(tokenResult.object.text);
        if(result){
            token = result.clientToken;
            session.custom.token = token;
            session.forms.synchronydigitalbuy.TokenId.value = session.custom.token;
        }
        session.forms.billing.paymentMethods.selectedPaymentMethodID.value = 'SYNCHRONY_FINANCIAL';
        var obj = new Object();
        obj.token = token;
        app.getView({
            obj: obj
        }).render('util/json');
    } catch(e) {
        var msg = e;
    }
}


///*
//* Web exposed methods
//*/
exports.GetDigitalBuyClientToken = guard.ensure(['https', 'get'], getDigitalBuyClientToken);
exports.PlaceOrder = guard.ensure(['https', 'post'], placeOrder);
exports.FillSynchronyDigitalBuyFormData = guard.ensure(['https', 'post'], fillSynchronyDigitalBuyFormData);

/*
 * Local methods
 */
exports.preFilledSynchronyDigitalBuyForm = preFilledSynchronyDigitalBuyForm;

