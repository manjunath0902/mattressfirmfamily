'use strict';

var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var Transaction = require('dw/system/Transaction');
var synchHelper = require('~/cartridge/scripts/helper');
var Billing = require('~/cartridge/scripts/billing');
var Logger = require('dw/system/Logger');
var URLUtils = require('dw/web/URLUtils');
var Synchrony = require('int_synchrony/cartridge/scripts/util/Form');
var Pipelet = require('dw/system/Pipelet');
var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;
/*
 * Process a request on getting API Token from Synchrony
 */
function getFields () {
	var params = request.httpParameterMap;
	//var order = new Pipelet('CreateOrderNo').execute();
	var billToHomePhone : params.phone.value;
    billToHomePhone = billToHomePhone ? billToHomePhone.toString() : "";
	var phoneNumber = billToHomePhone.replace(/[^\d]+/g, '').trim()
    //session.custom.SynchronyOrderNo = order.OrderNo;
	session.forms.synchrony.clientToken.value = session.custom.token;
	session.forms.synchrony.billToFirstName.value    =   params.firstname.value;
    session.forms.synchrony.billToLastName.value     =   params.lastname.value;
    session.forms.synchrony.billToAddress1.value     =   params.address1.value;
    session.forms.synchrony.billToAddress2.value     =   params.address2.value;
    session.forms.synchrony.billToCity.value         =   params.city.value;
    session.forms.synchrony.billToZipCode.value      =   params.zipcode.value;
    session.forms.synchrony.billToHomePhone.value    =   params.phone.value;
    session.forms.synchrony.states.state.value       =   params.state.value;
    session.forms.synchrony.country.value            =   params.country.value;
    session.forms.synchrony.billToAccountNumber.value = "";
    session.custom.synchronyEmail = params.email.value;
    session.custom.shippingInfo = params; 

}
function getToken () {
	session.forms.billing.paymentMethods.selectedPaymentMethodID.value = 'SYNCHRONY_FINANCIAL';
	var obj = new Object();
    obj.token = synchHelper.GetToken();
    app.getView({
        obj: obj
    }).render('util/json');
}

function getCitiesAndStatesForZipCode (){
	var zipCode = request.httpParameterMap.zipcode.value;	
	var citiesAndStateJSON = mattressPipeletHelper.getCitiesAndStatesForZipCode(zipCode);	
	app.getView({
        JSONData: citiesAndStateJSON
    }).render('util/output');	
}

function purchase() {
	synchHelper.AssignData();
	app.getView().render('empty_response');
}

function creditApply() {
	app.getView().render('empty_response');
}

function callBack() {
	var RemoveExisting = false;
	var SynchronyErrorPI;
	try {
		synchHelper.CreatePaymentInstrument();
	} catch (e) {
		SynchronyErrorPI = "Unable to create payment instrument";
		Logger.error("Error int_synchrony CallBack action: ("+SynchronyErrorPI+") "+e.message + " | " + e.toString());
		
		response.redirect(URLUtils.https('COBilling-Start', 'er', 1));
	}
	Billing.Save();
}

function setBillToAccountNumber() {
	if (request.httpParameterMap.billToAccountNumber) {
		session.forms.synchrony.billToAccountNumber.value = request.httpParameterMap.billToAccountNumber.value;
	}
}

/*
 * Local methods
 */
exports.GetToken = guard.ensure(['https', 'get'], getToken);
 exports.GetFields = guard.ensure(['https', 'get'], getFields);
 exports.Purchase = guard.ensure(['https', 'post'], purchase);
exports.CreditApply = guard.ensure(['https', 'post'], creditApply);
exports.CallBack = guard.ensure(['https', 'get'], callBack);
exports.SetBillToAccountNumber = guard.ensure(['https', 'get'], setBillToAccountNumber);
exports.GetCitiesAndStatesForZipCode = guard.ensure(['https', 'get'], getCitiesAndStatesForZipCode);
