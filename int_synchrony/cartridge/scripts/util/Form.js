var app = require('app_storefront_controllers/cartridge/scripts/app');
var Pipelet = require('dw/system/Pipelet');

/**
 * Fills the synchrony form a data from billing address
 * 
 * @returns
 */
function fillForm () {
    var order = new Pipelet('CreateOrderNo').execute();
    var billToHomePhone : String = app.getForm('billing').object.billingAddress.addressFields.phone.value;
    billToHomePhone = billToHomePhone ? billToHomePhone.toString() : "";
	var phoneNumber = billToHomePhone.replace(/[^\d]+/g, '').trim()
		
    session.custom.SynchronyOrderNo = order.OrderNo;
	session.forms.synchrony.clientToken.value = session.custom.token;
    // copy the address attributes
    session.forms.synchrony.billToFirstName.value    =   app.getForm('billing').object.billingAddress.addressFields.firstName.value;
    session.forms.synchrony.billToLastName.value     =   app.getForm('billing').object.billingAddress.addressFields.lastName.value;
    session.forms.synchrony.billToAddress1.value     =   app.getForm('billing').object.billingAddress.addressFields.address1.value;
    session.forms.synchrony.billToAddress2.value     =   app.getForm('billing').object.billingAddress.addressFields.address2.value || '';
    session.forms.synchrony.billToCity.value         =   app.getForm('billing').object.billingAddress.addressFields.city.value;
    session.forms.synchrony.billToZipCode.value      =   app.getForm('billing').object.billingAddress.addressFields.postal.value;
    session.forms.synchrony.billToHomePhone.value    =   phoneNumber;
    session.forms.synchrony.states.state.value       =   app.getForm('billing').object.billingAddress.addressFields.states.state.value;
    session.forms.synchrony.country.value            =   app.getForm('billing').object.billingAddress.addressFields.country.value;
    session.forms.synchrony.billToAccountNumber.value = "";
}

/*
 * Local methods
 */
exports.fillForm = fillForm;
