/**
* Helper file in order to use the defined methods for more than one controller
*
* @module  scripts/digitalbuy/digitalBuyHelper
*/

'use strict';

var app = require('app_storefront_controllers/cartridge/scripts/app');
var servicePath = require("/bc_sleepysc/cartridge/scripts/init/service-Synchrony.ds");
var merchantID = dw.system.Site.getCurrent().getCustomPreferenceValue('synchronyMerchantId');
var password = dw.system.Site.getCurrent().getCustomPreferenceValue('synchronyMerchPassword');
var synchronyGetTokenURL = dw.system.Site.getCurrent().getCustomPreferenceValue('synchronyGetTokenURL');
var synchronyGetStatusURL = dw.system.Site.getCurrent().getCustomPreferenceValue('synchronyGetStatusURL');
var Logger = require('dw/system/Logger');
var setPayLogger = Logger.getLogger('setPayTulo','setPay');
var OrderMgr = require('dw/order/OrderMgr');
var Transaction = require('dw/system/Transaction');
var OrderMgr = require('dw/order/OrderMgr');
var Money = require('dw/value/Money'); 

var digitalBuyHelper = {

/**
 * @function 
 * @description This function creates synchrony payment instrument
 * @returns boolean
 */		
createPaymentInstrument : function (statusResult) {
    var Cart = app.getModel('Cart');
    var basket :  Basket = Cart.get().object;
    var paymentType = 'SYNCHRONY_FINANCIAL';

    var clientTransactionID : String = 'synchronyDigitalBuyOrderNo' in session.custom && !empty(session.custom.synchronyDigitalBuyOrderNo) ? session.custom.synchronyDigitalBuyOrderNo : "";

    var amountToRedeem : Money = new Money(basket.totalGrossPrice.add(basket.giftCertificateTotalPrice).value, basket.getCurrencyCode());
    Transaction.begin();
    try{
        var paymentInstr : PaymentInstrument = null;
        // try to grab the existing one if its already defined, as there should only be one
        for each (var pi:PaymentInstrument in basket.getPaymentInstruments(paymentType)) {
            if(empty(paymentInstr)) {
                paymentInstr = pi;
            } else {
                basket.removePaymentInstrument(pi);
            }
        }
        if(empty(paymentInstr)) {
            paymentInstr = basket.createPaymentInstrument(paymentType, amountToRedeem);
        } else {
            paymentInstr.paymentTransaction.setAmount(amountToRedeem);
        }

        paymentInstr.paymentTransaction.setTransactionID(statusResult.transactionId);
        paymentInstr.paymentTransaction.custom.authCode = statusResult.AuthCode;
        paymentInstr.paymentTransaction.custom.synchronyClientTransactionID = statusResult.transactionId;//session.custom.SynchronyOrderNo;        
        paymentInstr.paymentTransaction.custom.synchronyClientToken = statusResult.TokenId;
        paymentInstr.paymentTransaction.custom.synchronyFullName = statusResult.FirstName + " " + statusResult.LastName;
        paymentInstr.paymentTransaction.custom.mfiFinAccountNumber = statusResult.AccountNumber;
        paymentInstr.paymentTransaction.custom.synchronyTransactionDate = statusResult.TransactionDate;
        paymentInstr.paymentTransaction.custom.synchronyTransactionDesc = statusResult.TransactionDescription;
        paymentInstr.paymentTransaction.custom.authAmount = amountToRedeem.value;
        paymentInstr.paymentTransaction.custom.approvalStatus = statusResult.StatusCode;
        paymentInstr.paymentTransaction.custom.synchronyCapitalPromoCode = statusResult.PromoCode;
        paymentInstr.paymentTransaction.custom.mfiPlanId = statusResult.PromoCode;
        paymentInstr.paymentTransaction.custom.mfiSynchronyToken = statusResult.AccountToken;
        
        } catch(e) {
        	setPayLogger.error("Error in int_synchrony:createPaymentInstrument: Error: " + e.name + " | Message: " + e.message + " | " + e.toString());
            return false;
        }
        basket.updateTotals();
        Transaction.commit();

    return true;
},

/**
 * @function 
 * @description This function creates synchrony payment instrument
 * It iterates three times to make sure that status has been received
 * @returns status from status API
 */
getStatusOfClosedModal : function () {
    var result = {};
    
    // default count for status API is 3 
    var synchronyServiceCount = 3;
  
	if('synchronyServiceCount' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.synchronyServiceCount != null) {
		synchronyServiceCount = dw.system.Site.current.preferences.custom.synchronyServiceCount;
	}

    // calls the status API for multiple times in order to make sure the availability of status API response
    for (var i = 0; i < synchronyServiceCount; i++) {
        try {
            //var service = ServiceRegistry.get("synchrony.http.PostRequest");
        	var serviceObj = servicePath.SynchronyService;
        	var token = session.custom.token;
            if (token) {
                var paramsBody = {
                        "merchantNumber": merchantID,
                        "password" : password,
                        "userToken": token
                };
                serviceObj.setURL(synchronyGetStatusURL);
                var getStatusResult = serviceObj.call({params: JSON.stringify(paramsBody), place:"getStatus"});
                result = JSON.parse(getStatusResult.object.text);
                
                if(!empty(result)) {
                	setPayLogger.info('Status API hit is:' + i);
                    break;
                }
            }
        }
        catch(e) {
            var msg = e.message;
        }
    }
    return result;
},

/**
 * @function 
 * @description This function fails the synchrony order for tracking purposes*/
failOrder : function () { //change name failorder
	var order;
	var cart = app.getModel('Cart').get();
    Transaction.wrap(function () {
        order = OrderMgr.createOrder(cart.object, session.custom.synchronyDigitalBuyOrderNo);
        OrderMgr.failOrder(order);
        cart.calculate();
    });
   
    session.custom.synchronyDigitalBuyOrderNo = null;
    setPayLogger.info('Order faied due to status API error');
}
}

exports.digitalBuyHelper = digitalBuyHelper;
