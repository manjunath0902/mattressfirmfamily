/**
* 	This script is a meant to be a temporary fix for the cert issue.
*	On 15.7 DW updated Certs to TLS > 1.0, however the synchrony cert IS 1.0.
*	For the time being Synchrony has made it so first it does the 1.0 cert than it does 1.1.
*	However there is a 5 minute minimum delay for the asynchronous call that has all the info from synchrony.
*	This xchange link explains the update from 1.0 to > 1.0: https://xchange.demandware.com/docs/DOC-30508
*
*	@input CurrentHPM : dw.web.HttpParameterMap
*	@input Session : dw.system.Session
*/
importPackage( dw.system );
importPackage( dw.object );
importPackage( dw.util );
importPackage( dw.web );
importPackage( dw.order );

var app = require('app_storefront_controllers/cartridge/scripts/app');
var Transaction = require('dw/system/Transaction');
var Cart = app.getModel('Cart');
var Money = require('dw/value/Money');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Logger = require('dw/system/Logger');
var OrderMgr = require('dw/order/OrderMgr');

/**
 * Capture data from Synchrony response and saves all in custom object
 */
function assignData() {
	// to ensure uniqueness of the ID of the custom object we have to combine values
	// accountNumber-clientTransactionID
	var clientTransactionID = request.httpParameterMap.ClientTransactionID.value;
	var accountNumber = request.httpParameterMap.AccountNumber.value;

	// find the order by the clientTransactionID, it should be the order #
	var seekIt:SeekableIterator = OrderMgr.searchOrders("orderNo={0}", "creationDate asc", clientTransactionID);
	var synchronyOrder:Order = seekIt.first();
	seekIt.close();

	if(!empty(synchronyOrder)) {
		// grab the paymentInstrument
		var paymentTransaction:PaymentTransaction = null;
		for each(var paymentInstrument:PaymentInstrument in synchronyOrder.getPaymentInstruments('SYNCHRONY_FINANCIAL')) {
			// check the token on the payment transaction object
			// token isn't passed back in the asynch call so we can't pivot off of that
			// only thing is the amount of the transaction
			if(!empty(paymentInstrument.paymentTransaction) && paymentInstrument.paymentTransaction.amount.available && paymentInstrument.paymentTransaction.amount.value == request.httpParameterMap.TransactionAmount.value) {
				paymentTransaction = paymentInstrument.paymentTransaction;
				break;
			}
			//if(!empty(paymentInstrument.paymentTransaction) && 'clientToken' in paymentInstrument.paymentTransaction.custom) {}
		}

		if(paymentTransaction) {
			// ok, we have the payment transaction
			// now store all the incoming attributes from the asynch call in the appropriate custom attributes on the payment transaction
			paymentTransaction.custom.authCode = request.httpParameterMap.AuthCode.value;
			paymentTransaction.custom.geFullName = request.httpParameterMap.FirstName.value + ' ' + request.httpParameterMap.LastName.value;
			paymentTransaction.custom.geAccountNumber = request.httpParameterMap.AccountNumber.value;
			paymentTransaction.custom.geTransactionDate = request.httpParameterMap.TransactionDate.value;
			paymentTransaction.custom.geTransactionDesc = request.httpParameterMap.TransactionDescription.value;
			paymentTransaction.custom.approvalStatus = request.httpParameterMap.Status.value;
			paymentTransaction.custom.authAmount = request.httpParameterMap.TransactionAmount.value;
			paymentTransaction.custom.geCapitalPromoCode = request.httpParameterMap.PromoCode.value;

			// now that we have all the Synchrony attributes we can flag the order as ready to export
			synchronyOrder.setExportStatus(Order.EXPORT_STATUS_READY);
		}
	} else {
		// then the order hasn't been submitted yet so we need to store the data temporarily
		// to ensure uniqueness of the ID of the custom object we have to combine values
		// accountNumber-clientTransactionID

		// Session ID seemed more appropriate here for the Key ID
		var ID = session.sessionID;
		if(!empty(clientTransactionID) && !empty(accountNumber)) {
			try {
				Transaction.begin();
				var co : CustomObject = CustomObjectMgr.getCustomObject("SynchronyResponse",ID);
				if (empty(co)) {
	     			co = CustomObjectMgr.createCustomObject("SynchronyResponse",ID);
				}
	     		co.custom.AuthCode=request.httpParameterMap.AuthCode.value;
	     		co.custom.AccountNumber=request.httpParameterMap.AccountNumber.value;
	     		co.custom.FirstName=request.httpParameterMap.FirstName.value;
	     		co.custom.LastName=request.httpParameterMap.LastName.value;
	     		co.custom.ClientTransactionID=request.httpParameterMap.ClientTransactionID.value;
	     		co.custom.PromoCode=request.httpParameterMap.PromoCode.value;
	     		co.custom.Status=request.httpParameterMap.Status.value;
	     		co.custom.TransactionDate=request.httpParameterMap.TransactionDate.value;
	     		co.custom.TransactionDescription=request.httpParameterMap.TransactionDescription.value;
	     		co.custom.TransactionAmount=request.httpParameterMap.TransactionAmount.value;
	     		if('token' in session.custom && !empty(session.custom.token)) {
					co.custom.ClientToken = session.custom.token;
	     		}
				Transaction.commit();
			} catch(e) {
				Logger.error("Error int_synchrony assignData: "+e.message + " | " + e.toString());
				return false;
			}
		} else {
			Logger.error("Error int_synchrony assignData: No Client Transaction ID  and/or Account Number passed in");
			return false;
		}
	}

	return true;
}

/**
 * Creates Payment Instrument based on response from Synchrony or custom object which was created before
 */
function createPaymentInstrument() {
	var Cart = app.getModel('Cart');
	var basket :  Basket = Cart.get().object;
	var paymentType = 'SYNCHRONY_FINANCIAL';

	var clientToken : String = 'token' in session.custom && !empty(session.custom.token) ? session.custom.token : "";
	var clientTransactionID : String = 'SynchronyOrderNo' in session.custom && !empty(session.custom.SynchronyOrderNo) ? session.custom.SynchronyOrderNo : "";

	if(empty(clientToken)) {
		Logger.error("Error CreatePaymentInstrument: Token is no longer in session, need to kick the user back to the beginning");
		return false;
	}

	// works for mattress firm
	var amountToRedeem : Money = new Money(basket.totalGrossPrice.add(basket.giftCertificateTotalPrice).value, basket.getCurrencyCode());
	//var amountToRedeem : Money = new Money(basket.getTotalNetPrice().add(basket.giftCertificateTotalPrice).value, basket.getCurrencyCode());
	Transaction.begin();
	try{
		var paymentInstr : PaymentInstrument = null;
		// try to grab the existing one if its already defined, as there should only be one
		for each (var pi:PaymentInstrument in basket.getPaymentInstruments(paymentType)) {
			if(empty(paymentInstr)) {
				paymentInstr = pi;
			} else {
				basket.removePaymentInstrument(pi);
			}
		}
		if(empty(paymentInstr)) {
			paymentInstr = basket.createPaymentInstrument(paymentType, amountToRedeem);
		} else {
			paymentInstr.paymentTransaction.setAmount(amountToRedeem);
		}

		paymentInstr.paymentTransaction.setTransactionID(session.custom.SynchronyOrderNo);
		paymentInstr.paymentTransaction.custom.synchronyClientTransactionID = session.custom.SynchronyOrderNo;
		if('token' in session.custom && !empty(session.custom.token)) {
			paymentInstr.paymentTransaction.custom.synchronyClientToken = clientToken;
		}

		// try and fetch the custom object based on the client token in the session
			var authIterator : SeekableIterator = CustomObjectMgr.queryCustomObjects("SynchronyResponse","custom.ClientTransactionID = {0}", null,clientTransactionID);
			if(!empty(authIterator) && authIterator.count > 0) {
				var authCO : CustomObject = authIterator.first();
				authIterator.close();
				// now assign the custom attributes
				paymentInstr.paymentTransaction.custom.authCode = authCO.custom.AuthCode;
				paymentInstr.paymentTransaction.setTransactionID(authCO.custom.ClientTransactionID);
				paymentInstr.paymentTransaction.custom.synchronyClientTransactionID = authCO.custom.ClientTransactionID;
				paymentInstr.paymentTransaction.custom.synchronyFullName = authCO.custom.FirstName + " " + authCO.custom.LastName;

				// saving here but we wil encrypt upon checkout
				paymentInstr.paymentTransaction.custom.mfiFinAccountNumber = authCO.custom.AccountNumber;

				paymentInstr.paymentTransaction.custom.synchronyTransactionDate = authCO.custom.TransactionDate;
				paymentInstr.paymentTransaction.custom.synchronyTransactionDesc = authCO.custom.TransactionDescription;
				paymentInstr.paymentTransaction.custom.approvalStatus = authCO.custom.Status;
				paymentInstr.paymentTransaction.custom.authAmount = amountToRedeem.value;
				paymentInstr.paymentTransaction.custom.synchronyCapitalPromoCode = authCO.custom.PromoCode;
				paymentInstr.paymentTransaction.custom.mfiPlanId = authCO.custom.PromoCode;

				// Delete the Custom Object
				CustomObjectMgr.remove(authCO);
			} else {
				if(!empty(authIterator)) {
					authIterator.close();
				}
				// if its empty we still just let the payment through as this script should ONLY be called on the successful callback from Synchrony
				// the asynch call will update the order
			}
		} catch(e) {
			Logger.error("Error in int_synchrony:createPaymentInstrument: Error: " + e.name + " | Message: " + e.message + " | " + e.toString());
			return false;
		}
		basket.updateTotals();
	    Transaction.commit();

	return true;
}

/**
 * Makes API call into Synchrony to get token
 */
var getToken = function() {
	var serviceTimeout = 5000;
	var httpClient : HTTPClient = new dw.net.HTTPClient();
	var token = "";
	var message : String;
	var url= dw.system.Site.current.getCustomPreferenceValue('synchronyLoginURL');
	var userID = dw.system.Site.current.getCustomPreferenceValue('synchronyMerchantNumber');
	var password = dw.system.Site.current.getCustomPreferenceValue('synchronyMerchantPassword');

	try {
		httpClient.setTimeout(serviceTimeout);
		httpClient.open('POST', url, userID, password);
		httpClient.send();
	} catch (e) {
		Logger.error("Error in int_synchrony:GetToken: Error: " + e.name + " | Message: " + e.message + " | " + e.toString());
	}

	if (httpClient.statusCode === 200) {
		message = httpClient.text.toString();
		token = message.replace(/^\w+/g, '').trim();
	} else {
		// error handling
		message = "An error occured with status code " + httpClient.statusCode;
		Logger.error(message);
	}
	session.custom.token = token;

	return session.custom.token;
}

/*
 * Local methods
 */
exports.CreatePaymentInstrument = createPaymentInstrument;
exports.AssignData = assignData;
exports.GetToken = getToken;