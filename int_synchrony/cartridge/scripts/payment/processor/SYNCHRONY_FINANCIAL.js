'use strict';

var Cart = require('app_storefront_controllers/cartridge/scripts/models/CartModel');
var PaymentMgr = require('dw/order/PaymentMgr');
var Transaction = require('dw/system/Transaction');
var Site = require('dw/system/Site');
var app = require('app_storefront_controllers/cartridge/scripts/app');
var Logger = require('dw/system/Logger');

/**
 * Creates a payment instrument
 */
function Handle(args) {
    var cart = Cart.get(args.Basket);

	Transaction.wrap(function () {
		// Grab the already setup Payment Instrument
		var paymentInstruments = cart.getPaymentInstruments('SYNCHRONY_FINANCIAL');
		if (paymentInstruments.length != 1) {
			throw "Synchrony error: more than one Synchrony PaymentInstrument found when handling transaction";
		}
		var paymentInstrument = paymentInstruments.get(0); // should only be one PaymentMethod

        // Encrypt the account number in-place
        if (Site.getCurrent().getCustomPreferenceValue('synchronyPublicKey')) {
            var cipher : Cipher = new dw.crypto.Cipher();
    		var key = Site.getCurrent().getCustomPreferenceValue('synchronyPublicKey');
            try {
               	var encrypted : String = cipher.encrypt(paymentInstrument.paymentTransaction.custom.mfiFinAccountNumber, key, "RSA", null, 0);
        		paymentInstrument.paymentTransaction.custom.mfiFinAccountNumber = encrypted;
            } catch(e) {
            	Logger.error("Error int_synchrony assign mfiFinAccountNumber: " + e.message + " | " + e.toString());
            }
        }

		if('token' in session.custom && !empty(session.custom.token)) {
			paymentInstrument.paymentTransaction.custom.synchronyClientToken = session.custom.token;
		}
    })

    return {success: true};
}

/**
 * Authorizes a payment using a credit card. The payment is authorized by using the CYBERSOURCE_BML processor only and
 * setting the order no as the transaction ID. Customizations may use other processors and custom logic to authorize
 * credit card payment.
 */
function Authorize(args) {
    return require('app_storefront_controllers/cartridge/scripts/payment/processor/BASIC_CREDIT').Authorize(args);
}

/*
 * Local methods
 */
exports.Handle = Handle;
exports.Authorize = Authorize;
