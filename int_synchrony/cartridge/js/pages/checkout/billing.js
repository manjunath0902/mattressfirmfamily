'use strict';

var ajax = require('../../ajax'),
    formPrepare = require('./formPrepare'),
    giftcard = require('../../giftcard'),
    util = require('../../util'); 

/**
 * @function
 * @description Fills the Credit Card form with the passed data-parameter and clears the former cvn input
 * @param {Object} data The Credit Card data (holder, type, masked number, expiration month/year)
 */
function setCCFields (data) {
    var $creditCard = $('[data-method="CREDIT_CARD"]');
    $creditCard.find('input[name$="creditCard_owner"]').val(data.holder).trigger('change');
    $creditCard.find('select[name$="_type"]').val(data.type).trigger('change');
    $creditCard.find('input[name*="_creditCard_number"]').val(data.maskedNumber).trigger('change');
    $creditCard.find('[name$="_month"]').val(data.expirationMonth).trigger('change');
    $creditCard.find('[name$="_year"]').val(data.expirationYear).trigger('change');
    $creditCard.find('input[name$="_cvn"]').val('').trigger('change');
}

/**
 * @function
 * @description Updates the credit card form with the attributes of a given card
 * @param {String} cardID the credit card ID of a given card
 */
function populateCreditCardForm (cardID) {
    // load card details
    var url = util.appendParamToURL(Urls.billingSelectCC, 'creditCardUUID', cardID);
    ajax.getJson({
        url: url,
        callback: function (data) {
            if (!data) {
                window.alert(Resources.CC_LOAD_ERROR);
                return false;
            }
            setCCFields(data);
        }
    });
}

/**
 * @function
 * @description Changes the payment method form depending on the passed paymentMethodID
 * @param {String} paymentMethodID the ID of the payment method, to which the payment method form should be changed to
 */
function updatePaymentMethod (paymentMethodID) {
    var $paymentMethods = $('.payment-method');
    $paymentMethods.removeClass('payment-method-expanded');

    var $selectedPaymentMethod = $paymentMethods.filter('[data-method="' + paymentMethodID + '"]');
    if ($selectedPaymentMethod.length === 0) {
        $selectedPaymentMethod = $('[data-method="Custom"]');
    }
    $selectedPaymentMethod.addClass('payment-method-expanded');

    // ensure checkbox of payment method is checked
    $('input[name$="_selectedPaymentMethodID"]').removeAttr('checked');
    $('input[value=' + paymentMethodID + ']').prop('checked', 'checked');
    if (paymentMethodID === 'SYNCHRONY_FINANCIAL') {
        // get token
//        ajax.getJson({
//            url: Urls.getToken,
//            callback: function (data) {
//                $('input[name="clientToken"]').val(data.token);
//            }
//        });
        $('.synchrony-payment-method').removeClass('hidden');
        $('.other-payment-method').addClass('hidden');
    } else if (paymentMethodID === 'PayPal') {
    	$('.synchrony-payment-method').addClass('hidden');
        $('.other-payment-method-button').addClass('hidden');
    	$('.other-payment-method').removeClass('hidden');
    }
    else {
        $('.other-payment-method').removeClass('hidden');
        $('.other-payment-method-button').removeClass('hidden');
        $('.synchrony-payment-method').addClass('hidden');
        formPrepare.validateForm();
    }
}

/**
 * @function
 * @description This function opens synchrony modal for Tulo
 */
function openSynchronyModal () {
    ajax.getJson({
        url: Urls.getClientToken,
        async: false,
        callback: function (data) {
            if(data && data.token) {
                $('input[name="tokenId"]').val(data.token);
                $('#dbuymodel3').removeClass('hidden-xs');
                var synchronyForm = $("#dbuyform3")[0]; 
                callCombinedModaldBuyProcess(synchronyForm);
            }
        }
    });
}

/**
 * @function
 * @description This function opens synchrony modal for Mattressfirm
 */
function openSynchronyDigitalBuyModal () {
	if ($(".mobile-pt-checkout-container").length) {
		$(".mobile-pt-checkout-container").css("overflow-y","hidden");
		$(".mobile-pt-checkout-container").css("-webkit-overflow-scrolling","unset");
	}
    ajax.getJson({
        url: Urls.getDigitalBuyClientToken,
        async: false,
        callback: function (data) {
            if(data && data.token) {
                $('input[name="tokenId"]').val(data.token);
                var synchronyDigitalBuyForm = $("#dbuyform3")[0];
                callCombinedModaldBuyProcess(synchronyDigitalBuyForm);
            }
        }
    });
    
}

/**
 * @function
 * @description This function get address values from DOM
 * Ajax call saves the addresses at backend 
 */
function fillSynchronyDigitalBuyForm() {
	var preFormData = {};
	var promoCode = $('input[name=promoCode]:checked').val();
	if($('#useasbillingaddress .input-checkbox').is(":checked")){
    	preFormData = {
        		'firstname': $('#dwfrm_singleshipping_shippingAddress_addressFields_firstName').val(),
        		'lastname': $('#dwfrm_singleshipping_shippingAddress_addressFields_lastName').val(),
        		'address1': $('#dwfrm_singleshipping_shippingAddress_addressFields_address1').val(),
        		'address2': $('#dwfrm_singleshipping_shippingAddress_addressFields_address2').val(),
        		'city': $('#dwfrm_singleshipping_shippingAddress_addressFields_city').val(),
        		'zipcode': $('#dwfrm_singleshipping_shippingAddress_addressFields_postal').val(),
        		'phone': $('#dwfrm_singleshipping_shippingAddress_addressFields_phone').val(),
        		'state': $('#dwfrm_singleshipping_shippingAddress_addressFields_states_state').val(),
        		'country': $('#dwfrm_singleshipping_shippingAddress_addressFields_country').val(),
        		'email' : $('#dwfrm_singleshipping_shippingAddress_email_emailAddress').val(),
        		'emailsubscription': $('#dwfrm_singleshipping_shippingAddress_addToSubscription').is(":checked"),
        		'sameAsShipping': true,
        		'firstnameBilling': $('#dwfrm_singleshipping_shippingAddress_addressFields_firstName').val(),
	    		'lastnameBilling': $('#dwfrm_singleshipping_shippingAddress_addressFields_lastName').val(),
	    		'address1Billing': $('#dwfrm_singleshipping_shippingAddress_addressFields_address1').val(),
	    		'address2Billing': $('#dwfrm_singleshipping_shippingAddress_addressFields_address2').val(),
	    		'cityBilling': $('#dwfrm_singleshipping_shippingAddress_addressFields_city').val(),
	    		'zipcodeBilling': $('#dwfrm_singleshipping_shippingAddress_addressFields_postal').val(),
	    		'phoneBilling': $('#dwfrm_singleshipping_shippingAddress_addressFields_phone').val(),
	    		'stateBilling': $('#dwfrm_singleshipping_shippingAddress_addressFields_states_state').val(),
	    		'countryBilling': $('#dwfrm_singleshipping_shippingAddress_addressFields_country').val()
        }
    }
    else{
		preFormData = {
	    		'firstname': $('#dwfrm_singleshipping_shippingAddress_addressFields_firstName').val(),
	    		'lastname': $('#dwfrm_singleshipping_shippingAddress_addressFields_lastName').val(),
	    		'address1': $('#dwfrm_singleshipping_shippingAddress_addressFields_address1').val(),
	    		'address2': $('#dwfrm_singleshipping_shippingAddress_addressFields_address2').val(),
	    		'city': $('#dwfrm_singleshipping_shippingAddress_addressFields_city').val(),
	    		'zipcode': $('#dwfrm_singleshipping_shippingAddress_addressFields_postal').val(),
	    		'phone': $('#dwfrm_singleshipping_shippingAddress_addressFields_phone').val(),
	    		'state': $('#dwfrm_singleshipping_shippingAddress_addressFields_states_state').val(),
	    		'country': $('#dwfrm_singleshipping_shippingAddress_addressFields_country').val(),
	    		'email' : $('#dwfrm_singleshipping_shippingAddress_email_emailAddress').val(),
	    		'emailsubscription': $('#dwfrm_singleshipping_shippingAddress_addToSubscription').is(":checked"),
	    		'sameAsShipping': false,
	    		'firstnameBilling': $('#dwfrm_billing_billingAddress_addressFields_firstName').val(),
	    		'lastnameBilling': $('#dwfrm_billing_billingAddress_addressFields_lastName').val(),
	    		'address1Billing': $('#dwfrm_billing_billingAddress_addressFields_address1').val(),
	    		'address2Billing': $('#dwfrm_billing_billingAddress_addressFields_address2').val(),
	    		'cityBilling': $('#dwfrm_billing_billingAddress_addressFields_city').val(),
	    		'zipcodeBilling': $('#dwfrm_billing_billingAddress_addressFields_postal').val(),
	    		'phoneBilling': $('#dwfrm_billing_billingAddress_addressFields_phone').val(),
	    		'stateBilling': $('#dwfrm_billing_billingAddress_addressFields_states_state').val(),
	    		'countryBilling': $('#dwfrm_billing_billingAddress_addressFields_country').val()
	        }
    }
	$('input[name="custFirstName"]').val(preFormData.firstnameBilling);
	$('input[name="custLastName"]').val(preFormData.lastnameBilling);
	$('input[name="custZipCode"]').val(preFormData.zipcodeBilling);
	$('input[name="custAddress1"]').val(preFormData.address1Billing);
	$('input[name="custCity"]').val(preFormData.cityBilling);
	$('input[name="custState"]').val(preFormData.stateBilling);
	$('input[name="phoneNumber"]').val(preFormData.phoneBilling);
	$('input[name="emailAddress"]').val(preFormData.email);
	$('input[name="transPromo1"]').val(promoCode);
    
	$.ajax({
		url: util.appendParamsToUrl(Urls.fillSynchronyDigitalBuyFormData, preFormData),
		type: 'POST',
		async: false,
		success: function () {
			openSynchronyDigitalBuyModal();
		},
		failure: function () {
		}
	});
}

function fillSynchronyDigitalBuyFormAB() {
	var preFormData = {};
	var promoCode = $('input[name=promoCode]:checked').val();
	if($('#useasbillingaddress .input-checkbox').is(":checked")){
    	preFormData = {
        		'firstname': $("#savedFirstName").val(),
        		'lastname':  $("#savedLastName").val(),
        		'address1':  $("#saveAddressOne").val(),
        		'address2':  $("#saveAddressTwo").val(),
        		'city':      $("#saveCityName").val(),
        		'zipcode':   $("#savedZippedCode").val(),
        		'phone':     $("#savedPhoneNumber").val(),
        		'state':     $("#saveStateName").val(),
        		'country':   "US",
        		'email' :    $("#savedEmail").val(),
        		'emailsubscription':$('#dwfrm_singleshipping_shippingAddress_addToSubscription').is(":checked"),
        		'sameAsShipping':   true,
        		'firstnameBilling': $("#savedFirstName").val(),
	    		'lastnameBilling':  $("#savedLastName").val(),
	    		'address1Billing':  $("#saveAddressOne").val(),
	    		'address2Billing':  $("#saveAddressTwo").val(),
	    		'cityBilling':      $("#saveCityName").val(),
	    		'zipcodeBilling':   $("#savedZippedCode").val(),
	    		'phoneBilling':     $("#savedPhoneNumber").val(),
	    		'stateBilling':     $("#saveStateName").val(),
	    		'countryBilling':   "US"
        }
    }
    else{
		preFormData = {
				'firstname':$("#savedFirstName").val(),
        		'lastname': $("#savedLastName").val(),
        		'address1': $("#saveAddressOne").val(),
        		'address2': $("#saveAddressTwo").val(),
        		'city':     $("#saveCityName").val(),
        		'zipcode':  $("#savedZippedCode").val(),
        		'phone':    $("#savedPhoneNumber").val(),
        		'state':    $("#saveStateName").val(),
        		'country':  "US",
        		'email' :   $("#savedEmail").val(),
	    		'emailsubscription':$('#dwfrm_singleshipping_shippingAddress_addToSubscription').is(":checked"),
	    		'sameAsShipping':   false,
	    		'firstnameBilling': $('#dwfrm_billing_billingAddress_addressFields_firstName').val(),
	    		'lastnameBilling':  $('#dwfrm_billing_billingAddress_addressFields_lastName').val(),
	    		'address1Billing':  $('#dwfrm_billing_billingAddress_addressFields_address1').val(),
	    		'address2Billing':  $('#dwfrm_billing_billingAddress_addressFields_address2').val(),
	    		'cityBilling':      $('#dwfrm_billing_billingAddress_addressFields_city').val(),
	    		'zipcodeBilling':   $('#dwfrm_billing_billingAddress_addressFields_postal').val(),
	    		'phoneBilling':     $('#dwfrm_billing_billingAddress_addressFields_phone').val(),
	    		'stateBilling':     $('#dwfrm_billing_billingAddress_addressFields_states_state').val(),
	    		'countryBilling':   $('#dwfrm_billing_billingAddress_addressFields_country').val()
	        }
    }
	$('input[name="custFirstName"]').val(preFormData.firstnameBilling);
	$('input[name="custLastName"]').val(preFormData.lastnameBilling);
	$('input[name="custZipCode"]').val(preFormData.zipcodeBilling);
	$('input[name="custAddress1"]').val(preFormData.address1Billing);
	$('input[name="custCity"]').val(preFormData.cityBilling);
	$('input[name="custState"]').val(preFormData.stateBilling);
	$('input[name="phoneNumber"]').val(preFormData.phoneBilling);
	$('input[name="emailAddress"]').val(preFormData.email);
	$('input[name="transPromo1"]').val(promoCode);
    
	$.ajax({
		url: util.appendParamsToUrl(Urls.fillSynchronyDigitalBuyFormData, preFormData),
		type: 'POST',
		async: false,
		success: function () {
			openSynchronyDigitalBuyModal();
		},
		failure: function () {
		}
	});
}

function validateShippingAddress() {
    var zipCode = $('#dwfrm_singleshipping_shippingAddress_addressFields_postal').val();
    var isValidAddress = false;
    var $continue = $('#synchronyDigitalBuyButton');
    if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCode)) {
    	var isValidCity = false, isValidState = false;            	
        var	city = $('#dwfrm_singleshipping_shippingAddress_addressFields_city').val();
        var	state = $('#dwfrm_singleshipping_shippingAddress_addressFields_states_state').val();        
        var citiesAndStatesForZipCode = getCitiesAndStatesForZipCode(zipCode); 
    	if (citiesAndStatesForZipCode != null && citiesAndStatesForZipCode.cities.length > 0) {                		
		    for (var i = 0; i < citiesAndStatesForZipCode.cities.length; i++){
		        var data = citiesAndStatesForZipCode.cities[i];
		        if (city.toString().toLowerCase() === data.city.toLowerCase()) {
		        	isValidCity = true;
				}
		        if (state.toString().toLowerCase() === data.state.toLowerCase()) { 
		        	isValidState = true;
				}      
		    }    		    
		    if (!isValidCity) {
		    	//display error for city
		    	if ($(".ship-city .error-message").length < 1) {
		            $(".ship-city .field-wrapper").append("<div class='error-message'>Please enter a valid city</div>");
		            if($("#dwfrm_singleshipping_shippingAddress_addressFields_city").length > 0) {
		            	$("#dwfrm_singleshipping_shippingAddress_addressFields_city").addClass("error");
		            }
				    $continue.attr('disabled', 'disabled');
		        }
		    }else{
		    	 if ($(".ship-city .error-message").length > 0) {
		             $(".ship-city .error-message").remove();
		         }
		    }    		    
		    if (!isValidState) {
		    	//display error for state
		    	if ($(".ship-state .error-message").length < 1) {
		            $(".ship-state .field-wrapper").append("<div class='error-message'>Please select a valid state </div>");
		            if($("#dwfrm_singleshipping_shippingAddress_addressFields_states_state").length > 0) {
		            	$("#dwfrm_singleshipping_shippingAddress_addressFields_states_state").addClass("error");
		            }
				    $continue.attr('disabled', 'disabled');
		        }    
		    }else{
		    	 if ($(".ship-state .error-message").length > 0) {
		             $(".ship-state .error-message").remove();
		         }
		    }
		    if ($(".ship-zipcode .error-message").length > 0) {
	            $(".ship-zipcode .error-message").remove();
	        }
	    }else {
	    	if ($(".ship-zipcode .error-message").length < 1) {
	            $(".ship-zipcode .field-wrapper").append("<div class='error-message'>Please enter a valid zipcode</div>");
	            if($("#dwfrm_singleshipping_shippingAddress_addressFields_postal").length > 0) {
	            	$("#dwfrm_singleshipping_shippingAddress_addressFields_postal").addClass("error");
	            }
			    $continue.attr('disabled', 'disabled');
	        }
	    }        	
    	if(isValidCity == true && isValidState == true) {
    		isValidAddress = true;            		      		       		
    	}else {
    		isValidAddress = false;            		
    	}            
        return isValidAddress; 
    }
    return isValidAddress; 
}

function validateShippingAddressAB() {
    var zipCode = $("#savedZippedCode").val();
    var isValidAddress = false;
    var $continue = $('#synchronyDigitalBuyButton');
    if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCode)) {
    	var isValidCity = false, isValidState = false;            	
        var	city = $("#saveCityName").val();
        var	state = $("#saveStateName").val();        
        var citiesAndStatesForZipCode = getCitiesAndStatesForZipCode(zipCode); 
    	if (citiesAndStatesForZipCode != null && citiesAndStatesForZipCode.cities.length > 0) {                		
		    for (var i = 0; i < citiesAndStatesForZipCode.cities.length; i++){
		        var data = citiesAndStatesForZipCode.cities[i];
		        if (city.toString().toLowerCase() === data.city.toLowerCase()) {
		        	isValidCity = true;
				}
		        if (state.toString().toLowerCase() === data.state.toLowerCase()) { 
		        	isValidState = true;
				}      
		    }    		    
		    if (!isValidCity) {
		    	//display error for city
		    	if ($(".ship-city .error-message").length < 1) {
		            $(".ship-city .field-wrapper").append("<div class='error-message'>Please enter a valid city</div>");
		            if($("#dwfrm_singleshipping_shippingAddress_addressFields_city").length > 0) {
		            	$("#dwfrm_singleshipping_shippingAddress_addressFields_city").addClass("error");
		            }
				    $continue.attr('disabled', 'disabled');
		        }
		    }else{
		    	 if ($(".ship-city .error-message").length > 0) {
		             $(".ship-city .error-message").remove();
		         }
		    }    		    
		    if (!isValidState) {
		    	//display error for state
		    	if ($(".ship-state .error-message").length < 1) {
		            $(".ship-state .field-wrapper").append("<div class='error-message'>Please select a valid state </div>");
		            if($("#dwfrm_singleshipping_shippingAddress_addressFields_states_state").length > 0) {
		            	$("#dwfrm_singleshipping_shippingAddress_addressFields_states_state").addClass("error");
		            }
				    $continue.attr('disabled', 'disabled');
		        }    
		    }else{
		    	 if ($(".ship-state .error-message").length > 0) {
		             $(".ship-state .error-message").remove();
		         }
		    }
		    if ($(".ship-zipcode .error-message").length > 0) {
	            $(".ship-zipcode .error-message").remove();
	        }
	    }else {
	    	if ($(".ship-zipcode .error-message").length < 1) {
	            $(".ship-zipcode .field-wrapper").append("<div class='error-message'>Please enter a valid zipcode</div>");
	            if($("#dwfrm_singleshipping_shippingAddress_addressFields_postal").length > 0) {
	            	$("#dwfrm_singleshipping_shippingAddress_addressFields_postal").addClass("error");
	            }
			    $continue.attr('disabled', 'disabled');
	        }
	    }        	
    	if(isValidCity == true && isValidState == true) {
    		isValidAddress = true;            		      		       		
    	}else {
    		isValidAddress = false;            		
    	}            
        return isValidAddress; 
    }
    return isValidAddress; 
}

function validateBillingAddress() {
    var zipCode = $('#dwfrm_billing_billingAddress_addressFields_postal').val();
    var isValidAddress = false;
    var $continue = $('#synchronyDigitalBuyButton');
    if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCode)) {
    	var isValidCity = false, isValidState = false;            	
    	var city = $('#dwfrm_billing_billingAddress_addressFields_city').val();
    	var state = $('#dwfrm_billing_billingAddress_addressFields_states_state').val(); 
        var citiesAndStatesForZipCode = getCitiesAndStatesForZipCode(zipCode);            	
    	if (citiesAndStatesForZipCode != null && citiesAndStatesForZipCode.cities.length > 0) {                 		
		    for (var i = 0; i < citiesAndStatesForZipCode.cities.length; i++){
		        var data = citiesAndStatesForZipCode.cities[i];
		        if (city.toString().toLowerCase() === data.city.toLowerCase()) {
		        	isValidCity = true;
				}
		        if (state.toString().toLowerCase() === data.state.toLowerCase()) {
		        	isValidState = true;
				}      
		    }    		    
	    	if (!isValidCity) {
		    	//display error for city
		    	if ($(".bill-city .error-message").length < 1) {
		            $(".bill-city .field-wrapper").append("<div class='error-message'>Please enter a valid city</div>");
		            $("#dwfrm_billing_billingAddress_addressFields_city").addClass("error");
				    $continue.attr('disabled', 'disabled');
		        }
		    }else{
		    	 if ($(".bill-city .error-message").length > 0) {
		             $(".bill-city .error-message").remove();
		         }
		    }
		    
		    if (!isValidState) {
		    	//display error for state
		    	if ($(".bill-state .error-message").length < 1) {
		            $(".bill-state .field-wrapper").append("<div class='error-message'>Please select a valid state </div>");
		            $("#dwfrm_billing_billingAddress_addressFields_states_state").addClass("error");
				    $continue.attr('disabled', 'disabled');
		        }    
		    }else{
		    	 if ($(".bill-state .error-message").length > 0) {
		             $(".bill-state .error-message").remove();
		         }
		    }
		    if ($(".bill-zipcode .error-message").length > 0) {
	            $(".bill-zipcode .error-message").remove();
	        }
	    }else {
	    	if ($(".bill-zipcode .error-message").length < 1) {
	            $(".bill-zipcode .field-wrapper").append("<div class='error-message'>Please enter a valid zipcode</div>");
	            $("#dwfrm_billing_billingAddress_addressFields_postal").addClass("error");
			    $continue.attr('disabled', 'disabled');
	        }
	    }      
    	if(isValidCity == true && isValidState == true) {
    		isValidAddress = true;            		      		       		
    	}else {
    		isValidAddress = false;            		
    	}            
        return isValidAddress; 
    }	
}

function getCitiesAndStatesForZipCode(zipCodeParam){
    var zipCode = zipCodeParam;
    var isValidAddress = false;
    var result = null;
    if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCode)) {            
        $.ajax({
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            type: "get",
            url: Urls.getCitiesAndStatesForZipCode,
            async: false,
            cache: false,
            data: {"zipcode": zipCode},
            success: function (response) {            	
            	if (response != null) {        		    
        		    result = response;
        	    }
            },
            error: function () {                
            }
        });
        return result;
    }	
}
function validateDeliveryDate(){
	if(SitePreferences.enableATPOnOrderSubmit == true){
		var selectedPaymentMethod = $('.payment-method-options').find(':checked').val(); 		
 		//if (selectedPaymentMethod=='SYNCHRONY_FINANCIAL') {
 			var urlATP = util.appendParamsToUrl(Urls.confirmATPAvailablity, {format:'ajax'});	
 			var showATPModal = false;
 			ajax.getJson({
 		        url: urlATP,
 		        async: false,
 		        callback: function (data) {
 		            if(data && data.success) {
 		            	if(typeof data.showATPCalendar !== 'undefined' && data.showATPCalendar == true){
 		            		showATPModal = true;
 		            	}
 		            }
 		        }
 		    });
 			
 			if(showATPModal === true){		
 				if ($('.isMobileDevice').length > 0 && $('.isMobileDevice').val() == 'true') {
 					// New Mobile view
 					if($('div.mobile-checkout-process-ab').length > 0 ){ 				
 						window.location.href = util.appendParamsToUrl(Urls.redirectToDeliveryAB, {deliveryScheduleTimeout : 'true'});
 					} else if($('#synchrony_submit_order.button-mobile-payment').length > 0 ){ 
 						window.location.href = util.appendParamsToUrl(Urls.COShippingMethodStart, {deliveryScheduleTimeout : 'true'});
 					}else{
 						continueToSynchronyDigitalBuyModal();
 					}
				} else {
					$('#synchronyDigitalBuyButton').trigger('synchrony.confim.delivery.date');
				}
 			}else { 
				continueToSynchronyDigitalBuyModal();
			} 				
 		//} // Submit after checking the ATP
	}else {
		openSynchronyDigitalBuyModal();
	}
}

function continueToSynchronyDigitalBuyModal(){
	
	 if ($('.mobile-checkout-process-ab').length > 0) {
        $('.use-my-card').addClass('disabled-btn');
    	//Nwe Checkout mobile redesign flow for opening synchrony Modal
    	var isValidShippingAddress = validateShippingAddressAB(); 
       if(isValidShippingAddress == true){
       	if($('#useasbillingaddress .input-checkbox').is(":checked")){        		
       		 fillSynchronyDigitalBuyFormAB();
           }else {            	
               var isValidBillingAddress = validateBillingAddress();
               if(isValidBillingAddress == true){
               	 fillSynchronyDigitalBuyFormAB();                	
               }            	
           }        	
       } else {
       	return false;
       }
    } else {
    	//Existing flow for opening synchrony Modal
       var isValidShippingAddress = validateShippingAddress(); 
       if(isValidShippingAddress == true){
       	if($('#useasbillingaddress .input-checkbox').is(":checked")){        		
       		 fillSynchronyDigitalBuyForm();
           }else {            	
               var isValidBillingAddress = validateBillingAddress();
               if(isValidBillingAddress == true){
               	 fillSynchronyDigitalBuyForm();                	
               }            	
           }        	
       } else {
       	return false;
       }
    }
}
/**
 * @function
 * @description loads billing address, Gift Certificates, Coupon and Payment methods
 */
exports.init = function () {
    var $checkoutForm = $('.checkout-billing');
    var $addGiftCert = $('#add-giftcert');
    var $giftCertCode = $('input[name$="_giftCertCode"]');
    var $addCoupon = $('#add-coupon');
    var $couponCode = $('input[name$="_couponCode"]');
    var $selectPaymentMethod = $('.payment-method-options');
    var selectedPaymentMethod = $selectPaymentMethod.find(':checked').val();
    var $inputCC = $('input[name*="_creditCard_number"]');
    var controlKeys = ['8', '13', '46', '45', '36', '35', '38', '37', '40', '39'];

    formPrepare.init({
        formSelector: 'form[id$="billing"]',
        continueSelector: '[name$="billing_save"]'
    });
    
    $('#checkout').on('click', function (e) {
        e.preventDefault();
        openSynchronyModal();       
    });
    
    $(document).on('hidden.bs.modal','#digBuyModal', function () {
    	if ($(".mobile-pt-checkout-container").length) {
    		$(".mobile-pt-checkout-container").css("overflow-y","scroll");
    		$(".mobile-pt-checkout-container").css("-webkit-overflow-scrolling","touch");
    	}
        $('#synchrony-save').click();
        if (SitePreferences.currentSiteId == 'Mattress-Firm') {
        	$('#synchronyDigitalBuyButton').attr('disabled', 'disabled');
            $('#synchrony_submit_order .btn-order-submit').addClass('loader-indicator-synchronydigitalbuybutton');
        } else {
        	$('#dbuymodel3').addClass('hidden-xs');
        }
    }); 

    // default payment method to 'CREDIT_CARD'
    updatePaymentMethod((selectedPaymentMethod) ? selectedPaymentMethod : 'CREDIT_CARD');
    $selectPaymentMethod.on('click', 'input[type="radio"]', function () {
        updatePaymentMethod($(this).val());
    });
    //CC validation!!!
    $inputCC.on('keypress', function (event) {
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		if (!event.which ||
			(48 <= event.which && event.which <= 57) ||
			(48 == event.which && $(this).attr("value")) ||
			isControlKey) {
			return;
		} else {
			event.preventDefault();
		}
	});
    // select credit card from list
    $('#creditCardList').on('change', function () {
        var cardUUID = $(this).val();
        if (!cardUUID) {
            return;
        }
        populateCreditCardForm(cardUUID);

        // remove server side error
        $('.required.error').removeClass('error');
        $('.error-message').remove();
    });

    $('#check-giftcert').on('click', function (e) {
        e.preventDefault();
        var $balance = $('.balance');
        if ($giftCertCode.length === 0 || $giftCertCode.val().length === 0) {
            var error = $balance.find('span.error');
            if (error.length === 0) {
                error = $('<span>').addClass('error').appendTo($balance);
            }
            error.html(Resources.GIFT_CERT_MISSING);
            return;
        }

        giftcard.checkBalance($giftCertCode.val(), function (data) {
            if (!data || !data.giftCertificate) {
                $balance.html(Resources.GIFT_CERT_INVALID).removeClass('success').addClass('error');
                return;
            }
            $balance.html(Resources.GIFT_CERT_BALANCE + ' ' + data.giftCertificate.balance).removeClass('error').addClass('success');
        });
    });

    $addGiftCert.on('click', function (e) {
        e.preventDefault();
        var code = $giftCertCode.val(),
            $error = $checkoutForm.find('.giftcert-error');
        if (code.length === 0) {
            $error.html(Resources.GIFT_CERT_MISSING);
            return;
        }

        var url = util.appendParamsToUrl(Urls.redeemGiftCert, {giftCertCode: code, format: 'ajax'});
        $.getJSON(url, function (data) {
            var fail = false;
            var msg = '';
            if (!data) {
                msg = Resources.BAD_RESPONSE;
                fail = true;
            } else if (!data.success) {
                msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
                fail = true;
            }
            if (fail) {
                $error.html(msg);
                return;
            } else {
                window.location.assign(Urls.billing);
            }
        });
    });

    $addCoupon.on('click', function (e) {
        e.preventDefault();
        var $error = $checkoutForm.find('.coupon-error'),
            code = $couponCode.val();
        if (code.length === 0) {
            $error.html(Resources.COUPON_CODE_MISSING);
            return;
        }

        var url = util.appendParamsToUrl(Urls.addCoupon, {couponCode: code, format: 'ajax'});
        $.getJSON(url, function (data) {
            var fail = false;
            var msg = '';
            if (!data) {
                msg = Resources.BAD_RESPONSE;
                fail = true;
            } else if (!data.success) {
                msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
                fail = true;
            }
            if (fail) {
                $error.html(msg);
                return;
            }

            //basket check for displaying the payment section, if the adjusted total of the basket is 0 after applying the coupon
            //this will force a page refresh to display the coupon message based on a parameter message
            if (data.success && data.baskettotal === 0) {
                window.location.assign(Urls.billing);
            }
        });
    });

    // trigger events on enter
    $couponCode.on('keydown', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            $addCoupon.click();
        }
    });
    $giftCertCode.on('keydown', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            $addGiftCert.click();
        }
    });
    if ($('.step.step-3').hasClass('active')) {
        $('.right-summary').css('display', 'block');
    }

    $('form[id*="billing"] *[name*="addressFields"]').on('change', function () {
        var idParts = $(this).attr('id').split('_');
        var fieldUniqId = idParts[idParts.length - 1];
        fieldUniqId = fieldUniqId.charAt(0).toUpperCase() + fieldUniqId.slice(1);
        var $updatedFiled;
        if (fieldUniqId === "Postal") {
            $updatedFiled = $('#synchronyForm').find('*[name*="billToZipCode"]');
        } else {
            $updatedFiled = $('#synchronyForm').find('*[name*="' + fieldUniqId + '"]');
        }
        if (typeof $updatedFiled !== 'undefined') {
            $updatedFiled.val($(this).val());
        }
    });

    $('#lookUpLink').on('click', function (e) {
        e.preventDefault();
        var divClass = $(this).attr('id'),
            $div = $('.' + divClass);

        $div.hide();
        $(this).hide();
        $('.purchaseLink, #purchaseLink').show();
    });

    $('#purchaseLink').on('click', function (e) {
        e.preventDefault();
        var divClass = $(this).attr('id'),
            $div = $('.' + divClass);

        $div.hide();
        $(this).hide();
        $('.lookUpLink, #lookUpLink').show();
    });

    $('#synchronyDigitalBuyButton').on('click', function (e) {        
        e.preventDefault();
        validateDeliveryDate();
        //continueToSynchronyDigitalBuyModal();
    });
    

    
};
