var Store = require('app_storefront_controllers/cartridge/controllers/Stores');
function getValidZipCode() {
	if(empty(request.getGeolocation()) && empty(session.custom.customerZip)) {
		return null;
	}
	var geolocationZip = request.getGeolocation() ? request.getGeolocation().getPostalCode() : null;
	var existingZipCode = session.custom.customerZip;
	var zipCode = geolocationZip;
	if(existingZipCode && existingZipCode != '00000') {
		zipCode = existingZipCode; 
	}
	return zipCode;
}
function getPreferedStoreByZipCode(zipCode) {        
	var nearestStores = Store.GetStoresByGeoLocation(zipCode);
    if(!empty(nearestStores) && nearestStores.length > 0){
    	return nearestStores[0]
    }
	return null;   
}

module.exports = {
		getValidZipCode: getValidZipCode,
		getPreferedStoreByZipCode: getPreferedStoreByZipCode
};

