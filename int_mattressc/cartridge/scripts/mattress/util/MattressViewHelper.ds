/**
* Demandware Script File
* This is meant to be included within isml templates to make displaying custom
* client data easier. Specifically this will handle various recycling fee
* cacluations
*
*/
importPackage( dw.system );
importPackage( dw.catalog );
importPackage( dw.util );
importPackage( dw.order );
importPackage( dw.value );


var adjustmentPromotionId : String = "RECYCLING_FEE";
var manualDiscountPromotionId : String = "StoreOperator_ManualDiscount";
var Order = require('dw/order');



var MattressViewHelper = {

	/*
	* Returns a boolean if the product has a recycling fee
	*
	*/
	hasRecycleFees : function(pli: ProductLineItem ) : Boolean
	{
		if (!empty(pli) && pli.getPriceAdjustmentByPromotionID(adjustmentPromotionId)) {
			return true;
		}
		// Edge case here assuming no recycle fee on main line item but might have it on option ( foundation )
		if (!empty(pli.optionModel)) {
			var optionProducts : Array = pli.getOptionProductLineItems().toArray();
			for (var i =0; i < optionProducts.length; i++) {
				if (optionProducts[i].getPriceAdjustmentByPromotionID(adjustmentPromotionId)) {
					return true;
				}
			}
		}
		return false;
	},

	/*
	* Returns a boolean if the product (product / option product) itself has a recycling fee
	*/
	hasRecycleFeesThisLineItemOnly : function(pli: ProductLineItem ) : Boolean
	{
		if (!empty(pli) && pli.getPriceAdjustmentByPromotionID(adjustmentPromotionId)) {
			return true;
		}
		return false;
	},
	/*
	* Remove the Manual Shipping Discount on Basket Level if they exist
	* Returns a boolean if the Shipping discount is removed or not.
	*/
	removeManualShippingDiscount : function(Basket) : Boolean
	{
		var isRemoved = false ;
		var paShippingAmountDiscount: PriceAdjustment = Basket.getShippingPriceAdjustmentByPromotionID(manualDiscountPromotionId);
	    // If the PriceAdjustment already existed then remove it before applying new
		if (!empty(paShippingAmountDiscount)) {
			Basket.removeShippingPriceAdjustment(paShippingAmountDiscount);
			if (empty(Basket.getShippingPriceAdjustmentByPromotionID(manualDiscountPromotionId))) {
				isRemoved = true;
			}
		}
	    //  Apply, update or remove Price adjustment.
		return isRemoved;
	},

	/*
	* Returns true if the given ProductLineItem / OptionLineItem qualifies for Manual Discount application
	*/
	isManualDiscountAllowed : function(pli: ProductLineItem) : Boolean
	{
		// if PLI is warranty - return false
		return true;
	},
	
	/*
	* Returns true if the given ProductLineItem / OptionLineItem has a PriceAdjustment applied, false otherwise
	*/
	hasManualDiscount : function(pli: ProductLineItem) : Boolean
	{
		if (!empty(pli) && pli.getPriceAdjustmentByPromotionID(manualDiscountPromotionId) && 
			pli.getPriceAdjustmentByPromotionID(manualDiscountPromotionId).getAppliedDiscount().fixedPrice != pli.basePrice) {
			return true;
		}
		return false;
	},
	
	/*
	* Returns the Fixed Unit Price that was set for the given ProductLineItem / OptionLineItem, during manual discount application
	*/
	getManualFixedUnitPrice : function(pli: ProductLineItem) : Number
	{
		var newFixedPrice;
		if (!empty(pli) && pli.getPriceAdjustmentByPromotionID(manualDiscountPromotionId)) {
			newFixedPrice = pli.getPriceAdjustmentByPromotionID(manualDiscountPromotionId).getAppliedDiscount().fixedPrice;
		}
		return newFixedPrice;
	},

	/*
	* Recycle Fees can be on the line item or on product option attached
	* to the line item. This will subtract all of those for display and
	* return a money object
	* Found in calculations that we need to either subtract fees
	* just from the line item or include it's options with
	* optional 3rd parameter subOptionFees
	*/
	subtractLIRecycleFees : function(pli : ProductLineItem, pliPrice : Money, subOptionFees : Boolean) : Money
	{
		if (typeof subOptionFees === 'undefined') {
			subOptionFees = true;
		}
		var priceWithoutFees : Money = pliPrice;
		var mainPriceAdjustment : PriceAdjustment = pli.getPriceAdjustmentByPromotionID(adjustmentPromotionId);
		if (!empty(mainPriceAdjustment)) {
			priceWithoutFees = priceWithoutFees.subtract(mainPriceAdjustment.netPrice);
		}

		if (subOptionFees) {
			// now subtract any product option price adjustments
			var optionProducts : Array = pli.getOptionProductLineItems().toArray();
			// For each product option
			for (var i =0; i < optionProducts.length; i++) {
				if (optionProducts[i].getPriceAdjustmentByPromotionID(adjustmentPromotionId)) {
					priceWithoutFees = priceWithoutFees.subtract(optionProducts[i].getPriceAdjustmentByPromotionID(adjustmentPromotionId).netPrice);
				}
			}
		}

		return priceWithoutFees;

	},

	/*
	* Recycle Fees can be on the line item or on product option attached
	* to the line item. This will add all of those for display and
	* return a money object
	*/
	getTotalRecycleFees : function(pli : ProductLineItem) : Money {
		var totalFee : Money = new Money(0.0, session.currency.currencyCode);

		var mainPriceAdjustment : PriceAdjustment = pli.getPriceAdjustmentByPromotionID(adjustmentPromotionId);
		if (!empty(mainPriceAdjustment)) {
			totalFee  = totalFee.add(mainPriceAdjustment.netPrice);
		}

		// now add any product option price adjustments
		var optionProducts : Array = pli.getOptionProductLineItems().toArray();
		// For each product option
		for (var i =0; i < optionProducts.length; i++) {
			if (optionProducts[i].getPriceAdjustmentByPromotionID(adjustmentPromotionId)) {
				totalFee = totalFee.add(optionProducts[i].getPriceAdjustmentByPromotionID(adjustmentPromotionId).netPrice);
			}
		}

		return totalFee;

	},
	
	/*
	* Recycle Fees for the passed line item (product / product option) only and
	* return a money object
	*/
	getRecycleFeesForThisLineItemOnly : function(pli : ProductLineItem) : Money {
		var totalFee : Money = new Money(0.0, session.currency.currencyCode);

		var mainPriceAdjustment : PriceAdjustment = pli.getPriceAdjustmentByPromotionID(adjustmentPromotionId);
		if (!empty(mainPriceAdjustment)) {
			totalFee  = totalFee.add(mainPriceAdjustment.netPrice);
		}
		return totalFee;
	},

	getNumNonRecycleAdjustments : function(pli : ProductLineItem) : Number {
		var numOfAdjustments = pli.priceAdjustments.length;
		if (pli.getPriceAdjustmentByPromotionID(adjustmentPromotionId)) {
			numOfAdjustments--;
		}
		return numOfAdjustments;
	},

	getNonRecycleAdjustments : function(pli : ProductLineItem) : ArrayList {
		var adjustments : ArrayList = new ArrayList();
		for (var i = 0; i < pli.priceAdjustments.length; i++) {
			if (pli.priceAdjustments[i].promotionID != adjustmentPromotionId) {
				adjustments.add(pli.priceAdjustments[i]);
			}
		}
		return adjustments;
	},
	
	getTotalRecycleFeeForCart : function(basket) : Money {
		var productLineItems: Iterator = basket.getAllProductLineItems().iterator();
        var totalRecycleFee : Money = new Money(0.0, session.currency.currencyCode);
        while (productLineItems.hasNext()) {
            var pli: ProductLineItem = productLineItems.next();
            var mainPriceAdjustment : PriceAdjustment = pli.getPriceAdjustmentByPromotionID(adjustmentPromotionId);
			if (!empty(mainPriceAdjustment)) {
				totalRecycleFee  = totalRecycleFee.add(mainPriceAdjustment.netPrice);
			}	
			// now add any product option price adjustments
			var optionProducts : Array = pli.getOptionProductLineItems().toArray();
			// For each product option
			for (var i =0; i < optionProducts.length; i++) {
				if (optionProducts[i].getPriceAdjustmentByPromotionID(adjustmentPromotionId)) {
					totalRecycleFee = totalRecycleFee.add(optionProducts[i].getPriceAdjustmentByPromotionID(adjustmentPromotionId).netPrice);
				}
			}
        }
		return totalRecycleFee;
	},
	
	getTotalRecycleFeeForCartWithoutOptionLineItems : function(basket) : Money {
		var productLineItems: Iterator = basket.getAllProductLineItems().iterator();
        var totalRecycleFee : Money = new Money(0.0, session.currency.currencyCode);
        while (productLineItems.hasNext()) {
            var pli: ProductLineItem = productLineItems.next();
            var mainPriceAdjustment : PriceAdjustment = pli.getPriceAdjustmentByPromotionID(adjustmentPromotionId);
			if (!empty(mainPriceAdjustment)) {
				totalRecycleFee  = totalRecycleFee.add(mainPriceAdjustment.netPrice);
			}
        }
		return totalRecycleFee;
	},
	
	getGrossTotalForCart : function(basket) : Money {
		var productLineItems: Iterator = basket.getAllProductLineItems().iterator();
        var totalGrossPrice : Money = new Money(0.0, session.currency.currencyCode);
        while (productLineItems.hasNext()) {
            var pli: ProductLineItem = productLineItems.next();
			var product = pli.getProduct();
			if (!empty(product)) {
				var PriceModel = product.getPriceModel();
				if (PriceModel.getPrice().available) {				
					var priceBook = PriceModel.priceInfo.priceBook;
					while (priceBook.parentPriceBook) {
						priceBook = priceBook.parentPriceBook ? priceBook.parentPriceBook : priceBook; 
					}
					var productTotalPrice = new Money(PriceModel.getPriceBookPrice(priceBook.ID) * pli.quantityValue, session.currency.currencyCode);
					totalGrossPrice = totalGrossPrice.add(productTotalPrice);
				}
			}
        }
		return totalGrossPrice;
	},
	
    isTaxableLineItem: function(li) {
        return (li instanceof Order.ProductLineItem || li instanceof Order.ShippingLineItem || li instanceof Order.ProductShippingLineItem);
    },

    

	getTotalRevenueForTaxPercent: function(basket): Number {
	    try {	        
	        var shipments = basket.getShipments().iterator();
	        var taxRate = 0;
	               
	        //loop through shipments to properly match up the shipping address with the correct products and shipping line items
	        while (shipments.hasNext()) {
	            var shipment = shipments.next();
	            var shipmentType = shipment.custom.shipmentType;
	            if (empty(shipment.getProductLineItems())) {
	                continue;
	            }
	            var lis = shipment.getProductLineItems().iterator();
	            while (lis.hasNext()) {
	                var li = lis.next();
	                if (this.isTaxableLineItem(li)) {	                	
	                    if (li.adjustedTax.value > 0) {
	                             taxRate = li.getTaxRate();
	                             break;
	                        }
	                }
	            }
	        }
	        return taxRate*100;
	    } catch (e) {
	        var msg = e;	
	    }
	},
	
	getGrossTotalWithOptionsForCart : function(basket) : Money {
		var productLineItems: Iterator = basket.getAllProductLineItems().iterator();
        var totalGrossPrice : Money = new Money(0.0, session.currency.currencyCode);
        while (productLineItems.hasNext()) {
            var pli: ProductLineItem = productLineItems.next();
			var product = pli.getProduct();
			if (!empty(product)){
				
				if (!empty(pli.bundledProductLineItem) && pli.bundledProductLineItem == false){
					// Optional product price is defined at product level. Pricebook price is for individual products
					if (!empty(pli.optionProductLineItem) && pli.optionProductLineItem){
						
						if(!empty(pli.netPrice) ){
							totalGrossPrice = totalGrossPrice.add(pli.netPrice);
						}
						
					}else{
						var PriceModel = product.getPriceModel();
						if (PriceModel.getPrice().available) {				
							var priceBook = PriceModel.priceInfo.priceBook;
							while (priceBook.parentPriceBook) {
								priceBook = priceBook.parentPriceBook ? priceBook.parentPriceBook : priceBook; 
							}
							var productTotalPrice = new Money(PriceModel.getPriceBookPrice(priceBook.ID) * pli.quantityValue, session.currency.currencyCode);
							totalGrossPrice = totalGrossPrice.add(productTotalPrice);
						}
					}
				}
			}
        }
		return totalGrossPrice;
	},
	
	/*
	* Cart Total with all types applicable fees including PLI, OLI, Tax, Shipping charges
	*/
	getCartTotal : function(basket) : Money {
		var productLineItems: Iterator = basket.getAllProductLineItems().iterator();
        var totalGrossPrice : Money = new Money(0.0, session.currency.currencyCode);
        while (productLineItems.hasNext()) { // Loop through all the PLIs (including OLIs) to sum up their total 
            var pli: ProductLineItem = productLineItems.next();
			var product = pli.getProduct();
			if (!empty(product)) {
				if (!empty(pli.bundledProductLineItem) && pli.bundledProductLineItem == false) {
					// Optional product price is defined at product level. Pricebook price is for individual products
					if (!empty(pli.optionProductLineItem) && pli.optionProductLineItem) {
						if(!empty(pli.netPrice)) {
							totalGrossPrice = totalGrossPrice.add(pli.netPrice);
						}
					} else {
						var PriceModel = product.getPriceModel();
						if (PriceModel.getPrice().available) {
							var priceBook = PriceModel.priceInfo.priceBook;
							while (priceBook.parentPriceBook) {
								priceBook = priceBook.parentPriceBook ? priceBook.parentPriceBook : priceBook; 
							}
							var productTotalPrice = new Money(PriceModel.getPriceBookPrice(priceBook.ID) * pli.quantityValue, session.currency.currencyCode);
							totalGrossPrice = totalGrossPrice.add(productTotalPrice);
						}
					}
				}
			}
        } // PLIs calculation complete
        
        // Get Total Recycle Fee.
        var totalRecycleFee : Money = new Money(0.0, session.currency.currencyCode);
        var plis: Iterator = basket.getAllProductLineItems().iterator();
        while (plis.hasNext()) {
            var pli: ProductLineItem = plis.next();
            var mainPriceAdjustment : PriceAdjustment = pli.getPriceAdjustmentByPromotionID(adjustmentPromotionId);
			if (!empty(mainPriceAdjustment)) {
				totalRecycleFee  = totalRecycleFee.add(mainPriceAdjustment.netPrice);
			}
        }
        if (totalRecycleFee) {
        	totalGrossPrice = totalGrossPrice.add(totalRecycleFee);
        }
        // Add Gift Certificate cost in cart total
        //totalGrossPrice = totalGrossPrice.add(giftCertificateTotalPrice);
        
        if (basket.totalTax.available) { // Add Tax in cart total
        	totalGrossPrice = totalGrossPrice.add(basket.totalTax);
        }
        
        if (basket.shippingTotalPrice >= 0) {
	        totalGrossPrice = totalGrossPrice.add(basket.shippingTotalPrice);
        }
                
        return totalGrossPrice;
	}
}

module.exports = MattressViewHelper;