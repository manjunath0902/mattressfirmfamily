package sandbox;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
//import org.openqa.selenium.security.UserAndPassword;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Sandbox {
	
	private static Logger log = LoggerFactory.getLogger(Sandbox.class);


	public static void main(String... args) throws Exception {		
		mfiHappyPathAsGuest();
		mfiHappyPathAsRegisteredUser();
	}
	
	private static void mfiHappyPath(WebDriver driver, Actions actions) throws Exception {
		//
		// close annoying modal
		//
		log.info("closing modal");
		clickWhenReady(driver, By.id("email-modal-close"), 10);
		
		//
		// hover over MATTRESSES top nav
		//
		log.info("mouse over Mattresses navigation");
		actions.moveToElement(driver.findElement(By.linkText("MATTRESSES")))
				//.pause(Duration.ofSeconds(2))
				.build()
				.perform();;
		
		
		//
		// Click "Shop All Mattresses" navigation link
		//
		log.info("clicking 'Shop All Mattresses'");
		actions.moveToElement(driver.findElement(By.linkText("SHOP ALL MATTRESSES")))			
			.click()
			.build()
			.perform();

		//
		// Scroll down, forcing ajax fetch of 12 more products
		//
		List<WebElement> tiles = Collections.emptyList();
		String scrollY = null;
		boolean stopScrolling = false;
		
		while (!stopScrolling) {        
        	executeJS(driver, "window.scrollBy(0,50)"); 
            tiles = driver.findElements(By.cssSelector("li.grid-tile .product-tile .product-image a"));
            scrollY = executeJS(driver, "return window.scrollY;").toString();
            log.info("found {} products on PLP, scroll position is {}", tiles.size(), scrollY);
            stopScrolling = tiles.size() > 21 || Double.valueOf(scrollY) > 6500.0;
            sleep(200);    		        
        };
        sleep(1000);
        if (tiles.size() <= 21) {
        	throw new RuntimeException("failed to scroll, found " + tiles.size() + " products, scrolled to " + scrollY);
        }   
		log.info("clicking the 22nd product tile");
        tiles.get(21).click();

		//
		//	PDP Size dropdown, select FULL, or 1st option if FULL doesn't exist
		//
        log.info("selecting FULL size (or first option if no FULL)");
		driver.findElement(By.cssSelector("span.fake-input")).click();
		List<WebElement> sizeOptions = driver.findElements(By.cssSelector("div.size-select > span > ul > li"));
		sizeOptions.stream()
			.filter(option -> option.getText().equalsIgnoreCase("FULL"))
			.findFirst()
			.orElse(sizeOptions.get(0)).click();


		//
		//	PDP foundatoin dropdown, select first
		//
		log.info("selecting first foundation");
		Select s = new Select(driver.findElement(By.className("product-option")));
		if (s.getOptions().size() > 1)
			s.selectByIndex(1);
		else
			s.selectByIndex(0);
		
		log.info("clicking add to cart");
		clickWhenReady(driver, By.id("add-to-cart"), 20);
		log.info("clicking view cart");
		clickWhenReady(driver, By.linkText("VIEW CART"), 20);
		log.info("clicking checkout");
		clickWhenReady(driver, By.id("checkout-button-ID"), 20);
		
		//
		// Pick a delivery window if available
		//
		List<WebElement> timeSlots = Collections.emptyList();
		timeSlots = driver.findElements(By.cssSelector(".delivery-schedule-box .atp .schedule .time-slot"));
		if (timeSlots.size() > 0)
			timeSlots.get(0).click();
		
		log.info("clicking continue checkout");
		clickWhenReady(driver, By.id("shippingMethodContinueCheckout"), 20);
	}

	private static void enterShippingBillingInfo(WebDriver driver, Actions actions) throws Exception
	{
		log.info("filling out checkout form");
		driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_email_emailAddress")).clear();
		driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_email_emailAddress")).sendKeys("fcth00+mfrm@gmail.com");
		driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_firstName")).sendKeys("Tom");
		driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_lastName")).sendKeys("Daly");
		driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_address1")).sendKeys("1000 S Oyster Bay Road");
		
		WebElement city = driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_city"));
		city.clear();
		city.sendKeys("Hicksville");
		
		WebElement zip = driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_postal"));
		zip.clear();
		zip.sendKeys("11801");
				
		executeJS(driver, "window.scrollBy(0,200)");
		driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_postal")).sendKeys("11714");
		new Select(driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_states_state"))).selectByValue("NY");
		driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_phone")).sendKeys("516-555-1234");
		driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_number")).sendKeys("4111111111111111");
		driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_expirydate")).sendKeys("10/24");
		driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_cvn")).sendKeys("717");
		driver.findElement(By.id("dwfrm_billing_billingAddress_scheduleLater")).click();		
		
		log.info("submitting order");
		getWhenVisible(driver, By.id("billingSubmitButton"), 20).click();
	}
	
	private static void mfiHappyPathAsGuest() throws Exception {
		
		
		String url = "https://storefront:face@staging-web-sleepys.demandware.net/on/demandware.store/Sites-Mattress-Firm-Site";
		WebDriver driver = openBrowser(url);
		log.info("opening browser, url: {}", url);
	    
		Actions actions = new Actions(driver);
		
		
		//
		// Run Common Happy Path
		//
		mfiHappyPath(driver, actions);
		
		//
		//	Checkout, customer details
		//
		enterShippingBillingInfo(driver, actions);
		
		// wait for confirmation page
		getWhenVisible(driver, By.cssSelector("button.print_button"), 30);
		
		
		// get order number
		String orderNumber = driver.findElement(By.cssSelector("div.order-number span.value")).getText();
		log.info("order submitted, Order Number: {}", orderNumber);
		
		
		log.info("taking screenshot");
		TakesScreenshot scrShot = ((TakesScreenshot)driver);
	    byte[] bytes = scrShot.getScreenshotAs(OutputType.BYTES);
	    Paths.get("./orders").toFile().mkdirs();
	    Path path = Paths.get("./orders/order" + orderNumber + ".png");
		Files.write(path, bytes);
		log.info("done, wrote {}", path);
		driver.close();
	}

	private static void mfiHappyPathAsRegisteredUser() throws Exception {
		
		
		//String url = "https://storefront:face@staging-web-sleepys.demandware.net/on/demandware.store/Sites-Mattress-Firm-Site";
		String url = "https://storefront:face@development-web-sleepys.demandware.net/on/demandware.store/Sites-Mattress-Firm-Site";
		//String url = "https://www.mattressfirm.com";
		WebDriver driver = openBrowser(url);
		log.info("opening browser, url: {}", url);
	    
		Actions actions = new Actions(driver);
		
		// Try logging in
		/*Alert alert = driver.switchTo().alert() ;
		alert.authenticateUsing(new UserAndPassword("debug", "2c9b7cfe67fa39373c606e9c7ab0c1e6"));
		driver.switchTo().defaultContent() ; 
		sleep(100);*/
		
		
		//
		// Run Common Happy Path
		//
		mfiHappyPath(driver, actions);
		
		//
		//	Login
		//
		log.info("Logging in");
		driver.findElement(By.linkText("SIGN IN")).click();
		driver.findElement(By.id("36:2;a")).sendKeys("fcth00+mfrm@gmail.com");
		driver.findElement(By.id("50:2;a")).sendKeys("Aa123456");
		driver.findElement(By.xpath("//button[@title='Log in']")).click();
				
		enterShippingBillingInfo(driver, actions);
		
		// wait for confirmation page
		getWhenVisible(driver, By.cssSelector("button.print_button"), 30);
		
		
		// get order number
		String orderNumber = driver.findElement(By.cssSelector("div.order-number span.value")).getText();
		log.info("order submitted, Order Number: {}", orderNumber);
		
		
		log.info("taking screenshot");
		TakesScreenshot scrShot = ((TakesScreenshot)driver);
	    byte[] bytes = scrShot.getScreenshotAs(OutputType.BYTES);
	    Paths.get("./orders").toFile().mkdirs();
	    Path path = Paths.get("./orders/order" + orderNumber + ".png");
		Files.write(path, bytes);
		log.info("done, wrote {}", path);
		driver.close();
	}
	
	/**
	 * sleep for specified {@code millis} milliseconds 
	 * @param millis
	 */
	private static void sleep(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
		}
		
	}



	/**
	 * Opens a browser window and return a new {@linkplain WebDriver} for the given URL 
	 * @param url
	 * @return
	 */
	private static WebDriver openBrowser(String url) {

		// Optional, if not specified, WebDriver will search your path for chromedriver.
		// System.setProperty("webdriver.chrome.driver", "C:\\java\\selenium-java-3.141.59\\drivers\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", "target/classes/drivers/chromedriver.exe");
		
		//WebDriver driver = new ChromeDriver();
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("start-maximized");
		options.setExperimentalOption("useAutomationExtension", false);	// stops "Error Loading Extension" popup
		WebDriver driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);				
		driver.manage().window().maximize();
		//	driver.manage().window().setSize(new Dimension(1600, 900));
		driver.get(url);
		return driver;
	}
	
	/**
	 * Execute a block of javascript code
	 * 
	 * @param driver
	 * @param script
	 */
	private static Object executeJS(WebDriver driver, String script) {
		return ((JavascriptExecutor) driver).executeScript(script);
	}

	

	/**
	 * returns the <b>locator</b> when it is visible, waiting up to <b>timeout</b> seconds, throws
	 * {@linkplain TimeoutException} if not visible in time
	 * 
	 * @param driver
	 * @param locator
	 * @param timeout
	 * @return
	 * @throws TimeoutException
	 */
	private static WebElement getWhenVisible(WebDriver driver, By locator, int timeout) throws TimeoutException {
		WebElement element = null;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		return element;
	}

	/**
	 * Wait up to <b>timeout</b> seconds for the <b>locator</b> to be clickable before clicking it,
	 * throws {@linkplain TimeoutException} if not clickable in time
	 * 
	 * @param driver
	 * @param locator
	 * @param timeout
	 * @throws TimeoutException
	 */
	private static void clickWhenReady(WebDriver driver, By locator, int timeout) {
		WebElement element = null;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		element = wait.until(ExpectedConditions.elementToBeClickable(locator));
		element.click();
	}
	
	

	
	static void multipleActionsExample(WebDriver driver) throws Exception {

		String baseUrl = "http://www.facebook.com/"; 

		driver.get(baseUrl);
		WebElement txtUsername = driver.findElement(By.id("email"));

		Actions builder = new Actions(driver);
		Action seriesOfActions = builder
			.moveToElement(txtUsername)
			.click()
			.keyDown(txtUsername, Keys.SHIFT)
			.sendKeys(txtUsername, "hello")
			.keyUp(txtUsername, Keys.SHIFT)
			.doubleClick(txtUsername)
			.contextClick()
			.build();
			
		seriesOfActions.perform() ;		
	}
}
