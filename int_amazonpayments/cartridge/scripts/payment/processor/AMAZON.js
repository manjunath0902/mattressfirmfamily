'use strict';

/* API Includes */
var Cart        = require('app_storefront_controllers/cartridge/scripts/models/CartModel'),
    PaymentMgr  = require('dw/order/PaymentMgr'),
    Transaction = require('dw/system/Transaction');

/**
 * Creates new Amazon payment instrument.
 */
function Handle(args) {
    var cart = Cart.get(args.Basket);

    Transaction.wrap(function() {
        cart.removeExistingPaymentInstruments('AMAZON_PAYMENTS');
        cart.createPaymentInstrument('AMAZON_PAYMENTS', cart.getNonGiftCertificateAmount());
    });

    return {success: true};
}


/**
 * Authorizes a payment using a amazon payment. The payment is authorized by using the AMAZON processor only
 * and setting the order no as the transaction ID. Customizations may use other processors and custom logic to
 * authorize credit card payment.
 */
function Authorize(args) {
    var orderNo           = args.OrderNo,
        paymentInstrument = args.PaymentInstrument,
        paymentProcessor  = PaymentMgr.getPaymentMethod(paymentInstrument.getPaymentMethod()).getPaymentProcessor();

    Transaction.wrap(function() {
        paymentInstrument.paymentTransaction.transactionID = orderNo;
        paymentInstrument.paymentTransaction.paymentProcessor = paymentProcessor;
    });

    return {authorized: true};
}

/*
 * Local methods
 */
exports.Handle = Handle;
exports.Authorize = Authorize;
