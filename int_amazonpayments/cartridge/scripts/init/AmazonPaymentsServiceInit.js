/**
 * Initialize HTTP service for the Amazon Payments
 */
var svc       = require('dw/svc'),
    amazonLib = require("../lib/amazonPaymentsLib.ds");

svc.ServiceRegistry.configure("amazonPayments.service", {
    createRequest: function(svc, params) {
        var credID = "amazonPayments.cred." + dw.system.Site.getCurrent().getCustomPreferenceValue('amazonCountryCode').value.toUpperCase();
        svc.setCredentialID(credID);

        var serviceUrl      = svc.configuration.credential.URL,
            parsedUrl       = serviceUrl.split('/'),
            httpHost        = parsedUrl[2],
            httpUri         = "/" + parsedUrl[3] + "/" + parsedUrl[4],
            amazonLibObject = new amazonLib(),
            requestObject   = {};

        switch (params.action) {
            case "orderReference" :
                requestObject = amazonLibObject.createOrderReferenceRequest(params.orderReferenceID, params.addressConsentToken, httpHost, httpUri);
                break;
            case "billingAgreement" :
                requestObject = amazonLibObject.createBillingAgreementDetailsRequest(params.billingAgreementID, params.addressConsentToken, httpHost, httpUri);
                break;
            case "setOrderReferenceDetails" :
                requestObject = amazonLibObject.setOrderReferenceDetails(params.orderReferenceID, params.amount, params.currencyCode, params.platformId, params.sellerNote, params.sellerOrderId, params.storeName, httpHost, httpUri);
                break;
            case "setBillingAgreementDetails" :
                requestObject = amazonLibObject.setBillingAgreementDetails(params.billingAgreementId, params.platformId, params.sellerNote, params.customerInformation, params.storeName, httpHost, httpUri);
                break;
            case "closeOrderReference" :
                requestObject = amazonLibObject.closeOrderReferenceRequest(params.orderReferenceID, httpHost, httpUri);
                break;
            case "confirmOrderReference" :
                requestObject = amazonLibObject.confirmOrderReferenceRequest(params.orderReferenceID, httpHost, httpUri);
                break;
            case "confirmBillingAgreement" :
                requestObject = amazonLibObject.confirmBillingAgreementRequest(params.billingAgreementID, httpHost, httpUri);
                break;
            case "captureDetails" :
                requestObject = amazonLibObject.createGetDetailsRequest(params.amazonCaptureId, httpHost, httpUri);
                break;
            case "requestCapture" :
                requestObject = amazonLibObject.createCaptureRequest(params.captureInfo, httpHost, httpUri);
                break;
            case "refundDetails" :
                requestObject = amazonLibObject.getRefundDetails(params.amazonRefundId, httpHost, httpUri);
                break;
            case "refund" :
                requestObject = amazonLibObject.refund(params.options, params.sellerRefundNote, httpHost, httpUri);
                break;
            case "getAuthorizationDetails" :
                requestObject = amazonLibObject.createGetAuthorizationDetailsRequest(params.amazonAuthorizationID, httpHost, httpUri);
                break;
            case "authorize" :
                requestObject = amazonLibObject.createAuthorizeRequest(params.authorizeInfo, httpHost, httpUri);
                break;
            case "authorizeOnBillingAgreement" :
                requestObject = amazonLibObject.createAuthorizeOnBillingAgreementRequest(params.authorizeInfo, httpHost, httpUri);
                break;
            case "validateBillingAgreement" :
                requestObject = amazonLibObject.createValidateBillingAgreementDetailsRequest(params.billingAgreementId, httpHost, httpUri);
                break;
            case "closeBillingAgreement" :
                requestObject = amazonLibObject.closeBillingAgreementRequest(params.billingAgreementId, params.reason, httpHost, httpUri);
                break;
            case "createOrderReferenceForId" :
                requestObject = amazonLibObject.createOrderReferenceForIdRequest(params.billingAgreementId, httpHost, httpUri);
                break;
            default :
                break;
        }

        svc.setRequestMethod("POST");
        svc.addHeader("Content-Type", "application/x-www-form-urlencoded");
        svc.addHeader("User-Agent", "Demandware Amazon Cartridge/v1.0 (Language=Java Script)");

        return requestObject;
    },

    parseResponse: function(svc, response) {
        return response.text;
    }
});
