/**
 * Initialize HTTP service for the Amazon Customer Profile
 */
var svc = require('dw/svc');

svc.ServiceRegistry.configure("customerProfile.service", {
    createRequest: function(svc, params) {
        var credID = "customerProfile.cred." + dw.system.Site.getCurrent().getCustomPreferenceValue('amazonCountryCode').value.toUpperCase();
        svc.setCredentialID(credID);
        svc.setRequestMethod("GET");
        svc.addHeader("User-Agent", "Demandware Amazon Cartridge/v1.0 (Language=Java Script)");
        svc.addHeader("Authorization", "bearer " + params.addressConsentToken);
    },

    parseResponse: function(svc, response) {
        return response.text;
    }
});

