/**
 * Initialize HTTP service for the Amazon Token
 */
var svc       = require('dw/svc'),
    amazonLib = require("../lib/amazonPaymentsLib.ds");

svc.ServiceRegistry.configure("checkAmazonToken.service", {
    createRequest: function(svc, params) {
        var credID = "checkAmazonToken.cred." + dw.system.Site.getCurrent().getCustomPreferenceValue('amazonCountryCode').value.toUpperCase();
        svc.setCredentialID(credID);

        var serviceUrl      = svc.configuration.credential.URL,
            amazonLibObject = new amazonLib();

        svc.setURL(serviceUrl + "?access_token=" + amazonLibObject.urlEncode(params.addressConsentToken));
        svc.setRequestMethod("GET");
        svc.addHeader("User-Agent", "Amazon/build1611 (Language=Java Script; Host=jane.laptop.example.com)");
    },

    parseResponse: function(svc, response) {
        return response.text;
    }
});
