'use strict';

/**
 * This module is used to validate instant payment notification
 *
 * @module validation
 */

/* API Includes */
var HTTPClient      = require('dw/net/HTTPClient'),
    Site            = require('dw/system/Site'),
    Bytes           = require('dw/util/Bytes'),
    Signature       = require('dw/crypto/Signature'),
    XMLStreamReader = require('dw/io/XMLStreamReader'),
    Reader          = require('dw/io/Reader'),
    Encoding        = require('dw/crypto/Encoding');

/* Script Modules */
var X509Lib   = require('~/cartridge/scripts/lib/x509Lib.ds'),
    amazonLib = require('../lib/amazonPaymentsLib.ds');

/**
 * This method is used to test instant payment notification
 * @param {dw.web.HttpParameterMap} httpParameterMap - HttpParameters
 * @param {dw.system.Request} curRequest - CurrentRequest
 * @param {String} controllerName - Notifications controller name Current controller name
 * @return {Object} - controller method to be called as a result of notification Target controller; Object with information for requests
 */
function validation(httpParameterMap, curRequest, controllerName) {
    var result = {
            error: false,
            controller: null,
            notificationData: null
        },
        // include amazon library
        amazonLibObject = new amazonLib(),
        messageType     = curRequest.httpHeaders['x-amz-sns-message-type'];

    // if notification type is empty we shouldn't process request
    if (empty(messageType)) {
        result.error = true;

        return result;
    }

    //notification list
    var notification = {
            BillingAgreementNotification: 'AmazonBillingAgreementId',
            OrderReferenceNotification: 'AmazonOrderReferenceId',
            PaymentAuthorize: 'AmazonAuthorizationId',
            PaymentCapture: 'AmazonCaptureId',
            PaymentRefund: 'AmazonRefundId'
        },
        jsonData        = JSON.parse(httpParameterMap.getRequestBodyAsString()),

        // get message with xml from json
        message          = JSON.parse(jsonData.Message),
        notificationType = message.NotificationType,

        // value for the property we will need to find
        searchValue      = notification[notificationType],
        signatureVersion = jsonData.SignatureVersion,
        signature        = jsonData.Signature,

        // get url to retrieve certificate
        signingCertURL   = jsonData.SigningCertURL,
        certificate      = '',
        notificationData = message.NotificationData;

    notification = {
        searchVal: '',
        notificationType: notificationType
    };

    // we need to process just notifications
    if (messageType == 'Notification') {
        // validate the host name of signingCertURL
        var pattern      = new RegExp('^sns\.[a-zA-Z0-9\-]{3,}\.amazonaws\.com(\.cn)?$'),
            checkPattern = false,
            spl          = signingCertURL.split('//');

        if (spl[1]) {
            var spl2 = spl[1].split('/');
            if (spl2[0]) {
                var checkPattern = pattern.test(spl2[0]);
            }
        }

        if (checkPattern === false) {
            result.error = true;

            return result;
        }

        // retrieve certificate from signingCertURL
        var httpClient = new HTTPClient();
        // 15 secs
        httpClient.setTimeout(Site.getCurrent().getCustomPreferenceValue('amazonIpNotificationsTimeout'));
        // clear old declaration
        message = null;

        httpClient.open('POST', signingCertURL);
        httpClient.send();

        // get certificate
        if (httpClient.statusCode == 200) {
            certificate = httpClient.text;
        }

        // retrieve public key from x509 certificate
        var certObject = new X509Lib.X509();
        certObject.readPublicKey(certificate);
        var base64Publickey = certObject.base64PublicKey;

        //calculate string to sign
        var stringToSign        = amazonLibObject.createSNSMessageStringToSign(jsonData),
            encodedStringToSign = Encoding.toBase64(new Bytes(stringToSign));

        // validate signature with public key
        var sig = new Signature(),
            signatureVerified = sig.verifySignature(signature, encodedStringToSign, base64Publickey, 'SHA1withRSA');

        // valid signature
        if (signatureVerified) {
            // find search property from notification list
            var reader = new XMLStreamReader(new Reader(notificationData));

            while (reader.hasNext()) {
                reader.next();

                if (reader.getEventType() == 1 && reader.getLocalName() == searchValue) {
                    notification.searchVal = reader.readXMLObject().valueOf().toString();
                    break;
                }

            }

            reader.close();

            result.controller       = controllerName + '-' + notificationType;
            result.notificationData = notification;
        } else {
            result.error = true;

            return result;
        }

    } else {
        result.error = true;

        return result;
    }

    return result;
}

module.exports = {
    validation: validation
};