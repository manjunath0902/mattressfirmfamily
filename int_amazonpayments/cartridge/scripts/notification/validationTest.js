'use strict';

/**
 * This module is used to test instant payment notification
 *
 * @module validationTest
 */

/* API Includes */
var XMLStreamReader = require('dw/io/XMLStreamReader'),
    Reader = require('dw/io/Reader');

/* Script Modules */
var amazonLib = require('../lib/amazonPaymentsLib.ds');

/**
 * This method is used to test instant payment notification
 * @param {dw.web.HttpParameterMap} httpParameterMap - HttpParameters
 * @param {dw.system.Request} curRequest - CurrentRequest
 * @param {String} controllerName - Notifications controller name Current controller name
 * @return {Object} - controller method to be called as a result of notification Target controller; Object with information for requests
 */
function validation(httpParameterMap, curRequest, controllerName) {
    var result = {
            error: false,
            controller: null,
            notificationData: null
        },
        // include amazon library
        amazonLibObject = new amazonLib(),
        messageType = curRequest.httpHeaders['x-amz-sns-message-type'];

    // if notification type is empty we shouldn't process request
    if (empty(messageType)) {
        result.error = true;

        return result;
    }

    //notification list
    var notification = {
            BillingAgreementNotification: 'AmazonBillingAgreementId',
            OrderReferenceNotification: 'AmazonOrderReferenceId',
            PaymentAuthorize: 'AmazonAuthorizationId',
            PaymentCapture: 'AmazonCaptureId',
            PaymentRefund: 'AmazonRefundId'
        },
        jsonData = JSON.parse(httpParameterMap.getRequestBodyAsString()),
        // get message with xml from json
        message = JSON.parse(jsonData.Message),
        notificationType = message.NotificationType,
        // value for the property we will need to find
        searchValue = notification[notificationType],
        signatureVersion = jsonData.SignatureVersion,
        signature = jsonData.Signature,
        // get url to retrieve certificate
        signingCertURL = jsonData.SigningCertURL,
        certificate = '',
        notificationData = message.NotificationData;

    notification = {
        searchVal: '',
        notificationType: notificationType
    };

    // we need to process just notifications
    if (messageType == 'Notification') {
        var signatureVerified = true;

        // valid signature
        if (signatureVerified) {
            // find search property from notification list
            var reader = new XMLStreamReader(new Reader(notificationData));

            while (reader.hasNext()) {
                reader.next();

                if (reader.getEventType() == 1 && reader.getLocalName() == searchValue) {
                    notification.searchVal = reader.readXMLObject().valueOf().toString();
                    break;
                }
            }

            reader.close();

            result.controller = controllerName + '-' + notificationType;
            result.notificationData = notification;
        } else {
            result.error = true;

            return result;
        }

    } else {
        result.error = true;

        return result;
    }

    return result;
}

module.exports = {
    validation: validation
};