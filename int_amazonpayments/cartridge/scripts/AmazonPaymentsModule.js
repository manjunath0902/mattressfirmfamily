'use strict';

/**
 * This module provides often-needed helper methods for Amazon Payments.
 *
 * @module AmazonPaymentsModule
 */

/* API Includes */
var OrderMgr        = require('dw/order/OrderMgr'),
    ArrayList       = require('dw/util/ArrayList'),
    Logger          = require('dw/system/Logger'),
    Transaction     = require('dw/system/Transaction'),
    Site            = require('dw/system/Site'),
    CurrentSite     = Site.getCurrent(),
    ContentMgr      = require('dw/content/ContentMgr'),
    ServiceRegistry = require('dw/svc/ServiceRegistry'),
    SecureRandom    = require('dw/crypto/SecureRandom'),
    Encoding        = require('dw/crypto/Encoding'),
    Order           = require('dw/order/Order'),
    Money           = require('dw/value/Money'),
    Resource        = require('dw/web/Resource');

/* Script Modules */
var amazonLib = require("../scripts/lib/amazonPaymentsLib.ds");

/**
 * Used to check if some of the products have already been captured.
 * @param {Object} apiData - The options for the capture operation
 * @return {Object} result - contains error and alreadyCaptured attributes
 */
function checkCaptureProducts(apiData) {
    var result = {
        error: false,
        alreadyCaptured: false
    };

    if (!apiData) {
        result.error = true;
        return result;
    }

    var order = OrderMgr.queryOrder('custom.amazonAuthorizationID = {0}', apiData.AmazonAuthorizationId);

    if (!empty(order) && order.custom.amazonSplitOrder) {
        // check the captured products
        var productIterator,
            product,
            capturedProducts = new ArrayList(order.custom.amazonCapturedProducts),
            requestProducts  = new ArrayList(apiData.CapturedProducts);

        productIterator  = requestProducts.iterator();

        while (productIterator.hasNext()) {
            product = productIterator.next();

            if (capturedProducts.contains(product.toString())) {
                result.alreadyCaptured = true;
                break;
            }
        }
    }

    return result;
}

/**
 * Used to make a call to RequestCapture service
 * @param {Object} apiData - The options for the capture operation
 * @return {Object} result - contains error, result (The capture info) and response(The full response xml string) attributes
 */
function requestCaptureAction(apiData) {
    var result = {
        error: false,
        result: null,
        response: null
    };

    if (!apiData) {
        result.error = true;
        return result;
    }

    var logger = Logger.getLogger('amazonPayments', 'amazonPayments'),
        // get order reference service
        service = ServiceRegistry.get('amazonPayments.service'),
        captureInfo = {
            AmazonAuthorizationId: apiData.AmazonAuthorizationId,
            CaptureAmount: apiData['CaptureAmount.Amount'],
            CaptureCurrencyCode: apiData['CaptureAmount.CurrencyCode'],
            CaptureReferenceId: apiData.CaptureReferenceId,
            SellerCaptureNote: apiData.SellerCaptureNote || '',
            OrderTotalAmount: apiData['OrderReferenceAttributes.OrderTotal.Amount'] || '',
            OrderTotalCurrencyCode: apiData['OrderReferenceAttributes.OrderTotal.CurrencyCode'] || '',
            SellerOrderId: apiData['OrderReferenceAttributes.SellerOrderAttributes.SellerOrderId'] || '',
            StoreName: apiData['OrderReferenceAttributes.SellerOrderAttributes.StoreName'] || ''
        },
        params = {
            'captureInfo': captureInfo,
            'action': 'requestCapture'
        },
        serviceResult   = service.call(params),
        amazonLibObject = new amazonLib();

    // check service status
    if (serviceResult.status == 'OK') {
        result.result   = amazonLibObject.takeCaptureInfoFromResponse(serviceResult.object);
        result.response = serviceResult.object;
    } else if (serviceResult.status == 'ERROR') {
        logger.error('Http Error ' + serviceResult.error);
        result.response = serviceResult.errorMessage;
        result.error    = true;
    } else {
        // the timeout was exceeded
        logger.error('Http Timeout when calling RequestCapture API');
        result.response = serviceResult.errorMessage;
        result.error    = true;
    }

    return result;
}

/**
 * Generate a random string of the specified value.
 * @param {Number} length - The length of the random string
 * @return {String} str - The generated string
 */
function getRandomString(length) {
    var length       = length,
        random       = new SecureRandom(),
        randomBytes  = random.generateSeed(length),
        randomString = Encoding.toBase64(randomBytes),
        str          = null;

    // base64 returns a-zA-Z0-9+/=
    str = randomString.substr(0, length)
        .replace('/', 'a', 'g')
        .replace('+', 'C', 'g')
        .replace('=', 'Q', 'g');

    return str;
}

/**
 * Used to prepare object for Authorize operation
 * @param {dw.order.Order} order - Order
 * @param {String} orderReferenceID - Order reference ID
 * @param {String} billingAgreementID - Billing agreement ID
 * @param {String} randomString - Random Reference ID
 * @return {Object} authObject - Authorized Object
 */
function apPrepareAuthorizeObject(order, orderReferenceID, billingAgreementID, randomString) {
    var authObject = {},
        orderAmount;

    // total order amount
    if (!empty(order.getTotalGrossPrice())) {
        orderAmount = order.getTotalGrossPrice();
    } else {
        orderAmount = order.getAdjustedMerchandizeTotalPrice(true).add(order.giftCertificateTotalPrice);
    }

    // get seller note from site preference content asset
    var sellerNoteAssetID = CurrentSite.getCustomPreferenceValue('amazonSellerAuthorizationNote');

    if (!empty(sellerNoteAssetID)) {
        var asset = ContentMgr.getContent(sellerNoteAssetID);

        if (!empty(asset) && !empty(asset.custom.body)) {
            authObject['SellerAuthorizationNote'] = asset.custom.body.toString();
        }
    }

    // prepare object
    if (orderReferenceID !== 'null' && (empty(session.forms.apShippingForm.consentStatus.value) || session.forms.apShippingForm.consentStatus.value == 'false')) {
        authObject['AmazonOrderReferenceId'] = orderReferenceID;
    } else {
        authObject['AmazonBillingAgreementId'] = billingAgreementID;
    }

    authObject['AuthorizationAmount.Amount']       = orderAmount.getValue().toString();
    authObject['AuthorizationAmount.CurrencyCode'] = orderAmount.getCurrencyCode();
    // reference id will be orderNo-randomString
    authObject['AuthorizationReferenceId']         = order.orderNo + '-' + randomString;

    return authObject;
}

/**
 * Check if some of the products have already been authorized.
 * @param {dw.order.Order} order
 * @param {Object} apiData
 * @return {Object} result - contains error and alreadyAuthorized attributes
 */
function checkAuthorizationProducts(order, apiData) {
    var result = {
        error: false,
        alreadyAuthorized: false
    };

    if (empty(apiData)) {
        result.error = true;
        return result;
    }

    if (order.custom.amazonSplitOrder) {
        //Check the Authorized products
        var productIterator,
            product,
            authorizedProducts = new ArrayList(order.custom.amazonAuthorizedProducts),
            requestProducts    = new ArrayList(apiData.AuthorizationProducts);

        productIterator = requestProducts.iterator();

        while (productIterator.hasNext()) {
            product = productIterator.next();

            if (authorizedProducts.contains(product.toString())) {
                result.alreadyAuthorized = true;
                break;
            }
        }
    }

    return result;
}

/**
 * Make a call to Authorize service
 * @param {Boolean} captureNow - Flag to trigger immediate capture
 * @param {Object} customAmount - If not empty it will be used instead of the defined amount.
 * @param {Boolean} customAPI - Flag to specify if custom API is used
 * @param {Object} options - The options for the Authorize operation
 * @param {dw.order.Order} order - Order
 * @return {Object} result - contains error, result(Authorized result), response(The full response xml string), capture(Flag to trigger immediate capture)
 */
function authorizeAction(captureNow, customAmount, customAPI, options, order) {
    var result = {
            error: false,
            result: null,
            response: null,
            capture: false
        },
        logger = Logger.getLogger('AuthorizeService'),
        amount = options['AuthorizationAmount.Amount'];

    if (!empty(customAmount)) {
        amount = customAmount;
    }

    // authorize and capture behaviour
    if (!customAPI && CurrentSite.getCustomPreferenceValue('amazonAuthorizeAndCapture')) {
        captureNow = true;
    } else {
        if (customAPI && options['CaptureNow'] && options['CaptureNow'] == 'true') {
            captureNow = true;
        }
    }

    var service = ServiceRegistry.get('amazonPayments.service'),
        authorizeInfo = {
            AuthorizationAmount: amount,
            AuthorizationCurrencyCode: options['AuthorizationAmount.CurrencyCode'],
            AuthorizationReferenceId: options.AuthorizationReferenceId,
            SellerAuthorizationNote: options.SellerAuthorizationNote || '',
            CaptureNow: captureNow
        };

    if (options.AmazonBillingAgreementId) {
        authorizeInfo.AmazonBillingAgreementId = options.AmazonBillingAgreementId;
    } else {
        authorizeInfo.AmazonOrderReferenceId = options.AmazonOrderReferenceId;
    }

    var params = {
            'authorizeInfo': authorizeInfo,
            'action': options.AmazonBillingAgreementId ? 'authorizeOnBillingAgreement' : 'authorize'
        },
        serviceResult       = service.call(params),
        amazonLibObject     = new amazonLib(),
        authorizationObject = {};

    if (!empty(serviceResult.object)) {
        authorizationObject = amazonLibObject.takeAuthorizationStatus(serviceResult.object);

        // update order custom attributes with amazonAuthorizationStatus, amazonAuthorizationID
        Transaction.wrap(function () {
            order.custom.amazonAuthorizationStatus = authorizationObject.authorizationStatus;
            // amazonAuthorizationID, string of id's separated by coma's
            if (!empty(order.custom.amazonAuthorizationID)) {
                order.custom.amazonAuthorizationID = order.custom.amazonAuthorizationID + ',' + authorizationObject.amazonAuthorizationId;
            } else {
                order.custom.amazonAuthorizationID = authorizationObject.amazonAuthorizationId;
            }
        });
    }

    Transaction.wrap(function () {
        if (options.AmazonBillingAgreementId) {
            // store billing agreement ID at order level
            order.custom.amazonBillingAgreementID = authorizeInfo.AmazonBillingAgreementId;
        } else {
            // store order reference ID at order level
            order.custom.amazonOrderReferenceID = authorizeInfo.AmazonOrderReferenceId;
        }
    });

    // store capture id if it's available.
    // Capture Id is available in case CaptureNow = true
    if (!empty(authorizationObject.amazonCaptureID)) {
        Transaction.wrap(function () {
            if (!empty(order.custom.amazonCaptureID)) {
                order.custom.amazonCaptureID = order.custom.amazonCaptureID + ',' + authorizationObject.amazonCaptureID;
            } else {
                order.custom.amazonCaptureID = authorizationObject.amazonCaptureID;
            }
        });
    }

    /* check service status */
    if (serviceResult.status == 'OK') {
        result.result   = authorizationObject;
        result.response = serviceResult.object;
    } else if (serviceResult.status == 'ERROR') {
        logger.error('Http Error ' + serviceResult.error);
        result.result   = serviceResult.error;
        result.response = serviceResult.errorMessage;
        result.error    = true;
        return result;
    } else {
        // the timeout was exceeded
        logger.error('Http Timeout when calling Authorize service');
        result.result   = serviceResult.error;
        result.response = serviceResult.errorMessage;
        result.error    = true;
        return result;
    }

    result.capture = captureNow;

    return result;
}

/**
 * Update the authorization status based on the authorization response.
 * @param {Boolean} captureNow -
 * @param {dw.order.Order} order - Order
 * @param {Object} customAmount - If not empty it will be used instead of the defined amount.
 * @param {Object} apiData
 * @return {Object} result - error status
 */
function updateAuthorizationAttributes(captureNow, order, customAmount, apiData) {
    var result = {
        error: false
    };

    if (!apiData) {
        result.error = true;
        return result;
    }

    // update order statuses in case captureNow = true
    if (captureNow) {
        var authorizedAmount = new Number(apiData['AuthorizationAmount.Amount']);
        if (!empty(customAmount)) {
            authorizedAmount = new Number(customAmount);
        }
        var currentAmount  = new Number(order.custom.amazonAuthorizedAmount),
            capturedAmount = new Number(order.custom.amazonCapturedAmount);

        capturedAmount += authorizedAmount;

        // Update Captured amount
        Transaction.wrap(function () {
            order.custom.amazonCapturedAmount = capturedAmount;
        });
    }

    if (order.custom.amazonSplitOrder) {
        //Update the Authorized amount
        var authorizedAmount = new Number(apiData['AuthorizationAmount.Amount']);

        if (!empty(customAmount)) {
            authorizedAmount = new Number(customAmount);
        }

        var currentAmount = new Number(order.custom.amazonAuthorizedAmount);
        currentAmount += authorizedAmount;
        Transaction.wrap(function () {
            order.custom.amazonAuthorizedAmount = currentAmount;
        });

        //Update the Authorized products
        if (!empty(apiData.AuthorizationProducts)) {
            var productList = new ArrayList(order.custom.amazonAuthorizedProducts),
                authorizedProducts = new ArrayList(apiData.AuthorizationProducts);
            productList.addAll(authorizedProducts);
            Transaction.wrap(function () {
                order.custom.amazonAuthorizedProducts = productList;
                // save capture products ids in case CaptureNow = true
                if (captureNow) {
                    order.custom.amazonCapturedProducts = productList;
                }
            });
        } else {
            var productList = new ArrayList(),
                LineItems   = order.getAllProductLineItems(),
                product,
                item;
            // on split orders, adds the products from the first Authorization to the Amazon Authorized Products
            for (var i = 0; i < LineItems.length; i++) {
                item = LineItems[i];

                product = item.getProduct();

                if (empty(product)) {
                    product = item.parent.getProduct();
                }

                if (!product.custom.amazonSplitPayment) {
                    productList.add(product.ID)
                }
            }
            Transaction.wrap(function () {
                order.custom.amazonAuthorizedProducts = productList;
                // save capture products ids in case CaptureNow = true
                if (captureNow) {
                    order.custom.amazonCapturedProducts = productList;
                }
            });
        }
    }

    return result;
}

/**
 * Update the authorization status based on the authorization response.
 * @param {Object} authorizationObject
 * @param {Object} APIData
 * @param {dw.order.Order} order
 * @return {Object}
 */
function updateAuthorizationStatus(authorizationObject, APIData, order) {
    var result = {
        error: false,
        sendMail: false
    };

    // check if we have authorization status
    if (empty(authorizationObject) || empty(authorizationObject.authorizationStatus)) {
        result.error = true;
        return result;
    }
    var authorizationStatus = authorizationObject.authorizationStatus;

    // Amazon Status:
    //  Pending - In asynchronous mode, all Authorization objects are in the Pending state for 30 seconds after you submit the Authorize request.
    //            In synchronous mode, an Authorization object cannot be in the Pending state.
    //  Declined - The authorization has been declined by Amazon.
    //  Open - In asynchronous mode, the Authorization object moves to the Open state after remaining in the Pending state for 30 seconds.
    //         In synchronous mode, the Authorization object immediately moves to the Open state.
    //  Closed - The authorization can be closed by calling the CloseAuthorization operation, by calling the CancelOrderReference operation, or it can be closed by Amazon.
    //           You cannot request captures against an authorization that is in the Closed state.
    //           In the event of a partial capture, the remaining amount will be credited back to the order reference.


    // DW Status:
    //  ORDER_STATUS_NEW
    //  ORDER_STATUS_OPEN
    //  ORDER_STATUS_COMPLETED
    //  ORDER_STATUS_CANCELLED
    //  ORDER_STATUS_REPLACED

    var status           = Order.ORDER_STATUS_CANCELLED,
        orderTotal       = order.getTotalGrossPrice().value,
        authorizedAmount = order.custom.amazonAuthorizedAmount;

    switch (authorizationStatus) {
        case 'Pending':
            status = Order.ORDER_STATUS_OPEN;
            break;
        case 'Declined':
            if (!empty(authorizationObject.reasonCode)) {
                switch (authorizationObject.reasonCode) {
                    case 'InvalidPaymentMethod':
                        status          = Order.ORDER_STATUS_OPEN;
                        result.sendMail = true;
                        break;
                    case 'AmazonRejected':
                        status = Order.ORDER_STATUS_CANCELLED;
                        break;
                    case 'ProcessingFailure':
                        status = Order.ORDER_STATUS_CANCELLED;
                        break;
                    case 'TransactionTimedOut':
                        status = Order.ORDER_STATUS_OPEN;
                        break;
                }
                break;
            } else {
                status = Order.ORDER_STATUS_CANCELLED;
                break;
            }
        case 'Open':
            status = Order.ORDER_STATUS_OPEN;
            break;
        case 'Closed':
            status = Order.ORDER_STATUS_OPEN;
            break;
    }

    if (authorizedAmount != orderTotal && order.custom.amazonSplitOrder) {
        status = Order.ORDER_STATUS_OPEN;
    }

    try {
        Transaction.wrap(function () {
            order.setStatus(status);
            order.custom.amazonAuthorizationStatus = authorizationStatus;
        });
    } catch (e) {
        result.error = true;
        return result;
    }

    if (order.custom.amazonDeferredOrder == true && authorizationStatus == 'Open') {
        Transaction.wrap(function () {
            order.addNote('Authorize', 'Your deferred order has been authorized.');
        });
    } else if (order.custom.amazonSplitOrder) {
        Transaction.wrap(function () {
            order.addNote('Authorize', 'The order was authorized. Authorize amount ' + APIData['AuthorizationAmount.Amount'].toString() + ' ' + APIData['AuthorizationAmount.CurrencyCode'].toString());
        });
    }

    return result;
}

/**
* Prepare the api data based on the gift certificates for the authorize service.
*
* @param {dw.order.Order} order - The order containing the gift certificates
* @param {String} orderReferenceID - Order reference ID
* @param {String} billingAgreementID - Billing agreement ID
* @param {String} randomString - Random Reference ID
* @return {Object}
*/
function apCaptureGiftCertificates(order, orderReferenceID, billingAgreementID, randomString) {
    var isGiftCapture     = false,
        authorizeData     = {},
        authObject        = {},
        // calculate any gift certificates added to the cart
        amount            = order.getGiftCertificateTotalPrice(),
        // get seller note from site preference content asset
        sellerNoteAssetID = CurrentSite.getCustomPreferenceValue('amazonSellerAuthorizationNote');

    if (!empty(sellerNoteAssetID)) {
        var asset = ContentMgr.getContent(sellerNoteAssetID);
        if (!empty(asset) && !empty(asset.custom.body)) {
            authObject['SellerAuthorizationNote'] = asset.custom.body.toString();
        }
    }

    // prepare object

    if (orderReferenceID !== 'null' && (empty(session.forms.apShippingForm.consentStatus.value) || session.forms.apShippingForm.consentStatus.value == 'false')) {
        authObject['AmazonOrderReferenceId'] = orderReferenceID;
    } else {
        authObject['AmazonBillingAgreementId'] = billingAgreementID;
    }

    authObject['AuthorizationAmount.Amount']       = amount.getValue().toString();
    authObject['AuthorizationAmount.CurrencyCode'] = amount.getCurrencyCode();
    // reference id will be orderNo-randomString
    authObject['AuthorizationReferenceId']         = order.orderNo + '-' + randomString;

    isGiftCapture = true;

    return {
         isGiftCapture : isGiftCapture,
         authorizeData : authObject
    };
}

/**
* Update the authorization status based on the authorization response.
*
* @param {Object} request
* @param {dw.order.Order} currentOrder
* @return {Boolean}
*/
function updateAuthorizationGiftCert(request, currentOrder) {
    if (empty(request)) {
        return false;
    }

    var authorizedAmount = new Number(request['AuthorizationAmount.Amount']),
        currentAmount    = new Number(currentOrder.custom.amazonAuthorizedAmount),
        capturedAmount   = new Number(currentOrder.custom.amazonCapturedAmount);

    capturedAmount += authorizedAmount;

    // Update the Authorized & Captured amount
    Transaction.wrap(function() {
        if (currentOrder.custom.amazonSplitOrder) {
            currentAmount += authorizedAmount;
            currentOrder.custom.amazonAuthorizedAmount = currentAmount;
        }

        currentOrder.custom.amazonCapturedAmount = capturedAmount;
    });

    return true;
}

/**
* Check if the current PaymentInstruments include the amazon payment.
*
* @param {dw.util.Collection} paymentInstruments - A collection of the current payment instruments
* @return {Boolean} isAmazonPaymentMethod
*/
function checkAmazonPayment(paymentInstruments) {
    var isAmazonPaymentMethod = false,
        paymentInstruments    = paymentInstruments.iterator(),
        instrument;

    while (paymentInstruments.hasNext()) {
        instrument = paymentInstruments.next();

        if (instrument.paymentMethod == 'AMAZON_PAYMENTS') {
            isAmazonPaymentMethod = true;
            break;
        }
    }
    return isAmazonPaymentMethod;
}

/**
* Check if the gift certificates cover the full, partial or none of the order amount
*
* @param {dw.util.Collection} paymentInstruments - The payment instruments that contain the gift certificates
* @param {Number} amount - The full order amount
* @return {String} status - The status of the order cover. It can be: full, partial, none
*/
function getGiftCertificateStatus(paymentInstruments, amount) {
    var status             = 'none',
        paymentInstruments = paymentInstruments.iterator(),
        instrument,
        hasGiftCertificate = false;

    while (paymentInstruments.hasNext()) {
        instrument = paymentInstruments.next();

        if (instrument.paymentMethod == instrument.METHOD_GIFT_CERTIFICATE) {
            hasGiftCertificate = true;
            break;
        }
    }

    // the amount is 0 if the gift certificate covered all the order amount
    if (hasGiftCertificate && (amount > 0)) {
        status = 'partial';
    } else if (hasGiftCertificate) {
        status = 'full';
    }

    return status;
}

/**
* Make a call to CheckToken service
*
* @param {String} addressConsentToken
* @return {Boolean} true if service result status is OK
*/
function checkTokenAction(addressConsentToken) {
    var logger = Logger.getLogger('CheckToken'),
        service = ServiceRegistry.get('checkAmazonToken.service'),
        params = {
            'addressConsentToken' : addressConsentToken
        },
        result = service.call(params);

    /* check service status */
    if (result.status == 'OK') {
        try {
            var resultObj = JSON.parse(result.object);

            // check if the access token belongs to up
            if (resultObj.aud != CurrentSite.getCustomPreferenceValue('amazonClientID')) {
                logger.error('Invalid access token.');
                return false;
            }
        } catch (e) {
            logger.error('Invalid JSON on the CheckToken service');
            return false;
        }
    } else if (result.status == 'ERROR') {
        logger.error('Http Error ' + result.error);
        return false;
    } else {
        // the timeout was exceeded
        logger.error('Http Timeout when calling CheckToken service');
        return false;
    }

    return true;
}

/**
* Make a call to GetOrderReference service
*
* @param {String} orderReferenceID - Order reference ID required for service call
* @param {dw.order.Order} addressConsentToken - AddressConsentToken is optional parameter
* @return {Object} resultObj
*/
function orderReferenceAction(orderReferenceID, addressConsentToken) {
    var logger = Logger.getLogger('amazonPayments', 'amazonPayments'),
        resultObj = {
            error: false
        };

    // get order reference service
    var service = ServiceRegistry.get('amazonPayments.service'),
        params = {
            'orderReferenceID'    : orderReferenceID,
            'addressConsentToken' : addressConsentToken,
            'action'              : 'orderReference'
        },
        result = service.call(params),
        amazonLibObject = new amazonLib();

    /* check service status */
    if (result.status == 'OK') {
        resultObj.address          = amazonLibObject.takeAddressFromResponse(result.object);
        resultObj.apAddressBilling = amazonLibObject.takeAddressBillingFromResponse(result.object);
        resultObj.constraints      = amazonLibObject.takeConstraintsFromResponse(result.object);
        resultObj.status           = amazonLibObject.takeReasonCodeFromResponse(result.object);
        resultObj.response         = result.object;
    } else if (result.status == 'ERROR') {
        logger.error('Http Error ' + result.error);
        resultObj.error    = true;
        resultObj.response = result.errorMessage;
    } else {
        // the timeout was exceeded
        logger.error('Http Timeout when calling GetOrderReference service');
        resultObj.error    = true;
        resultObj.response = result.errorMessage;
    }

    return resultObj;
}

/**
* Make a call to GetBillingAgreementDetails service
*
* @param {String} billingAgreementID - Billing Agreement ID required for service call
* @param {dw.order.Order} addressConsentToken - AddressConsentToken is optional parameter
* @return {Object} resultObj
*/
function billingAgreementAction(billingAgreementID, addressConsentToken) {
    var logger = Logger.getLogger('amazonPayments', 'amazonPayments'),
        resultObj = {
            error: false
        };

    // get order reference service
    var service = ServiceRegistry.get('amazonPayments.service'),
        params = {
            'billingAgreementID'  : billingAgreementID,
            'addressConsentToken' : addressConsentToken,
            'action'              : 'billingAgreement'
        },
        result = service.call(params),
        amazonLibObject = new amazonLib();

    /* check service status */
    if (result.status == 'OK') {
        resultObj.address          = amazonLibObject.takeAddressFromResponse(result.object);
        resultObj.apAddressBilling = amazonLibObject.takeAddressBillingFromResponse(result.object);
        resultObj.constraints      = amazonLibObject.takeConstraintsFromResponse(result.object);
        resultObj.status           = amazonLibObject.takeReasonCodeFromResponse(result.object);
        resultObj.remainingBalance = amazonLibObject.takeBillingAgreementRemainingBalanceFromResponse(result.object);
        resultObj.response         = result.object;
    } else if (result.status == 'ERROR') {
        logger.error('Http Error ' + result.error);
        resultObj.error    = true;
        resultObj.response = result.errorMessage;
    } else {
        // the timeout was exceeded
        logger.error('Http Timeout when calling GetBillingAgreementDetails service');
        resultObj.error    = true;
        resultObj.response = result.errorMessage;
    }

    return resultObj;
}

/**
* Update the order status based on the order reference response.
*
* @param {Object} status - Amazon Order Reference Status
* @param {dw.order.Order} order - Current order
* @return {Boolean} true if status or order is not empty
*/
function updateOrderReferenceStatus(status, currentOrder) {
    if (empty(status) || empty(currentOrder)) {
        return false;
    }

    // Amazon Order Reference Status:
    //  Draft - An Order Reference object is in the Draft state prior to be being confirmed by calling the ConfirmOrderReference operation.
    //  Open - An Order Reference object moves to the Open state after it is confirmed by calling the ConfirmOrderReference operation.
    //  Suspended - An Order Reference object moves to the Suspended state when there are problems with the payment method
    //  Canceled - The Order Reference object can be canceled by calling the CancelOrderReference operation or it can be canceled by Amazon.
    //  Closed - The Order Reference object can be closed by calling the CloseOrderReference operation or it can be closed by Amazon.

    // DW Status:
    //  ORDER_STATUS_NEW
    //  ORDER_STATUS_OPEN
    //  ORDER_STATUS_COMPLETED
    //  ORDER_STATUS_CANCELLED
    //  ORDER_STATUS_REPLACED

    var orderStatus = currentOrder.status.value;

    switch (status.orderReferenceStatus) {
        case 'Canceled':
            orderStatus = Order.ORDER_STATUS_CANCELLED;
            break;
    }

    try {
        Transaction.wrap(function() {
            currentOrder.setStatus(orderStatus);
            currentOrder.custom.amazonOrderReferenceStatus = status.orderReferenceStatus;
        });
    } catch (e) {
        return false;
    }

    return true;
}

/**
* Update the order status based on the GetBillingAgreementDetails response.
*
* @param {Object} status - Amazon Billing Agreement Status
* @param {dw.order.Order} order - Current order
* @return {Boolean} true if status or order is not empty
*/
function updateBillingAgreementStatus(status, currentOrder) {
    if (empty(status) || empty(currentOrder)) {
        return false;
    }

    var orderStatus = currentOrder.status.value;

    switch (status.billingAgreementStatus) {
        case 'Canceled':
            orderStatus = Order.ORDER_STATUS_CANCELLED;
            break;
    }

    try {
        Transaction.wrap(function() {
            currentOrder.setStatus(orderStatus);
            currentOrder.custom.amazonBillingAgreementStatus = status.billingAgreementStatus;
        });
    } catch (e) {
        return false;
    }

    return true;
}

/**
* Copies shipping address from Amazon to Demandware billing address form and current email to billingAddress.email form
*
* @param {dw.order.Basket} basket - Basket to copy the address
* @param {Object} billingAddressAmazon - The data grabbed from amazon
*/
function apCopyShippingAddressToForm(basket, billingAddressAmazon, billingAddressForm, emailForm, amazonForm) {
    //use billing address from GetOrderReferenceDetails API (if exists)
    var shippingAddress = basket.defaultShipment.shippingAddress;

    // if exists billing from the GetOrderReferenceDetails API use it
    if (!empty(billingAddressAmazon)) {
        // copy the address attributes
        //The shipping address for DE/AT is in the address2 row. So copy it, as we use the shipping address in the address1.
        if (billingAddressAmazon.countryCode.value == 'DE' || billingAddressAmazon.countryCode.value == 'AT') {
            if (billingAddressAmazon.address2 != '' && empty(billingAddressAmazon.address1)) {
                billingAddressForm.address1.value = billingAddressAmazon.address2;
            }
        } else {
            billingAddressForm.address1.value = billingAddressAmazon.address1;
            billingAddressForm.address2.value = billingAddressAmazon.address2;
        }

        // cut parts of name
        var fullName, firstName, lastName;

        if (!empty(billingAddressAmazon.fullName)) {
            fullName  = billingAddressAmazon.fullName.split(' ');
            firstName = fullName[0];
            fullName  = fullName.slice(1);
            lastName  = fullName.join(' ');
        }

        billingAddressForm.firstName.value    = firstName;
        billingAddressForm.lastName.value     = lastName;
        billingAddressForm.city.value         = billingAddressAmazon.city;
        billingAddressForm.postal.value       = billingAddressAmazon.postalCode;
        billingAddressForm.phone.value        = billingAddressAmazon.phone;
        billingAddressForm.states.state.value = billingAddressAmazon.stateCode;
        billingAddressForm.country.value      = billingAddressAmazon.countryCode;

    } else if (!empty(shippingAddress)) {
        // copy the address attributes
        //The shipping address for DE/AT is in the address2 row. So copy it, as we use the shipping address in the address1.
        if (shippingAddress.countryCode.value == 'DE' || shippingAddress.countryCode.value == 'AT') {
            if (shippingAddress.address2 != '' && empty(shippingAddress.address1)) {
                billingAddressForm.address1.value = shippingAddress.address2;
            }
        } else {
            billingAddressForm.address1.value = shippingAddress.address1;
            billingAddressForm.address2.value = shippingAddress.address2;
        }

        /*billingAddressForm.firstName.value    = shippingAddress.firstName;
        billingAddressForm.lastName.value     = shippingAddress.lastName;*/
        billingAddressForm.firstName.value = session.forms.singleshipping.shippingAddress.addressFields.firstName.value;
        billingAddressForm.lastName.value = session.forms.singleshipping.shippingAddress.addressFields.lastName.value;
        billingAddressForm.city.value         = shippingAddress.city;
        billingAddressForm.postal.value       = shippingAddress.postalCode;
        billingAddressForm.phone.value        = shippingAddress.phone;
        billingAddressForm.states.state.value = shippingAddress.stateCode;
        billingAddressForm.country.value      = shippingAddress.countryCode.value;
    }
    emailForm.emailAddress.value = amazonForm.email.value;

    return;
}

/**
* Make a call to ConfirmOrderReference service
*
* @param {String} orderReferenceID
* @return {Object} resultObj
*/
function confirmOrderReferenceAction(orderReferenceID) {
    var logger = Logger.getLogger('ConfirmOrderRef'),
        service = ServiceRegistry.get('amazonPayments.service'),
        params = {
            'orderReferenceID' : orderReferenceID,
            'action'           : 'confirmOrderReference'
        },
        result = service.call(params),
        resultObj = {
            error: false
        };

    /* check service status */
    if (result.status == 'OK') {
        resultObj.confirmOrderReferenceResult = result;
    } else if (result.status == 'ERROR') {
        logger.error('Http Error ' + result.error);
        resultObj.error                       = true;
        resultObj.confirmOrderReferenceResult = result.error;
    } else {
        // the timeout was exceeded
        logger.error('Http Timeout when calling ConfirmOrderReference API');
        resultObj.error                       = true;
        resultObj.confirmOrderReferenceResult = result.error;
    }

    return resultObj
}

/**
* Make a call to CreateOrderReferenceForId service
*
* @param {String} billingAgreementID
* @return {Object} resultObj
*/
function createOrderReferenceForIdAction(billingAgreementID) {
    var logger          = Logger.getLogger('CreateOrderReferenceForId'),
        service         = ServiceRegistry.get('amazonPayments.service'),
        params = {
            'billingAgreementId' : billingAgreementID,
            'action'             : 'createOrderReferenceForId'
        },
        result          = service.call(params),
        amazonLibObject = new amazonLib(),
        resultObj = {
            error: false
        };

    /* check service status */
    if (result.status == 'OK') {
        resultObj.createOrderReferenceForIdResult = amazonLibObject.takeOrderReferenceIdFromResponse(result.object);
    } else if (result.status == 'ERROR') {
        logger.error('Http Error ' + result.error);
        resultObj.error                           = true;
        resultObj.createOrderReferenceForIdResult = result.error;
    } else {
        // the timeout was exceeded
        logger.error('Http Timeout when calling CreateOrderReferenceForId API');
        resultObj.error                           = true;
        resultObj.createOrderReferenceForIdResult = result.error;
    }

    return resultObj;
}

/**
* Make a call to ConfirmBillingAgreement service
*
* @param {String} billingAgreementID
* @return {Object} resultObj
*/
function confirmBillingAgreementAction(billingAgreementID) {
    var logger = Logger.getLogger('ConfirmBillingAgreement'),
        service = ServiceRegistry.get('amazonPayments.service'),
        params = {
            'billingAgreementID' : billingAgreementID,
            'action'             : 'confirmBillingAgreement'
        },
        result = service.call(params),
        resultObj = {
            error: false
        };

    /* check service status */
    if (result.status == 'OK') {
        resultObj.confirmBillingAgreementResult = result;
    } else if (result.status == 'ERROR') {
        logger.error('Http Error ' + result.error);
        resultObj.error = true;
        resultObj.confirmBillingAgreementResult = result.error;
    } else {
        // the timeout was exceeded
        logger.error('Http Timeout when calling ConfirmBillingAgreement API');
        resultObj.error = true;
        resultObj.confirmBillingAgreementResult = result.error;
    }

    return resultObj
}

/**
* Determine if the current authorization was valid.
*
* @param {Object} authorizationObject
* @return {Object}
*/
function checkValidAuth(authorizationObject) {
    // InvalidPaymentMethod
    // - Display an error message to notify the buyer that the payment method they selected was declined
    // - Redisplay the Amazon Payment method widget with the same order reference id
    // - Do NOT call the SetOrderReferenceDetails
    // AmazonRejected - Fail order
    // ProcessingFailure - Ask the buyer to replace the order in a few minutes
    // TransactionTimedOut - retry the transaction in the async mode

    var result = {
            authError: null
        };

    // check if we have authorization status
    if (empty(authorizationObject) || empty(authorizationObject.authorizationStatus)) {
        result.error = true;
        return result;
    }

    var authorizationStatus = authorizationObject.authorizationStatus,
        reasonCode          = authorizationObject.reasonCode,
        asyncMode           = CurrentSite.getCustomPreferenceValue('amazonAsynchronous');

    // reset the flags
    session.custom.amzForceAsync = null;

    if (authorizationStatus == 'Declined') {
        if (!empty(reasonCode)) {

            switch (reasonCode) {
                case 'InvalidPaymentMethod':
                    result.error = true;
                    result.authError = {
                        code: reasonCode,
                        page: 'COBilling-Start',
                        message: Resource.msg('error.invalidPaymentMethod', 'apcheckout', null),
                        widget: 'wallet'
                    };
                    break;
                case 'AmazonRejected':
                    result.error = true;
                    result.authError = {
                        code: reasonCode,
                        page: 'COShipping-Start',
                        message: Resource.msg('error.amazonRejected', 'apcheckout', null),
                        widget: 'address'
                    };
                    break;
                case 'ProcessingFailure':
                    result.error = true;
                    result.authError = {
                        code: reasonCode,
                        message: Resource.msg('error.amazonProcessingFailure', 'apcheckout', null),
                        page: 'COSummary-Start'
                    };
                    break;
                case 'TransactionTimedOut':
                    if (asyncMode == true) {
                        result.timeout = true;
                        result.authError = {
                            code: reasonCode
                        };
                        // force the Asynchronous Mode of Authorization
                        session.custom.amzForceAsync = true;
                    } else {
                        result.error = true;
                        result.authError = {
                            code: reasonCode,
                            message: Resource.msg('error.amazonTransactionTimeOut', 'apcheckout', null),
                            page: 'COSummary-Start'
                        };
                    }
                    break;
            }
        }
    }

    return result;
}

/**
* Make a call to GetAuthorizationDetails service
*
* @param {String} amazonAuthorizationID - Authorization ID required for service call
* @return {Object} Authorized result
*/
function getAuthorizationDetailsAction(amazonAuthorizationID) {
    var logger = Logger.getLogger('GetAuthorizationDetailsService'),
        service = ServiceRegistry.get('amazonPayments.service'),
        params = {
            'amazonAuthorizationID' : amazonAuthorizationID,
            'action'                : 'getAuthorizationDetails'
        },
        result = service.call(params),
        amazonLibObject = new amazonLib(),
        resultObj = {
            error: false
        };

    /* check service status */
    if (result.status == 'OK') {
        resultObj.authorizationDetails = amazonLibObject.takeAuthorizationStatus(result.object);
        resultObj.response = result.object;
    } else if (result.status == 'ERROR') {
        logger.error('Http Error ' + result.error);
        resultObj.error = true;
        resultObj.response = result.errorMessage;
        resultObj.authorizationDetails = result.error;
    } else {
        // the timeout was exceeded
        logger.error('Http Timeout when calling GetAuthorizationDetails service');
        resultObj.error = true;
        resultObj.response = result.errorMessage;
        resultObj.authorizationDetails = result.error;
    }
    return resultObj;
}

/**
* Copies the billing address from the GetAuthorizationDetails response to the Order billing address for (DE, UK).
*
* @param {dw.order.Order} order
* @param {Object} authorizationObject
*/
function apStoreBillingAddress(order, authorizationObject) {
    if (!empty(authorizationObject.authorizationBillingAddress)) {
        Transaction.wrap(function() {
            order.billingAddress.setFirstName(authorizationObject.authorizationBillingAddress.firstName);
            order.billingAddress.setLastName(authorizationObject.authorizationBillingAddress.lastName);
            order.billingAddress.setAddress1(authorizationObject.authorizationBillingAddress.address1);
            order.billingAddress.setAddress2('');
            order.billingAddress.setCity(authorizationObject.authorizationBillingAddress.city);
            order.billingAddress.setPhone('');
            order.billingAddress.setPostalCode(authorizationObject.authorizationBillingAddress.postal);
            order.billingAddress.setCountryCode(authorizationObject.authorizationBillingAddress.countryCode);
            order.billingAddress.setStateCode('');
        });
    }

    return;
}

/**
* Make a call to GetCustomerProfile service
*
* @param {String} addressConsentToken
* @return {Object}
*/
function customerProfileAction(addressConsentToken) {
    var logger = Logger.getLogger('customerProfile'),
        service = ServiceRegistry.get('customerProfile.service'),
        params = {
            'addressConsentToken' : addressConsentToken
        },
        result = service.call(params),
        amazonLibObject = new amazonLib(),
        resultObj = {
            error: false
        };

    /* check service status */
    if (result.status == 'OK') {
        resultObj.customerProfileInfo = amazonLibObject.getUserInfoFromResponse(result.object);
    } else if (result.status == 'ERROR') {
        logger.error('Http Error ' + result.error);
        resultObj.error = true;
    } else {
        // the timeout was exceeded
        logger.error('Http Timeout when calling CustomerProfile service');
        resultObj.error = true;
    }
    return resultObj;
}

/**
* Encrypt account password
*
* @param {String} pass - Account password
* @return {String} encryptedPass - Encrypted password
*/
function encryptAccountPassword(pass) {
    var amazonLibObject = new amazonLib();

    if (empty(pass)) {
        return null;
    }
    var encryptedPass;

    try {
        encryptedPass = amazonLibObject.encryptAccountPassword(pass);
    } catch (e) {
        Logger.error('Error while encrypting an account password');
    }

    return encryptedPass;
}

/**
* Decrypt account password
*
* @param {String} pass - Encrypted account password
* @return {String} decryptedPass - Decrypted password
*/
function decryptAccountPassword(pass) {
    var amazonLibObject = new amazonLib();

    if (empty(pass)) {
        return null;
    }
    var decryptedPass;

    try {
        decryptedPass = amazonLibObject.decryptAccountPassword(pass);
    } catch (e) {
        Logger.error('Error while decrypting an account password');
    }

    return decryptedPass;
}

/**
* Copy billing address from shipping address
*
* @param {dw.order.OrderAddress} billingAddress
* @param {dw.order.OrderAddress} shippingAddress
* @param {Oblect} billingAddressAmazon
*/
function apCopyShippingAddressToBillingAddress(billingAddress, shippingAddress, billingAddressAmazon) {
    try {
        Transaction.begin();

        //if not empty billing address from GetOrderReferenceDetails API use it else copy from shipping
        if (!empty(billingAddressAmazon)) {
            //The shipping address for DE/AT is in the address2 row. So copy it, as we use the shipping address in the address1.
            if (billingAddressAmazon.countryCode.value == 'DE' || billingAddressAmazon.countryCode.value == 'AT') {
                if (billingAddressAmazon.address2 != '' && empty(billingAddressAmazon.address1)) {
                    billingAddress.setAddress1(billingAddressAmazon.address2);
                }
            } else {
                billingAddress.setAddress1(billingAddressAmazon.address1);
                billingAddress.setAddress2(billingAddressAmazon.address2);
            }

            // cut parts of name
            var fullName, firstName, lastName;

            if (!empty(billingAddressAmazon.fullName)) {
                fullName  = billingAddressAmazon.fullName.split(' ');
                firstName = fullName[0];
                fullName  = fullName.slice(1);
                lastName  = fullName.join(' ');
            }

            billingAddress.setFirstName(firstName);
            billingAddress.setLastName(lastName);
            billingAddress.setCity(billingAddressAmazon.city);
            billingAddress.setPhone(billingAddressAmazon.phone);
            billingAddress.setPostalCode(billingAddressAmazon.postalCode);
            billingAddress.setCountryCode(billingAddressAmazon.countryCode);
            billingAddress.setStateCode(billingAddressAmazon.stateCode);
        } else {
            // copy all data from shipping address
            billingAddress.setFirstName(shippingAddress.firstName);
            billingAddress.setSecondName(shippingAddress.secondName);
            billingAddress.setLastName(shippingAddress.lastName);
            billingAddress.setSalutation(shippingAddress.salutation);
            billingAddress.setAddress1(shippingAddress.address1);
            billingAddress.setAddress2(shippingAddress.address2);
            billingAddress.setCity(shippingAddress.city);
            billingAddress.setPhone(shippingAddress.phone);
            billingAddress.setPostalCode(shippingAddress.postalCode);
            billingAddress.setCountryCode(shippingAddress.countryCode);
            billingAddress.setStateCode(shippingAddress.stateCode);
        }

        Transaction.commit();
    } catch (e) {
        Transaction.rollback();
    }

    return;
}

/**
 * Check if Amazon Widgets should be displayed at checkout page
 *
 * @return {Boolean} amazonCheckout
 */
function apSetCheckoutMethod() {
    var amazonCheckout = false;

    // check guest checkout with amazon, if we have amazonGuestCheckout at session user clicked at pay with Amazon button
    if (session.custom.amazonGuestCheckout == true) {
        amazonCheckout = true;
    }

    return amazonCheckout;
}

/**
 * Creates a shipping address for the given shipment and copies
 * the address details from Amazon Payment Widget to the
 * created shipment address.
 * The method additionally sets the given gift options to the
 * shipment.
 *
 * @param {dw.order.Shipment} shipment - The shipment to create the address for
 * @param {Object} addressFields - The address object
 * @param {dw.web.FormGroup} giftOptions - The gift options form
 * @return {dw.order.OrderAddress} shippingAddress - The shipment's shipping address
 */
function apCreateShipmentShippingAddress(shipment, addressFields, giftOptions) {
    var shippingAddress = shipment.shippingAddress;

    // if the shipment has no shipping address yet, create one
    if (shippingAddress == null) {
        shippingAddress = shipment.createShippingAddress();
    }

    // copy the address details
    var fullName, firstName, lastName;

    if (!empty(addressFields.fullName)) {
        fullName  = addressFields.fullName.split(' ');
        firstName = fullName[0];
        fullName  = fullName.slice(1);
        lastName  = fullName.join(' ');
    }

    shippingAddress.setFirstName(firstName);
    shippingAddress.setLastName(lastName);
    shippingAddress.setAddress1(addressFields.address1);
    shippingAddress.setAddress2(addressFields.address2);
    shippingAddress.setCity(addressFields.city);
    shippingAddress.setPostalCode(addressFields.postalCode);
    shippingAddress.setStateCode(addressFields.stateCode);
    shippingAddress.setCountryCode(addressFields.countryCode);
    shippingAddress.setPhone(addressFields.phone);

    // copy the gift options
    if (giftOptions != null) {
        shipment.setGift(giftOptions.isGift.value);
        shipment.setGiftMessage(giftOptions.giftMessage.value);
    }

    return shippingAddress;
}

/**
 * This method is used to update the Capture status based on the Capture response.
 * @param {Object} apiData - current request
 * @param {dw.order.Order} apOrder - current Order
 * @return {{error: boolean}} - return object with error status
 */
function updateCaptureAttributes (apiData, apOrder) {
    var result = {
        error: false
    };

    if (empty(apiData)) {
        result.error = true;

        return result;
    }

    if (apOrder.custom.amazonSplitOrder) {
        //Update the Captured amount
        var capturedAmount = new Number(apiData['CaptureAmount.Amount']),
            currentAmount  = new Number(apOrder.custom.amazonCapturedAmount);

        currentAmount += capturedAmount;
        Transaction.wrap(function () {
            apOrder.custom.amazonCapturedAmount = currentAmount;
        });
        //Update the Captured products
        var productList      = new ArrayList(apOrder.custom.amazonCapturedProducts),
            capturedProducts = new ArrayList(apiData.CapturedProducts);

        productList.addAll(capturedProducts);
        Transaction.wrap(function () {
            apOrder.custom.amazonCapturedProducts = productList;
        });
    }

    return result;
}

/**
 * This method is used to update the order status based on the capture response.
 * @param {String} amazonCaptureID
 * @param {String} captureStatus
 * @param {dw.order.Order} currentOrder
 * @return {{error: boolean}}
 */
function updateOrderStatus(amazonCaptureID, captureStatus, currentOrder) {
    var result = {
        error: false
    };

    if (empty(captureStatus) || empty(amazonCaptureID) || empty(currentOrder)) {
        result.error = true;

        return result;
    }

    // Amazon Status:
    //  Pending - A Capture object is in the Pending state until it is processed by Amazon.
    //  Declined - You must submit a capture against an authorization within 30 days (two days for the Sandbox environment).
    //  Completed - The Capture object request has been processed and funds will be moved to your account in the next disbursement cycle.
    //  Closed - When the Capture object is move to the Closed state, you cannot request refunds against it.

    // DW Status:
    //  ORDER_STATUS_NEW
    //  ORDER_STATUS_OPEN
    //  ORDER_STATUS_COMPLETED
    //  ORDER_STATUS_CANCELLED
    //  ORDER_STATUS_REPLACED

    var status         = Order.ORDER_STATUS_CANCELLED,
        paymentStatus  = Order.PAYMENT_STATUS_NOTPAID,
        orderTotal     = currentOrder.getTotalGrossPrice().value,
        capturedAmount = currentOrder.custom.amazonCapturedAmount;

    switch (captureStatus) {
        case 'Pending':
            status = Order.ORDER_STATUS_OPEN;
            break;
        case 'Declined':
            status = Order.ORDER_STATUS_CANCELLED;
            break;
        case 'Completed':
            status        = Order.ORDER_STATUS_COMPLETED;
            paymentStatus = Order.PAYMENT_STATUS_PAID;
            break;
        case 'Closed':
            status = Order.ORDER_STATUS_COMPLETED;
            break;
    }

    if (capturedAmount != orderTotal && currentOrder.custom.amazonSplitOrder) {
        status        = Order.ORDER_STATUS_OPEN;
        paymentStatus = Order.PAYMENT_STATUS_PARTPAID;
    }

    try {
        Transaction.wrap(function () {
            currentOrder.setStatus(status);
            currentOrder.setPaymentStatus(paymentStatus);
        });
    } catch (e) {
        result.error = true;
        return result;
    }

    // added a check not to update capture ID if it's already at the order attribute
    if (!empty(currentOrder.custom.amazonCaptureID)) {
        if (currentOrder.custom.amazonCaptureID.indexOf(amazonCaptureID) == -1) {
            Transaction.wrap(function () {
                currentOrder.custom.amazonCaptureID = currentOrder.custom.amazonCaptureID + ',' + amazonCaptureID;
            });
        }
    } else {
        Transaction.wrap(function () {
            currentOrder.custom.amazonCaptureID = amazonCaptureID;
        });
    }

    Transaction.wrap(function () {
        currentOrder.custom.amazonCaptureStatus = captureStatus;
    });

    if (currentOrder.custom.amazonDeferredOrder == true) {
        Transaction.wrap(function () {
            currentOrder.addNote('Capture', 'Your deferred order has been captured.');
        });
    } else if (currentOrder.custom.amazonSplitOrder) {
        Transaction.wrap(function () {
            currentOrder.addNote('Capture', 'Your split order has been captured.');
        });
    }

    return result;
}

/**
 * This method is used to make a call to CloseOrderReference service
 * @param {String} apOrder - Order reference ID required for service call
 * @return {*}
 */
function closeOrderReference(apOrder) {
    var result = {
            error: false,
            apCloseResult: null
        },
        logger = Logger.getLogger('amazonPayments', 'amazonPayments'),
        // get order reference service
        service = ServiceRegistry.get('amazonPayments.service'),
        params = {
            'orderReferenceID': apOrder,
            'action': 'closeOrderReference'
        },
        serviceResult = service.call(params);

    // check service status
    if (serviceResult.status == 'OK') {
        result.apCloseResult = serviceResult;
    } else if (serviceResult.status == 'ERROR') {
        logger.error('Http Error ' + serviceResult.error);
        result.error = true;
        return result;
    } else {
        // the timeout was exceeded
        logger.error('Http Timeout when calling CloseOrderReference service');
        result.error = true;
        return result;
    }

    return result;
}

/**
* Make a call to SetOrderReference service
*
* @param {dw.order.Basket} basket
* @param {String} sellerOrderId
* @return {Object} result
*/
function setOrderReferenceDetails(basket, sellerOrderId) {
    var logger            = Logger.getLogger('SetOrderRef'),
        contentAssetId    = CurrentSite.getCustomPreferenceValue('amazonSellerNote'),
        amazonCountryCode = CurrentSite.getCustomPreferenceValue('amazonCountryCode'),
        orderReferenceID  = !empty(request.httpParameterMap.orderReferenceID.stringValue) ? request.httpParameterMap.orderReferenceID.stringValue : session.forms.apShippingForm.orderReferenceID.value,
        currencyCode      = basket.getCurrencyCode(),
        storeName         = request.httpHost,
        sellerNote,
        amount,
        platformId;

    if (!empty(contentAssetId)) {
        var asset = ContentMgr.getContent(contentAssetId);
        if (!empty(asset) && !empty(asset.custom.body)) {
            sellerNote = asset.custom.body.toString();
        }
    }
    if (!empty(basket.totalGrossPrice)) {
        amount = basket.totalGrossPrice.getValue().toString();
    } else {
        amount = basket.getAdjustedMerchandizeTotalPrice(true).add(basket.giftCertificateTotalPrice).getValue().toString();
    }

    if (amazonCountryCode.value == 'de' || amazonCountryCode.value == 'uk') {
        platformId = 'A3W2KFQRC2ULNV';
    } else {
        platformId = 'A1N516WGNT5EEM';
    }

    var service = ServiceRegistry.get('amazonPayments.service'),
        params = {
            'orderReferenceID' : orderReferenceID,
            'amount'           : amount,
            'currencyCode'     : currencyCode,
            'platformId'       : platformId,
            'sellerNote'       : sellerNote,
            'sellerOrderId'    : sellerOrderId,
            'storeName'        : storeName,
            'action'           : 'setOrderReferenceDetails'
        },
        result = service.call(params);

    /* check service status */
    if (result.status == 'OK') {
        return result;
    } else if (result.status == 'ERROR') {
        logger.error('Http Error ' + result.error);
        return null;
    } else {
        // the timeout was exceeded
        logger.error('Http Timeout when calling SetOrderReferenceDetails API');
        return null;
    }
}

/**
* Make a call to SetBillingAgreementDetails service
*
* @param {string} customerInfo
* @return {Object} result
*/
function setBillingAgreementDetails(customerInfo) {
    var logger             = Logger.getLogger('SetBillingAgreement'),
        contentAssetId     = CurrentSite.getCustomPreferenceValue('amazonSellerNote'),
        amazonCountryCode  = CurrentSite.current.getCustomPreferenceValue('amazonCountryCode'),
        billingAgreementId = !empty(request.httpParameterMap.billingAgreementID.stringValue) ? request.httpParameterMap.billingAgreementID.stringValue : session.forms.apShippingForm.billingAgreementID.value,
        storeName          = request.httpHost,
        sellerNote,
        customerInformation,
        platformId;

    if (!empty(contentAssetId)) {
        var asset = ContentMgr.getContent(contentAssetId);
        if (!empty(asset) && !empty(asset.custom.body)) {
            sellerNote = asset.custom.body.toString();
        }
    }

    if (!empty(customerInfo)) {
        customerInformation = customerInfo;
    }

    if (amazonCountryCode.value == 'de' || amazonCountryCode.value == 'uk') {
        platformId = 'A3W2KFQRC2ULNV';
    } else {
        platformId = 'A1N516WGNT5EEM';
    }

    var service = ServiceRegistry.get('amazonPayments.service'),
        params = {
            'billingAgreementId'  : billingAgreementId,
            'platformId'          : platformId,
            'sellerNote'          : sellerNote,
            'customerInformation' : customerInformation,
            'storeName'           : storeName,
            'action'              : 'setBillingAgreementDetails'
        },
        result = service.call(params);

    /* check service status */
    if (result.status == 'OK') {
        return result;
    } else if (result.status == 'ERROR') {
        logger.error('Http Error ' + result.error);
        return null;
    } else {
        // the timeout was exceeded
        logger.error('Http Timeout when calling SetOrderReferenceDetails API');
        return null;
    }
}
/**
* Make a call to CloseBillingAgreement service
*
* @param {string} customerInfo
* @return {Object} result
*/
function closeBillingAgreement(billingAgreementID, reason) {
    var logger             = Logger.getLogger('CloseBillingAgreement');
    var resultObj = {
            error: false
        };
    var service = ServiceRegistry.get('amazonPayments.service'),
        params = {
            'billingAgreementId' : billingAgreementID,
            'reason'             : (reason || 'Closing'),
            'action'             : 'closeBillingAgreement'
        };
    var result = service.call(params);

    /* check service status */
    if (result.status == 'OK') {
        resultObj.response = result.object;
    } else if (result.status == 'ERROR') {
        logger.error('Http Error ' + result.error);
        resultObj.error = true;
        resultObj.response = result.errorMessage;
    } else {
        // the timeout was exceeded
        logger.error('Http Timeout when calling CloseBillingAgreement service');
        resultObj.error = true;
        resultObj.response = result.errorMessage;
    }
    return resultObj;
}
/**
* Make a call to GetCaptureDetails service
*
* @param {String} amazonCaptureId - The capture identifier that was generated by Amazon on the earlier call to Capture
* @output {Object} resultObject - The result of the capture
*/
function captureDetailsAction(amazonCaptureId) {
    // get order reference service
    var logger = Logger.getLogger('amazonPayments', 'amazonPayments'),
        service = ServiceRegistry.get('amazonPayments.service'),
        params = {
            'amazonCaptureId' : amazonCaptureId,
            'action'          : 'captureDetails'
        },
        result = service.call(params),
        amazonLibObject = new amazonLib(),
        resultObj = {
            error: false
        };

    /* check service status */
    if (result.status == 'OK') {
        resultObj.result   = amazonLibObject.takeCaptureInfoFromResponse(result.object);
        resultObj.response = result.object;
    } else if (result.status == 'ERROR') {
        logger.error('Http Error ' + result.error);
        resultObj.error    = true;
        resultObj.response = result.errorMessage;
    } else {
        // the timeout was exceeded
        logger.error('Http Timeout when calling GetCaptureDetails service');
        resultObj.error    = true;
        resultObj.response = result.errorMessage;
    }

    return resultObj;
}

/**
* Check if some of the products have already been captured.
*
* @param {Object} Notification
* @return {Object}
*/
function checkSplitOrderCapture(notification) {
    var result = {
        error: false,
        fullyCaptured: false
    };

    if (empty(notification)) {
        result.error = true;
        return result;
    }

    var currentOrder = OrderMgr.searchOrder('custom.amazonAuthorizationID={0}', notification.searchVal);

    if (!empty(currentOrder)) {
        if (currentOrder.custom.amazonSplitOrder) {
            result.fullyCaptured = currentOrder.getTotalGrossPrice().value.toString() == currentOrder.custom.amazonCapturedAmount;
        } else {
            result.fullyCaptured = true;
        }
    }

   return result;
}

/**
* Make a call to GetRefundDetails service
*
* @param {String} amazonRefundId - The refund identifier that was generated by Amazon.
* @return {Object}
*/
function getRefundDetailsAction(amazonRefundId) {
    var logger = Logger.getLogger('ConfirmOrderRef'),
        service = ServiceRegistry.get('amazonPayments.service'),
        params = {
            'amazonRefundId' : amazonRefundId,
            'action'         : 'refundDetails'
        },
        result = service.call(params),
        amazonLibObject = new amazonLib(),
        resultObj = {
            error: false
        };

    /* check service status */
    if (result.status == 'OK') {
        resultObj.result = amazonLibObject.getRefundDetailsFromResponse(result.object);
        resultObj.response = result.object;
    } else if (result.status == 'ERROR') {
        logger.error('Http Error ' + result.error);
        resultObj.error = true;
        resultObj.response = result.errorMessage;
    } else {
        // the timeout was exceeded
        logger.error('Http Timeout when calling GetRefundDetails API');
        resultObj.error = true;
        resultObj.response = result.errorMessage;
    }

    return resultObj;
}

/**
* Update the order status based on the refund response.
*
* @param {dw.order.Order} apOrder
* @param {Object} apRefundDetails
* @return {Boolean}
*/
function updateRefundOrderStatus(apOrder, apRefundDetails) {
    if (empty(apRefundDetails) || empty(apOrder)) {
        return false;
    }

    // Amazon Refund Status:
    //  Pending - A Refund object is in the Pending state until it is processed by Amazon.
    //  Declined - Amazon has declined the refund because the maximum amount has been refunded on the capture.
    //  Completed - The refund request has been processed and funds will be returned to the buyer.

    // DW Status:
    //  ORDER_STATUS_NEW
    //  ORDER_STATUS_OPEN
    //  ORDER_STATUS_COMPLETED
    //  ORDER_STATUS_CANCELLED
    //  ORDER_STATUS_REPLACED

    var status = Order.ORDER_STATUS_CANCELLED;

    switch (apRefundDetails.refundStatus) {
        case 'Pending':
            status = Order.ORDER_STATUS_CANCELLED;
            break;
        case 'Declined':
            status = Order.ORDER_STATUS_CANCELLED;
            break;
        case 'Completed':
            status = Order.ORDER_STATUS_CANCELLED;
            break;
    }

    try {
        Transaction.wrap(function() {
            apOrder.setStatus(status);
            apOrder.addNote('Refund', 'The order was refunded. Refund amount: ' + apRefundDetails.refundAmount + ' ' + apRefundDetails.currencyCode);
            apOrder.custom.amazonRefundStatus = apRefundDetails.refundStatus;
        });
    } catch (e) {
        return false;
    }

   return true;
}

/**
* Make a call to Refund service
*
* @param {Object} options
* @return {Object} refundInfo
*/
function refundAction(options) {
    var logger         = Logger.getLogger('RefundService'),
        contentAssetId = CurrentSite.getCustomPreferenceValue('amazonSellerRefundNote'),
        refundNote;

    if (!empty(contentAssetId)) {
        var asset = ContentMgr.getContent(contentAssetId);
        if (!empty(asset) && !empty(asset.custom.body)) {
            refundNote = asset.custom.body.toString();
        }
    }

    var service = ServiceRegistry.get('amazonPayments.service'),
        params = {
            'options'          : options,
            'sellerRefundNote' : refundNote,
            'action'           : 'refund'
        },
        result = service.call(params),
        amazonLibObject = new amazonLib(),
        refundInfo = {
            error: false
        };

    /* check service status */
    if (result.status == 'OK') {
        refundInfo.result = amazonLibObject.getRefundIDFromResponse(result.object);
        refundInfo.response = result.object;
    } else if (result.status == 'ERROR') {
        logger.error('Http Error ' + result.error);
        refundInfo.error = true;
        refundInfo.response = result.errorMessage;
    } else {
        // the timeout was exceeded
        logger.error('Http Timeout when calling Refund API');
        refundInfo.error = true;
        refundInfo.response = result.errorMessage;
    }

    return refundInfo;
}

/**
* Make a call to ValidateBillingAgreement service
*
* @param {String} billingAgreementId
* @return {Object} validationResult
*/
function validateBillingAgreement(billingAgreementId) {
    var logger = Logger.getLogger('ValidateBillingAgreement'),
        service = ServiceRegistry.get('amazonPayments.service'),
        params = {
            'billingAgreementId' : billingAgreementId,
            'action'             : 'validateBillingAgreement'
        },
        result = service.call(params),
        amazonLibObject = new amazonLib(),
        validationResult = {
            error: false,
            authError: null
        };

    /* check service status */
    if (result.status == 'OK') {
        var asyncMode  = CurrentSite.getCustomPreferenceValue("amazonAsynchronous"),
            reasonCode = amazonLibObject.getValidationResultFromResponse(result.object).reasonCode;

        // reset the flags
        session.custom.amzForceAsync = null;

        if (!empty(reasonCode)) {
            switch (reasonCode) {
                case 'InvalidPaymentMethod' :
                    validationResult.error = true;
                    validationResult.authError = {
                        code: reasonCode,
                        page: 'COBilling-Start',
                        message: Resource.msg('error.invalidPaymentMethod', 'apcheckout', null),
                        widget: 'wallet'
                    };
                    break;

                 case 'ValidationTimedOut' :
                     if (asyncMode == true) {
                         validationResult.timeout = true;
                         validationResult.authError = {
                             code: reasonCode
                         };
                         // force the Asynchronous Mode of Authorization
                         currentSession.custom.amzForceAsync = true;
                     } else {
                         validationResult.error = true;
                         validationResult.authError = {
                             code: reasonCode,
                             message: Resource.msg('error.amazonTransactionTimeOut', 'apcheckout', null),
                             page: 'COSummary-Start'
                         };
                     }
                     break;
                 }
        }
    } else if (result.status == 'ERROR') {
        logger.error('Http Error ' + result.error);
    } else {
        // the timeout was exceeded
        logger.error('Http Timeout when calling ValidateBillingAgreement API');
    }

    return validationResult;
}

/**
* Handle a split payment on the COPlaceOrder pipeline
*
* @param {dw.order.Order} currentOrder - The order to be processed
* @return {Object}
*/
function splitPayment(currentOrder) {
    var lineItems         = currentOrder.getAllProductLineItems(),
        isSplitOrder      = false,
        product,
        splitAmount       = new Money(0, currentOrder.getCurrencyCode()),
        orderAmount       = new Money(0, currentOrder.getCurrencyCode()),
        giftCertAmount    = new Money(0, currentOrder.getCurrencyCode()),
        item,
        proratedPrice,
        shippingAmount    = currentOrder.adjustedShippingTotalGrossPrice,
        total,
        hasOrderPromotion = currentOrder.priceAdjustments.size() > 0;

    // determine if it's a split order and keep track of the amount for
    // the split items and the amount of the general items
    // in order to get the correct price
    for (var i = 0; i < lineItems.length; i++) {
        item = lineItems[i];
        product = item.getProduct();

        if (empty(product)) {
            product = item.parent.getProduct();
        }

        proratedPrice = item.getProratedPrice();

        if (!item.bundledProductLineItem) {
            if (product.custom.amazonSplitPayment) {
                isSplitOrder = true;
                splitAmount  = splitAmount.add(proratedPrice);

                if (!empty(item.shippingLineItem)) {
                    splitAmount    = splitAmount.add(item.shippingLineItem.adjustedGrossPrice);
                    shippingAmount = shippingAmount.subtract(item.shippingLineItem.adjustedGrossPrice);
                }

                // calculate the tax for the price item
                if (hasOrderPromotion) {
                    Transaction.wrap(function() {
                        item.updateTax(item.taxRate, item.proratedPrice);
                    });
                    splitAmount = splitAmount.add(item.tax);
                } else {
                    splitAmount = splitAmount.add(item.adjustedTax);
                }

            } else {
                orderAmount = orderAmount.add(proratedPrice);

                if (!empty(item.shippingLineItem)) {
                    orderAmount    = orderAmount.add(item.shippingLineItem.adjustedGrossPrice);
                    shippingAmount = shippingAmount.subtract(item.shippingLineItem.adjustedGrossPrice);
                }

                // calculate the tax for the price item
                if (hasOrderPromotion) {
                    Transaction.wrap(function() {
                        item.updateTax(item.taxRate, item.proratedPrice);
                    });
                    orderAmount = orderAmount.add(item.tax);
                } else {
                    orderAmount = orderAmount.add(item.adjustedTax);
                }
            }
        }
    }

    // update the split status for the current order

    Transaction.wrap(function() {
        currentOrder.custom.amazonSplitOrder = isSplitOrder;
    });


    // add the remaning shipping cost to get the full order amount
    if (orderAmount.getValue() > 0) {
        orderAmount = orderAmount.add(shippingAmount);
    } else {
        splitAmount = splitAmount.add(shippingAmount);
    }

    // calculate any gift certificates added to the cart
    giftCertAmount = currentOrder.getGiftCertificateTotalPrice();
    orderAmount = orderAmount.add( giftCertAmount );

    // total order amount
    if (!empty(currentOrder.getTotalGrossPrice())) {
        total = currentOrder.getTotalGrossPrice();
    } else {
        total = currentOrder.getAdjustedMerchandizeTotalPrice(true).add(currentOrder.giftCertificateTotalPrice);
    }

    var amount = orderAmount.add(splitAmount),
        fixValue,
        result = {
            error: false
        };

    // we need this logic because of calculation difference for promotions. We will need to add few cents to order amount to be the same as total
    fixValue = total.subtract(amount);

    if ((orderAmount.getValue() > 0) && !orderAmount.equals(giftCertAmount)) {
        orderAmount = orderAmount.add(fixValue);
    } else {
        splitAmount = splitAmount.add(fixValue);
    }

    result.orderAmount = orderAmount;
    result.isSplitOrder = isSplitOrder;


    if (!orderAmount.add(splitAmount).equals(total)) {
        result.error = true;
    }

    return result;
}

/**
* Set the billing address based on the response we get from amazon
*
* @param {Object} address - The data grabbed from amazon
* @param {dw.order.OrderAddress} billingAddress - The billing address to be updated
* @param {Object} billingAddressAmazon - The data grabbed from amazon
*/
function apSetBillingAddress(address, billingAddress, billingAddressAmazon) {
    // cut parts of name
    var fullName, firstName, lastName;

    Transaction.wrap(function() {
            //if not empty billing address from GetOrderReferenceDetails API use it else copy from shipping
        if (!empty(billingAddressAmazon)) {
            //The shipping address for DE/AT is in the address2 row. So copy it, as we use the shipping address in the address1.
            if (billingAddressAmazon.countryCode.value == 'DE' || billingAddressAmazon.countryCode.value == 'AT') {
                if (billingAddressAmazon.address2 != '' && empty(billingAddressAmazon.address1)) {
                    billingAddress.setAddress1(billingAddressAmazon.address2);
                }
            } else {
                billingAddress.setAddress1(billingAddressAmazon.address1);
                billingAddress.setAddress2(billingAddressAmazon.address2);
            }

            if (!empty(billingAddressAmazon.fullName)) {
                fullName  = billingAddressAmazon.fullName.split(' ');
                firstName = fullName[0];
                fullName  = fullName.slice(1);
                lastName  = fullName.join(' ');
            }

            billingAddress.setFirstName(firstName);
            billingAddress.setLastName(lastName);
            billingAddress.setCity(billingAddressAmazon.city);
            billingAddress.setPhone(billingAddressAmazon.phone);
            billingAddress.setPostalCode(billingAddressAmazon.postalCode);
            billingAddress.setCountryCode(billingAddressAmazon.countryCode);
            billingAddress.setStateCode(billingAddressAmazon.stateCode);
        } else {
            if (!empty(address.fullName)) {
                fullName  = address.fullName.split(' ');
                firstName = fullName[0];
                fullName  = fullName.slice(1);
                lastName  = fullName.join(' ');
            }

            // copy all data from shipping address
            billingAddress.setFirstName(firstName);
            billingAddress.setLastName(lastName);
            billingAddress.setAddress1(address.address1);
            billingAddress.setAddress2(address.address2);
            billingAddress.setCity(address.city);
            billingAddress.setPhone(address.phone);
            billingAddress.setPostalCode(address.postalCode);
            billingAddress.setCountryCode(address.countryCode);
            billingAddress.setStateCode(address.stateCode);
        }
    });

    return;
}

/**
* Check for pre-ordered/backordered products on the Cart pipeline
*
* @input {dw.order.Basket} basket
* @return {Object}
*/
function deferredPayment(basket) {
    if (!empty(basket)) {
        var lineItems = basket.getAllProductLineItems(),
            isDeferredPayment = false,
            product,
            item,
            availableProducts = false,
            result = {};

        // determine if the basket contains pre-ordere/backordered products.
        for (var i = 0; i < lineItems.length; i++) {
            item = lineItems[i];
            product = item.getProduct();

            if (empty(product)) {
                product = item.parent.getProduct();
            }

            // if product is preordered or backordered and amazonLateAuthorization = true it's deferred product
            if ((product.availabilityModel.availabilityStatus == dw.catalog.ProductAvailabilityModel.AVAILABILITY_STATUS_PREORDER ||
            product.availabilityModel.availabilityStatus == dw.catalog.ProductAvailabilityModel.AVAILABILITY_STATUS_BACKORDER) && product.custom.amazonLateAuthorization) {
                isDeferredPayment = true;
            } else if (product.availabilityModel.availabilityStatus == dw.catalog.ProductAvailabilityModel.AVAILABILITY_STATUS_IN_STOCK) {
                availableProducts = true;
            }
        }

        result.containsDeferredProducts = isDeferredPayment && availableProducts;
        result.isDeferredPayment = isDeferredPayment;
    }

    return result;
}

/**
* Check if a basket contains any split payment products
*
* @param {dw.order.Basket} basket - The basket to be checked
* @return {Boolean} isSplitPayment - Flag to determine if it's a split payment or not
*/
function checkSplitPayment(basket) {
    var amazonLibObj = new amazonLib();

    return amazonLibObj.isSplitPayment(basket);
}

/**
* Adjust the order amount that will be authorized by amazon taking into account
* the split payment and any gift certificate available on the order
*
* @param {Boolean} isSplitOrder - Flag to determine the split payment status
* @param {dw.value.Money} orderAmount - The amout calculated by the split order pipeline
* @param {dw.order.Order} order - The order containing the gift certificates
* @return {dw.value.Money} customAmount - The corrected amount for the authorization service
*/
function setOrderCustomAmount(isSplitOrder, orderAmount, order) {
    var giftIterator,
        giftCertificate;

    // if the order contains split payment products ignore any gift certificates
    if (!isSplitOrder) {
        // check if a gift certificat is attached and subtract from the order.
        giftIterator = order.getGiftCertificatePaymentInstruments().iterator();

        while (giftIterator.hasNext()) {
            giftCertificate = giftIterator.next();

            orderAmount = orderAmount.subtract(giftCertificate.paymentTransaction.amount);
        }
    }

    // subtract the gift certificate line items as they are captured separately
    orderAmount = orderAmount.subtract(order.getGiftCertificateTotalPrice());

    return orderAmount;
}

/**
* This script will remove all the gift certificate from a basket if it contains split payment products.
*
* @param {dw.order.Basket} basket - The basked to be checked
*/
function removeGiftCertificates(basket) {
    var amazonLibObject = new amazonLib(),
        isSplitPayment = amazonLibObject.isSplitPayment(basket);

    if (isSplitPayment) {
        // iterate over the list of payment instruments
        var gcPaymentInstrs = basket.getGiftCertificatePaymentInstruments(),
             iter = gcPaymentInstrs.iterator(),
             existingPI = null;

        // remove (possible multiple) gift certificate payment instruments
        Transaction.wrap(function() {
            while (iter.hasNext()) {
                existingPI = iter.next();
                basket.removePaymentInstrument(existingPI);
            }
        });
    }

    return;
}

function isRecurring(basket, billingAgreementAmount) {

    var recurringPayment        = false,
        requireBillingAgreement = false,
        consent                 = null,
        plisIter                = basket.getProductLineItems().iterator();
	
    if (CurrentSite.getCustomPreferenceValue('amazonIsEnable') && (CurrentSite.getCustomPreferenceValue('amazonEnableRecurringProducts') || CurrentSite.getCustomPreferenceValue('enableBillingAgreementsForAllProducts').value == 'required' || CurrentSite.getCustomPreferenceValue('enableBillingAgreementsForAllProducts').value == 'optional')) {

        while (plisIter.hasNext() && !recurringPayment) {
            var item = plisIter.next();
            var product = item.product;

            if ('amazonRequireBillingAgreement' in product.custom && product.custom.amazonRequireBillingAgreement) {
                requireBillingAgreement = true;
            } else if (product.isVariant()) {
            	if ('amazonRequireBillingAgreement' in product.masterProduct.custom && product.masterProduct.custom.amazonRequireBillingAgreement) {
                    requireBillingAgreement = true; 
                } 
            } 

            if (requireBillingAgreement) {
                recurringPayment = true;
            } else if (item.custom.amazonRecurringPurchase) {
                recurringPayment = true;
            }
        }

        if (!recurringPayment) {
            var enableBAForAllProducts = CurrentSite.getCustomPreferenceValue('enableBillingAgreementsForAllProducts').value,
                consent;

            if (enableBAForAllProducts == 'required' || enableBAForAllProducts == 'optional') {
                recurringPayment = true;
                consent          = enableBAForAllProducts;
            }
        }

        if (!empty(billingAgreementAmount)) {
            recurringPayment = false;
        }
    }

    return {
        isRecurring : recurringPayment,
        consent : consent
    }
}

function getNonGiftCertificateAmount(basket) {
	var giftCertTotal = new Money(0.0, basket.getCurrencyCode());
	
	// Gets the list of all gift certificate payment instruments
    var gcPaymentInstrs = basket.getGiftCertificatePaymentInstruments();
    var iter = gcPaymentInstrs.iterator();
    var orderPI = null;

    // Sums the total redemption amount.
    while (iter.hasNext()) {
        orderPI = iter.next();
        giftCertTotal = giftCertTotal.add(orderPI.getPaymentTransaction().getAmount());
    }

    // Gets the order total.
    var orderTotal = basket.getTotalGrossPrice();

    // Calculates the amount to charge for the payment instrument.
    // This is the remaining open order total that must be paid.
    var amountOpen = orderTotal.subtract(giftCertTotal);
	
	// Returns the open amount to be paid.
    return amountOpen.value;
}


module.exports = {
    checkCaptureProducts: checkCaptureProducts,
    requestCaptureAction: requestCaptureAction,
    getRandomString: getRandomString,
    apPrepareAuthorizeObject: apPrepareAuthorizeObject,
    checkAuthorizationProducts: checkAuthorizationProducts,
    authorizeAction: authorizeAction,
    updateAuthorizationAttributes: updateAuthorizationAttributes,
    updateAuthorizationStatus: updateAuthorizationStatus,
    apCaptureGiftCertificates: apCaptureGiftCertificates,
    updateAuthorizationGiftCert: updateAuthorizationGiftCert,
    checkAmazonPayment: checkAmazonPayment,
    getGiftCertificateStatus: getGiftCertificateStatus,
    checkTokenAction: checkTokenAction,
    orderReferenceAction: orderReferenceAction,
    billingAgreementAction: billingAgreementAction,
    updateOrderReferenceStatus: updateOrderReferenceStatus,
    updateBillingAgreementStatus: updateBillingAgreementStatus,
    apCopyShippingAddressToForm: apCopyShippingAddressToForm,
    confirmOrderReferenceAction: confirmOrderReferenceAction,
    confirmBillingAgreementAction: confirmBillingAgreementAction,
    checkValidAuth: checkValidAuth,
    getAuthorizationDetailsAction: getAuthorizationDetailsAction,
    apStoreBillingAddress: apStoreBillingAddress,
    customerProfileAction: customerProfileAction,
    encryptAccountPassword: encryptAccountPassword,
    decryptAccountPassword: decryptAccountPassword,
    apCopyShippingAddressToBillingAddress: apCopyShippingAddressToBillingAddress,
    apCreateShipmentShippingAddress: apCreateShipmentShippingAddress,
    apSetCheckoutMethod: apSetCheckoutMethod,
    updateCaptureAttributes: updateCaptureAttributes,
    updateOrderStatus: updateOrderStatus,
    closeOrderReference: closeOrderReference,
    setOrderReferenceDetails: setOrderReferenceDetails,
    setBillingAgreementDetails: setBillingAgreementDetails,
    captureDetailsAction: captureDetailsAction,
    checkSplitOrderCapture: checkSplitOrderCapture,
    getRefundDetailsAction: getRefundDetailsAction,
    updateRefundOrderStatus: updateRefundOrderStatus,
    refundAction: refundAction,
    splitPayment: splitPayment,
    apSetBillingAddress: apSetBillingAddress,
    deferredPayment: deferredPayment,
    checkSplitPayment: checkSplitPayment,
    setOrderCustomAmount: setOrderCustomAmount,
    validateBillingAgreement: validateBillingAgreement,
    removeGiftCertificates: removeGiftCertificates,
    closeBillingAgreement : closeBillingAgreement,
    createOrderReferenceForIdAction: createOrderReferenceForIdAction,
    isRecurring: isRecurring,
    getNonGiftCertificateAmount: getNonGiftCertificateAmount
};
