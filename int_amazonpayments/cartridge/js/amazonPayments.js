/**
* This file is not used, If any changes needs to be done in this file 
* Please do it in static > js > amazonPayments.js and minify it by placing below code in gulpfile.js and run "gulp minify-amazon-payments" cmd
* But don't commit the below code in gulpfile.js
*  gulp.task('minify-amazon-payments', () => {
*    return gulp.src('../int_amazonpayments/cartridge/static/default/js/amazonPayments.js', { allowEmpty: true })
*       .pipe(uglify({ noSource: true }))
*        .pipe(rename({ extname: '.min.js' }))
*       .pipe(gulp.dest('../int_amazonpayments/cartridge/static/default/js'))
*	});
*/
$(document).ready(function() {

    var progress = require('./progress'),
        tooltip = require('./tooltip'),
        validator = require('./validator'),
        dialog = require('./dialog'),
        util = require('./util');

    var shippingMethods,
        isRecurring = $('[name$="_billingAgreementID"]').val() !== 'null';

    var amazonPaymentsObject = {

        /*
        * This function is used to initialize address book widget
        */
        initializeAddressBookWidget : function() {

            var refID = null,
                billingAgreementId = null;

            var params = {
                sellerId: SitePreferences.AMAZON_MERCHANT_ID  || '',
                design: {
                    designMode: this.getDesignMode()
                 },
                onError: function(error) {
                    // display error message
                    alert(error.getErrorMessage());
                }
            };

            if (isRecurring) {
                billingAgreementId = $('[name$="_billingAgreementID"]').val();
                params.agreementType = 'BillingAgreement';
                params.amazonBillingAgreementId = billingAgreementId;
                params.onReady = function(billingAgreement) {
                    //
                };
            } else {
                refID = $('[name$="_orderReferenceID"]').val();
                params.amazonOrderReferenceId = refID;
                params.onOrderReferenceCreate = function(orderReference) {
                    //
                };
            };

            params.onAddressSelect = function(orderReference) {
                // disable continue button
            	//MAT-2109 Changes
            	$('#apShippingContinue').addClass('grey-disabled');
            	$('#apShippingContinue').attr('disabled', 'disabled');
            	
                // update shipping methods list
                var params = isRecurring ? {billingAgreementID: billingAgreementId} : {orderReferenceID: refID}
                amazonPaymentsObject.updateShippingMethodsList(params);
            };

            new OffAmazonPayments.Widgets.AddressBook(params).bind("addressBookWidgetDiv");
        },

        /*
         * Wallet widget for billing page.
         * Widget content id: walletWidgetDiv
         */
        initializeWalletWidget: function() {
            var params = {
                sellerId: SitePreferences.AMAZON_MERCHANT_ID || '',
                onPaymentSelect: function(orderReference) {
                    // enable continue button after payment selection
                    if (!isRecurring || $('#consentWidgetDiv').length == 0) {
                        $('[name$="_save"]').removeAttr('disabled');
                    }

                    /*$.ajax ({
                        url : isRecurring ? Urls.getBillingAgreementBillingDetails : Urls.getOrderReferenceBillingDetails,
                        method : "GET",
                        success : function(data) {
                            $("div.apAddressBilling").replaceWith(data);
                        }
                    });*/
                },
                design: {
                    designMode: this.getDesignMode()
                },
                onError: function(error) {
                    // display error message
                    alert(error.getErrorMessage());
                }
            };

            if (isRecurring) {
                params.amazonBillingAgreementId = $('[name$="_billingAgreementID"]').val();
            } else {
                params.amazonOrderReferenceId = $('[name$="_orderReferenceID"]').val();
            }

            // disable continue button
            $('[name$="_save"]').attr('disabled', 'disabled');

            new OffAmazonPayments.Widgets.Wallet(params).bind("walletWidgetDiv");

        },

        /*
         * Consent widget for billing page.
         * Widget content id: consentWidgetDiv
         */
        initializeConsentWidget: function() {
            var buyerBillingAgreementConsentStatus,
                billingAgreementId = $('[name$="_billingAgreementID"]').val(),
                consent = $('[name$="consent"]').val(),
                $orderSubmitBtn = $('[name$="_save"]');

            if (consent == 'optional') {
                $orderSubmitBtn.removeAttr('disabled');
            }

            new OffAmazonPayments.Widgets.Consent({
                sellerId: SitePreferences.AMAZON_MERCHANT_ID  || '',
                // amazonBillingAgreementId obtained from the Amazon Address Book widget.
                amazonBillingAgreementId: billingAgreementId,
                onReady: function(billingAgreementConsentStatus) {
                    // Called after widget renders
                    if (consent == 'optional') {
                        $orderSubmitBtn.removeAttr('disabled');
                    } else {
                    	if (typeof billingAgreementConsentStatus.getConsentStatus == 'function') {
                            buyerBillingAgreementConsentStatus = billingAgreementConsentStatus.getConsentStatus();
                    	}
                        if (buyerBillingAgreementConsentStatus == 'false') {
                            $orderSubmitBtn.attr('disabled', 'disabled');
                        } else {
                            $orderSubmitBtn.removeAttr('disabled');
                        }
                    }
                },
                onConsent: function(billingAgreementConsentStatus) {
                    // getConsentStatus returns true or false
                    // true – checkbox is selected – buyer has consented
                    // false – checkbox is unselected – buyer has not consented
                    buyerBillingAgreementConsentStatus = billingAgreementConsentStatus.getConsentStatus();

                    if (consent == 'optional') {
                        $orderSubmitBtn.removeAttr('disabled');
                    } else {
                        if (buyerBillingAgreementConsentStatus == 'false') {
                            $orderSubmitBtn.attr('disabled', 'disabled');
                        } else {
                            $orderSubmitBtn.removeAttr('disabled');
                        }
                    }

                    $.ajax ({
                        url: Urls.setConsentStatus,
                        method : "POST",
                        data: {
                             consentStatus: buyerBillingAgreementConsentStatus
                         }
                    });
                },
                design: {
                    designMode: this.getDesignMode()
                },
                onError: function(error) {
                    // display error message
                    alert(error.getErrorMessage());
                }
            }).bind("consentWidgetDiv");
        },

        /*
         * This function is used to determine the design mode for the amazon widgets.
         */
        getDesignMode: function() {
            // check if it's a touch device with a small screen
            if (('ontouchstart' in window) && $(window).width() < 600) {
                return 'smartphoneCollapsible';
            }

            return 'responsive';
        },

        /*
         * AmazonPayButton.
         * Content id: AmazonPayButton
         */
        initializeAmazonPayButton: function($btn){
            var authRequest;
            var data = {};
            data.merchantId = SitePreferences.AMAZON_MERCHANT_ID;
            data.scope = SitePreferences.AMAZON_SCOPE;
            var btnData = $btn.data();
            var id = $btn.attr('id');

            // attach a random generated id
            if (!id) {
                id = amazonPaymentsObject.getUniqueId();
                $btn.attr('id', id);
            }

            if (btnData.button == 'payButton') {
                data.type = SitePreferences.AMAZON_PAY_BUTTON_TYPE;
                data.color = SitePreferences.AMAZON_PAY_BUTTON_COLOR;
                data.size = SitePreferences.AMAZON_PAY_BUTTON_SIZE;
                data.redirect = SitePreferences.AMAZON_PAY_REDIRECT_URL;
            } else if (btnData.button == 'editButton') {
                data.type = 'EditOrUpdate';
                data.color = SitePreferences.AMAZON_PAY_BUTTON_COLOR;
                data.size = SitePreferences.AMAZON_PAY_BUTTON_SIZE;
                data.redirect = Urls.editButtonRedirect;
            } else {
                // login button functionality
                data.type = SitePreferences.AMAZON_LOGIN_BUTTON_TYPE;
                data.color = SitePreferences.AMAZON_LOGIN_BUTTON_COLOR;
                data.size = SitePreferences.AMAZON_LOGIN_BUTTON_SIZE;
                data.redirect = SitePreferences.AMAZON_LOGIN_REDIRECT_URL;
            }

            if (btnData.page == 'shipping') {
                data.redirect += '?shipping=true';
            }

            var params = {
                type: data.type,
                color: data.color,
                size: data.size,
                language: btnData.locale,
                useAmazonAddressBook: true,
                authorization: function() {
                    if ($('[name$="amazonPayLogin"]').val() == 'false') {
                        window.location.href = data.redirect;
                    } else {
                        var loginOptions = {scope: data.scope};
                        authRequest = amazon.Login.authorize(loginOptions, function(response) {
                            // do nothing here, instead wait for billing agreement to be generated in onSignIn
                        });
                    }
                },
                onError: function(error) {
                    // Write your custom error handling
                }
            };


            if ($('[name$="isRecurring"]').val() == 'true' || data.type == 'EditOrUpdate') {
                params.agreementType = 'BillingAgreement';

                params.onSignIn = function(billingAgreement) {
                    var billingAgreementId = billingAgreement.getAmazonBillingAgreementId(),
                        url = util.appendParamToURL(data.redirect, 'billingAgreementID', billingAgreementId);

                    if (data.type == 'EditOrUpdate') {
                        authRequest.onComplete(function(a) {
                            amazon.Login.retrieveProfile(a.access_token, function(resp) {
                                if ($('.emailAddress').text() != resp.profile.PrimaryEmail) {
                                    amazon.Login.logout();

                                    dialog.open({
                                        html: '<h1>' + Resources.AMAZON_LOGIN_ERROR + '</h1>',
                                        options: {
                                            buttons: [{
                                                text: Resources.OK,
                                                click: function () {
                                                    $(this).dialog('close');
                                                }
                                            }]
                                        }
                                    });
                                } else {
                                    window.location = url + '&access_token=' + a.access_token;
                                }
                            });
                        });
                    } else { 
                    	if (authRequest) {
                    		authRequest.onComplete(url);
                    	}
                    }

                };
            } else {
                 params.onSignIn = function(orderReference) {
                    var orderReferenceId = orderReference.getAmazonOrderReferenceId(),
                        url = util.appendParamToURL(data.redirect, 'orderReferenceID', orderReferenceId);

                    authRequest.onComplete(url);
                };
            }

            OffAmazonPayments.Button(id, data.merchantId, params);
        },

        /**
         * Adds the event listener on the logout button.
         */
        initLogout: function() {

            var $logoutBtn = $('.js-logout');

            if ((typeof amazon != 'undefined') && amazon.Login) {

                if ($logoutBtn.length) {
                    $logoutBtn.on('click', function() {
                        amazon.Login.logout();
                    });
                }
                // if an element on the page has this attribute
                // logout the user from the amazon account
                if ($('[data-amz-logout]').length) {
                    amazon.Login.logout();
                }
            }
        },

        /*
         * This function is used to update shipping methods list with appropriate shipping methods
         */
        updateShippingMethodsList : function(params) {
            var $shippingMethodList = $('#shipping-method-list');
            if (!$shippingMethodList || $shippingMethodList.length === 0) { return; }
            var url = util.appendParamsToUrl(Urls.shippingMethodsJSON, params);

            $.ajax({
                dataType: 'json',
                url: url,
            })
            .done(function (data) {
                if (!data) {
                    window.alert('Couldn\'t get list of applicable shipping methods.');
                    return false;
                }
                 
                //MAT-2109 changes
                var stateCode;
                if(data.length > 0){
                	stateCode = data[data.length-1];
            		amazonPaymentsObject.checkForRetrictedCheckout(stateCode);
                }
                
                if (shippingMethods && shippingMethods.toString() === data.toString()) {
                    $('#apShippingContinue').removeAttr('disabled');
                    // No need to update the UI.  The list has not changed.
                    return true;
                }

                // We need to update the UI.  The list has changed.
                // Cache the array of returned shipping methods.
                shippingMethods = data;
                // indicate progress
                progress.show($shippingMethodList);
                // load the shipping method form
                var smlUrl =  util.appendParamsToUrl(Urls.shippingMethodsList, params);
                $shippingMethodList.load(smlUrl, function () {
                    $shippingMethodList.fadeIn('fast');
                    // rebind the radio buttons onclick function to a handler.
                    $shippingMethodList.find('[name$="_shippingMethodID"]').click(function () {
                        amazonPaymentsObject.selectShippingMethod($(this).val(), params);
                    });

                    // update the summary
                    amazonPaymentsObject.updateSummary();
                    progress.hide();
                    $('#apShippingContinue').removeAttr('disabled');
                    tooltip.init();
                    //if nothing is selected in the shipping methods select the first one
                    if ($shippingMethodList.find('.input-radio:checked').length === 0) {
                        $shippingMethodList.find('.input-radio:first').attr('checked', true);
                    }
                });
            });
        },
            
        /*
         * *This method will restrict the user from checkout if Alaska, Hawaii & Puerto Rico states
         * is used for shipping address
         */
        checkForRetrictedCheckout : function(stateCode){
        	if(typeof stateCode != undefined && stateCode != null && stateCode != ''){
	    		stateCode = stateCode.toUpperCase();
	    		if(stateCode == 'AK' || stateCode == 'HI' || stateCode == 'PR'){
	    			$('.checkout-shipping .amazon-restrictedcheckout-msg').removeClass('hide');
	    			$('.amazon-continue-shipping').hide();
	    			$('.restrict-continue-shipping').show();
	    		}else{
	    			if(!$('.checkout-shipping .amazon-restrictedcheckout-msg').hasClass('hide')){
	    				$('.checkout-shipping .amazon-restrictedcheckout-msg').addClass('hide');
	    			}
	    			$('#apShippingContinue').removeClass('grey-disabled');
	    			$('#apShippingContinue').removeAttr('disabled', 'disabled');
	    			$('.amazon-continue-shipping').show();
	    			$('.restrict-continue-shipping').hide();
	    		}
        	}
        },

        /*
         * This function is used to select active shipping method
        */
        selectShippingMethod : function(shippingMethodID, params) {
            // nothing entered
            if (!shippingMethodID) {
                return;
            }
            params.shippingMethodID = shippingMethodID;
            // attempt to set shipping method
            var url = util.appendParamsToUrl(Urls.selectShippingMethodsList, params);
            $.ajax({
                dataType: 'json',
                url: url,
            })
            .done(function (data) {

                amazonPaymentsObject.updateSummary();

                if (!data || !data.shippingMethodID) {
                    window.alert('Couldn\'t select shipping method.');
                    return false;
                }
                // display promotion in UI and update the summary section,
                // if some promotions were applied
                $('.shippingpromotions').empty();
            });
        },

        /*
         * This function is used to update summary section
         */
        updateSummary : function() {
            var $summary = $('#secondary.summary');
            // indicate progress
            progress.show($summary);
            // load the updated summary area
            $summary.load(Urls.summaryRefreshURL, function () {
                // hide edit shipping method link
                $summary.fadeIn('fast');
                $summary.find('.checkout-mini-cart .minishipment .header a').hide();
                $summary.find('.order-totals-table .order-shipping .label a').hide();
            });
        },

        /**
         * This function will generate a unique id
         */
        getUniqueId: function () {
            // Math.random should be unique because of its seeding algorithm.
            // Convert it to base 36 (numbers + letters), and grab the first 9 characters
            // after the decimal.
            return 'random-' + Math.random().toString(36).substr(2, 9);
        },

        /*
         * this function is used to init Amazon Payments popup
         */
        initPopup : function() {
            $(document).on("click", "a.amazonpopup", function(e) {
                e.preventDefault();
                window.open($(this).attr('href'), '', 'width=626,height=436');
                return false;
            });
        },

        /*
         * this function is used to init popup for cancel billing agreement on orderhistory page
         */
        initCancelBAPopup : function() {
            $(document).on("click", "a.ap-cancel-order", function(e) {
                 e.preventDefault();

                 dialog.open({
                     url: $(e.target).attr('href'),
                     options: {
                         open: function() {
                             $('.close').on('click', function(e) {
                                 e.preventDefault();
                                 $.ajax({
                                     url: $(this).attr('href')
                                 }).done(function(data) {
                                     if (data.success) {
                                         window.location.assign(window.location.href);
                                     } else {
                                         $('.ba-wrapper').html(data.error);
                                     }
                                 });
                             });
                         }
                     }
                 });
            });
        },

        /*
         * This function will be used to scroll the page to the wallet widget if
         * it has the data-scroll-to-element attribute set.
         */
        scrollToWidget: function() {
            var $el = $('[data-scroll-to-element]').last();
            var offset;
            if ($el.length) {
                var offset = $el.offset();
                $("html, body").animate({ scrollTop: offset.top + "px" });
            }
        },

        /*
         * This function is used initialize necessary functions
         */
        init : function() {
            var $amazonBtns = $('.js-amazon-button');

            if ($('#addressBookWidgetDiv').length > 0) {
                amazonPaymentsObject.initializeAddressBookWidget();
            } else if ($('.apAddressShipping').length > 0 && $('[name$="_billingAgreementID"]').val()) {
                var params = {billingAgreementID: $('[name$="_billingAgreementID"]').val()};
                amazonPaymentsObject.updateShippingMethodsList(params);
            }

            if ($('#walletWidgetDiv').length > 0) {
                amazonPaymentsObject.initializeWalletWidget();
            }

            if ($('#consentWidgetDiv').length > 0) {
                amazonPaymentsObject.initializeConsentWidget();
            }

            if ($amazonBtns.length > 0) {
                $amazonBtns.each(function(i) {
                    amazonPaymentsObject.initializeAmazonPayButton( $(this) );
                });
                amazonPaymentsObject.initPopup();
            }

            amazonPaymentsObject.initCancelBAPopup();

            amazonPaymentsObject.initLogout();
            amazonPaymentsObject.scrollToWidget();
        }
    };

    amazonPaymentsObject.init();

});
