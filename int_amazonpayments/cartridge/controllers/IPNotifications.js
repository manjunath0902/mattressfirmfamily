'use strict';

/**
 * Controller for Amazon Payments Custom API
 *
 * @module controllers/IPNotification
 */

/* API Includes */
var Logger = require('dw/system/Logger');

/* Script Modules */
var app            = require('~/cartridge/scripts/app'),
    guard          = require('~/cartridge/scripts/guard'),
    validation     = require('~/cartridge/scripts/notification/validation'),
    validationTest = require('~/cartridge/scripts/notification/validationTest'),
    testX509Module = require('~/cartridge/scripts/testX509.ds');

/**
 * Callback for notification. The following is a list of notifications available from Amazon:
 * BillingAgreementNotification
 * OrderReferenceNotification
 * AuthorizationNotification
 * CaptureNotification
 * RefundNotification
 */
function notification() {
    var validationResult = validation.validation(request.httpParameterMap, request, 'IPNotifications');

    if (validationResult.error) {
        return app.getView().render('notification/aperrorresponse');
    }

    // prepare dynamic function call
    var dynamicArray      = validationResult.controller.split('-'),
        // dynamic function call
        dynamicController = require('~/cartridge/controllers/' + dynamicArray[0]),
        dynamicCallResult = dynamicController[dynamicArray[1]](validationResult.notificationData);

    if (dynamicCallResult.error) {
        return app.getView().render('notification/aperrorresponse');
    }

    return app.getView().render('notification/apsuccessresponse');
}

/**
 * This method is used to test X509 public key implementation
 */
function testX509() {
    var key = testX509Module.getKey();

    return app.getView({
        key: key
    }).render('notification/testx509key');
}

/**
 * This method is used to test instant payment notification
 */
function testNotification() {
    var testResult = validationTest.validation(request.httpParameterMap, request, 'IPNotifications');

    if (testResult.error) {
        return app.getView().render('notification/aperrorresponse');
    }

    // prepare dynamic function call
    var dynamicArray      = testResult.controller.split('-'),
        // dynamic function call
        dynamicController = app.getController(dynamicArray[0]),
        dynamicCallResult = dynamicController[dynamicArray[1]](testResult.notificationData);

    if (dynamicCallResult.error) {
        return app.getView().render('notification/aperrorresponse');
    }

    return app.getView().render('notification/apsuccessresponse');
}

/**
 * This method is used for OrderReferenceNotification
 */
function orderReferenceNotification(notificationData) {
    var result = require('~/cartridge/controllers/AmazonPayments').GetOrderReferenceResponse(notificationData);

    return !result.error;
}

/**
 * This method is used for BillingAgreementNotification
 */
function billingAgreementNotification(notificationData) {
    var result = require('~/cartridge/controllers/AmazonPayments').GetBillingAgreementDetailsResponse(notificationData);

    return !result.error;
}

/**
 * This method is used for AuthorizationNotification
 * @param {Object} notificationData
 * @return {Boolean} - true if no errors, false there are errors
 */
function paymentAuthorize(notificationData) {
    var result = require('~/cartridge/controllers/AmazonPayments').GetAuthorizationDetails(notificationData);

    return !result.error;
}

/**
 * This method is used for CaptureNotification
 * @param {Object} notificationData
 * @return {Boolean} - true if no errors, false there are errors
 */
function paymentCapture(notificationData) {
    return require('~/cartridge/controllers/AmazonPayments').GetCaptureStatus(notificationData);
}

/**
 * This method is used for RefundNotification
 * @param {Object} notificationData
 * @return {Boolean} - true if no errors, false there are errors
 */
function paymentRefund(notificationData) {
    var result = require('~/cartridge/controllers/AmazonPayments').GetRefundDetails(notificationData);

    return !result.error;
}

/*
 * Web exposed methods
 */
/** Callback for notification.
 * @see module:controllers/IPNotification~notification */
exports.Notification = guard.ensure(['post'], notification);
/** This method is used to test X509 public key implementation
 * @see module:controllers/IPNotification~testX509 */
exports.TestX509 = guard.ensure(['get'], testX509);
/** This method is used to test instant payment notification
 * @see module:controllers/IPNotification~testNotification */
exports.TestNotification = guard.ensure(['get'], testNotification);
/*
 * Local methods
 */
/** This method is used for OrderReferenceNotification
 * @see module:controllers/IPNotification~orderReferenceNotification */
exports.OrderReferenceNotification = orderReferenceNotification;
/** This method is used for BillingAgreementNotification
 * @see module:controllers/IPNotification~billingAgreementNotification */
exports.BillingAgreementNotification = billingAgreementNotification;
/** This method is used for AuthorizationNotification
 * @see module:controllers/IPNotification~paymentAuthorize */
exports.PaymentAuthorize = paymentAuthorize;
/** This method is used for CaptureNotification
 * @see module:controllers/IPNotification~paymentCapture */
exports.PaymentCapture = paymentCapture;
/** This method is used for RefundNotification
 * @see module:controllers/IPNotification~paymentRefund */
exports.PaymentRefund = paymentRefund;
