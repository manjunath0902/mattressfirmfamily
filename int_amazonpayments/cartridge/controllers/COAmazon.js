/**
* Description of the Controller and the logic it provides
*
* @module  controllers/COAmazon
*/

'use strict';

// HINT: do not put all require statements at the top of the file
// unless you really need them for all functions

/* API Includes */
var CustomerMgr = require('dw/customer/CustomerMgr');
var HashMap = require('dw/util/HashMap');
var Resource = require('dw/web/Resource');
var ShippingMgr = require('dw/order/ShippingMgr');
var Site = require('dw/system/Site');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');
var StoreMgr = require('dw/catalog/StoreMgr');
var StringUtils = require('dw/util/StringUtils');

/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var pipeletHelper = require('bc_sleepysc/cartridge/scripts/sleepys/util/SleepysPipeletsHelper').SleepysPipeletHelper;
var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;
var emailHelper = require("app_storefront_controllers/cartridge/scripts/util/SFEmailSubscriptionHelper").SFEmailSubscriptionHelper;
var StringHelpers = require('app_storefront_core/cartridge/scripts/util/StringHelpers');
var apController = require('int_amazonpayments/cartridge/controllers/AmazonPayments');
var apModule = require('int_amazonpayments/cartridge/scripts/AmazonPaymentsModule');
/**
* Description of the function
*
* @return {String} The string 'myFunction'
*/
/**
 * Starting point for the single shipping scenario. Prepares a shipment by removing gift certificate and in-store pickup line items from the shipment.
 * Redirects to multishipping scenario if more than one physical shipment is required and redirects to billing if all line items do not require
 * shipping.
 *
 * @transactional
 */
function start(amazonAuthError) {
    var cart, pageMeta, homeDeliveries;
    cart = app.getModel('Cart').get();
    session.custom.amazonGuestCheckout = true;
    session.custom.checkoutStep = 'shipping';
    //reseting AmazonAuthErrorMessage and AmazonResultErrorMessage sessions
    session.custom.AmazonAuthErrorMessage=null;
    session.custom.AmazonResultErrorMessage = null;
    session.forms.apShippingForm.addressConsentToken.value = !empty(request.httpParameterMap.access_token.stringValue) ? request.httpParameterMap.access_token.stringValue : null;
    if (session.custom.amazonGuestCheckout && !session.custom.closeBA) {
        apController.StoreOpenBillingAgreementID(cart);
    }

    if (cart) {
    	session.forms.cart.coupons.copyFrom(cart.object.couponLineItems);
        // Redirects to multishipping scenario if more than one physical shipment is contained in the basket.
        //physicalShipments = cart.getPhysicalShipments();
       // if (!(Site.getCurrent().getCustomPreferenceValue('enableMultiShipping') && physicalShipments && physicalShipments.size() > 1 )) {
       // Will use multi-ship for all orders as long as the site preference is set.
        if ( !(Site.getCurrent().getCustomPreferenceValue('enableMultiShipping')) ) {
            // Initializes the singleshipping form and prepopulates it with the shipping address of the default
            // shipment if the address exists, otherwise it preselects the default shipping method in the form.
            if (cart.getDefaultShipment().getShippingAddress()) {
                app.getForm(session.forms.singleshipping.shippingAddress.addressFields).copyFrom(cart.getDefaultShipment().getShippingAddress());
                app.getForm(session.forms.singleshipping.shippingAddress.addressFields.states).copyFrom(cart.getDefaultShipment().getShippingAddress());
                app.getForm(session.forms.singleshipping.shippingAddress).copyFrom(cart.getDefaultShipment());
            } else {
                if (customer.authenticated && customer.registered && customer.addressBook.preferredAddress) {
                    app.getForm(session.forms.singleshipping.shippingAddress.addressFields).copyFrom(customer.addressBook.preferredAddress);
                    app.getForm(session.forms.singleshipping.shippingAddress.addressFields.states).copyFrom(customer.addressBook.preferredAddress);
                }
            }

            session.forms.singleshipping.shippingAddress.email.emailAddress.value = cart.getCustomerEmail();
            
            var isInstorePickup = false;
            var storeId = '';
            var plisIterator = cart.getAllProductLineItems().iterator();
            while (plisIterator.hasNext()) {
                var pli = plisIterator.next();
                if (!empty(pli.custom.storePickupStoreID)) {
                    // this is in store pickup
                    isInstorePickup = true;
                    storeId = pli.custom.storePickupStoreID;
                }
            }
            
            if (empty(storeId)) {
            	if (session.customer.registered && !empty(session.customer.profile.custom.preferredStore)) {
					storeId = session.customer.profile.custom.preferredStore;
				} else if (session.custom.preferredStore) {
					storeId = session.custom.preferredStore;
				}
            }
            
            if (session.custom.deliveryOptionChoice == 'instorepickup' && !empty(storeId)) {
                // set select shipment to instore pickup
                session.forms.singleshipping.shippingAddress.shippingMethodID.value = 'storePickup';
                var store = StoreMgr.getStore(storeId);
                app.getForm('singleshipping').object.shippingAddress.addressFields.address1.value = store.address1;
                app.getForm('singleshipping').object.shippingAddress.addressFields.address2.value = store.address2;
                app.getForm('singleshipping').object.shippingAddress.addressFields.cities.city.value = store.city;
                app.getForm('singleshipping').object.shippingAddress.addressFields.postal.value = store.postalCode;
                app.getForm('singleshipping').object.shippingAddress.addressFields.states.state.value = store.stateCode;
                app.getForm('singleshipping').object.shippingAddress.addressFields.country.value = store.countryCode.value;
            } else {
            	app.getController('Cart').ChangeDeliveryOptionInternal('shipped');
            	session.custom.deliveryOptionChoice = 'shipped';
            	session.forms.singleshipping.shippingAddress.shippingMethodID.value = cart.getDefaultShipment().getShippingMethodID();
            	
            	// If customer updates the zip code, old address line field is invalid
            	if (session.custom.customerZipUpdated && session.custom.customerZipUpdated == true){
            		
            		var shopForm = app.getForm('singleshipping');
            		
            		if (shopForm){
            			shopForm.object.shippingAddress.addressFields.address1.value = "";
            			shopForm.object.shippingAddress.addressFields.address2.value = "";
            		}	
            		session.custom.customerZipUpdated = false;
                    
            	}
            	
            }

            // Prepares shipments.
            //homeDeliveries = prepareShipments();
            //initEmailAddress(cart);
            var showMattressRemovalOption = pipeletHelper.showMatressRemovalOptionCheck(cart);
            var mattressRemovalInBasket = cart.getProductLineItems('DWREMOVEBED').size() > 0 ? true : false;
            Transaction.wrap(function () {
                cart.calculate();
            });
        	//commented because this is not setting up title of the page
            /*pageMeta = require('app_storefront_controllers/cartridge/scripts/meta');
            pageMeta.update({
                pageTitle: Resource.msg('singleshipping.meta.pagetitle', 'checkout', 'SiteGenesis Checkout')
            });*/
        	var pageMetaData = request.pageMetaData;
        	pageMetaData.title = Resource.msg('singleshipping.meta.pagetitle', 'checkout', null);
            
        	var payWithAmazon = false;
        	if (apController.SetCheckoutMethod()) {
        		payWithAmazon = true;
        	}
    		session.custom.amazonDeliveryPage = true;
    		app.getView({
                ContinueURL: URLUtils.https('COShipping-SingleShipping'),
                Basket: cart.object,
                HomeDeliveries: homeDeliveries,
                ShowMattressRemovalOption: showMattressRemovalOption,
                MattressRemovalInBasket: mattressRemovalInBasket,
                PayWithAmazon: payWithAmazon,
        		AmazonAuthError: amazonAuthError
            }).render('checkout/shipping/amazonsingleshipping');
        } else {
            //app.getController('COShippingMultiple').Start();
            return;
        }
    } else {
        app.getController('Cart').Show();
        return;
    }

}

/* Exports of the controller */
///**
// * @see {@link module:controllers/COAmazon~myFunction} */
/*
* Web exposed methods
*/
/** Starting point for the single shipping scenario.
 * @see module:controllers/COShipping~start */
exports.Start = guard.ensure(['https'], start);

