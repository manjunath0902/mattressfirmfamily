'use strict';

/**
 * Controller for Amazon Payments Custom API
 *
 * @module controllers/APNotification
 */

/* API Includes */
var Logger = require('dw/system/Logger');

/* Script Modules */
var app       = require('~/cartridge/scripts/app'),
    guard     = require('~/cartridge/scripts/guard'),
    amazonLib = require("../scripts/lib/amazonPaymentsLib.ds");


/**
 * Parse custom API response and call necessary controller
 */
function API() {
    var template           = 'api/error',
        apiError           = null,
        response           = null,
        apCheckAccessError = authorize();

    if (!apCheckAccessError) {
        var apiHelperObj = apiHelper();

        if (!apiHelperObj.apiError) {
            var controller = require('~/cartridge/controllers/AmazonPayments'),
                apiData    = apiHelperObj.apiData,
                result;

            switch (apiData.Action) {
                case 'Authorize' :
                    result = controller.Authorize(apiData);
                    break;
                case 'Capture' :
                    result = controller.Capture(apiData);
                    break;
                case 'Refund' :
                    result = controller.Refund(apiData);
                    break;
                case 'GetAuthorizationDetails' :
                    apiData.searchVal = apiData.AmazonAuthorizationId;
                    result            = controller.GetAuthorizationDetails(apiData);
                    break;
                case 'GetCaptureDetails' :
                    apiData.searchVal = apiData.AmazonCaptureId;
                    result            = controller.GetCaptureDetails(apiData);
                    break;
                case 'GetRefundDetails' :
                    apiData.searchVal = apiData.AmazonRefundId;
                    result            = controller.GetRefundDetails(apiData);
                    break;
                case 'CloseBillingAgreement' :
                    apiData.searchVal = apiData.AmazonBillingAgreementId;
                    result            = controller.CloseBillingAgreement(apiData);
                    break;
            }

            if (result.error) {
                apiError = result.errorMessage;
            } else {
                template = 'api/response';
            }
            response = result.response;
        } else {
            apiError = apiHelperObj.apiError;
        }
    } else {
        apiError = apCheckAccessError;
    }

    app.getView({
        APIError: apiError,
        Response: response
    }).render(template);
}

/**
 * Authorize user before access to custom API
 * @return {String} - Error message
 */
function authorize() {
    var amazonLibAPI = new amazonLib(),
        apiError     = null;

    // shared key will be presented in request
    var sharedKey = request.httpHeaders['x-shared-key'];

    if (sharedKey) {
        var apPassword  = amazonLibAPI.getAmazonCustomAPIPassword(),
            apSharedKey = amazonLibAPI.generateHashKey(apPassword);

        // hashed key in request header is the same as hashed password
        if (!amazonLibAPI.compareHashKeys(sharedKey, apSharedKey)) {
            apiError = 'Shared key presented in request is not right';
            Logger.error(apiError);
        }
    } else {
        apiError = 'Shared key is not presented in request';
        Logger.error(apiError);
    }

    return apiError;
}

/**
* Parse API request
* @return {Object}
*/
function apiHelper() {
    //API list
    var actions = ['Capture', 'Refund', 'GetRefundDetails', 'GetCaptureDetails', 'GetAuthorizationDetails', 'Authorize', 'CloseBillingAgreement'],
        apiProperties = {
            //Parameters that are required for Capture API
            Capture : ['AmazonAuthorizationId', 'CaptureReferenceId', 'CaptureAmount.Amount', 'CaptureAmount.CurrencyCode'],
            //Parameters that are required for Refund API
            Refund : ['AmazonCaptureId', 'RefundReferenceId', 'RefundAmount.Amount', 'RefundAmount.CurrencyCode'],
            //Parameters that are required for GetRefundDetails API
            GetRefundDetails : ['AmazonRefundId'],
            //Parameters that are required for GetCaptureDetails API
            GetCaptureDetails : ['AmazonCaptureId'],
            //Parameters that are required for GetAuthorizationDetails API
            Authorize : ['AmazonOrderReferenceId', 'AuthorizationReferenceId', 'AuthorizationAmount.Amount', 'AuthorizationAmount.CurrencyCode'],
            //Parameters that are required for Authorize API
            GetAuthorizationDetails : ['AmazonAuthorizationId'],
            //Parameters that are required for Authorize API
            CloseBillingAgreement : ['AmazonBillingAgreementId']
        },
        result = {
            apiError: null
        };

    try {
        var jsonData   = JSON.parse(request.httpParameterMap.getRequestBodyAsString()),
            actionType = jsonData.Action;
    } catch (e) {
        result.apiError = 'Please specify a valid JSON structure';
        Logger.error(result.apiError);
        return result;
    }

    if (actionType && (actions.indexOf(actionType) > -1)) {
        //JSON validation
        try {
            var propertiesList = apiProperties[actionType];

            for (var i = 0; i < propertiesList.length; i++){
                if (!jsonData.hasOwnProperty(propertiesList[i])){
                    throw new Error(propertiesList[i]);
                }
            }
            result.apiData = jsonData;
        } catch (e) {
            result.apiError = e.message + ' parameter is missed';
            Logger.error(result.apiError);
            return result;
        }
    }
    else {
        result.apiError = 'Action type is not valid';
        Logger.error(result.apiError);
    }

    return result;
}

/*
 * Web exposed methods
 */
 /**
 * @see module:controllers/APNotification~API */
exports.API = guard.ensure(['post'], API);
