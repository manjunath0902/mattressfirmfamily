'use strict';

/**
 * Controller for Amazon Payments business logic.
 *
 * @module  controllers/AmazonPayments
 */

/* API Includes */
var OrderMgr        = require('dw/order/OrderMgr'),
    Resource        = require('dw/web/Resource'),
    Site            = require('dw/system/Site'),
    CurrentSite     = Site.getCurrent(),
    Transaction     = require('dw/system/Transaction'),
    SystemObjectMgr = require('dw/object/SystemObjectMgr'),
    Template        = require('dw/util/Template'),
    Mail            = require('dw/net/Mail'),
    HashMap         = require('dw/util/HashMap'),
    URLUtils        = require('dw/web/URLUtils'),
	Pipelet 		= require('dw/system/Pipelet');
/* Script Modules */
var app                  = require('~/cartridge/scripts/app'), 
    guard                = require('~/cartridge/scripts/guard'),
    amazonPaymentsModule = require('~/cartridge/scripts/AmazonPaymentsModule');

/**
 * Get order reference details from Amazon
 */
function getOrderReferenceDetails() {
    var apAddress = null;

    if (!empty(session.forms.apShippingForm.addressConsentToken.value)) {
        var orderReferenceResult = getOrderReferenceResponse().orderReferenceResult;

        apAddress = orderReferenceResult.address;
    }

    let r = require('~/cartridge/scripts/util/Response');
    r.renderJSON(apAddress);

    return;
}

/**
 * Get billing agreement details from Amazon
 */
function getBillingAgreementDetails() {
    var apAddress = null;

    if (!empty(session.forms.apShippingForm.addressConsentToken.value)) {
        var billingAgreementResult = getBillingAgreementDetailsResponse().billingAgreementResult;

        apAddress = billingAgreementResult.address;

    }

    let r = require('~/cartridge/scripts/util/Response');
    r.renderJSON(apAddress);

    return;
}

/**
 * Store consent status in form
 */
function setConsentStatus() {
    session.forms.apShippingForm.consentStatus.value = request.httpParameterMap.consentStatus.stringValue;

    var responseObject = {
        success: true
    };

    let r = require('~/cartridge/scripts/util/Response');
    r.renderJSON(responseObject);
}


/**
 * Get order reference billing details from Amazon
 */
function getOrderReferenceBillingDetails() {
    var currentBasket        = dw.order.BasketMgr.getCurrentBasket(),
        orderReferenceResult = getOrderReferenceResponse().orderReferenceResult,
        billingAddressForm   = app.getForm('billing').object.billingAddress.addressFields,
        emailForm            = app.getForm('billing').object.billingAddress.email,
        amazonForm           = app.getForm('apShippingForm').object;

    amazonPaymentsModule.apCopyShippingAddressToForm(currentBasket, orderReferenceResult.apAddressBilling, billingAddressForm, emailForm, amazonForm);

    app.getView().render('checkout/billing/apaddressbillingupdate');
}

/**
 * Get billing agreement billing details from Amazon
 */
function getBillingAgreementBillingDetails() {
    var currentBasket          = dw.order.BasketMgr.getCurrentBasket(),
        billingAgreementResult = getBillingAgreementDetailsResponse().billingAgreementResult,
        billingAddressForm     = app.getForm('billing').object.billingAddress.addressFields,
        emailForm              = app.getForm('billing').object.billingAddress.email,
        amazonForm             = app.getForm('apShippingForm').object;

    amazonPaymentsModule.apCopyShippingAddressToForm(currentBasket, billingAgreementResult.apAddressBilling, billingAddressForm, emailForm, amazonForm);

    app.getView().render('checkout/billing/apaddressbillingupdate');
}
/**
 * Close the Amazon Billing Agreement
 * @param {Object} apiData
 * @return {Object}
 */
function closeBillingAgreement(apiData) {
    var Transaction              = require('dw/system/Transaction'),
        CustomerMgr              = require('dw/customer/CustomerMgr'),
        OrderMgr                 = require('dw/order/OrderMgr'),
        closeBillingAgreementObj = amazonPaymentsModule.closeBillingAgreement(apiData.searchVal, null);
    var result = {
            error: false,
            response: closeBillingAgreementObj.response
        }

    if (closeBillingAgreementObj.error) {
        result.error = true;
    } else {
        CustomerMgr.processProfiles(function(profile) {
            Transaction.wrap(function () {
                profile.custom.amazonBillingAgreementID = '';
            });
        }, "custom.amazonBillingAgreementID={0}",apiData.searchVal);
        
        var orders = OrderMgr.searchOrders("custom.amazonBillingAgreementID={0}", 'creationDate ASC',apiData.searchVal);
        
        if (orders) {
            while (orders.hasNext()) {
                var order = orders.next();
                Transaction.wrap(function(){
                    order.custom.amazonBillingAgreementActive = false;
                })
            }
        }
    }

    return result;
}
/**
 * Set the PayWithAmazon from the session to null and redirect to the homepage
 */
function logout() {
    session.custom.PayWithAmazon = null;
    session.custom.customerInfo  = null;
    session.custom.amzUserId     = null;
    session.custom.amzUserEmail  = null;

    response.redirect(URLUtils.url('Home-Show'));
}

/**
 * Used for Login Button redirect url
 */
function loginButtonRedirect() {
    initAmazonForms();

    var customerProfileInfo = getCustomerProfile(),
        amazonTokenError    = false,
        amazonEmailExists   = false,
        payWithAmazon       = false;

    session.custom.customerInfo = JSON.stringify(customerProfileInfo.customerInfo);
    session.custom.amzUserId    = customerProfileInfo.customerInfo.user_id;
    session.custom.amzUserEmail = customerProfileInfo.customerInfo.email;

    if (customerProfileInfo.endNode === 'error') {
        amazonTokenError = true;
    } else if (customerProfileInfo.endNode === 'login') {
        amazonEmailExists = true;
    }

    if (customerProfileInfo.endNode !== 'error') {
        session.forms.apShippingForm.email.value               = customerProfileInfo.customerInfo.email;
        session.forms.apShippingForm.addressConsentToken.value = request.httpParameterMap.access_token.stringValue;
        session.custom.PayWithAmazon                           = true;
        payWithAmazon                                          = true;
    }

    if (request.httpParameterMap.shipping.value) {
        response.redirect(URLUtils.https('COCustomer-Start', 'payWithAmazon', payWithAmazon, 'amazonTokenError', amazonTokenError, 'amazonEmailExists', amazonEmailExists));
    } else {
        if (customer.authenticated) {
            response.redirect(URLUtils.https('Account-Show'));
        } else {
            response.redirect(URLUtils.https('Login-Show', 'payWithAmazon', payWithAmazon, 'amazonTokenError', amazonTokenError, 'amazonEmailExists', amazonEmailExists));
        }
    }
    return;
}

/**
 * Used for Pay Button redirect url
 */
function payButtonRedirect() {
    if (!request.httpParameterMap.access_token.stringValue) {
        session.forms.apShippingForm.billingAgreementID.value = customer.profile.custom.amazonBillingAgreementID;
        session.forms.apShippingForm.orderReferenceID.value   = null;
        session.custom.amazonGuestCheckout                    = true;

        response.redirect(URLUtils.https('COShipping-Start'));
        return;
    }

    initAmazonForms();

    if (memberRegistration()) {
        shipping();
        return;
    }

    var customerProfileInfo        = getCustomerProfile(),
        amazonTokenError           = false,
        payWithAmazonAlreadyExists = false,
        payWithAmazon              = false;

    session.custom.customerInfo = JSON.stringify(customerProfileInfo.customerInfo);
    session.custom.amzUserId    = customerProfileInfo.customerInfo.user_id;
    session.custom.amzUserEmail = customerProfileInfo.customerInfo.email;

    if (customerProfileInfo.endNode === 'error') {
        amazonTokenError = true;
        response.redirect(URLUtils.https('COCustomer-Start', 'payWithAmazon', payWithAmazon, 'amazonTokenError', amazonTokenError, 'payWithAmazonAlreadyExists', payWithAmazonAlreadyExists));
        return;
    } else if (customerProfileInfo.endNode === 'login') {
        payWithAmazonAlreadyExists = true;
    }

    session.forms.apShippingForm.email.value               = customerProfileInfo.customerInfo.email;
    session.forms.apShippingForm.addressConsentToken.value = request.httpParameterMap.access_token.stringValue;

    if (!payWithAmazonAlreadyExists) {
        shipping();
    } else {
        session.custom.PayWithAmazon = true;
        payWithAmazon                = true;
        response.redirect(URLUtils.https('COCustomer-Start', 'payWithAmazon', payWithAmazon, 'amazonTokenError', amazonTokenError, 'payWithAmazonAlreadyExists', payWithAmazonAlreadyExists));
    }
}

/**
 * Used for Pay Button redirect url
 */
function editButtonRedirect() {
    initAmazonForms();

    session.custom.closeBA                                 = true;
    session.forms.apShippingForm.email.value               = !empty(session.custom.customerInfo) ? JSON.parse(session.custom.customerInfo).email : '';
    session.forms.apShippingForm.addressConsentToken.value = request.httpParameterMap.access_token.stringValue;

    shipping();
}

function memberRegistration() {
    var result = false;

    if (CurrentSite.getCustomPreferenceValue('amazonEnableMemberRegistration') && !customer.authenticated) {
        var customerInfo = amazonPaymentsModule.customerProfileAction(request.httpParameterMap.access_token.stringValue);

        if (!customerInfo.error) {
            session.custom.customerInfo = JSON.stringify(customerInfo.customerProfileInfo);
        }

        var Customer = app.getModel('Customer'),
            amazonCustomer = Customer.retrieveCustomerByLogin(customerInfo.customerProfileInfo.email);

        if (amazonCustomer && amazonCustomer.getProfile().custom.amazonAccount == true && amazonCustomer.getProfile().custom.amazonAccountId == customerInfo.customerProfileInfo.user_id) {
            result = false;
        } else {
            result = true;
        }

        if (result) {
            session.forms.apShippingForm.email.value               = customerInfo.customerProfileInfo.email;
            session.forms.apShippingForm.addressConsentToken.value = request.httpParameterMap.access_token.stringValue;
        }
    }
    return result;
}

/**
 * Store order reference id at session
 */
function setOrderReferenceID() {
    session.custom.apOrderReferenceID = request.httpParameterMap.orderID.stringValue;

    var responseObject = {
        success: true
    };

    let r = require('~/cartridge/scripts/util/Response');
    r.renderJSON(responseObject);
}

/**
 * Store billing agreement at session
 */
function setBillingAgreement() {
    session.custom.newBA = request.httpParameterMap.newBA.booleanValue;

    var responseObject = {
        success: true
    };

    let r = require('~/cartridge/scripts/util/Response');
    r.renderJSON(responseObject);
}

/**
 * Display Amazon Payments popup
 */
function popup() {
    app.getView().render('amazonpopup');
}

/**
 * Store Billing Agreement ID from profile custom attribute at form
 */
function storeOpenBillingAgreementID(cart) {
    var recurringPayment = require('~/cartridge/scripts/AmazonPaymentsModule').isRecurring(cart).isRecurring;

    if (recurringPayment && customer.authenticated && !empty(customer.profile.custom.amazonBillingAgreementID)) {
        var bAId = customer.profile.custom.amazonBillingAgreementID;

        if (bAId && (empty(session.forms.apShippingForm.orderReferenceID.value) || session.forms.apShippingForm.orderReferenceID.value == 'null')) {
            session.forms.apShippingForm.billingAgreementID.value = bAId;
        }
    }

}

/**
 * Check if the current payment instruments contain the amazon payment
 * @param {dw.util.Collection} paymentInstruments - A collection of the current payment instruments
 * @return {Boolean} true if current payment instruments contain the amazon payment method
 */
function checkAmazonPayment(paymentInstruments) {
    return amazonPaymentsModule.checkAmazonPayment(paymentInstruments);
}

/**
 * Used to Capture Gift Certificates in case user ordered them with Amazon
 * @param {dw.order.Order} order - The order containing the gift certificates
 * @param {String} orderReferenceID - Order reference ID
 * @param {String} billingAgreementID - Billing agreement ID
 * @return {Object}
 */
function captureGiftCertificates(order, orderReferenceID, billingAgreementID) {
    var result = {
        authorizeResult: null,
        response: null,
        error: false
    };

    try {
        var randomString   = amazonPaymentsModule.getRandomString(5),
            apCaptureGCObj = amazonPaymentsModule.apCaptureGiftCertificates(order, orderReferenceID, billingAgreementID, randomString);

        if (apCaptureGCObj.isGiftCapture) {
            var authorizeAction = amazonPaymentsModule.authorizeAction(true, null, null, apCaptureGCObj.authorizeData, order),
                authResult = authorizeAction.result,
                response = authorizeAction.response;

            if (!authorizeAction.error && amazonPaymentsModule.updateAuthorizationGiftCert(apCaptureGCObj.authorizeData, order)) {
                result.error = false;
            } else {
                result.error = true;
            }
        } else {
            result.error = true;
        }

        result.authorizeResult = authResult;
        result.response        = response;
    } catch (e) {
        result.error = true;
    }

    return result;
}

/**
 * Check if the gift certificates cover the full, partial or none of the order amount.
 * @param {dw.util.Collection} paymentInstruments - The payment instruments that contain the gift certificates
 * @param {Number} amount - The full order amount
 * @return {String} status - The status of the order cover
 */
function checkGiftCertificateStatus(paymentInstruments, amount) {
    return amazonPaymentsModule.getGiftCertificateStatus(paymentInstruments, amount);
}

/**
 * Validates an amazon token
 * @return {Boolean} true if amazon token is valid
 */
function checkToken() {
    var accessToken = request.httpParameterMap.access_token.stringValue || session.forms.apShippingForm.addressConsentToken.value;

    return amazonPaymentsModule.checkTokenAction(accessToken);
}

/**
 * Call order reference details service from Amazon
 * @param {Object} notificationData - Notifications object available from Amazon
 * @param {dw.order.Order} order - Current order
 * @return {Object}
 */
function getOrderReferenceResponse(notificationData, order) {
    var resultObj = {
            error: false
        },
        orderReferenceID    = '',
        addressConsentToken = '';

    if (notificationData) {
        orderReferenceID = notificationData.searchVal;
    } else {
        orderReferenceID    = !empty(request.httpParameterMap.orderReferenceID.stringValue) ? request.httpParameterMap.orderReferenceID.stringValue : session.forms.apShippingForm.orderReferenceID.value;
        addressConsentToken = session.forms.apShippingForm.addressConsentToken.value;
    }

    var orderReferenceAction = amazonPaymentsModule.orderReferenceAction(orderReferenceID, addressConsentToken);

    resultObj.orderReferenceResult = orderReferenceAction;

    if (orderReferenceAction.error) {
        resultObj.error = true;
    } else {
        if (notificationData) {
            var amazonOrder = OrderMgr.searchOrder('custom.amazonOrderReferenceID={0}', orderReferenceID);

            if (!amazonOrder || !amazonPaymentsModule.updateOrderReferenceStatus(orderReferenceAction.status, amazonOrder)) {
                resultObj.error = true;
            }
        } else {
            if (order) {
                Transaction.wrap(function() {
                    order.custom.amazonOrderReferenceStatus = orderReferenceAction.status.orderReferenceStatus;
                });
            }
        }
        var apZip = orderReferenceAction.address.postalCode.split('-');
    	app.getForm('singleshipping.shippingAddress.addressFields.postal').value = apZip[0];
    	session.forms.singleshipping.shippingAddress.addressFields.postal.value = apZip[0];
    	//saving postal code in billing form
    	app.getForm('billing.billingAddress.addressFields.postal').value = apZip[0];
    	session.forms.billing.billingAddress.addressFields.postal.value = apZip[0];
    	session.custom.customerZip = apZip[0];
    }
   
    return resultObj;
}

/**
 * Call billing agreement details service from Amazon
 * @param {Object} notificationData - Notifications object available from Amazon
 * @param {dw.order.Order} order - Current order
 * @return {Object}
 */
function getBillingAgreementDetailsResponse(notificationData, order) {
    var resultObj = {
            error: false
        },
        billingAgreementID  = '',
        addressConsentToken = '';

    if (notificationData) {
        billingAgreementID = notificationData.searchVal;
    } else {
        billingAgreementID  = session.forms.apShippingForm.billingAgreementID.value;
        addressConsentToken = session.forms.apShippingForm.addressConsentToken.value;
    }

    var billingAgreementAction = amazonPaymentsModule.billingAgreementAction(billingAgreementID, addressConsentToken);

    resultObj.billingAgreementResult = billingAgreementAction;

    if (billingAgreementAction.error) {
        resultObj.error = true;
    } else {
        if (notificationData) {
            var amazonOrders = OrderMgr.searchOrders('custom.amazonBillingAgreementID={0}','creationDate ASC', billingAgreementID);
            while (amazonOrders.hasNext()) {
                    var amazonOrder = amazonOrders.next();
                    if (!amazonOrder || !amazonPaymentsModule.updateBillingAgreementStatus(billingAgreementAction.status, amazonOrder)) {
                        resultObj.error = true;
                    }
                }

            if (!amazonOrder || !amazonPaymentsModule.updateBillingAgreementStatus(billingAgreementAction.status, amazonOrder)) {
                resultObj.error = true;
            }
        } else {
            if (order) {
                Transaction.wrap(function() {
                    order.custom.amazonBillingAgreementStatus = billingAgreementAction.status.billingAgreementStatus;
                });
            }
        }
    }
    return resultObj;
}

/**
 * Confirm the order reference
 * @return {Object}
 */
function confirmOrderReference() {
    var orderReferenceID = !empty(request.httpParameterMap.orderReferenceID.stringValue) ? request.httpParameterMap.orderReferenceID.stringValue : session.forms.apShippingForm.orderReferenceID.value,
        result           = amazonPaymentsModule.confirmOrderReferenceAction(orderReferenceID);

    return result;
}

/**
 * Confirm the billing agreement
 * @return {Object}
 */
function confirmBillingAgreement() {
    var billingAgreementID = !empty(request.httpParameterMap.billingAgreementID.stringValue) ? request.httpParameterMap.billingAgreementID.stringValue : session.forms.apShippingForm.billingAgreementID.value,
        result             = amazonPaymentsModule.confirmBillingAgreementAction(billingAgreementID);

    return result;
}

/**
 * Determine the error for a declined amazon authorizationObject
 * @param {Object} authorizationObject
 * @return {Object}
 */
function checkPaymentDeclined(authorizationObject) {
    return amazonPaymentsModule.checkValidAuth(authorizationObject);
}

/**
 * Get authorization details by Instant Payment Notifications
 * @param {String} amazonAuthorizationID
 * @param {Object} apiData
 * @return {Boolean}
 */
function getAuthorizationDetails(apiData) {
    var authDetailsObj = amazonPaymentsModule.getAuthorizationDetailsAction(apiData.searchVal),
        result = {
            error: false,
            response: authDetailsObj.response
        };

    if (authDetailsObj.error) {
        result.error = true;
    } else {
        var order = OrderMgr.searchOrder('custom.amazonAuthorizationID={0}', apiData.searchVal);

        if (order) {
            var updateAuthorizationStatus = amazonPaymentsModule.updateAuthorizationStatus(authDetailsObj.authorizationDetails, apiData, order);

            if (!updateAuthorizationStatus.error && updateAuthorizationStatus.sendMail && CurrentSite.getCustomPreferenceValue('amazonEnableDWInvalidPaymentEmail')) {
                var Email = app.getModel('Email'),
                    invalidpaymentmethodMail = Email.get('mail/invalidpaymentmethod', order.customerEmail);

                invalidpaymentmethodMail.setSubject(Resource.msg('error.invalidpaymentmethodfor', 'apcheckout', null) + ' ' + order.orderNo);
                invalidpaymentmethodMail.send({
                    Order: order
                });
            }
            amazonPaymentsModule.apStoreBillingAddress(order, authDetailsObj);
        }
    }

    return result;
}

/**
 * Encrypt and store encrypted password at account custom attribute
 * @param {String} password - Account password
 * @param {dw.customer.Customer} amazonCustomer
 * @return {String} Encrypted password
 */
function storeGeneratedAccountPassword(password, amazonCustomer) {
    var encryptedPassword = amazonPaymentsModule.encryptAccountPassword(password);

    if (encryptedPassword) {
        Transaction.wrap(function() {
            amazonCustomer.getProfile().custom.amazonGeneratedAccountPassword = encryptedPassword;
        });

        return password;
    } else {
        return null;
    }
}

/**
 * Send an account creation email
 * @param {Object} customerInfo
 * @param {String} password - Account password
 * @param {dw.customer.Customer} amazonCustomer
 */
function mailAccountCreation(customerInfo, password, amazonCustomer) {
    if (CurrentSite.getCustomPreferenceValue('amazonEnableDWAccountCreationEmail')) {
        var Email        = app.getModel('Email'),
            accountEmail = Email.get('mail/accountcreation', customerInfo.email);

        accountEmail.setSubject(Resource.msg('accountemail.subject', 'apemail', null));
        accountEmail.send({
            CustomerInfo: customerInfo,
            Password: password,
            Customer: amazonCustomer
        });
    }
    return;
}

/**
 * Create a new customer based on the data received form amazon
 * @return {String}
 */
function createCustomer(customerInfo) {
    var password       = amazonPaymentsModule.getRandomString(10),
        Customer       = app.getModel('Customer'),
        amazonCustomer = Customer.retrieveCustomerByLogin(customerInfo.email);
    password = password + '%';
    if (amazonCustomer) {
        if (amazonCustomer.getProfile().custom.amazonAccount == true && amazonCustomer.getProfile().custom.amazonAccountId == customerInfo.user_id) {
            password = amazonPaymentsModule.decryptAccountPassword(amazonCustomer.getProfile().custom.amazonGeneratedAccountPassword);
        } else {
            return 'login';
        }
    } else if (password) {
        
        Transaction.begin();

        amazonCustomer = Customer.createNewCustomer(customerInfo.email, password);

        if (amazonCustomer) {
            amazonCustomer.getProfile().firstName              = customerInfo.firstName;
            amazonCustomer.getProfile().lastName               = customerInfo.lastName;
            amazonCustomer.getProfile().email                  = customerInfo.email;
            amazonCustomer.getProfile().custom.amazonAccount   = true;
            amazonCustomer.getProfile().custom.amazonAccountId = customerInfo.user_id;

            Transaction.commit();

            password = storeGeneratedAccountPassword(password, amazonCustomer);

            mailAccountCreation(customerInfo, password, amazonCustomer.object);
        } else {
            Transaction.rollback();
            return 'error';
        }
        
    } else {
        return 'error';
    }

    if (customer.authenticated && amazonCustomer && (customer.getProfile().email != amazonCustomer.getProfile().email)) {
        return 'next';
    }

    /*if (!Customer.login(amazonCustomer.getProfile().email, password, false)) {
        return 'error';
    }*/
    return 'next';
}

/**
 * Get profile details from Amazon
 * @return {Object}
 */
function getCustomerProfile() {
    var result = {
        endNode: 'error'
    }
    if (checkToken()) {
        var accessToken  = request.httpParameterMap.access_token.stringValue || session.forms.apShippingForm.addressConsentToken.value,
            customerInfo = amazonPaymentsModule.customerProfileAction(accessToken);

        if (!customerInfo.error) {
            result.endNode      = createCustomer(customerInfo.customerProfileInfo);
            result.customerInfo = customerInfo.customerProfileInfo;
            session.custom.amazonCustomer = customerInfo.customerProfileInfo;
        }
    }
    return result;
}

/**
 * Copy shipping address to billing address for Amazon Payments
 * @param {dw.order.Basket} basket
 * @param {Object} billingAddressAmazon - The data grabbed from amazon
 */
function copyShippingAddressToBilllingAddress(basket, billingAddressAmazon) {
    var shippingAddress = basket.getDefaultShipment().getShippingAddress(),
        billingAddress  = basket.getBillingAddress();

    if (!billingAddress) {
        Transaction.wrap(function() {
            billingAddress = basket.createBillingAddress();
        });
    }

    amazonPaymentsModule.apCopyShippingAddressToBillingAddress(billingAddress, shippingAddress, billingAddressAmazon);

    Transaction.wrap(function() {
        basket.customerEmail = session.forms.billing.billingAddress.email.emailAddress.value;
    });
}

/**
 * Handles the selected shipping address and shipping method. Copies the
 * address details and gift options to the basket's default shipment. Sets the
 * selected shipping method to the default shipment.
 *
 * @param {module:models/CartModel~CartModel} cart - A CartModel wrapping the current Basket
 * @param {Object} shippingAddress - The data grabbed from amazon
 */
function handleShippingSettings(cart, shippingAddress) {
    Transaction.wrap(function() {
        var shipment = cart.getDefaultShipment(),
            giftOptions = session.forms.singleshipping.shippingAddress;

        if (amazonPaymentsModule.apCreateShipmentShippingAddress(shipment, shippingAddress, giftOptions)) {
            cart.updateShipmentShippingMethod(cart.getDefaultShipment().getID(), session.forms.singleshipping.shippingAddress.shippingMethodID.value, null, null);
            cart.calculate();

            cart.validateForCheckout();
        }
    });
}

/**
 * Call Amazon Authorize operation to reserve a specified amount
 * against the payment method stored in the order reference
 * @transactional
 * @return {Object} result -
 */
function authorize(apiData, order, customAmount) {
    var result = {
            error: false,
            errorMessage: null
        },
        customAPIFlow,
        sendMail;

    if (empty(order)) {
        var amazonOrder;

        if (apiData.AmazonBillingAgreementId) {
            amazonOrder = OrderMgr.searchOrder('custom.amazonBillingAgreementID={0}', apiData.AmazonBillingAgreementId);
        } else {
            amazonOrder = OrderMgr.searchOrder('custom.amazonOrderReferenceID={0}', apiData.AmazonOrderReferenceId);
        }

        if (amazonOrder) {
            order         = amazonOrder;
            customAPIFlow = true;
        } else {
            result.error        = true;
            result.errorMessage = Resource.msg('error.noOrdersFoundByOrderReference', 'apcheckout', null);

            return result;
        }
    } else {
        customAPIFlow = false;

        var referenceRandomPart = amazonPaymentsModule.getRandomString(5),
            currentForms        = session.forms,
            apiData             = amazonPaymentsModule.apPrepareAuthorizeObject(order, currentForms.apShippingForm.orderReferenceID.value, currentForms.apShippingForm.billingAgreementID.value, referenceRandomPart);
    }

    var alreadyAuthorizedObject = amazonPaymentsModule.checkAuthorizationProducts(order, apiData),
        alreadyAuthorized       = alreadyAuthorizedObject.alreadyAuthorized;

    if (alreadyAuthorizedObject.error) {
        result.error = true;

        return result;
    }

    if (alreadyAuthorized) {
        return result;
    }

    var authorizeActionObject = amazonPaymentsModule.authorizeAction(null, customAmount, customAPIFlow, apiData, order),
        captureNow            = authorizeActionObject.capture,
        response              = authorizeActionObject.response,
        authoriseResult       = authorizeActionObject.result;

    result.response = response;

    if (authorizeActionObject.error) {
        result.error        = true;
        result.errorMessage = authoriseResult;

        return result;
    }

    result.amazonAuthorizationId = authoriseResult.amazonAuthorizationId;
    result.amazonCaptureID       = authoriseResult.amazonCaptureID;
    result.authorizationStatus   = authoriseResult.authorizationStatus;
    result.reasonCode            = authoriseResult.reasonCode;

    var updateAuthorizationAttributesObject = amazonPaymentsModule.updateAuthorizationAttributes(captureNow, order, customAmount, apiData);

    if (updateAuthorizationAttributesObject.error) {
        result.error = true;

        return result;
    }

    if (customAPIFlow) {
        var updateAuthorizationStatusObject = amazonPaymentsModule.updateAuthorizationStatus(authoriseResult, apiData, order);

        if (updateAuthorizationStatusObject.error) {
            result.error = true;

            return result;
        }

        sendMail = updateAuthorizationAttributesObject.sendMail;
    }

    if (sendMail && CurrentSite.getCustomPreferenceValue('amazonEnableDWInvalidPaymentEmail')) {
        var mailFrom            = CurrentSite.getCustomPreferenceValue('customerServiceEmail'),
            mailSubject         = Resource.msg('error.invalidpaymentmethodfor', 'apcheckout', null) + ' ' + order.orderNo,
            mailTemplate        = 'mail/invalidpaymentmethod',
            mailTo              = order.customerEmail,
            mailTemplateHashMap = new HashMap();

        mailTemplateHashMap.put('MailSubject', mailSubject);
        mailTemplateHashMap.put('Order', order);

        send(mailFrom, mailSubject, mailTemplate, mailTo, mailTemplateHashMap);
    }

    return result;
}

/**
 * @fixme Add possible changes when get to pipelines that are calling current method
 * This method is used to get the capture details
 * @param apiData
 * @param isSplitOrder
 * @param orderAmount
 * @return {*}
 */
function capture(apiData, isSplitOrder, orderAmount) {
    var checkCaptureProductsObject = amazonPaymentsModule.checkCaptureProducts(apiData),
        alreadyCaptured            = checkCaptureProductsObject.alreadyCaptured,
        result = {
            error: false
        };

    if (checkCaptureProductsObject.error) {
        result.error = true;

        return result;
    }

    if (alreadyCaptured) {
        return result;
    }

    if (!empty(isSplitOrder) && isSplitOrder) {
        apiData['CaptureAmount.Amount'] = orderAmount.getValue();
    }

    var requestCaptureActionObject = amazonPaymentsModule.requestCaptureAction(apiData),
        apCaptureInfo              = requestCaptureActionObject.result,
        response                   = requestCaptureActionObject.response;

    result.response = response;

    if (requestCaptureActionObject.error) {
        result.error = true;

        return result;
    }

    var updateOrderStatusObject = updateOrderStatus(apiData, apCaptureInfo);

    if (updateOrderStatusObject.error) {
        result = updateOrderStatusObject;
        return result;
    }

    if (apCaptureInfo.captureStatus == 'Completed') {
        if (CurrentSite.getCustomPreferenceValue('amazonEnableDWCompletionEmail')) {
            mailOrderComplete(updateOrderStatusObject.apOrder);
        }
    }

    return result;
}

/**
 * This pipeline is used to update the order status with the data from the capture response.
 * @param apiData
 * @param apCaptureInfo
 */
function updateOrderStatus(apiData, apCaptureInfo) {
    var apOrder = OrderMgr.searchOrder('custom.amazonAuthorizationID LIKE {0}', '*' + apiData.AmazonAuthorizationId.split(',')[0] + '*'),
        resultSuccess = {
            error: false,
            apOrder: apOrder
        },
        resultError = {
            response: Resource.msg('error.updateOrderFailed', 'apcheckout', null),
            error: true
        };

    if (apOrder) {
        var updateCaptureAttributesObject = amazonPaymentsModule.updateCaptureAttributes(apiData, apOrder);

        if (updateCaptureAttributesObject.error) {
            return resultError;
        }

        var updateOrderStatusObject = amazonPaymentsModule.updateOrderStatus(apCaptureInfo.captureID, apCaptureInfo.captureStatus, apOrder);

        if (updateOrderStatusObject.error) {
            return resultError;
        }

        return resultSuccess;
    }

    return resultError;
}

/**
 * This method is used to send the order complete mail for a specified order.
 * @param apOrder
 */
function mailOrderComplete(apOrder) {
    var order               = apOrder,
        mailFrom            = CurrentSite.getCustomPreferenceValue('customerServiceEmail'),
        mailSubject         = Resource.msg('email.completion.subject', 'apcheckout', null) + ' ' + order.orderNo,
        mailTemplate        = 'mail/ordercomplete',
        mailTo              = order.customerEmail,
        mailTemplateHashMap = new HashMap();

    mailTemplateHashMap.put('MailSubject', mailSubject);
    mailTemplateHashMap.put('Order', order);

    send(mailFrom, mailSubject, mailTemplate, mailTo, mailTemplateHashMap);
}

/**
 * This method is for sending of mail.
 * To use this pipeline, the following variables can't be null: MailFrom, MailSubject, MailTemplate & MailTo.
 * @param {String} mailFrom - the email address to use as the from address for the email.
 * @param {String} mailSubject - the subject of the email.
 * @param {String} mailTemplate - file system path to the ISML template
 * @param {String} mailTo - the to address List where the email is sent.
 * @param {dw.util.HashMap} mailTemplateHashMap
 */
function send(mailFrom, mailSubject, mailTemplate, mailTo, mailTemplateHashMap) {
    var mail     = new Mail(),
        template = new Template(mailTemplate),
        content  = template.render(mailTemplateHashMap);

    if (!empty(mailFrom) && !empty(mailSubject) && !empty(mailTemplate) && !empty(mailTo)) {
        return;
    }

    mail.addTo(mailTo);
    mail.setFrom(mailFrom);
    mail.setSubject(mailSubject);
    mail.setContent(content);

    mail.send();
}

/**
 * Check if Amazon Widgets should be displayed at checkout page
 * @return {Boolean} payWithAmazon
 */
function setCheckoutMethod() {
    var payWithAmazon = amazonPaymentsModule.apSetCheckoutMethod();

    if (payWithAmazon) {
        session.custom.PayWithAmazon = payWithAmazon;
    }

    return payWithAmazon;
}

/**
 * Init amazon forms
 */
function initAmazonForms() {
    app.getForm('apShippingForm').clear();

    if (request.httpParameterMap.billingAgreementID.stringValue) {
        session.forms.apShippingForm.billingAgreementID.value = request.httpParameterMap.billingAgreementID.value;
    } else if (request.httpParameterMap.orderReferenceID.stringValue) {
        session.forms.apShippingForm.orderReferenceID.value = request.httpParameterMap.orderReferenceID.value;
    }

    if (session.custom.closeBA) {
        delete session.custom.closeBA;
    }
}

/**
 * Redirect user to shipping page for Amazon Payment
 */
function shipping() {
    var accessToken = request.httpParameterMap.access_token.stringValue;

    if (accessToken) {
        session.forms.apShippingForm.addressConsentToken.value = accessToken;
        session.custom.amazonGuestCheckout                     = true;
    }

    response.redirect(URLUtils.https('COShipping-Start'));
}

/**
 * This method is used to close the order reference. Before an order can be closed we need to confirm it.
 * @param {dw.order.Order} apOrder - Order
 * @return {*} - object of 2 attributes: error and apCloseResult
 */
function closeOrderReference(apOrder) {
    return amazonPaymentsModule.closeOrderReference(apOrder.custom.amazonOrderReferenceID);
}

/**
 * Update amazon customer password
 * @return {String}
 */
function setCustomerAmazonPassword(password) {
    return storeGeneratedAccountPassword(password, customer);
}

/**
 * Update capture status of the order. If the status is complete it will send a confirmation mail and close the order reference
 * @param {Object} notificationData
 * @return {Object}
 */
function getCaptureStatus(notificationData) {
    var captureDetailsObj = getCaptureDetails(notificationData),
        result            = true;

    if (!captureDetailsObj.error) {
        if (captureDetailsObj.captureDetailsInfo.captureStatus == 'Completed') {
            var checkSplitOrderCapture = amazonPaymentsModule.checkSplitOrderCapture(notificationData);

            if (checkSplitOrderCapture.error) {
                result = false;
            } else if (checkSplitOrderCapture.fullyCaptured) {
                var closeOrderReferenceObj = closeOrderReference(captureDetailsObj.apOrder);

                if (closeOrderReferenceObj.error) {
                    result = false;
                } else if (CurrentSite.getCustomPreferenceValue('amazonEnableDWCompletionEmail')) {
                    mailOrderComplete(captureDetailsObj.apOrder);
                }
            }
        }
    } else {
        result = false;
    }

    return result;
}

/**
 * Get the details about a capture based on Amazon Capture ID and updates the order with the new status
 * @param {Object} notificationData
 * @return {Object}
 */
function getCaptureDetails(notificationData) {
    var captureDetailsObj = amazonPaymentsModule.captureDetailsAction(notificationData.searchVal),
        resultObj = {
            error: false,
            response: captureDetailsObj.response
        };

    if (captureDetailsObj.error) {
        resultObj.error = true;
    } else {
        resultObj.captureDetailsInfo = captureDetailsObj.result;
        var captureStatusObj = updateCaptureStatus(notificationData.searchVal, captureDetailsObj.result);

        if (captureStatusObj.error) {
            resultObj.error = true;
            resultObj.response = captureStatusObj.response;
        } else {
            resultObj.apOrder = captureStatusObj.apOrder;
        }
    }

    return resultObj;
}

/**
 * Update the capture status with the data from the capture response.
 * @param {String} amazonCaptureId
 * @param {Object} apCaptureInfo
 * @return {Object}
 */
function updateCaptureStatus(amazonCaptureId, apCaptureInfo) {
    var apOrder = OrderMgr.searchOrder('custom.amazonCaptureID LIKE {0}', '*' + amazonCaptureId + '*'),
        resultObj = {
            error: false
        };

    if (apOrder) {
        var updateOrderStatusObject = amazonPaymentsModule.updateOrderStatus(apCaptureInfo.captureID, apCaptureInfo.captureStatus, apOrder);

        if (updateOrderStatusObject.error) {
            resultObj.error    = true;
            resultObj.response = Resource.msg('error.updateOrderFailed', 'apcheckout', null);
        }
        resultObj.apOrder = apOrder;
    } else {
        resultObj.error    = true;
        resultObj.response = Resource.msg('error.updateOrderFailed', 'apcheckout', null);
    }

    return resultObj;
}

/**
 * Update the refunded order status
 * @param {String} amazonRefundID
 * @param {Object} apRefundDetails
 * @return {Object}
 */
function updateRefundedOrderStatus(amazonRefundID, apRefundDetails) {
    var apOrder   = OrderMgr.searchOrder('custom.amazonRefundID LIKE {0}', '*' + amazonRefundID + '*'),
        resultObj = {
            error: false
        };

    if (apOrder) {
        if (!amazonPaymentsModule.updateRefundOrderStatus(apOrder, apRefundDetails)) {
            resultObj.error    = true;
            resultObj.response = Resource.msg('error.updateOrderFailed', 'apcheckout', null);
        } else if (apRefundDetails.refundStatus == 'Completed' && CurrentSite.getCustomPreferenceValue('amazonEnableDWRefundEmail')) {
            var Email                  = app.getModel('Email'),
                refundconfirmationMail = Email.get('mail/refundconfirmation', apOrder.customerEmail);

            refundconfirmationMail.setSubject(Resource.msg('email.refund.subject', 'apcheckout', null) + ' ' + apOrder.orderNo);
            refundconfirmationMail.send({
                Order: apOrder,
                apRefundDetails: apRefundDetails
            });
        }
        resultObj.apOrder = apOrder;
    } else {
        resultObj.error    = true;
        resultObj.response = Resource.msg('error.updateOrderFailed', 'apcheckout', null);
    }

    return resultObj;
}

/**
 * Call the GetRefundDetails service
 * @param {Object} notificationData
 * @return {Object}
 */
function getRefundDetails(notificationData) {
    var refundDetailsObj = amazonPaymentsModule.getRefundDetailsAction(notificationData.searchVal),
        result = {
            error: false,
            response: refundDetailsObj.response
        }

    if (refundDetailsObj.error) {
        result.error = true;
    } else {
        var refundedOrderStatusObj = updateRefundedOrderStatus(notificationData.searchVal, refundDetailsObj.result);

        if (refundedOrderStatusObj.error) {
            result.error    = true;
            result.response = refundedOrderStatusObj.response;
        }
    }

    return result;
}

/**
 * Call the refund service
 * @param {Object} apiData
 * @return {Object}
 */
function refund(apiData) {
    var apRefundInfo = amazonPaymentsModule.refundAction(apiData),
        refundResult = {
            error: false,
            response: apRefundInfo.response
        };

    if (!apRefundInfo.error) {
        var apOrder = OrderMgr.searchOrder('custom.amazonCaptureID LIKE {0}', '*' + apiData.AmazonCaptureId + '*');

        if (apOrder) {
            Transaction.wrap(function() {
                apOrder.custom.amazonRefundID     = apRefundInfo.result.refundID;
                apOrder.custom.amazonRefundStatus = apRefundInfo.result.refundStatus;
            });

            var refundedOrderStatusInfo = updateRefundedOrderStatus(apRefundInfo.result.refundID, apRefundInfo.result);

            if (refundedOrderStatusInfo.error) {
                refundResult.error    = true;
                refundResult.response = refundedOrderStatusInfo.response;
            }
        } else {
            refundResult.error = true;
        }
    } else {
        refundResult.error = true;
    }

    return refundResult;
}

/**
 * Checks if the Current Order has back ordered or pre oredered products.
 * If it has and the product has late authorization attribute enabled, the Authorization is skipped.
 *
 * @param {dw.order.Order} order
 * @return {Boolean}
 */
function deferredPayments(order) {
    var preorder    = dw.catalog.ProductAvailabilityModel.AVAILABILITY_STATUS_PREORDER,
        backorder   = dw.catalog.ProductAvailabilityModel.AVAILABILITY_STATUS_BACKORDER,
        lineItemItr = order.allProductLineItems.iterator(),
        productLineItem;

    while (lineItemItr.hasNext()) {
        productLineItem = lineItemItr.next();
        if (!empty(productLineItem.product)) {
            var productStatus = productLineItem.product.availabilityModel.availabilityStatus,
                lateAuthorization = productLineItem.product.custom.amazonLateAuthorization;

            if ((productStatus == backorder || productStatus == preorder) && lateAuthorization == true) {
                Transaction.wrap(function() {
                    order.custom.amazonDeferredOrder = true;

                    if (session.forms.apShippingForm.billingAgreementID.value !== 'null' && session.forms.apShippingForm.orderReferenceID.value == 'null') {
                        order.custom.amazonBillingAgreementID = session.forms.apShippingForm.billingAgreementID.value;
                    } else {
                        order.custom.amazonOrderReferenceID = session.forms.apShippingForm.orderReferenceID.value;
                    }

                    order.addNote('Deferred Payment', 'Your order contains products eligible for deferred payments.');
                });

                return true;
            }
        }
    }

    return false;
}

/**
 * Check if there are any split payment products in the order
 *
 * @param {dw.order.Order} order
 * @return {Object}
 */
function splitPayment(order) {
    var splitPaymentObj = amazonPaymentsModule.splitPayment(order),
        result          = {};

    if (splitPaymentObj.error) {
        result.error = true;
    } else {
        Transaction.wrap(function() {
            if (session.forms.apShippingForm.billingAgreementID.value !== 'null' && session.forms.apShippingForm.orderReferenceID.value == 'null') {
                order.custom.amazonBillingAgreementID = session.forms.apShippingForm.billingAgreementID.value;
            } else {
                order.custom.amazonOrderReferenceID   = session.forms.apShippingForm.orderReferenceID.value;
            }
        });

        if ((splitPaymentObj.isSplitOrder && (splitPaymentObj.orderAmount.getValue() == 0))) {
            result.skip = true;
        }

        result.isSplitOrder = splitPaymentObj.isSplitOrder;
        result.orderAmount  = splitPaymentObj.orderAmount;
    }

    return result;
}
/**
 * Update the billing address based on the shipping address or on the apAddress captured from amazon
 *
 * @param {dw.order.Basket} basket
 * @param {Object} billingAddressAmazon
 */
function updateBillingAddress(basket, billingAddressAmazon) {
    if (basket.productLineItems.size() == 0) {
        var isRecurring = !empty(session.forms.apShippingForm.billingAgreementID.value) && session.forms.apShippingForm.billingAgreementID.value !== 'null',
            apResponse = isRecurring ? getBillingAgreementDetailsResponse() : getOrderReferenceResponse();

        if (!apResponse.error) {
            var apResult         = isRecurring ? apResponse.billingAgreementResult : apResponse.orderReferenceResult,
                apAddress        = apResult.address,
                apAddressBilling = apResult.apAddressBilling,
                billingAddress   = basket.getBillingAddress();

            if (!billingAddress) {
                Transaction.wrap(function() {
                    billingAddress = basket.createBillingAddress();
                });
            }

            amazonPaymentsModule.apSetBillingAddress(apAddress, billingAddress, apAddressBilling);

            Transaction.wrap(function() {
                basket.customerEmail = session.forms.billing.billingAddress.email.emailAddress.value;
            });
        }
    } else {
        copyShippingAddressToBilllingAddress(basket, billingAddressAmazon);
    }
}

/**
 * Order summary logic:
 * Checks for constraints, sets order reference, sets flags for deferred/split payments.
 *
 * @param {dw.order.Basket} basket
 * @return {Object}
 */
function summary(basket, paymentAmount, amazonAuthError) {
    var paymentInstruments = basket.paymentInstruments,
        result             = {};

    if (checkAmazonPayment(paymentInstruments)) {
        result.isAmazonPaymentMethod = true;
        result.gcStatus = checkGiftCertificateStatus(paymentInstruments, paymentAmount);

        var isRecurring = !empty(session.forms.apShippingForm.billingAgreementID.value) && session.forms.apShippingForm.billingAgreementID.value !== 'null';
        var useOpenBA   = isRecurring && customer.authenticated && customer.profile.custom.amazonBillingAgreementID && customer.profile.custom.amazonBillingAgreementID == session.forms.apShippingForm.billingAgreementID.value;


        if (!useOpenBA && session.forms.apShippingForm.orderReferenceID.value == 'null' && (empty(session.forms.apShippingForm.consentStatus.value) || session.forms.apShippingForm.consentStatus.value == 'false')) {
            var orderRef = amazonPaymentsModule.createOrderReferenceForIdAction(session.forms.apShippingForm.billingAgreementID.value);
            session.forms.apShippingForm.orderReferenceID.value   = orderRef.createOrderReferenceForIdResult.orderReferenceId;
            session.forms.apShippingForm.billingAgreementID.value = null;
            isRecurring = false;
        }

        if (empty(amazonAuthError) || (amazonAuthError.code != 'InvalidPaymentMethod')) {
            if (isRecurring) {
                var apResponse = getBillingAgreementDetailsResponse().billingAgreementResult;

                if (apResponse.status.billingAgreementStatus == 'Draft') {
                    var billingAgreementDetails = amazonPaymentsModule.setBillingAgreementDetails(null);
                }
                session.custom.amazonBillingAgreementActive = true;
            } else {
                var orderRefDetails = amazonPaymentsModule.setOrderReferenceDetails(basket, null);
                session.custom.amazonBillingAgreementActive = null;
            }
        }

        var apResponse = isRecurring ? getBillingAgreementDetailsResponse().billingAgreementResult : getOrderReferenceResponse().orderReferenceResult;

        if (isRecurring && (empty(session.forms.apShippingForm.orderReferenceID.value) || session.forms.apShippingForm.orderReferenceID.value == 'null')) {
            var remainingBalance = apResponse.remainingBalance.amount,
                orderAmount      = amazonPaymentsModule.getNonGiftCertificateAmount(basket);

            if (orderAmount > remainingBalance) {
                result.remainingBalance = remainingBalance;
                return result;
            }
        }

        result.apConstraints = apResponse.constraints;

        updateBillingAddress(basket, apResponse.apAddressBilling);
    }

    var deferredPaymentsObj = amazonPaymentsModule.deferredPayment(basket);

    result.billingAgreementDetails  = billingAgreementDetails;
    result.isDeferredPayment        = deferredPaymentsObj.isDeferredPayment;
    result.containsDeferredProducts = deferredPaymentsObj.containsDeferredProducts;
    result.isSplitPayment           = amazonPaymentsModule.checkSplitPayment(basket);

    return result;
}

/**
 * Make place order operations for amazon payments
 *
 * @param {dw.order.Order} order
 * @param {dw.value.Money} paymentAmount
 * @return {Object}
 */
function placeOrder(order, paymentAmount) {
    var paymentInstruments = order.paymentInstruments,
        gcStatus           = checkGiftCertificateStatus(paymentInstruments, paymentAmount),
        result = {
            error: false
        },
        timeout            = true,
        isRecurring        = !empty(session.forms.apShippingForm.billingAgreementID.value) && session.forms.apShippingForm.billingAgreementID.value !== 'null',
        useOpenBA          = isRecurring && customer.authenticated && customer.profile.custom.amazonBillingAgreementID && customer.profile.custom.amazonBillingAgreementID == session.forms.apShippingForm.billingAgreementID.value;


    if (!useOpenBA && session.forms.apShippingForm.orderReferenceID.value == 'null' && (empty(session.forms.apShippingForm.consentStatus.value) || session.forms.apShippingForm.consentStatus.value == 'false')) {
        isRecurring = false;
    }

    if (gcStatus !== 'full') {
        var apConfirmObj = isRecurring ? confirmBillingAgreement() : confirmOrderReference();

        if (apConfirmObj.error) {
            result.error = true;
            result.amazonResultError = isRecurring ? apConfirmObj.confirmBillingAgreementResult : apConfirmObj.confirmOrderReferenceResult;
            return result;
        } else {
            if (isRecurring) {
                while (timeout) {
                    timeout = false;
                    var validationRes = amazonPaymentsModule.validateBillingAgreement(session.forms.apShippingForm.billingAgreementID.value);

                    if (validationRes.error) {
                        result.error = true;
                        result.amazonAuthError = validationRes.authError;
                        return result;
                    } else if (validationRes.timeout) {
                        timeout = true;
                    }
                }

                if (!useOpenBA && session.custom.closeBA) {
                    amazonPaymentsModule.closeBillingAgreement(customer.profile.custom.amazonBillingAgreementID);
                    delete session.custom.closeBA;
                }
            }

           // var orderRefResponse = isRecurring ? getBillingAgreementDetailsResponse(null, order) : getOrderReferenceResponse(null, order);

            if (!deferredPayments(order)) {
                var splitPaymentObj = splitPayment(order);

                if (splitPaymentObj.error) {
                    result.error = true;
                    return result;
                } else if (!splitPaymentObj.skip) {
                    var orderAmount = amazonPaymentsModule.setOrderCustomAmount(splitPaymentObj.isSplitOrder, splitPaymentObj.orderAmount, order);

                    if (orderAmount.getValue() > 0) {
                        var customAmount = orderAmount.getValue().toString();
                        timeout = true;

                        while (timeout) {
                            timeout = false;
                            var authorizeObj = authorize(null, order, customAmount);

                            if (authorizeObj.error) {
                                result.error                 = true;
                                result.amazonResultError     = authorizeObj.errorMessage;
                                session.custom.amzForceAsync = null;
                                return result;
                            } else {
                                var notificationData = {};
                                notificationData.searchVal = authorizeObj.amazonAuthorizationId.toString();

                                var authDetailsObj         = getAuthorizationDetails(notificationData);

                                var paymentDeclinedObj     = checkPaymentDeclined(authorizeObj);

                                if (paymentDeclinedObj.error) {
                                    result.error             = true;
                                    result.amazonResultError = Resource.msg('error.authorizationDeclined', 'apcheckout', null);
                                    result.amazonAuthError   = paymentDeclinedObj.authError;
                                    return result;
                                } else if (paymentDeclinedObj.timeout) {
                                    timeout = true;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    timeout = true;

    while (timeout) {
        timeout = false;
        if (order.getGiftCertificateLineItems().size() > 0) {
            var captureGCObj = captureGiftCertificates(order, session.forms.apShippingForm.orderReferenceID.value, session.forms.apShippingForm.billingAgreementID.value);

            if (captureGCObj.error) {
                result.error = true;
                result.amazonResultError = captureGCObj.authorizeResult;
                session.custom.amzForceAsync = null;
                return result;
            } else {
                var paymentDeclinedObj = checkPaymentDeclined(captureGCObj.authorizeResult);

                if (paymentDeclinedObj.error) {
                    result.error             = true;
                    result.amazonResultError = Resource.msg('error.authorizationDeclined', 'apcheckout', null);
                    result.amazonAuthError   = result.authError;
                    return result;
                } else if (paymentDeclinedObj.timeout) {
                    timeout = true;
                }
            }
        }
    }

    return result;

}

/**
 * Render the Recurring Billing Order History page
 *
 */
function recurringBillingOrderHistory() {
    var PagingModel = require('dw/web/PagingModel'),
        ContentMgr  = require('dw/content/ContentMgr');

    var orders = OrderMgr.searchOrders('customerNo={0} AND status!={1} AND custom.amazonBillingAgreementID != null', 'creationDate desc', customer.profile.customerNo, dw.order.Order.ORDER_STATUS_REPLACED);
    
    var parameterMap     = request.httpParameterMap,
        pageSize         = parameterMap.sz.intValue || 5,
        start            = parameterMap.start.intValue || 0,
        orderPagingModel = new PagingModel(orders, orders.count);
    
    orderPagingModel.setPageSize(pageSize);
    orderPagingModel.setStart(start);

    var orderListForm = app.getForm('orders.orderlist');
    orderListForm.invalidate();
    orderListForm.clear();
    orderListForm.copyFrom(orderPagingModel.pageElements);

    var pageMeta = require('~/cartridge/scripts/meta');
    pageMeta.update(ContentMgr.getContent('myaccount-orderhistory'));

    app.getView({
        OrderPagingModel: orderPagingModel,
        ContinueURL: dw.web.URLUtils.https('Order-Orders')
    }).render('account/orderhistory/recurringbillingorders');
}


/**
 * Display popup to cancel billing agreement on orderhistory page
 */
function endRecurringBillingPopup() {
    app.getView({
        BillingAgreementID: request.httpParameterMap.baid.value
    }).render('account/orderhistory/apcancelorder');
}

/**
 * Calls closeBillingAgreement and returns the result status as json
 */
function endRecurringBilling() {
    var closeBillingAgreementResult = closeBillingAgreement({
        searchVal : request.httpParameterMap.baid.value
    });

    if (closeBillingAgreementResult.error) {
        app.getView({
            ErrorCode : dw.util.StringUtils.encodeString(closeBillingAgreementResult.response, dw.util.StringUtils.ENCODE_TYPE_HTML)
        }).render('util/errorjson');
    } else {
        app.getView().render('util/successjson');
    }
}

/*
 * Module exports
 */

/*
 * Web exposed methods
 */
/** Get order reference details from Amazon
 * @see module:controllers/AmazonPayments~getOrderReferenceDetails */
exports.GetOrderReferenceDetails = guard.ensure(['get'], getOrderReferenceDetails);
/** Get billing agreement details from Amazon
 * @see module:controllers/AmazonPayments~getBillingAgreementDetails */
exports.GetBillingAgreementDetails = guard.ensure(['get'], getBillingAgreementDetails);
/** Get order reference billing details from Amazon
 * @see module:controllers/AmazonPayments~getOrderReferenceBillingDetails */
exports.GetOrderReferenceBillingDetails = guard.ensure(['get'], getOrderReferenceBillingDetails);
/** Get billing agreement billing details from Amazon
 * @see module:controllers/AmazonPayments~getBillingAgreementBillingDetails */
exports.GetBillingAgreementBillingDetails = guard.ensure(['get'], getBillingAgreementBillingDetails);

/** Set the PayWithAmazon from the session to null and redirect to the homepage
 * @see module:controllers/AmazonPayments~logout */
exports.Logout = guard.all(logout);
/**
 * @see module:controllers/AmazonPayments~loginButtonRedirect */
exports.LoginButtonRedirect = guard.ensure(['get'], loginButtonRedirect);
/**
 * @see module:controllers/AmazonPayments~payButtonRedirect */
exports.PayButtonRedirect = guard.ensure(['get'], payButtonRedirect);
/**
 * @see module:controllers/AmazonPayments~editButtonRedirect */
exports.EditButtonRedirect = guard.ensure(['get'], editButtonRedirect);
/**
 * @see module:controllers/AmazonPayments~setOrderReferenceID */
exports.SetOrderReferenceID = guard.ensure(['post'], setOrderReferenceID);
 /**
  * @see module:controllers/AmazonPayments~setBillingAgreement */
exports.SetBillingAgreement = guard.ensure(['post'], setBillingAgreement);
/**
 * @see module:controllers/AmazonPayments~setConsentStatus */
exports.SetConsentStatus = guard.ensure(['post'], setConsentStatus);


/**
 * @see module:controllers/AmazonPayments~popup */
exports.Popup = guard.ensure(['get'], popup);

/** Render the Recurring Billing Order History page
 * @see module:controllers/AmazonPayments~recurringBillingOrderHistory */
exports.RecurringBillingOrderHistory = guard.ensure(['get'], recurringBillingOrderHistory);

/** Render End Recurring Billing page
 * @see module:controllers/AmazonPayments~endRecurringBilling */
exports.EndRecurringBilling = guard.ensure(['get'], endRecurringBilling);
/**
 * @see module:controllers/AmazonPayments~endRecurringBillingPopup */
exports.EndRecurringBillingPopup = guard.ensure(['get'], endRecurringBillingPopup);
/*
 * Local methods
 */
/** Check if the current payment instruments contain the amazon payment
 * @see module:controllers/AmazonPayments~checkAmazonPayment */
exports.CheckAmazonPayment = checkAmazonPayment;
/**
 * @see module:controllers/AmazonPayments~storeOpenBillingAgreementID */
exports.StoreOpenBillingAgreementID = storeOpenBillingAgreementID;
/** Call order reference details service from Amazon
 * @see module:controllers/AmazonPayments~getOrderReferenceResponse */
exports.GetOrderReferenceResponse = getOrderReferenceResponse;
/** Call billing agreement details service from Amazon
 * @see module:controllers/AmazonPayments~getBillingAgreementDetailsResponse */
exports.GetBillingAgreementDetailsResponse = getBillingAgreementDetailsResponse;
/** Get authorization details by Instant Payment Notifications
 * @see module:controllers/AmazonPayments~getAuthorizationDetails */
exports.GetAuthorizationDetails = getAuthorizationDetails;
/** Get profile details from Amazon
 * @see module:controllers/AmazonPayments~getCustomerProfile */
exports.GetCustomerProfile = getCustomerProfile;
/** Handles the selected shipping address and shipping method
 * @see module:controllers/AmazonPayments~handleShippingSettings */
exports.HandleShippingSettings = handleShippingSettings;
/** Call Amazon Authorize operation
 * @see module:controllers/AmazonPayments~authorize */
exports.Authorize = authorize;
/** Set Checkout Method
 * @see module:controllers/AmazonPayments~setCheckoutMethod */
exports.SetCheckoutMethod = setCheckoutMethod;
/** Store encrypted password at account custom attribute
 * @see module:controllers/AmazonPayments~storeGeneratedAccountPassword */
exports.StoreGeneratedAccountPassword = storeGeneratedAccountPassword;
/** Send an account creation email
 * @see module:controllers/AmazonPayments~mailAccountCreation */
exports.MailAccountCreation = mailAccountCreation;
/** Redirect user to shipping page
 * @see module:controllers/AmazonPayments~shipping */
exports.Shipping = shipping;
/** This method is used to get the capture details
 * @see module:controllers/AmazonPayments~capture */
exports.Capture = capture;
/** This method is used to send the order complete mail for a specified order.
 * @see module:controllers/AmazonPayments~mailOrderComplete */
exports.MailOrderComplete = mailOrderComplete;
/** This method is for sending of mail through the storefront.
 * @see module:controllers/AmazonPayments~send */
exports.Send = send;
/** This method is used to update the order status with the data from the capture response.
 * @see module:controllers/AmazonPayments~updateOrderStatus */
exports.UpdateOrderStatus = updateOrderStatus;
/** This method is used to close the order reference. Before an order can be closed we need to confirm it.
 * @see module:controllers/AmazonPayments~updateOrderStatus */
exports.CloseOrderReference = closeOrderReference;
/**
 * @see module:controllers/AmazonPayments~setCustomerAmazonPassword */
exports.SetCustomerAmazonPassword = setCustomerAmazonPassword;
/**
 * @see module:controllers/AmazonPayments~getCaptureStatus */
exports.GetCaptureStatus = getCaptureStatus;
/**
 * @see module:controllers/AmazonPayments~getCaptureDetails */
exports.GetCaptureDetails = getCaptureDetails;
/**
 * @see module:controllers/AmazonPayments~getRefundDetails */
exports.GetRefundDetails = getRefundDetails;
/**
 * @see module:controllers/AmazonPayments~refund */
exports.Refund = refund;
/**
 * @see module:controllers/AmazonPayments~deferredPayments */
exports.DeferredPayments = deferredPayments;
/**
 * @see module:controllers/AmazonPayments~splitPayment */
exports.SplitPayment = splitPayment;
/**
 * @see module:controllers/AmazonPayments~updateBillingAddress */
exports.UpdateBillingAddress = updateBillingAddress;
/**
 * @see module:controllers/AmazonPayments~summary */
exports.Summary = summary;
/**
 * @see module:controllers/AmazonPayments~placeOrder */
exports.PlaceOrder = placeOrder;
/**
 * @see module:controllers/AmazonPayments~closeBillingAgreement */
exports.CloseBillingAgreement = closeBillingAgreement;
