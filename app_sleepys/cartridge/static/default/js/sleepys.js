(function(app, $) {
	var _app = app;

	$(".header-banner a, #homepage_slider .jcarousel-clip a, #homepage_slider area, #homepage-pc li a, #homepage-ms a").on("click", function(e) {
		var zoneCode = $('#CustomerZone input[name=postalCode]').data('zoneCode') != null ? $('#CustomerZone input[name=postalCode]').data('zoneCode').replace(',' , '|') : '';
		if (zoneCode != '') {
		    e.preventDefault();
		    var url = $(this).attr("href");
		    if (url.indexOf('search?q=') > -1) {
		    	url += "&prefn1=zone_code&prefv1="+zoneCode;
		    } else {
		    	url += "#prefn1=zone_code&prefv1="+zoneCode;
		    }
		    window.location = url;
		}
	});

}(window.app = window.app || {}, jQuery));