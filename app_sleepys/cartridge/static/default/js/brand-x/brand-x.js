$(document).ready(function(){
	app.brandx.init('self');
});

//app scope
app.brandx = {};

//init
app.brandx.init = function(source){
	var scope = $(".brandx");
	if (scope.length ==0){
		return;
	}
	
	var hash = location.hash.replace( /^#/, '' );

	// look for variants
	scope.find('form:first input[type="hidden"].variations').each(function(k,v){
		var name = $(v).data('name');
		var value = $(v).val();
		
		if (name == 'firmness' && source != 'cart'){
			app.brandx.setProduct(value); 
		} else {
			scope.find('[name="'+name+'"]').val(value);
		}
	}).promise().done( function() {
		if (source != 'cart' && hash != ''){
			app.brandx.setProduct(hash); 
		} else {
			app.brandx.setProduct('responsive-cool-gel');
		}
    });
	
	if (source != 'cart'){
		//disable horizontal scroll 
		window.addEventListener("scroll", function() {
			if(window.pageXOffset > 0){
				window.scroll(0, window.pageYOffset)
			}
		}, false); 
	
		// detect hash change
		$(window).hashchange( function(){
			hash = location.hash.replace( /^#/, '' );
			app.brandx.setProduct(hash);
		});
		
		// change forms based on sizes
		scope.find('select#responsive-cool-gel-firmness-select, select#luxe-latex-firmness-select, select#flexfeel-hybrid-firmness-select').on('change', function(e){
			//change form
			app.brandx.setProduct($(this).val());
			//keep original value
			$(this).val($(this).data('original')); 
		});

	}
	
	scope.find('input, select').not(((source=='cart')?'':'select#responsive-cool-gel-firmness-select, ')+'select#luxe-latex-firmness-select, select#flexfeel-hybrid-firmness-select').on('change', function(e){
		var form = $(this).closest('form');
		app.brandx.updateVariants(form, $(this).find('option:selected'));
	});
	
	scope.find('input[name="zip"]').on('keyup', function(e){
		var zip = $(this).val();
		if( /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zip)){
			$('input[name="postalCode"]').val(zip).trigger('keyup');
			$(this).blur();
		}
	});
	
	// validate submission
	scope.find('button[type="submit"]').on('click', function(e){
		e.preventDefault();
		
		if ($(this).hasClass('btn-disabled')){
			return;
		}
		
		var form = $(this).closest('form');
		app.brandx.validate(form);
		if (form.find('.control-group.error').length == 0){
			var data = {
			  "format":"ajax",
			  "Quantity": form.find('[name="Quantity"]').val(),
			  "cartAction":"add",
			  "pid": form.find('[name="currentVariant"]').val(),
			  "delivery": form.find('[name="delivery"]').val()
			};
			
			var uuid = form.find('input[name="UUID"]').val();
			if (uuid.length > 0){
				data.cartAction = 'update';
				data.uuid = uuid;
			}
			
			$.ajax({
			  type: "post",
			  data: data,
			  url: app.urls.addProduct,
			  success: function(data){ 
				  if (uuid.length > 0){
					  app.cart.refresh();
				  } else {
					  app.minicart.show(data);
				  }
			  }
			});
		}
	}); 
}

app.brandx.setProduct = function(type){
	if (type != ''){
		setProduct(type);
		app.brandx.updateVariants($('#'+type+'-purchase-form'),$('#'+type+'-purchase-form').find('select[name="firmness"]'), false );
	}
}

//validate input
app.brandx.validate = function(form, emptyFieldErrors){
	if (typeof emptyFieldErrors === 'undefined'){
		emptyFieldErrors = true;
	}
	
	var hasEmptyfieldErrors = false;
	
	var errorBox = form.find('.j-error-box');
	errorBox.empty();
	
	form.find('input, select').not('[type="hidden"]').each(function(k,v){
		var controlGroup = $(this).closest('.control-group');
		
		if ($(this).val() == '' && $(this).attr('name')!='zip' ){
			hasEmptyfieldErrors = true;
			if (emptyFieldErrors){
				controlGroup.toggleClass('error', true );
				errorBox.append($('<li>').html( app.resources.BRAND_X_EMPTY_FIELD + ' '+$(this).attr('name')+'.'));
				return;
			}
		} else {
			controlGroup.toggleClass('error', false );
		}
		
		if ($(this).attr('name')=='zip' && $(this).val() == '' && form.find('select[name="delivery"]').val() == 'USER_DELIVERY' ){
			controlGroup.toggleClass('error', true );
			errorBox.append($('<li>').html(app.resources.BRAND_X_ZIP_REQUIRED)); 
			return;
		} else {
			controlGroup.toggleClass('error', false );
		}
		
		if ($(this).attr('name')=='delivery' && $(this).val() == 'USER_DELIVERY' && errorBox.children().length == 0){
			var zip = form.find('input[name="zip"]').val();
			if( /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zip)){
				var data = {format: 'json', zip: zip, pid : form.find('[name="currentVariant"]').val()};
				var url = app.urls.pdpZoneCheck;
				$.ajax({
					url: url,
					type: 'post',
					data: data,
					dataType: 'json',
					async: false,
					success : function (data) {
						if (data.status == 'ok'){
							controlGroup.toggleClass('error', !data.available );
							if (!data.available){ 
								errorBox.append($('<li>').html(app.resources.BRAND_X_NO_WHITE_GLOVE)); 
							}
						}
					}
				});
			} else {
				controlGroup.toggleClass('error', true );
			}
		}
	});	
	
	$('#add-to-cart').toggleClass('btn-disabled', errorBox.children().length > 0 || hasEmptyfieldErrors);
}

//update variants
app.brandx.updateVariants = function(form, elem, emptyFieldErrors){
	if (typeof emptyFieldErrors === 'undefined'){
		emptyFieldErrors = true;
	}
	
	var data = form.serialize();
	form.find('[name="currentVariant"]').val('');
	$.ajax({
	  type: "get",
	  url: form.attr('url')+'&'+data,
	  success: function(data){
		  if (data.status == "ok"){
			  for (var name in data.choices) {
				  $.each(data.choices[name], function(i, choice){
					  data.choices[name][i] = choice.toLowerCase();
				  }); 
				  form.find('[name="'+name+'"] option').not(':first').each(function(k,v){
					  if (typeof elem != 'undefined' && data.choices[name].indexOf($(v).val().toLowerCase()) == -1 && $(v)[0] != elem[0]){
						  $(v).prop('selected', false);
					  }
				  });
			  } 
			  form.find('[name="currentVariant"]').val(data.currentVariant);
			  form.find('.product-price > b').html('$'+parseFloat(data.price.replace(/[A-Za-z\s]/g,'')));
			  if (emptyFieldErrors){
				  form.find('.product-price > span').html('Our Price ');
			  }

              app.brandx.validate(form, emptyFieldErrors);  
              try{
                  utag_data.product_variant = [data.currentVariant.replace(/\//g,'_')];
              } catch (e){
              
              }
		  }
	  },
	  dataType: 'json'
	});
}
