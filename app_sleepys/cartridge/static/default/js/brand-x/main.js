'use strict';

function imagePreview(productType){
  var largeSource = $('.product--' + productType).find('.img-medium').data('large');
  $('.product--' + productType).find('.product-zoom').zoom({url: largeSource });

  $('.product--' + productType).find('.btn-preview').click(function(e) {
    var mediumSource = $(e.currentTarget).find('.img-small').data('medium');
    var largeSource = $(e.currentTarget).find('.img-small').data('large');
    $('.product--' + productType).find('.img-medium').attr('src', mediumSource);
    $('.product--' + productType).find('.zoomImg').attr('src', largeSource);
  });
}

function setProduct(productType) {
  switch (productType)
  {
    case 'responsive-cool-gel':

      $('.product--responsive-cool-gel').toggleClass("active", true);
      $('.product--luxe-latex').toggleClass("active", false);
      $('.product--flexfeel-hybrid').toggleClass("active", false);
      $( '.product-draggable').css({
        'display':'inline-block',
        'left':'0%',
        'right':'auto',
        'margin-left':'25px'
      });
      $('.product--responsive-cool-gel').transition({ x: 0, y: 0 , opacity: 1 }, 300, 'ease');
      $('.product--luxe-latex').transition({ x: '100%', y: '-100%' , opacity: 0 }, 300, 'ease');  
      $('.product--flexfeel-hybrid').transition({ x: '200%', y: '-100%' , opacity: 0 }, 300, 'ease');
      $('#responsive-cool-gel-mattress-select option[value="responsive-cool-gel"]').attr('selected','selected');
      history.replaceState(undefined, undefined, "#responsive-cool-gel");
      //window.location.hash = 'responsive-cool-gel';

      explore('responsive-cool-gel');
      break;
    case 'luxe-latex':

      $('.product--responsive-cool-gel').toggleClass("active", false);
      $('.product--luxe-latex').toggleClass("active", true);
      $('.product--flexfeel-hybrid').toggleClass("active", false);
      
      $( '.product-draggable').css({
        'display':'inline-block',
        'left':'50%',
        'right':'auto',
        'margin-left':'-20px'
      });
      $('.product--responsive-cool-gel').transition({ x: '-100%', y: 0 , opacity: 0 }, 300, 'ease');
      $('.product--luxe-latex').transition({ x: 0, y: '-100%' , opacity: 1 }, 300, 'ease');  
      $('.product--flexfeel-hybrid').transition({ x: '100%', y: '-100%' , opacity: 0 }, 300, 'ease');
      $('#luxe-latex-mattress-select option[value="luxe-latex"]').attr('selected','selected');
      history.replaceState(undefined, undefined, "#luxe-latex");
      //window.location.hash = 'luxe-latex';

      explore('luxe-latex');
      break;
    case 'flexfeel-hybrid':

      $('.product--responsive-cool-gel').toggleClass("active", false);
      $('.product--luxe-latex').toggleClass("active", false);
      $('.product--flexfeel-hybrid').toggleClass("active", true);
     
      $( '.product-draggable').css({
        'display':'inline-block',
        'left':'auto',
        'right':'0%',
        'margin-right':'25px'
      });
      $('.product--responsive-cool-gel').transition({ x: '-200%', y: 0 , opacity: 0 }, 300, 'ease');
      $('.product--luxe-latex').transition({ x: '-100%', y: '-100%' , opacity: 0 }, 300, 'ease');  
      $('.product--flexfeel-hybrid').transition({ x: 0, y: '-100%' , opacity: 1 }, 300, 'ease');
      $('#flexfeel-hybrid-mattress-select option[value="flexfeel-hybrid"]').attr('selected','selected');
      history.replaceState(undefined, undefined, "#flexfeel-hybrid");
      //window.location.hash = 'flexfeel-hybrid';

      explore('flexfeel-hybrid');
      break;
  }

  $('.product-zoom').trigger('zoom.destroy');
  imagePreview(productType);
}

function dragDropClick() {

  $( '.product-draggable').draggable({
    axis: "x",
    containment: "parent",
    snap: ".track-mark",
    snapMode: "inner",
    snapTolerance: 50
  });

  $( '.track-mark--responsive-cool-gel' ).droppable({
    tolerance:"touch",
    drop: function() {
      setProduct('responsive-cool-gel');
    }
  });

  $( '.track-mark--luxe-latex' ).droppable({
    tolerance:"touch",
    drop: function() {
      setProduct('luxe-latex');
    }
  });

  $( '.track-mark--flexfeel-hybrid' ).droppable({
    tolerance:"touch",
    drop: function() {
      setProduct('flexfeel-hybrid');
    }
  });

  $('.product-trigger--responsive-cool-gel').click(function(e) {
    setProduct('responsive-cool-gel');
  });

  $('.product-trigger--luxe-latex').click(function(e) {
    setProduct('luxe-latex');
  });

  $('.product-trigger--flexfeel-hybrid').click(function(e) {
    setProduct('flexfeel-hybrid');
  });

}

// Deprecated Function - no content to make use of it
// function cycleImages(sequence){

//     $('#cycle-' + sequence).waypoint(function(direction) {
//       if (direction === 'down') {
//         // window.console.log('visible');
//         $('#cycle-' + sequence).attr("src", "assets/img/" + sequence + ".gif");
//       }
//     }, {
//       offset: '30%'
//     });

//     $('#cycle-' + sequence).waypoint(function(direction) {
//       if (direction === 'up') {
//         // window.console.log('visible');
//         $('#cycle-' + sequence).attr("src", "assets/img/" + sequence + ".gif");
//       }
//     }, {
//       offset: '10%'
//     });

//     $('#cycle-' + sequence).waypoint(function(direction) {
//       if (direction === 'down') {
//         // window.console.log('offscreen');
//         $('#cycle-' + sequence).attr("src", "assets/img/" + sequence + ".jpg");
//       }
//     }, {
//       offset: '-30%'
//     });

//     $('#cycle-' + sequence).waypoint(function(direction) {
//       if (direction === 'up') {
//         // window.console.log('offscreen');
//         $('#cycle-' + sequence).attr("src", "assets/img/" + sequence + ".jpg");
//       }
//     }, {
//       offset: '60%'
//     });
// }

// Deprecated Function - no content to make use of it
// function compareReveal() {

//   $('.compare-cover').waypoint(function(direction) {
//     if (direction === 'down') {
//       // window.console.log('visible');
//       $('.compare-cover').transition({width: '5%'});
//     }
//   }, {
//     offset: '30%'
//   });

//   $('.compare-cover').waypoint(function(direction) {
//     if (direction === 'up') {
//       // window.console.log('visible');
//       $('.compare-cover').transition({width: '5%'});
//     }
//   }, {
//     offset: '10%'
//   });

//   $('.compare-cover').waypoint(function(direction) {
//     if (direction === 'down') {
//       // window.console.log('offscreen');
//       $('.compare-cover').transition({width: '100%'});
//     }
//   }, {
//     offset: '-30%'
//   });

//   $('.compare-cover').waypoint(function(direction) {
//     if (direction === 'up') {
//       // window.console.log('offscreen');
//       $('.compare-cover').transition({width: '100%'});
//     }
//   }, {
//     offset: '60%'
//   }); 
// }

function tabIndexLoop(){

  $('[tabindex="8"]').on({
      blur: function() {
        $('[tabindex="1"]').focus();
      }
  });

  $('[tabindex="16"]').on({
      blur: function() {
        $('[tabindex="9"]').focus();
      }
  });

  $('[tabindex="24"]').on({
      blur: function() {
        $('[tabindex="17"]').focus();
      }
  });
}

// Contains excess transitions calls, once product details are specfied animations should be re-addressed.

function explore(productType){

  $('.explore-product').transition({ opacity: 0 }, 0).css("display", "none");
  $('#explore-' + productType).css("display", "inline-block").transition({ opacity: 1 }, 800);

  $('#explore-' + productType + ' .top').transition({ y: 100, }, 0, 'ease');
  $('#explore-' + productType + ' .layer-2').transition({ y: 50, }, 0, 'ease');
  $('#explore-' + productType + ' .layer-3').transition({ y: -20, }, 0, 'ease');
  $('#explore-' + productType + ' .layer-4').transition({ y: -30 ,}, 0, 'ease');  
  $('#explore-' + productType + ' .layer-5').transition({ y: -100 ,}, 0, 'ease');  


  // All On
  $('#explore-' + productType + ' .explore-off').on('click',function() {
    $('.explore-next').css({display: 'none'});
    $('.explore-previous').css({display: 'none'});
    $('#explore-' + productType + ' .explore-overlay').transition({ opacity: 1 }, 300, 'ease');
    $('#explore-' + productType + ' .explore-on').css({display: 'inline-block'}).transition({ opacity: 1, x: -60 }, 300, 'ease');
    $('#explore-' + productType + ' .top').delay(300).transition({ y: 0 }, 300, 'ease');
    $('#explore-' + productType + ' .layer-2').delay(300).transition({ y: 0 }, 300, 'ease');
    $('#explore-' + productType + ' .layer-3').delay(300).transition({ y: 0 }, 300, 'ease');
    $('#explore-' + productType + ' .layer-4').delay(300).transition({ y: 0 }, 300, 'ease');
    $('#explore-' + productType + ' .layer-5').delay(300).transition({ y: 0 }, 300, 'ease');
    $('#explore-' + productType + ' .explore-off').transition({ opacity: 0 }, 300, 'ease').css({display: 'none'});
    $('#explore-' + productType + ' .btn-explore')
      .transition({ opacity: 10 }, 0)
      .transition({ x: -9999 }, 0);
    $('#explore-' + productType + ' .section-title').transition({ opacity: 0.0 }, 300, 'ease').css({display: 'none'});
    $('#close-explore-' + productType).show();
    $('#explore-' + productType + ' .explore-content').removeClass('closed').addClass('opened');
    $('#explore-' + productType + ' .explore-meta').delay(300).transition({ opacity: 1 }, 300, 'ease').css({display: 'inline-block'});
    $('#explore-' + productType + ' .explore-meta').css('z-index', '4');
  });

  $('#explore-' + productType + ' .btn-explore').on('click',function() {
    $('.explore-next').css({display: 'none'});
    $('.explore-previous').css({display: 'none'});
    $('#explore-' + productType + ' .explore-overlay').transition({ opacity: 1 }, 300, 'ease');
    $('#explore-' + productType + ' .explore-on').css({display: 'inline-block'}).transition({ opacity: 1, x: -60 }, 300, 'ease');
    $('#explore-' + productType + ' .top').delay(300).transition({ y: 0 }, 300, 'ease');
    $('#explore-' + productType + ' .layer-2').delay(300).transition({ y: 0 }, 300, 'ease');
    $('#explore-' + productType + ' .layer-3').delay(300).transition({ y: 0 }, 300, 'ease');
    $('#explore-' + productType + ' .layer-4').delay(300).transition({ y: 0 }, 300, 'ease');
    $('#explore-' + productType + ' .layer-5').delay(300).transition({ y: 0 }, 300, 'ease');
    $('#explore-' + productType + ' .explore-off').transition({ opacity: 0 }, 300, 'ease').css({display: 'none'});
    $('#explore-' + productType + ' .btn-explore')
      .transition({ opacity: 10 }, 0)
      .transition({ x: -9999 }, 0);
    $('#explore-' + productType + ' .section-title').transition({ opacity: 0.0 }, 300, 'ease').css({display: 'none'});
    $('#close-explore-' + productType).show();
    $('#explore-' + productType + ' .explore-content').removeClass('closed').addClass('opened');
    $('#explore-' + productType + ' .explore-meta').delay(300).transition({ opacity: 1 }, 300, 'ease').css({display: 'inline-block'});
    $('#explore-' + productType + ' .explore-meta').css('z-index', '4');
  });

  // Button Close All
  $('#close-explore-' + productType).on('click',function() {
    $('.explore-next').css({display: 'inline-block'});
    $('.explore-previous').css({display: 'inline-block'});
    $('#explore-' + productType + ' .explore-overlay').transition({ opacity: 0 }, 300, 'ease');
    $('#explore-' + productType + ' .explore-on').css({display: 'none'}).transition({ x: 0, y: 0, opacity: 0}, 300, 'ease');
    $('#explore-' + productType + ' .top').transition({ y: 100, }, 0, 'ease');
    $('#explore-' + productType + ' .layer-2').transition({ y: 50, }, 0, 'ease');
    $('#explore-' + productType + ' .layer-3').transition({ y: -20, }, 0, 'ease');
    $('#explore-' + productType + ' .layer-4').transition({ y: -30 ,}, 0, 'ease');
    $('#explore-' + productType + ' .layer-5').transition({ y: -100 ,}, 0, 'ease');
    $('#explore-' + productType + ' .explore-off').css({display: 'inline-block'}).transition({ opacity: 1 }, 300, 'ease');
    $('#explore-' + productType + ' .btn-explore')
      .transition({ x: 0 }, 0)
      .transition({ opacity: 1  }, 0);
    $('#explore-' + productType + ' .section-title').transition({ opacity: 1 }, 300, 'ease').css({display: 'block'});
    $('#explore-' + productType + ' .close').hide();
    $('#explore-' + productType + ' .explore-content').removeClass('opened').addClass('closed');
    $('#explore-' + productType + ' .explore-meta').transition({ opacity: 0 }, 300, 'ease').css({display: 'none'});
    $('#explore-' + productType + ' .explore-meta').css('z-index', '1');
  });

  // Meta Close All
  $('.explore-meta').on('click',function() {
    $('.explore-next').css({display: 'inline-block'});
    $('.explore-previous').css({display: 'inline-block'});
    $('#explore-' + productType + ' .explore-overlay').transition({ opacity: 0 }, 300, 'ease');
    $('#explore-' + productType + ' .explore-on').css({display: 'none'}).transition({ x: 0, y: 0, opacity: 0 }, 300, 'ease');
    $('#explore-' + productType + ' .top').transition({ y: 100, }, 0, 'ease');
    $('#explore-' + productType + ' .layer-2').transition({ y: 50, }, 0, 'ease');
    $('#explore-' + productType + ' .layer-3').transition({ y: -20, }, 0, 'ease');
    $('#explore-' + productType + ' .layer-4').transition({ y: -30 ,}, 0, 'ease');
    $('#explore-' + productType + ' .layer-5').transition({ y: -100 ,}, 0, 'ease');
    $('#explore-' + productType + ' .explore-off').css({display: 'inline-block'}).transition({ opacity: 1 }, 300, 'ease');
    $('#explore-' + productType + ' .btn-explore')
      .transition({ x: 0 }, 0)
      .transition({ opacity: 1  }, 0);
    $('#explore-' + productType + ' .section-title').transition({ opacity: 1 }, 300, 'ease').css({display: 'block'});
    $('#explore-' + productType + ' .close').hide();
    $('#explore-' + productType + ' .explore-content').removeClass('opened').addClass('closed');
    $('#explore-' + productType + ' .explore-meta').transition({ opacity: 0 }, 300, 'ease').css({display: 'none'});
    $('#explore-' + productType + ' .explore-meta').css('z-index', '1');
  });

  // Image Close All
  $('.explore-on').on('click',function() {
    $('.explore-next').css({display: 'inline-block'});
    $('.explore-previous').css({display: 'inline-block'});
      $('#explore-' + productType + ' .explore-overlay').transition({ opacity: 0 }, 300, 'ease');
    $('#explore-' + productType + ' .explore-on').css({display: 'none'}).transition({ x: 0, y: 0, opacity: 0}, 300, 'ease');
    $('#explore-' + productType + ' .top').transition({ y: 100, }, 0, 'ease');
    $('#explore-' + productType + ' .layer-2').transition({ y: 50, }, 0, 'ease');
    $('#explore-' + productType + ' .layer-3').transition({ y: -20, }, 0, 'ease');
    $('#explore-' + productType + ' .layer-4').transition({ y: -30 ,}, 0, 'ease');
    $('#explore-' + productType + ' .layer-5').transition({ y: -100 ,}, 0, 'ease');
    $('#explore-' + productType + ' .explore-off').css({display: 'inline-block'}).transition({ opacity: 1 }, 300, 'ease');
      $('#explore-' + productType + ' .btn-explore')
        .transition({ x: 0 }, 0)
        .transition({ opacity: 1  }, 0);
    $('#explore-' + productType + ' .section-title').transition({ opacity: 1 }, 300, 'ease').css({display: 'block'});
      $('#explore-' + productType + ' .close').hide();
      $('#explore-' + productType + ' .explore-content').removeClass('opened').addClass('closed');
    $('#explore-' + productType + ' .explore-meta').transition({ opacity: 0 }, 300, 'ease').css({display: 'none'});
      $('#explore-' + productType + ' .explore-meta').css('z-index', '1');      
  });

  // Exit All
  // $('#explore-' + productType).mouseleave(function() {
  //   if($(this).hasClass('closed')) {
  //     $('#explore-' + productType + ' .explore-on').transition({ opacity: 0 }, 300, 'ease');
  //   } else {
  //     $('#explore-' + productType + ' .explore-overlay').transition({ opacity: 0 }, 300, 'ease');
  //     $('#explore-' + productType + ' .explore-on').transition({ x: 0, y: 0, opacity: 0, scale:1 }, 300, 'ease');
  //     $('#explore-' + productType + ' .explore-off').transition({ opacity: 1 }, 300, 'ease');
  //     $('#explore-' + productType + ' .btn-explore')
  //       .transition({ x: 0 }, 0)
  //       .transition({ opacity: 1  }, 0);
  //     $('#explore-' + productType + ' .section-title').transition({ opacity: 1 }, 300, 'ease');
  //     $('#explore-' + productType + ' .close').hide();
  //     $('#explore-' + productType + ' .explore-content').removeClass('opened').addClass('closed');
  //     $('#explore-' + productType + ' .explore-meta').transition({ opacity: 0 }, 300, 'ease');
  //     $('#explore-' + productType + ' .explore-meta').css('z-index', '1');      
  //   }
  // });
  
}

function cycleExplore(){
  $('.explore-next').mouseup(function() {
    if($('.product--responsive-cool-gel').hasClass("active")) {
      setProduct('luxe-latex');
    } else if($('.product--luxe-latex').hasClass("active")) {
      setProduct('flexfeel-hybrid');
    } else if($('.product--flexfeel-hybrid').hasClass("active")) {
      setProduct('responsive-cool-gel');
    }
  });
  
  $('.explore-previous').mouseup(function() {
    if($('.product--responsive-cool-gel').hasClass("active")) {
      setProduct('flexfeel-hybrid');
    } else if($('.product--luxe-latex').hasClass("active")) {
      setProduct('responsive-cool-gel');
    } else if($('.product--flexfeel-hybrid').hasClass("active")) {
      setProduct('luxe-latex');
    }
  });
}

// scrollTo JS Library removed due to jQuery 1.8+ version required
// Basic scrollTop function implimented.

function customScroll(){

  $("#scroll-mattress-tiers").click(function() {
    $('html, body').animate({
        scrollTop: $("#mattress-tiers").offset().top
    }, 700);
    return false;
  });

}

function dropdownSelect(){

  $('.mattress-select').on('change', function() {
    setProduct( this.value );
  });

}

// $.fn.carouselHeights = function() {

//   var items = $(this), //grab all slides
//       heights = [], //create empty array to store height values
//       tallest; //create variable to make note of the tallest slide

//   var normalizeHeights = function() {

//       items.each(function() { //add heights to array
//           heights.push($(this).height()); 
//       });
//       tallest = Math.max.apply(null, heights); //cache largest value
//       items.each(function() {
//           $(this).css('min-height',tallest + 'px');
//       });
//   };

//   normalizeHeights();

//   $(window).on('resize orientationchange', function () {
//       //reset vars
//       tallest = 0;
//       heights.length = 0;

//       items.each(function() {
//           $(this).css('height','0'); //reset min-height
//       }); 
//       normalizeHeights(); //run it again 
//   });

// };

$(function () {

  dropdownSelect();
  customScroll();
  dragDropClick();
  // cycleImages('our-story');
  // cycleImages('delivery-your-way');
  // cycleImages('discover-your-comfort');
  // cycleImages('100-nights');
  // compareReveal();
  tabIndexLoop();
  cycleExplore();


  $('#value-carousel').carousel();
  $("#value-carousel .item").matchHeight({
      byRow: 0
  });

  $(".product").matchHeight();
  $(".product-tier").matchHeight();

  // See Trello card https://trello.com/c/EKO2iWYG/25-back-forward-window-hash-bug
  // Additonal functionality needed, client found bug

  if($('#product-display-page').length && window.location.hash == '#responsive-cool-gel') {
    setProduct('responsive-cool-gel');
    imagePreview('responsive-cool-gel');
  } else if($('#product-display-page').length && window.location.hash == '#luxe-latex') {
    setProduct('luxe-latex');
    imagePreview('luxe-latex');
  } else if($('#product-display-page').length && window.location.hash == '#flexfeel-hybrid') {
    setProduct('flexfeel-hybrid');
    imagePreview('flexfeel-hybrid');
  } else if ($('#product-display-page').length) {
    setProduct('responsive-cool-gel');
    imagePreview('responsive-cool-gel');    
  }

  $('body, html').bind('scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove', function(e){
    if ( e.which > 0 || e.type == "mousedown" || e.type == "mousewheel" || e.type == "touchmove"){
      $('html, body').stop();
    }
  });

});

$( window ).load(function() {

	window.console.log( 'Congrats, you are good to go kid.' );

  // scrollTo JS Library removed due to jQuery 1.8+ version required
  // Basic scrollTop function implimented.

  $('html, body').animate({
    scrollTop: $("#nav").offset().top
  }, 500);

});


