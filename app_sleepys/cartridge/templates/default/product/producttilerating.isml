<iscomment>
	This template renders a product tile using a product. The following parameters
	must be passed into the template module:
	
	product 		: the product to render the tile for
	showswatches 	: check, whether to render the color swatches (default is false)
	enableswatches	: flag indicating whether to enable the swatch links (default is false)
	showpricing		: check, whether to render the pricing (default is false)
	showpromotion	: check, whether to render the promotional messaging (default is false)
	showrating		: check, whether to render the review rating (default is false)
</iscomment>

<iscomment>set the product for local reuse</iscomment>
<isset name="Product" value="${pdict.product}" scope="page"/>
<isset name="OrgProduct" value="${null}" scope="page"/>

<iscomment>set default values</iscomment>
 
 
<isset name="showpromotion" value="${false}" scope="page"/>
<isif condition="${pdict.showpromotion != null}">
	<isset name="showpromotion" value="${pdict.showpromotion}" scope="page"/>
</isif>
<isset name="showrating" value="${false}" scope="page"/>
<isif condition="${pdict.showrating != null}">
	<isset name="showrating" value="${pdict.showrating}" scope="page"/>
</isif>
<isset name="showimage" value="${true}" scope="page"/>
<isif condition="${pdict.showimage != null}">
	<isset name="showimage" value="${pdict.showimage}" scope="page"/>
</isif>
<isset name="showshare" value="${true}" scope="page"/>
<isif condition="${pdict.showshare != null}">
	<isset name="showshare" value="${pdict.showshare}" scope="page"/>
</isif>

<isif condition="${!empty(Product)}">

	<iscomment>
		Get the colors selectable from the current product master.
	</iscomment>
	<isscript>
		var selectableColors : dw.util.Collection = new dw.util.ArrayList();
		if( Product.master )
		{
			var varModel : dw.catalog.ProductVariationModel = Product.variationModel;
			var varAttrColor : dw.catalog.ProductVariationAttribute = varModel.getProductVariationAttribute("color");
	
			if( varAttrColor != null )
			{
				var allColors : dw.util.Collection = varModel.getAllValues( varAttrColor );
				
				// filter out colors with not orderable variants
				for each( var color in allColors )
				{
					if( varModel.hasOrderableVariants( varAttrColor, color ) )
					{
						selectableColors.add( color );
					}
				}
			}
		}
	</isscript>
	
	<iscomment>
		Generate link to product detail page. If a color variation is available, the first color is used as link url.
	</iscomment>
	<isset name="productUrl" value="${URLUtils.http('Product-Show', 'pid', Product.ID)}" scope="page"/>
	<isif condition="${!empty(selectableColors) && selectableColors.size() > 0}">
		<isset name="colorVarAttr" value="${Product.variationModel.getProductVariationAttribute('color')}" scope="page"/>
		<isset name="productUrl" value="${Product.variationModel.urlSelectVariationValue('Product-Show', colorVarAttr, selectableColors.get(0))}" scope="page"/>
	</isif>

<div class="product-tile" id="${Product.UUID}" data-itemid="${Product.ID}" data-category="${empty(Product.primaryCategory)?Product.categories[0].displayName:Product.primaryCategory.displayName}" data-sku="${Product.ID}" data-price="${Product.priceModel.price}" data-brand="${Product.manufacturerName}" data-title="${Product.name}"><!-- dwMarker="product" dwContentID="${Product.UUID}" -->
		
<iscomment>Image</iscomment>
<iscomment>++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++</iscomment>

<div class="col-1">
	
	<iscomment>Render the thumbnail</iscomment>
		
	<iscomment>Determine the correct image, either first displayed color variation or default product thumbnail</iscomment>
	<isif condition="${!empty(selectableColors) && selectableColors.size() > 0}">
		<isset name="firstColorVariation" value="${selectableColors.get(0)}" scope="page"/>
		<isset name="image" value="${firstColorVariation.getImage('medium')}" scope="page"/>					
	<iselse>
		<isset name="image" value="${Product.getImage('medium',0)}" scope="page"/>					
	</isif>
	
	<iscomment>Amplience controls error images on their end.</iscomment>
	<isset name="thumbnailUrl" value="${pdict.CurrentRequest.httpProtocol+'://i1.adis.ws/i/hmk/'+Product.custom.external_id+'?$product_listing_page$'}" scope="page"/>
	<isset name="imageAlt" value="${Product.name}" scope="page"/>
	<isset name="imageTitle" value="${Product.name}" scope="page"/>
	
	<a class="thumb-link clearfix" href="${URLUtils.url('Product-Show','pid', Product.ID)}" title="${Product.name}">
		<img src="${thumbnailUrl}" alt="${imageAlt}" title="${imageTitle}"  />
	</a>
 
	<div class="clearfix">
 
		<iscomment>Rating ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++</iscomment>
	
		<isif condition="${showrating}">
				<isinclude template="bv/display/rr/inlineratings"/>
		</isif>	
	 
	</div>
	
</div>
		 

<div class="col-2">

  	<isinclude template="/rendering/sharebar.isml" />
 

		<h1 class="product-name clearfix" itemprop="name">
			<a href="${productUrl}" title="${Product.name}">
				<isprint value="${Product.name}"/>
			</a> 
		</h1>

	<button id="add-to-cart" type="submit" title="Add to Cart" value="Add to Cart" class="button-fancy-small add-to-cart">Add to Cart</button>
		
	
	

</div>
		

		
<iscomment>Promotion</iscomment>
<iscomment>++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++</iscomment>

<isif condition="${showpromotion}">

	<div class="product-promo">

		<iscomment>Render information on active product promotions</iscomment>
		<isset name="promos" value="${dw.campaign.PromotionMgr.activeCustomerPromotions.getProductPromotions(Product)}" scope="page"/>
		
		<isif condition="${!empty(promos)}">
			<isloop items="${promos}" alias="promo" status="promoloopstate">
				<div class="promotional-message <isif condition="${promoloopstate.first}"> first <iselseif condition="${promoloopstate.last}"> last</isif> ">
					<isprint value="${promo.calloutMsg}" encoding="off"/>
				</div>
			</isloop>
		</isif>

	</div>
	
</isif>
		


		
	</div><!--  END: .product-tile -->
</isif>