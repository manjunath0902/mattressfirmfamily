<iscomment>
	If it is a master product without a price range, get its pricing from its first variant.	
</iscomment>
<isif condition="${pdict.Product.master && !pdict.Product.priceModel.isPriceRange() && pdict.Product.variationModel.variants.size() > 0}"/>
	<iscomment>Preserve current product instance</iscomment>
	<isset name="OrgProduct" value="${pdict.Product}" scope="pdict"/>
	<isset name="Product" value="${pdict.OrgProduct.variationModel.variants[0]}" scope="pdict"/>
</isif>

<iscomment>
	Get the price model for this product.	
</iscomment>
<isset name="PriceModel" value="${pdict.Product.getPriceModel()}" scope="page"/>

<iscomment>
	Check whether this product has price in the sale pricebook.  If so, then
	display two prices:  crossed-out standard price and sales price.
</iscomment> 

<isinclude template="product/components/standardprice"/>
		
<isscript>
	importScript( "common/GetSalePriceListId.ds");
	
	//  Get min - max sale price and min - max standard price.
	var listPrice = "listPriceDefault" in dw.system.Site.current.preferences.custom ? dw.system.Site.getCurrent().getCustomPreferenceValue("listPriceDefault") : "DWSLP-list-prices";
	var salePriceListId : String = getSalePriceListId(); 
				
	var standardMin: Money = PriceModel.getMinPriceBookPrice(listPrice);
	var standardMax: Money = PriceModel.getMaxPriceBookPrice(listPrice);
	var saleMin: Money = PriceModel.getMinPriceBookPrice(salePriceListId);
	var saleMax: Money = PriceModel.getMaxPriceBookPrice(salePriceListId);
						
	
	var hasSalePrice: Boolean = false;
	if(!empty(saleMin) && !empty(saleMax)) {
		if (standardMin.value > saleMin.value || standardMax.value > saleMax.value) {
			hasSalePrice = true;
		}
	}
</isscript>

<isset name="PriceTable" value="${PriceModel.getPriceTable()}" scope="page"/>
<isset name="SalesPrice" value="${PriceModel.getPrice()}" scope="page"/>
<isset name="BasePriceQuantity" value="${PriceModel.getBasePriceQuantity()}" scope="page"/>
<isset name="ShowStandardPrice" value="${StandardPrice.available && SalesPrice.available && StandardPrice.compareTo(SalesPrice) == 1}" scope="page"/>
<isset name="currencyCode" value="${dw.system.Site.getCurrent().currencyCode}" scope="page"/>
<iscomment>
	Check whether there are any active customer promotions for this product.  If so, then
	display two prices:  crossed-out pricebook price and promotional price.

	Note:  we never display two crossed-out prices even if there is both a price-book
	discount and a promotion.
</iscomment>

<isset name="promos" value="${dw.campaign.PromotionMgr.activeCustomerPromotions.getProductPromotions(pdict.Product)}" scope="page"/>
<isset name="PromotionalPrice" value="${dw.value.Money.NOT_AVAILABLE}" scope="page"/>
<isset name="isPromoPrice" value="${false}" scope="page"/>

<isif condition="${! empty(promos)}">
	<isloop items="${promos}" var="promo">
		<isif condition="${promo.getPromotionClass() != null && promo.getPromotionClass().equals(dw.campaign.Promotion.PROMOTION_CLASS_PRODUCT)}">
			<isif condition="${pdict.Product.optionProduct}">
				<isif condition="${pdict.CurrentOptionModel != null}">
					<isset name="PromotionalPrice" value="${promo.getPromotionalPrice(pdict.Product, pdict.CurrentOptionModel)}" scope="page"/>
				<iselse>
					<isset name="PromotionalPrice" value="${promo.getPromotionalPrice(pdict.Product, pdict.Product.getOptionModel())}" scope="page"/>
				</isif>
			<iselse>
				<isset name="PromotionalPrice" value="${promo.getPromotionalPrice(pdict.Product)}" scope="page"/>
			</isif>
		</isif>
		<isbreak/>
	</isloop>
	
		<iscomment>Can remove this?</iscomment>
	<isif condition="${PromotionalPrice.available && SalesPrice.compareTo(PromotionalPrice) != 0}">
		<isset name="ShowStandardPrice" value="${true}" scope="page"/>
		<isset name="StandardPrice" value="${SalesPrice}" scope="page"/>
		<isset name="SalesPrice" value="${PromotionalPrice}" scope="page"/>
		<isset name="isPromoPrice" value="${true}" scope="page"/>
	</isif>
	
	<isscript>
			// Use promotional price as minSale price if it is lower.  
		if (!empty(saleMin)) {
			if ((PromotionalPrice.value != 0) && (PromotionalPrice.value < saleMin.value)) {
				saleMin = PromotionalPrice;
			}
		}
		else {
			if ((PromotionalPrice.value != 0) && (PromotionalPrice.value < standardMin.value)) {
				saleMin = PromotionalPrice;
			}
		}
		if(!empty(saleMin)) {
			if (standardMin.value > saleMin.value) {
				hasSalePrice = true;
			}
		}
	</isscript>
	
</isif>
	
	<div class="product-price">
		<isif condition="${pdict.Product.priceModel.isPriceRange()}">
			<isif condition="${hasSalePrice}">
				<span class="product-standard-price price-range has-sale-price"><isprint value="${dw.util.StringUtils.formatMoney(dw.value.Money(standardMin.value, currencyCode))}" /> - <isprint value="${dw.util.StringUtils.formatMoney(dw.value.Money(standardMax.value, currencyCode))}" /></span>
				<span class="product-sales-price price-range"><isprint value="${dw.util.StringUtils.formatMoney(dw.value.Money(saleMin.value, currencyCode))}" /> - <isprint value="${dw.util.StringUtils.formatMoney(dw.value.Money(saleMax.value, currencyCode))}" /></span>
			<iselse>
				<span class="product-standard-price price-range"><isprint value="${dw.util.StringUtils.formatMoney(dw.value.Money(standardMin.value, currencyCode))}" /> - <isprint value="${dw.util.StringUtils.formatMoney(dw.value.Money(standardMax.value, currencyCode))}" /></span>
			</isif>
		<iselse>
			<isif condition="${hasSalePrice}">
				<span class="product-standard-price has-sale-price" title="Regular Price"><isprint value="${dw.util.StringUtils.formatMoney(dw.value.Money(standardMin.value, currencyCode))}" /></span>
				<span class="product-sales-price" title="Sale Price"><isprint value="${dw.util.StringUtils.formatMoney(dw.value.Money(saleMin.value, currencyCode))}" /></span>
			<iselse>
				<span class="product-standard-price" title="Regular Price"><isprint value="${dw.util.StringUtils.formatMoney(dw.value.Money(standardMin.value, currencyCode))}" /></span>
			</isif>
		</isif>
	</div>
	
	<isset name="displayPrice" value="${(SalesPrice.valueOrNull != null && SalesPrice.valueOrNull > 0) ? SalesPrice : new dw.value.Money(0, currencyCode)}" scope="pdict"/> 
	
	<isif condition="${typeof showTieredPrice !== 'undefined' && showTieredPrice == true}">
		<iscomment> show price table, avoid display of empty table. Open div and table on demand. </iscomment>
		<isset name="scaledPriceTagOpened" value="${false}" scope="PAGE"/>
	
		<isloop iterator="${PriceTable.getQuantities()}" var="Quantity" status="pricingloopstatus">
			<iscomment> Don't show the price table entry that represents the sales price
				  for the products minimum order quantity (already shown above) </iscomment>
			<isif condition="${Quantity.compareTo(BasePriceQuantity) != 0}">
				<isif condition="${pricingloopstatus.getIndex() == 1}">
					<isset name="scaledPriceTagOpened" value="${true}" scope="PAGE"/>
					<div class="price-tiered">
						<isif condition="${PriceTable.getPercentage(Quantity) > 0}">
							${Resource.msg('pricing.payless','product',null)}
						<iselse>
							${Resource.msg('pricing.paymore','product',null)}
						</isif>
				</isif>
	
				<isset name="NextQuantity" value="${PriceTable.getNextQuantity(Quantity)}" scope="PAGE"/>	
			<div class="price-tiered-values">
					<isif condition="${NextQuantity != null}">						
						<isprint value="${Quantity}" formatter="#"/><isif condition="${Quantity != NextQuantity.getValue()-1}"><span class="divider">${Resource.msg('global.symbol.dash','global',null)}</span><isprint value="${NextQuantity.getValue()-1}" formatter="#"/></isif> ${Resource.msg('pricing.items','product',null)}						
					<iselse>						
						<isprint value="${Quantity}" formatter="#"/> ${Resource.msg('pricing.more','product',null)}						
					</isif>
					<iscomment>
						show percentages based on shown list price if list price is shown at all,
					 	otherwise show it based on effective price
					</iscomment>
					<isprint value="${PriceTable.getPrice(Quantity)}"/>
					(<isif condition="${ShowStandardPrice}"><isprint value="${PriceTable.getPrice(Quantity).percentLessThan(StandardPrice)}" formatter="#"/><iselse><isprint value="${PriceTable.getPercentage(Quantity)}" formatter="#"/></isif>${Resource.msg('pricing.off','product',null)})
				</div>
			</isif>
		</isloop>
		<iscomment> make sure, we close our tags, if opened </iscomment>
		<isif condition="${scaledPriceTagOpened}">			
			</div>
		</isif>
	</isif>


<isif condition="${!empty(pdict.OrgProduct)}">
	<iscomment>Restore current product instance</iscomment>
	<isset name="Product" value="${pdict.OrgProduct}" scope="pdict"/>
	<isset name="OrgProduct" value="${null}" scope="pdict"/>
</isif>