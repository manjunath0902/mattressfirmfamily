/**
* 
*	@input Basket : dw.order.Basket
*
*/

importPackage(dw.catalog);
importPackage(dw.system);
importPackage(dw.object);
importPackage(dw.order);
importPackage(dw.util);

var adjustmentPromotionId: String = "RECYCLING_FEE";
var recycleFeeTaxClassId: String = "sales";

function execute( args : PipelineDictionary ) : Number {
	
		//  Make sure we want to use this feature.
	if (!Site.current.getCustomPreferenceValue("recycleFee_active")) {
		return PIPELET_NEXT;
	}
		//  Get the Shipping address state code.
	var basket: Basket = args.Basket;
	var defaultShipment: Shipment;
	var shippingAddress: OrderAddress;
	var stateCode: String;
	if (!empty(basket)) {
		defaultShipment = basket.getDefaultShipment();
		if (!empty(defaultShipment)) {
			shippingAddress = defaultShipment.shippingAddress;
			if (!empty(shippingAddress)) {
				stateCode = shippingAddress.stateCode;
				if (!empty(stateCode)) {
					stateCode = stateCode.toUpperCase();
				}
			}
		}
	}
		//  If no shipping address exists, skip this script.
	if (empty(stateCode)) {
		return PIPELET_NEXT;
	}
		//  For each product in the basket...
	var productLineItems: Iterator = basket.getProductLineItems().iterator();
	var productLineItem: ProductLineItem;
	while (productLineItems.hasNext()) {
		var productLineItem: ProductLineItem = productLineItems.next();

		var taxRate: Number = productLineItem.getTaxRate();
		var product: Product = productLineItem.getProduct();
		var recycleStates: ArrayList = product.custom.recycleStates;
		var stateIndex = -1;
		if (!empty(recycleStates)) {
			stateIndex = recycleStates.indexOf(stateCode);
		}
		 
			//  If there should not be a fee on this product, for this state...
		if (stateIndex == -1) {
				//  Remove recycling fee if one exists.
			removeRecyclingFee(productLineItem);
		}
			//  If there should be a recycling fee on this product...
		else {
				//  Get the number of recycling fees that need to be applied to this product.

			var feeMultiplier = product.custom.recycleCount * productLineItem.quantityValue;
				//  If feeMultiplier is not set or 0, there should not be a fee for this item.
			if (empty(feeMultiplier) || feeMultiplier == 0) {
				removeRecyclingFee(productLineItem);
				continue;
			}
				
				//  Get the recycling fee price for this state.
			var feePrice: Number = getFeePrice(stateCode);
			if (empty(feePrice) || feePrice == 0) {
				removeRecyclingFee(productLineItem);
				continue;
			}
			
				//  Apply, update or remove Price adjustment.
			var priceAdjustment: PriceAdjustment = productLineItem.getPriceAdjustmentByPromotionID(adjustmentPromotionId);
				//  If this product already has a recycling fee price adjustment...
			if (!empty(priceAdjustment)) {
					//  Make sure it is using the correct price in case state code was changed since last time it was set.
				priceAdjustment.setPriceValue(feePrice * feeMultiplier);
				priceAdjustment.setTaxClassID(recycleFeeTaxClassId);

				var itemText: String = feeMultiplier+"|"+feePrice+"|";
				if (!empty(taxRate)) {
					itemText += taxRate;
				}
				priceAdjustment.setLineItemText(itemText);

			}
				//  If this product does not have a recycling fee price adjustment...
			else {
					//  Create a price adjustment for the recycling fee.
				var newAdjustment: PriceAdjustment = productLineItem.createPriceAdjustment(adjustmentPromotionId);
				newAdjustment.setPriceValue(feePrice * feeMultiplier);
				newAdjustment.setTaxClassID(recycleFeeTaxClassId);

				var itemText: String = feeMultiplier+"|"+feePrice+"|";
				if (!empty(taxRate)) {
					itemText += taxRate;
				}
				newAdjustment.setLineItemText(itemText);

			}
		}
	}
	return PIPELET_NEXT;
}

	//  Recycle Fee is determined by the price of a product with ID [stateCode]recycle which should be fed from Oracle.
function getFeePrice(stateCode: String) {
	var product: Product = ProductMgr.getProduct(stateCode+"recycle");
	if (empty(product)) {
		logError("Could not apply recycling fee. Missing reference product required for pricing: " + stateCode + "recycle.");
		return 0;
	}
	var fee: Number = product.priceModel.getPrice().value;
	if (empty(fee)) {
		logError("Could not apply recycling fee. Missing price on reference product: " + stateCode + "recycle.");
		return 0;
	}
	return fee;
}

/*
 	//  Original method of getting price used a CO to make a state->price mapping.
 	//  This funciton can be deleted after we confirm the product/ price book record way is working.
function getFeePriceByCO(stateCode: String) {
		//  Get the state->fee CO for this state.
	var stateFeeCO: CustomObject = CustomObjectMgr.getCustomObject("recycleFeeByStateCode", stateCode);
	if (empty(stateFeeCO)) {
			//  This is a problem - the product says it should have a fee, but there is no associated CO for this state.
		logError("Could not apply recycling fee. Missing recycleFeeByStateCode CO for state " + stateCode);
		return 0;
	}
	var feePrice: Number = stateFeeCO.custom.recycleFee;
	return feePrice;
}
*/

function removeRecyclingFee(productLineItem: ProductLineItem) {
	var priceAdjustment: PriceAdjustment = productLineItem.getPriceAdjustmentByPromotionID(adjustmentPromotionId);
	if (!empty(priceAdjustment)) {
		productLineItem.removePriceAdjustment(priceAdjustment);
	}
}

function logError(message: String) {
	var logger: Log = Logger.getLogger("RecyclingFee", 'RecyclingFee');
	var theMessage: String = message + "\n-------------------------------------------------"; 
	logger.error("\n - Error : "+message+"\n");
}
