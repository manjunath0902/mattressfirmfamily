/**
 * @input OAuthProviderID1 : String
 * @output ResponseOtherTokens : dw.util.Map
 * @output AccessToken : String
 * @output OAuthProviderID : String
 * @output ErrorStatus : String
 */
importPackage( dw.system );
importPackage( dw.net );
importPackage( dw.util );
importPackage( dw.customer );
importPackage( dw.web );


var sleepysHelper = require("bc_sleepys/cartridge/scripts/sleepys/util/SleepysUtils").SleepysHelper;
var serviceHelper = require("GeneralService");
var globalHelper = require("GlobalUtilities");

function execute( pdict : PipelineDictionary ) : Number
{
	var THIS_SCRIPT : String = 'ObtainAccessTokenFromSalesForceProvider: ';
	var pm : HttpParameterMap = pdict['CurrentHttpParameterMap'];
	
	//these three params are defined in the OAuth2 spec:
	var code : String = pm['code'];
	var state : String = pm['state'];
	var error : String = pm['error'];
	
	//internal implementation detail
	var provider : String = session.privacy['OAuthProviderID'];	 
	delete session.privacy['OAuthProviderID'];

	var expectedState : String = session.privacy['OAuthState']; //internal implementation detail
	delete session.privacy['OAuthState'];
	
	if (null == provider) {
		Logger.warn(THIS_SCRIPT + "Exiting because the OAuth Provider in the session is null.");
		//return PIPELET_ERROR;
		provider = globalHelper.getSfOAuth2ProviderID();
	}
	pdict.OAuthProviderID = provider;
	
	if (null == expectedState) {
		Logger.warn(THIS_SCRIPT + "Exiting because the OAuth state in the session is null.");
		//return PIPELET_ERROR;	
		expectedState = state.value;
	}
	
	if (null != state && expectedState != state) {
		Logger.warn(THIS_SCRIPT + "Exiting because the state passed in the request does not match the state in the session.");
		return PIPELET_ERROR;	
	}

	var redirectURL : String = request.httpProtocol + "://" + request.httpHost + request.httpPath;

	
	
	//Test env is commented out.
	var baseURL : String = globalHelper.getSfCustomerPortalBaseURL();
	var clientId: String =  sleepysHelper.getSFClientID(); //'3MVG9GiqKapCZBwHVr.WKWayE9keZeKCz8nGMn79SN1vTjXq9C.3nGT_TBk4GdV_Zz7DvUs4wQQI.QW_kMco1';
	var clientSecret:String = sleepysHelper.getSFClientSecret(); //'7153345151976192980'; 
	
	var formBody = 'scope=&code='+code+'&client_id=' + clientId + '&client_secret=' + clientSecret 
		+ '&grant_type=authorization_code&redirect_uri='+redirectURL;
	try {
		var service = serviceHelper.createService({
			"serviceName": "salesforce.customer.getToken",
			"contentType": "application/x-www-form-urlencoded; charset=UTF-8",
			'acceptType': 'application/json',
			"httpMethod": "POST",
			"headerParams": {
				"scope": "",
				"code": code,
				"client_id": clientId,
				"client_secret": clientSecret,
				"grant_type": "authorization_code",
				"redirect_uri": redirectURL
			}
		});

		//set endpoint
		var relativePath  : String =  service.getURL();
		var tokenURL : String = baseURL + relativePath;
        service.setURL(tokenURL);

		var result = service.call(formBody);
		var resultToken : String = result.object;

		if (result.error != 0 || result.errorMessage != null || result.mockResult) {
			Logger.warn(THIS_SCRIPT + "Got an error calling:" + tokenURL 
			+ ". The status code is:" + result.statusCode + " ,the text is:" + resultToken
			+ " and the error text is:" + result.errorMessage);
			return PIPELET_ERROR;
		}
		
		var tokenData : Object = JSON.parse(resultToken);
		if (null == tokenData) {
			Logger.warn(THIS_SCRIPT + "Data could not be extracted from the response:" + tokenData);
			return PIPELET_ERROR;
		}

		var accessToken : String = tokenData['access_token'];
		if (null == accessToken) {
			Logger.warn(THIS_SCRIPT + "Exiting because the [access_token] value is null.");
			return PIPELET_ERROR;	
		}
		pdict.AccessToken = accessToken;
		
		// the 'id' parameter is user's personal profile URL:
		var id : String = tokenData['id'];
		if (null == id) {
			Logger.warn(THIS_SCRIPT + "Exiting because the [id] value is null."); 
			return PIPELET_ERROR;	
		}
		pdict.ResponseOtherTokens = new HashMap();
		pdict.ResponseOtherTokens.put('id', id);
	} catch (e) {
		return PIPELET_ERROR;
	}
	return PIPELET_NEXT;
}
exports.execute = execute;