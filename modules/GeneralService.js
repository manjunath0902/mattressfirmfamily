/* API includes */
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

/** Global Variables */
var TIMEOUT_MSG = "Read timed out";
var TIMEOUT_ERROR = "Service Timed Out...";

/** General Service Configurations */
/*
 {
  "serviceName": "temp.service.id",
  "contentType": "application/x-www-form-urlencoded //etc.",
  'acceptType', 'application/json',
  "httpMethod": "GET/POST",
  "endpoint": "http/etc.com"
  "headerParams": {
    "param1": "1",
    "param2": "2",
    "param3": "3"
  }
}
*/

exports.createService = function (configs) {
    return LocalServiceRegistry.createService(configs.serviceName, {
        createRequest: function(service, args) {
            //Set Conetnt Type
            if(configs.contentType) {
                service.addHeader("Content-Type", configs.contentType);
            }

            //Set Accept Type
            if(configs.acceptType) {
                service.addHeader("Accept", configs.acceptType);
            }

            //Set Request Method
            if(configs.httpMethod) {
                service.setRequestMethod(configs.httpMethod);
            }

            //Set Header Parameters
            var headerParams = configs.headerParams;
            if(headerParams) {
                for (var param in headerParams) {
                    if (headerParams[param]) {
                        var key = param;
                        var value = headerParams[param];
                        service.addHeader(key, value);
                    }
                }
            }

            //Set Endpoint
            if(configs.endpoint) {
                service.setURL(configs.endpoint);
            }

           return args;
        },
        parseResponse : function(svc, listOutput) {
            return listOutput.text;
        },
        getRequestLogMessage : function(requestObj) {
           try {
               return requestObj;
           } catch(e) {}
           return requestObj;
       },
       getResponseLogMessage : function(responseObj) {
           if (responseObj instanceof dw.net.HTTPClient) {
               try {
                   return responseObj.text;
               } catch(e) {}
           }
           return responseObj;
       },
       filterLogMessage: function(msg) {
           if(msg === TIMEOUT_MSG) {
                return TIMEOUT_ERROR;
           }else {
                return msg;
           }
        }
    });
};
