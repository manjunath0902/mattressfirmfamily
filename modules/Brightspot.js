/* API includes */
var Site = require('dw/system/Site');



/**
 * Returns true if slot has custom attribute true
 *
 * @returns {Boolean}
 */
function validateSlot(slotContent) {
	if(slotContent.custom.isBrightspotSlot && Site.getCurrent().ID === 'Mattress-Firm' && (typeof slotContent.custom.brightspotPagePath !== 'undefined')) {
		return true;
	}
	return false;
};

/**
 * Returns true if asset has custom attribute true
 *
 * @returns {Boolean}
 */
function validateAsset(asset) {
	if(asset.custom.isBrightspotAsset && Site.getCurrent().ID === 'Mattress-Firm' && (typeof asset.custom.brightspotPagePath !== 'undefined')) {
		return true;
	}
	return false;
};

function renderBrightspotContents(element) {
    /* Local packages includes */
    var ServiceHelper = require("GeneralService");
    var errorMsg = '<!-- Some Error Occured While Calling Brightspot Service -->';
    var isElementValidated = false;
    if (element.custom.isBrightspotAsset) {
        isElementValidated = validateAsset(element);
    }
    else {
        isElementValidated = validateSlot(element);
    }
	if(isElementValidated) {
        var brightspotResource = element.custom.brightspotPagePath;
        try {
            var service = ServiceHelper.createService({
                "serviceName": "brightspot.homepage",
                "httpMethod": "GET"
            });
            //Update Service Endpoint
            var endpoint = service.getURL() + brightspotResource;
            service.setURL(endpoint);

            // make the call
		    var result = service.call();
            //error case
            if (result.error != 0 || result.errorMessage != null || result.mockResult) {
                return errorMsg;
            }
            else {
                var responseData = result.object;
                responseData = responseData.replace('<!DOCTYPE html>', '');
                responseData = responseData.replace('<html', '<brightspot-html');
                responseData = responseData.replace('</html>', '</brightspot-html>');
                responseData = responseData.replace('<head>', '<brightspot-head>');
                responseData = responseData.replace('</head>', '</brightspot-head>');
                return responseData;
            }
        }
        catch(error) {
            return errorMsg;
        }
	}
	return errorMsg;
}

exports.validateSlot = validateSlot;
exports.validateAsset = validateAsset;
exports.renderBrightspotSlot = renderBrightspotContents;
exports.renderBrightspotAsset = renderBrightspotContents;