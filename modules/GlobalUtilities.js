/* API includes */
var Site = require('dw/system/Site');

/** Returns Logout URL by Combining Base URL & Relative Path of Logout Resource */
exports.getSfCustomerPortalLogoutURL = function () {
    return Site.getCurrent().getCustomPreferenceValue("sfCustomerPortalBaseURL") + Site.getCurrent().getCustomPreferenceValue("sfLogoutURL");
};

/** Returns Redirect URL by Combining Base URL & Relative Path of Redirect Resource */
exports.getSfCustomerPortalLandingURL = function () {
    return Site.getCurrent().getCustomPreferenceValue("sfCustomerPortalBaseURL");
};

/** Returns Create Account URL by Combining Base URL & Relative Path of Register Resource */
exports.getSfCustomerPortalCreateAccount = function () {
    return Site.getCurrent().getCustomPreferenceValue("sfCustomerPortalBaseURL") + Site.getCurrent().getCustomPreferenceValue("sfCreateAccountLink");
};

/** Returns Static SalesForce OAuth2 Provider ID */
exports.getSfOAuth2ProviderID = function () {
    return "CommunityCloud";
};

/** Returns Waranty Claim URL by Combining Base URL & Relative Path of Waranty Claim Resource */
exports.getSfWarantyClaimURL = function () {
    return Site.getCurrent().getCustomPreferenceValue("sfCustomerPortalBaseURL") + Site.getCurrent().getCustomPreferenceValue("sfWarrantyClaimUrl");
};

/** Returns Customer Portal Base URL */
exports.getSfCustomerPortalBaseURL = function () {
    return Site.getCurrent().getCustomPreferenceValue("sfCustomerPortalBaseURL");
};