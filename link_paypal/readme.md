PayPal 2019 19.1.1 Link Cartridge
Please check ./documentation/EC Integration Guide.pdf (for SiteGenesis) and ./documentation/EC SFRA Integration Guide.pdf (for SFRA) as the integration reference.

The github repo / issues section are generally unmonitored. If you have any questions or concerns please contact the PayPal Braintree plugins team at braintreepaypalplugins@paypal.com
