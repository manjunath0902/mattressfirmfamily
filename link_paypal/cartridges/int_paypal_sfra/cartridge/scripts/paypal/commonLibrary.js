'use strict';

/* global empty */

var Transaction = require('dw/system/Transaction');

var commonLibrary = {};

/**
 * Converts map to object
 *
 * @param {dw.util.Map} map map to transform
 * @returns {Object} filtered
 */
commonLibrary.mapToObject = function (map) {
    if (empty(map)) {
        throw new Error('Cannot convert non map object or its sub class instance');
    }

    var data = {};
    var keys = map.keySet();
    for (var i = 0, lenght = keys.size(); i < lenght; i++) {
        var key = keys[i];
        data[key] = map.get(key);
    }

    return data;
};

/**
 * Returns Object with first, second, last names from a simple string person name
 *
 * @param {string} name Person Name
 * @returns {Object} person name Object
 */
commonLibrary.createPersonNameObject = function (name) {
    var nameNoLongSpaces = name.trim().replace(/\s+/g, ' ').split(' ');
    if (nameNoLongSpaces.length === 1) {
        return {
            firstName: name,
            secondName: null,
            lastName: null
        };
    }
    var firstName = nameNoLongSpaces.shift();
    var lastName = nameNoLongSpaces.pop();
    var secondName = nameNoLongSpaces.join(' ');
    return {
        firstName: firstName,
        secondName: secondName.length ? secondName : null,
        lastName: lastName
    };
};

/**
 * Returns null if value length is too long
 *
 * @param {string} param parameter value
 * @param {number} size max length of parameter
 * @returns {dw.util.HashMap} parameter value or null
 */
commonLibrary.validateParameterLength = function (param, size) {
    var result = null;

    if (empty(param) || param === 'null') {
        return result;
    }

    result = param.length <= size ? param : null;

    return result;
};

/**
 * Returns truncated description
 *
 * @param {string} description description
 * @param {number} size truncate size
 * @returns {string} truncated description
 */
commonLibrary.truncatePreferenceString = function (description, size) {
    if (!description) {
        return null;
    }
    var truncatedString = null;
    var encodedDescription = encodeURI(description);
    var newDescription = encodedDescription.substring(0, size);
    var separator = newDescription.lastIndexOf('.') + 1;
    if (separator > 0) {
        truncatedString = newDescription.slice(0, separator);
    } else {
        truncatedString = newDescription;
    }

    return truncatedString;
};

/**
 * Removes all payment instruments from the basket
 *
 * @param {dw.order.Basket} basket Basket object
 */
commonLibrary.removeAllPaymentInstrumentsFromBasket = function (basket) {
    Transaction.wrap(function () {
        var paymentInstruments = basket.getPaymentInstruments();
        var iterator = paymentInstruments.iterator();
        var instument = null;
        while (iterator.hasNext()) {
            instument = iterator.next();
            basket.removePaymentInstrument(instument);
        }
    });
};

module.exports = commonLibrary;
