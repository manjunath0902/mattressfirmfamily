# PayPal integration test

## Before runinng
To run integration test node version 8+ required. 
Install all dependencies from package.json in the root folder with `npm install` command

Add you sandbox credentials, url, code version in `dw.json` file in the root folder

File  `test/int_paypal_sfra/integration/it.config.js` contains option to test PayPal financial credit (default is false) and product id to test (default is false) sony-kdl-40w4100M.
## Test run
To run test use `npm run paypalTest:integration`
