
Feature('Checkout');

const assert = require('assert');

const data = require('./data.json');

Before( (I) => {
	I.amOnPage(data.loginPage)
	I.login(data.emailSavedPaypal, data.passwordSavedPaypal)
	I.wait(2)

});

Scenario(
'User can place order on shopping cart using saved PayPal account', 
async (I) => {
	
	I.amOnPage(data.productPageTV)	
	I.wait(4)
	I.click("Add to Cart")

	I.wait(3)
	I.click("div.minicart-total.hide-link-med")
	I.wait(4)
	
	I.switchTo('iframe.zoid-component-frame.zoid-visible')
	I.click('div.paypal-button')
	I.wait(2)
	I.switchTo()
	I.switchTo()
	I.fillShippingData(data.firstName, data.lastName, data.addressOne, data.country, data.state, data.city, data.zip, data.phone)
	I.click('Save')
	I.wait(4)
	I.click('Place Order')
	
	I.wait(10)
	//asserts for verifying placing order:
	I.see('Receipt')
	I.see('Thank you for your order')
	I.see("Payment:")
	I.see("PayPal")
	I.see(data.firstName)
	I.see(data.lastName)
	I.see(data.addressOne)
	I.see(data.phone)
	}
);

/*
//A scenario for removing saved addresses if any
Scenario(
 'Remove all saved addresses',
async (I) => {
	
	I.wait(2)
	I.click('View')
	I.wait(2) // Go to Edit addresses page
	I.click('button.remove-btn.remove-address.btn-light')
 	I.click('Yes')
	});


*/