exports.config = {
  tests: './*_test.js',
  output: './output',
  helpers: {
    WebDriver: {
      url: '',
      browser: 'chrome'
    }
  },
  plugins: {
    allure: {
		enabled : true
	},
	stepByStepReport: {
		enabled: false
	}
  },
  include: {
	I: './steps_file.js'
	
  },
  bootstrap: null,
  mocha: {},
  name: 'pyplec-auto'
}
