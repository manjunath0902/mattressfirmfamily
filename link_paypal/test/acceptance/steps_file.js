
// in this file you can append custom step methods to 'I' object

module.exports = function() {
  return actor({
	  
	login: function(email, password) {
		this.fillField('Email', email);
		this.fillField('Password', password);
		this.wait(2);
		this.click("button.btn.btn-block.btn-primary");
	},
	
	fillCardData: function(nameOnCard, cardNumber, cvv, expiration) {
		this.fillField("Name on Card", nameOnCard);
		this.switchTo("#braintree-hosted-field-number");
		this.fillField("#credit-card-number", cardNumber);
		this.switchTo();
		this.switchTo("#braintree-hosted-field-cvv");
		this.fillField("#cvv", cvv);
		this.switchTo();
		this.switchTo("#braintree-hosted-field-expirationDate");
		this.fillField("expiration", expiration);
		this.switchTo();
		this.switchTo();
	},

	checkoutPaypal: async function(paypalEmail, paypalPassword) {
		this.click("PayPal")
	
		const handleBeforePopup = await this.grabCurrentWindowHandle();
		const urlBeforePopup = await this.grabCurrentUrl();
		const allHandlesBeforePopup = await this.grabAllWindowHandles();
		assert.equal(allHandlesBeforePopup.length, 1, 'Single Window');
		
		this.switchTo("iframe.zoid-component-frame.zoid-visible")
		this.click("div.paypal-button")
				
		const allHandlesAfterPopup = await this.grabAllWindowHandles();
		assert.equal(allHandlesAfterPopup.length, 2, 'Two Windows');
		
		await this.switchToWindow(allHandlesAfterPopup[1]);
		const urlAfterPopup = await this.grabCurrentUrl();
		
		this.wait(6)
		this.fillField("Email", paypalEmail)
		this.click("Next")
		this.wait(3)
		this.fillField("Password", paypalPassword)
		this.click("Log In")
		this.wait(10)
		this.click("Continue")
		//I.wait(7)
		//I.click("Continue")
		await this.switchToWindow(handleBeforePopup);
		const currentURL = await this.grabCurrentUrl();
		assert.equal(currentURL, urlBeforePopup, 'Expected URL: Main URL');
	},
	
	cartPaypal: async function(paypalEmail, paypalPassword) {
	
		const handleBeforePopup = await this.grabCurrentWindowHandle();
		const urlBeforePopup = await this.grabCurrentUrl();
		const allHandlesBeforePopup = await this.grabAllWindowHandles();
		//assert.equal(allHandlesBeforePopup.length, 1, 'Single Window');
		this.switchTo("iframe.zoid-component-frame.zoid-visible");
		this.click("div.paypal-button");
		const allHandlesAfterPopup = await this.grabAllWindowHandles();
		//assert.equal(allHandlesAfterPopup.length, 2, 'Two Windows');
		await this.switchToWindow(allHandlesAfterPopup[1]);
		const urlAfterPopup = await this.grabCurrentUrl();
		this.wait(6);
		this.fillField("Email", paypalEmail);
		this.click("Next");
		this.wait(3);
		this.fillField("Password", paypalPassword);
		this.click("Log In");
		this.wait(10);
		this.click("Continue");
		//I.wait(7);
		//I.click("Continue");
		await this.switchToWindow(handleBeforePopup);
		const currentURL = await this.grabCurrentUrl();
		//assert.equal(currentURL, urlBeforePopup, 'Expected URL: Main URL');
	},
	
	fillShippingData: function(firstName, lastName, addressOne, country, state, city, zip, phone) {
		this.fillField("First Name", firstName)
		this.fillField("Last Name", lastName)
		this.fillField("Address 1", addressOne)
		this.selectOption("Country", country)
		this.selectOption("State", state)
		this.fillField("City", city)
		this.fillField("ZIP Code", zip)
		this.fillField("Phone Number", phone)
	}, 
	
	fillPaypalData: function(paypalEmail, paypalPassword) {
		
		this.wait(6)
		this.fillField("Email", paypalEmail)
		this.click("Next")
		this.wait(3)
		this.fillField("Password", paypalPassword)
		this.click("Log In")
		this.wait(10)
		this.click("Continue")
		
	},
	
	fillPaypalCreditData: function(paypalCreditEmail, paypalCreditPassword) {

		this.wait(15)
		this.click("More info")
		this.click("div.chooseOffer.buttons")
		this.wait(10)
		this.switchTo("iframe")
		this.fillField("Email", paypalCreditEmail);
		this.fillField("Password", paypalCreditPassword);
		this.click("Log In")
		this.wait(12)
		this.click("Continue")
		this.wait(4)
	}, 
	
	fillPaypalCreditDataPPCart: function(paypalCreditEmail, paypalCreditPassword) {

		this.wait(15)
		this.switchTo("iframe")
		this.fillField("Email", paypalCreditEmail);
		this.fillField("Password", paypalCreditPassword);
		this.click("Log In")
		this.wait(12)
		this.see('PayPal Credit')
		this.click("Agree & Pay")
		this.wait(8)
	}, 
	
	fillPaypalCreditDataPPCheckout: function(paypalCreditEmail, paypalCreditPassword) {

		this.wait(15)
		this.switchTo("iframe")
		this.fillField("Email", paypalCreditEmail);
		this.fillField("Password", paypalCreditPassword);
		this.click("Log In")
		this.wait(12)
		this.see('PayPal Credit')
		this.click("Agree & Continue")
		this.wait(8)
	} 


    // Define custom steps here, use 'this' to access default methods of I.
    // It is recommended to place a general 'login' function here.
  });
};

