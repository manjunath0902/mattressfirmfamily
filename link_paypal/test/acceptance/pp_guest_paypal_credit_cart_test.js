
Feature('Checkout');

const assert = require('assert');

const data = require('./data.json');



Scenario(
'Guest can place order using PayPal credit on shopping cart page', 
async (I) => {
	I.amOnPage(data.productPageTV)
	
	I.click("Add to Cart")
	I.wait(2)
	I.click('div.minicart-total.hide-link-med')
	I.wait(5)
	
	const handleBeforePopup = await I.grabCurrentWindowHandle();
	const urlBeforePopup = await I.grabCurrentUrl();
	const allHandlesBeforePopup = await I.grabAllWindowHandles();
	assert.equal(allHandlesBeforePopup.length, 1, 'Single Window');
	I.switchTo("iframe.zoid-component-frame.zoid-visible");
	I.click("credit");
	
	const allHandlesAfterPopup = await I.grabAllWindowHandles();
	assert.equal(allHandlesAfterPopup.length, 2, 'Two Windows');
	await I.switchToWindow(allHandlesAfterPopup[1]);
	const urlAfterPopup = await I.grabCurrentUrl();
	
	I.fillPaypalCreditData(data.paypalCreditEmail, data.paypalCreditPassword)
	
	await I.switchToWindow(handleBeforePopup);
	const currentURL = await I.grabCurrentUrl();
	
	//assert.equal(currentURL, urlBeforePopup, 'Expected URL: Main URL');
	
	I.wait(7)
	I.see("Place Order")
	
	I.click("Place Order")
	I.wait(15)

	//asserts for verifying placing order:
	I.see('Receipt')
	I.see('Thank you for your order')
	
	}
);
