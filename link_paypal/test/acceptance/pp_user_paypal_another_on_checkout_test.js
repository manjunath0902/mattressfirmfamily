Feature('Checkout');

const assert = require('assert');

const data = require('./data.json');


Before( (I) => {
	I.amOnPage(data.loginPage)
	I.login(data.email, data.password)
	I.wait(2)

});


Scenario(
'Verify that user can select another account on checkout flow', 
async (I) => {

	I.amOnPage(data.productPageTV)
	I.wait(2)
	I.click("Add to Cart")
	I.wait(2)

	I.click("div.minicart-total.hide-link-med")
	I.wait(5)
	I.click("Checkout")
	I.wait(3)
	
	I.fillShippingData(data.firstName, data.lastName, data.addressOne, data.country, data.state, data.city, data.zip, data.phone)
	
	I.click("Next: Payment")
	I.wait(3)
	I.fillField("#email", data.email)
	I.fillField('#phoneNumber', data.phone)

	I.click('PayPal')
	pause()
 	I.click('div.form-group.custom-control.custom-checkbox') // unselect saved account
 	I.wait(3)

	handleBeforePopup = await I.grabCurrentWindowHandle();
	urlBeforePopup = await I.grabCurrentUrl();
	allHandlesBeforePopup = await I.grabAllWindowHandles();
	assert.equal(allHandlesBeforePopup.length, 1, 'Single Window');
	
	I.switchTo("iframe.zoid-component-frame.zoid-visible")
	I.click("div.paypal-button")
	I.wait(3)
	
	allHandlesAfterPopup = await I.grabAllWindowHandles();
	assert.equal(allHandlesAfterPopup.length, 2, 'Two Windows');
	
	await I.switchToWindow(allHandlesAfterPopup[1]);
	urlAfterPopup = await I.grabCurrentUrl();
	
	I.wait(10)
	I.fillField("Email", data.paypalEmailCheck)
	I.fillField("Password", data.paypalPasswordCheck)
	I.click("Log In")
	I.wait(10)
	I.click("Continue")
	I.wait(2)
	I.click("Continue")
	I.wait(8)
	
	await I.switchToWindow(handleBeforePopup);
	currentURL = await I.grabCurrentUrl();
	assert.equal(currentURL, urlBeforePopup, 'Expected URL: Main URL');
	
	I.click('Next: Place Order')
	I.wait(10)
	I.click('Place Order')
	
	I.wait(10)

	//asserts for verifying placing order:
	I.see('Receipt')
	I.see('Thank you for your order')
	I.see("Payment:")
	I.see("PayPal")
	
	}
);


//In case of saving address - scenario for removing it:
Scenario(
 'Remove all saved addresses',
async (I) => {
	I.amOnPage('addressbook')
	I.wait(2)
	I.click('button.remove-btn.remove-address.btn-light')
 	I.wait(3)
 	I.click('Yes')
	});
