Feature('Checkout');

const assert = require('assert');

const data = require('./data.json');


Before( (I) => {
	I.amOnPage(data.loginPage)
	I.login(data.email, data.password)
	I.wait(2)

});


Scenario(
'Verify that saved default PayPal account is selected on checkout flow', 
async (I) => {

	I.amOnPage(data.productPageTV)
	I.wait(2)
	I.click("Add to Cart")
	I.wait(2)

	I.click("div.minicart-total.hide-link-med")
	I.wait(5)
	I.click("Checkout")
	I.wait(3)
	
	I.fillShippingData(data.firstName, data.lastName, data.addressOne, data.country, data.state, data.city, data.zip, data.phone)
	
	I.click("Next: Payment")
	I.wait(3)
	I.fillField("#email", data.email)
	I.fillField('#phoneNumber', data.phone)

	I.click('PayPal')

	I.click('Next: Place Order')
	I.wait(5)
	I.click('Place Order')
	
	I.wait(10)

	//asserts for verifying placing order:
	I.see('Receipt')
	I.see('Thank you for your order')
	I.see("Payment:")
	I.see("PayPal")
	
	}
);

/*
//A scenario for removing saved addresses if any
Scenario(
 'Remove all saved addresses',
async (I) => {
	pause()
	I.wait(2)
	I.click('Edit') // Go to Edit addresses page
	I.click('button.remove-btn.remove-address.btn-light')
 	I.click('Yes')
	});
*/