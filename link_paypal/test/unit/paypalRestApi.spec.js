const { paypalRestApiPath } = require('./path');
const { stub, assert } = require('sinon');

const prefs = {};
const paypalLogger = {
		getLogger: () => { }
}
const paypalApi = require('proxyquire').noCallThru()(paypalRestApiPath, {
	'~/cartridge/scripts/paypal/logger': paypalLogger,
    '~/cartridge/config/paypalPreferences': prefs
});


describe('paypalApi', function () {

	before(function () {
		stub(paypalApi, 'call');
	});

	afterEach(function () {
		paypalApi.call.reset();
	});

	after(function () {
		paypalApi.call.restore();
	});

	describe('getToken', function () {
		before(function () {
			paypalApi.oauth2.getToken();
		});
		it('should call paypal api with valid arguments', function () {
			assert.calledWith(paypalApi.call, 'POST', 'oauth2/token', {
				grant_type: 'client_credentials'
			}, 'application/x-www-form-urlencoded');
		});
	});

	describe('getActiveMerchantFinancingOptions', function () {
		describe('if country code was provided', function () {
			before(function () {
				paypalApi.credit.getActiveMerchantFinancingOptions('US');
			});
			it('should call paypal api with valid arguments', function () {
				const path = 'credit/active-merchant-financing-options?country_code=US'
				assert.calledWith(paypalApi.call, 'GET', path);
			});
		});
		describe('if country code was not provided', function () {
			before(function () {
				paypalApi.credit.getActiveMerchantFinancingOptions();
			});
			it('should call paypal api with valid arguments', function () {
				const path = 'credit/active-merchant-financing-options'
				assert.calledWith(paypalApi.call, 'GET', path);
			});
		});
	});

	describe('getCalculatedFinancingOptions', function () {
		const data = {
			id: '1'
		};
		before(function () {
			paypalApi.credit.getCalculatedFinancingOptions(data);
		});
		it('should call paypal api with valid arguments', function () {
			assert.calledWith(paypalApi.call, 'POST', 'credit/calculated-financing-options', data);
		});
	});
});