var { expect } = require('chai');
var { accountHelperPath } = require('./path');
var { stub, assert } = require('sinon');

const prefs = {
    PP_BillingAgreementState: null
}
const obj = {
    referenceId: 'id1',
    currencyCode: 'usd',
    error: undefined,
    responseData: {
        l_errorcode0: 10204
    }
}

let getCustomerBillingAgreement = stub();

let paypalHelper = {
    getCustomerBillingAgreement
}

const accountHelper = require('proxyquire').noCallThru()(accountHelperPath, {
    'dw/system/Transaction' : {
        wrap: (atr) => { atr() }
    },
    '~/cartridge/config/paypalPreferences' : prefs,
    '~/cartridge/scripts/paypal/paypalApi': {
        baUpdate: () => obj
    },
    '~/cartridge/scripts/paypal/paypalHelper': paypalHelper
});
require('dw-api-mock/demandware-globals');

describe('account helper file', () => {
    describe('determineUniqueAddressID', () => {
        describe('if no city was provided or city is empty string', () => {
            it('should return null', () => {
                expect(accountHelper.determineUniqueAddressID(null)).to.be.null;
            });
            it('should return null', () => {
                expect(accountHelper.determineUniqueAddressID('')).to.be.null;
            });
        });

        describe('if valid city was provided', () => {
            it('should return city plus counter', () => {
                var callback = stub();
                callback.onFirstCall().returns(1).onSecondCall().returns(null)
                const addressBook = {
                    getAddress: callback
                }
                expect(accountHelper.determineUniqueAddressID('London', addressBook)).to.be.equal('London-1');
            });
        });
    });
    describe('getEquivalentAddress', () => {
        describe('if iterator has next value and isEquivalentAddress equal true', () => {
            it('should return adress', () => {
                const addressBook = {
                    addresses: {
                        iterator: () => {
                            return new dw.util.Iterator([{
                                isEquivalentAddress: () => true,
                                id: 123
                                }])
                            }
                        }
                    }
                expect(accountHelper.getEquivalentAddress(addressBook, 'New Yorker street 5').id).to.be.equal(123);
            })
        })
        describe('if iterator does not have next value', () => {
            const addressBook = {
                addresses: {
                    iterator: () => new dw.util.Iterator([{
                            isEquivalentAddress: () => false
                        }])
                }
            }
            it('should return null', () => {
                expect(accountHelper.getEquivalentAddress(addressBook, 'New Yorker street 5')).to.be.null;
            })
        })
    })
    describe('getActualBillingAgreementData', () => {
        describe('if customer.authenticated equal false', () => {
            before(() => {
                customer = {
                    authenticated: false
                }
            })
            after(() => {
                customer = {
                    authenticated: undefined
                }
            })
            it('should return null', () => {
                let basket = {};
                expect(accountHelper.getActualBillingAgreementData(basket)).to.be.null;
            })
        })
        describe('if getCustomerBillingAgreement.hasAnyBillingAgreement equal false and prefs.PP_BillingAgreementState equal "DoNotCreate"', () => {
            before(() => {
                customer = {
                    authenticated: true
                };
                prefs.PP_BillingAgreementState = "DoNotCreate"
            })
            after(() => {
                customer = {
                    authenticated: undefined
                };
                prefs.PP_BillingAgreementState = null;
            })
            it('should return null', () => {
                paypalHelper.getCustomerBillingAgreement.withArgs('usd').returns({
                    hasAnyBillingAgreement: false
                })
                let basket = {
                    getCurrencyCode: () => 'usd'
                };
                expect(accountHelper.getActualBillingAgreementData(basket)).to.be.null;
            })
        })
        describe('if getCustomerBillingAgreement.hasAnyBillingAgreement equal true and prefs.PP_BillingAgreementState not equal "DoNotCreate"', () => {
            before(() => {
                prefs.PP_BillingAgreementState = 'Create';
                customer = {
                    authenticated: true
                }
                obj.error = false;
            })
            after(() => {
                customer = {
                    authenticated: undefined
                }
                obj.error = undefined;
            })
            it('should return response', () => {
                paypalHelper.getCustomerBillingAgreement.withArgs('usd').returns({
                    hasAnyBillingAgreement: true,
                    getDefault: () => ({id: 'id1'}),
                    currencyCode: 'usd'
                })
                let basket = {
                    getCurrencyCode: () => 'usd'
                };
                expect(accountHelper.getActualBillingAgreementData(basket)).to.deep.equal(obj);
            })
        })
        describe('if response.error equal true and response.responseData equal false', () => {
            const remove = stub();
            before(() => {
                prefs.PP_BillingAgreementState = 'Create';
                customer = {
                    authenticated: true
                }
                obj.error = true;
            })
            after(() => {
                customer = {
                    authenticated: undefined
                }
                obj.error = undefined;
            })
            it('should return null', () => {
                paypalHelper.getCustomerBillingAgreement.withArgs('usd').returns({
                    hasAnyBillingAgreement: true,
                    getDefault: () => ({id: 'id1'}),
                    remove,
                    currencyCode: 'usd'
                })
                let basket = {
                    getCurrencyCode: () => 'usd'
                };
                expect(accountHelper.getActualBillingAgreementData(basket)).to.be.null;
            })
        })
        describe('if response.responseData equal true', () => {
            let remove;
            before(() => {
                prefs.PP_BillingAgreementState = 'Create';
                customer = {
                    authenticated: true
                }
                obj.error = true;
                obj.responseData.l_errorcode0 = 10204;
                let basket = {
                    getCurrencyCode: () => 'usd'
                };
                remove = stub();
                paypalHelper.getCustomerBillingAgreement.withArgs('usd').returns({
                    hasAnyBillingAgreement: true,
                    getDefault: () => {
                        return ({id: 'id1'})
                    },
                    remove,
                    currencyCode: 'usd'
                });
                accountHelper.getActualBillingAgreementData(basket);
            })
            after(() => {
                customer = {
                    authenticated: undefined
                }
                obj.error = undefined;
            })
            it('should remove referenceId from customerBillingAgreement object', () => {
                assert.calledWith(remove, 'id1');
            })
        })
    })
    describe('saveShippingAddressToAccountFromBasket', () => {
        const custom = {
            paypalDefaultShippingAddressCurrencyCode: ''
        };
        const setPhone = stub();
        describe('if customer.authenticated equals false', () => {
            before(() => {
                customer = {
                    authenticated: false,
                    profile: {
                        addressBook: {
                            adresses: 'address'
                        }
                    }
                },
                    basket = {
                        defaultShipment: {
                            shippingAddress: null
                        }
                    }
            });
            after(() => {
                customer = {
                    authenticated: undefined
                }
            })
            it('should return undefined', () => {
                expect(accountHelper.saveShippingAddressToAccountFromBasket(basket)).to.be.equal(undefined);
            })
        })
        describe('if customer.authenticated equals true and usedAdress equals null', () => {
            before(() => {
                customer = {
                    authenticated: true,
                    profile: {
                        addressBook: {
                            adresses: 'address'
                        }
                    }
                },
                    basket = {
                        defaultShipment: {
                            shippingAddress: null
                        }
                    }
            });
            after(() => {
                customer = {
                    authenticated: undefined
                }
            });
            it('should return undefined', () => {
                expect(accountHelper.saveShippingAddressToAccountFromBasket(basket)).to.be.equal(undefined);
            })
        })
        describe('if customer.authenticated equals true and usedAdress exists', () => {
            before(() => {
                customer = {
                    authenticated: true,
                    profile: {
                        addressBook: {
                            addresses: {
                                iterator: () => {
                                    return new dw.util.Iterator([{
                                        custom: {
                                            paypalDefaultShippingAddressCurrencyCode: null
                                        }
                                    }])
                                }
                            }
                        }
                    }
                };
                basket = {
                    defaultShipment: {
                        shippingAddress: {
                            phone: '0931234567'
                        }
                    },
                    getCurrencyCode: () => 'usd'
                };
                stub(accountHelper, 'getEquivalentAddress').returns({
                    setPhone,
                    custom
                });
                accountHelper.saveShippingAddressToAccountFromBasket(basket);
            });
            after(() => {
                customer = {
                    authenticated: undefined
                }
            });
            it('should assign address', () => {
                expect(custom.paypalDefaultShippingAddressCurrencyCode).to.be.equal('usd');
            })
            it('should assign phone', () => {
                assert.calledWith(setPhone, '0931234567');
            })
            describe('if address equals null', () => {
                before(() => {
                    customer = {
                        authenticated: true,
                        profile: {
                            addressBook: {
                                addresses: {
                                    iterator: () => {
                                        return new dw.util.Iterator([{
                                            custom: {
                                                paypalDefaultShippingAddressCurrencyCode: null
                                            }
                                        }])
                                    }
                                }
                            }
                        }
                    };
                    basket = {
                        defaultShipment: {
                            shippingAddress: {
                                phone: '0931234567'
                            }
                        },
                        getCurrencyCode: () => 'usd'
                    }
                    accountHelper.getEquivalentAddress.returns(null);
                    stub(accountHelper, 'createAddress').returns({
                        setPhone,
                        custom
                    });
                    accountHelper.saveShippingAddressToAccountFromBasket(basket);
                })
                after(() => {
                    customer = {
                        authenticated: undefined
                    }
                    accountHelper.createAddress.restore();
                });
                it('should create address', () => {
                    assert.calledOnce(accountHelper.createAddress);
                })
            })
        })
    })
    describe('createAddress function', () => {
        describe('if addressID is empty', () => {
            let usedAddress = {
                city: ''
            }
            let addressBook;
            before(() => {
                stub(accountHelper, 'determineUniqueAddressID')
                accountHelper.determineUniqueAddressID.withArgs('', addressBook).returns(null);
            })
            after(() => {
                accountHelper.determineUniqueAddressID.restore();
            })
            it('should return undefined', () => {

                expect(accountHelper.createAddress(usedAddress, addressBook)).equal(null);
            })
        })
        describe('if addressID is not empty', () => {
            let address = {
                setFirstName: stub(),
                setLastName: stub(),
                setAddress1: stub(),
                setAddress2: stub(),
                setCity: stub(),
                setPostalCode: stub(),
                setStateCode: stub(),
                setCountryCode: stub()
            };
            let usedAddress = {
                firstName: 'Tim',
                lastName: 'Lu',
                address1: 'test string 1',
                address2: 'test string 2',
                city: 'Beijing',
                postalCode: '15525',
                stateCode: '021',
                countryCode: {
                    value: 12
                }
            }
            let addressBook = {
                createAddress: stub()
            };
            addressBook.createAddress.returns(address);
            before(() => {
                stub(accountHelper, 'determineUniqueAddressID');
                accountHelper.determineUniqueAddressID.withArgs('Beijing', addressBook).returns('Tim Lu, Beijing');
            })
            after(() => {
                accountHelper.determineUniqueAddressID.restore();
            })
            it('should check if it was called with argument', () => {
                accountHelper.createAddress(usedAddress, addressBook);
                assert.calledWith(addressBook.createAddress, 'Tim Lu, Beijing');
                assert.calledWith(address.setFirstName, 'Tim');
                assert.calledWith(address.setLastName, 'Lu');
                assert.calledWith(address.setAddress1, 'test string 1');
                assert.calledWith(address.setAddress2, 'test string 2');
                assert.calledWith(address.setCity, 'Beijing');
                assert.calledWith(address.setPostalCode, '15525');
                assert.calledWith(address.setStateCode, '021');
                assert.calledWith(address.setCountryCode, 12);
            })
        })
    })
});
