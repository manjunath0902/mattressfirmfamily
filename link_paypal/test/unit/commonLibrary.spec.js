var { expect } = require('chai');
var { commonLibraryPath } = require('./path');
var { stub, assert } = require('sinon');

const commonLibrary = require('proxyquire').noCallThru()(commonLibraryPath, {
    'dw/system/Transaction' : {
        wrap: (atr) => { atr() }
    }
});

require('dw-api-mock/demandware-globals');

describe('commonLibrary', () => {
    describe('createPersonNameObject function', () => {
        describe('trim, split an input string and assign a value', () => {
            let name;
            it('should return a firstName, lastName, secondName if the name.length === 3', () => {
                name = " Ivan Ivanovych Ivanov ";
                expect(commonLibrary.createPersonNameObject(name)).to.deep.equal({
                    firstName: "Ivan",
                    secondName: "Ivanovych",
                    lastName: "Ivanov"
                });
            });
            it('should return a firstName, lastName if the name.length === 2', () => {
                name = " Ivan  Ivanov ";
                expect(commonLibrary.createPersonNameObject(name)).to.deep.equal({
                    firstName: "Ivan",
                    secondName: null,
                    lastName: "Ivanov"
                });
            });
            it('should return firstName if name.length === 1', () => {
                name = "Ivan";
                expect(commonLibrary.createPersonNameObject(name)).to.deep.equal(
                    {
                        firstName: "Ivan",
                        secondName: null,
                        lastName: null
                    }
                );
            });
        });
    });
    describe('validateParameterLength function', () => {
        let size;
        describe('if param is empty or null', () => {
            it('should return null', () => {
                expect(commonLibrary.validateParameterLength('', size)).to.be.null;
            });
            it('should return null', () => {
                expect(commonLibrary.validateParameterLength(null, size)).to.be.null;
            });
        });
        describe('if param.length <= size', () => {
            size = 10;
            it('should reassign result', () => {
                expect(commonLibrary.validateParameterLength('teststring', size)).to.be.equal('teststring');
            });
        });
        describe('if param.length > size', () => {
            size = 10;
            it('should return null', () => {
                expect(commonLibrary.validateParameterLength('teststringbig', size)).to.be.null;
            });
        });
    });
    describe('truncatePreferenceString', () => {
        let size;
        describe('if description does not exist', () => {
            it('should return null', () => {
                expect(commonLibrary.truncatePreferenceString(null, size)).to.be.null;
            });
        });
        describe('if description exists', () => {
            describe('if separator > 0', () => {
                it('should slice', () => {
                    expect(commonLibrary.truncatePreferenceString('some.string', 6)).to.be.equal('some.');
                });
            });
            describe('if separator <= 0', () => {
                it('should substring only', () => {
                    expect(commonLibrary.truncatePreferenceString('somestring', 9)).to.be.equal('somestrin');
                })
            })
        })
    });
    describe('commonLibrary file', () => {
        describe('removeAllPaymentInstrumentsFromBasket function', () => {
            let instument = {
                iterator: () => {
                    return new dw.util.Iterator(['ggg'])
                }
            };
            let basket = {
                getPaymentInstruments: () => instument,
                removePaymentInstrument: stub()
            }
            basket.removePaymentInstrument.returns(true);
            it('should remove payment instrument from basket', () => {
                commonLibrary.removeAllPaymentInstrumentsFromBasket(basket);
                assert.calledWith(basket.removePaymentInstrument, 'ggg');
            })
        })
    })
});
