const { expect } = require('chai');
const { paypalHelperPath } = require('./path');
const { stub, assert } = require('sinon');

require('dw-api-mock/demandware-globals');

let prefs = {}
customer = {};
const createPersonNameObject = stub();

session = {
    forms: {
        billing: {
            paypal: {
                saveBillingAgreement: {
                    checked: true
                },
                useCustomerBillingAgreement: {
                    checked: false
                }
            }
        }
    }
}

const paypalHelper = require('proxyquire').noCallThru()(paypalHelperPath, {
    'dw/util/HashMap': dw.util.HashMap,
    'dw/system/Transaction': {
        wrap: (atr) => { atr() }
    },
    '~/cartridge/config/paypalPreferences': prefs,
    '~/cartridge/scripts/paypal/paypalApi': {},
    '~/cartridge/scripts/paypal/logger': {},
    '~/cartridge/scripts/paypal/commonLibrary': {
        createPersonNameObject
    }
});

describe('paypalHelper file', () => {
    describe('calculateNonGiftCertificateAmount function', () => {
        let Decimal = function (value) {
            this.value = value;
        };
        Decimal.prototype.subtract = function (money) {
            return new Decimal(this.value - money.value);
        };
        Decimal.prototype.value = null;

        let getAmount = new dw.value.Money(20);
        const gcPaymentInstrs = {
            iterator: () => {
                return new dw.util.Iterator([{
                    getPaymentTransaction: () => ({
                        getAmount: () => {
                            return getAmount;
                        }
                    })
                }]);
            }
        };

        let lineItemCtnr = {
            currencyCode: 'Us',
            getGiftCertificatePaymentInstruments: () => gcPaymentInstrs,
            totalGrossPrice: new dw.value.Money(100)
        };

        it('should return amount after discount', () => {
            expect(paypalHelper.calculateNonGiftCertificateAmount(lineItemCtnr).value).to.be.equal(80);
        })
    });
    describe('getPrefs function', () => {
        it('should return prefs as object and properties', () => {
            expect(paypalHelper.getPrefs()).equal(prefs);
        })
    })
    describe('checkIsNeedInitiateBillingAgreement function', () => {
        let isFromCart,
            basket = {
                getCurrencyCode: () => 'Usd'
            };
        describe('if one of BillingAgreementCases are true and BillingAgreementState not equal "DoNotCreate"', function () {
            before(() => {
                stub(paypalHelper, 'getCustomerBillingAgreement').withArgs('Usd').returns({
                    hasAnyBillingAgreement: false
                });
                prefs.PP_BillingAgreementState = 'BuyersChoose';
                customer.authenticated = true;
            })
            after(() => {
                paypalHelper.getCustomerBillingAgreement.restore();
                prefs = {};
                customer = {};
            })
            it('should return true', () => {
                isFromCart = true;
                expect(paypalHelper.checkIsNeedInitiateBillingAgreement(isFromCart, basket)).to.equal(true);
            })
        })
        describe('if all of BillingAgreementCases are false and BillingAgreementState equal "DoNotCreate"', function () {
            before(() => {
                stub(paypalHelper, 'getCustomerBillingAgreement').withArgs('Usd').returns({
                    hasAnyBillingAgreement: false
                });
                prefs.PP_BillingAgreementState = 'DoNotCreate';
            })
            after(() => {
                paypalHelper.getCustomerBillingAgreement.restore();
            })
            it('should return false', () => {
                isFromCart = true;
                expect(paypalHelper.checkIsNeedInitiateBillingAgreement(isFromCart, basket)).to.equal(false);
            })
        })
    })
    describe('getShippingRequestData function', function () {
        let lineItemContainer = {
            getDefaultShipment: () => ({
                getShippingAddress: () => null
            })
        };
        let isDataForDoreferenceTransaction,
            getShippingRequestData;
        data = {}

        describe('if shippingAddress is empty and equal Null', () => {
            before(() => {
                getShippingRequestData = paypalHelper.getShippingRequestData(lineItemContainer, isDataForDoreferenceTransaction);
            })
            it('should return not modified data', () => {
                expect(getShippingRequestData.get(null)).to.equal(undefined);
            })
        })
        describe('if shippingAddress NOT empty and prefix is "" ', () => {
            before(() => {
                lineItemContainer = {
                    getDefaultShipment: () => ({
                        getShippingAddress: () => ({
                            getFullName: () => 'James Smith',
                            getAddress1: () => 'street 1',
                            getAddress2: () => 'street 2',
                            getCity: () => 'Town',
                            getCountryCode: () => ({
                                getValue: () => 'uv8'
                            }),
                            getPhone: () => '12345678',
                            getStateCode: () => '123',
                            getPostalCode: () => 'zip99',
                            custom: {
                                isStore: true
                            }
                        })
                    })
                }
                isDataForDoreferenceTransaction = '1';
                getShippingRequestData = paypalHelper.getShippingRequestData(lineItemContainer, isDataForDoreferenceTransaction);
            })
            after(() => {
                lineItemContainer = {
                    getDefaultShipment: () => ({
                        getShippingAddress: () => ({
                            getFullName: () => 'James Smith',
                            getAddress1: () => 'street 1',
                            getAddress2: () => 'street 2',
                            getCity: () => 'Town',
                            getCountryCode: () => ({
                                getValue: () => 'uv8'
                            }),
                            getPhone: () => '12345678',
                            getStateCode: () => '123',
                            getPostalCode: () => 'zip99',
                            custom: {
                                isStore: false
                            }
                        })
                    })
                }
            })

            it('should set new key SHIPTOSTREET in HashMap', () => {
                expect(getShippingRequestData.get('SHIPTOSTREET')).to.equal('street 1');
            })
            it('should set new key SHIPTOSTREET2 in HashMap', () => {
                expect(getShippingRequestData.get('SHIPTOSTREET2')).to.equal('street 2');
            })
            it('should set new key SHIPTOCITY in HashMap', () => {
                expect(getShippingRequestData.get('SHIPTOCITY')).to.equal('Town');
            })
            it('should set new key SHIPTOCOUNTRYCODE in HashMap', () => {
                expect(getShippingRequestData.get('SHIPTOCOUNTRYCODE')).to.equal('UV8');
            })
            it('should set new key SHIPTONAME in HashMap with modified value', () => {
                expect(getShippingRequestData.get('SHIPTONAME')).to.equal('S2S James Smith');
            })
            it('should set new key SHIPTOPHONENUM in HashMap', () => {
                expect(getShippingRequestData.get('SHIPTOPHONENUM')).to.equal('12345678');
            })
            it('should set new key SHIPTOSTATE in HashMap', () => {
                expect(getShippingRequestData.get('SHIPTOSTATE')).to.equal('123');
            })
            it('should set new key SHIPTOZIP in HashMap', () => {
                expect(getShippingRequestData.get('SHIPTOZIP')).to.equal('zip99');
            })
        })
        describe('if prefix is "" and custom NOT stored', () => {
            before(() => {
                getShippingRequestData = paypalHelper.getShippingRequestData(lineItemContainer, isDataForDoreferenceTransaction);
            })
            after(() => {
                lineItemContainer = {
                    getDefaultShipment: () => ({
                        getShippingAddress: () => ({
                            getFullName: () => 'James Smith',
                            getAddress1: () => 'street 1',
                            getAddress2: () => 'street 2',
                            getCity: () => 'Town',
                            getCountryCode: () => ({
                                getValue: () => 'uv8'
                            }),
                            getPhone: () => '12345678',
                            getStateCode: () => '123',
                            getPostalCode: () => 'zip99',
                            custom: {
                                isStore: true
                            }
                        })
                    })
                }
            })
            it('should set new key SHIPTONAME in HashMap not modified', () => {
                expect(getShippingRequestData.get('SHIPTONAME')).to.equal('James Smith');
            })
        })
        describe('if shippingAddress NOT empty and prefix is "PAYMENTREQUEST_0_"', () => {
            before(() => {
                isDataForDoreferenceTransaction = null;
                getShippingRequestData = paypalHelper.getShippingRequestData(lineItemContainer, isDataForDoreferenceTransaction);
                lineItemContainer = {
                    getDefaultShipment: () => ({
                        getShippingAddress: () => ({
                            getFullName: () => 'James Smith',
                            getAddress1: () => 'street 1',
                            getAddress2: () => 'street 2',
                            getCity: () => 'Town',
                            getCountryCode: () => ({
                                getValue: () => 'uv8'
                            }),
                            getPhone: () => '12345678',
                            getStateCode: () => '123',
                            getPostalCode: () => 'zip99',
                            custom: {
                                isStore: true
                            }
                        })
                    })
                }
            })
            after(() => {
                lineItemContainer = {
                    getDefaultShipment: () => ({
                        getShippingAddress: () => ({
                            getFullName: () => 'James Smith',
                            getAddress1: () => 'street 1',
                            getAddress2: () => 'street 2',
                            getCity: () => 'Town',
                            getCountryCode: () => ({
                                getValue: () => 'uv8'
                            }),
                            getPhone: () => '12345678',
                            getStateCode: () => '123',
                            getPostalCode: () => 'zip99',
                            custom: {
                                isStore: false
                            }
                        })
                    })
                }
            })
            it('should set new key SHIPTOSTREET in HashMap', () => {
                expect(getShippingRequestData.get('PAYMENTREQUEST_0_SHIPTOSTREET')).to.equal('street 1');
            })
            it('should set new key SHIPTOSTREET2 in HashMap', () => {
                expect(getShippingRequestData.get('PAYMENTREQUEST_0_SHIPTOSTREET2')).to.equal('street 2');
            })
            it('should set new key SHIPTOCITY in HashMap', () => {
                expect(getShippingRequestData.get('PAYMENTREQUEST_0_SHIPTOCITY')).to.equal('Town');
            })
            it('should set new key SHIPTOCOUNTRYCODE in HashMap', () => {
                expect(getShippingRequestData.get('PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE')).to.equal('UV8');
            })
            it('should set new key SHIPTONAME in HashMap', () => {
                expect(getShippingRequestData.get('PAYMENTREQUEST_0_SHIPTONAME')).to.equal('S2S James Smith');
            })
            it('should set new key SHIPTOPHONENUM in HashMap', () => {
                expect(getShippingRequestData.get('PAYMENTREQUEST_0_SHIPTOPHONENUM')).to.equal('12345678');
            })
            it('should set new key SHIPTOSTATE in HashMap', () => {
                expect(getShippingRequestData.get('PAYMENTREQUEST_0_SHIPTOSTATE')).to.equal('123');
            })
            it('should set new key SHIPTOZIP in HashMap', () => {
                expect(getShippingRequestData.get('PAYMENTREQUEST_0_SHIPTOZIP')).to.equal('zip99');
            })
        })
        describe('if prefix is "PAYMENTREQUEST_0_" and custom NOT stored', () => {
            before(() => {
                lineItemContainer = {
                    getDefaultShipment: () => ({
                        getShippingAddress: () => ({
                            getFullName: () => 'James Smith',
                            getAddress1: () => 'street 1',
                            getAddress2: () => 'street 2',
                            getCity: () => 'Town',
                            getCountryCode: () => ({
                                getValue: () => 'uv8'
                            }),
                            getPhone: () => '12345678',
                            getStateCode: () => '123',
                            getPostalCode: () => 'zip99',
                            custom: {
                                isStore: false
                            }
                        })
                    })
                }
                getShippingRequestData = paypalHelper.getShippingRequestData(lineItemContainer, isDataForDoreferenceTransaction);
            })
            it('should set new key SHIPTONAME in HashMap with modified value', () => {
                expect(getShippingRequestData.get('PAYMENTREQUEST_0_SHIPTONAME')).to.equal('James Smith');
            })
        })
    })
    describe('removeAllPaypalPaymentInstruments', () => {
        after(() => {
            dw.order.PaymentMgr.getPaymentMethod.restore();
        });
        let paymentMethod, paymentProcessorId, removePaymentInstrument = stub();
        const paymentInstrument = {
            getPaymentMethod: () => paymentMethod,
        };
        const PaymentMethodInstance = {
            getPaymentProcessor: () => ({
                getID: () => paymentProcessorId
            })
        };
        const basket = {
            getPaymentInstruments: () => ({
                iterator: () => {
                    return new dw.util.Iterator([paymentInstrument]);
                }
            }),
            removePaymentInstrument
        };
        before(() => {
            stub(dw.order.PaymentMgr, "getPaymentMethod");
        });

        describe('if paymentProcessorId is PAYPAL', () => {
            it('remove paymentInstrument form the list', () => {
                paymentMethod = 'PayPal';
                paymentProcessorId = 'PAYPAL';
                dw.order.PaymentMgr.getPaymentMethod.withArgs(paymentMethod).returns(PaymentMethodInstance);
                paypalHelper.removeAllPaypalPaymentInstruments(basket);
                assert.calledWith(removePaymentInstrument, paymentInstrument);
            });
        });
    });
    describe('getPaypalPaymentInstrument iterate over paymentInstrumentList', () => {
        after(() => {
            dw.order.PaymentMgr.getPaymentMethod.restore();
        });
        let paymentMethod, paymentProcessorId;
        const PaymentMethodInstance = {
            getPaymentProcessor: () => ({
                getID: () => paymentProcessorId
            })
        };
        const basket = {
            getPaymentInstruments: () => ({
                iterator: () => {
                    return new dw.util.Iterator([{
                        getPaymentMethod: () => paymentMethod,
                    }]);
                }
            })
        };
        before(() => {
            stub(dw.order.PaymentMgr, "getPaymentMethod");
        });
        describe('if paymentMethod exists and paymentProcessorId is PAYPAL', () => {
            it('return paymentInstrument form the list', () => {
                paymentMethod = 'PayPal';
                paymentProcessorId = 'PAYPAL';
                dw.order.PaymentMgr.getPaymentMethod.withArgs('PayPal').returns(PaymentMethodInstance);
                expect(paypalHelper.getPaypalPaymentInstrument(basket).getPaymentMethod()).to.be.equal('PayPal');
            });
        });
        describe('if paymentMethod does not exist', () => {
            it('return null', () => {
                paymentMethod = null;
                dw.order.PaymentMgr.getPaymentMethod.returns(null);
                expect(paypalHelper.getPaypalPaymentInstrument(basket)).to.be.equal(null);
            });
        });
    });
    describe('calculateAppliedGiftCertificatesAmount function', () => {
        let getAmount = new dw.value.Money(17);
        const paymentInstruments = {
            iterator: () => {
                return new dw.util.Iterator([{
                    getPaymentTransaction: () => ({
                        getAmount: () => {
                            return getAmount;
                        }
                    })
                }]);
            }
        };
        let lineItemContainer = {
            getCurrencyCode: () => 'USD',
            getGiftCertificatePaymentInstruments: () => paymentInstruments
        };

        it('should return amount ', () => {
            expect(paypalHelper.calculateAppliedGiftCertificatesAmount(lineItemContainer).value).equal(17);
        });
    });
    describe('writeTransactionHistory', () => {
        let transactionId = 1;
        const customObject = {
            transactionsHistory: []
        };
        it('should return array with transactionId', () => {
            paypalHelper.writeTransactionHistory(customObject, transactionId);
            expect(customObject.transactionsHistory).to.deep.equal([1]);
        });
    });
    describe('updateBillingAddress', () => {
        let address;
        const data = {
            billingname: 'test',
            countrycode: 'test',
            phonenum: 'test',
            paymentrequest_0_shiptophonenum: 'test',
            street: 'test',
            city: 'test',
            zip: 'test',
            state: 'test'
        };
        describe('if !address', () => {
            before(() => {
                address = null;
            });
            it('should return false', () => {
                expect(paypalHelper.updateBillingAddress(data, address)).to.be.equal(false);
            });
        });
        describe('if address exists', () => {
            before(() => {
                address = {
                    setFirstName: stub(),
                    setSecondName: stub(),
                    setLastName: stub(),
                    setAddress1: stub(),
                    setCity: stub(),
                    setPostalCode: stub(),
                    setCountryCode: stub(),
                    setStateCode: stub(),
                    setPhone: stub()
                };
            });
            describe('if first name exists', () => {
                before(() => {
                    createPersonNameObject.returns(
                        names = {
                            firstName: 'Ivan'
                        }
                    );
                    paypalHelper.updateBillingAddress(data, address);
                });
                after(() => {
                    createPersonNameObject.reset();
                });
                it('should set firstName', () => {
                    assert.calledWith(address.setFirstName, 'Ivan');
                });
            });
            describe('if second name exists', () => {
                before(() => {
                    createPersonNameObject.returns(
                        names = {
                            secondName: 'Ivanovych'
                        }
                    );
                    paypalHelper.updateBillingAddress(data, address);
                });
                after(() => {
                    createPersonNameObject.reset();
                });
                it('should set secondName', () => {
                    assert.calledWith(address.setSecondName, 'Ivanovych');
                });
            });
            describe('if last name exists', () => {
                before(() => {
                    createPersonNameObject.returns(
                        names = {
                            lastName: 'Ivanov'
                        }
                    );
                    paypalHelper.updateBillingAddress(data, address);
                });
                after(() => {
                    createPersonNameObject.reset();
                });
                it('should set lastName', () => {
                    assert.calledWith(address.setLastName, 'Ivanov');
                });
            });
            describe('if address exists', () => {
                it('should create', () => {
                    assert.calledWith(address.setAddress1, 'test');
                    assert.calledWith(address.setCity, 'test');
                    assert.calledWith(address.setPostalCode, 'test');
                    assert.calledWith(address.setStateCode, 'test');
                    assert.calledWith(address.setPhone, 'test');
                });
            });
            describe('if prefs.isMFRA is true', () => {
                before(() => {
                    prefs.isMFRA = true;
                    paypalHelper.updateBillingAddress(data, address);
                });
                it('should create and set CountryCode to upperCase', () => {
                    assert.calledWith(address.setCountryCode, 'test');
                });
            });
            describe('if prefs.isMFRA is false', () => {
                before(() => {
                    prefs.isMFRA = false;
                    paypalHelper.updateBillingAddress(data, address);
                });
                it('should create and set CountryCode to LowerCase', () => {
                    assert.calledWith(address.setCountryCode, 'test');
                });
            });
        });
    });
    describe('getCustomerBillingAgreement', () => {
        let currencyCode, inpItem, state;
        let customerBillingAgreement;
        before(() => {
            customer = {
                profile: {
                    isCheckedUseCheckbox: undefined,
                    addressBook: 'address',
                    custom: {
                        paypalBillingAgreementData: JSON.stringify({
                            items: [{
                                id: 1
                            }]
                        })
                    }
                },
                authenticated: true
            }
            customerBillingAgreement = paypalHelper.getCustomerBillingAgreement(currencyCode);
        });
        describe('-remove method-', () => {
            let billingAgreementId;
            describe('if billingAgreementId empty', () => {
                it('should return false', () => {
                    billingAgreementId = null;
                    expect(customerBillingAgreement.remove(billingAgreementId)).to.be.equal(false);
                })
            })
            describe('if billingAgreementId not empty', () => {
                it('should return false', () => {
                    billingAgreementId = 1;
                    expect(customerBillingAgreement.remove(billingAgreementId)).to.be.equal(true);
                })
            })
        })
        describe('-getDefault method-', () => {
            describe('if items equal false', () => {
                before(() => {
                    customer = {
                        profile: {
                            addressBook: 'address',
                            custom: {
                                paypalBillingAgreementData: JSON.stringify({})
                            }
                        },
                        authenticated: true
                    }
                    customerBillingAgreement = paypalHelper.getCustomerBillingAgreement(currencyCode);
                });
                it('should return object with null values', () => {
                    expect(customerBillingAgreement.getDefault()).to.deep.equal({
                        id: null,
                        email: null,
                        currencyCode: null
                    });
                })
            });
            describe('if items equal true', () => {
                before(() => {
                    customer = {
                        profile: {
                            addressBook: 'address',
                            custom: {
                                paypalBillingAgreementData: JSON.stringify({
                                    items: [
                                        {
                                            id: 1,
                                            currencyCode: 'Usd'
                                        },
                                        {
                                            id: 2,
                                            currencyCode: 'Eur'
                                        }]
                                })
                            }
                        },
                        authenticated: true
                    }
                    customerBillingAgreement = paypalHelper.getCustomerBillingAgreement(currencyCode);
                });
                describe('if currencyCode exist and items currencyCode equal currencyCode', () => {
                    before(() => {
                        currencyCode = 'Usd';
                        customerBillingAgreement = paypalHelper.getCustomerBillingAgreement(currencyCode);
                    })
                    after(() => {
                        currencyCode = undefined;
                    })
                    it('should return items values', () => {
                        expect(customerBillingAgreement.getDefault()).to.deep.equal({ id: 1, currencyCode: 'Usd' });
                    })
                })
                describe('if currencyCode NOT exist but itemsfirst value exist', () => {
                    before(() => {
                        customer = {
                            profile: {
                                addressBook: 'address',
                                custom: {
                                    paypalBillingAgreementData: JSON.stringify({
                                        items: [
                                            {
                                                id: 1,
                                                currencyCode: 'Usd'
                                            }]
                                    })
                                }
                            },
                            authenticated: true
                        }
                        currencyCode = null;
                        customerBillingAgreement = paypalHelper.getCustomerBillingAgreement(currencyCode);
                    })
                    after(() => {
                        currencyCode = undefined
                    })
                    it('should return items first value', () => {
                        currencyCode = undefined;
                        expect(customerBillingAgreement.getDefault()).to.deep.equal({ id: 1, currencyCode: 'Usd' });
                    })
                })
            });
        });
        describe('-setUseCheckboxState method-', () => {
            it('should set checkboxState', () => {
                state = true;
                customerBillingAgreement.setUseCheckboxState(state);
                expect(customerBillingAgreement.isCheckedUseCheckbox).to.equal(true);
            })
        });
        describe('add func', () => {
            describe('if no email', () => {
                before(() => {
                    customer = {
                        profile: {
                            custom: {
                                paypalBillingAgreementData: JSON.stringify({
                                    items: []
                                })
                            }
                        }
                    }
                    inpItem = {
                        email: null,
                        id: '1'
                    }
                    customerBillingAgreement = paypalHelper.getCustomerBillingAgreement(currencyCode);
                });
                it('should return false', () => {
                    expect(customerBillingAgreement.add(inpItem)).to.be.equal(false);
                });
            });
            describe('if no id', () => {
                before(() => {
                    customer = {
                        profile: {
                            custom: {
                                paypalBillingAgreementData: JSON.stringify({
                                    items: []
                                })
                            }
                        }
                    }
                    inpItem = {
                        email: 'email@bla.com',
                        id: null
                    }
                    customerBillingAgreement = paypalHelper.getCustomerBillingAgreement(currencyCode);
                });
                it('should return false', () => {
                    expect(customerBillingAgreement.add(inpItem)).to.be.equal(false);
                });
            });
            describe('if id and email exist', () => {
                before(() => {
                    customer = {
                        profile: {
                            custom: {
                                paypalBillingAgreementData: JSON.stringify({
                                    items: []
                                })
                            }
                        }
                    }
                    inpItem = {
                        email: 'email@bla.com',
                        id: '1'
                    }
                    customerBillingAgreement = paypalHelper.getCustomerBillingAgreement(currencyCode);
                });
                it('should return true', () => {
                    expect(customerBillingAgreement.add(inpItem)).to.be.equal(true);
                });
            });
            describe('if id and email exist', () => {
                before(() => {
                    customer = {
                        profile: {
                            custom: {
                                paypalBillingAgreementData: JSON.stringify({
                                    items: [
                                        {
                                            email: 'email@bla.com',
                                            id: '1'
                                        }
                                    ]
                                })
                            }
                        }
                    }
                    customerBillingAgreement = paypalHelper.getCustomerBillingAgreement(currencyCode);
                });
                it('should return true', () => {
                    expect(customerBillingAgreement.add(inpItem)).to.be.equal(true);
                });
            });
        });
        describe('removeAll', () => {
            before(() => {
                customer = {
                    profile: {
                        custom: {
                            paypalBillingAgreementData: JSON.stringify({
                                items: []
                            })
                        }
                    }
                }
                customerBillingAgreement = paypalHelper.getCustomerBillingAgreement(currencyCode);
            });
            it('should make items equal []', () => {
                customerBillingAgreement.removeAll();
                expect(customerBillingAgreement.items).to.deep.equal([]);
            });
        });
        describe('getDefaultShippingAddress', () => {
            describe('if customer.profile', () => {
                before(() => {
                    currencyCode = 'usd';
                    customer = {
                        profile: {
                            custom: {
                                paypalBillingAgreementData: JSON.stringify({
                                    items: []
                                })
                            },
                            addressBook: {
                                addresses: {
                                    iterator: () => {
                                        return new dw.util.Iterator([{
                                            custom: {
                                                paypalDefaultShippingAddressCurrencyCode: 'euro'
                                            }
                                        }]);
                                    }
                                }
                            }
                        }
                    }
                    customerBillingAgreement = paypalHelper.getCustomerBillingAgreement(currencyCode);
                });
                it('should', () => {
                    expect(customerBillingAgreement.getDefaultShippingAddress()).to.be.equal(null);
                });
            });
            describe('if paypalDefaultShippingAddressCurrencyCode equals currencyCode', () => {
                before(() => {
                    currencyCode = 'usd';
                    customer = {
                        profile: {
                            custom: {
                                paypalBillingAgreementData: JSON.stringify({
                                    items: []
                                })
                            },
                            addressBook: {
                                addresses: {
                                    iterator: () => {
                                        return new dw.util.Iterator([{
                                            custom: {
                                                paypalDefaultShippingAddressCurrencyCode: currencyCode
                                            }
                                        }]);
                                    }
                                }
                            }
                        }
                    }
                    customerBillingAgreement = paypalHelper.getCustomerBillingAgreement(currencyCode);
                });
                it('should return address object', () => {
                    expect(customerBillingAgreement.getDefaultShippingAddress()).to.deep.equal({
                        custom: {
                            paypalDefaultShippingAddressCurrencyCode: 'usd'
                        }
                    });
                });
            });
        });
    });
});
