var { expect } = require('chai');
var { processorPath } = require('./path');
var { stub, assert } = require('sinon');

const prefs = {
	getPrefs: stub(),
	PP_BillingAgreementState: 'RequireAllBuyers',
	getCustomerBillingAgreement: stub()
}
let saveShippingAddressToAccountFromBasket = stub();
require('dw-api-mock/demandware-globals');

let accountHelpers = {
	saveShippingAddressToAccountFromBasket
}

const processor = require('proxyquire').noCallThru()(processorPath, {
	'dw/system/Transaction' : dw.system.Transaction,
	'dw/order': dw.order,
	'dw/value/Money': dw.value.Money,
	'dw/web/URLUtils': {},
	'~/cartridge/scripts/paypal/paypalHelper': prefs,
	'~/cartridge/scripts/paypal/paypalApi': {},
	'~/cartridge/scripts/paypal/accountHelpers': accountHelpers
});
describe('saveReferenceTransactionResult', function () {
	let transactionid, amt, currencycode, paymentstatus, correlationid;
	let paypalPaymentMethod = 'express';
	let paypalCorrelationId;
	let custom = {};
	let length = 1;
	let getPM = 'PayPal';
	let paypalPaymentStatus = {};
	let setPaymentStatus = stub();

	let data = {
		transactionid,
		amt,
		currencycode,
		paymentstatus,
		correlationid,
		paymentinfo_0_paymentstatus: 'Completed'
	};

	let order = {
		getPaymentInstruments: () => {
			return ([{
				getPaymentMethod: () => getPM,
				length
			}])
		},
		setPaymentStatus,
		custom
	}

	let setTransactionID = stub(); 

	let transactionsHistory;
	let paymentInstrument = {
		getPaymentTransaction: () => {
			return {
				setTransactionID,
				setAmount: stub(),
				custom: {
					transactionsHistory
				}
			}
		},
		custom: {
			transactionsHistory
		}
	};

	describe('paymentTransaction + paymentInstrument', function () {
		it('paymentTransaction.setTransactionID called with data.transactionid', function () {
			processor.saveReferenceTransactionResult(data, order, paymentInstrument);
			assert.calledWith(setTransactionID, data.transactionid);
		});
		it('paymentTransaction.custom.transactionsHistory = data.transactionid', function () {
		 	processor.saveReferenceTransactionResult(data, order, paymentInstrument);
		 	expect(custom.transactionsHistory).to.be.equal(data.transactionid);
		});
		it('paymentInstrument.custom.paypalPaymentStatus = data.paymentstatus', function () {
			processor.saveReferenceTransactionResult(data, order, paymentInstrument);
			expect(custom.paypalPaymentStatus).to.be.equal(data.paymentstatus);
	   });
	   it('paymentInstrument.custom.paypalCorrelationId = data.correlationid', function () {
		processor.saveReferenceTransactionResult(data, order, paymentInstrument);
		expect(custom.paypalCorrelationId).to.be.equal(data.correlationid);
		assert.calledWith(setPaymentStatus, dw.order.PAYMENT_STATUS_PAID);
   		});
	});

	describe('setPaymentStatus', function () {
		it('data.paymentinfo_0_paymentstatus == false', function () {
			data.paymentinfo_0_paymentstatus = 'Done';
			processor.saveReferenceTransactionResult(data, order, paymentInstrument);
			expect(custom.paypalPaymentMethod).to.be.equal(paypalPaymentMethod);
		});
		it('orderPaymentInstruments.length == false', function () {
			data.paymentinfo_0_paymentstatus = 'Completed';
			length = 2;
			processor.saveReferenceTransactionResult(data, order, paymentInstrument);
			expect(custom.paypalPaymentMethod).to.be.equal(paypalPaymentMethod);
		});
		it('orderPaymentInstruments[0].getPaymentMethod() == false', function () {
			data.paymentinfo_0_paymentstatus = 'Completed';
			length = 1;
			getPM = 'Braintree';
			processor.saveReferenceTransactionResult(data, order, paymentInstrument);
			expect(custom.paypalPaymentMethod).to.be.equal(paypalPaymentMethod);
		});
		it('order.custom.paypalPaymentMethod = express', function () {
			processor.saveReferenceTransactionResult(data, order, paymentInstrument);
			expect(custom.paypalPaymentMethod).to.be.equal(paypalPaymentMethod);
		});
	});
});

describe('saveReferenceTransactionResult', function () {
	let transactionsHistory, paypalAck, paypalToken, paypalPaymentStatus, paypalCorrelationId;
	let paypalPaymentMethod = 'express';
	let paypalEmail;
	let setTransactionID = stub();
	let length = 1;
	let custom = {};
	let getPM = 'PayPal';
	let currencyCode = 'US';
	let paymentInstrument = {
		getPaymentTransaction: () => {
			return {
				setTransactionID,
				setAmount: stub(),
				custom: {
					transactionsHistory
				}
			}
		},
		custom
	};

	let paymentinfo_0_transactionid, paymentinfo_0_amt, paymentinfo_0_currencycode, paymentinfo_0_ack, token, paymentinfo_0_paymentstatus, correlationid, billingagreementid;
	let data = {
		paymentinfo_0_transactionid,
		paymentinfo_0_amt,
		paymentinfo_0_currencycode,
		paymentinfo_0_transactionid,
		paymentinfo_0_ack,
		token,
		paymentinfo_0_paymentstatus,
		correlationid,
		billingagreementid
	};
	let authenticated = false;
	let isAuthenticated = () => {
		return authenticated;
	};
	let order = {
		getPaymentInstruments: () => {
			return ([{
				getPaymentMethod: () => getPM,
				length
			}])
		},
		getCurrencyCode: () => currencyCode,
		setPaymentStatus: stub(),
		custom,
		isAuthenticated,
		customer : {
			isAuthenticated
		}
	}

	let checked = true;
	before(() => {
        session.forms = {
            billing: {
                paypal: {
                    saveBillingAgreement: {
                        checked
                    }
                }
            }
        };
    });
    after(() => {
        session.forms = {};
    });

	describe('paymentTransaction + paymentInstrument', function () {
		it('paymentTransaction.setTransactionID called with data.transactionid', function () {
			processor.saveTransactionResult(data, order, paymentInstrument);
			assert.calledWith(setTransactionID, data.transactionid);
		});
		it('paymentTransaction.custom.transactionsHistory = data.paymentinfo_0_transactionid', function () {
		 	processor.saveTransactionResult(data, order, paymentInstrument);
		 	expect(custom.transactionsHistory).to.be.equal(data.paymentinfo_0_transactionid);
		});
		it('paymentInstrument.custom.paypalAck = data.paymentinfo_0_ack', function () {
			processor.saveTransactionResult(data, order, paymentInstrument);
			expect(custom.paypalAck).to.be.equal(data.paymentinfo_0_ack);
		});
	   it('paymentInstrument.custom.paypalToken = data.token', function () {
			processor.saveTransactionResult(data, order, paymentInstrument);
			expect(custom.paypalToken).to.be.equal(data.token);
		});
		it('paymentInstrument.custom.paypalPaymentStatus', function () {
			processor.saveTransactionResult(data, order, paymentInstrument);
			expect(custom.paypalPaymentStatus).to.be.equal(data.paymentinfo_0_paymentstatus);
		});
		it('paymentInstrument.custom.paypalCorrelationId = data.correlationid', function () {
			processor.saveTransactionResult(data, order, paymentInstrument);
			expect(custom.paypalCorrelationId).to.be.equal(data.correlationid);
		});
		
	});

	describe('order.customer.isAuthenticated() == true', function () {
		authenticated = true;
		data.billingagreementid = '0000';
		paymentInstrument.custom.paypalEmail = 'test@t.com';
		let obj = {
			id: data.billingagreementid,
			email: paymentInstrument.custom.paypalEmail,
			currencyCode
		}		
		let id = '111';
		let remove = stub();
		let add = stub();
		let getDefaultShippingAddress = stub();
		let customerBillingAgreement = {
			remove,
			add,
			getDefault: () => {
				return {
					id
				}
			},
			getDefaultShippingAddress
		};
		add.withArgs(obj);
		remove.withArgs(customerBillingAgreement.getDefault().id);
		prefs.getCustomerBillingAgreement.returns(customerBillingAgreement);

		describe('data.billingagreementid && needSaveBillingAgreement', function () {
			before(() => {
				processor.saveTransactionResult(data, order, paymentInstrument);
			});

			it('customerBillingAgreement.remove', function () {
				assert.calledWith(customerBillingAgreement.remove, customerBillingAgreement.getDefault().id);
			});
			it('customerBillingAgreement.add', function () {
				assert.calledWith(customerBillingAgreement.add, obj);
			});

		});

		describe('!customerBillingAgreement.getDefaultShippingAddress()', function () {
			before(() => {
				getDefaultShippingAddress.returns(true);
			});
			after(() => {
				getDefaultShippingAddress.returns(false);
			});

			it('saveShippingAddressToAccountFromBasket', function () {
				assert.calledWith(accountHelpers.saveShippingAddressToAccountFromBasket, order);
			});
		});
	});

	describe('setPaymentStatus', function () {
		paypalEmail = null;
		it('paymentInstrument.custom.paypalEmail = null', function () {
			processor.saveTransactionResult(data, order, paymentInstrument);
			expect(custom.paypalEmail).to.be.equal(paypalEmail);
		});
		it('data.paymentinfo_0_paymentstatus == false', function () {
			data.paymentinfo_0_paymentstatus = 'Done';
			processor.saveTransactionResult(data, order, paymentInstrument);
			expect(custom.paypalPaymentMethod).to.be.equal(paypalPaymentMethod);
		});
		it('orderPaymentInstruments.length == false', function () {
			data.paymentinfo_0_paymentstatus = 'Completed';
			length = 2;
			processor.saveTransactionResult(data, order, paymentInstrument);
			expect(custom.paypalPaymentMethod).to.be.equal(paypalPaymentMethod);
		});
		it('orderPaymentInstruments[0].getPaymentMethod() == false', function () {
			data.paymentinfo_0_paymentstatus = 'Completed';
			length = 1;
			getPM = 'Braintree';
			processor.saveTransactionResult(data, order, paymentInstrument);
			expect(custom.paypalPaymentMethod).to.be.equal(paypalPaymentMethod);
		});
		it('order.custom.paypalPaymentMethod = express', function () {
			processor.saveTransactionResult(data, order, paymentInstrument);
			expect(custom.paypalPaymentMethod).to.be.equal(paypalPaymentMethod);
		});
	});
});
