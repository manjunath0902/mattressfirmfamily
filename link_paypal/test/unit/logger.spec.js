const { expect } = require('chai');
const { loggerPath } = require('./path');
const { stub, assert } = require('sinon');

require('dw-api-mock/demandware-globals');

const paypalLogger = require('proxyquire').noCallThru()(loggerPath, {
    'dw/system': dw.system
});

describe('getLogger function', () => {
    let categoryName, fileName;
    before(() => {
        stub(dw.system.Logger, 'getLogger');
    });
    after(() => {
        dw.system.Logger.getLogger.restore();
    });
    describe('if categoryName exists', () => {
        before(() => {
            categoryName = 'Category';
            paypalLogger.getLogger(categoryName);
        })

        it('should return getLogger with args', () => {
            assert.calledWith(dw.system.Logger.getLogger, 'PayPal', 'Category');
        })
    })
    describe('if categoryName does not exist', () => {
        before(() => {
            categoryName = null;
            paypalLogger.getLogger(categoryName);
        })
        it('should return getLogger with args', () => {
            assert.calledWith(dw.system.Logger.getLogger, 'PayPal', 'PayPal_General');
        })
    })
    describe('if categoryName === Notification', () => {  
        before(() => {
            categoryName = 'Notification';
            paypalLogger.getLogger(categoryName);
        })

        it('should return getLogger with args', () => {
            assert.calledWith(dw.system.Logger.getLogger, 'PayPal_Notifications', 'Notification');
        })
    })
})