CHANGELOG
=========
## 18.3.1
* SFRA support up to 4.2.1 and SG up to 104.3.1
## 19.1.1
* Update SFRA support up to 4.4.1
* Remove `Retrieve Billing Address From PayPal` preference, now it required for checkout from cart
* fix [PayPal breaks support for custom price adjustments](https://github.com/SalesforceCommerceCloud/link_paypal/issues/15)
* fix [Cart - Cannot remove products](https://github.com/SalesforceCommerceCloud/link_paypal/issues/5)
* fix [Removing only PayPal payment instrument allow to call multiple authorizations](https://github.com/SalesforceCommerceCloud/link_paypal/issues/7)
* fix [Default API error messages not showing on checkout process](https://github.com/SalesforceCommerceCloud/link_paypal/issues/11)
* fix [shippingSummary.isml customization is no longer needed](https://github.com/SalesforceCommerceCloud/link_paypal/issues/4)