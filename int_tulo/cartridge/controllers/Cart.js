'use strict';

/**
 * Controller that adds and removes products and coupons in the cart.
 * Also provides functions for the continue shopping button and minicart.
 *
 * @module controllers/Cart
 */

/* API Includes */
var ArrayList = require('dw/util/ArrayList');
var ISML = require('dw/template/ISML');
var ProductListMgr = require('dw/customer/ProductListMgr');
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');
var Logger = require('dw/system/Logger');
var ProductUtils = require('app_storefront_core/cartridge/scripts/product/ProductUtils.js');

/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var pipeletHelper = require('bc_sleepysc/cartridge/scripts/sleepys/util/SleepysPipeletsHelper').SleepysPipeletHelper;
var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;
var StorePicker = require('~/cartridge/controllers/StorePicker');

var FrameUtils = require('~/cartridge/scripts/util/FrameUtils');
/**
 * Redirects the user to the last visited catalog URL if known, otherwise redirects to
 * a hostname-only URL if an alias is set, or to the Home-Show controller function in the default
 * format using the HTTP protocol.
 */
function continueShopping() {

    var location = require('~/cartridge/scripts/util/Browsing').lastCatalogURL();

    if (location) {
        response.redirect(location);
    } else {
        response.redirect(URLUtils.httpHome());
    }
}

/**
 * Invalidates the login and shipment forms. Renders the checkout/cart/cart template.
 */
function show() {
    var cartForm = app.getForm('cart');
    app.getForm('login').invalidate();

    cartForm.get('shipments').invalidate();
    var cart = app.getModel('Cart').get();
    //var cartAsset = app.getModel('Content').get('cart');

   // pageMeta = require('~/cartridge/scripts/meta');
  //  pageMeta.update(cartAsset);
    
    if (cart) {
        Transaction.wrap(function () {
            pipeletHelper.clearAllShipmentSchedules(cart);
            if (session.custom.deliveryOptionChoice == 'instorepickup') {
            	mattressPipeletHelper.splitShipmentsByType(cart);
            }
            cart.calculate();
        });
    }
    app.getView('Cart', {
        cart: app.getModel('Cart').get(),
        RegistrationStatus: false
    }).render('checkout/cart/cart');

}

/**
 * Handles the form actions for the cart.
 * - __addCoupon(formgroup)__ - adds a coupon to the basket in a transaction. Returns a JSON object with parameters for the template.
 * - __calculateTotal__ - returns the cart object.
 * - __checkoutCart__ - validates the cart for checkout. If valid, redirect to the COCustomer-Start controller function to start the checkout. If invalid returns the cart and the results of the validation.
 * - __continueShopping__ - calls the {@link module:controllers/Cart~continueShopping|continueShopping} function and returns null.
 * - __deleteCoupon(formgroup)__ - removes a coupon from the basket in a transaction. Returns a JSON object with parameters for the template
 * - __deleteGiftCertificate(formgroup)__ - removes a gift certificate from the basket in a transaction. Returns a JSON object with parameters for the template.
 * - __deleteProduct(formgroup)__ -  removes a product from the basket in a transaction. Returns a JSON object with parameters for the template.
 * - __editLineItem(formgroup)__ - gets a ProductModel that wraps the pid (product ID) in the httpParameterMap and updates the options to select for the product. Updates the product in a transaction.
 * Renders the checkout/cart/refreshcart template. Returns null.
 * - __login__ - calls the Login controller and returns a JSON object with parameters for the template.
 * - __logout__ - logs the customer out and returns a JSON object with parameters for the template.
 * - __register__ - calls the Account controller StartRegister function. Updates the cart calculation in a transaction and returns null.
 * - __unregistered__ - calls the COShipping controller Start function and returns null.
 * - __updateCart__ - In a transaction, removes zero quantity line items, removes line items for in-store pickup, and copies data to system objects based on the form bindings.
 * Returns a JSON object with parameters for the template.
 * - __error__ - returns null.
 *
 * __Note:__ The CartView sets the ContinueURL to this function, so that any time URLUtils.continueURL() is used in the cart.isml, this function is called.
 * Several actions have <b>formgroup</b> as an input parameter. The formgroup is supplied by the {@link module:models/FormModel~FormModel/handleAction|FormModel handleAction} function in the FormModel module.
 * The formgroup is session.forms.cart object of the triggered action in the form definition. Any object returned by the function for an action is passed in the parameters to the cart template
 * and is accessible using the $pdict.property syntax. For example, if a function returns {CouponStatus: status} is accessible via ${pdict.CouponStatus}
 * Most member functions return a JSON object that contains {cart: cart}. The cart property is used by the CartView to determine the value of
 * $pdict.Basket in the cart.isml template.
 *
 * For any member function that returns an object, the page metadata is updated, the function gets a ContentModel that wraps the cart content asset,
 * and the checkout/cart/cart template is rendered.
 *
 */
function submitForm() {
    // There is no existing state, so resolve the basket again.
    var cart, formResult, cartForm, cartAsset, pageMeta;
    cartForm = app.getForm('cart');
    cart = app.getModel('Cart').goc();    

    formResult = cartForm.handleAction({
        //Add a coupon if a coupon was entered correctly and is active.
        'addCoupon': function (formgroup) {
            var status, result;           
            if (formgroup.couponCode.htmlValue) {
            	
            	formgroup.couponCode.htmlValue = formgroup.couponCode.htmlValue.toUpperCase();
                status = Transaction.wrap(function () {
                    return cart.addCoupon(formgroup.couponCode.htmlValue);
                });

                if (status) {
                	for each(var couponLineItem in cart.object.couponLineItems) {
                    	if (couponLineItem.couponCode.toUpperCase() == formgroup.couponCode.htmlValue.toUpperCase()) {
                    		if (!couponLineItem.applied) {
                    			cart.removeCouponLineItem(couponLineItem);
                    		}
                    	}                    	
                    }
                	
                	Transaction.wrap(function () {
                		cart.removePersonaliCartRescue();
                		cart.removePersonaliFromLineItems();
                		cart.calculate();
                	});               	
                	var currentCouponBonusLineItems = cart.getBonusLineItemsByCouponCode(formgroup.couponCode.htmlValue.toUpperCase())
                    var currProdQualifiedBonusProducts=cart.getJsonByLineItems(currentCouponBonusLineItems);
                    result = {
                        cart: cart,
                        CouponStatus: status.CouponStatus,
                        qualifiedBonusProducts : currProdQualifiedBonusProducts,
                        action : 'addcoupon'
                    };
                } else {
                    result = {
                        cart: cart,
                        CouponError: 'NO_ACTIVE_PROMOTION'
                    };
                }
            } else {
                result = {
                    cart: cart,
                    CouponError: 'COUPON_CODE_MISSING'
                };
            }
            var showMattressRemovalOption = pipeletHelper.showMatressRemovalOptionCheck(cart);
            result.RegistrationStatus = false;
            result.ShowMattressRemovalOption = showMattressRemovalOption;

            app.getView('Cart', result).render('checkout/cart/cart');
            return;

        },
        'calculateTotal': function () {
            // Nothing to do here as re-calculation happens during view anyways
            return {
                cart: cart
            };
        },
        'checkoutCart': function () {
        	var mFinderEmptyCartLogger = Logger.getLogger('MattressFinder','Cart');
            mFinderEmptyCartLogger.info("Cart Checkout Cart Starts.");
            var validationResult, result;

            validationResult = cart.validateForCheckout();
            mFinderEmptyCartLogger.info("Cart Validation Result : " + validationResult.EnableCheckout + " .");
            mFinderEmptyCartLogger.info("Basket Status : " + validationResult.BasketStatus.message + " .");
            if (validationResult.EnableCheckout) {
                //app.getController('COCustomer').Start();
            	mFinderEmptyCartLogger.info("Jump to Checkout-Start.");
                response.redirect(URLUtils.https('Checkout-Start'));

            } else {
            	mFinderEmptyCartLogger.info("Invalid Jump from cart.");
                result = {
                    cart: cart,
                    BasketStatus: validationResult.BasketStatus,
                    EnableCheckout: validationResult.EnableCheckout
                };
            }
            return result;
        },
        'continueShopping': function () {
            continueShopping();
            return null;
        },
        'deleteCoupon': function (formgroup) {
        	var CouponBonusLineItems = cart.getBonusLineItemsByCouponCode(formgroup.getTriggeredAction().object.couponCode)
        	Transaction.wrap(function () {
                cart.removeCouponLineItem(formgroup.getTriggeredAction().object);
                cart.calculate();
            });            
            var currProdQualifiedBonusProducts=cart.getJsonByLineItems(CouponBonusLineItems);

            return {
                cart: cart,
                qualifiedBonusProducts : currProdQualifiedBonusProducts,
                action : 'deleteCoupon'
            };
        },
        'deleteGiftCertificate': function (formgroup) {
            Transaction.wrap(function () {
                cart.removeGiftCertificateLineItem(formgroup.getTriggeredAction().object);
            });

            return {
                cart: cart
            };
        },
        'deleteProduct': function (formgroup) {
        	var BonusProductLineItems = formgroup.getTriggeredAction().object.relatedBonusProductLineItems;
            Transaction.wrap(function () {
            	var triggeredProductLineItem = formgroup.getTriggeredAction().object;
            	// removeframe if there is any for this product
            	var frameLI = FrameUtils.GetFrameToRemove(cart, triggeredProductLineItem);
            	if(frameLI != false)
            	{
            		cart.removeProductLineItem(frameLI);
            	}
            	// clear frames if they are with main lineitem
            	
            	if(triggeredProductLineItem.custom.isQuoteItem) {
            		//removing triggered line item
            		cart.removeProductLineItem(triggeredProductLineItem);           		
            		
            		//removing cart updates related to quotation
            		cart.object.custom.mfiQuotationId = null;
					cart.object.custom.SalesPersonId = null;
					cart.object.custom.CustomerId = null;
					
					//adjusting existing product line items
					var allLineItems = cart.object.allProductLineItems;
					for (var i=0; i<allLineItems.length; i++) {
						var lineItem = allLineItems[i];
						if( lineItem instanceof dw.order.ProductLineItem && lineItem.custom.isQuoteItem) {
							var createdPriceAdjustment = lineItem.getPriceAdjustmentByPromotionID(lineItem.productID + "-quote");
							lineItem.removePriceAdjustment(createdPriceAdjustment);
							lineItem.custom.isQuoteItem = null;
							lineItem.custom.quotePrice = null;
							lineItem.custom.quotediscount = null;
							lineItem.custom.quoteQuantity = null;
						}
					}
            	}
            	else {
					cart.removePersonaliCartRescue();
            		cart.removeProductLineItem(formgroup.getTriggeredAction().object);
            	}
            	// if it is a frame lin item, clear its link from its main lineitems
        		if(triggeredProductLineItem.product.masterProduct.ID == dw.system.Site.getCurrent().getCustomPreferenceValue('tuloFrameProductID'))
        		{ // this line item  is a frame
        			FrameUtils.ClearFrameLinks(cart,triggeredProductLineItem.product.ID );
        		}
        		cart.calculate();
            });
			
			var currProdQualifiedBonusProducts=cart.getJsonByLineItems(BonusProductLineItems);
            return {
                cart: cart,
                qualifiedBonusProducts : currProdQualifiedBonusProducts,
                action : 'deleteProduct'
            };
        },
        'editLineItem': function (formgroup) {
            var product, productOptionModel;
            product = app.getModel('Product').get(request.httpParameterMap.pid.stringValue).object;
            productOptionModel = product.updateOptionSelection(request.httpParameterMap);

            Transaction.wrap(function () {
            	cart.removePersonaliCartRescue();
                cart.updateLineItem(formgroup.getTriggeredAction().object, product, request.httpParameterMap.Quantity.doubleValue, productOptionModel);
                cart.calculate();
            });

            ISML.renderTemplate('checkout/cart/refreshcart');
            return null;
        },
        'login': function () {
            // TODO should not be processed here at all
            var success, result;
            success = app.getController('Login').Process();

            if (success) {
                response.redirect(URLUtils.https('Checkout-Start'));
            } else if (!success) {
                result = {
                    cart: cart
                };
            }
            return result;
        },
        'logout': function () {
            var CustomerMgr = require('dw/customer/CustomerMgr');
            CustomerMgr.logoutCustomer();
            return {
                cart: cart
            };
        },
        'register': function () {
            app.getController('Account').StartRegister();
            Transaction.wrap(function () {
                cart.calculate();
            });

            return null;
        },
        'unregistered': function () {
        	response.redirect(URLUtils.https('Checkout-Start'));
            return null;
        },
        'updateCart': function () {

            Transaction.wrap(function () {
                var shipmentItem, item;

                // remove zero quantity line items
                for (var i = 0; i < session.forms.cart.shipments.childCount; i++) {
                    shipmentItem = session.forms.cart.shipments[i];

                    for (var j = 0; j < shipmentItem.items.childCount; j++) {
                        item = shipmentItem.items[j];

                        if (item.quantity.value === 0) {
                            cart.removeProductLineItem(item.object);
                        }
                    }
                }

                session.forms.cart.shipments.accept();
                cart.removePersonaliCartRescue();
                cart.updatePersonaliProducts();
                cart.checkInStoreProducts();
            });

            return {
                cart: cart
            };
        },
        'updateQuantity': function () {

            Transaction.wrap(function () {
                var shipmentItem, item, currentItem;
				var invalidateQuotationItems = false;
                // remove zero quantity line items
                for (var i = 0; i < session.forms.cart.shipments.childCount; i++) {
                    shipmentItem = session.forms.cart.shipments[i];
                    
                    for (var j = 0; j < shipmentItem.items.childCount; j++) {
                        item = shipmentItem.items[j];
                        
                        if (item.quantity.value === 0) {
                            cart.removeProductLineItem(item.object);
                        }
                        
                        //reviewing if the price got changed for any quotation line item if yes, remove the quotation discount
                        if(item.object != null && item.object instanceof dw.order.ProductLineItem && item.object.custom.isQuoteItem) {
                        	currentItem = cart.getProductLineItems(item.object.productID)[0];
                        	if(currentItem.custom.quoteQuantity > item.quantity.value){
								invalidateQuotationItems = true; //seems terms are not matching for Quotation Items so lets invalidate it
                        	 }
						}
                    }
                }
                
                
                //we securing multiple iteration and trying to acheive our goal in one shot
                if(invalidateQuotationItems){
                	invalidateCartForQuotation(cart); //we need this function because we wan't to invalidate every quotation item
                }

                session.forms.cart.shipments.accept();
                cart.removePersonaliCartRescue();
//                cart.updatePersonaliProducts();
                cart.checkInStoreProducts();
                cart.calculate(); 
            });

            return {
                cart: cart
            };
        },
        'error': function () {
            return null;
        }
    });

    if (formResult) {
    	//Cart page title not working here so thats why moved to Cart-Show method
        /*cartAsset = app.getModel('Content').get('cart');

        pageMeta = require('~/cartridge/scripts/meta');
        pageMeta.update(cartAsset);*/

       // response.redirect(URLUtils.https('Cart-Show'));
    	 app.getView('Cart', formResult).render('checkout/cart/cart');
    }
}

/*
* Function invalidates the curret quoted items in the cart [if one is void all quoted items will be invalidated]
*/
function invalidateCartForQuotation(cart){
	
	//removing cart updates related to quotation
    cart.object.custom.mfiQuotationId = null;
    cart.object.custom.SalesPersonId = null;
    cart.object.custom.CustomerId = null;
    
	for (var i = 0; i < session.forms.cart.shipments.childCount; i++) {
                    shipmentItem = session.forms.cart.shipments[i];
                    for (var j = 0; j < shipmentItem.items.childCount; j++) {
                        item = shipmentItem.items[j];
                        //reviewing if the price got changed for any quotation line item if yes, remove the quotation discount
                        if(item.object != null && item.object instanceof dw.order.ProductLineItem && item.object.custom.isQuoteItem) {
                        	var currentItem = cart.getProductLineItems(item.object.productID)[0];
							var createdPriceAdjustment = currentItem.getPriceAdjustmentByPromotionID(currentItem.productID + "-quote");
							currentItem.removePriceAdjustment(createdPriceAdjustment);
							currentItem.custom.isQuoteItem = null;
							currentItem.custom.quotePrice = null;
							currentItem.custom.quotediscount = null;
							currentItem.custom.quoteQuantity = null;
						}
                    }
                }
	
}


/**
 * Adds or replaces a product in the cart, gift registry, or wishlist.
 * If the function is being called as a gift registry update, calls the
 * {@link module:controllers/GiftRegistry~replaceProductListItem|GiftRegistry controller ReplaceProductListItem function}.
 * The httpParameterMap source and cartAction parameters indicate how the function is called.
 * If the function is being called as a wishlist update, calls the
 * {@link module:controllers/Wishlist~replaceProductListItem|Wishlist controller ReplaceProductListItem function}.
 * If the product line item for the product to add has a:
 * - __uuid__ - gets a ProductModel that wraps the product and determines the product quantity and options.
 * In a transaction, calls the {@link module:models/CartModel~CartModel/updateLineItem|CartModel updateLineItem} function to replace the current product in the line
 * item with the new product.
 * - __plid__ - gets the product list and adds a product list item.
 * Otherwise, adds the product and checks if a new discount line item is triggered.
 * Renders the checkout/cart/refreshcart template if the httpParameterMap format parameter is set to ajax,
 * otherwise renders the checkout/cart/cart template.
 */
function addProduct() {
    var cart = app.getModel('Cart').goc();
    var params = request.httpParameterMap;
    var format = empty(params.format.stringValue) ? '' : params.format.stringValue.toLowerCase();
    var Product = app.getModel('Product');
    var productOptionModel;
    var product;
    var template = 'checkout/cart/cart';
    var newBonusDiscountLineItem;
    var lastItemAdded = false;
    var lastItemAddedQty = false;
    var bonusProductStartIndex=0;
    
    if(cart.object.bonusLineItems.length>0){
    	bonusProductStartIndex = cart.object.bonusLineItems.length;
    }

    if (params.source && params.source.stringValue === 'giftregistry' && params.cartAction && params.cartAction.stringValue === 'update') {
        app.getController('GiftRegistry').ReplaceProductListItem();
        return;
    }

    if (params.source && params.source.stringValue === 'wishlist' && params.cartAction && params.cartAction.stringValue === 'update') {
        app.getController('Wishlist').ReplaceProductListItem();
        return;
    }

    // Updates a product line item.
    if (params.uuid.stringValue) {
        var lineItem = cart.getProductLineItemByUUID(params.uuid.stringValue);
        if (lineItem) {
            var productModel = Product.get(request.httpParameterMap.pid.stringValue);
            product = productModel.object;
            var quantity = parseInt(params.Quantity.value);
            productOptionModel = productModel.updateOptionSelection(request.httpParameterMap);

            Transaction.wrap(function () {
                cart.updateLineItem(lineItem, product, quantity, productOptionModel);
            });
            
            if (params.netotiate_offer_id.stringValue && !empty(params.netotiate_offer_id.stringValue)) {
        		var personaliResponse = cart.getPersonaliProductResponse(params.netotiate_offer_id.stringValue);
        		if (personaliResponse !== false) {
        			var adjustedPrice : Number = personaliResponse["adjusted_price"] / 100,
	    				originalPrice : Number = personaliResponse["original_price"] / 100,
	    				offerId : String = params.netotiate_offer_id.stringValue;
	    			if (adjustedPrice && originalPrice && adjustedPrice < originalPrice) {
	    				Transaction.wrap(function () {
	    					cart.removePersonaliCartRescue();
	        				personaliPriceAdjustment(lastItemAdded, offerId, params.Quantity.doubleValue, adjustedPrice, originalPrice);
	    	            });
	    			}
        		}
        		
        	} else if (lineItem.custom['personaliOfferID'] || lineItem.custom['personaliDiscount']) {
        		Transaction.wrap(function () {
        			delete lineItem.custom['personaliOfferID'];
            		delete lineItem.custom['personaliDiscount'];
        		});
        	}

            // If this product is to be picked up in a store -- update store id
            if (!empty(params.storeId.stringValue)) {
                // Determine if products in cart are eligible for in store pickup
                var allProductsAvailableForInStorePickup = product.custom.availableForInStorePickup;
                if (allProductsAvailableForInStorePickup) {
                    var plisIterator = cart.getAllProductLineItems().iterator();
                    while (plisIterator.hasNext()) {
                        var pli = plisIterator.next();
                        if (!(pli.product.custom.availableForInStorePickup)) {
                            allProductsAvailableForInStorePickup = false;
                        }
                    }
                    // all products are eligible
                    if (allProductsAvailableForInStorePickup) {
                        Transaction.wrap(function () {
                            var plisIterator = cart.getAllProductLineItems().iterator();
                            while (plisIterator.hasNext()) {
                                var pli = plisIterator.next();
                                pli.custom.storePickupStoreID = params.storeId.stringValue;
                            }
                         });
                    }
                }
            }
            if (format === 'ajax') {
               // template = 'checkout/cart/refreshcart';
            	//modified for MAT-505, edit the product and add option
            	template = 'checkout/cart/cart';
            }
        } else {
            app.getView('Cart', {Basket: cart}).render('checkout/cart/cart');
        }
    } else if (params.plid.stringValue) {
        // Adds a product to a product list.
        var productList = ProductListMgr.getProductList(params.plid.stringValue);
        cart.addProductListItem(productList && productList.getItem(params.itemid.stringValue), params.Quantity.doubleValue, params.cgid.value);
    } else {
        // Adds a product.
        product = Product.get(params.pid.stringValue);
        var previousBonusDiscountLineItems = cart.getBonusDiscountLineItems();

        if (product.object.isProductSet()) {
            var childPids = params.childPids.stringValue.split(',');
            var childQtys = params.childQtys.stringValue.split(',');
            var counter = 0;

            for (var i = 0; i < childPids.length; i++) {
                var childProduct = Product.get(childPids[i]);

                if (childProduct.object && !childProduct.isProductSet()) {
                    var childProductOptionModel = childProduct.updateOptionSelection(request.httpParameterMap);
                    cart.addProductItem(childProduct.object, parseInt(childQtys[counter], 10), params.cgid.value, childProductOptionModel);
                }
                counter++;
            }
        } else {
            productOptionModel = product.updateOptionSelection(request.httpParameterMap);
            //MAT-356            
            var lineItemsOfPid = cart.getProductLineItemByID(params.pid.stringValue);
            var productFoundInCart = false;
            var ProductUtils = require('app_storefront_core/cartridge/scripts/product/ProductUtils.js'); 
            if (lineItemsOfPid.length > 0 && (lineItemsOfPid.bonusProductLineItem == false || ProductUtils.isTuloMattress(params.pid.stringValue) || ProductUtils.isLivMattress(params.pid.stringValue) )) {
            	var productModel = product;
            	var lineItemQuantity = params.Quantity.value;
            	var productToAdd = productModel.object;	        	
	        	for each(var lineItem in lineItemsOfPid) {
        			var productOptions = productOptionModel.getOptions();
	        		if(productOptions.length >= 0){
	        			var lineItemOptions = lineItem.getOptionProductLineItems();
	        			// productOptions are of that product which is going into cart which is productToAdd
	        			//lineItemOptions, extracting all lineItem iterator's all option products into lineItemOptions
	        			productFoundInCart = compareProductOptionProducts_With_ProductLineItemOptionProducts (productOptions, lineItemOptions, productOptionModel)
		        		if (productFoundInCart) {
		        			lineItemQuantity = parseInt(params.Quantity.value) + lineItem.quantity;
		        			availabilityQty = lineItemQuantity;
				            Transaction.wrap(function () {
				            cart.updateLineItem(lineItem, productToAdd, lineItemQuantity, productOptionModel);
				            });
				            break;
		        		}		        			
	        		}
	        	}
	        }
	         if (!productFoundInCart){	        	
	            cart.addProductItem(product.object, params.Quantity.doubleValue, params.cgid.value, productOptionModel);
	        }
	        if(params.frameID && !empty(params.frameID) && !empty(params.frameID.stringValue))
	        {
	        	// add Frame Product 	        
	        	addFrame(params.pid.stringValue, params.frameID.stringValue, params.Quantity.doubleValue);
	        }
	         if(params.pillowID && !empty(params.pillowID) && !empty(params.pillowID.stringValue))
	        {
	        	// add pillow Product 	        
	        	addPillow(params.pillowID.stringValue, params.pillowQuantity.doubleValue);
	        }		
            if (params.source && params.source.stringValue !== 'recommendation') {
                var lastItemAddedCollection = cart.getProductLineItems(product.object.ID);
                lastItemAdded = lastItemAddedCollection[lastItemAddedCollection.size() -1];
                lastItemAddedQty = params.Quantity.value;
                if (params.netotiate_offer_id.stringValue && !empty(params.netotiate_offer_id.stringValue)) {
            		var personaliResponse = cart.getPersonaliProductResponse(params.netotiate_offer_id.stringValue);
            		if (personaliResponse !== false) {
            			var adjustedPrice : Number = personaliResponse["adjusted_price"] / 100,
            				originalPrice : Number = personaliResponse["original_price"] / 100,
            				offerId : String = params.netotiate_offer_id.stringValue;
            			if (adjustedPrice && originalPrice && adjustedPrice < originalPrice) {
            				Transaction.wrap(function () {
    	        				personaliPriceAdjustment(lastItemAdded, offerId, params.Quantity.doubleValue, adjustedPrice, originalPrice);
    	    	            });
            			}
            		}
            		
            	}
            }
            // If this product is to be picked up in a store -- update store id
            if (!empty(params.storeId.stringValue)) {
                // Determine if products in cart are eligible for in store pickup
                var allProductsAvailableForInStorePickup = product.object.custom.availableForInStorePickup;
                var plisIterator = cart.getAllProductLineItems().iterator();
                while (plisIterator.hasNext()) {
                    var pli = plisIterator.next();
                    if (!(pli.product.custom.availableForInStorePickup)) {
                        allProductsAvailableForInStorePickup = false;
                    }
                }
                // all products are eligible
                if (allProductsAvailableForInStorePickup) {
                    Transaction.wrap(function () {
                        var plisIterator = cart.getAllProductLineItems().iterator();
                        while (plisIterator.hasNext()) {
                            var pli = plisIterator.next();
                            pli.custom.storePickupStoreID = params.storeId.stringValue;
                        }
                     });
                }
            }
            if (!empty(session.custom.customerZone)) {
//            	lastItemAdded.custom.zoneCodeAddedWith = session.custom.customerZone;
            }
        }

        // When adding a new product to the cart, check to see if it has triggered a new bonus discount line item.
        newBonusDiscountLineItem = cart.getNewBonusDiscountLineItem(previousBonusDiscountLineItems);
    }
    
    Transaction.wrap(function () {
        cart.calculate();
    });
    
    var newlyAddedBonusItems = cart.object.bonusLineItems.length > 0 ? cart.object.bonusLineItems.toArray(bonusProductStartIndex, cart.object.bonusLineItems.length) : [];
    var currProdQualifiedBonusProducts = cart.getJsonByLineItems(newlyAddedBonusItems);
    if (format === 'ajax') {
        app.getView('Cart', {
            cart: cart,
            BonusDiscountLineItem: newBonusDiscountLineItem,
            LastItem: lastItemAdded,
            LastItemAddedQty: lastItemAddedQty,
            qualifiedBonusProducts : currProdQualifiedBonusProducts
        }).render(template);
    } else {
        response.redirect(URLUtils.url('Cart-Show'));
    }
}
/**
 * Adds multiple products to cart via a JSON string supplied in the parameters.
 * parameter name should be cartPayload and JSON format is as follows:
 * [{"pid":"mfiV000088629","qty":1},{"pid":"mfiV000088630","qty":4},{"pid":"mfiV000088631","qty":5}]
 */
function addMultipleProducts() {
    var Product = app.getModel('Product');
    var cart = app.getModel('Cart').goc();
    var cartPayload = request.httpParameterMap.cartPayload;
    try {
        var jsonPayload = JSON.parse(cartPayload);
        for (var i = 0; i < jsonPayload.length; i++){
            var jsonProduct = jsonPayload[i];
            //var product = Product.get(params.pid.stringValue);
            try {
                var product = Product.get(jsonProduct.pid);
                var productOptionModel = product.updateOptionSelection(request.httpParameterMap);
                cart.addProductItem(product.object, jsonProduct.qty, (!empty(product.object.primaryCategory) ? product.object.primaryCategory.ID : null), productOptionModel);
            } catch (e) {
                Logger.error("Error processing Cart-AddMultipleProducts: Invalid product ID: " + jsonProduct.pid);
            }
        }
    } catch (e) {
        Logger.error("Error processing Cart-AddMultipleProducts: " + e.message);
        show();
        return;
    }
    show();

}
/**
 * Displays the current items in the cart in the minicart panel.
 */
function miniCart() {

    var cart = app.getModel('Cart').get();
    app.getView({
        Basket: cart ? cart.object : null
    }).render('checkout/cart/cart');

}

/**
 * Adds the product with the given ID to the wish list.
 *
 * Gets a ProductModel that wraps the product in the httpParameterMap. Uses
 * {@link module:models/ProductModel~ProductModel/updateOptionSelection|ProductModel updateOptionSelection}
 * to get the product options selected for the product.
 * Gets a ProductListModel and adds the product to the product list. Renders the checkout/cart/cart template.
 */
function addToWishlist() {
    var productID, product, productOptionModel, productList, Product;
    Product = app.getModel('Product');

    productID = request.httpParameterMap.pid.stringValue;
    product = Product.get(productID);
    productOptionModel = product.updateOptionSelection(request.httpParameterMap);

    productList = app.getModel('ProductList').get();
    productList.addProduct(product.object, request.httpParameterMap.Quantity.doubleValue, productOptionModel);

    app.getView('Cart', {
        cart: app.getModel('Cart').get(),
        ProductAddedToWishlist: productID
    }).render('checkout/cart/cart');

}

/**
 * Adds a bonus product to the cart.
 *
 * Parses the httpParameterMap and adds the bonus products in it to an array.
 *
 * Gets the bonus discount line item. In a transaction, removes the bonus discount line item. For each bonus product in the array,
 * gets the product based on the product ID and adds the product as a bonus product to the cart.
 *
 * If the product is a bundle, updates the product option selections for each child product, finds the line item,
 * and replaces it with the current child product and selections.
 *
 * If the product and line item can be retrieved, recalculates the cart, commits the transaction, and renders a JSON object indicating success.
 * If the transaction fails, rolls back the transaction and renders a JSON object indicating failure.
 */
function addBonusProductJson() {
    var h, i, j, cart, data, productsJSON, bonusDiscountLineItem, product, lineItem, childPids, childProduct, ScriptResult, foundLineItem, Product;
    cart = app.getModel('Cart').goc();
    Product = app.getModel('Product');

    // parse bonus product JSON
    data = JSON.parse(request.httpParameterMap.getRequestBodyAsString());
    productsJSON = new ArrayList();

    for (h = 0; h < data.bonusproducts.length; h += 1) {
        // add bonus product at index zero (front of the array) each time
        productsJSON.addAt(0, data.bonusproducts[h].product);
    }

    bonusDiscountLineItem = cart.getBonusDiscountLineItemByUUID(request.httpParameterMap.bonusDiscountLineItemUUID.stringValue);

    Transaction.begin();
    cart.removeBonusDiscountLineItemProducts(bonusDiscountLineItem);

    for (i = 0; i < productsJSON.length; i += 1) {

        product = Product.get(productsJSON[i].pid).object;
        var quantity = Number(productsJSON[i].qty);
        lineItem = cart.addBonusProduct(bonusDiscountLineItem, product, new ArrayList(productsJSON[i].options), quantity);

        if (lineItem && product) {
            if (product.isBundle()) {

                childPids = productsJSON[i].childPids.split(',');

                for (j = 0; j < childPids.length; j += 1) {
                    childProduct = Product.get(childPids[j]).object;

                    if (childProduct) {

                        // TODO: CommonJSify cart/UpdateProductOptionSelections.ds and import here

                        var UpdateProductOptionSelections = require('app_storefront_core/cartridge/scripts/cart/UpdateProductOptionSelections');
                        ScriptResult = UpdateProductOptionSelections.update({
                            SelectedOptions:  new ArrayList(productsJSON[i].options),
                            Product: childProduct
                        });

                        foundLineItem = cart.getBundledProductLineItemByPID(lineItem.getBundledProductLineItems(),
                            (childProduct.isVariant() ? childProduct.masterProduct.ID : childProduct.ID));

                        if (foundLineItem) {
                            foundLineItem.replaceProduct(childProduct);
                        }
                    }
                }
            }
        } else {
            Transaction.rollback();

            let r = require('app_storefront_controllers/cartridge/scripts/util/Response');
            r.renderJSON({
                success: false
            });
            return;
        }
    }

    cart.calculate();
    Transaction.commit();

    let r = require('app_storefront_controllers/cartridge/scripts/util/Response');
    r.renderJSON({
        success: true
    });
}

/**
 * Adds a coupon to the cart using JSON.
 *
 * Gets the CartModel. Gets the coupon code from the httpParameterMap couponCode parameter.
 * In a transaction, adds the coupon to the cart and renders a JSON object that includes the coupon code
 * and the status of the transaction.
 *
 */
function addCouponJson() {
    var couponCode, cart, couponStatus;

    couponCode = request.httpParameterMap.couponCode.stringValue;
    cart = app.getModel('Cart').goc(); 
    
	if (couponCode) {
		couponCode = couponCode.toUpperCase();
		Transaction.wrap(function () {
			couponStatus =  cart.addCoupon(couponCode);
		});
		
		if (couponStatus) {			
			for each(var couponLineItem in cart.object.couponLineItems) {
            	if (couponLineItem.couponCode.toUpperCase() == couponCode.toUpperCase()) {
            		if (!couponLineItem.applied) {
            			cart.removeCouponLineItem(couponLineItem);
            		}
            	}                    	
            }
			Transaction.wrap(function () {
				cart.removePersonaliCartRescue();
				cart.removePersonaliFromLineItems();
			});
		}
	} 

    if (request.httpParameterMap.format.stringValue === 'ajax') {
        let r = require('app_storefront_controllers/cartridge/scripts/util/Response');
        var currentCouponBonusLineItems = cart.getBonusLineItemsByCouponCode(couponCode.toUpperCase());
        var currProdQualifiedBonusProducts=cart.getJsonByLineItems(currentCouponBonusLineItems);
        r.renderJSON({
            status: couponStatus.CouponStatus.code,
            message: Resource.msgf('cart.' + couponStatus.CouponStatus.code, 'checkout', null, couponCode),
            success: !couponStatus.CouponStatus.error,
            baskettotal: cart.object.adjustedMerchandizeTotalGrossPrice.value,
            CouponCode: couponCode,
            qualifiedBonusProducts : currProdQualifiedBonusProducts,
            action : 'addcoupon'
        });
    }
}

function changePickupLocation() {
    var cart = app.getModel('Cart').goc();
    var params = request.httpParameterMap;
    var format = empty(params.format.stringValue) ? '' : params.format.stringValue.toLowerCase();
    var pid = empty(params.pid.stringValue) ? '' : params.pid.stringValue;
    var preferredStore = StorePicker.GetPreferredStore();
   // If this product is to be picked up in a store -- update store id
   if (!empty(params.storeId.stringValue)) {
        Transaction.wrap(function () {
            // update the product line item with the store id
            var plisIterator = cart.getAllProductLineItems().iterator();
            while (plisIterator.hasNext()) {
                var pli = plisIterator.next();
                if (session.custom.isCart){
                    // update all products in the cart
                	pli.custom.storePickupStoreID = preferredStore.ID;
                } else {
                    pli.custom.storePickupStoreID = params.storeId.stringValue;
                }
            }
        });
    }
}

function changeDeliveryOption(option) {
    // change from pickup in store to shipped to you option
    var cart = app.getModel('Cart').goc();
    var params = request.httpParameterMap;
    var deliveryoption = (!empty(params.deliveryoption.value)) ? params.deliveryoption.stringValue : option;
    session.custom.deliveryOptionChoice = deliveryoption;
    var storeId = (deliveryoption == 'shipped') ? null : params.storeId.stringValue;
    Transaction.wrap(function () {
        var plisIterator = cart.getAllProductLineItems().iterator();
        while (plisIterator.hasNext()) {
            var pli = plisIterator.next();
            pli.custom.storePickupStoreID = storeId;
            pli.custom.secondaryWareHouse = storeId;
        }
    });
}

function personaliPriceAdjustment(productLineItem, offerId, quantity, adjustedPrice, originalPrice) {
	try {
		productLineItem.custom['personaliDiscount'] = (adjustedPrice - originalPrice);	//	pli.product.priceModel.price.value;
		productLineItem.custom['personaliOfferID'] = offerId;
		// Remove current price adjustment
		var currentPriceAdjustment : PriceAdjustment = productLineItem.getPriceAdjustmentByPromotionID(offerId);
		if(currentPriceAdjustment) {
			productLineItem.removePriceAdjustment(currentPriceAdjustment);
		}
		
		// Create price adjustment
		var priceAdjust = productLineItem.createPriceAdjustment(offerId);
		// Other product-level price adjustments will be removed in CalculateCart.ds
		// priceAdjust.setPriceValue((discountPrice - pli.product.priceModel.price.decimalValue) * params.Quantity.doubleValue);
		priceAdjust.setPriceValue( ( adjustedPrice - originalPrice ) * quantity );
		priceAdjust.setLineItemText(Resource.msg('personali.lineItemText', 'personali', null));
		
		return true;
	} catch (e) {
		var error = e;
		return false;
	}
}

function addCartRescuePriceAdjustments() {
	var cart = app.getModel('Cart').goc();
    var netotiateOfferId = request.httpParameterMap.netotiateOfferId.stringValue;
    if (!empty(netotiateOfferId)) {
    	Transaction.wrap(function () {
    		cart.getPersonaliCartRescueResponse(netotiateOfferId);
    	});
    	return netotiateOfferId;
    }
}

function compareProductOptionProducts_With_ProductLineItemOptionProducts (productOptions, lineItemOptions, productOptionModel) {
	var optionProductFound = false;
	if (lineItemOptions.length == 0) {
		optionProductFound = true;
	} else {
		for each(var i = 0 ; i < productOptions.length ; i++) {
			optionProductFound = false;
			var selectedOption = productOptionModel.getSelectedOptionValue(productOptions[i]);
			for each(var j = 0 ; j < lineItemOptions.length ; j++) {
				if(lineItemOptions[j].optionValueID == selectedOption.ID) {
					optionProductFound = true;
					break;
				}
			}
			if (!optionProductFound){
				return optionProductFound;
			}
		}
	}
	return optionProductFound;
}
function removeFrame()
{
	var cart = app.getModel('Cart').get();
	if(cart)
	{
		if(request.httpParameterMap.pli_uuid && !empty(request.httpParameterMap.pli_uuid.stringValue))
		{
			
			 var lineItem = cart.getProductLineItemByUUID(request.httpParameterMap.pli_uuid.stringValue);
			 if(lineItem != null)
			 {
			 	// get none option
				var product = lineItem.product;
			 	var POM = product.getOptionModel();
                var baseOption = POM.getOption('tuloBase');
                var optionValues = null;
                if(baseOption != null)
                {
                	 optionValues = POM.getOptionValues(baseOption);
                }
                var noneValue = null;
                if(optionValues != null)
                {
                	for each (var optionValue  in optionValues)
                	{
                		if(optionValue.ID == "none")
                		{
                			noneValue =  optionValue;
                			break;
                		}
                	}
                }
                // get option LI
                var baseOptionLineItem = null;
                var optionLineItems = lineItem.getOptionProductLineItems()
			 	for each (var opLi  in optionLineItems)
			 	{
			 		if(opLi.getOptionID() === baseOption.getID())
			 		{
			 			baseOptionLineItem = opLi;
			 			break;
			 		}
			 		
			 	}
                // set option to none
			 	if(baseOptionLineItem != null && noneValue != null)
			 	{
		 		  Transaction.wrap(function () {
			 		baseOptionLineItem.updateOptionValue(noneValue);
			 		  cart.calculate();
		 		  });	
			 	}
			 }
				
		} 
	}		
	show();
		
}

function addFrame(pid, frameID, frameQty)
{
    var cart = app.getModel('Cart').goc();
    var params = request.httpParameterMap;
    var Product = app.getModel('Product');
    var productOptionModel;
    var product;   
    var lastItemAdded = false;
    var lastItemAddedQty = false;

  
    // Adds a product.
    product = Product.get(frameID);    
    productOptionModel = product.updateOptionSelection(request.httpParameterMap);           
    var lineItemsOfPid = cart.getProductLineItemByID(frameID);
    var productFoundInCart = false;
    if (lineItemsOfPid.length > 0 && lineItemsOfPid.bonusProductLineItem == false) {
    	var productModel = product;
    	var lineItemQuantity = frameQty;
    	var productToAdd = productModel.object;	        	
    	for each(var lineItem in lineItemsOfPid) {
			var productOptions = productOptionModel.getOptions();
    		if(productOptions.length >= 0){
    			var lineItemOptions = lineItem.getOptionProductLineItems();
    			// productOptions are of that product which is going into cart which is productToAdd
    			//lineItemOptions, extracting all lineItem iterator's all option products into lineItemOptions
    			productFoundInCart = compareProductOptionProducts_With_ProductLineItemOptionProducts (productOptions, lineItemOptions, productOptionModel)
        		if (productFoundInCart) {
        			lineItemQuantity = parseInt(frameQty) + lineItem.quantity;
        			availabilityQty = lineItemQuantity;
		            Transaction.wrap(function () {
		            cart.updateLineItem(lineItem, productToAdd, lineItemQuantity, productOptionModel);
		            });
		            break;
        		}		       			
    		}
    	}
    }
    if (!productFoundInCart){	      	
        cart.addProductItem(product.object, frameQty, params.cgid.value, productOptionModel); 
    }
	 var lineItemsOfProduct = cart.getProductLineItemByID(pid);
	 if(lineItemsOfProduct && lineItemsOfProduct.length>0)
	 {
	 	pLI = lineItemsOfProduct[0];
	 	Transaction.wrap(function () {
	 		pLI.custom.linkedProductID = frameID;
	     });
	 }
}
function addFrameToCart()
{
	 var params = request.httpParameterMap;
	 if(params.frameID && !empty(params.frameID) && !empty(params.frameID.stringValue))
	 {
	 	// add Frame Product 	        
	 	addFrame(params.pid.stringValue, params.frameID.stringValue, params.Quantity.doubleValue);
	 }
	 show();
}

function addPillow( pillowID, pillowQty)
{
    var cart = app.getModel('Cart').goc();
    var params = request.httpParameterMap;
    var Product = app.getModel('Product');
    var productOptionModel;
    var product;   
    var lastItemAdded = false;
    var lastItemAddedQty = false;

  
    // Adds a product.
    product = Product.get(pillowID);    
    productOptionModel = product.updateOptionSelection(request.httpParameterMap);           
    var lineItemsOfPid = cart.getProductLineItemByID(pillowID);
    var productFoundInCart = false;
    if (lineItemsOfPid.length > 0 && lineItemsOfPid.bonusProductLineItem == false) {
    	var productModel = product;
    	var lineItemQuantity = pillowQty;
    	var productToAdd = productModel.object;	        	
    	for each(var lineItem in lineItemsOfPid) {
			var productOptions = productOptionModel.getOptions();
    		if(productOptions.length >= 0){
    			var lineItemOptions = lineItem.getOptionProductLineItems();
    			// productOptions are of that product which is going into cart which is productToAdd
    			//lineItemOptions, extracting all lineItem iterator's all option products into lineItemOptions
    			productFoundInCart = compareProductOptionProducts_With_ProductLineItemOptionProducts (productOptions, lineItemOptions, productOptionModel)
        		if (productFoundInCart) {
        			lineItemQuantity = parseInt(pillowQty) + lineItem.quantity;
        			availabilityQty = lineItemQuantity;
		            Transaction.wrap(function () {
		            cart.updateLineItem(lineItem, productToAdd, lineItemQuantity, productOptionModel);
		            });
		            break;
        		}		       			
    		}
    	}
    }
    if (!productFoundInCart){	      	
        cart.addProductItem(product.object, pillowQty, params.cgid.value, productOptionModel); 
    }
	 
}
/*
* Module exports
*/

/*
* Exposed methods.
*/
/** Adds a product to the cart.
 * @see {@link module:controllers/Cart~addProduct} */
exports.AddProduct = guard.ensure(['post'], addProduct);
/** Adds multiple products to the cart.
 * @see {@link module:controllers/Cart~addMultipleProducts} */
exports.AddMultipleProducts = guard.ensure(['https'], addMultipleProducts);
/** Invalidates the login and shipment forms. Renders the basket content.
 * @see {@link module:controllers/Cart~show} */
exports.Show = show;
exports.Show.public = true;
/** Form handler for the cart form.
 * @see {@link module:controllers/Cart~submitForm} */
exports.SubmitForm = guard.ensure(['post'], submitForm);
/** Redirects the user to the last visited catalog URL.
 * @see {@link module:controllers/Cart~continueShopping} */
exports.ContinueShopping = guard.ensure(['https'], continueShopping);
/** Adds a coupon to the cart using JSON. Called during checkout.
 * @see {@link module:controllers/Cart~addCouponJson} */
exports.AddCouponJson = guard.ensure(['get', 'https'], addCouponJson);
/** Displays the current items in the cart in the minicart panel.
 * @see {@link module:controllers/Cart~miniCart} */
exports.MiniCart = guard.ensure(['get'], miniCart);
/** Adds the product with the given ID to the wish list.
 * @see {@link module:controllers/Cart~addToWishlist} */
exports.AddToWishlist = guard.ensure(['get', 'https', 'loggedIn'], addToWishlist, {
    scope: 'wishlist'
});
/** Adds bonus product to cart.
 * @see {@link module:controllers/Cart~addBonusProductJson} */
exports.AddBonusProduct = guard.ensure(['post'], addBonusProductJson);
/** Changes pickup location on products  in cart.
 * @see {@link module:controllers/Cart~changePickupLocation} */
exports.ChangePickupLocation = guard.ensure(['post'], changePickupLocation);
/** Changes pickup in store to shipped to you
 * @see {@link module:controllers/Cart~changeDeliveryOption} */
exports.ChangeDeliveryOption = guard.ensure(['post'], changeDeliveryOption);
exports.ChangeDeliveryOptionInternal = changeDeliveryOption;
/** Adds Cart Rescue price adjustments from Personali
 * @see {@link module:controllers/Cart~addCartRescuePriceAdjustments} */
exports.AddCartRescuePriceAdjustments = guard.ensure(['post'], addCartRescuePriceAdjustments);
 /* @see {@link module:controllers/Cart~removeFrame} */
exports.RemoveFrame = guard.ensure(['get'], removeFrame);
/** Adds a frame to the cart.
 * @see {@link module:controllers/Cart~addFrameToCart} */
exports.AddFrameToCart = guard.ensure(['post'], addFrameToCart);
