'use strict';
/**
* Controller for all landing pages
*
* @module  controllers/LandingPage
*/
/* API Includes */
var URLUtils = require('dw/web/URLUtils');
var Resource = require('dw/web/Resource');
var Site = require('dw/system/Site');

/* Script Modules */
var app		= require('app_storefront_controllers/cartridge/scripts/app');
var guard	= require('app_storefront_controllers/cartridge/scripts/guard');
var Email	= app.getModel('Email');
var params	= request.httpParameterMap;


function showAirbnbForm() {
	app.getForm('airbnb').clear();
	session.forms.airbnb.clearFormElement();
	var Content = app.getModel('Content');
    var airbnbFormAsset = Content.get('airbnb-form');
    var pageMeta = require('app_storefront_controllers/cartridge/scripts/meta');
    pageMeta.update(airbnbFormAsset);
    app.getView({
        ContinueURL: URLUtils.https('Airbnb-SubmitAirbnbForm')
    }).render('landingpages/airbnb');
}

function submitAirbnbForm() {
	 var bnbData ={
		     	firstName : app.getForm('airbnb.firstName').value(),
		        lastName : app.getForm('airbnb.lastName').value(),
		      	superhost : app.getForm('airbnb.bnbSuperhost').value(),
		     	host : app.getForm('airbnb.bnbHost').value(),
		     	location : app.getForm('airbnb.location').value(),
		     	bedrooms : app.getForm('airbnb.bedrooms').value(),
		     	guestPY : app.getForm('airbnb.guestPY').value(),
		     	bnbLink : app.getForm('airbnb.bnbLink').value(),
		     	mattresses : app.getForm('airbnb.mattresses').value(),
		     	writeReview : app.getForm('airbnb.writeReview').value(),
		     	featureReview : app.getForm('airbnb.featureReview').value(),
		     	bnbDiscountCode : app.getForm('airbnb.bnbDiscountCode').value(),
		     	bnbEmail : app.getForm('airbnb.bnbEmail').value()
		     }
	 // Send Email.
     var email  = Site.current.getCustomPreferenceValue('emailID');
	 var ccEmail  = Site.current.getCustomPreferenceValue('ccEmailID');
	 var successFlag = false;
	 if(!empty(session.forms.airbnb.firstName.value)) {
			try {				
			    Email.get('landingpages/bnbformdata', email, ccEmail)
			        .setSubject('Airbnb Partner Form Submission')
			        .send({
			        	bnbData : bnbData
			        });
			    app.getForm('airbnb').clear();
			    session.forms.airbnb.clearFormElement();
			    session.forms.airbnb.firstName.value = null;
			   successFlag = true;
			}
		    catch (e) {
		    	successFlag = false;
			}
		    if (successFlag) {
		    	 response.redirect(URLUtils.https('Airbnb-ShowThankYou'));
		    }
	 } 
}

function showThankYou() {
	var Content = app.getModel('Content');
    var airbnbThankyouAsset = Content.get('airbnb-thankyou');
    var pageMeta = require('app_storefront_controllers/cartridge/scripts/meta');
    pageMeta.update(airbnbThankyouAsset);
	app.getView().render('landingpages/airbnbthankyou');
}

/*
 * Web exposed methods
 */
/** Contains the airbnb page preparation and display.
 * @see module:controllers/LandingPage~showAirbnbForm*/
exports.ShowAirbnbForm = guard.ensure(['https', 'get'], showAirbnbForm);
/** Handle the airbnb form .
 * @see module:controllers/LandingPage~submitAirbnbForm*/
exports.SubmitAirbnbForm = guard.ensure(['https', 'post'], submitAirbnbForm);

exports.ShowThankYou = guard.ensure(['https', 'get'], showThankYou);

