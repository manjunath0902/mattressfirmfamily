'use strict';

/**
 * Controller that renders the store finder and store detail pages.
 *
 * @module controllers/Stores
 */

/* API Includes */
var StoreMgr = require('dw/catalog/StoreMgr');
var SystemObjectMgr = require('dw/object/SystemObjectMgr');
var Transaction = require('dw/system/Transaction');
var Logger = require('dw/system/Logger');
var Resource = require('dw/web/Resource');
var Countries = require('app_storefront_core/cartridge/scripts/util/Countries');
/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var StorePicker = require('~/cartridge/controllers/StorePicker');


/************************** all set now lets start the real thing,  function... ********************/




function getProductVariations(id){
	let product = (id != null) ? dw.catalog.ProductMgr.getProduct(id) : null;
	if(product != null){
		return product.getVariants();
	}
	return null;
}

/*
 * @function: getNearbyStores
 * @return: {Stores: stores based on client's region}
 * 
 *  */

function getNearbyStores(maxDistance, zipCode) {	
	var countryCode = Countries.getCurrent({
		CurrentRequest: {
				locale: request.locale
			}
		}).countryCode;
	var distanceUnit = (countryCode == 'US') ? 'mi' : 'km';
	var storeMap = StoreMgr.searchStoresByPostalCode(countryCode, zipCode, distanceUnit, maxDistance);	
	return storeMap;
}

/*
 * @function: buildStoresResponse
 * @return: {Stores: stores,  storesJson: JSON.stringify(stores),  preferredStore: preferredStore}
 * stores builder for both actions... [find,findStores]
 */

function buildStoresResponse(storesMgrResult, preferredStore){
	//final processing
    var searchResult = null;
    var showSleepExperts = checkSleepStoreAvailability();
    
    if(storesMgrResult != null && storesMgrResult.length > 0){
		var storesSet = storesMgrResult.keySet();
    	var stores = [];
    	
    	//store distance 
    	for(var i=0; i< storesSet.length; i++) {

    		var productsAvailable = checkStoreInventory(storesSet[i].ID);
    		
    		if (productsAvailable.length > 0) {
    			//setup the first store if no store found 
        		if(preferredStore == null){
        			 preferredStore = setPreferredStore(storesSet[i].ID);
        		}
        		//setup the first store if no store found
        		
        		let store = getStoreTileStructre(storesSet[i], storesMgrResult, preferredStore);
        		stores.push(store);
			}
    		
    	}
    	 searchResult = {nozipcode: false, Stores: stores,  storesJson: JSON.stringify(stores),  preferredStore: preferredStore, showSleepExperts: showSleepExperts};
    }
    return searchResult;
}

/*
 * store til required structure
 */

function getStoreTileStructre(item, storesMgrResult, preferredStore, isDetailPage){
	storesMgrResult = storesMgrResult || null;
	isDetailPage = isDetailPage || false;
	
	var hours = isDetailPage ? prepareDetailsHours(item.storeHours, item.custom.timezoneOffset) : prepareListingHours(item.storeHours, item.custom.timezoneOffset);
	var schedule = prepareListingHours(item.storeHours, item.custom.timezoneOffset);
	var distance = (storesMgrResult == null || isDetailPage) ?  null :  storesMgrResult.get(item);
	
	var qAttr = "";
	if (item.address1 != null) qAttr += item.address1 + ", ";
	if (item.address2 != null) qAttr += item.address2 + ", ";
	if (item.city != null) qAttr += item.city + ", ";
	if (item.postalCode != null) qAttr += item.postalCode + ", ";
	if (item.stateCode != null) qAttr += item.stateCode + ", ";
	if (item.countryCode != null) qAttr += item.countryCode;

	var directionsLink = "//maps.google.com/maps?hl=en&f=q&q=" + encodeURI(qAttr);
	var markerType =  isDetailPage ?  ((item.ID == preferredStore) ? 4 : 3) : ((item.ID == preferredStore) ? 1 : 2);
	var url =  dw.web.URLUtils.url('Stores-Details','StoreID', item.ID).toString();
	var makeMyStoreURL =  dw.web.URLUtils.url('Stores-MakeItMyStore','storeId', item.ID).toString();
	var distance = (distance == null) ? distance : distance.toFixed(1);
	return { 
			 'ID' : item.ID,
			 'name' : item.name,
			 'address1' : item.address1,
			 'address2' : item.address2,
			 'city' : item.city,
			 'stateCode' : item.stateCode,
			 'postalCode' : item.postalCode,
			 'countryCode' : item.countryCode,
			 'phone' : item.phone,
			 'email' : item.email,
			 'lat' : item.latitude,
			 'lng' : item.longitude,
			 'distance' : distance,
			 'storeHours' : hours,
			 'schedule' : schedule,
			 'directionsLink': directionsLink,
			 'markerType': markerType,
			 'url' : url,
			 'makeMyStoreURL' : makeMyStoreURL,
		 };
        
}

/*
 * Office Hours Parser for [listing, details]
 */
function simplifyWorkingHours(storeHours, timezoneOffset){
	var storeHours = storeHours.toString().trim().replace(/\s+/g, '-');
	var rawData = storeHours.trim().split("-");
	var days = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
	var daySlots = [];
	var tmpDaySlot = {};
	var dayIndex = 0;
	var storeDay = getStoreDay(timezoneOffset);
	for(x = 0; x < rawData.length; x++){
		let chunk = rawData[x];
		if((days.indexOf(chunk.toLowerCase())) > -1){
			dayIndex = days.indexOf(chunk.toLowerCase());
			var tmpDaySlot = {
				day: chunk,
				open: null,
				openMeridiem: null,
				close: null,
				closeMeridiem: null,
				today: false
			};
			tmpDaySlot.today = dayIndex == storeDay;
			tmpDaySlot.day = days[dayIndex];
	    }
		else{
			if(chunk.toLowerCase().indexOf('am') > -1 || chunk.toLowerCase().indexOf('pm') > -1) {
				if(tmpDaySlot.openMeridiem == null) {
					tmpDaySlot.openMeridiem = chunk;		
	        	} 
				else {
					tmpDaySlot.closeMeridiem = chunk;		
	            }
	        }
			else {
				if(tmpDaySlot.open == null) {
					tmpDaySlot.open = chunk;		
	        	}
				else if(tmpDaySlot.close == null) {
					tmpDaySlot.close = chunk;		
	            }
	        }
			daySlots[dayIndex] = tmpDaySlot;
		}
		
	}
	return daySlots;
}

/*
 * parse order hours
 */
function parseOrderHours(tmpHours){
	var storeHours = [];
	if(!empty(tmpHours)){
		for(x = 0; x < tmpHours.length; x++){
			var dayHours = tmpHours[x];
			var hoursText = dayHours.open.substring(0,(dayHours.open.indexOf(':') > 0 ? dayHours.open.indexOf(':') : 0 )) + 
			dayHours.openMeridiem + " - " + 
			 dayHours.close.substring(0,(dayHours.close.indexOf(':') > 0 ? dayHours.close.indexOf(':') : 0 )) + 
			dayHours.closeMeridiem;
			var daySlot = {};
			daySlot.startDay = dayHours.day;
			daySlot.endDay = null;
			daySlot.hoursText = hoursText;
			if(storeHours.length <=0)
				storeHours[0] = daySlot;
			for(i = 0; i < storeHours.length; i++){
				var tmpDayHour = storeHours[i];
				if(hoursText == tmpDayHour.hoursText){
					if(storeHours[i].startDay != dayHours.day){
						storeHours[i].endDay = dayHours.day;
					}
				}
				else{
					var index = storeHours.length != null ? storeHours.length : 0;
					storeHours[index] = daySlot;
				}
			}
		}
	}
		
	return storeHours;	
}

/*
 * prepare listing hours
 */
function prepareListingHours(storeHours, timezoneOffset){
	var daySlots = simplifyWorkingHours(storeHours, timezoneOffset);
	var day = getStoreDay(timezoneOffset); 
	var today = day;
	var tomorrow = (today == 6) ? 0 : today + 1; //assuming day can't be higher than 6
	today = (typeof daySlots[today] != 'undefined') ? daySlots[today] : null;
	tomorrow = (typeof daySlots[tomorrow] != 'undefined') ? daySlots[tomorrow] : null;
	
	return {today: today, tomorrow: tomorrow};
}

/*
 * prepare details hours
 */
function prepareDetailsHours(storeHours, timezoneOffset){
	var daySlots = simplifyWorkingHours(storeHours, timezoneOffset);
	var date = new Date(); 
	var today = date.getDay();
	today = (typeof daySlots[today] != 'undefined') ? daySlots[today] : null;
	var tomorrow = (today == 6) ? 0 : today + 1; //assuming day can't be higher than 6
	tomorrow = (typeof daySlots[tomorrow] != 'undefined') ? daySlots[tomorrow] : null;
	let daysOrder = [1, 2, 3, 4, 5, 6, 0];
	var officeHours = [];
	for(x = 0; x < daysOrder.length; x++){
		officeHours[x] = (typeof daySlots[daysOrder[x]] != 'undefined') ? daySlots[daysOrder[x]] : null;
	}
	return officeHours;
}

/**
 * Check the sleep store based on geolocation.
 */
function checkSleepStoreAvailability() {
	var sleepStoreStateNames = dw.system.Site.getCurrent().getCustomPreferenceValue("sleepStoreStateNames");
    var currentStateName = request.getGeolocation().getRegionCode();
    var showSleepExperts = false;
    
    // loop over state names so sleep experts states can be identifies
	for each (var stateName in sleepStoreStateNames) { 
		if (currentStateName == stateName) { 
			showSleepExperts = true;
			break;
		}
	}
    return showSleepExperts;
}

/**
 * Provides a form to locate stores by geographical information.
 *
 * Clears the storelocator form. Gets a ContentModel that wraps the store-locator content asset.
 * Updates the page metadata and renders the store locator page (storelocator/storelocator template).
 */
function find() {
    var storeLocatorForm = app.getForm('storelocator');
    storeLocatorForm.clear();
   
    var Content = app.getModel('Content');
    var storeLocatorAsset = Content.get('store-locator');

    var pageMeta = require('app_storefront_controllers/cartridge/scripts/meta');
    pageMeta.update(storeLocatorAsset);
    
    var preferredStore = getPreferredStoreID();
    //in case no store found lets search based on client's location
    var geolocationZip = request.getGeolocation().getPostalCode(); 
    if(!empty(geolocationZip) && geolocationZip != null)
    {
		var storesMgrResult = getNearbyStores(15,geolocationZip);
		var data = buildStoresResponse(storesMgrResult, preferredStore);
		app.getView(data).render('storelocator/storelocator');
    }
    else
    {
    	var data = {nozipcode: true, showSleepExperts: false};
    	app.getView(data).render('storelocator/storelocator');
    }	
    
}



/**
 * The storelocator form handler. This form is submitted with GET.
 * Handles the following actions:
 * - findStores , by coordinates
 * In all cases, gets the search criteria from the form (formgroup) passed in by
 * the handleAction method and queries the platform for stores matching that criteria. Returns null if no stores are found,
 * otherwise returns a JSON object store, search key, and search criteria information. If there are search results, renders
 * the store results page (storelocator/storelocatorresults template), otherwise renders the store locator page
 * (storelocator/storelocator template).
 */
function findStores() {
	var preferredStore = getPreferredStoreID();
    var Content = app.getModel('Content');
    var storeLocatorAsset = Content.get('store-locator');
    var pageMeta = require('app_storefront_controllers/cartridge/scripts/meta');
    pageMeta.update(storeLocatorAsset);
    var storeLocatorForm = app.getForm('storelocator');
   
    var storesMgrResult = storeLocatorForm.handleAction({
        findStores: function(formGroup){
        	
        	var storeResults = null;	
        	let lat = parseFloat(formGroup.lat.value);
        	let lng = parseFloat(formGroup.lng.value);
        	let address = formGroup.address.value;
        	if(address != null && address != ""){
        		storeResults = StoreMgr.searchStoresByCoordinates(lat, lng, formGroup.distanceUnit.value, formGroup.maxdistance.value);
        	}
        	else{
        		 var geolocationZip = request.getGeolocation().getPostalCode();
        		 if(!empty(geolocationZip) && geolocationZip != null)
   				 {
        			storeResults = getNearbyStores(formGroup.maxdistance.value, geolocationZip);
   				 }	
        	}
            return storeResults;
            
        }
    });
    
    var data = buildStoresResponse(storesMgrResult, preferredStore);

    app.getView(data)
    .render('storelocator/storelocator'); 

}


/**
 * Renders the details of a store.
 *
 * Gets the store ID from the httpParameterMap. Updates the page metadata.
 * Renders the store details page (storelocator/storedetails template).
 */
function details() {

    var storeID = request.httpParameterMap.StoreID.value;
    var preferredStoreObject = StorePicker.GetPreferredStore(); //we will pass id because here, we are getting complete object
    var preferredStoreID = !empty(preferredStoreObject) ? preferredStoreObject.ID : 0;
    var storeObj = dw.catalog.StoreMgr.getStore(storeID);
    var store = getStoreTileStructre(storeObj, null,  preferredStoreID,  true);
    store.inventoryListID = storeObj.inventoryListID;
    var pageMeta = require('app_storefront_controllers/cartridge/scripts/meta');
    var nearByStores = [];
    var storesForMap = [];
    storesForMap.push(store);
    
    // MAT-496 set store meta (title and description)
    var storeDetailsMeta = new Object();
    storeDetailsMeta.pageTitle = Resource.msgf('storelocator.storelocatorresults.storedetails.pagetitle', 'storelocator', null, storeObj.name);
    storeDetailsMeta.pageDescription = Resource.msgf('storelocator.storelocatorresults.storedetails.pagedescripton', 'storelocator', null, storeObj.name, storeObj.stateCode);
    pageMeta.update(storeDetailsMeta);
    pageMeta.updatePageMetaData();
    
    var productsAvailable = checkStoreInventory(storeID);
    var products='';
    if (productsAvailable.length > 0) {
    	 products = productsAvailable.toString();    	
    	 var tmp1 = products.substring(0, products.lastIndexOf(",")).replace(/,/g, ', ');
    	 var tmp2 = products.substring(products.lastIndexOf(",")).replace(/,/g, ' and ');
    	 products = tmp1 + tmp2;
	}    
   
    if(store != null){
    	var postalCode = store.postalCode;
    	var countryCode = store.countryCode;
    	
    	var countryCode = Countries.getCurrent({
    		CurrentRequest: {
    				locale: request.locale
    			}
    		}).countryCode;
    	var distanceUnit = (countryCode == 'US') ? 'mi' : 'km';
    	
	    var storeMap = StoreMgr.searchStoresByCoordinates(store.lat, store.lng, distanceUnit, 15);	
		var storesSet = storeMap.keySet();
		
		
		
		//store distance 
	   	 for(i=0; i< storesSet.length; i++) {
	   		if(storesSet[i].ID == store.ID){//don't include the same one
	   			continue;
	   		}
	   		
	   		var productsAvailable = checkStoreInventory(storesSet[i].ID);
    		
    		if (productsAvailable.length > 0) {
    			let tmpStore = getStoreTileStructre(storesSet[i], storeMap, preferredStoreID);
    	   		nearByStores.push(tmpStore);
    	   		if(nearByStores.length == 3 ){
    	   			break;
    	   		}//i need only 3 but not the same store
			}
	   		
	   	 }
	   	 
	 	var currentStoreName = store.name;
		var storeNamesList = dw.system.Site.getCurrent().getCustomPreferenceValue("storeNamesList");
		var storeType = ""; 
		for each (var storeName in storeNamesList) { 
			if (currentStoreName.indexOf(storeName) > -1) { 
				currentStoreName = currentStoreName.replace(storeName, "");
				storeType = storeName;
				break;
			}
		}
	   	 
    }
    
    
    app.getView({Store: store, preferredStore: preferredStoreID,preferredStoreObject: preferredStoreObject, nearByStores: nearByStores, storesJson: JSON.stringify(storesForMap),Products : products, currentStoreName: currentStoreName, storeType: storeType})
    .render('storelocator/storedetails');

}

/**
 * Renders the checkstock info.
 *
 * Gets the storeInventoryListId from the httpParameterMap. 
 * Renders the store stock level
 */
function checkStock(storeInventoryListId){
	
	if(!empty(storeInventoryListId)) {
		var storeInventoryListId = storeInventoryListId;
	}
	else {
		var storeInventoryListId = request.httpParameterMap.storeInventoryListId.stringValue;
	}
	
	var comfortConfigurationJSON : String = dw.system.Site.getCurrent().getCustomPreferenceValue("tuloProducts");
	var iv = dw.catalog.ProductInventoryMgr.getInventoryList(storeInventoryListId);
	
	var tuloProducts = [];
	var stocks = [];
	if(comfortConfigurationJSON != null && iv != null){
		tuloProducts = JSON.parse(comfortConfigurationJSON);
	
		if(tuloProducts != null ){
			for(var i = 0; i< tuloProducts.length; i++)
				{
					let item = tuloProducts[i];
					stocks[i] = {item: item, items: []};
					let variations = getProductVariations(item.productID);
					if(variations != null){
						for(x = 0; x < variations.length; x++){
							let product = variations[x];
							
							let rec = iv.getRecord(product.ID);
							let stockLevel = ((rec != null) && typeof rec.stockLevel != 'undefined') ? rec.stockLevel.value : 0;
							let stockLabel = (stockLevel > 10) ? ((stockLevel / 10) * 10) + "+" :  stockLevel; 
							let size = parseInt(product.custom.size); //size 
							stocks[i].items.push({
									size: size,
									stockLabel: stockLabel,
									stock: stockLevel
							});

						}

					}
				}
		}
	}
	app.getView({
		stocks: stocks
	}).render('storelocator/stocklevel');
}

/**
 * Check store availability
 */
function checkStoreInventory(storeInventoryListId){
	if(!empty(storeInventoryListId)) {
		var storeInventoryListId = storeInventoryListId;
	}
	var comfortConfigurationJSON : String = dw.system.Site.getCurrent().getCustomPreferenceValue("tuloProducts");
	var iv = dw.catalog.ProductInventoryMgr.getInventoryList(storeInventoryListId);
	
	var tuloProducts = [];
	var stocks = [];
	if(comfortConfigurationJSON != null && iv != null){
		tuloProducts = JSON.parse(comfortConfigurationJSON);
	
		if(tuloProducts != null ){
			for(var i = 0; i< tuloProducts.length; i++)
				{
					let item = tuloProducts[i];
					let rec = iv.getRecord(item.productID.replace('tulo',''));
					let stockLevel = ((rec != null) && typeof rec.stockLevel != 'undefined') ? rec.stockLevel.value : 0;							
					if (stockLevel > 0) {
						stocks.push(item.name);
					}
				}
		}
	}

	//remove duplicates
	var result = stocks.filter(function (el, i, arr) {
		return arr.indexOf(el) === i;
	});
return result;

}

function getPreferredStoreID() {
	var store = null;
	if (session.customer.registered && !empty(session.customer.profile.custom.preferredStore)) {
		store = StoreMgr.getStore(session.customer.profile.custom.preferredStore);
	} else if (StoreMgr.getStore(session.custom.preferredStore)) {
		store = StoreMgr.getStore(session.custom.preferredStore);
	}
	return store != null ? store.ID : null;
}



/*
 * SetPrefferedStore: Extending it from StorePicker as it misses some of our required functionality
 * We suppose either storeId be passed through Http or by param
 */

function setPreferredStore(storeId) {
//	var form = session.forms;
	storeId = storeId ||  null;
	var customer = session.customer;
	if(empty(storeId)  && request.httpParameterMap.storeId.value != null) {
		storeId = request.httpParameterMap.storeId.value;
	}
	
	
	
	if(storeId != null){
		if (customer.registered) {
			Transaction.wrap(function () {
				customer.profile.custom.preferredStore = storeId;
			});
		}
		else {
			session.custom.preferredStore = storeId;
		}
	}
	return storeId;
}

function makeItMyStore(){
	var storeId = setPreferredStore();
	var store = StorePicker.GetPreferredStore(); //we will pass id because here, we are getting complete object
	var preferredStore = store != null ? store.ID : null;
	
	app.getView( {currentStoreId: storeId, preferredStore: preferredStore, Store: store})
    .render('storelocator/checkmystore');
}

/*
* store details specially designed for order details
*/
function fetchStoreDetailsForOrder(storeId)
{
	var store = StoreMgr.getStore(storeId);
	store = getStoreTileStructre(store, null, null, true);
	store.storeHours = parseOrderHours(store.storeHours); 
	return store; 	
}

/*
 * Returns day according to store location
 */
function getStoreDay(timezoneOffset) {
	var storeDay;
	if(timezoneOffset == null || empty(timezoneOffset)) {
		storeDay = new Date().getDay();
	}
	else {
		var targetDate = new Date();
		var timestamp = targetDate.getTime()/1000 + targetDate.getTimezoneOffset() * 60;
		timezoneOffset = parseFloat(timezoneOffset);
		var localdate = new Date(timestamp * 1000 + timezoneOffset);
		storeDay = localdate.getDay();
	}
	return storeDay;
}

/*
 * Exposed web methods
 */
/** Renders form to locate stores by geographical information.
 * @see module:controllers/Stores~find */
exports.Find = guard.ensure(['get'], find);
/** The storelocator form handler.
 * @see module:controllers/Stores~findStores */
exports.FindStores = guard.ensure(['post'], findStores);
/** Renders the details of a store.
 * @see module:controllers/Stores~details */
exports.Details = guard.ensure(['get'], details);

/** Ajax the checkstock for a store.
 * @see module:controllers/Stores~details */
exports.CheckStock = guard.ensure(['get'], checkStock);
//preferred store...
exports.GetPreferredStoreID = getPreferredStoreID;
exports.MakeItMyStore = guard.ensure(['post'], makeItMyStore);
exports.PrepareListingHours = prepareListingHours;
exports.PrepareDetailsHours = prepareDetailsHours;
exports.FetchStoreDetailsForOrder = fetchStoreDetailsForOrder;
exports.CheckStoreInventory = checkStoreInventory;