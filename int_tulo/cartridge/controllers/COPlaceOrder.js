'use strict';

/**
 * Controller that creates an order from the current basket. It's a pure processing controller and does
 * no page rendering. The controller is used by checkout and is called upon the triggered place order action.
 * It contains the actual logic to authorize the payment and create the order. The controller communicates the result
 * of the order creation process and uses a status object PlaceOrderError to set proper error states.
 * The calling controller is must handle the results of the order creation and evaluate any errors returned by it.
 *
 * @module controllers/COPlaceOrder
 */

/* API Includes */
var OrderMgr = require('dw/order/OrderMgr');
var PaymentMgr = require('dw/order/PaymentMgr');
var Resource = require('dw/web/Resource');
var Status = require('dw/system/Status');
var Transaction = require('dw/system/Transaction');
var Logger = require('dw/system/Logger');

/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var COBilling = require('~/cartridge/controllers/COBilling');
var COSummary = require('~/cartridge/controllers/COSummary');
var CheckoutController = require('~/cartridge/controllers/Checkout');

var Cart = app.getModel('Cart');
var Email = app.getModel('Email');
var Order = app.getModel('Order');
var PaymentProcessor = app.getModel('PaymentProcessor');

var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;
var PaymentInstrument = require('dw/order/PaymentInstrument');
/**
 * Responsible for payment handling. This function uses PaymentProcessorModel methods to
 * handle payment processing specific to each payment instrument. It returns an
 * error if any of the authorizations failed or a payment
 * instrument is of an unknown payment method. If a payment method has no
 * payment processor assigned, the payment is accepted as authorized.
 *
 * @transactional
 * @param {dw.order.Order} order - the order to handle payments for.
 * @return {Object} JSON object containing information about missing payments, errors, or an empty object if the function is successful.
 */
function handlePayments(order) {

    if (order.getTotalNetPrice() !== 0.00) {

        var paymentInstruments = order.getPaymentInstruments();

        if (paymentInstruments.length === 0) {
            return {
                missingPaymentInfo: true
            };
        }
        /**
         * Sets the transaction ID for the payment instrument.
         */
        var handlePaymentTransaction = function () {
            paymentInstrument.getPaymentTransaction().setTransactionID(order.getOrderNo());
        };

        for (var i = 0; i < paymentInstruments.length; i++) {
            var paymentInstrument = paymentInstruments[i];

            if (PaymentMgr.getPaymentMethod(paymentInstrument.getPaymentMethod()).getPaymentProcessor() === null) {

                Transaction.wrap(handlePaymentTransaction);

            } else {

                var authorizationResult = PaymentProcessor.authorize(order, paymentInstrument);
                if(authorizationResult.authorized){
                	return {};
                }else if (authorizationResult.not_supported || authorizationResult.error) {
                    if (!empty(authorizationResult.forterErrorCode)) {
                        return {
                            error           : true,
                            forterErrorCode : authorizationResult.forterErrorCode
                        };
                    } else {
                        return {error : true};
                    }
                }else if(authorizationResult.authorized == false) {
                    return {
                        error: true
                    };
                }else if(authorizationResult.review){
                	return{
                		review: true
                	};
                }

            }
        }
    }

    return {};
}

/**
 * The entry point for order creation. This function is not exported, as this controller must only
 * be called by another controller.
 *
 * @transactional
 * @return {Object} JSON object that is empty, contains error information, or PlaceOrderError status information.
 */
function start() {
    var cart = Cart.get();

    if (cart) {

       /* var COShipping = app.getController('COShipping');

        // Clean shipments.
        COShipping.PrepareShipments(cart);

        // Make sure there is a valid shipping address, accounting for gift certificates that do not have one.
        if (cart.getProductLineItems().size() > 0 && cart.getDefaultShipment().getShippingAddress() === null) {
            COShipping.Start();
            return {};
        }

        // Make sure the billing step is fulfilled, otherwise restart checkout.
        if (!session.forms.billing.fulfilled.value) {
            app.getController('COCustomer').Start();
            return {};
        }*/

        Transaction.wrap(function () {
            cart.calculate();
        });

        Transaction.wrap(function () {
            if (!COBilling.ValidatePayment(cart)) {
                COBilling.Start();
                return {};
            }
        });

        var validationResult = cart.validateForCheckout();

        // TODO - what are those used for - do they need to be returned/passed to a template ?
        var BasketStatus = validationResult.BasketStatus;

        // Recalculate the payments. If there is only gift certificates, make sure it covers the order total, if not
        // back to billing page.
        Transaction.wrap(function () {
            if (!cart.calculatePaymentTransactionTotal()) {
                COBilling.Start();
                return {};
            }
        });

        // Handle used addresses and credit cards.
        var saveCCResult = COBilling.SaveCreditCard();

        if (!saveCCResult) {
            return {
                error: true,
                PlaceOrderError: new Status(Status.ERROR, 'confirm.error.technical')
            };
        }

        // Creates a new order. This will internally ReserveInventoryForOrder and will create a new Order with status
        // 'Created'.  
        try {
          var order;
          Transaction.wrap(function () {
        	  if(session.custom.SynchronyOrderNo != null && !empty(session.custom.SynchronyOrderNo)){
        		  order = OrderMgr.createOrder(cart.object, session.custom.SynchronyOrderNo);  
        	  }
		      else {
		    	  order = OrderMgr.createOrder(cart.object);
		      }
          });
	          
        // set order level flag for isMFICore
        
	        Transaction.wrap(function () {
        		order.custom.isMFICore = mattressPipeletHelper.getOrderIsMFIStatus(order);
        		if(order.getShipments()[0].getShippingMethod().ID == 'storePickup') {
        			order.custom.mfiPickupDeliveryName = order.billingAddress.fullName;
        		}
	        });
        }
        catch(e) {
        	 Logger.error('Failed to set isMFICore on the Order with exception ' + e.toString());
        }

        if (!order) {
            // TODO - need to pass BasketStatus to Cart-Show ?
            BasketStatus = new Status(Status.ERROR);
            CheckoutController.Start();

            return {};
        } else {
            var handlePaymentsResult = handlePayments(order);

            if (handlePaymentsResult.error) {
            	session.custom.SynchronyOrderNo = null;
                return Transaction.wrap(function () {
                    OrderMgr.failOrder(order);
                    return {
                        error: true,
                        PlaceOrderError: new Status(Status.ERROR, handlePaymentsResult.forterErrorCode ? handlePaymentsResult.forterErrorCode.code : 'confirm.error.technical')
                    };
                });

            } else if (handlePaymentsResult.missingPaymentInfo) {
            	session.custom.SynchronyOrderNo = null;
                return Transaction.wrap(function () {
                    OrderMgr.failOrder(order);
                    return {
                        error: true,
                        PlaceOrderError: new Status(Status.ERROR, 'confirm.error.technical')
                    };
                });
            }else if(handlePaymentsResult.review) {
            	return submitImpl(order, true, cart);
            }else {
            	return submitImpl(order, false, cart);
            }



        }
    } else {
        app.getController('Home').Show();
        return {};
    }
}

/**
 * Submits an order.
 *
 * @transactional
 * @param {dw.order.Order} order - the order to submit.
 * @param {Boolean} reviewState - if order was marked for review by Cybersource Decision Mgr
 * @return {Boolean | Object} false if order cannot be placed. true if the order confirmation status is CONFIRMED.
 * JSON object containing error information, or the order and/or order creation information.
 */
function submitImpl(order, reviewState, cart) {

    var orderPlacementStatus = Transaction.wrap(function () {
        if (OrderMgr.placeOrder(order) === Status.ERROR) {
            OrderMgr.failOrder(order);
            session.custom.SynchronyOrderNo = null;
            return false;
        }
        if(!reviewState){
        	order.setConfirmationStatus(order.CONFIRMATION_STATUS_CONFIRMED);
        }

        return true;
    });

    if (orderPlacementStatus === Status.ERROR) {
        return {error: true};
    } else {
    	var ConfigUtils = new (require('int_suretax/cartridge/scripts/util/ConfigUtils'));
    	if(ConfigUtils.settings.isEnabled){
    		Transaction.wrap(function () {	    			
    			cart.calculate(ConfigUtils.settings.Finalize, order);
    		});
    	}
    }

    // Creates purchased gift certificates with this order.
    if (!createGiftCertificates(order)) {
        OrderMgr.failOrder(order);
        return {error: true};
    }

   
    // Mark order as EXPORT_STATUS_READY.
    Transaction.wrap(function () {
    	if(!reviewState){
    		order.setExportStatus(dw.order.Order.EXPORT_STATUS_READY);
    		order.setConfirmationStatus(dw.order.Order.CONFIRMATION_STATUS_CONFIRMED);
    	}else{
    		order.setExportStatus(dw.order.Order.EXPORT_STATUS_NOTEXPORTED);
    		order.setConfirmationStatus(dw.order.Order.CONFIRMATION_STATUS_NOTCONFIRMED);
    	}

    });
    
    var InventLocationId = ('InventLocationId' in dw.system.Site.current.preferences.custom && !empty(dw.system.Site.current.preferences.custom.InventLocationId)) ? dw.system.Site.getCurrent().getCustomPreferenceValue('InventLocationId') : '290005';
    var plisIterator = order.getAllProductLineItems().iterator();
	Transaction.wrap(function () {
		order.custom.InventLocationId = InventLocationId;
		while (plisIterator.hasNext()) {
            pli = plisIterator.next();
            pli.custom.InventLocationId = empty(pli.custom.InventLocationId) ? InventLocationId : pli.custom.InventLocationId;
        }
	});
    
    if(session.forms.singleshipping.shippingAddress.scheduleWithRep.value == 'contactme') {
	    var preferredcontact = session.forms.singleshipping.shippingAddress.preferredcontacts.preferredcontact.htmlValue;
	    var shipmentsIterator = order.getShipments().iterator();
	    Transaction.wrap(function () {
			while (shipmentsIterator.hasNext()) {
				var shi : Shipment = shipmentsIterator.next();
				shi.getShippingAddress().custom.preferred_contact_method = preferredcontact;
			}
	    });
    }


    try {
        // Saves Extra Order attributes [MFM-299]
        mattressPipeletHelper.saveExtraOrderAttributes(order);
    }
    catch(e) {
        Logger.error('Failed to set extra order attributes with exception ' + e.toString());
    }
     
    /** Setting Billing Address2 in Address1 for AX Support in Order  **/
    try {  
        var orderBillingAddress = order.getBillingAddress();  
        if (!empty(orderBillingAddress)) {  
            var billingAddress2 = orderBillingAddress.getAddress2();    
            if (!empty(billingAddress2)) {    
                var billingAddress1 = orderBillingAddress.getAddress1();    
                var updatedBillingAddress1 = billingAddress1 + ", " + billingAddress2;    
                Transaction.wrap(function() {    
                    orderBillingAddress.setAddress1(updatedBillingAddress1);        
                });    
            }  
        }  
    } catch (e) {  
        Logger.error('Failed to set Billing Address2 in Address1 for order {0} here are error details ' + e.toString(), order.getOrderNo());  
    }

    // Clears all forms used in the checkout process.
    session.forms.singleshipping.clearFormElement();
    session.forms.multishipping.clearFormElement();
    session.forms.billing.clearFormElement();

    var shipmentsIterator : Iterator = order.getShipments().iterator();
    Transaction.wrap(function () {
    	while (shipmentsIterator.hasNext()) {
            shipIt = shipmentsIterator.next();
            
            /** Setting Shipping Address2 in Address1 for AX Support in Order  **/
    		try {    
    		    var orderShippingAddress = shipIt.getShippingAddress();    
    		    if (!empty(orderShippingAddress)) {    
    		        var shippingAddress2 = orderShippingAddress.getAddress2();      
    		        if (!empty(shippingAddress2)) {      
    		            var shippingAddress1 = orderShippingAddress.getAddress1();      
    		            var updatedShippingAddress1 = shippingAddress1 + ", " + shippingAddress2;
    		            orderShippingAddress.setAddress1(updatedShippingAddress1);
    		        }    
    		    }    
    		} catch (e) {    
    		    Logger.error('Failed to set Shipping Address2 in Address1 for order {0} here are error details ' + e.toString(), order.getOrderNo());    
    		}

    		shipIt.custom.deliveryDate = empty(shipIt.custom.deliveryDate) ? '2049-12-31T06:11:55.699Z' : shipIt.custom.deliveryDate;
    		var productLineItems = shipIt.getProductLineItems();
		    for each(var productLineItem in productLineItems) {
		    	// Copy the product's manufacturer SKU into the product line items' custom attribute so it is
                // available when the order is exported.
                productLineItem.custom.manufacturerSKU = !empty(productLineItem.product) && !empty(productLineItem.product.manufacturerSKU) ? productLineItem.product.manufacturerSKU : null;
		    	var setCustomDeliveryDate = false,
		    		storeId = false;
		    	if (session.customer.registered && !empty(session.customer.profile.custom.preferredStore)) {
		    		storeId = session.customer.profile.custom.preferredStore;
		    	} else if (session.custom.preferredStore) {
		    		storeId = session.custom.preferredStore;
		    	}
		    	if (storeId && !empty(storeId) && session.custom.deliveryOptionChoice == 'instorepickup') {
		    		var key = productLineItem.productID + "-" + storeId;
		            var storeResponse = session.custom[key];
		            if (storeResponse && !empty(storeResponse)) {
		            	productLineItem.custom.deliveryDate = empty(storeResponse.SlotDate) ? '2049-12-31T06:11:55.699Z' : mattressPipeletHelper.getISODate(storeResponse.SlotDate);
		            	shipIt.custom.deliveryDate = empty(storeResponse.SlotDate) ? '2049-12-31T06:11:55.699Z' : mattressPipeletHelper.getISODate(storeResponse.SlotDate);
		            } else {
		            	setCustomDeliveryDate = true;
		            }
		    	} else {
		    		setCustomDeliveryDate = true;
		    	}
		    	if (setCustomDeliveryDate === true) {
		    		productLineItem.custom.deliveryDate = empty(productLineItem.custom.deliveryDate) ? shipIt.custom.deliveryDate : productLineItem.custom.deliveryDate;
		    	}
		    }
    	}
    });
    var isOnlyRedCarpetItemsInCart = mattressPipeletHelper.getOrderIsRedCarpetStatus(order);
     // Send order confirmation and clear used forms within the checkout process.
    Email.get('mail/orderconfirmation', order.getCustomerEmail())
        .setSubject((Resource.msg('order.orderconfirmation-email.001', 'order', null) + ' ' + order.getOrderNo()).toString())
        .send({
            Order: order,
            IsOnlyRedCarpetItemsInCart : isOnlyRedCarpetItemsInCart
        });

    return {
        Order: order,
        order_created: true
    };
}

/**
 * Creates a gift certificate for each gift certificate line item in the order
 * and sends an email to the gift certificate receiver.
 *
 * @param {dw.order.Order} order - the order to create the gift certificates for.
 * @return {Boolean} true if the order successfully created the gift certificates, false otherwise.
 */
function createGiftCertificates(order) {

    var giftCertificates = Order.get(order).createGiftCertificates();

    if (giftCertificates) {

        for (var i = 0; i < giftCertificates.length; i++) {
            var giftCertificate = giftCertificates[i];

            // Send order confirmation and clear used forms within the checkout process.
            Email.get('mail/giftcert', giftCertificate.recipientEmail)
                .setSubject(Resource.msg('resource.ordergcemsg', 'email', null) + ' ' + giftCertificate.senderName)
                .send({
                    GiftCertificate: giftCertificate
                });
        }

        return true;
    } else {
        return false;
    }
}

/**
 * Asynchronous Callbacks for OCAPI. These functions result in a JSON response.
 * Sets the payment instrument information in the form from values in the httpParameterMap.
 * Checks that the payment instrument selected is valid and authorizes the payment. Renders error
 * message information if the payment is not authorized.
 */
function submitPaymentJSON() {

    var order = Order.get(request.httpParameterMap.order_id.stringValue);
    if (order.object && (request.httpParameterMap.order_token.stringValue === order.getOrderToken())) {

        session.forms.billing.paymentMethods.clearFormElement();

        var requestObject = JSON.parse(request.httpParameterMap.requestBodyAsString);
        var form = session.forms.billing.paymentMethods;

        for (var requestObjectItem in requestObject) {
            var asyncPaymentMethodResponse = requestObject[requestObjectItem];

            var terms = requestObjectItem.split('_');
            if (terms[0] === 'creditCard' && terms[1] === 'number') {
                form.creditCard.number.setValue(asyncPaymentMethodResponse);
            } else if (terms[0] === 'creditCard' && terms[1] === 'cvn') {
                form.creditCard.cvn.setValue(asyncPaymentMethodResponse);
            } else if (terms[0] === 'creditCard' && terms[1] === 'month') {
                form.creditCard.month.setValue(Number(asyncPaymentMethodResponse));
            } else if (terms[0] === 'creditCard' && terms[1] === 'year') {
                form.creditCard.year.setValue(Number(asyncPaymentMethodResponse));
            } else if (terms[0] === 'creditCard' && terms[1] === 'owner') {
                form.creditCard.owner.setValue(asyncPaymentMethodResponse);
            } else if (terms[0] === 'creditCard' && terms[1] === 'type') {
                form.creditCard.type.setValue(asyncPaymentMethodResponse);
            } else if (terms[0] === 'selectedPaymentMethodID') {
                form.selectedPaymentMethodID.setValue(asyncPaymentMethodResponse);
            }
        }

        if (COBilling.HandlePaymentSelection('cart').error || handlePayments().error) {
            app.getView().render('checkout/components/faults');
            return;
        } else {
            app.getView().render('checkout/components/payment_methods_success');
            return;
        }
    } else {
        app.getView().render('checkout/components/faults');
        return;
    }
}

/*
 * Asynchronous Callbacks for SiteGenesis.
 * Identifies if an order exists, submits the order, and shows a confirmation message.
 */
function submit() {

    var order = Order.get(request.httpParameterMap.order_id.stringValue);
    if (!order.object || (request.httpParameterMap.order_token.stringValue !== order.getOrderToken())) {
        COBilling.Start();
        return;
    } else if (submitImpl().error) {
        COBilling.Start();
        return;
    }

    COSummary.ShowConfirmation();
}

/*
 * Module exports
 */

/*
 * Web exposed methods
 */
/** @see module:controllers/COPlaceOrder~submitPaymentJSON */
exports.SubmitPaymentJSON = guard.ensure(['https'], submitPaymentJSON);
/** @see module:controllers/COPlaceOrder~submitPaymentJSON */
exports.Submit = guard.ensure(['https'], submit);

/*
 * Local methods
 */
exports.Start = start;
