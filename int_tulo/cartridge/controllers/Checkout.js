'use strict';
/**
 * Controller for checkout customer information.
 *
 */

/* API Includes */
var CustomerMgr = require('dw/customer/CustomerMgr');
var HashMap = require('dw/util/HashMap');
var Resource = require('dw/web/Resource');
var ShippingMgr = require('dw/order/ShippingMgr');
var Site = require('dw/system/Site');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');
var StoreMgr = require('dw/catalog/StoreMgr');
var GiftCertificate = require('dw/order/GiftCertificate');
var GiftCertificateMgr = require('dw/order/GiftCertificateMgr');
var GiftCertificateStatusCodes = require('dw/order/GiftCertificateStatusCodes');
var PaymentInstrument = require('dw/order/PaymentInstrument');
var PaymentMgr = require('dw/order/PaymentMgr');
var ProductListMgr = require('dw/customer/ProductListMgr');
var Status = require('dw/system/Status');
var StringUtils = require('dw/util/StringUtils');
var Countries = require('app_storefront_core/cartridge/scripts/util/Countries');
//var Synchrony = require('int_synchrony/cartridge/scripts/util/Form');
var Logger = require('dw/system/Logger');

/* include Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var pipeletHelper = require('bc_sleepysc/cartridge/scripts/sleepys/util/SleepysPipeletsHelper').SleepysPipeletHelper;
var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;
var COBilling = require('~/cartridge/controllers/COBilling');
var StorePicker = require('~/cartridge/controllers/StorePicker');
var emailHelper = require("app_storefront_controllers/cartridge/scripts/util/SFEmailSubscriptionHelper").SFEmailSubscriptionHelper;

/* Checkout start - render delivery methods  */
function start() {	
	 var cart, pageMeta, homeDeliveries;
	    cart = app.getModel('Cart').get();
	    var geolocationZip = request.getGeolocation().getPostalCode();	   
	    var postalCode = empty(session.custom.customerZip) ? geolocationZip : session.custom.customerZip;					
		session.custom.customerZip = postalCode;
		
		 if (cart) {
			 	session.forms.cart.coupons.copyFrom(cart.object.couponLineItems);
			    var deliveryDate = session.custom.deliveryDate;
				var deliveryTime = session.custom.deliveryTime;
				
				if (empty(cart.getDefaultShipment().getShippingAddress()) && geolocationZip != null) {
					// Calculate estimated tax for geo-location 
					calculateGeolocationTax(cart);
				}
				
				if (empty(session.custom.deliveryOptionChoice)) {
					session.custom.deliveryOptionChoice='shipped';
				}
										
				session.forms.storepicker.postalCode.value = '';
						
				app.getView({
						Basket: cart.object,
                        DeliveryDate: deliveryDate,
                        DeliveryTime: deliveryTime, 
						ContinueURL: URLUtils.https('Checkout-Address'),
					}).render('checkout/deliverymethods');
	         			 
		    } else {
		        response.redirect(URLUtils.https('Home-Show'));
		        return;
		    }
}
function loadDeliveryOptions()
{
	var cart, pageMeta, homeDeliveries;
	cart = app.getModel('Cart').get();
	var geolocationZip = request.getGeolocation().getPostalCode();	   
	var postalCode = empty(session.custom.customerZip) ? geolocationZip : session.custom.customerZip;					
	session.custom.customerZip = postalCode;
	 if (cart) {
	    var deliveryDate = session.custom.deliveryDate;
		var deliveryTime = session.custom.deliveryTime;
		updateShippingAddressWithZip(session.custom.customerZip);
		if (empty(session.custom.deliveryOptionChoice)) {
			session.custom.deliveryOptionChoice='shipped';
		}
						
		if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule')) {					
			
			if(postalCode != null && !empty(postalCode))
			{
					
				var deliveryDatesArr = StorePicker.GetATPDeliveryDates(cart, postalCode);
				if (!empty(deliveryDatesArr)) {						
					var slotAvailable = 0;
					for (var i = 0; i < deliveryDatesArr.length; i++) {	
						
						if (slotAvailable > 0) {
							break;
						}
						var key = new Date(deliveryDatesArr[i]).toDateString().replace(' ', '', 'g');
						var deliveryTimeSlots = session.custom.deliveryDates[key]['timeslots'];	
						
						for (var j = 0; j < deliveryTimeSlots.length; j++) {    						
							if (deliveryTimeSlots[j]['available'] == 'yes' && deliveryTimeSlots[j]['slots'] > 0) {
								var startTime = deliveryTimeSlots[j]['startTime'];
	                			var endTime = deliveryTimeSlots[j]['endTime'];
	    						deliveryTime = StringUtils.formatDate(startTime, "h:mma").toString() +'-' + StringUtils.formatDate(endTime, "h:mma").toString();
	    						deliveryTime = deliveryTime.replace('PM',' pm', 'g').replace('AM',' am', 'g');
	    						deliveryDate = deliveryDatesArr[i];	
	    						session.custom.deliveryDate = deliveryDate;
	    						session.custom.deliveryTime = deliveryTime.replace('PM',' pm', 'g').replace('AM',' am', 'g');
	    						session.custom.deliveryZone = deliveryTimeSlots[j]['mfiDSZoneLineId'];
	    						slotAvailable++;
	    						break;
							}
                		}
						
					}
				}
				else{
					session.custom.deliveryDate =  '';
					session.custom.deliveryTime = '';
					deliveryDate = '';
					deliveryTime = '';
					
				}
			}	
		}		
		var coStep = 1;
		var phone = null;
		app.getView({
				Basket: cart.object,
				DeliveryDate: deliveryDate,
				DeliveryTime: deliveryTime,						
				DeliveryDatesArr:deliveryDatesArr,
				PostalCode: postalCode, 
				COStep: coStep,
				Phone: phone,
				ContinueURL: URLUtils.https('Checkout-Address'),
			}).render('checkout/components/atpdeliveryoptions');
     			 
    } else {		    	
        app.getController('Home').Show();
        return;
    }

}

/* Checkout render customer information and address */
function address() {		
	var cart;
	 cart = app.getModel('Cart').get();	 
	 if (cart) {
		 
			var billingForm = app.getForm('billing').object;
	        var paymentMethods = billingForm.paymentMethods;
	        if (!(paymentMethods.valid)) {
	           paymentMethods.clearFormElement();
	        }					
			 var pageMetaData = request.pageMetaData;
	     	 pageMetaData.title = Resource.msg('singleshipping.meta.pagetitle', 'checkout', null);
	         app.getView({
	             ContinueURL: URLUtils.https('Checkout-HandleAddressForm'),
	             Basket: cart.object
	         }).render('checkout/address'); 
	         return;
	    } else {
	        start();
	        return;
	    }
}


/* Checkout address handle action */
function handleAddressForm() {
    var billingForm = app.getForm('billing');  
    	billingForm.handleAction({
        save: function () {
            var cart = app.getModel('Cart').get();
				var _optOutFlag = !app.getForm('billing.billingAddress.email.addemailtosubscription').value();
        		var emailParams = {
        				emailAddress: session.forms.billing.billingAddress.email.emailAddress.value, 
        				zipCode: session.custom.customerZip, 
        				leadSource: 'checkout', 
        				siteId: dw.system.Site.getCurrent().getID(), 
        				optOutFlag: _optOutFlag
        		};
    			var returnResult = emailHelper.sendSFEmailInfo(emailParams);
    			if (returnResult.Status == 'SERVICE_ERROR'){
    				var returnResult = emailHelper.sendFailSafe(emailParams, returnResult.ErrorCode);
    			}

            if (cart) {               
                // Performs validation steps, based upon the entered billing and shipping address
                if ( !validateAddress() || !handleBillingAddress(cart)) 
        		{
                	response.redirect(URLUtils.https('Checkout-Address'));
                	return;
                } else {
                	session.forms.singleshipping.fulfilled.value = true;                            
                }
                
                handleShippingSettings(cart);
                
                // A successful address page will jump to the next checkout step.            
                response.redirect(URLUtils.https('COBilling-Start'));
                return;
                
            } else {
                // redirect to cart 
            	start();
            }
        },
        error: function () {        		
            response.redirect(URLUtils.https('Checkout-Address'));
            return;
        }
    });
}

/**
 * Prepares shipments. This function separates gift certificate line items from product
 * line items. It creates one shipment per gift certificate purchase
 * and removes empty shipments. If in-store pickup is enabled, it combines the
 * items for in-store pickup and removes them.
 * This function can be called by any checkout step to prepare shipments.
 *
 * @transactional
 * @return {Boolean} true if shipments are successfully prepared, false if they are not.
 */
function prepareShipments() {
    var cart, homeDeliveries;
    cart = app.getModel('Cart').get();

    homeDeliveries = Transaction.wrap(function () {

        homeDeliveries = false;
        //cart.updateGiftCertificateShipments();
        cart.removeEmptyShipments();

        if (Site.getCurrent().getCustomPreferenceValue('enableStorePickUp') && session.custom.deliveryOptionChoice == 'instorepickup') {
            homeDeliveries = cart.consolidateInStoreShipments();
            session.forms.singleshipping.inStoreShipments.shipments.clearFormElement();
            app.getForm(session.forms.singleshipping.inStoreShipments.shipments).copyFrom(cart.getShipments());

        } else if (session.custom.wasbopis > 0 && session.custom.deliveryOptionChoice == 'shipped') {
            //session.forms.singleshipping.clearFormElement();
            session.custom.wasbopis = 0;
            homeDeliveries = true;
            // clean up bopis shipments and pli's
            app.getController('Cart').ChangeDeliveryOptionInternal('shipped');
          	mattressPipeletHelper.splitShipmentsByType(cart);
          	cart.removeEmptyShipments();

        } else {
            homeDeliveries = true;
        }
        
        return homeDeliveries;
    });

    return homeDeliveries;
}

function updateDeliveryDateSelection(cart) {
	
	var deliveryDate = session.custom.deliveryDate;
	var deliveryTime = session.custom.deliveryTime;
	
	if (deliveryDate != 'null' && !empty(deliveryDate)) {
		var dateObj : Date = new Date(deliveryDate);
		deliveryDate = dateObj.toISOString();
	} else {
		deliveryDate = '2049-12-31T06:11:55.699Z';
	}
	
	if (deliveryTime != 'null' && !empty(deliveryTime) && session.custom.deliveryOptionChoice == 'shipped') {		 
	
	try {
		Transaction.wrap(function () {
    		var shipItr = cart.getShipments().iterator();
            while (shipItr.hasNext()) {
            	var shipment = shipItr.next();
				if (shipment.ID != "storePickup" && (shipment.custom.shipmentType == 'In-Market' || shipment.custom.shipmentType == 'Parcel')) {
            		shipment.custom.deliveryDate = deliveryDate;
            		shipment.custom.deliveryTime = deliveryTime;
	                var pliIterator = shipment.getProductLineItems().iterator();
	                var nonISODeliveryDate = deliveryDate.replace('-','/', 'g').split('T');
	                 
	            	while (pliIterator.hasNext()) {
	            		var pli : Product = pliIterator.next();
	                	pli.custom.deliveryDate = deliveryDate;
	                	if (shipment.custom.deliveryDate && dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule')) {
	                		var key = new Date(deliveryDate).toDateString().replace(' ', '', 'g');
	                		var deliveryTimeSlots = session.custom.deliveryDates[key]['timeslots'];
	                		var deliveryTimeSlot = false;
	                		for (var i = 0; i < deliveryTimeSlots.length; i++) {
	                			var startTime = deliveryTimeSlots[i]['startTime'].toString();
	                			var endTime = deliveryTimeSlots[i]['endTime'].toString();
	                			var deliveryTimes = deliveryTime.replace('pm',' pm', 'g').replace('am',' am', 'g').split('-');
	                			var deliveryStartTime = nonISODeliveryDate[0] + ' ' + deliveryTimes[0];
	                			var deliveryEndTime = nonISODeliveryDate[0] + ' ' + deliveryTimes[1];
	                			deliveryStartTime = new Date(deliveryStartTime).toString();
	                			deliveryEndTime = new Date(deliveryEndTime).toString();
	                			if (startTime == deliveryStartTime && endTime == deliveryEndTime) {
	                				deliveryTimeSlot = i;
	                				break;
	                			}
	                		}
	                		
	                		if (deliveryTimeSlot !== false) {
	                			pli.custom.InventLocationId = session.custom.deliveryDates[key]['timeslots'][deliveryTimeSlot]['location1'];
    	            			pli.custom.secondaryWareHouse = session.custom.deliveryDates[key]['timeslots'][deliveryTimeSlot]['location2'];
    	            			pli.custom.mfiDSZoneLineId = session.custom.deliveryDates[key]['timeslots'][deliveryTimeSlot]['mfiDSZoneLineId'];
    	            			pli.custom.mfiDSZipCode = session.custom.deliveryDates[key]['timeslots'][deliveryTimeSlot]['mfiDSZipCode'];
    	            			var optionLineItems = pli.optionProductLineItems;
    	            			for (var j = 0; j < optionLineItems.length; j++) {
    	            				var oli = optionLineItems[j];
    	            				if (oli.productID != 'none') {
    	            					oli.custom.InventLocationId = session.custom.deliveryDates[key]['timeslots'][deliveryTimeSlot]['location1'];
    	            				}
    	            			}
	                		}
	                	}
	            	}
            	}
            }
        });
	} catch (e) {
		var error = e;
	}
	} else {
		  var allShipments= cart.getShipments().iterator();
		  while (allShipments.hasNext()) {
			  var shipment =allShipments.next();
			  Transaction.wrap(function () {
			    shipment.custom.deliveryDate = null;
			    shipment.custom.deliveryTime = null;
			    session.custom.deliveryDate = null;
			    session.custom.deliveryTime = null;    
			    var pliIterator = shipment.getProductLineItems().iterator();
			    while (pliIterator.hasNext()) {
				     var pli : Product = pliIterator.next();
				     pli.custom.deliveryDate = null;
				     pli.custom.InventLocationId = null;
				     pli.custom.secondaryWareHouse = null;
				     pli.custom.mfiDSZoneLineId = null;
				     pli.custom.mfiDSZipCode = null;
				     session.custom.deliveryZone = null;
				     var optionLineItems = pli.optionProductLineItems;
				     for (var j = 0; j < optionLineItems.length; j++) {
				      var oli = optionLineItems[j];
				      if (oli.productID != 'none') {
				       oli.custom.InventLocationId = null;
				      }
				     }
			    }
			  });
		 }  
		}
}

/**
 * Handles the selected shipping address and shipping method. Copies the
 * address details and gift options to the basket's default shipment. Sets the
 * selected shipping method to the default shipment.
 *
 * @transactional
 * @param {module:models/CartModel~CartModel} cart - A CartModel wrapping the current Basket.
 */
function handleShippingSettings(cart) {

    Transaction.wrap(function () {
        var defaultShipment, shippingAddress, validationResult, BasketStatus, EnableCheckout;
        defaultShipment = cart.getDefaultShipment();
        shippingAddress = cart.createShipmentShippingAddress(defaultShipment.getID());
        var shippingMethodID;
        // Handle InStore Pickup - shipping address will be store address
        if (session.custom.deliveryOptionChoice == 'instorepickup') {
        	var storeId = '';
            var plisIterator = cart.getAllProductLineItems().iterator();
            while (plisIterator.hasNext()) {
                var pli = plisIterator.next();
                if(pli.product != null)
                {
                	 storeId = pli.custom.storePickupStoreID;
                     var key = pli.productID + "-" + storeId;
                     var storeResponse = session.custom[key];
                     if(storeResponse != null){
                    	 pli.custom.InventLocationId = storeResponse.Location1;
                         pli.custom.secondaryWareHouse = storeResponse.location2;
                         pli.custom.mfiATPLeadDate = storeResponse.MFIATPLeadDate;
                     }
                     
                }
               
            }
            var store = StoreMgr.getStore(storeId);
            shippingAddress.setAddress1(store.address1);
            shippingAddress.setAddress2(store.address2);
            shippingAddress.setCity(store.city);
            shippingAddress.setPostalCode(store.postalCode);
            shippingAddress.setStateCode(store.stateCode);
            shippingAddress.setCountryCode(store.countryCode.value);
            shippingAddress.setPhone(store.phone);          
            cart.setCustomerEmail(session.forms.billing.billingAddress.email.emailAddress.value);
            shippingMethodID='storePickup';
        } else {
            shippingAddress.setFirstName(session.forms.billing.shippingAddress.addressFields.firstName.value);
            shippingAddress.setLastName(session.forms.billing.shippingAddress.addressFields.lastName.value);
            shippingAddress.setAddress1(session.forms.billing.shippingAddress.addressFields.address1.value);
            shippingAddress.setAddress2(session.forms.billing.shippingAddress.addressFields.address2.value);
            shippingAddress.setCity(session.forms.billing.shippingAddress.addressFields.city.value);
            shippingAddress.setPostalCode(session.forms.billing.shippingAddress.addressFields.postal.value);
            shippingAddress.setStateCode(session.forms.billing.shippingAddress.addressFields.states.state.value);
            shippingAddress.setCountryCode('US');
            shippingAddress.setPhone(session.forms.billing.shippingAddress.addressFields.phone.value);
            cart.setCustomerEmail(session.forms.billing.billingAddress.email.emailAddress.value);
            shippingMethodID = cart.getDefaultShipment().getShippingMethodID();
        }
       
        // update shipping method
        var cartDefaultShipmentID = cart.getDefaultShipment().getID();        
        cart.updateShipmentShippingMethod(cartDefaultShipmentID, shippingMethodID, null, null);
        
        if (session.custom.deliveryOptionChoice == 'instorepickup') {
            var storePickupShipment = cart.getShipment(cartDefaultShipmentID);
            storePickupShipment.custom.storeId = storeId;
        } else if (session.custom.deliveryOptionChoice != 'instorepickup') {
        	var forceServiceCall = true;
            mattressPipeletHelper.splitShipmentsByType(cart.object, forceServiceCall);
        }
        
        var hasZIPchanged = session.custom.customerZip != shippingAddress.getPostalCode();
        if (hasZIPchanged) {
        	session.custom.customerZip = shippingAddress.getPostalCode();
        }
        cart.removeEmptyShipments();
        cart.calculate();

        validationResult = cart.validateForCheckout();

        // TODO - what are those variables used for, do they need to be returned ?
        BasketStatus = validationResult.BasketStatus;
        EnableCheckout = validationResult.EnableCheckout;

    });

    return;
}

/**
 * Validates the address.
 * @returns {boolean} Returns true if the address is valid. Returns false if the address is invalid.
 */
function validateAddress() {
	var isValidAddress = true, isValidBillingCity = false, isValidBillingState = false, isValidBillingZipCode = false, isValidShippingCity = false, isValidShippingState = false, isValidShippingZipCode = false;
	
    if (!app.getForm('billing').object.billingAddress.valid) {
    	isValidAddress = false;
    }
    
    var zipCode = app.getForm('billing.shippingAddress.addressFields.postal').value();
	var citiesAndStateJSON = mattressPipeletHelper.getCitiesAndStatesForZipCode(zipCode);
	
	if (!empty(citiesAndStateJSON)) {
		var cityData = JSON.parse(citiesAndStateJSON);   
	    for (var i = 0; i < cityData.cities.length; i++){
	        var data = cityData.cities[i];
	        if (app.getForm('billing.shippingAddress.addressFields.city').value().toLowerCase() === data.city.toLowerCase()) {
	        	isValidShippingCity = true;
			}
	        if (app.getForm('billing.shippingAddress.addressFields.states.state').value().toLowerCase() === data.state.toLowerCase()) {
	        	isValidShippingState = true;
			}      
	    }
	    isValidShippingZipCode = true;
    }
	

	if (!isValidShippingZipCode) {
       app.getForm('billing.shippingAddress.addressFields.postal').invalidate();
        isValidShippingCity = true;
        isValidShippingState = true;
        isValidAddress = false;
    }
	
    if (!isValidShippingCity) {
    	app.getForm('billing.shippingAddress.addressFields.city').invalidate();
        isValidAddress = false;
    }
    
    if (!isValidShippingState) {
    	app.getForm('billing.shippingAddress.addressFields.states.state').invalidate();
        isValidAddress = false;     
    }
    
    // validate billing address only if billing address is different than shipping address.
    if (!app.getForm('billing.billingAddress.useAsShippingAddress').value()) {
    	zipCode = app.getForm('billing.billingAddress.addressFields.postal').value();
    	citiesAndStateJSON = mattressPipeletHelper.getCitiesAndStatesForZipCode(zipCode);
    	if (!empty(citiesAndStateJSON)) {
    		var cityData = JSON.parse(citiesAndStateJSON);   
            for (var i = 0; i < cityData.cities.length; i++){
                var data = cityData.cities[i];
                if (app.getForm('billing.billingAddress.addressFields.city').value().toLowerCase() === data.city.toLowerCase()) {
                	isValidBillingCity = true;
        		}
                if (app.getForm('billing.billingAddress.addressFields.states.state').value().toLowerCase() === data.state.toLowerCase()) {
                	isValidBillingState = true;
        		}      
            }          
            isValidBillingZipCode = true;
		}
    	
    	if (!isValidBillingZipCode) {
            app.getForm('billing.billingAddress.addressFields.postal').invalidate();
            isValidBillingCity = true;
            isValidBillingState = true;
            isValidAddress = false;
        }
    	
    	if (!isValidBillingCity) {
            app.getForm('billing.billingAddress.addressFields.city').invalidate();
            isValidAddress = false;
        }
        
        if (!isValidBillingState) {
            app.getForm('billing.billingAddress.addressFields.states.state').invalidate();
            isValidAddress = false;
        }
	}

    return isValidAddress;
}



/**
 * Gets or creates a billing address and copies it to the billingaddress form. Also sets the customer email address
 * to the value in the billingAddress form.
 * @transaction
 * @param {module:models/CartModel~CartModel} cart - A CartModel wrapping the current Basket.
 * @returns {boolean} true
 */
function handleBillingAddress(cart) {

    var billingAddress = cart.getBillingAddress();
    Transaction.wrap(function () {

        if (!billingAddress) {
            billingAddress = cart.createBillingAddress();
        }

        app.getForm('billing.billingAddress.addressFields').copyTo(billingAddress);
        app.getForm('billing.billingAddress.addressFields.states').copyTo(billingAddress);
        billingAddress.setCountryCode('US');

        cart.setCustomerEmail(app.getForm('billing').object.billingAddress.email.emailAddress.value);
    });

    return true;
}

// calculate tax for geolocation
// set shipping address to geolocation

function calculateGeolocationTax(cart) {
	
	 Transaction.wrap(function () {
	        var defaultShipment, shippingAddress;
	        defaultShipment = cart.getDefaultShipment();
	        var shippingMethodID = defaultShipment.getShippingMethodID();
	        shippingAddress = cart.createShipmentShippingAddress(defaultShipment.getID());
	        shippingAddress.setCity(request.getGeolocation().getCity());
	        shippingAddress.setPostalCode(request.getGeolocation().getPostalCode());
	        shippingAddress.setStateCode(request.getGeolocation().getRegionCode());
	        shippingAddress.setCountryCode(request.getGeolocation().getCountryCode());				       
	        cart.updateShipmentShippingMethod(defaultShipment.getID(), shippingMethodID, null, null);
	        var forceServiceCall = true;
            mattressPipeletHelper.splitShipmentsByType(cart.object, forceServiceCall);
            cart.removeEmptyShipments();
	        cart.calculate();
	    });
	 return;
}

function verifyDeliveryZone() {
	var params = request.httpParameterMap;	
	var zipCode = params.zipCode.stringValue;
	var deliveryDate = session.custom.deliveryDate;
	var deliveryTime = session.custom.deliveryTime;
	var deliveryZone = session.custom.deliveryZone;

	var result = false;
	//verify shipping match with delivery zip or not  
	if(session.custom.deliveryOptionChoice == 'shipped'){
		var hasZIPchanged = session.custom.customerZip != zipCode;
		
		if (hasZIPchanged) {                    
		    
			result = true;
			var cart = app.getModel('Cart').get();	
			var deliveryDatesArr = null;
			if (deliveryDate != 'null' && !empty(deliveryDate)) {
				
				if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule') ) {	
					deliveryDatesArr = StorePicker.GetDeliveryZone(cart, zipCode, deliveryDate);
				}	
				if (!empty(deliveryDatesArr)) {			
					
					var key = new Date(deliveryDatesArr[0]).toDateString().replace(' ', '', 'g');
					var deliveryTimeSlots = session.custom.deliveryDates[key]['timeslots'];
					for (var j = 0; j < deliveryTimeSlots.length; j++) {    						
						if (deliveryTimeSlots[j]['available'] == 'yes' && deliveryTimeSlots[j]['slots'] > 0) {
							var startTime = deliveryTimeSlots[j]['startTime'];
							var endTime = deliveryTimeSlots[j]['endTime'];
							var slot = StringUtils.formatDate(startTime, "h:mma").toString() +'-' + StringUtils.formatDate(endTime, "h:mma").toString();
							if ((deliveryTime.toLowerCase().replace(' ', '', 'g') == slot.toLowerCase()) && (deliveryZone == deliveryTimeSlots[j]['mfiDSZoneLineId'])) {						
								result = false;
								break;
							}			
						}
					}				
				}
				
			} else{
				if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule') ) {	
					deliveryDatesArr = StorePicker.GetATPDeliveryDates(cart, zipCode);
				}
				if (!empty(deliveryDatesArr)) {			
					result = false;
					
					for (var i = 0; i < deliveryDatesArr.length; i++) {					
						if (result) {
							break;
						}
						var key = new Date(deliveryDatesArr[i]).toDateString().replace(' ', '', 'g');
						var deliveryTimeSlots = session.custom.deliveryDates[key]['timeslots'];	
						
						for (var j = 0; j < deliveryTimeSlots.length; j++) {    						
							if (deliveryTimeSlots[j]['available'] == 'yes' && deliveryTimeSlots[j]['slots'] > 0) {
								result = true;    						
	    						break;
							}
	            		}					
					}
				}
				else{
					result = false;
				}
			}		
		}
	}
	 app.getView({
	        JSONData: result
	    }).render('util/output');
}
function updateDeliveryZip()
{
	var result =  false;
	var params = request.httpParameterMap;
	var zipCode = params.zipCode.stringValue;
	if(zipCode == session.custom.customerZip)
	{
		result =  false;
	}
	else
	{
		session.custom.deliveryDate = "";
		session.custom.deliveryTime = "";
		session.custom.deliveryZone = "";
		session.custom.customerZip = zipCode;
		session.forms.storepicker.postalCode.value = zipCode;
		//update shipping address info so that tax is calculated for the new zip
		updateShippingAddressWithZip(zipCode);
		result =  true;
	}
	 app.getView({
	        JSONData: result
	   }).render('util/output');
}
function updateShippingAddressWithZip(zipCode)
{
	var cart = app.getModel('Cart').get();
	if(!empty(cart) && cart != null){
		Transaction.wrap(function () {
	        var defaultShipment, shippingAddress;
	        defaultShipment = cart.getDefaultShipment();
	      	shippingAddress = cart.createShipmentShippingAddress(defaultShipment.getID());
	      	var shippingMethodID = defaultShipment.getShippingMethodID();	       
	        var citiesAndStateJSON = mattressPipeletHelper.getCitiesAndStatesForZipCode(zipCode);   
	        var city = "";
	        var state = "";
	    	if (!empty(citiesAndStateJSON)) {
	    		var cityData = JSON.parse(citiesAndStateJSON);   
	            for (var i = 0; i < cityData.cities.length; i++){
	                var data = cityData.cities[i];
	                if (!empty(data.state)) {
	                	state = data.state.toUpperCase();
	                	city =  data.city;
	                	break;
	        		}
	            }
	            
	            if(!empty(state))
		        {
		        	shippingAddress.setCity(city);
		 	        shippingAddress.setPostalCode(zipCode);
		 	        shippingAddress.setStateCode(state);
		 	        shippingAddress.setCountryCode('US');				       
		 	        cart.updateShipmentShippingMethod(defaultShipment.getID(), shippingMethodID, null, null);
		 	        var forceServiceCall = true;
		            mattressPipeletHelper.splitShipmentsByType(cart.object, forceServiceCall);
		        }
			}
	    	else {
	    		shippingAddress.setCity(city);
	 	        shippingAddress.setPostalCode(zipCode);
	 	        shippingAddress.setStateCode(state);
	 	        shippingAddress.setCountryCode('US');				       
	 	        cart.updateShipmentShippingMethod(defaultShipment.getID(), shippingMethodID, null, null);
	 	        var forceServiceCall = true;
	            mattressPipeletHelper.splitShipmentsByType(cart.object, forceServiceCall);
	    	}        
	       cart.removeEmptyShipments();
	       cart.calculate();
	 
	    });
	}	
	 return;
}

/**
 * Handle coupon code checkout delivery page
 */
function handleCoupon() {
    var cartForm = app.getForm('cart');
    var cart = app.getModel('Cart').goc();
    cartForm.handleAction({    	
    	error: function () {
        	response.redirect(URLUtils.https('Checkout-Start'));
            return;
        },
        deleteCoupon: function (formgroup) {        
        	var couponBonusLineItems = cart.getBonusLineItemsByCouponCode(formgroup.getTriggeredAction().object.couponCode);
        	Transaction.wrap(function () {         
                cart.removeCouponLineItem(formgroup.getTriggeredAction().object);   
            });
            let r = require('app_storefront_controllers/cartridge/scripts/util/Response');
            return r.renderJSON(cart.getJsonByLineItems(couponBonusLineItems));
        }
    });
}
/*
* Web exposed methods
*/
exports.Start = guard.ensure(['https'], start);
exports.Address = guard.ensure(['https'], address);
exports.HandleAddressForm = guard.ensure(['https', 'post'], handleAddressForm);
exports.VerifyDeliveryZone = guard.ensure(['https', 'get'], verifyDeliveryZone);
exports.UpdateDeliveryZip = guard.ensure(['https', 'post'], updateDeliveryZip);
exports.LoadDeliveryOptions = guard.ensure(['https', 'get'], loadDeliveryOptions);
exports.HandleCoupon = guard.ensure(['https', 'post'], handleCoupon);

/*
 * Local methods
 */
exports.PrepareShipments = prepareShipments;
exports.UpdateDeliveryDateSelection = updateDeliveryDateSelection;