'use strict';
/**
 * Controller for retrieving a list of available stores with inventory for a specified product.
 *
 * @module controllers/StorePicker
 */

/* API Includes */
var HashMap = require('dw/util/HashMap');
var Countries = require('app_storefront_core/cartridge/scripts/util/Countries');
var StoreMgr = require('dw/catalog/StoreMgr');
var Site = require('dw/system/Site');
var URLUtils = require('dw/web/URLUtils');
var Transaction = require('dw/system/Transaction');
/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var cart = app.getModel('Cart').get();
var ProductUtils = require('app_storefront_core/cartridge/scripts/product/ProductUtils');
var ATPInventoryServices = require("bc_sleepysc/cartridge/scripts/init/services-ATPInventory");

function start() {
	app.getView({
		ContinueURL: URLUtils.http('StorePicker-HandleForm'),
		prodID: request.httpParameterMap.pid.value,
		isBopis: request.httpParameterMap.isBopis.value
	}).render('product/storepicker');
}
function startTulo() {	
	var geolocationZip = request.getGeolocation().getPostalCode();
	var customerZip = empty(session.custom.customerZip) ? geolocationZip : session.custom.customerZip;
	var postalCode = (empty(session.forms.storepicker.postalCode.value)) ? customerZip : session.forms.storepicker.postalCode.value;
	
	var distance = 100;
	var productID = request.httpParameterMap.pid.value;
	var isBopis = request.httpParameterMap.isBopis.value;
	session.custom.isBopis = isBopis;
	session.forms.storepicker.postalCode.value = postalCode;
	findInStoreFromATP(postalCode, distance, isBopis);
}
function handleFormTulo() {
	var geolocationZip = request.getGeolocation().getPostalCode();
	var customerZip = empty(session.custom.customerZip) ? geolocationZip : session.custom.customerZip;
	var postalCode = (empty(session.forms.storepicker.postalCode.value)) ? customerZip : session.forms.storepicker.postalCode.value;	
	var distance = session.forms.storepicker.maxdistance.value;
	var productID = session.forms.storepicker.productID.value;
	var isBopis = session.forms.storepicker.isBopis.value;
	session.custom.isBopis = isBopis;
	findInStoreFromATP(postalCode, distance, isBopis);
}
function handleForm() {
	var geolocationZip = request.getGeolocation().getPostalCode();
	var customerZip = empty(session.custom.customerZip) ? geolocationZip : session.custom.customerZip;
	var postalCode = (empty(session.forms.storepicker.postalCode.value)) ? customerZip : session.forms.storepicker.postalCode.value;	
	var distance = session.forms.storepicker.maxdistance.value;
	var productID = session.forms.storepicker.productID.value;
	var isBopis = session.forms.storepicker.isBopis.value;
	session.custom.isBopis = isBopis;
	findInStore(postalCode, distance, productID, isBopis);
}

function findInStore(customerZip, maxDistance, productID, isBopis) {
	session.custom.customerZip = customerZip;
	session.custom.maxDistance = maxDistance;
	session.custom.isCart = false;
	var storeArray = [];
	if (productID == null || productID == 'undefined' ) {
		productID = false;
		
		/*	TODO: Start	*/
		//evaluate cart
		// storeArray = evaluateCartForBopis();
		var storeMap = getNearbyStores(maxDistance)
		// convert map to array of stores
		for each( var store in storeMap.entrySet()) {
			storeArray.push(store);
		}
		/*	End	*/
		session.custom.preferredStore = !empty(storeArray[0]) ?  storeArray[0].key.ID : null ;
		session.custom.isCart = true;
	} else {
		var storeCount = 0;
		var storeMap = getNearbyStores(maxDistance)
		// convert map to array of stores
		for each( var store in storeMap.entrySet()) {
			if (storeCount == 4) {
				break;
			}
			
			var requestedDate = new Date();
			requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
			try {
				var requestPacket = '{"InventoryType": "Store","ZipCode": "' + customerZip + '","RequestedDate": "' + requestedDate + '","StoreId": "' + store.key.ID + '","Products": [{"ProductId": "' + productID + '","Quantity": 1}]}';
				var service = ATPInventoryServices.ATPInventoryTULO;
				var result = service.call(requestPacket);
				if (result.error != 0 || result.errorMessage != null || result.mockResult) {
					//return null;
				} else {
					var jsonResponse = JSON.parse(result.object.text);
					session.custom[productID + "-" + store.key.ID] = jsonResponse[0];
				}
			} catch (e) {
				var msg = e;
			}
			
			storeArray.push(store);
			storeCount++;
		}
	}
	
	app.getView({
		stores:storeArray,
		pid: productID,
		zip: customerZip,
		isBopis : isBopis,
		isCart : session.custom.isCart,
		ContinueURL: URLUtils.http('StorePicker-HandleForm')
	}).render('components/storepickup/storepicker');
}

function findInStoreFromATP(customerZip, maxDistance, isBopis) {
	session.custom.customerZip = customerZip;
	session.custom.maxDistance = maxDistance;
	session.custom.isCart = true;
	var storeArray = [];
	var products = new Array();
	var plis = cart.object.productLineItems;
	
	for (var i = 0; i < plis.length; i++ ) {
		var pli = plis[i];		
		products.push({
			'ProductId' : pli.productID,
			'Quantity' : pli.quantityValue
		});
		var optionLineItems = pli.optionProductLineItems;
		for (var j = 0; j < optionLineItems.length; j++) {
			var oli = optionLineItems[j];
			if (oli.productID != 'none') {
				products.push({
					'ProductId' : oli.productID,
					'Quantity' : oli.quantityValue
				});
			}
		}
	}
	
	if (products.length > 0) {
		var storeCount = 0;
		var storeMap = getNearbyStores(maxDistance)
		// convert map to array of stores
		session.custom.storeIDPicker = "";
		for each( var store in storeMap.entrySet()) {
			if (storeCount == 4) {
				break;
			}
			
			var requestedDate = new Date();
			requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
			try {
				var requestPacket = '{"InventoryType": "Store","ZipCode": "' + customerZip + '","RequestedDate": "' + requestedDate + '","StoreId": "' + store.key.ID + '","Products": ' + JSON.stringify(products) + '}';
				var service = ATPInventoryServices.ATPInventoryTULO;
				var result = service.call(requestPacket);
				if (result.error != 0 || result.errorMessage != null || result.mockResult) {
					//return null;
				} else {					
					var jsonResponse = JSON.parse(result.object.text);
					for (var i = 0; i < plis.length; i++) {
						var pli = plis[i];
						var response = jsonResponse[0];
						session.custom[pli.productID + '-' + store.key.ID] = response;
						var rrSe = response;
						if(storeCount == 0){
							Transaction.wrap( function() {
								pli.custom.storePickupStoreID = response.Location1;
								pli.custom.deliveryDate = response.SlotDate;							
								//session.custom.storeIDPicker += " ## "+response.Location1+"->"+response.SlotDate;
							});
						}	
											
					}
					storeArray.push(store);
				}
			} catch (e) {
				var msg = e;
			}
			
			
			storeCount++;
		}
		
		if (session.custom.preferredStore == null) {
			session.custom.preferredStore = !empty(storeArray[0]) ?  storeArray[0].key.ID : null ;
		} else {
			var isMatch=false;
			for (var i = 0; i < storeArray.length; i++) {				
				if (session.custom.preferredStore == storeArray[i].key.ID) {					
					isMatch=true;
					session.custom.preferredStore = !empty( storeArray[i].key.ID) ?   storeArray[i].key.ID : null ;					
					break;
				}				
			}

			if (!isMatch) {
				session.custom.preferredStore = !empty(storeArray[0]) ?  storeArray[0].key.ID : null ;
			}
		}
	}
		
	
	app.getView({
		Basket: cart.object,
		stores:storeArray,
		//pid: productID,
		zip: customerZip,
		isBopis : isBopis,
		isCart : session.custom.isCart,
		ContinueURL: URLUtils.http('StorePicker-HandleFormTulo')
	}).render('components/storepickup/storepicker');
}

function getNearbyStores(maxDistance) {
	var customerZip = session.custom.customerZip;
	var countryCode = Countries.getCurrent({
		CurrentRequest: {
				locale: request.locale
			}
		}).countryCode;
	var distanceUnit = (countryCode == 'US') ? 'mi' : 'km';
	var storeMap = StoreMgr.searchStoresByPostalCode(countryCode, customerZip, distanceUnit, maxDistance);	
	return storeMap;
}

function setPreferredStore(storeId) {
//	var form = session.forms;
	var customer = session.customer;
	if (customer.registered) {
		Transaction.wrap(function () {
			customer.profile.custom.preferredStore = !empty(request.httpParameterMap.storeId.value)?request.httpParameterMap.storeId.value:storeId;
		});
	} else if (request.httpParameterMap.storeId.value != null){
		session.custom.preferredStore = request.httpParameterMap.storeId.value;
	}
	else if(!empty(storeId) && storeId != null ){
		session.custom.preferredStore = storeId;
	}
	
}

function getPreferredStoreAddress() {
	var store = getPreferredStore();
	var prodId = request.httpParameterMap.pid ? request.httpParameterMap.pid : false;
	app.getView({
		preferredStore: store,
		pid: prodId	
	}).render('product/preferredStore');
}

function getPreferredStore() {
	var store = null;
	if (session.customer.registered && !empty(session.customer.profile.custom.preferredStore)) {
		store = StoreMgr.getStore(session.customer.profile.custom.preferredStore);
	} else if (StoreMgr.getStore(session.custom.preferredStore)) {
		store = StoreMgr.getStore(session.custom.preferredStore);
	}
	return store;
}

function updateBopisOption(data) {
	var params = request.httpParameterMap;
	var deliveryoption = params.deliveryoption.stringValue;
	session.custom.deliveryOptionChoice = deliveryoption;
}

function evaluateCartForBopis() {
	var storeMap = getNearbyStores(100);
	var storeArray = [];
	for (var i = 0; i < storeMap.entrySet().length; i++) {
		var store = storeMap.entrySet()[i];
		var hasProducts = storeHasProducts(store);
		if (hasProducts) {
			storeArray.push(store);
		}
		if (storeArray.length == 4) {
			break;
		}
	}
	return storeArray;
}

function getFirstAvailablestoreForBopis() {
	var storeMap = getNearbyStores(100);
	var storeCount = 0;
	var storeArray = [];
	for (var i = 0; i < storeMap.entrySet().length; i++) {
		//temp check to restrict to one store to eliminate quota error for default load
		if (storeCount == 1) {
				break;
			}
		var store = storeMap.entrySet()[i];
		var hasProducts = storeHasProducts(store);
		if (hasProducts) {
			storeArray.push(store);
		}
		if (storeArray.length == 1) {
			break;
		}
	 storeCount++;
	}
	return storeArray;
}

function getAvailablePickupStore() {
	var preferredStore = getPreferredStore();
	var storeArray = [];
	var storeId;
	//return preferredStore;
	// IF a preferred store is already selected, check if products are all available in preferred store
	if (preferredStore != null) {
		var hasProducts = storeHasProducts(preferredStore);
		if (hasProducts) {
			return preferredStore;
		} else {			
			storeArray = getFirstAvailablestoreForBopis();
			storeId = !empty(storeArray[0]) ?  storeArray[0].key.ID : null ;
			if (storeId != null) {
				session.custom.preferredStore = storeId;
				return StoreMgr.getStore(storeId)
			}
		}
	}
	else 
	{
		storeArray = getFirstAvailablestoreForBopis();
		storeId = !empty(storeArray[0]) ?  storeArray[0].key.ID : null ;
		if (storeId != null) {
			session.custom.preferredStore = storeId;
			return StoreMgr.getStore(storeId)
		}		
	}
	return null;
}

/** This function accepts a store and the line item container
 * it checks availability of each product line item in the store and returns
 * true only if all products are available.
 * @param store
 * @returns boolean
 */
function storeHasProducts(store) {
	var plis = cart.object.productLineItems;
	var storeID = (store instanceof dw.util.MapEntry) ? store.key.ID : store.ID;
	var storePostalCode = (store instanceof dw.util.MapEntry) ? store.key.postalCode : store.postalCode;
	var products = new Array();
	session.custom.wasbopis = (session.custom.wasbopis != null) ? session.custom.wasbopis : 0;
	for (var i = 0; i < plis.length; i++ ) {
		var pli = plis[i];
		/*if (empty(pli.custom.storePickupStoreID) && pli.custom.storePickupStoreID != storeID) {
			Transaction.wrap( function() {
				pli.custom.storePickupStoreID = storeID;
			});
		}*/
		products.push({
			'ProductId' : pli.productID,
			'Quantity' : pli.quantityValue
		});
		var optionLineItems = pli.optionProductLineItems;
		for (var j = 0; j < optionLineItems.length; j++) {
			var oli = optionLineItems[j];
			if (oli.productID != 'none') {
				products.push({
					'ProductId' : oli.productID,
					'Quantity' : oli.quantityValue
				});
			}
		}
	}
	
	if (products.length > 0) {
		var requestedDate = new Date();
		requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
		try {
			//storePostalCode = '77002';
			//storeID = '001056';
			var requestPacket = '{"InventoryType": "Store","ZipCode": "' + storePostalCode + '","RequestedDate": "' + requestedDate + '","StoreId": "' + storeID + '","Products": ' + JSON.stringify(products) + '}';
			var service = ATPInventoryServices.ATPInventoryTULO;
			var result = service.call(requestPacket);
			if (result.error != 0 || result.errorMessage != null || result.mockResult) {
				return false;
			} else {
				var jsonResponse = JSON.parse(result.object.text);
				for (var i = 0; i < plis.length; i++) {
					var pli = plis[i];
					var response = jsonResponse[0];
					session.custom[pli.productID + '-' + storeID] = response;
					if (pli.custom.storePickupStoreID != storeID) {
						Transaction.wrap( function() {
							pli.custom.storePickupStoreID = response.Location1;
							pli.custom.deliveryDate = response.SlotDate;
							session.custom.storeIDMain = response.Location1+"->"+response.SlotDate;
						});
					}
				}
			}
			return true;
		} catch (e) {
			var error = e;
		}
	}
	return false;
}
function updatePreferredStoreLineItems() {
	var plis = cart.object.productLineItems;
	
	
}

/**
 * resets the shipping options to Ship To Address and calls {@link module:controllers/COShipping~start|start} function.
 *
 * @transactional
 */
function changeDeliveryToShipAddress() {
	session.forms.singleshipping.clearFormElement();
	app.getController('Cart').ChangeDeliveryOption('shipped');
	session.custom.deliveryOptionChoice = 'shipped';
	app.getController('COShipping').Start();
}

function getATPDeliveryDatesForNextWeek() {
	var params = request.httpParameterMap;
	var cart = app.getModel('Cart').get();
	var postalCode = session.forms.singleshipping.shippingAddress.addressFields.postal.value;
	var format = params.format.stringValue;
	var date = params.currentWeekDate.stringValue;
	var deliveryDatesArr = getATPDeliveryDates(cart, postalCode, date);
	
	app.getView({
		DeliveryDatesArr: deliveryDatesArr
	}).render('checkout/components/delivery-weekly-schedule-mocked');
}

//should be called when ATP fails for core products and return its array
function makeATPFailedCoreProductsArray(products, postalCode) {
	var atpFailedCoreProductsArray = new Array();
	for (var i = 0; i < products.length; i++) {
		var coreProduct = products[i];
		atpFailedCoreProductsArray.push("Checkout_" + coreProduct.ProductId +"_" + postalCode);
	}
	return atpFailedCoreProductsArray;
}

function getATPDeliveryDates(cart, postalCode, date) {
	session.custom.atpFailedCoreProductsArray = null;
	var products = new Array();
	var deliveryDatesArr = new dw.util.ArrayList();
	
	var plis = cart.object.productLineItems;
	
	for (var i = 0; i < plis.length; i++ ) {
		var pli = plis[i];
		if (!empty(pli.product.custom.shippingInformation) && pli.product.custom.shippingInformation.toLowerCase() == 'core') {
			products.push({
				'ProductId' : pli.productID,
				'Quantity' : pli.quantityValue
			});
			var optionLineItems = pli.optionProductLineItems;
			for (var j = 0; j < optionLineItems.length; j++) {
				var oli = optionLineItems[j];
				if (oli.productID != 'none') {
					products.push({
						'ProductId' : oli.productID,
						'Quantity' : oli.quantityValue
					});
				}
			}
		}
	}
    if(products.length < 1) {
    	return deliveryDatesArr;
    }

		
	try {
		
		if (arguments.length > 2 && !empty(arguments[2])) {
			var requestedDate = new Date(arguments[2]);
			requestedDate.setDate(requestedDate.getDate() + 7);
			requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
		} else {
			var requestedDate = new Date();
			requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
		}
		
		var requestAtpAgain = true,
			requestCount = 0;
		do {
			var requestPacket = '{"InventoryType": "Delivery","ZipCode": "' + postalCode + '","RequestedDate": "' + requestedDate + '","StoreId": "","Products": ' + JSON.stringify(products) + '}';
			var service = ATPInventoryServices.ATPInventoryTULO;
			var result = service.call(requestPacket);
			requestCount++;
			if (result.error != 0 || result.errorMessage != null || result.mockResult) {
				session.custom.atpFailedCoreProductsArray = makeATPFailedCoreProductsArray(products, postalCode);
			} else {
				var jsonResponse = JSON.parse(result.object.text);
				if (arguments.length > 2 && !empty(arguments[2])) {
					var deliveryDateSession = session.custom.deliveryDates;
				} else {
					var deliveryDateSession = new Object();
				}
				
				var checkForDuplicates = new dw.util.ArrayList();
				for (var i = 0; i < jsonResponse.length; i++ ) {
					var response = jsonResponse[i];
					var deliveryDate = new Date(response.SlotDate);
					deliveryDate.setHours(10);
					
					var key = deliveryDate.toDateString().replace(' ', '', 'g');
					if (!deliveryDateSession[key]) {
						deliveryDateSession[key] = new Object();
						deliveryDateSession[key]['timeslots'] = new dw.util.ArrayList();
						deliveryDatesArr.add(deliveryDate);
					}
					
					var startTime = new Date(deliveryDate.toDateString() + ' ' + response.StartTime);
					var endTime = new Date(deliveryDate.toDateString() + ' ' + response.EndTime);
					if (checkForDuplicates.indexOf(key + startTime + endTime) == -1) {
						checkForDuplicates.add(key + startTime + endTime);
						deliveryDateSession[key]['timeslots'].add({
							'location1' : response.Location1,
							'location2' : response.location2,
							'mfiDSZoneLineId' : response.Zone,
							'mfiDSZipCode' : postalCode,
							'available' : response.Available.toLowerCase(),
							'startTime' : startTime,
							'endTime' : endTime,
							'slots' : Number(response.Slots)
						});
						
						var timeslotsComparator = new dw.util.PropertyComparator ('startTime', 'endTime');
						deliveryDateSession[key]['timeslots'].sort(timeslotsComparator);
						
						if (requestAtpAgain === true && response.Available.toLowerCase() == 'yes' && response.SlotDate !== null) {
							requestAtpAgain = false;
						}
					}
				}
				session.custom.deliveryDates = deliveryDateSession;
			}
			if (requestAtpAgain === true) {
				deliveryDatesArr = new dw.util.ArrayList();
				requestedDate = new Date(requestedDate);
				requestedDate.setDate(requestedDate.getDate() + 7);
				requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
			}
		} while (requestAtpAgain === true && requestCount < 1); 
		
	} catch (e) {
		var error = e;
	}
	return deliveryDatesArr;
}

function getDeliveryZone(cart, postalCode, date) {
	//session.custom.customerZip = postalCode;
	var products = new Array();
	var deliveryDatesArr = new dw.util.ArrayList();
	var isMattress =  false;
	var plis = cart.object.productLineItems;
	for (var i = 0; i < plis.length; i++ ) {
		var pli = plis[i];
		if (!empty(pli.product.custom.shippingInformation) && pli.product.custom.shippingInformation.toLowerCase() == 'core') {
			products.push({
				'ProductId' : pli.productID,
				'Quantity' : pli.quantityValue
			});
			var optionLineItems = pli.optionProductLineItems;
			for (var j = 0; j < optionLineItems.length; j++) {
				var oli = optionLineItems[j];
				if (oli.productID != 'none') {
					products.push({
						'ProductId' : oli.productID,
						'Quantity' : oli.quantityValue
					});
				}
			}
		}
	}
	
	if(products.length < 1) {
    	return deliveryDatesArr;
    }
		
	try {
		
		var requestedDate = new Date(date);
		requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
		var requestAtpAgain = true,
			requestCount = 0;
		do {
			var requestPacket = '{"InventoryType": "Delivery","ZipCode": "' + postalCode + '","RequestedDate": "' + requestedDate + '","StoreId": "","Products": ' + JSON.stringify(products) + '}';
			var service = ATPInventoryServices.ATPInventoryTULO;
			var result = service.call(requestPacket);
			requestCount++;
			if (result.error != 0 || result.errorMessage != null || result.mockResult) {
				
			} else {
				var jsonResponse = JSON.parse(result.object.text);
				var deliveryDateSession = new Object();
				
				var checkForDuplicates = new dw.util.ArrayList();
				for (var i = 0; i < jsonResponse.length; i++ ) {
					var response = jsonResponse[i];
					var deliveryDate = new Date(response.SlotDate);
					deliveryDate.setHours(10);
					
					var key = deliveryDate.toDateString().replace(' ', '', 'g');
					if (!deliveryDateSession[key]) {
						deliveryDateSession[key] = new Object();
						deliveryDateSession[key]['timeslots'] = new dw.util.ArrayList();
						deliveryDatesArr.add(deliveryDate);
					}
					
					var startTime = new Date(deliveryDate.toDateString() + ' ' + response.StartTime);
					var endTime = new Date(deliveryDate.toDateString() + ' ' + response.EndTime);
					if (checkForDuplicates.indexOf(key + startTime + endTime) == -1) {
						checkForDuplicates.add(key + startTime + endTime);
						deliveryDateSession[key]['timeslots'].add({
							'location1' : response.Location1,
							'location2' : response.location2,
							'mfiDSZoneLineId' : response.Zone,
							'mfiDSZipCode' : postalCode,
							'available' : response.Available.toLowerCase(),
							'startTime' : startTime,
							'endTime' : endTime,
							'slots' : Number(response.Slots)
						});
						
						var timeslotsComparator = new dw.util.PropertyComparator ('startTime', 'endTime');
						deliveryDateSession[key]['timeslots'].sort(timeslotsComparator);
						
						if (requestAtpAgain === true && response.Available.toLowerCase() == 'yes' && response.SlotDate !== null) {
							requestAtpAgain = false;
						}
					}
				}
				session.custom.deliveryDates = deliveryDateSession;
			}
			
		} while (requestAtpAgain === true && requestCount < 1); 
		
	} catch (e) {
		var error = e;
	}
	return deliveryDatesArr;
}

function getDeliveryDate(products, customerZip){
	var atpDelivery =  null;
	var requestedDate = new Date();	
	requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
	try {
		var deliveryDatesArr = new dw.util.ArrayList();
		var requestPacket = '{"InventoryType": "Delivery","ZipCode": "' + customerZip + '","RequestedDate": "' + requestedDate + '","StoreId": "","Products":' +JSON.stringify(products)+'}';
		var service = ATPInventoryServices.ATPInventoryTULO;
		var result = service.call(requestPacket);
		if (result.error != 0 || result.errorMessage != null || result.mockResult) {
			//return null;
			} else {
				var jsonResponse = JSON.parse(result.object.text);
				var checkForDuplicates = new dw.util.ArrayList();
				for (var i = 0; i < jsonResponse.length; i++ ) {
					var response = jsonResponse[i];
					if(response.Available == "YES" && response.Slots > 0)
					{
						var deliveryDate = new Date(response.SlotDate);
						deliveryDate.setHours(10);
						
						//var key = deliveryDate.toDateString().replace(' ', '', 'g');
						if (deliveryDatesArr.indexOf(deliveryDate) == -1) {
							
							deliveryDatesArr.add(deliveryDate);
						}							
						
					}
				
			}
			deliveryDatesArr.sort();
		}
	}catch (e) {
		var msg = e;
	}
		
	if(deliveryDatesArr.size() > 0)
	{
		atpDelivery = deliveryDatesArr[0];
	}
	return atpDelivery;
}
function getATPforStore(products,customerZip, storeID)
{
	var atpInventory = null;
	var requestedDate = new Date();
	//customerZip = '19406';
	
	requestedDate = (requestedDate.getMonth() + 1) + "/" + requestedDate.getDate() + "/" + requestedDate.getFullYear();
	try {
		var requestPacket = '{"InventoryType": "Store","ZipCode": "' + customerZip + '","RequestedDate": "' + requestedDate + '","StoreId": "' + storeID + '","Products": '+JSON.stringify(products)+'}';
		var service = ATPInventoryServices.ATPInventoryTULO;
		var result = service.call(requestPacket);
		if (result.error != 0 || result.errorMessage != null || result.mockResult) {
			//return null;
		} else {
			var jsonResponse = JSON.parse(result.object.text);
			var response = jsonResponse[0];
			var slotDate = null; 
			if(!empty(response) && response.SlotDate != null)
			{				
				var slotDate = new Date(response.SlotDate);
				slotDate.setHours(10);
				
			}
			atpInventory = {
							'Available':response.Available,
							'SlotDate':slotDate
							
							};
		}
	} catch (e) {
		var msg = e;
	}
	return atpInventory;
}
function preferredStoreMap()
{
	var store = null;
	store = getPreferredStore();
	var customerZip = request.getGeolocation().getPostalCode();
	if(request.httpParameterMap.zipCode && request.httpParameterMap.zipCode != null && !empty(request.httpParameterMap.zipCode.stringValue))
	{
		customerZip =  request.httpParameterMap.zipCode.stringValue;
		//session.custom.customerZip = customerZip;
	}
	/*else if(!empty(session.custom.customerZip))
	{
		customerZip = session.custom.customerZip;
	}*/
	var isStockAvailable = false;
	var Product = app.getModel('Product');
	var product = null;
	var productID =  null;
	if(!empty(request.httpParameterMap.productID))
	{
		productID =  request.httpParameterMap.productID.stringValue;
		product = Product.get(productID);
	}
	
	var storeCont = require('int_tulo/cartridge/controllers/Stores'); 
	var ProductUtils = require('app_storefront_core/cartridge/scripts/product/ProductUtils.js');
	var masterId = product.object.isVariant() ? product.object.masterProduct.ID : product.object.ID;
	var tuloComfortName = null;
	var availableComforts =null;
	var productsAvailable = null;
	
	if (masterId != dw.system.Site.getCurrent().getCustomPreferenceValue('tuloGeneralProductID')) {
		tuloComfortName =  ProductUtils.getProductComfortName(masterId);		
	}	
	
	if(store !== null)
	{
		productsAvailable = storeCont.CheckStoreInventory(store.ID);
		if (productsAvailable.length > 0) {
			availableComforts = productsAvailable.toString(); 	
			if (tuloComfortName == null) {
				isStockAvailable = true;			
			}
			else 
			{
				isStockAvailable = productsAvailable.indexOf(tuloComfortName) > -1 ? true : false;
			}
		}
		
	}

	if(!isStockAvailable && !empty(customerZip) && customerZip != null)
	{
		var countryCode = Countries.getCurrent({
			CurrentRequest: {
					locale: request.locale
				}
			}).countryCode;
		var distanceUnit = (countryCode == 'US') ? 'mi' : 'km';
		var maxDistance = 100; // default 100 miles radius
		var storeMap = StoreMgr.searchStoresByPostalCode(countryCode, customerZip, distanceUnit, maxDistance);
		var storesSet = storeMap.keySet(); 		
			
    	// iterate store to check inventory distance 
    	for(var i=0; i< storesSet.length; i++) {
    		productsAvailable = storeCont.CheckStoreInventory(storesSet[i].ID);    		
    		if (productsAvailable.length > 0) {    			
    			availableComforts = productsAvailable.toString(); 	
    			if (tuloComfortName != null) {
    				isStockAvailable = productsAvailable.indexOf(tuloComfortName) > -1 ? true : false;
    				if (isStockAvailable) {
    					store = storesSet[i];
    					break;
					}					
				}
				else 
				{
					isStockAvailable = true;
					store = storesSet[i];
					break;
				}
			}    		
    	}
	}

	if (!isStockAvailable) {
		store =null;
		availableComforts =null;
	}
	else
	{
		 var tmp1 = availableComforts.substring(0, availableComforts.lastIndexOf(",")).replace(/,/g, ', ');
    	 var tmp2 = availableComforts.substring(availableComforts.lastIndexOf(",")).replace(/,/g, ' and ');
    	 availableComforts = tmp1 + tmp2;
	}
	
	
	var atpInventory = null;
	var atpDelivery =  null;
	/*var products = '[{"ProductId": "' + productID + '","Quantity": 1}]';
    if(store != null)
    {
    	// check ATP inventory
    	atpInventory = getATPforStore(products,customerZip, store.ID)
    	//get ATP delivery
    	atpDelivery = getDeliveryDate(products, customerZip);
    	
    }*/
	
	if(isStockAvailable)
	{
		app.getView({
			nozipcode:  false,
			store: store,
			AvailableComforts: availableComforts,
			Product:product != null?product.object:null,
			ATPInventory:atpInventory,
			ATPDelivery:atpDelivery
		}).render('product/components/preferredStoreMap');
	}
	else if(customerZip == null || empty(customerZip))
	{
		app.getView({
			nozipcode:  true,
			store: null
		
		}).render('product/components/preferredStoreMap');
	}
	else
	{
		app.getView({
			nozipcode:  false,
			store: store,
			AvailableComforts: availableComforts,
			Product:product != null?product.object:null,
			ATPInventory:atpInventory,
			ATPDelivery:atpDelivery
		}).render('product/components/preferredStoreMap');
	}
	
}
function getATPDataForCart()
{
	var atpInventory = null;
	var atpDelivery =  null;
	var cart = app.getModel('Cart').get();
	var products = new Array();
	if(cart != null)
	{
		var plis = cart.object.productLineItems;
		if(plis.length > 0)
		{
			for (var i = 0; i < plis.length; i++ ) {
				var pli = plis[i];
				if (!empty(pli.product.custom.shippingInformation) && pli.product.custom.shippingInformation.toLowerCase() == 'core') {
					products.push({
						'ProductId' : pli.productID,
						'Quantity' : pli.quantityValue
					});
					var optionLineItems = pli.optionProductLineItems;
					for (var j = 0; j < optionLineItems.length; j++) {
						var oli = optionLineItems[j];
						if (oli.productID != 'none') {
							products.push({
								'ProductId' : oli.productID,
								'Quantity' : oli.quantityValue
							});
						}
					}
				}
			}
			var geolocationZip = request.getGeolocation().getPostalCode();	   
			var customerZip = empty(session.custom.customerZip) ? geolocationZip : session.custom.customerZip;
			if(products.length > 0) {
				atpDelivery = getDeliveryDate(products, customerZip);
				if (empty(atpDelivery)) {
					session.custom.deliveryDate = '';
					session.custom.deliveryTime = '';
				}
			}
		}
	}	
	
	app.getView({
		'ATPDelivery' : atpDelivery,
		'ATPInventory' : atpInventory
	}).render('checkout/cart/atp-delivery-info-cart');
	
}
function redCarpetLookup()
{
	var CObjectMgr = require('dw/object/CustomObjectMgr');
	var result =  false;
	var params = request.httpParameterMap;
	var zipCode = params.zipCode.stringValue;
	if(!empty(zipCode))
	{
		var zipInfo = CObjectMgr.getCustomObject("ZipInfo", zipCode);
		if(zipInfo && !empty(zipInfo) && zipInfo != null && zipInfo.custom.isRedCarpetZipCode ==  true)
		{
			result =  true;
		}
	}	
	 app.getView({
	        JSONData: result
	   }).render('util/output');
}

/*
* Web exposed methods
*/
/** Starting point for the single shipping scenario.
 * @see module:controllers/COShipping~start */
exports.Start = guard.ensure(['get'], start);
exports.StartTulo = guard.ensure(['get'], startTulo);
exports.FindInStore = guard.ensure(['get'], findInStore);
exports.HandleForm = guard.ensure(['post'], handleForm);
exports.HandleFormTulo = guard.ensure(['post'], handleFormTulo);
exports.GetPreferredStore = getPreferredStore;
exports.SetPreferredStore = guard.ensure(['post'], setPreferredStore);
exports.GetPreferredStoreAddress = guard.ensure(['get'], getPreferredStoreAddress);
exports.UpdateBopisOption = guard.ensure(['post'], updateBopisOption);
exports.GetNearbyStores = guard.ensure(['get'], getNearbyStores);
exports.GetAvailablePickupStore = guard.ensure(['get'], getAvailablePickupStore);
exports.EvaluateCartForBopis = evaluateCartForBopis;
exports.ChangeDeliveryToShipAddress = guard.ensure(['https', 'post'], changeDeliveryToShipAddress);
exports.GetATPDeliveryDates = getATPDeliveryDates;
exports.GetATPDeliveryDatesForNextWeek = guard.ensure(['get'], getATPDeliveryDatesForNextWeek);
exports.PreferredStoreMap = guard.ensure(['get'], preferredStoreMap);
exports.GetATPDataForCart = guard.ensure(['get'], getATPDataForCart);
exports.GetDeliveryZone = getDeliveryZone;
exports.RedCarpetLookup = guard.ensure(['post'], redCarpetLookup);
