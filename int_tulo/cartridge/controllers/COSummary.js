'use strict';

/**
 * This controller implements the last step of the checkout. A successful handling
 * of billing address and payment method selection leads to this controller. It
 * provides the customer with a last overview of the basket prior to confirm the
 * final order creation.
 *
 * @module controllers/COSummary
 */

/* API Includes */
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');

var mattressPipeletHelper = require('int_mattressc/cartridge/scripts/mattress/util/MattressPipeletsHelper').MattressPipeletHelper;
//Include Commerce Link Module
var CommerceLinkFactory = require('int_commercelink/cartridge/scripts/utils/CommerceLinkFactory');

var COBilling = require('~/cartridge/controllers/COBilling');
var Cart = app.getModel('Cart');

/**
 * Renders the summary page prior to order creation.
 */
function start() {
    var cart = Cart.get();

    // Checks whether all payment methods are still applicable. Recalculates all existing non-gift certificate payment
    // instrument totals according to redeemed gift certificates or additional discounts granted through coupon
    // redemptions on this page.
    if (!COBilling.ValidatePayment(cart)) {
        COBilling.Start();
        return;
    } else {

        Transaction.wrap(function () {
            cart.calculate();
        });

        Transaction.wrap(function () {
            if (!cart.calculatePaymentTransactionTotal()) {
                COBilling.Start();
            }
        });

        var pageMeta = require('app_storefront_controllers/cartridge/scripts/meta');
        pageMeta.update({pageTitle: Resource.msg('summary.meta.pagetitle', 'checkout', 'SiteGenesis Checkout')});
        app.getView({
            Basket: cart.object
        }).render('checkout/summary/summary');
    }
}

/**
 * This function is called when the "Place Order" action is triggered by the
 * customer.
 */
function submit() {
    // Calls the COPlaceOrder controller that does the place order action and any payment authorization.
    // COPlaceOrder returns a JSON object with an order_created key and a boolean value if the order was created successfully.
    // If the order creation failed, it returns a JSON object with an error key and a boolean value.
	var cart = Cart.get();
	if(!empty(cart.getPaymentInstruments('PayPal'))){
		var COBillingController = require('int_tulo/cartridge/controllers/COBilling');
        COBillingController.ResetPaymentForms();
        app.getForm(session.forms.billing.billingAddress.addressFields).copyTo(cart.getBillingAddress());
        app.getForm(session.forms.billing.billingAddress.addressFields.states).copyTo(cart.getBillingAddress());        
    }
	
	var COPlaceOrder = require('~/cartridge/controllers/COPlaceOrder');
	var placeOrderResult = COPlaceOrder.Start();
    if (placeOrderResult.error) {        
        if (!COBilling.ValidatePayment(cart)) {
            COBilling.Start();
            return;
        } else {

            Transaction.wrap(function () {
                cart.calculate();
            });

            app.getView({
                Basket: cart.object,
                PlaceOrderError: placeOrderResult.PlaceOrderError
            }).render('checkout/billing/billing');
        }
    } else if (placeOrderResult.order_created) {
    	//Real time Order Creation
    	var clOrderCreationStatus = dw.system.HookMgr.callHook(CommerceLinkFactory.HOOKS.ORDER.CREATE.ID, CommerceLinkFactory.HOOKS.ORDER.CREATE.SCRIPT, placeOrderResult.Order);
        var createOrderResponseDetails = clOrderCreationStatus.getDetail(CommerceLinkFactory.STATUS_CODES.RESPONSE_OBJECT);
        
        showConfirmation(placeOrderResult.Order);
    }
}

/**
 * Renders the order confirmation page after successful order
 * creation. If a nonregistered customer has checked out, the confirmation page
 * provides a "Create Account" form. This function handles the
 * account creation.
 */
function showConfirmation(order) {
    if (!customer.authenticated) {
        // Initializes the account creation form for guest checkouts by populating the first and last name with the
        // used billing address.
        var customerForm = app.getForm('profile.customer');
        customerForm.setValue('firstname', order.billingAddress.firstName);
        customerForm.setValue('lastname', order.billingAddress.lastName);
        customerForm.setValue('email', order.customerEmail);
        customerForm.setValue('emailconfirm', order.customerEmail);
        customerForm.setValue('orderNo', order.orderNo);     
    }
    app.getForm('storepicker').clearFormElement();
    session.custom.deliveryDate = null;
	session.custom.deliveryTime = null;
	session.custom.preferredStore = null;
	session.custom.SynchronyOrderNo = null;
    app.getForm('profile.login.passwordconfirm').clear();
    app.getForm('profile.login.password').clear();

    var pageMeta = require('app_storefront_controllers/cartridge/scripts/meta');
    pageMeta.update({pageTitle: Resource.msg('confirmation.meta.pagetitle', 'checkout', 'SiteGenesis Checkout Confirmation')});
    var isOnlyRedCarpetItemsInCart = mattressPipeletHelper.getOrderIsRedCarpetStatus(order);
    app.getView({
        Order: order,
        IsOnlyRedCarpetItemsInCart: isOnlyRedCarpetItemsInCart,
        ContinueURL: URLUtils.https('Account-RegistrationForm') // needed by registration form after anonymous checkouts
    }).render('checkout/confirmation/confirmation');
}

/*
 * Module exports
 */

/*
 * Web exposed methods
 */
/** @see module:controllers/COSummary~Start */
exports.Start = guard.ensure(['https'], start);
/** @see module:controllers/COSummary~Submit */
exports.Submit = guard.ensure(['https', 'post'], submit);
/** @see module:controllers/COSummary~SubmitFromPayPal 
 * this method called only from paypal to place order.
 * */
exports.SubmitFromPayPal = guard.ensure(['https'], submit);

/*
 * Local method
 */
exports.ShowConfirmation = showConfirmation;
