'use strict';

/**
 * Controller for Geo Location Dialog box
 *
 * @module controllers/GeoLocation
 */

var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');

/**
 * Prepare the data for Geo Location div 
 */
function start() {
	
	var geoLocEnabled = dw.system.Site.getCurrent().getPreferences().getCustom()["modalGeoLocEnabled"]; ; 
	var geoLocCookieNumOfDays = dw.system.Site.getCurrent().getPreferences().getCustom()["modalGeoLocCookieExpiration"];   
	var geoLoc = request.getGeolocation();
	var userCountry;
	var userCountryName;
	var countryCheck = false;  // Default is false, true for allowed countries (FR)
		
	if (geoLoc != null){
		 userCountry = geoLoc.getCountryCode(); 
		 userCountryName = geoLoc.getCountryName(); 
	}
	
	if (geoLocEnabled && userCountry != null && (userCountry == 'FR' || userCountry == 'BE' || userCountry == 'LU') ){
			countryCheck = true;
	}
	
    app.getView({
		CountryCheck : countryCheck,
		UserCountry : userCountry,
		UserCountryName: userCountryName,
		GeoLocCookieNumOfDays : geoLocCookieNumOfDays
    }).render('geolocation/geolocationmodal');
}
/**
 * Content rendered on the Country selection dialog box
 */
function showView() {
	var parameterMap = request.httpParameterMap;  

	app.getView({
		UserCountry : parameterMap.userCountry.stringValue,
		UserCountryName : parameterMap.userCountryName.stringValue
	}).render('geolocation/sfgeolocmodalform');
}
/*
 * Export the publicly available controller methods
 */
/** Renders the hidden div for Geo Location details
 * @see module:controllers/GeoLocation~Start */
exports.Start = guard.ensure(['include'], start);
/** Content rendered on the Country selection dialog box
 * @see module:controllers/GeoLocation~ShowView */
exports.ShowView = guard.ensure(['get'], showView);