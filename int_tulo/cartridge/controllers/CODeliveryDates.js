'use strict';
/**
 * Controller to get delivery dates.
 *
 * @module controllers/CODeliveryDates
 */

/* include Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var StorePicker = require('~/cartridge/controllers/StorePicker');


function start() {
	
	var deliveryDatesArr;
    var cart = app.getModel('Cart').get();	
    var geolocationZip = request.getGeolocation().getPostalCode();
	var customerZip = empty(session.custom.customerZip) ? geolocationZip : session.custom.customerZip;
	var postalCode = (empty(session.forms.storepicker.postalCode.value)) ? customerZip : session.forms.storepicker.postalCode.value;
	var params = request.httpParameterMap;	
	var format = params.format.stringValue;
	var coStep = params.coStep.stringValue;
	var phone = params.phone.stringValue;
	session.forms.storepicker.postalCode.value = postalCode;
	
	if (coStep == '2') {		
		postalCode = params.zipCode.stringValue;		
	}
	
	deliveryDatesArr = StorePicker.GetATPDeliveryDates(cart, postalCode);
	
	app.getView({
		DeliveryDatesArr:deliveryDatesArr,
		PostalCode: postalCode, 
		COStep: coStep,
		Phone: phone
	}).render('checkout/components/delivery-schedule-mocked-atp.isml');
}

function getNextWeekDeliveryDates() {
	
	var deliveryDatesArr;
    var cart = app.getModel('Cart').get();	
    var geolocationZip = request.getGeolocation().getPostalCode();
	var customerZip = empty(session.custom.customerZip) ? geolocationZip : session.custom.customerZip;
	var postalCode = (empty(session.forms.storepicker.postalCode.value)) ? customerZip : session.forms.storepicker.postalCode.value;
	
	session.forms.storepicker.postalCode.value = postalCode;
	
	var params = request.httpParameterMap;	
	var format = params.format.stringValue;
	var currentWeekDate = params.currentWeekDate.stringValue;
	
	deliveryDatesArr = StorePicker.GetATPDeliveryDates(cart, postalCode, currentWeekDate);
	
	app.getView({
		DeliveryDatesArr:deliveryDatesArr,
		PostalCode: postalCode,
	}).render('checkout/components/delivery-weekly-schedule-mocked');
}

function updateDeliveryDates() {

	var params = request.httpParameterMap;	
	var format = params.format.stringValue;
	var deiveryDate = params.deliveryDate.stringValue;
	var deliveryTime = params.deliveryTime.stringValue;
	var deliveryZone = params.deliveryZone.stringValue;
	var zipCode = empty(params.zipCode.stringValue) ? session.custom.customerZip : params.zipCode.stringValue;
	session.custom.deliveryDate =  deiveryDate;
	session.custom.deliveryTime = deliveryTime;
	session.custom.customerZip = zipCode;
	session.custom.deliveryZone = deliveryZone;

}

/*
* Web exposed methods
*/

exports.Start = guard.ensure(['get'], start);
exports.GetNextWeekDeliveryDates = guard.ensure(['get'], getNextWeekDeliveryDates);
exports.UpdateDeliveryDates = guard.ensure(['post'], updateDeliveryDates);
