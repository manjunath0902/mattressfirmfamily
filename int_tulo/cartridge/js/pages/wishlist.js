'use strict';

var addProductToCart = require('./product/addToCart'),
	page = require('../page'),
	util = require('../util');

var progress = require('../progress');
/**
 * @private
 * @function
 * @description Searches the input form and displays its relevant information
 */
function inStoreWishListHandler() {

	$('#quotation_search_form').on('submit', function (e) {
		e.preventDefault();
		if($(this).find('[name=quoteId]').val() == ""){
			return false;
		}
		// override form submission in order to prevent refresh issues
		progress.show($('#quotation_search_form'));
		var data = $(this).serialize(); 
		$.ajax({
			type: 'GET',
			url: $(this).attr('action'),
			data: data
		})
		.fail(function(jqXHR, textStatus, error){
			progress.hide($('#quotation_search_form'));
		  if(jqXHR.status == 403){
			  $("#quotation_search_form").show();
			  if(jqXHR.responseText != null){
				  $("#quotation_search_content_holder").html(jqXHR.responseText);
				  $("#quotation_search_content_holder").removeClass('hidden').show();
			  }
		  }
		})
		.done(function (res) {
			progress.hide($('#quotation_search_form'));
			$("#quotation_search_form").hide();
			$("#quotation_search_content_holder").html(res);
			$("#quotation_search_content_holder").removeClass('hidden').show();
		});
	});
}

exports.init = function () {
	
	inStoreWishListHandler();
	
	addProductToCart();
	$('#editAddress').on('change', function () {
		page.redirect(util.appendParamToURL(Urls.wishlistAddress, 'AddressID', $(this).val()));
	});

	//add js logic to remove the , from the qty feild to pass regex expression on client side
	$('.option-quantity-desired input').on('focusout', function () {
		$(this).val($(this).val().replace(',', ''));
	});

	$("#copy-input").click(function(){
		if (navigator.userAgent.match(/ipad|ipod|iphone/i)) {
		  var el = this;
		  var editable = el.contentEditable;
		  var readOnly = el.readOnly;
		  el.contentEditable = true;
		  el.readOnly = false;
		  var range = document.createRange();
		  range.selectNodeContents(el);
		  var sel = window.getSelection();
		  sel.removeAllRanges();
		  sel.addRange(range);
		  el.setSelectionRange(0, 999999);
		  el.contentEditable = editable;
		  el.readOnly = readOnly;
		}else{
			this.select();
		} 
		
		
		document.execCommand('copy');
		$(this).blur();
		$(this).parent().addClass('text-copied');
	}); 
	$("#wishlist-table").find('.ui-spinner-button').on('click', function (e) { 
    	 var quantityVal = $(this).closest('div.input-spinner').find('.ui-spinner-input').attr('aria-valuenow');
    	 var quantityOldVal = $(this).closest('div.input-spinner').find('.ui-spinner-input').attr('value');
    	 var priceString =  $(this).closest('div.prod-row').find('.price-sales').data('priceval');
    	if(quantityVal == 1 )
    	{
    		 var newHtml = "$"+priceString;
    		 priceString = priceString.toString();
    		 if(priceString.indexOf(".") == -1)
    		 {
        		 newHtml +=  ".00";
    		 }
    		$(this).closest('div.prod-row').find('.price-sales').html(newHtml) 
    	}
    	else if(quantityVal != quantityOldVal)
    	 {    		
        	 quantityVal = quantityVal*1;
        	 priceString = priceString*1;
        	 var finalVal = quantityVal*priceString;
        	 var finalValString = finalVal.toString();
        	 var newHtml = "$"+priceString+" x "+quantityVal+" = $"+finalVal;
        	 if(finalValString.indexOf(".") == -1)
    		 {
        		 newHtml +=  ".00";
    		 }
        	 $(this).closest('div.prod-row').find('.price-sales').html(newHtml);
    	 }
    	 
  
    });
};

