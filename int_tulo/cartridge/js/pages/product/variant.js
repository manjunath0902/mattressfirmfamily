'use strict';

var amplience = require('../../amplience'),
	ajax = require('../../ajax'),
	image = require('./image'),
	progress = require('../../progress'),
	productStoreInventory = require('../../storeinventory/product'),
	tooltip = require('../../tooltip'),
	util = require('../../util'),
	customjs = require('../../custom'),
	smoothscroll = require('../../smoothscroll');
var map = null;

/**
 * @description update product content with new variant from href, load new content to #product-content panel
 * @param {String} href - url of the new product variant
 **/
var updateContent = function (href, ampset) {
	if (ampset == null) {
		var ampset = '';
		}
	var $pdpForm = $('.pdpForm');
	var qty = $pdpForm.find('input[name="Quantity"]').first().val();
	var params = {
		Quantity: isNaN(qty) ? '1' : qty,
		format: 'ajax',
		productlistid: $pdpForm.find('input[name="productlistid"]').first().val()
	};
	var ratingStars  = "";
	if( $(".custom-reviews-holder-content").length > 0)
	{
		ratingStars = $(".custom-reviews-holder-content").html();
	}	
	//progress.show($('#pdpMain'));
	ajax.load({
		url: util.appendParamsToUrl(href, params),
		target: $('#product-content'),
		callback: function () {
			if( $(".custom-reviews-holder-content").length > 0)
			{
				$(".custom-reviews-holder-content").html(ratingStars);
			}	
			if( $(".custom-reviews-holder-topbar").length > 0)
			{
				$(".custom-reviews-holder-topbar").html(ratingStars);
			}	
			if (SitePreferences.STORE_PICKUP) {
				productStoreInventory.init();
			}
			if (ampset != '') {
				image.replaceImages();
				amplience.initZoomViewer(ampset);
			}
			tooltip.init();
			util.uniform();
			updateLabel();			
			$('.add-to-cart-disabled').on('click', showComfirmSelection);
			selectSize();
			showTooltip();
			openSyncTooltip();
			customjs.initPdpDialog();
			customjs.initMattressPDPBuyFixed();
			 $("#add-to-cart").removeClass('disabled-btn');  
			//calling general average bazaarvoice script
			/*if(typeof showAverageBazaarVoice != 'undefined' && showAverageBazaarVoice){
				bootstrapGeneralProductReviews();
			}*/
		}
	});
};
/**
* @description update product content with new variant from href, load new content to #primary panel
* @param {String} href - url of the new product variant
**/
var updateCompleteContent = function (pid, href, ampset) {
	if (ampset == null) {
		var ampset = '';
		}
	var $pdpForm = $('.pdpForm');
	var qty = $pdpForm.find('input[name="Quantity"]').first().val();
	var params = {
		Quantity: isNaN(qty) ? '1' : qty,
		format: 'ajax',
		productlistid: $pdpForm.find('input[name="productlistid"]').first().val()
	};

	//progress.show($('#pdpMain'));
	var historyURL = href;
	var ajaxURL = href;	
	//to retain size selection on comfort load
	var variationsData = $("#product-variations").data('attributes');
	if(variationsData)
	{
		if(variationsData.size && variationsData.size != null && variationsData.size.value && variationsData.size.value != "")
		{
			var selectedSize = variationsData.size.value;
			ajaxURL = util.appendParamToURL(ajaxURL,"dwvar_"+pid+"_size",selectedSize );
		}
		
	}
	ajax.load({
		url: util.appendParamsToUrl(ajaxURL, params),
		target: $('#primary'),
		callback: function () {
			if (SitePreferences.STORE_PICKUP) {
				productStoreInventory.init();
			}
			if (ampset != '') {
				image.replaceImages();
				amplience.initZoomViewer(ampset);
			}
			history.pushState({}, null, historyURL);
			var title = $("#pagemetadata").data("metatitle");
			document.title = title;
			customjs.init();
			amplience.initZoomViewer();		
			tooltip.init();
			util.uniform();
			updateLabel();
			$('.add-to-cart-disabled').on('click', showComfirmSelection);
			selectSize();
			showTooltip();
			openSyncTooltip();
			selectab();		
			customjs.initSliderPDP();
			customjs.initPdpDialog();
			customjs.initMattressPDPBuyFixed();
			loadPreferredMap();
		 	if(typeof utag_data !=  'undefined'){
		 		 utag.link(utag_data);
			 }
		 	$("#add-to-cart").removeClass('disabled-btn');
		}
	});
};


var updateLabel = function () {
	$('.product-variations .attribute').each(function (e) {
		var attributeTitle = $(this).find('.swatches').find(':selected').attr('title');
		if ($(this).find('.selected-attr-value').length == 0 && attributeTitle != '') {
			var attributeValue = '<span class="selected-attr-value">' + attributeTitle + "</span>";
			$(attributeValue).appendTo($(this).find('.label'));
		}
	});
}

var showComfirmSelection = function(e){
	e.preventDefault();
	$('.add-to-cart-disabled').closest('div.add-to-cart-area').addClass('comfort-not-select');
	e.stopPropagation();
};
var selectSize = function () {
	  // show selected values
	  $( ".selectedValue" ).each(function() {
		  var $value =  $(this).closest('.size-select-area').find('li.selected').attr('title');
		  if($value != null && $value != undefined)
	       {
			  $(this).val($value);  
	       }		  
		   	 
	  });	
	$('.size-select-area .fake-input').unbind("click");
	$('.size-select-area .fake-input').click(function(e){
		var $target =  $(this).closest('.size-select-area');
		var hasClass =  $target.hasClass('active-drop');
	    $( ".size-select-area").removeClass('active-drop');
	    if(hasClass)
	    {
	    	$target.removeClass('active-drop');
	    }
	    else
	    {
	    	$target.addClass('active-drop');
	    }
	    e.stopPropagation();
	});
	
	$(document).on("click", function(e) {
		// hide size/comfort tool tip on click outside
	    if ($(e.target).is(".size-select-area") === false) {
	    	$(".size-select-area").removeClass('active-drop');
	    }
	 // hide comfort tool tip on click outside of button
	  //  if ($(e.target).is(".add-to-cart-container") === false) {
	   // 	$(".add-to-cart-area").removeClass('comfort-not-select');
	  //  }
	 });
	$(document).mouseup(function(e) {
		    var hidedrop = $(".add-to-cart-container");
		    // if the target of the click isn't the container nor a descendant of the container
		    if (!hidedrop.is(e.target) && hidedrop.has(e.target).length === 0) 
		    {
		        $(".add-to-cart-area").removeClass('comfort-not-select');
		    }
	});
	$(".variation-select").off("click", "li.select-item");
	$(".variation-select").on("click", "li.select-item", function(e){
	    e.preventDefault();
	    if($(this).hasClass('selected') || $(this).hasClass('out-of-stock')){ $(".size-select-area").removeClass('active-drop');e.stopPropagation();return;}	
	    $(this).addClass("selected").siblings().removeClass("selected");
	    $(this).closest('.size-select-area').find(".selectedValue").val($(this).attr('title'));
	    $(this).closest('.size-select-area').removeClass('active-drop');
	    $(".tooltip-part").removeClass('active-tooltip');
	});
	
};

var selectab = function () {
	$( "#tabValue" ).each(function() {
		  var $value =  $(this).closest('.select-tabs').find('li.selected').attr('title');
	});
	$('.select-tabs .fake-input').click(function(e){
	    $(this).closest('.select-tabs').toggleClass('active-drop');
	    e.stopPropagation();

	});
	$(document).on("click", function(e) {
	    if ($(e.target).is(".select-tabs") === false) {
	    	$(".select-tabs").removeClass('active-drop');
	    }
	 });
	$(".links-tabs").on("click", "li", function(e){
	    e.preventDefault();
	    $(this).addClass("selected").siblings().removeClass("selected");
	    $(this).closest('.select-tabs').find("#tabValue").val($(this).attr('title'));
	    $(".select-tabs").removeClass('active-drop');
	});
	
};

var showTooltip = function () {
	$('.tooltip-part').mouseover(function(){
		$(this).closest('.tooltip-part').addClass('active-tooltip');
	});
	$('.tooltip-part').mouseleave(function(){
		$(this).closest('.tooltip-part').removeClass('active-tooltip');
	});
	$(".tooltip-part").on("click", "a.not-sure", function(e){
	   // e.preventDefault();
	    $(".tooltip-part").removeClass('active-tooltip');
	});
};
var openSyncTooltip = function () {
	$(".tooltip-sync-opener").click(function(){
		$(this).closest('.tooltip-sync').toggleClass('active-tooltip');
		if ($(window).width() < 768) {
			$('html').css("overflow", "hidden");
		}
	});
	$("#wrapper").click( function(event){
    if ( !$(event.target).closest('.tooltip-sync').length ) {
      $('.tooltip-sync').removeClass('active-tooltip');
      $('html').css("overflow", "visible");
    }
	});
	$(".tooltip-overlay").click( function(event){
    if ( !$(event.target).closest('.tooltip-sync .tooltip-holder').length ) {
      $('.tooltip-sync').removeClass('active-tooltip');
      $('html').css("overflow", "visible");
    }
	});
	$(".setpay-close").click( function(event){
      $('.tooltip-sync').removeClass('active-tooltip');
      $('html').css("overflow", "visible");
	});
};
var updateTuloSet = function (href) {	
	var params = {		
		format: 'ajax'		
	};
	
	var ajaxURL = href;	
	var setRating =  $("#pdp-set-rating").html();
	ajax.load({
		url: util.appendParamsToUrl(ajaxURL, params),
		target: $('#tulo-set-block'),
		callback: function () {	
			 $("#pdp-set-rating").html(setRating);
			//util.uniform();		
			selectSize();
			showTooltip();
			openSyncTooltip();	
		}
	});
};

var updatePillowInSet = function (href) {	
	
	var params = {			
			format: 'ajax'
		};
	
	var ajaxURL = util.appendParamsToUrl(href, params);	
	$.get( ajaxURL)
	.done(function( response ){
		if(response != null )
		{
			var pillowVariantID = response.pillowVariantID;
			var pillowVariantName = response.pillowVariantName;
			var pillowVariantManufacturerSKU = response.pillowVariantManufacturerSKU;
			var pillowVariantExternalId = response.pillowVariantExternalId;
			$("#pillowId").val(pillowVariantID);
			$('#tulo-set-pillow .ts-thumb .title').attr('pr1id',pillowVariantID);
			$('#tulo-set-pillow .ts-thumb .title').attr('pr1nm',pillowVariantName);
			$('#tulo-set-pillow .ts-thumb .title').attr('pr1va',pillowVariantManufacturerSKU);
			$('#tulo-set-pillow .ts-thumb .title').attr('pr1ca',pillowVariantExternalId);
			updateTuloSetPricing();
		}
		
	});	
	
};
var resetPillowInSet =  function(){
	var pillowMasterID =  $(".pillow-row").data('pillowmasterid');
	if(pillowMasterID != null && pillowMasterID != "")
	{
		// reset comfort
		 $( "li.li-comfort-pillow-inset" ).each(function() {
			if($(this).data('pid') == pillowMasterID)
			{
				$(this).addClass('selected').siblings().removeClass("selected");
				$("#selectedPillowComfortValue").val($(this).attr('title'));
				
			}
			   	 
		  });
		 // reset quantity drop down
		 $( "li.li-select-qty" ).each(function() {
			if($(this).data('value') == '1')
			{
				$(this).addClass('selected').siblings().removeClass("selected");
				var defaultVal =$("#pillowQty").data('defaultval');
				$("#pillowQty").val(defaultVal);
				
			}
			   	 
		  });	
	}		
	
};
var updateTuloSetPricing =  function(){		
	var $form =  $('#frmTuloSet');
	var ajaxdest = Urls.updateTuloSetPricing;
	var formData = $form.serialize();	
	$.post( ajaxdest, formData )
	.done(function( text ){
		var $response = $( text );
		$("#tulo-set-price").html($response);
		
		
	});	
	
};
var updateButtonLabel =  function(){		
	var $form = $("#frmTuloSet");
	var frameId = $form.find('#frameId').val();
	var pillowId = $form.find('#pillowId').val();
	var label = Resources.ADD_TO_CART ;
	if(frameId != "" && frameId != null && pillowId != "" && pillowId != null)
	{
		label = Resources.ADD_COMPLETE_SET ;
	}
	else if(pillowId != "" && pillowId != null)
	{
		label = Resources.ADD_TULO_PILLOW ;
	}
	else if(frameId != "" && frameId != null)
	{
		label = Resources.ADD_TULO_BASE ;
	}
	$form.find(".add-to-cart-tuloset").html(label);
	$form.find(".add-to-cart-tuloset").val(label);
};
module.exports = function () {
	smoothscroll.InitAnchors();
	var $pdpMain = $('.primary-content-pdp');
	// hover on swatch - should update main image with swatch image
	$pdpMain.on('mouseenter mouseleave', '.swatchanchor', function () {
		var largeImg = $(this).data('lgimg'),
			$imgZoom = $pdpMain.find('.main-image'),
			$mainImage = $pdpMain.find('.primary-image');

		if (!largeImg) { return; }
		// store the old data from main image for mouseleave handler
		$(this).data('lgimg', {
			hires: $imgZoom.attr('href'),
			url: $mainImage.attr('src'),
			alt: $mainImage.attr('alt'),
			title: $mainImage.attr('title')
		});
		// set the main image
		image.setMainImage(largeImg);
	});

	// click on swatch - should replace product content with new comfort
	$pdpMain.on('change', '.swatches', function (e) {
		
		updateCompleteContent($(this).data('pid'), $(this).val(), "");
	});
	$pdpMain.on('click', 'input[name="comfort_swatches1"]', function (e) {
		if($(this).attr('checked') == "checked")
		{		
			$(".tooltip-part").removeClass('active-tooltip');			
			e.stopPropagation();
			return;
		}
	});
	// click on swatch - should replace product content with new comfort -  selected from dropdown
	$pdpMain.on('click', '.li-comfort-select', function (e) {
		if ($(this).data('url').length === 0) { 
			return; 
		}
		updateCompleteContent($(this).data('pid'), $(this).data('url'), "");

	});
	// change drop down variation attribute - should replace product content with new variant
	$pdpMain.on('click', '.li-select', function (e) {	
		if( (!$(this).hasClass('select-item') && $(this).hasClass('selected')) || $(this).hasClass('out-of-stock') || $(this).data('url').length === 0)
		{
			$(".tooltip-part").removeClass('active-tooltip');	
			e.stopPropagation();
			return;
		}
		updateContent($(this).data('url'),"");
		
		// UTag for Change Size
		if(typeof utag_data !=  'undefined' && typeof utag != undefined && utag != null ){
	 		
			var prodNameContainer = $("div#product-content").find(".product-name-container");
			
			if(prodNameContainer.length > 0 ){
				
				var prodInfo = prodNameContainer.get(0);
				var optionProdInfo = $(".tulo-package-section").get(0);
				var prodImg = $("img.amp-main-img").length > 0 ? $("img.amp-main-img").attr("src") : [""];
				var prodPrc = ( $(this).get(0).title && $(this).get(0).title.length > 0 ) ? $(this).get(0).title.substring($(this).get(0).title.lastIndexOf("$") + 1 ) : "";
				
				utag.link({ "event_type": "change_size", "product_quantity": [ "1" ], 
					"product_name": 	(prodInfo.attributes['pr1nm'])? prodInfo.attributes['pr1nm'].value.toString().split() : [""], 
					"product_id": 		(prodInfo.attributes['pr1id'])? prodInfo.attributes['pr1id'].value.toString().split() : [""], 
					"product_sku": 		(prodInfo.attributes['pr1ca'])? prodInfo.attributes['pr1ca'].value.toString().split() : [""], 
					"product_brand": 	[ "Tulo" ], 
					"product_unit_price": prodPrc.split(), 
					"product_size": 	  $(this).data('value').split(),
					"product_url": 		window.location.href,
					"product_img_url":  prodImg,
					"option_product_name": 		(optionProdInfo && optionProdInfo.attributes['pr1nm'])? optionProdInfo.attributes['pr1nm'].value.toString().split(): [""],
					"option_product_price": 	(optionProdInfo && optionProdInfo.attributes['pr1pr'])? optionProdInfo.attributes['pr1pr'].value.toString().split(): [""],
					"option_product_value_id":  (optionProdInfo && optionProdInfo.attributes['pr1id'])? optionProdInfo.attributes['pr1id'].value.toString().split(): [""],
					"product_child_sku": $('.select-item.selected').length > 0 ? $('.select-item.selected').data('manufacturersku').split(): [""] });
 		
			}
		}
	});
	
	/*Events for tulo set section */
	$pdpMain.on('change', '.swatches_mattress', function (e) {		
		
		var ajaxURL = $(this).val(); 
		var pid =  $(this).data('pid');
		//to retain size selection on comfort load
		var variationsData = $(".data-variation-attributes").data('attributes');
		if(variationsData)
		{
			if(variationsData.size && variationsData.size != null && variationsData.size.value && variationsData.size.value != "")
			{
				var selectedSize = variationsData.size.value;
				ajaxURL = util.appendParamToURL(ajaxURL,"dwvar_"+pid+"_size",selectedSize );
			}
			
		}
		updateTuloSet( ajaxURL);
	});
	$pdpMain.on('click', 'input[name="comfort_swatches_mattress"]', function (e) {
		if($(this).attr('checked') == "checked")
		{		
			$(".tooltip-part").removeClass('active-tooltip');			
			e.stopPropagation();
			return;
		}
	});
	// click on swatch - should replace product content with new comfort -  selected from dropdown
	$pdpMain.on('click', '.li-comfort-select-mattress', function (e) {
		if ($(this).data('url').length === 0) { 
			return; 
		}
		var ajaxURL = $(this).data('url'); 
		var pid =  $(this).data('pid');
		//to retain size selection on comfort load
		var variationsData = $(".data-variation-attributes").data('attributes');
		if(variationsData)
		{
			if(variationsData.size && variationsData.size != null && variationsData.size.value && variationsData.size.value != "")
			{
				var selectedSize = variationsData.size.value;
				ajaxURL = util.appendParamToURL(ajaxURL,"dwvar_"+pid+"_size",selectedSize );
			}
			
		}
		updateTuloSet( ajaxURL);
	});
	// change drop down variation attribute - should replace product content with new variant
	$pdpMain.on('click', '.li-select-mattress', function (e) {	
		if( (!$(this).hasClass('select-item') && $(this).hasClass('selected')) || $(this).hasClass('out-of-stock') || $(this).data('url').length === 0)
		{
			$(".tooltip-part").removeClass('active-tooltip');	
			e.stopPropagation();
			return;
		}
		updateTuloSet($(this).data('url'));
	});
	// change pillow comfort selection in set
	$pdpMain.on('click', '.li-comfort-pillow-inset', function (e) {
		if ($(this).data('url').length === 0) { 
			return; 
		}
		var ajaxURL = $(this).data('url'); 		
		updatePillowInSet( ajaxURL);
	});
	// change pillow quantity selection in set
	$pdpMain.on('click', '.li-select-qty', function (e) {
		var qty = $(this).data('value');
		qty = isNaN(qty) ? '1' : qty;
		$('#pillowQuantity').val(qty);
		$('#tulo-set-pillow .ts-thumb .title').attr('pr1qt',qty);
		updateTuloSetPricing();	
	});
	$pdpMain.on("click","#inlcude-pillow", function(e) {
		e.preventDefault();
		$(".pillow-row .include-pillow").hide();
		var pillowVariantID = $(".pillow-row").data('pillowid');
		$("#pillowId").val(pillowVariantID);
		$(".pillow-row .pillow-set-included").show();
		$('#tulo-set-pillow .ts-thumb .title').attr('pr1qt',1);
		updateTuloSetPricing();
		updateButtonLabel();
		
    });
	$pdpMain.on("click","#remove-pillow", function(e) {
		e.preventDefault();
		//set pillow back to default
		resetPillowInSet();
		$(".pillow-row .pillow-set-included").hide();
		$(".pillow-row .include-pillow").show();    
		$("#pillowId").val('');
		$("#pillowQuantity").val('1');
		$('#tulo-set-pillow .ts-thumb .title').attr('pr1qt',0);
		updateTuloSetPricing();
		updateButtonLabel();	
	
    });
	$pdpMain.on("click", "#inlcude-base", function(e) {
		e.preventDefault();
		$(".base-row .include-base").hide();
		var frameId = $(".base-row").data('frameid');
		$("#frameId").val(frameId);
		$(".base-row .base-set-included").show();
		$('#tulo-set-frame .ts-thumb .title').attr('pr1qt',1);
		updateTuloSetPricing();
		updateButtonLabel();
		
    });
	$pdpMain.on("click", "#remove-base", function(e) {
		e.preventDefault();
	  	$(".base-row .base-set-included").hide();
	  	$(".base-row .include-base").show(); 
	  	$("#frameId").val('');
		$('#tulo-set-frame .ts-thumb .title').attr('pr1qt',0);
	  	updateTuloSetPricing();
	  	updateButtonLabel();
    });

	
	/* END -  events for tulo set section*/
	util.uniform();
	updateLabel();
	selectSize();
	showTooltip();
	openSyncTooltip();
	selectab();
	customjs.initMattressPDPBuyFixed();
	
	$('.add-to-cart-disabled').on('click', showComfirmSelection);
	
	customjs.initSliderPDP();
	
	// Select all links with hashes
	$('a[href^="#"].go')
	  // Remove links that don't actually link to anything
	  .not('[href="#"]')
	  .not('[href="#0"]')
	  .click(function(event) {
	    // On-page links
	    if (
	      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
	      && 
	      location.hostname == this.hostname
	    ) {
	      // Figure out element to scroll to
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
	      // Does a scroll target exist?
	      if (target.length) {
	        // Only prevent default if animation is actually gonna happen
	        event.preventDefault();
	        $('html, body').animate({
	          scrollTop: target.offset().top
	        }, 1000, function() {
	          // Callback after animation
	          // Must change focus!
	          var $target = $(target);
	          $target.focus();
	          if ($target.is(":focus")) { // Checking if the target was focused
	            return false;
	          } else {
	            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
	            $target.focus(); // Set focus again
	          };
	        });
	      }
	    }
	  });
	 
	if($('.pt_product-details').length > 0 && $('.product-feature-video').length > 0){

		var fixmeTop = $('.product-feature-video').offset().top - $('.sections-links').outerHeight()  - $('#header').outerHeight();
		
		$(window).scroll(function() {                  // assign scroll event listener
	    var currentScroll = $(document).scrollTop(); // get current position
	    if (currentScroll >= fixmeTop && !$('.sections-links').hasClass('fixed')) { 
	    	// apply position: fixed if you
	        $('.sections-links').addClass("fixed").css({                      // scroll to that element or below it
	            position: 'fixed',
	            top: $('#header').outerHeight(),
	            left: '0',
	            right: '0'
	        });
	    } else if(currentScroll < fixmeTop) {                                   // apply position: static
	    	$('.sections-links').removeClass("fixed").css({position: "relative", top: "0px"});
	    }

		});
		
	
	}  

	if($('.pt_product-details').length > 0 && $('.liv-layered-mattress').length > 0){

		var fixmeTop = $('.liv-layered-mattress').offset().top - $('.sections-links-mattresspdp').outerHeight()  - $('#header').outerHeight();
		
		$(window).scroll(function() {                  // assign scroll event listener
	    var currentScroll = $(document).scrollTop(); // get current position
	    if (currentScroll >= fixmeTop && !$('.sections-links-mattresspdp').hasClass('fixed')) { 
	    	// apply position: fixed if you
	        $('.sections-links-mattresspdp').addClass("fixed").css({                      // scroll to that element or below it
	            position: 'fixed',
	            top: $('#header').outerHeight(),
	            left: '0',
	            right: '0'
	        });
	    } else if(currentScroll < fixmeTop) {                                   // apply position: static
	    	$('.sections-links-mattresspdp').removeClass("fixed").css({position: "relative", top: "0px"});
	    }

		});
		
	
	}  
	
	 $(document).ready(function(){
		 // load map area 	  	  
		 if($("#pdp-store-map").length > 0 )
		 {
			 loadPreferredMap('');
		 }
		 $("#add-to-cart").removeClass('disabled-btn');  
	  });
};



/*********************************** Custom Implementation of General PDP ******************************/


//---- Configurations ----//

// API request configuration


/*$(document).ready(function(){
	if(typeof loadGeneralReviews != 'undefined' && (loadGeneralReviews)){
		bootstrapGeneralProductReviews();
	}
});
*/
//Templates needed for Bazaar Voice
var tplReviewsListing = '<% _.each(Results, function(review) { %>\
   <% var comfort = typeof review.SecondaryRatings["Quality"] == "undefined" ? 0 : review.SecondaryRatings["Quality"].Value; %>\
   <% var quality = typeof review.SecondaryRatings["Comfort"] == "undefined" ? 0 : review.SecondaryRatings["Comfort"].Value; %>\
   <% var productValue = typeof review.SecondaryRatings["Value"] == "undefined" ? 0 : review.SecondaryRatings["Value"].Value; %>\
   <% var ratingWidth = review.Rating * 20; %>\
   <% var comfortCss = comfort * 20; %>\
   <% var qualityCss = quality * 20; %>\
   <% var productValueCss = productValue * 20; %>\
	\
  <div class="review-row">\
      <div class="customer-info">\
        <div class="customer-name"><%= review.UserNickname %> </div>\
        <div><%= review.UserLocation %> </div>\
        <div>Reviews: <strong><%= review.TotalCommentCount %></strong></div>\
        <div>Votes:  <strong><%= review.TotalFeedbackCount %></strong></div>\
        \
      </div>\
      <div class="review-detail">\
		<div class="top-info">\
	        <div><div class="stars"><%= review.Rating%> <div class="highlight-stars" style="width:<%= ratingWidth %>%"></div></div></div>\
	        <h4><%= review.Title %></h4>\
		</div>\
		<div class="info-holder">\
			<div class="review-info">\
		        <p><%= review.ReviewText %></p>\
		        <div class="recommended-row"><%= review.IsRecommended  == 1 ? "<strong class=yes> Yes, </strong> I recommend this product." : "<strong class=no> No, </strong> I do not recommend this product." %></div>\
		\
		    </div>\
			  <div class="rating-right">\
			    <div class="rating-row"><span class="title">Quality of Product:</span> <div class="progress"><ul class="ratings-bars"><li></li><li></li><li></li><li></li><li></li></ul> <div class="progress-bar" style="width:<%= qualityCss %>%"></div></div></div> \
			    <div class="rating-row"><span class="title">Value of Product:</span> <div class="progress"><ul class="ratings-bars"><li></li><li></li><li></li><li></li><li></li></ul> <div class="progress-bar" style="width:<%= productValueCss %>%"></div></div></div> \
			    <div class="rating-row"><span class="title">Comfort:</span> <div class="progress"><ul class="ratings-bars"><li></li><li></li><li></li><li></li><li></li></ul> <div class="progress-bar" style="width:<%= comfortCss %>%"></div></div></div>\
			  </div>\
		</div>\
		<div class="bottom-info">\
			<div>Helpful? Yes ·  <%= review.TotalPositiveFeedbackCount %> | No · <%= review.TotalNegativeFeedbackCount %></div>\
		</div>\
	 </div>\
  </div>\
  \
      <% }); %>';

var tplAvgRating = '\
  <div class="reviews-summary">\
	<h3>Reviews</h3>\
      <div class="summary-left">\
		<% var starRatingsCount = starRatings.reduce(function(a,b){ return a+b; }, 0); %>\
        <% _.each(starRatings, function(rating, key) {  var ratingWidth =  ((rating * 100) / starRatingsCount); %>\
			<div class="rating-row"><span><%= key %>★ </span> <div class="progress"> <div class="progress-bar" style="width:<%= ratingWidth %>%"></div></div> <span><%= rating %></span> </div>\
          <% });  %>\
		<h6>Rating Snapshot</h6>\
      </div>\
      <div class="summary-right">\
		<h6>Average Customer Ratings</h6>\
        <div class="rating-row"><span class="title">Over All:</span> <div class="stars-part"> <div class="stars"> <div class="highlight-stars" style="width:<%= avgRatings.OverallRatingCss %>%"></div></div></div> <span><%= avgRatings.OverallRating %></span></div> \
        <div class="rating-row"><span class="title">Quality of Product:</span> <div class="progress"><ul class="ratings-bars"><li></li><li></li><li></li><li></li><li></li></ul> <div class="progress-bar" style="width:<%= avgRatings.QualityCss %>%"></div></div> <span><%= avgRatings.Quality %></span></div> \
        <div class="rating-row"><span class="title">Value of Product:</span> <div class="progress"><ul class="ratings-bars"><li></li><li></li><li></li><li></li><li></li></ul> <div class="progress-bar" style="width:<%= avgRatings.ValueCss %>%"></div></div> <span><%= avgRatings.Value %></span></div> \
        <div class="rating-row"><span class="title">Comfort:</span> <div class="progress"><ul class="ratings-bars"><li></li><li></li><li></li><li></li><li></li></ul> <div class="progress-bar" style="width:<%= avgRatings.ComfortCss %>%"></div></div> <span><%= avgRatings.Comfort %></span></div>\
      </div>\
  </div>';  

var productRatings = '\
	 <% var starRatingsCount = starRatings.reduce(function(a,b){ return a+b; }, 0); %>\
	 <div class="top-rating-row"><div class="stars-part"> <a href="#pdp-product-reviews" class="stars"> <div class="highlight-stars" style="width:<%= avgRatings.OverallRating * 20 %>%"></div><span class="hidden">Product Reviews</span></a></div> <span class="rank-review"><%= avgRatings.OverallRating %>\
	 <div class="top-review-summary">\
	     <% _.each(starRatings, function(rating, key) {  var ratingWidth =  ((rating * 100) / starRatingsCount); %>\
	   <div class="rating-row"><span><%= key %>★ </span> <div class="progress"> <div class="progress-bar" style="width:<%= ratingWidth %>%"></div></div> <span><%= rating %></span> </div>\
	       <% });  %>\
	   </div>\
	 </span><span class="total-review">(<%= starRatingsCount %>)</span></div>\
	 ';

var BTN_TULO_NEXT_PAGING = '#tuloBvPaging .btn-next';
var BTN_TULO_PREV_PAGING = '#tuloBvPaging .btn-prev';
var NUM_ITEM = 15;
//---- Configurations ----//
	function bootstrapGeneralProductReviews(){
	    // Disable cache-busting parameter names because we want to allow for API results to be cached
	    $.ajaxSetup({ cache: true });
	    // Make the request to the API using the base URL and key we set at the top of the file
	      loadAverageReviews();//load averageReviews
	      renderReviews();
	      
	      //prev btn paging
	      $(BTN_TULO_PREV_PAGING).click(function(e){
	    	  e.preventDefault();
	          renderReviews('prev');
	      });
	      
	      //next btn paging
	      $(BTN_TULO_NEXT_PAGING).click(function(e){
	    	  e.preventDefault();
	          renderReviews();//by default its ahead to NEXT...
	      });
	}
	function loadAverageReviews(){
		$.getJSON(api_server + "/products.json?callback=?",
		  "apiversion=" + api_version + "&passkey=" + bz_api_key + "&Filter=id:"+ tuloProducts +"&Stats=Reviews",
		  function (json) { //ths call is jut to fetch reviews
		    processOverAllRating(json);
		  });
	}
	function disableButton(btn){
		  btn.addClass('disabled');
	}
	  
	function enableButton(btn){
		  btn.removeClass('disabled');
	}  
	function renderReviews(cursorDirection, sortBy){
		num_items = NUM_ITEM;
		cursorDirection = cursorDirection || "next";
		var offset = loadedResults;
				
		if(cursorDirection == 'prev'){
			if( (offset - num_items) > 0){
				offset = offset - (num_items * 2);
				offset = offset > 0 ? offset : 0;
			}
			else{
				return false;
			}
		}		
	  
		sortBy = sortBy || "Rating:desc"; 
		var params = {
					apiversion : api_version,
					passkey : bz_api_key, 
					limit : num_items,
					offset : offset,
					Filter : "ProductId:"+ tuloProducts,
					Include : "Products",
					Stats : "Reviews",
					Sort : sortBy
					};
	    $.getJSON(api_server + "/reviews.json",params, 
	      function (json) {
	        // Render the template into a variable
	        var output = _.template(tplReviewsListing, json);
	        $("#module").html(output);
	        renderPagination(json, cursorDirection);
	      });
  }


  function renderPagination(json, cursorDirection){
    var totalResults = json.TotalResults;
    if(cursorDirection == 'prev')
        loadedResults -= json.Results.length; //resetting loaded results    	
    else
    	loadedResults += json.Results.length; //resetting loaded results
    //Next Button 
    if(totalResults > 0 && loadedResults < totalResults){
    	enableButton($(BTN_TULO_NEXT_PAGING));
    }
    else{
    	disableButton($(BTN_TULO_NEXT_PAGING));
    }
    
    //Prev Button 
    if(totalResults > 0 && (loadedResults - NUM_ITEM) > 0){
    	enableButton($(BTN_TULO_PREV_PAGING));
    }
    else{
    	disableButton($(BTN_TULO_PREV_PAGING));
    }
  }

  function processOverAllRating(json){
    var reviews = json.Results; //using in caps as we can make it to call back as well some time...
    var starRatings = [];
    var avgRatings = {Quality: 0, Comfort: 0, Value: 0, OverallRating: 0};
    _.each(reviews, function(review, key) { 
        
        /******************* Rating Distribution [5 Star Rating, Rating Snapshot] *****************/
        var rs = review.ReviewStatistics; //review statistics
        var rd = rs.RatingDistribution; //rating distribution
        _.each(rd, function(item){ //getting each rating and preparing for avg

            /******************* Rating Distribution [5 Star Rating, Rating Snapshot] *****************/
            var tmpRating = item.Count * 1;
            starRatings[item.RatingValue] = tmpRating + (starRatings[item.RatingValue] || 0) ;

            /******************* Rating Distribution [5 Star Rating, Rating Snapshot] *******************/
            /************************************** |<>| ************************************************/
            /************************************** |<>| ************************************************/
            /************************************** |<>| ************************************************/
            /************************************** |<>| ************************************************/
            /************************************** |<>| ************************************************/
        });

        /******************* Rating Distribution [Average Customer Ratings] *************************/

        var sra = rs.SecondaryRatingsAverages; //SecondaryRatingsAverages
        
        avgRatings.Quality += (sra.Quality.AverageRating || 0 ) * 1;
        
        avgRatings.Comfort += (sra.Comfort.AverageRating || 0 ) * 1;
        avgRatings.Value += (sra.Value.AverageRating  || 0 ) * 1;
        avgRatings.OverallRating += (rs.AverageOverallRating || 0 ) * 1;
      
        /******************* Rating Distribution [Average Customer Ratings] ***********************/          
    });
    
    avgRatings.OverallRating = (avgRatings.OverallRating / (reviews.length)).toFixed(1);
    avgRatings.Quality = (avgRatings.Quality / (reviews.length)).toFixed(1);
    avgRatings.Comfort = (avgRatings.Comfort / (reviews.length)).toFixed(1);
    avgRatings.Value = (avgRatings.Value / (reviews.length)).toFixed(1);
    
    //css Width
    avgRatings.OverallRatingCss = (avgRatings.OverallRating * 20).toFixed(1);
    avgRatings.QualityCss = (avgRatings.Quality * 20).toFixed(1);
    avgRatings.ComfortCss = (avgRatings.Comfort * 20).toFixed(1);
    avgRatings.ValueCss = (avgRatings.Value * 20).toFixed(1);
    
    
    // Render the template for over all rating into a variable
    var output = _.template(tplAvgRating, {avgRatings: avgRatings, starRatings: starRatings});
    $("#ratings-module").html(output);
 // Render the template for over all rating into a variable
    var output = _.template(productRatings, {avgRatings: avgRatings, starRatings: starRatings});
    
    $("#BVRRSummaryContainer").html(output);
  }
  
  
  
  /******************************** product title ***********************/
  
  /*$(window).resize(function () {
	  if ($(window).width() < 768) {
		$('.custom-reviews-holder-topbar').append($("#BVRRSummaryContainer"));
	  }else{
		$('.custom-reviews-holder-content').append($("#BVRRSummaryContainer"));
	  }
});*/
  /**
   * The ZoomControlButton helps creating zoom controls
   * This constructor takes the control DIV as an argument.
   * @constructor
   */
  function ZoomControlButton(controlDiv, map, type){
  // Set CSS for the control border.
	  var controlUI = document.createElement('div');
	  controlUI.style.backgroundColor = '#fff';
	  controlUI.style.border = '1px solid #e51970';
	  controlUI.style.borderRadius = '100%';
	  controlUI.style.cursor = 'pointer';
	  controlUI.style.marginBottom = '8px';
	  controlUI.style.marginRight = '8px';
	  controlUI.style.marginTop = '8px';
	  controlUI.style.textAlign = 'center';
	  controlUI.style.width = '24px';
	  controlUI.style.height = '24px';
	  controlUI.title = 'Click to '+ (type == '+' ? 'Zoom in' : 'Zoom out')+ ' the map';
	  controlDiv.appendChild(controlUI);

	  // Set CSS for the control interior.
	  var controlText = document.createElement('div');
	  controlText.style.color = '#e51970';
	//controlText.style.fontFamily = "akzidenz-grotesk";
	//controlText.style.fontWeight = 'bold';
	controlText.style.fontSize = '13px';
	controlText.style.lineHeight = '14px';
	controlText.style.padding = type == '+' ? '5px 4px 0' : '6px 4px 0';
    controlText.innerHTML = controlText.innerHTML = type == '-' ? '<i class="fa fa-minus" aria-hidden="true"></i>' : '<i class="fa fa-plus" aria-hidden="true"></i>'  ;;
    controlUI.appendChild(controlText);
  	
    if(type == '+'){
    	// Setup the click event listener - zoomIn
      google.maps.event.addDomListener(controlUI, 'click', function() {
        map.setZoom(map.getZoom() + 1);
      });
    }
    else{
    	// Setup the click event listener - zoomOut
      google.maps.event.addDomListener(controlUI, 'click', function() {
        map.setZoom(map.getZoom() - 1);
      });  
    }
   
  }

  /**
   * The ZoomControl adds a control to the map that works for Zoom In/Out
   * This constructor takes the control DIV as an argument.
   * @constructor
   */
  function ZoomControl(controlDiv, map) {

    ZoomControlButton(controlDiv, map, '+');
    ZoomControlButton(controlDiv, map, '-');

  }



  function setupMap(){
  	if(map == null){
  		map = new google.maps.Map(document.getElementById('map'), {
	        zoom: 17,
	        backgroundColor: '#000000',
	        center: new google.maps.LatLng(47.622503, -122.320626),
	        mapTypeId: 'roadmap',
	        disableDefaultUI: true,
	        styles: [
	            {
	                "featureType": "all",
	                "elementType": "labels.text.fill",
	                "stylers": [
	                    {
	                        "saturation": 36
	                    },
	                    {
	                        "color": "#333333"
	                    },
	                    {
	                        "lightness": 40
	                    }
	                ]
	            },
	            {
	                "featureType": "all",
	                "elementType": "labels.text.stroke",
	                "stylers": [
	                    {
	                        "visibility": "on"
	                    },
	                    {
	                        "color": "#ffffff"
	                    },
	                    {
	                        "lightness": 16
	                    }
	                ]
	            },
	            {
	                "featureType": "all",
	                "elementType": "labels.icon",
	                "stylers": [
	                    {
	                        "visibility": "off"
	                    }
	                ]
	            },
	            {
	                "featureType": "administrative",
	                "elementType": "geometry.fill",
	                "stylers": [
	                    {
	                        "color": "#fefefe"
	                    },
	                    {
	                        "lightness": 20
	                    }
	                ]
	            },
	            {
	                "featureType": "administrative",
	                "elementType": "geometry.stroke",
	                "stylers": [
	                    {
	                        "color": "#fefefe"
	                    },
	                    {
	                        "lightness": 17
	                    },
	                    {
	                        "weight": 1.2
	                    }
	                ]
	            },
	            {
	                "featureType": "landscape",
	                "elementType": "geometry",
	                "stylers": [
	                    {
	                        "color": "#f5f5f5"
	                    },
	                    {
	                        "lightness": 20
	                    }
	                ]
	            },
	            {
	                "featureType": "poi",
	                "elementType": "geometry",
	                "stylers": [
	                    {
	                        "color": "#f5f5f5"
	                    },
	                    {
	                        "lightness": 21
	                    }
	                ]
	            },
	            {
	                "featureType": "poi.park",
	                "elementType": "geometry",
	                "stylers": [
	                    {
	                        "color": "#dedede"
	                    },
	                    {
	                        "lightness": 21
	                    }
	                ]
	            },
	            {
	                "featureType": "road.highway",
	                "elementType": "geometry.fill",
	                "stylers": [
	                    {
	                        "color": "#ffffff"
	                    },
	                    {
	                        "lightness": 17
	                    }
	                ]
	            },
	            {
	                "featureType": "road.highway",
	                "elementType": "geometry.stroke",
	                "stylers": [
	                    {
	                        "color": "#ffffff"
	                    },
	                    {
	                        "lightness": 29
	                    },
	                    {
	                        "weight": 0.2
	                    }
	                ]
	            },
	            {
	                "featureType": "road.arterial",
	                "elementType": "geometry",
	                "stylers": [
	                    {
	                        "color": "#ffffff"
	                    },
	                    {
	                        "lightness": 18
	                    }
	                ]
	            },
	            {
	                "featureType": "road.local",
	                "elementType": "geometry",
	                "stylers": [
	                    {
	                        "color": "#ffffff"
	                    },
	                    {
	                        "lightness": 16
	                    }
	                ]
	            },
	            {
	                "featureType": "transit",
	                "elementType": "geometry",
	                "stylers": [
	                    {
	                        "color": "#f2f2f2"
	                    },
	                    {
	                        "lightness": 19
	                    }
	                ]
	            },
	            {
	                "featureType": "water",
	                "elementType": "geometry",
	                "stylers": [
	                    {
	                        "color": "#8bcbd5"
	                    },
	                    {
	                        "lightness": 17
	                    }
	                ]
	            }
	        ]
	    });
  		// Create the DIV to hold the control and call the ZoomControl()
  		  // constructor passing in this DIV.
  		  var zoomControlDiv = document.createElement('div');
  		  var zoomControl = new ZoomControl(zoomControlDiv, map);

  		  zoomControlDiv.index = 1;
  		  map.controls[google.maps.ControlPosition.TOP_RIGHT].push(zoomControlDiv);
  	}
  	return  map;
  }

  function setPlaceMarker(position){
	  var map = map = setupMap();
 	 	var color = "#e51970";
 		var smbPath = "M12,0C7.802,0,4,3.403,4,7.602c0,6.243,6.377,6.903,8,16.398c1.623-9.495,8-10.155,8-16.398C20,3.403,16.199,0,12,0z M12,9.987c-1.256,0-2.275-1.019-2.275-2.275c0-1.256,1.019-2.275,2.275-2.275s2.275,1.019,2.275,2.275 C14.275,8.969,13.256,9.987,12,9.987z";
 	    var pinSymbl = {
 	        'path': smbPath,
 	        'fillColor': color,
 	        'fillOpacity': 1,
 	        'strokeColor': "#fff",
 	       'strokeWeight': 1,
 	        'scale': 2

 	    };
 	    map.setCenter(position);
 		var mapPoint = new google.maps.Point(-8, -10);
 		var marker = new MarkerWithLabel({
 	        position: position,
 	        map: map,
 	        draggable: false,
 	        raiseOnDrag: false,
 	        labelContent: '',
 	        labelAnchor: mapPoint,
 	        labelClass: "gmap-placemarker-label", // the CSS class for the label
 	        labelInBackground: false,
 	        icon: pinSymbl
 	    });
  }
  function loadPreferredMap(zipCode)
  {
	 var productID = $("div#pdp-store-map").data('productid');
	 $.get( Urls.loadPreferredMap+"?productID="+productID+"&zipCode="+zipCode )
		.done(function( text ){
		var $response = $( text );	
		if($response)
		{
			$("div#pdp-store-map").html($response);
			 if(typeof latitude !=  'undefined' && typeof longitude !=  'undefined'){
				  setPlaceMarker(new google.maps.LatLng(latitude, longitude));
			  }
		}
	});	
  }
  $("div#pdp-store-map").on('blur focusout','#pdp-zipcode-text', function () {
  	var $postalcodetmp = this.value;
  	IsValidPDPZipCode($postalcodetmp);
	});
  
  $("div#pdp-store-map").on('keyup', '#pdp-zipcode-text', function () {
  	var $postalcodetmp = this.value;
  	EnableDisablePDPZip($postalcodetmp);
	});
  
  $("div#pdp-store-map").on('keypress','#pdp-zipcode-text',function (e) {
  	if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
          return false;
     }
  });
  $("div#pdp-store-map").on('click',"#btn-pdp-zipcode", function (e) {  
  	e.preventDefault();    	
  	var zipCode = $(this).closest('form').find('#pdp-zipcode-text').val();
  	if(zipCode == "" || zipCode ==  null || zipCode == undefined)
  	{
  		return;
  	}
  	ga('tealium_0.send', 'event', 'PDPMapLookup', 'Click', 'search_store');
  	loadPreferredMap(zipCode);
  });
  function IsValidPDPZipCode(zip) {
		var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
		if (!isValid && zip.length >0) {
			if ($("#form-pdp-zipcode .postalerror").length < 1) {
				$("#form-pdp-zipcode .fields-wrap").append("<span class='error postalerror'>" + Resources.INVALID_ZIP + "</span>");			
				$("#btn-pdp-zipcode").attr("disabled", "disabled");
			}
			return false;
		} else {
			if ($("#form-pdp-zipcode .postalerror").length > 0) {			
				$("#form-pdp-zipcode span.postalerror").remove();
				$("#btn-pdp-zipcode").removeAttr("disabled");
			}
			return true;
		}
	}

	function EnableDisablePDPZip(zip) {
		var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
		if (!isValid) {				
			$("#btn-pdp-zipcode").attr("disabled", "disabled");
			return false;
		}		
		 else {	
			$("#btn-pdp-zipcode").removeAttr("disabled");
			return true;
		}	
	}
 