'use strict';

var dialog = require('../../dialog'),
	minicart = require('../../minicart'),
	page = require('../../page'),
	util = require('../../util'),
	TPromise = require('promise'),
	_ = require('lodash'),
	addToCartIntercept = require('./addToCartIntercept'),
	utaghelper = require('../../utaghelper');

/**
 * @description Make the AJAX request to add an item to cart
 * @param {Element} form The form element that contains the item quantity and ID data
 * @returns {Promise}
 */
var addItemToCart = function (form) {
	var $form = $(form),
		$qty = $form.find('input[name="Quantity"]');
	if ($qty.length === 0 || isNaN($qty.val()) || parseInt($qty.val(), 10) === 0) {
		$qty.val('1');
	}
	return TPromise.resolve($.ajax({
		type: 'POST',
		url: util.ajaxUrl(Urls.addProduct),
		data: $form.serialize()
	}));
};

/**
 * @description Open modal to display bonus products
 */
var openBonusModal = function (response, parentPID) {
	var bonusModelURL = $(response).find('.select-bonus-' + parentPID).attr('href');
	if(typeof bonusModelURL != 'undefined' && bonusModelURL.length > 0){  
		bonusModelURL = bonusModelURL.replace('source=minicart','source=pdpaddtocart');
	    minicart.openBonusDialog({
	        url: bonusModelURL
	    });
	}
};

/**
 * @description Handler to handle the add to cart event
 */
var addToCart = function (e) {
	e.preventDefault();
	var $form = $(this).closest('form');
	var $btnSubmit = $form.find('button[id="add-to-cart"]');
	$btnSubmit.attr("disabled", true);
	addItemToCart($form).then(function (response) {
		var $uuid = $form.find('input[name="uuid"]');
		var parentPID = $form.find('input[name="pid"]').val();
		if ($uuid.length > 0 && $uuid.val().length > 0) {
			page.refresh();
		} else {
			// do not close quickview if adding individual item that is part of product set
			// @TODO should notify the user some other way that the add action has completed successfully
			if (!$(this).hasClass('sub-product-item')) {
				dialog.close();
			}

			//openBonusModal(response, parentPID);
			minicart.show(response);
			$btnSubmit.removeAttr("disabled");
		//	addToCartIntercept.show();
		}
		//Add Bonus product Utag
		utaghelper.addBonusProducttUtag();
		
	}.bind(this));
};

/**
 * @description Handler to handle the add base to cart event
 */
var addBaseToCart = function (e) {
	e.preventDefault();
	$("#baseOptionField").html($("#baseOptionHidden").html());
	$('.add-to-cart').click();
	addUtagWithOptionProduct();
	$("#baseOptionField").html('');
};
/***
 * it will send product detail to tealium
 * @returns
 */
function addUtagWithOptionProduct() {
	var prodDetailContainer = $("div#product-content").find(".product-name-container");
	var OptionProductContainer = $('.tulo-package-section');
	if(prodDetailContainer.length > 0 && OptionProductContainer.length > 0) {
		utaghelper.AddProductUtage(prodDetailContainer, OptionProductContainer);
	} 
}

/**
 * @description Handler to handle the add all items to cart event
 */
var addAllToCart = function (e) {
	e.preventDefault();
	var $productForms = $('#product-set-list').find('form').toArray();
	TPromise.all(_.map($productForms, addItemToCart))
		.then(function (responses) {
			dialog.close();
			// show the final response only, which would include all the other items
			minicart.show(responses[responses.length - 1]);
		});
};

/**
 * @description Handler to handle the add all items to cart event
 */
var addAllWishlistToCart = function (e) {
	e.preventDefault();
	var $productForms = $('#wishlist-table').find('form:has(".add-to-cart")').toArray();
	TPromise.all(_.map($productForms, addItemToCart))
		.then(function (responses) { 
			dialog.close();
			// show the final response only, which would include all the other items
			minicart.show(responses[responses.length - 1]);
//			location.reload();
		});
};

/**
 * @description special Handler to handle add to cart
 */
var addSpecialToCart = function (e, $this) {
	var $form = $this.closest('form');
	var $btnSubmit = $form.find('button[id="add-to-cart"]');
	$btnSubmit.attr("disabled", true);
	addItemToCart($form).then(function (response) {
		var $uuid = $form.find('input[name="uuid"]');
		if ($uuid.length > 0 && $uuid.val().length > 0) {
			page.refresh();
		} else {
			// do not close quickview if adding individual item that is part of product set
			// @TODO should notify the user some other way that the add action has completed successfully
			if (!$this.hasClass('sub-product-item')) {
				dialog.close();
			}

			minicart.show(response);
			$btnSubmit.removeAttr("disabled");
		//	addToCartIntercept.show();
		}
		
	}.bind(this));
};

/**
 * @function
 * @description Binds the click event to a given target for the add-to-cart handling
 */
module.exports = function () {
	$('.add-to-cart[disabled]').attr('title', $('.availability-msg').text());
	$('.primary-content-pdp').on('click', '.add-to-cart', addToCart);
	$('.primary-content-pdp').on('click', '#btnBaseToCart', addBaseToCart);
	$('#wishlist-table').on('click', '.add-to-cart', addToCart); //[M.D]
	$('#add-all-wishlist-to-cart').on('click', addAllWishlistToCart); //[M.D]y	
	$('.primary-content-pdp').on('click', '.add-to-cart-tuloset', addToCart);
};
module.exports.AddSpecialToCart = addSpecialToCart; 
