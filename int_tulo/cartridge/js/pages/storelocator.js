'use strict';
var dialog = require('../dialog');
var progress = require('../progress');
var mapLatLng = null;
var map;
var LAT;
var LNG;
var infowindow;
var ZOOM_LEVEL = 12;
var gmarkers = [];
var input = null;
var isFormSubmitted = false; //flag for form submission and managing places api
var placeChangedProcessCompleted = true; //flag it turns to true whenever placechange event is triggered
var autocompleteGmap = null; //google autocomplete
var center = null;
var currentPopup;
var bounds = null;

exports.init = function () {
	bounds =  new google.maps.LatLngBounds();
    $('.store-details-link').on('click', function (e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href')
        });
    });
};

//jQuery code for store locator page tabs
$('ul.tabs li').click(function () {
    var tab_id = $(this).attr('data-tab');

    $('ul.tabs li').removeClass('current');
    $('.tab-content').removeClass('current');

    $(this).addClass('current');
    $("#" + tab_id).addClass('current');
    if ($('#wrapper').hasClass('pt_store-locator')) {
        mapload();
    }
});

if ($('#wrapper').hasClass('pt_store-locator')) {
    mapload();
    formSubmitCheckForGoogle();
    //load more stores....
    $('[data-loadmorestores=1]').click(function () {
        var moreStores = $('[data-storeidx].hidden');
        $.each(moreStores, function (key, value) {
            var tilesToLoad = ($(window).width() < 768) ? 2 : 6;
            if (key < tilesToLoad) {
                $(value).removeClass('hidden');
                addMarker(getStore(stores, 'ID', $(value).attr('data-storeidx')), map, $(value).find('.loc-count').text());

                if ((key + 1) == moreStores.length) {
                    $('[data-loadmorestores=1]').hide();
                }
            }


        });
    });

    //mapload();
    //initialize();
    $(window).resize(function () {
        mapload();
    });
    $(function () {
    	$(document).on('click', '.btn-myStore', function (e) {
    		return false;
    	});
        //listing make my store...
        $(document).on('click', '.btn-makeMyStore', function (e) {

            e.preventDefault(); // if you want to cancel the event flow
            // do something
            var that = this;
            var url = $(this).attr('href');
            //			$(that).closest('.mystore-wrapper').html('Loading ...');

            $.ajax({
                type: 'POST',
                url: url,
                data: [],
                beforeSend: function () {
                    progress.show($(that).closest('.mystore-wrapper'));
                },
                success: function (res) {

                    var currentStoreId = $('[data-mystore=1]').attr('data-store');
                    var tmpStoreId = $(that).attr('data-store');
                    var currentStore = $('[data-mystore=1]').closest('.storeLocation ');
                    $('[data-store]').closest('.storeLocation').removeClass('my-store-location');
                    $('[data-store]').attr('data-mystore', 0).removeClass('white btn-myStore').addClass('black btn-makeMyStore');
                    $('[data-store=' + tmpStoreId + ']').attr('data-mystore', 1).addClass('white btn-myStore').removeClass('black btn-makeMyStore');
                    $('[data-store=' + tmpStoreId + ']').closest('.storeLocation').addClass('my-store-location');
                    $('[data-store]').closest('.store-info').find(".helpingText").show();
                    $('[data-store=' + tmpStoreId + ']').closest('.store-info').find(".helpingText").hide();
                    if (typeof gmarkers[tmpStoreId] != 'undefined') {
                        gmarkers[tmpStoreId].setMap(null);

                        var tmpStore = getStore(stores, 'ID', tmpStoreId);
                        tmpStore.markerType = tmpStore.markerType == 3 || tmpStore.markerType == 4 ? 4 : 1; //mark it to preferred
                        addMarker(tmpStore, map, $('[data-storeidx=' + tmpStoreId + ']').find('.loc-count').text());
                    }

                    if (typeof gmarkers[currentStoreId] != 'undefined') {
                        gmarkers[currentStoreId].setMap(null);
                        var tmpStore = getStore(stores, 'ID', currentStoreId);
                        tmpStore.markerType = tmpStore.markerType == 3 || tmpStore.markerType == 4 ? 3 : 2;
                        addMarker(tmpStore, map, $('[data-storeidx=' + currentStoreId + ']').find('.loc-count').text());
                    }



                },
                complete: function () {
                    progress.hide($(that).closest('.mystore-wrapper'));
                }
            });
            return false;
        });



		/* client to ask remove this "check Stock" button
		 * $(".tulo-stock .stock-header button").click(function(){
			var that = this;
			progress.show($(this).closest(".container"));

			$.ajax({
				type: 'GET',
				url: $(this).val(),
				data: []
			})
			.done(function (res) {
				$('.stock-levels').html(res);
				$(".tulo-stock .stock-levels").slideDown(500);       
				$(".progress-bar").each(function() {
					var wid = $(this).width() / $(this).parent().width() * 100;
					$(this)
					.width(0)
					.animate({
						width: wid + "%"
					}, 1200);
				});
				$(".tulo-stock .stock-header").slideUp(500);
				progress.hide($(that).closest(".container"));
			});

		}); */
    });
}


function getStore(obj, key, val) {

    var newObj = false;
    $.each(obj, function () {
        var testObject = this;
        $.each(testObject, function (k, v) {
            //alert(k);
            if (val == v && k == key) {
                newObj = testObject;
            }
        });
    });

    return newObj;
}

/**
 * The ZoomControlButton helps creating zoom controls
 * This constructor takes the control DIV as an argument.
 * @constructor
 */
function ZoomControlButton(controlDiv, map, type) {
    //	Set CSS for the control border.
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#fff';
    controlUI.style.border = '1px solid #e51970';
    controlUI.style.borderRadius = '100%';
    controlUI.style.cursor = 'pointer';
    controlUI.style.marginBottom = '8px';
    controlUI.style.marginRight = '8px';
    controlUI.style.marginTop = '8px';
    controlUI.style.textAlign = 'center';
    controlUI.style.width = '24px';
    controlUI.style.height = '24px';
    controlUI.title = 'Click to ' + (type == '+' ? 'Zoom in' : 'Zoom out') + ' the map';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior.
    var controlText = document.createElement('div');
    controlText.style.color = '#e51970';
    //	controlText.style.fontFamily = "akzidenz-grotesk";
    //	controlText.style.fontWeight = 'bold';
    controlText.style.fontSize = '13px';
    controlText.style.lineHeight = '14px';
    controlText.style.padding = type == '+' ? '5px 4px 0' : '6px 4px 0';
    controlText.innerHTML = controlText.innerHTML = type == '-' ? '<i class="fa fa-minus" aria-hidden="true"></i>' : '<i class="fa fa-plus" aria-hidden="true"></i>';;
    controlUI.appendChild(controlText);


    if (type == '+') {
        // Setup the click event listener - zoomIn
        google.maps.event.addDomListener(controlUI, 'click', function () {
            map.setZoom(map.getZoom() + 1);
        });
    }
    else {
        // Setup the click event listener - zoomOut
        google.maps.event.addDomListener(controlUI, 'click', function () {
            map.setZoom(map.getZoom() - 1);
        });
    }

}

/**
 * The ZoomControl adds a control to the map that works for Zoom In/Out
 * This constructor takes the control DIV as an argument.
 * @constructor
 */
function ZoomControl(controlDiv, map) {

    ZoomControlButton(controlDiv, map, '+');
    ZoomControlButton(controlDiv, map, '-');

}



function setupMap() {
    if (map == null) {
    	var lat = document.getElementById('dwfrm_storelocator_lat'); //lat
        var lng = document.getElementById('dwfrm_storelocator_lng'); //lng
        var geoLat = document.getElementById('geolat');
        var geoLng = document.getElementById('geolng');
    	if(typeof LAT == 'undefined' ){    		
    		LAT = lat.value  != ''  && lat.value  != null ? lat.value : geoLat.value;
    	}
    	if(typeof LNG == 'undefined'){    		
    		LNG = lng.value  != '' && lng.value  != null ? lng.value : geoLng.value;
    	}
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: ZOOM_LEVEL,
            backgroundColor: '#000000',
            center: new google.maps.LatLng(LAT, LNG),
            mapTypeId: 'roadmap',
            disableDefaultUI: true,
            styles: [
                {
                    "featureType": "all",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "saturation": 36
                        },
                        {
                            "color": "#333333"
                        },
                        {
                            "lightness": 40
                        }
                    ]
                },
                {
                    "featureType": "all",
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#ffffff"
                        },
                        {
                            "lightness": 16
                        }
                    ]
                },
                {
                    "featureType": "all",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#fefefe"
                        },
                        {
                            "lightness": 20
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#fefefe"
                        },
                        {
                            "lightness": 17
                        },
                        {
                            "weight": 1.2
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#f5f5f5"
                        },
                        {
                            "lightness": 20
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#f5f5f5"
                        },
                        {
                            "lightness": 21
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#dedede"
                        },
                        {
                            "lightness": 21
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        },
                        {
                            "lightness": 17
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        },
                        {
                            "lightness": 29
                        },
                        {
                            "weight": 0.2
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        },
                        {
                            "lightness": 18
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        },
                        {
                            "lightness": 16
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#f2f2f2"
                        },
                        {
                            "lightness": 19
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#8bcbd5"
                        },
                        {
                            "lightness": 17
                        }
                    ]
                }
            ]
        });
        // Create the DIV to hold the control and call the ZoomControl()
        // constructor passing in this DIV.
        var zoomControlDiv = document.createElement('div');
        var zoomControl = new ZoomControl(zoomControlDiv, map);

        zoomControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(zoomControlDiv);
    }
    return map;
}

function updateLatLng(location) {
    var lat = document.getElementById('dwfrm_storelocator_lat'); //lat
    var lng = document.getElementById('dwfrm_storelocator_lng'); //lng
    //	$(input).attr('data-processed-location', 'yes');
    if (location != null) {
        lat.value = location.lat();
        lng.value = location.lng();
    }


    placeChangedProcessCompleted = true;
    if (isFormSubmitted) {
        $("#dwfrm_storelocator_findStores").trigger('click');
    }
}

function formSubmitCheckForGoogle() {
    $("#dwfrm_storelocator").submit(function (event) {
        isFormSubmitted = true;
        var addressElem = document.getElementById('dwfrm_storelocator_address');
        var latElem = document.getElementById('dwfrm_storelocator_lat');
        var lngElem = document.getElementById('dwfrm_storelocator_lng');

        if (addressElem.value.trim() == '') {
            latElem.value = '';
            lngElem.value = '';
        }

        var lat = latElem.value.trim(); //lat
        var lng = lngElem.value.trim(); //lng

        if (lat != "" && lng != "" && placeChangedProcessCompleted) {
            return true;
        } else {
            event.preventDefault();
            fetchLatLngFromGoogle();//lets call it to update location and submit the form
        }
        return false;
    });

}
//find store google map code


function mapload() {
    var isAddedD = false;
    var isAddedM = false;
    $('#map').remove();
    if (!isAddedM && $(window).width() < 768) {
        isAddedM = true;
        $('#map').remove();
        map = null;
        $('.find-store-map-mobile').append('<div id="map" class="gmap3"></div>');
        initialize();
        //google.maps.event.addDomListener(window, "resize", initialize);
    } else if (!isAddedD && $(window).width() >= 768) {
        isAddedD = true;
        $('#map').remove();
        map = null;
        $('.find-store-map').append('<div id="map" class="gmap3"></div>');
        initialize();
        //google.maps.event.addDomListener(window, "resize", initialize);
    }
}



function fetchLatLngFromGoogle() {
    var location = null;
    var q = $(input).val(); //form search
    //	if(lat.val().trim() == "" || lat.val() == null || lng.val().trim() == "" || lng.val() == null){
    map = setupMap();
    //initialize places service to perform TextSearch
    var service = new google.maps.places.PlacesService(map);
    var request = {
    	query: q + '&country=US'
    };
    service.textSearch(request, function (results, status) {
        if (status == google.maps.places.PlacesServiceStatus.OK) {
            var place = results[0];
            location = place.geometry.location;
            updateLatLng(location);

        } else {
            $("#dialog_store_alert").dialog({
                width: 200,
                dialogClass: "no_address_found",
                draggable: false,
                modal: true
            });
        }
    });
    //	}



}


function initialize() {
    map = setupMap();

    /******* google places api ***********/
    var options = {
        //			types: ['(cities)'],
        componentRestrictions: { country: "us" }
    };

    input = document.getElementById('dwfrm_storelocator_address'); //autocomplete input
    $(input).on('keyup', function () {
        placeChangedProcessCompleted = false;
    });
    autocompleteGmap = new google.maps.places.Autocomplete(input, options);
    //setting up an event listener to resctrict google placemarker autocomplete
    $("#dwfrm_storelocator_country").bind('change', function () {
        var country = $(this).val();
        country = country == '' ? 'US' : country;
        autocompleteGmap.setComponentRestrictions({ 'country': country });
    });
    $("#dwfrm_storelocator_country").change(); // fake change to make sure restrictions applied
    autocompleteGmap.addListener('place_changed', placeChangedHandler);
    function placeChangedHandler() {
        // Get the place details from the autocomplete object.
        placeChangedProcessCompleted = false; //lets first make it false so that form doesn't get submitted automatically 
        var location = null;
        var place = autocompleteGmap.getPlace();
        if (place != null && typeof place.geometry != 'undefined' && typeof place.geometry.location != 'undefined') {
            location = place.geometry.location;
            updateLatLng(location);
        }
    }
    /******* google places api ***********/
    infowindow = new google.maps.InfoWindow();

    google.maps.event.addListenerOnce(map, 'idle', function () {
        //loaded fully
        if (typeof stores != 'undefined') {
            processMarkers(stores, map);
        }

    });


    // *
    // START INFOWINDOW CUSTOMIZE.
    // The google.maps.event.addListener() event expects
    // the creation of the infowindow HTML structure 'domready'
    // and before the opening of the infowindow, defined styles are applied.
    // *
    google.maps.event.addListener(infowindow, 'domready', function () {

        // Reference to the DIV that wraps the bottom of infowindow
        var iwOuter = $('.gm-style-iw');
        //iwOuter.parent().width('260px');
		/* Since this div is in a position prior to .gm-div style-iw.
		 * We use jQuery and create a iwBackground variable,
		 * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
		 */
        var iwBackground = iwOuter.prev();

        // Removes background shadow DIV
        iwBackground.children(':nth-child(2)').css({ 'display': 'none' });

        // Removes white background DIV
        iwBackground.children(':nth-child(4)').css({ 'display': 'none' });

        // Moves the infowindow 115px to the right.
        iwOuter.parent().parent().css({ left: '27px' });

        // Moves the shadow of the arrow 76px to the left margin.
        iwBackground.children(':nth-child(1)').attr('style', function (i, s) { return s + 'left: 135px !important;' });

        // Moves the arrow 76px to the left margin.
        iwBackground.children(':nth-child(3)').attr('style', function (i, s) { return s + 'left: 135px !important; background-color:#eee !important;' });
        iwBackground.children(':nth-child(3)').children().children().attr('style', function (i, s) { return s + 'background-color:#eee !important;' });
        // Changes the desired tail shadow color.
        iwBackground.children(':nth-child(3)').find('div').children().css({ 'box-shadow': 'rgba(00, 00, 00, 0.6) 0px 1px 6px', 'z-index': '1' });

        // Reference to the div that groups the close button elements.
        var iwCloseBtn = iwOuter.next();
        var imgClose = Urls.staticPath + '/images/icon-close.png';
        // Apply the desired effect to the close button
        iwCloseBtn.css({ opacity: '1', width: '16px', height: '16px', left: '272px', top: '30px', border: 'none', 'border-radius': '0', 'box-shadow': 'none' });
        iwCloseBtn.children('img').attr("src", imgClose);
        iwCloseBtn.children('img').css({ width: '16px', height: '16px', top: '0', left: '0' });
        // If the content of infowindow not exceed the set maximum height, then the gradient is removed.
        if ($('.iw-content').height() < 140) {
            $('.iw-bottom-gradient').css({ display: 'none' });
        }

        // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
        iwCloseBtn.mouseout(function () {
            $(this).css({ opacity: '1' });
        });
        iwCloseBtn.click(function(){
        	infowindow.close();
        })
    });


    google.maps.event.addListener(map, 'click', function () {
        infowindow.close();
    });


}
//google.maps.event.addDomListener(window, "load", initialize);
//custom - DK
function processMarkers(stores) {
	if(stores.length < 6){ //consider its details page
		 for (var i = 0; i < stores.length && i < 6; i++) {
		        var store = stores[i];
		        if (i == 0) {
		            map.setCenter(new google.maps.LatLng(store.lat, store.lng));
		            map.setZoom(ZOOM_LEVEL);  // Why 12? Because it looks good.
		        }
		        addMarker(store, map, (i + 1));
		    }
	}
	else{
		 var moreStores = $('[data-storeidx]:not(.hidden)');
		    $.each(moreStores, function (key, value) {
		    	var store = getStore(stores, 'ID', $(value).attr('data-storeidx'));
		    	var tmpNumber = $(value).find('.loc-count').text();
		        addMarker(store, map, tmpNumber);
		        if (key == 0) {
		            map.setCenter(new google.maps.LatLng(store.lat, store.lng));
		            map.setZoom(ZOOM_LEVEL);  // Why 12? Because it looks good.
		        }
		    });
	}
   
}

//add marker whenever needed
function addMarker(store, map, storeNumber) {
    var tmpPosition = new google.maps.LatLng(store.lat, store.lng);
    var markerAddress = store.address1 + ' ' + (store.address2 != null ? store.address2 : '') + ' ' + store.city + (store.city != null && store.stateCode != null ? ", " : "") + store.stateCode;
    var myStoreLabel = store.markerType == 1 || store.markerType == 4 ? "my store" : "make it my store";
    var myStoreClass = store.markerType == 1 || store.markerType == 4 ? "white btn-myStore" : "black btn-makeMyStore";
    storeNumber = store.markerType == 4 || store.markerType == 3 ? '' /*  as its on detail page */ : storeNumber;
    var schedule = 'Today: open until ' + store.schedule.today.close + store.schedule.today.closeMeridiem + ' <br> Tomorrow:' + store.schedule.tomorrow.open + store.schedule.tomorrow.openMeridiem + ' - ' + store.schedule.tomorrow.close + store.schedule.tomorrow.closeMeridiem;
    var storeNames = $('#storeNamesList').val();
    var storeImageUrl = $('#storeImageUrl').val(); 
    var storeNamesList = storeNames.split(",");
    var currentStoreName = store.name;
    var storeType = "";
    $.each(storeNamesList, function(index, storeName) {  
		if (currentStoreName.indexOf(storeName) > -1) { 
			currentStoreName = currentStoreName.replace(storeName, "");
			storeType = storeName;
			return false;  
		}
    });
    var feature = {
        storeNumber: storeNumber,
        position: tmpPosition,
        title: '<a href="' + store.url + '">'+ currentStoreName +'</a>',
        distance: store.distance,
        address: markerAddress,
        schedule: schedule,
        phone: store.phone,
        markerType: store.markerType,
        locBtn: '<span class="mystore-wrapper" ><a data-store="' + store.ID + '" data-storename="' + store.name + '"  href="' + store.makeMyStoreURL + '" class="btn-primary ' + myStoreClass + '"><i class="fa fa-check" aria-hidden="true"></i>' + myStoreLabel + ' </a></span>',
        moreinfoBtn: '<a href="' + store.url + '" class="btn-moreInfo-mobile"> <i class="fa fa-chevron-right" aria-hidden="true"></i> </a>',
        logo: storeType != "" ? '<img class="pull-right" src="' + storeImageUrl + storeType.replace(' ', '-').toLowerCase() + '-store-logo.svg" alt="' + storeType + '" title="' + storeType + '" />' : ""
    };

    var mapStyle = getMapStyle(feature.markerType);
    var markerOption = {
    	    latitude: store.lat,
    	    longitude: store.lng,
    	    color: mapStyle.color,
    	    text: storeNumber,
    	    markerType: store.markerType
    	  };
      
	  var marker = createMarker(markerOption);
	  marker.setMap(map);
	  bounds.extend(feature.position);


      if (feature.markerType > 2 || stores.length <= 1) {//if its store details
          map.setZoom(17);
      }
      else {
          map.fitBounds(bounds);
      }

      var featureTitle = feature.moreinfoBtn
      // must be a string (or a DOM node).  
      var content = '<div id="iw-container">' +
          '<div class="iw-title"><a href="'+feature.phone+'">' + feature.title + (feature.markerType > 2 ? '' : '<br><small>' + feature.distance + ' m</small>') + '</div>' +
          '<div class="iw-content">' +        
          '<p>' + feature.schedule + '</p>' +
          '<p><u>' + feature.address + '</u></p>' +
          '<p><a href="callto://'+feature.phone+'">' + feature.phone + '</a></p>' +
          '<div class="clearfix button-and-logo">' +
          feature.locBtn +
          '<div class="store-logo">' +
        	feature.logo +
          '</div></div>'+
          (feature.markerType > 2 ? " " : feature.moreinfoBtn) +
         
          '</div>' +
          '</div>';
      google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
          return function (evt) {
              infowindow.setContent(content);
              infowindow.open(map, marker);
          };
      })(marker, content, infowindow));
      gmarkers[store.ID] = marker;
	  

}

function makeSVGElement(tag, attrs) {
    var el = document.createElementNS('http://www.w3.org/2000/svg', tag);
    for (var k in attrs) {
        el.setAttribute(k, attrs[k]);
    }

    return el;
}

function getMapStyle(markerType) {

    var color = markerType == 2 || markerType == 3 ? "#8c1849" : "#e51970";
    var symbolPath = markerType > 2 ? "M12,0C7.802,0,4,3.403,4,7.602c0,6.243,6.377,6.903,8,16.398c1.623-9.495,8-10.155,8-16.398C20,3.403,16.199,0,12,0z M12,9.987c-1.256,0-2.275-1.019-2.275-2.275c0-1.256,1.019-2.275,2.275-2.275s2.275,1.019,2.275,2.275 C14.275,8.969,13.256,9.987,12,9.987z" : "M12,0C7.802,0,4,3.403,4,7.602c0,6.243,6.377,6.903,8,16.398c1.623-9.495,8-10.155,8-16.398C20,3.403,16.199,0,12,0z";

    return { color: color, symbolPath: symbolPath };
}


//generate icon for google map
function generateIcon(store, callback) {
    var mapStyle = getMapStyle(store.markerType);
    var fontSize = '6.2';
    var imageWidth = 50;
    var imageHeight = 50;
    var number = store.storeNumber * 1;
    if (number >= 1000) {
        fontSize = 5;
        imageWidth = imageHeight = 55;
    } else if (number < 1000 && number > 100) {
        fontSize = 5;
        imageWidth = imageHeight = 50;
    }

    var svg = makeSVGElement('svg', {
        viewBox: '0 0 40 40',
        'enable-background' : "new 0 0 40 40",
        'xml:space' : 'preserve'

    }); //keeping the orientation hardcoded

    var g = makeSVGElement('g', {});

    svg.appendChild(g);


    var iconPath = mapStyle.symbolPath;
    var path = makeSVGElement('path', {
        d: iconPath,
        fill: mapStyle.color
    });

    g.appendChild(path);
    var text = makeSVGElement('text', {
        'dx': 12,
        'dy': 10,
        'text-anchor': 'middle',
        'style': 'font-size:' + fontSize + '; fill: #fff; font-family: Arial, Verdana; font-weight: bold'
    });
    text.textContent = number
    g.appendChild(text);
    var svgNode = svg.cloneNode(true),
        image = new Image();

    var xmlSource = (new XMLSerializer()).serializeToString(svgNode);

    image.onload = (function (imageWidth, imageHeight) {
        var canvas = document.createElement('canvas'),
            context = canvas.getContext('2d'),
            dataURL;
        canvas.setAttribute('width', imageWidth);
        canvas.setAttribute('height', imageHeight);

        context.drawImage(image, 0, 0, imageWidth, imageHeight);

        dataURL = canvas.toDataURL();
        //generateIconCache[number] = dataURL;
        callback(dataURL);
    }).bind(this, imageWidth, imageHeight);

    image.src = 'data:image/svg+xml;base64,' + btoa(encodeURIComponent(xmlSource).replace(/%([0-9A-F]{2})/g, function (match, p1) {
        return String.fromCharCode('0x' + p1);
    }));
}


function createMarker(options) {
	var CustomShapeCoords = [16, 1.14, 21, 2.1, 25, 4.2, 28, 7.4, 30, 11.3, 30.6, 15.74, 25.85, 26.49, 21.02, 31.89, 15.92, 43.86, 10.92, 31.89, 5.9, 26.26, 1.4, 15.74, 2.1, 11.3, 4, 7.4, 7.1, 4.2, 11, 2.1, 16, 1.14];
	
	  //IE MarkerShape has problem
	  var markerObj = new google.maps.Marker({
	    icon: createSvgIcon(options.color, options.text, options.markerType),
	    position: {
	      lat: parseFloat(options.latitude),
	      lng: parseFloat(options.longitude)
	    },
	    draggable: false,
	    visible: true,
	    zIndex: 10,
	    shape: {
	      coords: CustomShapeCoords,
	      type: 'poly'
	    }
	  });

	  return markerObj;
}

function createSvgIcon(color, text, markerType) {
	  var div = $("<div></div>");
	  var circleColor = markerType > 2 ? '#ffffff' :  color;
	  var textX = 16;
	  if(window.navigator.userAgent.indexOf("Edge") > -1 ){ //as MS/edge was always sucks 
		  
	    if(text.length == 1)
	      textX = 13;
	    else if(text.length == 2)
	      textX = 10;
	    else if(text.length >= 3)
	      textX = 9;
	  }
	  var svg = $(
	    '<svg width="32px" height="43px"  viewBox="0 0 32 43" xmlns="http://www.w3.org/2000/svg">' +
	    '<path style="fill:'+color+';stroke:#fff;stroke-width:1;stroke-miterlimit:10;" d="M30.6,15.737c0-8.075-6.55-14.6-14.6-14.6c-8.075,0-14.601,6.55-14.601,14.6c0,4.149,1.726,7.875,4.5,10.524c1.8,1.801,4.175,4.301,5.025,5.625c1.75,2.726,5,11.976,5,11.976s3.325-9.25,5.1-11.976c0.825-1.274,3.05-3.6,4.825-5.399C28.774,23.813,30.6,20.012,30.6,15.737z"/>' +
	    '<circle style="fill:'+circleColor+';" cx="16" cy="16" r="5"/>' +
	    '<text dx="'+textX+'" dy="20" text-anchor="middle" style="font-size:10px;fill:#FFFFFF;">' + text + '</text>' +
	    '</svg>'
	  );
	  div.append(svg);

	  var dd = $("<canvas height='50px' width='50px'></cancas>");

	  var svgHtml = div[0].innerHTML;

	  canvg(dd[0], svgHtml);

	  var imgSrc = dd[0].toDataURL("image/png");
	  //"scaledSize" and "optimized: false" together seems did the tricky ---IE11  &&  viewBox influent IE scaledSize
	  //var svg = '<svg width="32px" height="43px"  viewBox="0 0 32 43" xmlns="http://www.w3.org/2000/svg">'
	  //    + '<path style="fill:#FFFFFF;stroke:#020202;stroke-width:1;stroke-miterlimit:10;" d="M30.6,15.737c0-8.075-6.55-14.6-14.6-14.6c-8.075,0-14.601,6.55-14.601,14.6c0,4.149,1.726,7.875,4.5,10.524c1.8,1.801,4.175,4.301,5.025,5.625c1.75,2.726,5,11.976,5,11.976s3.325-9.25,5.1-11.976c0.825-1.274,3.05-3.6,4.825-5.399C28.774,23.813,30.6,20.012,30.6,15.737z"/>'
	  //    + '<circle style="fill:' + color + ';" cx="16" cy="16" r="11"/>'
	  //    + '<text x="16" y="20" text-anchor="middle" style="font-size:10px;fill:#FFFFFF;">' + text + '</text>'
	  //    + '</svg>';
	  //var imgSrc = 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(svg);

	  var iconObj = {
	    size: new google.maps.Size(32, 43),
	    url: imgSrc,
	    scaledSize: new google.maps.Size(32, 43)
	  };

	  return iconObj;
	}