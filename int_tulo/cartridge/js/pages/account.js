'use strict';

var giftcert = require('../giftcert'),
	tooltip = require('../tooltip'),
	util = require('../util'),
	dialog = require('../dialog'),
	page = require('../page'),
	validator = require('../validator');

/**
 * @function
 * @description Initializes the events on the address form (apply, cancel, delete)
 * @param {Element} form The form which will be initialized
 */
function initializeAddressForm() {
	var $form = $('#edit-address-form');

	$form.find('input[name="format"]').remove();
	tooltip.init();
	//$("<input/>").attr({type:"hidden", name:"format", value:"ajax"}).appendTo(form);

	$("#dwfrm_profile_address_states_state").change(function () {
		console.log($("#dwfrm_profile_address_states_state :selected").text());
		$("#dwfrm_profile_address_states_state-span").text($("#dwfrm_profile_address_states_state :selected").text());
	});

	$("#dwfrm_profile_address_country").on('click', function (e) {
		$("#dwfrm_profile_address_country-span").text($("#dwfrm_profile_address_country :selected").text());
	});
	
	$('#dwfrm_profile_address_addressid').attr("maxlength", "50");
	$('#dwfrm_profile_address_firstname').attr("maxlength", "50");
	$('#dwfrm_profile_address_lastname').attr("maxlength", "50");
	$('#dwfrm_profile_address_address1').attr("maxlength", "50");
	$('#dwfrm_profile_address_address2').attr("maxlength", "50");
	$('#dwfrm_profile_address_city').attr("maxlength", "50");
	$('#dwfrm_profile_address_postal').attr("maxlength", "5");
	
	$form.on('click', '.apply-button', function (e) {
		e.preventDefault();
		if (!$form.valid()) {
			return false;
		}
		var url = util.appendParamToURL($form.attr('action'), 'format', 'ajax');
		var applyName = $form.find('.apply-button').attr('name');
		var options = {
			url: url,
			data: $form.serialize() + '&' + applyName + '=x',
			type: 'POST'
		};
		$.ajax(options).done(function (data) {
			if (typeof(data) !== 'string') {
				if (data.success) {
					dialog.close();
					page.refresh();
				} else {
					window.alert(data.message);
					return false;
				}
			} else {
				$('#dialog-container').html(data);
				account.init();
				tooltip.init();
				util.uniform();
			}
		});
	})
	.on('click', '.cancel-button, .close-button', function (e) {
		e.preventDefault();
		dialog.close();
	})
	.on('click', '.delete-button', function (e) {
		e.preventDefault();		
		if (window.confirm(String.format(Resources.CONFIRM_DELETE, Resources.TITLE_ADDRESS))) {
			var url = util.appendParamsToUrl(Urls.deleteAddress, {
				AddressID: $form.find('#addressid').val(),
				format: 'ajax'
			});
			$.ajax({
				url: url,
				method: 'POST',
				dataType: 'json'
			}).done(function (data) {
				if (data.status.toLowerCase() === 'ok') {
					dialog.close();
					page.refresh();
				} else if (data.message.length > 0) {
					window.alert(data.message);
					return false;
				} else {
					dialog.close();
					page.refresh();
				}
			});
		}
	});
	//postal code check. -- dwfrm_singleshipping_shippingAddress_addressFields_postal
	$('input.postal').first().blur(function () {
		var $postalcodetmp = this.value;
		IsValidZipCode($postalcodetmp);
		//console.log('test' + IsValidZipCode($postalcodetmp));
	});
	
	$('#dwfrm_profile_address_phone').keydown(function (e) {
		
		var key = e.charCode || e.keyCode || 0;
		var $phone = $(this);
		// Auto-format- do not expose the mask as the user begins to type
		if (key !== 8 && key !== 9) {
			if ($phone.val().length === 3) {
				$phone.val($phone.val() + '-');
			}
			if ($phone.val().length === 7) {
				$phone.val($phone.val() + '-');
			}
		}

		// Allow numeric (and tab, backspace, delete) keys only
		return (key == 8 ||
				key == 9 ||
				key == 46 ||
				(key >= 48 && key <= 57) ||
				(key >= 96 && key <= 105));
	})
	
	// add css class for apopobox validation on address
	$("#dwfrm_profile_address_address1").addClass("apopobox");
	validator.init();
}
/**
 * @private
 * @function
 * @description Toggles the list of Orders
 */
function toggleFullOrder () {
	$('.order-items')
		.find('li.hidden:first')
		.prev('li')
		.append('<a class="toggle">View All</a>')
		.children('.toggle')
		.click(function () {
			$(this).parent().siblings('li.hidden').show();
			$(this).remove();
		});
}
/**
 * @private
 * @function
 * @description Binds the events on the address form (edit, create, delete)
 */
function initAddressEvents() {
	var addresses = $('#addresses');
	if (addresses.length === 0) { return; }

	addresses.on('click', '.delete', function (e) {
		e.preventDefault();
		if (window.confirm(String.format(Resources.CONFIRM_DELETE, Resources.TITLE_ADDRESS))) {
			$.ajax({
				url: util.appendParamToURL($(this).attr('href'), 'format', 'ajax'),
				dataType: 'json'
			}).done(function (data) {
				if (data.status.toLowerCase() === 'ok') {
					page.redirect(Urls.addressesList);
				} else if (data.message.length > 0) {
					window.alert(data.message);
				} else {
					page.refresh();
				}
			});
		}
	});
}

function IsValidZipCode(zip) {
	var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
	if (!isValid && zip.length >0) {
		if ($(".postal .postalerror").length < 1) {
			$(".postal .field-wrapper").append("<span class='error postalerror'>" + Resources.INVALID_ZIP + "</span>");			
			$("#edit-address-form").find('button').attr("disabled", "disabled");
		}
		return false;
	} else {
		if ($(".postal .postalerror").length > 0) {			
			$(".postal span.postalerror").remove();
			$("#edit-address-form").find('button').removeAttr("disabled", "disabled");
		}
		return true;
	}
}
/**
 * @private
 * @function
 * @description Binds the events of the payment methods list (delete card)
 */
function initPaymentEvents() {
	$('.add-card').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href')
		});
	});

	var paymentList = $('.payment-list');
	if (paymentList.length === 0) { return; }

	util.setDeleteConfirmation(paymentList, String.format(Resources.CONFIRM_DELETE, Resources.TITLE_CREDITCARD));

	$('form[name="payment-remove"]').on('submit', function (e) {
		e.preventDefault();
		// override form submission in order to prevent refresh issues
		var button = $(this).find('.delete');
		$('<input/>').attr({
			type: 'hidden',
			name: button.attr('name'),
			value: button.attr('value') || 'delete card'
		}).appendTo($(this));
		var data = $(this).serialize();
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: data
		})
		.done(function () {
			page.redirect(Urls.paymentsList);
		});
	});
}
/**
 * @private
 * @function
 * @description init events for the loginPage
 */
function initLoginPage() {
	
	 if (localStorage.chkbx && localStorage.chkbx != '') {
         $('#dwfrm_login_rememberme').attr('checked', 'checked');
         $('#dwfrm_login_username').val(localStorage.usrname);
         $('#dwfrm_login_password').val(localStorage.pass);
         $('#dwfrm_login_rememberme').closest('span').addClass('checked');
     }

     $('#dwfrm_login_rememberme').click(function() {
         if ($('#dwfrm_login_rememberme').is(':checked')) {
             // save username and password
             localStorage.usrname = $('#dwfrm_login_username').val();
             localStorage.pass = $('#dwfrm_login_password').val();
             localStorage.chkbx = $('#dwfrm_login_rememberme').val();
         } else {
             localStorage.usrname = '';
             localStorage.pass = '';
             localStorage.chkbx = '';
         }
     });
	
	//o-auth binding for which icon is clicked
	$('.oAuthIcon').bind('click', function () {
		$('#OAuthProvider').val(this.id);
	});

	
	$('#password-reset').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href'),
			dialogClass: "password-reset-dialog",
			options: {
				title: Resources.FORGOTPASSWORD_TITLE,
				open: function () {
					validator.init();
					var $requestPasswordForm = $('[name$="_requestpassword"]'),
						$submit = $requestPasswordForm.find('[name$="_requestpassword_send"]');
						$($submit).on('click', function (e) {							
							if (!$requestPasswordForm.valid()) {
								return;
							}
							e.preventDefault();
							dialog.submit($submit.attr('name'));
						});
						// validate email
						$('#dwfrm_requestpassword_email').on('focusout blur', function () {							
							validateResetPasswordEmail($(this));	
						});
						$('#dwfrm_requestpassword_email').on('keypress bind', function () {							
							enableResetPassword($(this));	
						});
				}
			}
		});
	});
	
	$('.email input').first().on('focusout blur', function () {		
		validateResetPasswordEmail($(this));	
	});
	
	$('.email input').first().on('keypress bind', function () {		
		enableResetPassword($(this));	
	});
		
	$('#new-user').on('click', function (e) {
		e.preventDefault();		
		$('#tab2-li').addClass('ui-tabs-active ui-state-active');
		$('#tab1-li').removeClass('ui-tabs-active ui-state-active');
		$('#tabs-1').hide();
		$('#tabs-2').show();
	});
	
	$('#tab1-li').on('click', function (e) {
		e.preventDefault();		
		$('#tab1-li').addClass('ui-tabs-active ui-state-active');
		$('#tab2-li').removeClass('ui-tabs-active ui-state-active');
		$('#tabs-2').hide();
		$('#tabs-1').show();
	});
	
	$('#tab2-li').on('click', function (e) {
		e.preventDefault();		
		$('#tab2-li').addClass('ui-tabs-active ui-state-active');
		$('#tab1-li').removeClass('ui-tabs-active ui-state-active');
		$('#tabs-1').hide();
		$('#tabs-2').show();
	});
}


function validatePassword(pass) {
	var $continue = $('.form-row-button button');
	var passReg =/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
	if (pass.length > 0 && !passReg.test(pass)) {
		$continue.attr('disabled', 'disabled');
		if ($(".regpassword .errorEmailValidation").length < 1) {
			$(".regpassword .field-wrapper").append("<span class='error errorEmailValidation'>" + Resources.VALIDATE_PASS + "</span>");
		}
		return false;
	} else {
		$continue.removeAttr('disabled');
		if ($(".errorEmailValidation").length > 0) {
			$(".regpassword span.errorEmailValidation").remove();
		}
		return true;
	}
}

function validatePasswordConfirm(pass) {
	var $continue = $('.form-row-button button');
	var passReg =/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
	if (pass.length > 0 && !passReg.test(pass)) {
		$continue.attr('disabled', 'disabled');
		if ($(".regpasswordconfirm .errorEmailValidation").length < 1) {
			$(".regpasswordconfirm .field-wrapper").append("<span class='error errorEmailValidation'>" + Resources.VALIDATE_PASS + "</span>");
		}
		return false;
	} else {
		$continue.removeAttr('disabled');
		if ($(".errorEmailValidation").length > 0) {
			$(".regpasswordconfirm span.errorEmailValidation").remove();
		}
		return true;
	}
}


function validateResetPasswordEmail(obj) {
	var email = $(obj).val();
	var $continue = $('#resetPassword');
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	if (email.length > 0 && !emailReg.test(email)) {
		$continue.attr('disabled', 'disabled');
		$(obj).addClass('customerror');
		if ($(".email .errorEmailValidation").length < 1) {			
			$(".email .field-wrapper").append("<span class='error errorEmailValidation'>" + Resources.VALIDATE_EMAIL + "</span>");
		}
		return false;
	} else {
		$continue.removeAttr('disabled');
		if ($(".errorEmailValidation").length > 0) {
			$(obj).removeClass('customerror');
			$(".email span.errorEmailValidation").remove();
		}
		return true;
	}
}

function enableResetPassword(obj) {
	var email = $(obj).val();
	var $continue = $('#resetPassword');
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	if (email.length > 0 && !emailReg.test(email)) {
		$continue.attr('disabled', 'disabled');		
		return false;
	} else {
		$continue.removeAttr('disabled');
		if ($(".errorEmailValidation").length > 0) {
			$(obj).removeClass('customerror');
			$(".email span.errorEmailValidation").remove();
		}
		return true;
	}
}
/**
 * @private
 * @function
 * @description Binds the events of the order, address and payment pages
 */
function initializeEvents() {
	toggleFullOrder();
	initAddressEvents();
	initPaymentEvents();
	initLoginPage();
	initializeAddressForm();	
}

var account = {
	init: function () {
		initializeEvents();
		giftcert.init();
	},
	initCartLogin: function () {
		initLoginPage();
	}
};

module.exports = account;
