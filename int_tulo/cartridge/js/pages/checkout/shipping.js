'use strict';

var ajax = require('../../ajax'),
    formPrepare = require('./formPrepare'),
    progress = require('../../progress'),
    tooltip = require('../../tooltip'),
    util = require('../../util'),
    page = require('../../page'),
    dialog = require('../../dialog'),
    tealium = require('../../tealium'),
    utaghelper = require('../../utaghelper');

var shippingMethods;
/**
 * @function
 * @description Initializes gift message box, if shipment is gift
 */
function giftMessageBox() {
    // show gift message box, if shipment is gift
    $('.gift-message-text').toggleClass('hidden', $('input[name$="_shippingAddress_isGift"]:checked').val() !== 'true');
}

/**
 * @function
 * @description updates the order summary based on a possibly recalculated basket after a shipping promotion has been applied
 */
function updateSummary() {
    var $summary = $('#secondary.summary');
    // indicate progress
    progress.show($summary);

    // load the updated summary area
    $summary.load(Urls.summaryRefreshURL, function () {
        // hide edit shipping method link
        $summary.fadeIn('fast');
        $summary.find('.checkout-mini-cart .minishipment .header a').hide();
        $summary.find('.order-totals-table .order-shipping .label a').hide();
//        $('.checkout-mini-cart').mCustomScrollbar();
//		util.uniform();
//		var checkoutForm = $("form.address"),
//		city = checkoutForm.find('select[name$="_addressFields_cities_city"]'),
//		$formRow = city.closest(".form-row");
//		$formRow.find('#uniform-' + city.attr("name")).show();
//		city.show();
    });
}

function updateMiniSummary() {
    var $summary = $('#secondary.summary');

    // load the updated summary area
    $summary.load(Urls.summaryRefreshURL, function () {
        // hide edit shipping method link
        $summary.fadeIn('fast');       
        // init coupon event
        initCouponCode();
        // update order totals  
        $('.order-totals').html($summary.find('#order-totals').html());
        initRecycleFeeToolTip();
        if($(".time-slot.selected").length>0){
        	var deliveryMessageDate = $(".time-slot.selected").attr('data-formatted-date');
            var deliveryMessage = 'As soon as ' + deliveryMessageDate + ' via Red Carpet Delivery';
            $('div.delivery-message.red-carpet').text(deliveryMessage);
        }
        util.uniform();
    });
}

function initRecycleFeeToolTip(){	
	$(".recyclefeett").on({
	    mouseover: function() {
	    	$(this).closest('.recyclefeett').addClass('active-tt');
	    },
	    mouseout: function() {
	    	$(this).closest('.recyclefeett').removeClass('active-tt');
	    }
	});
}

/**
 * @function
 * @description Helper method which constructs a URL for an AJAX request using the
 * entered address information as URL request parameters.
 */
function getShippingMethodURL(url, extraParams) {
    var $form = $('.address');
    return util.appendParamsToUrl(url, extraParams);
}

/**
 * @function
 * @description selects a shipping method for the default shipment and updates the summary section on the right hand side
 * @param
 */
function selectShippingMethod(shippingMethodID) {
    // nothing entered
    if (!shippingMethodID) {
        return;
    }
    // attempt to set shipping method
    var url = getShippingMethodURL(Urls.selectShippingMethodsList, {shippingMethodID: shippingMethodID});
    ajax.getJson({
        url: url,
        callback: function (data) {
            updateSummary();
            if (!data || !data.shippingMethodID) {
                window.alert('Couldn\'t select shipping method.');
                return false;
            }
            // display promotion in UI and update the summary section,
            // if some promotions were applied
            $('.shippingpromotions').empty();

            // TODO the for loop below isn't doing anything?
            // if (data.shippingPriceAdjustments && data.shippingPriceAdjustments.length > 0) {
            // 	var len = data.shippingPriceAdjustments.length;
            // 	for (var i=0; i < len; i++) {
            // 		var spa = data.shippingPriceAdjustments[i];
            // 	}
            // }
        }
    });
}

/**
 * @function
 * @description Make an AJAX request to the server to retrieve the list of applicable shipping methods
 * based on the merchandise in the cart and the currently entered shipping address
 * (the address may be only partially entered).  If the list of applicable shipping methods
 * has changed because new address information has been entered, then issue another AJAX
 * request which updates the currently selected shipping method (if needed) and also updates
 * the UI.
 */
function updateShippingMethodList() {
    var $shippingMethodList = $('#shipping-method-list');
    if (!$shippingMethodList || $shippingMethodList.length === 0) { return; }
    var url = getShippingMethodURL(Urls.shippingMethodsJSON);

    ajax.getJson({
        url: url,
        callback: function (data) {
            if (!data) {
                window.alert('Couldn\'t get list of applicable shipping methods.');
                return false;
            }
            if (shippingMethods && shippingMethods.toString() === data.toString()) {
                // No need to update the UI.  The list has not changed.
                return true;
            }

            // We need to update the UI.  The list has changed.
            // Cache the array of returned shipping methods.
            shippingMethods = data;
            // indicate progress
            progress.show($shippingMethodList);

            // load the shipping method form
            var smlUrl = getShippingMethodURL(Urls.shippingMethodsList);
            $shippingMethodList.load(smlUrl, function () {
                $shippingMethodList.fadeIn('fast');
                // rebind the radio buttons onclick function to a handler.
                $shippingMethodList.find('[name$="_shippingMethodID"]').click(function () {
                    selectShippingMethod($(this).val());
                });

                // update the summary
                updateSummary();
                progress.hide();
                tooltip.init();
                util.uniform();
                //if nothing is selected in the shipping methods select the first one
                if ($shippingMethodList.find('.input-radio:checked').length === 0) {
                    $shippingMethodList.find('.input-radio:first').prop('checked', 'checked');
                }
            });
        }
    });
}

function LoadBillingCityStateDropdowns(data) {
    var checkoutForm = $("form.address");
    var postalCode = checkoutForm.find("input[name$='billing_billingAddress_addressFields_postal']");
    var city = checkoutForm.find('select[name$="billing_billingAddress_addressFields_cities_city"]');
    var state = checkoutForm.find('select[name$="billing_billingAddress_addressFields_states_state"]');
    var cityJSON = checkoutForm.find("input.cityJSON");
    if (data !== null) {
        city.empty();

        // Add "Choose a City"
        if (data.cities.length > 1) {
            city.append($("<option />").attr("value", "").text("Select City"));
        }

        for (var __i = 0; __i < data.cities.length; __i++) {
            var $opt = $("<option />");
            $opt.attr("value", data.cities[__i].city).text(data.cities[__i].city);

            // If there is only a single city object returned, auto select it.
            if (data.cities.length === 1) {
                $opt.attr("selected", "selected");
                city.val(data.cities[__i].city);
            }

            city.append($opt);

            if (__i === 0) {
                state.val(data.cities[__i].state);

                // Reset any error conditions on the state field
                state.closest(".form-row").removeClass("error").find(".error-message").hide();

                // State selected so hide drop down
                $(".state-col .dk_options_inner").css("display","none");
            }
        }
        // Enable the city/state select elements so they are submitted with form.
        city.removeAttr("disabled");
        state.removeAttr("disabled");
    }
}

function LoadShippingCityStateDropdowns(data) {
    var checkoutForm = $("form.address");
    var postalCode = checkoutForm.find("input[name$='billing_shippingAddress_addressFields_postal']");
    var city = checkoutForm.find('select[name$="billing_shippingAddress_addressFields_cities_city"]');
    var state = checkoutForm.find('select[name$="billing_shippingAddress_addressFields_states_state"]');
    var cityJSON = checkoutForm.find("input.cityJSON");
    if (data !== null) {
        city.empty();

        // Add "Choose a City"
        if (data.cities.length > 1) {
            city.append($("<option />").attr("value", "").text("Select City"));
        }

        for (var __i = 0; __i < data.cities.length; __i++) {
            var $opt = $("<option />");
            $opt.attr("value", data.cities[__i].city).text(data.cities[__i].city);

            // If there is only a single city object returned, auto select it.
            if (data.cities.length === 1) {
                $opt.attr("selected", "selected");
                city.val(data.cities[__i].city);
            }

            city.append($opt);

            if (__i === 0) {
                state.val(data.cities[__i].state);

                // Reset any error conditions on the state field
                state.closest(".form-row").removeClass("error").find(".error-message").hide();

                // State selected so hide drop down
                $(".state-col .dk_options_inner").css("display","none");
            }
        }
        // Enable the city/state select elements so they are submitted with form.
        city.removeAttr("disabled");
        state.removeAttr("disabled");
    }
}

function GetBillingCityStateFromZip(zipCode) {
    var checkoutForm = $("form.address");
    var postalCode = checkoutForm.find("input[name$='billing_billingAddress_addressFields_postal']");
    var city = checkoutForm.find('select[name$="billing_billingAddress_addressFields_cities_city"]');
    var state = checkoutForm.find('select[name$="billing_billingAddress_addressFields_states_state"]');
    var country = checkoutForm.find('select[name$="billing_billingAddress_addressFields_country"]');
    var cityJSON = checkoutForm.find("input.cityJSON");
    // 1. Verify a valid US 5-digit ZIP first, and that the same ZIP was not entered again
    if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCode)) { //&& zipCode != app.clientcache.SESSION_ZIP){

        postalCode.removeClass('error');
        postalCode.next('span.error').remove();
        $.ajax({
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: Urls.cityAndStateForZip,
            async: true,
            cache: false,
            data: {"zipcode": zipCode},

            beforeSend: function () {
                progress.show(".zip-city-state");
            },
            success: function (data) {
                if (data != null && data.cities.length > 0) {

                    // RESET : Make sure to enable the select if disabled and remove any fail over input element and reset cache for element.
                    var $formRow = city.closest(".form-row");
                    $formRow.find("input").remove();
                    $formRow.find("select").removeAttr("disabled").addClass('required');
                    $formRow.find('#uniform-' + city.attr("name")).show();
                    country.removeAttr("disabled");
                    LoadBillingCityStateDropdowns(data);

                    $.uniform.update(state);
                    $.uniform.update(city);

                    $('#dwfrm_singleshipping_shippingAddress_addressFields_cities_city-error').remove();
//					util.uniform();

                    // Fade-in City & State
//					$('.cover').remove();

                } else {
                    // No city objects returned from service, switch over to manual input.
                    CityFailOver("");
                }
            },
            error: function (data) {
                CityFailOver("");
            },
            complete: function () {
                progress.hide(".zip-city-state");
                StateIntercept(state.children().filter(':selected').first().val());
            }
        });
    }
}

function GetShippingCityStateFromZip(zipCode) {
    var checkoutForm = $("form.address");
    var postalCode = checkoutForm.find("input[name$='billing_shippingAddress_addressFields_postal']");
    var city = checkoutForm.find('select[name$="billing_shippingAddress_addressFields_cities_city"]');
    var state = checkoutForm.find('select[name$="billing_shippingAddress_addressFields_states_state"]');
    var country = checkoutForm.find('select[name$="billing_shippingAddress_addressFields_country"]');
    var cityJSON = checkoutForm.find("input.cityJSON");
    // 1. Verify a valid US 5-digit ZIP first, and that the same ZIP was not entered again
    if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCode)) { //&& zipCode != app.clientcache.SESSION_ZIP){

        postalCode.removeClass('error');
        postalCode.next('span.error').remove();
        $.ajax({
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: Urls.cityAndStateForZip,
            async: true,
            cache: false,
            data: {"zipcode": zipCode},

            beforeSend: function () {
                progress.show(".zip-city-state");
            },
            success: function (data) {
                if (data != null && data.cities.length > 0) {

                    // RESET : Make sure to enable the select if disabled and remove any fail over input element and reset cache for element.
                    var $formRow = city.closest(".form-row");
                    $formRow.find("input").remove();
                    $formRow.find("select").removeAttr("disabled").addClass('required');
                    $formRow.find('#uniform-' + city.attr("name")).show();
                    country.removeAttr("disabled");
                    LoadShippingCityStateDropdowns(data);

                    $.uniform.update(state);
                    $.uniform.update(city);

                    $('#dwfrm_billing_shippingAddress_addressFields_cities_city-error').remove();
//					util.uniform();

                    // Fade-in City & State
//					$('.cover').remove();

                } else {
                    // No city objects returned from service, switch over to manual input.
                    CityFailOver("");
                }
            },
            error: function (data) {
                CityFailOver("");
            },
            complete: function () {
                progress.hide(".zip-city-state");
                StateIntercept(state.children().filter(':selected').first().val());
            }
        });
    }
}
function StateIntercept(state) {
    if (state == 'HI') {
        var params = {cid: 'hawaii-intercept'};

        $.ajax({
            url: Urls.liveContent,
            data: params,
            dataType: 'json',
            success: function (data) {
                if (data.content && data.content.onlineFlag) {
                    var html = '<div class="live-content-wrapper">' + data.content.body + '</div>';

                    dialog.open({
                        html: html,
                        options: {
                            maxHeight: 635,
                            dialogClass: 'live-content-dialog',
                            title: data.content.name
                        }
                    });

                    if ($(".postal .errorHawaiiValidation").length < 1) {
                        $(".postal .field-wrapper").append("<span class='error errorHawaiiValidation'><a title='Click for more info' class='error hawaii-intercept'>Click for more info</a></span>");
                    }
                    $('form#dwfrm_singleshipping_shippingAddress').find('button').addClass('disabled');
                    $('.postal input').addClass("error");
                    $('#hawaii-message').show();
                    $('#hawaii-small').show();
                }
            },
            error: function (data) {
            }
        });
    } else {
        $(".postal").find("span.errorHawaiiValidation").remove();
        $('form#dwfrm_singleshipping_shippingAddress').find('button').removeClass('disabled');
        $('#hawaii-message').hide();
        $('#hawaii-small').hide();
    }
}

function CityFailOver(cityOverride, stateOverride) {
    var checkoutForm = $("form.address");
    var city = checkoutForm.find('select[name$="_addressFields_cities_city"]');
    var state = checkoutForm.find('select[name$="_addressFields_states_state"]');
    var _name = city.attr("name");
    var $formRow = city.closest(".form-row");
    $formRow.find("select").attr("disabled", "disabled").removeClass('required');
    $formRow.find('#uniform-' + _name).hide();
    city.empty();

    $formRow.find("input.input-text").remove();
    var $input = $("<input type='text' class='input-text required city-txt' id='" + _name + "' name='" + _name + "' value='" + (cityOverride.length > 3 ? cityOverride : '') + "' max-length='50' />");
    var fieldWrapper = $formRow.find('.field-wrapper');
    fieldWrapper.append($input);

    // Switch cache target.
    city = $input;

    state.removeAttr("disabled");
    if (stateOverride) {
        state.val(stateOverride);
    } else {
        state.val("");
    }
    state.trigger('change');
    $.uniform.update(state);
}

function KeepAddress() {
    var checkoutForm = $("form.address");
    var postalCode = checkoutForm.find("input[name$='_postal']");
    var city = checkoutForm.find('select[name$="_addressFields_cities_city"]');
    var state = checkoutForm.find('select[name$="_addressFields_states_state"]');
    var keepAddress = checkoutForm.find("input[name$='_keepAddress']");
    var fleetwiseToken = checkoutForm.find("input[name$='_fleetwiseToken']");
    var addToAddressBook = checkoutForm.find("input[name$='_addToAddressBook']");
    var addressSuggestion = checkoutForm.find("#addressSuggestion");
    // Keep Address
    $.ajax({
        type: "post",
        dataType: "html",
        url: Urls.keepAddress,
        async: true,
        cache: false,
        beforeSend: function () {
//			progress.show(".delivery-schedule-box");
        },
        success: function (data) {
            if (data !== null) {
                if (data.indexOf("<body>") == -1) {
                    // if there are delivery dates, disable continue button and show calendar
                    if (data.indexOf('delivery-dates-container') != -1) {
                        $("#delivery-schedule-button").click(function (e) {
                            e.preventDefault();
                            $('#delivery-schedule-button').hide();
                            $("#delivery-dates-container").show();
                        });
                    } else {
                        // no dates available, possibly bad zip, enable continue button
                        // if indeed it's a bad zip, clicking continue will take the user back to cart
                        $("button.checkout-button").removeClass("disabled");
                    }

                } else {
                    if (data.indexOf("pt_cart") > -1) {
                        window.location = urls.cartShow;
                    } else {
                        // no dates available, possibly bad zip, enable continue button
                        // if indeed it's a bad zip, clicking continue will take the user back to cart
                        $("button.checkout-button").removeClass("disabled");
                    }
                }
            } else {
                // no data available, possibly bad zip, enable continue button
                // if indeed it's a bad zip, clicking continue will take the user back to cart
                $("button.checkout-button").removeClass("disabled");
            }
        },
        error: function (data) {
//			$cache.deliveryScheduleBox.hide();

            // no dates available, possibly bad zip, enable continue button
            // if indeed it's a bad zip, clicking continue will take the user back to cart
            $("button.checkout-button").removeClass("disabled");
        },
        complete: function () {
//			progress.hide(".delivery-schedule-box");

            fleetwiseToken.val("USER_UNDECIDED");
//			deliveryDate.val("USER_UNDECIDED");

            updateSummary();
        }
    });
}

function initialCityStateZipLoad() {
    var checkoutForm = $("form.address");
    var postalCode = checkoutForm.find("input[name$='_postal']");
    var state = checkoutForm.find('select[name$="_addressFields_states_state"]');
    // Fade-in City & State
    if ($.trim(postalCode.val()).length === 5) {
        if (window.initialCity) {
            CityFailOver(window.initialCity, state.val());
            updateSummary();
        } else {
            //hideCityState();
        }
    } else {
        //hideCityState();
    }
}


function initDeliverySelection() {
		var $zipCodeForm = $('#dialog-container').find('form');
		var $spSubmit = $($zipCodeForm.find('[name="submit"]'));
		$($spSubmit).on('click', function (e) {
			e.preventDefault();
			var $postalcodetmp = $('#dwfrm_storepicker_postalCode').val();
			if (IsValidZipCode($postalcodetmp)) {
				getDeliveryDates($zipCodeForm);
			}
		});
	
    	var windowWidth = window.innerWidth;
		// 2- For all devices under or at 768px, use vertical orientation
		if(windowWidth < 768) {
			
			$('.schedule').not('.slick-initialized').slick({
				vertical: true,
				verticalSwiping: true,
				infinite: false,
				slidesToShow: 1,
				slidesToScroll: 1,
				swipe: false,
                swipeToSlide: false,   
	            prevArrow: $('.slick-prev-arrow'),
	            nextArrow: $('.slick-next-arrow')
			});
		} 
		// 3- For all devices over 768px, use horizontal orientation
		else {
			$('.schedule').not('.slick-initialized').slick({
				vertical: false,
				infinite: false,
				slidesToShow: 1,
				slidesToScroll: 1,
				verticalSwiping: false,
				swipe: false,
                swipeToSlide: false,
	            prevArrow: $('.slick-prev-arrow'),
	            nextArrow: $('.slick-next-arrow'),
			});
		};		
		
        $('.slick-prev-arrow').attr('tabindex',0);
        $('.slick-next-arrow').attr('tabindex',0);
        $('.slick-prev-arrow').click(function(e) {
        	e.preventDefault();
            $('.schedule').slick('slickGoTo', parseInt($('.schedule').slick('slickCurrentSlide')) - 1);
         });
        
        $('.slick-next-arrow').on('click', function (e) {
        	e.preventDefault();
        	$('.schedule').slick('slickGoTo', parseInt($('.schedule').slick('slickCurrentSlide')) + 1);
            var currentIndex = $('.schedule').slick('slickCurrentSlide');
            var slickSliderLength = $('.slick-slide').length;
            if (currentIndex == (slickSliderLength - 1) && $(this).data('current-week-date') != false) {
                var params = {
                        format: 'ajax',
                        currentWeekDate: $(this).data('current-week-date')
                };
                progress.show($('#dialog-container'));
                ajax.load({
                    url: util.appendParamsToUrl(Urls.getATPDeliveryDatesForNextWeekTulo, params),
                    callback: function (response) {
                        if (response != '') {                           
                            $('.schedule').slick('slickAdd', response, currentIndex, true);
                            if ($('.slick-next-arrow').data('current-week-date') == false) {
                                $('.schedule').slick('slickRemove', (currentIndex + 1));
                            }
                            if ($('.slick-next-arrow').data('current-count') == 0) {
                            	$('.schedule').slick('slickRemove',  currentIndex);
                            }
                            progress.hide();
                            initDeliverySelection();
                        }
                        progress.hide();
                        $('.slick-prev-arrow').attr('tabindex',0);
                        $('.slick-next-arrow').attr('tabindex',0);
                    }
                });
            }
            $('.slick-prev-arrow').attr('tabindex',0);
			$('.slick-next-arrow').attr('tabindex',0);
        });
        
        $('#dwfrm_storepicker_postalCode')
        .attr("maxlength", "5")       
        .on('blur focusout', function () {
			var $postalcodetmp = this.value;
			IsValidDeliveryZipCode($postalcodetmp);	
		});
               
        $('#dwfrm_storepicker_postalCode')        
        .on('keypress', function (e) {
        	 if (e.which != 13 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {           
                 return false;
            }			
		});
        
        $('#dwfrm_storepicker_postalCode').on('keyup', function () {
			var zip = this.value;
			var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
			if (!isValid) {				
				return false;
			} else {
				$("#updateZip").removeAttr("disabled", "disabled");		
				return true;
			}
		});
       
}

function initialDeliveryDates() {
    initDeliverySelection();
   
    var deliveryForm = $('.delivery-dates');
    
	deliveryForm.on("click", ".time-slot", function () {
		changeDeliveryDate(this);
	});
	deliveryForm.on("keypress", ".time-slot", function (ev) {
		if (ev.keyCode == 13 || ev.which == 13) {
			changeDeliveryDate(this);
		}
	});
	 //default select first slot
    $(".sched-date").find(".time-slot").each(function(){
	    if($(this).attr('data-delivery-time').length > 0) {
	    	$(".sched-date").removeClass("selected-deliverydate");
	    	$(".time-slot").removeClass("selected");
            $(this).closest(".sched-date").addClass('selected-deliverydate');
	    	$(this).addClass('selected');
	    	changeDeliveryDate(this);
	    	return false; // to exit each
	    }
	});
    
    // select slot in popup
    $(".sched-date").each(function(){				
	    if($(this).attr('data-delivery-date') == $(this).attr('data-selected-date')) {		    		    	
	    	$(this).find(".time-slot").each(function(){				
			    if($(this).attr('data-delivery-time') == $(this).attr('data-selected-time')) {
			    	$(".sched-date").removeClass("selected-deliverydate");
			    	$(".time-slot").removeClass("selected");
		            $(this).closest(".sched-date").addClass('selected-deliverydate');
			    	$(this).addClass('selected');
			    	changeDeliveryDate(this);
			    	return false; //// to exit each
			    }
			});
	    }
	});
	//choose delivery button click
	var $s = $('#dialog-container');		
	var $sp = $($s.find('[name="continue"]'));
	$($sp).on('click', function (e) {
		e.preventDefault();
		if ($(this).attr('data-step') == '2') {			
			// update delivery date and time to null 
			/** set pdict fleetwiseToken to currently selected value **/
	        $('#dwfrm_singleshipping_shippingAddress_addressFields_fleetwiseToken').val("fleetwisetoken");
	        var result = false;
	        var deliveryTime = $(".selected-time").text();
	        var deliveryDate = $(".selected-date").text();
	        var deliveryZone = $(".selected-zone").text();
	        var deliveryZip = $('.shippingpostal input').val();
	        
	        $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryDate').val(deliveryDate);
	        $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryTime').val(deliveryTime);	        
	       
        	var params = {
                    format: 'ajax',
                    deliveryDate: deliveryDate,
                    deliveryTime: deliveryTime,
                    zipCode: deliveryZip,
                    deliveryZone : deliveryZone
            };		
    		$.ajax({
    			url: util.appendParamsToUrl(Urls.updateDeliveryDates, params),
    			type: 'POST',
    			async: false,
				success: function () {									
					result = true;
				},
				failure: function () {
					window.alert(Resources.SERVER_ERROR);
					window.location.reload();
				}
    		});
    		dialog.close();
    		// trigger submit action of billing
    		if (result) {
    			$('#billing_save').click();
			}
			
		} else {

			/** set pdict fleetwiseToken to currently selected value **/
	        $('#dwfrm_singleshipping_shippingAddress_addressFields_fleetwiseToken').val("fleetwisetoken");
	        
	        var deliveryTime = $(".selected-time").text();
	        var deliveryDate = $(".selected-date").text();
	        var deliveryZone = $(".selected-zone").text();
	        var deliveryZip = $(".selected-zip").text();
	        $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryDate').val(deliveryDate);
	        $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryTime').val(deliveryTime);
	        
	        if (($(this).attr('data-prev-selected-date') == deliveryDate) && ($(this).attr('data-prev-selected-time') == deliveryTime ) && ($(this).attr('data-prev-selected-zip') == deliveryZip )) {
	        	dialog.close();
			}
	        else{
	        	var params = {
	                    format: 'ajax',
	                    deliveryDate: deliveryDate,
	                    deliveryTime: deliveryTime,
	                    zipCode: deliveryZip,
	                    deliveryZone : deliveryZone
	            };		
	    		$.ajax({
	    			url: util.appendParamsToUrl(Urls.updateDeliveryDates, params),
	    			type: 'POST',
					success: function () {
						dialog.close();
						window.location.reload();
					},
					failure: function () {
						window.alert(Resources.SERVER_ERROR);
						window.location.reload();
					}
	    		});
	        }
		}
        
	});
	
}

// return suffix according to the given day
function getDateSuffix(day) {
	if (day > 3 && day < 21) return 'th'; 
	switch (day % 10) {
		case 1:  return "st";
		break;
	    case 2:  return "nd";
	    break;
	    case 3:  return "rd";
	    break;
	    default: return "th";
	}
}

function changeDeliveryDate(obj) {
    if (!($(obj).hasClass("full"))) {
        /** grab time slot clicked and mark correct day and time slot as selected **/
        $(".time-slot").removeClass("selected");
        $(".header").removeClass("selected");
        $(obj).addClass("selected");
        $(obj).parent().children(".header").addClass("selected");
        $(".currently-chosen").removeClass("hide");
        var textdate = $(obj).closest(".sched-date").find(".week-date").data('date');
        var day = new Date(textdate).getDate();
        var suffix = getDateSuffix(day); 
        textdate += suffix;
        var deliveryMessageDate = $(obj).closest(".sched-date").find(".week-date").data('formatted-date');
        var deliveryMessage = 'As soon as ' + deliveryMessageDate + ' via Red Carpet Delivery';
        $('div.delivery-message.red-carpet').text(deliveryMessage);
        
        if($(".chosen-time").length > 0)
        {
        	 textdate += '<span> between </span>' + $(obj).text().replace('-', ' and ').replace(RegExp("pm", "g"), " pm").replace(RegExp("am", "g"), " am").trim();
        	 $(".chosen-time").html(textdate);	
        }
      
        else if($("#shipping-date-msg").length > 0)
        {
        	textdate = "Get Red Carpet Delivery <strong>"+textdate+"</strong>";
            textdate += ' between <strong>' + $(obj).text().replace(RegExp(" pm", "g"), "pm").replace(RegExp(" am", "g"),  "am").trim()+"</strong> <span class='color-black'>or choose another time below:</span>";
        	$("#shipping-date-msg").html(textdate);	
        }
        $(".sched-date").removeClass("selected-deliverydate");
        $(obj).closest(".sched-date").addClass('selected-deliverydate');            
        $(".selected-time").text($(obj).attr('data-delivery-time'));
        $(".selected-date").text($(obj).attr('data-date'));
        $(".selected-zone").text($(obj).attr('data-zone'));
        $(".selected-zip").text($(obj).attr('data-zip'));
        $("#choosedeliverydate").removeClass("disabled-btn");
        
        // from delivery options page, not dialog
        if($("#shipping-date-msg").length > 0)
        {
        	$('#dwfrm_singleshipping_shippingAddress_addressFields_fleetwiseToken').val("fleetwisetoken");
 	        
 	        var deliveryTime = $(".selected-time").text();
 	        var deliveryDate = $(".selected-date").text();
 	        var deliveryZone = $(".selected-zone").text();
 	        var deliveryZip = $(".selected-zip").text();
 	        $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryDate').val(deliveryDate);
 	        $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryTime').val(deliveryTime);
        }	
    }
}

function getDeliveryDates(deliveryDateForm) {
	progress.show($('#dialog-container'));
	$.ajax({
		type: 'get',
		url: deliveryDateForm.attr("action"),
		data: deliveryDateForm.serialize(),
		dataType: 'html',
		success: function (html) {
			renderDeliveryDates(html);			
			progress.hide();
		}.bind(this),
		failure: function () {
			window.alert(Resources.SERVER_ERROR);
		}
	});
}
function renderDeliveryDates(response) {
	// replace dialog with updated content		
	$('#dialog-container').html(response);
	initialDeliveryDates();

}
function initDeliverydate(e) {
	dialog.open({
        url: $(e.target).attr('href'),
		options:{
			open: function () {				
				initialDeliveryDates();
			},	

			width: 'auto', // overcomes width:'auto' and maxWidth bug
			maxWidth: 780,
			height: 'auto',				
			modal: true,
			fluid: true, //new option
			resizable: true,
			title:	$(e.target).attr('title'),
			draggable: true,
			responsive: true,
			autoResize:true            
		}
	});		
}

function callUtagLinkATPFailure (atpFailedCoreProductsArray) {
	if (atpFailedCoreProductsArray && atpFailedCoreProductsArray != "null") {
		var sliceArray = atpFailedCoreProductsArray.slice(1,-1);
		utag.link({ "eventCategory" : ["ATP"], "eventLabel" : sliceArray.split(","), "eventAction" : ["fail"]});
	}
}

function initDeliveryDatesAddress(zipCode, phone) {
	 var params = {
             format: 'ajax',
             coStep: '2',
             zipCode: zipCode,
             phone: phone
     };
	dialog.open({
		url: util.appendParamsToUrl(Urls.getATPDeliveryDates, params),
		options:{
			open: function () {
				initialDeliveryDates();
				var atpFailedCoreProductsArray = $('#delivery-dates-container').attr("data-atp-failedcoreproductsarray");
				callUtagLinkATPFailure(atpFailedCoreProductsArray);
			},	

			width: 'auto', // overcomes width:'auto' and maxWidth bug
			maxWidth: 780,
			height: 'auto',				
			modal: true,
			fluid: true, //new option
			resizable: true,
			title:	'Oops! Your address doesn’t match',
			draggable: true,
			responsive: true,
			autoResize:true            
		}
	});	
}

$("#shippingmethodsave").on('click', function (e) {
	e.preventDefault();


	/** set pdict fleetwiseToken to currently selected value **/
    $('#dwfrm_singleshipping_shippingAddress_addressFields_fleetwiseToken').val("fleetwisetoken");
    
    var deliveryTime = $(".selected-time").text();
    var deliveryDate = $(".selected-date").text();
    var deliveryZone = $(".selected-zone").text();
    var deliveryZip = $(".selected-zip").text();
    $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryDate').val(deliveryDate);
    $('#dwfrm_singleshipping_shippingAddress_addressFields_deliveryTime').val(deliveryTime);
    var $form =  $("#dwfrm_singleshipping_shippingAddress");
    if (($("#choosedeliverydate").attr('data-prev-selected-date') == deliveryDate) && ($("#choosedeliverydate").attr('data-prev-selected-time') == deliveryTime ) && ($("#choosedeliverydate").attr('data-prev-selected-zip') == deliveryZip )) {
    	$form.submit();
	}
    else{
    	var params = {
                format: 'ajax',
                deliveryDate: deliveryDate,
                deliveryTime: deliveryTime,
                zipCode: deliveryZip,
                deliveryZone : deliveryZone
        };		
		$.ajax({
			url: util.appendParamsToUrl(Urls.updateDeliveryDates, params),
			type: 'POST',
			success: function () {
				$form.submit();
			},
			failure: function () {
				window.alert(Resources.SERVER_ERROR);
				window.location.reload();
			}
		});
    }

});

$('#changedeliverydate').on('click', function (e) {
	e.preventDefault();
	var deliveryoption = $('.delivery-options-container').find('.input-radio:checked').val();				
	if (deliveryoption == 'shipped') {
		progress.show();
		initDeliverydate(e);		
	}		
});

function IsValidDeliveryZipCode(zip) {
	var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
	if (!isValid && zip.length >0) {
		if ($(".deliverypostal .postalerror").length < 1) {
			$(".deliverypostal .field-wrapper").append("<span class='error postalerror'>" + Resources.INVALID_ZIP + "</span>");
			$("#updateZip").attr("disabled", "disabled");
		}
		return false;
	} else {
		if ($(".deliverypostal .postalerror").length > 0) {			
			$(".deliverypostal span.postalerror").remove();	
			$("#updateZip").removeAttr("disabled", "disabled");
		}
		return true;
	}
}

function IsValidZipCode(zip) {
	var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
	if (!isValid) {
		$("#updateZip").attr("disabled", "disabled");
		return false;
	} else {
		$("#updateZip").removeAttr("disabled", "disabled");		
		return true;
	}
}

function validateEmail2(email) {
    var $continue = $('.form-row-button button');
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (!emailReg.test(email)) {
        $continue.attr('disabled', 'disabled');
        if ($(".emailaddr .error").length < 1) {
            $(".emailaddr .field-wrapper").append("<span class='error'>" + Resources.VALIDATE_EMAIL + "</span>");
        }
        return false;
    } else {
        $continue.removeAttr('disabled');
        if ($(".error").length > 0) {
            $(".emailaddr .error").remove();
        }
        return true;
    }
}

function IsValidBillingZipCode(zip) {
	var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
	if (!isValid && zip.length >0) {
		if ($(".billingpostal .postalerror").length < 1) {
			$(".billingpostal .field-wrapper").append("<span class='error postalerror'>" + Resources.INVALID_ZIP + "</span>");			
			$("#dwfrm_billing").find('button').attr("disabled", "disabled");
		}
		return false;
	} else {
		if ($(".billingpostal .postalerror").length > 0) {			
			$(".billingpostal span.postalerror").remove();
			$("#dwfrm_billing").find('button').removeAttr("disabled", "disabled");
		}
		return true;
	}
}

function IsValidShippingZipCode(zip) {
	var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
	if (!isValid && zip.length >0) {
		if ($(".shippingpostal .postalerror").length < 1) {
			$(".shippingpostal .field-wrapper").append("<span class='error postalerror'>" + Resources.INVALID_ZIP + "</span>");			
			$("#dwfrm_billing").find('button').attr("disabled", "disabled");
		}
		return false;
	} else {
		if ($(".shippingpostal .postalerror").length > 0) {			
			$(".shippingpostal span.postalerror").remove();
			$("#dwfrm_billing").find('button').removeAttr("disabled", "disabled");
		}
		return true;
	}
}

function IsValidDeliveryOptionZipCode(zip) {
	var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
	if (!isValid && zip.length >0) {
		if ($("#form-zip-Change .postalerror").length < 1) {
			$("#form-zip-Change .fields-wrap").append("<span class='error postalerror'>" + Resources.INVALID_ZIP + "</span>");			
			$("#btnZipChange").attr("disabled", "disabled");
		}
		return false;
	} else {
		if ($("#form-zip-Change .postalerror").length > 0) {			
			$("#form-zip-Change span.postalerror").remove();
			$("#btnZipChange").removeAttr("disabled", "disabled");
		}
		return true;
	}
}

function EnableDisableDeliveryZip(zip) {
	var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
	if (!isValid) {				
		$("#btnZipChange").attr("disabled", "disabled");
		return false;
	}		
	 else {	
		$("#btnZipChange").removeAttr("disabled", "disabled");
		return true;
	}	
}
// these 2 functions are on product.js (should include js?)
function addProductSelectStore(storeId, pid, format) {
    $.ajax({
        url: Urls.changePickupLocation,
        type: 'POST',
        data: {storeId: storeId, format: format}
    }).done(function () {
        dialog.close();
        page.redirect(Urls.cartShow);
    });
 }

function setPreferredStore(storeId) {
    User.storeId = storeId;
    $.ajax({
        url: Urls.setPreferredStore,
        type: 'POST',
        data: {storeId: storeId}
    });
}

function useShippingAsBilling(checkoutForm){
	
	var firstName = checkoutForm.find("input[name$='billing_shippingAddress_addressFields_firstName']").val();
	var lastName = checkoutForm.find("input[name$='billing_shippingAddress_addressFields_lastName']").val();
	var address1 = checkoutForm.find("input[name$='billing_shippingAddress_addressFields_address1']").val();
	var address2 = checkoutForm.find("input[name$='billing_shippingAddress_addressFields_address2']").val();
	var phone = checkoutForm.find("input[name$='billing_shippingAddress_addressFields_phone']").val();
    var postalCode = checkoutForm.find("input[name$='billing_shippingAddress_addressFields_postal']").val();
    var city = checkoutForm.find('input[name$="billing_shippingAddress_addressFields_city"]').val();
    var state = checkoutForm.find('select[name$="billing_shippingAddress_addressFields_states_state"]').val();
    
    $("input[name$='billing_billingAddress_addressFields_firstName']").val(firstName);
    $("input[name$='billing_billingAddress_addressFields_lastName']").val(lastName);
    $("input[name$='billing_billingAddress_addressFields_address1']").val(address1);
    $("input[name$='billing_billingAddress_addressFields_address2']").val(address2);
    $("input[name$='billing_billingAddress_addressFields_phone']").val(phone);    
    $("input[name$='billing_billingAddress_addressFields_postal']").val(postalCode);
    $('input[name$="billing_billingAddress_addressFields_city"]').val(city);
    $('select[name$="billing_billingAddress_addressFields_states_state"]').val(state);
}

function clearBillingForm(){	
    
    $("input[name$='billing_billingAddress_addressFields_firstName']").val('');
    $("input[name$='billing_billingAddress_addressFields_lastName']").val('');
    $("input[name$='billing_billingAddress_addressFields_address1']").val('');
    $("input[name$='billing_billingAddress_addressFields_address2']").val('');
    $("input[name$='billing_billingAddress_addressFields_phone']").val('');    
    $("input[name$='billing_billingAddress_addressFields_postal']").val('');
    $('input[name$="billing_billingAddress_addressFields_city"]').val('');
    $('select[name$="billing_billingAddress_addressFields_states_state"]').val('');
}

//Init coupon code checkout delivery page
function initCouponCode() {	
   
	$('#coupon-form').find('.cart-coupon-code a.coupon-label').on('click', function (e) {
		e.preventDefault();
	    $(this).closest('.cart-coupon-code').toggleClass('active-drop');
	    var couponCode = $('#coupon-form').find('input[name$="_couponCode"]');
        //remove existing error message 
        if ($(".cart-coupon-code .error").length > 0) {        	
        	$(couponCode).removeClass('error');        	
        	$(".cart-coupon-code .error").remove();
        }
	});
	
	$('#coupon-form').find('input[name$="_couponCode"]').on('keydown', function (e) {
        if (e.which === 13 && $(this).val().length === 0) { return false; }else if(e.which === 13 && $(this).val().length > 0){
        	e.preventDefault();
        	$('#coupon-form').find("#add-coupon").click();
        }
    });
	$('#coupon-form').find("#add-coupon").unbind('click');
	$('#coupon-form').find("#add-coupon").on('click', function (e) {
    	e.preventDefault();
        var couponCode = $('#coupon-form').find('input[name$="_couponCode"]');
        //remove existing error message 
        if ($(".cart-coupon-code .error").length > 0) {
        	$(couponCode).removeClass('error');        	
        	$(".cart-coupon-code span.error").remove();        	
        }
        var code = couponCode.val().trim();        
        if (code.length === 0) {        	
        	$(couponCode).addClass('error');        	
        	$(".cart-coupon-code").append("<span class='error'>" + Resources.COUPON_CODE_MISSING + "</span>");
            return;
        }
        // indicate progress
        progress.show('#coupon-form');
        couponCode.val('');
        var url = util.appendParamsToUrl(Urls.addCoupon, {couponCode: code, format: 'ajax'});
        $.getJSON(url, function (data) {
            var fail = false;
            var msg = '';
            if (!data) {
                msg = Resources.BAD_RESPONSE;
                fail = true;
            } else if (!data.success) {
                msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
                fail = true;
            }
            if (fail) {
            	$(couponCode).addClass('error');
            	$(".cart-coupon-code").append("<span class='error'>" + msg + "</span>");
            	progress.hide('#coupon-form');
                return;
            }
          
            if (data.success) {            	
            	updateMiniSummary();
            	if(data.action === 'addcoupon' && data.qualifiedBonusProducts != null) {        			
        			utaghelper.addProducttUtagByJson(data.qualifiedBonusProducts);
        		}
            }
        });
    });
	
	$('#coupon-form').find("#deleteCouponCheckout").unbind('click');
	$('#coupon-form').find("#deleteCouponCheckout").on('click', function (e) {
    	e.preventDefault();
    	
        var $form =  $('#coupon-form');
        $form.append( "<input type='hidden' class='form_submittype' name='" + this.name + "' value='true'>" );
    	var ajaxdest = $form.attr('action');
    	var formData = $form.serialize();
    	var refreshDeliveryOptions = false;
    	$(".mini-cart-product").each(function(){				
    	    if($(this).attr('data-bonus-product') == 'true') {		    		    	
    	    	refreshDeliveryOptions = true;
    	    	return false;
    	    }
    	});
    	progress.show($form);
		$.post(ajaxdest, formData).done(function(data) {			
			if (refreshDeliveryOptions) {
				loadDeliveryOptions();
			} else {			
				updateMiniSummary();
			}
			if(data!=null){
				utaghelper.removeProducttUtagByJson(data);
			}
		});       
    });
	
	
}

function initBOPIS(){
	// init all our variables after content is replaced	
	var $mainContainer = $(".store-delivery");
	var $preferredStoreForm	= $mainContainer.find('#SetPreferredStore');
	var $psSubmit			= $preferredStoreForm.find('button[type="submit"]');
	tealium.init();
	// Store info flip tile cick event
	$(document).on('click', '.store-info.more', function(e){
		$(this).closest('li').addClass('more-info-active');
		$(this).closest('li').removeClass('store-container-active');
	});
	$(document).on('click', '.store-more-info .btn-back', function(e){
		$(this).closest('li').addClass('store-container-active');
		$(this).closest('li').removeClass('more-info-active');
	});

	// disable selectability of unavailable stores
	$('.store-result.unavailable').find('input[type="radio"]').on('click', function () {
		return false;
	});
	
	
    		
	// if no preferred store is picked, disable the continue button
	if ($preferredStoreForm.find('.preferred-store').length == 0) {
		$psSubmit.attr('disabled', true);
	}		
	// update classes for styling when stores are selected
	$($preferredStoreForm.find('.select-preferred-store input')).change(function () {
		$("#shippingmethodsave").addClass('disabled-btn');
		tealium.trigger("Try-in-store", "Select Preferred Store", "", $(this).attr('data-tealium'));
		var $buttonContainer = $(this).closest('div');
		var $buttonText = $buttonContainer.find('.select-preferred-store-text');
		var $storeContainer = $(this).closest('li');
		setPreferredStore2(this.value);
		// reset other store containers
		$preferredStoreForm.find('.select-preferred-store-text').text(Resources.SELECT_STORE);
		$preferredStoreForm.find('li').removeClass('preferred-store');
		// update the current
		$buttonText.text(Resources.PREFERRED_STORE);
		$storeContainer.addClass('preferred-store');
		$.ajax({
			url: Urls.getAvailablePickupStore,
			type: 'GET'
		});
		$("#shippingmethodsave").removeClass('disabled-btn');
		var availableOn = $storeContainer.find('.store-result-date-bottom').data('availableon');
		if(availableOn != "" && availableOn != null)
		{
			var pickUpMsg = Resources.PICKUP_ASSOONAS +" " +availableOn+" ";
			$("#pickup-date-msg").html(pickUpMsg);
		}
		
		//$("div.selected-store").find(".store-name").html($storeContainer.find("span.storename").html());
	});

}

function loadDeliveryOptions()
{
	$("#shippingmethodsave").addClass('disabled-btn');
	$.get( Urls.loadDeliveryOptions )
 		.done(function( text ){
 		var $response = $( text );	
 		if($response)
 		{      
 			//trigger utag link if atp fails for core products
 			var atpFailedCoreProductsArray = $(text).filter('#homeDelivery').attr("data-atp-failedcoreproductsarray");
 			callUtagLinkATPFailure(atpFailedCoreProductsArray);
 			
 			$("div#delivery-options").html($response);	 
 			util.uniform();
 			initialDeliveryDates();
 			initBOPIS();
 			updateMiniSummary();
 			if($("input#instorepickup").length > 0 && $('input[name=dwfrm_cart_deliveryoptionradio]:checked').val() == "instorepickup" && 
 					$("input#instorepickup").data('store-id') == null)
 			{
 				
 			}
 			else
 			{
 				$("#shippingmethodsave").removeClass('disabled-btn');
 				progress.hide();
 			}
 			
 		}
 	});	
}

function setPreferredStore2(storeId) {
	User.storeId = storeId;
	$.ajax({
		url: Urls.setPreferredStore2,
		type: 'POST',
		data: {storeId: storeId}
	});
}

exports.init = function () {
   
    $('body').on('click', '.rcd-delivery-info-detail', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(this).attr('href'),
			options: {
				title:$(this).attr('title'),
				dialogClass: 'rcd-delivery-info-dialog',
				open: function () {
					$(".ui-dialog-title").css('text-align', 'center');
				}
			}
		});
	});
    
    $("#dwfrm_billing_billingAddress_email_emailAddress").on({
	  keydown: function(e) {
	    if (e.which === 32)
	      return false;
	  },
	  paste: function() {
		  var targetId = $(this);
		    setTimeout(function(){
		        targetId.val($.trim(targetId.val()));
		    }, 1);
	  }
	});
    
    $(".billingpostal input").on('keydown focusout blur',  function(e){
	  if ($(".billingpostal .error-message").length > 0) {
	   $(".billingpostal div.error-message").remove();
	  }
    });
    
	$(".shippingpostal input").on('keydown focusout blur',  function(e){
	  if ($(".shippingpostal .error-message").length > 0) {
	   $(".shippingpostal div.error-message").remove();
	  }
	});
     
    $('.billingpostal input').first().on('blur', function () {
    	var $postalcodetmp = this.value;
    	IsValidBillingZipCode($postalcodetmp);	
	});
    
    $('.shippingpostal input').first().on('blur', function () {
    	var $postalcodetmp = this.value;
    	IsValidShippingZipCode($postalcodetmp);	
	});
    
    $(".billingpostal input").on('input', function () {       
        this.value = this.value.replace(/[^0-9]+/g , '');
     });
    
    $(".shippingpostal input").on('input', function () {        
        this.value = this.value.replace(/[^0-9]+/g , '');
     });
   
    $(".billingcity input")	
	.on('keypress keydown focusout blur',  function(e){
		if ($(".billingcity .error-message").length > 0) {
			$(".billingcity div.error-message").remove();
		}
	});
    
    $("#dwfrm_billing_billingAddress_addressFields_states_state").on('change focusout',  function(e){
		if ($(".billingstate .error-message").length > 0) {
			$(".billingstate div.error-message").remove();
			$(".billingstate").removeClass('error');
		}
	});
    
    $("input[name^='dwfrm_billing_shippingAddress_addressFields_city']")	
	.on('keypress keydown focusout blur',  function(e){
		if ($(".shippingcity .error-message").length > 0) {
			$(".shippingcity div.error-message").remove();
		}
	});
    
    $("#dwfrm_billing_shippingAddress_addressFields_states_state")	
	.on('change focusout',  function(e){
		if ($(".shippingstate .error-message").length > 0) {
			$(".shippingstate div.error-message").remove();
			$(".shippingstate").removeClass('error');
		}
	});
    // add css class for apopobox validation on address
//	$("#dwfrm_billing_billingAddress_addressFields_address1").addClass("apopobox");
	$("#dwfrm_billing_shippingAddress_addressFields_address1").addClass("apopobox");

	$('#changedeliverydate').on('click', function (e) {
		e.preventDefault();
		var deliveryoption = $('.delivery-options-container').find('.input-radio:checked').val();				
		if (deliveryoption == 'shipped') {
			initDeliverydate(e);
		}		
	});  
   
    formPrepare.validateForm();
    $("body").on('click', '.change-store', function (e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href'),
            title: $(e.target).attr('title'),
            options: {
                height: 600
            },
            callback: function () {
                $('#dwfrm_pickupinstore').trigger('submit');
            }
        });
    });
    $("body").on('click', '.select-store', function (e) {
        var selectedStore = $(e.target).val();
        setPreferredStore(selectedStore);
        $("div[class*='store']").removeClass('selected');
        $('.store_' + selectedStore).addClass('selected');
        $(".select-store:contains('Preferred Store')").closest('button').first().text('Select Store');
        $(e.target).text('Preferred Store');
    });
    $('.pickup-in-store').on('click', function (e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href'),
            title: $(e.target).attr('title'),
            options: {
                height: 600
            },
            callback: function () {
                $('#dwfrm_pickupinstore').trigger('submit');
            }
        });
    });
    $('body').on('submit', '#dwfrm_pickupinstore', function (e) {
        e.preventDefault();
        // serialize the form and get the post url
        var buttonName = $('#dwfrm_pickupinstore').find('.pickupinstore').attr('name');
        var options = {
            url: Urls.pickupInStore,
            data: $('#dwfrm_pickupinstore').serialize() + '&' + buttonName + '=x',
            type: 'POST'
        };
        $.ajax(options).done(function (data) {
            if (typeof(data) !== 'string') {
                if (data.success) {
                    //dialog.close();
                    //page.refresh();
                } else {
                    window.alert(data.message);
                    return false;
                }
            } else {
                $('#dialog-container').html(data);
                //dialog.close();
                //page.refresh();
            }
        });

    });
    $("body").on('click', '.continue-with-select-store', function (e) {
        var selectedStore = $('.selected').attr('data-storeid');
        var pid = $('input[name="pid"]').val();
        var format = 'ajax';
        addProductSelectStore(selectedStore, pid, format);
    });
    
    if ($('.useasshippingaddress-wrap .input-checkbox').is(":checked")) {
        $('#checkout-billingInfo').fadeOut('slow');
    }
    $('.useasshippingaddress-wrap .input-checkbox').change(function(){
        if (this.checked) {
        	var checkoutForm = $("form.address");        	
        	useShippingAsBilling(checkoutForm);
            $('#checkout-billingInfo').fadeOut('slow');
        }
        else {
        	clearBillingForm();
            $('#checkout-billingInfo').fadeIn('slow');
        }                   
    });
    $('#billing_save').on('click', function (e) {    	
    	if ($('.useasshippingaddress-wrap .input-checkbox').is(":checked")) {
    		var checkoutForm = $("form.address");        	
        	useShippingAsBilling(checkoutForm);
        }
    	var isValid = jQuery('form.address').validate().checkForm();
    	if (isValid) {
        	var zoneChanged = false;
        	var zipCode = $('#dwfrm_billing_shippingAddress_addressFields_postal').val();
        	var phone = $('#dwfrm_billing_shippingAddress_addressFields_phone').val();
        	var params = {
                    format: 'ajax',
                    zipCode: zipCode
            };
        	progress.show();
    		$.ajax({
    			url: util.appendParamsToUrl(Urls.verifyDeliveryZone, params),
    			type: 'get',
    			async: false,
    			success: function (response) {
    		    	if (response) {
    		    		zoneChanged = true;
    				}		    	
    			},
    			failure: function () {
    				window.alert(Resources.SERVER_ERROR);
    				
    			}
    		});
    		
    		if (zoneChanged) {
        		e.preventDefault();    		
        		initDeliveryDatesAddress(zipCode, phone);
    		}
		}
		
    });
    $('#zipCode-text').on('blur focusout', function () {
    	var $postalcodetmp = this.value;
    	IsValidDeliveryOptionZipCode($postalcodetmp);
	});
    
    $('#zipCode-text').on('keyup', function () {
    	var $postalcodetmp = this.value;
    	EnableDisableDeliveryZip($postalcodetmp);
	});
    
    $('#zipCode-text').on('paste', function(e) {
        setTimeout(function() {
         EnableDisableDeliveryZip($('#zipCode-text').val());
        }, 1);
    });

    $('#zipCode-text').on('input', function (event) {
        this.value = this.value.replace(/[^0-9]+/g , '');
    });
    
    $('#changeZip').on('click', function (e) {
    	$('#zipchange-wrap').hide();
        $('#form-zip-Change').css( "display", "inline-block" );
        $('#zipCode-text').select().focus();
    });

    $("#btnZipChange").unbind('click').click(function (e) {  
    	e.preventDefault();    	
    	var zipCode = $(this).closest('form').find('#zipCode-text').val();
    	if(zipCode == "" || zipCode ==  null || zipCode == undefined)
    	{
    		return;
    	}
    	var params = {
                format: 'ajax',
                zipCode: zipCode
        };
    	
    	$.ajax({
			url: util.appendParamsToUrl(Urls.updateDeliveryZip, params),
			type: 'post',			
			success: function (response) {
		    	if (response) {
		    		progress.show();
		    		$('#form-zip-Change').css( "display", "none" );
		            $('#deliveryZipCode').text(zipCode);
		            $('#zipchange-wrap').show();
		    		loadDeliveryOptions();
				}
		    	else{
		    		$('#form-zip-Change').css( "display", "none" );
		    		$('#zipchange-wrap').show();		           
		    	}
			},
			failure: function () {
				window.alert(Resources.SERVER_ERROR);
				
			}
		});
    });
    
    $("#btnUpdateZip").unbind('click').click(function (e) {  
    	e.preventDefault();    	
    	var zipCode = $(this).closest('form').find('#zipCode-text').val();
    	if(zipCode == "" || zipCode ==  null || zipCode == undefined)
    	{
    		return;
    	}
    	var params = {
                format: 'ajax',
                zipCode: zipCode
        };
    	
    	$.ajax({
			url: util.appendParamsToUrl(Urls.updateDeliveryZip, params),
			type: 'post',			
			success: function (response) {
				window.location = Urls.deliveryOptionsPage;
			},
			failure: function () {
				window.alert(Resources.SERVER_ERROR);
				
			}
		});
    });
    
    
    initCouponCode();
    
    // calculate delivery options
//    $(document).ready(function(){
    	if($("div#delivery-options").length>0){
    		loadDeliveryOptions();
    		
    		// This function is for the event handling on the cart page for when user swaps between shipping and bopis 
    		$('.delivery-options-container').on("change",'.input-radio', function (e) {
    			var deliveryoption = $(this).val();
    			var storeId = $(this).attr('data-store-id');
    			$('#shippingmethodsave').addClass("disabled-btn");
    			if (deliveryoption == 'shipped') {
    				$("#delivery-grid").removeClass('delivery-schedule-hidden');
    				$("#bopis-grid").hide();
    				
    			}
    			else if (deliveryoption == 'instorepickup') {
    				$("#bopis-grid").show();
    				$("#delivery-grid").addClass('delivery-schedule-hidden');
    				
    			}
    			$.ajax({
    				url: Urls.changeDeliveryOption,
    				type: 'POST',
    				data: {deliveryoption: deliveryoption, storeId: storeId},
    				success: function (data) {
    					if (deliveryoption == 'shipped') {
    						$('#shippingmethodsave').removeAttr("disabled", "disabled");
    						$('#shippingmethodsave').removeClass("disabled-btn");					
    						
    					} else if ((deliveryoption == 'instorepickup') && storeId == 'null') {
    						$('#shippingmethodsave').attr("disabled", "disabled");
    						$('#shippingmethodsave').addClass("disabled-btn");
    					} else if ((deliveryoption == 'instorepickup')) {
    						$('#shippingmethodsave').removeAttr("disabled", "disabled");
    						$('#shippingmethodsave').removeClass("disabled-btn");	
    					}
    				},
    				async: false
    			});

    		});
    	}    	
	    	
//     });
};

exports.updateShippingMethodList = updateShippingMethodList;
