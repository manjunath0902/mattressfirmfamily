'use strict';

var util = require('../../util');
var shipping = require('./shipping');
var progress = require('../../progress');
var ajax = require('../../ajax');
var formPrepare = require('./formPrepare');

/**
 * @function
 * @description Selects the first address from the list of addresses
 */
exports.init = function () {
	var $form = $('.address');
	var originalwidth =  $form.find("div.selector").width();
	// select address from list
	$('select[name$="_addressList"]', $form).on('change', function () {
		var selected = $(this).children(':selected').first();
		var selectedAddress = $(selected).data('address');
		var screenwidth = $(window).width();
		var currentwidth =  $form.find(".select-address div.selector").width();
		if (!selectedAddress) { return; }
		util.fillAddressFields(selectedAddress, $form);
		//shipping.updateShippingMethodList();
		// re-validate the form
		util.uniform();
		var cPostalcode = $('.postal').val();
		var checkoutForm = $("form.address");
		var postalCode = checkoutForm.find("input[name$='_postal']").val();
		var newselectdiv = checkoutForm.find("div.selector");
		var newselectbox =  checkoutForm.find("select[name$='_addressList']").width();
		if (newselectbox > currentwidth) {
			checkoutForm.find("select[name$='_addressList']").width(originalwidth);
			newselectdiv.attr('style', 'width: ' + originalwidth + 'px !important');
		}
		GetCityStateFromZip(postalCode);
		//$form.validate().form();
	});
};


function LoadCityStateDropdowns(data) {
	var checkoutForm = $("form.address");
	var postalCode = checkoutForm.find("input[name$='_postal']");
	var city = checkoutForm.find('select[name$="_addressFields_cities_city"]');
	var state = checkoutForm.find('select[name$="_addressFields_states_state"]');
	var cityJSON = checkoutForm.find("input.cityJSON");
	if (data !== null) {
		city.empty();

		// Add "Choose a City"
		if (data.cities.length > 1) {
			city.append($("<option />").attr("value", "").text("Select City"));
		}

		for (var __i = 0; __i < data.cities.length; __i++) {
			var $opt = $("<option />");
			$opt.attr("value", data.cities[__i].city).text(data.cities[__i].city);

			// If there is only a single city object returned, auto select it.
			if (data.cities.length === 1) {
				$opt.attr("selected", "selected");
				city.val(data.cities[__i].city);
			}

			city.append($opt);

			if (__i === 0) {
				state.val(data.cities[__i].state);

				// Reset any error conditions on the state field
				state.closest(".form-row").removeClass("error").find(".error-message").hide();

				// State selected so hide drop down
				$(".state-col .dk_options_inner").css("display","none");
			}
		}
		// Enable the city/state select elements so they are submitted with form.
		city.removeAttr("disabled");
		state.removeAttr("disabled");
	}
}
function GetCityStateFromZip(zipCode) {
	var checkoutForm = $("form.address");
	var postalCode = checkoutForm.find("input[name$='_addressFields_postal']");
	var city = checkoutForm.find('select[name$="_addressFields_cities_city"]');
	var state = checkoutForm.find('select[name$="_addressFields_states_state"]');
	var country = checkoutForm.find('select[name$="_addressFields_country"]');
	var cityJSON = checkoutForm.find("input.cityJSON");
	// 1. Verify a valid US 5-digit ZIP first, and that the same ZIP was not entered again
	if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCode)) { //&& zipCode != app.clientcache.SESSION_ZIP){
		postalCode.removeClass('error');
		postalCode.next('span.error').remove();
		$.ajax({
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			url: Urls.cityAndStateForZip,
			async: true,
			cache: false,
			data: {"zipcode": zipCode},

			beforeSend: function () {
				progress.show(".zip-city-state");
			},
			success: function (data) {
				if (data != null && data.cities.length > 0) {

					// RESET : Make sure to enable the select if disabled and remove any fail over input element and reset cache for element.
					var $formRow = city.closest(".form-row");
					$formRow.find("input").remove();
					$formRow.find("select").removeAttr("disabled").addClass('required');
					$formRow.find('#uniform-' + city.attr("name")).show();
					country.removeAttr("disabled");
					LoadCityStateDropdowns(data);

					$.uniform.update(state);
					$.uniform.update(city);

					$('#dwfrm_singleshipping_shippingAddress_addressFields_cities_city-error').remove();
//					util.uniform();

					// Fade-in City & State
//					$('.cover').remove();

				} else {
					// No city objects returned from service, switch over to manual input.
					CityFailOver("");
				}
			},
			error: function (data) {
				CityFailOver("");
			},
			complete: function () {
				progress.hide(".zip-city-state");
			}
		});
	}
}

function CityFailOver(cityOverride, stateOverride) {
	var checkoutForm = $("form.address");
	var city = checkoutForm.find('select[name$="_addressFields_cities_city"]');
	var state = checkoutForm.find('select[name$="_addressFields_states_state"]');
	var _name = city.attr("name");
	var $formRow = city.closest(".form-row");
	$formRow.find("select").attr("disabled", "disabled").removeClass('required');
	$formRow.find('#uniform-' + _name).hide();
	city.empty();
	$formRow.find("input.input-text").remove();
	var $input = $("<input type='text' class='input-text required city-txt' id='" + _name + "' name='" + _name + "' value='" + (cityOverride.length > 3 ? cityOverride : '') + "' max-length='50' />");
	var fieldWrapper = $formRow.find('.field-wrapper');
	fieldWrapper.append($input);

	// Switch cache target.
	city = $input;

	state.removeAttr("disabled");
	if (stateOverride) {
		state.val(stateOverride);
	} else {
		state.val("");
	}
	state.trigger('change');
	$.uniform.update(state);
}
