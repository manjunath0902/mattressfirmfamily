/**
 * (c) 2009-2014 Demandware Inc. Subject to standard usage terms and conditions
 * For all details and documentation:
 * https://bitbucket.com/demandware/sitegenesis
 */

'use strict';

var countries = require('./countries'),
dialog = require('./dialog'),
minicart = require('./minicart'),
page = require('./page'),
tooltip = require('./tooltip'),
hoverIntent = require('./hoverintent'),
util = require('./util'),
validator = require('./validator'),
googleAnalytics = require('./google-analytics'),
content = require('./content'),
responsiveSlots = require('./responsiveslots/responsiveSlots'),
amplience = require('./amplience'),
zipzone = require('./zipzone'),
progress = require('./progress'),
customjs = require('./custom'),
tealium = require('./tealium'),
sfgeolocdialog = require('./sfgeolocdialog'),
sfemaildialog = require('./sfemaildialog');
//if jQuery has not been loaded, load from google cdn
if (!window.jQuery) {
	var s = document.createElement('script');
	s.setAttribute('src', 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js');
	s.setAttribute('type', 'text/javascript');
	document.getElementsByTagName('head')[0].appendChild(s);
}

require('./jquery-ext')();
require('./cookieprivacy')();
//require('./captcha')();


function initializeEvents() {
	var controlKeys = ['8', '13', '46', '45', '36', '35', '38', '37', '40', '39'];
	$('body')
	.on('keydown', 'textarea[data-character-limit]', function (e) {
		var text = $.trim($(this).val()),
		charsLimit = $(this).data('character-limit'),
		charsUsed = text.length;

		if ((charsUsed >= charsLimit) && (controlKeys.indexOf(e.which.toString()) < 0)) {
			e.preventDefault();
		}
	}).on('change keyup mouseup', 'textarea[data-character-limit]', function () {
		var text = $.trim($(this).val()),
		charsLimit = $(this).data('character-limit'),
		charsUsed = text.length,
		charsRemain = charsLimit - charsUsed;

		if (charsRemain < 0) {
			$(this).val(text.slice(0, charsRemain));
			charsRemain = 0;
		}

		$(this).next('div.char-count').find('.char-remain-count').html(charsRemain);
	});

	$('.secondary-navigation .toggle').click(function () {
		$(this).toggleClass('expanded').next('ul').toggle();
	});
	// add generic toggle functionality
	$('.toggle').next('.toggle-content').hide();
	$('.toggle').click(function () {
		$(this).toggleClass('expanded').next('.toggle-content').toggle();
	});

	// subscribe email box
	var $subscribeEmail = $('.subscribe-email');
	if ($subscribeEmail.length > 0)	{
		$subscribeEmail.focus(function () {
			var val = $(this.val());
			if (val.length > 0 && val !== Resources.SUBSCRIBE_EMAIL_DEFAULT) {
				return; // do not animate when contains non-default value
			}

			$(this).animate({color: '#999999'}, 500, 'linear', function () {
				$(this).val('').css('color', '#333333');
			});
		}).blur(function () {
			var val = $.trim($(this.val()));
			if (val.length > 0) {
				return; // do not animate when contains value
			}
			$(this).val(Resources.SUBSCRIBE_EMAIL_DEFAULT)
			.css('color', '#999999')
			.animate({color: '#333333'}, 500, 'linear');
		});
	}
	$('#zipfield').on('focus', function () {
		$(this).select();
		this.setSelectionRange(0,5);
	});
	$('#main').on('focus', '#pdp-delivery-zip', function () {
		$(this).select();
		this.setSelectionRange(0,5);
		setTimeout(function () {
			$('#pdp-delivery-zip').get(0).setSelectionRange(0, 5);
		}, 1);
	});
	$('.details').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href'),
			title: $(e.target).attr('title'),
			options: {
				height: 600
			}
		});
	});
	$('.privacylink').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href'),
			options: {
				height: 600,
				title: $(e.target).attr('title')
			}
		});
	});
	/*$('.privacy-policy').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href'),
			options: {
				height: 600,
				title: $(e.target).attr('title')
			}
		});
	});*/
	$('.dialogify').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href'),
			options: {
				title: $(e.target).attr('title')
			}
		});
	});

	// Footer chat button
	$('#contentChatWithUs').on('click', function () {
		$('.header-chat a[id^="liveagent_button_online"]').trigger('click');
	});

	// main menu toggle
	$('.menu-toggle').on('click', function () {
		$('#wrapper').toggleClass('menu-active');
		$('footer').toggleClass('footer-active');
	});
	$('.menu-category li .menu-item-toggle').on('click touchstart', function (e) {
		if (util.isMobileSize()) {
			var $parentLi = $(e.target).closest('li');
			e.preventDefault();
			$parentLi.find('.level-2').toggle();
			if ($parentLi.attr('class') == 'active') {
				window.location.href = $parentLi.find('a').attr('href');
			} else {
				$parentLi.siblings('li').removeClass('active').find('.menu-item-toggle i').removeClass('fa-chevron-down active').addClass('fa-chevron-right');
				$parentLi.siblings('li').find('li.active').removeClass('active').find('.menu-item-toggle i').removeClass('fa-chevron-down active').addClass('fa-chevron-right');
				$parentLi.toggleClass('active');
				$(e.target).find('i').toggleClass('fa-chevron-right fa-chevron-down active');
			}
		}
	});
	//top navigation pause
	var hoverconfig = {
			over: function () {
				$(this).find('div.level-2').css('display', 'block');
				$(this).children('a.has-sub-menu').addClass('open');
				$(this).addClass('open');
				var divwidth = $(this).find('div.level-2').width();
				var offset = $(this).find('div.level-2').offset();
				var positionleft = offset.left;
				var screenwidth = $(window).width();
				$('#grid-sort-header').blur();
				$('#grid-paging-header').blur();
				var positionright = screenwidth - (positionleft + divwidth);
				if (screenwidth > 767 && screenwidth < 1025) {
					if (positionright < 50) {
						$(this).find('div.level-2').css('right', 0);
					}
					return false;
				}
			},
			out: function () {
				$(this).find('div.level-2').css('display', 'none');
				$(this).children('a.has-sub-menu').removeClass('open');
			},
			timeout: 100,
			sensitivity: 3,
			interval: 100
	};

	var touchConfig = function () {
		$(document).off('touchend', '.menu-category > li > a');
		// fix for touch tablet devices: prevent first click
		if (util.isMobile() == true && screen.width > util.mobileWidth) {
			$(document).on('touchend', '.menu-category > li > a', function (e) {
				if ($(this).siblings('.level-2').is(':hidden') == true) {
					e.preventDefault();
					// hide all opened menus
					$('.menu-category > li').find('.level-2').css('display', 'none');
					$('.menu-category > li').children('a.has-sub-menu').removeClass('open');
					//open current submenu
					$(this).siblings('.level-2').css('display', 'block');
					$(this).addClass('open');
					var divwidth = $(this).siblings('.level-2').width();
					var offset = $(this).siblings('.level-2').offset();
					var positionleft = offset.left;
					var screenwidth = screen.width;
					var positionright = screenwidth - (positionleft + divwidth);
					if (screenwidth > 767 && screenwidth < 1025) {
						if (positionright < 50) {
							$(this).siblings('.level-2').css('right', 0);
						}
						return false;
					}
					//console.log('ddd' + positionright + 'ddd' + screenwidth);
				}
			})
		}
	};

	touchConfig();

	if (screen.width > util.mobileWidth) {
		$('.level-1 li').hoverIntent(hoverconfig);
	}
	$(window).resize(function () {
		if (screen.width > util.mobileWidth) {
			$('.level-1 li').removeClass('active').hoverIntent(hoverconfig);
		} else {
			$('.level-1 li').unbind("mouseenter").unbind("mouseleave");
			$('.level-1 li').removeProp('hoverIntentT');
			$('.level-1 li').removeProp('hoverIntentS');
		}
		touchConfig();
	});
	$('.print-page').on('click', function () {
		window.print();
		return false;
	});
	function validatesignupEmail(email) {
		var $continue = $('.home-email');
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		if (!emailReg.test(email) || email == "" || email == null || email ==  undefined) {
			//$continue.attr('disabled', 'disabled');
			if ($(".emailsignup #email-alert-address-error").length < 1) {

				$(".emailsignup #email-alert-address").after("<span id='email-alert-address-error' class='error'>" + Resources.VALIDATE_EMAIL + "</span>");
				$(".emailsignup #email-alert-address").removeClass('valid');
				$(".emailsignup #email-alert-address").addClass('error');
				$(".emailsignup #email-alert-address").attr('aria-invalid', true);
				$(".emailsignup #email-alert-address").attr('aria-describedby', "email-alert-address-error");
			} else {
				if ($(".emailsignup #email-alert-address-error").text().length == 0) {
					$(".emailsignup #email-alert-address").removeClass('valid');
					$(".emailsignup #email-alert-address").addClass('error');
					$(".emailsignup #email-alert-address-error").html(Resources.VALIDATE_EMAIL);
					$(".emailsignup #email-alert-address").attr('aria-invalid', true);
					$(".emailsignup #email-alert-address").attr('aria-describedby', "email-alert-address-error");
				} else {
					//console.log('22' + $(".emailsignup #email-alert-address-error").length);
				}
			}
			$(".emailsignup #email-alert-address-error").css('display','block');
			return false;
		} else {
			//$continue.removeAttr('disabled');
			if ($(".emailsignup #email-alert-address-errorr").length > 0) {
				$(".emailsignup #email-alert-address-error").remove();
				$(".emailsignup #email-alert-address").removeClass('error');
				$(".emailsignup #email-alert-address").addClass('valid');
				$(".emailsignup #email-alert-address").attr('aria-invalid', false);
				$(".emailsignup #email-alert-address").removeAttr('aria-describedby');
			}
			return true;
		}
	}
	/*email signup Ajax*/
	$('#email-alert-signup').on('submit', function (e) {
		e.preventDefault();
		$('#email-alert-signup').find("button[name='home-email']").attr('disabled', true);
		if($('#email-alert-signup').hasClass("submitted")){return false;}
		$('#email-alert-signup').addClass("submitted");
		var $this = $(this),
			emailSignupInput = $(this).find('#email-alert-address'),
			validationErrorBlock = $('#email-alert-address-error');
		var signupInputValue = $(this).find(emailSignupInput).val();
		var validatetest = validatesignupEmail(signupInputValue);
		if (validatetest && signupInputValue.length > 1) {
			var _zipCode = $('#footer-social-zipCode').val();
			var _siteId = $('#footer-social-siteId').val();
			var _leadSource = $('#footer-social-leadSource').val();
			var _optOutFlag = $('#footer-social-optOutFlag').val();
			var _gclid = $.cookie("_ga");
			var params = {emailAddress: signupInputValue, zipCode: _zipCode, leadSource: _leadSource, siteId: _siteId, optOutFlag: _optOutFlag, gclid: _gclid};

			$.ajax({
				url: Urls.sfEmailSubscription,
				data: params,
				type: 'post',
				success: function (data) {
					$this.hide().parent().append($(data));
				},
				error: function (errorThrown) {
					$('#email-alert-signup').find("button[name='home-email']").removeAttr("disabled");
					$('#email-alert-signup').removeClass("submitted");
					console.log(errorThrown);
				}
			});
		}
		else{
			$('#email-alert-signup').find("button[name='home-email']").removeAttr("disabled");
			$('#email-alert-signup').removeClass("submitted");
			e.stopImmediatePropagation();
			return false;
		}

	});

	$('.catalog').on('mouseenter', function () {
		var flyout = $('.product-flyout');
		var $this = $(flyout[1]);
		$this.addClass('active');
	});

	$('.product-flyout').on('mouseleave', function () {
		var flyout = $('.product-flyout');
		var $this = $(flyout[1]);
		$this.removeClass('active');
	});

	$('.mobile-menu').on('touchstart', function () {
		var $this = $('.mobile-flyout');
		if ($this.hasClass('active')) {
			$this.removeClass('active');
			$('.mobile-menu').removeClass('active');
		} else {
			$this.addClass('active');
			$('.mobile-menu').addClass('active');
		}
	});

	$('.mobile-shop').on('touchstart', function () {
		var flyout = $('.product-flyout');
		var mobileFlyout = $(flyout[0]);

		if (mobileFlyout.hasClass('active')) {
			mobileFlyout.removeClass('active');
		} else {
			mobileFlyout.addClass('active');
		}
	});
	//open live chat
	$('.chatlink').on('click', function (e) {
		e.preventDefault();
		if($('#helpButtonSpan').length > 0)
		{
			$('#helpButtonSpan').click();
		}
	});
	$('body').on('click','#helpButtonSpan', function (e) {

		utag.link(
				{event_type : "chat", event_category : "click", event_label : "Chat_open"}
				);
	});
	/**
	 *  Buy online pick up in store (BOPIS)  and Try in Store(TIS) functionality
	 */

	// BOPIS / Try In Store init zip form
	function initStorePicker(e) {
		tealium.trigger("Try-in-store", "Initiate try in store", "", $(e.toElement).attr('data-tealium'));
		dialog.open({
			url: $(e.target).attr('href'),
			options:{
				open: function () {
					/*var $storePickerForm = $('#dialog-container').find('form#StorePickerZip');
					var $spSubmit = $($storePickerForm.find('[name="submit"]'));
					$($spSubmit).on('click', function (e) {
						e.preventDefault();
						getStores($storePickerForm);
					});
					 */
					initStorePickerEvents();
				},
				width: '820',
				title:	$(e.target).attr('title'),
				draggable: false
			}
		});
		$('body').scrollTop(0);

	}
	// Triggers
	$('#main').on('click', 'a.store-picker', function (e) {
		e.preventDefault();
		var deliveryoption = $('.delivery-options-container').find('.input-radio:checked').val();
		if (deliveryoption == 'instorepickup') {
			progress.show();
			initStorePicker(e);
		}
	});
	$('.pickup-in-store').on('click', function (e) {
		e.preventDefault();
		initStorePicker(e);
	});


	// Helper functions
	function setPreferredStore(storeId) {
		User.storeId = storeId;
		$.ajax({
			url: Urls.setPreferredStore2,
			type: 'POST',
			data: {storeId: storeId}
		});
	}

	function addProductSelectStore(storeId, pid, format) {
		$.ajax({
			url: Urls.changePickupLocation,
			type: 'POST',
			data: {storeId: storeId, format: format, pid: pid}
		}).done(function () {
			dialog.close();
			page.redirect(Urls.cartShow);
		});
	}
	function getStores(storePickerForm) {
		tealium.trigger("Try-in-store", "Enter zip code", "", storePickerForm.find('input[id*=postalCode]').val());
		progress.show($('#dialog-container'));
		$.ajax({
			type: 'POST',
			url: storePickerForm.attr("action"),
			data: storePickerForm.serialize(),
			dataType: 'html',
			success: function (html) {
				renderStores(html);
				progress.hide();
			}.bind(this),
			failure: function () {
				window.alert(Resources.SERVER_ERROR);
			}
		});
	}
	function renderStores(html) {
		// replace dialog with updated content
		$('#dialog-container').html(html);
		initStorePickerEvents();

	}
	function initStorePickerEvents(){
		// init all our variables after content is replaced
		var $dialog				= $('#dialog-container');
		var $changeLink			= $dialog.find('#change-location');
		var $storeZipForm		= $dialog.find('form#StorePickerZip');
		var $preferredStoreForm	= $dialog.find('#SetPreferredStore');
		var $psSubmit			= $preferredStoreForm.find('button[type="submit"]');
		var $spSubmit			= $storeZipForm.find('[name="submit"]');

		tealium.init();

		// Store info flip tile cick event
		$(document).on('click', '.store-info.more', function(e){
			$(this).closest('li').addClass('more-info-active');
			$(this).closest('li').removeClass('store-container-active');
		});
		$(document).on('click', '.store-more-info .btn-back', function(e){
			$(this).closest('li').addClass('store-container-active');
			$(this).closest('li').removeClass('more-info-active');
		});

		// disable selectability of unavailable stores
		$('.store-result.unavailable').find('input[type="radio"]').on('click', function () {
			return false;
		});

		$('#dwfrm_storepicker_postalCode')
        .attr("maxlength", "5")
        .on('blur focusout', function () {
			var $postalcodetmp = this.value;
			IsValidStoreZipCode($postalcodetmp);
		});

		$('#dwfrm_storepicker_postalCode').on('keyup', function () {
			var zip = this.value;
			var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
			if (!isValid) {
				return false;
			} else {
				$("#updateZip").removeAttr("disabled", "disabled");
				return true;
			}
		});

		$('#dwfrm_storepicker_postalCode')
        .on('keypress', function (e) {
        	 if (e.which != 13 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                 return false;
            }
		});

		// if no preferred store is picked, disable the continue button
		if ($preferredStoreForm.find('.preferred-store').length == 0) {
			$psSubmit.attr('disabled', true);
		}
		// return to zip form when change location link is clicked
		$changeLink.on('click', function (e) {
			e.preventDefault();
			$preferredStoreForm.hide();
			$storeZipForm.show();
			$($spSubmit).on('click', function (e) {
				e.preventDefault();
				getStores($storeZipForm);
			});
		});
		// event for form submit
		$($spSubmit).on('click', function (e) {
			e.preventDefault();
			var $postalcodetmp = $('#dwfrm_storepicker_postalCode').val();
			if (IsValidZipCode($postalcodetmp)) {
				getStores($storeZipForm);
			}
		});
		// update classes for styling when stores are selected
		$($preferredStoreForm.find('.select-preferred-store input')).change(function () {
			tealium.trigger("Try-in-store", "Select Preferred Store", "", $(this).attr('data-tealium'));
			var $buttonContainer = $(this).closest('div');
			var $buttonText = $buttonContainer.find('.select-preferred-store-text');
			var $storeContainer = $(this).closest('li');
			setPreferredStore(this.value);
			// reset other store containers
			$preferredStoreForm.find('.select-preferred-store-text').text(Resources.SELECT_STORE);
			$preferredStoreForm.find('li').removeClass('preferred-store');
			// update the current
			$buttonText.text(Resources.PREFERRED_STORE);
			$storeContainer.addClass('preferred-store');
			$psSubmit.removeAttr('disabled');
			$("div.selected-store").find(".store-name").html($storeContainer.find("span.storename").html());
		});

		$($psSubmit).on('click', function (e) {
			var action = $(this).attr("name");
			e.preventDefault();
			var prevSelectedStore = $('#instorepickup').attr('data-store-id');
			var selectedStore = $('#dialog-container').find('.preferred-store input').val();
			if (prevSelectedStore == selectedStore) {
				dialog.close();
			}
			else{
				$.ajax({
					type: 'POST',
					url: $preferredStoreForm.attr("action"),
					data: $preferredStoreForm.serialize(),
					dataType: 'html',
					success: function (html) {

						setPreferredStore(selectedStore);
						$.ajax({
							url: Urls.getAvailablePickupStore,
							type: 'GET'
						});
						dialog.close();
						if ($('#data-variation-url') && $('#data-variation-url').length) {
							window.location = $('#data-variation-url').html();
						} else {
							window.location.reload();
						}
						dialog.close();
					}.bind(this),
					failure: function () {
						window.alert(Resources.SERVER_ERROR);
					}
				});
			}
		});
	}
	// This function is for the event handling on the cart page for when user swaps between shipping and bopis
	$('.delivery-options-container').find('.input-radio').on("change", function (e) {
		var deliveryoption = $(this).val();
		var storeId = $(this).attr('data-store-id');
		$('#shippingmethodsave').addClass("disabled-btn");
		$.ajax({
			url: Urls.changeDeliveryOption,
			type: 'POST',
			data: {deliveryoption: deliveryoption, storeId: storeId},
			success: function (data) {
				if (deliveryoption == 'shipped') {
					$('#shippingmethodsave').removeAttr("disabled", "disabled");
					$('#shippingmethodsave').removeClass("disabled-btn");

				} else if ((deliveryoption == 'instorepickup') && storeId == 'null') {
					$('#shippingmethodsave').attr("disabled", "disabled");
					$('#shippingmethodsave').addClass("disabled-btn");
				} else if ((deliveryoption == 'instorepickup')) {
					$('#shippingmethodsave').removeAttr("disabled", "disabled");
					$('#shippingmethodsave').removeClass("disabled-btn");
				}
			},
			async: false
		});

	});
	$('#mob-create-account').on('click', function (e) {
		 e.preventDefault();
		 $('#ConfirmationForm').toggleClass('hidden-xs');
		 $('#confirmationBtn').addClass('disabled-btn');
	});
}

function IsValidStoreZipCode(zip) {
	var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
	if (!isValid && zip.length >0) {
		if ($(".postal .postalerror").length < 1) {
			$(".postal .field-wrapper").append("<span class='error postalerror'>" + Resources.INVALID_ZIP + "</span>");
			$("#updateZip").attr("disabled", "disabled");
		}
		return false;
	} else {
		if ($(".postal .postalerror").length > 0) {
			$(".postal span.postalerror").remove();
			$("#updateZip").removeAttr("disabled", "disabled");
		}
		return true;
	}
}

function IsValidZipCode(zip) {
	var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
	if (!isValid) {
		$("#updateZip").attr("disabled", "disabled");
		return false;
	} else {
		$("#updateZip").removeAttr("disabled", "disabled");
		return true;
	}
}
/**
 * @private
 * @function
 * @description Adds class ('js') to html for css targeting and loads js specific styles.
 */
function initializeDom() {
	// add class to html for css targeting
	$('html').addClass('js');
	if (SitePreferences.LISTING_INFINITE_SCROLL) {
		$('html').addClass('infinite-scroll');
	}
	// load js specific styles
	util.limitCharacters();
	customjs.init();

}

function removeAnchorTarget()
{
	MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

	var observer = new MutationObserver(function(mutations, observer) {
		var searchString=Urls.HTTPHOST;

		var links = $(".sidebarBody").find('a');

		for(var i = 0; i < links.length; i++) {

			var thisLink = links[i];

			if (thisLink.href.toLowerCase().indexOf(searchString) > -1) {
				if(thisLink.hasAttribute("target")){
					thisLink.removeAttribute("target");
				}

			}

		}
	});

	observer.observe(document, {
	  subtree: true,
	  attributes: true
	});
}

function setCookieForDays(cname, cvalue, exdays) {
	  var d = new Date();
	  d.setTime(d.getTime() + (exdays*24*60*60*1000));
	  var expires = "expires="+ d.toString();
	  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function setCookieForHours(cname, cvalue, exhours) {
	  var d = new Date();
	  d.setTime(d.getTime() + (exhours*60*60*1000));
	  var expires = "expires="+ d.toUTCString();
	  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

//pass parameter name and this function will return parameter's value
function urlParams (name) {
	var url = new URL(window.location.href.toLowerCase());
	var value = url.searchParams.get(name);
	return value;
}

function checkWireCutterPromo() {
	if(window.location.href.toLowerCase().indexOf('utm_source') != -1) {
		var utm_source = urlParams('utm_source');
		if(utm_source && utm_source.indexOf('wirecutter') != -1) {
			//Promo will be active from 20 Aug 12:00 am till 21 Aug 3:00 am, 2019
			//So this will be considered as Eastern time
			var date1 = new Date(2019, 7, 21, 3, 0, 0, 0);
			
			var offset = new Date().getTimezoneOffset();// getting offset to make time in gmt+0 zone (UTC) (for gmt+5 offset comes as -300 minutes)
			var date2 = new Date();
			date2.setMinutes ( date2.getMinutes() + offset);// date2 now in UTC time
			
			//this promo is only active for one day and eastern time is 4 hours behind from UTC time.
			var easternTimeOffset = -240;
			date2.setMinutes ( date2.getMinutes() + easternTimeOffset);
			
			//date1=> Promo expiration dateTime in Eastern timeZone
			//date2=> Eastern time when user comes from wirecutter site
			var hours = Math.abs(date1 - date2) / 36e5;
			
			//Finally setting cookie with expiration hours
			setCookieForHours('utm_source', utm_source, parseInt(hours));
		}
	}
}

var pages = {
		account: require('./pages/account'),
		cart: require('./pages/cart'),
		checkout: require('./pages/checkout'),
		compare: require('./pages/compare'),
		product: require('./pages/product'),
		registry: require('./pages/registry'),
		search: require('./pages/search'),
		storefront: require('./pages/storefront'),
		wishlist: require('./pages/wishlist'),
		storelocator: require('./pages/storelocator')
};

var app = {
		init: function () {
			if (document.cookie.length === 0) {
				$('<div/>').addClass('browser-compatibility-alert').append($('<p/>').addClass('browser-error').html(Resources.COOKIES_DISABLED)).appendTo('#browser-check');
			}
			hoverIntent.init();
			initializeDom();
			initializeEvents();


			// init specific global components
			countries.init();
			tooltip.init();
			minicart.init();
			validator.init();
			util.uniform();
			content.init();
			amplience.initZoomViewer();
			zipzone.init();
			tealium.init();
			sfemaildialog.init();
			// execute page specific initializations
			$.extend(page, window.pageContext);
			var ns = page.ns;
			if (ns && pages[ns] && pages[ns].init) {
				pages[ns].init();
			}
			// Initialize the Google Analytics library
			if (typeof window.DW.googleAnalytics === 'object') {
				googleAnalytics.init(ns, window.DW.googleAnalytics.config);
			}
			responsiveSlots.init(ns || '');
			checkWireCutterPromo();
		}
};

//general extension functions
(function () {
	String.format = function () {
		var s = arguments[0];
		var i, len = arguments.length - 1;
		for (i = 0; i < len; i++) {
			var reg = new RegExp('\\{' + i + '\\}', 'gm');
			s = s.replace(reg, arguments[i + 1]);
		}
		return s;
	};
})();
//initialize app
$(document).ready(function () {
	app.init();
	setDayCookie();
	removeAnchorTarget();
	initAccountLeftNavigation();
	initCreditCardValidation();
	initBackgroundVideo();
	if(typeof enableRegisterFormValidation != 'undefined' && enableRegisterFormValidation){
		validateRegistrationForm('#RegistrationForm');
		jQuery('#RegistrationForm').on('blur keyup change', 'input', function(event) {
		  validateRegistrationForm('#RegistrationForm');
		});
	}

	if(typeof enableConfirmationFormValidation != 'undefined' && enableConfirmationFormValidation){
		enableConfirmationFormValidation
		validateConfirmationForm('#ConfirmationForm');
		jQuery('#ConfirmationForm').on('blur keyup change', 'input', function(event) {
			validateConfirmationForm('#ConfirmationForm');
		});
	}

	if(typeof enablePaymentMethod != 'undefined' && enablePaymentMethod){
		var paymentMethod = $("#paymentMethodName").val();
		if (paymentMethod.length > 0) {
			if(paymentMethod == "SYNCHRONY_FINANCIAL"){
				utag.link({ "eventCategory" : ["synchrony"], "eventLabel" : ["purchase"], "eventAction" : ["submit"] });
			}
		}
	}

//	$("#video-dialog").dialog({
//	autoOpen: false,
//	resizable: true,
//	width: 800,

//	open: function () {
//	$("#popup-video").get(0).play();
//	},
//	close: function () {
//	$("#popup-video").get(0).pause();
//	$("#bg-dark-vdo").get(0).play();
//	}
//	});

	// works on window resize
	$(window).resize( function(){
		customjs.initInputAutoScroll();
		customjs.initSameHeightBlocks();
		customjs.intiMobileCartCheckoutVal();
		$(function(){
			if($(window).width() > 768){
				$('body').removeClass('nav-active');
			}
		});
	});
	// Open Geo Location Dialog box
	sfgeolocdialog.show();
});

/*
 * Set cookie to get client day
 */
function setDayCookie() {
	var cookieName = "clientDay";
	var cookieValue = new Date().getDay();
	var expDate = new Date();
	var expMinutes = 10;
	expDate.setTime(expDate.getTime() + (expMinutes * 60 * 1000));
    document.cookie = cookieName + '=' + cookieValue + '; expires=' + expDate + '; path=/';
}

//click event for video section
//$(".btn-play").on("click", function () {
//$("#video-dialog").dialog("open");
//$("#bg-dark-vdo").get(0).pause();
//});


//<----- click events for test of recommendation menu------>
$(document).on('click', '.sleeping-style-selection button', function(){
	$('.quantity-section').show();
	$('.sleeping-style-selection').hide();
	$('.confirmation-section').hide();
});
$(document).on('click', '.quantity-section .btn-default', function(){
	$('.confirmation-section').show();
	$('.quantity-section').hide();
	$('.sleeping-style-selection').hide();
});
$(document).on('click', '.confirmation-section .btn-default', function(){
	$('.sleeping-style-selection').show();
	$('.quantity-section').hide();
	$('.confirmation-section').hide();
});
//ends here

//mobile navigation opener click event
$('body').off('click').on('click', '.menu-opener', function(){
	$('body').addClass('nav-active');
	$('html').css("overflow", "hidden");
});
//close mobile navigation click event
$('body').on('click', '.nav-close', function(){
	$('body').removeClass('nav-active');
	$('html').css("overflow", "visible");
});
//Show switch site mobile navigation click event
$('body').on('click', '.mobile-dropdown-switch-site-opener', function(){
	$('body').removeClass('nav-active');
	$('html').css("overflow", "visible");
	$('#mobile-dropdown-switch-menu').show();
});
//Hide switch site mobile navigation click event
$('body').on('click', '.ss-menu-close', function(){
	$('#mobile-dropdown-switch-menu').hide();
});
//mobile cart opener mobile menu
$('body').on('click', '.mob-cart-menu-opener', function(){
	$('body').removeClass('nav-active');
	$('body').addClass('mini-cart-active');
});

$("#wrapper").click( function(event){
    if ( !$(event.target).closest('#header #nav').length ) {
        $('body').removeClass('nav-active');
		$('html').css("overflow", "visible");
    }
});
$("#wrapper").click( function(event){
    if ( !$(event.target).closest('#header #mini-cart').length ) {
      $('html').removeClass('mini-cart-active');
      $('body').removeClass('mini-cart-active');
			$('html').css("overflow", "visible");
    }
});

//background vdo init function
function initBackgroundVideo() {
	jQuery('.bg-video').backgroundVideo({
		activeClass: 'video-active'
	});
}

//validating the confirmation registration form
function validateConfirmationForm(id) {
    var valid = jQuery(id).validate().checkForm();
    if (valid) {
        jQuery('.form-save').prop('disabled', false);
        jQuery('.form-save').removeClass('disabled-btn');
    } else {
        jQuery('.form-save').prop('disabled', 'disabled');
        jQuery('.form-save').addClass('disabled-btn');
    }
}


//validating the form
function validateRegistrationForm(id) {
  var valid = jQuery(id).validate().checkForm();
  var emailRgx = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  var emailRegValid = emailRgx.test($('.regemail input').first().val());
  var emailConfirmValid = emailRgx.test($('.emailconfirm input').first().val());

    if (valid && emailRegValid && emailConfirmValid) {
    	jQuery('.form-save').prop('disabled', false);
    	jQuery('.form-save').removeClass('disabled-btn');
    } else {
    	jQuery('.form-save').prop('disabled', 'disabled');
    	jQuery('.form-save').addClass('disabled-btn');
    }
}

function initAccountLeftNavigation() {
	var navID=$('#breadcrumbpage').text().replace(' ','').toLowerCase();;
	var link=$('#account-navigation li a[id^="' + navID + '"]');
	$(link).addClass("active");
	var mobnav=$('#mob-account-navigation li a[id^="' + navID + '"]');
	$("#selectedOption").val($(mobnav).text());
	$(mobnav).closest('li').addClass("selected");
}

function initCreditCardValidation() {
//	$('#billingSubmitButton').removeAttr("disabled", "disabled");
	var $inputCC = $('#CreditCardBlock input[name*="_creditCard_number"]');
	$('#dwfrm_billing_paymentMethods_creditCard_expirydate').attr("placeholder", "MM/YY");

	$inputCC.on('focusout', function (event) {
		var type= GetCardType($inputCC.val());
		if(type=="Visa")
		{
			$(this).addClass('Visa');
			$(this).removeClass('Discover');
			$(this).removeClass('MasterCard');
			$(this).removeClass('Amex');
			$(this).parent().addClass('Visa-wrapper');
			$(this).parent().removeClass('Discover-wrapper');
			$(this).parent().removeClass('MasterCard-wrapper');
			$(this).parent().removeClass('Amex-wrapper');
            $("input[name^='dwfrm_billing_paymentMethods_creditCard_cvn']" ).attr("maxlength", "3");
		}
		else if(type=="MasterCard")
		{
			$(this).addClass('MasterCard');
			$(this).removeClass('Visa');
			$(this).removeClass('Discover');
			$(this).removeClass('Amex');
			$(this).parent().addClass('MasterCard-wrapper');
			$(this).parent().removeClass('Visa-wrapper');
			$(this).parent().removeClass('Discover-wrapper');
			$(this).parent().removeClass('Amex-wrapper');
			$("input[name^='dwfrm_billing_paymentMethods_creditCard_cvn']" ).attr("maxlength", "3");
		}
		else if(type=="Amex")
		{
			$(this).addClass('Amex');
			$(this).removeClass('Visa');
			$(this).removeClass('MasterCard');
			$(this).removeClass('Discover');
			$(this).parent().addClass('Amex-wrapper');
			$(this).parent().removeClass('Visa-wrapper');
			$(this).parent().removeClass('MasterCard-wrapper');
			$(this).parent().removeClass('Discover-wrapper');
			$("input[name^='dwfrm_billing_paymentMethods_creditCard_cvn']" ).attr("maxlength", "4");
		}
		else if(type=="Discover")
		{
			$(this).addClass('Discover');
			$(this).removeClass('Visa');
			$(this).removeClass('MasterCard');
			$(this).removeClass('Amex');
			$(this).parent().addClass('Discover-wrapper');
			$(this).parent().removeClass('Visa-wrapper');
			$(this).parent().removeClass('MasterCard-wrapper');
			$(this).parent().removeClass('Amex-wrapper');
			$("input[name^='dwfrm_billing_paymentMethods_creditCard_cvn']" ).attr("maxlength", "3");
		}
		else
		{
			$(this).removeClass('Visa');
			$(this).removeClass('MasterCard');
			$(this).removeClass('Amex');
			$(this).removeClass('Discover');
			$(this).parent().removeClass('Visa-wrapper');
			$(this).parent().removeClass('MasterCard-wrapper');
			$(this).parent().removeClass('Amex-wrapper');
			$(this).parent().removeClass('Discover-wrapper');
		}
		if(type.length > 1){
			$("#dwfrm_billing_paymentMethods_creditCard_type").val(type);
			if ($(".ccnumber .errorccnumber").length > 0) {
				$(".ccnumber span.errorccnumber").remove();
				$('#billingSubmitButton').removeAttr("disabled", "disabled");
			}
		}
		else
		{
			$("#dwfrm_billing_paymentMethods_creditCard_type").val('Visa'); // default value
			if ($inputCC.val().length > 0) {
				if ($(".ccnumber .errorccnumber").length < 1) {
					$(".ccnumber .field-wrapper").append("<span class='error errorccnumber'>" + Resources.VALIDATE_CREDITCARD + "</span>");
				}
			}
			else if ($(".ccnumber .errorccnumber").length > 0) {
				$(".ccnumber span.errorccnumber").remove();
			}
			$('#billingSubmitButton').attr("disabled", "disabled");
		}
	});
	$inputCC.on('keypress keydown focusout blur',  function(e){
		if ($(".ccnumber .error-message").length > 0) {
			$(".ccnumber div.error-message").remove();
		}
	});
	$( "input[name^='dwfrm_billing_paymentMethods_creditCard_expirydate']" )
	.attr("maxlength", "5")
	.attr("onPaste", "return false")
	.on('keydown',  function(e){
		var val = this.value;
		var temp_val;
		if(!isNaN(val)) {
			if(val > 1 && val < 10 && val.length == 1) {
				temp_val = "0" + val + "/";
				$(this).val(temp_val);
			}
			else if (val >= 1 && val < 10 && val.length == 2 && e.keyCode != 8) {
				temp_val = val + "/";
				$(this).val(temp_val);
			}
			else if(val > 9 && val.length == 2 && e.keyCode != 8) {
				temp_val = val + "/";
				$(this).val(temp_val);
			}
		}

		-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault();

	});

	$( "input[name^='dwfrm_billing_paymentMethods_creditCard_cvn']" )
	.on('keypress keydown focusout blur',  function(e){
		if ($(".cvn .error-message").length > 0) {
			$(".cvn div.error-message").remove();
		}
	});

	$( "input[name^='dwfrm_billing_paymentMethods_creditCard_expirydate']" )
	.on('keypress keydown focusout blur',  function(e){
		if ($(".ccdate .error-message").length > 0) {
			$(".ccdate div.error-message").remove();
		}
	});

	$( "input[name^='dwfrm_billing_paymentMethods_creditCard_cvn']" )
	.attr("maxlength", "4")
	.on('keydown',  function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});


	$('#dwfrm_billing_paymentMethods_creditCard_expirydate').on('focusout', function (event) {
		var date= $(this).val();
		if (IsValidExpiryDate(date))
		{
			var datepart = date.split('/');
			$("#dwfrm_billing_paymentMethods_creditCard_expiration_month").val(datepart[0]);
			$("#dwfrm_billing_paymentMethods_creditCard_expiration_year").val('20' + datepart[1]);

			if ($(".expirydate .errordate").length > 0) {
				$(".expirydate span.errordate").remove();
				$('#billingSubmitButton').removeAttr("disabled", "disabled");
			}
		}
		else
		{
			if (date.length > 0 ) {
				if ($(".expirydate .errordate").length < 1) {
					$(".expirydate .field-wrapper").append("<span class='error errordate'>" + Resources.VALIDATE_DATE + "</span>");
				}
			}
			else if ($(".expirydate .errordate").length > 0) {
				$(".expirydate span.errordate").remove();
			}
			$('#billingSubmitButton').attr("disabled", "disabled");
		}
	});

	var $selectPaymentMethod = $('.payment-method-options');
	var selectedPaymentMethod = $selectPaymentMethod.find(':checked').val();
	$selectPaymentMethod.on('click', 'input[type="radio"]', function () {
		if($(this).val()=='PayPal'){
			$('#billingSubmitButton').hide();
		}
		else
		{
			$('#billingSubmitButton').show();
		}
	});

	//prevent multiple clicks of complete payment button
	jQuery('#billingSubmitButton').on("click", function (e) {
		var valid = jQuery('#dwfrm_billing').validate().checkForm();
	    if (valid) {
	    	$(this).addClass('disabled-btn');
	    }
	});

}

function IsValidExpiryDate(date) {
	var re = new RegExp("^(0[1-9]|1[0-2]|[1-9])\/(1[7-9]|[2-9][0-9])$");
	if (date.match(re) != null) {
		var datepart = date.split('/');
		var month = parseInt(datepart[0], 10),
		year = 2000 + parseInt(datepart[1], 10),
		currentMonth = new Date().getMonth() + 1,
		currentYear  = new Date().getFullYear();
		 if (year < currentYear || (year == currentYear && month < currentMonth)) {
			 return false;
		 }
		 return true;
	}
    return false;
}

function GetCardType(number)
{
	// visa
	var re = new RegExp("^4[0-9]{12}(?:[0-9]{3})?$");
	if (number.match(re) != null)
		return "Visa";

	// Master card
	var re = new RegExp("^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$");
	if (number.match(re) != null)
		return "MasterCard";

	// AMEX
	re = new RegExp("^3[47][0-9]{13}$");
	if (number.match(re) != null)
		return "Amex";

	// Discover
	re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
	if (number.match(re) != null)
		return "Discover";

	return "";
}
/*
 * jQuery video background plugin
 */
;(function($, $win) {
	var BgVideoController = (function() {
		var videos = [];

		return {
			init: function() {
				$win.on('load.bgVideo resize.bgVideo orientationchange.bgVideo', this.resizeHandler.bind(this));
			},

			resizeHandler: function() {
				if (this.isInit) {
					$.each(videos, this.resizeVideo.bind(this));
				}
			},

			buildPoster: function($video, $holder) {
				$holder.css({
					'background-image': 'url(' + $video.attr('poster') + ')',
					'background-repeat':'no-repeat',
					'background-size':'cover'
				});
			},

			resizeVideo: function(i) {
				var item = videos[i];
				var styles = this.getDimensions({
					videoRatio: item.ratio,
					maskWidth: item.$holder.outerWidth(),
					maskHeight: item.$holder.outerHeight()
				});

				item.$video.css({
					width: styles.width,
					height: styles.height,
					marginTop: styles.top,
					marginLeft: styles.left
				});
			},

			getRatio: function($video) {
				return $video[0].videoWidth / $video[0].videoHeight ||
				$video.attr('width') / $video.attr('height') ||
				$video.width() / $video.height();
			},

			getDimensions: function(data) {
				var ratio = data.videoRatio,
				slideWidth = data.maskWidth,
				slideHeight = slideWidth / ratio;

				if (slideHeight < data.maskHeight) {
					slideHeight = data.maskHeight;
					slideWidth = slideHeight * ratio;
				}
				return {
					width: slideWidth,
					height: slideHeight,
					top: (data.maskHeight - slideHeight) / 2,
					left: (data.maskWidth - slideWidth) / 2
				};
			},

			add: function($video, options) {
				var $holder = options.videoHolder ? $video.closest(options.videoHolder) : $video.parent();
				var item = {
						$video: $video,
						$holder: $holder,
						options: options
				};

				if ($video.attr('poster')) {
					this.buildPoster($video, $holder);
				}

				if ($video[0].readyState) {
					this.onVideoReady(item);
					$video[0].play();
				} else {
					$video.one('loadedmetadata', function() {
						this.onVideoReady(item);
					}.bind(this));
				}

				$video.one('play', function() {
					$holder.addClass(options.activeClass);
					this.makeCallback.apply($.extend(true, {}, this, item), ['onPlay']);
				}.bind(this));


				this.makeCallback.apply($.extend(true, {}, this, item), ['onInit']);
				return this;
			},

			onVideoReady: function(item) {
				if (!this.isInit) {
					this.isInit = true;
					this.init();
				}
				videos.push($.extend(item, {
					ratio: this.getRatio(item.$video)
				}));
				this.resizeVideo(videos.length - 1);
			},

			destroy: function($video) {
				if (!$video) {
					videos = videos.filter(this.destroySingle);
				} else {
					videos = videos.filter(function(item) {
						var removeFlag = item.$video.is($video);

						removeFlag && this.destroySingle(item);

						return !removeFlag;
					}.bind(this));
				}

				if (!videos.length) {
					this.isInit = false;
					$win.off('.bgVideo');
				}
			},

			destroySingle: function(item) {
				item.$video.removeAttr('style').removeData('BackgroundVideo')[0].pause();
				item.$holder.removeClass(item.options.activeClass);
			},

			makeCallback: function(name) {
				if (typeof this.options[name] === 'function') {
					var args = Array.prototype.slice.call(arguments);
					args.shift();
					this.options[name].apply(this, args);
				}
			}
		};
	}());

	$.fn.backgroundVideo = function(opt) {
		var args = Array.prototype.slice.call(arguments);
		var method = args[0];
		var options = $.extend({
			activeClass: 'video-active',
			videoHolder: null
		}, opt);

		return this.each(function() {
			var $video = jQuery(this);
			var instance = $video.data('BackgroundVideo');

			if (typeof opt === 'object' || typeof opt === 'undefined') {
				$video.data('BackgroundVideo', BgVideoController.add($video, options));
			} else if (typeof method === 'string' && instance) {
				if (typeof instance[method] === 'function') {
					args.shift();
					instance[method].apply(instance, args);
				}
			}
		});
	};

	window.BgVideoController = BgVideoController;

	return BgVideoController;
}(jQuery, jQuery(window)));
