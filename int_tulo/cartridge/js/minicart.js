'use strict';

var util = require('./util'),
	dialog = require('./dialog'),
	TPromise = require('promise'),
	page = require('./page'),
	progress = require('./progress'),
	utaghelper = require('./utaghelper');

var timer = {
	id: null,
	clear: function () {
		if (this.id) {
			window.clearTimeout(this.id);
			delete this.id;
		}
	},
	start: function (duration, callback) {
		this.id = setTimeout(callback, duration);
	}
};
var submitCartAction  = function (){
	var $form =  $('#cart-items-form');
	var ajaxdest = $form.attr('action');
	var formData = $form.serialize();
	progress.show($('.mini-cart-content'));
	$.post( ajaxdest, formData )
	.done(function( text ){
		var $response = $( text );
		//update content in minicart
		$("div#mini-cart").html($response);		
		minicart.init();
		var miniCartAction=$('.mini-cart-total').data('action')
		if(miniCartAction!=null && miniCartAction === 'addcoupon') {			
			utaghelper.addBonusProducttUtag();
		}
		else if(miniCartAction!=null && miniCartAction === 'deleteCoupon') {
			var productsJson = $('.mini-cart-total').data('bonusproducts') != null ? $('.mini-cart-total').data('bonusproducts') : null;
			utaghelper.removeProducttUtagByJson(productsJson);
		}
		else if(miniCartAction!=null && miniCartAction === 'deleteProduct') {
			var productsJson = $('.mini-cart-total').data('bonusproducts') != null ? $('.mini-cart-total').data('bonusproducts') : null;
			utaghelper.removeProducttUtagByJson(productsJson);
		}
		//atpDataForCart();
	});	
	
};
//remove frame option
var removeFrame = function(pli_uuid){
	progress.show($('.mini-cart-content'));
	$.get( Urls.removeFrame+"?pli_uuid="+pli_uuid )
	.done(function( text ){
		var $response = $( text );
		//update content in minicart
		$("div#mini-cart").html($response);		
		minicart.init();
		//atpDataForCart();
		
	});	
	
};
//refresh minicart
var refreshMiniCart = function(){
	progress.show($('.mini-cart-content'));
	$.get(Urls.cartShow)
	.done(function(text){
		var $response = $(text);
		//update content in minicart
		$("div#mini-cart").html($response);		
		minicart.init();
		//atpDataForCart();		
	});		
};

var atpDataForCart = function(){
	progress.hide($('.mini-cart-content'));
	$.get(Urls.getATPCartData)
	.done(function(text){
		var $response = $(text);	
		if($response)
		{
            $("div.delivery-message.red-carpet").html($response);
		}
		
	});	
};
var recycleFeeTooltip = function () {
	$(".recyclefeett").on({
    mouseover: function() {
    	$(this).closest('.recyclefeett').addClass('active-tt');
    },
    mouseout: function() {
    	var $recyclefee = $(this).closest('.recyclefeett');
		$recyclefee.removeClass('active-tt');
    }
	});
};
var updateCartForMobile =  function(){
	var cartValue = $('div.mini-cart-total').find('.minicart-quantity').html();
	$('ul.accounts-link').find('.minicart-quantity').html(cartValue);
	cartValue = cartValue*1;
	if(cartValue < 1)
	{
		$('ul.accounts-link').find('.mob-cart-menu-opener').addClass('mini-cart-empty');
	}
	else
	{
		$('ul.accounts-link').find('.mob-cart-menu-opener').removeClass('mini-cart-empty');
	}	
	
	
}
function addHiddenInput( name ){
	var $form =  $('#cart-items-form');
    $form.append( "<input type='hidden' class='form_submittype' name='" + name + "' value='true'>" );
  }

  function removeHiddenInput( name ){
	 var $form =  $('#cart-items-form');
    $form.find( ".form_submittype" ).remove();
 }
var lastTop;

var minicart = {
	init: function () {
		this.$el = $('#mini-cart');
		this.$content = this.$el.find('.mini-cart-content');
		this.$minicartlink = this.$el.find('.mini-cart-link');		
		// events
		$( ".spinner" ).spinner({
				min: 1,
				max: 50
		});
		this.$el.find('.cart-coupon-code a.coupon-label').unbind('click');
		this.$el.find('.cart-coupon-code a.coupon-label').on('click', function (e) {
			e.preventDefault();
		    $(this).closest('.cart-coupon-code').toggleClass('active-drop');
		    if ($(".cart-coupon-code .error").length > 0) {        	
	        	$('#dwfrm_cart_couponCode').removeClass('error');	        	
	        	$("span.error").remove();
	        }
		});
		this.$minicartlink.unbind('click');
		this.$minicartlink.on('click', function () {
			if (this.$content.not(':visible')) {
				this.show();
			}
		}.bind(this));
		$('.mob-cart-menu-opener').unbind('click');
		$('.mob-cart-menu-opener').on('click', function () {
			if (this.$content.not(':visible')) {
				this.show();
			}
		}.bind(this));
		this.$el.find('.mini-cart-close').unbind('click');
		this.$el.find('.mini-cart-close').on('click', function () {				
			this.close();
		}.bind(this));		

		/*$('.mini-cart-products').mCustomScrollbar({
			autoHideScrollbar: true
			});*/

		 var removeItemEvent = false;
		$('button[name$="deleteProduct"]').unbind('click');
	    $('button[name$="deleteProduct"]').on('click', function (e) {
	    	 e.preventDefault();
	        if (!removeItemEvent) {
	        	 addHiddenInput( this.name );
	        	// tealium analytics
	        	 
	        	 var price = $(this).closest('.cart-row').find('span.price-sales').html();
	    		 var productUnitPrice = "";	    		 
	    		 if(price) {
	    		 	productUnitPrice = price.replace(/[$,]/g, ""); 
	    		 }
	    		 
	    		 if(typeof utag != undefined && utag != null ){
	    			 
	    			 var cartRow = $(this).closest('.cart-row');
	    			 
	    			 if(cartRow.length > 0){
			    	
	    				 var nameDiv = cartRow.find('div.name');
	    				
	    				 if(nameDiv.length > 0){
	    					 
	    					 var prodQuantity = (cartRow.find('input.ui-spinner-input').length > 0) ? cartRow.find('input.ui-spinner-input').val() : "1";
	    					 var prodSize = (cartRow.find('div.attribute.size span.value').length > 0) ? cartRow.find('div.attribute.size span.value').get(0).innerHTML.replace(/\s+/g, '') : "";
	    					 var prodUrl =  nameDiv.find('a').length > 0 ? nameDiv.find('a').attr('href').split() : [""];
	    					 var prodImg = (cartRow.find('img.cart-product-img').length > 0 ) ? cartRow.find('img.cart-product-img').get(0).src : "";
	    					 var productPrice = productUnitPrice.length > 0 && prodQuantity.length > 0 ? (parseFloat(productUnitPrice) * parseInt(prodQuantity)).toFixed(2).toString().split() : "0"; 
	    					 
	    					 //Populate data for link product
	    					 var linkedProductId = nameDiv.data('linkedproductid');
	    					 var option_product_value_id = [""];
	    					 var option_product_name = [""];
	    					 var option_product_price = [""];	    					 
	    					 if(linkedProductId!=null) {
	    						 var linkedProductNameDiv = $('#cart-table .cart-row div.name[data-itemid='+linkedProductId+']');
	    						 if(linkedProductNameDiv.length > 0) { 
	    							 option_product_value_id=linkedProductNameDiv.data('masterid').toString().split();
	    							 option_product_name=linkedProductNameDiv.data('itemname').toString().split();
	    							 var OptionProductPrice=$(linkedProductNameDiv).closest('.cart-row').find('span.price-sales').html();
	    							 if(OptionProductPrice) {
	    								 option_product_price = OptionProductPrice.replace(/[$,]/g, "").split(); 
	    							 }
	    						 }
	    					 }
	    						 
	    					 
	    					 if(typeof utag != undefined && utag != null ){ 
				    			 utag.link({event_type : "remove", 
						    		"product_quantity" : prodQuantity.toString().split(), 
						    		"product_name" : 	 (nameDiv.data('itemname')) ? nameDiv.data('itemname').toString().split(): [""], 
						    		"product_id" : 		 (nameDiv.data('masterid')) ? nameDiv.data('masterid').toString().split(): [""],   
						    		"product_sku" : 	 (nameDiv.data('mastersku')) ? nameDiv.data('mastersku').toString().split(): [""], 
						    		"product_brand" : 	 ["Tulo"], 
						    		"product_unit_price": productUnitPrice.length > 0 ? productUnitPrice.split() : [""], 
						    		"product_price"		: productPrice,
						    		"product_variant" :  (nameDiv.data('itemvariant')) ? nameDiv.data('itemvariant').toString().split(): [""], 
						    		"product_size": 	 prodSize.toString().split(),
						 			"product_url":		 prodUrl.toString().split(),
						 			"product_img_url":	 prodImg.toString().split(),
						 			"option_product_name": option_product_name, 
						 			"option_product_price": option_product_price,
					    			"product_child_sku": (nameDiv.data('itemsku')) ? nameDiv.data('itemsku').toString().split(): [""],
					    			"option_product_value_id": option_product_value_id
						    	 	});
	    					 }
	    				 }
	    			 }
	    		 }
		    	 
	        	 submitCartAction();
	        } 
	    });
	    $('button[name$="removeFrame"]').unbind('click');
	    $('button[name$="removeFrame"]').on('click', function (e) {
	    	 e.preventDefault();
	    	 var pli_uuid = $(this).data('uuid');
	         removeFrame(pli_uuid);
	    });
	    this.$el.find('.ui-spinner-button').unbind('click');
	    this.$el.find('.ui-spinner-button').on('click', function (e) { 
	    	// tealium analytics
	    	if($(this).hasClass('ui-spinner-up'))
	    	{
	    		//add event - quantity increase
	    		
	    		var price = $(this).closest(".cart-row").find("input[name='productPrice']").val();
	    		var productPrice = ""; 
	    		if(price) {
	    			var productPrice = price.replace('USD ', ''); 
	    		}
	    		if(typeof utag != undefined && utag != null ){ 
	    			utag.link({ "event_type": "add", "product_quantity": [ "1" ], "product_name": [ $(this).closest('.cart-row').find('div.name').data('itemname') ], "product_id": [ $(this).closest('.cart-row').find('div.name').data('itemid') ], "product_brand": [ "Tulo" ], "product_unit_price": [ productPrice ],  "product_variant": [ $(this).closest('.cart-row').find('div.name').data('itemvariant')]});
	    		}
	    	}
	    	else if($(this).hasClass('ui-spinner-down'))
	    	{
	    		var price = $(this).closest(".cart-row").find("input[name='productPrice']").val();
	    		var productPrice = ""; 
	    		if(price) {
	    			var productPrice = price.replace('USD ', ''); 
	    		}
	    		//add event - quantity decrease
	    		if(typeof utag != undefined && utag != null ){ 
	    			utag.link({ "event_type": "remove", "product_quantity": [ "1" ], "product_name": [ $(this).closest('.cart-row').find('div.name').data('itemname') ], "product_id": [ $(this).closest('.cart-row').find('div.name').data('itemid') ], "product_brand": [ "Tulo" ], "product_unit_price": [ productPrice ],  "product_variant": [ $(this).closest('.cart-row').find('div.name').data('itemvariant')]});
	    		}	
	    	} 
			$(this).parents('div.item-details').find('button[name$="updateQuantity"]').click();
	    });
	    this.$el.find('button[name$="updateQuantity"]').unbind('click');
	    this.$el.find('button[name$="updateQuantity"]').on('click', function (e) {
	    	 e.preventDefault();	        
        	 addHiddenInput( this.name );
        	 submitCartAction();	        
	    });	
	    this.$el.find('input[name$="_couponCode"]').unbind('keydown');
	    this.$el.find('input[name$="_couponCode"]').on('keydown', function (e) {
	        if (e.which === 13 && $(this).val().length === 0) { return false; }else if(e.which === 13 && $(this).val().length > 0){
	        	e.preventDefault();
	        	$('.mini-cart-content').find('button[name$="addCoupon"]').click();
	        }
	    });

	    this.$el.find('button[name$="addCoupon"]').unbind('click');
	    this.$el.find('button[name$="addCoupon"]').on('click', function (e) {
	    	 e.preventDefault();
	    	 if ($(".cart-coupon-code .error").length > 0) {        	
		        	$('#dwfrm_cart_couponCode').removeClass('error');
		        	$("span.error").remove();
		     }
	    	 
	    	 if($(this).closest('div#mini-cart').find('input[name$="_couponCode"]').val().trim().length === 0){
	    		$('#dwfrm_cart_couponCode').addClass('error');
	         	$(".cart-coupon-code").append("<span class='error'>" + Resources.COUPON_CODE_MISSING + "</span>");
	            return false;
	    	 }	    	 
	    	 addHiddenInput( this.name );
        	 submitCartAction();
	    });
	    $('button[id="deleteCouponCart"]').unbind('click');
	    $('button[id="deleteCouponCart"]').on('click', function (e) {
	    	 e.preventDefault();	       
        	 addHiddenInput( this.name );
        	 submitCartAction();	        
	    });        
	    this.$el.find('.add-base-to-cart').unbind('click');
	    this.$el.find('.add-base-to-cart').on('click', function (e) {
	    	 e.preventDefault();
	    	 var form = $(this).closest('form');	    		
	    		
    		var $form = $(form);
			var $qty = $form.find('input[name="Quantity"]');
			if ($qty.length === 0 || isNaN($qty.val()) || parseInt($qty.val(), 10) === 0) {
				$qty.val('1');
			}	
			var ajaxdest = util.ajaxUrl(Urls.addFrameToCart);
			var formData =  $form.serialize();
			progress.show($('.mini-cart-content'));
    		$.post( ajaxdest, formData )
    		.done(function( text ){
    			var $response = $( text );
    			//update content in minicart
    			$("div#mini-cart").html($response);		
    			minicart.init();
    			//atpDataForCart();
    		});	
    		// Add base to cart
    		if(typeof utag_data !=  'undefined' && typeof utag != undefined && utag != null ){
    				
				var frameInfo = $(this).parent().parent().find('div.frame-info');
				
				if(frameInfo.length > 0){
					
					var prodName = (frameInfo.get(0).attributes['pr1nm'] ) ? frameInfo.get(0).attributes['pr1nm'].value : "";
					var prodId   = (frameInfo.get(0).attributes['pr1ma'] ) ? frameInfo.get(0).attributes['pr1ma'].value : "";
					var prodSku  = (frameInfo.get(0).attributes['pr1ca'] ) ? frameInfo.get(0).attributes['pr1ca'].value : "";
					var prodPrc  = (frameInfo.get(0).attributes['pr1pr'] ) ? frameInfo.get(0).attributes['pr1pr'].value : "";
					var prodSize = (frameInfo.get(0).attributes['pr1sz'] ) ? frameInfo.get(0).attributes['pr1sz'].value : "";
					var produrl  = (frameInfo.get(0).attributes['pr1ur'] ) ? frameInfo.get(0).attributes['pr1ur'].value : "";
					var prodImg	 = (frameInfo.get(0).attributes['pr1im'] ) ? frameInfo.get(0).attributes['pr1im'].value : "";
					var prodChild= (frameInfo.get(0).attributes['pr1va'] ) ? frameInfo.get(0).attributes['pr1va'].value : "";
					var prodsku =  (frameInfo.get(0).attributes['prlpsku'] ) ? frameInfo.get(0).attributes['prlpsku'].value : "";
					var prodCat	=	(frameInfo.get(0).attributes['prlcat'] ) ? frameInfo.get(0).attributes['prlcat'].value : "";
					
	    			utag.link({
	    				"enhanced_action": "add",
	    				"event_type" : "add_to_cart", 
	    				"product_quantity" :   ["1"], 
	    				"product_name" : 	   prodName.toString().split(), 
	    				"product_id" : 		   prodId.toString().split(),  
	    				"product_brand" :      ["Tulo"], 
	    				"product_sku": 		   prodSku.toString().split(),
	    				"product_unit_price" : prodPrc.toString().split(), 
	    				"product_price" 	 : prodPrc.toString().split(),
	    				"product_size": 	   prodSize.toString().split(),
	    				"product_url": 		   produrl.toString().split(),
	    				"product_img_url":     prodImg.toString().split(),	    				
	    				"product_child_sku":   prodChild.toString().split(),
	    				"product_category":    prodCat.toString().split(),
	    				"option_product_name": [""],
	    				"option_product_price": [""],
	    				"option_product_value_id": [""]
	    				});
				
				}
    		}
	    });
	   // $("#header a.menu-opener").unbind('click');
	   // $("#header a.menu-opener").on('click', function (e) {
	    	 updateCartForMobile();       
	    //});
	    	// remove empty bonus boxes 
	    	$('#mini-cart').find(".cart-bonus-bundle-sale").each(function(){
    			if ($(this).find(".bonus-product").length == 0) {        	
    				$(this).remove();				
    			}
	    	});
	    	this.$el.find('.select-bonus-items').unbind('click'); 
	    	this.$el.find('.select-bonus-items').on('click', function (e) {
		    e.preventDefault();
		    minicart.openBonusDialog({
		    	url: this.href
		    });
		});
	  if($('.recyclefeett').length > 0){
	  	recycleFeeTooltip();         
		}   
	  this.$el.find('.freesheet-info-detail').unbind('click');
	  this.$el.find('.freesheet-info-detail').on('click', function (e) {
			e.preventDefault();
			dialog.open({
				url: this.href,
				options: {
					dialogClass: 'freesheets-dialog',
					open: function () {
						$(".ui-dialog-title").css('text-align', 'center');
					}
				}
			});
		});
	},
	/**
	 * @function
	 * @description Shows the given content in the mini cart
	 * @param {String} A HTML string with the content which will be shown
	 */
	show: function (html) {
		this.$el.html(html);
		this.init();
		lastTop = $(window).scrollTop();		
		$("body").addClass("mini-cart-active");
		$("html").addClass("mini-cart-active");		
		$('html').css("overflow", "hidden");
		//atpDataForCart();
		
	},
	
	/**
	 * @function
	 * @description Slides down and show the contents of the mini cart
	 */
	slide: function () {
		timer.clear();
		// show the item
		var screenwidth = $(window).width();
		if (screenwidth > 767) {
			this.$content.slideDown('slow');
		}
		if (this.$minicartlink.hasClass('mini-cart-empty')) {
			this.$minicartlink.addClass("addnoline");
			this.$minicartlink.removeClass("addline");
		} else {
			this.$minicartlink.addClass("addline");
			this.$minicartlink.removeClass("addnoline");
		}
		// after a time out automatically close it
		timer.start(6000, this.close.bind(this));
	},
	/**
	 * @function
	 * @description Closes the mini cart with given delay
	 * @param {Number} delay The delay in milliseconds
	 */
	close: function (delay) {		
		$("body").removeClass("mini-cart-active");
		$("html").removeClass("mini-cart-active");
		$(window).scrollTop(lastTop);
		$('html').css("overflow", "visible");
		/*timer.clear();
		this.$content.slideUp(delay);
		this.$minicartlink.addClass("addnoline");
		this.$minicartlink.removeClass("addline");
		$('.mini-cart-link').removeClass('mobile-active');*/
	},
	
	openBonusDialog: function (params) {
		
		var $bonusProduct = $('#bonus-product-dialog');
		// create the dialog
		dialog.open({
			target: $bonusProduct,
			url: params.url,
			options: {
				width: 'auto', 
				maxWidth: 920,
				height: 'auto',				
				modal: true,
				fluid: true, 
				resizable: false,
				title: 'Get your free bonus items for a limited time!',
				draggable: true,
				responsive: true,
				autoResize:true
				
			},
			callback: function () {
				minicart.initBonusProductEvents();
				util.uniform();
				$(".ui-dialog-title").show();
				$(".ui-dialog").addClass('bonus-product-popup');
			}
		});		
	},
	initBonusProductEvents: function () {
		
		$('.size-select-area .fake-input').unbind("click"); 
		$('.size-select-area .fake-input').click(function(e){
			var $target =  $(this).closest('.size-select-area');
			var hasClass =  $target.hasClass('active-drop');
		    $( ".size-select-area").removeClass('active-drop');
		    if(hasClass)
		    {
		    	$target.removeClass('active-drop');
		    }
		    else
		    {
		    	$target.addClass('active-drop');
		    }
		    e.stopPropagation();
		});
		
		$(".variation-select").off("click", "li.select-item");
		$(".variation-select").on("click", "li.select-item", function(e){
		    e.preventDefault();
		    if($(this).hasClass('selected') || $(this).hasClass('out-of-stock')){ $(".size-select-area").removeClass('active-drop');e.stopPropagation();return;}	
		    $(this).addClass("selected").siblings().removeClass("selected");
		    $(this).closest('.size-select-area').find(".selectedValue").val($(this).attr('title'));
		    $(this).closest('.size-select-area').removeClass('active-drop');
		});

			$('#add-selected-bonus-products').on('click', function (e) {
		    	e.preventDefault();
		    	var bonusProductJSON = {};
		    	bonusProductJSON.bonusproducts = [];
		    	var isAnyProductSelected= false;
		    	//tulo pillow JSON data
		    	
		    	if ($('#tulo-pillow1').is(":checked")) {
			    	var pillow = {
			    			pid: $('#pillow1').find('.variation-select').find('.selected').attr('data-pid'),
			    			qty: $('#tulo-pillow1').attr('data-qty'),
			    			options: {}
			    		};
			    	
			    	bonusProductJSON.bonusproducts.push({product:pillow});
			    	isAnyProductSelected = true;
		    	}
		    	if ($('#tulo-pillow2').is(":checked")) {
			    	var pillow = {
			    			pid: $('#pillow2').find('.variation-select').find('.selected').attr('data-pid'),
			    			qty: $('#tulo-pillow2').attr('data-qty'),
			    			options: {}
			    		};
			    	
			    	bonusProductJSON.bonusproducts.push({product:pillow});
			    	isAnyProductSelected = true;
		    	}
		    	// tulo base JSON data
		    	if ($('#tulo-base').is(":checked")) {
			    	var tulobase = {
			    			pid: $('#tulo-base').attr('data-pid'),
			    			qty: $('#tulo-base').attr('data-qty'),
			    			options: {}
			    		};
			    	
			    	bonusProductJSON.bonusproducts.push({product:tulobase});
			    	isAnyProductSelected = true;
			    }
		    	
		    	if (isAnyProductSelected == false) {
					return;
				}
		    	
		    	var bliUUID = $(this).attr('data-bonusDiscountLineItemUUID'); 
		 		var url = util.appendParamsToUrl(Urls.addBonusProduct, {bonusDiscountLineItemUUID: bliUUID});
		 		
		 		// make the server call
		 		$.ajax({
		 			type: 'POST',
		 			dataType: 'json',
		 			cache: false,
		 			contentType: 'application/json',
		 			url: url,
		 			data: JSON.stringify(bonusProductJSON)
		 		})
		 		.done(function () {
		 			dialog.close();
		 			refreshMiniCart();
		 		})
		 		.fail(function (xhr, textStatus) {
		 			// failed
		 			if (textStatus === 'parsererror') {
		 				window.alert(Resources.BAD_RESPONSE);
		 			} else {
		 				window.alert(Resources.SERVER_CONNECTION_ERROR);
		 			}
		 		});
		    });
			
			// close bonus model on click of no thanks
			$('#bonus-no-thanks').on('click', function (e) {
				dialog.close();
			});
			// update add to cart button text
			$('.bonus-products input[type=checkbox]').change(function(){
			    var totalQty = 0;
				if ($('#tulo-pillow1').is(":checked")) {
					totalQty = totalQty + parseInt($('#tulo-pillow1').attr('data-qty'));
					$(document).find('#pillow1').find('.size-select-area .fake-input').unbind("click");
					$(document).find('#pillow1').find('.size-select-area .fake-input').click(function(e){
						var $target =  $(this).closest('.size-select-area');
						var hasClass =  $target.hasClass('active-drop');
					    $( ".size-select-area").removeClass('active-drop');
					    if(hasClass)
					    {
					    	$target.removeClass('active-drop');
					    }
					    else
					    {
					    	$target.addClass('active-drop');
					    }
					    e.stopPropagation();
					});
				}
				else{
					$(document).find('#pillow1').find('.size-select-area .fake-input').unbind("click"); 
				}
				
				if ($('#tulo-pillow2').is(":checked")) {
					totalQty = totalQty + parseInt($('#tulo-pillow2').attr('data-qty'));
					$(document).find('#pillow2').find('.size-select-area .fake-input').unbind("click");
					$(document).find('#pillow2').find('.size-select-area .fake-input').click(function(e){
						var $target =  $(this).closest('.size-select-area');
						var hasClass =  $target.hasClass('active-drop');
					    $( ".size-select-area").removeClass('active-drop');
					    if(hasClass)
					    {
					    	$target.removeClass('active-drop');
					    }
					    else
					    {
					    	$target.addClass('active-drop');
					    }
					    e.stopPropagation();
					});
				}
				else{
					$(document).find('#pillow2').find('.size-select-area .fake-input').unbind("click"); 
				}
				
				if ($('#tulo-base').is(":checked")) {
					totalQty = totalQty + parseInt($('#tulo-base').attr('data-qty'));		
				}
				var buttonText = 'Add free items to cart';
				if (totalQty === 0) {
					$('#add-selected-bonus-products').addClass('disabled-btn');
				}
				else{
					$('#add-selected-bonus-products').removeClass('disabled-btn');
					buttonText = (totalQty > 1) ? 'Add '+ totalQty +' free items to cart' : 'Add '+ totalQty +' free item to cart';
				}					
				$('#add-selected-bonus-products').text(buttonText);
			});
	}
	
};

module.exports = minicart;
