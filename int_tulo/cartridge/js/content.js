'use strict';

var dialog = require('./dialog'),
	progress = require('./progress');
function searchContent(){
	var $searchContainer = $(".help-and-faq-holder");	
	var $contentBoxes = $searchContainer.find(".help-faq-section");
	var $searchInput = $searchContainer.find("#search-faqs");
	var userInput =  $searchInput.val();
	//tulo-232 - event tracking
	ga('tealium_0.send', 'event', 'Help', 'Search', userInput);	
	  //make the input all lower case to make it compatible for searching
	  userInput = userInput.toLowerCase();

	  //Loop through all the content to find matches to the user input
	  $contentBoxes.each(function(){
	   var $thisTopic = $(this);
	    var headerText = $(this).find(".topic-head").text();
	 
	    //add the title and content of the contentbox to the searchable content, and make it lower case
	    var searchableContent = headerText; //+ " " + contentText+" "+questionText;
	    searchableContent = searchableContent.toLowerCase();
	    
	    //hide content that doesn't match the user input
	    if(searchableContent.indexOf(userInput) == -1){
	   
		   $(this).hide();  
		 //  var showBox =  false;
		   $(this).find(".faq-box").each(function(){
			    var questionText = $(this).find(".accordion-opener").text();
			    var contentText = $(this).find(".slide p").text();
			     var searchableInnerContent =  contentText+" "+questionText;
			     searchableInnerContent = searchableInnerContent.toLowerCase();
			    
			    //hide content that doesn't match the user input
			    if(searchableInnerContent.indexOf(userInput) == -1){
			      $(this).hide();
			    } else {    	
			    	$thisTopic.show();
			      $(this).show();
			    }
	 		});
	    } else {
	      $(this).show();
	      $(this).find(".faq-box").show();
	    }
	  });

}

function submitContactForm($form)
{
	var ajaxdest = $form.attr('action');
	var formData = $form.serialize();
	progress.show($('.help-contact-us'));
	$.post( ajaxdest, formData )
	.done(function( text ){
		var $response = $( text );
		//update content in minicart
		$(".help-contact-us").html($response);
		//tulo-232 - event tracking
		ga('tealium_0.send', 'event', 'Help', 'Contact Form', "Bottom Form");	
		content.initContactForm();
	});	
	
}
function setFAQPlaceholder(){
	if($("#search-faqs").length >0 )
	{
		
		 if ($(window).width() < 768) {
			 $("#search-faqs").attr("placeholder", "Search FAQs");
	  		}
		 else{
			 $("#search-faqs").attr("placeholder", "Search FAQs for your question or topic");
		 }
	}
	 
}
var content = {
	init: function () {
		this.initHeader();
		this.initLiveContent();
		this.initFaqSearch();
		this.initContactForm();
		this.initHelpGAEvents();
		setFAQPlaceholder();
		$( window ).resize( function(){
			setFAQPlaceholder();
		});
	},

	initHeader: function () {
		// Header Banner Slider Init
		$('.header-banner-top ul').slick({
			dots: false,
			arrows: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 2000
		});

		// Header Zip Dropdown
		$('.header-top .dropdown').on('click', function (e) {
			e.preventDefault();

			var link = $(this);
			var dropdown = $(this).next('.dropdown-block');

			if (link.hasClass('active')) {
				link.removeClass('active');
				dropdown.removeClass('active');
			} else {
				$('.header-top .dropdown').removeClass('active');
				$('.header-top .dropdown-block').removeClass('active');

				link.addClass('active');
				dropdown.addClass('active');

				if (link.parent().hasClass('header-zip')) {
					dropdown.find('input').focus();
				}
			}
		});
	},

	initLiveContent: function () {
		$('a.live-content').not('.initialized').on('click', function (e) {
			e.preventDefault();

			var contentId = $(this).data('content-id');
			if ((contentId != '') && (contentId != undefined)) {
				var params = {cid: contentId};

				$.ajax({
					url: Urls.liveContent,
					data: params,
					dataType: 'json',
					success: function (data) {
						if (data.content) {
							var html = '<div class="live-content-wrapper">' + data.content.body + '</div>';

							dialog.open({
								html: html,
								options: {
									maxHeight: 635,
									dialogClass: 'live-content-dialog',
									title: data.content.name
								}
							});
						}
					}
				});
			}
		}).addClass('initialized');
	},
	initFaqSearch :  function (){
		
		$("#help-search").find("#search-faqs").on("keyup", function(e){
			if($("#help-search").find("#search-faqs").val() == "")
			{
				// show all faqs if search field is empty
				$(".help-faq-section").show();
				$(".faq-box").show(); 
			}
			
		});
		var $searchContainer = $(".help-and-faq-holder");	
		var $searchBtn = $searchContainer.find("#search-faqs-button");
		$("#help-search").on("submit", function(e){
		  e.preventDefault();
			searchContent();
		});
	},
	initContactForm :  function (){		
		$("#ContactForm").on("submit", function(e){
		  e.preventDefault();
			submitContactForm($(this));
		});
	},
	initHelpGAEvents : function (){
		var $searchContainer = $(".help-and-faq-holder");	
		if($searchContainer.length > 0){
			$searchContainer.find(".chat-info").find(".chatlink").on("click", function(e){			 
				//tulo-232 - event tracking
				ga('tealium_0.send', 'event', 'Help', 'Live Chat', 'Top Link');	
			});
			$searchContainer.find(".store-info").find("a.btn-secondary").on("click", function(e){			 
				//tulo-232 - event tracking
				ga('tealium_0.send', 'event', 'Help', 'Find Store', 'Top Link');	
			});
			$searchContainer.find(".contact-info").find(".email a").on("click", function(e){			 
				//tulo-232 - event tracking
				ga('tealium_0.send', 'event', 'Help', 'Email Support', 'Top Link');	
			});
		}
		
		
	}
	
};

module.exports = content;