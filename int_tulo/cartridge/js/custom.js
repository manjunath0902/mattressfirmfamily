'use strict';
var progress = require('./progress'),
addToCart = require('./pages/product/addToCart'),
ajax = require('./ajax'),
util = require('./util'),
dialog = require('./dialog'),
amplience = require('./amplience');

//validating the airBnB form

function validateAirbnbForm(id) {
    var valid = jQuery(id).validate().checkForm();
      if (valid) {
          jQuery('#bnb_save').removeClass('disabled-btn');
      } else {
          jQuery('#bnb_save').addClass('disabled-btn');
      }
 }

var customjs =  {
	init: function() {
		this.textCarousel();
		this.productCarouselMobile();
		this.initAccordion();
		this.pdpBedCarousal();
		this.heroCarousel();
		this.initGeneralTabs();	
		this.accountSelectMenu();
		this.initSameHeightBlocks();
		this.initResizeActive();
			
		$('#isSuperHost').on('click', function(){
	        var radioValue = $("input[type='radio']:checked").val();
	        if(radioValue === "No"){
	            $('#isHost').css('display', 'block');
	            $('#mattPurchasing').css('display', 'block');
	        }
	        else if(radioValue === "Yes"){
	            $('#isHost').css('display', 'none');
	            $('#mattPurchasing').css('display', 'none');
	        }
		});
		
		if(typeof enableAirbnbFormValidation != 'undefined' && enableAirbnbFormValidation){
			if(performance.navigation.type == 2){  
				location.reload(true);
			}
			validateAirbnbForm('#dwfrm_airbnb');
		    jQuery('#dwfrm_airbnb').on('blur keyup change', 'input', function(event) {
		    	validateAirbnbForm('#dwfrm_airbnb');
		    });
		    $("#dwfrm_airbnb_bnbLink").addClass("validateurl");
		}
    
    if($('.competitive-comparison').length > 0){
    	this.initCategorySelection();
    	//this.initInputResizable();
    }
    
    if($('.competitive-comparison-mobile').length > 0){
    	this.initMobileCategorySelection();
    	//this.initInputResizable();
    }
        
    this.initBgVideoHomeHero();
    this.initReadOnlyField();
    //if($('#video-dialog iframe').length > 0){
    	//this.initVideoPopup();
    //}               
    this.initMobileDropOpener();
    this.intiMobileCartCheckoutVal();
    this.showHeaderPhone();
    this.initInputAutoScroll();
    this.initPdpDialog();
    this.mobileComparisonCarousel();
    this.gpdpReviewsCarousel();
    this.gpdpFixedComparisonHeader();
    if($('.ouradsPage-wrap').length > 0){
    	this.ourAds();
    } 
    this.handleRedCarpetForm();
    if($('.competitive-stores #counter').length > 0){
      this.competitiveStoresCounter();
  	}
    if($('.competitive-reviews .reviews-carousel-mobile').length > 0){
    	this.initCompetitiveReviewsMobile();
		}
    this.initClipboard();
    if($('.competitive-product-details').length > 0){
    	this.initSEOPDP();
    	this.initCompetitiveBuyFixed();
    }
    this.layersAnimation();
    this.initMattressPDPBuyFixed();
    if($('.promo-ads').length > 0){
    	this.homePromoAds();
    }	   

    if($('.liv-mattress-mobile-carousel').length > 0){
    	this.initLIVMattressMobileCarousel();
		}
		/*this.lazyloadBackgroundImages();*/
		this.SetPayHomeDialog();
		
	},
	//home page text carosuel
	textCarousel: function (){
		$('.textCarousel').slick({
			infinite: true,
			speed: 500,
			autoplay: true,
			autoplaySpeed: 6000,
			arrows: false,
			dots: true
		});
	},
	
	//home page hero carosuel
	heroCarousel: function (){
		$('.hero-slider').slick({
			infinite: true,
			speed: 200,
			autoplay: true,
			autoplaySpeed: 4000,
			arrows: false,
			dots: true,
			fade: true,
			pauseOnFocus: false,
			pauseOnHover: false,
			adaptiveHeight: false,
			draggable:false,
			responsive: [
				 { 
					breakpoint: 768,
				        settings: {
							autoplay: false,
							draggable:true,
							adaptiveHeight: false,
							fade: false,
							pauseOnFocus: true,
							pauseOnHover: true
				       }
				 }
			]
		});
	},

	//home page tulo products carosuel for mobile
	productCarouselMobile: function (){
		$('#home-products-carousel').slick({
			infinite: true,
			arrows: false,
			dots: true,
			slidesToShow: 1,
			responsive: [
		             {
		            	 breakpoint: 767,
		            	 settings: {
		            		 arrows: false,
		            		 slidesToShow: 1,
		            		 initialSlide: 1
		            		 }
		             }
		       ]
		});
	},
	
	// jqueryUi accordion generic
	initAccordion: function (){
		$( ".accordion" ).accordion({
			collapsible: true,
			active: false,
			heightStyle: "content"
		});
		$('.accordion-opener')
	        .off('click')
	    .click(function(e){
	    	// track faq expand event on help page
	    	if($(this).closest('.help-and-faq-holder').length > 0){
	    		if(!$(this).closest('.faq-box').find('div.ui-accordion-content').is(":visible"))
				{
	    			//tulo-232 - event tracking
	    			ga('tealium_0.send', 'event', 'Help', 'FAQ Link', $(this).text());	
				}
	    	}
	    	// end -  tack faq expand event on help page
	        $(this).next().toggle('fast');
	        $(this).toggleClass('ui-accordion-header-active');
	        e.preventDefault();
	    });
	},
	
	initInputResizable: function  () {
		function resizable (el, factor) {
			  var int = Number(factor) || 7.7;
			  function resize() {el.style.width = ((el.value.length+1) * int) + 'px'}
			  var e = 'keyup,keypress,focus,blur,change'.split(',');
			  for (var i in e) el.addEventListener(e[i],resize,false);
			  resize();
			}
		resizable(document.getElementById('selectedValue1'), 7);
		resizable(document.getElementById('selectedValue2'), 7);
	},
		
	//competitive-comparison custom dropdown 
	
	initCategorySelection: function (){
		var brand1, brand2;
		$(".queen-price").html($("#competitive").data('queenprice'));
		$( ".selectedValue" ).each(function() {
	        var $title =  $(this).closest('.select-area').find('li.selected').attr('title');
	        $(this).closest('.select-area').find('li.selected').hide();// hide currently selected value from the list 
	        if($title != null && $title != undefined)
	         {
	            $(this).val($title);  
	         }
	        
	        var brandID = $(this).closest('.select-area').find('li.selected').data('value');
	        var $comboID =  $(this).closest('.select-area').find("ul.categories-list").attr('id');
	        
		    if($comboID == "combo-1")
		    {
		    	$('.value-1').hide();
		    	$(".competitive-comparison").find("."+brandID).show();
		    	brand1 =  $(this).closest('.select-area').find('li.selected').attr('title');
		    	
		    }
		    else if($comboID == "combo-2")
		    {
		    	$('.value-2').hide();
		    	$(".competitive-comparison").find("."+brandID).show();
		    	brand2 = $(this).closest('.select-area').find('li.selected').attr('title');;
		    }            
	    });
		$(".categories-list").find( "li.li-category" )
		  .filter(function( index ) {
			  return  $(this).attr('title').indexOf(brand2) !== -1;
		  }).addClass('hidden').siblings().removeClass("hidden");
		$(".categories-list").find( "li.other-category" )
		  .filter(function( index ) {	
		    return  $(this).attr('title').indexOf(brand1) !== -1;
		  }).addClass('hidden').siblings().removeClass("hidden");
		
		
		$('.select-area .fake-input').click(function(e){
	        // dropdown open/close on click
	        var $target =  $(this).closest('.select-area');
	        var hasClass =  $target.hasClass('active-drop');
	        $( ".select-area").removeClass('active-drop');
	        if(hasClass)
	        {
	            $target.removeClass('active-drop');
	        }
	        else
	        {
	            $target.addClass('active-drop');
	        }
	        e.stopPropagation();
	    });
		
		$(".select-area").on("click", "li.li-category, li.other-category", function(e){
	        // selection of brand dropdown
		    e.preventDefault();
		    $(this).addClass("selected").siblings().removeClass("selected");
		    $(this).closest('.select-area').find(".selectedValue").val($(this).attr('title'));		 
		    $(this).closest('.select-area').removeClass('active-drop');
		    $(this).hide().siblings().show();// hide currently selected value from the list 		   
		    var $table = $(this).closest('.comparison-table');
		    var $currentValue = $(".selectedValue").val(),
	        $SelectList = $("ul.categories-list"),
	        $ListTitle = $(this).attr('title');
		    var $comboID =  $(this).closest('ul').attr('id');
		    var brandID = $(this).data('value');
		    if($comboID == "combo-1")
		    {
		    	$('.value-1').hide();
		    	$(".competitive-comparison").find("."+brandID).show();
		    	brand1 =  $(this).closest('.select-area').find('li.selected').attr('title');
		    	
		    }
		    else if($comboID == "combo-2")
		    {
		    	$('.value-2').hide();
		    	$("."+brandID).show();
		    	brand2 =  $(this).closest('.select-area').find('li.selected').attr('title');
		    }  
		    
		    $( "li.li-category" )
			  .filter(function( index ) {
				  return  $(this).attr('title').indexOf(brand2) !== -1;
			  }).addClass('hidden').siblings().removeClass("hidden");
			$( "li.other-category" )
			  .filter(function( index ) {	
			    return  $(this).attr('title').indexOf(brand1) !== -1;
			  }).addClass('hidden').siblings().removeClass("hidden");
			
	       // this.initInputResizable();

	    });
		$(document).on("click", function(e) {
			// hide drop down on click outside
		    if ($(e.target).is(".select-area") === false) {
		    	$(".select-area").removeClass('active-drop');
		    }
		
		 });
	},
	
	initMobileCategorySelection: function (){	
        var $title =  $('.select-area-mobile').find('li.selected').attr('title');
        $('.select-area-mobile').find('li.selected').hide();// hide currently selected value from the list 
        if($title != null && $title != undefined)
         {
            $("#selectedValue-mbl").val($title);  
         }
        
        var brandID = $('.select-area-mobile').find('li.selected').data('value');
        var $comboID =  $('.select-area-mobile').find("ul.categories-list").attr('id'); 
    	$('.value-mbl').hide();
    	$(".competitive-comparison-mobile").find("."+brandID).css('display', 'inline-block');
		$('.select-area-mobile .fake-input').click(function(e){
	        // dropdown open/close on click
	        var $target =  $(this).closest('.select-area-mobile');
	        var hasClass =  $target.hasClass('active-drop');
	        //$( ".select-area-mobile").removeClass('active-drop');
	        if(hasClass)
	        {
	            $target.removeClass('active-drop');
	        }
	        else
	        {
	            $target.addClass('active-drop');
	        }
	        e.stopPropagation();
	    });
		
		$(".select-area-mobile").on("click", "li.li-category", function(e){
	        // selection of brand dropdown
		    e.preventDefault();
		    $(this).addClass("selected").siblings().removeClass("selected");
		    $(this).closest('.select-area-mobile').find(".selectedValue-mbl").val($(this).attr('title'));		 
		    $(this).closest('.select-area-mobile').removeClass('active-drop');
		    $(this).hide().siblings().show();// hide currently selected value from the list 
		    var brandID = $(this).data('value');		  
	    	$('.value-mbl').hide();
	    	$(".competitive-comparison-mobile").find("."+brandID).css('display', 'inline-block');	
	    });
		$(document).on("click", function(e) {
			// hide drop down on click outside
		    if ($(e.target).is(".select-area-mobile") === false) {
		    	$(".select-area-mobile").removeClass('active-drop');
		    }
		
		 });
	},
	
	// carousal for pdp mobile setup-bed-carousal
	
	pdpBedCarousal :  function (){
		$('.setup-bed-carousal').slick({
			dots: false,
	    	arrows: false,
	    	centerPadding: '20px',
			infinite: false,
			speed: 300,
			slidesToShow: 4,
			slidesToScroll: 4,
			
		  responsive: [
		    {
		      breakpoint: 767,
		      settings: {
	    	  dots: true,
	    	  infinite: true,
	    	  speed: 300,
	    	  slidesToShow: 1,
	  			slidesToScroll: 1,	
		      }
		    }
		  ]
		}); 
	},
	initGeneralTabs : function () {
		// jqueryUi tabs generic
		 
		   $( function() {
		     $( ".tabs" ).tabs();
		   } );
	},	
	
	
	/**
	 * @description account mobile menu like a select
	 */

	accountSelectMenu: function () {
		$('.account-mobile-menu .fake-input').click(function(e){
		    $(this).closest('.dropdown-area').toggleClass('active-drop');
		    e.stopPropagation()
		});
		$(document).on("click", function(e) {
		    if ($(e.target).is(".account-mobile-menu") === false) {
		    	$(".dropdown-area").removeClass('active-drop');
		    }
		 });
		$(".links-drop").on("click", "li", function(e){
		    $("#selectedOption").val($(this).text());
		    $(".dropdown-area").removeClass('active-drop');
		});
		
	},
    initSameHeightBlocks : function () {
    								
        // Select and loop the container element of the elements you want to equalise
        $('.sameHeight-container').each(function(){  
          
          // Cache the highest

          var highestBox = 0;
          
          // Select and loop the elements you want to equalise
          $('.sameHeight-column', this).each(function(){
            $('.sameHeight-column').removeAttr("style");
            // If this box is higher than the cached highest then store it
            if($(this).height() > highestBox) {
              highestBox = $(this).height(); 
            }
          
          });  
                
          // Set the height of all those children to whichever was highest 
            if ($(window).width() > 768) {
    			$('.sameHeight-column',this).height(highestBox);
    		}      
                        
        }); 
        
    },
    initResizeActive : function () {
    	var win = $(window),
		doc = $('html'),
		resizeClass = 'resize-active',
		flag, timer;
		var removeClassHandler = function() {
			flag = false;
			doc.removeClass(resizeClass);
		};
		var resizeHandler = function() {
			if(!flag) {
				flag = true;
				doc.addClass(resizeClass);
			}
			clearTimeout(timer);
			timer = setTimeout(removeClassHandler, 500);
		};
		win.on('resize orientationchange', resizeHandler);
    },
    initBgVideoHomeHero : function () {
    	if($('.dedicated-video-area').find('#bg-dark-vdo').length > 0){
    		$('.dedicated-video-area').find('#bg-dark-vdo').get(0).pause();
    		$(document).on('click', '.btn-play-type-mattress', function(){
    			$(this).closest('.dedicated-video-area').toggleClass('active-click');
    			$('.dedicated-video-area').find('#bg-dark-vdo').get(0).play();
    		});
    	}
    },
    
    initReadOnlyField : function () {
	    $('input[readonly]').focus(function(){
	        this.blur();
	    });
    },
    
    initVideoPopup : function () {
    	
	  //click event for video section
	  //$(".btn-play").on("click", function () {
	//	$(this).closest('.dedicated-video').addClass('video-active');
	 //   var url = $('#video-dialog iframe').attr('src');
	 //   $("#video-dialog iframe").vimeo("play");
	 // });

	 

		//$(".btn-play-close").on("click", function () {
		//	$( ".dedicated-video").removeClass('video-active');
		//	$('#video-dialog iframe').attr('src', '');
		//	$('#video-dialog iframe').attr('src', url);
		//	$("#video-dialog iframe").vimeo("pause");
		//});

    },
    
    initPdpDialog : function () {

        $("#tulo-frame-modal").dialog({
            autoOpen : false, modal : true
        });
        $(".tulo-package-section a.learn-more").on( "click", function() {
              $("#tulo-frame-modal").dialog( "open" );
              $("#tulo-frame-modal").parent().addClass('frame-dialog');
        });
   

    },
    initMobileDropOpener : function () {
    	if ($(window).width() < 768) {
    		$('body').on('click', '.drop_opener', function(){
    			$(this).closest('li').toggleClass('dropdown-active');
    		});
    	}
    },
    intiMobileCartCheckoutVal : function (){
    	var cartItemText = $(".mini-cart-content").find('.text-tag').text();
    	var mainClass = $("#wrapper");
    	
    	if(mainClass.hasClass("pt_checkout")){
    		if ($(window).width() < 768) {
    			$("#mini-cart").find(".minicart-quantity").text(cartItemText);
    		}
    	}
    },

    //Our Ads page Script
	ourAds: function (){
		var playID = "1";
		var hash = window.location.hash;
		if(hash != "" && hash != null && hash != undefined)
		{
			playID = hash.slice(1);	
		}
			$(".video").each(function(){
			    $(this).hide();
			    if($(this).attr('data-video-id') == playID) {
			        $(this).show();
			        $(this)[0].src += "&autoplay=1";
			    }
			});
			$(".content").each(function(){
				$(this).hide();
			    if($(this).attr('data-content-id') == playID) {
			    	$(this).show();
			    }
			});
			$(".video-thumb").each(function(){
			    if($(this).attr('data-thumb-id') == playID) {
			    	$(this).hide();
			    }
			});
			$('.video-thumb').on( "click", function(e) {
			    e.preventDefault();
			    $('.video-thumb').show();
			    $(this).hide();
			    var thumbid = $(this).attr('data-thumb-id'); 
			    $(".video").each(function(){
			    	var videoURL = $(this).prop('src');
					videoURL = videoURL.replace("&autoplay=1", "");
					$(this).prop('src','');
					$(this).prop('src',videoURL);

			        $(this).hide();
			        if($(this).attr('data-video-id') == thumbid) {
			            $(this).show();
			            $(this)[0].src += "&autoplay=1";
			        }
			    });
			     $(".content").each(function(){
			        $(this).hide();
			        if($(this).attr('data-content-id') == thumbid) {
			            $(this).show();
			        }
			    });
			});		
	},
	initSliderPDP :  function (){
		$('.technology-section .layers-details-mobile').slick({
			  dots: true,
			  infinite: true,
			  speed: 300,
			  slidesToShow: 1,
			  arrows: false,
			  adaptiveHeight: true
			});
		$('.reviews-list').slick({
		  dots: true,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 3,
		  arrows: false,
		  adaptiveHeight: true,

			responsive: [
			    {
			      breakpoint: 767,
			      settings: {
		    	  dots: true,
		    	  infinite: true,
		    	  speed: 300,
		    	  slidesToShow: 1,
		  			slidesToScroll: 1,	
			      }
			    }
			  ]
		});
	},

	showHeaderPhone : function () {
		$('.Hphone-tablet').off().on("click", function(){
			$(this).closest('li').toggleClass('active-phone');
		});
		$(document).on("click", function(e) {
		   	var phonediv = $(".Hphone-tablet");
		   	var phonetooltip = $(".Hphone-tablet-wrap .tooltip-holder");
		    if (phonediv.has(e.target).length === 0 && phonetooltip.has(e.target).length === 0)
		    {
		       $(".Hphone-tablet-wrap").removeClass('active-phone');
		    }
		});
		$('.Hphone-mobile').off().on("click", function(){
			$(this).closest('li').toggleClass('active-phone');
		});
		$(document).on("click", function(e) {
		   	var phonediv = $(".Hphone-mobile");
		   	var phonetooltip = $(".Hphone-mobile-wrap .tooltip-holder");
		    if (phonediv.has(e.target).length === 0 && phonetooltip.has(e.target).length === 0)
		    {
		       $(".Hphone-mobile-wrap").removeClass('active-phone');
		    }
		});
	},
	initInputAutoScroll :  function (){
		/*$('.input-text').on('focus', function() {
		    $('html, body').animate({
		        scrollTop: $(document).scrollTop() + 60
		    }, 300);
		});*/
		/*if ($(window).width() < 1200) {
			$('input, button, a').focus(function () {
			    var footerHeight = 120; //footerHeight = footer height + element height + buffer
			    var element = $(this);
			    if (element.offset().top - ($(window).scrollTop()) > ($(window).height() - footerHeight)) {
			        $('html, body').animate({
			            scrollTop: element.offset().top - ($(window).height() - footerHeight)
			        }, 500);
			    }
			    console.log(element.offset().top - ($(window).height() - footerHeight))

			});
		}*/		
		if ($(window).width() < 1200) {
			$("#zipCode-text").focus(function(){
		        $(".embeddedServiceHelpButton").hide();	
		    });
		    $("#zipCode-text").blur(function(){
		        //$(".embeddedServiceHelpButton").show();
		        setTimeout(function() { $(".embeddedServiceHelpButton").show(); }, 1000);
		    }); 

			$('.pt_account input:text, .pt_account input:password, .pt_account textarea, .pt_content input:text, .pt_content input:password, .pt_content textarea, .checkout-shipping input:text, .checkout-shipping input:password, .checkout-shipping textarea, .checkout-billing input:text, .checkout-billing input:password, .checkout-billing textarea, #email-alert-address').focusin(function () {			
				$(".embeddedServiceHelpButton").hide();			
			});

			$('.pt_account input:text, .pt_account input:password, .pt_account textarea, .pt_content input:text, .pt_content input:password, .pt_content textarea, .checkout-shipping input:text, .checkout-shipping input:password, .checkout-shipping textarea, .checkout-billing input:text, .checkout-billing input:password, .checkout-billing textarea, #email-alert-address').focusout(function () {						
				$(".embeddedServiceHelpButton").show();			
			}); 
		} 		
		else {
			$("#zipCode-text").focus(function(){
		        $(".embeddedServiceHelpButton").show();	
		    });
		    $("#zipCode-text").blur(function(){
		        $(".embeddedServiceHelpButton").show();	
		    }); 

			$('.pt_account input:text, .pt_account input:password, .pt_account textarea, .pt_content input:text, .pt_content input:password, .pt_content textarea, .checkout-shipping input:text, .checkout-shipping input:password, .checkout-shipping textarea, .checkout-billing input:text, .checkout-billing input:password, .checkout-billing textarea, #email-alert-address').focusin(function () {			
				$(".embeddedServiceHelpButton").show();			
			});

			$('.pt_account input:text, .pt_account input:password, .pt_account textarea, .pt_content input:text, .pt_content input:password, .pt_content textarea, .checkout-shipping input:text, .checkout-shipping input:password, .checkout-shipping textarea, .checkout-billing input:text, .checkout-billing input:password, .checkout-billing textarea, #email-alert-address').focusout(function () {						
				$(".embeddedServiceHelpButton").show();			
			}); 
		} 		
	},
	//General PDP Mobile Comparison list carosuel
	mobileComparisonCarousel: function (){
		$('.mobile-comparison-list-carousel').slick({
			dots: true,
			centerMode: true,
			centerPadding: '30px',
			slidesToShow: 1,
			arrows: false,
			initialSlide: 1,
			autoplay: false
		});
	},
	//General PDP Mobile Reveiws carosuel
	gpdpReviewsCarousel: function (){
		$('.foam-reviews-carousel').slick({
			infinite: true,
			speed: 500,
			autoplaySpeed: 6000,
			arrows: false,
			dots: true,
			initialSlide: 1,
			autoplay: false
		});
	},

	gpdpFixedComparisonHeader: function() {
		if($('.pt_product-details').length > 0 && $('.GPDP-comparison-table').length > 0){

		var fixmeTop2 = $('.GPDP-comparison-table').offset().top - $('.comparison-section-title').outerHeight();
		var unfixmeTop2 = $('.GPDP-setup-is-easy').offset().top - $('.comparison-section-title').outerHeight() - $('#header').outerHeight();
		$(window).scroll(function() {                  // assign scroll event listener
	    var currentScroll = $(document).scrollTop(); // get current position
	    if (currentScroll >= fixmeTop2 && currentScroll < unfixmeTop2 && !$('.comparison-section-title').hasClass('fixed')) { 
	    	// apply position: fixed if you
	        $('.comparison-section-title').addClass("fixed").css({                      // scroll to that element or below it
	            position: 'fixed',
	            top: $('#header').outerHeight(),
	            left: '0',
	            right: '0'
	        });
	    } else if(currentScroll < fixmeTop2 || currentScroll > unfixmeTop2) {                                   // apply position: static
	    	$('.comparison-section-title').removeClass("fixed").css({position: "relative", top: "0px"});
	    }

		});
		
		}  
	},

	handleRedCarpetForm: function (){
		
		if($("#zip-red-carpet").length > 0){
			$("#zip-red-carpet").on('blur focusout','#zip-red-carpet-text', function () {
			  	var $postalcodetmp = this.value;
			  	IsValidPDPZipCode($postalcodetmp);
			});
		  
			$("#zip-red-carpet").on('keyup', '#zip-red-carpet-text', function () {
			  	var $postalcodetmp = this.value;
			  	EnableDisablePDPZip($postalcodetmp);
			});
		  
		  $("#zip-red-carpet").on('keypress','#zip-red-carpet-text',function (e) {
		  	if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
		          return false;
		     }
		  });
		
		 $("#zip-red-carpet").on('click', "#zip-red-carpet-submit", function(e) {
				e.preventDefault();
		    	var zipCode = $(this).closest('form').find('#zip-red-carpet-text').val();
		    	if(zipCode == "" || zipCode ==  null || zipCode == undefined)
		    	{
		    		return;
		    	}
		    	var params = {
		                format: 'ajax',
		                zipCode: zipCode
		        };
		    	progress.show('.section-zip-code');
		    	$('#red-zip').html(zipCode);
		    	$.ajax({
					url: Urls.redCarpetLookup,
					type: 'post',
					data: params,
					success: function (response) {
						progress.hide('.section-zip-code');
				    	if (response) {
				    		$(".not-eligible-message").addClass("hidden");
				    		$(".eligible-message").removeClass("hidden");
				    		
						}
				    	else{
				    		$(".eligible-message").addClass('hidden');	 
				    		$(".not-eligible-message").removeClass('hidden');				    		          
				    	}
					},
					failure: function () {
						progress.hide('.section-zip-code');
						window.alert(Resources.SERVER_ERROR);
						
					}
				});
			   
		  });
		}
		 function IsValidPDPZipCode(zip) {
				var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
				if (!isValid && zip.length >0) {
					if ($("#form-pdp-zipcode .postalerror").length < 1) {
						$("#form-pdp-zipcode .fields-wrap").append("<span class='error postalerror'>" + Resources.INVALID_ZIP + "</span>");			
						$("#btn-pdp-zipcode").attr("disabled", "disabled");
					}
					return false;
				} else {
					if ($("#form-pdp-zipcode .postalerror").length > 0) {			
						$("#form-pdp-zipcode span.postalerror").remove();
						$("#btn-pdp-zipcode").removeAttr("disabled");
					}
					return true;
				}
			}
	
			function EnableDisablePDPZip(zip) {
				var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
				if (!isValid) {				
					$("#btn-pdp-zipcode").attr("disabled", "disabled");
					return false;
				}		
				 else {	
					$("#btn-pdp-zipcode").removeAttr("disabled");
					return true;
				}	
			}
	},

	competitiveStoresCounter: function() {
		var a = 0;
		$(window).scroll(function() {

		  var oTop = $('.competitive-stores #counter').offset().top - window.innerHeight;
		  if (a == 0 && $(window).scrollTop() > oTop) {
		    $('.counter-value').each(function() {
		      var $this = $(this),
		        countTo = $this.attr('data-count');
		      $({
		        countNum: $this.text()
		      }).animate({
		          countNum: countTo
		        },

		        {

		          duration: 2000,
		          easing: 'swing',
		          step: function() {
		            $this.text(Math.floor(this.countNum));
		          },
		          complete: function() {
		            $this.text(this.countNum);
		            //alert('finished');
		          }

		        });
		    });
		    a = 1;
		  }

		});
	},
	initCompetitiveReviewsMobile :  function (){
		$('.competitive-reviews .reviews-carousel-mobile').slick({
			  dots: true,
			  infinite: true,
			  speed: 300,
			  slidesToShow: 1,
			  arrows: false,
			  adaptiveHeight: true
		});
	},
	initClipboard : function() {
		$( "[data-clipboard-trigger]").each(function(i, e){
			var $element = $(e);
			var $bubble = $( $(e).attr("data-clipboard-bubble") );
			var clipboard = new ClipboardJS(e);

			$bubble.bind("click", function(event){ event.preventDefault(); });

			// get the parent of the target copied content and insert
			// a bubble to alert the user they can paste now
			clipboard.on('success', function( e ) {
				var $parent = $( $( e.trigger ).attr("data-clipboard-target") );

				$parent.after($bubble);
				$bubble.css("display", "block");

				setTimeout(function(){
					$bubble.css("display", "none");
				}, 4000);
			});

			$(e).bind("click", function(event){ event.preventDefault(); });
		});

	},
	initCompetitiveBuyFixed : function() {
		if($('.competitive-product-details').length > 0){

			var fixmeTop = $('.competitive-product-details').offset().top + $('.competitive-product-buy').height();
			$(window).scroll(function() {                  // assign scroll event listener
			    var currentScroll = $(document).scrollTop(); // get current position
			    if (currentScroll >= fixmeTop && $('.competitive-product-buy').hasClass('fixed')) { 
			    	// apply position: fixed if you
			    	$('.competitive-product-buy').removeClass("fixed");
			    } else if(currentScroll < fixmeTop) {                                   // apply position: static
			    	$('.competitive-product-buy').addClass("fixed");
			    }
			});
				
		}  
	},
	initSEOPDP : function (){
		var displayUpward = function ($targetElement) {
			var windowHeight = $(window).height();
			var staticbottom =  $(".competitive-product-buy").offset().top + $(".competitive-product-buy").height();  //8190

			var scrollbottom = windowHeight - (staticbottom  - $(window).scrollTop())

			var varselectHeight = $targetElement.height();

			if (scrollbottom < varselectHeight ) {
				return true;
			}
			else {
				return false;
			} 
			
		};
		var selectSize = function () {	
			// show selected values
			  $( ".selectedValue" ).each(function() {
				  var $value =  $(this).closest('.size-select-area').find('li.selected').attr('title');
				  if($value != null && $value != undefined)
			       {
					  $(this).html($value);  
					  $(this).data("value",$value);  
			       }		  
				   	 
			  });	
			
			$('.size-select-area .fake-input').click(function(e){
				e.preventDefault();				
				var $target =  $(this).closest('.size-select-area');
				
				/*
				 *  Open the menus upward if less space available below
				 */
				$(".competitive-product-buy").removeClass("upward");
				if( !($(".competitive-product-buy").hasClass('fixed')) && displayUpward($target.find(".variation-select")))
				{
					$(".competitive-product-buy").addClass("upward");
				}
				//////
				var hasClass =  $target.hasClass('active-drop');
			    $( ".size-select-area").removeClass('active-drop');
			    if(hasClass)
			    {
			    	$target.removeClass('active-drop');
			    }
			    else
			    {
			    	$target.addClass('active-drop');
			    }
			    e.stopPropagation();
			});
			$(document).on("click", function(e) {
				// hide size/comfort tool tip on click outside
			    if ($(e.target).is(".size-select-area") === false) {
			    	$(".size-select-area").removeClass('active-drop');
			    }			 
			 });
			$(document).mouseup(function(e) {
			    var hidedrop = $(".add-to-cart-container");
			    // if the target of the click isn't the container nor a descendant of the container
			    if (!hidedrop.is(e.target) && hidedrop.has(e.target).length === 0) 
			    {
			        $(".add-to-cart-area").removeClass('comfort-not-select');
			    }
			});
			$(".variation-select").on("click", "li.select-item", function(e){
			    e.preventDefault();
			    if($(this).hasClass('selected') || $(this).hasClass('out-of-stock')){ $(".size-select-area").removeClass('active-drop');e.stopPropagation();return;}	
			    $(this).addClass("selected").siblings().removeClass("selected");
			    if($(this).hasClass('li-select')) // only for size select, update the label div to be used for mobile 
			    {
			    	if($(this).closest('.size-select').find('.mobile-caption').length > 0)
			    	{
			    		$(this).closest('.size-select').find('.mobile-caption').html($(this).data('displayval')+":");
			    	}
			    	
			    }
			   // $(this).closest('.size-select-area').find(".selectedValue").val($(this).attr('title'));
			    $(this).closest('.size-select-area').find(".selectedValue").html($(this).attr('title'));
			    $(this).closest('.size-select-area').find(".selectedValue").data("value",$(this).attr('title'));
			    $(this).closest('.size-select-area').removeClass('active-drop');
			    $(".tooltip-part").removeClass('active-tooltip');
			});
		}; // end selectSize
		
		// change content on size change
		var updateContent = function (href, ampset) {
			if (ampset == null) {
				var ampset = '';
				}
			var $pdpForm = $('.pdpForm');
			var qty = $pdpForm.find('input[name="Quantity"]').first().val();
			var params = {
				Quantity: isNaN(qty) ? '1' : qty,
				format: 'ajax'
			};			
			//progress.show($('#pdpMain'));
			ajax.load({
				url: util.appendParamsToUrl(href, params),
				target: $('#action-container'),
				callback: function () {
					selectSize();
				}
			});
		};
		
		// change content on comfort change
		var updateCompleteContent = function (pid, href, ampset) {
			var ajaxURL = href;	
			if (ampset == null) {
				var ampset = '';
				}
			var $pdpForm = $('.pdpForm');
			var qty = $pdpForm.find('input[name="Quantity"]').first().val();
			var params = {
				Quantity: isNaN(qty) ? '1' : qty,
				format: 'ajax'
			};			
			//to retain size selection on comfort load
			var variationsData = $(".product-variations").data('attributes');
			if(variationsData)
			{
				
				var selectedSize = variationsData.size.value;
				ajaxURL = util.appendParamToURL(ajaxURL,"dwvar_"+pid+"_size",selectedSize );
			}
			var isFixed =  $("#action-container").hasClass("fixed");
			ajax.load({
				url: util.appendParamsToUrl(ajaxURL, params),
				target: $('#seo-pdp-content'),
				callback: function () {	
					if(!isFixed)
					{
						$("#action-container").removeClass("fixed")
					}
					if (ampset != '') {
						image.replaceImages();
						amplience.initZoomViewer(ampset);
					}
					amplience.initZoomViewer();
					selectSize();
					
				}
			});
		};

		var $pdpMain = $('.competitive-product-details');
		//add to cart event
		$pdpMain.on('click', '.add-to-cart', function(e){
			e.preventDefault();
			addToCart.AddSpecialToCart(e, $(this));
		   
		});
		$pdpMain.on('click', '.li-comfort-select', function (e) {
			if ($(this).data('url').length === 0) { 
				return; 
			}
			updateCompleteContent($(this).data('pid'), $(this).data('url'), "");
		});
		// change drop down variation attribute - should replace product content with new variant
		$pdpMain.on('click', '.li-select', function (e) {	
			if( (!$(this).hasClass('select-item') && $(this).hasClass('selected')) || $(this).hasClass('out-of-stock') || $(this).data('url').length === 0)
			{
				$(".tooltip-part").removeClass('active-tooltip');	
				e.stopPropagation();
				return;
			}
			updateContent($(this).data('url'),"");
		});
		
		
		selectSize();//
		
	},
	layersAnimation : function() {
		
		if($('.pdp-layers-section').length > 0){
			$(".notation").on( "click", function(e) {
			    e.preventDefault();
			    $(".notation").removeClass( "active" );
			    $(this).addClass( "active" );
			    var notationid = $(this).attr("data-notation-id"); 
			    $(".layer-img").each(function(){
			        $(this).removeClass( "scale-up" );
			        if($(this).attr("data-image-id") == notationid) {
			            $(this).addClass( "scale-up" );
			        }
			    });
			    $(".layer-details").each(function(){
			        $(this).hide();
			        if($(this).attr("data-layer-id") == notationid) {
			            $(this).show();
			        }
			    });
			});	

			$(".layer-link").on( "click", function(e) {
			    e.preventDefault();
			    $(".default-details").hide();
			    $(".layers-img").removeClass("collapsed");
			    $(".layers-img").addClass("expanded");
			    $(".layer-link").removeClass( "active" );
			    $(this).addClass( "active" );
			    var linkid = $(this).attr("data-link-id"); 
			    $(".layer-img").each(function(){
			        $(this).removeClass( "move-right" );
			        if($(this).attr("data-image-id") == linkid) {
			            $(this).addClass( "move-right" );
			        }
			    });
			    $(".layer-details").each(function(){
			        $(this).hide();
			        if($(this).attr("data-layer-id") == linkid) {
			            $(this).show();
			        }
			    });
			});	

			$(".layers-img").on( "click", function(e) {
			    e.preventDefault();
			    $(this).removeClass("expanded");
			    $(this).addClass("collapsed");	
			    $(".layer-img").each(function(){
			        $(this).removeClass( "move-right" );
			    });
			    $(".layer-details").each(function(){
			        $(this).hide();
			    });	
			    $(".default-details").show();    
			    $(".layer-link").removeClass( "active" );
			    setTimeout(function(){
			        $(".layers-img").removeClass("collapsed");
			    	$(".layers-img").addClass("expanded");	
			   }, 1000);
			});	
		
		 	$(window).scroll(function() {    
	    	var elementTop = $(".sepecification-area-new").offset().top;
			var elementBottom = elementTop + $(".sepecification-area-new").outerHeight();
			var viewportTop = $(window).scrollTop();
			var viewportBottom = viewportTop + $(window).height();
	        if (elementBottom > viewportTop && elementTop < viewportBottom) {
	        	$(".layers-img-mobile").removeClass("collapsed");
	            $(".layers-img-mobile").addClass("expanded");
	            $(".layers-img").removeClass("collapsed");
	            $(".layers-img").addClass("expanded");
	        } else {
	          /*  $(".layers-img-mobile").removeClass("expanded");
	            $(".layers-img-mobile").addClass("collapsed");
	            $(".layers-img").removeClass("expanded");
	            $(".layers-img").addClass("collapsed");
	            $(".layer-img").removeClass( "scale-up" );
	            $(".layer-img").removeClass( "move-right" );
	            $(".layer-link").removeClass( "active" );
	            $(".notation").removeClass( "active" );
	            $(".layer-details").hide();
	            $(".default-details").show(); */
	        }
	    });
		}
	},

	initMattressPDPBuyFixed : function() {
		if($('.mattresspdp-product-buy').length > 0) {
			$('#wrapper').addClass("mattress-pdp-wrapper");
			var fixmeTop = $('.section-buy-product-new').height() - 150;
			var currentScroll = $(document).scrollTop(); 
			if (currentScroll >= fixmeTop) { 			    	
				$('.mattresspdp-product-buy').addClass("fixed");
			} else if(currentScroll < fixmeTop) {                                   
				$('.mattresspdp-product-buy').removeClass("fixed");
			}
			$(window).scroll(function() {                  
			    var currentScroll = $(document).scrollTop(); 
			    if (currentScroll >= fixmeTop) { 			    	
			    	$('.mattresspdp-product-buy').addClass("fixed");
			    } else if(currentScroll < fixmeTop) {                                   
			    	$('.mattresspdp-product-buy').removeClass("fixed");
			    }
			});
		}
	},  

	homePromoAds: function () {
		$('.promo-ads').slick({
			infinite: false,
			slidesToShow: 1,
  		slidesToScroll: 1,
  		speed: 500,
			autoplay: true,
			autoplaySpeed: 6000,
			arrows: false,
			dots: true
		});
	},

	//add simple support for lazyload background images:
	/*lazyloadBackgroundImages: function () {
		document.addEventListener('lazybeforeunveil', function(e){
	    var bg = e.target.getAttribute('data-bg');
	    if(bg){
	        e.target.style.backgroundImage = 'url(' + bg + ')';
	    }
		});
	},*/

	initLIVMattressMobileCarousel :  function (){
		$('.liv-mattress-mobile-carousel').slick({
			  dots: true,
			  infinite: true,
			  speed: 300,
			  slidesToShow: 1,
			  arrows: false,
			  adaptiveHeight: true
		});
	},

	SetPayHomeDialog :  function (){
		$('body').on('click', '.SetPay-home-details', function (e) {
			e.preventDefault();
			dialog.open({
				url: $(this).attr('href'),
				options: {
					title:$(this).attr('title'),
					dialogClass: 'SetPay-home-dialog',
					position:'center',
					open: function () {
						var dialog = this;
						$(".ui-dialog-title").css('text-align', 'center');
						$(".ui-widget-overlay").on("click", function() {
							$(dialog).dialog("close");
						});
					}
				}
			});
			return false;
		});
	}

};

module.exports = customjs;

