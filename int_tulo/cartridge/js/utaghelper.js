'use strict';
var util = require('./util');

var tagManager = {
		//Send Bonus Products Details to Tealium
		addBonusProducttUtag: function () {
			if(utag) {
				var bonusProducts = $('.mini-cart-total').data('bonusproducts') != null ? $('.mini-cart-total').data('bonusproducts') : null;		
				var index = 0;
				if(bonusProducts != null){
					while (bonusProducts[index]) {
						var bonusProduct = bonusProducts[index];
						var productImageURL = bonusProduct.productexternalid != 0 ? 'https://' + SitePreferences.amplienceHost +'/i/hmk/'+ bonusProduct.productexternalid+'?h=1220&w=1350' : '';
						var productURL = util.appendParamsToUrl(Urls.getHttpProductUrl, {"pid": bonusProduct.variantid});
						if(typeof utag != undefined && utag != null ){ 
							utag.link({ 
								"enhanced_actions"		:	"add", 
								"product_id"			:	[bonusProduct.productid],
					            "product_name"			:	[bonusProduct.productname],
					            "product_brand"			:	[bonusProduct.brand],
					            "product_quantity"		:	[bonusProduct.quantity.toString()],
					           	"product_price"			:	["0.00"],
					            "product_unit_price"	:	["0.00"],
					            "product_category"		:	[bonusProduct.productcategory],
					            "product_sku"			:	[bonusProduct.productsku],
					            "product_child_sku"		:	[bonusProduct.productchildsku], 
					            "product_img_url"		:	[productImageURL],
					            "product_url"			:	[productURL],
					            "product_size"			:	[bonusProduct.productsize]
							});
						}
						
						index++;
					}
				}
			}
		},
		addProducttUtagByJson: function (productsJson) {
			if(utag) {		
				var index = 0;
				if(productsJson != null){
					while (productsJson[index]) {
						var product = productsJson[index];
						var productImageURL = product.productexternalid != 0 ? 'https://' + SitePreferences.amplienceHost +'/i/hmk/'+ product.productexternalid+'?h=1220&w=1350' : '';
						var productURL = util.appendParamsToUrl(Urls.getHttpProductUrl, {"pid": product.variantid});
						if(typeof utag != undefined && utag != null ){ 
							utag.link({ 
								"enhanced_actions"		:	"add", 
								"product_id"			:	[product.productid],
					            "product_name"			:	[product.productname],
					            "product_brand"			:	[product.brand],
					            "product_quantity"		:	[product.quantity.toString()],
					           	"product_price"			:	["0.00"],
					            "product_unit_price"	:	["0.00"],
					            "product_category"		:	[product.productcategory],
					            "product_sku"			:	[product.productsku],
					            "product_child_sku"		:	[product.productchildsku], 
					            "product_img_url"		:	[productImageURL],
					            "product_url"			:	[productURL],
					            "product_size"			:	[product.productsize]
							});
						}
						
						index++;
					}
				}
			}
		},
		removeProducttUtagByJson: function (productsJson) {
			if(utag) {		
				var index = 0;
				if(productsJson != null){
					while (productsJson[index]) {
						var product = productsJson[index];
						var productImageURL = product.productexternalid != 0 ? 'https://' + SitePreferences.amplienceHost +'/i/hmk/'+ product.productexternalid+'?h=1220&w=1350' : '';
						var productURL = util.appendParamsToUrl(Urls.getHttpProductUrl, {"pid": product.variantid});
						if(typeof utag != undefined && utag != null ){ 
							utag.link({ 
								"enhanced_actions"		:	"remove", 
								"product_id"			:	[product.productid],
					            "product_name"			:	[product.productname],
					            "product_brand"			:	[product.brand],
					            "product_quantity"		:	[product.quantity.toString()],
					           	"product_price"			:	["0.00"],
					            "product_unit_price"	:	["0.00"],
					            "product_category"		:	[product.productcategory],
					            "product_sku"			:	[product.productsku],
					            "product_child_sku"		:	[product.productchildsku], 
					            "product_img_url"		:	[productImageURL],
					            "product_url"			:	[productURL],
					            "product_size"			:	[product.productsize]
							});
						}
						
						index++;
					}
				}
			}
		},
		AddProductUtage: function(prodDetailContainer, optionalProductContainer) {
			if(typeof utag != undefined && utag != null && utag_data != null ){ 
				var productURL = util.appendParamsToUrl(Urls.getHttpProductUrl, {"pid": utag_data.product_id});
				utag.link({ 
					"enhanced_actions"		:	"add", 
					"product_id"			:	utag_data.product_id,
		            "product_name"			:	prodDetailContainer.get(0).attributes['pr1nm'].value.toString().split(),
		            "product_brand"			:	prodDetailContainer.get(0).attributes['pr1br'].value.toString().split(),
		            "product_quantity"		:	prodDetailContainer.get(0).attributes['pr1qt'].value.toString().split(),
		           	"product_price"			:	prodDetailContainer.get(0).attributes['pr1pr'].value.toString().split(),
		            "product_unit_price"	:	prodDetailContainer.get(0).attributes['pr1pr'].value.toString().split(),
		            "product_category"		:	prodDetailContainer.get(0).attributes['prlcat'].value.toString().split(),
		            "product_sku"			:	prodDetailContainer.get(0).attributes['prlpsku'].value.toString().split(),
		            "product_child_sku"		:	prodDetailContainer.get(0).attributes['pr1va'].value.toString().split(), 
		            "product_img_url"		:	$("img.amp-main-img").length > 0 ? $("img.amp-main-img").attr("src").split(): [""],
		            "product_url"			:	productURL.split(),
		            "product_size"			:	$(".select-item.li-select.selected").length > 0 ? $(".select-item.li-select.selected").data('value').split() : [""],
		            "option_product_name"		: optionalProductContainer.get(0).attributes['pr1nm'].value.toString().split(),
    				"option_product_price"		: optionalProductContainer.get(0).attributes['pr1pr'].value.toString().split(),
    				"option_product_value_id"	: optionalProductContainer.get(0).attributes['pr1id'].value.toString().split()
				});
			}
		}
};

module.exports = tagManager;