'use strict';
/**
@class app.sfgoLocationdialog
* 
* Display the Country selection dialog box
*/

var dialog = require('./dialog'),
util = require('./util');

exports.show = function () {
		
		var obj = $('#sf-geo-loc-header-include-div');
				
		if (obj.length > 0){
			var dheight = 276;
			var dwidth = 425;
			var durl = obj.data('url');
			var cookieExpiration = parseInt(obj.data('cookie-expiration'));
			var siteId = obj.data('site-id');
			var cookieName = siteId + '_geoloc_sub';
	
			if ($.cookie(cookieName) == null) {
					
				$.cookie(cookieName, '1', {expires:cookieExpiration, path:'/'});
				
				// disable email signup dialog
				var emailTrigger = $('.sf-email-dialog-trigger');				
				if(emailTrigger.length > 0){
					
					emailTrigger.unbind('click'); 
				}
				// Open country selection dialog box
				dialog.open({
					url: durl,
					options: {
						dialogClass: 'dialog-switch-site',
						height: dheight,
						width:  dwidth,
						title: "",
						open: function () {
							$(".stay-us-site a").on('click', function(e) {
								e.preventDefault();
								$(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').click();
							});
							$(document).on('click', function(e) {
							    if ( !$(e.target).closest('.dialog-switch-site').length ) {
							    	e.preventDefault(); 
							    	$('.ui-dialog-content').dialog( "close" );
							    }
							});	
						}
					}
				
				});
				
			}
		}		
	};

