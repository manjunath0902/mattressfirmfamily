'use strict';
/**
 * Set Timezone Offset field for each store.
 *
 */

var StoreMgr = require('dw/catalog/StoreMgr');
var SystemObjectMgr = require('dw/object/SystemObjectMgr');
var Transaction = require('dw/system/Transaction');
var Logger = require('dw/system/Logger');
importPackage( dw.io );


function execute(args)
{
	var filename : String = "sitemap_store.xml"; 
	var file : File = new File(File.TEMP + "/" + filename);
	var fw : FileWriter = new FileWriter(file, "UTF-8", false);
	var xsw : XMLStreamWriter = new XMLStreamWriter(fw);
	xsw.writeStartDocument("UTF-8", "1.0");
	xsw.writeCharacters("\n");
	xsw.writeStartElement("urlset");
	xsw.writeAttribute("xmlns","http://www.sitemaps.org/schemas/sitemap/0.9");
	xsw.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
	xsw.writeAttribute("xsi:schemaLocation", "http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd");

	var storesMgrResult = dw.catalog.StoreMgr.searchStoresByPostalCode("US", "77005", "mi", 10000000000);
	if(storesMgrResult != null && storesMgrResult.length > 0){
		var storesSet = storesMgrResult.keySet();
    	var stores = [];
    }
    Logger.info("Total Stores: " + storesSet.length);
    var count = 1; 
	for(var storeindex in storesSet) {
		var store = storesSet[storeindex];
		
		var storeID = store.ID;		
		if(storeID != null) {
			// lat,lng tuple
			var storeURL = 'http://www.tulo.com/on/demandware.store/Sites-Tulo-Site/default/Stores-Details?StoreID='+storeID;
			xsw.writeStartElement("url");
		    xsw.writeCharacters(storeURL);
		    xsw.writeEndElement();
		
		}
	}
	xsw.writeEndElement();  //</Feed>
    xsw.writeEndDocument();
	   
    xsw.flush();
    xsw.close();
}
exports.execute = execute;
