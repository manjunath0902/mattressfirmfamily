importPackage( dw.system );
importPackage( dw.object );

var UtilFunctions = {
		isFrameInCart : function (productLineItems) {
			if(productLineItems == null || productLineItems.length == 0)
			{
				return false;
			}
			var isFrame =  false;
			var hasAllowedLocale = false;
			// loop over allowed locales
			for each (var lineItem in productLineItems) {		
				if(lineItem.getOptionProductLineItems().size() > 0) {
					for each (var optionLI in lineItem.optionProductLineItems){
						if (optionLI.optionID ==  'tuloBase' && optionLI.optionValueID != 'none') {
							isFrame = true;
							break;
						}
					
					}
				}
				if(isFrame == true)
				{
					break;
				}
				
			}		
			
			return isFrame;
		},
		getDateSuffix : function(day) {
			if (day > 3 && day < 21) return 'th'; 
			switch (day % 10) {
				case 1:  return "st";
				break;
	   	 		case 2:  return "nd";
	    		break;
	   			case 3:  return "rd";
	   			break;
	    		default: return "th";
			}	
		}



};

exports.UtilFunctions = UtilFunctions;
