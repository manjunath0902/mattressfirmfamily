'use strict';
importScript( "int_amplience:common/AmplienceSash.ds");
importScript( "int_amplience:common/AmplienceUtils.ds");
var ProductUtils = require('app_storefront_core/cartridge/scripts/product/ProductUtils.js');

function createCanonical(productSearchResultParam) {
    var productSearchModel = productSearchResultParam;
    var selectedRefinementsMapMeta = new dw.util.HashMap();    
    var parentCategoryName = productSearchModel.category.getParent().displayName;
    var categoryName = productSearchModel.category.displayName;
    var canonicalURL = "/" + categoryName + "/";
    if (productSearchModel != null && productSearchModel.refinements != null && productSearchModel.refinements.refinementDefinitions != null) {
        for (var i = 0; i < productSearchModel.refinements.refinementDefinitions.length; i++) {
            var selectedRefinement = productSearchModel.refinements.refinementDefinitions[i];
            if (selectedRefinement.isAttributeRefinement() && productSearchModel.isRefinedByAttribute(selectedRefinement.attributeID)) {
                var selectedRefinementArray = new Array();
                selectedRefinementArray = productSearchModel.refinements.getRefinementValues(selectedRefinement);
                for (var j = 0; j < selectedRefinementArray.length; j++) {
                    var refinementValue = selectedRefinementArray[j];
                    if (productSearchModel.isRefinedByAttributeValue(selectedRefinement.attributeID, refinementValue.value)) {
                        selectedRefinementsMapMeta.put(selectedRefinement.displayName, selectedRefinementArray[j].value);
                    }
                }
            }
        }
    }

    //get keys as per canonical rule
    if (selectedRefinementsMapMeta.containsKey("Brands")) {
        canonicalURL = canonicalURL + selectedRefinementsMapMeta.get("Brands") + "/";
    } else if (parentCategoryName == 'Brand') {
        canonicalURL = canonicalURL + categoryName + "/";
    }

    //for size
    if (selectedRefinementsMapMeta.containsKey("Size")) {
        canonicalURL = canonicalURL + selectedRefinementsMapMeta.get("Size") + "/";
    } else if (parentCategoryName == 'Size') {
        canonicalURL = canonicalURL + categoryName + "/";
    }

    //for type
    if (selectedRefinementsMapMeta.containsKey("Mattress Type")) {
        canonicalURL = canonicalURL + selectedRefinementsMapMeta.get("Mattress Type") + "/";
    } else if (parentCategoryName == 'Type') {
        canonicalURL = canonicalURL + categoryName + "/";
    }

    //for comfort    
    if (selectedRefinementsMapMeta.containsKey("Comfort")) {
        canonicalURL = canonicalURL + selectedRefinementsMapMeta.get("Comfort") + "/";
    } else if (parentCategoryName == 'Comfort') {
        canonicalURL = canonicalURL + categoryName + "/";
    }
    
    var canonicalURLStructure = canonicalURL.replace(/\s+/g, "-").toLowerCase();
    var result = canonicalURLStructure.split("/");
    var singleFinalCanonical = "/"+result[1]+'/'+result[2]+'/';
    var finalSingleCanonical = "";
    if(categoryName == 'Mattresses') {
        if(result.length == 7) {
        	finalSingleCanonical =  "/"+result[1]+'/'+result[2]+'/'+result[3]+'/'+result[4]+'/';
        	return finalSingleCanonical.replace(/\/+/g,"/");
    	}else {        		
    		for(var refinementsValue=0; refinementsValue < result.length; refinementsValue ++) {
    			finalSingleCanonical = finalSingleCanonical+ result[refinementsValue]+"/";
    		}
    		return finalSingleCanonical.replace(/\/+/g,"/");
    	}
    }else if (categoryName == 'Sale') {
    	if(result.length == 7 || result.length == 6) {
        	finalSingleCanonical =  "/"+result[1]+'/'+result[2]+'/'+result[3]+'/';
        	return finalSingleCanonical.replace(/\/+/g,"/");
    	}else {        		
    		for(var refinementsValue=0; refinementsValue < result.length; refinementsValue ++) {
    			finalSingleCanonical = finalSingleCanonical+ result[refinementsValue]+"/";
    		}
    		return finalSingleCanonical.replace(/\/+/g,"/");
    	}
    }else {
    	for(var refinementsValue=0; refinementsValue < result.length; refinementsValue ++) {
			finalSingleCanonical = finalSingleCanonical+ result[refinementsValue]+"/";
		}
		return finalSingleCanonical.replace(/\/+/g,"/");
    }

    return "";
}


function createContentAssetId(productSearchResultParam) {

    var productSearchModel = productSearchResultParam;
    var selectedRefinementsMapMeta = new dw.util.HashMap();
    var categoryName = productSearchModel.category.displayName;
    var contentAssetId = categoryName+"_";
    var parentCategoryName = productSearchModel.category.getParent().displayName;
    
    var refinementBrandArray = new Array();
    var refinementSizeArray = new Array();
    var refinementTypeArray = new Array();
    var refinementComfortArray = new Array();



    if (productSearchModel != null && productSearchModel.refinements != null && productSearchModel.refinements.refinementDefinitions != null) {
        for (var i = 0; i < productSearchModel.refinements.refinementDefinitions.length; i++) {
            var selectedRefinement = productSearchModel.refinements.refinementDefinitions[i];
            if (selectedRefinement.isAttributeRefinement() && productSearchModel.isRefinedByAttribute(selectedRefinement.attributeID)) {
                var selectedRefinementArray = new Array();
                selectedRefinementArray = productSearchModel.refinements.getRefinementValues(selectedRefinement);
                for (var j = 0; j < selectedRefinementArray.length; j++) {
                    var refinementsValues = selectedRefinementArray[j];
                    if (productSearchModel.isRefinedByAttributeValue(selectedRefinement.attributeID, refinementsValues.value)) {
                        var jsonData = {};
                        jsonData.name = selectedRefinement.displayName;
                        jsonData.value = selectedRefinementArray[j].value

                        if (selectedRefinement.displayName == 'Brands') {
                            refinementBrandArray.push(jsonData);
                        }
                        if (selectedRefinement.displayName == 'Size') {
                            refinementSizeArray.push(jsonData);
                        }
                        if (selectedRefinement.displayName == 'Mattress Type') {
                            refinementTypeArray.push(jsonData);
                        }
                        if (selectedRefinement.displayName == 'Comfort') {
                            refinementComfortArray.push(jsonData);
                        }
                    }
                }
            }
        }
    }

    if (refinementBrandArray && refinementBrandArray.length > 0) {
        for (var i = 0; i < refinementBrandArray.length; i++) {
            contentAssetId = contentAssetId + refinementBrandArray[i].value + "_";
        }
    } 

    if (refinementSizeArray && refinementSizeArray.length > 0) {
        for (var i = 0; i < refinementSizeArray.length; i++) {
            contentAssetId = contentAssetId + refinementSizeArray[i].value + "_";
        }
    } 

    if (refinementTypeArray && refinementTypeArray.length > 0) {
        for (var i = 0; i < refinementTypeArray.length; i++) {
            contentAssetId = contentAssetId + refinementTypeArray[i].value + "_";
        }
    } 

    if (refinementComfortArray && refinementComfortArray.length > 0) {
        for (var i = 0; i < refinementComfortArray.length; i++) {
            contentAssetId = contentAssetId + refinementComfortArray[i].value + "_";
        }
    } 


    //remove spaces and replace wih -
    return contentAssetId.substring(0, contentAssetId.length - 1).replace(/\s+/g, "-").replace(/\&+/g, "and").toLowerCase();
    
}

function createMultipleCanonical(productSearchResultParam) {

    var productSearchModel = productSearchResultParam;
    var selectedRefinementsMapMeta = new dw.util.HashMap();
    var contentAssetId = "";
    
    var parentCategoryName = productSearchModel.category.getParent().displayName;
    var categoryName = productSearchModel.category.displayName;
    var multipleCanonicalURL = "/"+categoryName+"/";
    var refinementBrandArray = new Array();
    var refinementSizeArray = new Array();
    var refinementTypeArray = new Array();
    var refinementComfortArray = new Array();



    if (productSearchModel != null && productSearchModel.refinements != null && productSearchModel.refinements.refinementDefinitions != null) {
        for (var i = 0; i < productSearchModel.refinements.refinementDefinitions.length; i++) {
            var selectedRefinement = productSearchModel.refinements.refinementDefinitions[i];
            if (selectedRefinement.isAttributeRefinement() && productSearchModel.isRefinedByAttribute(selectedRefinement.attributeID)) {
                var selectedRefinementArray = new Array();
                selectedRefinementArray = productSearchModel.refinements.getRefinementValues(selectedRefinement);
                for (var j = 0; j < selectedRefinementArray.length; j++) {
                    var refinementsValues = selectedRefinementArray[j];
                    if (productSearchModel.isRefinedByAttributeValue(selectedRefinement.attributeID, refinementsValues.value)) {
                        var jsonData = {};
                        jsonData.name = selectedRefinement.displayName;
                        jsonData.value = selectedRefinementArray[j].value

                        if (selectedRefinement.displayName == 'Brands') {
                            refinementBrandArray.push(jsonData);
                        }
                        if (selectedRefinement.displayName == 'Size') {
                            refinementSizeArray.push(jsonData);
                        }
                        if (selectedRefinement.displayName == 'Mattress Type') {
                            refinementTypeArray.push(jsonData);
                        }
                        if (selectedRefinement.displayName == 'Comfort') {
                            refinementComfortArray.push(jsonData);
                        }
                    }
                }
            }
        }
    }

    var brandValue = "";
    var sizeValue = "";
    var typeValue = "";
    var comfortValue = "";
    if (refinementBrandArray && refinementBrandArray.length == 1) {
    	brandValue = refinementBrandArray[0].value;
    }else {
    	brandValue = "";
    }
    if (refinementSizeArray && refinementSizeArray.length == 1) {
    	sizeValue = refinementSizeArray[0].value;
    }else {
    	sizeValue = "";
    }
    if (refinementTypeArray && refinementTypeArray.length == 1) {
    	typeValue = refinementTypeArray[0].value;
    }else {
    	typeValue = "";
    }
    if (refinementComfortArray && refinementComfortArray.length == 1) {
    	comfortValue = refinementComfortArray[0].value;
    }else {
    	comfortValue = "";
    }
    multipleCanonicalURL = multipleCanonicalURL+"/"+brandValue+"/"+sizeValue+"/"+typeValue+"/"+comfortValue+"/";
    return multipleCanonicalURL.replace(/\s+/g, "-").toLowerCase().replace(/\/+/g,"/");   
}

function createProductDetailPageLDJsonSchema(productInfo) {
	var product = productInfo.object;
	if(product != null)	{
		  /* pdpJsonSchema.aggregateRating = new Object();  
		   pdpJsonSchema.aggregateRating["@type"]="AggregateRating";
		   pdpJsonSchema.aggregateRating.ratingValue = (product.custom.bvAverageRating != null)?product.custom.bvAverageRating:"";
		   pdpJsonSchema.aggregateRating.reviewCount = (product.custom.bvReviewCount != null)?product.custom.bvReviewCount : "";
		   */
		
		if('AmplienceHost' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.AmplienceHost!='') {
			var AmplienceHost = dw.system.Site.current.preferences.custom.AmplienceHost;
		}
		if('AmplienceClientId' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.AmplienceClientId!='') {
			var AmplienceId = dw.system.Site.current.preferences.custom.AmplienceClientId;
		}
		   
		if(product.master){
		   var pdpJsonSchema = [];	   
			   	   
		   if(product.variationModel != null && product.variationModel.variants !=null && product.variationModel.variants.length > 0) {
			  pdpJsonSchema.offers = [];
			  variationCount = product.variationModel.variants.length;
			  for(var item=0; item < variationCount; item++) {
				var variantObject = product.variationModel.variants[item];
			    var variant = new Object();	  
			    variant["@context"] ="https://schema.org/";
		    	variant["@type"]="Product";
		    	variant.sku = variantObject.ID;
		    	if(!empty(AmplienceHost) && !empty(AmplienceId)) {
		    		variant.image = request.httpProtocol+'://'+AmplienceHost+'/i/'+AmplienceId+'/'+product.custom.external_id+'.jpg';
		    	}
		    	variant.name = product.name;
		    	variant.description = product.getShortDescription().toString();
		    	variant.url = dw.web.URLUtils.abs('Product-Show','pid', variantObject.ID).toString();
				variant.brand = new Object(); 
				variant.brand["@type"] = "Thing";
				variant.brand.name = product.brand;
		    	variant.offers = new Object();
		    	variant.offers["@type"]="Offer";
				variant.offers.priceCurrency = session.getCurrency().getCurrencyCode();
				if(variantObject.priceModel.price.value != '0') {
					variant.offers.price = variantObject.priceModel.price.value.toFixed(2);
				} else if (variantObject.priceModel.maxPrice.value != '0') {
					variant.offers.price = variantObject.priceModel.maxPrice.value.toFixed(2);
				} else {
					variant.offers.price = variantObject.priceModel.minPrice.value.toFixed(2);
				}
			    variant.offers.itemCondition="https://schema.org/NewCondition";
				variant.offers.availability="https://schema.org/InStock";
			    variant.offers.seller = new Object();
			    variant.offers.seller.name = "Tulo";
			    variant.offers.seller.type = "Organization";            
			    pdpJsonSchema.push(variant);
			  }
			}
		}else{
		   var pdpJsonSchema = new Object();	   
		   pdpJsonSchema["@context"] ="https://schema.org/";
		   pdpJsonSchema["@type"]="Product";
		   pdpJsonSchema.sku = product.ID;
		   if(!empty(AmplienceHost) && !empty(AmplienceId)) {
			   pdpJsonSchema.image = request.httpProtocol+'://'+AmplienceHost+'/i/'+AmplienceId+'/'+product.custom.external_id+'.jpg';
		   }
		   pdpJsonSchema.name = product.name;
		   pdpJsonSchema.description = product.getShortDescription().toString();
		   pdpJsonSchema.url = dw.web.URLUtils.abs('Product-Show','pid', product.ID).toString();
		   pdpJsonSchema.brand = new Object(); 
		   pdpJsonSchema.brand["@type"] = "Thing";
		   pdpJsonSchema.brand.name = product.brand;
		   pdpJsonSchema.offers = new Object();
		   pdpJsonSchema.offers["@type"]="Offer";
		   pdpJsonSchema.offers.priceCurrency = session.getCurrency().getCurrencyCode();
		   if(product.priceModel.price.value != '0') {
			   pdpJsonSchema.offers.price = product.priceModel.price.value.toFixed(2);
		   } else if (product.priceModel.maxPrice.value != '0') {
			   pdpJsonSchema.offers.price = product.priceModel.maxPrice.value.toFixed(2);
		   } else {
			   pdpJsonSchema.offers.price = product.priceModel.minPrice.value.toFixed(2);
		   }
		   pdpJsonSchema.offers.itemCondition="https://schema.org/NewCondition";
		   pdpJsonSchema.offers.availability="https://schema.org/InStock";
		   pdpJsonSchema.offers.seller = new Object();
		   pdpJsonSchema.offers.seller.name = "Tulo";
		   pdpJsonSchema.offers.seller.type = "Organization";
		}
	}	
	return JSON.stringify(pdpJsonSchema);
}

function createBreadCrumbSchema(breadCrumbArray) {
	var bcJsonSchema = new Object();
	bcJsonSchema["@context"] ="https://schema.org";
	bcJsonSchema["@type"] = "BreadcrumbList";
	bcJsonSchema.itemListElement = new Array();
	
	if(breadCrumbArray != null && breadCrumbArray.length > 0) {
		for(var bd=0; bd < breadCrumbArray.length; bd++) {
			var itemBD = breadCrumbArray[bd];
			
			var element = new Object();
			element["@type"] = "ListItem";
			element.position = bd+1;
			
			element.item = new Object();
			element.item.id = itemBD['id'];
			element.item.name = itemBD.name;
			bcJsonSchema.itemListElement.push(element);
		}	       
	}	
	//return bcJsonSchema.itemListElement[0];
    return bcJsonSchema;
}


exports.CreateCanonical = createCanonical;
exports.CreateContentAssetId = createContentAssetId;
exports.CreateMultipleCanonical = createMultipleCanonical;
exports.CreateProductDetailPageLDJsonSchema = createProductDetailPageLDJsonSchema;
exports.CreateBreadCrumbSchema = createBreadCrumbSchema;
