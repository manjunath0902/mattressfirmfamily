'use strict';
var app = require('app_storefront_controllers/cartridge/scripts/app');
var ProductUtils = require('app_storefront_core/cartridge/scripts/product/ProductUtils');

function getFrameLineItem(cart, mainLineItem)
{
	
	if(mainLineItem.product.masterProduct.ID == dw.system.Site.getCurrent().getCustomPreferenceValue('tuloFrameProductID'))
	{ // this line item  is a frame
		return false;
	}
	
	for each (var pli in cart.getProductLineItems()) {
		if ( pli.UUID != mainLineItem.UUID && pli.product  && pli.product.masterProduct.ID == dw.system.Site.getCurrent().getCustomPreferenceValue('tuloFrameProductID') && mainLineItem.custom && mainLineItem.custom.linkedProductID != null && mainLineItem.custom.linkedProductID ==  pli.product.ID) {
			return pli;
		}
	}
	
	return false;
}
function getFrameToRemove(cart, mainLineItem)
{
	var frameLI = getFrameLineItem(cart, mainLineItem);
	if(frameLI == false)
	{
		return false;
	}
	// check if frame is associated to some other LI, then don;t remove
	for each (var pli in cart.getProductLineItems()) {
		if ( pli.custom && pli.UUID != mainLineItem.UUID && pli.custom.linkedProductID != null && pli.custom.linkedProductID ==  frameLI.product.ID) {
			return false; // same frame is associated to another LI
		}
	}
	
	return frameLI;
}
function frameLIExists(pid)
{
	
	var cart = app.getModel('Cart').get();
	
	if(cart)
	{
		for each (var pli in cart.getProductLineItems()) {
			if ( pli.custom && pli.custom.linkedProductID !=null && pli.custom.linkedProductID == pid  && pli.product.masterProduct.ID == dw.system.Site.getCurrent().getCustomPreferenceValue('tuloFrameProductID')) {
				return true;
			}
		}
	}
	
	
	return false;
}
function clearFrameLinks(cart, frameID)
{
	if(cart)
	{
		for each (var pli in cart.getProductLineItems()) {
			if ( pli.custom && pli.custom.linkedProductID !=null && pli.custom.linkedProductID == frameID ) {
				pli.custom.linkedProductID = null;
			}
		}
	}
	
}
function isFrameInCart(productLineItems) {
	if(productLineItems == null || productLineItems.length == 0)
	{
		return false;
	}
	var isFrame =  false;
	for each (var lineItem in productLineItems) {
		if(lineItem.product.masterProduct.ID == dw.system.Site.getCurrent().getCustomPreferenceValue('tuloFrameProductID'))
		{ // this line item  is a frame
			isFrame =  true;
			break;
		}
	}		
	
	return isFrame;
}
function isPillowInCart(productLineItems) {
	if(productLineItems == null || productLineItems.length == 0)
	{
		return false;
	}
	var result =  false;
	for each (var lineItem in productLineItems) {
		if(ProductUtils.isPillow(lineItem.product.ID))
		{
			result =  true;
			break;
		}
	}		
	
	return result;
}
function allPillowsInCart(productLineItems) {
	if(productLineItems == null || productLineItems.length == 0)
	{
		return false;
	}
	var allPillows =  true;
	for each (var lineItem in productLineItems) {
		if(!ProductUtils.isPillow(lineItem.product.ID))
		{
			allPillows =  false;
			break;
		}
	}		
	
	return allPillows;
}
function isOtherItemInCart(productLineItems) {
	if(productLineItems == null || productLineItems.length == 0)
	{
		return false;
	}
	var isOtherItem =  false;
	for each (var lineItem in productLineItems) {
		if(lineItem.product.masterProduct.ID == dw.system.Site.getCurrent().getCustomPreferenceValue('tuloFrameProductID') || ProductUtils.isPillow(lineItem.product.ID))
		{
			isOtherItem =  true;
			break;
		}
	}		
	
	return isOtherItem;
}

function isTuloComfortMattressInCart(productLineItems) {
	if(productLineItems == null || productLineItems.length == 0)
	{
		return false;
	}
	var isComfortMattress =  false;
	for each (var lineItem in productLineItems) {
		if(ProductUtils.isTuloMattress(lineItem.product.ID))
		{
			isComfortMattress =  true;
			break;
		}
	}
	
	return isComfortMattress;
}

function isFiberSheetInCart(productLineItems) {
	if(productLineItems == null || productLineItems.length == 0)
	{
		return false;
	}
	var isFiberSheet =  false;
	for each (var lineItem in productLineItems) {
		if(ProductUtils.isFiberSheet(lineItem.product.ID))
		{
			isFiberSheet =  true;
			break;
		}
	}
	
	return isFiberSheet;
}

function isFrame(masterId) {
    if(masterId == dw.system.Site.getCurrent().getCustomPreferenceValue('tuloFrameProductID'))
    { // this item  is a frame
        return true;
    }
    return false;
}
exports.isFrame = isFrame;
exports.GetFrameLineItem = getFrameLineItem;
exports.FrameLIExists = frameLIExists;
exports.GetFrameToRemove = getFrameToRemove;
exports.ClearFrameLinks = clearFrameLinks;
exports.isFrameInCart = isFrameInCart;
exports.allPillowsInCart = allPillowsInCart;
exports.isPillowInCart = isPillowInCart;
exports.isOtherItemInCart = isOtherItemInCart;
exports.isTuloComfortMattressInCart = isTuloComfortMattressInCart;
exports.isFiberSheetInCart = isFiberSheetInCart;