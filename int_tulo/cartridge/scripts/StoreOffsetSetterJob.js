'use strict';
/**
 * Set Timezone Offset field for each store.
 *
 */

var StoreMgr = require('dw/catalog/StoreMgr');
var SystemObjectMgr = require('dw/object/SystemObjectMgr');
var Transaction = require('dw/system/Transaction');
var Logger = require('dw/system/Logger');


function execute(args)
{

	var storesMgrResult = StoreMgr.searchStoresByPostalCode("US", "77005", "mi", 10000000000000000000000000000);
	if(storesMgrResult != null && storesMgrResult.length > 0){
		var storesSet = storesMgrResult.keySet();
    	var stores = [];
    }
    Logger.info("Total Stores: " + storesSet.length);
    var count = 1; 
	for(var storeindex in storesSet) {
		var store = storesSet[storeindex];
		
		var storeID = store.ID;
		var lat = store.latitude;
		var lng = store.longitude;
		var tzo = store.custom.timezoneOffset;
		if(storeID != null && lat != null && lng != null && tzo == null) {
			// lat,lng tuple
			var loc = lat + ',' + lng;
			// current date/time of user computer
			var targetDate = new Date();
			// current UTC date/time expressed as seconds since midnight, January 1, 1970 UTC
			var timestamp = targetDate.getTime()/1000 + targetDate.getTimezoneOffset() * 60;
			var apikey = dw.system.Site.current.preferences.custom.GOOGLEMAPKEY;
			var apicall = 'https://maps.googleapis.com/maps/api/timezone/json?location=' + loc + '&timestamp=' + timestamp + '&key=' + apikey;
			
			// create new HTTPClient object
			var httpClient : dw.net.HTTPClient = new dw.net.HTTPClient();
			var message : String;
			// open GET request
			httpClient.open('GET', apicall);
			httpClient.send();
			// if request successful
			if (httpClient.statusCode == 200) {
			     message = httpClient.text;
			     // convert returned JSON string to JSON object
			     var output = JSON.parse(message);
			     // if API reports everything was returned successfully
			     if (output.status == 'OK') {
			    	 // get DST and time zone offsets in milliseconds
			    	 var offsets = output.dstOffset * 1000 + output.rawOffset * 1000;
			    	 Transaction.wrap(function () {
			    		 try{
			    			 store.custom.timezoneOffset = offsets;
			    			 Logger.info(count + ". ID: " + storeID + " Offset: " + offsets);
			    			 count++;
			    		 } catch (e) {
			    			 var err = e.message;
			    			 Logger.error(err);
			    		 }
			    	});
			    	 
			     }
			}
			else {
			    // error handling
			    message = "An error occurred with status code " + httpClient.statusCode;
			}
		}
	}
}
exports.execute = execute;
