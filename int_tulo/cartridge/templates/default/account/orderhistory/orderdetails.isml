<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isdecorate template="account/orderhistory/pt_orderhistory">
	<isinclude template="util/modules"/>
	<isset name="Order" value="${pdict.Order}" scope="page"/>
	<isset name="bctext2" value="${Resource.msg('account.orders.header','account',null)}" scope="pdict"/>
	<isset name="bcurl2" value="${URLUtils.https('Order-History')}" scope="pdict"/>
	<isset name="bctext3" value="${Resource.msgf('account.orders.orderdetail.header','account',null, Order.orderNo)}" scope="pdict"/>
	<isscript> 
		var UtilFunctions = require('~/cartridge/scripts/util/UtilFunctions');
        var day = new Date(Order.creationDate).getDate();
        var orderDateSuffix = UtilFunctions.UtilFunctions.getDateSuffix(day);
	</isscript>
	<div id="primary" class="primary-content">
		<isif condition="${!empty(pdict.Order)}">
		<div class="order-detail-holder">
			<div class="orderdetails">
			    <div class="order-information">
			    	<h2>${Resource.msg('account.orders.orderdate','account',null)} 
						<span class="value"><isprint value="${Order.creationDate}" formatter="MMMM d" /><isprint value="${orderDateSuffix}" /><isprint value="${Order.creationDate}" formatter=", yyyy" /></span>
					</h2>
			        <div class="orderstatusl">
			        	<isinclude template="account/orderhistory/orderstatusinclude"/>
			        </div>
			        <div class="ordertotal">
						<span class="label">${Resource.msg('global.ordertotal','locale',null)}</span>											
						<span class="value"><isprint value="${Order.totalGrossPrice}"/></span>
					</div>
					<div class="ordernumber">
				        <span class="label">${Resource.msg('order.orderdetails.ordernumber','order',null)}</span>
				        <span class="value"><isprint value="${Order.orderNo}"/></span>
			        </div>
			    </div>
				<div class="gethelp">
					<a href="${URLUtils.url('Page-Show','cid','help')}" title="Get help with this order" >${Resource.msg('account.orders.gethelp','account',null)} <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
				</div>
			</div>
			<div class="shipment-summry-payment-delievry">
				<div class="col">
					<div class="shipment"> 
					<isloop items="${Order.shipments}" var="shipment" status="shipmentloopstate" end="0">
	                	<isif condition="${shipment.shippingAddress != null}"> 
	                	   <isset name="shippingMethodID" value="${shipment.shippingMethodID}" scope="page" />
	                	    <isif condition="${shippingMethodID == 'storePickup'}">
	                	    <isset name="StoreId" value="${shipment.custom.storeId}" scope="page"/>
								<isscript>
									var storeLocator = require('~/cartridge/controllers/Stores');
									var store;
									if(typeof StoreId != 'undefined' && !empty(StoreId) && StoreId > 0){																					
											store = storeLocator.FetchStoreDetailsForOrder(StoreId);//get store and parse hours...																																							
									}
								</isscript>			                     
						       <h3>${Resource.msg('order.orderdetail.instorepickup','order',null)}</h3>
						       <div class="details">
									<p>${store.name}</p>
			                        <p>${store.address1} <isif condition="${store.address2 != null}">${store.address2}</isif></p>
			                        <p>${store.city}, ${store.stateCode} ${store.postalCode}</p>	                           
			                   </div>
							<iselse/>
								 <h3>${Resource.msg('order.orderdetails.shippingaddress','order',null)}</h3>
								 <div class="details">
									<isset name="address" value="${shipment.shippingAddress}" scope="page" />								
									<isminiaddress p_address="${address}"/>	                           
			                     </div>
							</isif>	 
		                    
	                    </isif>
	                </isloop>
					</div>
					<div class="deliveryinfo">
						
				        <isloop items="${Order.shipments}" var="shipment" status="shipmentloopstate" end="0">
			        		<isif condition="${shipment.custom.deliveryDate != null && shipment.custom.deliveryDate.indexOf('2049-12') == -1}">
			        			<isscript> 	
							        var day = new Date(shipment.custom.deliveryDate).getDate();
							        var shippingDateSuffix = UtilFunctions.UtilFunctions.getDateSuffix(day);
								</isscript>	
					        	<isif condition="${shipment.shippingMethodID == 'storePickup'}">			                     
							       <h3>${Resource.msg('order.orderdetail.instorepickupdate','order',null)}</h3>
								<iselse/>
									 <h3>${Resource.msg('order.orderdetail.deliveredon','order',null)}</h3>
								</isif>	
					       		<div><isprint value="${new Date(shipment.custom.deliveryDate)}"  formatter="MMMM d"/><isprint value="${shippingDateSuffix}" /><isprint value="${new Date(shipment.custom.deliveryDate)}" formatter=", yyyy" /></div>
					       	</isif>	 
				        </isloop>                  
					</div>
				</div>
				<div class="col">
				<div class="paymentintrument">
					<h3>Payment method</h3>
					<isloop items="${Order.getPaymentInstruments()}" var="paymentInstr">
		                <div class="details">
		                    <isif condition="${dw.order.PaymentInstrument.METHOD_GIFT_CERTIFICATE.equals(paymentInstr.paymentMethod)}">
		                        <div class="orderpaymentinstrumentsgc">
		                            <span class="label"><isprint value="${dw.order.PaymentMgr.getPaymentMethod(paymentInstr.paymentMethod).name}" /></span>
		                            <span class="value"><isprint value="${paymentInstr.maskedGiftCertificateCode}"/></span>
		                            <span class="payment-amount">
		                                <span class="label">${Resource.msg('global.amount','locale',null)}:</span>
		                                <span class="value"><isprint value="${paymentInstr.paymentTransaction.amount}"/></span>
		                            </span>
		                        </div>
		                    <iselseif condition="${dw.order.PaymentInstrument.METHOD_CREDIT_CARD.equals(paymentInstr.paymentMethod)}" >
		                    	<isminicreditcard card="${paymentInstr}" show_expiration="${true}"/>
		                    <iselse/>			                        
		                        <isprint value="${dw.order.PaymentMgr.getPaymentMethod(paymentInstr.paymentMethod).name}" />		                        
		                    </isif>
		                </div>
		            </isloop>				
				</div>
				<div class="order-totals">
					<isif condition="${shippingMethodID == 'storePickup'}">
						<isset name="shippinglabel" value="${Resource.msg('order.orderdetail.ordershipping.pickup','order',null)}" scope="page"/>
						<iselse/>
						<isset name="shippinglabel" value="${Resource.msg('order.orderdetail.ordershipping.delivery','order',null)}" scope="page"/>
					</isif>
			       <isordertotals p_lineitemctnr="${Order}" p_totallabel="${Resource.msg('order.orderdetail.total','order',null)}" p_taxlabel="${Resource.msg('order.orderdetail.ordertax','order',null)}" p_shippinglabel="${shippinglabel}" p_subtotallabel="${Resource.msg('order.orderdetail.subtotal','order',null)}"/>
			    </div>
			    </div>
			</div>
		    <div class="order-shipments">		        
		            <isif condition="${Order.productLineItems.size() > 0}">
		                <div class="summary-table-wrapper">
		                	<h2>${Resource.msg('order.orderdetail.itemsinorder','order',null)}</h2>
		                    <table class="item-list" id="cart-table" cellspacing="0">
					            <isif condition="${Order.productLineItems.size() > 0}">
					                <isloop items="${Order.productLineItems}" var="productLineItem" status="pliloopstate">
					                    <tr class="cart-row">
					                        <td class="item-image">
					                            <isif condition="${'AmplienceHost' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.AmplienceHost!=''}">
					                                <isset name="AmplienceHost" value="${dw.system.Site.current.preferences.custom.AmplienceHost}" scope="page" />
					                            </isif>
					                            <isif condition="${'AmplienceClientId' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.AmplienceClientId!=''}">
					                                <isset name="AmplienceId" value="${dw.system.Site.current.preferences.custom.AmplienceClientId}" scope="page" />
					                            </isif>
					                            <isset name="imgUrl" value="${pdict.CurrentRequest.httpProtocol+'://'+AmplienceHost+'/i/'+AmplienceId+'/'+ productLineItem.product.custom.external_id}" scope="page"/>
					                            <isif condition="${imgUrl}">
					                                <img src="${imgUrl}" alt="${productLineItem.productName}" title="${productLineItem.productName}"/>
					                                <iselse/>
					                                <img src="${URLUtils.staticURL('/images/noimagesmall.png')}" alt="${productLineItem.productName}" title="${productLineItem.productName}"/>
					                            </isif>
					                        </td>
					                        <td class="item-details">
					                            <iscomment>Display product line and product using module</iscomment>
					                            <isdisplayliproduct p_productli="${productLineItem}" p_editable="${false}"/>							
					                             <span>${Resource.msg('global.qty','locale',null)}: <isprint value="${productLineItem.quantity}" /></span>
					                         </td>
					                            <td class="item-price">
					                                <p class="mobile-show block-price-text">${Resource.msg('global.price','locale',null)}:</p>					                                
					                                <isset name="Quantity" value="${productLineItem.quantity}" scope="page" />	                                	
				                                	<isif condition="${productLineItem.bonusProductLineItem}">													
														<isset name="productTotal" value="${productLineItem.getAdjustedPrice()}" scope="page" />												
													<iselse>												
													 	<isif condition="${productLineItem.priceAdjustments.length > 0}" >
													 	<isset name="productTotal" value="${productLineItem.adjustedPrice}" scope="page" />
													 	<iselse>
													 	<isset name="productTotal" value="${productLineItem.netPrice}" scope="page" />												 													
													 	</isif>
													</isif>			                                	
				                                	<isif condition="${Quantity == '1'}">
				                                		<span class="price-sales"><isprint value="${productTotal}" /></span>
				                                	<iselse/>
					                                	<span class="price-sales">						                                	
						                                	$<isprint value="${productTotal / Quantity}" /> X <isprint value="${Quantity}" /> = <isprint value="${productTotal}" />						                                	
					                                	</span>
				                                	</isif>	
				                                	<isif condition="${productLineItem.product.masterProduct.ID != dw.system.Site.getCurrent().getCustomPreferenceValue('tuloFrameProductID')}">
														<a href="${URLUtils.http('Product-Show','pid', productLineItem.productID)}#section-5" class="btn-default white" >${Resource.msg('account.orders.writereview','account',null)}</a>
													</isif>	
					                          </td>                                                 
					                    </tr>
					                     <iscomment>Display option lineitem for frame</iscomment>
			                           	 <isif condition="${productLineItem.optionProductLineItems.size() > 0}">
											<isloop items="${productLineItem.optionProductLineItems}" var="optionLI" status="loopstate">
												<isif condition="${optionLI.optionID ==  'tuloBase' && optionLI.optionValueID != 'none'}">
													<isscript>
														var LIOptionModel =  productLineItem.optionModel;
														var optionSize = "";
														var optionName = "";
							             				if(LIOptionModel != null)
							             				{
							             					var liBaseOption = LIOptionModel.getOption('tuloBase');
							             					if(liBaseOption != null && !empty(liBaseOption))
							             					{
							             						optionName = liBaseOption.displayName.toLowerCase() ;
								             					selectedOV = LIOptionModel.getSelectedOptionValue(liBaseOption);
								             					if(selectedOV != null && !empty(selectedOV))
								             					{
								             						optionSize = selectedOV.displayValue.toLowerCase();
								             						optionSize = optionSize.charAt(0).toUpperCase() +optionSize.slice(1);
								             					}
								             					
								             				}	
							             				}
						             				</isscript>  
						             				 <tr class="cart-row">
								                        <td class="item-image">
								                           <img src="${URLUtils.staticURL('/images/pdp_frame_img02.jpg')}" alt="${optionLI.lineItemText}" title="${optionLI.lineItemText}"/>
								                        </td>
								                        <td class="item-details">								                          
								                            <div class="product-list-item">
			                             						<div class="name">${optionName}</div>
			                             						<div class="attribute">
																	<span class="label">Size:</span>
																	<span class="value">															
																		${optionSize}
																	</span>
																</div>
			                             					</div>							
								                             <span>${Resource.msg('global.qty','locale',null)}: <isprint value="${optionLI.quantity}" /></span>
								                         </td>
								                            <td class="item-price">
								                               <p class="mobile-show block-price-text">${Resource.msg('global.price','locale',null)}:</p>					                                
								                                   <isset name="Quantity" value="${optionLI.quantity}" scope="page" />	                                	
								                                												
																	 	<isif condition="${optionLI.priceAdjustments.length > 0}" >
																	 	<isset name="productTotal" value="${optionLI.adjustedPrice}" scope="page" />
																	 	<iselse>
																	 	<isset name="productTotal" value="${optionLI.netPrice}" scope="page" />												 													
																	 	</isif>
																			                                	
								                                	<isif condition="${Quantity == '1'}">
								                                		<span class="price-sales"><isprint value="${productTotal}" /></span>
								                                	<iselse/>
									                                	<span class="price-sales">						                                	
										                                	$<isprint value="${productTotal / Quantity}" /> X <isprint value="${Quantity}" /> = <isprint value="${productTotal}" />						                                	
									                                	</span>
								                                	</isif>	
								                             
								                          </td>                                                 
								                    </tr>
						             			</isif> 
						             		</isloop>  
						             	</isif>                      
					                </isloop>
					            </isif>	
		                </table>
		            </div>
		         </isif>		        
		    </div>		
			<iselse/>		
				<div class="not-found">
					${Resource.msg('account.orders.notfound','account',null)}
				</div>
			</isif>
		</div>
	</div>
</isdecorate>