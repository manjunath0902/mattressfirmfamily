<iscontent type="text/html" charset="UTF-8" compact="true"/>

<iscomment>
    Displays order details, such as order number, creation date, payment information,
    order totals and information for each contained shipment.
    This template module can be used in order confirmation pages as well as in the
    order history to render the details of a given order. Depending on the context
    being used in, one might omit rendering certain information.

    Parameters:

    order		: the order whose details to render
    orderstatus	: if set to true, the order status will be rendered
                  if set to false or not existing, the order status will not be rendered
    orderhistory   : if set to true, this is order history page.
</iscomment>
<isscript>
    var MattressViewHelper = require("int_mattressc/cartridge/scripts/mattress/util/MattressViewHelper.ds");
</isscript>

<isset name="Order" value="${pdict.order}" scope="page"/>
<isset name="orderhistory" value="${pdict.orderhistory}" scope="page"/>
<div class="summary-header">
    <h2>Your order has been placed</h2>
    <h5>Your best sleep is on the way.</h5>
</div>
<div class="orderdetails">
    <div class="order-information">
        <iscomment>Order Status</iscomment>
        <isif condition="${!empty(pdict.orderstatus) && pdict.orderstatus == 'true'}">
            <isinclude template="account/orderhistory/orderstatusinclude"/>
        </isif>
        <isif condition="${!empty(pdict.orderhistory) && pdict.orderhistory == 'true'}">
        <iselse/>
            <div class="print-link-wrapper">
                <a href="#" onclick="window.print();return false;" class="btn-primary white">${Resource.msg('order.orderdetails.printreceipt','order',null)}</a>
            </div>
        </isif>

    </div>
    <div class="right-float">
        <div class="order-payment-instruments mini-payment-instrument">
            <h3 class="section-header">
                <isif condition="${Order.paymentInstruments.length == 1}">
                    ${Resource.msg('order.orderdetails.paymentmethod','order',null)}
                <iselse/>
                    ${Resource.msg('order.orderdetails.paymentmethods','order',null)}
                </isif>
            </h3>
            <div class="order-date">
                <span class="label">${Resource.msg('order.orderdetails.orderplaced','order',null)}</span>
                <span class="value"><isprint value="${Order.creationDate}" style="DATE_LONG"/></span>
            </div>
            <div class="order-number">
                <span class="label">${Resource.msg('order.orderdetails.ordernumber','order',null)}</span>
                <span class="value"><isprint value="${Order.orderNo}"/></span>
            </div>
            <isloop items="${Order.getPaymentInstruments()}" var="paymentInstr">
                <div class="details">
                    <isif condition="${dw.order.PaymentInstrument.METHOD_GIFT_CERTIFICATE.equals(paymentInstr.paymentMethod)}">
                        <div class="orderpaymentinstrumentsgc">
                            <span class="label"><isprint value="${dw.order.PaymentMgr.getPaymentMethod(paymentInstr.paymentMethod).name}" /></span>
                            <span class="value"><isprint value="${paymentInstr.maskedGiftCertificateCode}"/></span>
                            <span class="payment-amount">
                                <span class="label">${Resource.msg('global.amount','locale',null)}:</span>
                                <span class="value"><isprint value="${paymentInstr.paymentTransaction.amount}"/></span>
                            </span>
                        </div>
                    <iselse/>
                        <div class="payment-type"><isprint value="${dw.order.PaymentMgr.getPaymentMethod(paymentInstr.paymentMethod).name}" /></div>
                        <isminicreditcard card="${paymentInstr}" show_expiration="${false}"/>
                        <div class="payment-amount">
                            <span class="label">${Resource.msg('global.amount','locale',null)}:</span>
                            <span class="value"><isprint value="${paymentInstr.paymentTransaction.amount}"/></span>
                        </div><!-- END: payment-amount -->
                    </isif>
                </div>
            </isloop>
        </div>
        <isset name="IsStorePickup" value="${session.custom.deliveryOptionChoice == 'instorepickup'}" scope="page"/>
        <isif condition="${IsStorePickup}">
            <isscript>
                var StoreMgr = require('dw/catalog/StoreMgr');
                var store = StoreMgr.getStore(Order.getDefaultShipment().custom.storeId);
            </isscript>
            <div class="minisummary-place-order clearfix">
                <h3 class="section-header">${Resource.msg('order.orderdetails.pickuplocation','order',null)}</h3>
                <isstorepickup_address p_store="${store}"/>
            </div>
        </isif>
        <div class="minisummary-place-order clearfix">
            <div class="order-billing mini-billing-address">
                <h3 class="section-header">${Resource.msg('order.orderdetails.billingaddress','order',null)}</h3>
                <div class="details">
                    <isminiaddress p_address="${Order.billingAddress}"/>
                </div>
            </div>
        </div>
        <isif condition="${!IsStorePickup}"/>
            <div class="minisummary-place-order clearfix">
                <div class="order-billing mini-billing-address">
                    <h3 class="section-header">${Resource.msg('order.orderdetails.shippingaddress','order',null)}</h3>
                    <div class="details">
                        <isloop items="${Order.shipments}" var="shipment" status="shipmentloopstate" end="0">
                            <isif condition="${shipment.shippingAddress != null}"/>
                                <div class="address-details">
                                    <isset name="address" value="${shipment.shippingAddress}" scope="page" />
                                    <div><isprint value="${address.firstName}"/> <isprint value="${address.lastName}"/></div>
                                    <div><isprint value="${address.address1}"/></div>
                                    <isif condition="${!empty(address.address2)}">
                                        <div><isprint value="${address.address2}"/></div>
                                    </isif>
                                    <div><isprint value="${address.city}"/>, <isprint value="${address.stateCode}"/> <isprint value="${address.postalCode}"/></div>
                                    <div><isprint value="${Resource.msg("country."+address.countryCode, "forms", null)}"/></div>
                                    <isif condition="${shipment.custom.shipmentType == 'instore'}">
                                        <div><isprint value="${address.phone}"/></div>
                                    </isif>
                                </div>
                            </isif>
                        </isloop>
                    </div>
                </div>
            </div>
        </isif>
    </div>
    <iscomment>render a box for each shipment</iscomment>
    <div class="order-shipments">
        <isloop items="${Order.shipments}" var="shipment" status="shipmentloopstate">
            <isif condition="${shipment.productLineItems.size() > 0}">
                <div class="summary-table-wrapper">
                    <table class="item-list" id="cart-table" cellspacing="0">
                        <thead>
                            <tr>
                                <th class="section-header"  colspan="2">${Resource.msg('global.product','locale',null)}</th>
                                <isif condition="${!empty(pdict.orderhistory) && pdict.orderhistory == 'true'}">
                                    <th class="section-header">${Resource.msg('global.qty','locale',null)}</th>
                                    <th class="section-header">${Resource.msg('global.price','locale',null)}</th>
                                <iselse/>
                                    <th class="section-header">${Resource.msg('global.price','locale',null)}</th>
                                    <th class="section-header">${Resource.msg('global.qty','locale',null)}</th>
                                </isif>
                                <isif condition="${!empty(pdict.orderhistory) && pdict.orderhistory == 'true'}">
                                <iselse/>
                                    <th class="section-header header-total-price"> ${Resource.msg('global.totalprice','locale',null)}</th>
                                </isif>
                            </tr>
                        </thead>
            </isif>

            <isif condition="${shipment.productLineItems.size() > 0}">
                <isloop items="${shipment.productLineItems}" var="productLineItem" status="pliloopstate">
                    <tr class="cart-row">
                        <td class="item-image">
                            <isif condition="${'AmplienceHost' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.AmplienceHost!=''}">
                                <isset name="AmplienceHost" value="${dw.system.Site.current.preferences.custom.AmplienceHost}" scope="page" />
                            </isif>
                            <isif condition="${'AmplienceClientId' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.AmplienceClientId!=''}">
                                <isset name="AmplienceId" value="${dw.system.Site.current.preferences.custom.AmplienceClientId}" scope="page" />
                            </isif>
                            <isset name="imgUrl" value="${pdict.CurrentRequest.httpProtocol+'://'+AmplienceHost+'/i/'+AmplienceId+'/'+ productLineItem.product.custom.external_id}" scope="page"/>
                            <isif condition="${imgUrl}">
                                <img src="${imgUrl}" alt="${productLineItem.product.productName}" title="${productLineItem.product.productName}"/>
                                <iselse/>
                                <img src="${URLUtils.staticURL('/images/noimagesmall.png')}" alt="${productLineItem.productName}" title="${productLineItem.productName}"/>
                            </isif>
                        </td>
                        <td class="item-details">
                            <iscomment>Display product line and product using module</iscomment>
                            <isdisplayliproduct p_productli="${productLineItem}" p_editable="${false}"/>

                         </td>
                         <isif condition="${!empty(pdict.orderhistory) && pdict.orderhistory == 'true'}">
                            <td class="item-quantity">
                                <p class="mobile-show block-qty-text">${Resource.msg('global.qty','locale',null)}:</p>
                                <isprint value="${productLineItem.quantity}" />
                            </td>
                            <td class="item-price">
                                <p class="mobile-show block-total-price-text">${Resource.msg('global.totalprice','locale',null)}:</p>
                                <isif condition="${productLineItem.bonusProductLineItem}">
                                    <div class="bonus-item">
                                        <isprint value="${bonusProductPriceValue}" />
                                    </div>
                                <iselse/>
                                    <iscomment>Otherwise, render price using call to adjusted price </iscomment>
                                    <isprint value="${MattressViewHelper.subtractLIRecycleFees(productLineItem, productLineItem.adjustedPrice, false)}" />
                                </isif>
                                <isif condition="${productLineItem.optionProductLineItems.size() > 0}">
                                    <isloop items="${productLineItem.optionProductLineItems}" var="optionLI">
                                        <isif condition="${optionLI.price > 0}">
                                            <p>+ <isprint value="${MattressViewHelper.subtractLIRecycleFees(optionLI, optionLI.adjustedPrice)}"/></p>
                                        </isif>
                                    </isloop>
                                </isif>
                            </td>
                        <iselse/>
                            <td class="item-price">
                                <p class="mobile-show block-price-text">${Resource.msg('global.price','locale',null)}:</p>
                                <isif condition="${productLineItem.product != null}">

                                    <iscomment>
                                        StandardPrice: quantity-one unit price from the configured list price
                                        book. SalesPrice: product line item base price. If these are
                                        different, then we display crossed-out StandardPrice and also
                                        SalesPrice.
                                    </iscomment>


                                    <iscomment>Get the price model for this	product.</iscomment>
                                    <isset name="PriceModel" value="${productLineItem.product.getPriceModel()}" scope="page" />


                                    <iscomment>Get StandardPrice from list price book.</iscomment>
                                    <isinclude template="product/components/standardprice" />

                                    <iscomment>Get SalesPrice from line item itself. MattressViewHelper.subtractLIRecycleFees(lineItem, </iscomment>
                                    <isset name="SalesPrice" value="${productLineItem.basePrice}" scope="page" />
                                    <isset name="liAdjustments" value="${productLineItem.priceAdjustments.length}" scope="page" />


                                    <isif condition="${StandardPrice.available && StandardPrice > SalesPrice}">
                                        <iscomment>StandardPrice and SalesPrice are different, show standard</iscomment>
                                        <div class="price-promotion">
                                            <span class="price-sales"><isprint value="${SalesPrice}" /></span>
                                            <span class="price-standard"><isprint value="${StandardPrice}" /></span>
                                        </div>
                                    <iselse/>
                                        <iscomment>
                                            <isif condition="${MattressViewHelper.getNumNonRecycleAdjustments(productLineItem) == 0}">
                                                <span class="price-sales"><isprint value="${SalesPrice}" /></span>
                                            </isif>
                                        </iscomment>
                                        <span class="price-sales"><isprint value="${SalesPrice}" /></span>
                                    </isif>


                                </isif>

                            </td>
                            <td class="item-quantity">
                                <p class="mobile-show block-qty-text">${Resource.msg('global.qty','locale',null)}:</p>
                                <isprint value="${productLineItem.quantity}" />
                            </td>
                        </isif>
                        <isif condition="${!empty(pdict.orderhistory) && pdict.orderhistory == 'true'}">
                        <iselse/>
                            <td class="item-total">
                                <p class="mobile-show block-total-price-text">${Resource.msg('global.totalprice','locale',null)}:</p>
                                <isif condition="${productLineItem.bonusProductLineItem}">
                                    <div class="bonus-item">
                                        <isprint value="${bonusProductPriceValue}" />
                                    </div>
                                <iselse/>
                                    <iscomment>Otherwise, render price using call to adjusted price </iscomment>
                                    <isprint value="${MattressViewHelper.subtractLIRecycleFees(productLineItem, productLineItem.adjustedPrice, false)}" />
                                </isif>
                                <isif condition="${productLineItem.optionProductLineItems.size() > 0}">
                                    <isloop items="${productLineItem.optionProductLineItems}" var="optionLI">
                                        <isif condition="${optionLI.price > 0}">
                                            <p>+ <isprint value="${MattressViewHelper.subtractLIRecycleFees(optionLI, optionLI.adjustedPrice)}"/></p>
                                        </isif>
                                    </isloop>
                                </isif>
                            </td>
                        </isif>
                    </tr>
                            <isif condition="${MattressViewHelper.hasRecycleFees(productLineItem)}">

                            <isset name="shippingState" value="${shipment.shippingAddress != null ? shipment.shippingAddress.stateCode.toUpperCase() : ""}" scope="page" >
                            <tr class="recycle-row">
                                <td></td>
                                <td class="item-details">${Resource.msgf('global.recycling-fee','locale', '', shippingState)}</td>
                                <td></td>
                                <isif condition="${!empty(pdict.orderhistory) && pdict.orderhistory == 'true'}">
                                <iselse><td></td>
                                </isif>
                                <td class="item-price"><isprint value="${MattressViewHelper.getTotalRecycleFees(productLineItem)}"></td>
                            </tr>
                        </isif>
                </isloop>
            </isif>


            <isif condition="${shipment.giftCertificateLineItems.size() > 0}">
                <div class="order-shipment-table">
                    <div class="order-shipment-details">
                        <iscomment>Shipment Gift Certificate</iscomment>
                        <isloop items="${shipment.giftCertificateLineItems}" var="giftCertificateLineItem" status="gcliloopstate">
                            <tr>
                            <div class="order-gift-cert-attributes order-shipment-address">
                                <div class="order-gift-cert-to">
                                    <div class="label">${Resource.msg('order.orderdetails.giftcertto','order',null)}</div>
                                    <div class="value"><isprint value="${giftCertificateLineItem.recipientName}"/></div>
                                    <div class="value"><isprint value="${giftCertificateLineItem.recipientEmail}"/></div>
                                </div>
                                <div class="order-gift-cert-from">
                                    <div class="label">${Resource.msg('order.orderdetails.giftcertfrom','order',null)}</div>
                                    <div class="value"><isprint value="${giftCertificateLineItem.senderName}"/></div>
                                    <div class="value"><isprint value="${Order.customerEmail}"/></div>
                                </div>
                            </div>
                            <div class="order-gift-cert-amount">
                                <div class="label">${Resource.msg('global.giftcertificate','locale',null)}</div>
                                <div class="value"><isprint value="${giftCertificateLineItem.price}"/></div>
                            </div>
                            <div class="shipping-method">
                                <div class="label">${Resource.msg('order.orderdetails.shippingmethod','order',null)}</div>
                                <div class="value">${Resource.msg('order.orderdetails.giftcertshipping','order',null)}</div>
                                <div class="value"><isprint value="${giftCertificateLineItem.recipientEmail}"/></div>
                            </div>
                            </tr>
                        </isloop>

                        <iscomment>if shipment is marked as gift</iscomment>
                        <isif condition="${shipment.gift}">
                            <div class="order-shipment-gift-message">
                                <div class="label">${Resource.msg('order.orderdetails.giftmessage','order',null)}</div>
                                <isif condition="${!empty(shipment.giftMessage)}">
                                    <div class="value"><isprint value="${shipment.giftMessage}"/></div>
                                </isif>
                            </div>
                        <iselse/>
                            <div class="order-shipment-gift-message">
                                <isset name="theGiftCert" value="${shipment.giftCertificateLineItems.iterator().next()}" scope="page"/>
                                <div class="label">${Resource.msg('order.orderdetails.giftmessage','order',null)}</div>
                                <isif condition="${!empty(theGiftCert.message)}">
                                    <div class="value"><isprint value="${theGiftCert.message}"/></div>
                                </isif>
                            </div>
                        </isif>
                    </div>
                </div>
            </isif>

            <isif condition="${shipment.productLineItems.size() > 0 || shipment.giftCertificateLineItems.size() > 0}">
                </table>
            </div>
            </isif>
        </isloop>
    </div>
</div>
