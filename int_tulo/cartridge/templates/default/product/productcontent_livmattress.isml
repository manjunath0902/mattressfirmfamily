<isinclude template="util/modules" />
<isset name="currentSite" value="${dw.system.Site.getCurrent().getID().toLowerCase()}" scope="page" />
<isset name="generalProductID" value="${dw.system.Site.getCurrent().getCustomPreferenceValue('tuloGeneralProductID')}" scope="page" />

<isscript>
	var ProductUtils = require('app_storefront_core/cartridge/scripts/product/ProductUtils.js');
	var masterId = pdict.Product.isVariant() ? pdict.Product.masterProduct.ID : pdict.Product.ID;
	var tuloComfortName =  ProductUtils.getProductComfortName(masterId);
</isscript>
<isif condition="${pdict.resetAttributes}">
	<isscript>		
		var url = dw.web.URLUtils.url('Product-Variation', 'pid', pdict.Product.ID, 'format', 'ajax');
		var qs = ProductUtils.getQueryString(pdict.CurrentHttpParameterMap, ["source", "uuid", "Quantity"]);
		if (qs && qs.length>0) { url+="&"+qs; }
	</isscript>
	<isinclude url="${url}"/>
<iselse/>
	<isset name="isQuickView" value="${pdict.CurrentHttpParameterMap.source.stringValue == 'quickview' || pdict.CurrentHttpParameterMap.source.stringValue == 'cart' || pdict.CurrentHttpParameterMap.source.stringValue == 'giftregistry' || pdict.CurrentHttpParameterMap.source.stringValue == 'wishlist'}" scope="page"/>
	<isscript>
		if (pdict.CurrentVariationModel && !empty(pdict.CurrentVariationModel.selectedVariant)) {
			pdict.Product = pdict.CurrentVariationModel.selectedVariant;
		}
		var masterId = pdict.Product.isVariant() ? pdict.Product.masterProduct.ID : pdict.Product.ID;
		var avm = pdict.Product.availabilityModel;
		pdict.available = avm.availability>0;

		var availableCount = "0";
		if (pdict.available && !empty(avm.inventoryRecord)) {
			availableCount = avm.inventoryRecord.perpetual ? "999" : avm.inventoryRecord.ATS.value.toFixed().toString();
		}
	</isscript>
	<iscomment>
		primary details
		=============================================================
	</iscomment>

	<h2 class="visually-hidden">Details</h2>
	<span class="visually-hidden" itemprop="url">${URLUtils.http('Product-Show','pid', pdict.Product.ID)}</span>

 	<iscomment>
        primary details
        =============================================================
    </iscomment>
    <div class="top-title right-align">
        <isif condition="${('logoImage' in pdict.Product.custom && !empty(pdict.Product.custom.logoImage)) || (!empty(brandContentAssetID) && !empty(dw.content.ContentMgr.getContent(brandContentAssetID)))}">
            <div class="product-logo">
            	<isif condition="${dw.content.ContentMgr.getContent(brandContentAssetID)}" >
            		<iscontentasset aid="${brandContentAssetID}" />
            	<iselse>
                	<img src="${pdict.Product.custom.logoImage.getURL()}"/>
            	</isif>
            </div>
        </isif>
        <div class="product-name-container" pr1id="${pdict.Product.getID()}" pr1nm="${StringUtils.stringToHtml(pdict.Product.getName())}" pr1br="${pdict.Product.getBrand()}" pr1va="${pdict.Product.isVariant() ? pdict.Product.getManufacturerSKU() : ''}" pr1ca="${pdict.Product.custom.external_id}" pr1pr="${ProductUtils.getPricing(pdict.Product).salePriceMoney.decimalValue}" pr1qt="1" prlcat="${ProductUtils.getProductCategoryFromProduct(pdict.Product)}" prlpsku="${pdict.Product.isVariant() ? pdict.Product.masterProduct.getManufacturerSKU() : pdict.Product.getManufacturerSKU()}">
            <h1 class="product-name" itemprop="name">
           		${Resource.msg('product.tulo','product',null)} <isprint value="${(pdict.Product.name).toLowerCase()}">
           		
            </h1>
            <h2 class="visually-hidden">Details</h2>
            <span class="visually-hidden" itemprop="url">${URLUtils.http('Product-Show','pid', pdict.Product.ID)}</span>

        </div>
        <div class="review-area">
        	
        	<iscomment>
		        reviews
		        =============================================================
		    </iscomment>
			<isif condition="${pdict.isNewBVEnabled}">
				<isinclude template="bv/display/rr/reviewsummary"/>
			</isif>
		    <isif condition="${!isQuickView}">
		   		<div style="display: none !important;"><isinclude template="bv/display/rr/reviewsummary"/></div>	
		    	<div class="custom-reviews-holder-content">			
		        </div>
		    </isif>
        </div>
		  <isif condition="${isQuickView}">
       		 <!-- shown only in quick view -->
        		<a href="${URLUtils.url('Product-Show','pid', pdict.Product.ID)}" title="${pdict.Product.name}" class="quickview-full-details">${Resource.msg('product.viewdetails','product',null)}</a>
    		</isif>
        <div class="product-number">
            ${Resource.msg('product.item','product',null)} <span itemprop="productID" data-masterid="${masterId}"><isprint value="${ pdict.available ? pdict.Product.ID : masterId }"/></span>
        </div>
    </div>
	
    <iscomment>
		product pricing
		=============================================================
	</iscomment>
	<isset name="isRequiredMasterPrice" value="false" scope="page"/>
    <isinclude template="product/components/pricing"/>
	<isset name="showTieredPrice" value="${true}" scope="page"/>
	<ispdppricing salefirst=""/>


	<iscomment>
		Key Features attribute bullets
		=============================================================
	</iscomment>

	<div class="product-key-features">
		<p><isprint value="${pdict.Product.shortDescription}"></p>
	</div>		

	<iscomment>
		variations
		=============================================================
	</iscomment>
		<isinclude template="product/components/variations_livmattress"/>
	<iscomment>
		add to cart form
		=============================================================
	</iscomment>
	<form action="${URLUtils.continueURL()}" method="post" id="${pdict.CurrentForms.product.addtocart.dynamicHtmlName}" class="pdpForm">
		<fieldset>			
			
			<iscomment>
				product promotions
				=============================================================
			</iscomment>
			<isset name="showTieredPrice" value="${false}" scope="page"/>
			<iscomment>
				<isinclude template="product/components/promotions"/>
			</iscomment>
			<div class="product-add-to-cart">
				<h2 class="visually-hidden">Product Actions</h2>		



				<isset name="pam" value="${pdict.Product.getAttributeModel()}" scope="page"/>
				<isset name="group" value="${pam.getAttributeGroup('mainAttributes')}" scope="page"/>
				<isinclude template="product/components/group"/>	

				<iscomment>
					product quantity
					=============================================================
				</iscomment>

				<div class="add-to-cart-container clearfix">
					<div class="inventory">
						<div class="quantity">
							<label for="Quantity">${Resource.msg('global.qty','locale',null)}:</label>
							<iscomment>
							<input type="text" class="input-text" name="Quantity" id="Quantity" size="2" maxlength="3" value="${Number(empty(pdict.CurrentHttpParameterMap.Quantity.stringValue) ? 1 : pdict.CurrentHttpParameterMap.Quantity.stringValue).toFixed()}" data-available="${availableCount}"/>
							</iscomment>
								<isscript>
    								var numArr : dw.util.ArrayList = new dw.util.ArrayList(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);
     							</isscript>
      							<select name="Quantity" class="quantity-dropdown uniform" id="Quantity">
       								<isloop iterator="${numArr.iterator()}" var="arrElem">
        								<option value="${Number(arrElem).toFixed()}"
        												<isif condition="${Number(empty(pdict.CurrentHttpParameterMap.Quantity.stringValue) ? 1 : pdict.CurrentHttpParameterMap.Quantity.stringValue).toFixed() == arrElem}">selected="selected"</isif>>
        										 ${Number(arrElem).toFixed()}
         								</option>
       								</isloop>
      							</select>
						</div>
					</div>

					<iscomment>
						add to cart submit
						=============================================================
					</iscomment>

					<isscript>
						var updateSources = ["cart", "giftregistry", "wishlist"];
						var source = pdict.CurrentHttpParameterMap.source.stringValue;
						//var buttonTitle = dw.web.Resource.msg('product.buytulo','product','Buy tulo')+" "+(!empty(tuloComfortName)?tuloComfortName:'');//dw.web.Resource.msg('global.addtocart','locale','Buy tulo');
						var buttonTitle = dw.web.Resource.msg('product.addtocart','product','Add to cart');
						var plid = null;
						if( updateSources.indexOf(source)>-1 ){
							buttonTitle = dw.web.Resource.msg('global.update','locale','Update');
							if( pdict.CurrentHttpParameterMap.productlistid && pdict.CurrentHttpParameterMap.productlistid.stringValue ) {
								plid = pdict.CurrentHttpParameterMap.productlistid.stringValue;
							}
						} else {
							// Only pass on white-listed sources
							source = null;
						}
					</isscript>
					<isset name="cartAction" value="add" scope="page"/>

					<isif condition="${pdict.CurrentHttpParameterMap.uuid.stringValue && !empty(pdict.CurrentHttpParameterMap.uuid.stringValue)}">
						<input type="hidden" name="uuid" id="uuid" value="${pdict.CurrentHttpParameterMap.uuid.stringValue}" />
						<isset name="cartAction" value="update" scope="page"/>
					</isif>
					<isif condition="${source}">
						<input type="hidden" name="source" id="source" value="${source}" />
					</isif>
					<isif condition="${plid}">
						<input type="hidden" name="productlistid" id="productlistid" value="${plid}" />
					</isif>
					<input type="hidden" name="cartAction" id="cartAction" value="${cartAction}" />
					<input type="hidden" name="pid" id="pid" value="${pdict.Product.ID}" />
					<isset name="disabledAttrComfort" value="${masterId !=  generalProductID? '' : ' disabled="disabled"'}" scope="page"/>
					<isset name="disabledAttrOOS" value="${pdict.available && !pdict.Product.master && !pdict.Product.variationGroup ? '' : ' disabled="disabled"'}" scope="page"/>
					
					<isif condition="${disabledAttrComfort.length == 0 && disabledAttrOOS.length == 0}">
						<button id="add-to-cart" type="submit" title="${buttonTitle}" value="${buttonTitle}" class="button-fancy-large add-to-cart disabled-btn">${buttonTitle}</button>
								
					<iselseif condition="${disabledAttrComfort.length > 0}">
						
						<div class="add-to-cart-area">
							<button id="add-to-cart" type="button" title="Select comfort" value="Select a comfort" class="button add-to-cart-disabled">${buttonTitle}</button>
							<div class="comfort-selection-dropdown">
								<div class="title">Which comfort level do you want?</div>
									<ul class="comfort-list">
										<isif condition="${!empty(comfortConfiguration)}">
											<isloop items="${comfortConfiguration}" var="comfort">		
												<isif condition="${masterId == comfort.productID}">
													<isset name="selectedComfortValue"  value="${comfort.name}" scope ="page" />							
													<isset name="selectedComfortID"  value="${comfort.productID}}" scope ="page" />
												</isif>
												<li>
													<label class="option-container ${comfort.name}">
														<input type="radio" name="comfort_swatches1" data-pid="${comfort.productID}" class="swatches" value="${URLUtils.url('Product-Show', 'pid', comfort.productID)}">
														<span>
															<strong>${comfort.name.charAt(0).toUpperCase() + comfort.name.slice(1)}</strong>
															 - ${comfort.description}
														</span>
													</label>
												</li>
											</isloop>
										</isif>					
									</ul>
									<a href="${URLUtils.url('Page-Show','cid','which-mattress')}" class="not-sure hidden">
										I'm not sure 
										<i class="fa fa-chevron-right" aria-hidden="true"></i>
									</a>
							</div>
						</div>
					<iselseif condition="${disabledAttrOOS.length > 0}">
						<isscript>
							var pvm : dw.catalog.ProductVariationModel = pdict.Product.getVariationModel();
							var it : dw.util.Iterator = pvm.getProductVariationAttributes().iterator();
							var array : Array = [];
							var options = '';
							var requiredOptions = '';
							while( it.hasNext() ) {
								var text : dw.object.ObjectAttributeDefinition = it.next();
								array.push( text.displayName );
							}
							options = array.join(', ');
							var lastIndex = options.lastIndexOf(',');
							if( lastIndex > 0 && options.length > 1 && array.length > 1) {
							 requiredOptions = options.substr(0,lastIndex) + ' ' + dw.web.Resource.msg('product.attributedivider', 'product', null) + options.substr(lastIndex+1, options.length);
							} else {
							 requiredOptions = options;
							}
							var buttonTitleDisabledSelectVariation = StringUtils.format(dw.web.Resource.msg('product.missingval','product', null), requiredOptions);
						</isscript>
						<button id="add-to-cart" type="button" title="out of stock" value="out of stock" class="button add-to-cart-disabled">out of stock</button>
					</isif>
					</div><!--  end add to cart container -->
				</div><!--  end details block -->
				<isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('ShowSYFOnPdp')}">
					<isinclude template="product/pdpFinancing"/>
				</isif>
			</fieldset>
		</form>
		
		<isif condition="${disabledAttrComfort.length == 0 && disabledAttrOOS.length == 0}">
			<isif condition="${pdict.available && !pdict.Product.bundle && !(pdict.Product.master || pdict.Product.variationGroup)  && dw.system.Site.getCurrent().getCustomPreferenceValue('enableWishlistOnPdp')}">
				<div class="wishlist">
					<a class="add-to-wishlist" href="${URLUtils.https('Wishlist-Add', 'pid', pdict.Product.ID, 'source', 'productdetail')}" title="${Resource.msg('global.addtowishlist.label','locale',null)}">${Resource.msg('global.addtowishlist','locale',null)} <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
					<iscomment>
						<a class="button simple" data-action="gift-registry" href="${URLUtils.https('GiftRegistry-AddProduct', 'pid', pdict.Product.ID, 'source', 'productdetail')}" title="${Resource.msg('global.addtogiftregistry.label','locale',null)}">${Resource.msg('global.addtogiftregistry','locale',null)}</a>
					</iscomment>
				</div>
			</isif>		
		</isif>	 	
 
	<iscomment>
		product actions
		=============================================================
	</iscomment>

	<div class="product-actions">

	</div><!--  end details block -->
	<iscomment>This is ugly, but it works until we can get a better solution</iscomment>
	<isif condition="${pdict.GetImages}">
		<div id="update-images" style="display:none">
			<isinclude template="product/components/productimages"/>
		</div>
	</isif>	
	
</isif>
