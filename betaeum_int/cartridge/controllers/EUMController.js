/**
* Description of the Controller and the logic it provides
*
* @module  controllers/betaeum_int
*/

/* Script Modules */

var ISML = require('dw/template/ISML');



function injectjs(){
	
	var host = request.httpHost;
	var eumEnabled = dw.system.Site.getCurrent().getCustomPreferenceValue("EUMEnabled");
	var eumID = dw.system.Site.getCurrent().getCustomPreferenceValue("EUMID");
	var eumVersion = dw.system.Site.getCurrent().getCustomPreferenceValue("EUMVersion");
	var eumLocalHosted = dw.system.Site.getCurrent().getCustomPreferenceValue("EUMLocaleHosted");
	
	if(eumEnabled == true && !empty(eumID) && !empty(eumVersion))
	{
		if(eumLocalHosted == true)
			{
				var eumURL = require('dw/web/URLUtils').staticURL('js/adrum-' + eumVersion + '.js');
			}
			else
			{
				var eumURL = '//cdn.appdynamics.com/adrum/adrum-' + eumVersion + '.js';
			}

		ISML.renderTemplate('controllereum', {eumID : eumID, eumURL: eumURL, eumEnabled: eumEnabled});
	}
		
}
/***
 * it will post page to EUM for tracking
 * @returns
 */
function trackpage(){
	var host = request.httpHost;
	var eumEnabled = dw.system.Site.getCurrent().getCustomPreferenceValue("EUMEnabled");
	var eumID = dw.system.Site.getCurrent().getCustomPreferenceValue("EUMID");
	var eumVersion = dw.system.Site.getCurrent().getCustomPreferenceValue("EUMVersion");
	
	if(eumEnabled == true && !empty(eumID) && !empty(eumVersion))
	{
		ISML.renderTemplate('trackpageseum');
	}
}


/*
 * Web exposed methods
 */
/** Renders a full featured product search result page.
 * @see module:controllers/Search~show 
 * */
exports.InjectJS = injectjs;
exports.InjectJS.public=true;
exports.TrackPage = trackpage;
exports.TrackPage.public=true;
