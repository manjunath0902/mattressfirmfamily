
/**
* Description of the Controller and the logic it provides
*
* @module  controllers/Finder
*/

'use strict';
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var MFService = require('int_finder/cartridge/scripts/finder_v3/mattressfinderservices_v3');
var Logger = require('dw/system/Logger').getLogger('finderv3', 'finder');
var Content = app.getModel('Content');
var pageMeta = require('app_storefront_controllers/cartridge/scripts/meta');
var ProductUtils = require('app_storefront_core/cartridge/scripts/product/ProductUtils.js');
var emailHelper = require("app_storefront_controllers/cartridge/scripts/util/SFEmailSubscriptionHelper").SFEmailSubscriptionHelper;
var StringHelpers = require('app_storefront_core/cartridge/scripts/util/StringHelpers.js');
var URLUtils = require('dw/web/URLUtils');
var ProductSearchModel = require('dw/catalog/ProductSearchModel');
var ArrayList = require('dw/util/ArrayList');
var HashMap = require('dw/util/HashMap');
//Adding Classes
var CatalogMgr = require('dw/catalog/CatalogMgr');
var Site = require('dw/system/Site');
var PagingModel = require('dw/web/PagingModel');
const DeaultPageSize = 6

/**
* Description of the function
*
* @return {String} The string 'myFunction'
*/
 var search = function(){
	 try {
		var accountPersonalDataAsset;
		var mattressfinderAsset = Content.get('mfinder-questions-v3');
		pageMeta.update(mattressfinderAsset);
		
		 
		 var apiKey = Site.current.getCustomPreferenceValue("mf_API_key_v3");
	     var result = MFService.GetAPIToken(apiKey);
	     var token = "";
	     if(!empty(result)) {
	    	 token = result.tokenValue;
		 }
	    //Get HeightWeight Data from custom preference
	 	//Previously we were fetching it from service, this can be found in KFinder.js file
	     var heightWeightData = JSON.parse(Site.current.getCustomPreferenceValue("heightWeightApiData"));
	    //Get Typology Data from custom preference
	 	//Previously we were fetching it from service, this can be found in KFinder.js file
	     var userTypologyData = JSON.parse(Site.current.getCustomPreferenceValue("typologyServiceData"));
	     
	     //session.custom.HeightWeightData = heightWeightData;
	     //session.custom.UserTypologyData = userTypologyData;
	     session.custom.mfToken = token;
	     
	     if(!empty(token) && !empty(heightWeightData) && !empty(userTypologyData)) {
	    	 app.getView({
		    	 ContinueURL: URLUtils.https('KFinder_v3-SubmitQuiz')
		     }).render('finder_v3/finder_v3');
	    	 return;
	     } else {
    		var resultTemplate = 'finder_v3/newresults_v3'; 
    	    
	    	 app.getView({
	    	    	IsServicesError: true
	    	    }).render(resultTemplate);
	     }
	 }
	 catch (e) {
		 Logger.error("Exception Occured While Calling KIOSK Services: " + e );
	 }
 }

var submitQuiz = function() {
	var token = session.custom.mfToken;
	var apiKey = Site.current.getCustomPreferenceValue("mf_API_key_v3");
	var result = MFService.GetAPIToken(apiKey);
	var token = "";
	if(!empty(result)) {
		token = result.tokenValue;
	}
	//Get HeightWeight Data from custom preference
	//Previously we were fetching it from service, this can be found in KFinder.js file
	var heightWeightData = JSON.parse(Site.current.getCustomPreferenceValue("heightWeightApiData"));
	
	//Get Typolog Data from custom preference
	//Previously we were fetching it from service, this can be found in KFinder.js file
	var userTypologyData = JSON.parse(Site.current.getCustomPreferenceValue("typologyServiceData"));
	session.custom.mfToken = token;
	var resultTemplate = 'finder_v3/newresults_v3';
	var showSignUpPopup = true;
	var isMobile = StringHelpers.isMobileDevice(request.httpUserAgent);
	if (!empty(token)) {
	    var results = MFService.GetRecommendedProducts(token, request.httpParameterMap, userTypologyData);
	    
	    showSignUpPopup = mattressFinderResultsComparison(results);
	    
	    session.custom.MFResults = results;
	    app.getView({
	    	ProductsData : results.data,
	    	PriceRange   : results.PriceRange,
	    	ShowSignUpPopup : showSignUpPopup,
	    	isMobile: isMobile
	    }).render(resultTemplate);
	} else {
		app.getView({
	    	IsServicesError: true,
	    	isMobile: isMobile
	    }).render(resultTemplate);
	}
}
/***
 * Collect Posted Parameters and redirect to Result Page 
 */
var submitReactQuiz = function() { 
	session.custom.QuizJsonData = !empty(request.httpParameterMap.data) ? request.httpParameterMap.data: {};
	response.redirect(URLUtils.https('KFinder_v3-RenderReactQuizResult'));
}
/***
 * Render the React Result page
 */
function renderReactQuizResult() {
	var params = request.httpParameterMap;
	var emailAddress = !empty(request.httpParameterMap.emailAddress) ? request.httpParameterMap.emailAddress.stringValue : '';
	var resultTemplate = 'finder_v4/results';
	var productIds = new ArrayList();
	var selectedSizeAtQuiz;
	var isMobile = StringHelpers.isMobileDevice(request.httpUserAgent);
	var productBenefits;
	var oldRecProductsString = !empty(session.custom.recProductString) ? session.custom.recProductString : '';
	var newRecProductsString = '';
	var isMobile = StringHelpers.isMobileDevice(request.httpUserAgent);
	var QuizJsonData;
	var fromEmail = false;
	var avatarData;
    var productPercentageHashMap = new HashMap();
    var zipCode = '';
    var isSearchGrid = ((params.format.stringValue === 'ajax' && (params.element.stringValue === 'refinements' || params.element.stringValue === 'searchgrid')) || params.format.stringValue === 'page-element');
	//If user comes from the results email 
	if(!empty(emailAddress)){
	    zipCode = ProductUtils.getRequestZipCode();
		var reqParams = {
			emailAddress : emailAddress
		};
		
		if (isSearchGrid) {
			var results = session.custom.MFResults;
		} else {
			var results = MFService.GetCustomerProfileAPi(reqParams);
			if(!empty(results) && !empty(results.data) && !empty(results.data.recommendedProducts)){
				results.data.recommendedProduct = results.data.recommendedProducts;
				session.custom.MFResults = results;
			}
		}
		if(!empty(results) && !empty(results.data) && !empty(results.data.recommendedProduct)){
			results.data.recommendedProduct = results.data.recommendedProduct;
			var recProducts = results.data.recommendedProducts;
			selectedSizeAtQuiz = !empty(results.data.customerResponses) ? JSON.parse(results.data.customerResponses).mattressSize : '';
    		for each(var product in recProducts){
    			var Product = dw.catalog.ProductMgr.getProduct('mfi' + product.sku);
       			var productID = (Product.master) ? ProductUtils.getVariantForSize(Product,selectedSizeAtQuiz) : Product.ID;
    			productIds.add(productID);
                productPercentageHashMap.put(productID, product.percentageMatch);
    			newRecProductsString = newRecProductsString +'|'+product.sku;
    		}
		}
		
		if(!empty(results) && !empty(results.data)){
			QuizJsonData = results.data;
			session.custom.QuizJsonData = QuizJsonData;
		}
		
		session.custom.MFEmailResults = results;
		fromEmail = true;
	}
	//If user comes from the quiz page
	else{
		if(session.custom.QuizJsonData){
			QuizJsonData = JSON.parse(session.custom.QuizJsonData.rawValue);
			selectedSizeAtQuiz = QuizJsonData.mattressSize ? QuizJsonData.mattressSize : '';
			zipCode = !empty(QuizJsonData.customerzipcode) ? QuizJsonData.customerzipcode : '';
			// If we we are coming for refinements then we will refine on previous data.
			if (isSearchGrid) {
				var results = session.custom.MFResults;
			} else {
				var results = MFService.GetRecommendedProductsV4(session.custom.QuizJsonData);
				session.custom.MFResults = results;
			}
			if (!empty(results)) {
	        	if (!empty(results.data) && !empty(results.data.recommendedProduct)) {
	        		var recProducts = results.data.recommendedProduct;
	    			for each(var product in recProducts){
	    				var Product = dw.catalog.ProductMgr.getProduct('mfi' + product.sku);
	       				var productID = (Product.master) ? ProductUtils.getVariantForSize(Product,selectedSizeAtQuiz) : Product.ID;
	    				productIds.add(productID);
                    	productPercentageHashMap.put(productID, product.percentageMatch);
	    				newRecProductsString = newRecProductsString +'|'+product.sku;
	    			}
	        	}
			}
		}
	}
	if (!empty(productIds)) {
		var isSearchResultEmpty = false;
        var productSearchModel = initializeProductSearchModel(productIds, params);
        productSearchModel.search();
        // In Case we dont get results fro removing filters then fallback to show all products recommended by the API on first time.
        if (isSearchGrid && (empty(productSearchModel) || productSearchModel.count === 0)) {
        	productSearchModel = initializeProductSearchModel(productIds);
            productSearchModel.search();
            isSearchResultEmpty = true;
        }
        var productHits = productSearchModel.getProductSearchHits();
        // If default Sorting Rule is not defined in BM "Recommended for You" then dont sort on Percentage Match Attribute of Product in API Response
        var DefaultRuleID = 'Recommended for You';
        var defaultSortingRule = dw.catalog.CatalogMgr.getSortingRule(DefaultRuleID);
        if (defaultSortingRule != null && ((!params.srule.submitted)  || (params.srule.submitted && params.srule.value == "Recommended for You")) ) {
        	productIDsCollection = MFService.GetTopMatchedSortedProductsV4(results.data.recommendedProduct, productHits);
        } else {
        	productIDsCollection = MFService.CovertSearchHitsToCollectionV4(productHits);
        }
        //Paging implementation
        var productsPagingList = new ArrayList(productIDsCollection);
        
        var productPagingModel = new PagingModel(productsPagingList);
    	if (params.start.submitted) {
        	productPagingModel.setStart(params.start.intValue);
    	}
    	
    	if (params.sz.submitted && params.sz.intValue <= 30) {
            productPagingModel.setPageSize(params.sz.intValue);
        }else {                	
        	productPagingModel.setPageSize(DeaultPageSize);                  
        }
	    //showSignUpPopup = mattressFinderResultsComparison(results);
    	session.custom.recProductString = newRecProductsString;
    	var showSignUpPopup = (oldRecProductsString !== newRecProductsString) ? true : false;
    	
    	//Product Benefits and edge case and avatar
    	if(!isSearchGrid && !empty(QuizJsonData)){
    		productBenefits = MFService.GetProductBenefitsToLookForV4(QuizJsonData);
    		avatarData = MFService.GetAvatarV4(QuizJsonData);
    	}
		var priceRange = !empty(results) ? results.PriceRange : undefined;
	    if (params.format.stringValue === 'ajax' && params.element.stringValue === 'searchgrid') {
	    	app.getView({
	    		ProductsData : productIDsCollection,
		    	ProductSearchResult: productSearchModel,
		    	ProductPagingModel: productPagingModel,
		    	selectedSizeAtQuiz : selectedSizeAtQuiz,
		    	fromEmail : fromEmail,
		    	isSearchResultEmpty : isSearchResultEmpty,
                ProductPercentageHashMap : productPercentageHashMap,
                ZipCode: zipCode
	        }).render('finder_v4/searchcontainer_mattress_matcher');
	    	 return;
	    } else if (params.format.stringValue === 'page-element') {
	    	app.getView({
		    	ProductSearchResult: productSearchModel,
		    	ProductPagingModel: productPagingModel,
		    	selectedSizeAtQuiz : selectedSizeAtQuiz,
		    	fromEmail : fromEmail,
                ProductPercentageHashMap : productPercentageHashMap,
                ZipCode: zipCode
	        }).render('finder_v4/productgridwrapper_mattress_matcher');
	    	 return;
	    } else {
		    app.getView({
		    	ProductsData : productIDsCollection,
		    	PriceRange   : priceRange,
		    	ShowSignUpPopup : showSignUpPopup,
		    	isMobile: isMobile,
		    	ProductSearchResult: productSearchModel,
		    	ProductPagingModel: productPagingModel,
		    	selectedSizeAtQuiz : selectedSizeAtQuiz,
		    	productBenefits : productBenefits,
		    	fromEmail : fromEmail,
		    	avatarData : avatarData,
                ProductPercentageHashMap : productPercentageHashMap,
                ZipCode: zipCode
		    }).render(resultTemplate);
	    }
	} else {
		app.getView({
	    	IsServicesError: true,
	    	isMobile: isMobile
	    }).render(resultTemplate);
	}
}


function mattressFinderResultsComparison(results) {
	if (!empty(results) && !empty(session.custom.MFResults)) {
		var latestRecoPIDsString = "";
		var oldRecoPIDsString = "";
	    
		latestRecoPIDsString = getRecommendationsString(results);
	    oldRecoPIDsString = getRecommendationsString(session.custom.MFResults);

		if (oldRecoPIDsString !== latestRecoPIDsString) { 
			return true;
		} else {
			return false;
		}
	}
	return true;
}

function getRecommendationsString (results) {
	var recommendationsPIDsString = "";
    var recommendationsArray = [];
	if(results.data.recommendedProduct.length > 0) {
    	for each(var prod in results.data.recommendedProduct) {
    		recommendationsArray.push(prod.sku.toString());
    	}
    	
    	recommendationsPIDsString = recommendationsArray.length > 0 ? recommendationsArray.join('|') : "";
    }
	return recommendationsPIDsString;
}

var getWeightByHeight = function() {
	
	var height = request.httpParameterMap.selectedHeight;
	//var lookupData = session.custom.HeightWeightData;
	var heightWeightData = JSON.parse(Site.current.getCustomPreferenceValue("heightWeightApiData"));
    
	var obj = MFService.GetWeightByHeight(height, heightWeightData);
	app.getView({
    	obj : obj
    }).render('util/json');
	
}

var sendResults = function() {
	var params = request.httpParameterMap;
	var emailAddress = params.emailID.value.toLowerCase().trim();
	var token = MFService.AuthenticateContact();
    if(!empty(token)) {
        var contactID = MFService.GetContact(token);
        var result = MFService.SendContact(token, session.custom.MFResults, contactID);
        app.getView({
            Result : result
        }).render('result');
    } else {
        var result = {};
        app.getView({
            Result : result
        }).render('result');
    }
}

var saveCustomerProfile = function() {
    var result = MFService.SaveCustomerProfile(session.custom.MFResults, session.custom.QuizJsonData);
    app.getView({
        Result : result
    }).render('finder_v4/result');
}

function hitTile_V3 () {
    var params = request.httpParameterMap;
    var productResult = dw.catalog.ProductMgr.getProduct(params.pid.stringValue);
    //check geo based available or not
    var geoAvailability = !empty(productResult) ? ProductUtils.checkGeoAvailability(productResult) : false;
    app.getView({
        Product: productResult,
        Alternate : params.alternate,
        geoAvailability : geoAvailability
    }).render('finder/productsearchhittilefinder_v3');
}

function hitTileMattressMatcher () {
    var params = request.httpParameterMap;
    var productResult = dw.catalog.ProductMgr.getProduct(params.pid.stringValue);
    var percentageMatch;
    //check geo based available or not
    var geoAvailability = !empty(productResult) ? ProductUtils.checkGeoAvailability(productResult) : false;
    var MFResults = params.fromEmail=='true' ? session.custom.MFEmailResults: session.custom.MFResults;
    var allProductBenefits = require('int_finder/cartridge/scripts/utils/productBenefitsFactory').allProductBenefiits;
    var Resource = require('dw/web/Resource');
    
    var productBenefits = [];
    if(!empty(MFResults)){
    	var recommendedProducts = MFResults.data.recommendedProduct;
    	if(recommendedProducts){
    		for (var i = 0; i < recommendedProducts.length ; i++){
    			let currentProduct = productResult.isVariant() ? productResult.masterProduct.ID : params.pid.stringValue;
                let recommendedProductsID = "mfi" + recommendedProducts[i].sku;
    			if(recommendedProductsID == currentProduct) {
    				percentageMatch = recommendedProducts[i].percentageMatch;
    				let mattributes = recommendedProducts[i].mattributes;
    				for(var j = 0 ; j < allProductBenefits.length ; j++){
    					let productBenefit = allProductBenefits[j];
    					if(productBenefit.id == "adjustableBaseSuitable" && recommendedProducts[i].adjustableBaseSuitable == 1){
    						productBenefit.value = 'Yes';
							productBenefits.push(productBenefit);
    					} else if(mattributes[productBenefit.id] && mattributes[productBenefit.id].toLowerCase() != "none"){
    						if(productBenefit.id == "comfort" && mattributes[productBenefit.id].toLowerCase() == "soft"){
    							productBenefit.value = "Plush";
    						} else {
    							productBenefit.value = mattributes[productBenefit.id];
    						}
							productBenefits.push(productBenefit);
    					}
    				}
    				break;
    			}
    		}
    	}
    }
    app.getView({
        Product: productResult,
        Alternate : params.alternate,
        geoAvailability : geoAvailability,
        percentageMatch: percentageMatch,
        productBenefits: productBenefits
    }).render('finder/productsearchhittilefinder_mattress_matcher');
}

function initializeProductSearchModel(productIds, httpParameterMap) {
	    var productSearchModel = new ProductSearchModel();
	    if (typeof(httpParameterMap) != "undefined") {
		    var nameMap = httpParameterMap.getParameterMap('prefn');
		    var valueMap = httpParameterMap.getParameterMap('prefv');
		
		    for (var i in nameMap) {
		        if (valueMap[i]) {
		        	productSearchModel.addRefinementValues(nameMap[i], valueMap[i]);
		        }
		    }
		    
	        if (httpParameterMap.pmin.submitted) {
	        	if (httpParameterMap.pmin.stringValue.indexOf('|') > -1) {
	        		var minPriceArray = httpParameterMap.pmin.stringValue.split('|').sort(function(a, b){return a - b});
	        		var pminValue = Number(minPriceArray[0]);
	        	} else {
	        		var pminValue = httpParameterMap.pmin.doubleValue;
	        	}
	        	productSearchModel.setPriceMin(pminValue);
	        }
	        if (httpParameterMap.pmax.submitted) {
	        	if (httpParameterMap.pmax.stringValue.indexOf('|') > -1) {
	        		var maxPriceArray = httpParameterMap.pmax.stringValue.split('|').sort(function(a, b){return b - a});
	        		var pmaxValue = Number(maxPriceArray[0]);
	        	} else {
	        		var pmaxValue = httpParameterMap.pmax.doubleValue;
	        	}
	        	productSearchModel.setPriceMax(pmaxValue);
	        }
	    
		    var sortingRule = httpParameterMap.srule.submitted ? CatalogMgr.getSortingRule(httpParameterMap.srule.value) : null;
		    if (sortingRule) {
		        productSearchModel.setSortingRule(sortingRule);
		    }
	    }
    	// only add category to search model if the category is online
        if (!empty(productIds)) {
        	productSearchModel.setProductIDs(productIds);
        	// Setting Mattresses Category for Refinements
        	productSearchModel.setCategoryID("5637146827");
        }
	    return productSearchModel;
}
/* Exports of the controller */
///**
// * @see {@link module:controllers/Finder~newFinder} */
exports.Search = guard.ensure(['get'], search);
//* @see {@link module:controllers/Finder~submitQuiz} */
exports.SubmitQuiz = guard.ensure(['get'], submitQuiz);
//* @see {@link module:controllers/Finder~getWeightByHeight} */
exports.GetWeightByHeight = guard.ensure(['get'], getWeightByHeight);
//* @see {@link module:controllers/Finder~sendResults} */
exports.SendResults = guard.ensure(['get'], sendResults);
//* @see {@link module:controllers/Finder~hitTile_V3} */
exports.HitTile_V3 = guard.ensure(['get'], hitTile_V3);
exports.HitTileMattressMatcher = guard.ensure(['get'], hitTileMattressMatcher);
exports.SubmitReactQuiz = guard.ensure(['https'], submitReactQuiz);
exports.RenderReactQuizResult = guard.ensure(['https'], renderReactQuizResult);
exports.SaveCustomerProfile = guard.ensure(['get'], saveCustomerProfile);
