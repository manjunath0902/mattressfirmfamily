$(function(){
  var $prevBtn = $('.mfinder__button--previous'),
  $lastBtn = $('.mfinder__button--last'),
  $nextBtn = $('.mfinder__button--next');
  if($(window).width() < 767){
		$('.pt-mfinder-body .scrollToTop').remove();
  }


  var scrollToTop = function() {
    var $window = $(window),
      headerTopNewHeight = $('.header-top-new > div').children(':visible').outerHeight(),
      headerBannerHeight = $('.header-banner').outerHeight(),
      headerMainHeight = $('.header-main').outerHeight(),
      combinedHeaderHeight;

    if ($window.width() < 1032) {
      $(window).scrollTop($('.mfinder').offset().top - headerTopNewHeight);
    }
    else {
      combinedHeaderHeight = headerTopNewHeight + headerBannerHeight + headerMainHeight;
      $(window).scrollTop($('.mfinder').offset().top - combinedHeaderHeight);
    }
  }

  var goToNextCard = function() {
    ($nextBtn.is(':visible')) ? $nextBtn.trigger('click') : $nextBtn.show().trigger('click');
    scrollToTop();
  }

  // Returns a function, that, as long as it continues to be invoked, will not
  // be triggered. The function will be called after it stops being called for
  // N milliseconds. If `immediate` is passed, trigger the function on the
  // leading edge, instead of the trailing.
  var debounce = function(func, wait, immediate) {
    var timeout;
    return function() {
      var context = this, args = arguments;
      var later = function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  };

  var heightSlider = function () {

    $('.mfinder__question-height').each(function() {
      var $self = $(this),
          $textWidthFt = $self.find('.mfinder__height-input--feet'),
          $textWidthIn = $self.find('.mfinder__height-input--inches');

      function getFeet(n) {
        return Math.floor(n / 12);
      }

      function getInches(n) {
        return (n % 12);
      }

      function invokeHeightSlider(){
        var $heightSlider = $self.find(".mfinder__height-slider");
        $heightSlider.slider({
          min: 36,
          max: 96,
          values: [67],
          range: false,
          animate: true,
          slide: function(event, ui) {
            for (var i = 0; i < ui.values.length; ++i) {
              $textWidthFt.val(getFeet(ui.values[i]));
              $textWidthIn.val(getInches(ui.values[i]));
            }
          }
        });

      }

      function heightInputOnBlur() {
        $self.find(".mfinder__height input").on("blur", function() {
          if (!$textWidthFt.val() || isNaN($textWidthFt.val())) {
            $textWidthFt.val(5);
          }

          if (!$textWidthIn.val() || isNaN($textWidthIn.val())) {
            $textWidthIn.val(7);
          }
        });
      }

      // Check value of height and width as keyed in by user
      function onFocusSelect() {
    	  $self.find(".mfinder__height-input--feet").on("keydown", function (el) {
    		  $(this).val("");
    	  });
      }
      function heightInputOnKeyup() {
          $self.find(".mfinder__height-input").on("keyup keypress blur input", function() {
            var $this = $(this),
                startFt = parseInt($textWidthFt.val(), 10),
                startIn = parseInt($textWidthIn.val(), 10),
                startTotal = (startFt * 12) + startIn;


            $this.val($(this).val().replace(/[^\d].+/, "").replace(/\b(0(?!\b))+/g, ""));

            if ((event.which < 48 || event.which > 57)) {
              event.preventDefault();
            }

            if (parseInt($textWidthFt.val()) < 3) {
              parseInt($textWidthFt.val(3), 10);
              return false;
            }
            else if (parseInt($textWidthFt.val()) > 8) {
              parseInt($textWidthFt.val(8), 10);
              return false;
            }

            if (parseInt($textWidthIn.val()) > 11) {
              parseInt($textWidthIn.val(11), 10);
              return false;
            }
            else if ($this.hasClass("mfinder__height-input--feet")) {
          	  parseInt($textWidthFt.val($this.val()), 10);
            }
            if ((parseInt($textWidthFt.val()) >= 8 && parseInt($textWidthIn.val()) > 0)) {
                parseInt($textWidthIn.val(0), 10);
            }

            $self.find(".mfinder__height-slider").slider('values', 0, startTotal);
          });
        }

      function heightSliderActive() {
       $self.find('.mfinder__height-slider-container').addClass('active');
      }


      function setPageTitle() {
        document.title = "Mattress Finder";
      }

      var init = function() {
        setPageTitle();
        heightSliderActive();
        invokeHeightSlider();
        heightInputOnBlur();
        heightInputOnKeyup();
        onFocusSelect();
      }

      init();
    });
  };

  var finderExpandCollapse = function () {
    if ($('.mfinder__why').length > 0 && $(window).width() <= 767) {
      $('.mfinder__why-question').off().on('click', function (e) {
        var $parent = $(e.target).parents('.mfinder__why');
        if ($parent.is('expanded')) {
          $parent.attr('aria-expanded', 'true');
        }
        else {
          $parent.attr('aria-expanded', 'false');
        }
        $parent.toggleClass('expanded');
      });
    }
  };

  var mobileRadiosGoToNext = function() {
    if ($(window).width() <= 767) {
      $('.mfinder__radio-slider').each(function (index) {
        var $radioList = $(this);

        $radioList.find('.mfinder__radio-input').on('change', function (e) {
          var $target = $(e.target),
              selectedIndex = $target.closest('.mfinder__radio').index();

          $radioList.find('.mfinder__radio').removeClass('active');
          $radioList.find('.mfinder__radio-input').removeProp('checked');
          $target.closest('.mfinder__radio').addClass('active');
          $target.prop('checked');

          // Set current value of slider, just in case user resizes viewport
          $radioList.find('.mfinder__radio-slider-control').slider('value', selectedIndex);

          if($radioList.hasClass('single-selection')) {
            goToNextCard();
          }
        });
      });
    }
  };

  var radioButtonsToSlider = function() {
    function init(){
      $(".mfinder__radio-slider").each(function() {
        var radios = $(this).find(":radio").hide(),
            radioName = $(this).find(":radio").attr('name'),
            $radioSlider = $(this);
        $(this).addClass('active');
        $("<div class='mfinder__radio-slider-control'></div>").slider({
          range: "min",
          value: 1,
          min: parseInt(radios.first().val(), 10),
          max: parseInt(radios.last().val(), 10),
          animate: true,
          slide: function(event, ui) {
            radios.filter("[data-choice=" + ui.value + "]").click();
            var currentLabel = parseInt(ui.value - 1);
            $(":radio[name="+radioName+"]").closest('.mfinder__radio').removeClass('active');
            $(":radio[name="+radioName+"]:checked").closest('.mfinder__radio').addClass('active');
          },
          change: function( event, ui ) {
            function delayGoToNextCard() {
              var goToNextTimer;
              function endGoToNextTimer() {
                window.clearTimeout(goToNextTimer);
                goToNextTimer = window.setTimeout(function () {
                  // go to next card after brief delay
                  goToNextCard();
                }, 750);
              }
              endGoToNextTimer();
            }

            if ($radioSlider.hasClass('single-selection')) {
              delayGoToNextCard();
            }
          }
        }).prependTo(this);
      });
    }

    if ($(window).width() >= 768) {
      init();
    }
    else {
    	$(".mfinder__radio-slider").removeClass('active');
      mobileRadiosGoToNext();
    }
  };

  var finderWizard = function () {
    var $mfinderQuestions = $('.mfinder__questions'),
    // question animation variables
    pos = 0,
    slideCount = $mfinderQuestions.find('.mfinder__question:visible').length,
    currentSlide = 0,
    // mobile step nav variables
    mobileStepPos = 0,
    mobileStepCount = $('.mfinder__step').length,
    currentMobileStep = 0,
    pathFlag;

    // Based on user selection in first question, set the path user will follow.
    // This determines what questions they will see.
    var setPathFlag = function () {
      var flag = $('.mfinder__question-who .radio-input.selected').find('input[type=radio][name=mfinder_question_who]').val();
      pathFlag = flag;
      $mfinderQuestions.attr('data-finder-path', pathFlag);
    };

    // Based on user selection in first question, set as your or their,
    // my or their.
    var setPronoun = function () {
      var defaultPronounValue = 'your',
      defaultPronounTwoValue = 'my',
      pronoun = $('.mfinder__question-who .radio-input.selected').find('input[type=radio][name=mfinder_question_who]').data('pronoun'),
      pronounTwo = $('.mfinder__question-who .radio-input.selected').find('input[type=radio][name=mfinder_question_who]').data('pronoun-two');

      if (pronoun) {
        $('.pronoun').html(pronoun);
        $('.pronoun-two').html(pronounTwo);
      }
      else {
        $('.pronoun').html(defaultPronounValue);
        $('.pronoun-two').html(defaultPronounTwoValue);
      }
    };

    var clearRadioInputs = function(el) {
      $(el).find('.radio-input').removeClass('selected').find('input:radio').removeAttr('checked');
    };

    // When user makes a choice on first question, reset any pre-existing selections
    var resetFinderForm = function () {
      clearRadioInputs('.mfinder__question-position');
      clearRadioInputs('.mfinder__question-weight');
      clearRadioInputs('.mfinder__question-comfort');
      clearRadioInputs('.mfinder__question-price');
      // Clear out checkbox
      $('.mfinder__symptoms-checkbox-list').find('input.mfinder__checkbox-input').prop('checked', false);
      $('.mfinder__step').not(':eq(0)').removeClass('step-set');
    };

    // buttonList functionality
    var buttonListSetDefaultBtn = function() {
      $('.mfinder__button-list').each(function (index) {
        var $buttonList = $(this);

        $buttonList.find('.radio-input').on('click', function(e) {
          var $target = $(e.target);
          $buttonList.find('.radio-input').removeClass('selected').find('input[type=radio]').prop('checked', false);
          $target.closest('div.radio-input').addClass('selected').find('input[type=radio]').prop('checked', 'checked');

          // Set the user's path, pronoun and reset the form at the first step.
          if ($('div.mfinder__question-who').find(e.target).length) {
            setPathFlag();
            setPronoun();
            resetFinderForm();
            slideCount = $mfinderQuestions.find('.mfinder__question:visible').length;
          }

          // Auto-advance the question if only one selection is provided.
          if($buttonList.hasClass('single-selection')) {
            goToNextCard();
          }
        });

      });
    };

    function removeNavButtonsFromTabIndex() {
      document.getElementById('prev-btn').removeAttribute('tabindex');
      document.getElementById('next-btn').removeAttribute('tabindex');
      document.getElementById('last-btn').removeAttribute('tabindex');
    }

    function togglePrevBtn() {
      (currentSlide === 0) ? $prevBtn.attr('disabled', 'disabled') : $prevBtn.removeAttr('disabled');
    }

    function checkFirstStepSelected() {
      var $firstStep = $('.mfinder__step:eq(0)');
      if (currentSlide === 0 && $('.mfinder__question:eq(0)').find('.radio-input').hasClass('selected') == true) {
        $nextBtn.removeClass('hidden');
        $firstStep.addClass('step-set');
      }
    }

    function firstStepEnableNextOnBtnClick() {
      $("input[name=mfinder_question_who]").closest('.radio-input').on('click', function () {
        checkFirstStepSelected();
      })
    }

    function toggleNextBtn() {
      if (currentSlide === (slideCount-1)) {
        $nextBtn.attr('disabled', 'disabled');
        $lastBtn.show();
      }
      else {
        $nextBtn.removeAttr('disabled');
        if ($lastBtn.is(':visible')) {
          $lastBtn.hide();
        }
      }
    }

    // Set an active-item class on question
    function setActiveItem() {
      $('.mfinder__question').removeClass('active-item');
      $('.mfinder__question:visible').eq(currentSlide).addClass('active-item');
      setActiveStepName();

      // In mobile resize according to
      var portion = 90,
          portionFloat = parseFloat('0.' + portion),
          portionPercent = portion + '%',
          portionMargins = (parseInt(100 - portion, 10) / 2) + '%',
          leftPos = parseInt($(window).width() * 0.0213, 10) + 'px',
          prevLeftPos = parseInt($(window).width() * 0.0746, 10) + 'px',
          nextLeftPos = parseInt($(window).width() * 0.0746, 10);

      if ($(window).width() < 768) {
        $('.mfinder__question')
          .width(($(window).width() * portionFloat) + 'px')
          .css({
            'flex-basis': portionPercent,
            'flex': '0 0 ' + portionPercent,
            'position': 'relative',
            'left': 0, //leftPos,
            'margin': '0 ' + portionMargins});

        $('.mfinder__question').removeClass('mfinder__question-prev mfinder__question-next');

        $('.mfinder__question.active-item').prev('.mfinder__question:visible').addClass('mfinder__question-prev');
        $('.mfinder__question.active-item').next('.mfinder__question:visible').addClass('mfinder__question-next');

        $('.mfinder__question-prev').css('left', prevLeftPos);
        $('.mfinder__question-next').css('left', -nextLeftPos + 'px');
      }
    }

    function setActiveStepName() {
      var $activeItem = $('.mfinder__question.active-item:visible'),
          activeStepName = $activeItem.attr('data-step-name'),
          activeStepNumber = parseInt($activeItem.attr('data-step-indicator'),10),
          $steps = $('.mfinder__steps');

      // Get active step name from mfinder__question and set on step.
      if ($steps.find('.mfinder__step[data-step="' + activeStepName + '"]').length) {
        $steps.find('.mfinder__step').removeClass('active-step-name');
        $steps.find('.mfinder__step[data-step="' + activeStepName + '"]').addClass('active-step-name');
        // Set active step indicator from mfinder__question and set on set indicator.
        if (activeStepNumber != null) {
          $steps.find('.mfinder__step').find('.mfinder__stepindicator').removeClass('active-step');
          $steps.find('.mfinder__step.active-step-name').find('.mfinder__stepindicator').eq(activeStepNumber).addClass('active-step');
        }
      }

      setMobileStepPosition();
    }

    function isRadioSelected(el) {
      return el.find('.radio-input').hasClass('selected') == true;
    }

    function isRadioSliderSelected(el) {
      return el.find('.mfinder__radio-input').hasClass('selected') == true;
    }

    function isCheckboxListChecked(el) {
      return el.find('input[type=checkbox]:checked').length > 0;
    }

    function validateSinglePageRadioQuestion(activeStepName) {
      if ( isRadioSelected($('.mfinder__question[data-step-name="'+ activeStepName +'"]:eq(0):visible')) ) {
        $('.mfinder__step[data-step="'+activeStepName+'"]').addClass('step-set');
      }
      else {
        $('.mfinder__step[data-step="'+activeStepName+'"]').removeClass('step-set');
      }
    }

    function setStepInHeader(stepName) {
      $('.mfinder__step[data-step="'+stepName+'"]').addClass('step-set');
    }

    function clearStepInHeader(stepName) {
      $('.mfinder__step[data-step="'+stepName+'"]').removeClass('step-set');
    }

    function onActiveStepCheckIfSet() {
      var $activeItem = $('.mfinder__question.active-item:visible'),
          activeStepName = $activeItem.data('step-name'),
          activeStepIndicator = $activeItem.data('step-indicator');

      if ($mfinderQuestions.attr('data-finder-path') === 'myself' || $mfinderQuestions.attr('data-finder-path') === 'someone_else') {

        // NOTE: all these questions check if question has been set by user when
        // they get to the next question
        if (activeStepName === 'symptoms') {
          // Check value for previous 'position' question.
          if ($('.mfinder__question[data-step-name="position"]:eq(0)').find('.radio-input').hasClass('selected')) {
            setStepInHeader('position');
          }
          else {
            clearStepInHeader('position');
          }

        }
        else if (activeStepName === 'features' && activeStepIndicator === 0) {
          if ( isCheckboxListChecked($('.mfinder__question[data-step-name="symptoms"]:eq(0):visible')) ) {
            setStepInHeader('symptoms');
          }
          else {
            clearStepInHeader('symptoms');
          }
        }
        else if (activeStepName === 'features' && activeStepIndicator === 2) {
          setStepInHeader('features');
        }
        else if (activeStepName === 'feel') {
          // Check 'weight'
          if ( isRadioSelected($('.mfinder__question[data-step-name="body"]:eq(2):visible')) ) {
            setStepInHeader('body');
          }
          else {
            clearStepInHeader('body');
          }
          // Check price if coming back from price to feel
          if ( isRadioSelected($('.mfinder__question[data-step-name="price"]:eq(0):visible')) ) {
            setStepInHeader('price');
          }
          else {
            clearStepInHeader('price');
          }
        }
        else if (activeStepName === 'price') {
          if ( isRadioSelected($('.mfinder__question[data-step-name="feel"]:eq(0):visible')) ) {
            setStepInHeader('feel');
          }
          else {
            clearStepInHeader('feel');
          }
        }
      }
      else if ($mfinderQuestions.attr('data-finder-path') === 'partner_and_me') {

        if (activeStepName === 'symptoms') {
          if ($('.mfinder__question[data-step-name="position"]:eq(0)').find('.radio-input').hasClass('selected') && $('.mfinder__question[data-step-name="position"]:eq(1)').find('.radio-input').hasClass('selected')) {
            setStepInHeader('position');
          }
          else {
            clearStepInHeader('position');
          }
        }
        else if (activeStepName === 'features') {
          if (isCheckboxListChecked($('.mfinder__question[data-step-name="symptoms"]:eq(0)')) && isCheckboxListChecked($('.mfinder__question[data-step-name="symptoms"]:eq(1)'))) {
            setStepInHeader('symptoms');
          }
          else {
            clearStepInHeader('symptoms');
          }
        }
        else if (activeStepName === 'body') {
          setStepInHeader('features');
        }
        else if (activeStepName === 'feel') {
          if ( isRadioSelected($('.mfinder__question[data-step-name="body"]:eq(2):visible')) && isRadioSelected($('.mfinder__question[data-step-name="body"]:eq(3):visible')) ) {
            setStepInHeader('body');
          }
          else {
            clearStepInHeader('body');
          }
          // Check price if coming back from price to feel
          if ( isRadioSelected($('.mfinder__question[data-step-name="price"]:eq(0):visible')) ) {
            setStepInHeader('price');
          }
          else {
            clearStepInHeader('price');
          }
        }
        else if (activeStepName === 'price') {
          if ( isRadioSelected($('.mfinder__question[data-step-name="feel"]:eq(0):visible')) && isRadioSelected($('.mfinder__question[data-step-name="feel"]:eq(1):visible')) ) {
            setStepInHeader('feel');
          }
          else {
            clearStepInHeader('feel');
          }
        }

      }

    }

    // Automatically set initial 'who' step as active.
    function setInitialActiveItem() {
      $('.mfinder__question').eq(currentSlide).addClass('active-item');
    }

    function updateMobileDots() {
      var $dot = $('.mfinder__mobile-step-dot'),
          lastSlideNumber = $('.mfinder__question:visible').length -1;

      if (currentSlide === 0) {
        $dot.removeClass('active-dot');
        $dot.eq(0).addClass('active-dot');
      }
      else if (currentSlide === 1) {
        $dot.removeClass('active-dot');
        $dot.eq(1).addClass('active-dot');
      }
      else if (currentSlide === 2) {
        $dot.removeClass('active-dot');
        $dot.eq(2).addClass('active-dot');
      }
      else if (currentSlide >= 3 && currentSlide < lastSlideNumber) {
        $dot.removeClass('active-dot');
        $dot.eq(3).addClass('active-dot');
      }
      else if (currentSlide === lastSlideNumber) {
        $dot.removeClass('active-dot');
        $dot.eq(4).addClass('active-dot');
      }

      // first dot
      if (currentSlide >= 4) {
        $dot.eq(0).addClass('small-dot');
      }
      else if (currentSlide === 0) {
        $dot.eq(0).removeClass('small-dot');
      }

      // last dot
      if (currentSlide < lastSlideNumber) {
        $dot.eq(4).addClass('small-dot');
      }
      else if (currentSlide === lastSlideNumber) {
        $dot.eq(4).removeClass('small-dot');
      }
    }

    function setMobileStepPosition() {
      if ($(window).width() <= 767) {
        $('.mfinder__steps-container').addClass('step-mobile-view');
      }
      else {
        $('.mfinder__steps-container').removeClass('step-mobile-view');
      }
    }

    // Sets a visual indicator (a bar) for the current step atop the finder.
    function setInitialActiveStep() {
      $('.mfinder__stepindicator').eq(currentSlide).addClass('active-step');
      setInitialActiveStepName();
    }

    // Set the initial active step name atop the finder.
    function setInitialActiveStepName() {
      $('.mfinder__stepindicator').eq(currentSlide).parents('.mfinder__step').addClass('active-step-name');
    }

    function showLoadingOverlay() {
      if (!$('.mfinder__loading').length) {
        $('<div class="mfinder__loading">').appendTo('.mfinder');
      }
      else {
        $('.mfinder__loading').show();
      }
    }

    function hideLoadingOverlay() {
      $('.mfinder__loading').hide();
    }

    function getMobileStepStatus() {
      var pathName = $('.mfinder__questions').data('finder-path'),
          questionStepName = $('.mfinder__questions').find('div.active-item').data('step-name'),
          questionSubStepNum = $('.mfinder__questions').find('div.active-item').data('step-indicator');
    }

    function setTransform() {
      $mfinderQuestions.css({'transform': 'translate3d(' + (-pos * $mfinderQuestions.width()) + 'px,0,0)'}).promise().done(function(){
        $mfinderQuestions.addClass('transitioning');
        showLoadingOverlay();
      });

      function doneTransitioning() {
        var setTransformTimer;
        function endSetTransformTimer() {
          window.clearTimeout(setTransformTimer);
          setTransformTimer = window.setTimeout(function () {
            $mfinderQuestions.removeClass('transitioning');
            hideLoadingOverlay();
          }, 1000);
        }
        endSetTransformTimer();
      }

      doneTransitioning();
    }

    // When ascending questions in mobile, logic to increment steps.
    function incrementMobileSteps() {
      var $nextActiveQuestion = $mfinderQuestions.find('.mfinder__question.active-item').nextAll('.mfinder__question:visible'),
          nextQuestionStepName = $nextActiveQuestion.data('step-name'),
          nextQuestionSubStepNum = $nextActiveQuestion.data('step-indicator');

      if (nextQuestionStepName === 'symptoms') {
        setMobileStepTransform(2);
      }
      else if (nextQuestionStepName === 'feel') {
        setMobileStepTransform(4);
      }

      currentMobileStep++;
    }

    // When descending questions in mobile, logic to decrement steps.
    function decrementMobileSteps() {
      var $prevActiveQuestion = $mfinderQuestions.find('.mfinder__question.active-item').prevAll('.mfinder__question:visible'),
          prevQuestionStepName = $prevActiveQuestion.data('step-name'),
          prevQuestionSubStepNum = $prevActiveQuestion.data('step-indicator');

      if (prevQuestionStepName === 'features' && prevQuestionSubStepNum === 2) {
        setMobileStepTransform(2);
      }
      else if (prevQuestionStepName === 'position') {
        setMobileStepTransform(0);
      }

      currentMobileStep--;
    }

    function setMobileStepTransform(mobileStepPos) {
      if ($('.step-mobile-view').length > 0) {
        $('.mfinder__steps').css({'transform': 'translate3d(' + (-mobileStepPos * $('.mfinder__step').width()) + 'px,0,0)'});
      }
    }

    function lastMobileStepTransform() {
      $('.mfinder__steps').css({'transform': 'translate3d(' + (-4 * $('.mfinder__step').width()) + 'px,0,0)'});
    }

    function onResizeSetMobileStepTransform() {
      var activeStep = ($('.active-step-name').index() !== -1) ? $('.active-step-name').index() : 0;

      if (activeStep === 0 || activeStep === 1) {
        $('.mfinder__steps').css({'transform': 'translate3d(0px,0,0)'});
      }
      else if (activeStep === 2) {
        $('.mfinder__steps').css({'transform': 'translate3d(-' + parseInt((($(window).width() - 1) / 3) * 2, 10) + 'px,0,0)'});
      }
      else if (activeStep >= 3 ||  activeStep <= 5) {
        $('.mfinder__steps').css({'transform': 'translate3d(-' + parseInt((($(window).width() - 1) / 3) * 3, 10) + 'px,0,0)'});
      }
      else if (activeStep === 6) {
        $('.mfinder__steps').css({'transform': 'translate3d(' + (-4 * $('.mfinder__step').width()) + 'px,0,0)'});
      }
    }

    function resetStepsForDesktop() {
      if ($(window).width() >= 768) {
        $('.mfinder__steps').css({'transform': 'translate3d(0px,0,0)'});
      }
    }

    function toggleIsSet() {
      $mfinderQuestions.removeClass('is-set');
      return setTimeout(function () {
        return $mfinderQuestions.addClass('is-set');
      });
    }
    function onClickLastBtn() {

    	$lastBtn.on('click', function(e) {
    		e.preventDefault();
        if ($mfinderQuestions.hasClass('transitioning')) {
          return false;
        }
    		$(this).parent().parent().find("form").submit();

    	});
    }

    function onChangeOfNoneCheckbox() {
      if ($('.mfinder__symptoms-checkbox-list').is(':visible')) {
        $('.symptoms-none').on('change', function(e) {
          $(e.target).closest('.mfinder__symptoms-checkbox-list').find('input.mfinder__checkbox-input').not(this).prop('checked', false);
        });

        $('input.mfinder__checkbox-input', '.mfinder__symptoms-checkbox-list').not('.symptoms-none').on('change', function(e) {
            $(e.target).closest('.mfinder__symptoms-checkbox-list').find('.symptoms-none').prop('checked', false);
        });
      }
    }

    function getWeightResult() {

      function getHeightData(el, el2) {
        var heightVal = $(el).find(".mfinder__height-input--feet").val() + "/" + $(el).find(".mfinder__height-input--inches").val();

        $.ajax({
          url: Urls.getWeightByHeight,
          data: {selectedHeight: heightVal},
          type: 'get',
          success: function (data) {
            var weight = JSON.parse(data);
            $(el2).find(".weight-1 label").text("Less than " + weight.min + " LBS") ;
            $(el2).find(".weight-2 label").text(weight.min + " - " + weight.max + " LBS") ;
            $(el2).find(".weight-3 label").text(weight.max + " LBS OR MORE") ;
          },
          error: function (errorThrown) {
            console.log(errorThrown);
          }
        });
      }

      function init() {
        // If myself/someone_else path, or partner and me.
        if ($('.mfinder__question-height:eq(0)').is(':visible')) {
          getHeightData($('#mfinder__height'), $('.mfinder__question-weight:eq(0)'));
        }

        // Only parnter and me.
        if ($('.mfinder__question-height:eq(1)').is(':visible')) {
          getHeightData($('#mfinder__height_partner'), $('.mfinder__question-weight:eq(1)'));
        }
      }

      init();
    }

    function onClickNextBtn() {
      $nextBtn.on('click', function(e){
        e.preventDefault();
        if ($mfinderQuestions.hasClass('transitioning')) {
          return false;
        }
        $mfinderQuestions.removeClass('is-reversing');
        pos = Math.min(pos + 1, slideCount - 1);
        setTransform();
        currentSlide++;
        if ($(window).width() <= 767) {
          incrementMobileSteps();
        }
        getWeightResult();
        setActiveItem();
        onActiveStepCheckIfSet();
        updateMobileDots();
        checkFirstStepSelected();
        firstStepEnableNextOnBtnClick();
        togglePrevBtn();
        toggleNextBtn();
        toggleIsSet();
      });
    }

    function onClickPrevBtn() {
      $prevBtn.on('click', function(e){
        e.preventDefault();
        if ($mfinderQuestions.hasClass('transitioning')) {
          return false;
        }
        $mfinderQuestions.addClass('is-reversing');
        pos = Math.max(pos - 1, 0);
        setTransform();
        currentSlide--;
        if ($(window).width() <= 767) {
          decrementMobileSteps();
        }
        setActiveItem();
        onActiveStepCheckIfSet();
        updateMobileDots();
        checkFirstStepSelected();
        firstStepEnableNextOnBtnClick();
        togglePrevBtn();
        toggleNextBtn();
        toggleIsSet();
      });
    }

    function invokeOnViewportChange() {
      setTransform();
      resetStepsForDesktop();
      finderExpandCollapse();
      if ($(window).width() >= 768 && ($('.mfinder__radio-slider').hasClass('active') === false)){
        radioButtonsToSlider();
      }
      else if ($(window).width() <= 767) {
        mobileRadiosGoToNext();
        onResizeSetMobileStepTransform();
      }
    }

    function updateOnResize() {
      // Debouncing greedy resize events.
      var onWindowResize = debounce(function() {
        invokeOnViewportChange();
      }, 250);

      $(window).on('resize', onWindowResize);
    }

    function updateOnOrientationChange() {
      if("onorientationchange" in window) {
        var onOrientationChange = debounce(function() {
          invokeOnViewportChange();
        }, 250);

        $(window).on('orientationchange', onOrientationChange);
      }
    }

    function setHeaderHeight() {
      $('.mfinder').css('padding-top', '152 px');
    }

    function init() {
      scrollToTop();
      setHeaderHeight();
      buttonListSetDefaultBtn();
      updateOnResize();
      updateOnOrientationChange();
      setInitialActiveItem();
      setInitialActiveStep();
      updateMobileDots();
      togglePrevBtn();
      checkFirstStepSelected();
      firstStepEnableNextOnBtnClick();
      onClickPrevBtn();
      onClickNextBtn();
      onClickLastBtn();
      removeNavButtonsFromTabIndex();
      radioButtonsToSlider();
      mobileRadiosGoToNext();
      finderExpandCollapse();
      heightSlider();
      onChangeOfNoneCheckbox();
      
      //MAT-1934 - Get src parameter from QueryString and skip the 1st question
      var src = getParameterValues('src');
      if(src!=null){
    	  var $mfinder_question_who = $('input:radio[name=mfinder_question_who]');
    	  if($mfinder_question_who.filter('[value=' + src +']').length>0) {
    		  $mfinder_question_who.filter('[value=' + src +']').attr('checked', true).trigger("click");
    	  }
      }
    }
    
    /***
     * it will return the value of QueryString parameter
     */
    function getParameterValues(param){
    	var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    	for (var i = 0; i < url.length; i++) {  
    	    var urlparam = url[i].split('=');  
    	    if (urlparam[0] == param) {  
    	        return urlparam[1];  
    	    }  
    	}
    }

    init();
  };

  var init = function () {
    finderWizard();
  };

  init();
});
