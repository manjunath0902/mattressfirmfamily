$(function () {
  var isMattressFinderVersion3 = typeof $('.finder-results.finder-results-v3') !== 'undefined' && $('finder-results finder-results-v3').length > 0 ? true : false;
  var debounce = function(func, wait, immediate) {
    var timeout;
    return function() {
      var context = this, args = arguments;
      var later = function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  };

  function enableResultsCarousel() {
    var $slickSlider = $('.related-recommendations'),
        settings = {
            dots: true,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: true,
            centerPadding: '4%',
        };

    function updateOnResize() {
      var onWindowResize = debounce(function() {
          if ($(window).width() >= 768){
            if ($slickSlider.hasClass('slick-initialized')) {
              $slickSlider.slick('unslick');
            }
          }
          else {
            if (!$slickSlider.hasClass('slick-initialized')) {
              return $slickSlider.slick(settings);
            }
          }
      }, 250);

      $(window).on('resize', onWindowResize);
    }


    $slickSlider.slick(settings);
    if ($(window).width() >= 768){
      if ($slickSlider.hasClass('slick-initialized')) {
        $slickSlider.slick('unslick');
      }
    }
    else {
      if (!$slickSlider.hasClass('slick-initialized')) {
        return $slickSlider.slick(settings);
      }
    }
    updateOnResize();
  }

  var scrollToTop = function() {
    var $window = $(window),
      headerTopNewHeight = $('.header-top-new > div').children(':visible').outerHeight(),
      headerBannerHeight = $('.header-banner').outerHeight(),
      headerMainHeight = $('.header-main').outerHeight(),
      combinedHeaderHeight;

    if ($window.width() < 1032) {
      $(window).scrollTop($('#finder').offset().top - headerTopNewHeight);
    }
    else {
      combinedHeaderHeight = headerTopNewHeight + headerBannerHeight + headerMainHeight;
      $(window).scrollTop($('#finder').offset().top - combinedHeaderHeight);
    }
    if ($window.width() < 1024 && $window.width() > 768) {
    	$(window).scrollTop($('#finder').offset().top - headerMainHeight);
    }
  };

  function invokeOnViewportChange() {
    //scrollToTop();
  }
  function bannerCheck() {

	  if ($(".pt_finder-result .header-countdown").length > 0 && $(window).width() < 768) {
      	  $(".bestmatch").css("padding-top" , "20px");
        }

  }
  function emailSlider() {
	  $('.email-result-copy').click(function() {
	      if($('.email-footer').hasClass('slide-up')) {
	        $('.email-footer').addClass('slide-down', 1000);
	        $('.email-footer').removeClass('slide-up'); 
	        $(this).find($(".fa")).removeClass('fa-angle-down').addClass('fa-angle-up');
	      } else {
	        $('.email-footer').removeClass('slide-down');
	        $('.email-footer').addClass('slide-up', 1000); 
	        $(this).find($(".fa")).removeClass('fa-angle-up').addClass('fa-angle-down');
	      }
	  });
  }
  function openMFEmailDesktopPopup() {
	  if ($('#ShowSignUpPopup').length && $('#ShowSignUpPopup').val() == 'true' && $('#isMobileDevice').length && $('#isMobileDevice').val() == 'false') {
			$( "#emaildialog" ).dialog({
			  autoOpen: true,
			  dialogClass: 'mattress-email-results-dialog',
			  width: 575,
			  modal: true,
			  resizable: false
			});   
			if ($(window).width() < 768) {
			    $("#emaildialog").dialog( "close" );
			}
	  }
  }

  function handleAltOptions() {
    var $altOptions = $('.finder-results__alt-options'),
      altOptionsExist = $altOptions.length;

    // If no groups shown, hide entire section
    function hideSectionIfNoOptions() {
      if ($('.finder-results__alt-option-group').is(':visible') == 0) {
        $('.finder-results__alt-options-section').hide();
      }
    }

    function toggleAltOptions() {
      var $altOptionGroup = $altOptions.find('.finder-results__alt-option-group');

      $altOptionGroup.each(function() {
        var $self = $(this);

        if ($self.find('.finder-results__alt-option').length == 0) {
          $self.hide();
        }
      });
      hideSectionIfNoOptions();
    }

    function setPageTitle() {
      document.title = "Mattress Finder";
    }

    function init() {
      setPageTitle();
      if (altOptionsExist) {
        toggleAltOptions();
      }
    }

    init();
  }

  // Expand and collapse try in store when store is found
  // Show hide overlay in mobile
  function initExpanderContainer() {
    $('.try-in-store__header').on('click', function () {

      var $header = $(this),
        //getting the next element
        $content = $header.next(),
        $container = $header.closest('.expander-container'),
        $overlay = $('.ui-widget-overlay'),
        overlayHtml = '<div class="ui-widget-overlay ui-front expander-overlay" style="z-index: 100000;"></div>';

      if (!$container.hasClass('expanded')) {
        $container.addClass('expanded');
      } else {
        $container.removeClass('expanded');
      }

      function onOverlayClickSlideUp() {
        $('.expander-overlay').on('click', function () {
          $header.click();
        });
      }

      function onResizeManageOverlay() {
        if ($container.hasClass('expanded')) {
          $container.removeClass('expanded');
          if ($('.expander-overlay').length) {
            $('.expander-overlay').click();
          }
        }
      }

      function callOnViewportChange() {
       // onResizeManageOverlay();
      }

      function changeOnResize() {
        // Debouncing greedy resize events.
        var onWindowResize = debounce(function () {
          callOnViewportChange();
        }, 250);

        $(window).on('resize', onWindowResize);
      }

      function changeOnOrientationChange() {
        if ("onorientationchange" in window) {
          var onOrientationChange = debounce(function () {
            callOnViewportChange();
          }, 250);

          $(window).on('orientationchange', onOrientationChange);
        }
      }

      changeOnResize();
      changeOnOrientationChange();

      // Only run if parent is expander-container
      if ($header.parents('.expander-container').length) {

        // Show overlay with expanded widget ontop if in mobile
        if ($overlay.length) {
          $overlay.detach();
          if ($container.hasClass('mobile-overlay')) {
            $container.removeClass('mobile-overlay')
          }
        } else {
          if ($(window).width() < 768) {
            $('body').append(overlayHtml);
            onOverlayClickSlideUp();
            if (!$container.hasClass('mobile-overlay')) {
              $container.addClass('mobile-overlay')
            }
          }
        }

        var ariaExp = ($container.hasClass('expanded')) ? 'true' : 'false';
        $header.find('a').attr('aria-expanded', ariaExp);
      }
    });
  }

  // function setActivePriceRanges() {
  // }

  function toggleEditPriceRange() {
    var $button = $('.edit-price-range__button'),
        $choices = $('.edit-price-range__choices');

    $button.on('click', function () {
      var $editPriceRange = $(this).closest('.edit-price-range');
      if ($choices.is(':hidden') ) {
        $choices.show();
        $editPriceRange.addClass('expanded');
      } else {
        $choices.hide();
        $editPriceRange.removeClass('expanded');
      }
    });
  }

  // On click of edit-price-range link add or remove ranges from query string
  function onClickEditPriceRange() {
    $('.edit-price-range__choice-link').on('click', function(e) {
      e.preventDefault();
      var $self = $(this),
        urlParams = new URLSearchParams(window.location.search),
        urlParamsArray = urlParams.getAll('mfinder_question_price'),
        arrayLength = urlParamsArray.length,
        existingPriceRanges = [],
        priceRangesPostDelete = [],
        myIndex = $self.parent().index() + 1,
        updatedPath;

      if (!$self.parent('li').hasClass('active-range')) {
        $self.parent('li').addClass('active-range');
      } else {
        $self.parent('li').removeClass('active-range');
      }

      // Get selected mfinder_question_price items from query string
      for (var i = 0; i < arrayLength; i++) {
        existingPriceRanges.push(parseInt(urlParamsArray[i],10));
      }
      
      if ($.inArray(myIndex, existingPriceRanges) > -1) {
        // Delete items from price range
        // Remove clicked item from array by value
        existingPriceRanges.splice($.inArray(myIndex, existingPriceRanges), 1);
        // Init a new array to manage selected items post delete
        priceRangesPostDelete = existingPriceRanges.slice();
        // Delete all mfinder_question_price from query string
        urlParams.delete('mfinder_question_price');
        
        // Append any selected items left to query string
        for (var j = 0; j < priceRangesPostDelete.length; j++) {
          urlParams.append('mfinder_question_price', priceRangesPostDelete[j]);
        }
        
        updatedPath = location.pathname + '?' + urlParams;     
        window.history.pushState({}, '', updatedPath);
        window.location.reload();

      } else {
        // Add items to price range
        urlParams.append('mfinder_question_price', myIndex);
        updatedPath = location.pathname + '?' + urlParams;
        window.history.pushState({}, '', updatedPath);
        window.location.reload();
      }     
    });
  }

  /* Populate edit-price-range with selected mfinder_question_price ranges
   * from query string.
   */
  function initEditPriceRange() {
    if (window.URLSearchParams) {
      var urlParams = new URLSearchParams(window.location.search),
          urlParamsArray = urlParams.getAll('mfinder_question_price'), 
          arrayLength = urlParamsArray.length;
    
      for (var i = 0; i < arrayLength; i++) {
        $('.edit-price-range__choice_' + urlParamsArray[i]).addClass('active-range');
      }
      toggleEditPriceRange();
      onClickEditPriceRange();
    } else {
      $('.edit-price-range').hide();
    }
  }

  function detectCountDown() {
    if ($('.header-countdown-wrapper').length) {
      $('.finder-search-results').addClass('has-countdown');
    }
  }

  function init() {
    detectCountDown();
    scrollToTop();
    enableResultsCarousel();
    handleAltOptions();
    bannerCheck();
    initExpanderContainer();
    initEditPriceRange();
    emailSlider();
    openMFEmailDesktopPopup();
  }

  init();
});
