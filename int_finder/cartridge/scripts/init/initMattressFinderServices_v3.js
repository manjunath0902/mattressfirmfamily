/**
 * Initialize HTTP services for a cartridge
 */
var Site = require('dw/system/Site');
var Service = require('dw/svc/Service');
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var TIMEOUT_MSG = "Read timed out";
var TIMEOUT_ERROR = "Service Timed Out...";

/** Mattress Finder Authentication Service */
var mattressFinderAuthAPi = LocalServiceRegistry.createService("mattress.finder.authenticate", {
    createRequest: function(service , args) {
		service.setRequestMethod("GET");
		service.addHeader("Content-Type", "application/json") ;
		if(!empty(args.apiKey)) {
			service.addHeader("api-key", args.apiKey) ;
		}
    },
    parseResponse : function(svc, listOutput) {
        return listOutput.text;
    },
    getRequestLogMessage : function(requestObj) {
       try {
           var jsonString = JSON.stringify(requestObj);
           return jsonString;
       } catch(e) {}
       return requestObj;
   },
   getResponseLogMessage : function(responseObj) {
       if (responseObj instanceof dw.net.HTTPClient) {
           try {
               return responseObj.text;
           } catch(e) {}
       }
       return responseObj;
   }
});

/** Mattress Finder Products Search Service */
var mattressFinderRecommendAPi = LocalServiceRegistry.createService("mattress.finder.recommendProducts", {
    createRequest: function(service , args) {
		service.setRequestMethod("GET");
		service.addHeader("Content-Type", "application/json") ;
		if(!empty(args.token)) {
			service.addHeader("Authorization", "Bearer "+args.token) ;
		}
		if(!empty(args.queryParameters))
		{
			service.addParam("storecode", args.queryParameters.StoreCode);
			service.addParam("budgetvalue", args.queryParameters.BudgetValue);
			service.addParam("Support", args.queryParameters.Support);
			service.addParam("PressureRelief", args.queryParameters.PressureRelief);
			service.addParam("Comfort", args.queryParameters.Comfort);
			service.addParam("MotionSeparation", args.queryParameters.MotionSeperation);
			service.addParam("SleepsCool", args.queryParameters.SleepsCool);
			service.addParam("Craftsmanship", args.queryParameters.Craftsmanship);
			service.addParam("warehouseId", args.queryParameters.warehouseId);
			service.addParam("dwsessionid", args.queryParameters.dwSessionId);
			service.addParam("MattressSize", args.queryParameters.mattressSize);
			service.addParam("GoogleClientId", args.queryParameters.googleClientId);
			service.addParam("StoreZipCode", args.queryParameters.storeZipCode);
			service.addParam("CustomerZipCode", args.queryParameters.customerZipCode);
			service.addParam("CustomerResponses", JSON.stringify(args.queryParameters.customerResponses));
		}
    },
    parseResponse : function(svc, listOutput) {
        return listOutput.text;
    },
    getRequestLogMessage : function(requestObj) {
       try {
           var jsonString = JSON.stringify(requestObj);
           return jsonString;
       } catch(e) {}
       return requestObj;
   },
   getResponseLogMessage : function(responseObj) {
       if (responseObj instanceof dw.net.HTTPClient) {
           try {
               return responseObj.text;
           } catch(e) {}
       }
       return responseObj;
   }
});

/** Save Profile Service Back to BI Rule Engine via Mattress Finder */
var mattressFinderCustomerProfileAPi = LocalServiceRegistry.createService("mattress.finder.saveCustomerProfile", {
    createRequest: function(service, args) {
        service.addHeader("Content-Type", "application/json");
        service.setRequestMethod("POST");
        if(!empty(args.token)) {
           service.addHeader("Authorization", "Bearer "+args.token);
       }
       return args.queryParameters;
    },
    parseResponse : function(svc, listOutput) {
        return listOutput.text;
    },
    getRequestLogMessage : function(requestObj) {
       try {
           return requestObj;
       } catch(e) {}
       return requestObj;
   },
   getResponseLogMessage : function(responseObj) {
       if (responseObj instanceof dw.net.HTTPClient) {
           try {
               return responseObj.text;
           } catch(e) {}
       }
       return responseObj;
   },
   filterLogMessage: function(msg) {
	   if(msg === TIMEOUT_MSG) {
			return TIMEOUT_ERROR;
	   }else {
			return msg;
	   }
	}
});

/** Salesforce Contact Upsert API Used for Sending Mattress Finder Data to SFDC */
var salesforceContatctAPi = LocalServiceRegistry.createService("salesforce.contact.update", {
     createRequest: function(service, args) {
     	service.addHeader("Content-Type", "application/json");
     	var baseURL = service.getConfiguration().getCredential().getURL();
		if(args) {
			if(!empty(args.token)) {
		     	service.setRequestMethod("GET");
	     		service.addHeader("Authorization", "Bearer "+args.token);
	     		if(!empty(args.email)) {
	     			var URL = baseURL  + "query?q=SELECT+id,email+from+contact+where+email='" + args.email + "'";
	     			service.setURL(URL);
	     		}
	     	}
			else if(!empty(args.payload)) {
     			var requestBody = JSON.stringify(args.payload);
     			service.addHeader("Content-Type", "application/json");
     			service.setRequestMethod("POST");
     			var URL = baseURL + "sobjects/Contact";//baseURL  + "sobjects/Contact";
     			service.setURL(URL);
     			return requestBody;
     		}
		}
		return;
     },
     parseResponse : function(svc, listOutput) {
         return listOutput;
     },
     getRequestLogMessage : function(requestObj) {
        try {
            var jsonString = JSON.stringify(requestObj);
            return jsonString;
        } catch(e) {}
        return requestObj;
    },
    getResponseLogMessage : function(responseObj) {
        if (responseObj instanceof dw.net.HTTPClient) {
            try {
                return responseObj.text;
            } catch(e) {}
        }
        return responseObj;
    },
    filterLogMessage: function(msg) {
 	   if(msg === TIMEOUT_MSG) {
 			return TIMEOUT_ERROR;
 	   } else {
 			return msg;
 	   }
 	}
});

/** Salesforce Contact Upsert API Used for Getting token Related to SFDC Integration */
var salesforceContactAuthAPi = LocalServiceRegistry.createService("salesforce.contact.token", {
    createRequest: function(service, args) {
    	service.addHeader("Content-Type", "application/json");
		service.setRequestMethod("POST");

		var clientID = Site.current.getCustomPreferenceValue("sfContactConsumerKey");
		var clientSecret = Site.current.getCustomPreferenceValue("sfContactSecretKey");
		var username = service.getConfiguration().getCredential().getUser();
		var password = service.getConfiguration().getCredential().getPassword();

		service.addParam("grant_type", "password");
		service.addParam("client_id", clientID);
		service.addParam("client_secret",clientSecret);
		service.addParam("username",username);
		service.addParam("password",password);

		return;
    },
    parseResponse : function(svc, listOutput) {
        return listOutput;
    },
    getRequestLogMessage : function(requestObj) {
       try {
           var jsonString = JSON.stringify(requestObj);
           return jsonString;
       } catch(e) {}
       return requestObj;
   },
   getResponseLogMessage : function(responseObj) {
       if (responseObj instanceof dw.net.HTTPClient) {
           try {
               return responseObj.text;
           } catch(e) {}
       }
       return responseObj;
   },
   filterLogMessage: function(msg) {
	   if(msg === TIMEOUT_MSG) {
			return TIMEOUT_ERROR;
	   } else {
			return msg;
	   }
	}
});

/** Global Exports */

/** Mattress Finder Authentication Service Used for Security */
exports.MattressFinderAuthAPi = mattressFinderAuthAPi;

/** Mattress Finder Search API Returns Product Result Set Against Quize */
exports.MattressFinderRecommendAPi = mattressFinderRecommendAPi;

/** Save Profile Service Back to BI Rule Engine via Mattress Finder */
exports.MattressFinderCustomerProfileAPi = mattressFinderCustomerProfileAPi;

/** Salesforce Contact Upsert API Used for Getting token Related to SFDC Integration */
exports.SalesforceContactAuthAPi = salesforceContactAuthAPi;

/** Salesforce Contact Upsert API Used for Sendind Mattress Finder Data to SFDC */
exports.SalesforceContactAPi = salesforceContatctAPi;