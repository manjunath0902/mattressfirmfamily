/**
 * Initialize HTTP services for a cartridge
 */
var Site = require('dw/system/Site');
var Service = require('dw/svc/Service');
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var TIMEOUT_MSG = "Read timed out";
var TIMEOUT_ERROR = "Service Timed Out...";
var dwLogger = require("dw/system/Logger");

/** Mattress Finder Authentication Service */
var mattressFinderAuthAPi = LocalServiceRegistry.createService("mattress.finder.authenticate", {
    createRequest: function(service , args) {
		service.setRequestMethod("GET");
		service.addHeader("Content-Type", "application/json") ;
		if(!empty(args.apiKey)) {
			service.addHeader("api-key", args.apiKey) ;
		}
    },
    parseResponse : function(svc, listOutput) {
        return listOutput.text;
    },
    getRequestLogMessage : function(requestObj) {
       try {
           var jsonString = JSON.stringify(requestObj);
           return jsonString;
       } catch(e) {}
       return requestObj;
   },
   getResponseLogMessage : function(responseObj) {
       if (responseObj instanceof dw.net.HTTPClient) {
           try {
               return responseObj.text;
           } catch(e) {}
       }
       return responseObj;
   }
});

/** Mattress Finder Products Search Service */
var mattressFinderRecommendAPiV4 = LocalServiceRegistry.createService("mattress.finder.recommendProducts", {
    createRequest: function(service , args) {
    	var client_id = Site.current.getCustomPreferenceValue("mm_client_id");
    	var client_secret = Site.current.getCustomPreferenceValue("mm_client_secret");
		service.setRequestMethod("POST");
		service.addHeader("Content-Type", "application/json") ;
		service.addHeader("Accept", "application/json") ;
		service.addHeader("client_id", client_id) ;
		service.addHeader("client_secret", client_secret) ;
		
		var request={};
		if(!empty(args) && !empty(args.rawValue)){
			var parsedJsonRequest = JSON.parse(args.rawValue);
			request = {
			    "storecode": parsedJsonRequest.storecode,
			    "storezipcode": parsedJsonRequest.storezipcode,
			    "customerzipcode": parsedJsonRequest.customerzipcode,
			    "warehouseid": parsedJsonRequest.warehouseid,
			    "tealiumVisitorID": parsedJsonRequest.tealiumVisitorID,
			    "googleClientId": parsedJsonRequest.googleClientId,
			    "tealiumSessionID": parsedJsonRequest.tealiumSessionID,
			    "budgetValue": '$|$$|$$$|$$$$',
			    "mattressSize": parsedJsonRequest.mattressSize,
			    "support": parsedJsonRequest.support,
			    "pressureRelief": parsedJsonRequest.pressureRelief,
			    "comfort": parsedJsonRequest.comfort,
			    "motionSeparation": parsedJsonRequest.motionSeparation,
			    "sleepsCool": parsedJsonRequest.sleepsCool,
			    "customerResponses":JSON.stringify(parsedJsonRequest.customerResponses),
			    "partnerResponses":JSON.stringify(parsedJsonRequest.partnerResponses)
			   }
		}
		
	   return JSON.stringify(request);
    },
    parseResponse : function(svc, listOutput) {
        return listOutput.text;
    },
    mockCall : function(svc, params) {
    	return {
			statusCode: 200,
			statusMessage: "Success",
			text: JSON.stringify({
	        	  "message": "",
	        	  "status": true,
	        	  "data": {
	        	    "recommendedProduct": [
	        	      {
	        	        "percentageMatch": 85,
	        	        "productCatalogId": 13800,
	        	        "productDetailPage": "https://www.mattressfirm.com/sleepys/basic-8.25-inch-firm-innerspring-mattress/mfi126879.html",
	        	        "productName": "Basic 8.25\" Firm Innerspring Mattress",
	        	        "bvAvgRating": 3.962138085,
	        	        "bvRatingRange": 5,
	        	        "bvReviewCount": 492.0,
	        	        "isAvailableToTryInStore": "no",
	        	        "tryInStoreName": "Monaca",
	        	        "isParcelable": "0",
	        	        "shippingInformation": "Core",
	        	        "adjustableBaseSuitable": "0",
	        	        "mattressConstruction": "Innerspring,Foam",
	        	        "mattressHeight": "8.2500000000000000",
	        	        "manufacturerName": "Sleepy's",
	        	        "collectionName": "Basic",
	        	        "sleepPosition": "Back and Stomach",
	        	        "queenOLPPrice": 199.9900,
	        	        "twinOLPPrice": 169.9900,
	        	        "twinXLOLPPrice": 0.0000,
	        	        "fullOLPPrice": 199.9900,
	        	        "kingOLPPrice": 249.9900,
	        	        "calKingOLPPrice": 0.0000,
	        	        "queenSalesPrice": 0.0000,
	        	        "twinSalesPrice": 0.0000,
	        	        "fullSalesPrice": 0.0000,
	        	        "kingSalesPrice": 0.0000,
	        	        "calKingSalesPrice": 0.0000,
	        	        "twinXLSalesPrice": 0.0000,
	        	        "rank": 1,
	        	        "sku": "126879",
	        	        "mattributes": {
	        	          "comfort": "Firm",
	        	          "craftsmanship": "None",
	        	          "motionSeparation": "None",
	        	          "pressureRelief": "Standard",
	        	          "sleepsCool": "None",
	        	          "support": "Standard",
	        	          "pricingTier": "$"
	        	        }
	        	      },
	        	      {
	        	        "percentageMatch": 73.75,
	        	        "productCatalogId": 13786,
	        	        "productDetailPage": "https://www.mattressfirm.com/serta/perfect-sleeper-charlotte-11.5-inch-medium-plush-euro-top-mattress/mfi133945.html",
	        	        "productName": "Perfect Sleeper Charlotte 11.5\" Medium Plush Euro Top Mattress",
	        	        "bvAvgRating": 3.963636364,
	        	        "bvRatingRange": 5,
	        	        "bvReviewCount": 127.0,
	        	        "isAvailableToTryInStore": "no",
	        	        "tryInStoreName": "Monaca",
	        	        "isParcelable": "0",
	        	        "shippingInformation": "Core",
	        	        "adjustableBaseSuitable": "1",
	        	        "mattressConstruction": "Pocketed Coil,Innerspring,Encased Coil,Foam,Memory Foam,Gel Memory Foam",
	        	        "mattressHeight": "11.5000000000000000",
	        	        "manufacturerName": "Serta",
	        	        "collectionName": "Perfect Sleeper",
	        	        "sleepPosition": "All",
	        	        "queenOLPPrice": 699.9900,
	        	        "twinOLPPrice": 499.9900,
	        	        "twinXLOLPPrice": 0.0000,
	        	        "fullOLPPrice": 679.9900,
	        	        "kingOLPPrice": 899.9900,
	        	        "calKingOLPPrice": 0.0000,
	        	        "queenSalesPrice": 0.0000,
	        	        "twinSalesPrice": 0.0000,
	        	        "fullSalesPrice": 0.0000,
	        	        "kingSalesPrice": 0.0000,
	        	        "calKingSalesPrice": 0.0000,
	        	        "twinXLSalesPrice": 0.0000,
	        	        "rank": 2,
	        	        "sku": "133945",
	        	        "mattributes": {
	        	          "comfort": "Medium",
	        	          "craftsmanship": "None",
	        	          "motionSeparation": "Good",
	        	          "pressureRelief": "Enhanced",
	        	          "sleepsCool": "Good",
	        	          "support": "Enhanced",
	        	          "pricingTier": "$"
	        	        }
	        	      },
	        	      {
	        	        "percentageMatch": 87.5,
	        	        "productCatalogId": 13802,
	        	        "productDetailPage": "https://www.mattressfirm.com/sleepys/relax-10.5-inch-pillow-top-innerspring-mattress/mfi126882.html",
	        	        "productName": "Relax 10.5\" Pillow Top Innerspring Mattress",
	        	        "bvAvgRating": 3.981958763,
	        	        "bvRatingRange": 5,
	        	        "bvReviewCount": 241.0,
	        	        "isAvailableToTryInStore": "no",
	        	        "tryInStoreName": "Monaca",
	        	        "isParcelable": "0",
	        	        "shippingInformation": "Core",
	        	        "adjustableBaseSuitable": "0",
	        	        "mattressConstruction": "Pillow Top,Innerspring,Foam",
	        	        "mattressHeight": "10.5000000000000000",
	        	        "manufacturerName": "Sleepy's",
	        	        "collectionName": "Basic",
	        	        "sleepPosition": "Side",
	        	        "queenOLPPrice": 549.9900,
	        	        "twinOLPPrice": 449.9900,
	        	        "twinXLOLPPrice": 0.0000,
	        	        "fullOLPPrice": 529.9900,
	        	        "kingOLPPrice": 649.9900,
	        	        "calKingOLPPrice": 0.0000,
	        	        "queenSalesPrice": 499.9900,
	        	        "twinSalesPrice": 399.9900,
	        	        "fullSalesPrice": 479.9900,
	        	        "kingSalesPrice": 599.9900,
	        	        "calKingSalesPrice": 0.0000,
	        	        "twinXLSalesPrice": 0.0000,
	        	        "rank": 3,
	        	        "sku": "126882",
	        	        "mattributes": {
	        	          "comfort": "Soft",
	        	          "craftsmanship": "None",
	        	          "motionSeparation": "None",
	        	          "pressureRelief": "Maximum",
	        	          "sleepsCool": "None",
	        	          "support": "Standard",
	        	          "pricingTier": "$"
	        	        }
	        	      },
	        	      {
	        	        "percentageMatch": 75,
	        	        "productCatalogId": 13766,
	        	        "productDetailPage": "https://www.mattressfirm.com/beautyrest/br800-13.5-inch-plush-pillow-top-mattress/mfi134249.html",
	        	        "productName": "BR800 13.5\" Plush Pillow Top Mattress",
	        	        "bvAvgRating": 3.673469388,
	        	        "bvRatingRange": 5,
	        	        "bvReviewCount": 32.0,
	        	        "isAvailableToTryInStore": "no",
	        	        "tryInStoreName": "Monaca",
	        	        "isParcelable": "0",
	        	        "shippingInformation": "Core",
	        	        "adjustableBaseSuitable": "1",
	        	        "mattressConstruction": "Pillow Top,Pocketed Coil,Innerspring,Encased Coil,Foam,Memory Foam,Gel Memory Foam",
	        	        "mattressHeight": "13.5000000000000000",
	        	        "manufacturerName": "Beautyrest",
	        	        "collectionName": "Beautyrest",
	        	        "sleepPosition": "Side",
	        	        "queenOLPPrice": 799.9900,
	        	        "twinOLPPrice": 649.9900,
	        	        "twinXLOLPPrice": 0.0000,
	        	        "fullOLPPrice": 729.9900,
	        	        "kingOLPPrice": 1099.9900,
	        	        "calKingOLPPrice": 0.0000,
	        	        "queenSalesPrice": 599.9900,
	        	        "twinSalesPrice": 487.4900,
	        	        "fullSalesPrice": 547.4900,
	        	        "kingSalesPrice": 824.9900,
	        	        "calKingSalesPrice": 0.0000,
	        	        "twinXLSalesPrice": 0.0000,
	        	        "rank": 30,
	        	        "sku": "134249",
	        	        "mattributes": {
	        	          "comfort": "Soft",
	        	          "craftsmanship": "None",
	        	          "motionSeparation": "Good",
	        	          "pressureRelief": "Maximum",
	        	          "sleepsCool": "Good",
	        	          "support": "Enhanced",
	        	          "pricingTier": "$"
	        	        }
	        	      },
	        	      {
	        	        "percentageMatch": 72.5,
	        	        "productCatalogId": 13758,
	        	        "productDetailPage": "https://www.mattressfirm.com/beautyrest/silver-brs900-c-13.75-inch-extra-firm-mattress/mfi134254.html",
	        	        "productName": "Silver BRS900-C 13.75\" Extra Firm Mattress",
	        	        "bvAvgRating": 3.655737705,
	        	        "bvRatingRange": 5,
	        	        "bvReviewCount": 34.0,
	        	        "isAvailableToTryInStore": "no",
	        	        "tryInStoreName": "Monaca",
	        	        "isParcelable": "0",
	        	        "shippingInformation": "Core",
	        	        "adjustableBaseSuitable": "1",
	        	        "mattressConstruction": "Pocketed Coil,Innerspring,Encased Coil,Foam,Memory Foam,Gel Memory Foam",
	        	        "mattressHeight": "13.7500000000000000",
	        	        "manufacturerName": "Beautyrest",
	        	        "collectionName": "Silver",
	        	        "sleepPosition": "Back and Stomach",
	        	        "queenOLPPrice": 912.9900,
	        	        "twinOLPPrice": 843.9900,
	        	        "twinXLOLPPrice": 0.0000,
	        	        "fullOLPPrice": 898.9900,
	        	        "kingOLPPrice": 1117.9900,
	        	        "calKingOLPPrice": 0.0000,
	        	        "queenSalesPrice": 730.3900,
	        	        "twinSalesPrice": 675.1900,
	        	        "fullSalesPrice": 719.1900,
	        	        "kingSalesPrice": 894.3900,
	        	        "calKingSalesPrice": 0.0000,
	        	        "twinXLSalesPrice": 0.0000,
	        	        "rank": 31,
	        	        "sku": "134254",
	        	        "mattributes": {
	        	          "comfort": "Firm",
	        	          "craftsmanship": "None",
	        	          "motionSeparation": "Good",
	        	          "pressureRelief": "Standard",
	        	          "sleepsCool": "Good",
	        	          "support": "Enhanced",
	        	          "pricingTier": "$$"
	        	        }
	        	      },
	        	      {
	        	        "percentageMatch": 72.5,
	        	        "productCatalogId": 13792,
	        	        "productDetailPage": "https://www.mattressfirm.com/serta/perfect-sleeper-select-kleinmon-ii-10.5-inch-firm-mattress/mfi134242.html",
	        	        "productName": "Perfect Sleeper Select Kleinmon II 10.5\" Firm Mattress",
	        	        "bvAvgRating": 4.444444444,
	        	        "bvRatingRange": 5,
	        	        "bvReviewCount": 12.0,
	        	        "isAvailableToTryInStore": "no",
	        	        "tryInStoreName": "Monaca",
	        	        "isParcelable": "0",
	        	        "shippingInformation": "Core",
	        	        "adjustableBaseSuitable": "1",
	        	        "mattressConstruction": "Pocketed Coil,Innerspring,Encased Coil,Foam,Memory Foam,Gel Memory Foam",
	        	        "mattressHeight": "10.5000000000000000",
	        	        "manufacturerName": "Serta",
	        	        "collectionName": "Perfect Sleeper",
	        	        "sleepPosition": "Back and Stomach",
	        	        "queenOLPPrice": 699.9900,
	        	        "twinOLPPrice": 599.9900,
	        	        "twinXLOLPPrice": 0.0000,
	        	        "fullOLPPrice": 679.9900,
	        	        "kingOLPPrice": 999.9900,
	        	        "calKingOLPPrice": 0.0000,
	        	        "queenSalesPrice": 489.9900,
	        	        "twinSalesPrice": 419.9900,
	        	        "fullSalesPrice": 475.9900,
	        	        "kingSalesPrice": 699.9900,
	        	        "calKingSalesPrice": 0.0000,
	        	        "twinXLSalesPrice": 0.0000,
	        	        "rank": 32,
	        	        "sku": "134242",
	        	        "mattributes": {
	        	          "comfort": "Firm",
	        	          "craftsmanship": "None",
	        	          "motionSeparation": "Good",
	        	          "pressureRelief": "Standard",
	        	          "sleepsCool": "Good",
	        	          "support": "Enhanced",
	        	          "pricingTier": "$"
	        	        }
	        	      }
	        	    ]
	        	  }
	        	})
		};

    },
    getRequestLogMessage : function(requestObj) {
       try {
           var jsonString = JSON.stringify(requestObj);
           return jsonString;
       } catch(e) {}
       return requestObj;
   },
   getResponseLogMessage : function(responseObj) {
       if (responseObj instanceof dw.net.HTTPClient) {
           try {
               return responseObj.text;
           } catch(e) {}
       }
       return responseObj;
   }
});

/** Save Profile Service Back to BI Rule Engine via Mattress Finder */
var mattressFinderCustomerProfileAPi = LocalServiceRegistry.createService("mattress.finder.saveCustomerProfile", {
    createRequest: function(service, args) {
    	var client_id = Site.current.getCustomPreferenceValue("mm_client_id");
    	var client_secret = Site.current.getCustomPreferenceValue("mm_client_secret");
		service.setRequestMethod("POST");
		service.addHeader("Content-Type", "application/json") ;
		service.addHeader("Accept", "application/json") ;
		service.addHeader("client_id", client_id) ;
		service.addHeader("client_secret", client_secret) ;
		return args;
    },
    parseResponse : function(svc, listOutput) {
        return listOutput.text;
    },
    getRequestLogMessage : function(requestObj) {
       try {
           return requestObj;
       } catch(e) {}
       return requestObj;
   },
   getResponseLogMessage : function(responseObj) {
       if (responseObj instanceof dw.net.HTTPClient) {
           try {
               return responseObj.text;
           } catch(e) {}
       }
       return responseObj;
   },
   filterLogMessage: function(msg) {
	   if(msg === TIMEOUT_MSG) {
			return TIMEOUT_ERROR;
	   }else {
			return msg;
	   }
	}
});

//getting the customer profile using email address to get the recommended products.
var mattressFinderGetCustomerProfileAPi = LocalServiceRegistry.createService("mattress.finder.getCustomerProfile", {
    createRequest: function(service, args) {
    	var client_id = Site.current.getCustomPreferenceValue("mm_client_id");
    	var client_secret = Site.current.getCustomPreferenceValue("mm_client_secret");
    	service.addHeader("Content-Type", "application/json");
    	service.addHeader("client_id", client_id);
		service.addHeader("client_secret", client_secret);
		service.setRequestMethod("GET");
		service.addParam("emailAddress", args.emailAddress);
        
		return ;
    },
    parseResponse : function(svc, listOutput) {
        return listOutput.text;
    },
    getRequestLogMessage : function(requestObj) {
       return requestObj;
    },
    getResponseLogMessage : function(responseObj) {
    	if (responseObj instanceof dw.net.HTTPClient) {
	       try {
	    	   return responseObj.text;
           } catch(e) {}
    	}
    	return responseObj;
   },
   filterLogMessage: function(msg) {
	   if(msg === TIMEOUT_MSG) {
			return TIMEOUT_ERROR;
	   }else {
			return msg;
	   }
	}
});

/** Salesforce Contact Upsert API Used for Sending Mattress Finder Data to SFDC */
var salesforceContatctAPi = LocalServiceRegistry.createService("salesforce.contact.update", {
     createRequest: function(service, args) {
     	service.addHeader("Content-Type", "application/json");
     	var baseURL = service.getConfiguration().getCredential().getURL();
		if(args) {
			if(!empty(args.token)) {
		     	service.setRequestMethod("GET");
	     		service.addHeader("Authorization", "Bearer "+args.token);
	     		if(!empty(args.email)) {
	     			var URL = baseURL  + "query?q=SELECT+id,email+from+contact+where+email='" + args.email + "'";
	     			service.setURL(URL);
	     		}
	     	}
			else if(!empty(args.payload)) {
     			var requestBody = JSON.stringify(args.payload);
     			service.addHeader("Content-Type", "application/json");
     			service.setRequestMethod("POST");
     			var URL = baseURL + "sobjects/Contact";//baseURL  + "sobjects/Contact";
     			service.setURL(URL);
     			return requestBody;
     		}
		}
		return;
     },
     parseResponse : function(svc, listOutput) {
         return listOutput;
     },
     getRequestLogMessage : function(requestObj) {
        try {
            var jsonString = JSON.stringify(requestObj);
            return jsonString;
        } catch(e) {}
        return requestObj;
    },
    getResponseLogMessage : function(responseObj) {
        if (responseObj instanceof dw.net.HTTPClient) {
            try {
                return responseObj.text;
            } catch(e) {}
        }
        return responseObj;
    },
    filterLogMessage: function(msg) {
 	   if(msg === TIMEOUT_MSG) {
 			return TIMEOUT_ERROR;
 	   } else {
 			return msg;
 	   }
 	}
});

/** Salesforce Contact Upsert API Used for Getting token Related to SFDC Integration */
var salesforceContactAuthAPi = LocalServiceRegistry.createService("salesforce.contact.token", {
    createRequest: function(service, args) {
    	service.addHeader("Content-Type", "application/json");
		service.setRequestMethod("POST");

		var clientID = Site.current.getCustomPreferenceValue("sfContactConsumerKey");
		var clientSecret = Site.current.getCustomPreferenceValue("sfContactSecretKey");
		var username = service.getConfiguration().getCredential().getUser();
		var password = service.getConfiguration().getCredential().getPassword();

		service.addParam("grant_type", "password");
		service.addParam("client_id", clientID);
		service.addParam("client_secret",clientSecret);
		service.addParam("username",username);
		service.addParam("password",password);

		return;
    },
    parseResponse : function(svc, listOutput) {
        return listOutput;
    },
    getRequestLogMessage : function(requestObj) {
       try {
           var jsonString = JSON.stringify(requestObj);
           return jsonString;
       } catch(e) {}
       return requestObj;
   },
   getResponseLogMessage : function(responseObj) {
       if (responseObj instanceof dw.net.HTTPClient) {
           try {
               return responseObj.text;
           } catch(e) {}
       }
       return responseObj;
   },
   filterLogMessage: function(msg) {
	   if(msg === TIMEOUT_MSG) {
			return TIMEOUT_ERROR;
	   } else {
			return msg;
	   }
	}
});

/** Global Exports */

/** Mattress Finder Authentication Service Used for Security */
exports.MattressFinderAuthAPi = mattressFinderAuthAPi;

/** Mattress Finder Search API Returns Product Result Set Against Quize */
exports.MattressFinderRecommendAPiV4 = mattressFinderRecommendAPiV4;

/** Save Profile Service Back to BI Rule Engine via Mattress Finder */
exports.MattressFinderCustomerProfileAPi = mattressFinderCustomerProfileAPi;

exports.MattressFinderGetCustomerProfileAPi = mattressFinderGetCustomerProfileAPi;

/** Salesforce Contact Upsert API Used for Getting token Related to SFDC Integration */
exports.SalesforceContactAuthAPi = salesforceContactAuthAPi;

/** Salesforce Contact Upsert API Used for Sendind Mattress Finder Data to SFDC */
exports.SalesforceContactAPi = salesforceContatctAPi;