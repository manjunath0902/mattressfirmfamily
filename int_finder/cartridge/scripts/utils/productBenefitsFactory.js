/**
*	Name: productBenefitsFactory
*/

var Resource = require('dw/web/Resource');

var __ProductBenefitsFactory : Object = {
	allProductBenefiits : [
		{
			id: 'comfort',
			name: Resource.msg('finder.productbenefit.comfort', 'finder', null),
			disruptors: []
		},
		{
			id: 'support',
			name: Resource.msg('finder.productbenefit.support', 'finder', null),
			disruptors: [
				{ 
					id : 'backPain',
					name : Resource.msg('finder.productbenefit.backpain', 'finder', null),
					description : Resource.msg('finder.productbenefit.support.backpain', 'finder', null)
				},
				{ 
					id : 'soreness',
					name : Resource.msg('finder.productbenefit.soreness', 'finder', null),
					description : Resource.msg('finder.productbenefit.support.soreness', 'finder', null)
				},
				{ 
					id : 'snoring',
					name : Resource.msg('finder.productbenefit.snoring', 'finder', null),
					description : Resource.msg('finder.productbenefit.support.snoring', 'finder', null)
				},
				{ 
					id : 'sleepApnea',
					name : Resource.msg('finder.productbenefit.sleepapnea', 'finder', null),
					description : Resource.msg('finder.productbenefit.support.sleepapnea', 'finder', null)
				}
			],
			defaultMessage : Resource.msg('finder.productbenefit.support.defaultmessage', 'finder', null)
		},
		{
			id: 'pressureRelief',
			name: Resource.msg('finder.productbenefit.pressurerelief', 'finder', null),
			disruptors: [ 
				{ 
					id : 'neckPain',
					name : Resource.msg('finder.productbenefit.neckpain', 'finder', null),
					description : Resource.msg('finder.productbenefit.pressurerelief.neckpain', 'finder', null)
				},
				{ 
					id : 'shoulderPain',
					name : Resource.msg('finder.productbenefit.shoulderpain', 'finder', null),
					description : Resource.msg('finder.productbenefit.pressurerelief.shoulderpain', 'finder', null)
				},
				{ 
					id : 'hipPain',
					name : Resource.msg('finder.productbenefit.hippain', 'finder', null),
					description : Resource.msg('finder.productbenefit.pressurerelief.hippain', 'finder', null)
				},
				{ 
					id : 'soreness',
					name : Resource.msg('finder.productbenefit.soreness', 'finder', null),
					description : Resource.msg('finder.productbenefit.pressurerelief.soreness', 'finder', null)
				},
				{ 
					id : 'tossAndTurn',
					name : Resource.msg('finder.productbenefit.tossandturn', 'finder', null),
					description : Resource.msg('finder.productbenefit.pressurerelief.tossandturn', 'finder', null)
				}
			],
			defaultMessage : Resource.msg('finder.productbenefit.pressurerelief.defaultmessage', 'finder', null)
		},
		{
			id: 'motionSeparation',
			name: Resource.msg('finder.productbenefit.motionseparation', 'finder', null),
			disruptors: [ 
				{ 
					id : 'tossAndTurn',
					name : Resource.msg('finder.productbenefit.tossandturn', 'finder', null),
					description : Resource.msg('finder.productbenefit.motionseparation.tossandturn', 'finder', null)
				}
			],
			defaultMessage : Resource.msg('finder.productbenefit.motionseparation.defaultmessage', 'finder', null)
		},
		{
			id: 'sleepsCool',
			name: Resource.msg('finder.productbenefit.cooling', 'finder', null),
			disruptors: [ 
				{ 
					id : 'sleepHot',
					name : Resource.msg('finder.productbenefit.runninghot', 'finder', null),
					description : Resource.msg('finder.productbenefit.cooling.runninghot', 'finder', null)
				}
			]
		},
		{
			id: 'adjustableBaseSuitable',
			name: Resource.msg('finder.productbenefit.adjustablebasesuitable', 'finder', null),
			disruptors: [
				{ 
					id : 'neckPain',
					name : Resource.msg('finder.productbenefit.neckpain', 'finder', null),
					description : Resource.msg('finder.productbenefit.adjustableBaseSuitable.neckpain', 'finder', null)
				},
				{ 
					id : 'backPain',
					name : Resource.msg('finder.productbenefit.backpain', 'finder', null),
					description : Resource.msg('finder.productbenefit.adjustableBaseSuitable.backpain', 'finder', null)
				},
				{ 
					id : 'snoring',
					name : Resource.msg('finder.productbenefit.snoring', 'finder', null),
					description : Resource.msg('finder.productbenefit.adjustableBaseSuitable.snoring', 'finder', null)
				},
				{ 
					id : 'sleepApnea',
					name : Resource.msg('finder.productbenefit.sleepapnea', 'finder', null),
					description : Resource.msg('finder.productbenefit.adjustableBaseSuitable.sleepapnea', 'finder', null)
				},
				{ 
					id : 'acidReflux',
					name : Resource.msg('finder.productbenefit.acidreflux', 'finder', null),
					description : Resource.msg('finder.productbenefit.adjustableBaseSuitable.acidreflux', 'finder', null)
				},
				{ 
					id : 'allergies',
					name : Resource.msg('finder.productbenefit.allergies', 'finder', null),
					description : Resource.msg('finder.productbenefit.adjustableBaseSuitable.allergies', 'finder', null)
				}
			]
		}
	]
}
// maybe just exports
module.exports=__ProductBenefitsFactory;