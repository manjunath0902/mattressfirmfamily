var util = require('./util');

var Results = [];        // Category and Answer Pairs
var Titles = [];        // List of the Question and Answer strings that go in the tooltip on the progress bar
var Refinements = [];    // List of Refinements on the category
var activeStep = 0;        // Counter
var preload = new Array();
var headingImages = new Array();
var dialog = require('./dialog');
var quickview = require('./quickview');
var tooltipoptions = {
    top: -100,
    left: -28,
    extraClass: "arrow-finder",
    showURL: true,
    delay: 250
};

function initializeCache () {
    $cache = {
        page: $('#finder-questions'),
        question: $('.finder-question-box'),
        images: $('img'),
        hintIcon: $('.hint'),
        hintBox: $('#hint-text'),
        hintBoxModal: $('#hint-text-modal'),
        header: $('#header-container'),
        form: $(".finder-submit"),
        emailDlg: $('#finder-modal'),
        mainWrapper: $('#wrapper.pt_finder-result #main.finder-main'),
        stepZero: $('#mattress-finder-heading'),
        startQuiz: $('#mattress-finder-heading').find('#start a'),
        finderWrapper: $('.finder-wrapper'),
        progressWrapper: $('#progress-wrapper'),
        progress: $('.progress-steps'),
    };
}
function QuickViewButtons() {
	$('.finder-quickview').on('click', function (e) {
		e.preventDefault();
		quickViewData = new Object();
		quickViewData = $(this).data('result');
		quickview.show({
			url: $(this).attr('href'),
			source: 'quickview',
			quickViewData: quickViewData
		});
	});
}

function calculateMarginTop(_image, step) {
    if (step.find('div').data('activestep') == "3") {
        var imgHeight = _image.height();
        var boxHeight =  step.find('.mattress-box').height();
        var imgMarginTop = (boxHeight - imgHeight)/2;

        return _image.css('margin-top', imgMarginTop);
    }
}

function calculateMarginLeft(_image, step) {
    var imgWidth = _image.width();
    var boxWidth =  step.find('.mattress-box').width();
    var imgMarginLeft = (boxWidth - imgWidth)/2;

    return _image.css('margin-left', imgMarginLeft);
}

function initSliderAnswers(step) {
    if (step.find('div').data('activestep') == "5") {
        step.find('li.answer').each(function() {
            step.find('.mattress-box').append($(this).find('.answer-desktop'));
            $(this).hide();
        });
        step.find('.mattress-box').find('.answer-desktop').each(function() {
            $(this).hide();
        });
    } else {
        step.find('li.answer').each(function() {
            step.find('.mattress-box').append($(this).find('img'));
            $(this).hide();
        });
        step.find('.mattress-box').find('img').each(function() {
            calculateMarginTop($(this), step);
            calculateMarginLeft($(this), step);
            $(this).hide();
        });
    }
}

function initMobileSliderAnswers(step) {
    if (step.find('div').data('activestep') == "5") {
        step.find('li.answer').each(function() {
            step.find('.mattress-box').append($(this).find('.answer-desktop'));
            $(this).hide();
        });
        step.find('.mattress-box').find('.answer-desktop').each(function() {
            $(this).hide();
        });
    } else {
        step.find('li.answer').each(function() {
            step.find('.mattress-box').append($(this).find('img'));
            //$(this).hide();
        });
        step.find('.mattress-box').find('img').each(function() {
            calculateMarginTop($(this), step);
            calculateMarginLeft($(this), step);
            $(this).hide();
        });
    }
}

function toggleNextButton(show) {
    if (show) {
        if (parseInt($(window).width()) < 767) {
            $('#no-issue-button-mobile button').show();
            //$('#finder-next-button-mobile a').hide();
        } else {
            $('#finder-next-button button').show();
            $('#no-issue-button-mobile button').show();
            $('#finder-next-button a').hide();
        }
    } else {
        if (parseInt($(window).width()) < 767) {
            $('#no-issue-button-mobile button').hide();
            $('#finder-next-button-mobile a').show();
        } else {
            $('#finder-next-button button').hide();
            $('#no-issue-button-mobile button').hide();
            $('#finder-next-button a').show();
        }
    }
}

function mattressDimensions(index) {
    var w = 212;
    var h = 288;
    switch(index) {
        //Twin
        case 1:
            w = 140;
            h = 270;
            break;

        //Twin XL
        case 2:
            w = 140;
            h = 287;
            break;

        //Full
        case 3:
            w = 193;
            h = 270;
            break;

        //Queen
        case 4:
            w = 212;
            h = 288;
            break;

        //Cal King
        case 5:
            w = 258;
            h = 301;
            break;

        //King
        case 6:
            w = 272;
            h = 287;
            break;
    }

    var dimensions = new Array();
    dimensions.push(w);
    dimensions.push(h);

    return dimensions;
}

function loadInterface(step) {
    var stepNumber = step.find('div').data('activestep');
    if (stepNumber % 2 == 0) {
        $cache.mainWrapper.removeClass('light');
        $cache.progressWrapper.removeClass('light');
        $cache.question.removeClass('light');
        step.find('.mattress-box').removeClass('light');
    } else {
        $cache.mainWrapper.addClass('light');
        $cache.progressWrapper.addClass('light');
        $cache.question.addClass('light');
        step.find('.mattress-box').addClass('light');
    }

    // initialize default next buttons
    toggleNextButton(false);

    /*
    *  Mobile
    */
    if (parseInt($(window).width()) < 767) {
        var containerWidth = $(window).width();
        $('.finder-question-box, .finder-question-box.current').css('width', containerWidth);
        var container = $('.finder-question-box.current').find('ul.finder-question').not('.selectpoints');
        var imgContainer = $('.finder-question-box.current').find('.mattress-box');
        if (step.find('ul.finder-question').hasClass('slider') || step.find('ul.finder-question').hasClass('icons')) {
            initMobileSliderAnswers(step);
            if (step.find('.mattress-box').find('.answer-desktop').length > 0) {
                step.find('.mattress-box .answer-desktop').on('click', function() {
                    // initially remove all active classes
                    $(this).siblings('.answer-desktop').removeClass('active');
                    // add active class if it is not already active
                    if ($(this).not('.active')) {
                        $(this).addClass('active');
                    }
                    var imgIndex = $(this).data('answerindex');
                    step.find('li.answer[data-answerindex="' + imgIndex + '"]').find('.mobile-answer-click-area').click();
                });
            } else {
                step.find('.mattress-box img').on('click', function() {
                    var $this = $(this);
                    // initially reset all img sources
                    $this.siblings('img').each(function() {
                        var origSrc = $(this).data('origsrc');
                        $(this).attr('src', origSrc);
                    });
                    // if the image is not on, swap the src to the "on" img src
                    var origSrc = $this.data('origsrc');
                    var onSrc = $this.data('onsrc');
                    if ($this.attr('src') == onSrc) {
                        $this.attr('src', origSrc);
                    } else {
                        $this.attr('src', onSrc);
                    }

                    var imgIndex = $this.data('answerindex');
                    step.find('li.answer[data-answerindex="' + imgIndex + '"]').find('.mobile-answer-click-area').click();
                });
            }
        }
        var selectedAnswer = 1;
        var inputChecked = false;
        step.find('li.answer').each(function() {
            if ($(this).find('span.check-finder-mobile').hasClass('clicked') || $(this).find('input').attr('checked') == true || $(this).find('input').attr('checked') == "checked") {
                selectedAnswer = $(this).data('answerindex');
                inputChecked = true;
            }
            if (!inputChecked && $(this).data('defaultanswer')) {
                selectedAnswer = $(this).data('answerindex');
            }
        });
        if (step.find('.mattress-box').find('.answer-desktop').length > 0) {
            step.find('.mattress-box').find('.answer-desktop[data-answerindex="' + selectedAnswer + '"]').show();
        } else {
            step.find('.mattress-box').find('img[data-answerindex="' + selectedAnswer + '"]').show();
        }
        step.find('li.answer[data-answerindex="' + selectedAnswer + '"]').click();

        if (step.find('ul.finder-question').not('.selectpoints')) {
            container.not('.slick-initialized').slick({
                dots: true,
                infinite: false,
                initialSlide: selectedAnswer - 1,
                slidesToShow: 1,
                slidesToScroll: 1,
                swipeToSlide: true
            });

            imgContainer.on( "swipeleft", swipeLeftHandler );
            imgContainer.on( "swiperight", swipeRightHandler );
            function swipeLeftHandler() {
                console.log('swipe left');
                container.slick('slickNext');
            }
            function swipeRightHandler() {
                console.log('swipe right');
                container.slick('slickPrev');
            }

            container.on('afterChange', function(event, slick, currentSlide, nextSlide){
                if (step.find('.mattress-box').find('.answer-desktop').length > 0) {
                    step.find('.mattress-box .answer-desktop').each(function() {
                        $(this).hide();
                    });
                } else {
                    step.find('.mattress-box img').each(function() {
                        $(this).hide();
                    });
                }
                currentSlide = currentSlide + 1;

                // Set h and w of mattress based on slider
                if (step.find('div').data('activestep') == "1") {
                    var dimensions = mattressDimensions(currentSlide);
                    $('.mattress-box').each(function() {
                        $(this).css('width', dimensions[0]);
                        $(this).css('height', dimensions[1]);
                    });
                }

                if (step.find('.mattress-box').find('.answer-desktop').length > 0) {
                    step.find('.mattress-box').find('.answer-desktop[data-answerindex="' + currentSlide + '"]').show();
                } else {
                    var _image = step.find('.mattress-box').find('img[data-answerindex="' + currentSlide + '"]');
                    _image.show();
                    calculateMarginTop(_image, step);
                    calculateMarginLeft(_image, step);
                }
            });
        }
    }

    /*
    *  Desktop
    */
    if (parseInt($(window).width()) > 767) {
        if (step.find('ul.finder-question').hasClass('slider')) {
            var selectedAnswer = 1;
            var inputChecked = false;
            step.find('li.answer').each(function() {
                if ($(this).find('input').attr('checked') == true || $(this).find('input').attr('checked') == "checked") {
                    selectedAnswer = $(this).data('answerindex');
                    inputChecked = true;
                }
                if (!inputChecked && $(this).data('defaultanswer')) {
                    selectedAnswer = $(this).data('answerindex');
                }
            });

            // hide all answers and append icons
            initSliderAnswers(step);

            if (step.find('div').data('activestep') == "5") {
                var _desc = step.find('.mattress-box').find('.answer-desktop[data-answerindex="' + selectedAnswer + '"]');
                _desc.show();
            } else {
                step.find('li.answer[data-answerindex="' + selectedAnswer + '"]').show();
                var _image = step.find('.mattress-box').find('img[data-answerindex="' + selectedAnswer + '"]');
                _image.show();
            }

            /*
            *  Slider
            */
            step.find('.mattress-slider').slider({
                range: "max",
                min: 1,
                max: step.find('li.answer').length,
                step: 1,
                value: selectedAnswer,
                create: function( event, ui ) {
                    step.find('li.answer[data-answerindex="' + selectedAnswer + '"]').click();
                },
                slide: function( event, ui ) {

                    // Mattress Size
                    if (step.find('div').data('activestep') == "1") {
                        // Set h and w of mattress based on slider
                        var dimensions = mattressDimensions(ui.value);
                        $('.mattress-box').each(function() {
                            $(this).css('width', dimensions[0]);
                            $(this).css('height', dimensions[1]);
                        });
                    }

                    initSliderAnswers(step);

                    if (step.find('div').data('activestep') == "5") {
                        var _desc = step.find('.mattress-box').find('.answer-desktop[data-answerindex="' + ui.value + '"]');
                        _desc.show();
                    } else {
                        // show selected answer from slider
                        step.find('li.answer[data-answerindex="' + ui.value + '"]').show();
                        var _image = step.find('.mattress-box').find('img[data-answerindex="' + ui.value + '"]');
                        _image.show();

                        calculateMarginTop(_image, step);
                        calculateMarginLeft(_image, step);
                    }

                    // simulate a click based on slider selection
                    step.find('li.answer[data-answerindex="' + ui.value + '"]').click();
                }
            });
        }

        /*
        *  Icons
        */
        if (step.find('ul.finder-question').hasClass('icons')) {

            // initially hide all answers and setup default
            step.find('li.answer').each(function() {
                $(this).hide();
                var image = $(this).find('img').detach();
                step.find('.mattress-box').append(image);
            });

            // initially hide all mattress-box images
            step.find('.mattress-box').find('img').each(function() {
                calculateMarginTop($(this), step);
                calculateMarginLeft($(this), step);
                $(this).hide();
            });

            step.find('ul.answer-icons li').on('click', function() {
                var iconSrc = $(this).find('img').attr('src');
                // remove active class
                step.find('ul.answer-icons li').each(function() {
                    $(this).removeClass('active');
                });
                // hide all mattress-box images
                step.find('.mattress-box').find('img').each(function() {
                    $(this).hide();
                });
                step.find('.mattress-box img[src="' + iconSrc + '"]').show();

                // hide all answers and only show selected mattress-box image
                step.find('li.answer').each(function() {
                    $(this).hide();
                });

                $(this).addClass('active');
                var answerIndex = $(this).data('answerindex');
                step.find('li.answer[data-answerindex="' + answerIndex + '"]').show();
                step.find('li.answer[data-answerindex="' + answerIndex + '"]').click();
            });

            // select first icon by default if nothing is selected
            var selectedAnswer = 1;
            step.find('li.answer').each(function() {
                if ($(this).find('input').attr('checked') == true || $(this).find('input').attr('checked') == "checked") {
                    selectedAnswer = $(this).data('answerindex');
                }
                if ($(this).data('defaultAnswer') && ($(this).find('input').attr('checked') != true || $(this).find('input').attr('checked') != "checked")) {
                    selectedAnswer = $(this).data('answerindex');
                }
            });

            step.find('li.answer[data-answerindex="' + selectedAnswer + '"]').show();
            step.find('.mattress-box').find('img[data-answerindex="' + selectedAnswer + '"]').show();
            step.find('ul.answer-icons li[data-answerindex="' + selectedAnswer + '"]').click();

        }
    }

    /*
    *  Select Points (e.g. Pain Points)
    */
    if (step.find('ul.finder-question').hasClass('selectpoints')) {
        toggleNextButton(true);

        $('#finder-next-button button').on('click', function(e) {
            e.preventDefault();
            step.find('li.answer[data-answer="noIssues"]').click();
            $('[id^=finder-next-button] a#finder-next').click();
        });
        $('#no-issues-button-mobile button').on('click', function(e) {
            e.preventDefault();
            $('[id^=finder-next-button] a#finder-next').click();
        });

        // initially hide all answers and setup default
        step.find('li.answer').each(function() {
            $(this).hide();
            // ignore "No Issues"
            if ($(this).data('answer') != 'noIssues') {
                var sp = $(this).find('div.selectpoint').detach();
                step.find('.mattress-box').append(sp);
            }
        });

        step.find('.mattress-box div.selectpoint').unbind('click');
        step.find('.mattress-box div.selectpoint').on('click', function() {
            $(this).toggleClass('active');
            if (step.find('.mattress-box div.selectpoint.active').length > 0) {
                // if there is an active point, remove NO ISSUES button and replace with Next
                toggleNextButton(false);
            } else {
                toggleNextButton(true);
            }
            var answerIndex = $(this).data('answerindex');
            step.find('li.answer[data-answerindex="' + answerIndex + '"]').click();
        });

        step.find('li.answer[data-answerindex="1"]').show();
    }
}

function initializeEvents () {
    $cache.finderWrapper.hide();
    $cache.startQuiz.on('click', function(e) {
        e.preventDefault();
        $cache.stepZero.find('.heading-container').fadeOut('fast');
        $cache.stepZero.slideUp();
        $cache.finderWrapper.fadeIn();
        $cache.mainWrapper.addClass('light');
        $cache.question.addClass('light');
        utag.link({
			eventCategory: 'Mattress Finder',
			eventAction: 'Mattress Finder Start',
			eventLabel: 'Selected'
		});
        loadInterface($cache.question.eq(0));
    });

    $('a[id^=finder-next]').bind('click', finderNext);

    //var containerWidth = $(window).width();
    //$('.related-recommendations').css('width', containerWidth);
    $('.related-recommendations').slick({
        dots: true,
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        appendDots: $('#carousel-dots'),
        appendArrows: $('#carousel-arrows')
    });

	QuickViewButtons();

    $('body').on('keyup', '#Where', function (e) {
        var isValidZip = /(^\d{5}$)|(^\d{5}-\d{4}$)/.test($('#Where').val());
        if (isValidZip === true) {
            $('.zip-button').removeClass('greyed-out');
            $('.zip-button').prop('disabled', false);
        }
        if (isValidZip === false) {
            $('.zip-button').addClass('greyed-out');
            $('.zip-button').prop('disabled', true);
        }
    });
    var isValidZip = /(^\d{5}$)|(^\d{5}-\d{4}$)/.test('90210');
    $('body').on('click', '.finder-back', function (e) {
        e.preventDefault();
        var backButtonAriaLabel = "Back, now active question number " + activeStep.toString();
        $('.finder-back').attr('aria-label', backButtonAriaLabel);
        var nextButtonAriaLabel = "Next, now active question number " + activeStep.toString();
        $('[id^=finder-next]').removeAttr('aria-label');
        activeStep = activeStep - 1;
        changeQuestion ();
        $cache.progress.find('img').css('display', 'none');
        if (activeStep > 0) {
            $cache.progress.eq(activeStep - 1).addClass('active');
        } else {
            $cache.progress.removeClass('active');
            $cache.progress.eq(activeStep).addClass('active');
        }
        $('.finder-question-box').removeClass('current');
        $('.finder-question').hide();
        $('.finder-question-box').hide();
        $('.finder-question-box').eq(activeStep).addClass('current');
        $('.finder-question-box').eq(activeStep).find('.finder-question').show();
        $('.finder-question-box').eq(activeStep).show();
        try {
            $('.finder-question-box').eq(activeStep).find('select option:eq(0)').prop('selected', true);
        } catch (f) {

        }
        if (activeStep == 0) {
            $('.finder-back-button').hide();
            $('.bestmatch').html("<span>Let's get started</span>");
        }
        Quiz.SavedAnswers.pop();
    });
    $cache.progress.click(function () {
        $this = $(this);
        var chosenStep = ($this.attr('id') - 1);
        if (chosenStep == 0) {
            $('.finder-back-button').hide();
            $('.bestmatch').html("<span>Let's get started</span>");
        }
        for (i = 0; i < activeStep; i++) {
            Quiz.SavedAnswers.pop();
        }
        if (chosenStep <= activeStep) {
            activeStep = chosenStep;
            changeQuestion ();
            $cache.progress.find('img').css('display', 'none');
            $this.eq(activeStep - 1).addClass('active');
            for (var i = 0, j = $('.finder-question-box').length; i < j; i++) {
                if (i == activeStep) {
                    $('.finder-question-box').eq(i).addClass('current');
                    $('.finder-question-box').eq(activeStep - 1).removeClass('current');
                    $('.finder-question-box').eq(i).show();
                } else {
                    $('.finder-question-box').eq(i).removeClass('current');
                    $('.finder-question-box').eq(i).hide();
                }
            }
            $('.finder-question').hide();
            $('.finder-question-box').eq(activeStep).addClass('current');
            $('.finder-question-box').eq(activeStep - 1).removeClass('current');
            $('.finder-question-box').eq(activeStep).find('.finder-question').show();
            try {
                $('.finder-question-box').eq(activeStep).find('select option:eq(0)').prop('selected', true);
            } catch (e) {

            }
        }
    });

    $('.progress-steps').on('click', function(ev) {
		$this = $(this);
        var chosenStep = ($this.data('step') - 1);
        if (chosenStep == 0) {
            $('.finder-back-button').hide();
            $('.bestmatch').html("<span>Let's get started</span>");
        }
        for (i = 0; i < activeStep; i++) {
            Quiz.SavedAnswers.pop();
        }
        if (chosenStep <= activeStep) {
            activeStep = chosenStep;
            changeQuestion ();
            $cache.progress.find('img').css('display', 'none');
            $this.eq(activeStep - 1).addClass('active');
            for (var i = 0, j = $('.finder-question-box').length; i < j; i++) {
                if (i == activeStep) {
                    $('.finder-question-box').eq(i).addClass('current');
                    $('.finder-question-box').eq(activeStep - 1).removeClass('current');
                    $('.finder-question-box').eq(i).show();
                } else {
                    $('.finder-question-box').eq(i).removeClass('current');
                    $('.finder-question-box').eq(i).hide();
                }
            }
            $('.finder-question').hide();
            $('.finder-question-box').eq(activeStep).addClass('current');
            $('.finder-question-box').eq(activeStep - 1).removeClass('current');
            $('.finder-question-box').eq(activeStep).find('.finder-question').show();
            try {
                $('.finder-question-box').eq(activeStep).find('select option:eq(0)').prop('selected', true);
            } catch (e) {

            }
        }
		//}
    });
    if ($('body').find('.featured-best-result').html() != null) {
		var itemId = $('body').find('.featured-best-result > .featured-product > .first-result-info > .finder-itemnum').attr('data-itemid');
		if (itemId != null) {
			$(window).load(function () {
				utag.link({
					eventCategory: 'Mattress Finder',
					eventAction: itemId,
					eventLabel: 'Recommended'
				});
			});
		}
    }

    // This fires when users make a selection in the quiz on a select/dropdown
    $("body").on('change', ".Dropdown", function (e) {
        var $this = $(this);
        var $category = $this.parent().attr("id");
        var $answer = $this.find("option:selected").data("answer");
        var $readable = $this.find("option:selected").data("readable");
        var $refinements = $this.find("option:selected").data("refinements");
        var $activeStepImg = $("#" + (activeStep + 1) + " img");

        // holds a list of refinments that are available on the Category
        // this gets built in pt_findersearchresult.isml
        var refsInCat = Object.keys($refines.refinements);

        //checks for final question, if not the final question, insert a comma to separate question/answer pair
        if ($this.hasClass("final-question")) {
            finalQuestionBuild($category, $answer, $readable, $refinements, $activeStepImg, refsInCat);
        } else {
            answerGathering($category, $answer, $readable, $refinements, $activeStepImg);
        }
    });

    // This fires when the user hits the button under the zipcode field
    $(".zipcode").click(function (e) {
        e.preventDefault();
        var $this = $(this);
        var $category = $this.parent().attr("id");
        var $answer = $("#Where").val();
        var $readable = $("#Where").val();
        var $refinements = "zipcode|" + $("#Where").val();
        var $activeStepImg = $("#" + (activeStep + 1) + " img");

        // holds a list of refinments that are available on the Category
        // this gets built in pt_findersearchresult.isml
        var refsInCat = Object.keys($refines.refinements);

        //checks for final question, if not the final question, insert a comma to separate question/answer pair
        if ($this.hasClass("final-question")) {
            finalQuestionBuild($category, $answer, $readable, $refinements, $activeStepImg, refsInCat);
        } else {
            answerGathering($category, $answer, $readable, $refinements, $activeStepImg);
        }
    });
    $(".details-finder").click(function (e) {
        e.preventDefault();
        if ($(this).parent().parent().parent().find(".answer-description").css("display") == "none") {
            $(this).parent().parent().parent().find(".answer-tile i").attr("class", "fa fa-angle-up");
            $(this).parent().parent().parent().find(".answer-description").show();
            $(this).parent().parent().parent().parent().find(".answer-description").css("max-width",$(window).width());
        } else {
            $(this).parent().parent().parent().find(".answer-description").hide();
            $(this).parent().parent().parent().find(".answer-tile i").attr("class", "fa fa-angle-down");
        }
    });
    // this fires when a user hovers an answer icon area
    /*
    $(".answer").on("mouseover", function () {
        var answerID = $(this).parent().parent().find("[id^=finder-question-wrapper]").attr("id");
        var answerIndex = parseInt(answerID[answerID.length - 1]);
        var origHeading = $("#finder-question-wrapper-right-" + answerIndex + " h3").html();
        var origImage = $("#finder-answer-image-" + answerIndex).attr("src");
        var origDescr = $("#finder-qa-desc-" + answerIndex).html();
        $("#finder-question-wrapper-right-" + answerIndex + " h3").html($(this).data("heading"));
        $("#finder-answer-image-" + answerIndex).attr("src",$(this).data("imagelink"));
        $("#finder-qa-desc-" + answerIndex).html($(this).data("description"));
         $(".answer").on("mouseout", function () {
            $("#finder-question-wrapper-right-" + answerIndex + " h3").html(origHeading);
            $("#finder-answer-image-" + answerIndex).attr("src",origImage);
            $("#finder-qa-desc-" + answerIndex).html(origDescr);
        });
    });
    */
    $(".answer").keypress(function (ev) {
		if (ev.keyCode == 13 || ev.which == 13) {
			if (parseInt($(window).width()) > 767) {
				if ($(this).find("input").attr("checked") == true || $(this).find("input").attr("checked") == "checked") {
					$(this).find("input").removeAttr("checked");
					var answertileAriaLabel = $(this).find(".check-finder-desktop").parent().parent().attr('aria-label');
					answertileAriaLabel = answertileAriaLabel.replace(" checked","");
					$(this).find(".answer-tile").attr("aria-label",answertileAriaLabel);
					$(this).find(".check-finder-desktop").removeClass("clicked");
					if ($(this).siblings().find(".clicked").length < 1) {
						$("[id^=finder-next]").addClass("disabled");
					} else {
						$("[id^=finder-next]").removeClass("disabled");
					}
				} else {
					$(this).find("input").attr("checked", "checked");
					$(this).find(".check-finder-desktop").addClass("clicked");
					var answertileAriaLabel = $(this).find(".check-finder-desktop").parent().parent().attr('aria-label');
					answertileAriaLabel = answertileAriaLabel + " checked";
					$(this).find(".answer-tile").attr("aria-label",answertileAriaLabel);
					$("[id^=finder-next]").removeClass("disabled");
				}
			} else {}
		}
	});
    // This fires when the user selects an icon
   $(".answer").click(function (e) {
        e.preventDefault();
        // remove all other checked answers for this question except for multiselect
        if (!$(this).parent('ul').hasClass('selectpoints')) {
            $(this).siblings().find("input").each(function() {
                if ($(this).attr("checked") == true || $(this).attr("checked") == "checked") {
                    $(this).removeAttr("checked");
                }
            });
            $(this).siblings().find("span.check-finder-desktop").each(function() {
                if ($(this).hasClass("clicked")) {
                    $(this).removeClass("clicked");
                }
            });
        }
        //if (parseInt($(window).width()) > 767) {
            if ($(this).find("input").attr("checked") == true || $(this).find("input").attr("checked") == "checked") {
                $(this).find("input").removeAttr("checked");
                $(this).find(".check-finder-desktop").removeClass("clicked");
                var answertileAriaLabel = $(this).find(".check-finder-desktop").parent().parent().attr('aria-label');
                answertileAriaLabel = answertileAriaLabel.replace(" checked","");
                $(this).find(".answer-tile").attr("aria-label",answertileAriaLabel);
                /*
                if ($(this).siblings().find(".clicked").length < 1) {
                    $("#finder-next").addClass("disabled");
                }else {
                    $("#finder-next").removeClass("disabled");
                }
                */
            } else {
                $(this).find("input").attr("checked", "checked");
                $(this).find(".check-finder-desktop").addClass("clicked");
                var answertileAriaLabel = $(this).find(".check-finder-desktop").parent().parent().attr('aria-label');
                answertileAriaLabel = answertileAriaLabel + " checked";
                //if (!$(this).find(".answer-tile").attr("aria-label").hasClass("checked")) {
                    $(this).find(".answer-tile").attr("aria-label",answertileAriaLabel);
                //}
                //$("#finder-next").removeClass("disabled");
            }
        //} else {}
    });
    Quiz.SavedAnswers = [];

    //$("#finder-next").on("click", function (e) {
    function finderNext(e) {
        // if the span was clicked, move up one parent
        var $this = $(e.target);
        if (e.target.tagName.toLowerCase() === 'span') {
            $this = $(e.target).parent('a#finder-next');
        }
        e.preventDefault();
        if ($this.hasClass("disabled") == true) {
            return;
        }
        var checkedLis = $this.parent().parent().find(".current ul li").find(".clicked");
        var $category = $this.parent().attr("id");
        var questionIndex = 0;
        var answerIndex = 0;
        var myClickedLi = {};
        var answerMatrix = {};
        answerMatrix.question = {
            id: "",
            answers: []
        };
        for (var i = 0, j = checkedLis.length; i < j; i++) {
            myClickedLi = checkedLis[i] ;
            questionIndex = $(myClickedLi).data("questionindex") - 1;
            answerIndex = $(myClickedLi).data("answerindex");
            if (answerMatrix.question.id == "") {
                answerMatrix.question.id = questionIndex;
            }
            answerMatrix.question.answers.push(answerIndex);
        }
        Quiz.SavedAnswers.push(answerMatrix);
        sendFinderAnalytics(questionIndex);
        if ($this.parent().parent().find(".current ul li.final-question").length > 0) {
            var answerRefinements = []
            var stagedRefinements = []; // holds all the mapped refinements to each answer
            var finalRefinements = [];  // holds pared down list that removes duplicate refinements
            var intersection2ranges = {};
            var refsInCat = Object.keys($refines.refinements);
            //Results[activeStep] = $category + '=' + $answer; No longer used due to matrix
            var currQuest = $("#" + (activeStep + 1)).attr("data-title");
            currQuest = currQuest.substring(0, (currQuest.indexOf("?") + 1));
            //Titles[activeStep] = currQuest + ' : ' + $readable; No longer used due to matrix
            //Refinements[activeStep] = $refinements; No longer used due to matrix

            // joins the questions and answers for tooltips into an array
            var finalReadable = "";// Titles.join(""); No longer used due to matrix
            var tmpReadable = "";
            for (i = 0; i < Quiz.Questions.length; i++) {
                tmpReadable = Quiz.Questions[i].question_Text + " : ";
                for (a = 0; a < Quiz.SavedAnswers[i].question.answers.length; a++) {
                    tmpReadable = tmpReadable + Quiz.Questions[i].Answers[(Quiz.SavedAnswers[i].question.answers[a]) - 1].answerText + " ~ ";
                }
                finalReadable = finalReadable + tmpReadable + "|";
            }
            utag.link({
                eventCategory: 'Mattress Finder',
                eventAction: $("#" + (activeStep + 1)).attr("data-title"),
                eventLabel: 'Selected'
            });
            // passes the string of tooltips via a form input
            $cache.form.append("<input type='hidden' name='ReadableResults' id='ReadableResults' value='" + finalReadable + "'>");

            var pair = [];
            // adds the actually answers to the querystring on the form
            // AND
            //builds answerRefinements
            //each question has at least one answer
            //each answer has at least one refinement
            //multiple answers are possible
            //build the object to hold each question's answer's refinments seperated by a !
            for (i = 0; i < Quiz.SavedAnswers.length; i++) {
                var questionID = Quiz.Questions[Quiz.SavedAnswers[i].question.id].Finder_Question_ID;
                var answerIDs = "";
                var tempRefines = "";
                for (j = 0;j < Quiz.SavedAnswers[i].question.answers.length; j++) {
                    answerIDs = answerIDs + Quiz.SavedAnswers[i].question.answers[j];
                    if (typeof(Quiz.Questions[Quiz.SavedAnswers[i].question.id].Answers[Quiz.SavedAnswers[i].question.answers[j] - 1].refinementString) !== 'undefined') {
                        tempRefines = tempRefines + Quiz.Questions[Quiz.SavedAnswers[i].question.id].Answers[Quiz.SavedAnswers[i].question.answers[j] - 1].refinementString + "!";
                    }
                    if (j + 1 != Quiz.SavedAnswers[i].question.answers.length) {
                        answerIDs = answerIDs + ",";
                    }
                }
                tempRefines = tempRefines.substring(0, tempRefines.length - 1);
                answerRefinements.push(tempRefines)
                $cache.form.append("<input type='hidden' name='" + questionID + "' id='" + questionID + "' value='" + answerIDs + "'>");
            }
            // builds stagedRefinements - Holds all mapped refinements to each answer in an object
            for (i = 0; i < answerRefinements.length; i++) {
                if (answerRefinements[i].indexOf("!") > -1) {
                    pair = answerRefinements[i].split('!');
                    for (var a = 0, b = pair.length; a < b; a++) {
                        innerPair = pair[a].split("|");
                        innerPair[1].replace(/-/g, ",");
                        stagedRefinements.push(innerPair);
                    }
                } else {
                    pair = answerRefinements[i].split("|");
                    stagedRefinements.push(pair);
                }
            }
            // Grab all the names of the refinements in the stagedRefinement list
            var names = Object.keys(stagedRefinements);
            var finalRefinementsCount = 0;
            var refinementA = "";
            var values = "";
            var key = "";
            // holds the list of refinements in stagedRefinements that occure in the list of refinements from the category
            var holder = [];
            // Builds the final list of refinements to send to the search.  Each refinement, from the stagedRefinements, that exists
            // on the category gets put into finalRefinements where "key" is the refinement name or "prefv#" in the search pipelet
            // This for statement builds all keys then the next builds all the values.
            for (var a = 0, b = stagedRefinements.length; a < b; a++) {
                refinementA = stagedRefinements[a][0];
                if (JSON.stringify(refsInCat).indexOf(refinementA) > -1 && !JSON.stringify(holder).indexOf(refinementA) > -1) {
                    finalRefinements[finalRefinementsCount] = new Object();
                    holder.push(refinementA);
                    finalRefinements[finalRefinementsCount].key = refinementA;
                    finalRefinements[finalRefinementsCount].values = new Array();
                    finalRefinementsCount++;
                }
            }
            var counter = 0;
            // This for statement builds all values then the previous builds all the keys.
            for (a = 0, b = stagedRefinements.length; a < b; a++) {
                counter = 0;
                values = "";
                if (stagedRefinements[a][1] == null || stagedRefinements[a][1] == undefined || stagedRefinements[a][1] == "") {
                    values = " - ";
                } else {
                    values = stagedRefinements[a][1];
                }
                key = stagedRefinements[a][0];
                var matchFlag = false;
                for (i = 0, j = finalRefinements.length; i < j; i++) {
                    if (JSON.stringify(finalRefinements[i].key).indexOf(key) > -1) {
                        if (values != "") {
                            var valueArray = values.split("-");
                        }
                        for (var k = 0, l = valueArray.length; k < l; k++) {
                            if (finalRefinements[i].values.length < 1) {
                                finalRefinements[i].values.push(valueArray[k]);
                            } else {
                                if (!JSON.stringify(finalRefinements[i].values).indexOf(valueArray[k]) > -1) {
                                    finalRefinements[i].values.push(valueArray[k]);
                                }
                            }
                        }
                    }

                }
            }

            // Adding the refinements to the querystring for prefv# and prefn#
            for (i = 0, j = finalRefinements.length; i < j; i++) {
                $cache.form.append("<input type='hidden' name='prefn" + (i + 1) + "' id='prefn" + (i + 1) + "' value='" + finalRefinements[i].key + "'>");
                $cache.form.append("<input type='hidden' name='prefv" + (i + 1) + "' id='prefv" + (i + 1) + "' value='" + finalRefinements[i].values.join("|") + "'>");
            }
            // Needed to be verified that is used
            //$cache.form.append("<input type='hidden' name='prefn" + (i + 1) + "' id='prefn" + (i + 1) + "' value='isMFIproduct'>");
            //$cache.form.append("<input type='hidden' name='prefv" + (i + 1) + "' id='prefv" + (i + 1) + "' value='true'>");

            // handle price refinements
            var priceRefinements = {};
            var newArrayIndex = 0;
            for (i = 0, j = stagedRefinements.length; i < j; i++) {
                if (stagedRefinements[i][0] == "price") {
                    priceRefinements[newArrayIndex] = stagedRefinements[i][1].split("-");
                    newArrayIndex++
                }
            }
            if (priceRefinements.length > 1) {
                intersection2ranges = intersect_safe(priceRefinements[0], priceRefinements[1]);
                var highestPrice = 0;
                var lowestPrice = 0;
                if (intersection2ranges.length > 1) {
                    intersection2ranges.sort(function (a, b) {
                        return a - b
                    });

                    highestPrice = intersection2ranges[(intersection2ranges.length) - 1];

                    lowestPrice = intersection2ranges[0];

                }
            } else {
                priceRefinements[0].sort(function (a, b) {
                    return a - b
                });
                highestPrice = priceRefinements[0][(priceRefinements[0].length) - 1];
                lowestPrice = priceRefinements[0][0];

            }
            highestPrice = highestPrice.replace(",", "");
            lowestPrice = lowestPrice.replace(",", "");
            if (parseInt(highestPrice) < parseInt(lowestPrice)) {
                var holderPrice = highestPrice;
                highestPrice = lowestPrice;
                lowestPrice = holderPrice;
            }

            $cache.form.append("<input type='hidden' name='pmax' id='pmax' value='" + highestPrice + "'>");
            $cache.form.append("<input type='hidden' name='pmin' id='pmin' value='" + lowestPrice + "'>");

            // adding sort rule per requirements
            $cache.form.append("<input type='hidden' name='srule' id='srule' value='category-position'/>");
            $cache.form.append("<input type='hidden' name='cgid' id='cgid' value='" + $cache.form.data('category') + "'/>");
            $cache.form.submit();
        } else {
            activeStep++;
            if ($('.finder-back-button').is(":visible")) {
				var questionNumber = activeStep + 1;
				var nextButtonAriaLabel = "Next button, now active question number " + questionNumber.toString();
				$('[id^=finder-next]').attr('aria-label', nextButtonAriaLabel);
				$('.finder-back').removeAttr('aria-label');
            } else if ($('.finder-back-button').is(":hidden") && !$('[id^=finder-next]').hasClass('disabled')) {
				var questionNumber = activeStep + 1;
				var nextButtonAriaLabel = "Next button, Back button is now enabled, now active question number " + questionNumber.toString();
				$('[id^=finder-next]').attr('aria-label', nextButtonAriaLabel);
				$('.finder-back').removeAttr('aria-label');
            }
            changeQuestion ();
            if (parseInt($(window).width()) < 767) {
                $(".finder-back-button.back-mobile").css("display", "block");
            } else {
                $(".finder-back-button.back-desktop").css("display", "block");
            }
            var widthOfUL = 0;
            if (parseInt($(window).width()) > 767) {
                widthOfUL = $(".finder-question-box.current ul.finder-question li").length * 196;
            } else {
                widthOfUL = "100%";
            }
        }
        $($(".finder-question-wrapper-left-focus")).attr("tabindex",-1).focus();
    }
    /*
        var $this = $(this);
        var $category = $this.parent().attr("id");
        var $answer = $this.data("answer");
        var $readable = $this.data("readable");
        var $refinements = $this.data("refinements");
        var $activeStepImg = $("#" + (activeStep + 1) + " img");

        // holds a list of refinments that are available on the Category
        // this gets built in pt_findersearchresult.isml
        var refsInCat = Object.keys($refines.refinements);

        // shared styles led to using answer class on both the icons and dropdowns.
        if ($this.hasClass(".dropdown") == true) {
            return false;
        }

        //checks for final question, if not the final question, insert a comma to separate question/answer pair
        if ($this.hasClass("final-question")) {
            finalQuestionBuild($category, $answer, $readable, $refinements, $activeStepImg, refsInCat);
        } else {
            answerGathering($category, $answer, $readable, $refinements, $activeStepImg);
     *  }
     *
     */
    $(".mobile-answer-click-area").click(function (e) {
        if (parseInt($(window).width()) <= 767) {
            if ($(this).parent().find("input").attr("checked") == true || $(this).parent().find("input").attr("checked") == "checked") {
                $(this).parent().find("input").removeAttr("checked");
                $(this).find(".check-finder-mobile").removeClass("clicked");
            } else {
                $(this).parent().find("input").attr("checked", "checked");
                $(this).find(".check-finder-mobile").addClass("clicked");
                $("#finder-next").removeClass("disabled");
            }
        }
    });
    function changeQuestion () {

        //show proper question with animate - compatible with IE 8
        //$cache.question.eq(activeStep).addClass("current").removeClass("hidden").animate({opacity: 1}, 700);
        $cache.question.eq(activeStep-1).slideUp();
        $cache.question.eq(activeStep).fadeIn('slow').addClass("current").removeClass("hidden");
        //add disabled class to next button if no answers are checked and remove it if there are.
        /*
        $.each($cache.question.eq(activeStep).find("input"), function () {
            if ($(this).attr("checked") == true || $(this).attr("checked") == "checked") {
                $("#finder-next").removeClass("disabled");
                return false;
            }else {
                if (!$("#finder-next").hasClass("disabled")) {
                    $("#finder-next").addClass("disabled");
                }
            }
        });
        */
        //set proper step bubble
        $cache.progress.removeClass("active");
        $cache.progress.removeClass("answered");
        for (var i = 1, j = $cache.progress.length + 1; i < j; i++) {
            if ((i - 1) < activeStep) {
                $cache.progress.eq(i - 1).addClass("answered");
            }
            if ((i) == activeStep) {
                $cache.progress.eq(i).addClass("active");
            }
        }
        // hide Let's Get Started
        $('.bestmatch').html("");

        //hide current question
        $cache.question.eq(activeStep - 1).hide();
        $cache.question.eq(activeStep - 1).removeClass("current");
        $cache.question.eq(activeStep).show();
        $cache.question.eq(activeStep).find(".finder-question").show();
        loadInterface($cache.question.eq(activeStep));
    }

    function intersect_safe (a, b) { // this function finds the intersection between two arrays. We are using this to compare answers on the finder
        var ai = 0;
        var bi = 0;
        var result = [];

        while (ai < a.length && bi < b.length) {
            if (a[ai] < b[bi]) {
                ai++;
            } else if (a[ai] > b[bi]) {
                bi++;
            } else {
                result.push(a[ai]);
                ai++;
                bi++;
            }
        }

        return result;
    }

    function finalQuestionBuild ($category, $answer, $readable, $refinements, $activeStepImg, refsInCat) {
        var stagedRefinements = []; // holds all the mapped refinements to each answer
        var finalRefinements = [];  // holds pared down list that removes duplicate refinements
        var intersection2ranges = {};
        Results[activeStep] = $category + '=' + $answer;
        var currQuest = $("#" + (activeStep + 1)).attr("data-title");
        currQuest = currQuest.substring(0, (currQuest.indexOf("?") + 1));
        Titles[activeStep] = currQuest + ' : ' + $readable;
        Refinements[activeStep] = $refinements;

        // Append the answer to the question in the tooltip on the progress bar step.
        $("#" + (activeStep + 1)).attr("data-title", (currQuest + " " + $readable));

        // joins the questions and answers for tooltips into an array
        var finalReadable = Titles.join("");
        utag.link({
            eventCategory: 'Mattress Finder',
            eventAction: $("#" + (activeStep + 1)).attr("data-title"),
            eventLabel: 'Selected'
        });
        // passes the string of tooltips via a form input
        $cache.form.append("<input type='hidden' name='ReadableResults' id='ReadableResults' value='" + finalReadable + "'>");

        var pair = [];
        // adds the actually answers to the querystring on the form
        for (i = 0; i < Results.length; i++) {
            pair = Results[i].split('=');
            $cache.form.append("<input type='hidden' name='" + pair[0] + "' id='" + pair[0] + "' value='" + pair[1] + "'>");
        }
        // builds stagedRefinements - Holds all mapped refinements to each answer in an object
        for (i = 0; i < Refinements.length; i++) {
            if (Refinements[i].indexOf("!") > -1) {
                pair = Refinements[i].split('!');
                for (var a = 0, b = pair.length; a < b; a++) {
                    innerPair = pair[a].split("|");
                    innerPair[1].replace(/-/g, ",");
                    stagedRefinements.push(innerPair);
                }

            } else {
                pair = Refinements[i].split("|");
                stagedRefinements.push(pair);
            }
        }
        // Grab all the names of the refinements in the stagedRefinement list
        var names = Object.keys(stagedRefinements);
        var finalRefinementsCount = 0;
        var refinementA = "";
        var values = "";
        var key = "";
        // holds the list of refinements in stagedRefinements that occure in the list of refinements from the category
        var holder = [];
        // Builds the final list of refinements to send to the search.  Each refinement, from the stagedRefinements, that exists
        // on the category gets put into finalRefinements where "key" is the refinement name or "prefv#" in the search pipelet
        // This for statement builds all keys then the next builds all the values.
        for (var a = 0, b = stagedRefinements.length; a < b; a++) {
            refinementA = stagedRefinements[a][0];
            if (JSON.stringify(refsInCat).indexOf(refinementA) > -1 && !JSON.stringify(holder).indexOf(refinementA) > -1) {
                finalRefinements[finalRefinementsCount] = new Object();
                holder.push(refinementA);
                finalRefinements[finalRefinementsCount].key = refinementA;
                finalRefinements[finalRefinementsCount].values = new Array();
                finalRefinementsCount++;
            }
        }
        var counter = 0;
        // This for statement builds all values then the previous builds all the keys.
        for (a = 0, b = stagedRefinements.length; a < b; a++) {
            counter = 0;
            values = "";
            if (stagedRefinements[a][1] == null || stagedRefinements[a][1] == undefined || stagedRefinements[a][1] == "") {
                values = " - ";
            } else {
                values = stagedRefinements[a][1];
            }
            key = stagedRefinements[a][0];
            var matchFlag = false;
            for (i = 0, j = finalRefinements.length; i < j; i++) {
                if (JSON.stringify(finalRefinements[i].key).indexOf(key) > -1) {
                    if (values != "") {
                        var valueArray = values.split("-");
                    }
                    for (var k = 0, l = valueArray.length; k < l; k++) {
                        if (finalRefinements[i].values.length < 1) {
                            finalRefinements[i].values.push(valueArray[k]);
                        } else {
                            if (!JSON.stringify(finalRefinements[i].values).indexOf(valueArray[k]) > -1) {
                                finalRefinements[i].values.push(valueArray[k]);
                            }
                        }
                    }
                }

            }
        }

        // Adding the refinements to the querystring for prefv# and prefn#
        for (i = 0, j = finalRefinements.length; i < j; i++) {
            $cache.form.append("<input type='hidden' name='prefn" + (i + 1) + "' id='prefn" + (i + 1) + "' value='" + finalRefinements[i].key + "'>");
            $cache.form.append("<input type='hidden' name='prefv" + (i + 1) + "' id='prefv" + (i + 1) + "' value='" + finalRefinements[i].values.join("|") + "'>");
        }
        // Needed to be verified that is used
        //$cache.form.append("<input type='hidden' name='prefn" + (i + 1) + "' id='prefn" + (i + 1) + "' value='isMFIproduct'>");
        //$cache.form.append("<input type='hidden' name='prefv" + (i + 1) + "' id='prefv" + (i + 1) + "' value='true'>");

        // handle price refinements
        var priceRefinements = {};
        var newArrayIndex = 0;
        for (i = 0, j = stagedRefinements.length; i < j; i++) {
            if (stagedRefinements[i][0] == "price") {
                priceRefinements[newArrayIndex] = stagedRefinements[i][1].split("-");
                newArrayIndex++
            }
        }
        if (priceRefinements.length > 1) {
            intersection2ranges = intersect_safe(priceRefinements[0], priceRefinements[1]);
            var highestPrice = 0;
            var lowestPrice = 0;
            if (intersection2ranges.length > 1) {
                intersection2ranges.sort(function (a, b) {
                    return a - b
                });

                highestPrice = intersection2ranges[(intersection2ranges.length) - 1];

                lowestPrice = intersection2ranges[0];

            }
        } else {
            priceRefinements[0].sort(function (a, b) {
                return a - b
            });
            highestPrice = priceRefinements[0][(priceRefinements[0].length) - 1];
            lowestPrice = priceRefinements[0][0];

        }
        highestPrice = highestPrice.replace(",", "");
        lowestPrice = lowestPrice.replace(",", "");
        if (parseInt(highestPrice) < parseInt(lowestPrice)) {
            var holderPrice = highestPrice;
            highestPrice = lowestPrice;
            lowestPrice = holderPrice;
        }

        $cache.form.append("<input type='hidden' name='pmax' id='pmax' value='" + highestPrice + "'>");
        $cache.form.append("<input type='hidden' name='pmin' id='pmin' value='" + lowestPrice + "'>");

        // adding sort rule per requirements
        $cache.form.append("<input type='hidden' name='srule' id='srule' value='category-position'/>");
        $cache.form.append("<input type='hidden' name='cgid' id='cgid' value='" + $cache.form.data('category') + "'/>");
        $cache.form.submit();
    }

    function answerGathering ($category, $answer, $readable, $refinements, $activeStepImg) {
        // This builds the Results, Titles, Refinements for each question
        Results[activeStep] = $category + '=' + $answer;
        var currQuest = $("#" + (activeStep + 1)).attr("data-title");
        currQuest = currQuest.substring(0, (currQuest.indexOf("?") + 1));
        Titles[activeStep] = currQuest + ' : ' + $readable + '|';
        Refinements[activeStep] = $refinements;

        // Append the answer to the question in the tooltip on the progress bar step.
        $("#" + (activeStep + 1)).attr("data-title", (currQuest + " " + $readable));
        //$(".progress-bubbles").tooltip(tooltipoptions);
        // Set checkmark
        $activeStepImg.css("display", "block");
        utag.link({
            eventCategory: 'Mattress Finder',
            eventAction: $("#" + (activeStep + 1)).attr("data-title"),
            eventLabel: 'Selected'
        });
        //increment step and get next question
        activeStep++;

        changeQuestion ();

        if (parseInt($(window).width()) < 767) {
            $(".finder-back-button.back-mobile").css("display", "inline-block");
        } else {
            $(".finder-back-button.back-desktop").css("display", "inline-block");
        }
        if (parseInt($(window).width()) > 1024) {
            $('html, body').animate({scrollTop: 220}, 250);
        } else if (parseInt($(window).width()) == 1024) {
            $('html, body').animate({scrollTop: 220}, 250);
        } else if (parseInt($(window).width()) < 1024 && parseInt($(window).width()) > 375) {
            $('html, body').animate({scrollTop: 275}, 250);
        } else {
            $('html, body').animate({scrollTop: 300}, 250);
        }
        var widthOfUL = 0;

        if (parseInt($(window).width()) > 767) {
            widthOfUL = $(".finder-question-box.current ul.finder-question li").length * 196;
        } else {
            widthOfUL = "100%";
        }
        //$(".finder-question-box.current ul.finder-question").css("width",widthOfUL);
        //$(".finder-question-box.current ul.finder-question").css("margin","0 auto");
    }
	function sendFinderAnalytics(questionIndex) {
		var quizQuestion = Quiz.Questions[questionIndex];
		var questionText = quizQuestion.question_Text;
		var answerMatrix = Quiz.SavedAnswers[questionIndex];
		var selectedAnswersArray = answerMatrix.question.answers;
		var answersArray = [];
		for (var i = 0; i < selectedAnswersArray.length; i++) {
			var answerIndex = selectedAnswersArray[i] - 1;
			var answerText = quizQuestion.Answers[answerIndex].answerText;
			answersArray.push(answerText);
		}
		var eventAction = questionText + " " + answersArray.join(" ~ ");
		eventAction = eventAction.replace(/&rdquo;/g, '"');
		eventAction = eventAction.replace(/&quot;/g, '"');
		utag.link({
			eventCategory: 'Mattress Finder',
			eventAction: eventAction,
			eventLabel: 'Selected'
		});
	}

	$("body").on("click", ".store-locator-recommendations", function (e) {
        utag.link({
                      eventCategory: 'Mattress Finder',
                      eventAction: 'Click',
                      eventLabel: 'Store Locator Banner'
        });
	});
}

function initializeResultsPills () {
	$(body).on("load", $(".answer-pills"), function () {
		var answerLiText = "", answerUlElement = $("ul");
	});
}

function initializeDOM () {
    // Hide tooltip when mousing over nearby elements. This is needed to faciltate the change made in
    // change made injquery.findertooltip.min.js to allow clicking of a link inside of the tooltip.
    var $tooltip = $("#tooltip");
    $("body")
        .on("mouseover", ".finder-results-banner ", function () {
            $tooltip.hide().css("opacity", "")
        })
        .on("mouseover", "#mattress-finder-heading p", function () {
            $tooltip.hide().css("opacity", "")
        })
        .on("mouseover", ".retake-button", function () {
            $tooltip.hide().css("opacity", "")
        })
        .on("mouseover", ".results-wrapper", function () {
            $tooltip.hide().css("opacity", "")
        })
        .on("mouseover", "#page-wrapper .bestmatch", function () {
            $tooltip.hide().css("opacity", "")
        })
        .on("mouseover", "#page-wrapper .question-wrapper", function () {
            $tooltip.hide().css("opacity", "")
        })
        .on("mouseover", "#mattress-finder-heading", function () {
            $tooltip.hide().css("opacity", "")
        });

    //$(".progress-bubbles").tooltip(tooltipoptions);
    /*
    var $elements = $(".finder-hide-excess");
    var linkhref = "";
    for (i = 0, j = $elements.length; i < j; i++) {
        if ($elements[i].offsetHeight < $elements[i].scrollHeight) {
            linkhref = $elements.eq(i).find("a").attr("href");
            $elements.eq(i).after("<div class='readmore'><a href='" + linkhref + "'>Read More ></a></div>");
        } else {
            // your element doesn't have overflow
        }
    }
    */
    var $refinements;
    //set class on body element to define the background image
    $cache.page.addClass("finder-question-" + (activeStep));
    //show proper question with animate - compatible with IE 8
    $cache.question.hide();
    $cache.question.eq(0).show();
    $cache.question.eq(0).addClass("current");
    //$cache.question.removeClass("hidden").animate({ opacity: 1 }, 700);
    $(".finder-question").hide();
    $('.finder-back-button').hide();
    var keys = Object.keys($previousAnswers.answers);
    if (keys.length > 0) {
        for (var i = 0, j = $cache.question.length; i < j; i++) {
            if ((i + 1) == $currentQuestion) {
                $cache.question.eq($currentQuestion - 1).addClass("current").removeClass("hidden").animate({opacity: 1}, 700);
                $cache.question.eq($currentQuestion - 2).addClass("hidden");
                $cache.question.eq($currentQuestion - 1).find(".finder-question").show();
                //set progress bubble
                $cache.progress.eq($currentQuestion - 1).addClass("active");
                $cache.question.eq($currentQuestion).removeClass("hidden").animate({opacity: 1}, 700);
                $cache.question.eq($currentQuestion).find(".finder-question").show();

            } else if ((i + 1) < $currentQuestion) {
                $cache.question.eq(i).hide();
                for (a = 0, b = keys.length; a < b; a++) {
                    $category = keys[a];
                    $answerKey = $previousAnswers.answers[keys[a]];
                    var $answer = "";
                    $answers = $("li.answer");
                    for (var y = 0, z = $answers.length; y < z; y++) {
                        if ($answers.eq(y).data("answer") == $answerKey) {
                            $answer = $answers.eq(y).data("readable");
                            y = z;
                        }
                    }
                    $thisQuestion = $(".finder-question-box").find("div#" + $category);
                    $thisProgressBubble = $(".progress-steps").find("#" + (a + 1).toString());
                    if ($cache.question.eq(i).find("div#" + $category).length > 0 ||
                        $cache.question.eq(i).find("ul#" + $category).length > 0 ||
                        $cache.question.eq(i).find("div#" + $category + "-ID").length > 0) {
                        Results[i] = $category + '=' + $answerKey;
                        Titles[i] = $("#" + (i + 1).toString()).attr("title") + ' : ' + $answer + '|';
                        if ($cache.question.eq(i).find("select").length > 0) {
                            $cache.question.eq(i).find("select").val($answerKey);
                            $refinements = $cache.question.eq(i).find("select").find("option:selected").data("refinements");
                        } else if ($cache.question.eq(i).find("input").length > 0) {
                            $refinements = "zipcode|" + $answerKey;
                        } else {
                            var liAnswers = $cache.question.eq(i).find("li.answer");
                            for (e = 0, f = liAnswers.length; e < f; e++) {
                                var liAttributes = liAnswers[e].attributes;
                                for (y = 0, z = liAttributes.length; y < z; y++) {
                                    if (liAttributes[y].value == $answerKey) {
                                        for (w = 0; w < z; w++) {
                                            if (liAttributes[w].name == "data-refinements") {
                                                $refinements = liAttributes[w].value;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        Refinements[i] = $refinements;
                        //$("#"+(i+1).toString()).attr("title", ($("#"+(i+1)).data("title")  + " " + $answer) );
                        $("#" + (i + 1).toString()).attr("data-title", ($("#" + (i + 1)).data("title") + " " + $answer));
                        $cache.progress.eq(i).addClass("answered");
                        $cache.progress.eq(i).find("img").css("display", "block");
                        activeStep++;
                    }

                }

            }
            $cache.question.eq($currentQuestion - 1).show();
            //$(".progress-bubbles").tooltip(tooltipoptions);
        }

    } else {
        $cache.question.eq(0).addClass("current").removeClass("hidden").animate({opacity: 1}, 700);
        $cache.question.eq(0).find(".finder-question").show();
        //set progress bubble
        $cache.progress.eq(0).addClass("active");
    }
}
function getDefaultAvailabilityMessageAjax(pid, targatedProductElement) {
	if (targatedProductElement!=undefined && targatedProductElement.length > 0) {
		var url = util.appendParamsToUrl(Urls.getDefaultAvailabilityMessage, {productId: pid});
		targatedProductElement.load(url, function () {
		});
	}
}

function loadATPAvailabilityFinderV3Message() {
	//.atp-required-product we are using this element class to call the ATP of the product, This product
	//class only exist on the element which qualify for the ATP handled in back-end.
	// Criteria to qualify for the Atp. If Product is a variant and shipping information = core.
	var $atpProducts = $('.availability-web-finder.atp-required-product');
	$atpProducts.each(function() {
		var $selectedShippingBlock = $(this);
		$atpProducts.append('<div class="loader-indicator-atp"></div>');
		var pid = $selectedShippingBlock.find('.pid').val();
		var url = util.appendParamsToUrl(Urls.getATPAvailabilityMessageAjax, {productId: pid,qty: 1, format: 'ajax', atpContext:'finder'});
		var productShippingInformation = $selectedShippingBlock.attr("data-product-shipping-information");
		var customerZipCode = $selectedShippingBlock.attr("data-customer-zipcode");
		var $replaceMeBlock_ab = $selectedShippingBlock.find('#replace-me-finder');
		if (productShippingInformation && productShippingInformation == "core") {
			$.getJSON(url, function (data) {
				var fail = false;
				var msg = '';
				if (!data) {
					msg = Resources.BAD_RESPONSE;
					fail = true;
				} else if (!data.success) {
					fail = true;
				}
				if (fail) {
					// trigger tealium tag when ATP fails
					if(productShippingInformation && customerZipCode && productShippingInformation == "core") {
						//utag.link({ "eventCategory" : ["ATP"], "eventLabel" : ["PDP_" + pid + "_" + customerZipCode], "eventAction" : ["fail"] });
					}
					//show default ATP message AB
					if ($replaceMeBlock_ab != null && $replaceMeBlock_ab.length) {
						$replaceMeBlock_ab.parent().remove();
						getDefaultAvailabilityMessageAjax(pid, $selectedShippingBlock);
					}
					$selectedShippingBlock.find(".loader-indicator-atp").remove();
					//$error.html(msg);
					return;
				}
				if (data.success) {
					if ($replaceMeBlock_ab != null && $replaceMeBlock_ab.length) {
						if (data.availabilityClass) {
							$replaceMeBlock_ab.removeClass();
							$replaceMeBlock_ab.addClass(data.availabilityClass);
						}
					}
					if (data.showATPMessage) { /* !pdict.Product.master 
												&& !pdict.Product.variationGroup
												&& pdict.Product.custom.shippingInformation.toLowerCase() == 'core' 
												&& dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpAvailabilityMessage') 
												&& session.custom.customerZip} */
						if (data.availabilityDate != null && data.isInStock === true) {
							var availabilitySuccess_ab = Resources.ATP_AVAILABILITY_SUCCESS;
							var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
							//2018-04-05T00:00:00.000Z
							var dateTime = data.availabilityDate;
							var dateOnly = dateTime.split('T');
							var ymd = dateOnly[0].split('-');
							var date = ymd[2];
							var month = months[Number(ymd[1])-1];
							var availabilityDateString = month + ' ' + date;
							if (data.dateDifferenceString != '') {
								availabilitySuccess_ab = availabilitySuccess_ab.replace(" tempDate",': <span class="availability-date-string">' + data.dateDifferenceString + ' ' + availabilityDateString + '</span>');
							} else {
								availabilitySuccess_ab = availabilitySuccess_ab.replace(" tempDate",': <span class="availability-date-string">' +  availabilityDateString + '</span>');
							}
							if($replaceMeBlock_ab != null) {							
								$replaceMeBlock_ab.find('p').html(availabilitySuccess_ab);
							}							
						} else if ($replaceMeBlock_ab != null && $replaceMeBlock_ab.length) {
							$replaceMeBlock_ab.parent().remove();
							getDefaultAvailabilityMessageAjax(pid, $selectedShippingBlock);
						}
					} else if ($replaceMeBlock_ab != null && $replaceMeBlock_ab.length) {
						$replaceMeBlock_ab.parent().remove();
						getDefaultAvailabilityMessageAjax(pid, $selectedShippingBlock);
					}
					$selectedShippingBlock.find(".loader-indicator-atp").remove();
				}
			});
		} else {
			$selectedShippingBlock.find(".loader-indicator-atp").remove();
		}
	});
}
Finder = {
    init: function () {
      var $finderProductTiles = $('.finder-results-v3 .featured-product-v3');
      if ($finderProductTiles.length > 0) {
	       loadATPAvailabilityFinderV3Message();
      }
      if ($('#mfinder-results').length === 0) {

        initializeCache();
        initializeDOM();
        initializeEvents();
        //this.initProgressBar();
        this.initQuestionImageRows();
        this.setImageHeadingArray();
        this.preloadAnswerImages();
      }
      else {
        return false;
      }
    },
    initProgressBar: function () {
        var $screenWidth = $(document).width(),
            $steps = $(".progress-steps"),
            $bar = $("#progress-wrapper"),
            $line = $(".progress-line"),
            $stepLength = 0,
            $stepLineLength = 0,
            $barLength = 0,
            $lineLength = 0;
        if ($screenWidth > 500) {
            var wrapperwidth,wrapperinnerwidth;
            wrapperwidth = parseInt($(".finder-wrapper").css("width"));
            wrapperinnerwidth = wrapperwidth - parseInt($(".finder-wrapper").css("margin-left")) - parseInt($(".finder-wrapper").css("margin-right"));
            stepLength = parseInt(wrapperinnerwidth / ($steps.length));//68; //pixel width of each step in the progress bar
            stepLineLength = stepLength; //pixel width of background line needed for progress bar

            //set length for the overall bar by multiplying number of step nodes by width of each
            $barLength = $steps.length * stepLength;

            //set length for the line by multiplying number of step nodes by width needed to cover each node
            $lineLength = $steps.length * stepLineLength - (stepLength / 1);

            //set width for bar and line to accomodate the number of steps
            $steps.css("width", stepLength);
            $bar.css("width", $(".finder-wrapper").css("width"));
            $line.css("width", $lineLength);
            }else {
            $stepLength = 100 / $steps.length; //get percentage that each node can occupy
            $steps.css("width", $stepLength + "%"); //set percentage width
        }
        $("#progress-wrapper").css("visibility", "visible");
    },
    initQuestionImageRows: function () {
        var widthOfUL = 0;
        //alert(parseInt($(window).width() ));
        if (parseInt($(window).width()) > 767) {
            widthOfUL = $(".finder-question-box.current ul.finder-question li").length * 196;
        } else {
            widthOfUL = "100%";
        }

        //$(".finder-question-box.current ul.finder-question").css("width",widthOfUL);
        //$(".finder-question-box.current ul.finder-question").css("margin","0 auto");
    },
    setImageHeadingArray: function () {
        $("li.answer").each(function (index) {
            preload.push($(this).data("imagelink"));
        });
    },
    preloadAnswerImages: function () {
        for (i = 0; i < preload.length; i++) {
            headingImages[i] = new Image()
            headingImages[i].src = preload[i]
        }
    }
};

module.exports = Finder;
