var util = require('./util'),
	dialog = require('./dialog'),
	progress = require('./progress'),
	ResponsiveSlots = require('./responsiveslots/responsiveSlots');

var refinementsCollection = [];
var refinementDivPosition;
var _ = require('lodash');
var tealiumPostedProucts = [];

function infiniteScroll() {
	// getting the hidden div, which is the placeholder for the next page
	var loadingPlaceHolder = $('.infinite-scroll-placeholder[data-loading-state="unloaded"]');
	// get url hidden in DOM
	var gridUrl = loadingPlaceHolder.attr('data-grid-url');

	if (loadingPlaceHolder.length === 1 && util.elementInViewport(loadingPlaceHolder.get(0), 250)) {
		// Update Multiple Pricing in URL if we have Prices Selected
		var pmax = '', pmin = '';
		var selectedPriceRefinements = $('a.price-refinement').closest('ul').find('li.selected');
		if (gridUrl.indexOf('pmax') > -1 && gridUrl.indexOf('pmin') > -1) {
			gridUrl = util.removeParamFromURL(gridUrl, 'pmax');
			gridUrl = util.removeParamFromURL(gridUrl, 'pmin');
		}
		$.each(selectedPriceRefinements, function (index, value) {
			var priceValues = $(this).find('a').data();
			pmin += (index == selectedPriceRefinements.length - 1) ? Number(priceValues.valuefrom) : Number(priceValues.valuefrom) + '|';
			pmax += (index == selectedPriceRefinements.length - 1) ? Number(priceValues.valueto) : Number(priceValues.valueto) + '|';
		});
		if (pmin != '' && pmax != '') {
			pmin = pmin.split('|').sort(function (a, b) { return a - b }).join('|');
			pmax = pmax.split('|').sort(function (a, b) { return b - a }).join('|');
			gridUrl = util.appendParamsToUrl(gridUrl, { 'pmax': pmax, 'pmin': pmin });
		}
		// switch state to 'loading'
		// - switches state, so the above selector is only matching once
		// - shows loading indicator
		loadingPlaceHolder.attr('data-loading-state', 'loading');
		loadingPlaceHolder.addClass('infinite-scroll-loading');


		// named wrapper function, which can either be called, if cache is hit, or ajax repsonse is received
		var fillEndlessScrollChunk = function (html) {
			loadingPlaceHolder.removeClass('infinite-scroll-loading');
			loadingPlaceHolder.attr('data-loading-state', 'loaded');
			$('div.MM-results_featured-products-list').append(html);
		};
		// else do query via ajax
		$.ajax({
			type: 'GET',
			dataType: 'html',
			url: util.appendParamsToUrl(gridUrl, util.getQueryStringParams(util.getQueryString(window.location.href))),
			success: function (response) {
				// put response into cache
				try {
					//sessionStorage['scroll-cache_' + gridUrl] = response;
				} catch (e) {
					// nothing to catch in case of out of memory of session storage
					// it will fall back to load via ajax
				}
				// update UI
				fillEndlessScrollChunk(response);
				// Intialize Tile Specific Events Here
				loadATPAvailabilityFinderV3Message();
				initExpanderContainer();
				submitProductTag();
			}
		});
	}
}
/**
 * @private
 * @function
 * @description replaces breadcrumbs, lefthand nav and product listing with ajax and puts a loading indicator over the product listing
 */
function updateProductListing(url, isStopEventTriggered, isClearAll) {

	// Clear All will have same URL thats why need flag true to pass below check
	var isClearAll = (typeof (isClearAll) != "undefined" && isClearAll == true);
	if ((!url || url === sessionStorage['refinedURLState']) && !isClearAll) {
		return;
	}

	progress.show();

	var filterOpen = false;
	if ($('.refinements').find('#nav_label').prop('checked')) {
		filterOpen = true;
	}

	var currentURL = window.location.href;

	//store all classes
	refinementsCollection = [];
	$(".refinement-big-container .refinement").each(function (index) {
		var refinementObj = {};
		refinementObj.divId = $(this).attr('id');
		refinementObj.existingClass = $(this).find('h3').attr('class');
		refinementObj.displayBlock = $(this).find('ul').css('display');
		refinementsCollection.push(refinementObj);
	});
	refinementDivPosition = $(".refinements").scrollTop();
	url = util.appendParamsToUrl(url, util.getQueryStringParams(util.getQueryString(window.location.href)));
	$('#main.finder-main .MM-results-wrapper').load(util.appendParamsToUrl(url, { 'format': 'ajax', 'element': 'searchgrid' }), function () {
		//get site name
		if (filterOpen) {
			$('.refinements').find('#nav_label').click();
			var _closeButton = $('.refinement-big-container').find('div.close-reminement');
			if (_closeButton.hasClass('sticky')) {
				_closeButton.removeClass('sticky');
			} else {
				_closeButton.addClass('sticky');
			}
		}
		initContainerHeight();
		initRefinementButtons(isStopEventTriggered);
		progress.hide();
		// util.uniform();
		//history.pushState(undefined, '', url);
		CloseRefinement();
		// Making ATP Call on updating Grid
		loadATPAvailabilityFinderV3Message();
		//Intialize Try in Store and Tealium on Update Products Grid
		initExpanderContainer();
		// maintaining state for refinedURL
		sessionStorage['refinedURLState'] = url;
		// in Case we dont get any results then reset refinement and grid urls.
		if ($('input[name=isSearchResultEmpty]').length > 0 && $('input[name=isSearchResultEmpty]').val() == "true") {
			sessionStorage['refinedURLState'] = window.location.href;
			sessionStorage['currentRefinementURLState'] = window.location.href;
		}
	});
	// end test
}

function updateRefinements(url, isStopEventTriggered, isClearAll) {
	var isClearAll = (typeof (isClearAll) != "undefined" && isClearAll == true);
	if ((!url || url === sessionStorage['currentRefinementURLState']) && !isClearAll) {
		return;
	}

	progress.show();


	var filterOpen = false;
	if ($('#nav_label').prop('checked')) {
		filterOpen = true;
	}

	var currentURL = window.location.href;

	//store all classes
	refinementsCollection = [];
	$(".refinement-big-container .refinement").each(function (index) {
		var refinementObj = {};
		refinementObj.divId = $(this).attr('id');
		refinementObj.existingClass = $(this).find('h3').attr('class');
		refinementObj.displayBlock = $(this).find('ul').css('display');
		refinementsCollection.push(refinementObj);
	});
	refinementDivPosition = $(".refinements").scrollTop();
	// In Case of Mobile MVC PLP Update only Refinement Panel.
	url = util.appendParamsToUrl(url, util.getQueryStringParams(util.getQueryString(window.location.href)));
	$.ajax({
		url: util.appendParamsToUrl(url, { 'format': 'ajax', 'element': 'searchgrid' }),
		type: "GET",
		dataType: "html"
	}).done(function (data) {
		//Save Refinement URL State
		sessionStorage['currentRefinementURLState'] = url;
		if ($(data).find('.mobile-refinements-filter__inner').length > 0) {
			$('#main.finder-main .refinements .mobile-refinements-filter__inner').html($(data).find('.mobile-refinements-filter__inner').html());
			//get site name
			if (filterOpen) {
				var _closeButton = $('.refinement-big-container').find('div.close-reminement');
				if (_closeButton.hasClass('sticky')) {
					_closeButton.fadeTo(300, 0, function () {
						$(this).removeClass('sticky');
					});
				} else {
					_closeButton.addClass('sticky');
				}
			}
			initContainerHeight();
			initRefinementButtons(isStopEventTriggered);
			progress.hide();
			// util.uniform();
			//history.pushState(undefined, '', url);
			CloseRefinement();
		}
	});
	/*$('#main.finder-main .refinements .mobile-refinements-filter').load(util.appendParamToURL(url, 'format', 'ajax', 'element', 'refinements') + " .mobile-refinements-filter__inner", function () {
		//Save Refinement URL State
		sessionStorage['currentRefinementURLState'] = url;
		//get site name
		if (filterOpen) {
			$('#nav_label').click();
			var _closeButton = $('.refinement-big-container').find('div.close-reminement');
			if (_closeButton.hasClass('sticky')) {
				_closeButton.removeClass('sticky');
			} else {
				_closeButton.addClass('sticky');
			}
		}
		initContainerHeight();
		initRefinementButtons(isStopEventTriggered);
		progress.hide();
		// util.uniform();
		//history.pushState(undefined, '', url);
		CloseRefinement();
	});*/
	// end test
}

/**
 * @private
 * @function
 * @description initialize refinement buttons for mobile
 */
function initRefinementButtons(isStopEventTriggered) {
	var _closeButton = $('.refinement-big-container').find('div.close-reminement');
	$('.refinement-big-container label.nav-title').on('click', function (e) {
		if ($(window).width() < 768) {
			// Show mobile filter view
			if (!$('#mobile-refinements-filter').hasClass('show-mobile-refinements-filter')) {
				$('#mobile-refinements-filter').addClass('show-mobile-refinements-filter');
				if ($('.overlay').length) {
					$('.overlay').css('display', 'none');
				}
				$(window).scrollTop(0);
			}

			if (_closeButton.hasClass('sticky')) {
				_closeButton.removeClass('sticky');
			} else {
				_closeButton.addClass('sticky');
			}
		} else {
			// Show desktop filter view
			if (!$('#mobile-refinements-filter').hasClass('show-desktop-refinements-filter')) {
				$('#mobile-refinements-filter').addClass('show-desktop-refinements-filter show-desktop-refinements-filter__drawer__open');
				$(window).scrollTop(0);
			}
		}
		fireUtag('Sort & Filter', 'Products')
	});

	$(window).on('scroll', function () {
		if ($('.refinement-big-container nav').height() > 0) {
			var containerHeight = parseInt($('.refinement-big-container nav')[0].scrollHeight);
			var position = parseInt(containerHeight);
			if (parseInt($(document).scrollTop()) >= position) {
				_closeButton.addClass('relative');
			} else {
				_closeButton.removeClass('relative');
			}
		}
	});
	// Refinements Truncate functionality
	$(".hideRefinement").hide();
	$(".refinements-see-less").hide();
	$(".refinements-see-more").on("click", function () {
		/*
		 * MAT-1733 | Astound Sprint 3 - [PLP] Mattress Filters
		 * If you select a value within a collapsed facet the facet should stay open to the “see more” for that filter.
		 */
		var filterId = $(this).parents('ul').parent().attr('id');	// ID of Filter/Refinement to which this "see more" belongs
		if (sessionStorage.getItem('keepFacetsOpenForRefinements') == null) {
			sessionStorage.setItem('keepFacetsOpenForRefinements', filterId);
		} else {
			if (sessionStorage.getItem('keepFacetsOpenForRefinements').indexOf(filterId) == -1) { // if this filter already does not exists in the session variable
				var allFilterIds = sessionStorage.getItem('keepFacetsOpenForRefinements').split(',');
				allFilterIds.push(filterId);
				sessionStorage.setItem('keepFacetsOpenForRefinements', allFilterIds);
			}
		}

		$(this).parents('ul').find(".hideRefinement").show();
		$(this).parents('ul').find(".refinements-see-less").show();
		$(this).hide();
	});
	$(".refinements-see-less").on("click", function () {
		/*
		 * MAT-1733 | Astound Sprint 3 - [PLP] Mattress Filters
		 * If you select a value within a collapsed facet the facet should stay open to the “see more” for that filter.
		 */
		var filterId = $(this).parents('ul').parent().attr('id');	// ID of Filter/Refinement to which this "see more" belongs
		if (sessionStorage.getItem('keepFacetsOpenForRefinements') != null) {
			if (sessionStorage.getItem('keepFacetsOpenForRefinements').indexOf(filterId) != -1) { // if this exists then remove it
				var allFilterIds = sessionStorage.getItem('keepFacetsOpenForRefinements').split(',');
				var filterIndex = allFilterIds.indexOf(filterId);
				if (filterIndex > -1) {
					allFilterIds.splice(filterIndex, 1); // Remove this element
					sessionStorage.setItem('keepFacetsOpenForRefinements', allFilterIds);
				}
			}
		}

		$(this).parents('ul').find(".hideRefinement").hide();
		$(this).parents('ul').find(".refinements-see-more").show();
		$(this).hide();

	});
	// Keep the Facets open those were expanded by the Customer
	openFacets();
}

$(document).ready(function (e) {
	openFacets();
});

function openFacets() {
	if (sessionStorage.getItem('keepFacetsOpenForRefinements') != null) {
		var openFacets = sessionStorage.getItem('keepFacetsOpenForRefinements').split(',');
		for (var i = 0; i < openFacets.length; i++) {
			if (openFacets[i]) {
				$('#' + openFacets[i] + ' h3').removeClass('expanded-title');
				$('#' + openFacets[i] + ' ul').css("display", "block");
				if ($('#' + openFacets[i]).find('.refinements-see-more').length) {
					$('#' + openFacets[i]).find('.refinements-see-more').click();
				}
			}

		}
	}


	//set refinement classes
	for (var x = 0; x < refinementsCollection.length; x++) {
		var jObj = refinementsCollection[x];
		var elemId = jObj.divId;
		var elemClass = jObj.existingClass;
		var diplayProp = jObj.displayBlock;
		if (elemId) {
			$('#' + elemId + ' h3').removeClass();
			$('#' + elemId + ' h3').addClass(elemClass);
			$('#' + elemId + ' ul').css("display", diplayProp);
		}
	}

	$(".refinements").scrollTop(refinementDivPosition);
}


/**
 * @private
 * @function
 * @description Set min-height of sub-category container based on height of filters
 */
function initContainerHeight() {
	if ($('.refinements').length > 0 && parseInt($(window).width()) > 767) {
		var filterNavHeight = $('.refinements').height();
		$('#primary').css('min-height', filterNavHeight + 'px');
	}
}

function CloseRefinement() {
	$('#apply-filter-btn').on('click', function () {
		$("body").removeClass('mobile-refinements-filter-show');
		$('#nav_label').click();
		var container = $('.refinement-big-container').offset().top;
		$('html,body').animate({
			scrollTop: container
		}, 'slow');

		$('.show-desktop-refinements-filter').removeClass('show-desktop-refinements-filter__drawer__open');
		$('body').removeClass('scrollHidden');
		$('html').removeClass('scrollHidden');
		if ($('.overlay').length) {
			$('.overlay').css('display', 'none');
		}

		var closeButton = $('.refinement-big-container').find('div.close-reminement.sticky');
		if (closeButton.length > 0) {
			closeButton.removeClass('sticky');
		}

		if (typeof (sessionStorage['currentRefinementURLState']) != undefined && sessionStorage['currentRefinementURLState'] != sessionStorage['refinedURLState']) {
			updateProductListing(sessionStorage['currentRefinementURLState']);
		}
		//$('#mobile-refinements-filter').removeClass('show-mobile-refinements-filter');
	});
}

function desktopRefinementsDrawerOpener() {
	$(document).on('click', '.nav-title', function () {
		if (!$('.show-desktop-refinements-filter').hasClass('show-desktop-refinements-filter__drawer__open')) {
			$('.show-desktop-refinements-filter').addClass('show-desktop-refinements-filter__drawer__open');
		}
		if ($(window).width() > 767) {
			if ($('.overlay').length) {
				$('.overlay').css('display', 'block');
				$('body').addClass('scrollHidden');
				$('html').addClass('scrollHidden');
			} else {
				$('#main').append('<div class="overlay" style="display: block;"></div>');
				onCloseOverlay();
			}

		}
	});
}

function onCloseOverlay() {
	$('.overlay').on('click', function () {
		$('body').removeClass('scrollHidden');
		$('html').removeClass('scrollHidden');
		$('.thank-you-close-button').css('display', 'none');
		$('.show-desktop-refinements-filter').removeClass('show-desktop-refinements-filter__drawer__open');
		$('.MM-product__drawer').removeClass('MM-product__drawer__open');
		$('.mattress-email-results-dialog').css('display', 'none');
		// Close email modal signup on mobile
		$('.email-footer').addClass('slide-down', 1000);
		$('.email-footer').removeClass('slide-up');
		setTimeout(function () {
			$('.overlay').css('display', 'none');
			$('.email-footer__slider').removeClass('collapsed');
		}, 1000);
		if ($('.overlay').length) {
			$('.overlay').css('display', 'none');
		}
	});
}

function setDesktopRefinementsDrawer() {
	//$(".nav-title").trigger('click');
	if ($(window).width() > 767) {
		if (!$('#mobile-refinements-filter').hasClass('show-desktop-refinements-filter')) {
			$('#mobile-refinements-filter').removeClass('show-mobile-refinements-filter');
			$('#mobile-refinements-filter').addClass('show-desktop-refinements-filter');
		}
	} else {
		if ($('#mobile-refinements-filter').hasClass('show-desktop-refinements-filter')) {
			$('#mobile-refinements-filter').removeClass('show-desktop-refinements-filter');
		}
	}
}


/**
 * @private
 * @function
 * @description Initializes events for the following elements:<br/>
 * <p>refinement blocks</p>
 * <p>updating grid: refinements, pagination, breadcrumb</p>
 * <p>item click</p>
 * <p>sorting changes</p>
 */
function initializeEvents() {
	var $main = $('#main.finder-main');
	// compare checked

	// handle toggle refinement blocks
	$main.on('click', '.refinement h3', function () {
		$(this).toggleClass('expanded-title')
			.siblings('ul').toggle()
			.siblings('.refinement-top-level').toggle();

		if (!$(this).hasClass('expanded-title')) {
			$(this).closest('.refinement').removeClass('collapsed-refinement').addClass('expanded-refinement');
		} else {
			$(this).closest('.refinement').removeClass('expanded-refinement').addClass('collapsed-refinement');
		}
	});

	/*  */
	// handle toggle refinement blocks on enter key press
	$main.on('keypress', '.refinement h3', function (ev) {
		ev.preventDefault();
		if (ev.keyCode == 13 || ev.which == 13) {
			$(this).toggleClass('expanded-title')
				.siblings('ul').toggle()
				.siblings('.refinement-top-level').toggle();
		}
	});


	// handle events for updating grid
	$main.on('click', '.refinements a, .pagination a, .breadcrumb-refinement-value a, a.selected-filter,.selected-filters-mvc a.selected-filters__clear-all', function (e) {

		// don't intercept for category and folder refinements, as well as unselectable
		if ($(this).parents('.category-refinement').length > 0 || $(this).parents('.folder-refinement').length > 0 || $(this).parent().hasClass('unselectable')) {
			return;
		}
		e.preventDefault();
		/*
		* This fixes the issue of the main site loading into the #main div when no filters are selected (MAT-518)
		*/
		var selectedBreadcrumbs = $('span.breadcrumb-refinement-value').length;
		var selectedRefinements = $('div.refinement ul li.selected').length;
		// handle breadcrumbs
		if ($(e.target).hasClass('breadcrumb-relax')) {
			if (selectedBreadcrumbs === 1) {
				var url = $('span.breadcrumb-refinement-value a').attr('href');
				updateProductListing(url);
				return;
			}
		}

		if ($(this).hasClass('selected-filters__clear-all')) { 	//Handle Clear All Filter on Selected Filters
			updateProductListing(window.location.href, undefined, true);
			fireUtag('Clear All', 'Products');
			return;
		} else if ($(this).hasClass('breadcrumb-refinement-clear-all')) { //Handle Clear All Filter on Refinement Panel
			if (sessionStorage['currentRefinementURLState'] != window.location.href) {
				updateRefinements(window.location.href, undefined, true);
				fireUtag('Clear All', 'Products');
			}
			return;
		}

		//Update Pricing in action URL in Case for multiple selection
		if (!$(this).hasClass('selected-filters__clear-all')) {
			var pmax = '', pmin = '';
			if ($(this).hasClass('breadcrumb-relax-filters') && $(this).hasClass('price-relax')) {
				if (this.href.indexOf('pmax') > -1 && this.href.indexOf('pmin') > -1) {
					this.href = util.removeParamFromURL(this.href, 'pmax');
					this.href = util.removeParamFromURL(this.href, 'pmin');
				}
				var selectedPriceRanges = $(this).siblings('a.price-relax');
				if (selectedPriceRanges.length > 0) {
					$.each(selectedPriceRanges, function (index, value) {
						var priceValues = $(this).data();
						pmin += (index == selectedPriceRanges.length - 1) ? Number(priceValues.valuefrom) : Number(priceValues.valuefrom) + '|';
						pmax += (index == selectedPriceRanges.length - 1) ? Number(priceValues.valueto) : Number(priceValues.valueto) + '|';
					});
				}
			}
			if ($(this).hasClass('price-refinement')) { // In Case of Price Refinement Selection
				var currentRefinementMin = Number($(this).data('valuefrom')),
					currentRefinementMax = Number($(this).data('valueto'));
				if (this.href.indexOf('pmax') > -1 && this.href.indexOf('pmin') > -1) {
					this.href = util.removeParamFromURL(this.href, 'pmax');
					this.href = util.removeParamFromURL(this.href, 'pmin');
				}
				var $currentPrice = $(this).closest('li');
				var selectedPriceRanges = $currentPrice.siblings('li.selected');
				if ($currentPrice.hasClass('selected')) { //In Case Unselect Price Refinement, Exclude Selected Price Refinement from URL
					$.each(selectedPriceRanges, function (index, value) {
						var priceValues = $(this).find('a').data();
						pmin += (index == selectedPriceRanges.length - 1) ? Number(priceValues.valuefrom) : Number(priceValues.valuefrom) + '|';
						pmax += (index == selectedPriceRanges.length - 1) ? Number(priceValues.valueto) : Number(priceValues.valueto) + '|';
					});
				} else { // In Case new Price Range Selection
					$.each(selectedPriceRanges, function (index, value) {
						var priceValues = $(this).find('a').data();
						pmin += (index == selectedPriceRanges.length - 1) ? Number(priceValues.valuefrom) : Number(priceValues.valuefrom) + '|';
						pmax += (index == selectedPriceRanges.length - 1) ? Number(priceValues.valueto) : Number(priceValues.valueto) + '|';
					});
					pmin += (pmin.length > 0) ? '|' + currentRefinementMin : currentRefinementMin;
					pmax += (pmax.length > 0) ? '|' + currentRefinementMax : currentRefinementMax;
				}
			} else if (!$(this).hasClass('price-relax') && $('a.price-refinement').length > 0 && $('a.price-refinement').closest('ul').find('li.selected').length > 0) { // in Case any other price selection to make sure selected prices not miss in search Query URL
				var selectedPriceRefinements = $('a.price-refinement').closest('ul').find('li.selected');
				if (this.href.indexOf('pmax') > -1 && this.href.indexOf('pmin') > -1) {
					this.href = util.removeParamFromURL(this.href, 'pmax');
					this.href = util.removeParamFromURL(this.href, 'pmin');
				}
				$.each(selectedPriceRefinements, function (index, value) {
					var priceValues = $(this).find('a').data();
					pmin += (index == selectedPriceRefinements.length - 1) ? Number(priceValues.valuefrom) : Number(priceValues.valuefrom) + '|';
					pmax += (index == selectedPriceRefinements.length - 1) ? Number(priceValues.valueto) : Number(priceValues.valueto) + '|';
				});
			}
			if (pmin != '' && pmax != '') {
				pmin = pmin.split('|').sort(function (a, b) { return a - b }).join('|');
				pmax = pmax.split('|').sort(function (a, b) { return b - a }).join('|');
				this.href = util.appendParamsToUrl(this.href, { 'pmax': pmax, 'pmin': pmin });
			}
		}
		// handle refinements
		var target = $(e.target).closest('li');
		if (target.hasClass('selected') && selectedRefinements === 1) {
			updateRefinements(this.href);//window.location = this.href;
			return;
		}
		/* End MAT-518 fix */

		// Handle Relax Refinements PLP Filters MVC
		if ($(this).hasClass('breadcrumb-relax-filters')) {
			updateProductListing(this.href, undefined, true);
			fireUtag('Remove Filter Click', 'Products')
		} else {
			updateRefinements(this.href);
		}
		if ($(this).closest('.search-result-options.bottom').length > 0) {
			window.location.hash = '#primary';
		}
		//window.location.href = this.href;
	});

	// handle sorting change
	$main.on('change', '.sort-by select', function (e) {
		e.preventDefault();
		var sortTargetUrl = $(this).find('option:selected').val();
		var pmax = '', pmin = '';
		var selectedPriceRefinements = $('a.price-refinement').closest('ul').find('li.selected');
		if (sortTargetUrl.indexOf('pmax') > -1 && sortTargetUrl.indexOf('pmin') > -1) {
			sortTargetUrl = util.removeParamFromURL(sortTargetUrl, 'pmax');
			sortTargetUrl = util.removeParamFromURL(sortTargetUrl, 'pmin');
		}
		$.each(selectedPriceRefinements, function (index, value) {
			var priceValues = $(this).find('a').data();
			pmin += (index == selectedPriceRefinements.length - 1) ? Number(priceValues.valuefrom) : Number(priceValues.valuefrom) + '|';
			pmax += (index == selectedPriceRefinements.length - 1) ? Number(priceValues.valueto) : Number(priceValues.valueto) + '|';
		});
		if (pmin != '' && pmax != '') {
			pmin = pmin.split('|').sort(function (a, b) { return a - b }).join('|');
			pmax = pmax.split('|').sort(function (a, b) { return b - a }).join('|');
			sortTargetUrl = util.appendParamsToUrl(sortTargetUrl, { 'pmax': pmax, 'pmin': pmin });
		}
		updateRefinements(sortTargetUrl);
	});

	function onShowSelectedFilters() {
		if ($('.selected-filters').length) {
			$('.results-hits-total').css({ 'position': 'absolute', 'bottom': '7px' })
		}
	}

	function onClickHideMobileFilters() {
		$main.on('click', '.close-filter', function (e) {
			e.preventDefault();
			$(".nav-title").trigger('click');
			$('.show-desktop-refinements-filter').removeClass('show-desktop-refinements-filter__drawer__open');
			if ($('.overlay').length) {
				$('.overlay').css('display', 'none');
			}
			$('body').removeClass('scrollHidden');
			$('html').removeClass('scrollHidden');
			// in Case if User Click Close Filter, Retail User Previous Selection if User Change any thing
			var currentURLPLPMVC = window.location.href;
			if (sessionStorage['refinedURLState'] != sessionStorage['currentRefinementURLState']) {
				updateRefinements(sessionStorage['refinedURLState'], undefined);
			}
		});
	}

	$('.refinement-big-container nav .refinement').last().addClass('refinement-last');
	CloseRefinement();
	onShowSelectedFilters();
	onClickHideMobileFilters();

}

function getDefaultAvailabilityMessageAjax(pid, targatedProductElement) {
	if (targatedProductElement != undefined && targatedProductElement.length > 0) {
		var url = util.appendParamsToUrl(Urls.getDefaultAvailabilityMessage, { productId: pid });
		targatedProductElement.load(url, function () {
		});
	}
}

function loadATPAvailabilityFinderV3Message() {
	//.atp-required-product we are using this element class to call the ATP of the product, This product
	//class only exist on the element which qualify for the ATP handled in back-end.
	// Criteria to qualify for the Atp. If Product is a variant and shipping information = core.
	var $atpProducts = $('.availability-web-finder.atp-required-product:not(.loaded)');
	$atpProducts.each(function () {
		var $selectedShippingBlock = $(this);
		$atpProducts.append('<div class="loader-indicator-atp"></div>');
		var pid = $selectedShippingBlock.find('.pid').val();
		var url = util.appendParamsToUrl(Urls.getATPAvailabilityMessageAjax, { productId: pid, qty: 1, format: 'ajax', atpContext: 'finder' });
		var productShippingInformation = $selectedShippingBlock.attr("data-product-shipping-information");
		var customerZipCode = $selectedShippingBlock.attr("data-customer-zipcode");
		var $replaceMeBlock_ab = $selectedShippingBlock.find('#replace-me-finder');
		if (productShippingInformation && productShippingInformation == "core") {
			$.getJSON(url, function (data) {
				var fail = false;
				var msg = '';
				if (!data) {
					msg = Resources.BAD_RESPONSE;
					fail = true;
				} else if (!data.success) {
					fail = true;
				}
				if (fail) {
					// trigger tealium tag when ATP fails
					if (productShippingInformation && customerZipCode && productShippingInformation == "core") {
						//utag.link({ "eventCategory" : ["ATP"], "eventLabel" : ["PDP_" + pid + "_" + customerZipCode], "eventAction" : ["fail"] });
					}
					//show default ATP message AB
					if ($replaceMeBlock_ab != null && $replaceMeBlock_ab.length) {
						$replaceMeBlock_ab.parent().remove();
						getDefaultAvailabilityMessageAjax(pid, $selectedShippingBlock);
					}
					$selectedShippingBlock.find(".loader-indicator-atp").remove();
					//$error.html(msg);
					return;
				}
				if (data.success) {
					if ($replaceMeBlock_ab != null && $replaceMeBlock_ab.length) {
						if (data.availabilityClass) {
							$replaceMeBlock_ab.removeClass();
							$replaceMeBlock_ab.addClass(data.availabilityClass);
						}
					}
					if (data.showATPMessage) { /* !pdict.Product.master 
													&& !pdict.Product.variationGroup
													&& pdict.Product.custom.shippingInformation.toLowerCase() == 'core' 
													&& dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpAvailabilityMessage') 
													&& session.custom.customerZip} */
						if (data.availabilityDate != null && data.isInStock === true) {
							var availabilitySuccess_ab = Resources.ATP_AVAILABILITY_SUCCESS;
							var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
							//2018-04-05T00:00:00.000Z
							var dateTime = data.availabilityDate;
							var dateOnly = dateTime.split('T');
							var ymd = dateOnly[0].split('-');
							var date = ymd[2];
							var month = months[Number(ymd[1]) - 1];
							var availabilityDateString = month + ' ' + date;
							if (data.dateDifferenceString != '') {
								availabilitySuccess_ab = availabilitySuccess_ab.replace(" tempDate", ': <span class="availability-date-string">' + data.dateDifferenceString + ' ' + availabilityDateString + '</span>');
							} else {
								availabilitySuccess_ab = availabilitySuccess_ab.replace(" tempDate", ': <span class="availability-date-string">' + availabilityDateString + '</span>');
							}
							if ($replaceMeBlock_ab != null) {
								$replaceMeBlock_ab.find('p').html(availabilitySuccess_ab);
							}
						} else if ($replaceMeBlock_ab != null && $replaceMeBlock_ab.length) {
							$replaceMeBlock_ab.parent().remove();
							getDefaultAvailabilityMessageAjax(pid, $selectedShippingBlock);
						}
					} else if ($replaceMeBlock_ab != null && $replaceMeBlock_ab.length) {
						$replaceMeBlock_ab.parent().remove();
						getDefaultAvailabilityMessageAjax(pid, $selectedShippingBlock);
					}
					$selectedShippingBlock.find(".loader-indicator-atp").remove();
				}
			});
		} else {
			$selectedShippingBlock.find(".loader-indicator-atp").remove();
		}
		$selectedShippingBlock.addClass('loaded');
	});
}



//Expand and collapse try in store when store is found
// Show hide overlay in mobile
function initExpanderContainer() {
	$('.try-in-store__header').off().on('click', function () {

		var $header = $(this),
			//getting the next element
			$content = $header.next(),
			$container = $header.closest('.expander-container'),
			$overlay = $('.ui-widget-overlay'),
			overlayHtml = '<div class="ui-widget-overlay ui-front expander-overlay" style="z-index: 100000;"></div>';

		if (!$container.hasClass('expanded')) {
			$container.addClass('expanded');
		} else {
			$container.removeClass('expanded');
		}

		function onOverlayClickSlideUp() {
			$('.expander-overlay').on('click', function () {
				$header.click();
			});
		}

		function onResizeManageOverlay() {
			if ($container.hasClass('expanded')) {
				$container.removeClass('expanded');
				if ($('.expander-overlay').length) {
					$('.expander-overlay').click();
				}
			}
		}

		function callOnViewportChange() {
			// onResizeManageOverlay();
		}

		function changeOnResize() {
			// Debouncing greedy resize events.
			var onWindowResize = debounce(function () {
				callOnViewportChange();
			}, 250);

			$(window).on('resize', onWindowResize);
		}

		function changeOnOrientationChange() {
			if ("onorientationchange" in window) {
				var onOrientationChange = debounce(function () {
					callOnViewportChange();
				}, 250);

				$(window).on('orientationchange', onOrientationChange);
			}
		}

		changeOnResize();
		changeOnOrientationChange();

		// Only run if parent is expander-container
		if ($header.parents('.expander-container').length) {

			// Show overlay with expanded widget ontop if in mobile
			if ($overlay.length) {
				$overlay.detach();
				if ($container.hasClass('mobile-overlay')) {
					$container.removeClass('mobile-overlay')
				}
			} else {
				if ($(window).width() < 768) {
					$('body').append(overlayHtml);
					onOverlayClickSlideUp();
					if (!$container.hasClass('mobile-overlay')) {
						$container.addClass('mobile-overlay')
					}
				}
			}

			var ariaExp = ($container.hasClass('expanded')) ? 'true' : 'false';
			$header.find('a').attr('aria-expanded', ariaExp);
		}

		var tryinstorelabel = 'Try in-store - ' + Math.round($header.closest('.featured-product-v3').attr('data-tealium'));
		fireUtag(tryinstorelabel, 'Products');
	});
}

// Drawer opener for mobile only
function productBenefitsDrawerMobileOpener() {
	$('.MM-product-benefits__show-flyout-button').click(function () {
		$('body').addClass('scrollHidden');
		$('html').addClass('scrollHidden');
		$('.MM-product__drawer').addClass('MM-product__drawer__open');
		if ($('.overlay').length) {
			$('.overlay').css('display', 'block');
		} else {
			$('#main').append('<div class="overlay" style="display:block"></div>');
			onCloseOverlay();
		}
	});
	$('.MM-product__drawer__close-button').click(function () {
		$('body').removeClass('scrollHidden');
		$('html').removeClass('scrollHidden');
		$('.MM-product__drawer').removeClass('MM-product__drawer__open');
		$('.overlay').css('display', 'none');
	});
}


// Email slider goes down
function emailSlider_down() {
	$('.email-module-close-button').on('click', function () {
		$('.email-footer').addClass('slide-down', 1000);
		$('.email-footer').removeClass('slide-up');
		setTimeout(function () {
			$('.email-footer__slider').removeClass('collapsed');
		}, 1000);
		$('.overlay').css('display', 'none');
	});
}

// Email slider goes up
function emailSlider_up() {
	if ($('.email-footer').hasClass('slide-down')) {
		$('.email-footer__slider-button').click(function () {
			$('#sub-email-address').css('display', 'block');
			$('.email-footer').find('.email-module-wrapper').show();
			$('.email-footer').find('#result').hide();
			$('.exacttarget-email').val('');
			$('.email-footer').removeClass('slide-down');
			$('.email-footer').addClass('slide-up', 1000);
			setTimeout(function () {
				$('.overlay').css('display', 'block');
			}, 1000);
			$('.email-footer__slider').addClass('collapsed');
		});
	}
}

function saveCustomerProfileEvent() {
	$(document).on('focus', 'input.exacttarget-email', function () {
		$(this).removeClass('error');
		$(this).attr('placeholder', "Email Address");
	});
	$('.exacttarget-email-button-mm').click(function (e) {
		e.preventDefault();
		var _exacttargetFormContainer = $('div.mattressfinder-desktop-popup');
		var _exacttargetEmailInput = $('.exacttarget-email');
		var valid = exactTargetValidation(_exacttargetEmailInput);
		if (valid) {
			$('.loader-icon').css('display','block');
			var email = _exacttargetEmailInput.val();
			var _gclid = getCookie("_ga");
			var _leadSource = $(".exacttarget-form-container").find('#footer-social-leadSource').val();
			var _optOutFlag = ($('.exacttarget-form-container__signup-checkbox').is(':checked')) ? 1 : 0;
			var tealiumVisitorID = "";
			var googleClientID = "";
			var tealiumSessionID = "";
			if(utag_data != null){
				tealiumVisitorID = utag_data.tealium_visitor_id ;
				googleClientID = utag_data.clientID_ga;
				tealiumSessionID = utag_data.tealium_session_id;
			}

			var _exactTargetForm = _exacttargetFormContainer.find('form.exacttargetform');
			//if(_exactTargetForm.length > 0){
			_exactTargetForm.hide();
			$.ajax({
				url: Urls.saveCustomerProfile,
				data: { emailID: email, googleClientID: _gclid, optOut: _optOutFlag, lead: _leadSource, tealiumVisitorID:tealiumVisitorID, googleClientID:googleClientID, tealiumSessionID:tealiumSessionID },
				type: 'get',
				success: function (data) {
					$('.loader-icon').css('display','none');
					if ($('#isMobileDevice').length && $('#isMobileDevice').val() == 'true') {
						$('.email-footer.slide-up').height('249');
						$('.email-footer').find('.email-module-wrapper').hide();
						$('.email-footer').find('#result').html(data).show();
						$('.thank-you-close-button').click(function () {
							$('.thank-you-close-button').css('display', 'none');
							$('.email-footer').addClass('slide-down', 1000);
							$('.email-footer').removeClass('slide-up');
							setTimeout(function () {
								$('.email-footer__slider').removeClass('collapsed');
							}, 1000);
							$('.overlay').css('display', 'none');
						});
					} else {
						_exacttargetFormContainer.html(data);
					}
				},
				error: function (errorThrown) {
					console.log(errorThrown);
				}
			});
			//}
			fireUtag('Email Submit', 'Email Prompt')
		}
	});
}

// Email modal 
function openMFEmailDesktopPopup() {
	if ($('#ShowSignUpPopup').length && $('#ShowSignUpPopup').val() == 'true' && $('#isMobileDevice').length && $('#isMobileDevice').val() == 'false') {
		$.ajax({
			url: Urls.getMMEmailContent,
			type: 'get',
			success: function (data) {
				dialog.open({
					html: data,
					options: {
						width: 575,
						dialogClass: 'mattress-email-results-dialog'
					}
				});
				
				saveCustomerProfileEvent();
				util.uniform();
			},
			error: function (errorThrown) {
				console.log(errorThrown);
			}
		});
	}
}

// Desktop email button

function emailPopUp() {
	$('.MM-main-banner__email-button').click(function () {
		fireUtag('Email Results', 'Email Results')
		$.ajax({
			url: Urls.getMMEmailContent,
			type: 'get',
			success: function (data) {
				dialog.open({
					html: data,
					options: {
						width: 575,
						dialogClass: 'mattress-email-results-dialog'
					}
				});
				saveCustomerProfileEvent();
				util.uniform();
			},
			error: function (errorThrown) {
				console.log(errorThrown);
			}
		});
	});
}

$(document).on('click','.ui-icon-closethick',function(){
	fireUtag('Email Close', 'Email Prompt')
});

$(document).on('change','#footer-social-optOutFlag',function(){
	if (!this.checked) {
		fireUtag('Email Signup Opt Out', 'Email Prompt')
	}
});

function edgeCaseSlider() {
	$(".MM-edge").on('click', function () {
		var $abc = $(this).find('.MM-product-benefits__title-message');
		$(this).toggleClass('expanded');
		$abc.slideDown();
		if ($(this).hasClass('expanded')) {
			$abc.slideDown();

		}
		else {
			$abc.slideUp();

		}

	});
}

function submitButtonInputRemoval() {
	$('#submit-button').click(function () {
		var _exacttargetEmailInput = $('.exacttarget-email');
		var valid = exactTargetValidation(_exacttargetEmailInput);
		if (valid) {
			$('#sub-email-address').css('display', 'none')
		}
	});
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function validateEmail(email) {
	var emailReg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return emailReg.test(email);
}

function exactTargetValidation(emailField) {
	var emailaddress = emailField.val();
	if (!validateEmail(emailaddress) || emailaddress == null || emailaddress == '') {
		if ($('.MM-new-main').length > 0) {
			emailField.addClass('error');
			emailField.val('');
			emailField.attr('placeholder', "Please enter a valid email");
		}
		else {
			emailField.addClass('error');
			emailField.val('');
			emailField.attr('placeholder', "Enter Valid Email");
		}
		return false;
	}
	return true;
}

MattressMatcher = {
	init: function () {
		$(window).on('scroll', infiniteScroll);
		productBenefitsDrawerMobileOpener();
		initProductBenefits();
		initOnWindowResize();
		initProductBenefitsSlickGrid();
		openMFEmailDesktopPopup();
		emailPopUp();
		emailSlider_down();
		emailSlider_up();
		edgeCaseSlider();
		submitButtonInputRemoval();
		initializeEvents();
		desktopRefinementsDrawerOpener();
		onCloseOverlay();
		setDesktopRefinementsDrawer();
		initContainerHeight();
		initRefinementButtons();
		sessionStorage['refinedURLState'] = window.location.href;
		sessionStorage['currentRefinementURLState'] = window.location.href;
		var $finderProductTiles = $('.finder-results-v3 .featured-product-v3');
		if ($finderProductTiles.length > 0) {
			loadATPAvailabilityFinderV3Message();
		}
		initExpanderContainer();

		saveCustomerProfileEvent();

	}
};

module.exports = MattressMatcher;

// debounce function
var debounce = function (func, wait, immediate) {
	var timeout;
	return function () {
		var context = this, args = arguments;
		var later = function () {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

// If more than one item is shown in mobile, hide all before resizing to 
// desktop and tablet.
function resetProductBenefits() {
	if ($('.item.active').length > 1) {
		$('.MM-product-benefits__item ').removeClass('active');
		$('.MM-product-benefits__desktop-nav-list').removeClass('has-active');
	}
}

// debounced onWindowResize: put any events that need to be done on resize here
var onWindowResize = debounce(function () {
	resetProductBenefits();
	setDesktopRefinementsDrawer();
}, 250);

function initOnWindowResize() {
	$(window).on('resize', onWindowResize);
}

function initProductBenefits() {
	$(document).on('click', '.MM-product-benefits__mobile-nav-link', function () {
		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
		} else {
			$(this).addClass('active');
		}
	});

	$(document).on('click', ".MM-product-benefits__inner-container a", function (e) {
		e.preventDefault();
		if($(this).hasClass('activeProductBenefit')){
			$('.MM-product-benefits__desktop-nav ul li').removeClass('active');
			if ($(e.target).is('.MM-product-benefits__desktop-nav a')) {

				if ($('.item.active').length === 0) {
					$(e.target).closest('.MM-product-benefits__desktop-nav-list').removeClass('has-active');
				} else {
					$(e.target).closest('.MM-product-benefits__desktop-nav-list').addClass('has-active');
				}

				$('.item').removeClass('active');
				$(this).removeClass('activeProductBenefit');
			} else {
				var currentLink = $(this).data('tab');
				$(currentLink).toggleClass('active');
			}
			
		} else {
			$('.MM-product-benefits__desktop-nav ul li').removeClass('active');

			// In desktop or tablet show only one tab at a time
			$(e.target).closest('li').addClass('active');
			if ($(e.target).is('.MM-product-benefits__desktop-nav a')) {

				if ($('.item.active').length === 0) {
					$(e.target).closest('.MM-product-benefits__desktop-nav-list').removeClass('has-active');
				} else {
					$(e.target).closest('.MM-product-benefits__desktop-nav-list').addClass('has-active');
				}

				$('.item').removeClass('active');	
			}

			var currentLink = $(this).data('tab');
			$(currentLink).toggleClass('active');
			$('.MM-product-benefits__desktop-nav-link').removeClass('activeProductBenefit');
			$(this).addClass('activeProductBenefit');
		}
		fireUtag($(this).find('h5').text(), 'Benefits');
	});

	$('.MM-product-benefits__show-flyout-button').click(function () {
		fireUtag('See All', 'Benefits');

	});
}

function initProductBenefitsSlickGrid() {
	$('.MM-product-benefits__inner-containers').slick({
		nextArrow: '<div class="MM-product-benefits__right-caret"></div>',
		prevArrow: '<div class="MM-product-benefits__left-caret"></div>'
	});
}

function fireUtag(Label, Action) {
	if (typeof (utag) != "undefined") {
		utag.link({ eventCategory: 'Mattress Matcher Results', eventLabel: Label, eventAction: Action });
	}
}

$(document).on('click','.financing-message-mattress-matcher',function(){
	var $financingMessage = $(this);
	var financinglabel = 'Financing - ' + Math.round($financingMessage.closest('.featured-product-v3').attr('data-tealium'));
	fireUtag(financinglabel, 'Products');
});

$(document).on('click','.mm-product-image-link',function(){
	var productImagelabel = 'Product Image - ' + Math.round($(this).closest('.featured-product-v3').attr('data-tealium'));
	fireUtag(productImagelabel, 'Products');
});

$(document).on('click','.mm-product-name-link',function(){
	var productImagelabel = 'Product Name - ' + Math.round($(this).closest('.featured-product-v3').attr('data-tealium'));
	fireUtag(productImagelabel, 'Products');
});

$(document).on('click','.mm-learn-more',function(){
	var productNamelabel = 'Shop Now - ' + Math.round($(this).closest('.featured-product-v3').attr('data-tealium'));
	fireUtag(productNamelabel, 'Products');
});

$(window).load(function(){
	$('.MM-main-banner__section-2__image').contents().find('[id ^= "Disruptor"]').css('display', 'none');
	var customerDisruptors = [];
	var partnerDisruptors = [];

	if ($('#MM-avatar').data('customer-disruptors') != '' && typeof $('#MM-avatar').data('customer-disruptors') != "undefined") {
		customerDisruptors = $('#MM-avatar').data('customer-disruptors').split('|');
	}
	if ($('#MM-avatar').data('partner-disruptors') != '' && typeof $('#MM-avatar').data('partner-disruptors') != "undefined") {
		partnerDisruptors = $('#MM-avatar').data('partner-disruptors').split('|');
	}

	var allDisruptors = customerDisruptors.concat(partnerDisruptors);

	for (var i = 0; i < allDisruptors.length; i++) {
		$('.MM-main-banner__section-2__image').contents().find('[id = "' + allDisruptors[i] + '"]').css('display', 'block');
	}
	
	$('#MM-avatar').css('visibility', 'visible');
	submitProductTag();
});

function submitProductTag() {
	var featurProdutcs =$('.MM-results_featured-products-list .featured-product-v3');
	featurProdutcs.each(function() {
		currentIndex= Math.round($(this).data('tealium'));
		if(!tealiumPostedProucts.includes(currentIndex)) {
			var productName = $(this).find('.p_name').length > 0 ? ($(this).find('.p_name').text()).trim() : "";
			var productID = $(this).find('.product-tiles').length > 0 ? $(this).find('.product-tiles').data('itemid') : '';
			var brandName = $(this).data('brand-name');
			var category = $(this).data('category');	
			var price = $(this).find('.price-sales').length > 0 ? $(this).find('.price-sales').text().trim() : $(this).find('.product-sales-price').length > 0 ? $(this).find('.product-sales-price').text().trim() : '';		
		
			if (typeof (utag) != "undefined") {
				utag.link({ product_impression_id: productID, 
					product_impression_name: productName, 
					product_impression_brand: brandName, 
					product_impression_category:category, 
					product_impression_list:'Mattress Matcher Results',
					product_impression_position:currentIndex,
					product_impression_price:price });
				}
			tealiumPostedProucts.push(currentIndex);
		}
	});
}
