'use strict';

/**
 * @description Initializes the Mattress Finder Version Three Client Side JS.
 */
var util = require('./util');

/**
 * @function
 * @description Get Height and Weight Data on User Height and update Weight Labels.
 * @Parms @height type JQuery Object	Feet Input Field
 * @Parms @inches type JQuery Object	Inches Input Field
 */
function getWeightByHeight(height , inches) {
	var heightFeet = height.val() || "";
	var heightInch = inches.val() || "";
	// in case we dont have value then value then set default value
	if (heightFeet.length == 0 && heightInch.length == 0) {
		heightFeet = 5;
		heightInch = 7;
	}
	if (heightFeet && heightInch) {
		var heightVal = heightFeet + "/" + heightInch;
		$.ajax({
		    url: Urls.getWeightByHeight_v3,
		    data: {selectedHeight: heightVal},
		    type: 'get',
		    success: function (data) {
		      var weight = JSON.parse(data);
		      if (weight != undefined && weight.max != undefined && weight.min != undefined) {
			      $(".weight-1 span").text("Less than " + weight.min + " lbs.");
			      $(".weight-1 input").attr('aria-label', "Less than " + weight.min + " lbs.");

			      $(".weight-2 span").text(weight.min + " - " + weight.max + " lbs.");
			      $(".weight-2 input").attr('aria-label', weight.min + " - " + weight.max + " lbs.");

			      $(".weight-3 span").text(weight.max + " lbs. or more");
			      $(".weight-3 input").attr('aria-label', weight.max + " lbs. or more");
		      }
		    },
		    error: function (errorThrown) {
		      console.log(errorThrown);
		    }
		});
    }
}
/**
 * @function
 * @description Set Height value on previous and next button click.
 * @Parms @cname type @JqueryObject clicked button.
 */
function setHeightValue (actionButton) {
	var HeightInputs = actionButton.parents('.question').find('.mfinder__height');
	if (HeightInputs.length > 0) {
		var feet   = HeightInputs.find('.mfinder__height-input--feet');
		var inches = HeightInputs.find('.mfinder__height-input--inches');
		if (feet.length > 0 && inches.length > 0) {
			if (!feet.val()) {
				feet.val('5');
				inches.val('7');
			} else if (!inches.val()) {
				inches.val('0');
			}
			getWeightByHeight(feet, inches);
		}
	}
}

/**
 * @function
 * add class on left nav bar for visited slide
 */
function visitedQuestion(){
	 $( ".wayfinding-navigation .slick-current" ).addClass( "visited" );
}

/**
 * @function
 * @description Move Slick Slides Up on Previous, Next and Skip Button.
 * @Parms @cname type @string cookie name
 */
function scrollSlideToTop() {
	$('html,body').animate({
        scrollTop: $("#main").offset().top
	}, "fast");
}

/* On click of any wayfinding link or click of Previous button, if weight slide
 * is active (i.e. slide-index is 6) set weight link to visited.
 */
function closeQuestions(){
		var $activeSlide = $('.mfinder_questions-wrap .slick-list');
		if ($activeSlide.length > 0) {
			var $whyAskQuestions = $activeSlide.find('.mfinder__why-question');
			if ($whyAskQuestions.length > 0) {
				$whyAskQuestions.hasClass('activeDes') ? $whyAskQuestions.removeClass('activeDes') : null;
				$whyAskQuestions.next('.description').length > 0 && $whyAskQuestions.next('.description').hasClass('show-description') ? $whyAskQuestions.next('.description').removeClass('show-description') : null;
			}
		}
}

exports.init = function () {
	if ($('.mfinder_v3').length > 0) {

		// Get Google Client Id from cookie and set in hidden input field
		var googleClientId = util.getCookie("_ga");
		if ($('#google_client_ID').length > 0 && googleClientId.length > 0) {
			$('#google_client_ID').val(googleClientId);
		}

		// Update Weight data on base of selected Height.
		$('#mfinder__height-1, #mfinder__height-2').on('focusout', function(e) {
			$(this).val($(this).val().replace(/[^\d].+/, ""));
			getWeightByHeight($("#mfinder__height-1"), $("#mfinder__height-2"));
		});

		// Toggle Why We Ask Question Detail
		$(".mfinder__why-question").on("click" , function(event) {
			var thisQ = $(this);
			var dsHeight = 135; //$('.description').height();
			var stHeight = $('.mfinder_questions-wrap .slick-current').height();
			if (thisQ.hasClass('activeDes')) {
				thisQ.removeClass('activeDes');
				thisQ.next(".description").removeClass("show-description");
				$('.mfinder_questions-wrap .slick-list').css('height', stHeight - 36);  
				if (screen.width < 768) {
					$('.mfinder_questions-wrap .slick-list').css('height', stHeight - 0);
				}
				
			} else {
				$('.mfinder_questions-wrap .slick-list').css('height', stHeight + dsHeight);
				var $questionParentBlockAsk = thisQ.parents('.mfinder_questions-wrap').find('.mfinder__why-question');
				if ($questionParentBlockAsk.length > 0) {
					$questionParentBlockAsk.removeClass('activeDes');
					$questionParentBlockAsk.next(".description").removeClass("show-description");
				}
				thisQ.addClass('activeDes');
				thisQ.next(".description").addClass("show-description");
			}
		});
		
		$('.clearAllcheckboxes').on('change', function(e) {
			$(e.target).closest('.multiple-selection').find('input').not(this).prop('checked', false);
		});

		$('input[type="checkbox"]').not('.clearAllcheckboxes').on('click', function(e) {
			$(this).closest('.multiple-selection').find('.clearAllcheckboxes').prop('checked', false);
		});

		$(".next_button").click(function(e) {
			e.preventDefault();

			//set Height values
			setHeightValue($(this));
			closeQuestions();
			$(".slick-next").click();
			scrollSlideToTop();
			visitedQuestion();
		});

		$(".prev_button").click(function(e) {
			e.preventDefault();
			closeQuestions();
			setHeightValue($(this));
			$(".slick-prev").click();
			scrollSlideToTop();
		});

		$(".skip").click(function(e) {
			e.preventDefault();
			$(".slick-next").click();
			visitedQuestion();
			scrollSlideToTop();
			var HeightInputs = $(this).parents('.question').find('.multiple-selection,.single-selection');
			if (HeightInputs.length > 0) {
				HeightInputs.find('input:visible').prop('checked', false);
			}
			if($(".mfinder_question-7").hasClass("slick-active")) {
				var $feet = $(".mfinder_question-6 .mfinder__height input#mfinder__height-1");
				var $inches = $(".mfinder_question-6 .mfinder__height input#mfinder__height-2");
				if ($feet.length > 0 && $inches.length > 0) {
					$feet.val("5");
					$inches.val("7");
					getWeightByHeight($feet, $inches);
				}
			}
		});

		$(".mattress-finder-results").click(function() {
			$(this).closest('form').submit();
		});

		$('.mfinder_questions-wrap').slick({
			infinite: false,
			slide: '.question',
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: true,
			swipe: false,
			touchMove: false,
			accessibility: false,
			adaptiveHeight: true,
			fade: true,
			asNavFor: '.slider-nav',
			cssEase: 'linear'
		});
		$('.slider-nav').slick({
			slidesToShow: 8,
			slidesToScroll: 6,
			vertical: true,
			asNavFor: '.mfinder_questions-wrap',
			accessibility: false,
			adaptiveHeight: false,
			infinite : false,
			arrows : false,
			dots: false,
			focusOnSelect: true
		});
		$('.wayfinding-navigation').on('click','.visited', function() {
			closeQuestions();
		});
		var $textWidthFt = $('#mfinder__height-1');
		var $textWidthIn = $('#mfinder__height-2');
		$(".mfinder__height-input").on("keyup keypress blur input", function() {
			var startFt = parseInt($textWidthFt.val(), 10),
			startIn = parseInt($textWidthIn.val(), 10);
			$(this).val($(this).val().replace(/[^\d].+/, "").replace(/\b(0(?!\b))+/g, ""));

			if ((event.which < 48 || event.which > 57)) {
				event.preventDefault();
			}

			if (parseInt($textWidthFt.val()) < 3) {
				parseInt($textWidthFt.val(3), 10);
				return false;
			} else if (parseInt($textWidthFt.val()) > 8) {
				parseInt($textWidthFt.val(8), 10);
				return false;
			}

			if (parseInt($textWidthIn.val()) > 11) {
				parseInt($textWidthIn.val(11), 10);
				return false;
			} else if ($(this).hasClass("mfinder__height-input--feet")) {
				parseInt($textWidthFt.val($(this).val()), 10);
			}

			if ((parseInt($textWidthFt.val()) == 8 && parseInt($textWidthIn.val()) > 0)) {
				parseInt($textWidthIn.val(0), 10);
			}
        });
        // unselect radio button
        $(".mfinder-V3-container input[type='radio']").click(function() {
            var previousValue = $(this).attr('previousValue');
            var name = $(this).attr('name');
        
            if (previousValue == 'checked') {
              $(this).removeAttr('checked');
              $(this).attr('previousValue', false);
            } else {
              $("input[name="+name+"]:radio").attr('previousValue', false);
              $(this).attr('previousValue', 'checked');
            }
          });
        
        // Mattress Matcher Page drawer opener
        $('.MM-product__button').click(function () {
            $('body').css('background-color', 'rgb(191,190,189)');
            $('.MM-product__drawer').addClass('MM-product__drawer__open');
        });
        
        // Mattress Matcher Page drawer close
        $('.MM-product__drawer__close-button').click(function () {
            $('body').css('background-color', 'rgb(0,0,0,0)');
            $('.MM-product__drawer').removeClass('MM-product__drawer__open');
        });
	}
	// on page load we will set default height weight values in session for 5 Feet, 7 Inches
	getWeightByHeight($("#mfinder__height-1"), $("#mfinder__height-2"));
};


