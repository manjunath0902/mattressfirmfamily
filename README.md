# Setting up the builder files to work with a new project (Team Lead Only)

## 1. Update the launch files with the correct cartridge names

- In the gulp_builder project folder there is a folder called .externalToolBuilders with two .launch files called:

```
Gulp Javascript Builder.launch
Gulp Sass Builder.launch
```

The Sass builder launch file contains references to app_storefront_core. For the build to work properly they need to be updated to point to the new core project (e.g. app_agaci, app_gopro, etc.)

The Javascript builder launch file contains references to app_storefront_richUI. For the build to work properly they need to be updated to point to the new richUI project (e.g. app_agaci_richUI, app_gopro_richUI, etc.)

## 2. Update the gulp javascript configuration file with the correct settings

In the gulp_builder project folder there is a file called: 

```
config.json
```

Open this file and update the configuration settings for the new sites. Each site's settings is an object. For example:

```javascript
{
	// List of all of the sites
	"sites": [
		// Site Genesis settings
		{
			// Name of the site. This is used to create the temporary folders
			"name": "site-genesis",
			// Cartridge where the javascript will be served from
			"publicJavascript": "app_storefront_richUI",
			// List of the cartridges, in Demandware cartridge path order, used to setup the javascript overrides and compile using Browserify
			"cartridges": [ "int_lgc_googleanalytics", "app_storelocator", "app_storefront_richUI" ]
		}
	]
}
```

# Getting started with the Node/Gulp/libSass project builder

## 1. Install the proper NodeJS version for your OS. 

	http://nodejs.org/

## 2. Install gulp globally:

From the command line type:

Windows:	
	
	% npm install -g gulp	

Mac and Linux:

	% sudo npm install -g gulp
			
## 3. From the command line, navigate to the base directory for this cartridge. You should see the package.json file in the directory.

From the command line type:

Windows:
		
	% npm install

Mac and Linux:
	
	% sudo npm install

## 4. Perform the same commands above for the richUI cartridge's package.json file. This is used to install the front end Javascript packages.
					
## 5. Try It Out!

- Go to the style.scss script (cartridge/static/default/sass/style.scss)
- Change something in there and save it.
- You should see your console say something like:

```
BUILD SUCCESSFUL
Total time: 1 second
```

- Inspect your style.css to reassure yourself (app_storefront_core/cartridge/static/default/css/style.css)

end
