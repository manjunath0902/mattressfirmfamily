<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isdecorate template="checkout/pt_checkout">
<isinclude template="util/modules"/>

<iscomment>
    This template visualizes the first step of the single shipping checkout
    scenario. It renders a form for the shipping address and shipping method
    selection. Both are stored at a single shipment only.
</iscomment>

<iscomment>Report this checkout step (we need to report two steps)</iscomment>

<isreportcheckout checkoutstep="${2}" checkoutname="${'ShippingAddress'}"/>
<isreportcheckout checkoutstep="${3}" checkoutname="${'ShippingMethod'}"/>

<isscript>
    importScript("app_storefront_core:cart/CartUtils.ds");
    importScript("app_storefront_core:cart/pickupDates.ds");
    var MattressViewHelper = require("int_mattressc/cartridge/scripts/mattress/util/MattressViewHelper.ds");
    var productListAddresses = CartUtils.getAddressList(pdict.Basket, pdict.CurrentCustomer, true);
    var adjustmentPromotionId = "RECYCLING_FEE";

</isscript>
<isset name="instoreShipmentsExists" value="${false}" scope="page" />
<isset name="Shipments" value="${pdict.Basket.shipments.toArray().reverse()}" scope="page"/>
<isset name="IsStorePickup" value="${session.custom.deliveryOptionChoice == 'instorepickup'}" scope="page"/>
    <iscomment>checkout progress indicator</iscomment>

	<iscomment>
    <ischeckoutprogressindicator step="2" rendershipping="${pdict.Basket.productLineItems.size() == 0 ? 'false' : 'true'}"/>
	</iscomment>
	<div class="page-shipping-method">
	<isset name="showGroundDelivery" value="${true}" scope="page"/>
	<isset name="redCarpetDelivery" value="${true}" scope="page"/>

    <isset name="tempShowGroundDelivery" value="${false}" scope="page"/>
	<isset name="tempRedCarpetDelivery" value="${false}" scope="page"/>
	<isset name="hybridDelivery" value="${false}" scope="page"/>

	<isloop items="${Shipments}" var="shipment" status="loopstate">
		<isif condition="${shipment.productLineItems.length <= 0 || shipment.custom.shipmentType == null && shipment.UUID==pdict.Basket.defaultShipment.UUID && !empty(shipment.shippingAddress) && empty(shipment.shippingAddress.address1)}">
			<iscontinue/>
		</isif>
		<isif condition="${shipment.custom.shipmentType != 'instore' && (shipment.custom.shipmentType == 'In-Market' || shipment.custom.shipmentType == 'Parcel') && !IsStorePickup}">
			<isset name="tempRedCarpetDelivery" value="${true}" scope="page"/>
		</isif>
		<isif condition="${shipment.custom.shipmentType != 'In-Market' && shipment.custom.shipmentType != 'Parcel' && shipment.custom.shipmentType != 'instore'}">
			 <isset name="tempShowGroundDelivery" value="${true}" scope="page"/>
		</isif>
		<isif condition="${tempShowGroundDelivery && tempRedCarpetDelivery}">
			 <isset name="hybridDelivery" value="${true}" scope="page"/>
			 <isbreak>
		</isif>
	</isloop>

    <isloop items="${Shipments}" var="shipment" status="loopstate">
		<isif condition="${shipment.productLineItems.length <= 0 || shipment.custom.shipmentType == null && shipment.UUID==pdict.Basket.defaultShipment.UUID && !empty(shipment.shippingAddress) && empty(shipment.shippingAddress.address1)}">
			<iscontinue/>
		</isif>

		<isset name="enableDateDeliveries" value="${false}" scope="page" >
		<isif condition="${redCarpetDelivery && shipment.custom.shipmentType == 'In-Market' || shipment.custom.shipmentType == 'Parcel'}">
			<isif condition="${request.httpParameterMap.deliveryScheduleChanged && request.httpParameterMap.deliveryScheduleChanged.stringValue === 'true'}">
			   <div class="top-shipping-method-mobile">
					<isif condition="${shipment.custom.shipmentType != 'instore' && (shipment.custom.shipmentType == 'In-Market' || shipment.custom.shipmentType == 'Parcel') && !IsStorePickup}">
						<h2 class="address-legend mobile-delivery-changed-heading">
							${Resource.msg('minishipments.deliverychanged.heading','checkout',null)}
						</h2>
						<p class="mobile-delivery-changed-message">${Resource.msg('minishipments.deliverychanged.description','checkout',null)}</p>
					</isif>
					<isif condition="${shipment.custom.shipmentType == 'instore' || IsStorePickup}">
						<h2 class="address-legend">
							${Resource.msg('minishipments.choosedelivery','checkout',null)}
						</h2>
					<iselseif condition="${shipment.shippingAddress != null && pdict.Basket.productLineItems.size() > 0}"/>
						<h2 class="address-legend">
							${Resource.msg('minishipments.choosedelivery','checkout',null)}
						</h2>
					</isif>
				</div>
			<iselse>
				<div class="top-shipping-method-mobile">
					<isif condition="${shipment.custom.shipmentType == 'instore' || IsStorePickup}">
						<h2 class="address-legend">
							${Resource.msg('minishipments.choosedelivery','checkout',null)}
						</h2>
					<iselseif condition="${shipment.shippingAddress != null && pdict.Basket.productLineItems.size() > 0}"/>
						<h2 class="address-legend">
							${Resource.msg('minishipments.choosedelivery','checkout',null)}
						</h2>
					</isif>
					<isif condition="${shipment.custom.shipmentType != 'instore' && (shipment.custom.shipmentType == 'In-Market' || shipment.custom.shipmentType == 'Parcel') && !IsStorePickup}">
						<isif condition="${hybridDelivery}">
							<p>${Resource.msg('opc.redcarpetdelivery.hybrid.mobile','checkout',null)}</p>
						<iselse>
							<p>${Resource.msg('opc.redcarpetdeliverymobile.line1','checkout',null)}</p>
						</isif>
						<p>${Resource.msg('opc.redcarpetdeliverymobile.line2','checkout',null)} <a href="/s/Mattress-Firm/deliver-options-C.html" class="details" title="${Resource.msg('opc.redcarpetdelivery','checkout',null)}">Learn More</a></p>
					</isif>
				</div>
			</isif>

				<div id="store-picker-enter-zip" class="changezip-deliverymethodpage">
					<div id="show-change-zipcode-id" class="">
						<isprint value="${Resource.msgf('singleshipping.deliverto','checkout', '', session.custom.customerZip)}" encoding="off"/>
						<isif condition="${!(request.httpParameterMap.deliveryScheduleChanged && request.httpParameterMap.deliveryScheduleChanged.stringValue === 'true')}">

							<button id="change-zip-id" class="button-text" >
		                	   	${Resource.msg('singleshipping.changezip','checkout', null)}
			                </button>
			               <div class="form-caption">${Resource.msg('singleshipping.zipcodemessage','checkout',null)}</div>

						</isif>
					</div>
					<isif condition="${!(request.httpParameterMap.deliveryScheduleChanged && request.httpParameterMap.deliveryScheduleChanged.stringValue === 'true')}">

						<div id="enter-zipcode-section-id" class="try-in-store-list-container visually-hidden">
							<ul class="try-in-store-list">
								<li>
									<div class="form-row label-above form-input">
											<label for="postalCodeInput"><span>${Resource.msg('singleshipping.confirmzipcode','checkout',null)}</span><span class="required-indicator">*</span></label>
											<div class="field-wrapper">												
												<isif condition="${!empty(session.custom.customerZip)}">
													<input id="zipcodeInput" maxlength="5" class="input-text valid" type="tel" name="postalCodeInput" value="${session.custom.customerZip}" aria-invalid="false">
												<iselse>
													<input id="zipcodeInput" maxlength="5" class="input-text valid" type="tel" name="postalCodeInput" value="" aria-invalid="false">
												</isif>
											</div>
									</div>
								</li>
								<li>
										<button id="zipcodeUpdate" type="submit" value="submit">Update</button>
								</li>
							</ul>
						<div id="zipcodeError" class="error"></div>
							<div class="form-caption">${Resource.msg('singleshipping.zipcodemessage','checkout',null)}</div>
						</div>
					</isif>
				</div>
			<isset name="redCarpetDelivery" value="${false}" scope="page"/>
		</isif>
		<form action="${URLUtils.continueURL()}" method="post" id="${pdict.CurrentForms.singleshipping.shippingAddress.htmlName}" class="checkout-shipping address form-horizontal form-border-bottom-zero">
		 	<div class="mobile-shipping-method-screen-one">
			 	<isif condition="${showGroundDelivery && shipment.custom.shipmentType != 'In-Market' && shipment.custom.shipmentType != 'Parcel' && shipment.custom.shipmentType != 'instore'}">
				 	<isif condition="${hybridDelivery}">
				 		<p class="hybrid-p">${Resource.msg('opc.grounddelivery.hybrid.mobile','checkout',null)}</p>
				 	<iselse>
				 		<p class="ground-p">${Resource.msg('opc.grounddeliverymobile.line1','checkout',null)}</p>
					</isif>
					<p>${Resource.msg('opc.grounddeliverymobile.line2','checkout',null)} <a href="/s/Mattress-Firm/deliver-options-II.html" class="details" title="${Resource.msg('opc.redcarpetdelivery','checkout',null)}">Learn more</a></p>
				</isif>


				<table class="item-list shipping-method-table"  cellspacing="0">

				<isif condition="${shipment.custom.shipmentType != 'In-Market' && shipment.custom.shipmentType != 'Parcel' && shipment.custom.shipmentType != 'instore'}">
					<isloop items="${shipment.productLineItems}" var="productLineItem" status="pliloopstate">
						 <isif condition="${pliloopstate.first && showGroundDelivery}">
							<thead>
								 <tr class="cart-row">
									 <th class="section-header" colspan="2">${Resource.msg('global.product','locale',null)}</th>
			                         <th class="section-header">${Resource.msg('global.price','locale',null)}</th>
			                         <th class="section-header">${Resource.msg('global.qty','locale',null)}</th>
			                         <th class="section-header header-total-price">${Resource.msg('global.totalprice','locale',null)}</th>
			                    </tr>
							</thead>
							<isset name="showGroundDelivery" value="${false}" scope="page"/>
						 </isif>
		                 <tr class="cart-row">
		                     <td class="item-image">
								 <isif condition="${'AmplienceHost' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.AmplienceHost!=''}">
									 <isset name="AmplienceHost" value="${dw.system.Site.current.preferences.custom.AmplienceHost}" scope="page" />
								 </isif>
								 <isif condition="${'AmplienceClientId' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.AmplienceClientId!=''}">
									 <isset name="AmplienceId" value="${dw.system.Site.current.preferences.custom.AmplienceClientId}" scope="page" />
								 </isif>
								 <isset name="imgUrl" value="${pdict.CurrentRequest.httpProtocol+'://'+AmplienceHost+'/i/'+AmplienceId+'/'+ productLineItem.product.custom.external_id}" scope="page"/>
		                         <isif condition="${imgUrl}">
		                             <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" class="lazyload" data-src="${imgUrl}" alt="${productLineItem.product.name}" title="${productLineItem.product.name}"/>
		                         <iselse/>
		                             <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" class="lazyload" data-src="${URLUtils.staticURL('/images/noimagesmall.png')}" alt="${productLineItem.product.name}" title="${productLineItem.product.ame}"/>
		                         </isif>
								 <iscomment>
									 <isif condition="${productLineItem.bonusProductLineItem}">
										 <div class="bonus-item">
											 <isset name="bonusProductPrice" value="${productLineItem.getAdjustedPrice()}" scope="page"/>
											 <isinclude template="checkout/components/displaybonusproductprice" />
											 <isprint value="${bonusProductPriceValue}" />
										 </div>
									 </isif>
								 </iscomment>
		                     </td>

		                     <td class="item-details">
		                      		<div class="mobile-message-delivery">
		                      			 <isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('enableStorePickUp')}">
			                                  <isset name="lineItem" value="${productLineItem}" scope="page" />
	                             		</isif>
		                      		</div>
		                         <iscomment>Display product line and product using module</iscomment>
		                         <isdisplayliproduct p_productli="${productLineItem}" p_editable="${false}" p_hidepromo="${false}"/>

								 <iscomment>Product Existence and Product Availability</iscomment>
								 <iscomment>
									 <div class="cart-availability-container">
										 <isif condition="${!productLineItem.bonusProductLineItem || productLineItem.getBonusDiscountLineItem() != null}">
											 <isset name="product" value="${productLineItem.product}" scope="page" />
											 <isset name="quantity" value="${pdict.Basket.getAllProductQuantities().get(productLineItem.product).value}" scope="page" />
											 <isset name="lineItem" value="${productLineItem}" scope="page" />
												 <isif condition="${product.custom.shippingInformation.toLowerCase() == 'core' && dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpAvailabilityMessage') && session.custom.customerZip}">
												 	<isinclude template="checkout/cart/atp-cartavailability" />
												 <iselse>
												 	<isinclude template="checkout/cart/cartavailability" />
												 </isif>
										 </isif>
										<isscript>
											var productInventoryRecord = productLineItem.product.availabilityModel.inventoryRecord;
											var messageId = productInventoryRecord.custom.messageType;
										 	var detailsContentId : String = 'defaultShippingInfo';
											if (dw.content.ContentMgr.getContent("deliver-options-" + messageId) != null) {
												detailsContentId = "deliver-options-" + messageId;
											} else {
												var shippingInfo : String = productLineItem.product.custom.shippingInformation;
												if (dw.content.ContentMgr.getContent(shippingInfo) != null) {
													detailsContentId = shippingInfo;
												}
											}
										</isscript>
										 <a href="${URLUtils.url('Page-Show', 'cid', detailsContentId)}" class="details" title="${Resource.msg('global.details','locale',null)}">(${Resource.msg('global.details','locale',null)})</a>
									 </div>
								 </iscomment>

								 <isif condition="${MattressViewHelper.hasRecycleFees(productLineItem)}">
								 	 <isset name="shippingState" value="${shipment.shippingAddress != null ? shipment.shippingAddress.stateCode.toUpperCase() : ""}" scope="page" >
									 <div class="recycle-fee">
										 <span class="label">${Resource.msgf('global.recycling-fee','locale', '', shippingState)}: </span>
										 <span class="value"><isprint value="${MattressViewHelper.getTotalRecycleFees(productLineItem)}"></span>
									 </div>
								 </isif>
								 <div class="attribute mobile-only qty">
									 <div class="label">${Resource.msg('global.qty','locale',null)}</div>
									 <div class="value"><isprint value="${productLineItem.quantity}" /></div>
								 </div>
								 <isif condition="${productLineItem.bonusProductLineItem}">
									 <div class="bonus-item">
										 <isset name="bonusProductPrice" value="${productLineItem.getAdjustedPrice()}" scope="page"/>
										 <isinclude template="checkout/components/displaybonusproductprice" />
										 <isif condition="${bonusProductPrice == 0}">
										 	${Resource.msg('global.freebonus','locale',null)}
										 </isif>
									 </div>
								 </isif>
								 <isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('enableStorePickUp')}">
	                                  <isset name="lineItem" value="${productLineItem}" scope="page" />
	                                  <isinclude template="checkout/cart/storepickup/deliveryoptions" />
	                             </isif>
		                     </td>
							<td class="item-quantity">
		                         <isprint value="${productLineItem.price}" />
		                     </td>
		                     <td class="item-quantity">
		                         <isprint value="${productLineItem.quantity}" />
		                     </td>
		                     <td class="item-quantity">
		                         <isprint value="${productLineItem.adjustedPrice}" />
		                     </td>
		                 </tr>
		                 <isif condition="${!empty(productLineItem.product.custom.dw_mattress_type) || !empty(productLineItem.product.custom.mattress_type)}">
		                 	<isset name="enableDateDeliveries" value="${true}" scope="page" />
		                 </isif>
		             </isloop>
		           </isif>
					<iscomment>Shipment gift options</iscomment>
					<iscomment>
		                 <isscript>
		                     var attributes = {
		                         rows: 4,
		                         cols: 10,
		                         'data-character-limit': 250
		                     };
		                 </isscript>
		                 <isif condition="${shipment.productLineItems.size() > 0}">
		                     <tr class="cart-row form-horizontal">
		                         <td colspan="4">
		                         <isif condition="${shipment.custom.shipmentType == 'instore' && dw.system.Site.getCurrent().getCustomPreferenceValue('enableStorePickUp')}">
		                             <div class="form-row">
		                                 <isinputfield rowclass="store-pickup-message-text" formfield="${shipmentItem.storePickupMessage}" type="textarea" attributes="${attributes}"/>
		                             </div>
		                         <iselse/>
		                             <isinputfield formfield="${shipmentItem.isGift}" type="radio"/>
		                             <isinputfield rowclass="gift-message-text" formfield="${shipmentItem.giftMessage}" type="textarea" attributes="${attributes}"/>
		                         </isif>
		                         </td>
		                     </tr>
		                 </isif>
					</iscomment>
		         </table>


				<isif condition="${shipment.custom.shipmentType != 'instore' && (shipment.custom.shipmentType == 'In-Market' || shipment.custom.shipmentType == 'Parcel') && !IsStorePickup}">
					<isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule')}">
						<div class="delivery-schedule-box" tabindex="0">
							<isset name="selectedTimeSlot" value="${shipment.custom.deliveryDate}" scope="page"/>
							<isinclude template="checkout/components/mob-delivery-schedule-mocked-atp" />
						</div>
					<iselse />
						<div class="delivery-schedule-box" tabindex="0">
						 	<isset name="selectedTimeSlot" value="${shipment.custom.deliveryDate}" scope="page"/>
							<isif condition="${!dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpDeliverySchedule')}">
								<isinclude template="checkout/components/delivery-schedule-mocked" />
							</isif>
						</div>
					</isif>
				</isif>
			</isloop>
			<div class="btn-next-shipping-screen">
                <button type="submit" id="shippingMethodNextScreen" class="button-fancy-large" form="${pdict.CurrentForms.singleshipping.shippingAddress.htmlName}" name="${pdict.CurrentForms.singleshipping.shippingAddress.saveDeliveryDate.htmlName}"><span>Next</span></button>
			</div>
		</div>

			<input type="hidden" name="${pdict.CurrentForms.singleshipping.shippingAddress.deliveryScheduleChanged.htmlName}" value="${request.httpParameterMap.deliveryScheduleChanged && request.httpParameterMap.deliveryScheduleChanged.stringValue === 'true'}"/>
			<input type="hidden" name="${pdict.CurrentForms.singleshipping.secureKeyHtmlName}" value="${pdict.CurrentForms.singleshipping.secureKeyValue}"/>
			<input type="hidden" name="${pdict.CurrentForms.singleshipping.shippingAddress.useAsBillingAddress.htmlName}" type="hidden" value="${pdict.CurrentForms.singleshipping.shippingAddress.useAsBillingAddress.value}"/>
			<input id="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.fleetwiseToken.htmlName}" name="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.fleetwiseToken.htmlName}" type="hidden" />
			<input id="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.deliveryDate.htmlName}" name="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.deliveryDate.htmlName}" type="hidden" />
			<input id="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.deliveryTime.htmlName}" name="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.deliveryTime.htmlName}" type="hidden" />

		</div>
		</div><!-- End mobile-shipping-method-screen-two  -->
	</form>
	</div>
</isdecorate>
