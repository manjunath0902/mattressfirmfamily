'use strict';

var util = require('./util'),
	bonusProductsView = require('./bonus-products-view');

var timer = {
	id: null,
	clear: function () {
		if (this.id) {
			window.clearTimeout(this.id);
			delete this.id;
		}
	},
	start: function (duration, callback) {
		this.id = setTimeout(callback, duration);
	}
};

var minicart = {
	init: function () {
		this.$el = $('#mini-cart');
		this.$content = this.$el.find('.mini-cart-content');
		this.$minicartlink = this.$el.find('.mini-cart-link');
		// events
		this.$el.find('.mini-cart-total').on('mouseenter touchstart focusin', function () {
			if (this.$content.not(':visible')) {
				//this.slide();
				var screenwidth = $(window).width();
				this.slide();
				//if (screenwidth > 767) {
				//	this.slide();
				//} else {
				//	this.$content.hide();
				//}
			}
		}.bind(this));
		this.$el.find('.mini-cart-link').on('click ', function (e) {
			if ($(this).hasClass('mini-cart-empty')) {
				e.preventDefault();
				this.close();
				console.log('log');
			}

			if (util.isMobile() && (!$(this).hasClass('mobile-active'))) {
				e.preventDefault();
				$(this).addClass('mobile-active');
			}
		});
		this.$content.on('mouseenter touchstart focusin', function () {
			timer.clear();
		}).on('mouseleave touchend focusout', function () {
			timer.clear();
			timer.start(30, this.close.bind(this));
		}.bind(this));
		$('.mini-cart-products').mCustomScrollbar();
	},
	/**
	 * @function
	 * @description Shows the given content in the mini cart
	 * @param {String} A HTML string with the content which will be shown
	 */
	show: function (html) {
		this.$el.html(html);
		this.init();
		this.$minicartlink.addClass("addline");
		this.$minicartlink.removeClass("addnoline");
	},
	/**
	 * @function
	 * @description Slides down and show the contents of the mini cart
	 */
	slide: function () {
		timer.clear();
		// show the item
		var screenwidth = $(window).width();
		if (screenwidth > 767) {
			this.$content.slideDown('slow');
		}
		if (this.$minicartlink.hasClass('mini-cart-empty')) {
			this.$minicartlink.addClass("addnoline");
			this.$minicartlink.removeClass("addline");
		} else {
			this.$minicartlink.addClass("addline");
			this.$minicartlink.removeClass("addnoline");
		}
		// after a time out automatically close it
		timer.start(6000, this.close.bind(this));
	},
	/**
	 * @function
	 * @description Closes the mini cart with given delay
	 * @param {Number} delay The delay in milliseconds
	 */
	close: function (delay) {
		timer.clear();
		this.$content.slideUp(delay);
		this.$minicartlink.addClass("addnoline");
		this.$minicartlink.removeClass("addline");
		$('.mini-cart-link').removeClass('mobile-active');
	}
};

module.exports = minicart;
