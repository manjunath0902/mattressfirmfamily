// Sticky Nav
$(window).bind("scroll", function() {
  if (parseInt($(window).width()) < 767) {
    window_scrolled();
  }
});

// Hide mobile header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('header.header-main').outerHeight();

var position = $(window).scrollTop();

var navbarHeightDesktop = $('header.header-main').outerHeight();
var stickyScrollPoint = 106;

function window_scrolled() {
  var windowOffset = $(document).scrollTop();
  didScroll = true;
  if (parseInt($(window).width()) < 767) {
    // mobile scroll
    setInterval(function () {
      if (didScroll) {
        hasScrolled();
        didScroll = false;
      }
    }, 250);
  } else {
    lastScrollTop = windowOffset;
  }
}

// Fix for unwanted submenu showing when in sticky nav
$('ul.header-nav li.first-level').hover(function (e) {
  var mY = $(e.target).position().top;
  var navPos = $(this).find('.header-nav-item a').position().top;
  var distance = Math.floor(mY - navPos);
  // show subnav when less than 50px away
  if (distance < 50 && distance > -1) {
    $(this).find('.header-nav-submenu').addClass('active');
  } else {
    $(this).find('.header-nav-submenu').removeClass('active');
  }
});

function hasScrolled() {
  var st = $(document).scrollTop();
  // Make sure they scroll more than delta
  if (Math.abs(lastScrollTop - st) <= delta) {
    return;
  }

  if (parseInt(st) < navbarHeight || (st > lastScrollTop && st > navbarHeight)) {
    // Scroll Down
    $('#wrapper').removeClass('sticky');
  } else {
    // Scroll Up
    if (st + $(window).height() < $(document).height()) {
      $('#wrapper').addClass('sticky');
    }
  }

  lastScrollTop = st;
}
