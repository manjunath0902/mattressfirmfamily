/**
 *    (c) 2009-2014 Demandware Inc.
 *    Subject to standard usage terms and conditions
 *    For all details and documentation:
 *    https://bitbucket.com/demandware/sitegenesis
 */

"use strict";

var countries = require("./countries"),
	dialog = require("./dialog"),
	minicart = require("./minicart"),
	flipclock = require('./flipclock'),
	page = require("./page"),
	pdpTabs = require("./pdp-tabs"),
	expand = require("./expand"),
	rating = require("./rating"),
	searchplaceholder = require("./searchplaceholder"),
	searchsuggest = require("./searchsuggest"),
	searchsuggestbeta = require("./searchsuggest-beta"),
	tooltip = require("./tooltip"),
	scrollTop = require("./scroll-top"),
	stickyNav = require("./sticky-nav"),
	hoverIntent = require("./hoverintent"),
	util = require("./util"),
	validator = require("./validator"),
	googleAnalytics = require("./google-analytics"),
	content = require("./content"),
	responsiveSlots = require("./responsiveslots/responsiveSlots"),
	amplience = require("./amplience"),
	zipzone = require("./zipzone");

// if jQuery has not been loaded, load from google cdn
if (!window.jQuery) {
	var s = document.createElement("script");
	s.setAttribute(
		"src",
		"https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"
	);
	s.setAttribute("type", "text/javascript");
	document.getElementsByTagName("head")[0].appendChild(s);
}

require("./jquery-ext")();
require("./cookieprivacy")();
//require('./captcha')();

function initializeEvents() {
	var controlKeys = [
		"8",
		"13",
		"46",
		"45",
		"36",
		"35",
		"38",
		"37",
		"40",
		"39"
	];
	$("body")
		.on("keydown", "textarea[data-character-limit]", function(e) {
			var text = $.trim($(this).val()),
				charsLimit = $(this).data("character-limit"),
				charsUsed = text.length;

			if (
				charsUsed >= charsLimit &&
				controlKeys.indexOf(e.which.toString()) < 0
			) {
				e.preventDefault();
			}
		})
		.on(
			"change keyup mouseup",
			"textarea[data-character-limit]",
			function() {
				var text = $.trim($(this).val()),
					charsLimit = $(this).data("character-limit"),
					charsUsed = text.length,
					charsRemain = charsLimit - charsUsed;

				if (charsRemain < 0) {
					$(this).val(text.slice(0, charsRemain));
					charsRemain = 0;
				}

				$(this)
					.next("div.char-count")
					.find(".char-remain-count")
					.html(charsRemain);
			}
		);

	/**
	 * initialize search suggestions, pending the value of the site preference(enhancedSearchSuggestions)
	 * this will either init the legacy(false) or the beta versions(true) of the the search suggest feature.
	 * */
	var $searchContainer = $(".search-search");
	if (SitePreferences.LISTING_SEARCHSUGGEST_LEGACY) {
		searchsuggest.init($searchContainer, Resources.SIMPLE_SEARCH);
	} else {
		searchsuggestbeta.init($searchContainer, Resources.SIMPLE_SEARCH);
	}

	$(".secondary-navigation .toggle").click(function() {
		$(this)
			.toggleClass("expanded")
			.next("ul")
			.toggle();
	});
	// add generic toggle functionality
	$(".toggle")
		.next(".toggle-content")
		.hide();
	$(".toggle").click(function() {
		$(this)
			.toggleClass("expanded")
			.next(".toggle-content")
			.toggle();
	});
	
	// Refinements Truncate functionality
	$(".hideRefinement").hide();
	$(".refinements-see-less").hide();
	$(".refinements-see-more").on("click", function() {
		$(this).parent().find(".hideRefinement").show();
		$(this).parent().find(".refinements-see-less").show();
		$(this).hide();
	});
	$(".refinements-see-less").on("click", function() {
		$(this).parent().find(".hideRefinement").hide();
		$(this).parent().find(".refinements-see-more").show();
		$(this).hide();
		
	});
	
	var $deliveryDates = $("#delivery-dates-container");
	var deliveryConfig = function() {
		//Select Delivery Date Checking
		if ($deliveryDates.find("input[type='radio']").length > 0) {
			if (!$deliveryDates.find("input[type='radio']").is(":checked")) {
				$("form#dwfrm_singleshipping_shippingAddress")
					.find("button")
					.addClass("disabled");
			} else {
				if (
					$deliveryDates
						.find("input[value='scheduledelivery']")
						.is(":checked")
				) {
					if (
						$(".delivery-dates-wrapper")
							.find(".time-slot")
							.hasClass("selected")
					) {
						$("form#dwfrm_singleshipping_shippingAddress")
							.find("button")
							.removeClass("disabled");
						$deliveryDates.find(".error").hide();
					} else {
						$("form#dwfrm_singleshipping_shippingAddress")
							.find("button")
							.addClass("disabled");
					}
				} else {
					$("form#dwfrm_singleshipping_shippingAddress")
						.find("button")
						.removeClass("disabled");
					$deliveryDates.find(".error").hide();
				}
			}
		}
	};
	deliveryConfig();
	$deliveryDates.find("input[type='radio']").change(function() {
		deliveryConfig();
	});
	$(".delivery-dates").on("click focusin", ".time-slot", function() {
		setTimeout(function() {
			deliveryConfig();
		}, 500);
	});
	$("form#dwfrm_singleshipping_shippingAddress").submit(function() {
		if (
			$(this)
				.find("button")
				.hasClass("disabled")
		)
			$deliveryDates.find(".error").show();
	});
	$("form#dwfrm_singleshipping_shippingAddress button").click(function(e) {
		if ($(this).hasClass("disabled")) {
			e.preventDefault();
			$deliveryDates.find(".error").show();
		}
	});
	// subscribe email box
	var $subscribeEmail = $(".subscribe-email");
	if ($subscribeEmail.length > 0) {
		$subscribeEmail
			.focus(function() {
				var val = $(this.val());
				if (
					val.length > 0 &&
					val !== Resources.SUBSCRIBE_EMAIL_DEFAULT
				) {
					return; // do not animate when contains non-default value
				}

				$(this).animate(
					{ color: "#999999" },
					500,
					"linear",
					function() {
						$(this)
							.val("")
							.css("color", "#333333");
					}
				);
			})
			.blur(function() {
				var val = $.trim($(this.val()));
				if (val.length > 0) {
					return; // do not animate when contains value
				}
				$(this)
					.val(Resources.SUBSCRIBE_EMAIL_DEFAULT)
					.css("color", "#999999")
					.animate({ color: "#333333" }, 500, "linear");
			});
	}
	$("#zipfield").on("focus", function() {
		$(this).select();
		this.setSelectionRange(0, 5);
	});
	$("#main").on("focus", "#pdp-delivery-zip", function() {
		$(this).select();
		this.setSelectionRange(0, 5);
		setTimeout(function() {
			$("#pdp-delivery-zip")
				.get(0)
				.setSelectionRange(0, 5);
		}, 1);
	});

	$("#main").on("click", ".details", function(e) {
		e.preventDefault();
		var params = {
			dUrl: $(e.target).attr("href"),
			dheight: 600
		};
		dialogify(params);
	});
	$(".privacylink").on("click", function(e) {
		e.preventDefault();
		var params = {
			dUrl: $(e.target).attr("href"),
			dheight: 600,
			dTitle: $(e.target).attr("title")
		};
		dialogify(params);
	});
	$(".privacy-policy").on("click", function(e) {
		e.preventDefault();
		var params = {
			dUrl: $(e.target).attr("href"),
			dheight: 600,
			dTitle: $(e.target).attr("title")
		};
		dialogify(params);
	});
	$(".dialogify").on("click", function(e) {
		e.preventDefault();
		var params = {
			dUrl: $(e.target).attr("href"),
			dClass: $(e.target).attr("data-class"),
			dheight: $(e.target).attr("data-height"),
			dTitle: $(e.target).attr("title")
		};
		dialogify(params);
	});
	var dialogify = function(params) {
		dialog.open({
			url: params.dUrl,
			options: {
				dialogClass: params.dClass,
				height: params.dheight,
				title: params.dTitle,
				open: function() {
					var dialog = this;
					$(dialog).scrollTop(0);
					$(".ui-widget-overlay").on("click", function() {
						$(dialog).dialog("close");
					});
					if (params.dHideTitle) {
						$(".ui-dialog-title").hide();
						$(".ui-dialog-titlebar").css("height", "auto");
					}
				}
			}
		});
	};

	// Footer chat button
	$("#contentChatWithUs").on("click", function() {
		$('.header-chat a[id^="liveagent_button_online"]').trigger("click");
	});

	//Open Chat Widet Upon clicking header and footer links
	$( document ).on("click", "#pdpChatWithUsNew, .contentChatWithUs", function(e) {
		e.preventDefault();
	  if($('#helpButtonSpan').length > 0) {
			$('#helpButtonSpan').click();
		}
	});

	// main menu toggle
	$(".menu-toggle").on("click", function() {
		$("#wrapper").toggleClass("menu-active");
		$("footer").toggleClass("footer-active");
	});
	//search button on header
	$(".header-search").on("click", function() {
		 $(".searchTopCategory").slideToggle("500", "easeInOutCirc");
		 $(".close").slideToggle("500", "easeInOutCirc");
	});
	$(".close").on("click", function() {
		 $(".searchTopCategory").slideToggle("500", "easeInOutCirc");
		 $(".close").slideToggle("500", "easeInOutCirc");
	});
	$(".arrow-right-1").on("click", function() {
		 $(".about-footer-content").slideToggle("500", "easeInOutCirc");
	});
	$(".arrow-right-2").on("click", function() {
		 $(".customer-service-footer-content").slideToggle("500", "easeInOutCirc");
	});
	$(".arrow-right-3").on("click", function() {
		 $(".legal-info-footer-content").slideToggle("500", "easeInOutCirc");
	});
	$(".arrow-right-footer-menu").on("click", function() {
		 var footerMenuDiv = $(this).data("footer-menu-div");
		 $("#" + footerMenuDiv).slideToggle("500", "easeInOutCirc");
	});
	$(".menu-category li .menu-item-toggle").on("click touchstart", function(
		e
	) {
		if (util.isMobileSize()) {
			var $parentLi = $(e.target).closest("li");
			e.preventDefault();
			$parentLi.find(".level-2").toggle();
			if ($parentLi.attr("class") == "active") {
				window.location.href = $parentLi.find("a").attr("href");
			} else {
				$parentLi
					.siblings("li")
					.removeClass("active")
					.find(".menu-item-toggle i")
					.removeClass("fa-chevron-down active")
					.addClass("fa-chevron-right");
				$parentLi
					.siblings("li")
					.find("li.active")
					.removeClass("active")
					.find(".menu-item-toggle i")
					.removeClass("fa-chevron-down active")
					.addClass("fa-chevron-right");
				$parentLi.toggleClass("active");
				$(e.target)
					.find("i")
					.toggleClass("fa-chevron-right fa-chevron-down active");
			}
		}
	});
	//top navigation pause
	var hoverconfig = {
		over: function() {
			$(this)
				.find("div.level-2")
				.css("display", "block");
			$(this)
				.children("a.has-sub-menu")
				.addClass("open");
			$(this).addClass("open");
			var divwidth = $(this)
				.find("div.level-2")
				.width();
			var offset = $(this)
				.find("div.level-2")
				.offset();
			var positionleft = offset.left;
			var screenwidth = $(window).width();
			$("#grid-sort-header").blur();
			$("#grid-paging-header").blur();
			var positionright = screenwidth - (positionleft + divwidth);
			if (screenwidth > 767 && screenwidth < 1025) {
				if (positionright < 50) {
					$(this)
						.find("div.level-2")
						.css("right", 0);
				}
				return false;
			}
		},
		out: function() {
			$(this)
				.find("div.level-2")
				.css("display", "none");
			$(this)
				.children("a.has-sub-menu")
				.removeClass("open");
		},
		timeout: 100,
		sensitivity: 3,
		interval: 100
	};

	var touchConfig = function() {
		$(document).off("touchend", ".menu-category > li > a");
		// fix for touch tablet devices: prevent first click
		if (util.isMobile() == true && screen.width > util.mobileWidth) {
			$(document).on("touchend", ".menu-category > li > a", function(e) {
				if (
					$(this)
						.siblings(".level-2")
						.is(":hidden") == true
				) {
					e.preventDefault();
					// hide all opened menus
					$(".menu-category > li")
						.find(".level-2")
						.css("display", "none");
					$(".menu-category > li")
						.children("a.has-sub-menu")
						.removeClass("open");
					//open current submenu
					$(this)
						.siblings(".level-2")
						.css("display", "block");
					$(this).addClass("open");
					var divwidth = $(this)
						.siblings(".level-2")
						.width();
					var offset = $(this)
						.siblings(".level-2")
						.offset();
					var positionleft = offset.left;
					var screenwidth = screen.width;
					var positionright = screenwidth - (positionleft + divwidth);
					if (screenwidth > 767 && screenwidth < 1025) {
						if (positionright < 50) {
							$(this)
								.siblings(".level-2")
								.css("right", 0);
						}
						return false;
					}
					//console.log('ddd' + positionright + 'ddd' + screenwidth);
				}
			});
		}
	};

	touchConfig();

	if (screen.width > util.mobileWidth) {
		$(".level-1 li").hoverIntent(hoverconfig);
	}
	$(window).resize(function() {
		if (screen.width > util.mobileWidth) {
			$(".level-1 li")
				.removeClass("active")
				.hoverIntent(hoverconfig);
		} else {
			$(".level-1 li")
				.unbind("mouseenter")
				.unbind("mouseleave");
			$(".level-1 li").removeProp("hoverIntentT");
			$(".level-1 li").removeProp("hoverIntentS");
		}
		touchConfig();
	});
	$(".print-page").on("click", function() {
		window.print();
		return false;
	});
	function validatesignupEmail(email) {
		var $continue = $(".home-email");
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		if (!emailReg.test(email)) {
			//$continue.attr('disabled', 'disabled');
			if ($(".emailsignup #email-alert-address-error").length < 1) {
				if ($(".emailsignup .error2").length > 0) {
					$(".emailsignup .error2").remove();
				}
				$(".emailsignup form").append(
					"<span id='email-alert-address-error' class='error'>" +
						Resources.VALIDATE_EMAIL +
						"</span>"
				);
			} else {
				if ($(".emailsignup .error2").length > 0) {
					$(".emailsignup .error2").remove();
				}
				if (
					$(".emailsignup #email-alert-address-error").text()
						.length == 0
				) {
					$(".emailsignup #email-alert-address-error").html(
						Resources.VALIDATE_EMAIL
					);
					$(".emailsignup form").append(
						"<span id='email-alert-address-error' class='error2'>" +
							Resources.VALIDATE_EMAIL +
							"</span>"
					);
				} else {
					//console.log('22' + $(".emailsignup #email-alert-address-error").length);
				}
			}
			$("#email-alert-address-error").css("display", "block");
			return false;
		} else {
			//$continue.removeAttr('disabled');
			if ($(".emailsignup #email-alert-address-errorr").length > 0) {
				$(".emailsignup form #email-alert-address-error").remove();
			}
			if ($(".emailsignup .error2").length > 0) {
				$(".emailsignup .error2").remove();
			}
			return true;
		}
	}
	var emailSignUpFlag = false;
	/*email signup Ajax*/
	$("#email-alert-signup").on("submit", function(e) {
		e.preventDefault();
		var $this = $(this),
			emailSignupInput = $(this).find("#email-alert-address"),
			validationErrorBlock = $("#email-alert-address-error");
		var signupInputValue = $(this)
			.find(emailSignupInput)
			.val();
		var validatetest = validatesignupEmail(signupInputValue);
		if (validatetest && signupInputValue.length > 1 && !emailSignUpFlag) {
			emailSignUpFlag = true;
			var emailPageSource = $(this).data("pagesource");
			var params = {
				email: signupInputValue,
				pageSource: emailPageSource
			};
			$.ajax({
				url: Urls.emailAjaxSubscription,
				data: params,
				type: "post",
				success: function(data, url) {
					$this
						.hide()
						.parent()
						.append($(data));
					//console.log('test2=' + url);
				},
				error: function(errorThrown) {
					//console.log(errorthrown);
				}
			});
		}
	});
	/* Chat link click tracking */
	$("a[id^='liveagent_button_']").each(function() {
		$(this).on("click", function(e) {
			utag.link({ event_name: "chat_link" });
		});
	});
	$("button[name='dwfrm_login_register']").on("click", function() {
		window.location.href = Urls.accountRegister;
	});
}
/**
 * @private
 * @function
 * @description Adds class ('js') to html for css targeting and loads js specific styles.
 */
function initializeDom() {
	// add class to html for css targeting
	$("html").addClass("js");
	if (SitePreferences.LISTING_INFINITE_SCROLL) {
		$("html").addClass("infinite-scroll");
	}
	// load js specific styles
	util.limitCharacters();
}

var pages = {
	account: require("./pages/account"),
	cart: require("./pages/cart"),
	checkout: require("./pages/checkout"),
	compare: require("./pages/compare"),
	product: require("./pages/product"),
	registry: require("./pages/registry"),
	search: require("./pages/search"),
	storefront: require("./pages/storefront"),
	wishlist: require("./pages/wishlist"),
	storelocator: require("./pages/storelocator")
};

var app = {
	init: function() {
		if (document.cookie.length === 0) {
			$("<div/>")
				.addClass("browser-compatibility-alert")
				.append(
					$("<p/>")
						.addClass("browser-error")
						.html(Resources.COOKIES_DISABLED)
				)
				.appendTo("#browser-check");
		}
		hoverIntent.init();
		initializeDom();
		initializeEvents();

		// init specific global components

		countries.init();
		tooltip.init();
		scrollTop.init();
		minicart.init();
		validator.init();
		rating.init();
		searchplaceholder.init();
		util.uniform();
		content.init();
		amplience.initZoomViewer();
		zipzone.init();
		// execute page specific initializations
		$.extend(page, window.pageContext);
		var ns = page.ns;
		if (ns && pages[ns] && pages[ns].init) {
			pages[ns].init();
		}
		// Initialize the Google Analytics library
		if (typeof window.DW.googleAnalytics === "object") {
			googleAnalytics.init(ns, window.DW.googleAnalytics.config);
		}
		responsiveSlots.init(ns || "");
		
	    $("#add-to-cart").removeClass('disabled-btn');
	    $("#checkout-button-ID").removeClass('disabled-btn');
	    $("#shippingMethodContinue").removeClass('disabled-btn');
	}
};

// general extension functions
(function() {
	String.format = function() {
		var s = arguments[0];
		var i,
			len = arguments.length - 1;
		for (i = 0; i < len; i++) {
			var reg = new RegExp("\\{" + i + "\\}", "gm");
			s = s.replace(reg, arguments[i + 1]);
		}
		return s;
	};
})();

function dealOfTheDay() {
	var $timer = $(".deal-of-the-day-counter"),
		countDownDate = new Date("Jun 4, 2070 23:59:99").getTime();

	var x = setInterval(function() {
		var now = new Date().getTime(),
			distance = countDownDate - now,
			hours = Math.floor(
				(distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
			),
			mins = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)),
			sec = Math.floor((distance % (1000 * 60)) / 1000);

		$timer.text(hours + ":" + mins + ":" + sec);
	}, 1000);
}

function orderSummarySticky() {
	$(window).scroll(function () {
		var currentScroll = $(document).scrollTop(); // get current position
		if (currentScroll > 75) {
			// apply position: fixed if you
			$('#cart-summary-right').addClass("order-summary-fixed");
			$('.checkout-mini-cart-wrapper').addClass("order-summary-fixed");
			//$('.checkout-mini-cart-wrapper').mCustomScrollbar();
			$('#confirmation-right').mCustomScrollbar();
		} else {
			$('#cart-summary-right').removeClass("order-summary-fixed");
			$('.checkout-mini-cart-wrapper').removeClass("order-summary-fixed");
		}
	});
}
function carousels() {
	$(".mobile-carousel").slick({
		infinite: false,
		slidesToShow: 3,
		autoplay: false,
		dots: false,
		arrows: false,
		responsive: [
			{
				breakpoint: 648,
				settings: {
					dots: true,
					infinite: true,
					speed: 300,
					autoplay: true,
					autoplaySpeed: 7000,
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

	$(".product-carousel").slick({
		infinite: false,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 4,
		autoplay: false,
		autoplaySpeed: 7000,
		dots: true,
		responsive: [
			{
				breakpoint: 1032,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 648,
				settings: {
					arrows: false,
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

	$(".product-carousel-with-left-sidebar").slick({
		infinite: false,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 3,
		autoplay: false,
		autoplaySpeed: 7000,
		dots: true,
		responsive: [
			{
				breakpoint: 1032,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 648,
				settings: {
					arrows: false,
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	if (screen.width < 767) {
		$("#deliveryBlocks").slick({
			dots: true,
			infinite: true,
			arrows: false
		});
		$("#topBrandsLinks").slick({
			dots: true,
			infinite: true,
			arrows: false
		});
	}
}

carousels();
dealOfTheDay();
orderSummarySticky();

// initialize ap
$(document).ready(function() {
	app.init();
});
//Add Promo Code on Order Summary
$(document).on('click', 'button#add-promo-code-label-id', function (e) {
    e.preventDefault();
    $('#add-promo-code-label-id').addClass('visually-hidden');
    $('#add-promo-code-input-id').removeClass('visually-hidden');
});

$('#checkout-button-ID').on('click tap', function (e) { 
	$(this).addClass('disabled-btn');
});

$('#shippingMethodContinue').on('click tap', function (e) { 
	$(this).addClass('disabled-btn');
});
