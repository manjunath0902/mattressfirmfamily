'use strict';

var account = require('./account'),
    bonusProductsView = require('../bonus-products-view'),
    quickview = require('../quickview'),
    cartStoreInventory = require('../storeinventory/cart'),
    util = require('../util'),
    page = require('../page'),
    dialog = require('../dialog');

/**
 * @private
 * @function
 * @description Binds events to the cart page (edit item's details, bonus item's actions, coupon code entry)
 */
function initializeEvents() {
    $('#cart-table').on('click', '.item-edit-details a', function (e) {
        e.preventDefault();
        quickview.show({
            url: e.target.href,
            source: 'cart'
        });
    })
    .on('click',  '.item-details .bonusproducts a', function (e) {
        e.preventDefault();
        bonusProductsView.show(this.href);
    });
    $('#cart-items-form').on('click', '.bonus-item-actions a', function (e) {
        e.preventDefault();
        bonusProductsView.show({
        	url: this.href
        });
    });
    
    // override enter key for coupon code entry
    $('form input[name$="_couponCode"]').on('keydown', function (e) {
        if (e.which === 13 && $(this).val().length === 0) { return false; }
    });

    //to prevent multiple submissions of the form when removing a product from the cart
    var removeItemEvent = false;
    $('button[name$="deleteProduct"]').on('click', function (e) {
        if (removeItemEvent) {
            e.preventDefault();
        } else {
            removeItemEvent = true;
        }
    });
    if (!util.isMobileSize()) {
        $('.cart-footer-slot').appendTo('.cart-footer');
    }
    /*$('.quantity-dropdown').on('change', function () {
        $('#update-cart').trigger('click');
    });*/
    //MAT-557 binding event when coupon code response renders on cart
    // if this code uncommented then comment50 to 52 line number
    $('.primary-content').on('change','.quantity-dropdown', function () {
        $('#update-cart').trigger('click');
    });
    $('.primary-content').on('click', '.bonus-item-actions a', function (e) {
        e.preventDefault();
        var $bonusProduct = $('#bonus-product-dialog');
        //bonusProductsView.show(this.href);
        var dialogWidth = $(this).parent().prev().val() || 650;
		// create the dialog
		dialog.open({
			target: $bonusProduct,
			url: this.href,
			options: {
				width: dialogWidth,
				title: Resources.BONUS_PRODUCTS
			},
			callback: function () {
				initializeGrid();
				hideSwatches();
				util.uniform();
				$(".ui-dialog-title").show();
				$(".ui-dialog").addClass('bonus-product-popup');
				amplience.initManyProductImages("", $('.col-1'));
				$(".ui-dialog").find('a.continue').on('click', function (e) {
					e.preventDefault();
					$('.ui-dialog-titlebar-close').trigger('click');				
				});
				$(".ui-dialog").find('.ui-icon-closethick').on('click', function (e) {
					e.preventDefault();					
					//page.refresh();
				});
			}
		});
    });
}


var addPersonaliCartRescue = function (netotiate_offer_id) {
	var options = {
		url: Urls.addCartRescuePriceAdjustments,
		data: {netotiateOfferId: netotiate_offer_id},
		type: 'POST'
	};
    $.ajax(options).done(function (data) {
        if (typeof(data) !== 'string') {
            if (data.success) {
                //dialog.close();
                //page.refresh();
            } else {
                window.alert(data.message);
                return false;
            }
        } else {
            console.log(data);
            //dialog.close();
            page.refresh();
        }
    });
}
var cart = {
	init : function () {
    initializeEvents();
    var $addCoupon = $('#add-coupon');
	var $couponCode = $('input[name$="_couponCode"]');
	var $checkoutForm = $('.checkout-billing');
    if (SitePreferences.STORE_PICKUP) {
        cartStoreInventory.init();
    }
    account.initCartLogin();
    $("body").on('click', '.change-store', function (e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href'),
            title: $(e.target).attr('title'),
            options: {
                height: 600
            }
        });
    });
    $("body").on('click', '.select-store', function (e) {
        var selectedStore = $(e.target).val();
        setPreferredStore(selectedStore);
        $("div[class*='store']").removeClass('selected');
        $('.store_' + selectedStore).addClass('selected');
        $(".select-store:contains('Preferred Store')").closest('button').first().text('Select Store');
        $(e.target).text('Preferred Store');
    });

    $('body').on('submit', '#dwfrm_pickupinstore', function (e) {
        e.preventDefault();
        // serialize the form and get the post url
        var buttonName = $('#dwfrm_pickupinstore').find('.pickupinstore').attr('name');
        var options = {
            url: Urls.pickupInStore,
            data: $('#dwfrm_pickupinstore').serialize() + '&' + buttonName + '=x',
            type: 'POST'
        };
        $.ajax(options).done(function (data) {
            if (typeof(data) !== 'string') {
                if (data.success) {
                    //dialog.close();
                    //page.refresh();
                } else {
                    window.alert(data.message);
                    return false;
                }
            } else {
                $('#dialog-container').html(data);
                //dialog.close();
                //page.refresh();
            }
        });

    });   
    
    $(document).on("click","#showCouponDisclaimerMsgMobile", function(e){
    	e.preventDefault();
    	/*$(this).next().addClass('active');*/
    	 var $html = $(this).next().find('.couponDisclaimerMsg').children('.promo-modal').html();
    	 dialog.open({
				html: $html,
				options: {
					autoOpen: false,
					dialogClass: 'promo-modal-class'
					
				}
			});
    });
    
    /*$(document).on("click","#hideCouponDisclaimerMsg-mob", function(e){
    	e.preventDefault();
    	$(this).closest('.mobileCouponDisclaimerMsg-popup').removeClass('active');
    });*/
    
    $(document).on("click","#showCouponDisclaimerMsg", function(e){
    	e.preventDefault();
        $('.couponDisclaimerMsg').slideDown("slow");
    });
     
    $(document).on("click","#hideCouponDisclaimerMsg", function(e){
    	e.preventDefault();
    	$('.couponDisclaimerMsg').slideUp("slow");
    });
    
    //MAT-557 coupon code default behavior to ajax
  $(".primary-content").on('click',"#add-coupon",  function (e) {
		e.preventDefault();	
		 addHiddenInput( this.name );
    	 submitCartAction(e);
	});

	// trigger events on enter
   $(".primary-content").on('keydown',$couponCode, function (e) {
	   e.stopPropagation();
	   if (e.which === 13) {
			e.preventDefault();
			$("#add-coupon").click();
		}
	});
/* 
 // This code has been written in app.js because it was needed on multiple pages
 //Add Promo Code on Order Summary
   $('.primary-content').on('click', '#add-promo-code-label-id', function (e) {
   	e.preventDefault();
   	$('#add-promo-code-label-id').addClass('visually-hidden');
   	$('#add-promo-code-input-id').removeClass('visually-hidden');
   });
*/   
   
// #coupon-success-message is the id of overlay which comes after bonus product addition in basket via coupon code.
   $('#coupon-success-message').on('click tap', function (e) {
       e.preventDefault();
       $('#coupon-success-message').hide();
   });
	//#promo-success-message is the id of overlay which comes after discount applied on basket via coupon code.
   $('#promo-success-message').on('click tap', function (e) {
	   	e.preventDefault();
	   	$('#promo-success-message').hide();
   });
   
	var submitCartAction  = function (e){
		e.preventDefault();
		var $form =  $('#cart-items-form');
		var ajaxdest = $form.attr('action');
		var url = util.appendParamsToUrl(ajaxdest, {format: 'ajax'});
		var formData = $(":input").serialize(); //$form.serialize();
		$.post( url, formData )
		.done(function( text ){
			var $response = $( text );
			var divcontent = $('.item-quantity.item-quantity-details');
			$(".primary-content").html($response);
			if($('#couponError').val() == "false") {
				page.refresh();
			}
			/*if($('#couponApplied').length > 0 && $('#couponApplied').val() == "true" && $('#couponError').val() == "false") {
				if($('#couponType').val() == "BONUS") {
					$(function(){
						page.refresh();
						// #coupon-success-message is the id of overlay which comes after bonus product addition in basket via coupon code.
						$('#coupon-success-message').show();
					    setTimeout(function() {
						    $('#coupon-success-message').fadeOut('fast');
						    page.refresh();
						}, 2000);
					});
				} else {
					$(function(){
						page.refresh();
						//#promo-success-message is the id of overlay which comes after discount applied on basket via coupon code.
						$('#promo-success-message').show();
						setTimeout(function() {
						    $('#promo-success-message').fadeOut('fast');
						    page.refresh();
						}, 2000);
					});
				}
			}*/
			$(".primary-content").find('.item-quantity.item-quantity-details').each(function(e) {
				$(this).html($(divcontent[e]).html());
			});
			$("#checkout-button-ID").removeClass('disabled-btn');
		});	
	};
	function addHiddenInput( name ){
		var $form =  $('#cart-items-form');
	    $form.append( "<input type='hidden' class='form_submittype' name='" + name + "' value='true'>" );
	  }
    $("body").on('click', '.continue-with-select-store', function (e) {
        var selectedStore = $('.selected').attr('data-storeid');
        var pid = $('input[name="pid"]').val();
        var format = 'ajax';
        addProductSelectStore(selectedStore, pid, format);
    });
    global.addPersonaliCartRescue = addPersonaliCartRescue;
}
};
module.exports = cart;