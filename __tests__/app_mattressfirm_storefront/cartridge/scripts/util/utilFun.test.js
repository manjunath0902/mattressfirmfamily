/* Requiring Package To Be Tested */
const libFunctions = require('../../../../../app_mattressfirm_storefront/cartridge/scripts/util/utilFun');
const basket= require('../../../../../__mocks__/dw/order/Basket');

/** Preparing Mocks */
jest.mock('app_storefront_controllers/cartridge/scripts/app', () => ({
    getModel: jest.fn().mockImplementation((modelName) => {
        return {
            get: jest.fn().mockImplementation((parameter) => {
                var ProductMock = require('../../../../../__mocks__/dw/catalog/Product');
                let object = new ProductMock();
                object.manufacturerSKU = parameter;
                object.variationModel = "variationModel";
                /** Returning Product Object in Same Way as Product Model Returns */
                let product = new Object();
                product.object = object;
                return product;
            })
        };
    })
}));

/* Site Preferences Mock */
jest.mock('dw/system/Site', () => {
    var current = {
        preferences: {
            custom : {
                enableChicagoDCDelivery              : true,
                parcelableProductIDs               : ['126721', '134673']
            }
        },
        getCustomPreferenceValue: jest.fn().mockImplementation((key) => {
            return current.preferences.custom[key];
        })
    };
    return {
        current: current
    };
});
empty = jest.fn().mockImplementationOnce((key) => {
	if(typeof key == 'undefined' || key == "" || key == null) {
		return true;
	} else {
		return false;
	}
});

jest.mock('dw/order/Basket', () => {
	var basket = {
		shipments: {
			"In-Market": {
				productLineItems: {
					length: 2
				}
			},
			"Drop-Ship": {
				productLineItems: [
					{
						custom : {
							isDeliveryTier : "false"
						}
					}
				]
			},
			"Parcel": {
				productLineItems: {
					length: 2
				}
			},
			"Nationwide": {
				productLineItems: {
					length: 2
				}
			}
		},
		getShipment: jest.fn().mockImplementation((key) => {
			return basket.shipments[key];
		})
	};
	return basket;
});

describe('isChicagoProduct() Test Suit', () => {
    beforeAll(() => {
      /* Runs before all tests */
      /** Defining Globals */
        global.empty = jest.fn().mockReturnValueOnce(true);
        //mockImplementation((args) => {
            //return args ? true : false;
        //});
    });
    afterAll(() => {
      /* Runs after all tests */
      jest.clearAllMocks();
    });
    beforeEach(() => {
      /* Runs before each test */
    });
    afterEach(() => {
      /* Runs after each test */
    });
    
    test('Should Return True', () => {
        var result = libFunctions.UtilFunctions.isChicagoProduct("126721");//V000099611
        expect(result).toBeTruthy();
    });

    test('Should Return Any Boolean', () => {
        var result = libFunctions.UtilFunctions.isChicagoProduct();
        expect(result).toEqual(expect.any(Boolean));
    });
    
    test('Should Return False', () => {
        var result = libFunctions.UtilFunctions.isChicagoProduct("");
        expect(result).toBeFalsy();
    });
    
  });

  describe('chicagoInventLocationId() Test Suit', () => {
    beforeAll(() => {
      /* Runs before all tests */
      /* Site Preferences Mock */
        jest.mock('dw/system/Site', () => {
            var current = {
                preferences: {
                    custom : {
                        enableChicagoDCDelivery              : true,
                        chicagoInventLocationId              : '119800'
                    }
                },
                getCustomPreferenceValue: jest.fn().mockImplementation((key) => {
                    return current.preferences.custom[key];
                })
            };
            return {
                current:        current
            };
        });
    });
    afterAll(() => {
      /* Runs after all tests */
    });
    beforeEach(() => {
      /* Runs before each test */
    });
    afterEach(() => {
      /* Runs after each test */
    });
    
    var defaultLocationID = "119800";
    it('Should Return Invnetory Location ID', () => {
        var result = libFunctions.UtilFunctions.chicagoInventLocationId();
        expect(defaultLocationID).toEqual('119800');
    });
    
    it('Should Return Empty Invnetory Location ID', () => {
        var result = libFunctions.UtilFunctions.chicagoInventLocationId();
        expect('').not.toEqual('119800');
    });
  });

describe('Valid shipment count', () => {
    test('Should Return an integer', () => {
        var result = libFunctions.UtilFunctions.validShipmentsCount(basket);
		expect(result).toBeLessThan(5);
    });
  });
