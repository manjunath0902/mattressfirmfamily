const GlobalUtilities = require('./../../modules/GlobalUtilities');
var Site = require('dw/system/Site');

describe('Tests Suit for Global Utills Library', () => {
    /** Tests Start */

    test('getSfCustomerPortalBaseURL() Function Should Return Valid Base URL', () => {
        Site.setCurrentTestSite(new Site());
        var currentSite = Site.getCurrent();
        currentSite.setTestCusomPreferences({
            sfCustomerPortalBaseURL: "https://dev-mattressfirmcustomercommunity.cs4.force.com"
        });
        var result = isValidUrl(GlobalUtilities.getSfCustomerPortalBaseURL());
        expect(result).toBeTruthy();
    });

    test('getSfWarantyClaimURL() Function Should Return Valid Combined Waranty Claim URL of Base + Relative', () => {
        Site.setCurrentTestSite(new Site());
        var currentSite = Site.getCurrent();
        currentSite.setTestCusomPreferences({
            sfCustomerPortalBaseURL: "https://dev-mattressfirmcustomercommunity.cs4.force.com",
            sfWarrantyClaimUrl: "/s/warranty-claim"
        });
        var result = isValidUrl(GlobalUtilities.getSfCustomerPortalBaseURL());
        expect(result).toBeTruthy();
    });

    test('getSfWarantyClaimURL() Function Should Not Return Valid Combined Waranty Claim URL of Base + Relative', () => {
        Site.setCurrentTestSite(new Site());
        var currentSite = Site.getCurrent();
        currentSite.setTestCusomPreferences({
            sfCustomerPortalBaseURL: "dev-mattressfirmcustomercommunity.cs4.force.com",
            sfWarrantyClaimUrl: "/s/warranty-claim"
        });
        var result = isValidUrl(GlobalUtilities.getSfCustomerPortalBaseURL());
        expect(result).not.toBeTruthy();
    });

    test('getSfOAuth2ProviderID() Function Should Return Valid Auth2 Provider ID', () => {
        Site.setCurrentTestSite(new Site());
        var result = GlobalUtilities.getSfOAuth2ProviderID();
        expect(result).toBe("CommunityCloud");
    });

    test('getSfCustomerPortalCreateAccount() Function Should Return Valid Combined Register URL of Base + Relative', () => {
        Site.setCurrentTestSite(new Site());
        var currentSite = Site.getCurrent();
        currentSite.setTestCusomPreferences({
            sfCustomerPortalBaseURL: "https://dev-mattressfirmcustomercommunity.cs4.force.com",
            sfCreateAccountLink: "/s/login/SelfRegister"
        });
        var result = isValidUrl(GlobalUtilities.getSfCustomerPortalCreateAccount());
        expect(result).toBeTruthy();
    });

    test('getSfCustomerPortalCreateAccount() Function Should Not Return Valid Combined Register URL of Base + Relative', () => {
        Site.setCurrentTestSite(new Site());
        var currentSite = Site.getCurrent();
        currentSite.setTestCusomPreferences({
            sfCustomerPortalBaseURL: "dev-mattressfirmcustomercommunity.cs4.force.comSelfRegister",
            sfCreateAccountLink: "SelfRegister"
        });
        var result = isValidUrl(GlobalUtilities.getSfCustomerPortalCreateAccount());
        expect(result).not.toBeTruthy();
    });

    test('getSfCustomerPortalLandingURL() Function Should Return Valid Landing URL', () => {
        Site.setCurrentTestSite(new Site());
        var currentSite = Site.getCurrent();
        currentSite.setTestCusomPreferences({
            sfCustomerPortalBaseURL: "https://dev-mattressfirmcustomercommunity.cs4.force.com"
        });
        var result = isValidUrl(GlobalUtilities.getSfCustomerPortalLandingURL());
        expect(result).toBeTruthy();
    });

    test('getSfCustomerPortalLogoutURL() Function Should Return Valid Combined Logout URL of Base + Relative', () => {
        Site.setCurrentTestSite(new Site());
        var currentSite = Site.getCurrent();
        currentSite.setTestCusomPreferences({
            sfCustomerPortalBaseURL: "https://dev-mattressfirmcustomercommunity.cs4.force.com",
            sfLogoutURL: "/secur/logout.jsp"
        });
        var result = isValidUrl(GlobalUtilities.getSfCustomerPortalLogoutURL());
        expect(result).toBeTruthy();
    });

    test('getSfCustomerPortalCreateAccount() Function Should Not Return Valid Combined Logout URL of Base + Relative', () => {
        Site.setCurrentTestSite(new Site());
        var currentSite = Site.getCurrent();
        currentSite.setTestCusomPreferences({
            sfCustomerPortalBaseURL: "dev-mattressfirmcustomercommunity.cs4.force.comSelfRegister",
            sfLogoutURL: ".jsp"
        });
        var result = isValidUrl(GlobalUtilities.getSfCustomerPortalLogoutURL());
        expect(result).not.toBeTruthy();
    });
    /** Tests End */
});

function isValidUrl(str) {
    try {
      new URL(str);
    } catch (e) {
      return false;  
    }
  
    return true;
}