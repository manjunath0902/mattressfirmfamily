/* Requiring Package To Be Tested */
const Brightspot = require('./../../modules/Brightspot');
var Site = require('dw/system/Site');
var ServiceHelper = require("GeneralService");

/** Define Global Asset's Object */
var element = {};

describe('validateAsset() Tests Suit for Site ID (Mattress-Firm)', () => {
    /** Tests Start */
    test('Function Should Return True When Site ID is Mattress-Firm', () => {
        initializeTestData();
        var result = Brightspot.validateAsset(element);
        expect(result).toBeTruthy();
        resetTestData();
    });

    test('Function Should Return False When Site ID is Not Mattress-Firm', () => {
        initializeTestData();
        Site.setCurrentTestSite(new Site("Test-Site"));
        var result = Brightspot.validateAsset(element);
        expect(result).not.toBeTruthy();
        resetTestData();
    });
    /** Tests End */
});

describe('validateAsset() Tests Suit for Custom Attribute (isBrightspotAsset)', () => {
    /** Tests Start */
    test('Function Should Return True When Custom Attribute (isBrightspotAsset) is True', () => {
        initializeTestData();
        element.custom.isBrightspotAsset = true;
        var result = Brightspot.validateAsset(element);
        expect(result).toBeTruthy();
        resetTestData();
    });

    test('Function Should Return False When Custom Attribute (isBrightspotAsset) is False', () => {
        initializeTestData();
        element.custom.isBrightspotAsset = false;
        var result = Brightspot.validateAsset(element);
        expect(result).not.toBeTruthy();
        resetTestData();
    });

    test('Function Should Return False When Custom Attribute (isBrightspotAsset) is Null', () => {
        initializeTestData();
        element.custom.isBrightspotAsset = null;
        var result = Brightspot.validateAsset(element);
        expect(result).not.toBeTruthy();
        resetTestData();
    });
    /** Tests End */
});

describe('validateAsset() Tests Suit for Custom Attribute (brightspotPagePath)', () => {
    /** Tests Start */
    test('Function Should Return True When Custom Attribute (brightspotPagePath) Has Some Value', () => {
        initializeTestData();
        element.custom.brightspotPagePath = {
            displayValue: "Some Value",
            value: "Some Value"
        };
        var result = Brightspot.validateAsset(element);
        expect(result).toBeTruthy();
        resetTestData();
    });

    test('Function Should Return True When Custom Attribute (brightspotPagePath) Has Nothing', () => {
        initializeTestData();
        element.custom.brightspotPagePath = undefined;
        var result = Brightspot.validateAsset(element);
        expect(result).not.toBeTruthy();
        resetTestData();
    });
    /** Tests End */

});

describe('validateSlot() Tests Suit for Custom Attribute (isBrightspotSlot)', () => {
    /** Tests Start */
    test('Function Should Return True When Custom Attribute (isBrightspotSlot) is True', () => {
        initializeTestData();
        element.custom.isBrightspotSlot = true;
        var result = Brightspot.validateSlot(element);
        expect(result).toBeTruthy();
        resetTestData();
    });

    test('Function Should Return False When Custom Attribute (isBrightspotSlot) is False', () => {
        initializeTestData();
        element.custom.isBrightspotSlot = false;
        var result = Brightspot.validateSlot(element);
        expect(result).not.toBeTruthy();
        resetTestData();
    });

    test('Function Should Return False When Custom Attribute (isBrightspotSlot) is Null', () => {
        initializeTestData();
        element.custom.isBrightspotSlot = null;
        var result = Brightspot.validateSlot(element);
        expect(result).not.toBeTruthy();
        resetTestData();
    });
    /** Tests End */
});

describe('validateSlot() Tests Suit for Custom Attribute (brightspotPagePath)', () => {
    /** Tests Start */
    test('Function Should Return True When Custom Attribute (brightspotPagePath) Has Some Value', () => {
        initializeTestData();
        element.custom.brightspotPagePath = {
            displayValue: "Some Value",
            value: "Some Value"
        };
        var result = Brightspot.validateSlot(element);
        expect(result).toBeTruthy();
        resetTestData();
    });

    test('Function Should Return True When Custom Attribute (brightspotPagePath) Has Nothing', () => {
        initializeTestData();
        element.custom.brightspotPagePath = undefined;
        var result = Brightspot.validateSlot(element);
        expect(result).not.toBeTruthy();
        resetTestData();
    });
    /** Tests End */

});

describe('renderBrightspotAsset() Tests Suit for Assets SSR Rendering', () => {
    /** Tests Start */
    test('Function Should Return Valid HTM When Valid Asset is Passed', () => {
        initializeTestData();
        /* Site Preferences Mock */
        jest.mock('GeneralService', () => {
            return {
                createService: jest.fn().mockImplementation(() => {
                    return {
                        getURL : jest.fn().mockImplementation(() => {
                            return jest.fn().mockReturnValue("Some.Service.Endpoint");
                        }),
                        setURL : jest.fn().mockImplementation(() => {}),
                        call : jest.fn().mockImplementation(() => {
                            return {
                                error: 0,
                                errorMessage: null,
                                mockResult: null,
                                object: '<!DOCTYPE html><html><head></head></html>'
                            }
                        }),
                    };
                })
            };
        });
        var result = Brightspot.renderBrightspotAsset(element);
        expect(result).toMatch(/brightspot/);
        resetTestData();
    });

    test('Function Should Return Valid HTM When Valid Slot is Passed', () => {
        initializeTestData();
        /* Site Preferences Mock */
        jest.mock('GeneralService', () => {
            return {
                createService: jest.fn().mockImplementation(() => {
                    return {
                        getURL : jest.fn().mockImplementation(() => {
                            return jest.fn().mockReturnValue("Some.Service.Endpoint");
                        }),
                        setURL : jest.fn().mockImplementation(() => {}),
                        call : jest.fn().mockImplementation(() => {
                            return {
                                error: 0,
                                errorMessage: null,
                                mockResult: null,
                                object: '<!DOCTYPE html><html><head></head></html>'
                            }
                        }),
                    };
                })
            };
        });
        var result = Brightspot.renderBrightspotAsset(element);
        expect(result).toMatch(/brightspot/);
        resetTestData();
    });

    test('Function Should Not Return Valid HTM When Wrong Slot is Passed', () => {
        initializeTestData();
        //setting wrong custom attribute
        element.custom.isBrightspotSlot = null;
        element.custom.isBrightspotAsset = null;

        /* Site Preferences Mock */
        jest.mock('GeneralService', () => {
            return {
                createService: jest.fn().mockImplementation(() => {
                    return {
                        getURL : jest.fn().mockImplementation(() => {
                            return jest.fn().mockReturnValue("Some.Service.Endpoint");
                        }),
                        setURL : jest.fn().mockImplementation(() => {}),
                        call : jest.fn().mockImplementation(() => {
                            return {
                                error: 0,
                                errorMessage: null,
                                mockResult: null,
                                object: '<!DOCTYPE html><html><head></head></html>'
                            }
                        }),
                    };
                })
            };
        });
        var result = Brightspot.renderBrightspotAsset(element);
        expect(result).toMatch(/Some Error Occured While Calling Brightspot Service/);
        resetTestData();
    });

    test('Function Should Not Return Valid HTM When Wrong Asset is Passed', () => {
        initializeTestData();
        //setting wrong custom attribute
        element.custom.isBrightspotSlot = null;
        element.custom.isBrightspotAsset = null;

        /* Site Preferences Mock */
        jest.mock('GeneralService', () => {
            return {
                createService: jest.fn().mockImplementation(() => {
                    return {
                        getURL : jest.fn().mockImplementation(() => {
                            return jest.fn().mockReturnValue("Some.Service.Endpoint");
                        }),
                        setURL : jest.fn().mockImplementation(() => {}),
                        call : jest.fn().mockImplementation(() => {
                            return {
                                error: 0,
                                errorMessage: null,
                                mockResult: null,
                                object: '<!DOCTYPE html><html><head></head></html>'
                            }
                        }),
                    };
                })
            };
        });
        var result = Brightspot.renderBrightspotAsset(element);
        expect(result).toMatch(/Some Error Occured While Calling Brightspot Service/);
        resetTestData();
    });
    /** Tests End */

});

function initializeTestData() {
    element = {
        custom: {
            isBrightspotAsset: true,
            isBrightspotSlot: true,
            brightspotPagePath: {
                displayValue: "Back to School Sale",
                value:        "back-to-school-sale?syndicate=true"
            }
        },
        isMockedEelement: true
    };
    Site.setCurrentTestSite(new Site("Mattress-Firm"));
    jest.clearAllMocks();
}

function resetTestData() {
    element = null;
}