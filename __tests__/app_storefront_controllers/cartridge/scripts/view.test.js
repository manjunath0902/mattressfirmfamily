  /* Requiring Package To Be Tested */
  const view = require('./../../../../app_storefront_controllers/cartridge/scripts/view');

  /* Setting up empty decorator string */
  var emptyDecorator = "util/pt_empty";

  /* Declaring function parameters */
  var decoratorName = null;
  var customValues = new Array().fill(null);

  /* Requiring Request Mock */
  var Request = require('./../../../../__mocks__/dw/system/Request');

  /* Test Suite */
  describe('Decorator Script', () => {
    beforeAll(() => {
      global.request = new Request();
    });


    beforeEach(() => {
        customValues.fill(null);
    });

    /** Test 1 */
    test('Should Match with Empty Decorator', () => {

        //setting request query string
        global.request.httpParameters = new Object();
        global.request.httpParameters.format = "ajax";
        decoratorName = "util/pt_empty";

        //Calling view decorator functions
        let result = view.decorate(decoratorName, customValues);

        //Pass returned result to expect function
        expect(result).toEqual(emptyDecorator);
    });


    /** Test 2 */
    test('Should Not Match with Empty Decorator', () => {

        //setting request query string
        global.request.httpParameters = new Object();
        global.request.httpParameters.format = null;
        decoratorName = "";

        //Calling view decorator functions
        let result = view.decorate(decoratorName, customValues);

        //Pass returned result to expect function
        expect(result).not.toEqual(emptyDecorator);
    });

    /** Test 3 */
    test('Should Not Return Empty Decorator String with Http Param', () => {

        //setting request query string
        global.request.httpParameters = new Object();
        global.request.httpParameters.format = "ajax";
        decoratorName = "util/pt_empty";

        //Calling view decorator functions
        let result = view.decorate(decoratorName, customValues);

        //Pass returned result to expect function
        expect(result).not.toEqual(null);
    });

    /** Test 4 */
    test('Should Not Return Empty Decorator String without Http Param', () => {

        //setting request query string
        global.request.httpParameters = new Object();
        global.request.httpParameters.format = null;
        decoratorName = "util/pt_empty";

        //Calling view decorator functions
        let result = view.decorate(decoratorName, customValues);

        //Pass returned result to expect function
        expect(result).not.toEqual(null);
    });

    /** Test 5 */
    test('Should Return Empty Decorator String with Http Param & Non Empty Array', () => {

        //setting request query string
        global.request.httpParameters = new Object();
        global.request.httpParameters.format = "ajax";
        decoratorName = "util/pt_empty";
        customValues.push("some value");

        //Calling view decorator functions
        let result = view.decorate(decoratorName, customValues);

        //Pass returned result to expect function
        expect(result).not.toEqual(null);
    });

  });