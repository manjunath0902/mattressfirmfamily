var tealiumHeldper = require("../../../../../int_tealium_controller/cartridge/scripts/helpers/tealiumHelper");
var Request = require("dw/system/Request");
var Session = require("dw/system/Session");
//var Stores = require("app_storefront_controllers/cartridge/controllers/Stores");
describe("getValidZipCode() - Tests Suit for valid Zip Code", () => {
	beforeEach(() => {
		global.request = new Request();
		global.session = new Session();
	});
	afterEach(() => {
		global.request = null;
		global.session = null;
	});
	beforeAll(() => {
		global.empty = jest
			.fn()
			.mockReturnValue(false) // Default
			.mockReturnValueOnce(true) // First call
			.mockReturnValueOnce(true); // 2nd call
	});
	/** Tests Start */
	test("unit-test-1 - With Empty Request and Session", () => {
		global.request.setzipcode("");
		const customZipCode = {
			customerZip: "",
		};
		var result = tealiumHeldper.getValidZipCode();
		expect(result).toBeNull;
	});
	test("unit-test-2 - With undefined Request and Session", () => {
		var result = tealiumHeldper.getValidZipCode();
		expect(result).toBeUndefined;
	});
	test("unit-test-3 - Return zipcode from session", () => {
		global.request.setzipcode("08536");
		const customZipCode = {
			customerZip: "77025",
		};
		global.session.setCustom(customZipCode);
		var result = tealiumHeldper.getValidZipCode();
		expect(result).toBe("77025");
	});

	test("unit-test-4 - Return zip code from Request", () => {
		global.request.setzipcode("08536");
		const customZipCode = {
			customerZip: "00000",
		};
		global.session.setCustom(customZipCode);
		var result = tealiumHeldper.getValidZipCode();
		expect(result).toBe("08536");
	});
	/** Tests End */
});

jest.mock("app_storefront_controllers/cartridge/controllers/Stores", () => {
	return {
		GetStoresByGeoLocation: jest.fn().mockImplementation((zipcode) => {
			var nearestStores = [
				{
					ID: "162047",
					name: "Princeton",
					address1: "3520 Brunswick Pike",
					address2: "3520 Brunswick Pike",
					city: "Princeton",
					stateCode: "NJ",
					postalCode: "08540",
					countryCode: "92",
					phone: "(609) 919-0924",
					email: "customerservice@mattressfirm.com",
					lat: "40.309946",
					lng: "-74.662789",
					distance: "4.4",
					storeHours: "9 to 5",
					directionsLink:
						"https://www.google.com/maps?hl=en&f=q&q=3520+Brunswick+Pike,+Princeton,+08540,+NJ,+US",
					warehouseId: "166800",
				},
				{
					ID: "162059",
					name: "East Windsor Town Center",
					address1: "370 US Highway 130",
					address2: "370 US Highway 130",
					city: "East Windsor",
					stateCode: "NJ",
					postalCode: "08520",
					countryCode: "92",
					phone: "(609) 919-0924",
					email: "customerservice@mattressfirm.com",
					lat: "40.309946",
					lng: "-74.662789",
					distance: "5.0",
					storeHours: "9 to 5",
					directionsLink:
						"https://www.google.com/maps?hl=en&f=q&q=3520+Brunswick+Pike,+Princeton,+08540,+NJ,+US",
					warehouseId: "166800",
				},
				{
					ID: "166800",
					name: "Mercerville",
					address1: "1297 Route 33",
					address2: "1297 Route 33",
					city: "Hamilton Township",
					stateCode: "NJ",
					postalCode: "08690",
					countryCode: "92",
					phone: "(609) 586-1048",
					email: "customerservice@mattressfirm.com",
					lat: "40.309946",
					lng: "-74.662789",
					distance: "8.6",
					storeHours: "9 to 5",
					directionsLink:
						"https://www.google.com/maps?hl=en&f=q&q=3520+Brunswick+Pike,+Princeton,+08540,+NJ,+US",
					warehouseId: "166800",
				},
			];
			return nearestStores.filter(
				(store) => store.postalCode === zipcode
			);
		}),
	};
});
describe("getpreferedStore - Tests Suit for prefered Store", () => {
	/** Tests Start */
	test("unit-test-1 - Check a zipcode which don't have store", () => {
		//passing a zipcode which does not exist in mock collection
		var result = tealiumHeldper.getPreferedStoreByZipCode("0811");
		expect(result).toBeNull;
	});
	test("unit-test-2 - Check a zipcode which have store", () => {
		//passing a zipcode which does not exist in mock collection
		var result = tealiumHeldper.getPreferedStoreByZipCode("08540");
		expect(result.postalCode).toBe("08540");
	});
});
