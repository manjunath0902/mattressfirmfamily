/* Requiring Package To Be Tested */
const wmapHelpers = require('../../../../../int_makepayment/cartridge/scripts/util/wmapHelpers');

describe('isServiceError() Test Suit', () => {
    test('Should return true if error is not equal to 0, error message available or mockResult is true in result' , () => {
        var result = wmapHelpers.UtilHelpers.isServiceError({error:20 , errorMessage : "error" , mockResult:true});
		expect(result).toBeTruthy();
    });

    test('Should return false if error is equal to 0, no error message available or mock result is false in result', () => {
        var result = wmapHelpers.UtilHelpers.isServiceError({error:0 , errorMessage : null , mockResult:false});
		expect(result).toBeFalsy();
    });

    test('Should return true if error is 0 , error message is some string and moke result is false', () => {
        var result = wmapHelpers.UtilHelpers.isServiceError({error:0 , errorMessage : 'error' , mockResult:false});
		expect(result).toBeTruthy();
    });

    test('Should return true if error is 0 , error message is null and moke result is true', () => {
        var result = wmapHelpers.UtilHelpers.isServiceError({error:0 , errorMessage : null , mockResult:true});
		expect(result).toBeTruthy();
    });

    test('Should return true if error is not 0 , error message is null and moke result is false', () => {
        var result = wmapHelpers.UtilHelpers.isServiceError({error:20 , errorMessage : null , mockResult:false});
		expect(result).toBeTruthy();
    });

    test('Should return true if error is 0 , error message is some string and moke result is true', () => {
        var result = wmapHelpers.UtilHelpers.isServiceError({error:0 , errorMessage : "error" , mockResult:true});
		expect(result).toBeTruthy();
    });

    test('Should return false if error is not equal to 0, error message available and mock result is false in result', () => {
        var result = wmapHelpers.UtilHelpers.isServiceError({error:20 , errorMessage : "error" , mockResult:false});
		expect(result).toBeTruthy();
    });

    test('Should return false if error is not equal to 0, no error message available and mock result is true in result', () => {
        var result = wmapHelpers.UtilHelpers.isServiceError({error:20 , errorMessage : null , mockResult:true});
		expect(result).toBeTruthy();
    });

});

describe('removeMakeaPaymentOrdersCustomObject() Test Suit', () => {
    test('Should return true if cl code is true ' , () => {
        var result = wmapHelpers.UtilHelpers.isOrderPaidOrCancelled(4002);
		    expect(result).toBeTruthy();
    });

    test('Should return true if cl code is true ' , () => {
      var result = wmapHelpers.UtilHelpers.isOrderPaidOrCancelled(4004);
      expect(result).toBeTruthy();
    });

    test('Should return true if cl code is true ' , () => {
      var result = wmapHelpers.UtilHelpers.isOrderPaidOrCancelled(null);
      expect(result).toBeFalsy();
    });
});


