# README 

This repository is digested by the [sleepys](/mediahive/sleepys/wiki/Development_Workflow/#markdown-header-mattresscom) repository as a git-submodule; the steps that were taken to achieve this (which should not need to be repeated) were:

```
$ cd sleepys
$ git checkout development
$ git submodule add git@bitbucket.org:mediahive/mattresscom.git cartridges/app_mattresscom
$ git add .gitmodules cartridges/app_mattresscom
$ git commit -am "Add app_mattresscom cartridge as git-submodule"
$ git push
```

Note: With the addition of the mattress.com submodule the Jenkins CI configuration for the sleepys job had to be updated to include a new "Invoke Ant" build step with a `sass.compile` target for the app_mattresscom build file.