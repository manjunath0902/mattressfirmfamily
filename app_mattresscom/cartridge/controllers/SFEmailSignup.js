'use strict';

/**
 * Controller 
 *
 * @module controllers/SFEmailSignup
 */

/* API includes */
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var StringUtils = require('dw/util/StringUtils');
var Transaction = require('dw/system/Transaction');

/* Script Modules */
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var emailHelper = require("~/cartridge/scripts/util/SFEmailSubscriptionHelper").SFEmailSubscriptionHelper; 

function showForm() 
{
	app.getView('EmailSignup').render('emailsubscriptions/SFEmailModalForm');
}

// Recieves the AJAX submission of email address and responds
function submitEmail() {
	var params = request.httpParameterMap;
	var emailAddress : String = params.emailAddress.stringValue;
	var firstName : String = params.firstName.stringValue;
	var leadSource : String = params.leadSource.stringValue;
	var zipCode : String = params.zipCode.stringValue;
	var zipCodeModal : String = params.zipCodeModal.stringValue;
	var siteId : String = params.siteId.stringValue;
	var optOutFlag = params.optOutFlag.booleanValue;
	var gclid = params.gclid.stringValue;

	//save e-mail address using lower case
	emailAddress = emailAddress.toLowerCase();
	// REGEX to check email
	if (!/^[a-zA-Z0-9]+(?:(\.|_)[A-Za-z0-9!#$%&'*+/=?^`{|}~-]+)*@(?!([a-zA-Z0-9]*\.[a-zA-Z0-9]*\.[a-zA-Z0-9]*\.))(?:[A-Za-z0-9](?:[a-zA-Z0-9-]*[A-Za-z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/.test(emailAddress)) {
	// email falied validation
	}
	var emailParams = {
			emailAddress: emailAddress, 
			firstName: firstName, 
			zipCode: zipCode, 
			zipCodeModal: zipCodeModal, 
			leadSource: leadSource, 
			siteId: siteId, 
			optOutFlag: optOutFlag,
			gclid: gclid,
			dwsid: session.sessionID
			};
	
	var returnResult = emailHelper.sendSFEmailInfo(emailParams);
	
	if (leadSource == 'modal') {
		var responseTemplate = 'SFEmailResponseModal';
	} else {
		var responseTemplate = 'SFEmailResponseFooter';
	}
	if (leadSource == 'mattress_finder') {
		if (returnResult.Status == 'SERVICE_ERROR'){
			emailHelper.sendFailSafe(emailParams, returnResult.ErrorCode);
		}
		app.getView({
			JSONResponse: returnResult
		}).render('util/responsejson');
	} else {
		if (returnResult.Status == 'SERVICE_ERROR'){
			var returnResult = emailHelper.sendFailSafe(emailParams, returnResult.ErrorCode);
			app.getView('EmailSignup', {SFEmailStatus : returnResult.Status, SFEmailMsg : '', SFEmailErrorCode: '', contactID: ''}).render('emailsubscriptions/' + responseTemplate);
		} else {
			app.getView('EmailSignup', {SFEmailStatus : returnResult.Status, SFEmailMsg : returnResult.Msg, SFEmailErrorCode: returnResult.ErrorCode, contactID: returnResult.ContactID}).render('emailsubscriptions/' + responseTemplate);
		}
	}
	
}

function submitQuestions() {
	var params = request.httpParameterMap;
	var questionNum : Number = params.questionNum.value;
	var numQuestions : Number = params.numQuestions.value;
	var contactID : String = params.contactID.stringValue;
	
	if (questionNum == numQuestions) {
		var returnResult = emailHelper.sendSFQuestions(params);
		var responseTemplate = 'SFQuestionsResponseModal';
		
		if (returnResult.Status == 'SERVICE_ERROR'){
			var returnResult = emailHelper.sendQuestionsFailSafe(params, 'modal', returnResult.ErrorCode);
			app.getView('Questions', {SFEmailStatus : returnResult.Status, SFEmailMsg : '', SFEmailErrorCode: '', ContactID: returnResult.ContactID}).render('emailsubscriptions/' + responseTemplate);
		} else {
			app.getView('Questions', {SFEmailStatus : returnResult.Status, SFEmailMsg : '', SFEmailErrorCode: '', ContactID: returnResult.ContactID}).render('emailsubscriptions/' + responseTemplate);
		}
	} else {
		var responseTemplate = 'SFQuestions';
		app.getView('Questions',  params).render('emailsubscriptions/' + responseTemplate);
	}		
}

function submitQuestionsLanding() {
	var params = request.httpParameterMap;
	var questionNum : Number = params.questionNum.value ? params.questionNum.value : 0;
	var numQuestions : Number = params.numQuestions.value ? params.numQuestions.value : 1;
	
	if (questionNum == numQuestions) {
		var returnResult = emailHelper.sendSFQuestions(params);
		var responseTemplate = 'SFQuestionsResponseLanding';
		
		if (returnResult.Status == 'SERVICE_ERROR'){
			var returnResult = emailHelper.sendQuestionsFailSafe(params, 'landing', returnResult.ErrorCode);
			app.getView('Questions', {SFEmailStatus : returnResult.Status, SFEmailMsg : '', SFEmailErrorCode: '', ContactID: returnResult.ContactID}).render('emailsubscriptions/' + responseTemplate);
		} else {
			app.getView('Questions', {SFEmailStatus : returnResult.Status, SFEmailMsg : '', SFEmailErrorCode: '', ContactID: returnResult.ContactID}).render('emailsubscriptions/' + responseTemplate);
		}
	} else {
		var responseTemplate = 'SFQuestionsLanding';
		app.getView('Questions',  params).render('emailsubscriptions/' + responseTemplate);
	}		
}

function processFailSafe() {
	var returnResult = emailHelper.processFailSafe();
	return returnResult;
}

function processQuestionsFailSafe() {
	var returnResult = emailHelper.processQuestionsFailSafe();
	return returnResult;
}

exports.Submit = guard.ensure(['post'], submitEmail);
exports.SubmitQuestions = guard.ensure(['post'], submitQuestions);
exports.SubmitQuestionsLanding = guard.ensure(['https', 'get'], submitQuestionsLanding);
exports.ProcessFailSafe = processFailSafe;
exports.ProcessQuestionsFailSafe = processQuestionsFailSafe;
exports.Form = guard.ensure(['get'], showForm);

