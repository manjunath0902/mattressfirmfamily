/**
 *    (c) 2009-2014 Demandware Inc.
 *    Subject to standard usage terms and conditions
 *    For all details and documentation:
 *    https://bitbucket.com/demandware/sitegenesis
 */

"use strict";

var dialog = require("./dialog"),
util = require("./util"),
sfemaildialog = require("./sfemaildialog");

// if jQuery has not been loaded, load from google cdn
if (!window.jQuery) {
	var s = document.createElement("script");
	s.setAttribute(
		"src",
		"https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"
	);
	s.setAttribute("type", "text/javascript");
	document.getElementsByTagName("head")[0].appendChild(s);
}

require("./jquery-ext")();
//require("./cookieprivacy")();
//require('./captcha')();



/**
 * @private
 * @function
 * @description Adds class ('js') to html for css targeting and loads js specific styles.
 */



var app = {
	init: function() {
		sfemaildialog.init();
	}
};


// initialize ap
$(document).ready(function() {
	app.init();
});






