<iscomment>
	This is the footer for all pages. Be careful caching it if it contains
	user dependent information. Cache its elements instead if necessary (do not forget
	to change the isinclude into a pipeline include for that).
</iscomment>
<isinclude template="util/modules"/>

<footer id="footer" role="contentinfo"> 
	<div class="wrapper">
		<nav class="footer-left">
   		 	<ul class="menu-category level-1">
	   		 <li class="col-6">
		   		 <h5 class="level-1">${Resource.msg('footer.column1Header','common',null)} </h5>
		   		 <div class="level-2 ">
			   		 <div class="menu-wrapper">
				   		 <ul class="level-2">
					   		 <iscomponent pipeline="Page-Include" cid="footer-col1" />
				   		 </ul>
				   	</div> 
				  </div>
			 </li>            
			  <li class="col-6">
		   		<h5 class="level-1">${Resource.msg('footer.column2Header','common',null)}</h5>
		   		 <div class="level-2 ">
			   		 <div class="menu-wrapper">
				   		 <ul class="level-2">
					   		 <iscomponent pipeline="Page-Include" cid="footer-col2" />
				   		 </ul>
				   	</div> 
				  </div>
			 </li>
			 
			<li class="col-6">
		   		 <h5 href="${URLUtils.url('Page-Show', 'cid', 'cs-landing', 'fdid', 'customer-service')}" class="level-1">${Resource.msg('footer.column3Header','common',null)}</h5>
		   		 <div class="level-2 ">
			   		 <div class="menu-wrapper">
				   		 <ul class="level-2">
					   		 <iscomponent pipeline="Page-Include" cid="footer-col3" />
				   		 </ul>
				   	</div>
				  </div>
			 </li>	
			 
			 <li class="col-6">
		    
		   		 <h5 class="level-1">${Resource.msg('footer.column4Header','common',null)}</h5>
	 
		   		 <div class="level-2 ">
			   		 <div class="menu-wrapper">
				   		 <ul class="level-2">
					   		 <iscomponent pipeline="Page-Include" cid="footer-col4" />
				   		 </ul>
				   	</div>
				  </div>
			 </li>
			 
			 <li class="col-6">
		   		 <h5 href="${URLUtils.url('Stores-Find', 'page', 'our-stores')}" class="level-1">${Resource.msg('footer.column5Header','common',null)}</h5>
		   		 <div class="level-2 ">
			   		 <div class="menu-wrapper">
			   		 	 <ul class="level-2">
					   		 <iscomponent pipeline="Page-Include" cid="footer-col5" />
				   		 </ul>
				   	</div>
				  </div>
			 </li>
			 
		  </ul>
		  
		</nav>
		
		<div class="footer-right">
 
			<div class="email-signup">
				<h5 class="newsletter">${Resource.msg('forms.emailsignup.newsletter','forms',null)}</h5>			
				<div class="email-text"><isprint value="${Resource.msg('forms.emailsignup.wehavenewdeals','forms',null)}" encoding="off"/></div>			
				<form action="${URLUtils.continueURL()}" method="post" name="emailsignup" id="emailsignup" class="clearfix">
					<div class="email-signup-field">
						<label class="vh" for="${pdict.CurrentForms.emailsignup.email.htmlName}">${Resource.msg('forms.subscribe.email.default','forms',null)}</label>
						<input type="text" name="${pdict.CurrentForms.emailsignup.email.htmlName}" id="${pdict.CurrentForms.emailsignup.email.htmlName}" type="input" placeholder="${Resource.msg('forms.emailsignup.emailexample','forms',null)}" />
						<input type="hidden" name="pageSource" id="pageSourceFooter" value="footer" />
						<input type="submit" class="image-email" value="${Resource.msg('global.submit','locale',null)}" name="${pdict.CurrentForms.emailsignup.subscribe.htmlName}" />
					</div>
				</form>
			</div>
			 
			<div class="footer-social"> 
				<iscontentasset aid="social-sites" />
			</div>
		
		</div>
		
		<div class="nav-bottom-copyright primary-section-divider">
 
			<iscontentasset aid="footer-copy"/>
		
		</div>
	
	</div><!-- end /wrapper -->
</footer><!-- /footer -->
	


<iscomment>
	Customer registration can happen everywhere in the page flow. As special tag in the pdict
	is indicating it. So we have to check on every page, if we have to report this event for
	the reporting engine.
</iscomment>
<isinclude template="util/reporting/ReportUserRegistration.isml"/>

<isinclude template="components/footer/footer_UI"/>

<isscript>

var captured_email = "";

if (!empty(pdict.CurrentForms.singleshipping.shippingAddress.email.emailAddress.value)){
	captured_email = pdict.CurrentForms.singleshipping.shippingAddress.email.emailAddress.value;
} else if (!empty(session.custom.captured_email)){
	captured_email = session.custom.captured_email;
}

</isscript>

<iscomponent pipeline="Tealium-FooterInclude"
pid=${pdict.CurrentHttpParameterMap.pid.stringValue}
pagename=${(pdict.CurrentPageMetaData.title != null && !empty(pdict.CurrentPageMetaData.title) ) ? pdict.CurrentPageMetaData.title+'' : null}
searchterm=${pdict.CurrentHttpParameterMap.q.stringValue}
searchresultscount=${!empty(pdict.ProductSearchResult) ? pdict.ProductSearchResult.count+'' : null}
orderno=${ (pdict.Order != null && !empty(pdict.Order.orderNo) ) ? pdict.Order.orderNo+'' : null} 
contentsearchresultscount=${!empty(pdict.ContentSearchResult) ? pdict.ContentSearchResult.count+'' : null}
pagecgid=${pdict.CurrentHttpParameterMap.cgid.stringValue}
isconversion=${('PageIsOrderConfirmation' in pdict && pdict.PageIsOrderConfirmation===true) ? 'true' : 'false'}
pagecontext=${empty(pageContext) ? '' : pageContext.type}
category_clean=${empty(pdict.ProductSearchResult) || empty(pdict.ProductSearchResult.category) ? '' : pdict.ProductSearchResult.category.displayName}
captured_email=${captured_email}  />

<iscomment>An image for Adroit to use for piggy-backing tracking</iscomment>
<isscript>importScript('app_sleepys:util/URLUtilsHelper.ds');</isscript>
<script type="text/javascript" src="${URLUtils.staticURL('/js/mattresscom.js')}"></script>
<img src="${URLUtils.staticURL('/images/akamai_pixel.jpg')}" alt="" style="position:absolute;left:-9999px">
<!--- E-mail Subscription --->
<isset name="currentCustomerEmail" value="${pdict.CurrentCustomer.profile != null ? pdict.CurrentCustomer.profile.email : ''}" scope="page" />
<isset name="paramName" value="${dw.system.Site.current.preferences.custom.modalEmailSrcParamName}" scope="page" />
<isset name="paramValue" value="${dw.system.Site.current.preferences.custom.modalEmailSrcParamValue}" scope="page" />
<isset name="callFromEmail" value="${!empty(pdict.CurrentHttpParameterMap[paramName].stringValue) && pdict.CurrentHttpParameterMap[paramName].stringValue == paramValue}" scope="page" />
<isinclude url="${URLUtils.url('EmailSubscription-Include', 'email', currentCustomerEmail, 'callFromEmail', callFromEmail)}" />
