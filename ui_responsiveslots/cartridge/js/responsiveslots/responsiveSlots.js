/* global console */
/*
 * @fileoverview Logic for the initializing responsive content slots behavior. This code
 * relies on the jQuery and Demandware app JS libraries to be loaded.
 *
 * @module responsiveslots/responsiveSlots
 */
'use strict';

var ajax = require('../ajax'),
    util = require('../util'),
	globalInit = require('./globalInit');

/**
 * Contains the functionality for enabling responsive slots on a given page
 * @namespace ResponsiveSlots
 */
var ResponsiveSlots = {

    /**
     * Mobile viewport max-width in pixels.
     * @type {Number}
     * @default
     */
    mobileMaxWidth: 767,

    /**
     * Tablet viewport max-width in pixels.
     * @type {Number}
     * @default
     */
    tabletMaxWidth: 1024,

    /**
     * Defines which namespaces contain slot content initializations. This
     * likely need to be edited for a given project to support JavaScript
     * behaviors specific to content within the responsive slots.
     * @namespace slotContentNamespaces
     * @memberof ResponsiveSlots
     */
    slotContentNamespaces: {
        /**
         * JavaScript behavior for the 'storefront' namespace. This is used
         * on the home page.
         * @type {Object}
         */
        storefront: require('./namespaces/storefront'),
        search: require('./namespaces/search'),
        cart: require('./namespaces/cart')
    },

    /**
     * Initializes the responsive slots for the current namespace/page and
     * sets up the resize event to detect when an update needs to occur.
     *
     * @param namespace {String} The namespace of the current page.
     */
    init: function (namespace) {
        var self = this;

        /*
         * Extend this with required properties generated server-side.
         * (see 'responsiveslots/util/resources.isml')
         */
        if (window.ResponsiveSlotsResources) {
            for (var i in window.ResponsiveSlotsResources) {
                if (window.ResponsiveSlotsResources.hasOwnProperty(i)) {
                    this[i] = window.ResponsiveSlotsResources[i];
                }
            }
        }

        this.namespace = namespace;
        this.checkViewport();
        this.smartResize(function () {
            self.checkViewport(true);
        });

    },

    /**
     * Ensure viewport passed from the server-side matches what we see on
     * client side. If not, call function to set proper viewport in session. If
     * so, call function to initialize any viewport specific content behavior.
     *
     * @param isReload {Boolean} Whether this function is triggered via an AJAX
     * reload.
     */
    checkViewport: function (isReload) {
        var calculatedViewport = 'mobile'; //default
        if (window.innerWidth > this.tabletMaxWidth) {
            calculatedViewport = 'desktop';
        } else if (window.innerWidth > this.mobileMaxWidth) {
            calculatedViewport = 'tablet';
        }

        if (calculatedViewport !== this.viewport) {
            this.setViewport(calculatedViewport);
        } else if (!isReload) {
            this.initSlotContent(this.viewport);
        }
    },

    /**
     * Set the viewport to a new viewport on the client and session, then call
     * the function which reloads the slots.
     *
     * @param viewport {String} The viewport to set.
     */
    setViewport: function (viewport) {
        var self = this,
            url = util.appendParamsToUrl(this.setViewportURL, {viewport: viewport});

        ajax.getJson({
            url: url,
            callback: function (data) {
                if (data && data.viewport) {
                    self.viewport = data.viewport;
                    self.reloadSlots();
                }
            }
        });
    },

    /**
    * Reload all responsive content slots on page via AJAX, then call content
    * behavior initialization function.
    */
    reloadSlots: function () {
		var self = this,
            $slots = $('[id].responsive-slot');

        if ($slots.length) {
            var deferreds = [];

            $slots.each(function () {
                var $slot = $(this),
                    slotID = $slot.attr('id'),
                    context = $slot.attr('context') || '',
                    contextID = $slot.attr('contextid') || '',
                    url = util.appendParamsToUrl(self.showURL, {slotid: slotID, context: context, contextid: contextID});

                var xhr = $.ajax({
                    dataType: 'html',
                    cache: false,
                    url: url
                })
                .done(function (response) {
                    // Success
                    $slot.html(response);
                    return true;
                })
                .fail(function (xhr, textStatus) {
                    // Failed
                    if (textStatus === 'parsererror') {
                        console.error(Resources.BAD_RESPONSE);
                    } else {
                        console.error(Resources.SERVER_CONNECTION_ERROR);
                    }
                    return false;
                });

                deferreds.push(xhr);
            });

            $.when.apply($, deferreds).done(function () {
                self.initSlotContent();
            });
        }
    },

    /**
    * Initialize the responsive slot content for the given name space and
    * viewport. This is used for code that will execute which is specific
    * to the content loaded in for the given viewport.
    */
    initSlotContent: function () {
        if (this.slotContentNamespaces[this.namespace] && typeof this.slotContentNamespaces[this.namespace].init === 'function') {
            this.slotContentNamespaces[this.namespace].init(this.viewport);
        }
		globalInit.init();
    },

    /**
     * Execute callback function when the user has stopped resizing the screen.
     * @param callback {Function} The callback function to execute.
     */
    smartResize: function (callback) {
        var timeout;

        window.addEventListener ('resize', function () {
            clearTimeout(timeout);
            timeout = setTimeout(callback, 100);
        });

        return callback;
    }

};

module.exports = ResponsiveSlots;
