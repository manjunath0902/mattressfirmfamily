'use strict';

var content = require('../content');

var globalInit = {
	init: function () {
		content.initLiveContent();
	}
};

module.exports = globalInit;