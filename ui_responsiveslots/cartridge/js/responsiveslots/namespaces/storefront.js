'use strict';

var util = require('./../../util');

/**
 * Logic for 'storefront' namespace responsive content slots content.
 * @namespace storefront
 */
module.exports = {
	/**
	 * Initializes the functionality within the responsive slots for the
	 * 'storefront' namespace. This is used on the home page.
	 *
	 * @param viewport {String} The viewport size of the current window,
	 * 'mobile', 'tablet' or 'desktop'.
	 */
	init: function (viewport) {
		this.initHomeHero();
		this.initRecommendations();

		if (viewport === 'mobile') {
			// Initialization for mobile content
		} else if (viewport === 'tablet') {
			// Initialization for tablet content
		} else {
			// Initialization for desktop content
		}
	},

	initHomeHero: function () {
		var $homeHeroWrapper = $(".home-hero-wrapper");
		if (!$homeHeroWrapper.hasClass('slick-carousel')) {
			$homeHeroWrapper.addClass('slick-carousel');
		}
		var container = $('#home-hero ul');
		container.slick({
			dots: true
		});
	},

	initRecommendations: function () {
		var container = $('.product-recommendations ul');
		container.slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			infinite: false,
			responsive: [
				{
					breakpoint: util.tabletWidth,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: util.mobileWidth,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						variableWidth: false
					}
				}
			]
		});
	}
};