'use strict';

var util = require('./../../util');

/**
 * Logic for 'search' namespace responsive content slots content.
 * @namespace search
 */
module.exports = {
	/**
	 * Initializes the functionality within the responsive slots for the
	 * 'search' namespace. This is used on the home page.
	 *
	 * @param viewport {String} The viewport size of the current window,
	 * 'mobile', 'tablet' or 'desktop'.
	 */
	init: function (viewport) {
		this.initRecommendations();
		
		if (viewport === 'mobile') {
			// Initialization for mobile content
		} else if (viewport === 'tablet') {
			// Initialization for tablet content
		} else {
			// Initialization for desktop content
		}
	},


	initRecommendations: function () {
		var container = $('.product-recommendations ul');
		container.slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			infinite: false,
			variableWidth: false,
			responsive: [
				{
					breakpoint: util.tabletWidth,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: util.mobileWidth,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		});
	}
};