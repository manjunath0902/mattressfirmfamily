'use strict';
/**
 * View to render the responsive content slots.
 * @module views/ResponsiveSlotView
 */
var Class = require('app_storefront_controllers/cartridge/scripts/util/Class').Class;

/**
 * Helper function for rendering responsive content slots.
 * @class module:views/ResponsiveSlotView~ResponsiveSlotView
 * @extends module:util/Class~Class
 * @lends module:views/ResponsiveSlotView~ResponsiveSlotView.prototype
*/
var ResponsiveSlotView = Class.extend({

    /**
     * Whether or not this view is being used from an ISML module.
     * @type {Boolean}
     * @default
     */
    isISMLModule: false,

    /**
     * The source of the slot configuration data.
     * @type {HttpParameterMap}
     * @default request.httpParameterMap
     */
    dataSource: request.httpParameterMap,

    /**
     * The path to the template used to render this view.
     * @type {string}
     * @default
     */
    template: 'responsiveslots/slots/global',

    /**
     * The parameters used to render this view, such as the Folder or
     * Category used as a context-object in the content slot.
     * @type {object}
     * @default
     */
    viewParameters: {},

    /**
     * Prepare parameters and template needed for rendering the content slot.
     */
    prepareView: function () {
        if (this.dataSource.slotid) {
            this.viewParameters.SlotID = this.isISMLModule ? this.dataSource.slotid : this.dataSource.slotid.stringValue;
            this.viewParameters.context = this.isISMLModule ? this.dataSource.context : this.dataSource.context.stringValue,
            this.viewParameters.contextID = this.isISMLModule ? this.dataSource.contextid : this.dataSource.contextid.stringValue;

            if (this.viewParameters.context && this.viewParameters.contextID) {
                if (this.viewParameters.context === "folder") {
                    this.setFolder(this.viewParameters.contextID);
                } else if (this.viewParameters.context === "category") {
                    this.setCategory(this.viewParameters.contextID);
                }
            }
        } else {
            this.template = 'responsiveslots/util/empty';
        }
    },

    /**
     * Set reference to the folder which is the context object for the content slot.
     * Also sets the template to use to retrieve the configured slot.
     * @param {string} id The ID for the content library folder.
     */
    setFolder: function (id) {
        var folder = require('dw/content/ContentMgr').getFolder(id);

        if (folder) {
            this.viewParameters.Folder = folder;
            this.template = 'responsiveslots/slots/folder';
        } else {
            this.template = 'responsiveslots/util/empty';
        }
    },

    /**
     * Set reference to the category which is the context object for the content slot.
     * Also sets the template to use to retrieve the configured slot.
     * @param {string} id The ID for the category.
     */
    setCategory: function (id) {
        var category = require('dw/catalog/CatalogMgr').getCategory(id);

        if (category) {
            this.viewParameters.Category = category;
            this.template = 'responsiveslots/slots/category';
        } else {
            this.template = 'responsiveslots/util/empty';
        }
    },

    /**
     * Constructs view for responsive content slots functionality.
     * @constructs module:views/ResponsiveSlotView~ResponsiveSlotView
     * @param {Object} moduleParameters The parameters to pass to the template.
     */
    init: function (moduleParameters) {
        if (!session.custom.viewport) {
            session.custom.viewport = session.custom.device;
        }
        if (moduleParameters.slotid) {
            this.dataSource = moduleParameters;
            this.isISMLModule = true;
        }
        this.prepareView();
        return this;
    },

    /**
     * Renders the view for responsive content slots functionality.
     */
    render: function () {
        require('dw/template/ISML').renderTemplate(this.template, this.viewParameters);
        return this;
    },

    /**
     * Returns the parameters for responsive content slots module.
     */
    getModuleParameters: function () {
        this.viewParameters.template = this.template;
        if (this.viewParameters.context && this.viewParameters.contextID) {
            this.viewParameters.HTMLAttribute = 'context=' + this.viewParameters.context + ' contextid=' + this.viewParameters.contextID;
        } else {
            this.viewParameters.HTMLAttribute = '';
        }
        return this.viewParameters;
    }

});

module.exports = ResponsiveSlotView;
