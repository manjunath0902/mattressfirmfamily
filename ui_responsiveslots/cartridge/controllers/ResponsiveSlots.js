'use strict';

/**
 * This controller handles setting/getting the saved viewport in the session.
 * It also renders the content slots as configured for the current viewport.
 * This controller is intended to be used in conjunction with dynamic customer
 * groups to render slot configurations specific to the customer's viewport.
 *
 * @module controllers/ResponsiveSlots
 */

var guard = require('app_storefront_controllers/cartridge/scripts/guard');

/**
 * Sets the current viewport in a session variable.
 * Returns the whether the request is successful via JSON response.
 */
function setViewport() {
    session.custom.viewport = request.httpParameterMap.viewport.stringValue || session.custom.device;
    require('app_storefront_controllers/cartridge/scripts/util/Response').renderJSON({
        viewport: session.custom.viewport
    });
}

/**
 * Renders the view for the content slot.
 */
function show(pdict) {
    var ResponsiveSlot = require('~/cartridge/scripts/views/ResponsiveSlotView'),
        slot = new ResponsiveSlot(pdict);

    slot.render();
}

/*
 * Web exposed methods
 */
/** Sets the current viewport in a session variable. Returns the whether the request is successful via JSON response.
 * @see module:controllers/ResponsiveSlots~setViewport */
exports.SetViewport = guard.ensure(['get'], setViewport);
/** Renders the responsive content slot.
 * @see module:controllers/ResponsiveSlots~show */
exports.Show = guard.ensure(['get'], show);
