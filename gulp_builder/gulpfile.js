// Include the different modules to be used throughout the program
var nconf				= require('nconf'),
    gulp				= require('gulp'),
    gutil				= require('gulp-util'),
    sass				= require('gulp-sass'),
    sassLint			= require('gulp-sass-lint'),
    sourcemaps			= require('gulp-sourcemaps'),
    prefix				= require('gulp-autoprefixer'),
    rename				= require('gulp-rename'),
    minifyCSS			= require('gulp-minify-css'),
    bless				= require('gulp-bless'),
    browserify			= require('browserify'),
    source				= require('vinyl-source-stream'),
    xtend				= require('xtend'),
    jscs				= require('gulp-jscs'),
    jshint				= require('gulp-jshint'),
    stylish				= require('jshint-stylish'),
    argv				= require('yargs').argv,
    uglify				= require('gulp-uglify'),
    del					= require('del'),
    vinylPaths			= require('vinyl-paths'),
    Q					= require('q'),
    fileSystem			= require('fs'),
    svgSprite			= require('gulp-svg-sprite'),
    zip					= require('gulp-zip'),
    request				= require('request'),
    url					= require('url'),
    dwTools				= require('./deploy/dw-tools'),
    _					= require('lodash'),
    workingPath			= argv.basedir || '.',
    allStyleDirectories = [],
    allJavascriptFiles	= [],
    allStaticLocales	= [],
    allSVGIconProjects	= [],
    enableSassLinting	= false; //set to true to enable linting (does not like SG sass)

// Load a file store onto nconf with the specified settings
nconf.use('file', { file: workingPath + '/config.json' });

// Retrieve the site information from the configuration file
var sites = nconf.get('sites');

// Create a new instance of the DemandwareTools object for use throughout the application
var dwToolOptions = _.assign({}, nconf.get('deployment'), argv);
var tools = new dwTools(dwToolOptions);

/**
 * Builds a set of temporary working directories for the
 * build process based on the given paths and directory name
 *
 * @param options
 * @returns {Promise}
 */
function buildTempDir (options) {
    // Create an empty options object if one wasn't provided
    var paths = options.paths,
        dirName = options.dirName,
        srcOptions = options.srcOptions || {},
        destOptions = options.destOptions || {};

    // Define the source project based on the given projects
    var deferred = Q.defer(),
        stream = gulp.src(paths, srcOptions);

    // Rename the files/folders if a function is provided
    if (typeof options.rename === 'function') {
        stream = stream.pipe(rename(options.rename));
    }

    stream = stream.pipe(gulp.dest(dirName, destOptions));

    // When the stream has finished processing set the promise to resolved
    stream.on('end', function () {
        deferred.resolve();
    });

    return deferred.promise;
}

/**
 * Create a new file with the given filename and content
 *
 * @param filename {String}
 * @param text {String}
 * @returns {Promise}
 */
function createFile (filename, text) {
    var deferred = Q.defer();

    fileSystem.writeFile(filename, text, function () {
        deferred.resolve();
    });

    return deferred.promise;
}

/**
 * Remove a new file with the given filename
 *
 * @param filename {String}
 * @returns {Promise}
 */
function removeFile (filename) {
    var deferred = Q.defer();

    fileSystem.unlink(filename, function () {
        deferred.resolve();
    });

    return deferred.promise;
}

/**
 * Updates all of the references to cartridges in the given file to the suffixed version
 *
 * @param path {String}
 */
function updateCartridgeReferences (path) {
    fileSystem.readFile(path, 'utf8', function (err,data) {
        if (err) { return console.error(err); }

        var result = data;

        for ( var i = 0; i < sites.length; i++ ) {
            // Retrieve the project names from the cartridge paths
            for ( var j = 0; j < sites[i].cartridges.length; j++ ) {
                // Replace the name of the catridge in each file with the suffixed name
                var parts = sites[i].cartridges[j].split('/'),
                    regex = new RegExp(parts[1] + '(:|/)', 'g');

                result = result.replace(regex, parts[1] + '_' + argv.cartridgeSuffix + '$1');
            }
        }

        fileSystem.writeFile(path, result, 'utf8', function (err) {
            if (err) { return console.error(err); }
        });
    });
}

//lint the sass files to ensure proper formatting/syntax
gulp.task('sass-lint', function () {
    if (!enableSassLinting) {
        console.log('Sass linting disabled (enableSassLinting variable in gulpfile.js set to false)');
        return;
    }
    console.log('Sass linting enabled (enableSassLinting variable in gulpfile.js set to true)');
    var sassFiles = workingPath + '/../**/cartridge/scss/**/*.s+(a|c)ss';
    // Lint the Sass files
    gulp.src(sassFiles)
        .pipe(sassLint())
        .pipe(sassLint.format())
        .pipe(sassLint.failOnError());
});

//Retrieve the paths for all scss/css path pairs in the workspace
gulp.task('get-style-directories', ['sass-lint'], function () {
    var vp = vinylPaths(),
    paths = [];

    for (var i = 0; i < sites.length; i++) {
        for ( var j = 0; j < sites[i].cartridges.length; j++ ) {
            var path = sites[i].cartridges[j] + '/cartridge/scss/*';

            if ( sites[i].cartridges[j].split('/')[0] === '.') {
                path = '../' + path;
            } else {
                path = '../../' + path;
            }
            paths.push(path);
        }
    }

    //add exclusion for all files, so we only have directories
    paths.push('!../**/*.*');

    // Build the array of scss/css path pairs
    return gulp.src(paths, {read: false})
        .pipe(vp)
        .on('end', function () {
            for ( var i = 0; i < vp.paths.length; i++ ) {
                var scssPath = vp.paths[i],
                    localeParts = vp.paths[i].split(/\/|\\/),
                    locale = localeParts[localeParts.length-1],
                    cssPath = vp.paths[i].replace('scss','static') + '/css',
                    pathSet = { scssPath: scssPath, cssPath: cssPath };
                allStyleDirectories.push(pathSet);
            }
        });
});

//Compile from sass, add vendor prefixes and write sourcemap
gulp.task('sass', ['get-style-directories'], function () {
    // Stores the list of promises returned from building Sass files
    var promises = [];
    // Compile the style main style sheet from the source Sass
    var buildSass = function (options) {
        var deferred = Q.defer(),
            stream = gulp.src(options.scssPath + '/[^_]*.s+(a|c)ss')
                .pipe(sourcemaps.init())
                .pipe(sass())
                .pipe(prefix('last 5 versions'))
                .pipe(sourcemaps.write('./'))
                .pipe(gulp.dest(options.cssPath));
        // When the stream has finished processing set the promise to resolved
        stream.on('end', function () {
            deferred.resolve();
        });

        return deferred.promise;
    };

    for ( var i = 0; i < allStyleDirectories.length; i++ ) {
        promises.push(buildSass(allStyleDirectories[i]));
    }

    return Q.all(promises);
});

//Create minified CSS version
gulp.task('minify', ['sass'], function () {
    // Stores the list of promises returned from building each projects minified style files
    var promises = [];
    // Compile the style main style sheet from the source Sass
    var minifyStyles = function (localePath) {
        var deferred = Q.defer(),
            stream = gulp.src([localePath + '/*.css', '!' + localePath + '/*.min.*'])
                    .pipe(minifyCSS({processImport: false}))
                    .pipe(rename({suffix: '.min'}))
                    .pipe(gulp.dest(localePath));
        // When the stream has finished processing set the promise to resolved
        stream.on('end', function () {
            deferred.resolve();
        });

        return deferred.promise;
    };

    for ( var i = 0; i < allStyleDirectories.length; i++ ) {
        promises.push(minifyStyles(allStyleDirectories[i].cssPath));
    }

    return Q.all(promises);
});

//Create minified CSS and generate split IE version for 4095 selector limit
gulp.task('bless', ['minify'], function () {
    // Stores the list of promises returned from building each projects IE bless files
    var promises = [];
    // Compile the style main style sheet from the source Sass
    var blessStyles = function (localePath) {
        var deferred = Q.defer(),
            stream = gulp.src([localePath + '/*.min.css'])
            	.pipe(bless())
        		.pipe(gulp.dest(localePath.replace('/css', '/ie-css')));
        // When the stream has finished processing set the promise to resolved
        stream.on('end', function () {
            deferred.resolve();
        });

        return deferred.promise;
    };

    for ( var i = 0; i < allStyleDirectories.length; i++ ) {
        promises.push(blessStyles(allStyleDirectories[i].cssPath));
    }

    return Q.all(promises);
});

// Retrieve the paths for the javascript files based on the config
gulp.task('get-javascript-paths', function () {
    for ( var i = 0; i < sites.length; i++ ) {
        for ( var j = sites[i].cartridges.length - 1; j >= 0; j-- ) {
            var path = sites[i].cartridges[j] + '/cartridge/js/**/*.js';

            // Make sure we set up the pathing to work correctly for SVN and Git projects respectively
            if ( sites[i].cartridges[j].split('/')[0] === '.') {
                path = '../' + path;
            } else {
                path = '../../' + path;
            }

            allJavascriptFiles.push(path);
        }
    }
});

//Retrieve the paths for all projects in the workspace that contain SVG sprite files
gulp.task('get-svg-sprite-projects', function () {
    var vp = vinylPaths(),
        paths = [];

    for (var i = 0; i < sites.length; i++) {
        for ( var j = 0; j < sites[i].cartridges.length; j++ ) {
            var path = sites[i].cartridges[j] + '/cartridge/static/default/images/svg-icons';

            if ( sites[i].cartridges[j].split('/')[0] === '.') {
                path = '../' + path;
            } else {
                path = '../../' + path;
            }

            paths.push(path);
        }
    }

    // Build the set of projects that contain Sass files
    return gulp.src(paths, {read: false})
    .pipe(vp)
    .on('end', function () {
        for ( var i = 0; i < vp.paths.length; i++ ) {
            var parts = vp.paths[i].split(/\/|\\/);
            allSVGIconProjects.push(parts[parts.length - 6]);
        }
    });
});

//Combine the SVG files in the given path into one file
gulp.task('combine-svg', ['get-svg-sprite-projects'], function () {
    // Stores the list of promises returned from building each projects SVG sprite images
    var promises = [];
    // Compile the style icon SVG from the individual files
    var buildSVG = function (proj) {
        var deferred = Q.defer(),
            projectPath = '../' + proj + '/cartridge',
            stream = gulp.src(projectPath + '/static/default/images/svg-icons/*.svg')
                        .pipe(svgSprite({
                            mode: {
                                symbol: { // Activate the «view» mode
                                    inline: true,
                                    dest: './',
                                    sprite: '../images/icons.svg',
                                    bust: false,
                                    render: {
                                        scss: {
                                            dest: '../../../scss/compiled/_svg-icons.scss'
                                        } // Activate Sass output
                                    }
                                },
                                defs: {
                                    inline: false,
                                    dest: './',
                                    sprite: '../../../templates/default/components/icons-symbols.isml'
                                }
                            }
                        }))
                        .pipe(gulp.dest(projectPath + '/static/default/images'));
        // When the stream has finished processing set the promise to resolved
        stream.on('end', function () {
            deferred.resolve();
        });

        return deferred.promise;
    };

    for ( var i = 0; i < allSVGIconProjects.length; i++ ) {
        promises.push(buildSVG(allSVGIconProjects[i]));
    }

    return Q.all(promises);
});

// Store Locater Minify
gulp.task('minify-store-locator', () => {
  return gulp.src('../app_storefront_core/cartridge/static/default/lib/storelocator/storelocator.js', { allowEmpty: true }) 
    .pipe(uglify({noSource: true}))
	.pipe(rename({extname:'.min.js'}))
    .pipe(gulp.dest('../app_storefront_core/cartridge/static/default/lib/storelocator'))
});

//minify amazonPayments js file
/* gulp.task('minify-amazon-payments', () => {
    return gulp.src('../int_amazonpayments/cartridge/static/default/js/amazonPayments.js', { allowEmpty: true })
       .pipe(uglify({ noSource: true }))
        .pipe(rename({ extname: '.min.js' }))
       .pipe(gulp.dest('../int_amazonpayments/cartridge/static/default/js'))
	}); */

// Create the temp directories for each projects javascript
gulp.task('copy-core', function () {
    var builds = [];

    for ( var i = 0; i < sites.length; i++ ) {
        var paths = [];

        for ( var j = sites[i].cartridges.length - 1; j >= 0; j-- ) {
            var path = sites[i].cartridges[j] + '/cartridge/js/**/*.js';

            // Make sure we set up the pathing to work correctly for SVN and Git projects respectively
            if ( sites[i].cartridges[j].split('/')[0] === '.') {
                path = '../' + path;
            } else {
                path = '../../' + path;
            }

            paths.push(path);
        }

        builds.push( buildTempDir({
            paths: paths,
            dirName: workingPath + '/temp-' + sites[i].name
        }));
    }

    return Q.all(builds);
});

// Remove the temporary directories from the working path
gulp.task('delete', ['javascript'], function() {
    return gulp.src(workingPath + '/temp-*')
        .pipe(vinylPaths(del));
});

// Create the combined Javascript file from the different modules
gulp.task('javascript', ['copy-core'], function () {
    // List of Browserify bundles used to compile each project's Javascript
    var bundles = [];
    // Use the given bundler stream to bundle all of the located files together using Browserify
    var rebundle = function (bundler, proj) {
        var deferred = Q.defer(),
            stream = bundler.bundle()
                    .on('error', function (e) {
                        deferred.reject(e);
                    })
                    .pipe(source('app.js'))
                    .pipe(gulp.dest('../' + proj + '/cartridge/static/default/js'));
        // When the stream has finished processing set the promise to resolved
        stream.on('end', function () {
            deferred.resolve();
        });

        return deferred.promise;
    };

    for ( var i = 0; i < sites.length; i++ ) {
        var opts = {
            entries: workingPath + '/temp-'+ sites[i].name +'/app.js',
            debug: true
        },
        bundler = browserify(opts);
        bundles.push(rebundle(bundler, sites[i].publicJavascript));
    }

    return Q.all(bundles);
});

// Make sure that the Javascript follows the given formatting guidelines (contained in .jscsrc)
gulp.task('jscs', ['get-javascript-paths'], function () {
    // Run jscs on all files included but not excluded by the given paths
    return gulp.src(allJavascriptFiles)
        .pipe(jscs());
});

// Make sure that the Javascript has been correctly coded
gulp.task('jshint', ['get-javascript-paths'], function () {
    // Run jshint on all files included but not excluded by the given paths
    return gulp.src(allJavascriptFiles)
        .pipe(jshint({
            "camelcase": true,
            "curly": true,
            "eqeqeq": true,
            "quotmark": "single",
            "undef": true,
            "unused": true,
            "browser": true,
            "strict": true,
            "jquery": true,
            "browserify": true,
            "globals": {
                "SitePreferences": true,
                "IntegrationHooks": true,
                "Urls": true,
                "Resources": true,
                "User": true
            }
        }))
        .pipe(jshint.reporter(stylish));
});

// Minify the javascript in every Rich UI project
gulp.task('uglify', ['javascript'], function() {
    var uglifiedFiles = [];

    var uglifyFiles = function (paths) {
        var deferred = Q.defer(),
            stream = gulp.src(paths.src)
                        .pipe(uglify().on('error', function(e){
                            console.log(e);
                        }))
                        .pipe(rename({extname:'.min.js'}))
                        .pipe(gulp.dest(paths.dest));
        // When the stream has finished processing set the promise to resolved
        stream.on('end', function () {
            deferred.resolve();
        });

        return deferred.promise;
    };

    for ( var i = 0; i < sites.length; i++ ) {
        var jsPath = '../' + sites[i].publicJavascript + '/cartridge/static/default/js';

        uglifiedFiles.push(uglifyFiles({
            src: [ jsPath + '/**/*.js', '!' + jsPath + '/**/*.min.js' ], // Files to include in, or exclude from, the source
            dest: jsPath // Folder to output the minfied files to
        }));
    }

    return Q.all(uglifiedFiles);
});


// Specialty method just for MattressFirm. This allows the support SVN build script to continue to do the final deploy
gulp.task('copy-master-arch-cartridges', ['js-deploy', 'styles-deploy'], function () {
    // Create a working directory with all of the cartridges
    var builds = [];

    for ( var i = 0; i < sites.length; i++ ) {

        // Retrieve the project names from the cartridge paths
        for ( var j = 0; j < sites[i].cartridges.length; j++ ) {
            var parts = sites[i].cartridges[j].split('/');

            var path = sites[i].cartridges[j] + '/**/*.*',
                base = parts[0];

            // Make sure we set up the pathing to work correctly for SVN and Git projects respectively
            if (parts[0] === '.') {
                path = '../' + path;
                base = '../' + base;
            } else {
                path = '../../' + path;
                base = '../../' + base;
            }

            // Setup the temporary working directory options
            // This is really the only change so it copies to the support "cartridges" directory
            var tempDirOptions = {
                paths: path,
                dirName: '.',
                srcOptions: {base: base},
                destOptions: {cwd: '../../cartridges/'}
            };

            // Add a suffix to the cartridge if one is provided by the configuration
            if (typeof argv.cartridgeSuffix === 'string' && argv.cartridgeSuffix) {
                tempDirOptions.rename = function (path) {
                    path.dirname = path.dirname.replace(/^(\w+)\//g, '$1_' + argv.cartridgeSuffix + '/');
                };
            }

            builds.push(buildTempDir(tempDirOptions));
        }
    }

    return Q.all(builds);
});

// Copy the Demandware cartridges to the working folder for compression
gulp.task('copy-cartridges', ['js-deploy', 'styles-deploy'], function () {
    // Create a working directory with all of the cartridges
    var builds = [];

    for ( var i = 0; i < sites.length; i++ ) {

        // Retrieve the project names from the cartridge paths
        for ( var j = 0; j < sites[i].cartridges.length; j++ ) {
            var parts = sites[i].cartridges[j].split('/');

            var path = sites[i].cartridges[j] + '/**/*.*',
                base = parts[0];

            // Make sure we set up the pathing to work correctly for SVN and Git projects respectively
            if (parts[0] === '.') {
                path = '../' + path;
                base = '../' + base;
            } else {
                path = '../../' + path;
                base = '../../' + base;
            }

            // Setup the temporary working directory options
            var tempDirOptions = {
                paths: path,
                dirName: '.',
                srcOptions: {base: base},
                destOptions: {cwd: workingPath + '/deploy/working/' + tools.buildVersion}
            };

            // Add a suffix to the cartridge if one is provided by the configuration
            if (typeof argv.cartridgeSuffix === 'string' && argv.cartridgeSuffix) {
                tempDirOptions.rename = function (path) {
                    path.dirname = path.dirname.replace(/^(\w+)\//g, '$1_' + argv.cartridgeSuffix + '/');
                };
            }

            builds.push(buildTempDir(tempDirOptions));
        }
    }

    return Q.all(builds);
});

gulp.task('update-cartridge-properties', ['copy-cartridges'], function () {
    // Only update the properties files if they are in copies of the cartridges
    if (typeof argv.cartridgeSuffix === 'string' && argv.cartridgeSuffix) {
        var builds = [];

        for ( var i = 0; i < sites.length; i++ ) {
            // Retrieve the project names from the cartridge paths
            for ( var j = 0; j < sites[i].cartridges.length; j++ ) {
                var parts = sites[i].cartridges[j].split('/'),
                    cartridgeName = parts[1] + '_' + argv.cartridgeSuffix,
                    oldFilename = workingPath + '/deploy/working/' + cartridgeName + '/cartridge/' + parts[1] + '.properties',
                    newFilename = workingPath + '/deploy/working/' + cartridgeName + '/cartridge/' + cartridgeName + '.properties',
                    content = 'demandware.cartridges.' + cartridgeName + '.id=' + cartridgeName + '\ndemandware.cartridges.' + cartridgeName + '.multipleLanguageStorefront=true';

                builds.push(removeFile(oldFilename));
                builds.push(createFile(newFilename, content));
            }
        }

        return Q.all(builds);
    } else {
        console.log('No properties update necessary');
    }
});

gulp.task('update-cartridge-references', ['update-cartridge-properties'], function () {
    if (typeof argv.cartridgeSuffix === 'string' && argv.cartridgeSuffix) {
        var vp = vinylPaths();

        return gulp.src([workingPath + '/deploy/working/**/*.ds', workingPath + '/deploy/working/**/*.isml', workingPath + '/deploy/working/**/*.xml'])
            .pipe(vp)
            .on('end', function () {
                for ( var k = 0; k < vp.paths.length; k++ ) {
                    var path = vp.paths[k];

                    updateCartridgeReferences(path);
                }
            });
    } else {
        console.log('No catridge reference update necessary');
    }
});

gulp.task('zipFiles', ['copy-cartridges'], function () {
    return gulp.src('deploy/working/**/*')
            .pipe(zip(tools.buildVersion + '.zip'))
            .pipe(gulp.dest('deploy/output'))
            .on('end', function () {
                del([
                    'deploy/working/**/*',
                    'deploy/output/**/*'
                ]);
            });
});

gulp.task('uploadCode', ['zipFiles'], function () {
    var destinations = [];

    if (tools.twoFactor === 'true') {
        destinations.push('https://' + tools.twoFactorInstance + tools.instanceRoot + tools.cartridgePath + tools.buildVersion + '.zip');
    } else {
        for (var i in tools.instances) {
            destinations.push('https://' + tools.instances[i] + tools.instanceRoot + tools.cartridgePath + tools.buildVersion + '.zip');
        }
    }

    var uploads = [];

    for (var i in destinations) {
        uploads.push(tools.uploadFiles(destinations[i], true));
    }

    return Q.all(uploads);
});

gulp.task('unzipCode', ['uploadCode'], function () {
    // Retrieve the deployment information from the configuration file
    var deployment = nconf.get('deployment'),
        instanceRoot = deployment.instanceRoot;

    if (argv.activationInstances) {
        var servers = argv.activationInstances.split(',');
    } else if (deployment.activationInstances) {
        var servers = deployment.activationInstances.split(',');
    } else {
        console.error('Please enter an activationInstance list in the configuration.');
    }

    var unzipInstances = [];

    for (var i in servers) {
        unzipInstances.push(tools.unzipCode(servers[i]));
    }

    return Q.all(unzipInstances);
});

gulp.task('activateCode', ['unzipCode'], function () {
    // Retrieve the deployment information from the configuration file
    var deployment = nconf.get('deployment'),
        instanceRoot = deployment.instanceRoot;

    if (argv.activationInstances) {
        var servers = argv.activationInstances.split(',');
    } else if (deployment.activationInstances) {
        var servers = deployment.activationInstances.split(',');
    } else {
        console.error('Please enter an activationInstance list in the configuration.');
    }

    var instanceActivations = [];

    for (var i in servers) {
        instanceActivations.push(tools.activateCode(servers[i]));
    }

    return Q.all(instanceActivations);
});



gulp.task('sass-fulfillment', function (){
    return gulp.src('../app_fulfillment_core/cartridge/scss/default/*.scss')
      .pipe(sass())
      .pipe(gulp.dest('../app_fulfillment_core/cartridge/static/default/css'))
});

// Run the style tasks, for local development. Add 'bless' tasks to support IE development where selectors exceed 4096. (On command line: gulp styles)
gulp.task('styles', ['sass-lint', 'svg', 'get-style-directories', 'sass', 'minify', 'bless', 'sass-fulfillment']);

//Run the style tasks, including minify and bless (On command line: gulp stylesMinAndBless)
gulp.task('styles-deploy', ['sass-lint', 'svg', 'get-style-directories', 'sass', 'minify', 'bless', 'sass-fulfillment']);

// Run the Javascript tasks (On command line: gulp js)
gulp.task('js', ['get-javascript-paths', 'copy-core', 'javascript', 'delete']);

//Run the Javascript tasks (On command line: gulp js)
gulp.task('js-deploy', ['get-javascript-paths', 'copy-core', 'javascript', 'delete', 'uglify']);

//Run the SVG tasks (On command line: gulp svg)
gulp.task('svg', ['get-svg-sprite-projects', 'combine-svg']);

// Run the deployment tasks (On command line: gulp deploy)
gulp.task('deploy', ['styles-deploy', 'js-deploy', 'copy-master-arch-cartridges']); // Commenting out for unified build script, 'copy-cartridges', 'update-cartridge-properties', 'update-cartridge-references', 'activateCode']);
//Quickly compile js and sass with one command, gulp go
gulp.task('go', ['js', 'sass']);