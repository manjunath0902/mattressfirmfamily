'use strict';

var fileSystem	= require('fs'),
	path		= require('path'),
	Q			= require('q'),
	request		= require('./bm_request'),
	url			= require('url'),
	http		= require('https');

var MAX_RETRY = 1;

/**
 * Contains the Demandware server functionality to facilitate deployments
 *
 * @constructor
 */
function DemandwareTools (options) {
	this.basicAuth = options.basicAuth;
	this.overwriteRelease = options.overwriteRelease;
	this.user = options.user;
	this.password = options.password;
	this.instanceRoot = options.instanceRoot;
	this.instances = (typeof options.instances !== 'undefined' && options.instances.length > 0) ? options.instances.split(',') : [];
	this.activationInstances = (typeof options.activationInstances !== 'undefined' && options.activationInstances.length > 0) ? options.activationInstances.split(',') : [];
	this.cartridgePath = '/on/demandware.servlet/webdav/Sites/Cartridges/';
	this.twoFactor = options.twoFactor;
	this.twoFactorInstance = options.twoFactorInstance;
	this.twoFactorp12 = options.twoFactorp12;
	this.twoFactorPassword = options.twoFactorPassword;
	this.buildVersion = options.buildVersion ? options.buildVersion : process.env.BUILD_ID;
}

DemandwareTools.prototype.uploadFiles = function (dest, enc, suppress) {
	var initialMethod;

	// Turn off unauthorized rejections for TLS connections
	process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

	if (this.overwriteRelease === true) {
		grunt.log.warn('Release will be overwritten');
		initialMethod = 'DELETE';
	} else {
		initialMethod = 'PUT';
	}

	var self = this,
		deferred = Q.defer(),
		deconstructedDest = url.parse(dest),
		distFile = 'deploy/output/' + this.buildVersion + '.zip',
		data = fileSystem.readFileSync(distFile),
		httpOptions = {
			method : initialMethod,
			hostname : deconstructedDest.hostname,
			port : deconstructedDest.port || 443,
			path : deconstructedDest.path
		};

	if (this.basicAuth === true) {
		var user = this.user;
		var pass = this.password;

		if (typeof user !== 'string' || typeof pass !== 'string') {
			console.error('basic_auth specified, but not provided');
			return false;
		}

		httpOptions.auth = user + ':' + pass;
	}

	if (enc && this.twoFactor === 'true') {
		console.log("Two Factor Enabled Using File: "
				+ this.twoFactorp12);
		// This condition might be able to automatically format the hostname if
		// it is formatted incorrectly.
		if (httpOptions.hostname.indexOf('cert') !== 0) {
			throw new Error('Incorrect hostname ' + httpOptions.hostname
					+ ' used with two factor auth');
		}

		httpOptions.pfx = fileSystem.readFileSync(this.twoFactorp12);
		httpOptions.passphrase = this.twoFactorPassword;
		httpOptions.honorCipherOrder = true;
		httpOptions.rejectUnauthorized = false;
		httpOptions.securityOptions = 'SSL_OP_NO_SSLv3'
	}

	var req = http.request(httpOptions, function(res) {
		console.log('Status: ' + res.statusCode);
		if (httpOptions.method === 'DELETE') {
			if (res.statusCode === 204) {
				console.log('Remote file removed');
			} else if (res.statusCode === 401) {
				deferred.reject(new Error('Authentication Failed'));
				return;
			} else if (res.statusCode === 404) {
				deferred.reject(new Error('Remote file did not exist'));
				return;
			} else if (res.statusCode === 405) {
				deferred.reject(new Error('Remote server does not support webdav!'));
				return;
			} else {
				deferred.reject(new Error('Unknown error occurred!'));
				return;
			}

			this.overwrite = false;
			this.uploadFiles(dest, true);
		} else {
			if (res.statusCode === 201 || res.statusCode === 200) {
				console.log(dest);
				console.log('Successfully deployed');
			} else if (res.statusCode === 204) {
				deferred.reject(new Error('Remote file exists!'));
				return;
			} else if (res.statusCode === 401) {
				deferred.reject(new Error('Authentication failed'));
				return;
			} else if (res.statusCode === 405) {
				deferred.reject(new Error('Remote server does not support webdav!'));
				return;
			} else {
				deferred.reject(new Error('Unknown error occurred!'));
				return;
			}
			deferred.resolve();
		}
	});

	req.on('data', function(chunk) {
		console.log(chunk);
	});

	req.on('error', function(e) {
		console.error('problem with request: ' + e.message);
		console.error(e.stack);

		// We need to be able to retry here, because the second request, (after
		// DELETE) will say the key is already in the hash table... which is
		// fine, we just need to trigger a new request.
		//
		// However... in case of real errors, we only want to retry a max of five
		// times to avoid an infinite loop.
		if (MAX_RETRY--) {
			self.uploadFiles(dest, true, true);
		}
	});

	if (httpOptions.method === 'DELETE') {
		console.log('Removing existing zip');
		req.end();
	} else {
		if (!suppress) {
			console.log('Deploying zip to ' + dest);
		}
		req.end(data, 'binary');
	}

	return deferred.promise;
};

DemandwareTools.prototype.validateTwoFactorHostName = function(httpOptions) {
	// This condition might be able to automatically format the hostname if
	// it is formatted incorrectly.
	if (httpOptions.hostname.indexOf('cert') !== 0) {
		throw new Error('Incorrect hostname ' + httpOptions.hostname + ' used with two factor auth');
	}
};

DemandwareTools.prototype.setTwoFactorHttpOptions = function (httpOptions) {
	if (this.twoFactor === 'true') {
		console.log("Two Factor Enabled Using File: " + this.twoFactorp12);

		httpOptions.pfx = fileSystem.readFileSync(this.twoFactorp12);
		httpOptions.passphrase = this.twoFactorPassword;
		httpOptions.honorCipherOrder = true;
		httpOptions.rejectUnauthorized = false;
		httpOptions.securityOptions = 'SSL_OP_NO_SSLv3';
	}
};


DemandwareTools.prototype.unzipCode = function (server) {
	var deferred = Q.defer(),
		url = 'https://' + server + this.instanceRoot + this.cartridgePath + this.buildVersion + '.zip',
		self = this,
		unzipRequest = request({
			url: url,
			method: 'POST',
			auth: {
				user: this.user,
				pass: this.password
			},
			form: {
				method: 'UNZIP'
			},
			jar: true,
			rejectUnauthorized: false
		});

	unzipRequest.on('response', function (response) {
		if (response.statusCode === 201) {
			console.log('Code unzipped on '+ server);
		} else if (response.statusCode === 404) {
			deferred.reject(new Error('Given file', this.buildVersion + '.zip', 'not found'));
			return;
		} else {
			deferred.reject(new Error('Unknown Error: ' + response.statusCode));
			return;
		}

		var deleteRequest = request({
			url: url,
			method: 'POST',
			auth: {
				user: self.user,
				pass: self.password
			},
			form: {
				method: 'DELETE'
			},
			jar: true,
			followRedirect: true,
			rejectUnauthorized: false
		});

		deleteRequest.on('response', function (response) {
			if (response.statusCode === 204) {
				console.log('Zip file deleted.');
				deferred.resolve();
			} else {
				deferred.reject(new Error('Unknown Error: ' + response.statusCode));
			}

			return;
		});
	});

	return deferred.promise;
};

DemandwareTools.prototype.activateCode = function (server) {
    var self = this,
        deferred = Q.defer(),
        loginRequest = request({
            method: 'POST',
            url: 'https://' + server + this.instanceRoot + '/on/demandware.store/Sites-Site/default/ViewApplication-ProcessLogin',
            form: {
                LoginForm_Login: this.user,
                LoginForm_Password: this.password,
                LoginForm_RegistrationDomain: 'Sites'
            },
            jar: true,
            followRedirect: true,
            rejectUnauthorized: false
        }, function(error, response, body) {

            if (response.statusCode === 302 || response.statusCode === 200) {
                var activationRequest = request({
                    method: 'POST',
                    url: 'https://' + server + self.instanceRoot + '/on/demandware.store/Sites-Site/default/ViewCodeDeployment-Activate',
                    form: {
                        CodeVersionID: self.buildVersion
                    },
                    jar: true,
                    rejectUnauthorized: false
                }, function(error, response, body) {
                        if (response.statusCode === 200) {
                            deferred.resolve();
                            return;
                        } else {
                            deferred.reject(new Error('Unknown Error: ' + response.statusCode));
                            return;
                        }
                });
            } else {
                deferred.reject(new Error('Unknown Error: ' + response.statusCode));
                return;
            }

        });

    return deferred.promise;
};

module.exports = DemandwareTools;
