# Setting up the deployment project in Jenkins

## Note: This guide assumes that the Jenkins server has already been set up by the OSC team

### Add the new project to the Jenkins server
  
  On the Jenkins server, you will start at the dash board and it should look something like this:
  
  ![Jenkins Dashboard][jenkins-dashboard]

  [jenkins-dashboard]: gulp_builder/images/jenkins-dashboard.png
  
  Please choose the "New Item" link in the top left to continue
  
  On the "New Item" page you will have several options to choose from. You should see a screen similar to the one below:
  
  ![Jenkins New Item][jenkins-new-item]

  [jenkins-new-item]: gulp_builder/images/jenkins-new-item.png
  
  If this is a new server with no projects on it, please use the "Freestyle Project" option. Otherwise, if this server already has several projects setup it may be faster to use the "Copy existing Item" option to reduce setup time. This option will copy over all of the settings from the chosen project into your new project. After choosing one of these options make sure to enter a name for your project in the "Item name" field, preferably using dashes instead of spaces between words. After this click the "OK" button at the bottom of the form.
  
  On the next page you should see a screen similar to the one below:
  
  ![Jenkins Project Configuration][jenkins-project-config]

  [jenkins-project-config]: gulp_builder/images/jenkins-project-config.png
  
  To continue the setup please choose either the "Multiple SCMs" option for Git repositories or the "Subversion" option for SVN repositories.
  
  If you are setting up Git repositories please read the following instructions. If not please skip below to the section on Subversion.
 
### Git Repository Setup
 
  To setup a Git repositories in Jenkins for use with our build script please choose the "Multiple SCMs" option under "Source Code Management"
  
  When the option has been chosen you should see a screen similar to the one below:

  ![Multiple SCMS][multiple-scms]

  [multiple-scms]: gulp_builder/images/multiple-scms.png

  To add a Git repository click the "Add SCM" drop down and choose the "Git option"

  After choosing the options you should see the below options:

  ![Git Options][git-options]

  [git-options]: gulp_builder/images/git-options.png

  Add the SSH Git url (e.g. git@code.lcgosc.com:demandware-standard-library/lyons-demandware-storefront-core-application.git) to the "Repository URL" field

  Then click the "Add" button next to the "Credentials" drop down to add a set of credentials to log into the Git server

  In the field "Branch Specifier (blank for 'any')" specify the branch name if you decide to use one other than master (e.g. */develop, */release, etc.)

  Add some additional options to the Git build by clicking the "Add" drop down under the "Additional Behaviours" section and choosing the following options:

  1. Checkout to a sub-directory
  2. Wipe out repository & force clone 

  In the "Local subdirectory for repo" field under the "Check out to a sub-directory" option add a name for the Git repository. The name should correspond to the name of the repository (e.g. demandware-lea-project, demandware-enhanced-store-locator, etc.). These also need to match the cartridge paths contained in gulp_builder/config.json so that the deployment scripts can find the files.

  Repeat the above steps for each repo that is part of the project

### Subversion Repository Setup

  Setting up a SVN repository is similar to setting up a Git repository but there are a few key differences.

  Instead of using the "Multiple SCMs" to setup the repository, please choose the "Subversion" option. This is because the Subversion plugin can handle multiple repos natively.

  Enter the "Repository URL" and "Credentials" exactly as the Git repo (except with a different URL)

  If using multiple respositories please enter a folder name in the "Local module directory" field. This is useful if there is a different repo for the private key used for two factor authentication

  To add multiple repos with SVN use the "Add module..." button. This will add another set of fields to use in setting up the next repo.

### Build Options Setup

  To setup the build options to allow for deployment to the servers scroll to the bottom of the Jenkin's project settings. There is a section called "Build" where the final part of the setup will occur.

  In the Build section there is an "Advanced..." button. After clicking the button you should see a section similar to the following:

  ![Build Options][build-options]

  [build-options]: gulp_builder/images/build-options.png

  This section will allow you to enter the build file and the properties for the build.

  In the "Build File" field enter the path relative to the Jenkins workspace to your deployment script. This should be similar to what is in the path in the image above.

  In the "Properties" field enter all of the properties needed for this project. For an explanation of all of the properties please refer to the [Build Properties](BuildProperties.md) document.

  After all of the properties have been entered click the "Apply" button at the bottom of the screen. This will save your project options.

  To test out the build, scroll to the top of the configuration and click the "Build Now" option in the upper left. If everything was setup correctly the build should complete successfully.
