To use the auto data deploy feature, use the following steps:

**Step 1**
Copy config.example.json to config.json and populate with your sandbox credentials. These will need to be updated whenever your password changes.

* user: your username
* pass: your password
* server: the prefix of your sandbox (ex. "dev01" or "demo")

**Step 2**
Install node module dependencies

* From the command line, navigate to the "utilities" folder (that contains this README)
* Run `npm install`

**Step 3**
Do this each time you pull new data_impex updates from repository

* From the command line, navigate to the "utilities" folder (that contains this README)
* Run `gulp deploy-and-import-data`