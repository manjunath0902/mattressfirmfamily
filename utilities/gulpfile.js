var gulp		= require('gulp');
	fs 			= require('fs'),
	zip			= require('gulp-zip'),
	path	   	= require('path'),
	Q		   	= require('q'),
	request	   	= require('request'),
	url		   	= require('url'),
	http 		= require('https'),
	nconf		= require('nconf');
	
// Load a file store onto nconf with the specified settings
nconf.use('file', { file: './config.json' });
var deployOptions = nconf.get('deployOptions');

// Zips up data file
gulp.task('zip-data-files', function() {
	var dataFolderPath = deployOptions.siteDataDirectory + '/' + deployOptions.siteDataFolderName;
	
	return gulp.src(dataFolderPath + '/**', {cwd: '../', base: '../' + deployOptions.siteDataDirectory})
		.pipe(zip(deployOptions.siteDataFolderName + '.zip'))
		.pipe(gulp.dest('./'));
});

gulp.task('delete-data-zip', ['zip-data-files'], function() {
	var fileName = deployOptions.siteDataFolderName + '.zip';
	var webDAVPath = '/on/demandware.servlet/webdav/Sites/Impex/src/instance/',
		initialMethod = 'DELETE';
		
	var dest = 'https://' + deployOptions.server + deployOptions.instanceRoot + webDAVPath + fileName;
	
	// Turn off unauthorized rejections for TLS connections
	process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
		
	var self = this,
		deferred = Q.defer(),
		deconstructedDest = url.parse(dest),
		distFile = './' + fileName,
		data = fs.readFileSync(distFile),
		httpOptions = {
			method : initialMethod,
			hostname : deconstructedDest.hostname,
			port : deconstructedDest.port || 443,
			path : deconstructedDest.path
		};
		
	httpOptions.auth = deployOptions.user + ':' + deployOptions.pass;
	
	var req = http.request(httpOptions, function(res) {
		if (res.statusCode === 204) {
			console.log('Remote file removed');
		} else if (res.statusCode === 401) {
			deferred.reject(new Error('Authentication Failed'));
			return;
		} else if (res.statusCode === 404) {
			console.log('Remote file did not exist');
		} else if (res.statusCode === 405) {
			deferred.reject(new Error('Remote server does not support webdav!'));
			return;
		} else {
			deferred.reject(new Error('Unknown error occurred!'));
			return;
		}
		
		deferred.resolve();
	});
	
	req.on('error', function(e) {
		console.log("Remote file does not exist");
		deferred.resolve();
	});
	
	req.end();
	
	return deferred.promise;
});

// Uploads zip file to DW server using WebDAV
gulp.task('upload-data-zip', ['delete-data-zip', 'zip-data-files'], function() {
	
	var fileName = deployOptions.siteDataFolderName + '.zip';
	var webDAVPath = '/on/demandware.servlet/webdav/Sites/Impex/src/instance/',
		initialMethod = 'PUT';
		
	var dest = 'https://' + deployOptions.server + deployOptions.instanceRoot + webDAVPath + fileName;
	
	// Turn off unauthorized rejections for TLS connections
	process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
	
	var self = this,
		deferred = Q.defer(),
		deconstructedDest = url.parse(dest),
		distFile = './' + fileName,
		data = fs.readFileSync(distFile),
		httpOptions = {
			method : initialMethod,
			hostname : deconstructedDest.hostname,
			port : deconstructedDest.port || 443,
			path : deconstructedDest.path
		};
	
	httpOptions.auth = deployOptions.user + ':' + deployOptions.pass;
	
	var req = http.request(httpOptions, function(res) {
		console.log('Status: ' + res.statusCode);
		if (res.statusCode === 201 || res.statusCode === 200) {
			console.log(dest);
			console.log('Successfully deployed');
		} else if (res.statusCode === 204) {
			deferred.reject(new Error('Remote file exists!'));
			return;
		} else if (res.statusCode === 401) {
			deferred.reject(new Error('Authentication failed'));
			return;
		} else if (res.statusCode === 405) {
			deferred.reject(new Error('Remote server does not support webdav!'));
			return;
		} else {
			deferred.reject(new Error('Unknown error occurred!'));
			return;
		}
		deferred.resolve();
		
		req.on('data', function(chunk) {
			console.log(chunk);
		});
	});
	
	req.end(data, 'binary');
	
	return deferred.promise;
});

// Imports ZIP into DW server using HTTP posts
gulp.task('import-data', ['deploy-data'], function() {
	
	var self = this,
		deferred = Q.defer(),
		loginRequest = request({
			method: 'POST',
			url: 'https://' + deployOptions.server + deployOptions.instanceRoot + '/on/demandware.store/Sites-Site/default/ViewApplication-ProcessLogin',
			form: {
				LoginForm_Login: deployOptions.user,
				LoginForm_Password: deployOptions.pass,
				LoginForm_RegistrationDomain: 'Sites'
			},
			jar: true,
			followRedirect: true,
			rejectUnauthorized: false
		});
	
		loginRequest.on('response', function (response) {
		if (response.statusCode === 302 || response.statusCode === 200) {
			var httpOptions = {
				method: 'POST',
				url: 'https://' + deployOptions.server + deployOptions.instanceRoot + '/on/demandware.store/Sites-Site/default/ViewSiteImpex-Dispatch',
				form: {
					ImportFileName: deployOptions.siteDataFolderName + '.zip',
					realmUse: false,
					import: ''
				},
				jar: true,
				rejectUnauthorized: false
			};
			
			var importRequest = request(httpOptions);
			
			importRequest.on('response', function (response) {
				if (response.statusCode === 200) {
					console.log("import ok!");
					deferred.resolve();
					return;
				} else {
					console.log("import failed");
					deferred.reject(new Error('Unknown Error: ' + response.statusCode));
					return;
				}
			});
		} else {
			deferred.reject(new Error('Unknown Error: ' + response.statusCode));
			return;
		}
		
	});

	return deferred.promise;
});

gulp.task('deploy-data', ['upload-data-zip']);
gulp.task('deploy-and-import-data', ['deploy-data', 'import-data']);