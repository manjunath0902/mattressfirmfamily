'use strict';

/******* app.scrolltotop ********/
/**
 * @function
 * @Animated Scroll To Top Button
 */



exports.init = function () {
	function CheckPos() {
		var siteNameVal = $("input[name='siteName']").val();
		var windowW = $(window).width();
		if (SitePreferences.CURRENT_SITE == '1800Mattress-RV') {
			return;
		}
		if (siteNameVal == 'Mattress-Firm' && windowW < 767 && $('.refinement-big-container nav').height() > 0) {
			return;
		}
		if ($(window).scrollTop() > scroled) {
			$('.scrollToTop').fadeIn('slow');
		} else {
			$('.scrollToTop').fadeOut('slow');
		}
	}
	var windowW = $(window).width();
	//if (windowW > 767) {
		var scroled = 600;
		$('body').append('<a href="javascript: void(0)" class="scrollToTop button">Back to Top</a>');
		//Check to see if the window is top if not then display button

		$(window).scroll(function () {
			CheckPos();
		});
		window.addEventListener('scroll', function () {
			CheckPos();
		});
		//Click event to scroll to top
		$('.scrollToTop').click(function (e) {
			e.preventDefault();
			$('html, body').animate({scrollTop: 0},800);
			ga('tealium_0.send', 'event', 'All Pages', 'Click', 'Back To Top');
			return false;
		});
	//}
};
