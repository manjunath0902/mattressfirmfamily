'use strict';

/**
 * @private
 * @function
 * @description Initializes events for the Geo Browser Location.
 */
var latitude = 0; 
var longitude = 0;
var geoStateCode;

function updateCustomerZipCode () {
	var url = Urls.setCustomerZipCode;	
	var data = {zipCode: geoStateCode};
	$.ajax({
		url: url,
		type: 'get',
		data: data,							
		success: function (data) {           
		   if(data){			   
			   window.location.reload();
		   }           
		},
		error: function (error) {
			var msg = error;
			initDeliveryLocationEvents();
		}
	});
}

function getZipCodeByCoordinates() {
	var googleMapkey = SitePreferences.GoogleMapKey;
	var url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+latitude+','+longitude+'&key='+googleMapkey;
	$.ajax({
		url: url,
		type: 'get',		
		dataType: 'json',		
		success: function (data) {
         if(data != "") {
        	 geoStateCode = data.results[0].formatted_address.split(',')[2].trim().split(' ')[1];
        	 updateCustomerZipCode();
         }
		},
		error: function (error) {
			var msg = error;			
		}
	});
}

function getLocationByBrowser() {
  if (navigator.geolocation) {
	navigator.geolocation.getCurrentPosition(showPosition,showError);
  } else { 
    var error = "Geolocation is not supported by this browser.";
  }
}

function showPosition(position) {
  latitude = position.coords.latitude; 
  longitude = position.coords.longitude; 
  getZipCodeByCoordinates();
}

function showError(error) {
	  var errorType;
	  switch(error.code) {
	    case error.PERMISSION_DENIED:
	      errorType = "User denied the request for Geolocation."
	      break;
	    case error.POSITION_UNAVAILABLE:
	    	errorType = "Location information is unavailable."
	      break;
	    case error.TIMEOUT:
	    	errorType = "The request to get user location timed out."
	      break;
	    case error.UNKNOWN_ERROR:
	    	errorType = "An unknown error occurred."
	      break;
	  }
	}
	/*************** app.browserGeoLocation public object ***************/
var userCurrentGeoLocation = {
	init: function () {
		getLocationByBrowser();
	}
};
module.exports = userCurrentGeoLocation;