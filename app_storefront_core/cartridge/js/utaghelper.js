'use strict';
var util = require('./util');

var tagManager = {
		//Send Bonus Products Details to Tealium
		addBonusProducttUtag: function () {
			if(SitePreferences.currentSiteId == 'Mattress-Firm') {
				var bonusProducts = $('.mini-cart-total').data('bonusproducts') != null ? $('.mini-cart-total').data('bonusproducts') : $('#pdpATC').data('bonusproducts') != null ? $('#pdpATC').data('bonusproducts') : null;		
				var index = 0;
				if(bonusProducts != null){
					while (bonusProducts[index]) {
						var bonusProduct = bonusProducts[index];
						var productImageURL = bonusProduct.productexternalid != 0 ? 'https://' + SitePreferences.amplienceHost +'/i/hmk/'+ bonusProduct.productexternalid+'?h=1220&w=1350' : '';
						var productURL = util.appendParamsToUrl(Urls.getHttpProductUrl, {"pid": bonusProduct.variantid});
						if(typeof utag != undefined && utag != null ){ 
							utag.link({ 
								"enhanced_actions"		:	"add", 
								"product_id"			:	[bonusProduct.productid],
					            "product_name"			:	[bonusProduct.productname],
					            "product_brand"			:	[bonusProduct.brand],
					            "product_quantity"		:	[bonusProduct.quantity],
					           	"product_price"			:	["0.00"],
					            "product_unit_price"	:	["0.00"],
					            "product_category"		:	[bonusProduct.productcategory],
					            "product_sku"			:	[bonusProduct.productsku],
					            "product_child_sku"		:	[bonusProduct.productchildsku], 
					            "product_img_url"		:	[productImageURL],
					            "product_url"			:	[productURL],
					            "product_size"			:	[bonusProduct.productsize]
							});
						}
						
						index++;
					}
				}
			}
		},
		
		addBonusProductUtagByItem: function (selectedBonusItem) {
			if(SitePreferences.currentSiteId == 'Mattress-Firm' && selectedBonusItem.length > 0) {
				var productImageURL = selectedBonusItem.data('productexternalid') != 0 ? 'https://' + SitePreferences.amplienceHost +'/i/hmk/'+ selectedBonusItem.data('productexternalid')+'?h=1220&w=1350' : '';
				var productURL = util.appendParamsToUrl(Urls.getHttpProductUrl, {"pid": selectedBonusItem.data('variantid')});
				var productDetail = selectedBonusItem.closest('.product-detail');
				var quantity = productDetail.find('div.quantity-part .display-value').text();
				var productSize = "";
				if(jQuery.isEmptyObject(productDetail.find('div.product-variations').data('attributes')) == false) {
					productSize = productDetail.find('div.product-variations').data('attributes')['size'].value;
				}
				var unitPrice = selectedBonusItem.data('price')!=null && selectedBonusItem.data('price').length > 0 ? parseFloat(selectedBonusItem.data('price').split(" ")[1].replace(/[^0-9.-]+/g,"")) : 0.00
				if(typeof utag != undefined && utag != null ){ 
					utag.link({ 
						"enhanced_actions"		:	"add", 
						"product_id"			:	selectedBonusItem.data('productid') !=null && selectedBonusItem.data('productid').length > 0 ? selectedBonusItem.data('productid').split() : [""],
				        "product_name"			:	selectedBonusItem.data('productname') !=null && selectedBonusItem.data('productname').length > 0 ? selectedBonusItem.data('productname').split() : [""],
				        "product_brand"			:	selectedBonusItem.data('brand') !=null && selectedBonusItem.data('brand').length > 0 ? selectedBonusItem.data('brand').split() : [""],
				        "product_quantity"		:	quantity.length > 0 ? quantity.split() : [""],
				        "product_price"			:	(unitPrice * Number(quantity)).toString().split(),
				        "product_unit_price"	:	unitPrice.toString().split(),
				        "product_category"		:	selectedBonusItem.data('productcategory') !=null && selectedBonusItem.data('productcategory').length > 0 ? selectedBonusItem.data('productcategory').split() : [""],
				        "product_sku"			:	selectedBonusItem.data('productsku') !=null && selectedBonusItem.data('productsku').toString().length > 0 ? selectedBonusItem.data('productsku').toString().split() : [""],
				        "product_child_sku"		:	selectedBonusItem.data('productchildsku') !=null && selectedBonusItem.data('productchildsku').length > 0 ? selectedBonusItem.data('productchildsku').split() : [""], 
				        "product_img_url"		:	productImageURL.length > 0 ? productImageURL.split() : [""],
				        "product_url"			:	productURL.length > 0 ? productURL.split() : [""],
				        "product_size"			:	productSize.length > 0 ? productSize.split() : [""]
					});
				}
			}			
		}
};

module.exports = tagManager;