'use strict';
/**
@class app.zipzone
* this also services the Customer Zone in the Header
*/
var $cache,
	progress = require('./progress');

$cache = {
	parent: $('#primary'),
	locationParent: $('.header-zip')
};


/**
 * @private
 * @function
 * @description Initializes events for the PDP Zip/Zone check.
 */
function initializeEvents() {		 
	$(document).on("submit","#pdp-delivery-form",function (e) {
		e.preventDefault();
		initZipFromPDP();
	});
	initZipFromHeader();
}


function IsValidZipCodeheadertop(formname, zip) {
	var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
	var $zipForm = formname;
	if (!isValid) {
		if ($zipForm.find(".error").length < 1) {
			//$(".postal .field-wrapper").append("<span class='error'>" + Resources.INVALID_ZIP + "</span>");
			$zipForm.append("<span class='error' tabindex='0' tabindex='0'>" + Resources.INVALID_ZIP + "</span>");
			$zipForm.find('button').attr("disabled", "disabled");
			//$zipForm.addClass("error");
		}
		return false;
	} else {
		if ($zipForm.find(".error").length > 0) {
			$zipForm.find("span.error").remove();
			$zipForm.find('button').removeAttr("disabled", "disabled");
			//$zipForm.removeClass("error");
		}
		return true;
	}
}

function IsValidZipCodePdp(zip) {
	if (zip.length > 0) {
		var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
		if (!isValid) {
			if ($('#pdp-delivery-form').find("span.error").length < 1) {
				$('#pdp-delivery-form').append("<span class='error' tabindex='0'>" + Resources.INVALID_ZIP + "</span>");
				$('#pdp-delivery-form').find('button').attr("disabled", "disabled");
				//$zipForm.addClass("error");
			}
			return false;
		} else {
			if ($('#pdp-delivery-form').find("span.error").length > 0) {
				$('#pdp-delivery-form').find("span.error").remove();
				$('#pdp-delivery-form').find('button').removeAttr("disabled", "disabled");
			}
			return true;
		}
	}
	if ($('#pdp-delivery-form').find("span.error").length > 0) {
		$('#pdp-delivery-form').find("span.error").remove();
		$('#pdp-delivery-form').find('button').attr("disabled", "disabled");
	}
	return true;
}
function IsValidZipCodeStoreFinderModal(zip) {
	var appendErrorToThisDiv = $('#StorePickerZip');
	if (zip.length > 0) {
		var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
		if (!isValid) {
			if (appendErrorToThisDiv.find("span.error").length < 1) {
				appendErrorToThisDiv.append("<span class='error' tabindex='0'>" + Resources.INVALID_ZIP + "</span>");
				appendErrorToThisDiv.find('button').attr("disabled", "disabled");
				//$zipForm.addClass("error");
			}
			return false;
		} else {
			if (appendErrorToThisDiv.find("span.error").length > 0) {
				appendErrorToThisDiv.find("span.error").remove();
				appendErrorToThisDiv.find('button').removeAttr("disabled", "disabled");
			}
			return true;
		}
	}
	if (appendErrorToThisDiv.find("span.error").length > 0) {
		appendErrorToThisDiv.find("span.error").remove();
		appendErrorToThisDiv.find('button').attr("disabled", "disabled");
	}
	return true;
}

function initZipFromHeader() {
	var $zipForm;
	$zipForm = $cache.locationParent.find('form[name="zone-header-form"]');
	/*$zipForm.find('input.p-code').first().keyup(function () {
		var $postalcodetmp = this.value;
		IsValidZipCodeheadertop($zipForm, $postalcodetmp);
	});*/
	
	$zipForm.find('input.p-code').first().on('keyup input change paste',function(){
		var $postalcodetmp = this.value;
		IsValidZipCodeheadertop($zipForm, $postalcodetmp);
	});

	$zipForm.submit(function (e) {
		e.preventDefault();
		var _zip = $.trim($(this).find('input#zipfield').val());
		// 1. Verify a valid US 5-digit ZIP first, and that the same ZIP was not entered again
		if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(_zip)) {
			var data = {zip: _zip};
			var url = Urls.initZoneCheck;

			$.ajax({
				url: url,
				type: 'post',
				data: data,
				dataType: 'html',
				beforeSend: function () { progress.show(); },
				success: function (data) {
					// Check what was returned
					if ($.trim(data) === 'invalidZip') {
						$cache.locationParent.find('.p-code').addClass('invalidZip').val('Invalid Zip Code');
					} else {
						$cache.locationParent.html(data);
						if ($('.variation-select').length > 0) {
							$('#va-size').change();
						} else {
							//window.location.reload();
							var zoneCode = $('#set-zone-header input[name=postalCode]').data('zoneCode') !== null ? $('.find-by-zip input[name=postalCode]').data('zoneCode').replace(',' , '|') : '';

							var newUrl = window.location.href;
							if ((zoneCode !== '' && v_category !== '') || (newUrl.indexOf('search?q=') > -1 && newUrl.indexOf('zone_code') > 1)) {
								if (window.location.hash.length > 0) {
									newUrl = newUrl.substring(0, newUrl.indexOf('#') - 1);
									newUrl = newUrl + '#prefn1=zone_code&prefv1=' + zoneCode;
								} else if (newUrl.indexOf('search?q=') > -1) {
									newUrl = newUrl.substring(0, newUrl.indexOf('prefv1'));
									newUrl = newUrl + 'prefv1=' + zoneCode;
								}
							}
							newUrl = newUrl.replace(window.location.hash,"");
							window.location.href = newUrl;
						}
					}
				},
				complete: function () { progress.hide(); }
			});
		}
	});
}

function initZipFromPDP() {
	var _zip = $.trim($('#pdp-delivery-zip').val());
	// 1. Verify a valid US 5-digit ZIP first, and that the same ZIP was not entered again
	if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(_zip)) {
		var data = {zip: _zip, pid: $('#pid').val()};
		var url = Urls.pdpZoneCheck;
		$.ajax({
			url: url,
			type: 'post',
			data: data,
			beforeSend: function () {
				progress.show('#product-content');
			},
			dataType: 'html',
			success: function () {				
				var url = $("#product-content span[itemprop='url']").html();
				if(url) {
					window.location.href = url;
				} else {
					location.reload();
				}
				
			},
			error: function (request, status, error) {
				/* service is down or had an error so enable the add to cart button */
				$('#add-to-cart').removeAttr('disabled');
			},
			complete: function () {
				progress.hide('#product-content');
			}
		});
	}
}
	/*************** app.pdpZipZone public object ***************/
var zipzone = {
	init: function () {
		initializeEvents();
	}
};
$('#pdp-delivery-zip').first().keyup(function () {
	var $postalcodetmp = this.value;
	IsValidZipCodePdp($postalcodetmp);
});

$(document).on("keyup focusout","#dwfrm_storepicker_postalCode",function() {
	var $postalcodetmp = this.value;
	IsValidZipCodeStoreFinderModal($postalcodetmp);
});
module.exports = zipzone;