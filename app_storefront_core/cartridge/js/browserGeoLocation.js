'use strict';

/**
 * @private
 * @function
 * @description Initializes events for the Geo Browser Location.
 */
var latitude = 0; 
var longitude = 0;
var geoStateCode;

function setGeoBrowserZipCode() {	
	var url = Urls.setGeoBrowserZipCode;	
	var data = {zip: geoStateCode};
	$.ajax({
		url: url,
		type: 'get',
		data: data,
		dataType: 'json',		
		success: function (data) {
			if(data != null && data.IsZipCodeSet) {
                window.location.reload();
			}
		},
		error: function (error) {
			var msg = error;			
		}
	});
}

function getZipCodeByCoordinates() {
	var googleMapkey = SitePreferences.GoogleMapKey;
	var url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+latitude+','+longitude+'&key='+googleMapkey;
	$.ajax({
		url: url,
		type: 'get',		
		dataType: 'json',		
		success: function (data) {
         if(data != "") {
        	//geoStateCode = data.results[0].formatted_address.split(',')[2].trim().split(' ')[1];
        	 var IndexHavingUSCountry=0;
        	 var isCountryFound=false;
        	 var isPostalCodeFound=false;
        	 if(data.results.length > 0){
	        	 for(var index=0; index<data.results.length; index++){
	        		 var addressComponents = data.results[index].address_components;
	        		 for(var i = 0; i < addressComponents.length; i++) {
	        			 if(addressComponents[i].types[0] == 'country' && addressComponents[i].short_name.toUpperCase()=='US'){                     
	        				 isCountryFound=true;
		                 }
	        			 if(addressComponents[i].types[0] == 'postal_code'){
	        				 isPostalCodeFound=true;
		                 }
		        	 }
	        		 if(isCountryFound && isPostalCodeFound){
	        			 IndexHavingUSCountry=index;
	        			 break;
	        		 }
	        		 else{
	        			 isCountryFound=false;
	        			 isPostalCodeFound=false;
	        		 }
	        	 }
	        	 if(isCountryFound && isPostalCodeFound){
		        	 var addressComponents = data.results[IndexHavingUSCountry].address_components;	        	 
		        	 for(var i = 0; i < addressComponents.length; i++) {
		        		 if(addressComponents[i].types[0] == 'postal_code'){                     
		                	 geoStateCode = addressComponents[i].long_name;
		                	 break;
		                 }
		        	 }
		        	 setGeoBrowserZipCode();
	        	 }
        	 }
         }
		},
		error: function (error) {
			var msg = error;			
		}
	});
}

function getLocationByBrowser() {
  if (navigator.geolocation) {
	navigator.geolocation.getCurrentPosition(showPosition,showError);
  } else { 
    var error = "Geolocation is not supported by this browser.";
  }
}

function showPosition(position) {
  latitude = position.coords.latitude; 
  longitude = position.coords.longitude; 
  getZipCodeByCoordinates();
}

function showError(error) {
	  var errorType;
	  switch(error.code) {
	    case error.PERMISSION_DENIED:
	      errorType = "User denied the request for Geolocation."
	      break;
	    case error.POSITION_UNAVAILABLE:
	    	errorType = "Location information is unavailable."
	      break;
	    case error.TIMEOUT:
	    	errorType = "The request to get user location timed out."
	      break;
	    case error.UNKNOWN_ERROR:
	    	errorType = "An unknown error occurred."
	      break;
	  }
	}

function getLocationByIP() {	
	var url = Urls.getLocationByIP;
	$.ajax({
		url: url,
		type: 'get',		
		dataType: 'json',		
		success: function (data) {
         if(data == null || data.CustomerZipCode == null || data.CustomerZipCode == "") {		 
        	 getLocationByBrowser();        	 
         }
		},
		error: function (error) {
			var msg = error;			
		}
	});
}
	/*************** app.browserGeoLocation public object ***************/
var browserGeoLocation = {
	init: function () {
		getLocationByIP();
	}
};
module.exports = browserGeoLocation;