'use strict';

var tealium = {
	init: function () {
		var test = "";
	},
	trigger: function (eCat, eAction, eLabel, eValue) {
		utag.link({
			ga_eventCategory: eCat,
			ga_eventAction: eAction,
			ga_eventLabel: eLabel,
			ga_eventValue: eValue
		});
	}
};
module.exports = tealium;
