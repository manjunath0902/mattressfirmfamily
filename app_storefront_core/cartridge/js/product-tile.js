'use strict';

var imagesLoaded = require('imagesloaded'),
	quickview = require('./quickview'),
	util = require('./util'),
	ResponsiveSlots = require('./responsiveslots/responsiveSlots');

function initQuickViewButtons() {
	$('.tiles-container .product-image').on('mouseenter', function () {
		var $qvButton = $('#quickviewbutton');
		if ($qvButton.length === 0) {
			$qvButton = $('<a id="quickviewbutton" class="quickview">' + Resources.QUICK_VIEW + '</a>');
		}
		var $link = $(this).find('.thumb-link');
		$qvButton.attr({
			'href': $link.attr('href'),
			'title': $link.attr('title')
		}).appendTo(this);
		$qvButton.on('click', function (e) {
			e.preventDefault();
			quickview.show({
				url: $(this).attr('href'),
				source: 'quickview'
			});
		});
	});
}

function gridViewToggle() {
	$('.toggle-grid').on('click', function () {
		$('.search-result-content').toggleClass('wide-tiles');
		$(this).toggleClass('wide');
		if ($('.search-result-content').hasClass('wide-tiles')) {
			$(".product-key-features-plp").show();
			utag.link({
				eventCategory: 'PLP',
				eventAction: 'Select',
				eventLabel: 'List View'
			});
		} else {
			$(".product-key-features-plp").hide();
		}
		if ( SitePreferences.CURRENT_SITE == "1800Mattress-RV" ) {
			if ($('.search-result-content').hasClass('wide-tiles')) {
				$('.trimmed-name').show();
				$('.product-text-hover').hide();
				var uptoPercent = $('.search-result-content').find('.product-standard-price');
				if(uptoPercent) {
					uptoPercent.css( "padding-left", "0px" );
					uptoPercent.siblings( ".percentageDiscount" ).css( "margin-left", "-70px" );
				}
				if ($('.product-standard-price').hasClass('has-sale-price')) {
					$('.has-sale-price').siblings( ".percentageDiscount" ).css( "margin-left", "0px" );
				}
			} else {
				$('.trimmed-name').hide();
				$('.product-text-hover').show();
			}
			
		}
	});
	$('.toggle-grid').on('keypress', function (ev) {
		if (ev.keyCode == 13 || ev.which == 13) {
			$('.search-result-content').toggleClass('wide-tiles');
			$(this).toggleClass('wide');
			if ($('.search-result-content').hasClass('wide-tiles')) {
				$(".product-key-features-plp").show();
				utag.link({
					eventCategory: 'PLP',
					eventAction: 'Select',
					eventLabel: 'List View'
				});
			} else {
				$(".product-key-features-plp").hide();
			}
		}
	});
}
function compareToolPadding() {
	var $compareTool = $('.tiles-container .product-tile .compare-check'); //compareTool if on then its padding will be handled by compareToolPadding class
	if ($compareTool.length > 0) {
		$(".search-result-content .product-tile").addClass('compareToolPadding');
	}
}

function getDateSuffix(day) {
	if (day > 3 && day < 21) return 'th'; 
	switch (day % 10) {
		case 1:  return "st";
		break;
	 		case 2:  return "nd";
		break;
			case 3:  return "rd";
			break;
		default: return "th";
	}	
}

/**
 * @private
 * @function
 * @description Initializes events on the product-tile for the following elements:
 * - swatches
 * - thumbnails
 */
function initializeEvents() {
	if (SitePreferences.isQVenabled) {
		initQuickViewButtons();
	}
	gridViewToggle();
	compareToolPadding();
	$(document).on('mouseleave','.swatch-list', function () {
		// Restore current thumb image
		var $tile = $(this).closest('.product-tile'),
			$thumb = $tile.find('.product-image .thumb-link img').eq(0),
			data = $thumb.data('current');

		$thumb.attr({
			src: data.src,
			alt: data.alt,
			title: data.title
		});
	});
	$(document).on('click','.swatch-list .swatch', function (e) {
		if ($(this).hasClass('selected')) { return; }
		if (!$(this).hasClass('selected')) {
			e.preventDefault();
		}
		var $tile = $(this).closest('.product-tile');
		$(this).closest('.swatch-list').find('.swatch.selected').removeClass('selected');
		$(this).addClass('selected');
		$tile.find('.thumb-link').attr('href', $(this).attr('href'));
		$tile.find('name-link').attr('href', $(this).attr('href'));

		var data = $(this).children('img').filter(':first').data('thumb');
		var $thumb = $tile.find('.product-image .thumb-link img').eq(0);
		var currentAttrs = {
			src: data.src,
			alt: data.alt,
			title: data.title
		};
		$thumb.attr(currentAttrs);
		$thumb.data('current', currentAttrs);
	}).on('mouseenter','.swatch-list .swatch', function () {
		// get current thumb details
		var $tile = $(this).closest('.product-tile'),
			$thumb = $tile.find('.product-image .thumb-link img').eq(0),
			data = $(this).children('img').filter(':first').data('thumb'),
			current = $thumb.data('current');

		// If this is the first time, then record the current img
		if (!current) {
			$thumb.data('current', {
				src: $thumb[0].src,
				alt: $thumb[0].alt,
				title: $thumb[0].title
			});
		}

		// Set the tile image to the values provided on the swatch data attributes
		$thumb.attr({
			src: data.src,
			alt: data.alt,
			title: data.title
		});
	});
	// AB Test impressions: PLP_Master_vs_Variation_tiles
	if (SitePreferences.CURRENT_SITE === "Mattress-Firm" ) {
		if(typeof utag !== 'undefined' && typeof utag_data !== 'undefined' && typeof utag_data['page_type'] !== 'undefined' 
			&& (utag_data['page_type'] == "category")){
			var abtestSegmentField = $('#ab_test_master_variant');
			var abtestVariantActive = $('#ab_test_master_variant_active');
			var categoryIdField = $('#IsHawaiiPLP');
			var selectedSize = $("div.refinement-big-container div#size li.selected");
			if(abtestSegmentField.length > 0 && abtestVariantActive.length > 0 && categoryIdField.length > 0 && selectedSize.length > 0){					
				var segmentName = abtestSegmentField.val();
				var variantViewActive = abtestVariantActive.val();	
				var categoryId = categoryIdField.val();
					
				if(segmentName == "SegmentA" && variantViewActive == 'true'){
					utag.link({"eventCategory" : "PLP Filter Size Test", "eventLabel" : "PLP Filter Size - VAR1", "eventAction" : "PLP Filter Size VAR1 - Impression", "nonInteraction" : "true"});
				}else if (segmentName == "Control" && categoryId == '5637146827' && selectedSize.length == 1){
					utag.link({"eventCategory" : "PLP Filter Size Test", "eventLabel" : "PLP Filter Size - CTRL", "eventAction" : "PLP Filter Size CTRL - Impression", "nonInteraction" : "true"});
				}
			}
		}
	}
}

function loadAvailabilityMessage(pid, container, defaultMsg, sizeRefinementCount) {
	
	var result = "";	
	var containerText = (container && typeof container.text !== "undefined") ? container.text() : "";	
	var deliveryMessageExist = (containerText && containerText.trim().length > 0) ? true : false;
	
	if (pid && container && !deliveryMessageExist){
		
		var url = util.appendParamsToUrl(Urls.getPLPATPAvailabilityMessage, {productId: pid, qty: 1, sizeCount: sizeRefinementCount});
		var nonAtpMessage = Resources.ATP_AVAILABILITY_PLP;
		var deliveryMsgHtml = '<span class="plp-tile-availability-datestr">' + defaultMsg + '.</span>';
		nonAtpMessage = nonAtpMessage.replace("tempDate", deliveryMsgHtml);
		
		$.getJSON(url, function (data) {
			var fail = false;
			var msg = '';
			if (!data) {
				msg = Resources.BAD_RESPONSE;
				fail = true;
			} else if (!data.success) {
				fail = true;
			}
			if (fail) {
				container.html(nonAtpMessage);
				return result;
			} else if (data.success) {

				if (data.showATPMessage) {
					if (data.availabilityDate != null && data.isInStock === true) {
						var availabilitySuccess = Resources.ATP_AVAILABILITY_PLP;
						var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
						
						//2018-04-05T00:00:00.000Z
						var dateTime = data.availabilityDate;
						var dateOnly = dateTime.split('T');
						var ymd = dateOnly[0].split('-');
						var date = ymd[2];
						var month = months[Number(ymd[1])-1];
						var availabilityDateString = month + ' ' + date;
						if (data.dateDifferenceString != '' ) {
							var dateText = '<span class="plp-tile-availability-datestr">' + data.dateDifferenceString + ' ' + availabilityDateString + '.</span>';
							availabilitySuccess = availabilitySuccess.replace("tempDate", dateText);
						} else {
							var dateText = '<span class="plp-tile-availability-datestr">' + availabilityDateString + '.</span>';
							availabilitySuccess = availabilitySuccess.replace("tempDate", dateText);
						}
						container.html(availabilitySuccess);
					}else {
						
						container.html(nonAtpMessage);
					}					
				} else {
					
					container.html(nonAtpMessage);
				}
			}
		});
	}
}

function initializeTileEvents(){	
    $('.btn-more-features').unbind("click");	
	$('.btn-more-features').on('click', function(){
		$(this).closest('.comfort-and-features').toggleClass('more-features-visible');
	});
	$( ".features-list .feature" ).on( "mouseenter touchstart", function( event ) {
		$('.feature-details').hide();
		$(this).find('.feature-details').show();
	});
	$( ".features-list .feature" ).on( "mouseleave touchend", function( event ) {
		$('.feature-details').hide();
		$(this).find('.feature-details').hide();
	});
	$('.btn-more-features-mobile').on('click', function(){
		$(this).siblings('.features-list').show();
	});
	$('.close-features').on('click', function(){
		$(this).closest('.features-list').hide();
	});		


	if (SitePreferences.CURRENT_SITE === "Mattress-Firm" ) {
		
		var searchContent = $(".search-result-content");
		var inMarket = (searchContent.length === 1) ? searchContent.data('inmarket') : false;
		
		if(inMarket){
			// Core Products ATP/non-ATP messaging
			if(SitePreferences.enableATPCacheOnPLP){
				$(".search-result-items").find(".plp-tile-availability").each(function(){			
					var prodTile = $(this).closest(".product-tile");
					
					if(prodTile.length === 1){
						var prodId = prodTile.data('itemid');
						var defMsg = prodTile.data('deliverymsg');
						var sizeRefinementCount = $('div.refinement.size ul li.selected').length;
						//var sizeRefinements =  $('div.refinement.size ul li.selected').find(".refinement-text").text();
						defMsg = decodeURIComponent(defMsg);
						
						if(!$(this).hasClass('loaded')){
							loadAvailabilityMessage(prodId, $(this), defMsg, sizeRefinementCount);
							$(this).addClass('loaded');
						}
					}
				});
			}
			// Non Core Product messaging  
			if(SitePreferences.enablePLPAvailabilityMessage){
				
				$(".search-result-items").find(".default-availability-msg").each(function(){		
					var prodTile = $(this).closest(".product-tile");
					if(prodTile.length === 1){
						var defMsg = prodTile.data('deliverymsg');
						if(defMsg){
							defMsg = decodeURIComponent(defMsg);
						}
						if(!$(this).hasClass('loaded') && defMsg){
							var availabilitySuccess = Resources.ATP_AVAILABILITY_PLP;
							var dateText = '<span class="default-availability-msg-datestr">' + defMsg + '.</span>';
							availabilitySuccess = availabilitySuccess.replace("tempDate", dateText);
							$(this).html(availabilitySuccess);
							$(this).addClass('loaded');
						}
					}
				});
			}
		}
		// // AB Test clicks: PLP_Master_vs_Variation_tiles
		$(".search-result-items").find("div.product-tile div.product-image, div.product-tile div.product-name").on('click',function(e){
			
			if(typeof utag !== 'undefined' && typeof utag_data !== 'undefined' && typeof utag_data['page_type'] !== 'undefined' && (utag_data['page_type'] == "category")){
				
				var abtestSegmentField = $('#ab_test_master_variant');
				var abtestVariantActive = $('#ab_test_master_variant_active');
				var categoryIdField = $('#IsHawaiiPLP');
				var selectedSize = $("div.refinement-big-container div#size li.selected");
				if(abtestSegmentField.length > 0 && abtestVariantActive.length > 0 && categoryIdField.length > 0 && selectedSize.length > 0){					
					var segmentName = abtestSegmentField.val();
					var variantViewActive = abtestVariantActive.val();	
					var categoryId = categoryIdField.val();
					
					if(segmentName == "SegmentA" && variantViewActive == 'true'){
						utag.link({"eventCategory" : "PLP Filter Size Test", "eventLabel" : "PLP Filter Size - VAR1", "eventAction" : "PLP Filter Size VAR1 - Click"});
					}else if (segmentName == "Control" && categoryId == '5637146827' ){
						utag.link({"eventCategory" : "PLP Filter Size Test", "eventLabel" : "PLP Filter Size - CTRL", "eventAction" : "PLP Filter Size CTRL - Click"});						
					}
				}
			}
		});
	}
}

exports.init = function () {
	var $tiles = $('.tiles-container .product-tile');
	if ($tiles.length === 0) { return; }
	imagesLoaded('.product-image').on('done', function () {
		/*$tiles.syncHeight()
			.each(function (idx) {
				idx = idx;
				$(this).data('idx', idx);
			});*/
	});
	ResponsiveSlots.smartResize(function () {
		$tiles
		.each(function () {
			$(this).removeAttr('style');
		});
	});
	initializeEvents();
	initializeTileEvents();
};

/* 
*  This is needed in order re-init the layout of the product tile
*  with compare enabled after inifinte scroll loads.
*/
exports.initCompare = function () {
	compareToolPadding();
	// Do not run if we are on Search page on MattressFirm site
	if ((window.pageContext && window.pageContext.type && window.pageContext.type === 'search') && SitePreferences.CURRENT_SITE === "Mattress-Firm") {
		return false;
	} else {
		var util = require('./util');
		util.uniform();
	}
};

/* 
*  Initialize new product tile events after each infinite scroll load
*/
exports.initTileEvents = function () {
	initializeTileEvents();
};

/* 
*  This initializes the comfort level indicators for the MFRM
*  sub-category landing pages. Needs to be initialized after
*  each infinite scroll load.
*/
exports.initComfortLevelTagging = function () {
	$('.comfort-level.mattressfirm').find('span').each(function() {
		var _this = $(this);
		var comfortLevelClass = _this.attr('class');
		if (comfortLevelClass == _this.data('comfortlevel')) {
			_this.addClass('active');
			_this.prev('span').css('border', 'none');
			_this.on('click', function() {
				window.location = _this.data('pdplink');
			});
		}
	});
};

$('.comfort-level.1800mattressrv').find('span').each(function() {
	 if ($(this).hasClass('active')){
		 $(this).prev('span').css('border', 'none');
	 }
});


if ($('.pdp-comfort-scale-container').hasClass('scale-comfort-level-1')) {
	$('.comfort-scale-items > .comfort-level-1').css('margin-left', '4px');
	$('.comfort-scale-items > .comfort-level-1').prev('span').css('border', 'none');
}
if ($('.pdp-comfort-scale-container').hasClass('scale-comfort-level-2')) {
	$('.comfort-scale-items > .comfort-level-2').css('margin-left', '4px');
	$('.comfort-scale-items > .comfort-level-2').prev('span').css('border', 'none');
}
if ($('.pdp-comfort-scale-container').hasClass('scale-comfort-level-3')) {
	$('.comfort-scale-items > .comfort-level-3').css('margin-left', '4px');
	$('.comfort-scale-items > .comfort-level-3').prev('span').css('border', 'none');
}
if ($('.pdp-comfort-scale-container').hasClass('scale-comfort-level-4')) {
	$('.comfort-scale-items > .comfort-level-4').css('margin-left', '4px');
	$('.comfort-scale-items > .comfort-level-4').prev('span').css('border', 'none');
}
if ($('.pdp-comfort-scale-container').hasClass('scale-comfort-level-5')) {
	$('.comfort-scale-items > .comfort-level-5').css('margin-left', '4px');
	$('.comfort-scale-items > .comfort-level-5').prev('span').css('border', 'none');
}


// add carousel init to the global window object so that the asyncronous carousel can access it
window.productRecs = function() {

	var container = {
		initialize: function($el) {
			this.$el = $el;
		},
		showCarousel: function() {
			var dfd = new $.Deferred();

			var recs = (this.$el.find('li').length ? this.$el.find('li') : this.$el.find('div'));
			var cqSlot = this.$el.closest('div[id^="cq_recomm_slot"]');
			if (window.CQuotient && recs.length > 0 && cqSlot.length > 0) {
				dfd.resolve();
			}

			return dfd.promise();
		}
	};

	function slider(slider) {
		if(slider.hasClass('slick-slider')) {
			return false;
		} else {
			slider.slick({
				infinite: false,
				speed: 300,
				slide: '.grid-tile',
				slidesToShow: 4,
				slidesToScroll: 4,
				responsive: [{
						breakpoint: 1025,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 3
						}
					},
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 1.3,
							slidesToScroll: 1,
							dots:true,
						}
					}
				]
			});
		}
	}

	container.initialize($('.recs-slider'));
	var promise = container.showCarousel();
	promise.done(function() {
		$('.recs-slider').each(function() {
			slider($(this));
		});
	});
};