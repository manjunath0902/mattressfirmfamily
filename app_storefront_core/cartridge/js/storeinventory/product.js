'use strict';

var _ = require('lodash'),
    dialog = require('../dialog'),
    inventory = require('./'),
    page = require('../page');

var newLine = '\n';
var pdpStoreTemplate = function (store) {
    return [
        '<li class="store-list-item ' + (store.storeId === User.storeId ? ' selected' : '') + '">',
        '	<div class="store-address">' + store.address1 + ', ' + store.city + ' ' + store.stateCode +
        ' ' + store.postalCode + '</div>',
        '	<div class="store-status" data-status="' + store.statusclass + '">' + store.status + '</div>',
        '</li>'
    ].join(newLine);
};
var pdpStoresListingTemplate = function (stores) {
    if (stores && stores.length) {
        return [
            '<div class="store-list-pdp-container">',
            (stores.length > 1 ? '	<a class="stores-toggle collapsed" href="#">' + Resources.SEE_MORE + '</a>' : ''),
            '	<ul class="store-list-pdp">',
            _.map(stores, pdpStoreTemplate).join(newLine),
            '	</ul>',
            '</div>'
        ].join(newLine);
    }
};

var storesListing = function (stores) {
    // list all stores on PDP page
    if ($('.store-list-pdp-container').length) {
        $('.store-list-pdp-container').remove();
    }
    $('.availability-results').append(pdpStoresListingTemplate(stores));
};

$('.tab-label').keypress(function (e) {
    if (e.keyCode === 32 || e.keyCode === 13) {
        var tabID = $(this).prop('for');
        $('.tab-switch').prop('checked',false);
        $("#" + tabID).prop('checked',true);
    }
});



var productInventory = {
    setPreferredStore: function (storeId) {
        User.storeId = storeId;
        $.ajax({
            url: Urls.setPreferredStore,
            type: 'POST',
            data: {storeId: storeId}
        });
    },
    productSelectStore: function () {
        var self = this;
        inventory.getStoresInventory(this.pid).then(function (stores) {
            inventory.selectStoreDialog({
                stores: stores,
                selectedStoreId: User.storeId,
                selectedStoreText: Resources.PREFERRED_STORE,
                continueCallback: storesListing,
                selectStoreCallback: self.setPreferredStore
            });
        }).done();
    },
    addProductSelectStore: function (storeId, pid, format) {
        $.ajax({
            url: Urls.addProduct,
            type: 'POST',
            data: {storeId: storeId, pid: pid, format: format}
        }).done(function () {
            dialog.close();
            page.redirect(Urls.cartShow);
        });
     },
     changeShippingMethod: function () {
         $.ajax({
             url: Urls.setPreferredStore,
             type: 'POST',
             data: {storeId: storeId}
         });
     },
    init: function () {
        var $availabilityContainer = $('.availability-results'),
            self = this;
        this.pid = $('input[name="pid"]').val();

        $('#product-content .set-preferred-store').on('click', function (e) {
            e.preventDefault();
            if (!User.zip) {
                inventory.zipPrompt(function () {
                    self.productSelectStore();
                });
            } else {
                self.productSelectStore();
            }
        });

        if ($availabilityContainer.length) {
            if (User.storeId) {
                inventory.getStoresInventory(this.pid).then(storesListing);
            }

            // See more or less stores in the listing
            $availabilityContainer.on('click', '.stores-toggle', function (e) {
                e.preventDefault();
                $('.store-list-pdp .store-list-item').toggleClass('visible');
                if ($(this).hasClass('collapsed')) {
                    $(this).text(Resources.SEE_LESS);
                } else {
                    $(this).text(Resources.SEE_MORE);
                }
                $(this).toggleClass('collapsed');
            });
        }

        $('#pdpChatWithUs').on('click', function (e) {
        	if(SitePreferences.CURRENT_SITE === "Mattress-Firm" || SitePreferences.CURRENT_SITE === "Sleepys-RV") {
        		/* Trigering New Chat from Footer Link*/
        		e.preventDefault();
        		if($('#helpButtonSpan').length > 0) {
    				$('#helpButtonSpan').click();
    			}
        	}
        	else {
        		$('.header-chat a[id^="liveagent_button"]:visible').trigger('click');
        	}
        });
        $('.add-to-wishlist').on('click', function (e) {
	        e.preventDefault();
	        var quantity;
	        var $form = $(this).closest('form');
	        var wishlist_url = encodeURI($(this).attr('href'));
			if (document.location.protocol != 'https:') {
				wishlist_url = wishlist_url.replace("https","http");
			}
			$.ajax({
				url: Urls.loginStatus,
				type: 'Get',
				datatype:"text"
			})
			.done(function (response) {
				if (response == 'true')	{
					// make the server call
					$.ajax({
						url: wishlist_url,
						data: $form.serialize() + '&format=ajax',
						type: 'GET'
					})
					// success
					.done(function (response) {
						dialog.open({
							html: $(response),
							options: {
								autoOpen: true,
								dialogClass: 'wishlist-dialog',
								title: 'ADDED TO WISHLIST'
							}
						});
					})
					.error(function (xhr, status, error) {
	                });
				} else {
					var separator = wishlist_url.indexOf('?') !== -1 ? '&' : '?';
					window.top.location = wishlist_url + separator + $form.serialize();
				}
			});
		});
	}
};

$(document).on('click','.read-details-link .get-details', function (e) {
	e.preventDefault();
	$(this).closest('.read-details-link').addClass('tooltip-active');
});

$(document).on('click','.se-close', function (e) {
	e.preventDefault();
	$(this).closest('.read-details-link').removeClass('tooltip-active');
});

$(document).on("click", function(event) {
    if ( $(event.target).is(".get-details") === false && $(event.target).is(".se-tooltip") === false ) {
    	$('.read-details-link').removeClass('tooltip-active');
    }
});


module.exports = productInventory;