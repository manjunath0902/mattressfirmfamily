'use strict';

var _ = require('lodash'),
	dialog = require('../dialog'),
	TPromise = require('promise'),
	util = require('../util');

var newLine = '\n';
var storeTemplate = function (store, selectedStoreId, selectedStoreText) {
	return [
		'<li class="store-tile ' + store.storeId + (store.storeId === selectedStoreId ? ' selected' : '') + '">',
		'	<p class="store-name">' + store.name + '</p>',
		'	<p class="store-address">',
		'		' + store.address1 + '<br/>',
		'		' + store.city + ', ' + store.stateCode + ' ' + store.postalCode,
		'	</p>',
		'	<p class="store-hours"><div class="store-hours-label">' + Resources.HOURS + '</div>' + store.storeHours + '</p>',
		'	<p class="store-status" data-status="' + store.statusclass + '">' + store.status + '</p>',
		'	<button class="select-store-button" data-store-id="' + store.storeId + '"' +
		(store.statusclass !== 'store-in-stock' ? 'disabled="disabled"' : '') + '>',
		'		' + (store.storeId === selectedStoreId ? selectedStoreText : Resources.SELECT_STORE),
		'	</button>',
		'</li>'
	].join(newLine);
};

var storeListTemplate = function (stores, selectedStoreId, selectedStoreText) {
	if (stores && stores.length) {
		return [
			'<div class="store-list-container">',
				'<div class="used-zip-code">' + Resources.FOR_ZIP_CODE + ' ' + User.zip + '</div>',
				'<a href="#" class="change-location" id="changeLocation">' + Resources.CHANGE_LOCATION + '</a>',
				'<ul class="store-list">',
					_.map(stores, function (store) {
						return storeTemplate(store, selectedStoreId, selectedStoreText);
					}).join(newLine),
				'</ul>',
			'</div>',
			'<div class="store-list-pagination">',
			'</div>'
		].join(newLine);
	} else {
		return '<div class="no-results">' + Resources.INVALID_ZIP + '</div>';
	}
};

var zipPromptTemplate = function () {
	return [
		'<div id="preferred-store-panel" class="preferred-store-panel">',
			'<div class="form-row">' +
				'<label for="zipCode">' + Resources.ENTER_ZIP_CODE + '</label>',
				'<input type="text" id="user-zip" name="zipCode"/>' +
			'</div>' +
			'<div class="form-row selectbox">' +
				'<label for="">' + Resources.RADIUS + '</label>' +
				'<select name="radius" id="store-radius" class="uniform">' +
					'<option value="100">100 Miles</option>' +
					'<option value="200">200 Miles</option>' +
					'<option value="400">400 Miles</option>' +
				'</select>' +
			'</div>' +
			'<button class="zip-prompt-button" id="zipPromptButton">' + Resources.SEARCH + '</button>' +
		'</div>'
	].join(newLine);
};

/**
 * @description test whether zipcode is valid for either US or Canada
 * @return {Boolean} true if the zipcode is valid for either country, false if it's invalid for both
 **/
var validateZipCode = function (zipCode) {
	var regexes = {
		canada: /^[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ]( )?\d[ABCEGHJKLMNPRSTVWXYZ]\d$/i,
		usa: /^\d{5}(-\d{4})?$/
	},
		valid = false;
	if (!zipCode) { return; }
	_.each(regexes, function (re) {
		var regexp = new RegExp(re);
		valid = regexp.test(zipCode);
	});
	return valid;
};

var storeinventory = {
	zipPrompt: function (callback) {
		var self = this;
		dialog.open({
			html: zipPromptTemplate(),
			options: {
				title: Resources.PICKUP_IN_STORE,
				maxWidth: 820,
				open: function () {
					util.uniform();
					$('#user-zip').on('keypress', function (e) {
						if (e.which === 13) {
							// trigger the search button
							$('.ui-dialog-buttonset .ui-button').trigger('click');
						}
					});
					$('#zipPromptButton').on('click', function () {
						var zipCode = $('#user-zip').val();
						var radius = $('#store-radius').val();
						if (validateZipCode(zipCode)) {
							self.setUserZip(zipCode);
							self.setUserRadius(radius);
							if (callback) {
								callback(zipCode);
							}
						}
					});
				}
			}
		});
	},
	getStoresInventory: function (pid) {
		return TPromise.resolve($.ajax({
			url: util.appendParamsToUrl(Urls.storesInventory, {
				pid: pid,
				zipCode: User.zip,
				radius: User.radius
			}),
			dataType: 'json'
		}));
	},
	/**
	 * @description open the dialog to select store
	 * @param {Array} options.stores
	 * @param {String} options.selectedStoreId
	 * @param {String} options.selectedStoreText
	 * @param {Function} options.continueCallback
	 * @param {Function} options.selectStoreCallback
	 **/
	selectStoreDialog: function (options) {
		var self = this,
			stores = options.stores,
			selectedStoreId = options.selectedStoreId,
			selectedStoreText = options.selectedStoreText,
			storeList = storeListTemplate(stores, selectedStoreId, selectedStoreText);
		dialog.open({
			html: storeList,
			options: {
				title: Resources.PICKUP_IN_STORE,
				maxWidth: 820,
				dialogClass: 'select-store-dialog',
				buttons: [{
					text: Resources.CONTINUE,
					click: function () {
						if (options.continueCallback) {
							options.continueCallback(stores);
						}
						dialog.close();
					}
				}],
				open: function () {
					$('.select-store-button').on('click', function (e) {
						e.preventDefault();
						var storeId = $(this).data('storeId');
						// if the store is already selected, don't select again
						if (storeId === selectedStoreId) { return; }
						selectedStoreId = storeId;
						$('.store-list .store-tile.selected').removeClass('selected')
							.find('.select-store-button').removeClass('secondary-large').text(Resources.SELECT_STORE);
						$(this).text(selectedStoreText).addClass('secondary-large')
							.closest('.store-tile').addClass('selected');
						if (options.selectStoreCallback) {
							options.selectStoreCallback(storeId);
						}
						$('.select-store-dialog .ui-dialog-buttonset .ui-button').text(Resources.CONTINUE_WITH_PREFERRED_STORE);
					});
					$('#changeLocation').on('click', function (e) {
						e.preventDefault();
						self.setUserZip(null);
						// trigger the event to start the process all over again
						$('.set-preferred-store').trigger('click');
						$('.select-store-dialog .ui-dialog-buttonpane button').remove();
					});
				}
			}
		});
	},
	setUserZip: function (zip) {
		User.zip = zip;
		$.ajax({
			type: 'POST',
			url: Urls.setZipCode,
			data: {
				zipCode: zip
			}
		});
	},
	setUserRadius: function (radius) {
		User.radius = radius;
	},
	shippingLoad: function () {
		var $checkoutForm = $('.address');
		$checkoutForm.off('click');
		$checkoutForm.on('click', 'input[name$="_shippingAddress_isGift"]', function () {
			$(this).parent().siblings('.gift-message-text').toggleClass('hidden', $('input[name$="_shippingAddress_isGift"]:checked').val());
		});
	}
};

module.exports = storeinventory;
