'use strict';

var dialog = require('./dialog');

var content = {
	init: function () {
		this.initHeader();
		this.initLiveContent();
	},

	initHeader: function () {
		// Header Banner Slider Init
		$('.header-banner-top ul').slick({
			dots: false,
			arrows: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 2000
		});

		// Header Zip Dropdown
		$('.header-top .dropdown').on('click', function (e) {
			e.preventDefault();

			var link = $(this);
			var dropdown = $(this).next('.dropdown-block');

			if (link.hasClass('active')) {
				link.removeClass('active');
				dropdown.removeClass('active');
			} else {
				$('.header-top .dropdown').removeClass('active');
				$('.header-top .dropdown-block').removeClass('active');

				link.addClass('active');
				dropdown.addClass('active');

				if (link.parent().hasClass('header-zip')) {
					dropdown.find('input').focus();
				}
			}
		});
	},

	initLiveContent: function () {
		$('a.live-content').not('.initialized').on('click', function (e) {
			e.preventDefault();

			var contentId = $(this).data('content-id');
			if ((contentId != '') && (contentId != undefined)) {
				var params = {cid: contentId};

				$.ajax({
					url: Urls.liveContent,
					data: params,
					dataType: 'json',
					success: function (data) {
						if (data.content) {
							var html = '<div class="live-content-wrapper">' + data.content.body + '</div>';

							dialog.open({
								html: html,
								options: {
									maxHeight: 635,
									dialogClass: 'live-content-dialog',
									title: data.content.name
								}
							});
						}
					}
				});
			}
		}).addClass('initialized');
	}
};

module.exports = content;