'use strict';
/**
@class app.setSFEmailDialogHandler
*/

var dialog = require('./dialog'),
util = require('./util');

var setSFEmailDialogHandler = function (timeout) {
		//e.preventDefault();
		var obj = $('#sf-email-footer-include-div');
		var dheight = parseInt(obj.data('height'));
		var dwidth = parseInt(obj.data('width'));
		var durl = obj.data('url');
		var wait = timeout ? timeout : parseInt(obj.data('wait')) * 1000;
		var cookieExpiration = parseInt(obj.data('cookieExpiration'));
		var siteId = obj.data('siteId');
		var cookieName = siteId + '_email_sub';

		var customerAuthenticated = obj.data('customerAuthenticated') == true;
		var customerSubscribed = obj.data('customerSubscribed') == true;
		var callFromEmail = obj.data('callFromEmail') == true;
		
		if(typeof $.cookie !== "undefined") {
			if (customerSubscribed || callFromEmail) {
				$.cookie(cookieName, '1', {expires:365, path:'/'});
			} else if ($.cookie(cookieName) == null && !customerAuthenticated && (!customerAuthenticated || !customerSubscribed)) {
				setTimeout(function () {
					//e.preventDefault();
					dialog.open({
						url: durl,
						options: {
							height: dheight,
							width: dwidth,
							title: null,
							open: function () {
								var dialog = this;
								$(dialog).scrollTop(0);
								$('.ui-widget-overlay').on('click', function () {
									$(dialog).dialog('close');
								});
	
								$('#sf-email-subscription-form').on('submit', function (e) {
									e.preventDefault();
									var _email = $('#email').val();
									var _leadSource = $('#leadSource').val();
									var _leadCreative = $('#leadCreative').val();
									var _zipCode = $('#zipCode').val();
									var _zipCodeModal = $('#zip').val();
									var _fname = $('#name').val();
									var _siteId = $('#siteId').val();
									var _optOutFlag = $('#optOutFlag').val();
									var _gclid = $.cookie("_ga");
									if (/^[a-zA-Z0-9]+(?:(\.|_)[A-Za-z0-9!#$%&'*+/=?^`{|}~-]+)*@(?!([a-zA-Z0-9]*\.[a-zA-Z0-9]*\.[a-zA-Z0-9]*\.))(?:[A-Za-z0-9](?:[a-zA-Z0-9-]*[A-Za-z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/.test(_email)) {
										var params = {firstName: _fname, emailAddress: _email, zipCode: _zipCode, zipCodeModal: _zipCodeModal, leadSource: _leadSource, leadCreative: _leadCreative, siteId: _siteId, optOutFlag: _optOutFlag, gclid: _gclid};
										$("body").attr("style", "cursor:wait;");
										$.ajax({
											url: Urls.sfEmailSubscription,
											data: params,
											type: 'post',
											success: function (data) {
												$('#sf-email-subscription-content').html(data);
												$('#sf-start-shopping-button').show();
												$("body").attr("style", "cursor:default;");
											},
											error: function (request, status, error) {
												$("body").attr("style", "cursor:default;");
											}
										});
									} else {
										$('#sf-email-container').find('.error').remove().end().append(
											$('<div/>', {"class":"error"}).html('Please enter a valid email address')
										);
									}
								});
								$('#sf-start-shopping-button').on('click', function (e) {
									e.preventDefault();
									$(dialog).dialog('close');
								});
							}
						}
					})
				}, wait);
				$.cookie(cookieName, '1', {expires:cookieExpiration, path:'/'});
			}
		}
	};

exports.init = function (timeout) {
	//$('.sf-email-dialog-trigger').on('click', setSFEmailDialogHandler);
	setSFEmailDialogHandler(timeout);
};