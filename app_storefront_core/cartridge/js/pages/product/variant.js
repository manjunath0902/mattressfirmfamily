'use strict';

var amplience = require('../../amplience'),
	ajax = require('../../ajax'),
	image = require('./image'),
	progress = require('../../progress'),
	productStoreInventory = require('../../storeinventory/product'),
	tooltip = require('../../tooltip'),
	util = require('../../util'),
	updatePDPPrice = require('./updatePDPPrice');

var selectedQty = 1;

var isParamsValid = function (href) {
    var vars = [], hash;
    var hashes = href.slice(href.indexOf('?') + 1).split('&');
    var isValid = true;
    if(hashes != null && hashes.length > 0) {
	    for(var i = 0; i < hashes.length; i++) {
	        hash = hashes[i].split('=');
	        if(hash != null && hash[1] == "") {             
	            isValid = false;
	            break;
	        }
	    }
    }
    return isValid;
}

var variantInfo =  {
	"isSizeVariant" : false,
	"isFromNewPDP" : false
};
//Flag to used differentiate b/w variation dropdown open from keyboard or mouse.
var isComingFromKeyboard = false;
/**
 * @description update product content with new variant from href, load new content to #product-content panel
 * @param {String} href - url of the new product variant
 **/
var updateContent = function (href, ampset) {
	
    var isParamValid = isParamsValid(href);

	if (ampset == null) {
		var ampset = '';
		}
	var $pdpForm = $('.pdpForm');
	//var qty = $pdpForm.find('input[name="Quantity"]').first().val();
	// Preserve selected quantity MAT-1430	PDP - Quantity reverts back to '1' if Size is changed.
	var qtyAvailable = $pdpForm.find('#Quantity');
	if (qtyAvailable.length > 0 ) { 
	    var qty = (qtyAvailable).first().val(); //$pdpForm.find('#Quantity').first().val();
		selectedQty = isNaN(qty) ? '1' : qty;
	} 
	
	var params = {
		Quantity: isNaN(qty) ? '1' : qty,
		format: 'ajax',
		productlistid: $pdpForm.find('input[name="productlistid"]').first().val(),
		recurringPayment: $pdpForm.find('input[name="recurringPayment"]:checked').val()
	};

	if ( SitePreferences.CURRENT_SITE !== "Mattress-Firm" ) {
		progress.show($('#pdpMain'));
		ajax.load({
			url: util.appendParamsToUrl(href, params),
			target: $('#product-content'),
			callback: function () {
				if (SitePreferences.STORE_PICKUP) {
					productStoreInventory.init();
				}
				if (ampset != '') {
					image.replaceImages();
					amplience.initZoomViewer(ampset);
				}
				tooltip.init();
				util.uniform();
				updateLabel();
				$(document).trigger("pdpContentUpdateEvent");
				focusOnPDPdropDown();
				updatePDPPrice();
				dimensionModuleselectvalue();
				progressiveLeaseEstimator();
	            progress.hide($('#pdpMain'));	
	            if ($('.ui-tooltip').length > 1) {
	        		$('.ui-tooltip:first').hide();
				}
	            $("#add-to-cart").removeClass('disabled-btn');
			}
		});
	}
	
	if(SitePreferences.CURRENT_SITE === "Mattress-Firm"){
        ajax.load({
            url: util.appendParamsToUrl(href, params),
            target: $('#product-content'),
            callback: function () {
                if (SitePreferences.STORE_PICKUP) {
                    productStoreInventory.init();
				}
                if (ampset != '') {
                	image.replaceImages();
                	amplience.initZoomViewer(ampset);
                }
                tooltip.init();
                util.uniform();
                updateLabel();
                $(document).trigger("pdpContentUpdateEvent");
                focusOnPDPdropDown();
                updatePDPPrice();
                dimensionModuleselectvalue();
                progressiveLeaseEstimator();
                progress.hide($('#pdpMain'));                
                loadATPAvailabilityMessage();
                $(".select-variation-dropdown").prop('disabled', false);
                $("#lockDropdwon").val("false");
                $("#add-to-cart").removeClass('disabled-btn');
                selectSize();
                
                if($(".product-financing-custom").length == 2) {
                	   $(".product-financing-custom.no-credit").addClass('allignTop');
                }
                //Update the utag_data before adding utag
                updateUtagData();
                if(variantInfo.isSizeVariant){    	                 	
                	addSizeChangeUtag();
                }                
                variantInfo.isSizeVariant = false;
                variantInfo.isFromNewPDP = false;
                UpdateAddtoCartButtonState();
            }
        });
        if(isParamValid != true){
            $("#add-to-cart").prop('disabled', true);
            if ($('.ui-tooltip').length > 1) {
        		$('.ui-tooltip').hide();
        	}
		}        
	}	
};
if(SitePreferences.CURRENT_SITE === "1800Mattress-RV"){
	if (util.isMobileSize()) {
		if ($('.product-key-features').length > 0) {
			$('.product-key-features').hide();
			$('.product-key-features').insertBefore('.pdp-comfort-scale-container').css("display", "block");
		}
		if ($('.uptopercent').length > 0) {
			$('.uptopercent').hide();
			$('.uptopercent').insertAfter('.top-sec').css("display", "block");
		}
	}
}

/***
 * Iterate all variant drop downs and disable add to cart button if any is missing   
 * @returns
 */ 
function UpdateAddtoCartButtonState() {
	if ( SitePreferences.CURRENT_SITE === "Mattress-Firm" ) {
		if (('.select-variation-dropdown').length>0) {
			var disableButton = false;
			$('.select-variation-dropdown').each(function (e) { 
				if($(this).find('option.selected').text().length==0){
					disableButton=true;
				}				
			});
			if(disableButton) {
				$("#add-to-cart").prop('disabled', true);
			}
		}
	}
}

function loadATPAvailabilityMessage() {
	$(".availability-web").append('<div class="loader-indicator-atp"></div>');
	var pid = $('#pid').val();
	var url = util.appendParamsToUrl(Urls.getATPAvailabilityMessageAjax, {productId: pid,qty: 1, format: 'ajax', atpContext:'product'});
	var productShippingInformation = $("#product-content").attr("data-product-shipping-information");
	var customerZipCode = $("#product-content").attr("data-customer-zipcode");
	var hasDisabledClass = $("#add-to-cart").hasClass("add-to-cart-disabled");
	// dont make Atp Ajax Call if product is chicago delivery. 
	var isChicagoProduct = $('.product-actions').data("ischicago-product")!='undefined' ? $('.product-actions').data("ischicago-product") : false;
	if (productShippingInformation && productShippingInformation == "core" && hasDisabledClass == false && isChicagoProduct == false){
		$.getJSON(url, function (data) {
			// Adding Try Catch Block to stop loader if Any Exception Occur in Client Side JS.
			try {
					var fail = false;
					var msg = '';
					if (!data) {
						msg = Resources.BAD_RESPONSE;
						fail = true;
					} else if (!data.success) {
						fail = true;
					}
					if (fail) {
						// trigger tealium tag when ATP fails // Call only when utag is defined
						if (productShippingInformation && customerZipCode && productShippingInformation == "core") {
							(typeof utag != 'undefined' && utag != null) ? utag.link({ "eventCategory" : ["ATP"], "eventLabel" : ["PDP_" + pid + "_" + customerZipCode], "eventAction" : ["fail"] }) : null;
						}
						
						//show default ATP message
						if ($('#replaceMeID') != null && $('#replaceMeID').length) {
							$("#replaceMeID").parent().remove();
							getDefaultAvailabilityMessageAjax(pid);
						}
						//show default ATP message AB
						if ($('#replaceMeID_ab') != null && $('#replaceMeID_ab').length) {
							$("#replaceMeID_ab").parent().remove();
							getDefaultAvailabilityMessageAjax(pid);
						}
						
						$(".availability-web .loader-indicator-atp").remove();
						//$error.html(msg);
						return;
					}
					if (data.success) {
						if ($('#replaceMeID') != null && $('#replaceMeID').length) {
							if (data.availabilityClass) {
								$("#replaceMeID").removeClass();
								$('#replaceMeID').addClass(data.availabilityClass);
							}
						}
						
						if ($('#replaceMeID_ab') != null && $('#replaceMeID_ab').length) {
							if (data.availabilityClass) {
								$("#replaceMeID_ab").removeClass();
								$('#replaceMeID_ab').addClass(data.availabilityClass);
							}
						}
						if (data.showATPMessage) { /* !pdict.Product.master 
													&& !pdict.Product.variationGroup
													&& pdict.Product.custom.shippingInformation.toLowerCase() == 'core' 
													&& dw.system.Site.getCurrent().getCustomPreferenceValue('enableAtpAvailabilityMessage') 
													&& session.custom.customerZip} */
							if (data.availabilityDate != null && data.isInStock === true) {
								var availabilitySuccess = Resources.ATP_AVAILABILITY_SUCCESS;
								var availabilitySuccess_ab = Resources.ATP_AVAILABILITY_SUCCESS_AB;
								var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
								
								//2018-04-05T00:00:00.000Z
								var dateTime = data.availabilityDate;
								var dateOnly = dateTime.split('T');
								var ymd = dateOnly[0].split('-');
								var date = ymd[2];
								var month = months[Number(ymd[1])-1];
								var availabilityDateString = month + ' ' + date;
								if (data.dateDifferenceString != '') {
									availabilitySuccess = availabilitySuccess.replace("tempDate", data.dateDifferenceString + ' ' + availabilityDateString);
									availabilitySuccess_ab = availabilitySuccess_ab.replace("tempDate", data.dateDifferenceString + ' ' + availabilityDateString);
								} else {
									availabilitySuccess = availabilitySuccess.replace("tempDate", availabilityDateString);
									availabilitySuccess_ab = availabilitySuccess_ab.replace("tempDate", availabilityDateString);
								}
								if($('#replaceMeID') != null){
									$('#replaceMeID').find('p').text(availabilitySuccess);							
								}
								if($('#replaceMeID_ab') != null) {							
									$('#replaceMeID_ab').find('p').text(availabilitySuccess_ab);
								}
								
								
								if (data.detailsContentId != '') {
									var anchorDetails;
									if($('#replaceMeID') != null) {
										anchorDetails = $('#replaceMeID').find('a');
									}
									
									if($('#replaceMeID_ab') != null) {
										anchorDetails = $('#replaceMeID_ab').find('a');
									}
									
									if ($('#replaceMeID') != null && anchorDetails.length == 0) {
										$("#replaceMeID").find('p').after("<a></a>");
									}
									
									if ($('#replaceMeID_ab') != null && anchorDetails.length == 0) {
										$("#replaceMeID_ab").find('p').after("<a></a>");
									}
									if ($('#replaceMeID') != null) {
										anchorDetails = $('#replaceMeID').find('a');
									}
									if ($('#replaceMeID_ab') != null) {
										anchorDetails = $('#replaceMeID_ab').find('a');
									}
									
									var hrefDetails = util.appendParamsToUrl(Urls.pageShow, {cid: data.detailsContentId});
									anchorDetails.attr("href", hrefDetails);
									anchorDetails.addClass('details');
									anchorDetails.prop('title', 'details');
									anchorDetails.text('Details');
								}
							} else if ($('#replaceMeID') != null && $('#replaceMeID').length) {
								$("#replaceMeID").parent().remove();
								getDefaultAvailabilityMessageAjax(pid);
							} else if ($('#replaceMeID_ab') != null && $('#replaceMeID_ab').length) {
								$("#replaceMeID_ab").parent().remove();
								getDefaultAvailabilityMessageAjax(pid);
							}
						} else if ($('#replaceMeID') != null && $('#replaceMeID').length) {
							$("#replaceMeID").parent().remove();
							getDefaultAvailabilityMessageAjax(pid);
						}
						else if ($('#replaceMeID_ab') != null && $('#replaceMeID_ab').length) {
							$("#replaceMeID_ab").parent().remove();
							getDefaultAvailabilityMessageAjax(pid);
						}
						$(".availability-web .loader-indicator-atp").remove();
					}
			} catch (e) { // Safety Check. In Case we get Any Exception in Client Side JS
				//show default ATP message AB
				if ($('#replaceMeID_ab') != null && $('#replaceMeID_ab').length) {
					$("#replaceMeID_ab").parent().remove();
					getDefaultAvailabilityMessageAjax(pid);
				}
				$(".availability-web .loader-indicator-atp").remove();
				console.log('ATP Client JS Crashed');
			}
		}).error(function() { // In Case Ajax Call Crashes or Fails on Server or Client Side Due to Any Reason Remove Loader.
			//show default ATP message AB
			if ($('#replaceMeID_ab') != null && $('#replaceMeID_ab').length) {
				$("#replaceMeID_ab").parent().remove();
				getDefaultAvailabilityMessageAjax(pid);
			}
			$(".availability-web .loader-indicator-atp").remove();
			console.log('ATP Ajax Call Failed');
		});
	} else {		
		$(".availability-web .loader-indicator-atp").remove();
	}
	//Replace with the selected quantity after size change MAT-1430	PDP - Quantity reverts back to '1' if Size is changed.
	   $('.pdpForm #Quantity').val(selectedQty);
}

function getDefaultAvailabilityMessageAjax(pid) {
	var availabilityMessageSection = $('.availability-web');
	var url = util.appendParamsToUrl(Urls.getDefaultAvailabilityMessage, {productId: pid});
	availabilityMessageSection.load(url, function () {
	});
}

function focusOnPDPdropDown() {
	$(".select-variation-dropdown").focus();
	$(".fake-input").focus();
}

function constructAmpset () {
    var color_masterid = $('.swatches.color').find(':selected').data('masterid');
    var size_masterid = $('.swatches.size').find(':selected').data('masterid');
    var team_masterid = $('.swatches.variation-select').find(':selected').data('masterid');

    var color_ampset = $('.swatches.color').find(':selected').data('ampset');
    var size_ampset = $('.swatches.size').find(':selected').data('ampset');
    var team_ampset = $('.swatches.variation-select').find(':selected').data('ampset');
    
    var ampset = "";
    var masterid = (size_masterid != undefined) ? size_masterid : ((color_masterid != undefined) ? color_masterid : ((team_masterid != undefined) ? team_masterid : ''));

    ampset = ((size_ampset != undefined) ? size_ampset : '') + ((color_ampset != undefined) ? color_ampset : '') + ((team_ampset != undefined) ? team_ampset : '');
    
    if (ampset != ""){
        ampset = masterid + '_' + ampset + '_MSET';
    } else {
        ampset = "";
    }
    return ampset;
}

function constructAmpsetForCustomDD () {
	var color_masterid = $('.select-item.color.selected').data('masterid');
    var size_masterid = $('.select-item.size.selected').attr("data-masterid");
    var team_masterid = $('.swatches.variation-select').find(':selected').data('masterid');

    var color_ampset = $('.select-item.color.selected').data('ampset');
    var size_ampset = $('.select-item.size.selected').attr("data-ampset");
    var team_ampset = $('.swatches.variation-select').find(':selected').data('ampset');
    
    var ampset = "";
    var masterid = (size_masterid != undefined) ? size_masterid : ((color_masterid != undefined) ? color_masterid : ((team_masterid != undefined) ? team_masterid : ''));

    ampset = ((size_ampset != undefined) ? size_ampset : '') + ((color_ampset != undefined) ? color_ampset : '') + ((team_ampset != undefined) ? team_ampset : '');
    
    if (ampset != ""){
        ampset = masterid + '_' + ampset + '_MSET';
    } else {
        ampset = "";
    }
    return ampset;
}
/***
 * it will add unlink on Size variation change
 * @returns
 */
function addSizeChangeUtag () {
	//Build the Product Image URL
	var productExternalID = $('#pdpMain').length > 0 ?  $('#pdpMain').data('productexternalid') : "";
	var productImageURL = 'https://' + SitePreferences.amplienceHost +'/i/hmk/'+ productExternalID+'?h=1220&w=1350';
	if(typeof utag_data !=  'undefined' && typeof utag != undefined && utag != null ){
		utag.link({
	        "enhanced_action": 'Select_Size',
	        "product_id": utag_data.product_id,
	        "product_brand": utag_data.product_brand,
	        "product_category": utag_data.product_category,
	        "product_name": utag_data.product_name,
	        "product_sku": utag_data.product_sku,
	        //"product_unit_price": utag_data.product_unit_price,
	        "product_img_url": productImageURL.split(),
	        //"product_unit_price_num": utag_data.product_unit_price_num,
	        //"product_unit_price_as_num": utag_data.product_unit_price_as_num,
	        "option_product_name": $(':input[name*="_boxSpring"] option:selected').lenght>0 ? $(':input[name*="_boxSpring"] option:selected').data("option-product-name").split() : [""],
	        "option_product_price": $(':input[name*="_boxSpring"] option:selected').lenght>0 ? $(':input[name*="_boxSpring"] option:selected').data("option-price").split() : [""],
	        "option_product_value_id": $(':input[name*="_boxSpring"] option:selected').lenght>0 ? $(':input[name*="_boxSpring"] option:selected').val().split() : [""],
	        "product_size": utag_data.product_size,
	        "as_product_quantity": $('#Quantity').length>0 && $('#Quantity').val() !=null ? $('#Quantity').val().split() : $('#Quantity option:selected').length>0 && $('#Quantity option:selected').val()!=null ? $('#Quantity option:selected').val().split() : [""],
	        "product_url": utag_data.product_url,
	        "product_child_sku": utag_data.product_child_sku
	    });
	}
}

function updateUtagData() {
	if(variantInfo.isSizeVariant) {
		if(variantInfo.isFromNewPDP){
			if($('.select-item.size.selected').length > 0 ){
				utag_data.product_size = $('.select-item.size.selected').data('ampset').split();
				utag_data.product_url = $('.select-item.size.selected').data('url').split();
				var manufacturerSKU = $('.select-item.size.selected').data('manufacturersku');
				if(manufacturerSKU){
					utag_data.product_child_sku = manufacturerSKU.toString().split();
				}else {
					utag_data.product_child_sku = [""];
				}
			}else{
				utag_data.product_size = [""];
				utag_data.product_url =  [""];
				utag_data.product_child_sku = [""];
			}
		}
		else {
			utag_data.product_size = $('.swatches.size :selected').length > 0 ? $('.swatches.size :selected').data('ampset').split() : [""];
			utag_data.product_url =  $('.swatches.size :selected').length > 0 ? $('.swatches.size :selected').val().split() : [""];
			utag_data.product_child_sku = $('.swatches.size :selected').length > 0 ? $('.swatches.size :selected').data('manufacturersku').split() : [""];
		}	
	}
}

var updateLabel = function () {
	$('.product-variations .attribute').each(function (e) {
		var attributeTitle = $(this).find('.swatches').find(':selected').attr('title');
		if ($(this).find('.selected-attr-value').length == 0 && attributeTitle != '') {
			var attributeValue = '<span class="selected-attr-value">' + attributeTitle + "</span>";
			$(attributeValue).appendTo($(this).find('.label'));
		}
	});
}
/***
 * This function call when user select the variation dropdown.
 * @param @selectedOption selected li JQuery Element and @triggeredEvent triggered Event Object.
 * @returns
 */ 
function onSelectVariation(selectedOption, triggeredEvent) {
	variantInfo.isFromNewPDP = true;
	var variationAttribute = selectedOption.length> 0 ? selectedOption.data('va') : '';
	var variantAvailable = selectedOption.length> 0 && selectedOption.data('variantavailable') != 'undefined' ? selectedOption.data('variantavailable') : null;
	if(variantAvailable != null && !Boolean(variantAvailable)) {
		return;
	}
	if(variationAttribute.toLowerCase() === 'size') {
		$('.fake-input').html('');
		if($("#lockDropdwon").val() === "true") {
			return;
		}			
		$("#lockDropdwon").val("true");
		variantInfo.isSizeVariant = true;
		utag.link({eventCategory: 'Link - Product Detail', eventAction: 'Product Detail Column - Size', eventLabel: selectedOption.length>0 ? selectedOption.data('value') : ''});
	}
	else {
		variantInfo.isSizeVariant = false;
	}
	
	if( (!selectedOption.hasClass('select-item') && selectedOption.hasClass('selected')) || selectedOption.hasClass('out-of-stock') || selectedOption.data('url').length === 0)
	{
		$(".tooltip-part").removeClass('active-tooltip');	
		triggeredEvent.stopPropagation();
		return;
	}
	    
	//Amplience image logic - if size is clicked, do nothing.  If color is clicked and "selected," Look for color.
	//Note - Amplience breaks if the image doesn't exist.  They have no "size" based images, it would seem.  So, if size is clicked, then do nothing.
	if (selectedOption.hasClass("select-item li-select")) {
		selectedOption.addClass("selected").siblings().removeClass("selected");
		var ampset = constructAmpsetForCustomDD();
	}
	
	if(selectedOption.hasClass("variation-select")){
		if(ampset != undefined && ampset != null) {
			updateContent(selectedOption.data('url'), ampset);
		}
	}
	else{
		updateContent(selectedOption.data('url'), ampset);
	}
	
	 if ( selectedOption.hasClass("variation-select") && (ampset == undefined || ampset == null || ampset == "")) {
        $("#add-to-cart").prop('disabled', true);
    }
}

$(window).load(function() { 
	var index=0;
	$('.pdp-recommender-1 .search-result-items li').each(function() {
		index++;
		var $productTile = $(this).find('.product-tile');
		if($productTile.length>0) {
			utag.link({
				product_impression_id: $productTile.data('itemid'),
				product_impression_name: $productTile.find('.name-link-text').length>0 ? $(this).find('.name-link-text').text().trim():'', 
				product_impression_brand: $productTile.data('brand-name'), 
				product_impression_category: $productTile.data('category'), 
				product_impression_list: 'Product Details - You May Also Like',
				product_impression_position: index,
				product_impression_price: $productTile.find('.product-sales-price').length>0 ? $productTile.find('.product-sales-price').text().trim():$productTile.find('.product-standard-price').length>0 ? $productTile.find('.product-standard-price').text().trim():''}
			);
		}		
	});
	index=0;
	$('.pdp-recommender-2 .search-result-items li').each(function() {
		index++;
		var $productTile = $(this).find('.product-tile');
		if($productTile.length>0) {
			utag.link({
				product_impression_id: $productTile.data('itemid'),
				product_impression_name: $productTile.find('.name-link-text').length>0 ? $(this).find('.name-link-text').text().trim():'', 
				product_impression_brand: $productTile.data('brand-name'), 
				product_impression_category: $productTile.data('category'), 
				product_impression_list: 'Product Details - Frequently Bought Together',
				product_impression_position: index,
				product_impression_price: $productTile.find('.product-sales-price').length>0 ? $productTile.find('.product-sales-price').text().trim():$productTile.find('.product-standard-price').length>0 ? $productTile.find('.product-standard-price').text().trim():''});
		}
	
	});
});

module.exports = function () {
	var $pdpMain = $('#pdpMain');
	
	// hover on swatch - should update main image with swatch image
	$pdpMain.on('mouseenter mouseleave', '.swatchanchor', function () {
		var largeImg = $(this).data('lgimg'),
			$imgZoom = $pdpMain.find('.main-image'),
			$mainImage = $pdpMain.find('.primary-image');

		if (!largeImg) { return; }
		// store the old data from main image for mouseleave handler
		$(this).data('lgimg', {
			hires: $imgZoom.attr('href'),
			url: $mainImage.attr('src'),
			alt: $mainImage.attr('alt'),
			title: $mainImage.attr('title')
		});
		// set the main image
		image.setMainImage(largeImg);
	});

	
	// click on swatch - should replace product content with new variant
	$pdpMain.on('change', '.swatches', function (e) {
		if ( SitePreferences.CURRENT_SITE === "Mattress-Firm" ) {            
			 $(".select-variation-dropdown").prop('disabled', true);
        }       
		//Amplience image logic - if size is clicked, do nothing.  If color is clicked and "selected," Look for color.
		//Note - Amplience breaks if the image doesn't exist.  They have no "size" based images, it would seem.  So, if size is clicked, then do nothing.
		if ($(this).hasClass("color") || $(this).hasClass("size") || $(this).hasClass("variation-select")) {
			//If the swatch that was clicked has the class "selected" at this point, the swatch is in process of being deselected.
			if ($(this).hasClass("selected")) {
				var ampset = "";
			} else {
                ampset = constructAmpset();
			}
			if($(this).hasClass('size') && $('.swatches.size').find(':selected').data('ampset') != undefined && SitePreferences.CURRENT_SITE === "Mattress-Firm"){
				variantInfo.isFromNewPDP = false;
				variantInfo.isSizeVariant = true;
			}
		}
		if($(this).hasClass("variation-select")){
			if(ampset != undefined && ampset != null) {
				updateContent($(this).val(), ampset);
			}
		}
		else{
			updateContent($(this).val(), ampset);
		}
		
		if ( $(this).hasClass("variation-select") && (ampset == undefined || ampset == null || ampset == "")) {
             $("#add-to-cart").prop('disabled', true);
        }
	});
	
	$pdpMain.on('click', '.product-variations .li-select', function (event) {				
		onSelectVariation($(this), event);
	});		
	//CPP-10 Select Variation Dropdown with Tabs in Mattress Firm.
	if ( SitePreferences.CURRENT_SITE === "Mattress-Firm" ) {
		$pdpMain.on('keypress keyup keydown', '.product-variations .li-select', function (event) {				
			if ((event.keyCode == 13 || event.which == 13) && !isComingFromKeyboard) {
				onSelectVariation($(this), event);
			}
		});
						
		//SHOP-3157- PDP Recommender Tracking events
		$pdpMain.on('click', '.pdp-recommender-1 .slick-next.slick-arrow', function () {				
			if (typeof (utag) != "undefined") {
				utag.link({eventCategory: 'Link - Product Detail', eventAction: 'You May Also Like - Slider', eventLabel: 'Right'});
			}
		});
		
		$pdpMain.on('click', '.pdp-recommender-1 .slick-prev.slick-arrow', function () {				
			if (typeof (utag) != "undefined") {
				utag.link({eventCategory: 'Link - Product Detail', eventAction: 'You May Also Like - Slider', eventLabel: 'Left'});
			}
		});
		$pdpMain.on('click', '.pdp-recommender-2 .slick-next.slick-arrow', function () {				
			if (typeof (utag) != "undefined") {
				utag.link({eventCategory: 'Link - Product Detail', eventAction: 'Frequently Bought Together - Slider', eventLabel: 'Right'});
			}
		});
		
		$pdpMain.on('click', '.pdp-recommender-2 .slick-prev.slick-arrow', function () {				
			if (typeof (utag) != "undefined") {
				utag.link({eventCategory: 'Link - Product Detail', eventAction: 'Frequently Bought Together - Slider', eventLabel: 'Left'});
			}
		});
	}
	$(document).ready(function() {
		if ( SitePreferences.CURRENT_SITE === "Mattress-Firm" ) {
			initAccordion();
			restrictFAQsView();
			dimensionModuleselectvalue();
			progressiveLeaseEstimator();
			if($('div.product-swatches').length > 0){
				$("ul.swatch-list li:first-child a").click();
			}
			
			var variantDropDowns= $('.select-variation-dropdown option:selected');
			if(variantDropDowns != null && variantDropDowns.length > 0) {
		        for (var i = 0; i < variantDropDowns.length; i++) {                  
		           if ($(variantDropDowns[i]).text().indexOf("Select") > -1) {
		               $("#add-to-cart").prop('disabled', true);
		           }
		        }
			}
					
		}
        //
		const urlParams = new URLSearchParams(window.location.search);
		const myParam = urlParams.get('v1');
		//		
        if (SitePreferences.CURRENT_SITE === "Mattress-Firm") {
            if ($('.swatches.color').hasClass("selected") || $('.swatches.size').hasClass("selected") || $('.swatches.variation-select').hasClass("selected")) {
				var ampset = "";
			} else {
				//The swatch is being selected at this point, so look for that color-based image.
                var ampset = constructAmpset();
                //if (window.isMattressProductPage) { 
                    var ampset = constructAmpsetForCustomDD();
                //} else {
                  //var ampset = ""; //constructAmpset();
                //}                                        
			}
            if (ampset != '') {
				image.replaceImages();
				amplience.initZoomViewer(ampset);
			}
		}
	});

	if ( SitePreferences.CURRENT_SITE === "Mattress-Firm" ) {
		if ($('.defaultselect').length  > 0) {          
	        $(".select-variation-dropdown").prop('disabled', true);          
	        var selectOption = $('.defaultselect');
	        selectOption.addClass('selected');
	        selectOption.attr('selected','selected');          
	        updateContent(selectOption.val(), "");
	    }
	}	
	// change drop down variation attribute - should replace product content with new variant
	//$pdpMain.on('change', '.variation-select', function () {
		//if ($(this).val().length === 0) { return; }
		//updateContent($(this).val(),"");
	//});
	util.uniform();
	updateLabel();
	loadATPAvailabilityMessage();
	selectSize();
};

var restrictFAQsView = function() {
	var items = $('.accordion li');

	if(items.length > 0){
		items.each(function(idx, li) {
		    if(idx > 4){
		    	if($(this).hasClass('hide')){
		    		$(this).removeClass('hide');
		    	} else {
		    		$(this).addClass('hide');
		    	}
			}
		});
		
		if(items.length > 5){
			if($('#faq-viewmore').hasClass('hide')){
				$('#faq-viewmore').removeClass('hide');
			} else {
				$('#faq-viewmore').addClass('hide');
			}
			
		}
	}
}


var initAccordion = function (){
	$( ".accordion" ).accordion({
		collapsible: true,
		active: false,
		heightStyle: "content"
	});
	$('.accordion-opener').off('click').click(function(e){
        $(this).next().toggle('fast');
        $(this).toggleClass('ui-accordion-header-active');
        e.preventDefault();
    });
	
	$('#faq-viewmore').on('click', function (e) {
        e.preventDefault();
        restrictFAQsView();
    });
};

function dimensionModuleselectvalue() {
	if($('.dimension-section').length > 0) {
		var selecteddize =  $('#va-size li.size.selected').attr("data-ampset");  
		var s = 0;
		var ind;
		$( "#dd-variant-dropdown .dd-options .dd-option label" ).each(function() {
			var ddoptiontext = $(this).text().toUpperCase();
			if(ddoptiontext == selecteddize) {	
				ind = s;
		      return false;
			}
			s++;
			});
		$('#dd-variant-dropdown').ddslick('select', {index: ind });
	}	
}
/***
 * Open Progressive Lease Estimator on PDPs 
 */
function progressiveLeaseEstimator() {
	// Progressive slot is available
	if ($('.prog-pdp-modal').length > 0){
		
		$('.prog-pdp-modal').click(function (e) {
			var price = 0;
			var state = "";
			var divPrice = $('div.product-price span.price-sales');
			var divState = $("div.geo-address-zip");
			// Get price-sales
			if(divPrice.length > 0 && divPrice.text() ){
			
				price = divPrice.text().trim().replace(/[^0-9.-]+/g,"");
			}			
			// Get State code
			if(divState.length > 0 && divState.text()){				
				var comps = divState.text().split(",");				
				if(comps && comps.length && comps.length > 1 && comps[1]){					
					state = comps[1].trim().substr(0, 2);
				}
			}

			ProgHangtag.OpenPaymentEstimator(SitePreferences.progressiveLeasingStoreID, state, price, "pdp","right");
		});
		
		$('.prog-pdp-modal__no-credit-needed').click(function() {
			if (typeof (utag) != "undefined") {
				utag.link({eventCategory: 'Link - Product Detail', eventAction: 'Product Detail Column - Progressive Estimate', eventLabel: 'No Credit Needed'});
			}
		});
		
		$('.prog-pdp-modal__learn-more').click(function() {
			if (typeof (utag) != "undefined") {
				utag.link({eventCategory: 'Link - Product Detail', eventAction: 'Product Detail Column - Progressive Estimate', eventLabel: 'Get Estimate'});
			}
		});
		
	}			
}
/***
 * This function update the all li states while selecting variation.
 * @param @selectedVariant selected li JQuery Element and @triggeredEvent triggered Event Object.
 * @returns
 */
function updateDropDownContent(selectedVariant, triggeredEvent) {
    if(selectedVariant.hasClass('selected') || selectedVariant.hasClass('out-of-stock')){ $(".size-select-area").removeClass('active-drop');triggeredEvent.stopPropagation();return;}    
    selectedVariant.addClass("selected").siblings().removeClass("selected");
    selectedVariant.closest('.size-select-area').find(".selectedValue").val(selectedVariant.attr('title'));
    selectedVariant.closest('.size-select-area').removeClass('active-drop');
    $(".tooltip-part").removeClass('active-tooltip');
}
/***
 * This function uses to close the variation drop down.
 * @param @selectedVariant selected variation picker span JQuery Element.
 * @returns
 */
function closeDropDown(variationPicker) {
	var $target = variationPicker.closest('.size-select-area');
	$target.length > 0 && $target.hasClass('active-drop') ? $target.removeClass('active-drop') : null;
}
/***
 * This function call when user open and close dropdown.
 * @param @variationPicker selected variation picker span JQuery Element and @triggeredEvent triggered Event Object.
 * @returns
 */ 
function initVariationDropDown (variationPicker, triggeredEvent) {
	if($("#lockDropdwon").val() === "true") {
		return;
	}
	var $target =  variationPicker.closest('.size-select-area');
	var hasClass =  $target.hasClass('active-drop');
    $( ".size-select-area").removeClass('active-drop');
    if(hasClass)
    {
    	$target.removeClass('active-drop');
    }
    else
    {
    	$target.addClass('active-drop');
    }
    triggeredEvent.stopPropagation();
}
var selectSize = function () {
	  // show selected values
	  $( ".selectedValue" ).each(function() {
		  var $value =  $(this).closest('.size-select-area').find('li.selected').attr('title');
		  var $dimension = $(this).closest('.size-select-area').find('li.selected').children('.size-dimension').val();
		  if($value != null && $value != undefined)
	       {
			  $(this).val($value);  
	       }
		  if($dimension != null && $dimension != undefined)
	       {
			  $(this).next('.selected-size-dimension').html($dimension);
	       }	
		   	 
	  });	
	$('.size-select-area .fake-input').unbind("click");
	$('.size-select-area .fake-input').on('click',function(e){
		initVariationDropDown($(this), e);
	});
	
	$(document).on("click", function(e) {
		// hide size/comfort tool tip on click outside
	    if ($(e.target).is(".size-select-area") === false) {
	    	if($(".size-select-area").hasClass('active-drop') && $('.no-variation').text().length > 0 && $('.no-variation').text() == 'Select Size'){
	    		$('#pdp-error').removeClass('hide');
	    		$('.fake-input').addClass('error');
	    	}
	    	$(".size-select-area").removeClass('active-drop');
	    }
	 // hide comfort tool tip on click outside of button
	  //  if ($(e.target).is(".add-to-cart-container") === false) {
	   // 	$(".add-to-cart-area").removeClass('comfort-not-select');
	  //  }
	 });
	$(document).mouseup(function(e) {
		    var hidedrop = $(".add-to-cart-container");
		    // if the target of the click isn't the container nor a descendant of the container
		    if (!hidedrop.is(e.target) && hidedrop.has(e.target).length === 0) 
		    {
		        $(".add-to-cart-area").removeClass('comfort-not-select');
		    }
	});
	$(".variation-select").off("click", "li.select-item");
	$(".variation-select").on("click", "li.select-item", function(e){
	    e.preventDefault();
	    updateDropDownContent($(this), e);
	    if(typeof $(this).data('va') !== 'undefined' && $(this).data('va') === 'option') {
	    	if (typeof (utag) != "undefined") {
				utag.link({eventCategory: 'Link - Product Detail', eventAction: 'Add a Frame', eventLabel:  $(this).text().trim()});
			}
	    }
	});
	
	//CPP-10 Select Variation Dropdown with Tabs in Mattress Firm.
	if ( SitePreferences.CURRENT_SITE === "Mattress-Firm" ) {
		// use this var to capture shift + tab to close dropdown when we get focus out from dropdown selector
		var isShiftKeyPlusTabPressed;
		$('.size-select-area .fake-input').on('keydown',function(e){
			isShiftKeyPlusTabPressed = (e.keyCode == 9 || e.which == 9) && (e.shiftKey);
			if (e.keyCode == 13 || e.which == 13) {
				isComingFromKeyboard = true;
				initVariationDropDown($(this), e);
				if ($(this).parent().hasClass("active-drop")) {
					var variantDropDown = $(this).nextAll('ul.variation-select');
					if (variantDropDown.length > 0 && variantDropDown.find('li.select-item').length > 0) {
						variantDropDown.find('li.select-item').first().focus();
					}
				}
	        }	
			if (e.keyCode === 27 || e.which === 27) {
				closeDropDown($(this));
				$(this).focus();
	    	}
		});
		
		$('.size-select-area .fake-input').on('focusout',function(e){
			if (isShiftKeyPlusTabPressed) {	
				closeDropDown($(this));
	    	}
		});
		var isShiftKeyPressed, pressedKey;
	    $(".variation-select").on("keydown", "li.select-item", function(e) {
	    	isShiftKeyPressed = e.shiftKey;
	    	pressedKey = e.keyCode || e.which;
	    	if ((e.keyCode == 9 || e.which == 9)) {
	    		var PreventDefaultRequired = ($(this).hasClass('last-variation')) || ($(this).hasClass('first-variation') && isShiftKeyPressed);
	    		!PreventDefaultRequired ? e.preventDefault() : null;
	    	} else if (e.keyCode === 40 || e.which === 40) {
	    		e.preventDefault();
	    		var nextOption = $(this).next('li');
	    		nextOption.length > 0 ? nextOption.focus() : null;
	    	} else if (e.keyCode === 38 || e.which === 38) {
	    		e.preventDefault();
	    		var prevOption = $(this).prev('li');
	    		prevOption.length > 0 ? prevOption.focus() : null;
	    	} else if (e.keyCode === 13 || e.which === 13) {
	    		isComingFromKeyboard = false;
	    		updateDropDownContent($(this),e);
	    	}
	    	else if (e.keyCode === 27 || e.which === 27) {
				closeDropDown($(this));
				$('.size-select-area .fake-input').focus();
	    	}
	    });
	    // Detect Key for focus out on last item, if only tab pressed then remove active-drop class.
	    $(".variation-select").on("focusout", "li.select-item.last-variation", function(e) {
	    	if (!isShiftKeyPressed && pressedKey == 9) {
	    		closeDropDown($(this));
	    	}
	    });
	}

	 $('#btn-minus-qty').click(function(e){
	        // Stop acting like a button
	        e.preventDefault();        
	        var currentVal=parseInt($('.pdpForm #Quantity').val());
	        if(currentVal==1)
	        	return;
	        if (!isNaN(currentVal)) {
	            // Increment
	            $('.pdpForm #Quantity').val(currentVal - 1);
	        } else {
	            // Otherwise put a 0 there
	            $('.pdpForm #Quantity').val(0);
	        }
	        if (typeof (utag) != "undefined") {
	        	utag.link({eventCategory: 'Link - Product Detail', eventAction: 'Product Detail Column - Quantity Change', eventLabel: 'subtract'});
	        }
	    });
	    
	    $('#btn-plus-qty').click(function(e){
	        // Stop acting like a button
	        e.preventDefault();        
	        var currentVal=parseInt($('.pdpForm #Quantity').val());
	        if(currentVal==20)
	        	return;
	        if (!isNaN(currentVal)) {
	            // Increment
	            $('.pdpForm #Quantity').val(currentVal + 1);
	        } else {
	            // Otherwise put a 0 there
	            $('.pdpForm #Quantity').val(0);
	        }
	        if (typeof (utag) != "undefined") {
	        	utag.link({eventCategory: 'Link - Product Detail', eventAction: 'Product Detail Column - Quantity Change', eventLabel: 'add'});
	        }
	    });
	
};
