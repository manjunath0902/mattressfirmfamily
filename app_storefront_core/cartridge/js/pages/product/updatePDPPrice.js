'use strict';

module.exports = function () {
	var util = require('../../util'),
		ajax = require('../../ajax');
	var $pdpMain = $('#pdpMain');
	
	var $options = $pdpMain.find('.product-options select');
	
	if ($options.length > 0 || $pdpMain.find('.product-options .li-select').length > 0) {
		var selectedItem = null;
		if($options.length > 0) {
			var selectedItem = $options.children().filter(':selected').first(); //$(this).children().filter(':selected').first();
		}
		else if($pdpMain.find('.product-options .li-select.selected').length > 0) {
			var selectedItem = $pdpMain.find('.product-options .li-select.selected');
		}
		if(selectedItem != null) {
			var salesPrice = $pdpMain.find('.product-add-to-cart .price-sales');
			salesPrice.text(selectedItem.data('combined'));
			
			var standardPrice = $pdpMain.find('.price-standard');
			standardPrice.text(selectedItem.data('combined-standard'));
	
			if (selectedItem.data('combined-standard') == selectedItem.data('combined')) {
				standardPrice.addClass('price-standard-hidden');
				salesPrice.removeClass('price-sales-more');
			} else {
				standardPrice.removeClass('price-standard-hidden');
				salesPrice.addClass('price-sales-more');
			}
			var params = {
					saleP: selectedItem.data('combined'),
					stdP: selectedItem.data('combined-standard')
				};
				ajax.load({
					url: util.appendParamsToUrl(Urls.getFinancingCost, params),
					type: 'text/html',
					target: $('.tooltip-price')
				});
		}
	}
		
};