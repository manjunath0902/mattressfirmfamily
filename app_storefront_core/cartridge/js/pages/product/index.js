'use strict';

var dialog = require('../../dialog'),
	productStoreInventory = require('../../storeinventory/product'),
	tooltip = require('../../tooltip'),
	util = require('../../util'),
	addToCart = require('./addToCart'),
	availability = require('./availability'),
	image = require('./image'),
	productNav = require('./productNav'),
	productSet = require('./productSet'),
	recommendations = require('./recommendations'),
	variant = require('./variant'),
	updatePDPPrice = require('./updatePDPPrice'),
	ajax = require('../../ajax');

/**
 * @description Initialize product detail page with reviews, recommendation and product navigation.
 */
function initializeDom() {
	productNav();
	recommendations();
	tooltip.init();
}

/**
 * @description Initialize event handlers on product detail page
 */
function updatePDPTabs() {
	if (util.isMobileSize()) {
		$('.tabs').find('input:radio').each(function () {
			$("<input type='checkbox' />").addClass('tab-switch').attr({
				name: this.name,
				value: this.value,
				id: this.id
			}).insertBefore(this);
		}).remove();
	}
}
/**
 * @description Select the option product and update the PDP pricing and availability. on click and keyup
 * @param optionSelector expected the selected option element
 */
function optionProductSelection(optionSelector) {
	optionSelector.addClass("selected").siblings().removeClass("selected");		
	var optionName = optionSelector.data('option');
	if(optionName != 'undefined'){			
		var optionalProductNameSpan = $('#frame-'+optionName);			
		if(optionalProductNameSpan.length>0) {
			optionalProductNameSpan.html(optionSelector.data('value').toString());
		}
		var optionalProductPriceSpan = $('#frame-price-'+optionName);
		if(optionalProductPriceSpan.length>0){
			optionalProductPriceSpan.html(optionSelector.data('option-price').toString());
		}
		if($('#'+optionName).length>0){
			$('#'+optionName).val(optionSelector.data('valueid'));
		}
		updatePDPPrice();
        if (typeof(personaliDataLayer) != 'undefined' && personaliDataLayer && personaliDataLayer[0]) {
            personaliDataLayer[0].option.price = selectedItem.data('option-price');
            personaliDataLayer[0].option.sku = selectedItem.val();
        }
        var updateATPAvailability = new CustomEvent('updateATPAvailability');
        window.dispatchEvent(updateATPAvailability);
	}
}
function initializeEvents() {
	var $pdpMain = $('#pdpMain');

	addToCart();
	availability();
	variant();
	image();
	productSet();
	productStoreInventory.init();

	// Add to Wishlist and Add to Gift Registry links behaviors
	$pdpMain.on('click', '[data-action="wishlist"], [data-action="gift-registry"]', function () {
		var data = util.getQueryStringParams($('.pdpForm').serialize());
		if (data.cartAction) {
			delete data.cartAction;
		}
		var url = util.appendParamsToUrl(this.href, data);
		this.setAttribute('href', url);
	});

	// product options
	$pdpMain.on('change', '.product-options select', function () {
		updatePDPPrice();
        if (typeof(personaliDataLayer) != 'undefined' && personaliDataLayer && personaliDataLayer[0]) {
            personaliDataLayer[0].option.price = selectedItem.data('option-price');
            personaliDataLayer[0].option.sku = selectedItem.val();
        }
        var updateATPAvailability = new CustomEvent('updateATPAvailability');
        window.dispatchEvent(updateATPAvailability);
	});
	
	// product options
	$pdpMain.on('click', '.product-options .li-select', function () {
		optionProductSelection($(this));
	});
	//CPP-10 Select Variation Dropdown with Tabs in Mattress Firm.
	if ( SitePreferences.CURRENT_SITE === "Mattress-Firm" ) {
		$pdpMain.on('keypress keyup keydown', '.product-options .li-select', function (e) {
			if (e.keyCode == 13 || e.which == 13) {
				optionProductSelection($(this));
			}
		});
	}
	$pdpMain.on('change', '.quantity select.quantity-dropdown', function () {
		var updateATPAvailability = new CustomEvent('updateATPAvailability');
		window.dispatchEvent(updateATPAvailability);
	});

	window.addEventListener('updateATPAvailability', function (e) {
		var productShippingInformation = $("#product-content").attr("data-product-shipping-information");
		var hasDisabledClass = $("#add-to-cart").hasClass("add-to-cart-disabled");
		var $pdpForm = $('.pdpForm');
		var optId = null;
		if($pdpForm.find('.product-options .product-option').length>0) {
			optId = $pdpForm.find('.product-options .product-option').first().val();
		} 
		else if($pdpMain.find('.product-options .li-select.selected').length > 0) {
			optId = $pdpMain.find('.product-options .li-select.selected').data('valueid');
		}
		// dont make Atp Ajax Call if product is chicago delivery. 
		var isChicagoProduct = $('.product-actions').data("ischicago-product")!='undefined' ? $('.product-actions').data("ischicago-product") : false;
		if (productShippingInformation && productShippingInformation == "core" && hasDisabledClass == false && isChicagoProduct == false){
			var qty = $pdpForm.find('select[name="Quantity"]').first().val();			
			var params = {
				qty: isNaN(qty) ? '1' : qty,
				productId: $pdpForm.find('input[name="pid"]').first().val(),
				optionId: optId,
				atpContext:'product'
			};
			$('.pdpForm #optionValue').val(params.optionId);
			ajax.load({
				url: util.appendParamsToUrl(Urls.getATPAvailabilityMessage, params),
				target: $('.availability-web')
			});
			
			var atpCallStatus = $("#atpCallStatus").attr("data-atpcallstatus");
			var customerZipCode = $("#product-content").attr("data-customer-zipcode");
			
			if(productShippingInformation && customerZipCode && atpCallStatus && atpCallStatus == "false") {
				utag.link({ "eventCategory" : ["ATP"], "eventLabel" : ["PDP_" + params.productId + "_" + customerZipCode], "eventAction" : ["fail"] });
			}
		} else if (hasDisabledClass == false && isChicagoProduct == true) {
			// dont made ajax call for optional product if Variant is Chicago DC Product. 
			// Also Optional Product Should be selected.
			return false;
		} else {
			/*var boxSpringSelection = $pdpForm.find('.product-options .product-option').first().val();
			if(boxSpringSelection.length > 0) {
				$('.pdpForm #optionValue').val(boxSpringSelection);
			}*/			
			if(optId.length > 0) {
				$('.pdpForm #optionValue').val(optId);
			}
		}
				
	});

	// prevent default behavior of thumbnail link and add this Button
	$pdpMain.on('click', '.thumbnail-link, .unselectable a', function (e) {
		e.preventDefault();
	});

	$('.size-chart-link a').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href')
		});
	});
	if (($('.all-product-promos').children().length > 0 || $('.single-finance-promo').children().length > 0) && $(window).width() < 1025) {
		$('.see-more-offers').css('display', 'block');
	}
	$('.see-more-offers').on('click', function () {
		$('.more-promos').fadeIn();
		$(this).css('display', 'none');
		$('.see-fewer-offers').css('display', 'block');
	});
	$('.see-fewer-offers').on('click', function () {
		$('.more-promos').hide();
		$(this).css('display', 'none');
		$('.see-more-offers').css('display', 'block');
	});
	$('.tab-label').on('keypress', function (e) {
		if (e.keyCode === 32 || e.keyCode === 13) {
			var tabID = $(this).prop('for');
			$('.tab-switch').prop('checked',false);
			$("#" + tabID).prop('checked',true);
		}
	});
}

var product = {
	initializeEvents: initializeEvents,
	init: function () {
		initializeDom();
		initializeEvents();
		updatePDPTabs();
		updatePDPPrice();
	}
};

module.exports = product;
