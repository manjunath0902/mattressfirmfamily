'use strict';
exports.init = function () {
	if ($("#home-2").children().length == 0) {
		$("#home-2").parent().addClass("no-padding");
		}
		// Moved the static script from Htmlhead.isml
		var WebFontConfig = {
			google: { families: [ 'Ubuntu:400,700:latin' ] }
		};
		(function() {
			var wf = document.createElement('script');
			wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
			wf.type = 'text/javascript';
			wf.async = 'true';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(wf, s);
		})();
			
		// HP Carousel UTAGS
  		var prodCard = $('.product-card.slick-slide');

  	    if ( prodCard.length > 0){
  	    	
  	    	prodCard.each( function(index){
	   			$(this).on( "click", function(e){		   		
	   				var index = parseInt($(this).attr('data-slick-index') ) + 1;
	   				var divText = $(this).find('div.product-name-long').text();
	   				var prodName = divText.replace(/(\r\n|\n|\r)/gm, "");
	   				
	   				if (utag && utag.link){
	   					utag.link(
		   				{ "eventCategory" : ["Best Selling Products- HP"], 
		   				  "eventLabel" : [index + ' - ' + prodName], 
		   				  "eventAction" : ["click"] 
		   				});
	   				}
	   			});
	   		});
  	    }
	};