'use strict';

var account = require('./account'),
    bonusProductsView = require('../bonus-products-view'),
    quickview = require('../quickview'),
    cartStoreInventory = require('../storeinventory/cart'),
    util = require('../util'),
    page = require('../page'),
    dialog = require('../dialog');

/**
 * @private
 * @function
 * @description Binds events to the cart page (edit item's details, bonus item's actions, coupon code entry)
 */
function initializeEvents() {
    $('#cart-table').on('click', '.item-edit-details a', function (e) {
        e.preventDefault();
        quickview.show({
            url: e.target.href,
            source: 'cart'
        });
    })
    .on('click',  '.item-details .bonusproducts a', function (e) {
        e.preventDefault();
        bonusProductsView.show(this.href);
    });
    $('#cart-items-form').on('click', '.bonus-item-actions a', function (e) {
        e.preventDefault();
        bonusProductsView.show({
        	url: this.href
        });
    });

    // override enter key for coupon code entry
    $('form input[name$="_couponCode"]').on('keydown', function (e) {
        if (e.which === 13 && $(this).val().length === 0) { return false; }
    });

    //to prevent multiple submissions of the form when removing a product from the cart
    var removeItemEvent = false;
    $('button[name$="deleteProduct"]').on('click', function (e) {
        if (removeItemEvent) {
            e.preventDefault();
        } else {
            removeItemEvent = true;
        }
    });
    if (!util.isMobileSize()) {
        $('.cart-footer-slot').appendTo('.cart-footer');
    }
    $('.quantity-dropdown').on('change', function () {
        $('#update-cart').trigger('click');
    });
}


var addPersonaliCartRescue = function (netotiate_offer_id) {
	var options = {
		url: Urls.addCartRescuePriceAdjustments,
		data: {netotiateOfferId: netotiate_offer_id},
		type: 'POST'
	};
    $.ajax(options).done(function (data) {
        if (typeof(data) !== 'string') {
            if (data.success) {
                //dialog.close();
                //page.refresh();
            } else {
                window.alert(data.message);
                return false;
            }
        } else {
            console.log(data);
            //dialog.close();
            page.refresh();
        }
    });
}

exports.init = function () {
    initializeEvents();
    if (SitePreferences.STORE_PICKUP) {
        cartStoreInventory.init();
    }
    account.initCartLogin();
    $("body").on('click', '.change-store', function (e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href'),
            title: $(e.target).attr('title'),
            options: {
                height: 600
            }
        });
    });
    $("body").on('click', '.select-store', function (e) {
        var selectedStore = $(e.target).val();
        setPreferredStore(selectedStore);
        $("div[class*='store']").removeClass('selected');
        $('.store_' + selectedStore).addClass('selected');
        $(".select-store:contains('Preferred Store')").closest('button').first().text('Select Store');
        $(e.target).text('Preferred Store');
    });

    $('body').on('submit', '#dwfrm_pickupinstore', function (e) {
        e.preventDefault();
        // serialize the form and get the post url
        var buttonName = $('#dwfrm_pickupinstore').find('.pickupinstore').attr('name');
        var options = {
            url: Urls.pickupInStore,
            data: $('#dwfrm_pickupinstore').serialize() + '&' + buttonName + '=x',
            type: 'POST'
        };
        $.ajax(options).done(function (data) {
            if (typeof(data) !== 'string') {
                if (data.success) {
                    //dialog.close();
                    //page.refresh();
                } else {
                    window.alert(data.message);
                    return false;
                }
            } else {
                $('#dialog-container').html(data);
                //dialog.close();
                //page.refresh();
            }
        });

    });
    $("body").on('click', '.continue-with-select-store', function (e) {
        var selectedStore = $('.selected').attr('data-storeid');
        var pid = $('input[name="pid"]').val();
        var format = 'ajax';
        addProductSelectStore(selectedStore, pid, format);
    });
    global.addPersonaliCartRescue = addPersonaliCartRescue;
};
