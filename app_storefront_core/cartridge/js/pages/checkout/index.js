'use strict';

var address = require('./address'),
	dialog = require('../../dialog'),
	billing = require('./billing'),
	multiship = require('./multiship'),
	shipping = require('./shipping');

/**
 * @function Initializes the page events depending on the checkout stage (shipping/billing)
 */
function IsValidZipCode(zip) {
    var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
    if (!isValid) {
        return false;
    } else {
        return true;
    }
}

exports.init = function () {
	address.init();
	if ($('.checkout-shipping').length > 0) {
		shipping.init();
	} else if ($('.checkout-multi-shipping').length > 0) {
		multiship.init();
	} else {
		billing.init();
	}
	// phone mask
	$('.checkout-shipping .phones input').keydown(function (e) {
		$(".phones.required #temp-error").remove();
		var key = e.charCode || e.keyCode || 0;
		var $phone = $(this);
		// Auto-format- do not expose the mask as the user begins to type
		if (key !== 8 && key !== 9) {
			if ($phone.val().length === 3) {
				$phone.val($phone.val() + '-');
			}
			if ($phone.val().length === 7) {
				$phone.val($phone.val() + '-');
			}
		}

		// Allow numeric (and tab, backspace, delete) keys only
		return (key == 8 ||
				key == 9 ||
				key == 46 ||
				(key >= 48 && key <= 57) ||
				(key >= 96 && key <= 105));
	})

	.bind('focus click', function () {
		var $phone = $(this);
		if ($phone.val().length === 0) {
			$phone.val('');
		} else {
			var val = $phone.val();
			$phone.val('').val(val); // Ensure cursor remains at the end
		}
	});
// phone mask end
// zipcode changed
	$('.checkout-shipping .postal input').first().keyup(function () {
		var $postalcodetmp = this.value;
	});
	$('.checkout-shipping .postal input').focusout(function () {
	}).blur(function () {
		var changedziptext = this.value;
		var topziptext = $('.header-top .dropdown span').text();
		if (topziptext == changedziptext) {
		} else {
			Showpopupzip();
		}
	});
//
	function Showpopupzip() {
		//e.preventDefault();
		dialog.open({
			url: $('.checkout-shipping .zipcodechange').attr('href'),
			options: {
				height: 200,
				title: $('.checkout-shipping .zipcodechange').attr('title')
			}
		});
	}
//zipcode changed
	function IsValidZipCode(zip) {
		var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
		if (!isValid) {
			if ($(".postal").parent().find(".error").length < 1) {
				//$(".postal .field-wrapper").append("<span class='error'>" + Resources.INVALID_ZIP + "</span>");
				$(".postal").parent().append("<span class='error'>" + Resources.INVALID_ZIP + "</span>");
				$(".postal").addClass("error");
				$("#edit-address-form").find('button').attr("disabled", "disabled");
			}
			return false;
		} else {
			if ($(".postal").parent().find(".error").length > 0) {
				$(".postal").parent().find("span.error").remove();
				$(".postal").removeClass("error");
				$("#edit-address-form").find('button').removeAttr("disabled", "disabled");
			}
			return true;
		}
	}
//
// Haulaway Service validation
	$("#dwfrm_singleshipping_pickups").keypress(function (e) {
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			$("#errmsg").show();
				return false;
		} else {
			$("#errmsg").hide();
		}
	});
// Haulaway Service vlidation end
// email address check.
	function validateEmail(email) {
		var $continue = $('.form-row-button button');
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		if (!emailReg.test(email)) {
			$continue.attr('disabled', 'disabled');
			if ($(".emailaddr .errorEmailValidation").length < 1) {
				$(".emailaddr .field-wrapper").append("<span class='error errorEmailValidation'>" + Resources.VALIDATE_EMAIL + "</span>");
			}
			return false;
		} else {
			$continue.removeAttr('disabled');
			if ($(".errorEmailValidation").length > 0) {
				$(".emailaddr span.errorEmailValidation").remove();
			}
			return true;
		}
	}
	$('.emailaddr input').first().on('keyup change', function () {
		var $email = this.value;
		validateEmail($email);
	});
// email address check end.
// postal code check. -- dwfrm_singleshipping_shippingAddress_addressFields_postal
	function IsValidZipCode(zip) {
		var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);

		if (!isValid && (zip != "")) {
			if ($(".postal .errorZipValidation").length < 1) {
				$(".postal .field-wrapper").append("<span class='error errorZipValidation'>" + Resources.INVALID_ZIP + "</span>");
			}
			if ($(".postal .errorHawaiiValidation").length > 0) {
				$(".postal span.errorHawaiiValidation").remove();
				$(".postal").removeClass("errorHawaiiValidation");
			}
			return false;
		} else {
			if ($(".postal .errorZipValidation").length > 0) {
				$(".postal span.errorZipValidation").remove();
			}
			return true;
		}
	}
	$('.postal input').first().keyup(function () {
		var $postalcodetmp = this.value;
		IsValidZipCode($postalcodetmp);
	});
//postal code check
	if (SitePreferences.currentSiteId != 'Mattress-Firm') {
		$('.checkout-mini-cart').mCustomScrollbar();
	}
	//if on the order review page and there are products that are not available diable the submit order button
	if ($('.order-summary-footer').length > 0) {
		if ($('.notavailable').length > 0) {
			$('.order-summary-footer .submit-order .button-fancy-large').attr('disabled', 'disabled');
		}

		$('#ihaveread').change(function () {
			if ($(this).is(":checked")) {
				$('#orderSubmit').removeClass('disabled');
				$('.error-display').addClass('hidden');
			} else {
				$('#orderSubmit').addClass('disabled');
			}
		});
		$("form.submit-order").on('click', '#orderSubmit', function (e) {
			if ($(this).hasClass('disabled')) {
				$('.error-display').removeClass('hidden');
				e.preventDefault();
			}
		});
	}
};
