<iscontent type="text/html" charset="UTF-8" compact="true"/>
<iscomment>This template renders the breadcrumb navigation for product search results</iscomment>

<iscomment>get the current paging model for convenient reuse</iscomment>

<isscript>
	var StringHelpers = require('~/cartridge/scripts/util/StringHelpers');
	var URLUtils = require('dw/web/URLUtils');
	var Resource = require('dw/web/Resource');
	var ArrayList = require('dw/util/ArrayList');
	var ProductSearchModel = require('dw/catalog/ProductSearchModel');

	var pagingModel = pdict.ProductPagingModel;
	var psr = pdict.ProductSearchResult;
	var searchPhrase = psr.getSearchPhrase();
	var suggestedSearchPhrase = psr.getSearchPhraseSuggestions().suggestedPhrases.next().phrase;
	var searchPhraseURL = URLUtils.url('Search-Show', 'q', searchPhrase);
	var suggestedSearchPhraseURL = suggestedSearchPhrase ? URLUtils.url('Search-Show', 'q', suggestedSearchPhrase) : '';
	var resultsText = Resource.msg('searchbreadcrumbs.resultstext', 'search', null);
	if (pagingModel) {
		searchPhraseURL = pagingModel.appendPaging(searchPhraseURL, 0);
	}
	if (suggestedSearchPhrase) {
		resultsText = Resource.msg('searchbreadcrumbs.searchphrase', 'search', null);
	}

	var category = psr.getCategory();
	var categories = new ArrayList();
	var cat = category;
	while (cat.parent) {
		if (cat.online) {
			categories.addAt(0, cat);
		}
		cat = cat.parent;
	}
</isscript>
<isset name="curentZipRefinement" value="${session.custom.customerZip}" scope="page" />
<isset name="customerZoneHash" value="${!empty(pdict.CurrentSession.custom.customerZone) ? pdict.CurrentSession.custom.customerZone.replace(',', '|', 'g') : ''}" scope="page" />
<isset name="customerZoneArray" value="${customerZoneHash.split('|')}" scope="page" />
<isset name="customerZoneSelected" value="false" scope="page" />
<isloop items="${customerZoneArray}" var="customerZone">
	<isif condition="${pdict.ProductSearchResult.isRefinedByAttributeValue('zone_code', customerZone)}">
		<isset name="customerZoneSelected" value="true" scope="page" />
		<isbreak>
	</isif>
</isloop>
<div class="breadcrumb">
	<isif condition="${psr && searchPhrase}">
		<div class="breadcrumb-top">
			<a href="${URLUtils.httpHome()}" title="${Resource.msg('global.home','locale',null)}" class="breadcrumb-element">${Resource.msg('global.home','locale',null)} &nbsp; > &nbsp;</a>
			<span class="breadcrumb-result-text"> ${resultsText} &quot;${searchPhrase}&quot;</span>
		</div>
		<div class="clearfix"></div>
		<isif condition="${psr.count>0}">
			<div class="search-result-no-hits-top has-items">
				<p>${resultsText}</p>
				 <a href="${searchPhraseURL}" title="${searchPhrase}" class="search-results-link">
				 	<h4>&quot;${searchPhrase}&quot;</h4>
				 </a>
				<isif condition="${suggestedSearchPhrase}">
					<span >${Resource.msg('searchbreadcrumbs.showresults', 'search', null)} &quot;<a href="${suggestedSearchPhraseURL}" title="${suggestedSearchPhrase}" class="no-hits-search-term-suggest">${suggestedSearchPhrase}</a>&quot;</span>
				</isif>
			</div>
		<iselse/>
			<div class="search-result-no-hits-top">
				<h4>${Resource.msg('search.resultsfor','search',null)}<span> "<isprint value="${pdict.ProductSearchResult.searchPhrase}"/>"</span></h4>
			</div>
		</isif>
	</isif>

	<isif condition="${psr && category}">
		<isactivedatacontext category="${category}"/>
		<isif condition="${!searchPhrase}">
			<a href="${URLUtils.httpHome()}" title="${Resource.msg('global.home','locale',null)}" class="breadcrumb-element">${Resource.msg('global.home','locale',null)}</a>
		</isif>
		<isloop items="${categories}" var="cat" status="loopstate">
			<isif condition="${loopstate.first && !psr.categorySearch}">
				<span class="breadcrumb-result-text">${Resource.msg('searchbreadcrumbs.in','search',null)}</span>
			</isif>
			<isif condition="${psr.categorySearch}">
				<isset name="catLinkUrl" value="${ProductSearchModel.urlForCategory('Search-Show', cat.ID)}" scope="page"/>
			<iselse/>
				<isset name="catLinkUrl" value="${ProductSearchModel.urlForCategory(URLUtils.url('Search-Show','q', psr.searchPhrase), cat.ID)}" scope="page"/>
			</isif>
			<a class="breadcrumb-element" href="${catLinkUrl}" title="${Resource.msg('global.breadcrumbs.label.prefix','locale',null)} ${cat.displayName}">${cat.displayName}</a>
		</isloop>

	</isif>
	<iscomment>SORTING INFO </iscomment>
	<isif condition="${!empty(psr) && !empty(psr.sortingRule) && (psr.category != null)}">
		<isif condition="${psr.sortingRule.ID}">
			<span class="breadcrumb-refined-by">${Resource.msg('searchbreadcrumbs.sortedby', 'search', null)}</span>
		</isif>
		<isloop items="${dw.catalog.CatalogMgr.getSortingOptions()}" var="sortingOption">
			<isif condition="${sortingOption.getID() == psr.sortingRule.ID}">
				<span class="breadcrumb-refinement">${sortingOption.getDisplayName()}</span>
			</isif>
		</isloop>
	</isif>
 	<iscomment>REFINEMENT INFO </iscomment>
 	<isif condition="${psr && ((psr.refinedByPrice && psr.priceMax) || psr.refinedByAttribute)}">
			<isset name="refinementCount" value="${0}" scope="page">
			<isset name="nearMeRef" value="${pdict.CurrentSession.custom.nearMe}" scope="page">
			<isif condition="${psr.refinedByPrice && psr.priceMax}">
				<isset name="refinementCount" value="${refinementCount + 1}" scope="page"/>
			</isif>
			<isloop items="${psr.refinements.refinementDefinitions}" var="definition" status="attributes">
				<isif condition="${psr.isRefinedByAttribute(definition.attributeID)}">
					<isset name="refinementCount" value="${refinementCount + 1}" scope="page"/>
				</isif>
				
			</isloop>
			
		<div class="breadcrumb-refinement-mobile <isif condition="${refinementCount == 1 && customerZoneSelected == 'true'}">breadcrumb-refinement-mobile-hide</isif>">
		<isif condition="${(psr.refinements.priceRefinementDefinition && psr.refinedByPrice) || (psr.refinements.refinementDefinitions.size() > 0)}">
			<span class="breadcrumb-refined-by">${Resource.msg('searchbreadcrumbs.refinedby', 'search', null)}</span>
		</isif>

		<iscomment>Price</iscomment>
		<isif condition="${psr.refinements.priceRefinementDefinition && psr.refinedByPrice}">
			<isscript>
				// for price refinements, we use the display value of the price refinement definition's value
				var breadcrumbLabel = null

				var refinements = psr.refinements;
				var prdValues = refinements.getRefinementValues(refinements.priceRefinementDefinition);
				if (prdValues.iterator().hasNext()) {
					breadcrumbLabel = prdValues.iterator().next().getDisplayValue();
				}
			</isscript>
			<span class="breadcrumb-refinement" data-divider="${Resource.msg('searchbreadcrumbs.attributedivider','search',null)}">
				<span class="breadcrumb-refinement-name">
					<isprint value="${psr.refinements.priceRefinementDefinition.displayName}"/>:
				</span>

				<span class="breadcrumb-refinement-value">
					<isif condition="${!empty(breadcrumbLabel)}">
						<isprint value="${breadcrumbLabel}"/>
					<iselse/>
						<isprint value="${psr.priceMin}"/>
						${Resource.msg('searchbreadcrumbs.to','search',null)}
						<isprint value="${psr.priceMax}"/>
					</isif>

					<isif condition="${empty(pagingModel)}">
						<isset name="breadcrumbHref" value="${psr.urlRelaxPrice('Search-Show')}" scope="page" />
					<iselse/>
						<isset name="breadcrumbHref" value="${pagingModel.appendPaging(psr.urlRelaxPrice('Search-Show'), 0)}" scope="page" />
					</isif>
					<a class="breadcrumb-relax" href="${StringHelpers.unsanitizeOR(breadcrumbHref)}" title="${Resource.msg('global.remove', 'locale', null)}">x</a><span class="comma">,</span>
				</span>
			</span>

		</isif>

		<iscomment>attributes</iscomment>
		<isset name="hasAttributes" value="${false}" scope="page" />
		<isloop items="${psr.refinements.refinementDefinitions}" var="definition" status="attributes">
			<isif condition="${definition.attributeID.equals('zone_code')}">
					<isnext>
			</isif>
			<isif condition="${definition.isAttributeRefinement() && psr.isRefinedByAttribute(definition.attributeID)}">
				<span class="breadcrumb-refinement" data-divider="${Resource.msg('searchbreadcrumbs.attributedivider','search',null)}">
					<span class="breadcrumb-refinement-name">
						<isprint value="${definition.displayName}"/>:
					</span>
					<isset name="hasAttributes" value="${true}" scope="page" />
					<isloop items="${psr.refinements.getRefinementValues(definition)}" var="value" status="values">
						<isif condition="${psr.isRefinedByAttributeValue(definition.attributeID, value.value)}">
							<span class="breadcrumb-refinement-value">
									${Resource.msg('refinement.' + definition.attributeID + '.' + value.displayValue, 'locale', value.displayValue)}
								<isif condition="${empty(pagingModel)}">
									<isset name="breadcrumbHref" value="${psr.urlRelaxAttributeValue('Search-Show', definition.attributeID, value.value)}" scope="page" />
								<iselse/>
									<isset name="breadcrumbHref" value="${pagingModel.appendPaging(psr.urlRelaxAttributeValue('Search-Show', definition.attributeID, value.value), 0)}" scope="page" />
								</isif>
								<a class="breadcrumb-relax" href="${StringHelpers.unsanitizeOR(breadcrumbHref)}" title="${Resource.msg('global.remove', 'locale', null)}">x</a>
								<span class="comma">,</span>
							</span>
						</isif>
					</isloop>
				</span>
			</isif>
		</isloop>
		<isif condition=${!empty(searchPhrase)}>
			<isset name="removeAllHref" value="${URLUtils.url('Search-Show','q', psr.searchPhrase)}" scope="page" />
		<iselse>
			<isset name="removeAllHref" value="${catLinkUrl}" scope="page" />
		</isif>
		<a href="${removeAllHref}" title="${Resource.msg('searchrefinebar.clearall','search',null)}" class="breadcrumb-refinement-clear-all">${Resource.msg('searchrefinebar.clearall','search',null)}</a>
		<div class="clearfix"></div>
		</div>
	</isif>
	
</div><!-- END: breadcrumb -->
