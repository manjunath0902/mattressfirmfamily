<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isdecorate template="checkout/pt_checkout_place_order"/>
<isinclude template="util/modules"/>
<isinclude template="responsiveslots/util/module" />

<iscomment>
	This template visualizes the last step of the checkout, the order summary
	page prior to the actual order placing.
	It displays the complete content of the cart including product line items,
	bonus products, redeemed coupons and gift certificate line items.
</iscomment>

<isreportcheckout checkoutstep="${5}" checkoutname="${'OrderSummary'}"/>
	<isif condition="${!pdict.CurrentForms.multishipping.entered.value}">
		<ischeckoutprogressindicator step="4" multishipping="false" rendershipping="${pdict.Basket.productLineItems.size() == 0 ? 'false' : 'true'}"/>
	<iselse/>
		<ischeckoutprogressindicator step="4" multishipping="true" rendershipping="${pdict.Basket.productLineItems.size() == 0 ? 'false' : 'true'}"/>
	</isif>

	<div class="summary-message">${Resource.msg('summary.message','checkout',null)}</div>
	<isif condition="${pdict.PlaceOrderError != null}">
		<div class="error-form">
			<div class="error-form-inside">
				${Resource.msg(pdict.PlaceOrderError.code,'checkout',null)}
			</div>
		</div>	
	</isif>
	<isinclude template="checkout/minisummary_place_order"/>

	
	<h3 class="section-header shipping-delivery">
		${Resource.msg('minishipments.shippingaddress','checkout',null)}
	</h3>
		
	<isloop items="${pdict.Basket.shipments}" var="shipment" status="shipmentloopstate">
		<isif condition="${shipment.productLineItems.size() > 0 || shipment.giftCertificateLineItems.size() > 0}">
			<div class="summary-table-wrapper">
			<isif condition="${shipment.custom.shipmentType == 'instore'}"/>
				<h3 class="shipping-name">
					${Resource.msg('minishipments.instorepickup','checkout',null)}
				</h3>
				<iselse />
				<h3 class="shipping-name">
					${Resource.msg('minishipments.shippingaddress','checkout',null)}
				</h3>
			</isif>
			
			<table class="item-list" id="cart-table" cellspacing="0">
				<thead>
					<tr>
						<th class="section-header"  colspan="2">${Resource.msg('global.product','locale',null)}</th>
						<th class="section-header">${Resource.msg('global.price','locale',null)}</th>
						<th class="section-header">${Resource.msg('global.qty','locale',null)}</th>
						<th class="section-header header-total-price">${Resource.msg('global.totalprice','locale',null)}</th>
					</tr>
				</thead>
		</isif>

				<isif condition="${shipment.productLineItems.size() > 0 || shipment.giftCertificateLineItems.size() > 0}">
					<isloop items="${shipment.productLineItems}" var="productLineItem" status="pliloopstate">

						<tr class="cart-row <isif condition="${pliloopstate.first}"> first <iselseif condition="${pliloopstate.last}"> last</isif>">

							<td class="item-image">
								<isif condition="${'AmplienceHost' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.AmplienceHost!=''}">
									<isset name="AmplienceHost" value="${dw.system.Site.current.preferences.custom.AmplienceHost}" scope="page" />
								</isif>
								<isif condition="${'AmplienceClientId' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.AmplienceClientId!=''}">
									<isset name="AmplienceId" value="${dw.system.Site.current.preferences.custom.AmplienceClientId}" scope="page" />
								</isif>
								<isset name="imgUrl" value="${pdict.CurrentRequest.httpProtocol+'://'+AmplienceHost+'/i/'+AmplienceId+'/'+ productLineItem.product.custom.external_id}" scope="page"/>
								<isif condition="${imgUrl}">
									<img src="${imgUrl}" alt="${productLineItem.product.productName}" title="${productLineItem.product.productName}"/>
								<iselse/>
									<img src="${URLUtils.staticURL('/images/noimagesmall.png')}" alt="${productLineItem.productName}" title="${productLineItem.productName}"/>
								</isif>
							</td>

							<td class="item-details">
								<iscomment>Display product line and product using module</iscomment>
								<isdisplayliproduct p_productli="${productLineItem}" p_editable="${false}"/>
								
 							<div class="cart-availability-container">
							 	<isif condition="${!productLineItem.bonusProductLineItem || productLineItem.getBonusDiscountLineItem() != null}">
									 <isset name="product" value="${productLineItem.product}" scope="page" />
									 <isset name="quantity" value="${pdict.Basket.getAllProductQuantities().get(productLineItem.product).value}" scope="page" />
									 <isinclude template="checkout/cart/cartavailability" />
								 </isif>
							 	<a href="${URLUtils.url('Page-Show', 'cid', 'deliver-options-home')}" class="details" title="${Resource.msg('global.details','locale',null)}">(${Resource.msg('global.details','locale',null)})</a>
						 	</div>
							</td>

							<td class="item-price">
								<p class="mobile-show block-price-text">${Resource.msg('global.price','locale',null)}:</p>
											<isif condition="${productLineItem.product != null}">

												<iscomment>
													StandardPrice: quantity-one unit price from the configured list price
													book. SalesPrice: product line item base price. If these are
													different, then we display crossed-out StandardPrice and also
													SalesPrice.
												</iscomment>


												<iscomment>Get the price model for this	product.</iscomment>
												<isset name="PriceModel" value="${productLineItem.product.getPriceModel()}" scope="page" />


												<iscomment>Get StandardPrice from list price book.</iscomment>
												<isinclude template="product/components/standardprice" />


												<iscomment>Get SalesPrice from line item itself.</iscomment>
												<isset name="SalesPrice" value="${productLineItem.basePrice}" scope="page" />
												<isif condition="${StandardPrice.available && StandardPrice > SalesPrice}">
													<iscomment>StandardPrice and SalesPrice are different, show standard</iscomment>
													<div class="price-promotion">
														<span class="price-sales"><isprint value="${SalesPrice}" /></span>
														<span class="price-standard"><isprint value="${StandardPrice}" /></span>	
													</div>
												<iselse/>
													<isif condition="${productLineItem.priceAdjustments.length == 0}">
														<span class="price-sales"><isprint value="${SalesPrice}" /></span>
													</isif>
												</isif>
												<isif condition="${productLineItem.priceAdjustments.length > 0}">
													<span class="price-adjusted-total">
														<span><isprint value="${productLineItem.getAdjustedPrice()}" /></span>
													</span>
													<span class="price-unadjusted">
														<span><isprint value="${productLineItem.getPrice()}" /> </span>
													</span>
												<iselse/>

													<iscomment>Display non-adjusted item total.</iscomment>
													<span class="price-total">
														<isprint value="${productLineItem.getAdjustedPrice()}" />
													</span>

												</isif>
												

											</isif>
							</td>

							<td class="item-quantity">
								<p class="mobile-show block-qty-text">${Resource.msg('global.qty','locale',null)}:</p>
								<isprint value="${productLineItem.quantity}" />
							</td>

							<td class="item-total">
								<p class="mobile-show block-total-price-text">${Resource.msg('global.totalprice','locale',null)}:</p>
								<isif condition="${productLineItem.bonusProductLineItem}">
									<div class="bonus-item">
										<isprint value="${bonusProductPriceValue}" />
									</div>
								<iselse/>
									<iscomment>Otherwise, render price using call to adjusted price </iscomment>
									<isprint value="${productLineItem.adjustedPrice}" />
								</isif>
								<isif condition="${productLineItem.optionProductLineItems.size() > 0}">
									<isloop items="${productLineItem.optionProductLineItems}" var="optionLI">
										<isif condition="${optionLI.price > 0}">
											<p>+ <isprint value="${optionLI.adjustedPrice}"/></p>
										</isif>
									</isloop>
								</isif>
							</td>

						</tr>

					</isloop>

					<isloop items="${shipment.giftCertificateLineItems}" var="giftCertificateLineItem" status="gcliloopstate">

						<tr  class="cart-row <isif condition="${gcliloopstate.first}"> first <iselseif condition="${gcliloopstate.last}"> last</isif>">

							<td class="item-image">
								<img src="${URLUtils.staticURL('/images/gift_cert.gif')}" alt="<isprint value="${giftCertificateLineItem.lineItemText}"/>" />
							</td>

							<td class="item-details">
								<div class="gift-certificate-to">
									<span class="label">${Resource.msg('global.to','locale',null)}:</span>
									<span class="value">
										<isprint value="${giftCertificateLineItem.recipientName}"/>
										(<isprint value="${giftCertificateLineItem.recipientEmail}"/>)
									</span>
								</div>
								<div class="gift-certificate-from">
									<span class="label">${Resource.msg('global.from','locale',null)}:</span>
									<span class="value"><isprint value="${giftCertificateLineItem.senderName}"/></span>
								</div>
							</td>

							<td class="item-quantity" colspan="2">1</td>

							<td  class="item-total">
								<isprint value="${giftCertificateLineItem.price}"/>
							</td>

						</tr>

					</isloop>

				</isif>
	<isif condition="${shipment.productLineItems.size() > 0 || shipment.giftCertificateLineItems.size() > 0}">
		</table>

		<div class="item-shipping-address">
			<isif condition="${shipment.custom.shipmentType == 'instore'}"/>
				<div class="shipping-name">
					${Resource.msg('minishipments.name.instorepickup','checkout',null)}
				</div>
			<iselseif condition="${shipment.shippingAddress != null && pdict.Basket.productLineItems.size() > 0}"/>
				<div class="shipping-name">
					<span class="mobile-hide">${Resource.msg('minishipments.name.shippingaddress','checkout',null)}</span>
					<span class="mobile-show">${Resource.msg('minishipments.name.shippingaddress.simple','checkout',null)}</span>
				</div>
			</isif>

			<isif condition="${shipment.shippingAddress != null}"/>
				<div class="address-details">
					<isset name="address" value="${shipment.shippingAddress}" scope="page" />
					<div><isprint value="${address.firstName}"/> <isprint value="${address.lastName}"/></div>
					<div><isprint value="${address.address1}"/></div>
					<isif condition="${!empty(address.address2)}">
							<div><isprint value="${address.address2}"/></div>
					</isif>
					<div><isprint value="${address.city}"/>, <isprint value="${address.stateCode}"/> <isprint value="${address.postalCode}"/></div>
					<div><isprint value="${Resource.msg("country."+address.countryCode, "forms", null)}"/></div>
					<isif condition="${shipment.custom.shipmentType == 'instore'}">
						<div><isprint value="${address.phone}"/></div>
					</isif>
					<a href="${URLUtils.https('COShipping-Start')}">${Resource.msg('global.edit','locale',null)}</a>
				</div>
			</isif>
		</div>
		
	</isif>
	</isloop>

	<isif condition="${pdict.Basket.couponLineItems.size() > 0 || pdict.Basket.priceAdjustments.size() > 0}">
		<table class="item-list" id="cart-table-additional" cellspacing="0">
			
			<iscomment>RENDER COUPON/ORDER DISCOUNTS</iscomment>
			<isloop items="${pdict.Basket.couponLineItems}" var="couponLineItem" status="cliloopstate">

				<isif condition="${couponLineItem.valid}">

					<tr class="cart-row rowcoupons <isif condition="${cliloopstate.first}"> first <iselseif condition="${cliloopstate.last}"> last</isif>">

						<td  class="item-details">
							<div class="name">${Resource.msg('summary.coupon','checkout',null)}</div>
							<div class="cart-coupon">
								<span class="label">${Resource.msg('summary.couponnumber','checkout',null)}</span>
								<span class="value"><isprint value="${couponLineItem.couponCode}"/></span>
							</div>
							<isloop items="${couponLineItem.priceAdjustments}" var="Promo" status="loopstate">
								<div class="discount clearfix <isif condition="${loopstate.first}"> first <iselseif condition="${loopstate.last}"> last</isif>">
									<span class="label"><isprint value="${Promo.lineItemText}"/></span>
									<span class="value">(<isprint value="${Promo.price}"/>)</span>
								</div>
							</isloop>
						</td>
						<td class="item-total">
							<isif condition="${couponLineItem.applied}">
								<span class="coupon-applied">${Resource.msg('summary.applied','checkout',null)}</span>
							<iselse/>
								<span class="coupon-not-applied">${Resource.msg('summary.notapplied','checkout',null)}</span>
							</isif>
							<div class="item-edit-details">
								<a href="${URLUtils.url('Cart-Show')}">${Resource.msg('global.editdetails','locale',null)}</a>
							</div>
						</td>

					</tr>

				</isif>

			</isloop>

			<isloop items="${pdict.Basket.priceAdjustments}" var="priceAdjustment" status="cliloopstate">

				<tr>
					<td colspan="2" class="cart-promo-bottom">
						<div>
							<span class="label">${Resource.msg('summary.orderdiscount','checkout',null)}</span>
							<span class="value"><isprint value="${priceAdjustment.lineItemText}"/></span>
						</div>
					</td>
				</tr>

			</isloop>

		
		</table>
	</isif>
	</div>
		<div class="order-summary-footer">
			<div class="place-order-totals-wrapper">
				<div class="place-order-totals">
					<isordertotals p_lineitemctnr="${pdict.Basket}" p_showshipmentinfo="${false}" p_shipmenteditable="${false}" p_totallabel="${Resource.msg('summary.ordertotal','checkout',null)}"/>
				</div>

				<form action="${URLUtils.https('COSummary-Submit')}" method="post" class="submit-order">
					<fieldset>
						<div class="form-row">
							<a class="back-to-cart" href="${URLUtils.url('Cart-Show')}">
								<isprint value="${Resource.msg('summary.editcart','checkout',null)}" encoding="off" />
							</a>

							<div id="cexcg" class="checkbox-highlight">
								<iscomment> TODO: add this to forms and make continue button disabled until checked </iscomment>
								<div class="form-row label-inline">
									<div class="field-wrapper">
										<input type="checkbox" id="ihaveread" required class="uniform"/>
									</div>
									<label>${Resource.msg('summary.ihavereadthe','checkout',null)}
										<span class="summary-terms-and-privacy">
											<iscontentasset aid="summary-terms-and-privacy" />
										</span>
									</label>
								</div>
								<div class="error error-display hidden">${Resource.msg('summary.pleaseconfirm','checkout',null)}</div>
							</div>

							<button id="orderSubmit" class="disabled button-fancy-large" type="submit" name="submit" value="${Resource.msg('global.submitorder','locale',null)}">
								${Resource.msg('global.submitorder','locale',null)}
							</button>
						</div>
					</fieldset>
				</form>
			</div>

			<div class="placeorder-slot-wrapper">
				<isresponsiveslot slotid="placeorder-slot" context="global" />
			</div>
		</div>

</isdecorate>