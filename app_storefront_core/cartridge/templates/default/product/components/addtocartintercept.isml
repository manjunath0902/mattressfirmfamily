<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isinclude template="responsiveslots/util/module" />

<div class="add-to-cart-intercept-wrapper">
	<div class="clearfix">
		<div class="justaddedproduct-wrapper">
			<div class="justaddedproduct clearfix">
				<div class="product-image">
					<iscomment>Render the thumbnail</iscomment>
					<iscomment>If image couldn't be determined, display a "no image" medium.</iscomment>
					<isscript>
						var sash : String = '';
						if(pdict.Product != null){
							sash = pdict.Product.custom.plp_sash;
						}
						importScript( "int_amplience:common/AmplienceSash.ds");
						sash = getProductSash(sash, "&");
					</isscript>
					<isif condition="${'AmplienceHost' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.AmplienceHost!=''}">
						<isset name="AmplienceHost" value="${dw.system.Site.current.preferences.custom.AmplienceHost}" scope="page" />
					</isif>
					<isif condition="${'AmplienceClientId' in dw.system.Site.current.preferences.custom && dw.system.Site.current.preferences.custom.AmplienceClientId!=''}">
						<isset name="AmplienceId" value="${dw.system.Site.current.preferences.custom.AmplienceClientId}" scope="page" />
					</isif>
					<isset name="thumbnailUrl" value="${pdict.CurrentRequest.httpProtocol+'://'+AmplienceHost+'/i/'+AmplienceId+'/'+pdict.LastItem.product.custom.external_id+'?$ma_product_listing_page$'+sash}" scope="page"/>
					<isset name="imageAlt" value="${pdict.LastItem.product.name}" scope="page"/>
					<isset name="imageTitle" value="${pdict.LastItem.product.name}" scope="page"/>
					<isset name="thumbnailAlt" value="" scope="page" />
					<isset name="thumbnailTitle" value="" scope="page" />

					<img src="${thumbnailUrl}" alt="${pdict.LastItem.product.name}" title="${pdict.LastItem.product.name}"/>
				</div>

				<div class="product-info">
					<div class="product-name">
						<isprint value="${pdict.LastItem.productName}"/>
					</div>

					<div class="product-attributes">
						<isdisplayvariationvalues product="${pdict.LastItem.product}"/>
					</div>
					
					<div class="product-options">
						<isif condition="${pdict.LastItem.optionProductLineItems.size() > 0}">
							<isloop items="${pdict.LastItem.optionProductLineItems}" var="optionLI">
								<span class="value"><isprint value="${optionLI.lineItemText}" /></span>
							</isloop>
						</isif>
					</div>
					
					<div class="product-pricing">
						<span class="label">${Resource.msg('global.qty','locale',null)}:</span>
						<span class="value"><isprint value="${pdict.LastItemAddedQty}"/></span>
		
						<isset name="Product" value="${pdict.LastItem.product}" scope="pdict"/>
						<isinclude template="product/components/setpricing"/>

						<isif condition="${pdict.LastItem.bonusProductLineItem}">
							<isset name="bonusProductPrice" value="${pdict.LastItem.getAdjustedPrice()}" scope="page"/>
							<isinclude template="checkout/components/displaybonusproductprice" />
							<isprint value="${bonusProductPriceValue}" />
						<iselse/>
							<isset name="productTotal" value="${pdict.LastItem.getAdjustedPrice()}" scope="page"/>
                            <isset name="standardTotal" value="${StandardPrice.multiply(new Number(pdict.LastItem.getQuantityValue()))}" scope="page" />
							<isif condition="${Number(productTotal.decimalValue) !== Number(standardTotal.decimalValue)}">
								<isset name="saleClass" value="Adjusted-Price" scope="page" />
							</isif>
							<isif condition="${pdict.LastItem.optionProductLineItems.size() > 0}">
								<isloop items="${pdict.LastItem.optionProductLineItems}" var="optionLI">
									<isif condition="${optionLI.adjustedPrice}">
										<isset name="productTotal" value="${productTotal.add(optionLI.adjustedPrice)}" scope="page"/>
									</isif>
									<isif condition="${saleClass && optionLI.price}">
										<isset name="standardTotal" value="${standardTotal.add(optionLI.price)}" scope="page"/>
									</isif>
								</isloop>
							</isif>
						</isif>
						
						<isif condition="${saleClass}">
							<span class="product-price standard-price"><isprint value="${standardTotal.divide(pdict.LastItem.getQuantityValue()).multiply(new Number(pdict.LastItemAddedQty))}"/></span>
						<iselse>
							<isif condition="${session.custom.showStandardPriceIntercept}">
			          			<span class="price-standard"><isif
			                    condition="${session.custom.standardPriceIntercept.valueOrNull != null && session.custom.standardPriceIntercept.valueOrNull > 0}"><isprint
			                    value="${session.custom.standardPriceIntercept}"/><iselse/>${Resource.msg('product.pricing.noprice','product',null)}</isif></span>
		        			</isif>
						</isif>
						<span class="product-price ${saleClass}"><isprint value="${productTotal.divide(pdict.LastItem.getQuantityValue()).multiply(new Number(pdict.LastItemAddedQty))}"/></span>
					</div>
					
					<isif condition="${pdict.BonusDiscountLineItem!=null && pdict.BonusDiscountLineItem.promotion!=null}">
						<div class="bonus-discount-container">
							 <isif condition="${dw.system.Site.getCurrent().ID == 'Mattress-Firm'}">
								 <div class="bonus-product-alert">
									<p>${Resource.msg('product.bonusproductalertmessageline1','product',null)}</p>
									<p>${Resource.msg('product.bonusproductalertmessageline2','product',null)}</p>
								</div>
							<iselse/>
								<div class="bonus-product-alert">
									<isprint value="${Resource.msg('product.bonusproductalert','product',null)}"/>
								</div>
							</isif>
							<div class="promo-details-block">
								<isprint value="${empty(pdict.BonusDiscountLineItem.promotion.details) ? '' : pdict.BonusDiscountLineItem.promotion.details.markup}"/>
							</div>
						</div>
					</isif>
				</div>
			</div>

			<div class="add-to-cart-intercept-slot">
				<isresponsiveslot slotid="add-to-cart-intercept" context="global" />
			</div>
		</div>

		<isinclude template="product/components/recommendations-intercept"/>
	</div>

	<div class="button-wrapper clearfix">
		<a href="${URLUtils.https('Cart-Show')}" title="${Resource.msg('minicart.viewcart.label','checkout',null)}" class="button">${Resource.msg('minicart.viewcart','checkout',null)}</a>
		<a href="#" class="continue">${Resource.msg('global.continueshopping','locale',null)}</a>
	</div>
</div>
