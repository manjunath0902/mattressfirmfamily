<isif condition="${pdict.resetAttributes}">
	<isscript>
		var ProductUtils = require('~/cartridge/scripts/product/ProductUtils.js');
		var url = dw.web.URLUtils.url('Product-Variation', 'pid', pdict.Product.ID, 'format', 'ajax');
		var qs = ProductUtils.getQueryString(pdict.CurrentHttpParameterMap, ["source", "uuid", "Quantity"]);
		if (qs && qs.length>0) { url+="&"+qs; }
	</isscript>
	<isinclude url="${url}"/>
<iselse/>
	<isset name="isQuickView" value="${pdict.CurrentHttpParameterMap.source.stringValue == 'quickview' || pdict.CurrentHttpParameterMap.source.stringValue == 'cart' || pdict.CurrentHttpParameterMap.source.stringValue == 'giftregistry' || pdict.CurrentHttpParameterMap.source.stringValue == 'wishlist'}" scope="page"/>
	<isscript>
		if (pdict.CurrentVariationModel && !empty(pdict.CurrentVariationModel.selectedVariant)) {
			pdict.Product = pdict.CurrentVariationModel.selectedVariant;
		}
		var masterId = pdict.Product.isVariant() ? pdict.Product.masterProduct.ID : pdict.Product.ID;
		var avm = pdict.Product.availabilityModel;
		pdict.available = avm.availability>0;

		var availableCount = "0";
		if (pdict.available && !empty(avm.inventoryRecord)) {
			availableCount = avm.inventoryRecord.perpetual ? "999" : avm.inventoryRecord.ATS.value.toFixed().toString();
		}
	</isscript>
	<iscomment>
		primary details
		=============================================================
	</iscomment>

	<h2 class="visually-hidden">Details</h2>
	<span class="visually-hidden" itemprop="url">${URLUtils.http('Product-Show','pid', pdict.Product.ID)}</span>

	
	
	<iscomment>
		Key Features attribute bullets
		=============================================================
	</iscomment>
		
		
		
	<div class="product-key-features">
		<p class="block-paragraph-head">Features</p>
			<ul>
				<isloop items="${pdict.Product.getAttributeModel().getAttributeGroup('pdpFeatures').getAttributeDefinitions()}" var="productFeaturesDefinition" status="loopstate1">
					<isset name="currentFeature" value="${pdict.Product.getAttributeModel()}" scope="page"/>
					<isif condition="${!empty(currentFeature.getDisplayValue(productFeaturesDefinition))}" />
						<li><isprint value="${currentFeature.getDisplayValue(productFeaturesDefinition)}" /></li>
					</isif>
				</isloop>
			</ul>
	<isif condition="${!isQuickView}">		
		<a href="#pdp_tabs" class="read-more">Read More</a>
	</isif>	
	</div>
	<iscomment>
		Delivery Zip Form
		=============================================================
	</iscomment>
	<isinclude url="${URLUtils.url('RemoteInclude-RenderTemplate','template', 'product/components/deliveryzip')}">
	<iscomment>
		variations
		=============================================================
	</iscomment>

	<isinclude template="product/components/variations"/>

	<iscomment>
		add to cart form
		=============================================================
	</iscomment>
	
   	<isinclude template="product/components/pricing"/>

	<form action="${URLUtils.continueURL()}" method="post" id="${pdict.CurrentForms.product.addtocart.dynamicHtmlName}" class="pdpForm">
		<fieldset>
			<iscomment>
				product options (Must be inside form)
				=============================================================
			</iscomment>

			<h2 class="visually-hidden">Add to cart options</h2>
			<isinclude template="product/components/options"/>

			<iscomment>
				product promotions
				=============================================================
			</iscomment>
			<isset name="showTieredPrice" value="${false}" scope="page"/>
			<iscomment>
				<isinclude template="product/components/promotions"/>
			</iscomment>
			<div class="product-add-to-cart">
				<h2 class="visually-hidden">Product Actions</h2>

				<iscomment>
					availability
					=============================================================
				</iscomment>

				<isif condition="${pdict.Product.custom.availableForInStorePickup && dw.system.Site.getCurrent().getCustomPreferenceValue('enableStorePickUp')}">
					<div class="availability-storepickup">
						<div class="availability-web">
							<label for="Stock">${Resource.msg('product.availability','product',null)}</label>
							<isif condition="${!pdict.Product.master && !pdict.Product.variationGroup}">
								<span class="value"> <isinclude template="product/components/availability"/></span>
							<iselse/>
								<div class="availability-novariation">${Resource.msg('product.selectforstock','product',null)}</div>
							</isif>
							<a href="${URLUtils.url('Page-Show', 'cid', 'deliver-options-home')}" class="details" title="${Resource.msg('global.details','locale',null)}">${Resource.msg('global.details','locale',null)}</a>
	
						</div>
						<div class="availability-instore">
							<label for="Stock">${Resource.msg('product.instorestocknew','product',null)}</label>
							<isif condition="${empty(pdict.CurrentHttpParameterMap.uuid.value)}">
								<div id="${pdict.Product.ID}" class="availability-results availability-msg store-stock">
									<span class="label set-preferred-store"><a href="${URLUtils.url('StoreInventory-SetZipCodeCore','pid', pdict.Product.ID)}" title="${pdict.Product.name}">${Resource.msg('storelist.check.availablity','storepickup',null)}</a></span>
								</div>
							<iselse/>
								<div id="${pdict.CurrentHttpParameterMap.uuid.value}" class="availability-results store-stock"></div>
							</isif>
						</div>
					</div>
				<iselse/>
					<div class="availability-storepickup">
					<div class="availability-web">
						<label for="Stock">${Resource.msg('global.availability','locale',null)}:</label>
						<isif condition="${!pdict.Product.master && !pdict.Product.variationGroup}">
							<span class="value"><isinclude template="product/components/availability"/></span>
						<iselse/>
							<div class="availability-novariation">${Resource.msg('product.selectforstock','product',null)}:</div>
						</isif>
						<a href="${URLUtils.url('Page-Show', 'cid', 'deliver-options-home')}" class="details" title="${Resource.msg('global.details','locale',null)}">${Resource.msg('global.details','locale',null)}</a>
	
					</div>
					</div>
				</isif>

				<iscomment>
					product pricing
					=============================================================
				</iscomment>

				<isset name="showTieredPrice" value="${true}" scope="page"/>
				<ispdppricing salefirst=""/>

				<isset name="pam" value="${pdict.Product.getAttributeModel()}" scope="page"/>
				<isset name="group" value="${pam.getAttributeGroup('mainAttributes')}" scope="page"/>
				<isinclude template="product/components/group"/>

				<iscomment>
					product quantity
					=============================================================
				</iscomment>

				<div class="add-to-cart-container">
					<div class="inventory">
						<div class="quantity">
							<label for="Quantity">${Resource.msg('global.qty','locale',null)}:</label>
							<input type="text" class="input-text" name="Quantity" id="Quantity" size="2" maxlength="3" value="${Number(empty(pdict.CurrentHttpParameterMap.Quantity.stringValue) ? 1 : pdict.CurrentHttpParameterMap.Quantity.stringValue).toFixed()}" data-available="${availableCount}"/>
						</div>
					</div>

					<iscomment>
						add to cart submit
						=============================================================
					</iscomment>

					<isscript>
						var updateSources = ["cart", "giftregistry", "wishlist"];
						var source = pdict.CurrentHttpParameterMap.source.stringValue;
						var buttonTitle = dw.web.Resource.msg('global.addtocart','locale','Add to Cart');
						var plid = null;
						if( updateSources.indexOf(source)>-1 ){
							buttonTitle = dw.web.Resource.msg('global.update','locale','Update');
							if( pdict.CurrentHttpParameterMap.productlistid && pdict.CurrentHttpParameterMap.productlistid.stringValue ) {
								plid = pdict.CurrentHttpParameterMap.productlistid.stringValue;
							}
						} else {
							// Only pass on white-listed sources
							source = null;
						}
					</isscript>

					<isset name="cartAction" value="add" scope="page"/>

					<isif condition="${pdict.CurrentHttpParameterMap.uuid.stringValue}">
						<input type="hidden" name="uuid" id="uuid" value="${pdict.CurrentHttpParameterMap.uuid.stringValue}" />
						<isset name="cartAction" value="update" scope="page"/>
					</isif>
					<isif condition="${source}">
						<input type="hidden" name="source" id="source" value="${source}" />
					</isif>
					<isif condition="${plid}">
						<input type="hidden" name="productlistid" id="productlistid" value="${plid}" />
					</isif>
					<input type="hidden" name="cartAction" id="cartAction" value="${cartAction}" />
					<input type="hidden" name="pid" id="pid" value="${pdict.Product.ID}" />
					<isset name="disabledAttr" value="${pdict.available && !pdict.Product.master && !pdict.Product.variationGroup && !(!pdict.AvailInZone && session.custom.customerZip) ? '' : ' disabled="disabled"'}" scope="page"/>
					<isif condition="${disabledAttr.length == 0}">
						<button id="add-to-cart" type="submit" title="${buttonTitle}" value="${buttonTitle}" class="button-fancy-large add-to-cart">${buttonTitle}</button>
					<iselse/>
						<isscript>
							var pvm : dw.catalog.ProductVariationModel = pdict.Product.getVariationModel();
							var it : dw.util.Iterator = pvm.getProductVariationAttributes().iterator();
							var array : Array = [];
							var options = '';
							var requiredOptions = '';
							while( it.hasNext() ) {
								var text : dw.object.ObjectAttributeDefinition = it.next();
								array.push( text.displayName );
							}
							options = array.join(', ');
							var lastIndex = options.lastIndexOf(',');
							if( lastIndex > 0 && options.length > 1 && array.length > 1) {
							 requiredOptions = options.substr(0,lastIndex) + ' ' + dw.web.Resource.msg('product.attributedivider', 'product', null) + options.substr(lastIndex+1, options.length);
							} else {
							 requiredOptions = options;
							}
							var buttonTitleDisabledSelectVariation = StringUtils.format(dw.web.Resource.msg('product.missingval','product', null), requiredOptions);
						</isscript>
						<button id="add-to-cart" type="button" title="${buttonTitleDisabledSelectVariation}" value="${buttonTitleDisabledSelectVariation}" class="button add-to-cart-disabled"<isprint value="${disabledAttr}" encoding="off"/>>${buttonTitle}</button>
					</isif>
					</div><!--  end add to cart container -->
				</div><!--  end details block -->
			</fieldset>
		</form>

	<iscomment>
		product actions
		=============================================================
	</iscomment>

	<div class="product-actions">
		<isscript>
			var Url = require('~/cartridge/scripts/util/Url');
			pdict.url = Url.getCurrent(pdict);
			pdict.title = pdict.Product.name;
		</isscript>
		<iscomment>
		<isif condition="${pdict.available && !pdict.Product.bundle && !(pdict.Product.master || pdict.Product.variationGroup)}">
			<a class="button simple" data-action="wishlist" href="${URLUtils.https('Wishlist-Add', 'pid', pdict.Product.ID, 'source', 'productdetail')}" title="${Resource.msg('global.addtowishlist.label','locale',null)}">${Resource.msg('global.addtowishlist','locale',null)}</a>
			<a class="button simple" data-action="gift-registry" href="${URLUtils.https('GiftRegistry-AddProduct', 'pid', pdict.Product.ID, 'source', 'productdetail')}" title="${Resource.msg('global.addtogiftregistry.label','locale',null)}">${Resource.msg('global.addtogiftregistry','locale',null)}</a>
		</isif>
		</iscomment>
		
		<iscomment>PDP Omni-Channel Section asset include</iscomment>
		<isset name="omni_channel" value="${dw.content.ContentMgr.getContent('pdpOmniChannel')}" scope="page"/>
		<isif condition="${omni_channel != null}">
			<div class="omni-channel-container">
				 <isprint value="${omni_channel.custom.body}" encoding="off"/>
			</div>
		</isif>
		
		
		<div class="socialsharing">
			<isinclude template="components/socialsharing"/>
		</div>

	</div><!--  end details block -->
	<iscomment>This is ugly, but it works until we can get a better solution</iscomment>
	<isif condition="${pdict.GetImages}">
		<div id="update-images" style="display:none">
			<isinclude template="product/components/productimages"/>
		</div>
	</isif>
</isif>
