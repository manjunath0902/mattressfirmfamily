/**
importScript('search/ProductGridUtils.ds');
*/
importPackage ( dw.system );
importPackage ( dw.util );
importPackage ( dw.catalog );
importPackage ( dw.web );
importPackage ( dw.value );
importPackage ( dw.object );


/**
*	Provides functions to make work around the product grid easier and better readable
*	@namespace
*
*/
function ProductGridUtils(){}

/*
* Gets a list of variation attribute values, which are referenced by a given product search hit
* @param productHit {ProductSearchHit} the product search hit, given by the Scripting API
* @param variationAttributeID {String} ID of the varitiation attribute
*/
ProductGridUtils.getRepresentedVariationValueIDs = function (productHit, variationAttributeID) {
	var representedColors = [];
	var colorValues = productHit.getRepresentedVariationValues(variationAttributeID);
	for each (var colorValue in colorValues) {
		representedColors.push(colorValue.ID);
	}

	return representedColors;
};

/*
* Build the URL used for the product grid to include a hit tile.
* Use with care, as this is essential to the caching strategy. It should just use parameters gathered from the search hit.
* @param productHit {dw.catalog.ProductSearchHit} OR {dw.catalog.Product} the product search hit, given by the Scripting API
* @param cgidValue {String}
*/
ProductGridUtils.buildHitTileURL = function (productHit, cgidValue, abParam) {
	
	
	// productHit.product is 'object' for type dw.catalog.ProductSearchHit, 'boolean' for type dw.catalog.Product
	var product : Product = productHit;
	if (typeof productHit.product === 'object') {
		product = productHit.getProduct();
	}
	var productId    : String  = product.getID();
	var minPrice     : Money   = product.getPriceModel().getMinPrice();
	var maxPrice     : Money   = product.getPriceModel().getMaxPrice();
	var isMaster     : Boolean = product.isMaster();
	var isPriceRange : Boolean = product.getPriceModel().isPriceRange();
	var variants     : Array   = (isMaster) ? product.getVariants() : [product];

	
	var productHitURL = URLUtils.url('Product-HitTile');
	productHitURL.append('pid', productId);
	if (!empty(cgidValue)){
		productHitURL.append('cgid', cgidValue);
	}
	productHitURL.append('maxprice', maxPrice);
	productHitURL.append('minprice', minPrice);
	
	var prices = [];
	for each (var variant in variants) {
		if (variant.online) {
			var prodPrice = {};
			prodPrice.price = variant.priceModel.price;
			
			//Make sure the price is available
			if(variant.priceModel.price.available) {
				var priceBook =  variant.getPriceModel().priceInfo.priceBook;
	            while (priceBook.parentPriceBook) {
	            	priceBook = priceBook.parentPriceBook ? priceBook.parentPriceBook : priceBook; 
				}
				StandardPrice = variant.getPriceModel().getPriceBookPrice(priceBook.ID);
	                     
				prodPrice.standardPrice = StandardPrice;			
			
				prodPrice.id = variant.ID;
				prices.push(prodPrice);
			}
		}
			
	}
	
	if (prices.length > 0) {
		prices.sort(function (a,b) {
			return a.price - b.price || a.standardPrice - b.standardPrice;
		});
		productHitURL.append('vpidmin', prices[0].id);
		productHitURL.append('vpidmax', prices[prices.length - 1].id);
	}

	/*
		Relies on the fact that the represented product is a variant if color slicing is enabled
	 	See script API doc @ProductSearchHit.product
	*/
	productHitURL.append('showswatches', isMaster);
	productHitURL.append('showpricing', true);
	productHitURL.append('showpromotion', true);
	productHitURL.append('showrating', true);
	productHitURL.append('showcompare', true);
	productHitURL.append('pricerange', isPriceRange);
	if(!empty(abParam)){
		productHitURL.append('plp_tiles', abParam);
	}
	
	return productHitURL;
};
/*
* Build the URL used for the product grid to include a hit tile.
* Use with care, as this is essential to the caching strategy. It should just use parameters gathered from the search hit.
* @param productHit the product search hit, given by the Scripting API
*/
ProductGridUtils.getCompareClass = function (category) {
	if (category && 'enableCompare' in category.custom && category.custom.enableCompare) {
		return 'show-compare';
	} else {
		return 'hide-compare';
	}
};

/*
* Checks if search is being performed for Mattresses only
* @param productSearchResults
*/
ProductGridUtils.isMattressCategory = function (category) {
	if (category && 'isMattressCateogry' in category.custom && category.custom.isMattressCateogry) {
		return true;
	}
	return false;
};

/*
* Build the URL used for the product grid to include a hit tile.
* Use with care, as this is essential to the caching strategy. It should just use parameters gathered from the search hit.
* @param productHit the product search hit, given by the Scripting API
*/
ProductGridUtils.clearanceSearch = function (productSearchModel) {
	var StoreMgr = require('dw/catalog/StoreMgr');
	var Countries = require('app_storefront_core/cartridge/scripts/util/Countries');
	
	var geolocationZip = request.getGeolocation().getPostalCode();
	var existingZipCode = session.custom.customerZip;
	var zipCode = geolocationZip;
	if(existingZipCode) {
		zipCode = existingZipCode; 
	}
	var countryCode = Countries.getCurrent({
		CurrentRequest: {
				locale: request.locale
			}
		}).countryCode;
	var distanceUnit = (countryCode == 'US') ? 'mi' : 'km';
	var zipInfo = dw.object.CustomObjectMgr.getCustomObject('ZipInfo', zipCode);
	var warehouseZipInfo = zipInfo.custom.warehouseId;
	var storeMap = StoreMgr.searchStoresByPostalCode(countryCode, zipCode, distanceUnit, null);
	var store, warehouseAssociationFlag = false;
	for (var i = 0; i < storeMap.entrySet().length; i++) { 
		store = storeMap.entrySet()[i];
		if(!empty(store)) {
			store = store.key;
			if(warehouseZipInfo && warehouseZipInfo.length > 0){
					for( var j=0; j < warehouseZipInfo.length; j++){
						if(!empty(store.custom.warehouseId) && store.custom.warehouseId == warehouseZipInfo[j]) {
						warehouseAssociationFlag = true;
						break;
					}
				}
				
			}
			
		}
		if(warehouseAssociationFlag == true){
			break;
		}
		
	}
	
	if(!empty(store) && warehouseAssociationFlag) {
		//var warehouseAssociationID = store.custom.warehouseId;
		var warehouseAssociationID = warehouseZipInfo.join('|');
		var productSearchModel : ProductSearchModel = productSearchModel;//new ProductSearchModel();
		//productSearchModel.setRecursiveCategorySearch(true);
		//productSearchModel.setCategoryID("clearance-and-overstock");
		productSearchModel.setOrderableProductsOnly(true);
		productSearchModel.setRefinementValues("warehouseIds", warehouseAssociationID);
		productSearchModel.search();
		
		if (productSearchModel.emptyQuery) {
        	response.redirect(URLUtils.abs('Home-Show'));
	    } 
	    else if (productSearchModel.count > 0) {
	
	        if ((productSearchModel.count > 1) || productSearchModel.refinedSearch || (contentSearchModel.count > 0)) {
	            var productPagingModelNew = new PagingModel(productSearchModel.productSearchHits, productSearchModel.count);                	
	            productPagingModelNew.setPageSize(30);                                
	
	            if (productSearchModel.category) {
	                //require('~/cartridge/scripts/meta').update(productSearchModel.category,productSearchModel);
	            }
	
	            return {
                    ProductSearchResult: productSearchModel,
                    ContentSearchResult: null,
                    ProductPagingModel: productPagingModelNew,
                    SearchPromo: 		null,
                    NoIndexOverride: 	false,
                    Template:	 		'rendering/category/categoryproducthits.isml',
                    Count:				productSearchModel.count,
                    EmptyClearance:		false
                };
	        }
	    } 
	    else {
	        return {
	            ProductSearchResult: productSearchModel,
	            Template: 'search/emptyclearance.isml',
	            EmptyClearance: true
	        };
	    }
	}
	else {
        return {
            ProductSearchResult: productSearchModel,
            Template: 'search/emptyclearance.isml',
            EmptyClearance: true
        };
    }
};
