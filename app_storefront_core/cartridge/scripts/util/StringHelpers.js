'use strict';

/**
 * Sanitize a string by removing the whitespaces
 *
 * @param inS String to sanitize
 *
 **/
function sanitize(inS) {
    return inS.replace(/\W/g, '');
}

/**
 * unsanitizeOR a string by replaced %7c with '|' pipes
 *
 * @param anURL URL String to sanitize
 *
 **/
function unsanitizeOR(anURL) {
    return anURL.toString().replace('%7c', '|', 'g');
}

/**
 * cleanupID cleans a product id
 *
 * @param a a String to cleanup
 *
 **/
function cleanupID(s) {
    return (s === null) ? s : s.replace(new RegExp('[^a-z0-9_\-]', 'gi'), '_').toLowerCase();
}

/**
 * detect device type
 *
 * @param httpUserAgent
 *
 **/
function isMobileDevice(httpUserAgent) {
	
	var isMobile = false;	
    var iPhoneDevice : String = "iPhone";    
    var andriodDevice : String = "Android";
    var blackberry : String ="BlackBerry";
    var opera : String ="Opera";
    var windows: String="Windows";
    var symbian: String="Symbian";

    if (httpUserAgent.indexOf(iPhoneDevice) > 1) {
    	
    	isMobile = true;
    	
    } else if (httpUserAgent.indexOf(andriodDevice) > 1) {
    	
    	if (httpUserAgent.toLowerCase().indexOf("mobile") > 1) 
	    	isMobile = true;    

    } else if (httpUserAgent.indexOf(blackberry) > 1) {
    	
    	if (httpUserAgent.toLowerCase().indexOf("mobile") > 1) 
	    	isMobile = true;	    
        
    } else if (httpUserAgent.indexOf(opera) > 1) {
    	
    	if (httpUserAgent.toLowerCase().indexOf("mobi") > 1) 
	    	isMobile = true;
	    	
    } else if (httpUserAgent.indexOf(windows) > 1) {
    	
    	if (httpUserAgent.toLowerCase().indexOf("mobile") > 1) 
	    	isMobile = true;
    
    }else if (httpUserAgent.indexOf(symbian) > 1) {
    	   	isMobile = true;
    
    }
    
   return isMobile;

}
/**
 * format phone number in format (XXX) XXX-XXXX
 *
 * @param a a String to format 
 *
 **/
function formatPhoneNumber(phone) {
	 var tmp = (""+phone).replace(/\D/g, '');
	 var m = tmp.match(/^(\d{3})(\d{3})(\d{4})$/);
	 return (!m) ? "" : "(" + m[1] + ") " + m[2] + "-" + m[3];
};

/**
 * format phone number in format XXX-XXX-XXXX
 *
 * @param a a String to format 
 *
 **/
function formatAXPhoneNumber(phone) {
	 var tmp = (""+phone).replace(/\D/g, '');
	 var m = tmp.match(/^(\d{3})(\d{3})(\d{4})$/);
	 return (!m) ? "" : m[1] + "-" + m[2] + "-" + m[3];
};

module.exports.sanitize = sanitize;
module.exports.unsanitizeOR = unsanitizeOR;
module.exports.cleanupID = cleanupID;
module.exports.formatPhoneNumber = formatPhoneNumber;
module.exports.formatAXPhoneNumber = formatAXPhoneNumber;
module.exports.isMobileDevice = isMobileDevice;
