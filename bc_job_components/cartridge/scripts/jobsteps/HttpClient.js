'use strict';
importPackage( dw.net );
var Logger = require('dw/system/Logger').getLogger('cs.job.HttpClient');

/**
 * Execute a simple http request
 *
 * Job Parameters:
 *
 *   URL: request url
 *   Method: request method (get, post, put...)
 *   Timeout: timeout in milliseconds
 */

var File = require('dw/io/File');
var Status = require('dw/system/Status');

var FileHelper = require('~/cartridge/scripts/file/FileHelper');
var ServiceMgr = require('~/cartridge/scripts/services/ServiceMgr');
var StepUtil = require('~/cartridge/scripts/util/StepUtil');

/**
 * The main function.
 *
 * @returns {dw.system.Status} The exit status for the job step
 */
var run = function run() {
    var args = arguments[0];
    var url : String = args.URL;
    var method : String = args.Method
    var timeout : Long = args.Timeout
    var httpClient : HTTPClient = new HTTPClient();
    httpClient.open(method, url);
    httpClient.setTimeout(timeout);
    httpClient.send();

    Logger.info("http status code: {}", httpClient.getStatusCode());
    Logger.info("response body: {}", httpClient.getText());
    
    if (httpClient.statusCode == 200)
    {
        Logger.info("success");
    }
    else
    {
        return new Status(Status.ERROR, 'ERROR', 'Request Failed');
    }
 

    return new Status(Status.OK, 'OK', 'Request Sent.');
};

exports.Run = run;
