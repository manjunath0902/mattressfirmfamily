![BazaarVoice.png](https://bitbucket.org/repo/kkqEEe/images/407219294-BazaarVoice.png)
### Bazaarvoice LINK Cartridge ###

* **Description:** Bazaarvoice helps all types of businesses capture, display, share, and analyze customer conversations online. Our combination of technology and personalized services help brands build online communities that drive measureable business goals.  We make it easy for you to let your customers share opinions, knowledge, and experiences.
* **Categories:** Social, Ratings and Reviews
* **Version:** 16.1.1
* **Last Certification Date:** 2-NOV-2016
* **Controller Friendly:** **YES**
* [Installation Documentation](https://bitbucket.org/demandware/link_bazaarvoice/src/db0712bd071a956aa837bd0b6a0c65544dfdc5be/documentation/?at=master)

### Contribution guidelines ###
There are two ways you can contribute to this project:

1. File an `Issue` using the BitBucket `Issues` facility in the Navigation Menu.  There are no guarantees that issues that are filed will be addressed or fixed within a certain time frame, but logging issues is an important step in improving the quality of these integrations.

2. If you have a suggested code fix, please fork this repository and issue a 'pull request' against it.  The LINK partner will evaluate the pull request, test it, and possibly integrate it back into the main code branch.  Even if the LINK partner does not choose to adopt your pull request, you've still helped the community because the pull request is now logged with this repository where other customers, clients, and integrators can see it and any of them can choose to adopt your suggested changes.

Thank you for helping improve the quality of this cartridge!

### Contact ###

* <partner-support@bazaarvoice.com>