/**
* Personali Integration
*
* @module  controllers/Personali
*/

'use strict';
var app = require('app_storefront_controllers/cartridge/scripts/app');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var BasketMgrObject = require('dw/order/BasketMgr');

// HINT: do not put all require statements at the top of the file
// unless you really need them for all functions

/**
* Create Personali Offer dataLayer on PDP
*/
function productOfferButton() {
	var ProductMgrObject = require('dw/catalog/ProductMgr');
	var params = request.httpParameterMap;
    var productResult = ProductMgrObject.getProduct(params.pid);
	var getBasketResult = BasketMgrObject.getCurrentOrNewBasket();
    
    app.getView({
        Product: productResult,
        Basket: getBasketResult,
    }).render('personali/productofferbutton');
}

/**
* Create Personali Offer dataLayer on Cart
*/
function cartRescue() {
	var GetBasketResult = BasketMgrObject.getCurrentOrNewBasket();
	
	app.getView({
		Basket: GetBasketResult,
	}).render('personali/cartrescue');
}

/**
* Create Personali Offer dataLayer on Order Confirmation Page
*/
function orderConfirmationTracking() {
	var OrderMgrObject = require('dw/order/OrderMgr');
	var orderNo : String = request.httpParameterMap.orderNo;
	var order : dw.order = OrderMgrObject.getOrder(orderNo);
	
	app.getView({
		Order: order,
	}).render('personali/orderconfirmtracking');
}

exports.ProductOfferButton = guard.ensure(['get'], productOfferButton);
exports.CartRescue = guard.ensure(['get'], cartRescue);
exports.OrderConfirmationTracking = guard.ensure(['get'], orderConfirmationTracking);